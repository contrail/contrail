package org.ow2.contrail.federation.federationapi.identityprovider;

import org.apache.log4j.Logger;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * Calls federation identity provider REST API for user identity management.
 * 
 * @author ales
 *
 */
public class FederationIdentityProvider {

	protected static Logger logger =
		Logger.getLogger(FederationIdentityProvider.class);

	public static String CONTENT_TYPE_APP_JSON = "application/json";

	protected static ClientConfig config ;
	protected static Client client ;
	protected static WebResource service ;
	protected static String uri;
	protected static Boolean enabled = false;
	
	/**
	 * Code for posting the resource.
	 * 
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static ClientResponse post(String path, String content) throws Exception{
		if(!enabled)return null;
		logger.debug("Entering post");
		ClientResponse response = service.path(path).type(CONTENT_TYPE_APP_JSON).post(ClientResponse.class, content);
		if (!(response.getStatus() == 201)) {
			logger.error(String.format("HTTP error: %s",response.getStatus()));
			throw new Exception("Failed : HTTP error code : "
			     + response.getStatus());			
		}
		logger.debug(String.format("Got response: %s", response.getHeaders().toString()));
		logger.debug("Exiting post");
		return response;
	}
	
	public static ClientResponse put(String path, String content) throws Exception{
		if(!enabled)return null;
		logger.debug("Entering put");
		ClientResponse response = service.path(path).type(CONTENT_TYPE_APP_JSON).put(ClientResponse.class, content);
		if (! (response.getStatus() == 201 || response.getStatus() == 204)) {
			logger.error(String.format("HTTP error: %s",response.getStatus()));
			throw new Exception("Failed : HTTP error code : "
			     + response.getStatus());			
		}
		logger.debug(String.format("Got response: %s", response.getHeaders().toString()));
		logger.debug("Exiting put");
		return response;
	}
	
	public static ClientResponse delete(String path) throws Exception{
		if(!enabled)return null;
		logger.debug("Entering delete");
		ClientResponse response = service.path(path).type(CONTENT_TYPE_APP_JSON).delete(ClientResponse.class);
		if (! (response.getStatus() == 201 || response.getStatus() == 204)) {
			logger.error(String.format("HTTP error: %s",response.getStatus()));
			logger.debug("I have tried to access " + path);
			throw new Exception("Failed : HTTP error code : "
			     + response.getStatus());			
		}
		logger.debug(String.format("Got response: %s", response.getHeaders().toString()));
		logger.debug("Exiting delete");
		return response;
	}
	
	
	public static void init(String accessPoint) throws Exception {
		logger.debug("Entering init");				
		uri = accessPoint.trim();
		if(!uri.endsWith("/"))
			uri=uri+"/";
		logger.debug(String.format("federation id prov uri: %s", uri));
		config = new DefaultClientConfig();
		client = Client.create(config);
		service = client.resource(uri);
		logger.debug(String.format("Web resource %s created.", uri));
		enabled = true;
		logger.debug("Exiting init");
	}
	
	/**
	 * Test method for HTTP PUT/POST/DELETE
	 * @param args
	 */
	public static void main (String[] args) throws Exception{
		init("http://0.0.0.0:8888/federation-id-prov");
		String content = "{\"attribute\":\"value\"}";
		//post("users", content);
		//put("users/1", content);
		//delete("users/2");
	}

}
