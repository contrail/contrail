
/**
 * InvalidNegotiationIDExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.ow2.contrail.federation.federationapi.slas.negotiation;

public class InvalidNegotiationIDExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.InvalidNegotiationIDExceptionE faultMessage;

    
        public InvalidNegotiationIDExceptionException() {
            super("InvalidNegotiationIDExceptionException");
        }

        public InvalidNegotiationIDExceptionException(java.lang.String s) {
           super(s);
        }

        public InvalidNegotiationIDExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public InvalidNegotiationIDExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.InvalidNegotiationIDExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.InvalidNegotiationIDExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    