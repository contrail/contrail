package org.ow2.contrail.federation.federationapi.resources;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.mindrot.jbcrypt.BCrypt;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.*;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslator;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslatorImplNoOsgi;
import org.ow2.contrail.federation.federationdb.jpa.dao.*;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.slasoi.slamodel.primitives.STND;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Path("/users/{userUuid}")
public class UserResource {

    protected static Logger logger = Logger.getLogger(UserResource.class);

    private String userUuid;

    public UserResource(@PathParam("userUuid") String userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * Returns the JSON representation of the given user.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response get() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            String baseUri = RestUriBuilder.getUserUri(user);
            JSONObject json = new JSONObject();
            json.put("username", user.getUsername());
            json.put("firstName", user.getFirstName());
            json.put("lastName", user.getLastName());
            json.put("email", user.getEmail());
            json.put("password", user.getPassword());
            json.put("uuid", user.getUuid());
            json.put("attributes", baseUri + "/attributes");
            json.put("slas", baseUri + "/slas");
            json.put("slats", baseUri + "/slats");
            json.put("applications", baseUri + "/applications");
            json.put("ids", baseUri + "/ids");
            json.put("roles", baseUri + "/roles");
            json.put("groups", baseUri + "/groups");
            json.put("ovfs", baseUri + "/ovfs");
            json.put("providers", baseUri + "/providers");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    public Response delete() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(user);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.delete("users/" + user.getUserId());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject userData) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();

            if (userData.has("username"))
                user.setUsername((String) userData.get("username"));
            if (userData.has("firstName"))
                user.setFirstName((String) userData.get("firstName"));
            if (userData.has("attributes"))
                user.setAttributes((String) userData.get("attributes"));
            if (userData.has("lastName"))
                user.setLastName((String) userData.get("lastName"));
            if (userData.has("password")) {
                String plain_text_password = (String) userData.get("password");
                // Hash a password for the first time
                // gensalt's log_rounds parameter determines the complexity
                // the work factor is 2**log_rounds, and the default is 10
                String hashed = BCrypt.hashpw(plain_text_password, BCrypt.gensalt(12));
                user.setPassword(hashed);
            }
            if (userData.has("email"))
                user.setEmail((String) userData.get("email"));
            if (userData.has("uuid"))
                user.setUuid((String) userData.get("uuid"));

            em.getTransaction().commit();

            try {
                FederationIdentityProvider.put("users/" + user.getUserId(), userData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/attributes")
    public Response getAttributes() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            for (UserhasAttribute attribute : user.getUserhasAttributeList()) {
                String uri = String.format("%s/attributes/%s", RestUriBuilder.getUserUri(user),
                        attribute.getUserhasAttributePK().getAttributeUuid());
                Attribute attr = new AttributeDAO(em).findByUuid(attribute.getUserhasAttributePK().getAttributeUuid());
                JSONObject o = new JSONObject();
                o.put("name", attr.getName());
                o.put("uri", uri);
                json.put(o);
            }
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/attributes")
    public Response postAttribute(JSONObject attrData) throws Exception {
        logger.debug("Entring add user attribute.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserhasAttribute attribute = new UserhasAttribute();

            if (attrData.has("attributeUuid")) {
                attribute.setUserhasAttributePK(
                        new UserhasAttributePK(user.getUserId(), attrData.getString("attributeUuid")));
            }
            else {
                logger.error("The attribute 'attributeUuid' is missing.");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            if (attrData.has("value"))
                attribute.setValue((String) attrData.get("value"));

            attribute.setUser(user);

            if (attrData.has("referenceId")) {
                logger.debug("Setting referenceId");
                int referenceId = FederationDBCommon.getIdFromString(attrData.getString("referenceId"));
                attribute.setReferenceId(referenceId);
            }

            em.getTransaction().begin();
            em.persist(attribute);
            user.getUserhasAttributeList().add(attribute);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post(String.format("users/%d/attributes", user.getUserId()), attrData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%s", attribute.getUserhasAttributePK().getAttributeUuid()));
            return Response.created(resourceUri).build();
        }
        finally {
            logger.debug("Exiting add user attribute.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/ids")
    public Response getIds() throws Exception {
        User user = new UserDAO().findByUuid(userUuid);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONArray json = new JSONArray();
        String userUri = RestUriBuilder.getUserUri(user);
        for (UserhasidentityProvider idp : user.getUserhasidentityProviderList()) {
            String uri = String.format("%s/ids/%s", userUri,
                    idp.getUserhasidentityProviderPK().getIdpUuid());
            JSONObject o = new JSONObject();
            o.put("identity", idp.getIdentity());
            o.put("uri", uri);
            json.put(o);
        }
        return Response.ok(json.toString()).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/ids")
    public Response postId(JSONObject idData) throws Exception {
        logger.debug("Entering user postId");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserhasidentityProvider uid = new UserhasidentityProvider();
            if (!idData.has("identityProviderId")) {
                logger.error("identityProviderId not provided");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            // Get Idp
            IdentityProvider idp = new IdentityProviderDAO(em).findByUuid(idData.getString("identityProviderId"));
            if (idp == null) {
                logger.error("Identity Provider with id " + idData.getString("identityProviderId") + " not found.");
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            UserhasidentityProvider uhasidp = new UserhasIdentityProviderDAO(em).findById(
                    user.getUserId(), idp.getIdpUuid());
            if (uhasidp != null) {
                logger.debug("User already has this provider registered");
                // resource is already registered
                return Response.status(Response.Status.CONFLICT).build();
            }
            uid.setIdentityProvider(idp);
            uid.setUserhasidentityProviderPK(new UserhasidentityProviderPK());
            uid.getUserhasidentityProviderPK().setUserId(user.getUserId());
            uid.getUserhasidentityProviderPK().setIdpUuid(idp.getIdpUuid());
            if (idData.has("identity")) {
                uid.setIdentity(idData.getString("identity"));
            }
            else {
                //user identity is mandatory
                logger.error("missing identity - it is mandatory.");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            if (idData.has("attributes"))
                uid.setAttributes(idData.getString("attributes"));

            em.getTransaction().begin();

            user.getUserhasidentityProviderList().add(uid);
            idp.getUserhasidentityProviderList().add(uid);
            em.persist(uid);

            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post(String.format("users/%d/ids", user.getUserId()), idData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%s", uid.getUserhasidentityProviderPK().getIdpUuid()));
            logger.debug("Exiting post");
            return Response.created(resourceUri).build();
        }
        catch (Exception err) {
            logger.error(err.getMessage());
            return Response.serverError().build();
        }
        finally {
            logger.debug("Exiting user Ids");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /////// VOs

    public Response getVOs() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response postVOs(String content) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response getVO(String VOID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response putVO(String VOID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response deleteVO(String VOID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response getCEEs() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response postCEEs(String content) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response getCEE(String CEEID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response putCEE(String CEEID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response deleteCEE(String CEEID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Get user SLAs.
     */
    @GET
    @Produces("application/json")
    @Path("/slas")
    public Response getSLAs() throws Exception {
        logger.debug("Entering get SLAs");

        User user = new UserDAO().findByUuid(userUuid);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONArray uriList = new JSONArray();
        for (UserSLA sla : user.getUserSLAList()) {
            String uri = String.format("%s/slas/%d", RestUriBuilder.getUserUri(user), sla.getSLAId());
            JSONObject o = new JSONObject();
            o.put("sla", sla.getSla());
            o.put("uri", uri);
            uriList.put(o);
        }
        return Response.ok(uriList.toString()).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/slas")
    public Response postSLAs(JSONObject slaData) throws Exception {
        logger.debug("Entering user post SLAs");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserSLA sla = new UserSLA();
            if (slaData.has("sla"))
                sla.setSla(slaData.getString("sla"));
            if (slaData.has("content"))
                sla.setContent(slaData.getString("content"));
            if (slaData.has("templateurl"))
                sla.setTemplateURL(slaData.getString("templateurl"));
            if (slaData.has("slatId")) {
                org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate slat =
                        new UserSLATemplateDAO(em).findById(
                                FederationDBCommon.getIdFromString(slaData.getString("slatId")));
                if (slat != null) {
                    logger.debug("Found UserSLATemplate with id " + slat.getSlatId());
                    sla.setSLATemplateId(slat);
                    slat.getUserSLAList().add(sla);
                }
                else {
                    logger.error("Could not found SLATemplate with id: " + slaData.getString("slatId"));
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
            }
            sla.setUserId(user);

            em.getTransaction().begin();
            em.persist(sla);
            user.getUserSLAList().add(sla);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", sla.getSLAId()));
            logger.debug("Exiting post");
            return Response.created(resourceUri).build();
        }
        catch (Exception err) {
            logger.error(err.getMessage());
            return Response.serverError().build();
        }
        finally {
            logger.debug("Exiting user post SLAs");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/slats")
    public Response getSLAtemplates() throws Exception {
        User user = new UserDAO().findByUuid(userUuid);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONArray uriList = new JSONArray();
        for (UserSLATemplate slat : user.getUserSLATemplateList()) {
            JSONObject o = new JSONObject();
            o.put("name", slat.getName());
            o.put("uri", RestUriBuilder.getUserSlatUri(slat));
            uriList.put(o);
        }
        return Response.ok(uriList.toString()).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/slats")
    public Response createUserSLAT(JSONObject json) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserSLATemplate userSlat = null;
            String ovfLink;
            String userSlatUuid;
            try {
                userSlat = new UserSLATemplate();
                userSlat.setContent(json.getString("content"));
                userSlat.setName(json.getString("name"));
                userSlat.setUserId(user);

                //parse sla template and retrieve OVF link
                SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
                org.slasoi.slamodel.sla.SLATemplate slaSoiSLAT = slaTranslator.parseSlaTemplate(userSlat.getContent());
                STND ovfProperty = new STND("OVF_URL");
                ovfLink = slaSoiSLAT.getInterfaceDeclrs()[0].getPropertyValue(ovfProperty);
                userSlatUuid = slaSoiSLAT.getUuid().toString();

                if (json.has("url"))
                    userSlat.setUrl(json.getString("url"));
                if (json.has("slatId")) {
                    String slatId = json.getString("slatId");
                    SLATemplate resslat = new SLATemplateDAO(em).findById(
                            FederationDBCommon.getIdFromString(slatId));
                    if (resslat != null) {
                        logger.debug("Found SLATemplate with id " + resslat.getSlatId());
                        userSlat.setSlatId(resslat);
                        resslat.getUserSLATemplateList().add(userSlat);
                    }
                    else {
                        throw new Exception("Invalid slatId: " + slatId);
                    }
                }
            }
            catch (Exception e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
            }

            // retrieve the OVF content
            HttpClient client = new HttpClient();
            GetMethod getMethod = null;
            int status = 0;
            try {
                getMethod = new GetMethod(ovfLink);
                status = client.executeMethod(getMethod);
            }
            catch (Exception e) {
                throw new Exception(String.format("Failed to retrieve OVF file from '%s': %s", ovfLink, e.getMessage()));
            }
            if (status != 200) {
                throw new Exception(String.format(
                        "Failed to retrieve OVF file from %s. Status code: %d", ovfLink, status));
            }
            String ovfContent = getMethod.getResponseBodyAsString(100000);

            // create UserOvf
            UserOvf userOvf = new UserOvf();
            userOvf.setContent(ovfContent);
            userOvf.setName(userSlatUuid + "-OVF");
            userOvf.setAttributes("{}");;
            userOvf.setUserId(user);

            userSlat.setUserOvf(userOvf);

            em.getTransaction().begin();
            em.persist(userSlat);
            em.persist(userOvf);
            user.getUserSLATemplateList().add(userSlat);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", userSlat.getSLATemplateId()));
            return Response.created(resourceUri).build();
        }
        catch (Exception e) {
            logger.error(e);
            return Response.serverError().build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    public Response getStorages() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response postStorages(String content) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response getStorage(String VSTID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response putStorage(String VSTID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Response deleteStorage(String VSTID) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @GET
    @Produces("application/json")
    @Path("/applications")
    public Response getApplications() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            for (Application application : user.getApplicationList()) {
                String uri = String.format("%s/applications/%s", RestUriBuilder.getUserUri(user),
                        application.getUuid());
                JSONObject o = new JSONObject();
                o.put("name", application.getName());
                o.put("uri", uri);
                json.put(o);
            }
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Add application to a user.
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/applications")
    public Response postApplications(JSONObject appData) throws Exception {
        logger.debug("Entering user add application.");

        org.json.JSONObject attributes;
        try {
            if (!appData.has("attributes")) {
                throw new Exception("Required attribute 'attributes' is missing.");
            }
            else {
                attributes = appData.getJSONObject("attributes");
            }
            if (!attributes.has("userSLATemplateUrl")) {
                throw new Exception("Required attribute 'attributes.userSLATemplateUrl' is missing.");
            }
        }
        catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Application app = new Application();
            String uuid = UUID.randomUUID().toString();
            app.setUuid(uuid);
            if (appData.has("name"))
                app.setName(appData.getString("name"));
            else {
                logger.error("The application does not have a name attribute.");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            if (appData.has("deploymentDesc"))
                app.setDeploymentDesc((String) appData.get("deploymentDesc"));
            if (appData.has("applicationOvf"))
                app.setApplicationOvf((String) appData.get("applicationOvf"));
            app.setAttributes(attributes.toString());

            app.setState("registered");

            em.getTransaction().begin();
            em.persist(app);
            user.getApplicationList().add(app);
            em.getTransaction().commit();

            URI resourceUri = new URI(app.getUuid());
            return Response.created(resourceUri).build();
        }
        finally {
            logger.debug("Exiting user add application.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/roles")
    public Response getRoles() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray UriList = new JSONArray();
            for (URole role : user.getURoleList()) {
                String uri = String.format("/roles/%d", role.getRoleId());
                JSONObject o = new JSONObject();
                o.put("name", role.getName());
                o.put("uri", uri);
                UriList.put(o);
            }
            return Response.ok(UriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/roles")
    public Response postRole(JSONObject roleData) throws Exception {

        if (!roleData.has("roleId")) {
            logger.error("User role ID has to be provided.");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            String roleString = roleData.getString("roleId");
            int roleId = Integer.parseInt(roleString.substring(roleString.lastIndexOf("/") + 1));
            logger.debug("Got Role Id: " + roleId);
            URole newrole = new URoleDAO(em).findById(roleId);

            em.getTransaction().begin();
            user.getURoleList().add(newrole);
            newrole.getUserList().add(user);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post(String.format("users/%d/roles", user.getUserId()), roleData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%d", newrole.getRoleId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/roles/{roleId}")
    public Response getRole(@PathParam("roleId") int roleId) throws Exception {
        return new URoleResource(roleId).get();
    }

    /**
     * Deletes a user role.
     *
     * @return
     * @throws Exception
     */
    @DELETE
    @Path("/roles/{roleId}")
    public Response deleteUserRole(@PathParam("roleId") int roleId) throws Exception {
        logger.debug("Entering delete user role");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            URole role = new URoleDAO(em).findById(roleId);
            if (role == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            logger.debug(String.format("Deleting role %d for user %d", role.getRoleId(), user.getUserId()));

            em.getTransaction().begin();
            user.getURoleList().remove(role);
            role.getUserList().remove(user);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.delete(String.format("users/%d/roles/%d",
                        user.getUserId(), role.getRoleId()));
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting delete user role");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/groups")
    public Response getGroups() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray UriList = new JSONArray();
            for (UGroup group : user.getUGroupList()) {
                JSONObject o = new JSONObject();
                o.put("name", group.getName());
                o.put("uri", RestUriBuilder.getGroupUri(group));
                UriList.put(o);
            }
            return Response.ok(UriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/groups")
    public Response postGroups(JSONObject groupData) throws Exception {

        if (!groupData.has("groupId")) {
            logger.error("User groups ID has to be provided.");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            String groupString = groupData.getString("groupId");
            int groupId = Integer.parseInt(groupString.substring(groupString.lastIndexOf("/") + 1));
            logger.debug("Got Group Id: " + groupId);
            UGroup newgroup = new UGroupDAO(em).findById(groupId);
            if (newgroup == null) {
                String message = String.format("Group with id %d doesn't exist.", groupId);
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            em.getTransaction().begin();
            user.getUGroupList().add(newgroup);
            newgroup.getUserList().add(user);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post(String.format("users/%d/groups", user.getUserId()), groupData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%d", newgroup.getGroupId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Path("/groups/{groupId}")
    public Response removeUserGroup(@PathParam("groupId") int groupId) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            UGroup group = new UGroupDAO(em).findById(groupId);
            if (group == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            user.getUGroupList().remove(group);
            group.getUserList().remove(user);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.delete(String.format("users/%d/groups/%d", user.getUserId(), group.getGroupId()));
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }


    @GET
    @Produces("application/json")
    @Path("/ovfs")
    public Response getOvfs() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            for (UserOvf ovf : user.getUserOvfList()) {
                JSONObject o = new JSONObject();
                o.put("name", ovf.getName());
                o.put("uri", RestUriBuilder.getUserOvfUri(ovf));
                json.put(o);
            }
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/ovfs")
    public Response postOvf(JSONObject ovfjson) throws Exception {
        logger.debug("Entering post of users OVFs");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserOvf userovf = new UserOvf();
            if (ovfjson.has("name"))
                userovf.setName(ovfjson.getString("name"));
            if (ovfjson.has("attributes"))
                userovf.setAttributes(ovfjson.getString("attributes"));
            if (ovfjson.has("content"))
                userovf.setContent(ovfjson.getString("content"));
            if (ovfjson.has("providerOvfId")) {
                int providerOvfId = Integer.parseInt(ovfjson.getString("providerOvfId").substring(ovfjson.getString("providerOvfId").lastIndexOf("/") + 1));
                logger.debug("Got providerOvfId " + providerOvfId);
                Ovf providerOvf = new OvfDAO(em).findById(providerOvfId);
                if (providerOvf == null) {
                    return Response.status(Response.Status.BAD_REQUEST).entity("Invalid providerOvfId.").build();
                }
                userovf.setProviderOvfId(providerOvf);
            }
            else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Missing attribute providerOvfId.").build();
            }
            userovf.setUserId(user);

            em.getTransaction().begin();
            em.persist(userovf);
            user.getUserOvfList().add(userovf);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", userovf.getOvfId()));
            logger.debug("Exiting post of users application's OVF");
            return Response.created(resourceUri).build();
        }
        catch (Exception err) {
            logger.error(err.getMessage());
            return Response.serverError().build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/providers")
    public Response postProvider(JSONObject provData) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            if (!provData.has("providerId")) {
                return Response.status(Response.Status.BAD_REQUEST).entity("provider ID has to be provided.").build();
            }

            String provString = provData.getString("providerId");
            // Get Idp
            Provider idp = new ProviderDAO(em).findById(
                    FederationDBCommon.getIdFromString(provString)
            );
            if (idp == null) {
                String msg = "Identity Provider with id " + provString + " not found.";
                return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
            }

            em.getTransaction().begin();
            idp.getUserList().add(user);
            user.getProviderList().add(idp);
            em.getTransaction().commit();
            URI resourceUri = new URI(String.format("%s/providers/%d", RestUriBuilder.getUserUri(user),
                    idp.getProviderId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("/providers/{provId}")
    public Response deleteProvider(int provId) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Provider prov = new ProviderDAO(em).findById(provId);
            if (prov == null || !user.getProviderList().contains(prov)) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            user.getProviderList().remove(prov);
            prov.getUserList().remove(user);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Build a list of providers with their names.
     */
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/providers")
    public Response getProviders() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            JSONObject o = new JSONObject();
            for (Provider provider : user.getProviderList()) {
                String name = provider.getName();
                o.put("name", name);
                o.put("uri", RestUriBuilder.getProviderUri(provider));
                json.put(o);
            }
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Register the user with the federation core.
     *
     * @param user
     * @throws Exception
     */
    protected static List<String> registerUserWithTheCore(User user, EntityManager em) throws Exception {
        logger.debug("Entering registerUserWithTheCore");
        try {
            logger.debug("Registering user with the federation core.");
            Query providerQuery = em.createNamedQuery("Provider.findAll");
            List<Provider> providerList = providerQuery.getResultList();
            logger.debug("Trying to register the user with these providers: " + providerList);
            List<String> createdAtProviders = MyServletContextListener.getFederationCore().propagateUser(user, providerList);
            logger.debug("Federation core registered the user with these providers: " + createdAtProviders);
            if (createdAtProviders == null) {
                logger.error("User could not be registered with the federation core.");
                logger.debug("Exiting registering new user.");
                throw new Exception("User could not be registered with the federation core.");
            }
            logger.debug("Exiting registerUserWithTheCore");
            return createdAtProviders;
        }
        catch (Exception err) {
            logger.debug(err.getMessage());
            logger.error(FederationDBCommon.getStackTrace(err));
            logger.error("User could not be registered with the federation core.");
            throw new Exception("Could not register user with the federation core.");
        }
    }

    @PUT
    @Path("/propagate")
    public Response propagateUser() throws Exception {
        logger.debug("Entering propagateUser");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            List<String> createdAtProviders = registerUserWithTheCore(user, em);
            // TODO: Add users to Account table with provider.

            return Response.status(Response.Status.OK).build();
        }
        catch (Exception err) {
            logger.error("Error creating the user with providers.");
            logger.error(err.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/dashboard")
    public Response getDashboard() throws Exception {
        logger.debug("Entering GET dashboard");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject jsonDashboard = new JSONObject();

            JSONArray jsonTempArray = new JSONArray();
            for (URole role : user.getURoleList()) {
                JSONObject o = new JSONObject();
                o.put("name", role.getName());
                o.put("uri", RestUriBuilder.getRoleUri(role));
                jsonTempArray.put(o);
            }
            jsonDashboard.put("roles", jsonTempArray);

            jsonTempArray = new JSONArray();
            for (UserhasAttribute attribute : user.getUserhasAttributeList()) {
                JSONObject o = new JSONObject();
                o.put("name", attribute.getAttribute().getName());
                o.put("uri", RestUriBuilder.getUserAttrUri(attribute));
                jsonTempArray.put(o);
            }
            jsonDashboard.put("attributes", jsonTempArray);

            jsonTempArray = new JSONArray();
            for (UGroup group : user.getUGroupList()) {
                JSONObject o = new JSONObject();
                o.put("name", group.getName());
                o.put("uri", RestUriBuilder.getGroupUri(group));
                jsonTempArray.put(o);
            }
            jsonDashboard.put("groups", jsonTempArray);

            jsonTempArray = new JSONArray();
            for (UserhasidentityProvider idp : user.getUserhasidentityProviderList()) {
                String uri = String.format("%s/ids/%s", RestUriBuilder.getUserUri(user),
                        idp.getUserhasidentityProviderPK().getIdpUuid());
                JSONObject o = new JSONObject();
                o.put("identity", idp.getIdentity());
                o.put("uri", uri);
                jsonTempArray.put(o);
            }
            jsonDashboard.put("identities", jsonTempArray);

            jsonTempArray = new JSONArray();
            for (Application application : user.getApplicationList()) {
                String uri = String.format("%s/applications/%d", RestUriBuilder.getUserUri(user),
                        application.getApplicationId());
                JSONObject o = new JSONObject();
                o.put("name", application.getName());
                o.put("state", application.getState());
                o.put("slaUri", application.getSlaUrl());
                o.put("applicationOvf", application.getApplicationOvf());
                o.put("deploymentDesc", application.getDeploymentDesc());
                o.put("uri", uri);
                jsonTempArray.put(o);
            }
            jsonDashboard.put("applications", jsonTempArray);

            return Response.ok(jsonDashboard.toString()).build();
        }
        catch (Exception err) {
            logger.error("Error in dashboard:");
            logger.error(FederationDBCommon.getStackTrace(err));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(err.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("selection_criteria")
    public Response getSelectionCriteria() throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            TypedQuery<Object[]> q = em.createNamedQuery("UserSelectionCriterion.findByUserIdJoinSC", Object[].class);
            q.setParameter(1, user.getUserId());
            List<Object[]> resultList = q.getResultList();

            JSONArray scArray = new JSONArray();
            for (Object[] row : resultList) {
                String name = (String) row[0];
                Double defaultValue = (Double) row[1];
                Double value = (Double) row[2];

                JSONObject scObj = new JSONObject();
                scObj.put("name", name);
                scObj.put("value", (value != null) ? value : defaultValue);
                scObj.put("default", defaultValue);
                scArray.put(scObj);
            }

            return Response.ok(scArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Produces("application/json")
    @Path("selection_criteria")
    public Response updateSelectionCriteria(String content) throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            JSONArray scArray = new JSONArray(content);
            for (int i = 0; i < scArray.length(); i++) {
                JSONObject scObj = new JSONObject(scArray.getString(i));
                String name = scObj.getString("name");
                double value = scObj.getDouble("value");

                TypedQuery<SelectionCriterion> q = em.createNamedQuery("SelectionCriterion.findByName",
                        SelectionCriterion.class);
                q.setParameter("name", name);
                SelectionCriterion sc = null;
                try {
                    sc = q.getSingleResult();
                }
                catch (NoResultException e) {
                    throw new JSONException("Invalid selection criterion: " + name);
                }

                UserSelectionCriterion usc = new UserSelectionCriterion(user.getUserId(), sc.getScId());
                usc.setValue(value);
                em.merge(usc);
            }
            em.getTransaction().commit();
            return Response.noContent().build();
        }
        catch (JSONException e) {
            em.getTransaction().rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Path("oauth/access_tokens")
    public Response getAccessTokens(@Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/access_tokens", userUuid), httpServletRequest);
    }

    @GET
    @Path("oauth/access_tokens/{token}")
    public Response getAccessTokenInfo(@PathParam("token") String token,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/access_tokens/%s", userUuid, token), httpServletRequest);
    }

    @GET
    @Path("oauth/access_tokens/{token}/access_log")
    public Response getAccessTokenAccessLog(@PathParam("token") String token,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/access_tokens/%s/access_log", userUuid, token), httpServletRequest);
    }

    @GET
    @Path("oauth/access_log")
    public Response getAccessLog(@Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/access_log", userUuid), httpServletRequest);
    }

    @GET
    @Path("oauth/accesses_per_rs")
    public Response getAccessTokenInfo(@Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/accesses_per_rs", userUuid), httpServletRequest);
    }

    @GET
    @Path("oauth/trust/organizations")
    public Response getOrganizationsTrust(@Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations", userUuid), httpServletRequest);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("oauth/trust/organizations")
    public Response addOrganizationTrust(@Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations", userUuid), httpServletRequest);
    }

    @GET
    @Path("oauth/trust/organizations/{organizationId}")
    public Response getOrganizationTrust(@PathParam("organizationId") int organizationId,
                                         @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d", userUuid, organizationId),
                httpServletRequest);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("oauth/trust/organizations/{organizationId}")
    public Response updateOrganizationTrust(@PathParam("organizationId") int organizationId,
                                         @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d", userUuid, organizationId),
                httpServletRequest);
    }

    @DELETE
    @Path("oauth/trust/organizations/{orgId}")
    public Response deleteOrganizationTrust(@PathParam("orgId") int orgId,
                                         @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d", userUuid, orgId),
                httpServletRequest);
    }

    @GET
    @Path("oauth/trust/organizations/{orgId}/clients")
    public Response getClientsTrust(@PathParam("orgId") int orgId,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d/clients", userUuid, orgId),
                httpServletRequest);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("oauth/trust/organizations/{orgId}/clients")
    public Response addClientTrust(@PathParam("orgId") int orgId,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d/clients", userUuid, orgId),
                httpServletRequest);
    }

    @GET
    @Path("oauth/trust/organizations/{orgId}/clients/{clientId}")
    public Response getClientTrust(@PathParam("orgId") int orgId, @PathParam("clientId") int clientId,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d/clients/%d", userUuid, orgId, clientId),
                httpServletRequest);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("oauth/trust/organizations/{orgId}/clients/{clientId}")
    public Response updateClientTrust(@PathParam("orgId") int orgId, @PathParam("clientId") int clientId,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d/clients/%d", userUuid, orgId, clientId),
                httpServletRequest);
    }

    @DELETE
    @Path("oauth/trust/organizations/{orgId}/clients/{clientId}")
    public Response deleteClientTrust(@PathParam("orgId") int orgId, @PathParam("clientId") int clientId,
                                       @Context HttpServletRequest httpServletRequest) throws Exception {
        return forwardToAS(String.format("/owners/%s/trust/organizations/%d/clients/%d", userUuid, orgId, clientId),
                httpServletRequest);
    }

    private Response forwardToAS(String path, HttpServletRequest httpServletRequest) throws Exception {
        URI baseUri = Conf.getInstance().getAddressOAuthAS().resolve("admin/");
        Map<String, String> rewriteRules = new HashMap<String, String>();
        rewriteRules.put("\"\\/owners\\/" + userUuid, "\"/users/" + userUuid + "/oauth");

        Map<String, String> locationHeaderRewriteRules = new HashMap<String, String>();
        locationHeaderRewriteRules.put(baseUri + "owners/" + userUuid, "/users/" + userUuid + "/oauth");

        RestProxy restProxy = new RestProxy(baseUri, rewriteRules, locationHeaderRewriteRules);

        return restProxy.forward(path, httpServletRequest);
    }
}
