package org.ow2.contrail.federation.federationapi.opennebula;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xml.dtm.ref.DTMNodeList;
import org.ow2.contrail.federation.federationapi.opennebula.resources.CPU;
import org.ow2.contrail.federation.federationapi.opennebula.resources.Memory;
import org.ow2.contrail.federation.federationapi.opennebula.resources.OperatingSystem;
import org.ow2.contrail.federation.federationapi.opennebula.resources.VM;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class SimpleOvfParser {
	private static final String RESOURCE_TYPE_CPU = "3";
	private static final String RESOURCE_TYPE_MEM = "4";
	private static final String RESOURCE_TYPE_DISK = "17";
	
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private DocumentBuilder db;
	private Node docEle = null;
	private XPathFactory factory = XPathFactory.newInstance();
    private XPath xpath = factory.newXPath();
    
    public SimpleOvfParser(String ovf) {
    	if(ovf != null && !ovf.isEmpty()) {
    		try {
    			db = dbf.newDocumentBuilder();
    			Document dom = db.parse(new ByteArrayInputStream(ovf.getBytes()));
    			this.docEle = dom.getDocumentElement();
    		} catch (ParserConfigurationException e) {
    			e.printStackTrace();
    		} catch (SAXException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
   }
    
	public ArrayList<VM> getVms() {
		try {
			DTMNodeList nodes = (DTMNodeList) xpath.evaluate("VirtualSystem", this.docEle, XPathConstants.NODESET);
			ArrayList<VM> vms = new ArrayList<VM>();
			System.out.println("Parsing OVF document");
			for(int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				
				String name = null;
				Node nodeName = (Node) xpath.evaluate("Name", node, XPathConstants.NODE);
				name = nodeName.getTextContent();
				
				DTMNodeList virtualHardware = (DTMNodeList) xpath.evaluate("VirtualHardwareSection/Item", node, XPathConstants.NODESET);
				CPU c = null;
				Memory m = null;
				OperatingSystem os = null;
				System.out.println("Creating new virtual machine named \"" + name + "\"");
				for(int j=0; j<virtualHardware.getLength(); j++) {
					Node item = virtualHardware.item(j);
					Node rtype = (Node) xpath.evaluate("ResourceType", item, XPathConstants.NODE);
					if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_CPU)) {
						c = new CPU();
						Node instanceId = (Node) xpath.evaluate("InstanceID", item, XPathConstants.NODE);
						c.setId(Integer.parseInt(instanceId.getTextContent()));
						Node elementName = (Node) xpath.evaluate("ElementName", item, XPathConstants.NODE);
						c.setName(elementName.getTextContent());
						Node virtualQuantity = (Node) xpath.evaluate("VirtualQuantity", item, XPathConstants.NODE);
						c.setNumVirtualCpus(Integer.parseInt(virtualQuantity.getTextContent()));
						System.out.println("Setting number of VCPUs to " + virtualQuantity.getTextContent());
					}
					else if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_MEM)) {
						m = new Memory();
						Node instanceId = (Node) xpath.evaluate("InstanceID", item, XPathConstants.NODE);
						m.setId(Integer.parseInt(instanceId.getTextContent()));
						Node elementName = (Node) xpath.evaluate("ElementName", item, XPathConstants.NODE);
						m.setName(elementName.getTextContent());
						Node virtualQuantity = (Node) xpath.evaluate("VirtualQuantity", item, XPathConstants.NODE);
						m.setSize(Integer.parseInt(virtualQuantity.getTextContent()));
						System.out.println("Setting memory to " + virtualQuantity.getTextContent());
					}
					// XXX: Only first disk is OS disk
					else if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_DISK)) {
						os = new OperatingSystem();
						Node instanceId = (Node) xpath.evaluate("InstanceID", item, XPathConstants.NODE);
						os.setId(Integer.parseInt(instanceId.getTextContent()));
						Node elementName = (Node) xpath.evaluate("ElementName", item, XPathConstants.NODE);
						os.setName(elementName.getTextContent());
						Node hostResource = (Node) xpath.evaluate("HostResource", item, XPathConstants.NODE);
						os.setImageTemplateName(hostResource.getTextContent());
						System.out.println("Setting disk to " + hostResource.getTextContent());
					}
				}
				if(name != null && os != null && c != null && m != null) {
					vms.add(new VM(os, c, m, name));
				}
			}
			return vms;

		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
