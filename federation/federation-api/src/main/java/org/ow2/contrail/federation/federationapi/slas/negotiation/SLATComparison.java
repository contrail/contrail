package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SLATComparison {
    private String slatUUID1;
    private String slatUUID2;
    private HashMap<String, AgreementTerm> agreementTerms;

    public SLATComparison() {
        this.agreementTerms = new HashMap<String, AgreementTerm>();
    }

    public String getSlatUUID1() {
        return slatUUID1;
    }

    public void setSlatUUID1(String slatUUID1) {
        this.slatUUID1 = slatUUID1;
    }

    public String getSlatUUID2() {
        return slatUUID2;
    }

    public void setSlatUUID2(String slatUUID2) {
        this.slatUUID2 = slatUUID2;
    }

    public Map<String, AgreementTerm> getAgreementTerms() {
        return agreementTerms;
    }

    public AgreementTerm getAgreementTerm(String id) {
        return agreementTerms.get(id);
    }

    public void addAgreementTerm(AgreementTerm agreementTerm) {
        agreementTerms.put(agreementTerm.getAgreementTermId(), agreementTerm);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("slatUUID1", slatUUID1);
        o.put("slatUUID2", slatUUID2);

        JSONArray agreementTermsArr = new JSONArray();
        for (AgreementTerm term : agreementTerms.values()) {
            agreementTermsArr.put(term.toJSON());
        }
        o.put("agreementTerms", agreementTermsArr);

        return o;
    }

    public static class GuaranteedState {
        private String guaranteeId;
        private String qosTerm;
        private String qosTermUri;
        private String operator;
        private SLATAbstract.Threshold threshold1;
        private SLATAbstract.Threshold threshold2;
        private String target;
        private Status status;

        public GuaranteedState(String guaranteeId) {
            this.guaranteeId = guaranteeId;
        }

        public GuaranteedState(SLATAbstract.GuaranteedState guarantee, Status status) {
            guaranteeId = guarantee.getGuaranteeId();
            qosTerm = guarantee.getQosTerm();
            qosTermUri = guarantee.getQosTermUri();
            operator = guarantee.getOperator();
            target = guarantee.getTarget();
            this.status = status;
        }

        public String getGuaranteeId() {
            return guaranteeId;
        }

        public void setGuaranteeId(String guaranteeId) {
            this.guaranteeId = guaranteeId;
        }

        public String getQosTerm() {
            return qosTerm;
        }

        public void setQosTerm(String qosTerm) {
            this.qosTerm = qosTerm;
        }

        public String getQosTermUri() {
            return qosTermUri;
        }

        public void setQosTermUri(String qosTermUri) {
            this.qosTermUri = qosTermUri;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }

        public SLATAbstract.Threshold getThreshold1() {
            return threshold1;
        }

        public void setThreshold1(SLATAbstract.Threshold threshold1) {
            this.threshold1 = threshold1;
        }

        public SLATAbstract.Threshold getThreshold2() {
            return threshold2;
        }

        public void setThreshold2(SLATAbstract.Threshold threshold2) {
            this.threshold2 = threshold2;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("guaranteeId", guaranteeId);
            o.put("qosTerm", qosTerm);
            o.put("qosTermUri", qosTermUri);
            o.put("operator", operator);
            o.put("threshold1", (threshold1 != null) ? threshold1.toJSON() : JSONObject.NULL);
            o.put("threshold2", (threshold2 != null) ? threshold2.toJSON() : JSONObject.NULL);
            o.put("target", target);
            o.put("status", status);
            return o;
        }
    }

    public static class AgreementTerm {
        private String agreementTermId;
        private Map<String, GuaranteedState> guarantees;
        private Status status;

        public AgreementTerm(String agreementTermId) {
            this.agreementTermId = agreementTermId;
            guarantees = new HashMap<String, GuaranteedState>();
        }

        public String getAgreementTermId() {
            return agreementTermId;
        }

        public void setAgreementTermId(String agreementTermId) {
            this.agreementTermId = agreementTermId;
        }

        public Map<String, GuaranteedState> getGuarantees() {
            return guarantees;
        }

        public GuaranteedState getGuarantee(String id) {
            return guarantees.get(id);
        }

        public void addGuarantee(GuaranteedState guarantee) {
            guarantees.put(guarantee.getGuaranteeId(), guarantee);
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("agreementTermId", agreementTermId);

            JSONArray guaranteesArr = new JSONArray();
            for (GuaranteedState guaranteedState : guarantees.values()) {
                guaranteesArr.put(guaranteedState.toJSON());
            }
            o.put("guarantees", guaranteesArr);

            o.put("status", status);
            return o;
        }
    }

    static enum Status {
        NOT_CHANGED,
        CHANGED,
        ADDED,
        REMOVED
    }
}
