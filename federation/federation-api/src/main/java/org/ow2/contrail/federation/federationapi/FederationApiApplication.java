package org.ow2.contrail.federation.federationapi;

import org.ow2.contrail.federation.federationapi.resources.AttributesResource;
import org.ow2.contrail.federation.federationapi.resources.ProvidersResource;
import org.ow2.contrail.federation.federationapi.resources.RootResource;
import org.ow2.contrail.federation.federationapi.resources.UsersResource;
//import org.ow2.contrail.federation.federationapi.resources.attributes.ProfileTypesResource;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class FederationApiApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(ProvidersResource.class);
        classes.add(UsersResource.class);
        classes.add(AttributesResource.class);
        //classes.add(ProfileTypesResource.class);
        classes.add(RootResource.class);
        return classes;
    }
}
