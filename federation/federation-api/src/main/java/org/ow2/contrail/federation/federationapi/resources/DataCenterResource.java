package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/dcs/{dcId}")
public class DataCenterResource {
    private String providerUuid;
    private int dcId;

    public DataCenterResource(@PathParam("providerUuid") String providerUuid, @PathParam("dcId") int dcId) {
        this.providerUuid = providerUuid;
        this.dcId = dcId;
    }

    /**
     * Returns the JSON representation of the given data center.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getDC() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the given data center.
     *
     * @return
     */
    @PUT
    public Response updateDC() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all storages of the data center.
     *
     * @return
     */
    @GET
    @Path("/storages")
    @Produces("application/json")
    public Response getStorages() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new storage for the provider's data center.
     *
     * @return
     */
    @POST
    @Path("/storages")
    public Response createStorage() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param storageId
     * @return
     */
    @Path("/storages/{sid}")
    public StorageResource findStorage(@PathParam("sid") int storageId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all networks of the data center.
     *
     * @return
     */
    @GET
    @Path("/networks")
    @Produces("application/json")
    public Response getNetworks() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new network for the provider's data center.
     *
     * @return
     */
    @POST
    @Path("/networks")
    public Response createNetwork() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param networkId
     * @return
     */
    @Path("/networks/{nid}")
    public NetworkResource findNetwork(@PathParam("nid") int networkId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all clusters of the data center.
     *
     * @return
     */
    @GET
    @Path("/clusters")
    @Produces("application/json")
    public Response getClusters() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new cluster for the provider's data center.
     *
     * @return
     */
    @POST
    @Path("/clusters")
    public Response createCluster() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param clusterId
     * @return
     */
    @Path("/clusters/{cid}")
    public ClusterResource findCluster(@PathParam("cid") int clusterId) {
        throw new UnsupportedOperationException();
    }
}
