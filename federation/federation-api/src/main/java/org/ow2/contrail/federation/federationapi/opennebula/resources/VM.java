package org.ow2.contrail.federation.federationapi.opennebula.resources;


import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;
import org.ow2.contrail.federation.federationapi.opennebula.ApplicationProvisioner;

public class VM {
	private Float vmCap; // cap CPU to this value (0:1)
	private String name;
	private CPU cpu;
	private OperatingSystem os;
	private Memory memory;
	private String info;
	private String hostname;
	private String id; // Internal opennebula ID
	private VirtualMachine oneVm; // OpenNebula virtual machine representation
	
	public VM(OperatingSystem os, CPU cpu, Memory memory, String name) {
		this.setOs(os);
		this.cpu = cpu;
		this.memory = memory;
		this.name = name;
		this.vmCap = (float) 1;
	}
	
	public CPU getCpu() {
		return cpu;
	}
	
	public void setCpu(CPU cpu) {
		this.cpu = cpu;
	}
	
	public Memory getMemory() {
		return memory;
	}
	
	public void setMemory(Memory memory) {
		this.memory = memory;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public void provision(Client oneClient, String networkId) {
		if(this.cpu != null && this.os != null && this.memory != null && this.name != null) {
			try {
				OneResponse rc = VirtualMachine.allocate(
					oneClient,
					ApplicationProvisioner.COMMON_VM_TEMPLATE
						.replaceAll("vmNAME", this.name)
						.replaceAll("vmCAP", String.valueOf(this.vmCap))
						.replaceAll("vmVCPU", String.valueOf(this.cpu.getNumVirtualCpus()))
						.replaceAll("vmMEM", String.valueOf(this.memory.getSizeInMegabytes()))
						.replaceAll("vmDISKOS", this.os.getImageTemplateName())
						.replaceAll("vmNETWORK", networkId)
				);
				if(rc.isError()) {
					throw new Exception(rc.getErrorMessage());
			    }
				setId(rc.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}				
		}
	}

	
	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		if(hostname == null && this.oneVm != null) {
			OneVm oneVm = new OneVm(this.oneVm);
			String ip = oneVm.getVmIpAddress();
			hostname = "vm-" + ip.replace(".", "-");
		}
		return hostname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public VirtualMachine getOneVm() {
		return oneVm;
	}

	public void setOneVm(VirtualMachine vm) {
		this.oneVm = vm;
	}

	public void shutdown() {
		this.oneVm.shutdown();		
	}

	public OperatingSystem getOs() {
		return os;
	}

	public void setOs(OperatingSystem os) {
		this.os = os;
	}


}
