/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.OAuthHttpClient;
import org.ow2.contrail.federation.federationapi.exceptions.RestApiException;
import org.ow2.contrail.federation.federationapi.utils.Conf;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationapi.utils.oauth.OAuthATManager;
import org.ow2.contrail.federation.federationapi.utils.oauth.OAuthATManagerFactory;
import org.ow2.contrail.federation.federationdb.jpa.dao.ApplicationDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserOvfDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.VmDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ales
 */
@Path("/applications/{appUuid}")
public class ApplicationResource {

    protected static Logger logger = Logger.getLogger(ApplicationResource.class);
    private String appUuid;

    public ApplicationResource(@PathParam("appUuid") String appUuid) {
        this.appUuid = appUuid;
    }

    /**
     * Gets the info on application. If user is not given, list users who
     * use this application.
     */
    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject();
            json.put("applicationId", app.getApplicationId());
            json.put("uuid", app.getUuid());
            json.put("deploymentDesc", app.getDeploymentDesc());
            json.put("name", app.getName());
            json.put("applicationOvf", app.getApplicationOvf());
            json.put("attributes", app.getAttributes());
            json.put("state", app.getState());
            JSONArray arr = new JSONArray();
            for (UserOvf ovf : app.getUserOvfList()) {
                arr.put(RestUriBuilder.getUserOvfUri(ovf));
            }
            json.put("ovfs", arr);
            arr = new JSONArray();

            logger.debug("Listing all users who use this application.");
            for (User appuser : app.getUserList()) {
                arr.put(RestUriBuilder.getUserUri(appuser));
            }
            json.put("users", arr.toString());

            JSONArray providersArr = new JSONArray();
            for (Provider provider : app.getProviderList()) {
                providersArr.put(RestUriBuilder.getProviderUri(provider));
            }
            json.put("providers", providersArr);

            JSONArray vmsArr = new JSONArray();
            for (Vm vm : app.getVmList()) {
                vmsArr.put(RestUriBuilder.getVmUri(vm));
            }
            json.put("vms", vmsArr);

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    public Response delete() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(app);

            for (User appuser : app.getUserList()) {
                appuser.getApplicationList().remove(app);
                logger.debug("No user given, deleting for user " + appuser.getUserId());
            }
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject appData) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();

            if (appData.has("name"))
                app.setName(appData.getString("name"));
            if (appData.has("deploymentDesc"))
                app.setDeploymentDesc(appData.getString("deploymentDesc"));
            if (appData.has("applicationOvf"))
                app.setApplicationOvf(appData.getString("applicationOvf"));
            if (appData.has("attributes"))
                app.setAttributes(appData.getString("attributes"));
            if (appData.has("state"))
                app.setState(appData.getString("state"));
            if (appData.has("ovfs")) {
                logger.debug("got array of OVFs");
                JSONArray array = appData.getJSONArray("ovfs");
                logger.debug("Updating the list of ovfs for user.");

                for (UserOvf userovf : app.getUserOvfList()) {
                    userovf.getApplicationList().remove(app);
                }

                app.getUserOvfList().clear();
                for (int i = 0; i < array.length(); ++i) {
                    String str = array.getString(i);
                    int ovfid = Integer.parseInt(str.substring(str.lastIndexOf("/") + 1));
                    logger.debug("Got UserOvf id " + ovfid);
                    UserOvf userovf = new UserOvfDAO(em).findById(ovfid);
                    if (userovf == null) {
                        logger.error("Could not find userOvf with id " + ovfid);
                        throw new WebApplicationException(Response.Status.NOT_FOUND);
                    }
                    app.getUserOvfList().add(userovf);
                    userovf.getApplicationList().add(app);
                }

            }
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/ovfs")
    public Response getOvfs() throws Exception {
        logger.debug("Entering getOvfs");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray uriList = new JSONArray();
            logger.debug("User is null, listing ovfs per app per user");
            // for each app user
            for (User appuser : app.getUserList()) {
                // for each app
                for (UserOvf ovf : app.getUserOvfList()) {
                    JSONObject o = new JSONObject();
                    o.put("name", ovf.getName());
                    o.put("uri", RestUriBuilder.getUserOvfUri(ovf));
                    uriList.put(o);
                }
            }

            return Response.ok(uriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
            logger.debug("Exiting getOvfs");
        }
    }

    //	/**
    //	 * Create new User OVF.
    //	 */
    //	@Override
    //	public Response postOvfs(String content) throws Exception {
    //		logger.debug("Entering post of users application's OVF");
    //		JSONObject ovfjson = null;
    //		try{
    //			ovfjson = new JSONObject(content);
    //		}
    //		catch (JSONException err){
    //			logger.error(err.getMessage());
    //			logger.error(FederationDBCommon.getStackTrace(err));
    //			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
    //		}
    //		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
    //		try {
    //			UserOvf userovf = new UserOvf();
    //			if(ovfjson.has("name"))
    //				userovf.setName(ovfjson.getString("name"));
    //			if(ovfjson.has("attributes"))
    //				userovf.setAttributes(ovfjson.getString("attributes"));
    //			em.getTransaction().begin();
    //			em.persist(userovf);
    //			app.getUserOvfList().add(userovf);
    //			app = em.merge(app);
    //			em.getTransaction().commit();
    //			URI resourceUri = new URI(String.format("/users/%d/applications/%d/ovfs/%d", user.getUserId(), app.getApplicationId(), userovf.getOvfId()));
    //			logger.debug("Exiting post of users application's OVF");
    //			return Response.created(resourceUri).build();
    //		}
    //		catch(Exception err){
    //			logger.error(err.getMessage());
    //			return Response.serverError().build();
    //		}
    //		finally {
    //			PersistenceUtils.getInstance().closeEntityManager(em);
    //		}
    //	}

    /**
     * Returns all providers on which the application runs
     *
     * @return
     * @throws JSONException
     */
    @GET
    @Path("/providers")
    @Produces("application/json")
    public Response getProviders() throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray jsonArray = new JSONArray();
            for (Provider provider : app.getProviderList()) {
                JSONObject o = new JSONObject();
                String uri = String.format("%s/providers/%d", RestUriBuilder.getApplicationUri(app),
                        provider.getProviderId());
                o.put("providerUri", RestUriBuilder.getProviderUri(provider));
                o.put("uri", uri);
                jsonArray.put(o);
            }

            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Add given providers to the selected application
     *
     * @return
     */
    @POST
    @Path("/providers")
    @Consumes("application/json")
    public Response addProviders(String requestBody) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("addProviders() started. Data: " + requestBody);
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            List<Provider> providerList = new ArrayList<Provider>();
            try {
                JSONArray jsonArray = new JSONArray(requestBody);
                Pattern uriPattern = Pattern.compile("^/providers/([\\w-]+)$");

                for (int i = 0; i < jsonArray.length(); i++) {
                    String providerUri = jsonArray.getString(i);
                    Matcher m = uriPattern.matcher(providerUri);
                    if (m.find()) {
                        String id = m.group(1);
                        Provider provider;
                        if (id.matches("\\d+")) {
                            int providerId = Integer.parseInt(id);
                            provider = new ProviderDAO(em).findById(providerId);
                        }
                        else {
                            provider = new ProviderDAO(em).findByUuid(id);
                        }
                        if (provider == null) {
                            throw new Exception(String.format("Provider '%s' not found.", providerUri));
                        }
                        else {
                            if (!app.getProviderList().contains(provider)) {
                                providerList.add(provider);
                            }
                        }
                    }
                    else {
                        throw new Exception("Invalid provider URI: " + providerUri);
                    }
                }
            }
            catch (Exception e) {
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
            }

            if (providerList.size() == 0) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            em.getTransaction().begin();

            for (Provider provider : providerList) {
                app.getProviderList().add(provider);
                provider.getApplicationList().add(app);
            }

            em.getTransaction().commit();

            logger.trace("addProviders() finished successfully.");
            return Response.status(Response.Status.CREATED).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Removes selected provider from the application
     *
     * @param providerId
     * @return
     * @throws Exception
     */
    @DELETE
    @Path("/providers/{providerId}")
    @Produces("application/json")
    public Response removeProvider(@PathParam("providerId") int providerId) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace(String.format("removeProvider(%d) started.", providerId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            if (app.getProviderList() == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            for (Provider provider : app.getProviderList()) {
                if (provider.getProviderId() == providerId) {
                    em.getTransaction().begin();
                    app.getProviderList().remove(provider);
                    provider.getApplicationList().remove(app);
                    em.getTransaction().commit();

                    logger.trace("removeProvider() finished successfully.");
                    return Response.status(Response.Status.NO_CONTENT).build();
                }
            }
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all VMs of the application
     *
     * @return
     * @throws JSONException
     */
    @GET
    @Path("/vms")
    @Produces("application/json")
    public Response getVms() throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {

            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            String appUri = RestUriBuilder.getApplicationUri(app);
            JSONArray jsonArray = new JSONArray();
            for (Vm vm : app.getVmList()) {
                JSONObject o = new JSONObject();
                String uri = String.format("%s/vms/%d", appUri, vm.getVmId());
                o.put("vmUri", RestUriBuilder.getVmUri(vm));
                o.put("uri", uri);
                jsonArray.put(o);
            }

            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Add given VMs to the selected application
     *
     * @return
     */
    @POST
    @Path("/vms")
    @Consumes("application/json")
    public Response addVms(String requestBody) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("addVms() started. Data: " + requestBody);
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            List<Vm> vmList = new ArrayList<Vm>();
            try {
                JSONArray jsonArray = new JSONArray(requestBody);
                Pattern uriPattern = Pattern.compile("^/providers/([\\w-]+)/vms/(\\d+)$");

                for (int i = 0; i < jsonArray.length(); i++) {
                    String vmUri = jsonArray.getString(i);
                    Matcher m = uriPattern.matcher(vmUri);
                    if (m.find()) {
                        String providerUuid = m.group(1);
                        Provider provider;
                        if (providerUuid.matches("\\d+")) {
                            int providerId = Integer.parseInt(providerUuid);
                            provider = new ProviderDAO(em).findById(providerId);
                        }
                        else {
                            provider = new ProviderDAO(em).findByUuid(providerUuid);
                        }

                        if (provider == null) {
                            throw new Exception(String.format("VM '%s' not found.", vmUri));
                        }

                        int vmId = Integer.parseInt(m.group(2));
                        Vm vm = new VmDAO(em).findById(provider.getProviderId(), vmId);
                        if (vm == null) {
                            throw new Exception(String.format("VM '%s' not found.", vmUri));
                        }

                        if (!app.getVmList().contains(vm)) {
                            vmList.add(vm);
                        }
                    }
                    else {
                        throw new Exception("Invalid VM URI: " + vmUri);
                    }
                }
            }
            catch (Exception e) {
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
            }

            if (vmList.size() == 0) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            em.getTransaction().begin();

            for (Vm vm : vmList) {
                app.getVmList().add(vm);
                vm.setApplicationId(app);
            }

            em.getTransaction().commit();

            logger.trace("addVms() finished successfully.");
            return Response.status(Response.Status.CREATED).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Removes selected VM from the application
     *
     * @param vmId
     * @return
     * @throws Exception
     */
    @DELETE
    @Path("/vms/{vmId}")
    @Produces("application/json")
    public Response removeVm(@PathParam("vmId") int vmId) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace(String.format("removeVm(%d) started.", vmId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            if (app.getVmList() == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            for (Vm vm : app.getVmList()) {
                if (vm.getVmId() == vmId) {
                    em.getTransaction().begin();
                    app.getVmList().remove(vm);
                    vm.setApplicationId(null);
                    em.getTransaction().commit();

                    logger.trace("removeVm() finished successfully.");
                    return Response.status(Response.Status.NO_CONTENT).build();
                }
            }
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("/accountingOAuthTest")
    public Response accountingOAuthTest() throws Exception {
        logger.debug("accountingOAuthTest() started.");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        // TODO: authorization?

        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            if (app.getUserList().size() == 0) {
                throw new RestApiException(Response.Status.BAD_REQUEST, "No users found.");
            }
            User user = app.getUserList().get(0);

            logger.debug("Asking OAuthATManager for an access token on behalf of the user " + user.getUuid());
            OAuthATManager oAuthATManager = OAuthATManagerFactory.getOAuthATManager();
            AccessToken accessToken = oAuthATManager.getAccessToken(user);

            URI uri = new URI("https://localhost:8443/accounting/accounting/test");
            logger.debug(String.format("Calling OAuth protected accounting api (%s) using access token %s",
                    uri, accessToken.getValue()));
            OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                    Conf.getInstance().getOAuthClientKeystoreFile(),
                    Conf.getInstance().getOAuthClientKeystorePass(),
                    Conf.getInstance().getOAuthClientTruststoreFile(),
                    Conf.getInstance().getOAuthClientTruststorePass());

            HttpResponse response = oAuthHttpClient.get(uri, accessToken.getValue());
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new RestApiException(Response.Status.INTERNAL_SERVER_ERROR,
                        "Failed to obtain accounting data: " + response.getStatusLine());
            }
            String content = oAuthHttpClient.getContent(response);
            logger.debug("Got response from the accounting.");

            logger.debug("accountingOAuthTest() finished successfully.");
            return Response.status(Response.Status.OK).entity(content).build();
        }
        catch (Exception e) {
            logger.error("accountingOAuthTest() failed: " + e.getMessage(), e);
            throw e;
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("selection_criteria")
    public Response getSelectionCriteria() throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            // assume the app belongs to only one user
            User user = app.getUserList().get(0);

            TypedQuery<Object[]> q = em.createNamedQuery("ApplicationSelectionCriterion.findByAppIdUserId",
                    Object[].class);
            q.setParameter(1, user.getUserId());
            q.setParameter(2, app.getApplicationId());
            List<Object[]> resultList = q.getResultList();

            JSONArray scArray = new JSONArray();
            for (Object[] row : resultList) {
                String name = (String) row[0];
                Double appValue = (Double) row[1];
                Double userValue = (Double) row[2];
                Double defaultValue = (Double) row[3];

                Double value;
                if (appValue != null) {
                    value = appValue;
                }
                else {
                    if (userValue != null) {
                        value = userValue;
                    }
                    else {
                        value = defaultValue;
                    }
                }

                JSONObject scObj = new JSONObject();
                scObj.put("name", name);
                scObj.put("value", value);
                scArray.put(scObj);
            }

            return Response.ok(scArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Produces("application/json")
    @Path("selection_criteria")
    public Response updateSelectionCriteria(String content) throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            JSONArray scArray = new JSONArray(content);
            for (int i = 0; i < scArray.length(); i++) {
                JSONObject scObj = new JSONObject(scArray.getString(i));
                String name = scObj.getString("name");
                double value = scObj.getDouble("value");

                TypedQuery<SelectionCriterion> q = em.createNamedQuery("SelectionCriterion.findByName",
                        SelectionCriterion.class);
                q.setParameter("name", name);
                SelectionCriterion sc = null;
                try {
                    sc = q.getSingleResult();
                }
                catch (NoResultException e) {
                    throw new JSONException("Invalid selection criterion: " + name);
                }

                ApplicationSelectionCriterion asc = new ApplicationSelectionCriterion(
                        app.getApplicationId(), sc.getScId());
                asc.setValue(value);
                em.merge(asc);
            }
            em.getTransaction().commit();
            return Response.noContent().build();
        }
        catch (JSONException e) {
            em.getTransaction().rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
