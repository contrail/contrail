package org.ow2.contrail.federation.federationapi.slas.negotiation;

import java.util.List;

public interface INegotiationClient {
    String initiateNegotiation(String slat) throws Exception;

    String[] negotiate(String negotiationID, String slat) throws Exception;

    String createAgreement(String negotiationID, String slat) throws Exception;

    boolean cancel(String negotiationID, List<String> cancellationReason) throws Exception;
}
