package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/networks/{networkId}")
public class NetworkResource {
    private String providerUuid;
    private int networkId;

    public NetworkResource(@PathParam("providerUuid") String providerUuid, @PathParam("networkId") int networkId) {
        this.providerUuid = providerUuid;
        this.networkId = networkId;
    }

    /**
     * Returns the JSON representation of the given network.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getNetwork() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the given network.
     *
     * @return
     */
    @PUT
    public Response updateNetwork() {
        throw new UnsupportedOperationException();
    }
}
