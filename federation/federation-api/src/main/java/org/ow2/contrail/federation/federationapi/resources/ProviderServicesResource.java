package org.ow2.contrail.federation.federationapi.resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.ServiceDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.Service;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/providers/{providerUuid}/services")
public class ProviderServicesResource {
    private String providerUuid;

    public ProviderServicesResource(@PathParam("providerUuid") String providerUuid) {
        this.providerUuid = providerUuid;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray jsonArray = new JSONArray();
            for (Service service : provider.getServiceList()) {
                JSONObject o = new JSONObject();
                o.put("name", service.getName());
                o.put("uri", RestUriBuilder.getProviderServiceUri(service));
                jsonArray.put(o);
            }

            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addService(String data) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            Service service;
            try {
                JSONObject json = new JSONObject(data);
                service = new Service(json);
                service.setProvider(provider);
            }
            catch (JSONException e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
            }

            em.getTransaction().begin();
            em.persist(service);
            em.getTransaction().commit();

            URI location = new URI(service.getServiceId().toString());
            return Response.created(location).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{serviceId:\\d+}")
    public Response getService(@PathParam("serviceId") int serviceId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Service service = new ServiceDAO(em).findById(providerUuid, serviceId);
            if (service == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            return Response.ok(service.toJSON().toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{serviceName:[\\w-]+}")
    public Response getServiceByName(@PathParam("serviceName") String serviceName) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Service service = new ServiceDAO(em).findByName(providerUuid, serviceName);
            if (service == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            return Response.ok(service.toJSON().toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{serviceId}")
    public Response updateService(@PathParam("serviceId") int serviceId, String data) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Service service = new ServiceDAO(em).findById(providerUuid, serviceId);
            if (service == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject(data);
            em.getTransaction().begin();
            service.update(json);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(String.format("Update failed: %s", e.getMessage())).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Path("{serviceId}")
    public Response removeService(@PathParam("serviceId") int serviceId) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Service service = new ServiceDAO(em).findById(providerUuid, serviceId);
            if (service == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(service);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
