package org.ow2.contrail.federation.federationapi.utils.oauth;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.CCFlowClient;
import org.ow2.contrail.federation.federationapi.exceptions.OAuthException;
import org.ow2.contrail.federation.federationapi.utils.Conf;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

import javax.servlet.ServletContext;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MemoryOAuthATManager implements OAuthATManager {
    private static Logger log = Logger.getLogger(MemoryOAuthATManager.class);

    private Map<String, AccessToken> accessTokenCache;
    private CCFlowClient ccFlowClient;

    public MemoryOAuthATManager() throws URISyntaxException {
        accessTokenCache = new HashMap<String, AccessToken>();
        init();

        log.debug("MemoryOAuthATManager initialized successfully.");
    }

    public MemoryOAuthATManager(ServletContext context) throws URISyntaxException {
        accessTokenCache = new HashMap<String, AccessToken>();
        context.setAttribute("accessTokenCache", accessTokenCache);
        init();

        log.debug("MemoryOAuthATManager initialized successfully.");
    }

    private void init() throws URISyntaxException {
        ccFlowClient = new CCFlowClient(
                new URI(Conf.getInstance().getOAuthASTokenEndpoint()),
                Conf.getInstance().getOAuthClientKeystoreFile(),
                Conf.getInstance().getOAuthClientKeystorePass(),
                Conf.getInstance().getOAuthClientTruststoreFile(),
                Conf.getInstance().getOAuthClientTruststorePass());
        ccFlowClient.setClientId(Conf.getInstance().getOAuthClientId());
        ccFlowClient.setClientSecret(Conf.getInstance().getOAuthClientSecret());
    }

    @Override
    public AccessToken getAccessToken(User user) throws OAuthException {
        log.debug("Retrieving access token for the user " + user.getUuid());
        if (accessTokenCache.containsKey(user.getUuid())) {
            log.debug("Access token found in the cache.");
            AccessToken accessToken = accessTokenCache.get(user.getUuid());
            Date now = new Date();
            if (accessToken.getExpireTime().getTime() - now.getTime() > 60*1000) {
                log.debug("Returning access token " + accessToken.getValue());
                return accessToken;
            }
            else {
                log.debug("Access token has expired.");
                accessTokenCache.remove(user.getUuid());
            }
        }

        log.debug("Requesting new access token from the authorization server.");
        AccessToken accessToken = null;
        try {
            accessToken = ccFlowClient.requestAccessToken(user.getUuid(), "");
            log.debug("Access token obtained successfully.");
        }
        catch (Exception e) {
            log.debug("Failed to obtain an access token: " + e.getMessage(), e);
            throw new OAuthException("Failed to obtain an OAuth access token: " + e.getMessage(), e);
        }

        accessTokenCache.put(user.getUuid(), accessToken);
        log.debug("Returning access token " + accessToken.getValue());

        return accessToken;
    }
}
