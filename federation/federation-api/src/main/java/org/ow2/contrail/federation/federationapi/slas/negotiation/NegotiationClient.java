package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.apache.axis2.AxisFault;
import org.ow2.contrail.federation.federationapi.exceptions.NegotiationException;
import org.ow2.contrail.federation.federationapi.utils.Conf;

import java.util.List;

public class NegotiationClient implements INegotiationClient {
    private ContrailNegotiationStub client;

    public NegotiationClient() throws AxisFault {
        String endpoint = Conf.getInstance().getSlaNegotiationEndpoint();
        client = new ContrailNegotiationStub(endpoint);
    }

    @Override
    public String initiateNegotiation(String slat) throws java.lang.Exception {
        ContrailNegotiationStub.InitiateNegotiation request = new ContrailNegotiationStub.InitiateNegotiation();
        request.setSlaTemplate(slat);
        ContrailNegotiationStub.InitiateNegotiationResponse response = client.initiateNegotiation(request);
        if (response.get_return() == null) {
            throw new NegotiationException("Contrail-SLAManager returned empty response.");
        }
        else {
            return response.get_return();
        }
    }

    @Override
    public String[] negotiate(String negotiationID, String slat) throws Exception {
        ContrailNegotiationStub.Negotiate request = new ContrailNegotiationStub.Negotiate();
        request.setNegotiationID(negotiationID);
        request.setSlaTemplate(slat);
        ContrailNegotiationStub.NegotiateResponse response = client.negotiate(request);
        String[] result = response.get_return();
        if (result == null ||
                (result.length == 1 && result[0] == null)) {
            throw new NegotiationException("Contrail-SLAManager returned empty response.");
        }
        else {
            return result;
        }
    }

    @Override
    public String createAgreement(String negotiationID, String slat) throws Exception {
        ContrailNegotiationStub.CreateAgreement request = new ContrailNegotiationStub.CreateAgreement();
        request.setNegotiationID(negotiationID);
        request.setSlaTemplate(slat);
        ContrailNegotiationStub.CreateAgreementResponse response = client.createAgreement(request);
        if (response.get_return() == null) {
            throw new NegotiationException("Contrail-SLAManager returned empty response.");
        }
        else {
            return response.get_return();
        }
    }

    @Override
    public boolean cancel(String negotiationID, List<String> cancellationReason) throws Exception {
        ContrailNegotiationStub.CancelNegotiation request = new ContrailNegotiationStub.CancelNegotiation();
        request.setNegotiationID(negotiationID);
        String[] cancellationReasonArr = (cancellationReason != null) ?
            cancellationReason.toArray(new String[cancellationReason.size()]) : null;
        request.setCancellationReason(cancellationReasonArr);
        try {
            ContrailNegotiationStub.CancelNegotiationResponse response = client.cancelNegotiation(request);
            return response.get_return();
        }
        catch (Exception e) {
            throw new NegotiationException("Failed to cancel negotiation: " + e.getMessage());
        }
    }
}
