package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/vos/{voId}/edcs/{edcId}")
public class EDCResource {

    private String providerUuid;
    private int voId;
    private int edcId;

    public EDCResource(@PathParam("providerUuid") String providerUuid,
                       @PathParam("voId") int voId,
                       @PathParam("edcId") int edcId) {
        this.providerUuid = providerUuid;
        this.voId = voId;
        this.edcId = edcId;
    }

    /**
     * Returns the JSON representation of the elastic DC for the provider's VO.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getEDC() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the provider’s elastic DC with the given ID.
     *
     * @return
     */
    @PUT
    public Response updateEDC() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all storages of the EDC.
     *
     * @return
     */
    @GET
    @Path("/storages")
    @Produces("application/json")
    public Response getStorages() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new storage for the provider's EDC.
     *
     * @return
     */
    @POST
    @Path("/storages")
    public Response createStorage() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param storageId
     * @return
     */
    @Path("/storages/{sid}")
    public StorageResource findStorage(@PathParam("sid") int storageId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all networks of the EDC.
     *
     * @return
     */
    @GET
    @Path("/networks")
    @Produces("application/json")
    public Response getNetworks() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new network for the provider's EDC.
     *
     * @return
     */
    @POST
    @Path("/networks")
    public Response createNetwork() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param networkId
     * @return
     */
    @Path("/networks/{nid}")
    public NetworkResource findNetwork(@PathParam("nid") int networkId) {
        throw new UnsupportedOperationException();
    }
}
