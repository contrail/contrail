package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.OvfDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Ovf;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/ovfs/{ovfId}")
public class OvfResource {

    protected static Logger logger = Logger.getLogger(OvfResource.class);

    private int ovfId;

    public OvfResource(@PathParam("ovfId") int ovfId) {
        this.ovfId = ovfId;
    }

    /**
     * Returns the JSON representation of the selected OVF.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getOvf() throws Exception {
        logger.debug("Entering getOvf");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Ovf ovf = new OvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject ovfJson = new JSONObject();
            ovfJson.put("providerId", String.format("/providers/%d", ovf.getProviderId().getProviderId()));
            ovfJson.put("ovfId", ovf.getOvfId());
            ovfJson.put("name", ovf.getName());
            ovfJson.put("attributes", ovf.getAttributes());
            ovfJson.put("content", ovf.getContent());
            ovfJson.put("uri", RestUriBuilder.getOvfUri(ovf));
            logger.debug("Exiting getOvf");
            return Response.ok(ovfJson.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected OVF.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateOvf(JSONObject ovfData) throws Exception {
        logger.debug("Modifying ovf");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Ovf ovf = new OvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            ovf.setName(ovfData.getString("name"));
            ovf.setAttributes(ovfData.toString());
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting modifying ovf");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected OVF.
     *
     * @return
     */
    @DELETE
    public Response removeOvf() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Ovf ovf = new OvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(ovf);
            // TODO: ??? provider.getOvfList().remove(ovf);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
