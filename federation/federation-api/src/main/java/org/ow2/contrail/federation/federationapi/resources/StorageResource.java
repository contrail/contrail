package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/storages/{storageId}")
public class StorageResource {
    private String providerUuid;
    private int storageId;

    public StorageResource(@PathParam("providerUuid") String providerUuid, @PathParam("storageId") int storageId) {
        this.providerUuid = providerUuid;
        this.storageId = storageId;
    }

    /**
     * Returns the JSON representation of the given storage.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getStorage() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the given storage.
     *
     * @return
     */
    @PUT
    public Response updateStorage() {
        throw new UnsupportedOperationException();
    }
}
