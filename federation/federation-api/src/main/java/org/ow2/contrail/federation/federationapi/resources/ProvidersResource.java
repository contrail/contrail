package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.exceptions.RestApiException;
import org.ow2.contrail.federation.federationapi.utils.*;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/providers")
public class ProvidersResource {
    private static Logger log = Logger.getLogger(ProvidersResource.class);

    /**
     * Returns list of all providers the current user has access to.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getProviders() throws JSONException {
        log.trace("getProviders() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Query query = em.createNamedQuery("Provider.findAll");
            List<Provider> providerList = query.getResultList();

            JSONArray jsonArray = new JSONArray();
            for (Provider provider : providerList) {
                if (provider.getUuid().equals(Conf.FEDERATION_PROVIDER_UUID)) {
                    continue;
                }
                JSONObject o = new JSONObject();
                o.put("name", provider.getName());
                o.put("uri", RestUriBuilder.getProviderUri(provider));
                jsonArray.put(o);
            }

            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Creates a new provider.
     *
     * @return
     */
    @POST
    @Consumes("application/json")
    public Response addProvider(JSONObject providerData) throws JSONException, URISyntaxException {
        log.trace("addProvider() started.");

        Provider provider;
        try {
            provider = new Provider(providerData);
        }
        catch (Exception e) {
            throw new RestApiException(Response.Status.BAD_REQUEST, "Invalid provider data: " + e.getMessage());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(provider);
            em.getTransaction().commit();

            // Federeation core needs providerId, we need to add provider after insertion
            // into the DB (providerId is know after DB insertion).
            try {
                MyServletContextListener.getFederationCore().providerCreation(provider);
            }
            catch (Exception e1) {
                log.error("Error trying to create the provider on the federation core.");
                log.error(FederationDBCommon.getStackTrace(e1));
                log.debug("Removing the provider from the database.");
                em.getTransaction().begin();
                em.remove(provider);
                em.getTransaction().commit();

                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }

            URI resourceUri = new URI(provider.getUuid());
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
