/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.dao.AttributeDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Attribute;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author ales
 */
@Path("/attributes/{attrUuid}")
public class AttributeResource {

    protected static Logger logger =
            Logger.getLogger(AttributeResource.class);

    public static final String PROVIDER_SUBJECT_SLASOI_ID = "urn:contrail:names:provider:subject:slasoi-id";

    private String attrUuid;

    public AttributeResource(@PathParam("attrUuid") String attrUuid) {
        this.attrUuid = attrUuid;
    }

    /* (non-Javadoc)
      * @see org.ow2.contrail.federation.federationapi.interfaces.BaseSingle#get()
      */
    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Attribute attribute = new AttributeDAO(em).findByUuid(attrUuid);
            if (attribute == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject attr = null;
            attr = new JSONObject();
            attr.put("uuid", attribute.getAttributeUuid());
            attr.put("name", attribute.getName());
            attr.put("uri", attribute.getUri());
            attr.put("defaultValue", attribute.getDefaultValue());
            attr.put("reference", attribute.getReference());
            attr.put("description", attribute.getDescription());

            return Response.ok(attr.toString()).build();
        }
        finally {
            logger.debug("Exiting remove attribute.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /* (non-Javadoc)
      * @see org.ow2.contrail.federation.federationapi.interfaces.BaseSingle#delete()
      */
    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering remove attribute.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Attribute attribute = new AttributeDAO(em).findByUuid(attrUuid);
            if (attribute == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(attribute);
            try {
                FederationIdentityProvider.delete("attributes/" + attribute.getAttributeUuid());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting remove attribute.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /* (non-Javadoc)
      * @see org.ow2.contrail.federation.federationapi.interfaces.BaseSingle#put(java.lang.String)
      */
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject attrData) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Attribute attribute = new AttributeDAO(em).findByUuid(attrUuid);
            if (attribute == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();

            if (attrData.has("name"))
                attribute.setName(attrData.getString("name"));
            if (attrData.has("uri"))
                attribute.setUri((String) attrData.get("uri"));
            if (attrData.has("defaultValue"))
                attribute.setDefaultValue((String) attrData.get("defaultValue"));
            if (attrData.has("reference"))
                attribute.setReference((String) attrData.get("reference"));
            if (attrData.has("description"))
                attribute.setDescription((String) attrData.get("description"));

            try {
                FederationIdentityProvider.put("attributes/" + attribute.getAttributeUuid(), attrData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
                throw err;
            }

            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
