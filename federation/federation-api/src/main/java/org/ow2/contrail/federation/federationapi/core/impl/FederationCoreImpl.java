/**
 * This holds implementation of the federation core's calls.
 * 
 *  Update: We have commented out everything that was federation-core related since 
 *  it is broken. 
 * 
 * @author ales.cernivec@xlab.si
 */
package org.ow2.contrail.federation.federationapi.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.ow2.contrail.federation.federationapi.core.IFederationCore;
import org.ow2.contrail.federation.federationcore.FederationCore;
import org.ow2.contrail.federation.federationcore.aem.application.IApplicationManagement;
import org.ow2.contrail.federation.federationcore.providermgmt.IProviderManagement;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.usermgmt.IUserManagement;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;

/**
 * This is main federation core implementation. This class calls functions
 * of the federation core provided by the CNR.  
 * 
 * @author g.anastasi@isti.cnr.it, ales.cernivec@xlab.si
 *
 */
public class FederationCoreImpl implements IFederationCore {
	
	/**
	 * This is scaDomain used by the federation-core.
	 */
	private SCADomain scaDomain = null;
	IApplicationManagement aem = null;

	protected static Logger logger =
		Logger.getLogger(FederationCoreImpl.class);

	static String loggerTag = "[FederationCoreImpl] ";
	/**
	 * Initiates federation-core's scaDomain.
	 */
	@Override
	public void init(String configurationFile) throws Exception 
	{
		logger.debug(loggerTag + "Enabling federation core.");
		scaDomain = FederationCore.initCore(configurationFile);
		if (scaDomain == null){			
			logger.debug("Federation core could not be enabled");
			throw new Exception("Federation core could not be enabled");
		}
		aem = scaDomain.getService(IApplicationManagement.class,
				"ApplicationManagementServiceComponent/IApplicationManagement");
	}
	

	/**
	 * Creates user descriptor in federation core. 
	 * @param user
	 * @return
	 * @throws Exception If federation core is disabled.
	 */
	@Override
	public List<String> propagateUser(User user, List<Provider> providers) throws Exception {
		logger.debug(loggerTag + "Entering createUserDescription");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		List<String> listP = new ArrayList<String>();		
		IUserManagement u = scaDomain.getService(IUserManagement.class,
				"UserManagementServiceComponent/IUserManagement");
		listP = u.propagateUser(user, providers);
		logger.debug(loggerTag + "Exiting createUserDescription");
		return listP;
	}

	/** 
	 * Register provider in Federation
	 * @param provider
	 */
	@Override
	public void providerCreation(Provider provider) throws Exception {
		logger.debug(loggerTag + "Entering providerCreation");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		IProviderManagement prov = scaDomain.getService(IProviderManagement.class, "ProviderManagementServiceComponent/IProviderManagement");
		prov.registerProvider(provider);
		logger.debug(loggerTag + "Exiting providerCreation");

	}

	/** 
	 * Start the Application of the user.
	 * @param user
	 * @param app
	 * @param proposal
	 * @return boolean
	 */
	@Override
	public boolean startApplication(User user, Application app) throws Exception {
		logger.debug(loggerTag + "Entering startApplication");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		boolean result = aem.startApp(user, app);
		logger.debug(loggerTag + "Exiting startApplication");
		return result;
	}

	/** 
	 * Create Agreement for the Application submitted. This method conclude the negotiation with F-SLAM 
	 * creating an agreement with the proposal that has to be accepted.
	 * @param user
	 * @param app
	 * @param proposal
	 * @return boolean
	 */
	@Override
	public boolean createAgreement(User user, Application app, int proposalId) throws Exception {
		logger.debug(loggerTag + "Entering createAgreement");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		boolean result = aem.createAgreement(user, app, proposalId);
		logger.debug(loggerTag + "Exiting createAgreement");
		return result;
	}
	/** 
	 * Stop the Application of the user.
	 * @param user
	 * @param app
	 */
	@Override
	public void stopApplication(User user, Application app) throws Exception {
		logger.debug(loggerTag + "Entering stopApplication");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		aem.stopApp(user, app);
		logger.debug(loggerTag + "Exiting stopApplication");
	}

	/** 
	 * Submit the Application of the user. This method initiate the negotiation with F-SLAM 
	 * and return a set of proposal(s) in an HashMap with key the negotiation id.
	 * The return type is changed to allow the user to accept the returned proposal(s).
	 * This method calls the corresponding method submitApp in the ApplicationManagement of the federation-core.
	 * @param user
	 * @param app
	 * @param userSLATemplate
	 * @return HashMap<String, FederationProposal>
	 */
	@Override
	public boolean submitApplication(User user, Application app, UserSLATemplate userSLATemplate) throws Exception 
	{
		boolean returnStatus = false;
		logger.debug(loggerTag + "Entering submitApplication");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		returnStatus = aem.submitApp(user, app, userSLATemplate);
		logger.debug(loggerTag + "Exiting submitApplication");
		return returnStatus;
	}
	
	/** 
	 * Cancel the submission of an application. This method call the cancelSubmission which cancel
	 * the negotiation in F-SLAM 
	 * @param user
	 * @param app
	 * @param proposal
	 * @return boolean
	 */
	@Override
	public boolean cancelSubmission(User user, Application app) throws Exception {
		logger.debug(loggerTag + "Entering cancelSubmission");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		boolean res = aem.cancelSubmission(user, app);
		logger.debug(loggerTag + "Exting cancelSubmission");
		return res;
	}

	@Override
	public boolean killApplication(User user, Application app) throws Exception {
		boolean returnStatus = false;
		logger.debug(loggerTag + "Entering killApplication");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		returnStatus = aem.killApp(user, app);
		logger.debug(loggerTag + "Exiting killApplication");
		return returnStatus;
	}
	/**
	 * 
	 */
	@Override
	public void revokeAccessApplication(User user, Application app) throws Exception{
		logger.debug(loggerTag + "Entering revokeAccessApplication");
		if (scaDomain == null)
			throw new Exception("federation core is disabled.");
		aem.killApp(user, app);
		logger.debug(loggerTag + "Exiting revokeAccessApplication");
	}

}
