package org.ow2.contrail.federation.federationapi.opennebula.resources;

public class OperatingSystem extends Resource {
	private String imageTemplateName;
	
	public String getImageTemplateName() {
		return imageTemplateName;
	}

	public void setImageTemplateName(String imageTemplateName) {
		this.imageTemplateName = imageTemplateName;
	}
	
}
