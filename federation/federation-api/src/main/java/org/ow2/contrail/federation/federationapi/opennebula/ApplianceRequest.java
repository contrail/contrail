package org.ow2.contrail.federation.federationapi.opennebula;

import java.util.List;

import org.ow2.contrail.federation.federationapi.opennebula.resources.VMjson;

public class ApplianceRequest {
	private String applianceName;
	private List<VMjson> vmList;
	public String getApplianceName() {
		return applianceName;
	}
	public void setApplianceName(String applianceName) {
		this.applianceName = applianceName;
	}
	public List<VMjson> getVmList() {
		return vmList;
	}
	public void setVmList(List<VMjson> vmList) {
		this.vmList = vmList;
	}
}
