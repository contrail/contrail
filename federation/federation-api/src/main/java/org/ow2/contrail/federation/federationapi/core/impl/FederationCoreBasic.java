/**
 * 
 */
package org.ow2.contrail.federation.federationapi.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.core.IFederationCore;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;

/**
 * This is an implementation of the core which basically does nothing. If
 * the core is disabled this one should be created instead of it. 
 * 
 * @author ales.cernivec@xlab.si
 */
public class FederationCoreBasic implements IFederationCore {

	protected static Logger logger =
		Logger.getLogger(FederationCoreBasic.class);
	
	@Override
	public void init(String configrationFile) throws Exception {
		logger.debug("Entering init");
		logger.debug("Exiting init");
	}

	/**
	 * Registers the user with providers.
	 * 
	 * @return ArrayList list of providers
	 */
	@Override
	public List<String> propagateUser(User user, List<Provider> providers) throws Exception {
		logger.debug("Entering createUserDescriptor");
		List<String> listP = new ArrayList<String>();
		logger.debug("Exiting createUserDescriptor");
		return listP;
	}

	@Override
	public void providerCreation(Provider provider) throws Exception {
		logger.debug("entering provider creation");
		logger.debug("exiting provider creation");		
	}

	@Override
	public boolean startApplication(User user, Application app) throws Exception {
		logger.debug("entering start application");
		logger.debug("exiting start application");
		return true;
	}
	
	@Override
	public boolean createAgreement(User user, Application app, int proposalId) throws Exception {
		logger.debug("entering createAgreement");
		logger.debug("exiting createAgreement");
		return true;
	}

	@Override
	public void stopApplication(User user, Application app) throws Exception {
		logger.debug("entering stop application");
		logger.debug("exiting stop application");	
	}

	@Override
	public boolean submitApplication(User user, Application app, UserSLATemplate userSLATemplate) throws Exception {
		logger.debug("entering submit application");
		logger.debug("exiting submit application");
		return true;
	}

	@Override
	public boolean cancelSubmission(User user, Application app) {
		logger.debug("entering cancel submission application");
		logger.debug("exiting cancel submission application");
		return true;
	}

	@Override
	public void revokeAccessApplication(User user, Application app) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean killApplication(User user, Application app) throws Exception {
		logger.debug("entering killApplication");
		logger.debug("exiting killApplication");
		return true;
		
	}

}
