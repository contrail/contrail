package org.ow2.contrail.federation.federationapi.opennebula.resources;

public class Memory extends Resource {
	private String allocationUnits = "byte * 2^20";
	private int size;
	
	public Memory() {
		this.setDescription("Memory Size");
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSizeInMegabytes() {
		if(allocationUnits.equalsIgnoreCase("byte * 2^20"))
			return String.valueOf(size);
		// TODO
		return "128";
	}
	
	
	
}
