package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.federation.federationapi.exceptions.SlaParsingException;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.slamodel.core.*;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.*;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class SLATComparator {

    public SLATComparison compareSLATs(String slatXml1, String slatXml2) throws SlaParsingException {
        SLASOITemplateParser slasoiParser = new SLASOITemplateParser();

        SLATAbstract slatAbstract1;
        try {
            SLATemplate slat1 = slasoiParser.parseTemplate(slatXml1);
            slatAbstract1 = parseSLAT(slat1);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA template: " + e.getMessage(), e);
        }

        SLATAbstract slatAbstract2;
        try {
            SLATemplate slat2 = slasoiParser.parseTemplate(slatXml2);
            slatAbstract2 = parseSLAT(slat2);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA template: " + e.getMessage(), e);
        }

        return compareSLATs(slatAbstract1, slatAbstract2);
    }

    public JSONObject getSLATComparisonTreeGrid(SLATComparison slatComparison) throws Exception {
        JSONObject treeGridData = new JSONObject();
        treeGridData.put("text", ".");
        JSONArray agrTerms = new JSONArray();
        treeGridData.put("children", agrTerms);

        for (SLATComparison.AgreementTerm agreementTerm : slatComparison.getAgreementTerms().values()) {
            JSONObject agrTerm = new JSONObject();
            agrTerms.put(agrTerm);
            agrTerm.put("iconCls", "agreement-term");
            agrTerm.put("id", agreementTerm.getAgreementTermId());
            agrTerm.put("name", agreementTerm.getAgreementTermId());
            agrTerm.put("status", agreementTerm.getStatus());
            JSONArray guarantees = new JSONArray();
            agrTerm.put("children", guarantees);

            for (SLATComparison.GuaranteedState guaranteedState : agreementTerm.getGuarantees().values()) {
                JSONObject guarantee = new JSONObject();
                guarantees.put(guarantee);
                guarantee.put("id", guaranteedState.getGuaranteeId());
                guarantee.put("name", guaranteedState.getGuaranteeId());
                guarantee.put("target", guaranteedState.getTarget());
                guarantee.put("qosTerm", guaranteedState.getQosTerm());
                guarantee.put("operator", guaranteedState.getOperator());

                SLATAbstract.Threshold threshold1 = guaranteedState.getThreshold1();
                if (threshold1 != null) {
                    String threshold1String = threshold1.getValue().toString();
                    if (!threshold1.getUnit().equals("")) {
                        threshold1String += " " + threshold1.getUnit();
                    }
                    guarantee.put("thresholdOld", threshold1String);
                }

                SLATAbstract.Threshold threshold2 = guaranteedState.getThreshold2();
                if (threshold2 != null) {
                    String threshold2String = threshold2.getValue().toString();
                    if (!threshold2.getUnit().equals("")) {
                        threshold2String += " " + threshold2.getUnit();
                    }
                    guarantee.put("thresholdNew", threshold2String);
                }

                guarantee.put("status", guaranteedState.getStatus());
                guarantee.put("iconCls", "qos-term");
                guarantee.put("leaf", "true");
            }
        }

        return treeGridData;
    }

    private SLATComparison compareSLATs(SLATAbstract slatStatus1, SLATAbstract slatStatus2) throws
            SlaParsingException {

        SLATComparison comparison = new SLATComparison();

        // compare SLATs
        // check if any agreement terms are removed from SLAT (exist in SLAT1 but not in SLAT2)
        for (String agreementTermId : slatStatus1.getAgreementTerms().keySet()) {
            if (!slatStatus2.getAgreementTerms().containsKey(agreementTermId)) {
                SLATAbstract.AgreementTerm agreementTerm1 = slatStatus1.getAgreementTerm(agreementTermId);
                SLATComparison.AgreementTerm agreementTerm = new SLATComparison.AgreementTerm(agreementTermId);
                agreementTerm.setStatus(SLATComparison.Status.REMOVED);
                comparison.addAgreementTerm(agreementTerm);

                // copy guarantees from SLAT1
                for (SLATAbstract.GuaranteedState guarantee1 : agreementTerm1.getGuarantees().values()) {
                    SLATComparison.GuaranteedState guarantee = compareGuaranteedStates(guarantee1, null);
                    agreementTerm.addGuarantee(guarantee);
                }
            }
        }

        // check if any agreement terms are added to SLAT (don't exist in SLAT1 but do exist in SLAT2)
        for (String agreementTermId : slatStatus2.getAgreementTerms().keySet()) {
            if (!slatStatus1.getAgreementTerms().containsKey(agreementTermId)) {
                SLATAbstract.AgreementTerm agreementTerm2 = slatStatus2.getAgreementTerm(agreementTermId);
                SLATComparison.AgreementTerm agreementTerm = new SLATComparison.AgreementTerm(agreementTermId);
                agreementTerm.setStatus(SLATComparison.Status.ADDED);
                comparison.addAgreementTerm(agreementTerm);

                // copy guarantees from SLAT2
                for (SLATAbstract.GuaranteedState guarantee2 : agreementTerm2.getGuarantees().values()) {
                    SLATComparison.GuaranteedState guarantee = compareGuaranteedStates(null, guarantee2);
                    agreementTerm.addGuarantee(guarantee);
                }
            }
        }

        // agreement terms that exist both in SLAT1 and SLAT2 - check for any changes
        for (String agreementTermId : slatStatus1.getAgreementTerms().keySet()) {
            SLATAbstract.AgreementTerm agreementTerm1 = slatStatus1.getAgreementTerm(agreementTermId);
            SLATAbstract.AgreementTerm agreementTerm2 = slatStatus2.getAgreementTerm(agreementTermId);

            if (agreementTerm2 == null) {
                continue;
            }

            SLATComparison.AgreementTerm agreementTerm = new SLATComparison.AgreementTerm(agreementTermId);
            comparison.addAgreementTerm(agreementTerm);

            // check if any guarantees are removed (exist in SLAT1 but not in SLAT2)
            for (SLATAbstract.GuaranteedState guarantee1 : agreementTerm1.getGuarantees().values()) {
                if (!agreementTerm2.getGuarantees().containsKey(guarantee1.getGuaranteeId())) {
                    SLATComparison.GuaranteedState guarantee = compareGuaranteedStates(guarantee1, null);
                    agreementTerm.addGuarantee(guarantee);
                }
            }

            // check if any guarantees are added (don't exist in SLAT1 but do exist in SLAT2)
            for (SLATAbstract.GuaranteedState guarantee2 : agreementTerm2.getGuarantees().values()) {
                if (!agreementTerm1.getGuarantees().containsKey(guarantee2.getGuaranteeId())) {
                    SLATComparison.GuaranteedState guarantee = compareGuaranteedStates(null, guarantee2);
                    agreementTerm.addGuarantee(guarantee);
                }
            }

            // check if any guarantees are changed
            for (SLATAbstract.GuaranteedState guarantee1 : agreementTerm1.getGuarantees().values()) {
                SLATAbstract.GuaranteedState guarantee2 = agreementTerm2.getGuarantee(guarantee1.getGuaranteeId());
                if (guarantee2 == null) {
                    continue;
                }

                SLATComparison.GuaranteedState guarantee = compareGuaranteedStates(guarantee1, guarantee2);
                agreementTerm.addGuarantee(guarantee);
            }

            // check if anything is changed in agreement term and set the status
            SLATComparison.Status newStatus = SLATComparison.Status.NOT_CHANGED;
            for (SLATComparison.GuaranteedState guarantee : agreementTerm.getGuarantees().values()) {
                if (guarantee.getStatus() != SLATComparison.Status.NOT_CHANGED) {
                    newStatus = SLATComparison.Status.CHANGED;
                    break;
                }
            }
            agreementTerm.setStatus(newStatus);
        }

        return comparison;
    }

    private SLATComparison.GuaranteedState compareGuaranteedStates(SLATAbstract.GuaranteedState guarantee1,
                                                                   SLATAbstract.GuaranteedState guarantee2) throws SlaParsingException {
        SLATComparison.GuaranteedState guarantee;
        if (guarantee1 == null) {
            guarantee = new SLATComparison.GuaranteedState(guarantee2, SLATComparison.Status.ADDED);
            guarantee.setThreshold1(null);
            guarantee.setThreshold2(guarantee2.getThreshold());
        }
        else if (guarantee2 == null) {
            guarantee = new SLATComparison.GuaranteedState(guarantee1, SLATComparison.Status.REMOVED);
            guarantee.setThreshold1(guarantee1.getThreshold());
            guarantee.setThreshold2(null);
        }
        else if (guarantee1.equals(guarantee2)) {
            guarantee = new SLATComparison.GuaranteedState(guarantee1, SLATComparison.Status.NOT_CHANGED);
            guarantee.setThreshold1(guarantee1.getThreshold());
            guarantee.setThreshold2(guarantee2.getThreshold());
        }
        else {
            if (!guarantee1.getOperator().equals(guarantee2.getOperator()) ||
                    !guarantee1.getQosTerm().equals(guarantee2.getQosTerm()) ||
                    !guarantee1.getQosTermUri().equals(guarantee2.getQosTermUri()) ||
                    !guarantee1.getTarget().equals(guarantee2.getTarget())) {
                throw new SlaParsingException(
                        String.format("Failed to compare guaranteed state %s.", guarantee1.getGuaranteeId()));
            }
            guarantee = new SLATComparison.GuaranteedState(guarantee1, SLATComparison.Status.CHANGED);
            guarantee.setThreshold1(guarantee1.getThreshold());
            guarantee.setThreshold2(guarantee2.getThreshold());
        }
        return guarantee;
    }

    public String updateSLAT(String slatXml, JSONObject slatAbstractJson) throws Exception {
        SLASOITemplateParser slasoiParser = new SLASOITemplateParser();

        SLATAbstract slatStatus1;
        SLATemplate slat;
        try {
            slat = slasoiParser.parseTemplate(slatXml);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA template: " + e.getMessage(), e);
        }

        SLATAbstract slatAbstract;
        try {
            slatAbstract = new SLATAbstract(slatAbstractJson);
        }
        catch (JSONException e) {
            throw new JSONException("Failed to parse the SLAT abstract JSON: " + e.getMessage());
        }

        try {
            updateSLAT(slat, slatAbstract);
        }
        catch (Exception e) {
            throw new Exception("Failed to update SLA template: " + e.getMessage(), e);
        }

        SLASOITemplateRenderer renderer = new SLASOITemplateRenderer();
        return renderer.renderSLATemplate(slat);
    }

    public String updateSLATFromTreeGrid(String slatXml, JSONObject diff) throws Exception {
        SLASOITemplateParser slasoiParser = new SLASOITemplateParser();

        SLATemplate slat;
        SLATAbstract slatAbstract;
        try {
            slat = slasoiParser.parseTemplate(slatXml);
            slatAbstract = parseSLAT(slat);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA template: " + e.getMessage(), e);
        }

        try {
            JSONArray modified = diff.getJSONArray("modified");
            JSONArray removed = diff.getJSONArray("removed");

            for (int i = 0; i < removed.length(); i++) {
                JSONObject o = removed.getJSONObject(i);
                String id = o.getString("id");
                String type = o.getString("type");
                if (type.equals("1")) { // aggrement term
                    slatAbstract.getAgreementTerms().remove(id);
                }
                else if (type.equals("2")) { // guarantee
                    boolean isRemoved = false;
                    for (String agreementTermId : slatAbstract.getAgreementTerms().keySet()) {
                        SLATAbstract.AgreementTerm agreementTerm = slatAbstract.getAgreementTerm(agreementTermId);
                        if (agreementTerm.getGuarantee(id) != null) {
                            agreementTerm.getGuarantees().remove(id);
                            isRemoved = true;
                            break;
                        }
                    }
                    if (!isRemoved) {
                        throw new Exception("Guarantee cannot be found: " + id);
                    }
                }
                else {
                    throw new Exception("Invalid record type: " + type);
                }
            }

            for (int i = 0; i < modified.length(); i++) {
                JSONObject o = modified.getJSONObject(i);
                String id = o.getString("id");
                String type = o.getString("type");
                JSONObject modifiedFields = o.getJSONObject("modified");

                if (!type.equals("2")) {
                    throw new Exception("Only guarantee can be modified.");
                }

                // find the guarantee
                SLATAbstract.GuaranteedState guarantee = null;
                for (String agreementTermId : slatAbstract.getAgreementTerms().keySet()) {
                    SLATAbstract.AgreementTerm agreementTerm = slatAbstract.getAgreementTerm(agreementTermId);
                    guarantee = agreementTerm.getGuarantee(id);
                    if (guarantee != null) {
                        break;
                    }
                }
                if (guarantee == null) {
                    throw new Exception("Guarantee cannot be found: " + id);
                }

                // update the guarantee
                if (modifiedFields.has("operator")) {
                    guarantee.setOperator(modifiedFields.getString("operator"));
                }
                if (modifiedFields.has("target")) {
                    guarantee.setTarget(modifiedFields.getString("target"));
                }
                if (modifiedFields.has("thresholdOld")) {
                    String value = modifiedFields.getString("thresholdOld").trim();
                    SLATAbstract.Threshold threshold = guarantee.getThreshold();
                    if (threshold.getUnit() != null &&
                            !guarantee.getThreshold().getUnit().equals("")) {
                        String[] valueArr = value.split(" +");
                        String numValue = valueArr[0];
                        String unit = valueArr[1];
                        threshold.setValue(numValue);
                        threshold.setUnit(unit);
                    }
                    else {
                        threshold.setValue(value);
                    }

                    guarantee.setThreshold(threshold);
                }
            }

            updateSLAT(slat, slatAbstract);
        }
        catch (Exception e) {
            throw new Exception("Failed to update SLA template: " + e.getMessage(), e);
        }

        SLASOITemplateRenderer renderer = new SLASOITemplateRenderer();
        return renderer.renderSLATemplate(slat);
    }

    private void updateSLAT(SLATemplate slat, SLATAbstract slatAbstract) throws Exception {
        if (!slat.getUuid().getValue().equals(slatAbstract.getSlatUUID())) {
            throw new Exception("SLATs UUID doesn't match.");
        }

        for (SLATAbstract.AgreementTerm agreementTermA : slatAbstract.getAgreementTerms().values()) {
            AgreementTerm agreementTerm = slat.getAgreementTerm(agreementTermA.getAgreementTermId());
            for (SLATAbstract.GuaranteedState guaranteeA : agreementTermA.getGuarantees().values()) {
                SLATAbstract.Threshold threshold = guaranteeA.getThreshold();

                Guaranteed guaranteedState = agreementTerm.getGuaranteed(guaranteeA.getGuaranteeId());
                DomainExpr domainExpr =
                        ((TypeConstraintExpr) ((Guaranteed.State) guaranteedState).getState()).getDomain();

                if (domainExpr instanceof SimpleDomainExpr) {
                    ValueExpr valueExpr = ((SimpleDomainExpr) domainExpr).getValue();

                    if (valueExpr instanceof CONST) {
                        ((CONST) valueExpr).setValue(threshold.getValue().toString());
                    }
                    else if (valueExpr instanceof ID) {
                        String variableName = ((ID) valueExpr).getValue();
                        VariableDeclr variableDeclr = agreementTerm.getVariableDeclr(variableName);
                        CONST customisedValue = ((Customisable) variableDeclr).getValue();
                        customisedValue.setValue(threshold.getValue().toString());
                    }
                    else {
                        throw new Exception("SLA format is not supported.");
                    }
                }
            }
        }
    }

    public SLATAbstract parseSLAT(String slatXml) throws SlaParsingException {
        try {
            SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
            SLATemplate slat = slasoiParser.parseTemplate(slatXml);
            return parseSLAT(slat);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA template: " + e.getMessage(), e);
        }
    }

    public SLATAbstract parseSLAT(SLATemplate slat) throws Exception {
        SLATAbstract slatAbstract = new SLATAbstract();
        slatAbstract.setSlatUUID(slat.getUuid().getValue());

        // get provider
        STND[] propertyKeys = slat.getPropertyKeys();
        if (propertyKeys != null) {
            for (STND propertyKey : propertyKeys) {
                if ("ProvidersList".equals(propertyKey.getValue())) {
                    String propertyValue = slat.getPropertyValue(propertyKey);
                    JSONObject o = new JSONObject(propertyValue);
                    JSONArray providersList = o.getJSONArray("ProvidersList");
                    for (int i = 0; i < providersList.length(); i++) {
                        JSONObject providerData = providersList.getJSONObject(i);
                        String providerUuid = providerData.getString("provider-uuid");
                        // providerUuid is actually provider ID not UUID. We have to convert it to UUID.
                        // TODO: SLAM returns provider's ID instead of UUID
                        int providerId = Integer.parseInt(providerUuid);
                        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
                        try {
                            Provider provider = new ProviderDAO(em).findById(providerId);
                            if (provider == null) {
                                throw new Exception("Invalid provider-uuid: no such provider.");
                            }
                            slatAbstract.addProvider(provider.getUuid());
                        }
                        finally {
                            PersistenceUtils.getInstance().closeEntityManager(em);
                        }
                    }
                }
            }
        }

        for (AgreementTerm agreementTerm : slat.getAgreementTerms()) {
            SLATAbstract.AgreementTerm agrTermAbstract = new SLATAbstract.AgreementTerm();
            agrTermAbstract.setAgreementTermId(agreementTerm.getId().getValue());

            for (Guaranteed guaranteed : agreementTerm.getGuarantees()) {
                if (guaranteed instanceof Guaranteed.State) {
                    SLATAbstract.GuaranteedState guaranteedState = processGuaranteedState(guaranteed, agreementTerm);
                    agrTermAbstract.addGuarantee(guaranteedState);
                }
            }

            slatAbstract.addAgreementTerm(agrTermAbstract);
        }

        return slatAbstract;
    }

    private SLATAbstract.GuaranteedState processGuaranteedState(Guaranteed guaranteedState,
                                                                AgreementTerm agreementTerm) throws SlaParsingException {
        String guaranteedStateId = guaranteedState.getId().toString();

        FunctionalExpr appliesToExpr =
                (FunctionalExpr) ((TypeConstraintExpr) (((Guaranteed.State) guaranteedState).getState())).getValue();
        ValueExpr[] parameters = appliesToExpr.getParameters();
        assert parameters.length == 1;
        String appliesTo = parameters[0].toString();

        String metricUri = appliesToExpr.getOperator().getValue();
        String metricName = metricUri.substring(metricUri.indexOf('#') + 1);
        DomainExpr domain =
                ((TypeConstraintExpr) (((Guaranteed.State) guaranteedState).getState())).getDomain();

        SLATAbstract.GuaranteedState guarantee = new SLATAbstract.GuaranteedState();
        guarantee.setGuaranteeId(guaranteedStateId);
        guarantee.setQosTerm(metricName);
        guarantee.setQosTermUri(metricUri);
        guarantee.setTarget(appliesTo);

        Expression expression = parseDomainExpr(agreementTerm, domain);
        if (expression.operands.size() == 1 &&
                expression.operands.get(0) instanceof Constant) {
            SLATAbstract.Threshold threshold = new SLATAbstract.Threshold();
            Constant constant = (Constant) expression.operands.get(0);
            threshold.setValue(constant.getValue());
            threshold.setUnit(constant.getUnit());
            threshold.setDataType(constant.getDataType());
            guarantee.setThreshold(threshold);
            guarantee.setOperator(expression.getOperator());
        }
        else if (expression.operator.equals("or")) {
            List<Object> values = new ArrayList<Object>();
            String dataType = null;
            String unit = null;
            for (Operand operand : expression.getOperands()) {
                Constant constant = (Constant) ((Expression) operand).getOperands().get(0);
                values.add(constant.getValue());
                if (dataType == null) {
                    dataType = constant.getDataType();
                }
                else if (!dataType.equals(constant.getDataType())) {
                    throw new SlaParsingException(String.format("Unexpected datatype %s in guaranteed state %s.",
                            constant.getDataType(), guaranteedStateId));
                }
                if (unit == null) {
                    unit = constant.getUnit();
                }
                else if (!unit.equals(constant.getUnit())) {
                    throw new SlaParsingException(String.format("Unexpected unit %s in guaranteed state %s.",
                            constant.getUnit(), guaranteedStateId));
                }
            }
            SLATAbstract.Threshold threshold = new SLATAbstract.Threshold();
            threshold.setValue(values);
            threshold.setUnit(unit);
            threshold.setDataType(dataType);
            guarantee.setThreshold(threshold);
            guarantee.setOperator("is_element_of");
        }
        return guarantee;
    }

    private Expression parseDomainExpr(AgreementTerm agreementTerm, DomainExpr domainExpr) throws SlaParsingException {
        if (domainExpr instanceof SimpleDomainExpr) {
            String operatorUri = ((SimpleDomainExpr) domainExpr).getComparisonOp().getValue().toString();
            String operatorName = operatorUri.substring(operatorUri.indexOf('#') + 1);

            ValueExpr valueExpr = ((SimpleDomainExpr) domainExpr).getValue();
            String threshold;
            String dataType;
            if (valueExpr instanceof CONST) {
                threshold = ((CONST) valueExpr).getValue();
                dataType = ((CONST) valueExpr).getDatatype().getValue();
            }
            else if (valueExpr instanceof ID) {
                String variableName = ((ID) valueExpr).getValue();
                VariableDeclr variableDeclr = agreementTerm.getVariableDeclr(variableName);
                CONST customisedValue = ((Customisable) variableDeclr).getValue();
                threshold = customisedValue.getValue();
                dataType = customisedValue.getDatatype().getValue();
            }
            else {
                throw new SlaParsingException("SLA format is not supported.");
            }

            String unit = "";
            if (dataType.startsWith("http://www.slaatsoi.org/coremodel/units#")) {
                unit = dataType.substring(dataType.indexOf('#') + 1);
            }

            Constant constant = new Constant();
            constant.value = threshold;
            constant.dataType = dataType;
            constant.unit = unit;

            Expression expression = new Expression();
            expression.operator = operatorName;
            expression.operatorUri = operatorUri;
            expression.operands.add(constant);

            return expression;
        }
        else if (domainExpr instanceof CompoundDomainExpr) {
            Expression expression = new Expression();
            expression.operator = ((CompoundDomainExpr) domainExpr).getLogicalOp().toString();
            expression.operatorUri = ((CompoundDomainExpr) domainExpr).getLogicalOp().getValue();
            for (DomainExpr subDomainExpr : ((CompoundDomainExpr) domainExpr).getSubExpressions()) {
                Expression subExpression = parseDomainExpr(agreementTerm, subDomainExpr);
                expression.operands.add(subExpression);
            }
            return expression;
        }
        else {
            throw new SlaParsingException("Unsupported type of domain expression: " + domainExpr.getClass().getName());
        }
    }

    static interface Operand {
        public JSONObject toJSON() throws JSONException;
    }

    static class Expression implements Operand {
        private String operator;
        private String operatorUri;
        private List<Operand> operands;

        Expression() {
            operands = new ArrayList<Operand>();
        }

        public Expression(JSONObject o) {
            operator = null;
            operator = (String) o.keys().next();

        }

        public String getOperator() {
            return operator;
        }

        public String getOperatorUri() {
            return operatorUri;
        }

        public List<Operand> getOperands() {
            return operands;
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            JSONArray operandsArr = new JSONArray();
            for (Operand operand : operands) {
                operandsArr.put(operand.toJSON());
            }
            o.put(operator, operandsArr);
            return o;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Expression other = (Expression) o;

            if (!operands.equals(other.operands)) return false;
            if (!operator.equals(other.operator)) return false;
            if (!operatorUri.equals(other.operatorUri)) return false;

            return true;
        }
    }

    static class Constant implements Operand {
        private Object value;
        private String dataType;
        private String unit;

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("value", value);
            o.put("dataType", dataType);
            o.put("unit", unit);
            return o;
        }

        public Object getValue() {
            return value;
        }

        public String getDataType() {
            return dataType;
        }

        public String getUnit() {
            return unit;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Constant constant = (Constant) o;

            if (!dataType.equals(constant.dataType)) return false;
            if (unit != null ? !unit.equals(constant.unit) : constant.unit != null) return false;
            if (!value.equals(constant.value)) return false;

            return true;
        }
    }
}
