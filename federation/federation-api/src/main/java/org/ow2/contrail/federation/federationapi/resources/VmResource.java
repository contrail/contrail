package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.VmDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Vm;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/vms/{vmId}")
public class VmResource {
    private static Logger log = Logger.getLogger(VmResource.class);
    private String providerUuid;
    private int vmId;

    public VmResource(@PathParam("providerUuid") String providerUuid, @PathParam("vmId") int vmId) {
        this.providerUuid = providerUuid;
        this.vmId = vmId;
    }

    /**
     * Returns the JSON representation of the given vm.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getVm() throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getVm(ID=%d) started.", vmId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Vm vm = new VmDAO(em).findById(providerUuid, vmId);
            if (vm == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            org.json.JSONObject json = vm.toJSON();
            json.put("uri", RestUriBuilder.getVmUri(vm));

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected vm.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateVm(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateVm(ID=%d) started. Data: %s", vmId, requestBody));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Vm vm = new VmDAO(em).findById(providerUuid, vmId);
            if (vm == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject(requestBody);

            em.getTransaction().begin();
            vm.update(json);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            log.error("Update failed: ", e);
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Update failed: %s.", e.getMessage())).
                            build());
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected vm.
     *
     * @return
     */
    @DELETE
    public Response removeVm() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeVm(ID=%d) started.", vmId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Vm vm = new VmDAO(em).findById(providerUuid, vmId);
            if (vm == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(vm);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns the VMEx.
     *
     * @return
     */
    @GET
    @Path("/vmex")
    @Produces("application/json")
    public Response getVmex() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the VMEx.
     *
     * @return
     */
    @PUT
    @Path("/vmex")
    public Response updateVmex() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns the vmslot.
     *
     * @return
     */
    @GET
    @Path("/vmslot")
    @Produces("application/json")
    public Response getVmslot() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the vmslot.
     *
     * @return
     */
    @PUT
    @Path("/vmslot")
    public Response updateVmslot() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns the vmapp.
     *
     * @return
     */
    @GET
    @Path("/vmapp")
    @Produces("application/json")
    public Response getVmapp() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the vmapp.
     *
     * @return
     */
    @PUT
    @Path("/vmapp")
    public Response updateVmapp() throws Exception {
        throw new UnsupportedOperationException();
    }
}
