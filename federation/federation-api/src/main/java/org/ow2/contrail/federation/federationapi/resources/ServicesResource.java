package org.ow2.contrail.federation.federationapi.resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.entities.Service;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/services")
public class ServicesResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices(@QueryParam("name") String name) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Service> query = qb.createQuery(Service.class);
            Root<Service> root = query.from(Service.class);
            if (name != null) {
                query.where(qb.equal(root.get("name"), name));
            }
            List<Service> services = em.createQuery(query).getResultList();

            JSONArray jsonArray = new JSONArray();
            for (Service service : services) {
                JSONObject o = service.toJSON();
                o.put("uri", RestUriBuilder.getProviderServiceUri(service));
                jsonArray.put(o);
            }

            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
