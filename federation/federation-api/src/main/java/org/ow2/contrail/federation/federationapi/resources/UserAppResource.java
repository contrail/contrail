/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ApplicationDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserOvfDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserSLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * @author ales
 */
@Path("/users/{userUuid}/applications/{appUuid}")
public class UserAppResource {

    protected static Logger logger = Logger.getLogger(UserAppResource.class);
    private String appUuid;
    private String userUuid;

    public UserAppResource(@PathParam("userUuid") String userUuid, @PathParam("appUuid") String appUuid) {
        this.userUuid = userUuid;
        this.appUuid = appUuid;
    }

    /**
     * Gets the info on application. If user is not given, list users who
     * use this application.
     */
    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject();
            json.put("applicationId", app.getApplicationId());
            json.put("uuid", app.getUuid());
            json.put("deploymentDesc", app.getDeploymentDesc());
            json.put("name", app.getName());
            json.put("applicationOvf", app.getApplicationOvf());
            json.put("attributes", app.getAttributes());
            json.put("state", app.getState());
            JSONArray arr = new JSONArray();
            for (UserOvf ovf : app.getUserOvfList()) {
                arr.put(RestUriBuilder.getUserOvfUri(ovf));
            }
            json.put("ovfs", arr);

            JSONArray providersArr = new JSONArray();
            for (Provider provider : app.getProviderList()) {
                providersArr.put(RestUriBuilder.getProviderUri(provider));
            }
            json.put("providers", providersArr);

            JSONArray vmsArr = new JSONArray();
            for (Vm vm : app.getVmList()) {
                vmsArr.put(RestUriBuilder.getVmUri(vm));
            }
            json.put("vms", vmsArr);

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    public Response delete() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(app);
            logger.debug("User given, deleting only for user " + user.getUserId());
            user.getApplicationList().remove(app);

            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(String content) throws Exception {
        JSONObject appData = null;
        logger.debug("Updating the application with new data: " + content);
        try {
            appData = new JSONObject(content);
        }
        catch (Exception err) {
            logger.error(err.getMessage());
            logger.error(FederationDBCommon.getStackTrace(err));
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
        	Application app = new ApplicationDAO(em).findByUuid(appUuid);
        	if (app == null) {
        		logger.error("Application with " + appUuid + " not found. ");
        		throw new WebApplicationException(Response.Status.NOT_FOUND);
        	}
        	em.getTransaction().begin();
        	if (appData.has("name"))
        		app.setName(appData.getString("name"));
        	if (appData.has("deploymentDesc"))
        		app.setDeploymentDesc(appData.getString("deploymentDesc"));
        	if (appData.has("applicationOvf"))
        		app.setApplicationOvf(appData.getString("applicationOvf"));
        	if (appData.has("attributes"))
        		app.setAttributes(appData.getString("attributes"));
        	if (appData.has("state"))
        		app.setState(appData.getString("state"));
        	if (appData.has("ovfs")) {
        		logger.debug("got array of OVFs");
        		JSONArray array = appData.getJSONArray("ovfs");
        		logger.debug("Updating the list of ovfs for user.");           

        		for (UserOvf userovf : app.getUserOvfList()) {
        			userovf.getApplicationList().remove(app);
        			//userovf = em.merge(userovf);
        		}
        		app.getUserOvfList().clear();
        		for (int i = 0; i < array.length(); ++i) {
        			String str = array.getString(i);
        			int ovfid = Integer.parseInt(str.substring(str.lastIndexOf("/") + 1));
        			logger.debug("Got UserOvf id " + ovfid);
        			UserOvf userovf = new UserOvfDAO().findById(ovfid);
        			if (userovf == null) {
        				logger.error("Could not find userOvf with id " + ovfid);
        				throw new WebApplicationException(Response.Status.NOT_FOUND);
        			}
        			app.getUserOvfList().add(userovf);
        			userovf.getApplicationList().add(app);
        			//userovf = em.merge(userovf);
        		}
        	}
        	//app = em.merge(this.app);
        	em.getTransaction().commit();        
        	return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e){
        	logger.error(e.getMessage());
            logger.error(FederationDBCommon.getStackTrace(e));
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        finally {
        	logger.debug("Finalizing put application.");
        	PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Produces("application/json")
    @Path("submit")
    public Response submitApplication() throws Exception {
        logger.debug("Entering submit application");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Application application = new ApplicationDAO(em).findByUuid(appUuid);
            if (application == null || !user.getApplicationList().contains(application)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            org.json.JSONObject appAttributes = new org.json.JSONObject(application.getAttributes());
            String userSLATemplateUrl = appAttributes.getString("userSLATemplateUrl");
            
            //UserSLATemplate userSLATemplate = new UserSLATemplateDAO(em).findByUrl(userSLATemplateUrl);
            
            int slatId = Integer.parseInt(userSLATemplateUrl.substring(userSLATemplateUrl.lastIndexOf("/") + 1));
			logger.debug("Got UserSLAT id to use: " + slatId);
            UserSLATemplate userSLATemplate = new UserSLATemplateDAO(em).findById(slatId);
            
            if (userSLATemplate == null) {
                String message = String.format(
                        "Cannot find corresponding UserSLATemplate with URL '%s'.", userSLATemplateUrl);
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            // set applicationOvf attribute
            UserOvf userOvf = userSLATemplate.getUserOvf();
            String ovfUri = RestUriBuilder.getUserOvfUri(userOvf);
            em.getTransaction().begin();
            application.setApplicationOvf(ovfUri);
            em.getTransaction().commit();

            UserResource.registerUserWithTheCore(user, em);
            MyServletContextListener.getFederationCore().submitApplication(user, application, userSLATemplate);

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            logger.error("Failed to submit the application: " + e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Failed to submit the application: " + e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Produces("application/json")
    @Path("start")
    public Response startApplication() throws Exception {
        logger.debug("Entering start application");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        User user;
        Application application;
        try {
            user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            application = new ApplicationDAO(em).findByUuid(appUuid);
            if (application == null || !user.getApplicationList().contains(application)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }

        try {
            boolean status = MyServletContextListener.getFederationCore().startApplication(user, application);
            if (!status) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity("Failed to start the application.").build();
            }
            else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        }
        catch (Exception e) {
            logger.error("Failed to start the application: " + e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Failed to start the application: " + e.getMessage()).build();
        }
    }

    @PUT
    @Produces("application/json")
    @Path("stop")
    public Response stopApplication() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        User user;
        Application application;
        try {
            user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            application = new ApplicationDAO(em).findByUuid(appUuid);
            if (application == null || !user.getApplicationList().contains(application)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }

        logger.debug("Stopping application " + application.getUuid());

        try {
            MyServletContextListener.getFederationCore().stopApplication(user, application);
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            logger.error("Failed to stop the application: " + e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Failed to stop the application: " + e.getMessage()).build();
        }
    }

    @PUT
    @Produces("application/json")
    @Path("kill")
    public Response killApplication() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        User user;
        Application application;
        try {
        	user = new UserDAO(em).findByUuid(userUuid);
        	if (user == null) {
        		return Response.status(Response.Status.NOT_FOUND).build();
        	}

        	application = new ApplicationDAO(em).findByUuid(appUuid);
        	if (application == null || !user.getApplicationList().contains(application)) {
        		throw new WebApplicationException(Response.Status.NOT_FOUND);
        	}

        	logger.debug("Killing application " + application.getUuid());

        	try {
        		boolean status = MyServletContextListener.getFederationCore().killApplication(user, application);
        		if (!status) {
        			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        					.entity("Failed to kill the application.").build();
        		}

                // set application state
                em.getTransaction().begin();
                application.setState("killed");
                em.getTransaction().commit();

                logger.debug("Application has been killed successfully.");
                return Response.status(Response.Status.NO_CONTENT).build();
        	}
        	catch (Exception e) {
        		logger.error("Failed to kill the application: " + e.getMessage(), e);
        		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        				.entity("Failed to kill the application: " + e.getMessage()).build();
        	}
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("ovfs")
    public Response getOvfs() throws Exception {
        logger.debug("Entering getOvfs");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray uriList = new JSONArray();
            for (UserOvf ovf : app.getUserOvfList()) {
                JSONObject o = new JSONObject();
                o.put("name", ovf.getName());
                o.put("uri", RestUriBuilder.getUserOvfUri(ovf));
                uriList.put(o);
            }
            return Response.ok(uriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
            logger.debug("Exiting getOvfs");
        }
    }

    /**
     * Create an Ovf inside user application.
     */
    @POST
    @Consumes("application/json")
    @Path("ovfs")
    public Response postUserApplicationsOvfs(JSONObject content) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Application app = new ApplicationDAO(em).findByUuid(appUuid);
            if (app == null) {
                logger.error("not found");
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            if (!content.has("ovfId")) {
                logger.error("User groups ID has to be provided.");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            String strovfId = content.getString("ovfId");
            // Get Ovf
            UserOvf ovf = new UserOvfDAO(em).findById(
                    FederationDBCommon.getIdFromString(strovfId)
            );
            if (ovf == null) {
                logger.error("OVF document with id " + content.getString("ovfId") + " not found.");
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            em.getTransaction().begin();
            app.getUserOvfList().add(ovf);
            ovf.getApplicationList().add(app);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/users/%d/applications/%d/ovfs/%d",
                    ovf.getUserId().getUserId(), app.getApplicationId(), ovf.getOvfId()));
            return Response.created(resourceUri).build();
        }
        finally {
            logger.debug("Exiting post application ovf.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Path("ovfs/{ovfId}")
    public Response deleteUserApplicationsOvf(@PathParam("ovfId") int ovfId) throws Exception {
        logger.debug("Entering delete");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            UserOvf ovf = new UserOvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Application app = new ApplicationDAO(em).findByUuid(appUuid);

            em.getTransaction().begin();
            app.getUserOvfList().remove(ovf);
            ovf.getApplicationList().remove(app);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting delete");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
