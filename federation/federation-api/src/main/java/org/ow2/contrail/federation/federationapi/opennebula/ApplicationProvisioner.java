package org.ow2.contrail.federation.federationapi.opennebula;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;
import org.opennebula.client.vm.VirtualMachinePool;
import org.ow2.contrail.federation.federationapi.opennebula.resources.VM;
import org.ow2.contrail.federation.federationapi.opennebula.resources.VMjson;

import com.google.gson.Gson;

public class ApplicationProvisioner {
	private String monitoringRestUri;
	private Client oneClient;
	private String networkId;
	private String applicationName = "Contrail demo application";
	private ArrayList<VM> vms;
		
	public static final String COMMON_VM_TEMPLATE = "NAME = \"vmNAME\" CPU = vmCAP VCPU = vmVCPU MEMORY = vmMEM\n" +
			"OS = [ boot=\"hd\", arch=\"x86_64\" ]\n" +
			"DISK = [\n" +
				"image = \"vmDISKOS\"" +
			"]\n" +
			"NIC = [\n" +
				"NETWORK = \"vmNETWORK\"\n" +
			"]\n" +
			"GRAPHICS = [\n" +
				"type=\"vnc\",\n" +
				"listen=\"localhost\"\n" +
			"]\n";
	
	public ApplicationProvisioner(String username, String password) throws Exception {
		oneClient = new Client(username + ":" + password, "http://" + OpenNebulaConnector.getInstance().ONE_RPC_HOST + ":" + OpenNebulaConnector.getInstance().ONE_RPC_PORT + "/RPC2");
		vms = new ArrayList<VM>();
		networkId = OpenNebulaConnector.getInstance().VIRTUAL_NETWORK;
		monitoringRestUri = OpenNebulaConnector.getInstance().MONITORING_REST_URI;
	}
	
	public void provision(String ovfDocument) {
		SimpleOvfParser ovfParser = new SimpleOvfParser(ovfDocument);
		vms = ovfParser.getVms();
		if(vms != null && vms.size() > 0) {
			for(VM vm : vms) {
				vm.provision(oneClient, networkId);
			}
			try {
				Thread.sleep(3000);
				initiateMonitoringRequest();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void updateVms() {
		VirtualMachinePool vmPool = new VirtualMachinePool(OpenNebulaConnector.getInstance().getAdminClient(), -2);
		OneResponse response = vmPool.info();
		if (response.getErrorMessage() != "null") {
			Iterator<VirtualMachine> iterator = vmPool.iterator();
			while (iterator.hasNext()) {
				VirtualMachine vm = iterator.next();
				for(VM virtualMachine : vms) {
					if(
						virtualMachine.getId() != null
						&& virtualMachine.getId() != null
						&& virtualMachine.getId().equalsIgnoreCase(vm.getId())
					) {
						virtualMachine.setOneVm(vm);
					}
				}
			}
		}
		else
			System.out.println(response.getErrorMessage());
	}
	
	private void initiateMonitoringRequest() {
		System.out.println("Initiating monitoring request");
		updateVms();
		try {
			ApplianceRequest ar = new ApplianceRequest();
			ar.setApplianceName(applicationName + " " + System.currentTimeMillis());
			ArrayList<VMjson> jsonVms = new ArrayList<VMjson>();
			for(VM vm : vms) {
				VMjson vmj = new VMjson(
						vm.getHostname(),
						vm.getHostname() + "." + OpenNebulaConnector.getInstance().VM_DOMAIN,
						OpenNebulaConnector.getInstance().CLUSTER_FQDN
					);
				jsonVms.add(vmj);
			}
			ar.setVmList(jsonVms);
			Gson gson = new Gson();
			String json = gson.toJson(ar);
			PostMethod post = new PostMethod(monitoringRestUri);
			post.addParameter("registrationRequest", json);
			HttpClient client = new HttpClient();
			int status = client.executeMethod(post);
			if(status == 200) {
				System.out.println("Successfully initiated monitoring request");
			}
			else {
				System.out.println("Problems initiating monitoring request, HTTP response code " + status + " (" + post.getResponseBodyAsString() + ")");
			}
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
