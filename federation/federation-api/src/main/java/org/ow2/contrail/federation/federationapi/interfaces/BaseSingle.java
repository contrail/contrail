/**
 * 
 */
package org.ow2.contrail.federation.federationapi.interfaces;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * @author ales
 *
 */
public interface BaseSingle {

	/**
	 * Returns list of possible subresources.
	 *
	 * @return
	 */
	public ArrayList<String> getSubresources() throws Exception;

	@GET
	@Produces("application/json")
	public Response get() throws Exception;

	@DELETE
	public Response delete() throws Exception;

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response put(String content) throws Exception;

}
