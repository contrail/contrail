package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.DBUtils;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.entities.SelectionCriterion;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("selection_criteria")
public class SelectionCriteriaResource {
    private static Logger log = Logger.getLogger(ProviderResource.class);

    @GET
    @Produces("application/json")
    public Response getSelectionCriteria() throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {

            TypedQuery<SelectionCriterion> q = em.createNamedQuery("SelectionCriterion.findAll", SelectionCriterion.class);
            List<SelectionCriterion> resultList = q.getResultList();

            JSONArray scArray = new JSONArray();
            for (SelectionCriterion sc : resultList) {
                JSONObject scObj = new JSONObject();
                scObj.put("id", sc.getScId());
                scObj.put("name", sc.getName());
                scObj.put("defaultValue", sc.getDefaultValue());
                scArray.put(scObj);
            }

            return Response.ok(scArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Produces("application/json")
    public Response addSelectionCriteria(String content) throws JSONException, URISyntaxException {

        SelectionCriterion sc;
        try {
            JSONObject scData = new JSONObject(content);
            sc = new SelectionCriterion();
            sc.setName(scData.getString("name"));
            sc.setDefaultValue(scData.getDouble("defaultValue"));
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(sc);
            em.getTransaction().commit();

            URI resourceUri = new URI(sc.getScId().toString());
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("{id}")
    public Response getSelectionCriterion(@PathParam("id") int id) throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {

            SelectionCriterion sc = em.find(SelectionCriterion.class, id);
            if (sc == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject scObj = new JSONObject();
            scObj.put("id", sc.getScId());
            scObj.put("name", sc.getName());
            scObj.put("defaultValue", sc.getDefaultValue());

            return Response.ok(scObj.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Produces("application/json")
    @Path("{id}")
    public Response addSelectionCriteria(@PathParam("id") int id, String content) {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {

            SelectionCriterion sc = em.find(SelectionCriterion.class, id);
            if (sc == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            try {
                JSONObject scData = new JSONObject(content);
                if (scData.has("name")) {
                    sc.setName(scData.getString("name"));
                }
                if (scData.has("defaultValue")) {
                    sc.setDefaultValue(scData.getDouble("defaultValue"));
                }
            }
            catch (JSONException e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
            }

            em.getTransaction().begin();
            em.merge(sc);
            em.getTransaction().commit();

            return Response.noContent().build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("{id}")
    public Response deleteSelectionCriterion(@PathParam("id") int id) throws JSONException {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {

            SelectionCriterion sc = em.find(SelectionCriterion.class, id);
            if (sc == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(sc);
            em.getTransaction().commit();

            return Response.noContent().build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
