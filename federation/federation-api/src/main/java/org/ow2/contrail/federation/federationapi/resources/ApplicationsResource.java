/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author ales
 */
@Path("/applications")
public class ApplicationsResource {

    protected static Logger logger =
            Logger.getLogger(ApplicationsResource.class);

    /**
     * Returns list of collection.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getAppList() throws Exception {
        logger.debug("Entering get");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createNamedQuery("Application.findAll");
            List<Application> appList = query.getResultList();
            JSONArray UriList = new JSONArray();
            for (Application app : appList) {
                JSONObject o = new JSONObject();
                o.put("name", app.getName());
                o.put("uri", RestUriBuilder.getApplicationUri(app));
                UriList.put(o);
            }
            logger.debug("Exiting get");
            return Response.ok(UriList.toString()).build();
        }
        finally {
            logger.debug("Exiting get");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
