package org.ow2.contrail.federation.federationapi.identityprovider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * This is at this point deprecated but may be usefull soon.
 * @author ales
 *
 */
public interface IFederationIdentityProvider {
	
	public void init(String accessPoint) throws Exception;
	
	public ClientResponse post(String path,String content)throws Exception;
	public ClientResponse put(String path,String content)throws Exception;
	public ClientResponse delete(String path)throws Exception;
	
	public ClientResponse addUser(String content) throws Exception;
	public ClientResponse deleteUser(int id) throws Exception;	
	public ClientResponse modifyUser(int id, String content) throws Exception;
	
	public ClientResponse addIdp(String content) throws Exception;
	public ClientResponse deleteIdp(int id) throws Exception;	
	public ClientResponse modifyIdp(int id, String content) throws Exception;
	
	public ClientResponse addIdpAcc(String content) throws Exception;
	public ClientResponse deleteIdpAcc(int userId, int id) throws Exception;	
	public ClientResponse modifyIdpAcc(int userId,int id, String content) throws Exception;
	
	public ClientResponse addRole(String content) throws Exception;
	public ClientResponse deleteRole(int id) throws Exception;
	public ClientResponse modifyRole(int id, String content) throws Exception;
	
	public ClientResponse addGroup(String content) throws Exception;
	public ClientResponse deleteGroup(int id) throws Exception;
	public ClientResponse modifyGroup(int id, String content) throws Exception;
	
	public ClientResponse addAttribute(String content) throws Exception;
	public ClientResponse deleteAttribute(int id) throws Exception;
	public ClientResponse modifyAttribute(int id, String content) throws Exception;
	
}
