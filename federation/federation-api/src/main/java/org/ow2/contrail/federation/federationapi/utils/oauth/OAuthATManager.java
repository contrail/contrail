package org.ow2.contrail.federation.federationapi.utils.oauth;

import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.federation.federationapi.exceptions.OAuthException;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public interface OAuthATManager {

    public AccessToken getAccessToken(User user) throws OAuthException;

}
