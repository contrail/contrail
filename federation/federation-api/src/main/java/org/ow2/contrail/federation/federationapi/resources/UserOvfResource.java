/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserOvfDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserOvf;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author ales
 */
@Path("/users/{userUuid}/ovfs/{ovfId}")
public class UserOvfResource {

    protected static Logger logger =
            Logger.getLogger(UserOvfResource.class);

    private String userUuid;
    private int ovfId;

    /**
     * @param userUuid can be null!
     * @param ovfId
     */

    public UserOvfResource(@PathParam("userUuid") String userUuid, @PathParam("ovfId") int ovfId) {
        this.userUuid = userUuid;
        this.ovfId = ovfId;
    }

    /**
     * Get users application's ovf.
     */
    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserOvf ovf = new UserOvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            logger.debug("entering get");
            JSONObject json = new JSONObject();
            json.put("ovfId", ovf.getOvfId());
            json.put("name", ovf.getName());
            json.put("attributes", ovf.getAttributes());
            json.put("content", ovf.getContent());
            json.put("providerOvfId", String.format("/providers/%s/ovfs/%d",
                    ovf.getProviderOvfId().getProviderId().getProviderId(), ovf.getProviderOvfId().getOvfId()));
            if (userUuid != null) {
                User user = new UserDAO(em).findByUuid(userUuid);
                if (user == null) {
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
                logger.debug("User is not null, creating a list of applications for the user");
                json.put("userId", String.format("users/%d", user.getUserId()));
                json.put("applications", String.format("users/%d/ovfs/%d/applications", user.getUserId(), ovf.getOvfId()));
            }
            logger.debug("Exiting get");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Get applications of the ovf.
     */
    @GET
    @Produces("application/json")
    @Path("/applications")
    public Response getApplications() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserOvf ovf = new UserOvfDAO(em).findById(userUuid, ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            for (Application app : ovf.getApplicationList()) {
                String uri = String.format("%s/applications/%d",
                        RestUriBuilder.getUserUri(ovf.getUserId()), app.getApplicationId());
                JSONObject o = new JSONObject();
                o.put("name", app.getName());
                o.put("uri", uri);
                json.put(o);
            }
            logger.debug("Exiting get");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Delete users application's ovf.
     */
    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering delete user ovf resource");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            UserOvf ovf = new UserOvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            User user = new UserDAO(em).findByUuid(userUuid);

            em.getTransaction().begin();
            em.remove(ovf);
            if (user != null) {
                logger.debug("Removing ovf from user's list.");
                user.getUserOvfList().remove(ovf);
            }
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting delete user ovf resource");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Modify users application's ovf.
     */
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject json) throws Exception {
        logger.debug("Entering put user ovf resource");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            UserOvf ovf = new UserOvfDAO(em).findById(ovfId);
            if (ovf == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            if (json.has("name"))
                ovf.setName(json.getString("name"));
            if (json.has("attributes"))
                ovf.setAttributes(json.getString("attributes"));
            if (json.has("content"))
                ovf.setContent(json.getString("content"));
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting put user ovf resource");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

}
