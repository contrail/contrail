
/**
 * OperationNotPossibleExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.ow2.contrail.federation.federationapi.slas.negotiation;

public class OperationNotPossibleExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.OperationNotPossibleExceptionE faultMessage;

    
        public OperationNotPossibleExceptionException() {
            super("OperationNotPossibleExceptionException");
        }

        public OperationNotPossibleExceptionException(java.lang.String s) {
           super(s);
        }

        public OperationNotPossibleExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public OperationNotPossibleExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.OperationNotPossibleExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.OperationNotPossibleExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    