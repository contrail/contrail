package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.ow2.contrail.federation.federationapi.utils.Conf;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class NegotiationClientMock implements INegotiationClient {

    public NegotiationClientMock() {
    }

    @Override
    public String initiateNegotiation(String slat) throws java.lang.Exception {
        return UUID.randomUUID().toString();
    }

    @Override
    public String[] negotiate(String negotiationID, String slat) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        String num;
        try {
            Query q = em.createQuery(
                    "SELECT COUNT(p) FROM SLATemplateProposal p WHERE p.negotiationId = :negotiationId");
            q.setParameter("negotiationId", negotiationID);
            long count = (Long) q.getSingleResult();
            num = (count == 0) ? "1" : "2";
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }

        String filePath = Conf.getInstance().getAppDataRoot() +
                String.format("/sla-negotiation-scenario/slat-proposal%s.xml", num);
        File f = new File(filePath);
        if (!f.canRead()) {
            throw new FileNotFoundException("SLA template file can not be found: " + f.getAbsolutePath());
        }
        String[] result = new String[1];
        result[0] = new Scanner(f).useDelimiter("\\Z").next();
        return result;
    }

    @Override
    public String createAgreement(String negotiationID, String slat) throws Exception {
        String filePath = Conf.getInstance().getAppDataRoot() +
                "/sla-negotiation-scenario/sla.xml";
        String slaXml = new Scanner(new File(filePath)).useDelimiter("\\Z").next();
        return slaXml;
    }

    @Override
    public boolean cancel(String negotiationID, List<String> cancellationReason) throws Exception {
        return true;
    }
}
