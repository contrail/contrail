package org.ow2.contrail.federation.federationapi.authorization;

public enum Action {
    READ,
    WRITE
}
