package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SLATAbstract {
    private String slatUUID;
    private List<String> providers = new ArrayList<String>();
    private HashMap<String, AgreementTerm> agreementTerms;

    public SLATAbstract() {
        this.agreementTerms = new HashMap<String, AgreementTerm>();
    }

    public SLATAbstract(JSONObject o) throws JSONException {
        this.agreementTerms = new HashMap<String, AgreementTerm>();
        this.slatUUID = o.getString("slatUUID");
        JSONArray terms = o.getJSONArray("agreementTerms");
        for (int i = 0; i < terms.length(); i++) {
            JSONObject agreementTermJson = terms.getJSONObject(i);
            AgreementTerm agreementTerm = new AgreementTerm(agreementTermJson);
            agreementTerms.put(agreementTerm.getAgreementTermId(), agreementTerm);
        }
    }

    public String getSlatUUID() {
        return slatUUID;
    }

    public void setSlatUUID(String slatUUID) {
        this.slatUUID = slatUUID;
    }

    public Map<String, AgreementTerm> getAgreementTerms() {
        return agreementTerms;
    }

    public AgreementTerm getAgreementTerm(String id) {
        return agreementTerms.get(id);
    }

    public void addAgreementTerm(AgreementTerm agreementTerm) {
        agreementTerms.put(agreementTerm.getAgreementTermId(), agreementTerm);
    }

    public List<String> getProviders() {
        return providers;
    }

    public void addProvider(String providerUuid) {
        this.providers.add(providerUuid);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("slatUUID", slatUUID);
        JSONArray providersArr = new JSONArray();
        for (String providerUuid : providers) {
            providersArr.put(providerUuid);
        }
        o.put("providers", providersArr);

        JSONArray agreementTermsArr = new JSONArray();
        for (AgreementTerm term : agreementTerms.values()) {
            agreementTermsArr.put(term.toJSON());
        }
        o.put("agreementTerms", agreementTermsArr);

        return o;
    }

    public static class GuaranteedState {
        private String guaranteeId;
        private String qosTerm;
        private String qosTermUri;
        private String operator;
        private Threshold threshold;
        private String target;

        public GuaranteedState() {
        }

        public GuaranteedState(JSONObject o) throws JSONException {
            guaranteeId = o.getString("guaranteeId");
            qosTerm = o.getString("qosTerm");
            qosTermUri = o.getString("qosTermUri");
            operator = o.getString("operator");
            threshold = new Threshold(o.getJSONObject("threshold"));
            target = o.getString("target");
        }

        public String getGuaranteeId() {
            return guaranteeId;
        }

        public void setGuaranteeId(String guaranteeId) {
            this.guaranteeId = guaranteeId;
        }

        public String getQosTerm() {
            return qosTerm;
        }

        public void setQosTerm(String qosTerm) {
            this.qosTerm = qosTerm;
        }

        public String getQosTermUri() {
            return qosTermUri;
        }

        public void setQosTermUri(String qosTermUri) {
            this.qosTermUri = qosTermUri;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }

        public Threshold getThreshold() {
            return threshold;
        }

        public void setThreshold(Threshold threshold) {
            this.threshold = threshold;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("guaranteeId", guaranteeId);
            o.put("qosTerm", qosTerm);
            o.put("qosTermUri", qosTermUri);
            o.put("operator", operator);
            o.put("threshold", threshold.toJSON());
            o.put("target", target);
            return o;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GuaranteedState that = (GuaranteedState) o;

            if (guaranteeId != null ? !guaranteeId.equals(that.guaranteeId) : that.guaranteeId != null) return false;
            if (operator != null ? !operator.equals(that.operator) : that.operator != null) return false;
            if (qosTerm != null ? !qosTerm.equals(that.qosTerm) : that.qosTerm != null) return false;
            if (qosTermUri != null ? !qosTermUri.equals(that.qosTermUri) : that.qosTermUri != null) return false;
            if (target != null ? !target.equals(that.target) : that.target != null) return false;
            if (threshold != null ? !threshold.equals(that.threshold) : that.threshold != null) return false;

            return true;
        }
    }

    public static class Threshold {
        private Object value;
        private String unit;
        private String dataType;

        public Threshold() {
        }

        public Threshold(JSONObject o) throws JSONException {
            value = o.get("value");
            unit = o.getString("unit");
            dataType = o.getString("dataType");
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("value", value);
            o.put("unit", unit);
            o.put("dataType", dataType);
            return o;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Threshold other = (Threshold) o;

            if (dataType != null ? !dataType.equals(other.dataType) : other.dataType != null) return false;
            if (unit != null ? !unit.equals(other.unit) : other.unit != null) return false;

            if (value == null && other.getValue() == null) {
                return true;
            }
            else if (value == null && other.getValue() != null ||
                    value != null && other.getValue() == null) {
                return false;
            }
            else if (value instanceof String && other.getValue() instanceof String &&
                    NumberUtils.isNumber((String) value) && NumberUtils.isNumber((String) other.getValue())) {
                double valueD = Double.valueOf((String) value);
                double otherD = Double.valueOf((String) other.getValue());
                return valueD == otherD;
            }
            else {
                return value.equals(other.getValue());
            }
        }
    }

    public static class AgreementTerm {
        private String agreementTermId;
        private Map<String, GuaranteedState> guarantees;

        public AgreementTerm() {
            guarantees = new HashMap<String, GuaranteedState>();
        }

        public AgreementTerm(JSONObject o) throws JSONException {
            agreementTermId = o.getString("agreementTermId");

            guarantees = new HashMap<String, GuaranteedState>();
            JSONArray guaranteesArr = o.getJSONArray("guarantees");
            for (int i = 0; i < guaranteesArr.length(); i++) {
                JSONObject guaranteeJson = guaranteesArr.getJSONObject(i);
                GuaranteedState guarantee = new GuaranteedState(guaranteeJson);
                guarantees.put(guarantee.getGuaranteeId(), guarantee);
            }
        }

        public String getAgreementTermId() {
            return agreementTermId;
        }

        public void setAgreementTermId(String agreementTermId) {
            this.agreementTermId = agreementTermId;
        }

        public Map<String, GuaranteedState> getGuarantees() {
            return guarantees;
        }

        public GuaranteedState getGuarantee(String id) {
            return guarantees.get(id);
        }

        public void addGuarantee(GuaranteedState guarantee) {
            guarantees.put(guarantee.getGuaranteeId(), guarantee);
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject o = new JSONObject();
            o.put("agreementTermId", agreementTermId);

            JSONArray guaranteesArr = new JSONArray();
            for (GuaranteedState guaranteedState : guarantees.values()) {
                guaranteesArr.put(guaranteedState.toJSON());
            }
            o.put("guarantees", guaranteesArr);
            return o;
        }
    }
}
