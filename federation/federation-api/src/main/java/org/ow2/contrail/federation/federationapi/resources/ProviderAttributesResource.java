package org.ow2.contrail.federation.federationapi.resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Attribute;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Iterator;

@Path("/providers/{providerUuid}/attributes")
public class ProviderAttributesResource {
    private String providerUuid;

    public ProviderAttributesResource(@PathParam("providerUuid") String providerUuid) {
        this.providerUuid = providerUuid;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAttributes() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject attributes = new JSONObject(provider.getAttributes());
            Iterator it = attributes.keys();
            JSONArray result = new JSONArray();
            while (it.hasNext()) {
                String attrUuid = (String) it.next();
                String attrValue = attributes.getString(attrUuid);
                Attribute attr = em.find(Attribute.class, attrUuid);
                if (attr == null) {
                    throw new Exception(String.format(
                            "Invalid attribute with UUID '%s' found at provider %s.", attrUuid, provider.getUuid()));
                }

                JSONObject attrData = new JSONObject();
                attrData.put("name", attr.getName());
                attrData.put("value", attrValue);
                attrData.put("uri",
                        UriBuilder.fromResource(ProviderAttributesResource.class).path(attrUuid).build(providerUuid));
                result.put(attrData);
            }

            return Response.ok(result.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAttributes(String data) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            JSONArray attrDataArray = new JSONArray(data);

            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            JSONObject attributes = new JSONObject(provider.getAttributes());

            for (int i = 0; i < attrDataArray.length(); i++) {
                JSONObject o = attrDataArray.getJSONObject(i);
                String attrUuid = o.getString("uuid");
                String attrValue = o.getString("value");
                Attribute attr = em.find(Attribute.class, attrUuid);
                if (attr == null) {
                    throw new JSONException(String.format("Invalid attribute uuid '%s'.", attrUuid));
                }
                attributes.put(attrUuid, attrValue);
            }

            em.getTransaction().begin();
            provider.setAttributes(attributes.toString());
            em.getTransaction().commit();

            return Response.noContent().build();
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Invalid JSON data: " + e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{attrUuid:[\\w-]+}")
    public Response getAttribute(@PathParam("attrUuid") String attrUuid) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject attributes = new JSONObject(provider.getAttributes());
            if (!attributes.has(attrUuid)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            String attrValue = attributes.getString(attrUuid);

            Attribute attr = em.find(Attribute.class, attrUuid);
            if (attr == null) {
                throw new Exception(String.format(
                        "Invalid attribute with UUID '%s' found at provider %s.", attrUuid, provider.getUuid()));
            }

            JSONObject attrData = new JSONObject();
            attrData.put("name", attr.getName());
            attrData.put("value", attrValue);
            attrData.put("uri",
                    UriBuilder.fromResource(ProviderAttributesResource.class).path(attrUuid).build(providerUuid));

            return Response.ok(attrData.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{attrUuid:[\\w-]+}")
    public Response updateAttribute(@PathParam("attrUuid") String attrUuid, String data) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject attributes = new JSONObject(provider.getAttributes());
            if (!attributes.has(attrUuid)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject o = new JSONObject(data);
            String attrValue = o.getString("value");
            attributes.put(attrUuid, attrValue);

            em.getTransaction().begin();
            provider.setAttributes(attributes.toString());
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Invalid JSON data: " + e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Path("{attrUuid:[\\w-]+}")
    public Response removeAttribute(@PathParam("attrUuid") String attrUuid) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject attributes = new JSONObject(provider.getAttributes());
            if (!attributes.has(attrUuid)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            attributes.remove(attrUuid);

            em.getTransaction().begin();
            provider.setAttributes(attributes.toString());
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
