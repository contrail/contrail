package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ServerDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Server;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/servers/{serverId}")
public class ServerResource {
    private static Logger log = Logger.getLogger(ServerResource.class);
    private String providerUuid;
    private int serverId;

    public ServerResource(@PathParam("providerUuid") String providerUuid, @PathParam("serverId") int serverId) {
        this.providerUuid = providerUuid;
        this.serverId = serverId;
    }

    /**
     * Returns the JSON representation of the given server.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getServer() throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getServer(ID=%d) started.", serverId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Server server = new ServerDAO(em).findById(providerUuid, serverId);
            if (server == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            org.json.JSONObject json = server.toJSON();
            json.put("uri", RestUriBuilder.getServerUri(server));

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected server.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateServer(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateServer(ID=%d) started. Data: %s", serverId, requestBody));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Server server = new ServerDAO(em).findById(providerUuid, serverId);
            if (server == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject(requestBody);

            em.getTransaction().begin();
            server.update(json);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            log.error("Update failed: ", e);
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Update failed: %s.", e.getMessage())).
                            build());
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected server.
     *
     * @return
     */
    @DELETE
    public Response removeServer() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeServer(ID=%d) started.", serverId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Server server = new ServerDAO(em).findById(providerUuid, serverId);
            if (server == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(server);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
