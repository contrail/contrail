/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserhasIdentityProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.IdentityProvider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProvider;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author ales
 */
@Path("/users/{userUuid}/ids/{idpUuid}")
public class UserIdentityProviderResource {

    protected static Logger logger =
            Logger.getLogger(UserIdentityProviderResource.class);

    private String userUuid;
    private String idpUuid;

    public UserIdentityProviderResource(@PathParam("userUuid") String userUuid, @PathParam("idpUuid") String idpUuid) {
        this.userUuid = userUuid;
        this.idpUuid = idpUuid;
    }

    @GET
    @Produces("application/json")
    public Response get() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            UserhasidentityProvider uidp = new UserhasIdentityProviderDAO(em).findById(user.getUserId(), idpUuid);
            if (uidp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject json = new JSONObject();
            json.put("userId", String.format("/users/%d", user.getUserId()));
            json.put("identityProviderId", String.format("/idps/%s", uidp.getIdentityProvider().getIdpUuid()));
            json.put("identity", uidp.getIdentity());
            json.put("attributes", uidp.getAttributes());
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering delete user idp.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            UserhasidentityProvider uidp = new UserhasIdentityProviderDAO(em).findById(user.getUserId(), idpUuid);
            if (uidp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            IdentityProvider idp = uidp.getIdentityProvider();

            em.getTransaction().begin();
            user.getUserhasidentityProviderList().remove(uidp);
            idp.getUserhasidentityProviderList().remove(uidp);
            em.remove(uidp);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.delete(
                        String.format("users/%d/ids/%s", user.getUserId(), idp.getIdpUuid()));
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting delete user idp.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject idData) throws Exception {
        logger.debug("Entering user put");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            UserhasidentityProvider uidp = new UserhasIdentityProviderDAO(em).findById(user.getUserId(), idpUuid);
            if (uidp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            IdentityProvider idp = uidp.getIdentityProvider();

            em.getTransaction().begin();

            if (idData.has("identity"))
                uidp.setIdentity(idData.getString("identity"));
            if (idData.has("attributes"))
                uidp.setAttributes(idData.getString("attributes"));

            em.getTransaction().commit();

            try {
                FederationIdentityProvider.put(String.format("users/%s/ids/%s", user.getUserId(),
                        idp.getIdpUuid()), idData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception err) {
            logger.error(err.getMessage());
            return Response.serverError().build();
        }
        finally {
            logger.debug("Exiting user put");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
