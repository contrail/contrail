package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ClusterDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.ServerDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.VmDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Cluster;
import org.ow2.contrail.federation.federationdb.jpa.entities.Server;
import org.ow2.contrail.federation.federationdb.jpa.entities.Vm;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/providers/{providerUuid}/clusters/{clusterId}")
public class ClusterResource {
    private static Logger log = Logger.getLogger(ClusterResource.class);
    private String providerUuid;
    private int clusterId;

    public ClusterResource(@PathParam("providerUuid") String providerUuid, @PathParam("clusterId") int clusterId) {
        this.providerUuid = providerUuid;
        this.clusterId = clusterId;
    }

    /**
     * Returns the JSON representation of the given cluster.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getCluster() throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getCluster(ID=%d) started.", clusterId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            org.json.JSONObject json = cluster.toJSON();
            String resourceUri = RestUriBuilder.getClusterUri(cluster);
            json.put("uri", resourceUri);
            json.put("servers", resourceUri + "/servers");
            json.put("vms", resourceUri + "/vms");

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected cluster.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateCluster(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateCluster(ID=%d) started. Data: %s", clusterId, requestBody));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject json = new JSONObject(requestBody);

            em.getTransaction().begin();
            cluster.update(json);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            log.error("Update failed: ", e);
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Update failed: %s.", e.getMessage())).
                            build());
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected cluster.
     *
     * @return
     */
    @DELETE
    public Response removeCluster() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeCluster(ID=%d) started.", clusterId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(cluster);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all servers registered at the given cluster.
     *
     * @return
     */
    @GET
    @Path("/servers")
    @Produces("application/json")
    public Response getServers() throws Exception {
        log.trace("getServers() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray json = new JSONArray();
            for (Server server : cluster.getServerList()) {
                JSONObject o = new JSONObject();
                String uri = String.format("%s/servers/%d", RestUriBuilder.getClusterUri(cluster), server.getServerId());
                o.put("uri", uri);
                o.put("baseUri", RestUriBuilder.getServerUri(server));
                json.put(o);
            }

            log.trace("getServers() finished successfully.");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Registers server at the selected cluster.
     *
     * @return
     */
    @POST
    @Path("/servers")
    @Consumes("application/json")
    public Response registerServer(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("registerServer() started. Data: " + requestBody);
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Server server;
            try {
                JSONObject json = new JSONObject(requestBody);
                String serverURI = json.getString("serverURI");
                Pattern uriPattern = Pattern.compile(
                        String.format("^/providers/%d/servers/(\\d+)$", cluster.getProviderId().getProviderId()));
                Matcher m = uriPattern.matcher(serverURI);
                if (!m.find()) {
                    throw new Exception("Invalid server URI: " + serverURI);
                }
                int serverId = Integer.parseInt(m.group(1));
                server = new ServerDAO(em).findById(providerUuid, serverId);
                if (server == null) {
                    throw new Exception(String.format("Server '%s' not found.", serverURI));
                }
            }
            catch (Exception e) {
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
            }

            if (!cluster.getServerList().contains(server)) {
                em.getTransaction().begin();
                server.getClusterList().add(cluster);
                cluster.getServerList().add(server);
                em.getTransaction().commit();
            }
            // no problem if server is already registered

            URI resourceUri = new URI(String.format("/%d", server.getServerId()));
            log.trace("registerServer() finished successfully.");
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns data about specific server registration
     *
     * @return
     */
    @GET
    @Path("/servers/{id}")
    @Produces("application/json")
    public Response getServerRegistration(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getServerRegistration(%d) started.", id));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            for (Server server : cluster.getServerList()) {
                if (server.getServerId() == id) {
                    JSONObject o = new JSONObject();
                    String uri = String.format("%s/servers/%d", RestUriBuilder.getClusterUri(cluster), server.getServerId());
                    o.put("uri", uri);
                    o.put("baseUri", RestUriBuilder.getServerUri(server));
                    log.trace("getServerRegistration() finished successfully. Server registration found.");
                    return Response.ok(o.toString()).build();
                }
            }

            log.trace("Server is not registered at given cluster.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Unregisters given server from given cluster.
     *
     * @return
     */
    @DELETE
    @Path("/servers/{id}")
    @Produces("application/json")
    public Response unregisterServer(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("unregisterServer(%d) started.", id));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            for (Server server : cluster.getServerList()) {
                if (server.getServerId() == id) {
                    em.getTransaction().begin();
                    server.getClusterList().remove(cluster);
                    cluster.getServerList().remove(server);
                    em.getTransaction().commit();

                    return Response.status(Response.Status.NO_CONTENT).build();
                }
            }

            log.trace("Server is not registered at given cluster.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all vms registered at the given cluster.
     *
     * @return
     */
    @GET
    @Path("/vms")
    @Produces("application/json")
    public Response getVMs() throws Exception {
        log.trace("getVms() started.");
        Cluster cluster = new ClusterDAO().findById(providerUuid, clusterId);
        if (cluster == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONArray json = new JSONArray();
        for (Vm vm : cluster.getVmList()) {
            JSONObject o = new JSONObject();
            String uri = String.format("%s/vms/%d", RestUriBuilder.getClusterUri(cluster), vm.getVmId());
            o.put("uri", uri);
            o.put("baseUri", RestUriBuilder.getVmUri(vm));
            json.put(o);
        }

        log.trace("getVms() finished successfully.");
        return Response.ok(json.toString()).build();
    }

    /**
     * Registers vm at the selected cluster.
     *
     * @return
     */
    @POST
    @Path("/vms")
    @Consumes("application/json")
    public Response registerVM(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("registerVm() started. Data: " + requestBody);
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Vm vm;
            try {
                JSONObject json = new JSONObject(requestBody);
                String vmURI = json.getString("vmURI");
                Pattern uriPattern = Pattern.compile(
                        String.format("^/providers/%d/vms/(\\d+)$", cluster.getProviderId().getProviderId()));
                Matcher m = uriPattern.matcher(vmURI);
                if (!m.find()) {
                    throw new Exception("Invalid vm URI: " + vmURI);
                }
                int vmId = Integer.parseInt(m.group(1));
                vm = new VmDAO(em).findById(providerUuid, vmId);
                if (vm == null) {
                    throw new Exception(String.format("Vm '%s' not found.", vmURI));
                }
            }
            catch (Exception e) {
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
            }

            if (!cluster.getVmList().contains(vm)) {
                em.getTransaction().begin();
                vm.getClusterList().add(cluster);
                cluster.getVmList().add(vm);
                em.getTransaction().commit();
            }
            // no problem if vm is already registered

            URI resourceUri = new URI(String.format("/%d", vm.getVmId()));
            log.trace("registerVm() finished successfully.");
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns data about specific vm registration
     *
     * @return
     */
    @GET
    @Path("/vms/{id}")
    @Produces("application/json")
    public Response getVMRegistration(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getVmRegistration(%d) started.", id));
        }

        Cluster cluster = new ClusterDAO().findById(providerUuid, clusterId);
        if (cluster == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        for (Vm vm : cluster.getVmList()) {
            if (vm.getVmId() == id) {
                JSONObject o = new JSONObject();
                String uri = String.format("%s/vms/%d", RestUriBuilder.getClusterUri(cluster), vm.getVmId());
                o.put("uri", uri);
                o.put("baseUri", RestUriBuilder.getVmUri(vm));
                log.trace("getVmRegistration() finished successfully. Vm registration found.");
                return Response.ok(o.toString()).build();
            }
        }

        log.trace("Vm is not registered at given cluster.");
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

    /**
     * Unregisters given vm from given cluster.
     *
     * @return
     */
    @DELETE
    @Path("/vms/{id}")
    @Produces("application/json")
    public Response unregisterVM(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("unregisterVm(%d) started.", id));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Cluster cluster = new ClusterDAO(em).findById(providerUuid, clusterId);
            if (cluster == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            for (Vm vm : cluster.getVmList()) {
                if (vm.getVmId() == id) {
                    em.getTransaction().begin();
                    vm.getClusterList().remove(cluster);
                    cluster.getVmList().remove(vm);
                    em.getTransaction().commit();

                    return Response.status(Response.Status.NO_CONTENT).build();
                }
            }

            log.trace("Vm is not registered at given cluster.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all storages of the selected cluster.
     *
     * @return
     */
    @GET
    @Path("/storages")
    @Produces("application/json")
    public Response getStorages() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new storage for the selected cluster.
     *
     * @return
     */
    @POST
    @Path("/storages")
    public Response createStorage() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param storageId
     * @return
     */
    @Path("/storages/{sid}")
    public StorageResource findStorage(@PathParam("sid") int storageId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all networks of the selected cluster.
     *
     * @return
     */
    @GET
    @Path("/networks")
    @Produces("application/json")
    public Response getNetworks() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new network for the selected cluster.
     *
     * @return
     */
    @POST
    @Path("/networks")
    public Response createNetwork() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Sub-resource locator method. Returns the sub-resource object that can handle the remainder
     * of the request.
     *
     * @param networkId
     * @return
     */
    @Path("/networks/{nid}")
    public NetworkResource findNetwork(@PathParam("nid") int networkId) {
        throw new UnsupportedOperationException();
    }

}
