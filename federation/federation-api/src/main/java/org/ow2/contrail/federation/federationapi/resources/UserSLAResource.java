/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserSLADAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserSLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLA;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author ales.cernivec@xlab.si
 */
@Path("/users/{userUuid}/slas/{slaId}")
public class UserSLAResource {

    protected static Logger logger =
            Logger.getLogger(UserSLAResource.class);

    private String userUuid;
    private int slaId;

    public UserSLAResource(@PathParam("userUuid") String userUuid, @PathParam("slaId") int slaId) {
        this.userUuid = userUuid;
        this.slaId = slaId;
    }

    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            UserSLA userSLA = new UserSLADAO(em).findById(slaId);
            if (userSLA == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject sla = new JSONObject();
            sla.put("id", String.format("%s/slas/%d", RestUriBuilder.getUserUri(userSLA.getUserId()),
                    userSLA.getSLAId()));
            sla.put("sla", userSLA.getSla());
            sla.put("templateurl", userSLA.getTemplateURL());
            sla.put("userId", String.format("/users/%d", userSLA.getUserId().getUserId()));
            sla.put("content", userSLA.getContent());
            if (userSLA.getSLATemplateId() != null)
                sla.put("slatId", RestUriBuilder.getUserSlatUri(userSLA.getSLATemplateId()));
            else
                sla.put("slatId", JSONObject.NULL);
            return Response.ok(sla.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /* (non-Javadoc)
      * @see org.ow2.contrail.federation.federationapi.interfaces.BaseSingle#delete()
      */
    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering delete");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            UserSLA userSLA = new UserSLADAO(em).findById(slaId);
            if (userSLA == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            logger.debug("deleting userSLA");
            em.getTransaction().begin();
            UserSLATemplate userSlat = userSLA.getSLATemplateId();
            if (userSlat != null) {
                logger.debug("Removing UserSLA from UserSLATemplate" + userSlat.getSLATemplateId());
                userSlat.getUserSLAList().remove(userSLA);
            }
            em.remove(userSLA);
            em.getTransaction().commit();
            logger.debug("Exiting delete");
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject slaData) throws Exception {
        logger.debug("Entering put");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLA userSLA = new UserSLADAO(em).findById(slaId);
            if (userSLA == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();

            if (slaData.has("sla")) {
                userSLA.setSla(slaData.getString("sla"));
            }
            if (slaData.has("content")) {
                userSLA.setContent(slaData.getString("content"));
            }
            if (slaData.has("templateurl")) {
                userSLA.setTemplateURL(slaData.getString("templateurl"));
            }
            if (slaData.has("slatId")) {
                UserSLATemplate slat = new UserSLATemplateDAO(em).findById(
                        FederationDBCommon.getIdFromString(slaData.getString("slatId")));
                if (slat != null) {
                    logger.debug("Found UserSLATemplate with id " + slat.getSLATemplateId());
                    UserSLATemplate slatBefore = userSLA.getSLATemplateId();
                    if (slatBefore != null) {
                        logger.debug("Removing UserSLAResource from UserSLATemplate" + slatBefore.getSLATemplateId());
                        slatBefore.getUserSLAList().remove(userSLA);
                    }
                    userSLA.setSLATemplateId(slat);
                    slat.getUserSLAList().add(userSLA);
                }
                else {
                    logger.error("Could not found SLATemplate with id: " + slaData.getString("slatId"));
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
            }

            em.getTransaction().commit();

            logger.debug("Exiting put");
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

}
