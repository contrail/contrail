package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

public class CEEResource {

    private int providerId;
    private int voId;
    private int ceeId;

    public CEEResource(int providerId, int voId, int ceeId) {
        this.providerId = providerId;
        this.voId = voId;
        this.ceeId = ceeId;
    }

    /**
     * Returns the JSON representation of the given CEE.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getCEE() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the given CEE.
     *
     * @return
     */
    @PUT
    public Response updateCEE() {
        throw new UnsupportedOperationException();
    }
}
