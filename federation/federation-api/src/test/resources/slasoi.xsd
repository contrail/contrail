<?xml version="1.0" encoding="UTF-8"?>
<xs:schema
	targetNamespace="http://www.slaatsoi.eu/slamodel"
	elementFormDefault="qualified"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:slasoi="http://www.slaatsoi.eu/slamodel"
    >

    <!--
	SVN FILE: $Id: $ 

	Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	    * Redistributions of source code must retain the above copyright
	      notice, this list of conditions and the following disclaimer.
	    * Redistributions in binary form must reproduce the above copyright
	      notice, this list of conditions and the following disclaimer in the
	      documentation and/or other materials provided with the distribution.
	    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
	      names of its contributors may be used to endorse or promote products
	      derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	@author         $Author: $
	@version        $Rev: $
	@lastrevision   $Date: $
	@filesource     $URL: $
	-->
	
    <!-- Using XML Schema's extension mechanisms wherever possible -->
    
    <!-- SLA and SLATemplate model -->
    
    <!-- SLA Template -->
    <xs:element name="SLATemplate">
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="slasoi:AnnotatedType">
                    <xs:sequence>
                        <xs:element name="UUID" type="slasoi:UUIDType" minOccurs="1" maxOccurs="1"/>
                        <xs:element name="ModelVersion" type="xs:string" minOccurs="1" maxOccurs="1"/>
                        <xs:element name="Party" type="slasoi:AgreementPartyType" minOccurs="1" maxOccurs="unbounded"/>
                        <xs:element name="InterfaceDeclr" type="slasoi:InterfaceDeclrType" minOccurs="1" maxOccurs="unbounded"/>
                        <xs:element name="VariableDeclr" type="slasoi:VariableDeclrType" minOccurs="0" maxOccurs="unbounded"/>
                        <xs:element name="AgreementTerm" type="slasoi:AgreementTermType" minOccurs="1" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    
    <!-- SLA -->
    <xs:element name="SLA">
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="slasoi:AnnotatedType">
                    <xs:sequence>
                        <xs:sequence>
                            <xs:element name="UUID" type="slasoi:UUIDType" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="ModelVersion" type="xs:string" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="EffectiveFrom" type="slasoi:TimeType" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="EffectiveUntil" type="slasoi:TimeType" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="TemplateId" type="slasoi:UUIDType" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="AgreedAt" type="slasoi:TimeType" minOccurs="1" maxOccurs="1"/>
                            <xs:element name="Party" type="slasoi:AgreementPartyType" minOccurs="2" maxOccurs="unbounded"/>
                            <xs:element name="InterfaceDeclr" type="slasoi:InterfaceDeclrType" minOccurs="1" maxOccurs="unbounded"/>
                            <xs:element name="VariableDeclr" type="slasoi:VariableDeclrType" minOccurs="0" maxOccurs="unbounded"/>
                            <xs:element name="AgreementTerm" type="slasoi:AgreementTermType" minOccurs="1" maxOccurs="unbounded"/>
                        </xs:sequence>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    
    <!-- AgreementParty -->
    <xs:complexType name="AgreementPartyType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Operative" type="slasoi:OperativeType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Role" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- AgreementOperative -->
    <xs:complexType name="OperativeType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- VariableDeclr -->
    <xs:complexType name="VariableDeclrType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:choice>
                    <xs:sequence>
                        <xs:element name="Var" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                        <xs:element name="Expr" type="slasoi:ExprType" minOccurs="1" maxOccurs="1"/>
                    </xs:sequence>
                    <xs:element name="Customisable" type="slasoi:CustomisableType" minOccurs="1"/>
                </xs:choice>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- CustomisableType -->
    <xs:complexType name="CustomisableType">
        <xs:sequence>
            <xs:element name="Var" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Value" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Expr" type="slasoi:DomainExprType" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- AgreementTermType -->
    <xs:complexType name="AgreementTermType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Precondition" type="slasoi:ConstraintExprType" minOccurs="0" maxOccurs="1"/>
                    <xs:element name="VariableDeclr" type="slasoi:VariableDeclrType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Guaranteed" type="slasoi:GuaranteedType" minOccurs="1" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- Guaranteed -->
    <xs:complexType name="GuaranteedType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:choice>
                    <xs:element name="State" type="slasoi:GuaranteedStateType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Action" type="slasoi:GuaranteedActionType" minOccurs="1" maxOccurs="1"/>
                </xs:choice>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- GuaranteedState -->
    <xs:complexType name="GuaranteedStateType">
        <xs:sequence>
            <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Priority" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1" nillable="true"/>
            <xs:element name="Constraint" type="slasoi:ConstraintExprType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Precondition" type="slasoi:ConstraintExprType" minOccurs="0" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- GuaranteedAction -->
    <xs:complexType name="GuaranteedActionType">
        <xs:sequence>
            <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="ActorRef" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Policy" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Precondition" type="slasoi:EventExprType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Postcondition" type="slasoi:GuaranteedActionDefnType" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
      <!-- GuaranteedActionDefn -->
    <xs:complexType name="GuaranteedActionDefnType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:choice>
                    <xs:element name="TerminationAction" type="slasoi:TerminationActionType" minOccurs="1" maxOccurs="1"/>
					<xs:element name="PenaltyAction" type="slasoi:PenaltyActionType" minOccurs="1" maxOccurs="1"/>
					<xs:element name="ProductOfferingPrice" type="slasoi:ProductOfferingPriceType" minOccurs="1" maxOccurs="1"/>
					<xs:element name="TerminationClause" type="slasoi:TerminationClauseType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="CompoundAction" type="slasoi:CompoundActionType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="CustomAction" type="slasoi:CustomActionType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Invocation" type="slasoi:InvocationType" minOccurs="1" maxOccurs="1"/>
                </xs:choice>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
	<!-- Termination Action Type-->
	<xs:complexType name="TerminationActionType">
				<xs:sequence>
					<xs:element name="Name" type="xs:string" minOccurs="1" maxOccurs="1"/>
					<xs:element name="TerminationClauseID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
    </xs:complexType>
	<!-- Penalty Action Type-->
	<xs:complexType name="PenaltyActionType">
				<xs:sequence>
					<xs:element name="Price" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
    </xs:complexType>	
	
<!--Product Offering Price Type-->		
	<xs:complexType name="ProductOfferingPriceType">
				<xs:sequence>
				    <xs:element name="ID" type="slasoi:IDType"/>
					<xs:element name="Product" type="slasoi:ProductType" minOccurs="0" maxOccurs="1"/>
					<xs:element name="Name" type="xs:string"/>
					<xs:element name="Description" type="xs:string"/>
					<xs:element name="BillingFrequency" type="slasoi:STNDType"/>												
					<xs:element name="ValidFrom" type="slasoi:TimeType"/>
					<xs:element name="ValidUntil" type="slasoi:TimeType"/>
					<xs:element name="ComponentProdOfferingPrice" maxOccurs="unbounded" type="slasoi:ComponentProdOfferingPriceType"/>												
				</xs:sequence>
	</xs:complexType>
<!--Product  Type-->	
<xs:complexType name="ProductType">
    <xs:complexContent>
        <xs:extension base="slasoi:AnnotatedType">
			<xs:sequence>
				<xs:element name="ID" type="slasoi:IDType"/>
				<xs:element name="Name" type="xs:string"/>
			</xs:sequence>
	    </xs:extension>
	</xs:complexContent>
</xs:complexType>
<!--Component Offer Price Type-->	
<xs:complexType name="ComponentProdOfferingPriceType">
			<xs:sequence>
				<xs:element name="ID" type="slasoi:IDType"/>
				<xs:element name="PriceType" type="slasoi:STNDType"/>
				<xs:element name="Price" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1"/>
				<xs:element name="Quantity" type="slasoi:CONSTType"/>
				<xs:element name="PriceModification" minOccurs="0" maxOccurs="unbounded" type="slasoi:PriceModificationType"/>		
			</xs:sequence>
</xs:complexType>

<!--Price Modification Type-->		
	<xs:complexType name="PriceModificationType">
				<xs:sequence>
						<xs:element name="Type" type="slasoi:STNDType"/>																		
						<xs:element name="Value" type="slasoi:CONSTType"/>							
				</xs:sequence>
	</xs:complexType>
<!--Modification Price Type-->		
	<!--xs:complexType name="ModificationPriceType">
				<xs:choice>
					<xs:element name="Amount" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Percentage" type="slasoi:CONSTType" minOccurs="1" maxOccurs="1"/>				
				</xs:choice>
	</xs:complexType-->												
					
	 <!-- TerminationClauseType-->
	 <xs:complexType name="TerminationClauseType">
		<xs:sequence>
			<xs:element name="ID" type="slasoi:IDType"/>
			<xs:element name="TerminationInitiator" type="slasoi:STNDType"/>
			<xs:element name="TerminationClauseType" type="xs:string"/>
			<xs:element name="TerminationClauseDescription" type="xs:string"/>
			<xs:element name="NotificationPeriod" type="slasoi:CONSTType"/>
			<xs:element name="NotificationMethod" type="slasoi:STNDType"/>
			<xs:element name="Fees" minOccurs="0" type="slasoi:CONSTType"/>
		</xs:sequence>
	 </xs:complexType>
    
    <!-- CompoundAction -->
    <xs:complexType name="CompoundActionType">
    </xs:complexType>
    
    <!-- CustomAction -->
    <xs:complexType name="CustomActionType">
                <xs:sequence>
                    <xs:any processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
    </xs:complexType>
    
    <!-- Invocation -->
    <xs:complexType name="InvocationType">
                <xs:sequence>
                    <xs:element name="Endpoint" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Operation" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Parameters" type="slasoi:MapIdValueExpr" minOccurs="0" maxOccurs="1"/>
                </xs:sequence>
    </xs:complexType>
    
    <!-- Service Descriptions -->
    
    <!-- InterfaceDeclrType -->
    <xs:complexType name="InterfaceDeclrType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="ProviderRef" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Endpoint" type="slasoi:EndpointType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Interface" type="slasoi:InterfaceType" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- Endpoint -->
    <xs:complexType name="EndpointType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Location" type="slasoi:UUIDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Protocol" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- Interface -->
    <xs:complexType name="InterfaceType">
        <xs:choice>
            <xs:element name="InterfaceRef" type="slasoi:InterfaceRefType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="InterfaceSpec" type="slasoi:InterfaceSpecType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="InterfaceResourceType" type="slasoi:InterfaceResourceTypeType" minOccurs="1" maxOccurs="1"/>
        </xs:choice>
    </xs:complexType>
    
    <!-- InterfaceRefType -->
    <xs:complexType name="InterfaceRefType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="InterfaceLocation" type="slasoi:UUIDType"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- InterfaceResourceTypeType -->
    <xs:complexType name="InterfaceResourceTypeType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Name" type="xs:string" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- InterfaceSpecType -->
    <xs:complexType name="InterfaceSpecType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Name" type="xs:string" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Extended" type="slasoi:IDType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Operation" type="slasoi:InterfaceOperationType" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- InterfaceOperationType -->
    <xs:complexType name="InterfaceOperationType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Name" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Input" type="slasoi:InterfaceOperationPropertyType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Output" type="slasoi:InterfaceOperationPropertyType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Related" type="slasoi:InterfaceOperationPropertyType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Fault" type="slasoi:STNDType" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- InterfaceOperationProperty -->
    <xs:complexType name="InterfaceOperationPropertyType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Name" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Auxiliary" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Datatype" type="slasoi:STNDType" minOccurs="0" maxOccurs="1"/>
                    <xs:element name="Domain" type="slasoi:DomainExprType" minOccurs="0" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- ServceRef -->
    <xs:complexType name="ServiceRefType">
        <xs:sequence>
            <xs:element name="InterfaceList" type="slasoi:IDListType" minOccurs="0" maxOccurs="1"/>
            <xs:element name="OperationList" type="slasoi:IDListType" minOccurs="0" maxOccurs="1"/>
            <xs:element name="EndpointList" type="slasoi:IDListType" minOccurs="0" maxOccurs="1"/>            
        </xs:sequence>
    </xs:complexType>
    
    <!-- IDList -->
    <xs:complexType name="IDListType">
        <xs:sequence>
            <xs:element name="ID" type="slasoi:IDType" minOccurs="1" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
    
    
    <!-- Ground Expressions -->
    
    <!-- Annotated -->
    <xs:complexType name="AnnotatedType">
        <xs:sequence>
            <xs:element name="Text" type="xs:string" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Properties" type="slasoi:MapStndAny" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- MAP<STND, ANY> -->
    <xs:complexType name="MapStndAny">
        <xs:sequence>
            <xs:element name="Entry" type="slasoi:StndAnyEntryType" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- Map<ID, ValueExpr -->
    <xs:complexType name="MapIdValueExpr">
        <xs:sequence>
            <xs:element name="Entry" type="slasoi:IdValueExprEntryType" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- StndAnyEntry -->
    <xs:complexType name="StndAnyEntryType">
        <xs:sequence>
            <xs:element name="Key" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Value" type="xs:anyType" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- IdValueExprEntry -->
    <xs:complexType name="IdValueExprEntryType">
        <xs:sequence>
            <xs:element name="Key" type="slasoi:IDType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Value" type="slasoi:ValueExprType" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- ConstraintExpression -->
    <xs:complexType name="ConstraintExprType">
        <xs:choice>
            <xs:element name="CompoundConstraintExpr" type="slasoi:CompoundConstraintExprType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="TypeConstraintExpr" type="slasoi:TypeConstraintExprType" minOccurs="1" maxOccurs="1"/>
        </xs:choice>
    </xs:complexType>
    
    <!-- TypeConstraintExpr -->
    <xs:complexType name="TypeConstraintExprType">
            <xs:sequence>
                <xs:element name="Value" type="slasoi:ValueExprType" minOccurs="1" maxOccurs="1"/>
                <xs:element name="Domain" type="slasoi:DomainExprType" minOccurs="1" maxOccurs="1"/>
                <xs:element name="Error" type="slasoi:CONSTType" minOccurs="0" maxOccurs="1"/>
            </xs:sequence>
    </xs:complexType>
    
    <!-- CompoundConstraintExpr -->
    <xs:complexType name="CompoundConstraintExprType">
        <xs:choice>
           <xs:sequence>
               <xs:element name="Subexpression" type="slasoi:ConstraintExprType" minOccurs="1" maxOccurs="unbounded"/>
               <xs:element name="LogicalOp" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
           </xs:sequence>
        </xs:choice>
    </xs:complexType>
    
    <!-- DomainExpr -->
    <xs:complexType name="DomainExprType">
        <xs:choice>
            <xs:element name="SimpleDomainExpr" type="slasoi:SimpleDomainExprType" minOccurs="1" maxOccurs="1"/>
            <xs:element name="CompoundDomainExpr" type="slasoi:CompoundDomainExprType" minOccurs="1" maxOccurs="1"/>
        </xs:choice>
    </xs:complexType>
    
    <!-- SimpleDomainExpr -->
    <xs:complexType name="SimpleDomainExprType">
        <xs:sequence>
            <xs:element name="ComparisonOp" type="slasoi:STNDType" minOccurs="1" maxOccurs="1" />
            <xs:element name="Value" type="slasoi:ValueExprType" minOccurs="1" maxOccurs="1" />
        </xs:sequence>
    </xs:complexType>
    
    <!-- CompoundDomainExpr -->
    <xs:complexType name="CompoundDomainExprType">
        <xs:choice>
            <xs:sequence>
                <xs:element name="Subexpression" type="slasoi:DomainExprType" minOccurs="1" maxOccurs="unbounded"/>
                <xs:element name="LogicalOp" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
            </xs:sequence>
        </xs:choice>
    </xs:complexType>
    
    <!-- EventExpr -->
    <xs:complexType name="EventExprType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Operator" type="slasoi:STNDType" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="Parameter" type="slasoi:ExprType" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- FuncExpr -->
    <xs:complexType name="FuncExprType">
        <xs:complexContent>
            <xs:extension base="slasoi:AnnotatedType">
                <xs:sequence>
                    <xs:element name="Operator" type="slasoi:STNDType"/>
                    <xs:element name="Parameter" type="slasoi:ValueExprType" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <!-- Primitives -->
    
    <!-- Expr -->
    <xs:complexType name="ExprType">
        <xs:choice>
            <xs:element name="ValueExpr" type="slasoi:ValueExprType"/>
            <xs:element name="ConstraintExpr" type="slasoi:ConstraintExprType"/>
        </xs:choice>
    </xs:complexType>
    
    <!-- ValueExpr -->
    <xs:complexType name="ValueExprType">
        <xs:choice>
            <xs:element name="ID" type="slasoi:IDType"/>
            <xs:element name="BOOL" type="slasoi:BoolType"/>
            <xs:element name="CONST" type="slasoi:CONSTType"/>
            <xs:element name="TIME" type="slasoi:TimeType"/>
            <xs:element name="PATH" type="slasoi:PathType"/>
            <xs:element name="UUID" type="slasoi:UUIDType"/>
            <xs:element name="STND" type="slasoi:STNDType"/>
            <xs:element name="FuncExpr" type="slasoi:FuncExprType"/>
            <xs:element name="EventExpr" type="slasoi:EventExprType"/>
            <xs:element name="DomainExpr" type="slasoi:DomainExprType"/>
            <xs:element name="ServiceRef" type="slasoi:ServiceRefType"/>
            <xs:element name="ListValueExpr" type="slasoi:ListValueExprType"/>
        </xs:choice>
    </xs:complexType>
    
    <!-- UUIDType -->
    <xs:simpleType name="UUIDType">
        <xs:union memberTypes="xs:anyURI"/>
    </xs:simpleType>
    
    <!-- STNDTType -->
    <xs:simpleType name="STNDType">
        <xs:union memberTypes="slasoi:UUIDType"/>
    </xs:simpleType>
    
    <!-- Time -->
    <xs:simpleType name="TimeType">
        <xs:union memberTypes="xs:dateTime"/>
    </xs:simpleType>
    
    <!-- Bool -->
    <xs:simpleType name="BoolType">
        <xs:union memberTypes="xs:boolean"/>
    </xs:simpleType>
    
    <!-- IDType -->
    <xs:simpleType name="IDType">
        <xs:union memberTypes="slasoi:PathType slasoi:UUIDType">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:pattern value="[A-Za-z0-9\-._~!$&amp;'\(\)\*\+,;=:@%/?]*"/>
                    <xs:whiteSpace value="collapse"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:union>
    </xs:simpleType>
    
    <!-- PathType -->
    <xs:simpleType name="PathType">
        <xs:restriction base="xs:string">
            <xs:pattern value="[A-Za-z0-9][A-Za-z0-9\-._~!$&amp;'\(\)\*\+,;=:@%/?]*/|#[A-Za-z0-9][A-Za-z0-9\-._~!$&amp;'\(\)\*\+,;=:@%/?]*"></xs:pattern>
        </xs:restriction>
    </xs:simpleType>
    
    <!-- CONSTType -->
    <xs:complexType name="CONSTType">
        <xs:sequence>
            <xs:element name="Value" type="xs:string" minOccurs="1" maxOccurs="1"/>
            <xs:element name="Datatype" type="slasoi:STNDType" minOccurs="0" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>
    
    <!-- List<ValueExpr> -->
    <xs:complexType name="ListValueExprType">
        <xs:sequence>
            <xs:element name="Value" type="slasoi:ValueExprType" minOccurs="1" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
	
    
    <!-- Types needed as elements -->
    <xs:element name="Party" type="slasoi:AgreementPartyType"/>
    <xs:element name="ModelVersion" type="xs:string"/>
    <xs:element name="Annotated" type="slasoi:AnnotatedType"/>
    <xs:element name="InterfaceDeclr" type="slasoi:InterfaceDeclrType"/>
    <xs:element name="DomainExpr" type="slasoi:DomainExprType"/>
    <xs:element name="EventExpr" type="slasoi:EventExprType"/>
    <xs:element name="FuncExpr" type="slasoi:FuncExprType"/>
    <xs:element name="ConstraintExpr" type="slasoi:ConstraintExprType"/>
    <xs:element name="VariableDeclr" type="slasoi:VariableDeclrType"/>
    <xs:element name="AgreementTerm" type="slasoi:AgreementTermType"/>
    <xs:element name="InterfaceOperation" type="slasoi:InterfaceOperationType"/>
    <xs:element name="Related" type="slasoi:InterfaceOperationPropertyType"/>
    <xs:element name="EffectiveFrom" type="slasoi:TimeType"/>
    <xs:element name="EffectiveUntil" type="slasoi:TimeType"/>
    <xs:element name="TemplateId" type="slasoi:UUIDType"/>
    <xs:element name="AgreedAt" type="slasoi:TimeType"/>
    
</xs:schema>
