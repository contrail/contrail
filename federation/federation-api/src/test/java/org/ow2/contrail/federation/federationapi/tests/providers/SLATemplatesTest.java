package org.ow2.contrail.federation.federationapi.tests.providers;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.core.impl.FederationCoreBasic;
import org.ow2.contrail.federation.federationapi.resources.ISLATemplateResource;
import org.ow2.contrail.federation.federationapi.resources.ProviderResource;
import org.ow2.contrail.federation.federationapi.resources.ProvidersResource;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;

public class SLATemplatesTest {
    private static Logger log = Logger.getLogger(SLATemplatesTest.class);

    @Before
    public void setUp() {
        PersistenceUtils.createInstance("testPU");
        MyServletContextListener.setFederationCore(new FederationCoreBasic());
    }

    @Test
    public void testAddSLATemplate() throws Exception {
        log.info("testAddSLATemplate() started.");
        ProvidersResource providersResource = new ProvidersResource();
        Response response;
        // register new provider
        JSONObject providerJson = new JSONObject();
        providerJson.put("name", "ProviderA");
        providerJson.put("typeId", 3);
        providerJson.put("providerUri", "contrail://providerA");
        response = providersResource.addProvider(providerJson.toString());
        assertEquals(response.getStatus(), 201);
        ProviderResource providerResource = providersResource.findProvider(1);

        // add new SLA template
        JSONObject slatJson = new JSONObject();
        slatJson.put("name", "Gold SLA Template");
        slatJson.put("url", "http://contrail.xlab.si/slats/GoldSLAT.xml");
        response = providerResource.addSLATemplate(slatJson.toString());
        assertEquals(response.getStatus(), 201);

        // get registered SLATs list
        response = providerResource.getSLATemplates();
        assertEquals(response.getStatus(), 200);
        JSONArray array = new JSONArray((String) response.getEntity());
        JSONObject o = array.getJSONObject(0);
        assertEquals(o.getString("name"), slatJson.getString("name"));
        assertEquals(o.getString("url"), slatJson.getString("url"));
        assertEquals(o.getString("uri"), "/providers/1/slats/1");

        // get SLAT 1
        ISLATemplateResource slatResource = providerResource.findSLATemplate(1);
        response = slatResource.getSLATemplate();
        assertEquals(response.getStatus(), 200);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), slatJson.getString("name"));
        assertEquals(o.getString("url"), slatJson.getString("url"));

        // update SLA template
        slatJson.put("name", "Silver SLA Template");
        response = slatResource.updateSLATemplate(slatJson.toString());
        assertEquals(response.getStatus(), 204);

        // get updated SLAT
        slatResource = providerResource.findSLATemplate(1);
        response = slatResource.getSLATemplate();
        assertEquals(response.getStatus(), 200);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), slatJson.getString("name"));
        assertEquals(o.getString("url"), slatJson.getString("url"));

        // remove SLAT
        slatResource.removeSLATemplate();

        // get registered SLATs list - should be empty
        response = providerResource.getSLATemplates();
        assertEquals(response.getStatus(), 200);
        array = new JSONArray((String) response.getEntity());
        assertEquals(array.length(), 0);

        log.info("testAddSLATemplate() finished successfully.");
    }

    @After
    public void tearDown() throws Exception {
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }
}
