package org.ow2.contrail.federation.federationapi.authorization.herasaf;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.ow2.contrail.federation.federationapi.authorization.Action;
import org.ow2.contrail.federation.federationapi.authorization.Authorizer;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.URole;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HerasafAuthorizerTest {
    private static Logger log = Logger.getLogger(HerasafAuthorizerTest.class);
    private static final String USER_UUID = "ABCDE";
    private Authorizer authorizer;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        PersistenceUtils.createInstance("testPU");

        MockServletContext sc = new MockServletContext("");
        sc.addInitParameter(ContextLoader.CONFIG_LOCATION_PARAM,
                "/herasaf-authorizer-test-context.xml");
        ServletContextListener listener = new ContextLoaderListener();
        ServletContextEvent event = new ServletContextEvent(sc);
        listener.contextInitialized(event);

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        authorizer = (Authorizer) ctx.getBean("authorizer");

        User user = new User();
        user.setUuid(USER_UUID);

        URole role = new URole();
        role.setName("FederationUser");
        List<URole> roleList = new ArrayList<URole>();
        roleList.add(role);
        user.setURoleList(roleList);

        EntityManager em = null;
        try {
            em = PersistenceUtils.getInstance().getEntityManager();
            em.getTransaction().begin();
            em.persist(role);
            em.persist(user);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }

    }

    /*
    Test authorization based on XACML policy 'xacml-test-policy.xml'
     */
    @Test
    public void testAuthorize() throws Exception {
        log.trace("testAuthorize() started.");

        User user = UserDAO.findByUuid(USER_UUID);

        // access should be permitted
        assertTrue(
                authorizer.isAuthorized(user, "/users/ABCDE/ovfs", Action.READ));

        // access should be denied
        assertFalse(
                authorizer.isAuthorized(user, "/users/ABCDE1", Action.READ));

        assertFalse(
                authorizer.isAuthorized(user, "/providers/1/slats", Action.WRITE));

        log.trace("testAuthorize() finished successfully.");
    }

    @After
    public void tearDown() throws Exception {
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }
}
