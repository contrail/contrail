package org.ow2.contrail.federation.federationapi;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;

import java.util.UUID;

public class RestApiStressTest {
    private static String baseUrl = "http://localhost:8080/federation-api";
    private static int NUMBER_OF_THREADS = 20;
    private static int NUMBER_OF_CYCLES = 200;

    public void stressTest() throws Exception {
        // create provider
        JSONObject providerJson = new JSONObject();
        providerJson.put("name", "Test Provider-" + UUID.randomUUID().toString());
        providerJson.put("providerUri", "http://testprovider.com");
        providerJson.put("typeId", 8);
        PostMethod post = new PostMethod(baseUrl + "/providers");
        RequestEntity requestEntity = new StringRequestEntity(providerJson.toString(), "application/json", "UTF-8");
        post.setRequestEntity(requestEntity);
        HttpClient client = new HttpClient();
        int status = client.executeMethod(post);
        if (status != 201) {
            throw new Exception("Failed to create provider resource: invalid response code " + status);
        }
        String providerUri = post.getResponseHeader("Location").getValue();

        Thread[] threads = new Thread[NUMBER_OF_THREADS];
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            WorkerThread workerThread = new WorkerThread(providerUri, i + "_", NUMBER_OF_CYCLES);
            Thread thread = new Thread(workerThread);
            thread.start();
            threads[i] = thread;
        }
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            threads[i].join();
        }

        // delete provider
        DeleteMethod deleteMethod = new DeleteMethod(providerUri);
        status = client.executeMethod(deleteMethod);
    }

    public static void main(String[] args) throws Exception {
        new RestApiStressTest().stressTest();
    }

    class WorkerThread implements Runnable {

        private String providerUri;
        private String threadName;
        private int num;

        public WorkerThread(String providerUri, String threadName, int num) {

            this.providerUri = providerUri;
            this.threadName = threadName;
            this.num = num;
        }

        @Override
        public void run() {
            HttpClient client = new HttpClient();
            for (int i = 0; i < num; i++) {
                try {
                    // add server
                    JSONObject serverJson = new JSONObject();
                    String name = threadName + i;
                    serverJson.put("name", name);
                    PostMethod post = new PostMethod(providerUri + "/servers");
                    RequestEntity requestEntity = new StringRequestEntity(serverJson.toString(),
                            "application/json", "UTF-8");
                    post.setRequestEntity(requestEntity);
                    int status = client.executeMethod(post);
                    if (status != 201) {
                        System.out.println(String.format(
                                "Failed to create server %s: invalid response code %d", name, status));
                    }
                    String serverUri = post.getResponseHeader("Location").getValue();

                    // delete server
                    DeleteMethod deleteMethod = new DeleteMethod(serverUri);
                    status = client.executeMethod(deleteMethod);
                }
                catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }
}
