package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.federation.federationapi.exceptions.NegotiationException;
import org.ow2.contrail.federation.federationapi.utils.Conf;

import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@Ignore
public class NegotiationTest {

    @Before
     public void initialize() throws Exception {
        Conf.getInstance().load();
    }

    @Test
    public void testNegotiation() throws java.lang.Exception {
        INegotiationClient negotiationClient = new NegotiationClient();
        String slat = new Scanner(new File("src/test/resources/ContrailSLAtemplateExampleAll-1.xml")).useDelimiter("\\Z").next();
        String negotiationID = negotiationClient.initiateNegotiation(slat);
        assertNotNull(negotiationID);

        String[] slatProposal = negotiationClient.negotiate(negotiationID, slat);
        assertNotNull(slatProposal[0]);

        String sla = negotiationClient.createAgreement(negotiationID, slatProposal[0]);
        assertNotNull(sla);
        assertTrue(sla.startsWith("<slam:SLA"));
    }

    @Test
    public void testCancelNegotiation() throws java.lang.Exception {
        INegotiationClient negotiationClient = new NegotiationClient();
        String slat = new Scanner(new File("src/test/resources/ContrailSLAtemplateExampleAll-1.xml")).useDelimiter("\\Z").next();
        String negotiationID = negotiationClient.initiateNegotiation(slat);
        assertNotNull(negotiationID);

        String[] slatProposal = negotiationClient.negotiate(negotiationID, slat);
        assertNotNull(slatProposal[0]);

        boolean status = negotiationClient.cancel(negotiationID, null);
        assertTrue(status);

        try {
            String sla = negotiationClient.createAgreement(negotiationID, slatProposal[0]);
            fail();
        }
        catch (NegotiationException e) {
        }
    }
}
    