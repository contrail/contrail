package org.ow2.contrail.federation.federationapi;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerException;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.federationapi.tests.JsonMessageBodyWriter;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SelectionCriteriaTest extends JerseyTest {
    String userUuid = "f4d19b86-2d0a-356a-a7ec-f6198630e866";
    String appUuid = "28819fe0-1608-11e3-8ffd-0800200c9a66";

    public SelectionCriteriaTest() throws TestContainerException {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.federation.federationapi.resources")
                .clientConfig(new DefaultClientConfig(JsonMessageBodyWriter.class))
                .build());
    }

    @Before
    public void setUp() {
        PersistenceUtils.createInstance("testPU");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new User();
            user.setUsername("testuser");
            user.setUuid(userUuid);
            user.setFirstName("Test");
            user.setLastName("User");
            user.setEmail("testuser@contrail.eu");
            user.setPassword("password");

            Application app = new Application();
            app.setUuid(appUuid);

            List<User> userList = new ArrayList<User>();
            userList.add(user);
            app.setUserList(userList);

            List<Application> appList = new ArrayList<Application>();
            appList.add(app);
            user.setApplicationList(appList);

            em.getTransaction().begin();
            em.persist(user);
            em.persist(app);
            em.getTransaction().commit();
        }
        finally {
            em.close();
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }

    @Test
    public void testSelectionCriteria() throws JSONException {
        WebResource webResource = resource();

        JSONObject scData;

        // create global selection criteria
        scData = new JSONObject();
        scData.put("name", "criterion1");
        scData.put("defaultValue", 0.1);
        ClientResponse response = webResource.path("/selection_criteria").post(ClientResponse.class, scData.toString());
        assertEquals(response.getStatus(), 201);

        scData = new JSONObject();
        scData.put("name", "criterion2");
        scData.put("defaultValue", 0.2);
        response = webResource.path("/selection_criteria").post(ClientResponse.class, scData.toString());
        assertEquals(response.getStatus(), 201);

        scData = new JSONObject();
        scData.put("name", "criterion3");
        scData.put("defaultValue", 0.3);
        response = webResource.path("/selection_criteria").post(ClientResponse.class, scData.toString());
        assertEquals(response.getStatus(), 201);

        // check global selection criteria
        String scArrayString = webResource.path("/selection_criteria").get(String.class);
        JSONArray scArray = new JSONArray(scArrayString);
        assertEquals(scArray.length(), 3);
        // criterion1
        scData = new JSONObject(scArray.getString(0));
        assertEquals(scData.getInt("id"), 1);
        assertEquals(scData.get("name"), "criterion1");
        assertEquals(scData.getDouble("defaultValue"), 0.1, 1E-6);
        // criterion2
        scData = new JSONObject(scArray.getString(1));
        assertEquals(scData.getInt("id"), 2);
        assertEquals(scData.get("name"), "criterion2");
        assertEquals(scData.getDouble("defaultValue"), 0.2, 1E-6);
        // criterion1
        scData = new JSONObject(scArray.getString(2));
        assertEquals(scData.getInt("id"), 3);
        assertEquals(scData.get("name"), "criterion3");
        assertEquals(scData.getDouble("defaultValue"), 0.3, 1E-6);

        // create user specific selection criterion
        scData = new JSONObject();
        scData.put("name", "criterion2");
        scData.put("value", "0.21");
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(scData);
        response = webResource.path(String.format("/users/%s/selection_criteria", userUuid))
                .put(ClientResponse.class, jsonArray.toString());
        assertEquals(response.getStatus(), 204);

        // get user selection criteria
        scArrayString = webResource.path(String.format("/users/%s/selection_criteria", userUuid))
                .get(String.class);
        scArray = new JSONArray(scArrayString);
        assertEquals(scArray.length(), 3);
        // criterion1
        scData = new JSONObject(scArray.getString(0));
        assertEquals(scData.get("name"), "criterion1");
        assertEquals(scData.getDouble("value"), 0.1, 1E-6); // default value

        // criterion2
        scData = new JSONObject(scArray.getString(1));
        assertEquals(scData.get("name"), "criterion2");
        assertEquals(scData.getDouble("value"), 0.21, 1E-6); // user value

        // criterion3
        scData = new JSONObject(scArray.getString(2));
        assertEquals(scData.get("name"), "criterion3");
        assertEquals(scData.getDouble("value"), 0.3, 1E-6); // default value

        // create application specific selection criterion
        scData = new JSONObject();
        scData.put("name", "criterion3");
        scData.put("value", "0.31");
        jsonArray = new JSONArray();
        jsonArray.put(scData);
        response = webResource.path(String.format("/applications/%s/selection_criteria", appUuid))
                .put(ClientResponse.class, jsonArray.toString());
        assertEquals(response.getStatus(), 204);

        // get user selection criteria
        scArrayString = webResource.path(String.format("/applications/%s/selection_criteria", appUuid))
                .get(String.class);
        scArray = new JSONArray(scArrayString);
        assertEquals(scArray.length(), 3);
        // criterion1
        scData = new JSONObject(scArray.getString(0));
        assertEquals(scData.get("name"), "criterion1");
        assertEquals(scData.getDouble("value"), 0.1, 1E-6); // default value

        // criterion2
        scData = new JSONObject(scArray.getString(1));
        assertEquals(scData.get("name"), "criterion2");
        assertEquals(scData.getDouble("value"), 0.21, 1E-6); // user value

        // criterion3
        scData = new JSONObject(scArray.getString(2));
        assertEquals(scData.get("name"), "criterion3");
        assertEquals(scData.getDouble("value"), 0.31, 1E-6); // application value

        // get criterion3
        String scDataString = webResource.path("/selection_criteria/3").get(String.class);
        scData = new JSONObject(scDataString);
        assertEquals(scData.getInt("id"), 3);
        assertEquals(scData.getString("name"), "criterion3");
        assertEquals(scData.getDouble("defaultValue"), 0.3, 1E-6);

        // update criterion 3
        scData = new JSONObject();
        scData.put("name", "criterion3a");
        scData.put("defaultValue", 0.33);
        response = webResource.path("/selection_criteria/3").put(ClientResponse.class, scData.toString());
        assertEquals(response.getStatus(), 204);

        // get user selection criteria, check criterion 3
        scArrayString = webResource.path(String.format("/users/%s/selection_criteria", userUuid))
                .get(String.class);
        scArray = new JSONArray(scArrayString);
        scData = new JSONObject(scArray.getString(2));
        assertEquals(scData.get("name"), "criterion3a");
        assertEquals(scData.getDouble("value"), 0.33, 1E-6); // default value
    }
}
