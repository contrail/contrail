package org.ow2.contrail.federation.federationapi.tests.providers;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.core.impl.FederationCoreBasic;
import org.ow2.contrail.federation.federationapi.resources.IProviderResource;
import org.ow2.contrail.federation.federationapi.resources.ProvidersResource;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

public class ProvidersResourceTest {
    private static Logger log = Logger.getLogger(ProvidersResourceTest.class);

    @Before
    public void setUp() {
        PersistenceUtils.createInstance("testPU");
        MyServletContextListener.setFederationCore(new FederationCoreBasic());
    }

    @Test
    public void testAddProvider() throws Exception {
        log.info("testAddProvider() started.");
        ProvidersResource providersResource = new ProvidersResource();
        Response response;

        // register new provider
        JSONObject providerJson = new JSONObject();
        providerJson.put("name", "ProviderA");
        providerJson.put("location", "SI");
        providerJson.put("email", "info@providerA.com");
        providerJson.put("typeId", 3);
        providerJson.put("providerUri", "contrail://providerA");
        response = providersResource.addProvider(providerJson.toString());
        assertEquals(response.getStatus(), 201);
        assertEquals(response.getMetadata().get("Location").get(0).toString(), "/1");

        // try to register already registered provider
        response = providersResource.addProvider(providerJson.toString());
        assertEquals(response.getStatus(), 409); // Conflict

        // get registered providers list
        response = providersResource.getProviders();
        assertEquals(response.getStatus(), 200);
        JSONArray array = new JSONArray((String) response.getEntity());
        JSONObject provider1 = array.getJSONObject(0);
        assertEquals(provider1.getString("name"), "ProviderA");
        assertEquals(provider1.getString("uri"), "/providers/1");

        // get provider 1
        IProviderResource providerResource = providersResource.findProvider(1);
        response = providerResource.getProvider();
        assertEquals(response.getStatus(), 200);
        JSONObject o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), providerJson.getString("name"));
        assertEquals(o.getString("location"), providerJson.getString("location"));
        assertEquals(o.getString("email"), providerJson.getString("email"));
        assertEquals(o.getInt("typeId"), 3);
        assertEquals(o.getString("providerUri"), "contrail://providerA");

        // update provider (1)
        JSONObject updateData = new JSONObject();
        updateData.put("name", "ProviderA1");
        updateData.put("location", "UK");
        response = providerResource.updateProvider(updateData.toString());
        assertEquals(response.getStatus(), 204);

        // get updated provider
        providerResource = providersResource.findProvider(1);
        response = providerResource.getProvider();
        assertEquals(response.getStatus(), 200);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), "ProviderA1");
        assertEquals(o.getString("location"), "UK");
        assertEquals(o.getString("email"), providerJson.getString("email"));

        // update provider (2)
        updateData = new JSONObject();
        updateData.put("newKey", "value");
        response = providerResource.updateProvider(updateData.toString());
        assertEquals(response.getStatus(), 204);

        // get updated provider
        providerResource = providersResource.findProvider(1);
        response = providerResource.getProvider();
        assertEquals(response.getStatus(), 200);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), "ProviderA1");
        assertEquals(o.getString("newKey"), "value");

        // update provider (3)
        updateData = new JSONObject();
        updateData.put("newKey", JSONObject.NULL);
        response = providerResource.updateProvider(updateData.toString());
        assertEquals(response.getStatus(), 204);

        // get updated provider
        providerResource = providersResource.findProvider(1);
        response = providerResource.getProvider();
        assertEquals(response.getStatus(), 200);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.get("newKey"), JSONObject.NULL);

        // remove provider 1
        response = providerResource.removeProvider();
        assertEquals(response.getStatus(), 204);

        // check if provider 1 was removed
        try {
            providersResource.findProvider(1);
            fail();
        }
        catch (Exception e) {
            assertEquals(e.getClass(), WebApplicationException.class);
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 404);
        }

        // get registered providers list - should be empty
        response = providersResource.getProviders();
        assertEquals(response.getStatus(), 200);
        array = new JSONArray((String) response.getEntity());
        assertEquals(array.length(), 0);
        log.info("testAddProvider() finished successfully.");
    }

    @After
    public void tearDown() throws Exception {
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }
}
