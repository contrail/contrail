# federation-api REST server URL
FEDERATION_CLI_URL=http://localhost:8080/federation-api

# operations on provider
java -jar federation-cli.jar add-provider -data "{'name':'CloudProvider','email':'cloudprovider.com','country':'UK','typeId':3,'providerUri':'contrail://CloudProvider'}"
java -jar federation-cli.jar add-provider -data "{'name':'TestProvider','typeId':3,'providerUri':'contrail://TestProvider'}"
java -jar federation-cli.jar getAll-provider
java -jar federation-cli.jar get-provider -providerId 1
java -jar federation-cli.jar update-provider -providerId 1 -data "{'country':'SI'}"
java -jar federation-cli.jar delete-provider -providerId 2

# operations on server
java -jar federation-cli.jar add-server -providerId 1 -data "{'name':'server1.cloudprovider.com', 'RAM':'16 GB'}"
java -jar federation-cli.jar add-server -providerId 1 -data "{'name':'server2.cloudprovider.com', 'RAM':'32 GB'}"
java -jar federation-cli.jar add-server -providerId 1 -data "{'name':'server3.cloudprovider.com', 'RAM':'32 GB'}"
java -jar federation-cli.jar getAll-server -providerId 1
java -jar federation-cli.jar update-server -providerId 1 -serverId 1 -data "{'RAM':'8 GB'}"
java -jar federation-cli.jar get-server -providerId 1 -serverId 1
java -jar federation-cli.jar delete-server -providerId 1 -serverId 1

# operations on VM
java -jar federation-cli.jar add-vm -providerId 1 -data "{'name':'vm1', 'OS':'Ubuntu 11.10'}"
java -jar federation-cli.jar add-vm -providerId 1 -data "{'name':'vm2', 'OS':'Ubuntu 11.10'}"
java -jar federation-cli.jar add-vm -providerId 1 -data "{'name':'vm3', 'OS':'Debian 6.0.4'}"
java -jar federation-cli.jar getAll-vm -providerId 1
java -jar federation-cli.jar update-vm -providerId 1 -vmId 1 -data "{'OS':'Ubuntu 11.04'}"
java -jar federation-cli.jar get-vm -providerId 1 -vmId 1
java -jar federation-cli.jar delete-vm -providerId 1 -vmId 1

# operations on cluster
java -jar federation-cli.jar add-cluster -providerId 1 -data "{'name':'Cluster1'}"
java -jar federation-cli.jar add-cluster -providerId 1 -data "{'name':'Cluster2'}"
java -jar federation-cli.jar add-cluster -providerId 1 -data "{'name':'Cluster3'}"
java -jar federation-cli.jar getAll-cluster -providerId 1
java -jar federation-cli.jar update-cluster -providerId 1 -clusterId 2 -data "{'newKey':'value'}"
java -jar federation-cli.jar get-cluster -providerId 1 -clusterId 2
java -jar federation-cli.jar delete-cluster -providerId 1 -clusterId 1

# linking/unlinking server to/from cluster
java -jar federation-cli.jar linkServer-cluster -providerId 1 -clusterId 2 -serverId 2
java -jar federation-cli.jar linkServer-cluster -providerId 1 -clusterId 2 -serverId 3
java -jar federation-cli.jar getLinkedServers-cluster -providerId 1 -clusterId 2
java -jar federation-cli.jar getLinkedServer-cluster -providerId 1 -clusterId 2 -serverId 3
java -jar federation-cli.jar unlinkServer-cluster -providerId 1 -clusterId 2 -serverId 3

# linking/unlinking VM to/from cluster
java -jar federation-cli.jar linkVm-cluster -providerId 1 -clusterId 2 -vmId 2
java -jar federation-cli.jar linkVm-cluster -providerId 1 -clusterId 2 -vmId 3
java -jar federation-cli.jar getLinkedVms-cluster -providerId 1 -clusterId 2
java -jar federation-cli.jar getLinkedVm-cluster -providerId 1 -clusterId 2 -vmId 3
java -jar federation-cli.jar unlinkVm-cluster -providerId 1 -clusterId 2 -vmId 3

# operations with virtual organization
java -jar federation-cli.jar add-vo -providerId 1 -data "{'name':'Virtual Organization 1', 'email':'info@vo1.com'}"
java -jar federation-cli.jar add-vo -providerId 1 -data "{'name':'Virtual Organization 2', 'email':'info@vo2.com'}"
java -jar federation-cli.jar getAll-vo -providerId 1
java -jar federation-cli.jar update-vo -providerId 1 -voId 1 -data "{'newKey':'value'}"
java -jar federation-cli.jar get-vo -providerId 1 -voId 1
java -jar federation-cli.jar delete-vo -providerId 1 -voId 1

# linking/unlinking cluster to/from virtual organization
java -jar federation-cli.jar linkCluster-vo -providerId 1 -voId 2 -clusterId 2
java -jar federation-cli.jar linkCluster-vo -providerId 1 -voId 2 -clusterId 3
java -jar federation-cli.jar getLinkedClusters-vo -providerId 1 -voId 2
java -jar federation-cli.jar getLinkedCluster-vo -providerId 1 -voId 2 -clusterId 2
java -jar federation-cli.jar unlinkCluster-vo -providerId 1 -voId 2 -clusterId 3

# operations with OVFs
java -jar federation-cli.jar add-ovf -providerId 1 -data "{'name':'Test OVF 1'}"
java -jar federation-cli.jar add-ovf -providerId 1 -data "{'name':'Test OVF 2'}"
java -jar federation-cli.jar getAll-ovf -providerId 1
java -jar federation-cli.jar get-ovf -providerId 1 -ovfId 1
java -jar federation-cli.jar delete-ovf -providerId 1 -ovfId 2

# operations with SLA templates (SLATs)
java -jar federation-cli.jar add-slat -providerId 1 -data "{'url':'http://contrail.xlab.si/slats/GoldSLAT.xml','name':'Gold SLA Template'}"
java -jar federation-cli.jar add-slat -providerId 1 -data "{'url':'http://contrail.xlab.si/slats/SilverSLAT.xml','name':'Silver SLA Template'}"
java -jar federation-cli.jar getAll-slat -providerId 1
java -jar federation-cli.jar update-slat -providerId 1 -slatId 2 -data "{'url':'http://contrail.xlab.si/slats/SilverSLAT-1.xml'}"
java -jar federation-cli.jar get-slat -providerId 1 -slatId 2
java -jar federation-cli.jar delete-slat -providerId 1 -slatId 1

################
# Users resource

# operations with users
java -jar federation-cli.jar add-user -data "{username: bob,firstName: Bob,lastName: Sinclare, email:bob@contrail.eu, password: mypassbob }"
java -jar federation-cli.jar getAll-user
java -jar federation-cli.jar get-user -userId 1
java -jar federation-cli.jar update-user -userId 1 -data "{username: newbob,firstName: Bob,lastName: Sinclare, email:bob@contrail.eu, password: mypassbob }"
java -jar federation-cli.jar delete-user -userId 1

# operations with roles
java -jar federation-cli.jar add-userRole -useId 1 -roleId 1
java -jar federation-cli.jar getAll-userRole
java -jar federation-cli.jar get-userRole -userId 1 -roleId 1
java -jar federation-cli.jar delete-userRole -userId 1 -roleId 1

# operations with userGroups
java -jar federation-cli.jar add-userGroup -userId 1 -groupId 1
java -jar federation-cli.jar getAll-userGroup
java -jar federation-cli.jar get-userGroup -userId 1 -groupId 1
java -jar federation-cli.jar delete-userGroup -userId 1 -groupId 1

# https protocol and certificate-based authentication
FEDERATION_CLI_URL=https://localhost:8443/federation-api
FEDERATION_CLI_CACERT=/path/to/cacerts.jks:contrail
FEDERATION_CLI_CLIENTCERT=/path/to/client.jks:contrail

java -jar federation-cli.jar getAll-provider
