package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;
import org.ow2.contrail.federation.federationcli.exceptions.NotAvailableMethodException;

public class Attributes extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"attributeId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/attributes/%s", args.get("attributeId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = {};
		checkArguments(args, requiredParams);
		return sendGETRequest("/attributes");
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"data"};
		checkArguments(args, requiredParams);
		return sendPOSTRequest("/attributes", args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		String[] requiredParams = {"attributeId", "data"};
		checkArguments(args, requiredParams);
		String uri = String.format("/attributes/%s", args.get("attributeId"));
		return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"attributeId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/attributes/%s", args.get("attributeId"));
		return sendDELETERequest(uri);
	}

}
