package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

public class Group extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/groups/%s", args.get("groupId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = {};
		checkArguments(args, requiredParams);
		return sendGETRequest("/groups");
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"data"};
		checkArguments(args, requiredParams);
		return sendPOSTRequest("/groups", args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId", "data"};
		checkArguments(args, requiredParams);
		String uri = String.format("/groups/%s", args.get("groupId"));
		return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/groups/%s", args.get("groupId"));
		return sendDELETERequest(uri);
	}

}
