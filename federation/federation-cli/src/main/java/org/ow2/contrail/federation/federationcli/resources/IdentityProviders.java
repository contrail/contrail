/**
 * 
 */
package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

/**
 * @author ales
 *
 */
public class IdentityProviders extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"identityProviderId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/idps/%s", args.get("identityProviderId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = {};
		checkArguments(args, requiredParams);
		return sendGETRequest("/idps");
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"data"};
		checkArguments(args, requiredParams);
		return sendPOSTRequest("/idps", args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		String[] requiredParams = {"identityProviderId", "data"};
		checkArguments(args, requiredParams);
		String uri = String.format("/idps/%s", args.get("identityProviderId"));
		return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"identityProviderId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/idps/%s", args.get("identityProviderId"));
		return sendDELETERequest(uri);
	}

}
