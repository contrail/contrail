/**
 * 
 */
package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

/**
 * @author ales
 *
 */
public class UserIdentityProvider extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"identityProviderId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/ids/%s", args.get("userId"), args.get("identityProviderId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = { "userId" };
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/ids", args.get("userId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"data", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/ids", args.get("userId"));
		return sendPOSTRequest(uri, args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"userId", "identityProviderId", "data"};
        checkArguments(args, requiredParams);
        String uri = String.format("/usera/%s/ids/%s", args.get("userId"), args.get("identityProviderId"));
        return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"identityProviderId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/ids/%s", args.get("userId"), args.get("identityProviderId"));
		return sendDELETERequest(uri);
	}

}
