package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;
import org.ow2.contrail.federation.federationcli.exceptions.NotAvailableMethodException;

public class UserGroup extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/groups/%s", args.get("userId"), args.get("groupId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = { "userId" };
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/groups", args.get("userId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/groups", args.get("userId"));
		String data = String.format("{\"groupId\": %s }", args.get("groupId"));
		return sendPOSTRequest(uri, data);
	}
	
	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"groupId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/groups/%s", args.get("userId"), args.get("groupId"));
		return sendDELETERequest(uri);
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		throw new NotAvailableMethodException();
	}

}
