package org.ow2.contrail.federation.federationcli.resources;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Ovf extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "ovfId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/ovfs/%s", args.get("providerId"), args.get("ovfId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/ovfs", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/ovfs", args.get("providerId"));
        return sendPOSTRequest(uri, args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "ovfId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/ovfs/%s", args.get("providerId"), args.get("ovfId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "ovfId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/ovfs/%s", args.get("providerId"), args.get("ovfId"));
        return sendDELETERequest(uri);
    }
}
