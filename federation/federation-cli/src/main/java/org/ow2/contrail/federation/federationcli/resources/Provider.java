package org.ow2.contrail.federation.federationcli.resources;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Provider extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {};
        checkArguments(args, requiredParams);

        return sendGETRequest("/providers");
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"data"};
        checkArguments(args, requiredParams);

        return sendPOSTRequest("/providers", args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s", args.get("providerId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s", args.get("providerId"));
        return sendDELETERequest(uri);
    }
}
