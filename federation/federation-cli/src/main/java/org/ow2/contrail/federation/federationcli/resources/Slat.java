package org.ow2.contrail.federation.federationcli.resources;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Slat extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "slatId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/slats/%s", args.get("providerId"), args.get("slatId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/slats", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/slats", args.get("providerId"));
        return sendPOSTRequest(uri, args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "slatId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/slats/%s", args.get("providerId"), args.get("slatId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "slatId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/slats/%s", args.get("providerId"), args.get("slatId"));
        return sendDELETERequest(uri);
    }
}
