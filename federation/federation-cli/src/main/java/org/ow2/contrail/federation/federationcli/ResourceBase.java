package org.ow2.contrail.federation.federationcli;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.ow2.contrail.federation.federationcli.exceptions.InvalidSyntaxException;
import org.ow2.contrail.federation.federationcli.exceptions.OperationFailedException;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class ResourceBase {
    protected RESTClient restClient;
    protected boolean rawResponse = false;
    protected boolean debugMode;

    public abstract Response get(Map<String, String> args) throws Exception;

    public abstract Response getAll(Map<String, String> args) throws Exception;

    public abstract Response add(Map<String, String> args) throws Exception;

    public abstract Response update(Map<String, String> args) throws Exception;

    public abstract Response delete(Map<String, String> args) throws Exception;

    protected boolean checkArguments(Map<String, String> args, String[] requiredParams) throws InvalidSyntaxException, IOException {
        boolean dataParamRequired = false;
        for (String requiredParam : requiredParams) {
            if (requiredParam.equals("data")) {
                dataParamRequired = true;
                break;
            }
        }

        if (dataParamRequired && !args.containsKey("data")) {
            String data = readFromStdIn();
            args.put("data", data);
        }

        boolean isOk = true;
        if (args.size() == requiredParams.length) {
            for (String requiredParam : requiredParams) {
                if (!args.containsKey(requiredParam)) {
                    isOk = false;
                }
            }
        }
        else {
            isOk = false;
        }

        if (!isOk) {
            List<String> requiredParamsList = Arrays.asList(requiredParams);
            throw new InvalidSyntaxException("Invalid arguments are given. Following arguments are required: " +
                    requiredParamsList.toString());
        }
        else {
            return true;
        }
    }

    protected Response sendGETRequest(String uri) {
        WebResource.Builder webResource = restClient.getWebResource(uri);
        ClientResponse response = webResource.get(ClientResponse.class);

        String content = getResponseContent(response);

        Response restResponse = null;
        if (response.getStatus() == 200) {
            restResponse = new Response(response.getStatus(), Response.Status.SUCCESS);
            restResponse.setContent(content);
            restResponse.setRawResponse(rawResponse);
        }
        else {
            restResponse = new Response(response.getStatus(), Response.Status.ERROR);
            addDetailedMessage(restResponse, content);
        }

        // debug data
        if (debugMode) {
            Response.DebugData debugData = new Response.DebugData("GET", restClient.getRootURL() + uri, null,
                    response, content);
            debugData.addRequestHeader("Accept", "application/json");
            restResponse.setDebugData(debugData);
        }

        return restResponse;
    }

    private String getResponseContent(ClientResponse response) {
        if (response.getStatus() != 204) {
            return response.getEntity(String.class);
        }
        else {
            return null;
        }
    }

    protected Response sendPUTRequest(String uri, String requestEntity) {
        WebResource.Builder webResource = restClient.getWebResource(uri);
        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).
                put(ClientResponse.class, requestEntity);

        String content = getResponseContent(response);
        Response restResponse = null;
        if (response.getStatus() == 204) {
            restResponse = new Response(response.getStatus(), Response.Status.SUCCESS);
        }
        else {
            restResponse = new Response(response.getStatus(), Response.Status.ERROR);
            addDetailedMessage(restResponse, content);
        }

        // debug data
        if (debugMode) {
            Response.DebugData debugData = new Response.DebugData("PUT", restClient.getRootURL() + uri, requestEntity,
                    response, content);
            debugData.addRequestHeader("Accept", "application/json");
            restResponse.setDebugData(debugData);
        }

        return restResponse;
    }

    protected Response sendPOSTRequest(String uri, String requestEntity) throws Exception {
        WebResource.Builder webResource = restClient.getWebResource(uri);
        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).
                post(ClientResponse.class, requestEntity);

        String content = getResponseContent(response);
        Response restResponse;
        if (response.getStatus() == 201) {
            restResponse = new Response(response.getStatus(), Response.Status.SUCCESS);
            if (response.getHeaders().containsKey("Location")) {
                restResponse.addHeader("Location", response.getHeaders().get("Location").get(0));
            }
            else {
                throw new OperationFailedException("Invalid response received. Missing Location header.");
            }
        }
        else {
            restResponse = new Response(response.getStatus(), Response.Status.ERROR);
            addDetailedMessage(restResponse, content);
        }

        // debug data
        if (debugMode) {
            Response.DebugData debugData = new Response.DebugData("POST", restClient.getRootURL() + uri, requestEntity,
                    response, content);
            debugData.addRequestHeader("Accept", "application/json");
            restResponse.setDebugData(debugData);
        }

        return restResponse;
    }

    protected Response sendDELETERequest(String uri) {
        WebResource.Builder webResource = restClient.getWebResource(uri);
        ClientResponse response = webResource.delete(ClientResponse.class);

        String content = getResponseContent(response);
        Response restResponse = null;
        if (response.getStatus() == 204) {
            restResponse = new Response(response.getStatus(), Response.Status.SUCCESS);
        }
        else {
            restResponse = new Response(response.getStatus(), Response.Status.ERROR);
            addDetailedMessage(restResponse, content);
        }

        // debug data
        if (debugMode) {
            Response.DebugData debugData = new Response.DebugData("DELETE", restClient.getRootURL() + uri, null,
                    response, content);
            debugData.addRequestHeader("Accept", "application/json");
            restResponse.setDebugData(debugData);
        }

        return restResponse;
    }

    public RESTClient getRestClient() {
        return restClient;
    }

    public void setRestClient(RESTClient restClient) {
        this.restClient = restClient;
    }

    public boolean isRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(boolean rawResponse) {
        this.rawResponse = rawResponse;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    private void addDetailedMessage(Response restResponse, String content) {
        if (content != null && !content.equals("")) {
            restResponse.setDetailedMessage(content);
        }
    }

    private String readFromStdIn() throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null && line.length() != 0) {
            sb.append(line);
            sb.append("\n");
        }
        return sb.toString();
    }
}
