package org.ow2.contrail.federation.federationcli.exceptions;

public class InvalidSyntaxException extends Exception {
    public InvalidSyntaxException(String message) {
        super(message);
    }
}
