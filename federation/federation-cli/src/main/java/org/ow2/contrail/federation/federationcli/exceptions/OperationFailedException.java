package org.ow2.contrail.federation.federationcli.exceptions;

public class OperationFailedException extends Exception {
    public OperationFailedException(String s) {
        super(s);
    }
}
