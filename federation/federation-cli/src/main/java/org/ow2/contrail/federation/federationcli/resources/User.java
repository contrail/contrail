/**
 * 
 */
package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

/**
 * @author ales
 *
 */
public class User extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		 String[] requiredParams = {"userId"};
	        checkArguments(args, requiredParams);
	        String uri = String.format("/users/%s", args.get("userId"));
	        return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = {};
        checkArguments(args, requiredParams);
        return sendGETRequest("/users");
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"data"};
        checkArguments(args, requiredParams);
        return sendPOSTRequest("/users", args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"userId", "data"};
        checkArguments(args, requiredParams);
        String uri = String.format("/users/%s", args.get("userId"));
        return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"userId"};
        checkArguments(args, requiredParams);
        String uri = String.format("/users/%s", args.get("userId"));
        return sendDELETERequest(uri);
	}

}
