package org.ow2.contrail.federation.federationcli.resources;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Server extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "serverId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/servers/%s", args.get("providerId"), args.get("serverId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/servers", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/servers", args.get("providerId"));
        return sendPOSTRequest(uri, args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "serverId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/servers/%s", args.get("providerId"), args.get("serverId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "serverId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/servers/%s", args.get("providerId"), args.get("serverId"));
        return sendDELETERequest(uri);
    }
}
