Feature: user manipulation

	@register_user
	Scenario: register user
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And username "rcucumber"
			And first name "Ruby" and last name "Cucumber"
			And email "ruby.cucumber@contrail.eu"
			And md password "d09c86f76ccf5ecf6c57e990ee098200f8822cc3"
		When get users request is made
			And user registration data is posted
		Then result code should be "201"
			And response should be empty
			And new url location should be created

	@user_add_slat
	Scenario: add SLA template to user
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And user "rcucumber"
			And provider name "CloudProvider2"
		When provider is found
			And SLAT 1 URI retrieved
		When get user with id request is made
			And user is added sla template
		Then result code should be "201"
			And response should be empty
			And new url location should be created
	
	@user_combine_slat_ovf
	Scenario: combine chosen SLA with OVF
		
	