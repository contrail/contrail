Feature: deployment scenario
	
	@deploy_check
	Scenario: get provider's list of OVFs
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And provider name "CloudProvider2"
		When provider is found
			And ovfs are returned
		Then list should not be empty
	
	@deploy_check
	Scenario: get users's list of SLA Templates
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And user "rcucumber"
		When get user with id request is made
			And sla templates are returned
		Then list should not be empty
	
	@deploy_initialize_slaovf_simple
	Scenario: register user and application
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And VEP server url is "http://10.31.1.10:10500"
			And user "rcucumber"
			And provider name "CloudProvider2"
			And user SLAOVF document "2" is required
			And application name is "mytestapp"
		When provider is found
			And get user with id request is made
			And sla templates are returned
			#And SLAOVF document is read
			#And OVFs are extracted from OVFSLA file
			And static OVF is read from "input/static-example.ovf"
		When user is registered with VEP
			And OVFs are registered in application
			And OVFs are initialized
		Then OVF is ready to deploy
		
	@deploy_slaovf
	Scenario: deploy application
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And VEP server url is "http://10.31.1.1:10500"
			And user "rcucumber"
			And user SLAOVF document "1" is required
			And provider name "CloudProvider2"
		When get user with id request is made
			And SLAOVF document is read
			And OVFs are extracted from OVFSLA file
			And provider is found
		Then deploy can be made
			And deploy should succeed
			
	@retrieve_ovf_direct
	Scenario: get OVFs from a given SLAOVF
		Given SLAEx url "http://10.31.1.10:8080/headNodeRestProxy/sla/slaextractor"
			And SLAOVF document at URL "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml"
		When SLAOVF is read
			And SLAOVF is posted to SLAEx
		Then OVF should be returned
			And OVF link should be "http://contrail.xlab.si/test-files/ubuntu-test-xlab.ovf"
			
	@deploy_initialize_slaovf
	Scenario: register user and application
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And VEP server url is "http://10.31.1.10:10500"
			And SLAEx url "http://10.31.1.10:8080/headNodeRestProxy/sla/slaextractor"
			And user "rcucumber"
			And provider name "CloudProvider2"
			And user SLAOVF document "1" is required
			And application name is "mytestapp-sla"
		When provider is found
			And get user with id request is made
			And sla templates are returned
		When SLAOVF URL is retrieved from provider SLA
			And SLAOVF is read 
			And SLAOVF is posted to SLAEx
			And OVF is read from result
		When user is registered with VEP
			And OVFs are registered in application
			And OVFs are initialized
		Then OVF is ready to deploy
		
	@deploy_slaovf_d	
	Scenario: deploy by direct call
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And VEP server url is "http://10.31.1.10:10500"
			And application name is "mytestapp-sla"
			And user "rcucumber"
		When get user with id request is made
			And OVF id is retrieved
		Then deploy OVF with id
	
	@deploy_start_monitoring_directcall	
	Scenario: call start monitoring directly 
		Given rest server url "http://10.31.1.3:8080/federation-api"
		 	And VEP server url is "http://10.31.1.10:10500"
			And monitoring REST url is "http://10.31.1.10:8080/headNodeRestProxy/monitoring"
			And user "rcucumber"
			And application name is "mytestapp-sla"
			And deploy response from OVF is in "input/ovf-deploy-response.json"
			And ZooKeeper server is "localhost" on port "2181"
		When get user with id request is made
			And OVF id is retrieved
			And results from OVF deploy are retrieved
			And VEP VM IDs are extracted from the result
			And ONE VM IDs are retrieved from VM descriptions
		When start monitoring is called
		When data is stored in ZooKeeper
		Then result should be "200"
			#And listener should get messages
			
	@deploy_sla_terms
	Scenario: connect SLA and deployed routing keys
		Given rest server url "http://10.31.1.3:8080/federation-api"
			And SLAEx url "http://10.31.1.10:8080/headNodeRestProxy/sla/slaextractor"
			And user "rcucumber"
			And provider name "CloudProvider2"
			And user SLAOVF document "1" is required
			And application name is "mytestapp-sla"
			And application instance number is "5"
			And ZooKeeper server is "localhost" on port "2181"
		When provider is found
			And get user with id request is made
			And sla templates are returned
		When SLAOVF URL is retrieved from provider SLA
			And SLAOVF is read
		When data is read from ZooKeeper
		When data is used with SLAEX
		Then SLA monitoring configuration should exist
	