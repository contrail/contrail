require "rubygems"
require "pp"
require "test/unit"

include Test::Unit::Assertions

Given /^rest server url "([^"]*)"$/ do |arg1|
  @url = arg1
end

When /^get providers request is made$/ do
  @res = makeGetRequest(@url+"/providers")
  #assert_equal(@res.empty?, false, "Should get some results!")
end

Then /^result code should be "([^"]*)"$/ do |arg1|
  code = arg1
  assert_equal(code, @res[:code], "Should return code #{code} but has returned #{@res[:code]}")
end

Then /^response should not be empty$/ do
  assert_equal(@res[:response].empty?, false, "should not be empty!")
end

When /^first returned provider is posted$/ do
  first = @res[:response][0]
  firsturl = first["uri"]
  data = makeGetRequest(@url+firsturl)
  @res = makePostRequest(@url+"/providers", data[:response].to_json)
end

Then /^response should be empty$/ do
  assert_equal(@res[:response], nil, "is not empty!")
end

When /^new provider with name prefix "([^"]*)" and email postfix "([^"]*)" is posted$/ do |arg1, arg2|
  count = 100
  count = @res[:response].size + 1 if not @res.empty?
  params = {
    "name" => "#{arg1}-#{count}",
    #"email" => "#{count}.#{arg2}",
    "typeId" => 42,
    "providerUri" => "http://10.31.1.5:10500",
    "country_codes" => ["UK", "IT", "SI", "FR"],
    "supported_terms" => ["term1", "term2", "term3"]
  }.to_json  
  @res = makePostRequest(@url+"/providers", params)
  pp @res
end

Then /^new url location should be created$/ do
  assert_equal(@res[:headers].has_key?(:location), true, "location of new post not created but should be there")
  @location = @res[:headers][:location]
end

When /^new server with "([^"]*)" cpu, "([^"]*)" cores, each "([^"]*)" Mhz and "([^"]*)" Mb of RAM is added to last provider$/ do |arg1, arg2, arg3, arg4|
  lst = @res[:response].last
  lsturl = lst["uri"]
  puts lsturl
  
  servers = makeGetRequest(@url+lsturl+"/servers")
  puts "got servers: #{servers[:response]}"
  count = servers[:response].size + 1
  
  @serverCpu = arg1
  @serverCores = arg2
  @serverSpeed = arg3
  @serverRam = arg4
  
  server = {
    "name" => "#{arg1} server number-#{count}",
    "cpu_cores" => arg2,
    "cpu_speed" => arg3,
    "ram_total" => arg4,
    "ram_free" =>"2763",
    "cpu_load_one" => "0.09",
    "ram_used" => "1152",
    "cpu_load_five" => "0.04"    
  }.to_json
  puts server  
  
  @res = makePostRequest(@url+lsturl+"/servers", server)
end

When /^new server with "([^"]*)" cpu, "([^"]*)" cores, each "([^"]*)" Mhz and "([^"]*)" Mb of RAM is added to named provider$/ do |arg1, arg2, arg3, arg4|
  @serverCpu = arg1
  @serverCores = arg2
  @serverSpeed = arg3
  @serverRam = arg4
  
  servers = makeGetRequest(@url+@providerUrl+"/servers")
  puts "got servers: #{servers[:response]}"
  count = servers[:response].size + 1
  
  server = {
    "name" => "server #{count}",
    "cpu" => arg1,
    "cores" => arg2,
    "speed" => arg3,
    "ram" => arg4
  }.to_json
  puts server  
  @res = makePostRequest(@url+@providerUrl+"/servers", server)
end

Then /^attributes of last server should be the same as given$/ do
  serverObj = makeGetRequest(@location)
  server = serverObj[:response]
  assert_equal(@serverCpu, server["cpu"], "cpu should be equal")
  assert_equal(@serverCores, server["cores"], "cores should be equal")
  assert_equal(@serverSpeed, server["speed"], "speed should be equal")
  assert_equal(@serverRam, server["ram"], "ram should be equal")
end

Given /^provider name "([^"]*)"$/ do |arg1|
  @providerName = arg1
end

Given /^cluster name prefix "([^"]*)"$/ do |arg1|
  @clusterNamePrefix = arg1
end

When /^provider is found$/ do
  provurl = getUrlForResourceNameFrom(@url+"/providers", @providerName)
  puts "provider url: #{provurl}"
  assert_equal(provurl.nil?, false, "did not find provider with that name")
  @providerUrl = provurl
end

When /^new cluster is made for provider$/ do
  clusters = makeGetRequest(@url+@providerUrl+"/clusters")
  count = clusters[:response].size + 1
  cluster = {
    "name" => "#{@clusterNamePrefix}-#{count}"
  }.to_json
  @res = makePostRequest(@url+@providerUrl+"/clusters", cluster)
end

Given /^server set is defined by "([^"]*)"$/ do |arg1|
  set = File.open(arg1).read
  servers = JSON.parse(set)
  puts servers
  assert_not_equal(servers.empty?, true, "should get some server definitions")
  @serverSet = servers
end

When /^server set is added to last cluster$/ do
  res = makeGetRequest(@url+@providerUrl+"/clusters")
  clusters = res[:response]
  assert_not_equal(clusters.empty?, true, "there should be some clusters first")
  puts clusters
  clstrurl = clusters.last["uri"]
  @serverSet.each { 
    |s|
    s["name"] = "server #{Time.new.usec}" if not s.has_key?("name")
  }
  puts "cluster url: #{clstrurl}"
  locs = []
  @serverSet.each {
    |s|
    #@res = makePutRequest(@url+@providerUrl+"/servers", s.to_json)
    @res = makePostRequest(@url+@providerUrl+"/servers", s.to_json)
    assert_equal(@res[:code], "201", "server not created")
    locs << @res[:headers][:location]
  }
  locs.map! { |l| l.gsub(@url, "")}
  
  @res = makePostRequest(@url+clstrurl+"/servers", locs.to_json)
end

Then /^cluster should have all servers listed$/ do
  #depends whether just given ones are in the cluster or the new
  #ones are appended to the existing cluster
  pending # express the regexp above with the code you wish you had
end

Given /^minimum server requirements with "([^"]*)" cores and "([^"]*)" MB ram$/ do |arg1, arg2|
  @minReq = { "cpu_cores" => arg1.to_i, "ram_total" => arg2.to_i }
end

When /^all providers and clusters are checked$/ do
  serverUrls = listAll(@url, ["/providers", "/clusters", "/servers"])
  @goodServerUrls = findMatching(serverUrls, @minReq)
end

Then /^one result should be returned$/ do
  assert_equal(@goodServerUrls.size, 1, "should be unique, check database or add new unique server")
end

Then /^some servers should be found$/ do
  assert_not_equal(@goodServerUrls.empty?, true, "servers were not found")
end

Given /^ovf file url is "([^"]*)"$/ do |arg1|
  @ovfurl = arg1
end

When /^ovf is added to the provider$/ do
  ovfdata = {
    "name" => "Dummy OVF", 
    "url" => @ovfurl
  }.to_json
  @res = makePostRequest(@url+@providerUrl+"/ovfs", ovfdata)
end

Given /^SLAT name "([^"]*)" and url "([^"]*)"$/ do |arg1, arg2|
  @slatname = arg1
  @slaturl = arg2
end

When /^SLAT is added to the provider$/ do
  slatdata = {
    "name" => @slatname,
    "url" => @slaturl
  }.to_json
  @res = makePostRequest(@url+@providerUrl+"/slats", slatdata)
end

Given /^VO name "([^"]*)"$/ do |arg1|
  @voname = arg1
end

When /^VO is added to provider$/ do
  vodata = {
    "name" => @voname
  }.to_json
  @res = makePostRequest(@url+@providerUrl+"/vos", vodata)
end