require 'xmlsimple'

Given /^message on common queue "([^"]*)"$/ do |arg1|
  msg = File.open(arg1).read
  hmsg = XmlSimple.xml_in(msg)
  puts hmsg
  #need to extract hostname
  @server_hostname = ""
  hmsg["Value"].each {
    |hash|
    @server_hostname = hash["content"] if hash["id"] == "hostname"
  }
  puts @server_hostname
end

Given /^message on memory queue "([^"]*)"$/ do |arg1|
  msg = File.open(arg1).read
  hmsg = XmlSimple.xml_in(msg)
  puts hmsg
  hmsg["Value"].each {
    |hash|
    @server_ram_total = hash["content"] if hash["id"] == "total"
    @server_ram_used = hash["content"] if hash["id"] == "used"
    @server_ram_free = hash["content"] if hash["id"] == "free"
  }
  puts @server_ram_free, @server_ram_used, @server_ram_total
end

Given /^message on cpu queue "([^"]*)"$/ do |arg1|
  msg = File.open(arg1).read
  hmsg = XmlSimple.xml_in(msg)
  puts hmsg
  hmsg["Value"].each {
    |hash|
    @server_cpu_cores = hash["content"] if hash["id"] == "cores"
    @server_cpu_speed = hash["content"] if hash["id"] == "speed"
    @server_cpu_load_one = hash["content"] if hash["id"] == "load_one"
    @server_cpu_load_five = hash["content"] if hash["id"] == "load_five"
  }
  puts @server_cpu_cores, @server_cpu_speed, @server_cpu_load_one, @server_cpu_cores
end

When /^server data is prepared$/ do
  @server_data = {
    "name" => @server_hostname,
    "ram_total" => @server_ram_total,
    "ram_used" => @server_ram_used,
    "ram_free" => @server_ram_free,
    "cpu_cores" => @server_cpu_cores,
    "cpu_speed" => @server_cpu_speed,
    "cpu_load_one" => @server_cpu_load_one,
    "cpu_load_five" => @server_cpu_load_five
  }
  puts @server_data
end

When /^new server is registered on provider$/ do
  @res = makePostRequest(@url+@providerUrl+"/servers", @server_data.to_json)
end

Given /^cluster "([^"]*)"$/ do |arg1|
  @clustername = arg1
end

When /^cluster is found$/ do
  res = makeGetRequest(@url+@providerUrl+"/clusters")
  clusters = res[:response]
  assert_not_equal(clusters.empty?, true, "there should be some clusters first")
  puts clusters
  @clusterurl = nil
  clusters.each {
    |c|
    @clusterurl = c["uri"] if c["name"] == @clustername 
  }
  assert_not_equal(@clustername, nil, "cluster should exist")
  puts @clusterurl
end

When /^server is added to cluster$/ do
  res = makeGetRequest(@url+@providerUrl+"/servers")
  servers = res[:response]
  assert_not_equal(servers.empty?, true, "there should be some servers first")
  puts servers
  servers.each {
    |s|
    @serverurl = s["uri"] if s["name"] == @server_hostname
  }
  assert_not_equal(@serverurl, nil, "server should be registered first")
  puts @serverurl
  params = [@serverurl].to_json
  
  @res = makePutRequest(@url+@clusterurl+"/servers", params)
end

Then /^result code should be "([^"]*)" or "([^"]*)"$/ do |arg1, arg2|
  if @res.empty?
    puts "seems like error 400 is returned and there is no response"
  else 
    code = @res[:code]
    puts "code: #{code}"
    if code != arg1 and code != arg2
      assert(false)
    end
  end
end