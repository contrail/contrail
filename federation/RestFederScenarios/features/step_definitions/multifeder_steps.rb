@@userFederNodesUrl = {}

=begin
defines the structure that reflects the testing environment 
for the multi-federation nodes
=end
Given /^standard multi\-federation configuration with "([^"]*)" nodes$/ do |arg1|
  @@federNodes = {
    "0" => {
      :url => "http://10.31.1.3:8080/federation-api"
    },
    
    "1" => {
      :url => "http://10.31.1.3:8080/federation-api"
    },
    "2" => {
      :url => "http://10.31.1.4:8080/federation-api"
    }
  }
end

=begin
maps the user defined name for a node with the o
=end
Given /^node "([^"]*)" is federation node "([^"]*)"$/ do |arg1, arg2|
  @@userFederNodesUrl[arg1] = @@federNodes[arg2][:url]
  pp @@userFederNodesUrl
end

When /^get providers on node "([^"]*)" is called$/ do |arg1|
  url = @@userFederNodesUrl[arg1]
  assert_not_nil(url, "The federation node does not exist")
  @provsDataProducer = makeGetRequest(url+"/providers")[:response]
  pp @provsDataProducer
end

When /^the first provider is selected from the list$/ do
  assert_not_empty(@provsDataProducer, "There are no providers on the selected node.")
  @selectedProvProducerData = @provsDataProducer[0]
  pp @selectedProvProducerData
  assert_not_nil(@selectedProvProducerData)
end

When /^UUID of the provider is stored$/ do
  selectedProvProducerDataUri = @selectedProvProducerData["uri"]
  assert_not_nil(selectedProvProducerDataUri)
  @selectedProvUUID = selectedProvProducerDataUri.gsub("/providers/", "")
  assert_not_nil(@selectedProvUUID)
  pp @selectedProvUUID
end

When /^new SLAT "([^"]*)" at URL "([^"]*)" is registered on selected provider on node "([^"]*)"$/ do |arg1, arg2, arg3|
  slatdata = {
    "name" => arg1,
    "url" => arg2
  }.to_json
  pp @@userFederNodesUrl
  pp @selectedProvProducerData
  
  url = @@userFederNodesUrl[arg3]+@selectedProvProducerData["uri"]+"/slats"
  pp url
  @res = makePostRequest(url, slatdata)
end

Then /^get SLATs on selected provider on node "([^"]*)" should return the same list as on node "([^"]*)"$/ do |arg1, arg2|
  assert_not_nil(@@userFederNodesUrl[arg1], "missing user defined name for federation node #{arg1}")
  url1 = @@userFederNodesUrl[arg1] + "/providers/" + @selectedProvUUID + "/slats"
  pp url1
  list1 = makeGetRequest(url1)[:response]
  
  assert_not_nil(@@userFederNodesUrl[arg2], "missing user defined name for federation node #{arg2}")
  url2 = @@userFederNodesUrl[arg2] + "/providers/" + @selectedProvUUID + "/slats"
  pp url2
  list2 = makeGetRequest(url2)[:response]
  
  #pp list1
  #pp list2
  list1.each { |item|
    assert(list2.include?(item))
  }
end

When /^new SLAT "([^"]*)" at URL "([^"]*)" is registered "([^"]*)" times concurrently on random federation node$/ do |arg1, arg2, arg3|
  slatdata = {
    "name" => arg1,
    "url" => arg2
  }.to_json
  fedNodes = @@userFederNodesUrl.size
  rnd = arg3.to_i.times.map{ Random.rand(@@userFederNodesUrl.size) }
  urls = rnd.map { |x| @@userFederNodesUrl[@@userFederNodesUrl.keys[x]] }
  #pp urls  
  threads = []
  curr = 0
  urls.each {
    |federUrl|
    url = federUrl+@selectedProvProducerData["uri"]+"/slats"
    pp url
    threads << Thread.new() {
      sleep(0.2*curr.to_f)
      @res = makePostRequest(url, slatdata)
      pp "running thread for #{federUrl}"
    }
    curr += 1
  }
  pp "waiting the threads to finish"
  threads.each { |aThread|  aThread.join }
end

When /^random SLATs are deleted "([^"]*)" times concurrently on random federation node$/ do |arg1|
  rnd = arg1.to_i.times.map{ Random.rand(@@userFederNodesUrl.size) }
  urls = rnd.map { |x| @@userFederNodesUrl[@@userFederNodesUrl.keys[x]] }
  
  threads = []
  curr = 0
  urls.each {
    |federSlatUrl|
    pp "current: #{curr}"
    threads << Thread.new() {
      list = makeGetRequest(federSlatUrl+@selectedProvProducerData["uri"]+"/slats")[:response]
      puts "current number of SLATs: #{list.size}"
      pos = Random.rand(list.size)
      slatUri = list[pos]["uri"]
      puts "deleting: #{federSlatUrl+slatUri}"
      #sleep(0.2*curr.to_f)
      res = makeDeleteRequest(federSlatUrl+slatUri)
      pp res[:code]
    }
    threads.each { |aThread|  aThread.join }
    curr += 1
  }
  #url = "http://10.31.1.9:8080/federation-api/providers/1dc7995d-db94-4156-904d-cdb6b08d371a/slats/5"
  #res = makeDeleteRequest(url)
  #pp res
  pp "waiting the threads to finish"
  threads.each { |aThread|  aThread.join }
  
end

When /^random SLATs with name "([^"]*)" at URL "([^"]*)" are added "([^"]*)" times concurrently on random federation node$/ do |arg1, arg2, arg3|
  slatdata = {
    "name" => arg1,
    "url" => arg2
  }

  rnd = arg3.to_i.times.map{ Random.rand(@@userFederNodesUrl.size) }
  urls = rnd.map { |x| @@userFederNodesUrl[@@userFederNodesUrl.keys[x]] }

  threads = []
  curr = 0
  urls.each {
    |federSlatUrl|
    pp "current: #{curr}"
    threads << Thread.new() {
      data = slatdata.clone
      data["name"] += "#{curr}"
      #sleep(0.07*curr.to_f)
      res = makePostRequest(federSlatUrl+@selectedProvProducerData["uri"]+"/slats", data.to_json)
      #sleep(0.05)
      makeDeleteRequest(res[:headers][:location])
    }
    #threads.each { |aThread|  aThread.join }
    curr += 1
  }
  #url = "http://10.31.1.9:8080/federation-api/providers/1dc7995d-db94-4156-904d-cdb6b08d371a/slats/5"
  #res = makeDeleteRequest(url)
  #pp res
  pp "waiting the threads to finish"
  threads.each { |aThread|  aThread.join }
  
end

