Feature: Register server from sensors
	
	@server_init
	Scenario: Register server from common metrics
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-4"
			And message on common queue "input/server_common.xml"
			And message on memory queue "input/server_memory.xml"
			And message on cpu queue "input/server_cpu.xml"
		When provider is found
			And server data is prepared
			And new server is registered on provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created	
	
	@server_add_to_cluster
	Scenario: Register given server to cluster
	Given rest server url "http://10.31.1.9:8080/federation-api"
		And provider name "XLAB-test-4"
		And message on common queue "input/server_common.xml"
		And cluster "cluster-1"
	When provider is found
		And cluster is found
		And server is added to cluster
	Then result code should be "204" or "400"
		And response should be empty
				
	@server_find_test
	Scenario: find clusters with servers better then given
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And minimum server requirements with "4" cores and "1024" MB ram
		When all providers and clusters are checked
		Then some servers should be found	
