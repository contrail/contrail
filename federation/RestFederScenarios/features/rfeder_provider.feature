Feature: provider scenarios
	
	@add_new_provider		
	Scenario: register new provider
		Given rest server url "http://10.31.1.3:8080/federation-api"
		When get providers request is made
			And new provider with name prefix "XLAB-test" and email postfix "test.info@xlab.si" is posted
		Then result code should be "201"
			And response should be empty
			And new url location should be created
	
	@provider_check
	Scenario: get current registered providers
		Given rest server url "http://10.31.1.9:8080/federation-api"
		When get providers request is made
		Then result code should be "200"
			And response should not be empty
					
	Scenario: register existing provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
		When get providers request is made
			And first returned provider is posted
		Then result code should be "409"
			And response should be empty
	
	@add_server_to_last
	Scenario: register new server with last provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
		When get providers request is made
			And new server with "i7" cpu, "8" cores, each "2600" Mhz and "8192" Mb of RAM is added to last provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created
			And attributes of last server should be the same as given

	@add_server
	Scenario: register new server with last provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-7"
		When provider is found
			And new server with "i7" cpu, "8" cores, each "2600" Mhz and "8192" Mb of RAM is added to named provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created
			And attributes of last server should be the same as given
			
	@add_cluster
	Scenario: register server set to new cluster of named provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-4"
			And cluster name prefix "cluster" 
		When provider is found
			And new cluster is made for provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created

	@add_servers_to_cluster
	Scenario: register new server set to last cluster of provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-7"
			And server set is defined by "input/server_set_1.txt"
		When provider is found
			And server set is added to last cluster
		Then result code should be "204"
			And response should be empty
			And cluster should have all servers listed
	
	@add_unique_server_to_cluster
	Scenario: register new server set to last cluster of provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-7"
			And server set is defined by "input/server_set_unique.txt"
		When provider is found
			And server set is added to last cluster
		Then result code should be "204"
			And response should be empty
			And cluster should have all servers listed
		
	@find_unique_server
	Scenario: find cluster with unique server equal or better then given
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And minimum server requirements with "12" cores and "8192" MB ram
		When all providers and clusters are checked
		Then one result should be returned
		
	@find_servers
	Scenario: find clusters with servers better then given
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And minimum server requirements with "8" cores and "8192" MB ram
		When all providers and clusters are checked
		Then some servers should be found	
		
	@provider_add_ovf_url
	Scenario: add OVF url to provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-1"
			And ovf file url is "http://contrail.xlab.si/test-files/ubuntu-test-xlab.ovf"
		When provider is found
			And ovf is added to the provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created

	@provider_add_slat
	Scenario: add SLA template to provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-2"
			And SLAT name "Test SLA" and url "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml"
		When provider is found
			And SLAT is added to the provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created

	@provider_add_vo
	Scenario: add VO to the provider
		Given rest server url "http://10.31.1.9:8080/federation-api"
			And provider name "XLAB-test-1"
			And VO name "Test Group"
		When provider is found
			And VO is added to provider
		Then result code should be "201"
			And response should be empty
			And new url location should be created
