require 'rubygems'
require 'pp'
require 'json'
require 'rest_client'
require 'net/http'
require 'zookeeper'
require 'xmlsimple'

def makeGetRequest(url)
  puts "making get req on #{url}"
  res = {}
  begin
    RestClient.get(url){ 
      |response, request, result|
      res[:response] = JSON.parse(response)
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers
    }
  rescue
    puts "ERROR?!?"
  end
  res
end

def makeGetRequestWithUser(url, user, content_type = :json)
  puts "making get req on #{url}"
  res = {}
  begin
    RestClient.get(url, "X-Username" => user, :content_type => content_type, :accept => :json){ 
      |response, request, result|
      res[:response] = JSON.parse(response)
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers
    }
  rescue
    puts "ERROR?!?"
  end
  res
end


def makePostRequest(url, params, ctype = :json)
  puts "making post req on #{url} with params #{params}"
  res = {}  
  begin
    RestClient.post(url, params, :content_type => ctype, :accept => :json) {
      |response, request, result|
            
      if not response.empty?
        res[:response] = JSON.parse(response)
      else 
        res[:response] = nil
      end
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers      
    }
  rescue
    puts "ERROR?!?"
  end
  puts "result: "
  puts res
  res
end  

def makePostRequestStringResponse(url, params, ctype = :json)
  puts "making post req on #{url} with params #{params}"
  res = {}  
  begin
    RestClient.post(url, params, :content_type => ctype, :accept => :json) {
      |response, request, result|

      #pp request
      #puts
      #pp response
      #puts
      #pp result

      if not response.empty?
        res[:response] = response
      else 
        res[:response] = nil
      end
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers            
    }
  rescue
    puts "ERROR?!?"
  end
  puts "result: "
  puts res
  res
end

def makePostRequestStringResponseNoAccept(url, params, ctype = :json)
  puts "making post req on #{url} with params #{params}"
  res = {}  
  begin
    RestClient.post(url, params, :content_type => ctype) {
      |response, request, result|

      #pp request
      #puts
      #pp response
      #puts
      #pp result

      if not response.empty?
        res[:response] = response
      else 
        res[:response] = nil
      end
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers            
    }
  rescue
    puts "ERROR?!?"
  end
  puts "result: "
  puts res
  res
end

def makePutRequestWithUser(url, user, params, content_type = :json)
  puts "making put req on #{url} with params #{params}"
  res = {}  
  begin
    RestClient.put(url, params, "X-Username" => user, :content_type => content_type, :accept => :json) {
      |response, request, result|
      
      if not response.empty?
        res[:response] = JSON.parse(response)
      else 
        res[:response] = nil
      end
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers      
    }
  rescue
    puts "ERROR?!?"
  end
  puts "result: "
  puts res
  res
end


def makePutRequest(url, params)
  puts "making put req on #{url} with params #{params}"
  res = {}  
  begin
    RestClient.put(url, params, :content_type => :json, :accept => :json) {
      |response, request, result|
      
      if not response.empty?
        res[:response] = JSON.parse(response)
      else 
        res[:response] = nil
      end
      res[:result] = result
      res[:code] = result.code
      res[:headers] = response.headers      
    }
  rescue
    puts "ERROR?!?"
  end
  puts "result: "
  puts res
  res
end


def makeDeleteRequest(url)
  puts "making delete req on #{url}"
  res = {}  
  begin
    RestClient.delete(url) {
        |response, request, result|
        puts "delete response: #{response}"
        puts "delete result: #{result}"
        res[:response] = JSON.parse(response) if not response.empty?
        res[:result] = result
        res[:code] = result.code
        res[:headers] = response.headers
      }
    rescue
      puts "ERROR?!?"
    end
    res
end

def getUrlForResourceNameFrom(url, name, attribute="name")
  puts "searching for provider named #{name}"
  puts "making get req on #{url}"
  begin
    RestClient.get(url){ 
      |response, request, result|
      puts "response"
      puts response
      resources = JSON.parse(response)
      resources.each {
        |r|
        #puts "resource #{r["name"]}"
        return r["uri"] if r[attribute] == name
      }
    }
  rescue
    puts "ERROR?!?"
  end
  nil
end

def listAll(baseUrl, urlOrder)
  puts "searching for all #{urlOrder}"
  urls = [baseUrl]
  urlOrder.each {
    |pUrl|
    tmpUrls = []
    urls.each {
      |url|
      res = makeGetRequest(url+pUrl)
      #puts res[:response]
      res[:response].each {
        |resource|
        tmpUrls << baseUrl + resource["uri"]
      }
    }
    #puts "\nSetting tmpurls\n"
    urls = tmpUrls
    #puts urls
  }
  puts "results:"
  puts urls
  urls
end

def findMatching(urls, reqs)
  result = []
  urls.each {
    |url|
    puts "checkign resource #{url}"
    res = makeGetRequest(url)
    obj = res[:response]
    puts "server specs: #{obj}"
    match = true
    reqs.each {
      |k, v|
      if not obj.has_key?(k)
        match = false 
        puts "attribute #{k} does not exist"
      elsif v > obj[k].to_i
        match = false 
        puts "attribute #{k} with value #{obj[k]} too small"
      end
    }
    result << url if match == true
  }
  puts "results"
  puts result
  result
end

def findDeployedData(deployedData, target)
  deployedData.each {
    |data|
    return data if data["name"] == target
  }
end

def parseSlaExXml(slaex,deployedData)
  puts "parsing SlaEx XML"
  #puts slaex
  h = XmlSimple.xml_in(slaex, { 'ForceArray' => false })
  #puts h.inspect
  return nil if not h.has_key?("serviceLevelObjectives")
  h["serviceLevelObjectives"]["serviceLevelObjective"].each {
    |slo|
    #get all guarantees
    slo["guarantees"]["guarantee"].each {
      |grntee|
      #puts grntee
      if grntee.class == Hash and grntee.has_key?("constraint")
        operands = grntee["constraint"]["expression"]["operand"] 
        #puts "operand: #{operands}"
        operands.each {
          |operand|
          if operand.has_key?("metric")
            target = operand["metric"]["target"]["appliance"]["sid"]
            data = findDeployedData(deployedData, target)
            routingKey = data["routingKey"]
            puts "found target #{target} with routing key #{routingKey} and deploy data #{data}"
            operand["metric"]["target"]["appliance"]["routingKey"] = routingKey
          end
        }
      end
    }
  }
  xml = XmlSimple.xml_out(h)
  xml
end