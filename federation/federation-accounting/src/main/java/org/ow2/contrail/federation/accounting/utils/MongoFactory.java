package org.ow2.contrail.federation.accounting.utils;

import com.mongodb.Mongo;

import java.net.UnknownHostException;

public class MongoFactory {
    private static Mongo mongo;

    public static void init() throws UnknownHostException {
        mongo = new Mongo(Conf.getInstance().getMongoDBHost(), Conf.getInstance().getMongoDBPort());
    }

    public static Mongo getMongo() {
        return mongo;
    }
}
