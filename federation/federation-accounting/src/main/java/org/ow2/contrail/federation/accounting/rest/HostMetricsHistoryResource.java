package org.ow2.contrail.federation.accounting.rest;

import com.mongodb.*;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.oauth.client.OAuthFilter;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.federation.accounting.jobs.HostMetricsHistoryJob;
import org.ow2.contrail.federation.accounting.jobs.JobStatus;
import org.ow2.contrail.federation.accounting.scheduler.Scheduler;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.DateUtils;
import org.ow2.contrail.federation.accounting.utils.MongoFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/reports/host_metrics_history")
public class HostMetricsHistoryResource {

    @Context
    HttpServletRequest httpServletRequest;

    @Context
    UriInfo uriInfo;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createHostMetricsHistoryReport(JSONObject data) throws Exception {
        String providerUuid;
        List<String> sids;
        List<String> metrics;
        Date startTime = null;
        Date endTime = null;
        int numberOfIntervals;
        TokenInfo tokenInfo;
        try {
            // providerUuid
            providerUuid = data.getString("providerUuid");

            // sids
            JSONArray sidsArray = data.getJSONArray("sids");
            if (sidsArray.length() == 0) {
                throw new JSONException("Empty sids array - not yet implemented.");
            }
            sids = new ArrayList<String>();
            for (int i = 0; i < sidsArray.length(); i++) {
                sids.add(sidsArray.getString(i));
            }

            // metrics
            JSONArray metricArray = data.getJSONArray("metrics");
            if (metricArray.length() == 0) {
                throw new JSONException("Empty metrics array.");
            }
            metrics = new ArrayList<String>();
            for (int i = 0; i < metricArray.length(); i++) {
                metrics.add(metricArray.getString(i));
            }

            if (data.has("startTime")) {
                startTime = DateUtils.parseDate(data.getString("startTime"));
            }
            if (data.has("endTime")) {
                endTime = DateUtils.parseDate(data.getString("endTime"));
            }

            numberOfIntervals = data.getInt("numberOfIntervals");

            tokenInfo = OAuthFilter.getAccessTokenInfo(httpServletRequest);
            if (tokenInfo == null) {
                throw new Exception("OAuth access token is required.");
            }
        }
        catch (Exception e) {
            throw new WebApplicationException(Response.status(
                    Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }

        HostMetricsHistoryJob job = new HostMetricsHistoryJob(providerUuid, sids, metrics);
        job.setStartTime(startTime);
        job.setEndTime(endTime);
        job.setNumberOfIntervals(numberOfIntervals);
        job.setAccessToken(tokenInfo.getAccessToken());

        Scheduler scheduler = SchedulerFactory.getScheduler();
        scheduler.addJob(job);

        URI location = new URI(job.getJobId());
        JSONObject content = new JSONObject();
        URI absoluteUri = uriInfo.getAbsolutePathBuilder().path(job.getJobId()).build();
        content.put("location", absoluteUri.toString());
        return Response.created(location).entity(content).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{jobId}")
    public Response getStatus(@PathParam("jobId") String jobId) throws JSONException {

        JSONObject statusJson = new JSONObject();

        Scheduler scheduler = SchedulerFactory.getScheduler();
        HostMetricsHistoryJob job;
        try {
            job = (HostMetricsHistoryJob) scheduler.getJob(jobId);
        }
        catch (ClassCastException e) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }

        if (job != null) {
            statusJson.put("jobStatus", job.getJobStatus());
            statusJson.put("executionTime", job.getJobExecutionTime());
        }
        else {
            DBObject report = findReport(jobId);

            statusJson.put("jobStatus", report.get("status"));
            statusJson.put("executionTime", report.get("executionTime"));

            if (report.get("status").equals(JobStatus.SUCCESS.name())) {
                URI contentUri = uriInfo.getAbsolutePathBuilder().path("content").build();
                statusJson.put("reportUri", contentUri);
            }
            else {
                statusJson.put("errorMsg", report.get("errorMsg"));
            }
        }

        return Response.ok(statusJson.toString()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{jobId}/content")
    public Response getReport(@PathParam("jobId") String jobId) throws JSONException {

        DBObject report = findReport(jobId);
        return Response.ok((String) report.get("result")).build();
    }

    private DBObject findReport(String jobId) {
        Mongo mongo = MongoFactory.getMongo();
        DB mongoDB = mongo.getDB(Conf.getInstance().getMongoDBDatabase());
        DBCollection jobsColl = mongoDB.getCollection(Conf.MONGO_JOBS_COLLECTION);
        BasicDBObject searchQuery = new BasicDBObject().append("jobId", jobId);
        DBObject report = jobsColl.findOne(searchQuery);
        if (report == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (!report.get("type").equals(HostMetricsHistoryJob.class.getSimpleName())) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        return report;
    }

}
