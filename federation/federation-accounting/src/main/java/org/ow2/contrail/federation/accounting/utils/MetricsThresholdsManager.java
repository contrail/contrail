package org.ow2.contrail.federation.accounting.utils;

import com.mongodb.*;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;

public class MetricsThresholdsManager {
    private DBCollection thresholdsColl;

    public MetricsThresholdsManager() {
        Mongo mongo = MongoFactory.getMongo();
        DB mongoDB = mongo.getDB(Conf.getInstance().getMongoDBDatabase());
        thresholdsColl = mongoDB.getCollection(Conf.MONGO_THRESHOLDS_COLLECTION);
    }

    public JSONObject getMetricsThresholds(String providerUuid) throws IOException, JSONException {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("providerUuid", providerUuid);
        DBObject doc = thresholdsColl.findOne(searchQuery);
        if (doc != null) {
            return new JSONObject((String) doc.get("thresholds"));
        }
        else {
            return null;
        }
    }

    public void storeMetricsThresholds(String providerUuid, JSONObject thresholds) throws IOException {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("providerUuid", providerUuid);

        DBObject doc = new BasicDBObject();
        doc.put("providerUuid", providerUuid);
        doc.put("thresholds", thresholds.toString());

        thresholdsColl.update(searchQuery, doc, true, false);
    }
}
