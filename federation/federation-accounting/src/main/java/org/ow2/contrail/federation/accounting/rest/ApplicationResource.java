package org.ow2.contrail.federation.accounting.rest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.accounting.jobs.AppMetricHistoryJob;
import org.ow2.contrail.federation.accounting.scheduler.Scheduler;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.ow2.contrail.federation.accounting.utils.DateUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/reports/applications/{appUuid}/metrics_history")
public class ApplicationResource {

    private String appUuid;

    @Context
    UriInfo uriInfo;

    public ApplicationResource(@PathParam("appUuid") String appUuid) {
        this.appUuid = appUuid;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("metrics_history")
    public Response createMetricHistoryReport(JSONObject data) throws Exception {
        List<String> metrics;
        Date startTime = null;
        Date endTime = null;
        int maxNumberOfIntervals;
        try {
            JSONArray metricArray = data.getJSONArray("metrics");
            if (metricArray.length() == 0) {
                throw new JSONException("Empty metrics array.");
            }
            metrics = new ArrayList<String>();
            for (int i = 0; i < metricArray.length(); i++) {
                metrics.add(metricArray.getString(i));
            }

            if (data.has("startTime")) {
                startTime = DateUtils.parseDate(data.getString("startTime"));
            }
            if (data.has("endTime")) {
                endTime = DateUtils.parseDate(data.getString("endTime"));
            }

            maxNumberOfIntervals = data.getInt("maxNumberOfIntervals");
        }
        catch (Exception e) {
            throw new WebApplicationException(Response.status(
                    Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }

        AppMetricHistoryJob job = new AppMetricHistoryJob(appUuid, metrics);
        job.setStartTime(startTime);
        job.setEndTime(endTime);
        job.setMaxNumberOfIntervals(maxNumberOfIntervals);

        Scheduler scheduler = SchedulerFactory.getScheduler();
        scheduler.addJob(job);

        URI location = new URI(job.getJobId());
        JSONObject content = new JSONObject();
        URI absoluteUri = uriInfo.getAbsolutePathBuilder().path(job.getJobId()).build();
        content.put("location", absoluteUri.toString());
        return Response.created(location).entity(content).build();
    }
}
