package org.ow2.contrail.federation.accounting.rest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.accounting.utils.MetricsThresholdsManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/metrics_thresholds")
public class MetricsThresholdsResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("providers/{providerUuid}")
    public JSONObject getThresholds(@PathParam("providerUuid") String providerUuid) throws IOException, JSONException {
        MetricsThresholdsManager manager = new MetricsThresholdsManager();
        JSONObject thresholds = manager.getMetricsThresholds(providerUuid);

        return thresholds;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("providers/{providerUuid}")
    public Response putThresholds(@PathParam("providerUuid") String providerUuid, JSONObject content) throws IOException {
        MetricsThresholdsManager manager = new MetricsThresholdsManager();
        manager.storeMetricsThresholds(providerUuid, content);

        return Response.noContent().build();
    }
}
