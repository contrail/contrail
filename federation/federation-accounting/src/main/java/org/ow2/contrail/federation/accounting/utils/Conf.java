package org.ow2.contrail.federation.accounting.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class Conf {
    private static Conf instance = new Conf();
    private static Logger log = LoggerFactory.getLogger(Conf.class);
    private Properties props;

    public static final String MONGO_JOBS_COLLECTION = "jobs";
    public static final String MONGO_THRESHOLDS_COLLECTION = "metrics_thresholds";

    public static Conf getInstance() {
        return instance;
    }

    private Conf() {
    }

    public void load(String confFilePath) throws Exception {
        load(new File(confFilePath));
    }

    public void load(File confFile) throws Exception {
        props = new Properties();
        try {
            props.load(new FileInputStream(confFile));
            log.info(String.format("Configuration loaded successfully from file '%s'.", confFile));
        }
        catch (IOException e) {
            throw new Exception(String.format("Failed to read configuration file '%s': %s", confFile, e.getMessage()));
        }
    }

    public String getMongoDBHost() {
        return props.getProperty("mongodb.host");
    }

    public int getMongoDBPort() {
        return Integer.parseInt(props.getProperty("mongodb.port"));
    }

    public String getMongoDBDatabase() {
        return props.getProperty("mongodb.database");
    }

    public int getSchedulerPoolSize() {
        return Integer.parseInt(props.getProperty("scheduler.pool.size"));
    }

    public String getKeystoreFile() {
        return props.getProperty("keystore.file");
    }

    public String getKeystorePass() {
        return props.getProperty("keystore.pass");
    }

    public String getTruststoreFile() {
        return props.getProperty("truststore.file");
    }

    public String getTruststorePass() {
        return props.getProperty("truststore.pass");
    }

    public String getOAuthClientId() {
        return props.getProperty("oauthClient.clientId");
    }

    public String getOAuthClientSecret() {
        return props.getProperty("oauthClient.clientSecret");
    }

    public URI getAddressFederationApi() throws URISyntaxException {
        String address = props.getProperty("address.federation-api");
        if (!address.endsWith("/")) {
            address += "/";
        }
        return new URI(address);
    }

    public URI getAddressOAuthAS() throws URISyntaxException {
        String address = props.getProperty("address.oauth-as");
        if (!address.endsWith("/")) {
            address += "/";
        }
        return new URI(address);
    }
}
