package org.ow2.contrail.federation.accounting.jobs;

import com.mongodb.*;
import org.bson.types.ObjectId;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.MongoFactory;

import java.util.Date;
import java.util.UUID;

public abstract class Job implements Runnable {
    protected JobStatus jobStatus;
    protected String jobId;
    protected Date created;
    protected String accessToken;
    protected DBCollection jobsColl;
    protected ObjectId _id;
    protected Date jobStartTime;

    protected Job() {
        jobId = UUID.randomUUID().toString();
        created = new Date();

        Mongo mongo = MongoFactory.getMongo();
        DB mongoDB = mongo.getDB(Conf.getInstance().getMongoDBDatabase());
        jobsColl = mongoDB.getCollection(Conf.MONGO_JOBS_COLLECTION);
    }

    public String getJobId() {
        return jobId;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public DBObject export() throws Exception {
        DBObject jobData = new BasicDBObject();
        jobData.put("jobId", jobId);
        jobData.put("created", created);
        jobData.put("status", jobStatus.name());
        jobData.put("type", this.getClass().getSimpleName());
        jobData.put("accessToken", accessToken);
        return jobData;
    }

    public void persist() throws Exception {
        DBObject jobData = this.export();
        jobsColl.insert(jobData);
        _id = (ObjectId) jobData.get("_id");
    }

    public Double getJobExecutionTime() {
        if (jobStartTime == null) {
            return null;
        }

        Date endTime = new Date();
        return (endTime.getTime() - jobStartTime.getTime())/1000.0;
    }

    public abstract JSONObject getDetailedStatus() throws JSONException;
}
