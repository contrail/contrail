package org.ow2.contrail.federation.accounting.jobs;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.OAuthHttpClient;
import org.ow2.contrail.common.oauth.client.atmanager.OAuthATManager;
import org.ow2.contrail.common.oauth.client.atmanager.OAuthATManagerFactory;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Date;
import java.util.List;

public class HostMetricsHistoryJob extends Job {
    private static Logger log = LoggerFactory.getLogger(HostMetricsHistoryJob.class);

    private String providerUuid;
    private List<String> sids;
    private List<String> metrics;
    private Date startTime;
    private Date endTime;
    private int numberOfIntervals;

    public HostMetricsHistoryJob(String providerUuid, List<String> sids, List<String> metrics) {
        this.providerUuid = providerUuid;
        this.sids = sids;
        this.metrics = metrics;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setNumberOfIntervals(int numberOfIntervals) {
        this.numberOfIntervals = numberOfIntervals;
    }

    @Override
    public DBObject export() throws Exception {
        DBObject jobData = super.export();
        BasicDBList sidsArray = new BasicDBList();
        sidsArray.addAll(sids);
        jobData.put("sids", sidsArray);
        BasicDBList metricsArray = new BasicDBList();
        for (String metric : metrics) {
            metricsArray.add(metric);
        }
        jobData.put("metrics", metricsArray);
        jobData.put("startTime", startTime);
        jobData.put("endTime", endTime);
        jobData.put("numberOfIntervals", numberOfIntervals);
        return jobData;
    }

    @Override
    public JSONObject getDetailedStatus() throws JSONException {
        return null;
    }

    @Override
    public void run() {
        log.debug("Job {} started.", jobId);
        jobStatus = JobStatus.RUNNING;
        jobStartTime = new Date();

        try {
            retrieveHistory();

            // store status
            BasicDBObject update = new BasicDBObject();
            update.append("status", jobStatus.name())
                    .append("executionTime", getJobExecutionTime());
            updateJob(update);

            log.debug("Job {} finished with status {}.", jobId, jobStatus);
        }
        catch (Exception e) {
            log.error(String.format("Job %s failed with an error: %s", jobId, e.getMessage()), e);
            jobStatus = JobStatus.ERROR;
        }
    }

    private void retrieveHistory() throws Exception {

        try {

            log.debug("Obtaining an access token to query federation-api...");
            OAuthATManager oauthATManager = OAuthATManagerFactory.getOAuthATManager();
            AccessToken accessTokenForFedApi = oauthATManager.getAccessToken("FEDERATION");

            OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                    Conf.getInstance().getKeystoreFile(),
                    Conf.getInstance().getKeystorePass(),
                    Conf.getInstance().getTruststoreFile(),
                    Conf.getInstance().getTruststorePass());

            log.debug("Asking federation-api for the provider-accounting service address at the provider {}.",
                    providerUuid);

            URI serviceUri = Conf.getInstance().getAddressFederationApi().resolve(
                    String.format("providers/%s/services/provider-accounting", providerUuid));
            HttpResponse serviceResponse = oAuthHttpClient.get(serviceUri, accessTokenForFedApi.getValue());
            String respContent = oAuthHttpClient.getContent(serviceResponse);

            if (serviceResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new Exception(String.format(
                        "Failed to obtain provider-accounting service address for the provider %s " +
                                "from the federation-api: %s", providerUuid, serviceResponse.getStatusLine()));
            }
            JSONObject serviceInfo = new JSONObject(respContent);
            String address = serviceInfo.getString("address");
            if (!address.endsWith("/")) {
                address += "/";
            }
            log.debug("Provider-accounting service address is {}", address);
            String target = address + "metrics_history/condensed_data";

            log.debug("Retrieving metrics history from the provider {}...", providerUuid);

            JSONObject requestParams = new JSONObject();
            requestParams.put("source", "host");
            JSONArray sidsArr = new JSONArray();
            for (String sid : sids) {
                sidsArr.put(sid);
            }
            requestParams.put("sids", sidsArr);

            JSONArray metricsArray = new JSONArray();
            for (String metric : metrics) {
                metricsArray.put(metric);
            }
            requestParams.put("metrics", metricsArray);
            if (startTime != null) {
                requestParams.put("startTime", DateUtils.format(startTime));
            }
            if (endTime != null) {
                requestParams.put("endTime", DateUtils.format(endTime));
            }
            requestParams.put("numberOfIntervals", numberOfIntervals);
            HttpEntity entity = new StringEntity(requestParams.toString(), ContentType.APPLICATION_JSON);

            log.debug("Sending HTTP request to " + target);
            HttpResponse response = oAuthHttpClient.post(new URI(target), accessToken, entity);
            log.debug("Received response: " + response.getStatusLine());

            String content = oAuthHttpClient.getContent(response);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                updateJob(new BasicDBObject("result", content));
                jobStatus = JobStatus.SUCCESS;
            }
            else {
                String message = String.format(
                        "Unexpected response received from the provider %s:\n%s\n%s",
                        providerUuid, response.getStatusLine().toString(), content);
                log.debug(message);
                updateJob(new BasicDBObject("errorMsg", message));
                jobStatus = JobStatus.FAILED;
            }
        }
        catch (Exception e) {
            String message = String.format(
                    "Failed to obtain accounting data from the provider %s: %s",
                    providerUuid, e.getMessage());
            log.error(message, e);
            updateJob(new BasicDBObject("errorMsg", message));
            jobStatus = JobStatus.ERROR;
        }
    }

    private void updateJob(BasicDBObject o) {
        BasicDBObject searchQuery = new BasicDBObject().append("_id", _id);
        BasicDBObject update = new BasicDBObject();
        update.append("$set", o);
        jobsColl.update(searchQuery, update);
    }
}
