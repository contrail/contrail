package org.ow2.contrail.federation.accounting;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.federation.accounting.jobs.JobStatus;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.DateUtils;
import org.ow2.contrail.federation.accounting.utils.MongoFactory;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.fail;

@Ignore
public class HostMetricHistoryJobTest extends JerseyTest {

    public HostMetricHistoryJobTest() throws Exception {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.federation.accounting.rest")
                .contextPath("fed-acc").build());
    }

    @Before
    public void setUp() throws Exception {
        SchedulerFactory.initScheduler();
        Conf.getInstance().load("src/test/resources/federation-accounting-test.properties");
        MongoFactory.init();
    }

    @Test
    public void test() throws Exception {
        WebResource webResource = resource();
        String baseUri = webResource.getURI().toString();

        // submit job 1: cpu_usage metric history
        Calendar startTimeCal = Calendar.getInstance();
        startTimeCal.set(2012, 5, 1, 0, 0, 0);
        Calendar endTimeCal = Calendar.getInstance();
        endTimeCal.set(2012, 5, 1, 1, 0, 0);

        JSONObject postData = new JSONObject();

        postData.put("providerUuid", "myProvider");

        // sids
        JSONArray sids = new JSONArray();
        sids.put("host001.test.com");
        sids.put("host002.test.com");
        sids.put("host003.test.com");
        postData.put("sids", sids);

        // metrics
        JSONArray metrics = new JSONArray();
        metrics.put("cpu.load_one");
        metrics.put("cpu.load_five");
        metrics.put("cpu.idle");
        postData.put("metrics", metrics);

        // startTime and endTime
        postData.put("startTime", DateUtils.format(startTimeCal.getTime()));
        postData.put("endTime", DateUtils.format(endTimeCal.getTime()));

        ClientResponse response = webResource.path("/reports/host_metrics_history")
                .post(ClientResponse.class, postData);

        if (response.getClientResponseStatus().getStatusCode() != 201) {
            fail("Invalid response code: " + response.getClientResponseStatus().toString());
        }
        String statusUriAbs = response.getHeaders().getFirst("Location");
        String statusUri = statusUriAbs.replaceFirst(baseUri, "");

        /*String reportUri = response.getString("report");
        reportUri = reportUri.replaceFirst(baseUri, "");
        String statusUri = response.getString("status");
        statusUri = statusUri.replaceFirst(baseUri, "");
        String jobId = reportUri.substring(reportUri.lastIndexOf("/") + 1);*/

        JSONObject status = webResource.path(statusUri).get(JSONObject.class);
        System.out.println(status.toString(3));

        // wait until the job finishes
        Date startTime = new Date();
        while (true) {
            Thread.sleep(500);
            status = webResource.path(statusUri).get(JSONObject.class);
            if (status.get("jobStatus").equals(JobStatus.SUCCESS.name())) {
                System.out.println(status.toString(3));
                break;
            }
            Date now = new Date();
            if (now.getTime() - startTime.getTime() > 30000) {
                System.out.println(status.toString(3));
                fail("Timeout!");
            }
        }

        // get report
        String reportContentUri = status.getString("reportUri").replaceFirst(baseUri, "");
        JSONObject report = webResource.path(reportContentUri).get(JSONObject.class);
        System.out.println();
        System.out.println(report.toString(3));
    }
}
