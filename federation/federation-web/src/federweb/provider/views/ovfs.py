from uuid import uuid4

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse
from django import forms

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailMixin, \
    RestRemoveView, RestDetailView
from federweb.provider.views import ProvidersMixin

class OvfsMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'ovfs'
    
    @cached_property
    def model(self):
        return self.provider.ovfs

class OvfsList(OvfsMixin, RestMixin, TemplateView):
    pass

class OvfsDetail(OvfsMixin, RestDetailView):
    pass

class OvfForm(forms.Form):
    name = forms.CharField()
    
    def save(self, ovf):
        data = self.cleaned_data
        
        ovf.name = data['name']
        
        return bool(ovf.save())

class OvfsCreate(OvfsMixin, FormView):
    action_name = 'create'
    form_class = OvfForm
    
    @property
    def success_url(self):
        return reverse('provider_ovfs', args=[self.provider.id])
    
    def get_initial(self):
        return {
            'name': 'OVF-' + uuid4().hex
        }
    
    def form_valid(self, form):
        ovf = self.model.new()
        
        if form.save(ovf):
            messages.success(self.request, u'OVF has been created.')
            
            return HttpResponseRedirect(self.get_success_url())
        
        messages.error(self.request, 'OVF could not be created.')
        
        return self.form_invalid(form)

class OvfsEdit(OvfsMixin, RestDetailMixin, FormView):
    action_name = 'edit'
    form_class = OvfForm
    
    @property
    def success_url(self):
        return reverse('provider_ovfs', args=[self.provider.id])
    
    def get_form_kwargs(self):
        kw = super(OvfsEdit, self).get_form_kwargs()
        kw['initial'] = {
            'name': self.obj.name
        }
        return kw
    
    def form_valid(self, form):
        if form.save(self.obj):
            messages.success(self.request, u'OVF has been saved.')
        
            return HttpResponseRedirect(self.get_success_url())
        
        messages.error(self.request, 'OVF could not be saved.')
        
        return self.form_invalid(form)

class OvfsRemove(OvfsMixin, RestRemoveView):
    message = u'OVF was successfully removed.'
    
    @property
    def url(self):
        return reverse('provider_ovfs', args=[self.provider.id])
