from uuid import uuid4

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse
from django import forms

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailMixin, \
    RestRemoveView, RestDetailView
from federweb.provider.views import ProvidersMixin

class SlatsMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'slats'
    
    @cached_property
    def model(self):
        return self.provider.slats

class SlatsList(SlatsMixin, RestMixin, TemplateView):
    pass

class SlatsDetail(SlatsMixin, RestDetailView):
    pass

class SlatForm(forms.Form):
    name = forms.CharField()
    url = forms.URLField()
    
    def save(self, slat):
        data = self.cleaned_data
        
        slat.name = data['name']
        slat.url = data['url']
        
        return bool(slat.save())

class SlatsCreate(SlatsMixin, FormView):
    action_name = 'create'
    form_class = SlatForm
    
    @property
    def success_url(self):
        return reverse('provider_slats', args=[self.provider.id])
    
    def get_initial(self):
        slat_id = uuid4().hex[:8]
        return {
            'name': 'SLAT-' + slat_id,
            #'url': 'http://contrail-project.eu/slat/' + slat_id
        }
    
    def form_valid(self, form):
        slat = self.model.new()
        
        if form.save(slat):
            messages.success(self.request, u'SLAT has been created.')
            
            return HttpResponseRedirect(self.get_success_url())
        
        messages.error(self.request, 'SLAT could not be created.')
        
        return self.form_invalid(form)

class SlatsEdit(SlatsMixin, RestDetailMixin, FormView):
    action_name = 'edit'
    form_class = SlatForm
    
    @property
    def success_url(self):
        return reverse('provider_slats', args=[self.provider.id])
    
    def get_form_kwargs(self):
        kw = super(SlatsEdit, self).get_form_kwargs()
        kw['initial'] = {
            'name': self.obj.name,
            'url': self.obj.url
        }
        return kw
    
    def form_valid(self, form):
        if form.save(self.obj):
            messages.success(self.request, u'SLAT has been saved.')
        
            return HttpResponseRedirect(self.get_success_url())
        
        messages.error(self.request, 'SLAT could not be saved.')
        
        return self.form_invalid(form)

class SlatsRemove(SlatsMixin, RestRemoveView):
    message = u'SLAT was successfully removed.'
    
    @property
    def url(self):
        return reverse('provider_slats', args=[self.provider.id])
