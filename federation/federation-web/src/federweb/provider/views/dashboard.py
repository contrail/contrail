from django.views.generic.base import TemplateView

from federweb.base.views import AppNodeMixin
from federweb.provider.views import ProvidersMixin

class Dashboard(ProvidersMixin, AppNodeMixin, TemplateView):
    node_name = 'dashboard'
