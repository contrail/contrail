from django.views.generic import TemplateView

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailView
from federweb.provider.views import ProvidersMixin

class VosMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'vos'
    
    @cached_property
    def model(self):
        return self.provider.vos

class VosList(VosMixin, RestMixin, TemplateView):
    pass

class VosDetail(VosMixin, RestDetailView):
    pass
