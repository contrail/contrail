from django.conf.urls.defaults import patterns, url, include

from federweb.provider.views import ProvidersList
from federweb.provider.views.dashboard import Dashboard
from federweb.provider.views.ovfs import OvfsList, OvfsCreate, OvfsDetail,\
    OvfsEdit, OvfsRemove
from federweb.provider.views.slats import SlatsList, SlatsCreate, SlatsDetail,\
    SlatsEdit, SlatsRemove
from federweb.provider.views.vms import VmsList, VmsDetail
from federweb.provider.views.servers import ServersList, ServersDetail
from federweb.provider.views.clusters import ClustersList, ClustersDetail
from federweb.provider.views.vos import VosList, VosDetail

providers_urlpatterns = patterns(r'',
    url(r'^$', Dashboard.as_view(), name='provider_dashboard'),
    
    url(r'^/ovfs$', OvfsList.as_view(), name='provider_ovfs'),
    url(r'^/ovfs/create$', OvfsCreate.as_view(), name='provider_ovfs_create'),
    url(r'^/ovfs/(?P<id>\d+)$', OvfsDetail.as_view(), name='provider_ovfs_detail'),
    url(r'^/ovfs/(?P<id>\d+)/edit$', OvfsEdit.as_view(), name='provider_ovfs_edit'),
    url(r'^/ovfs/(?P<id>\d+)/remove$', OvfsRemove.as_view(), name='provider_ovfs_remove'),
    
    url(r'^/slats$', SlatsList.as_view(), name='provider_slats'),
    url(r'^/slats/create$', SlatsCreate.as_view(), name='provider_slats_create'),
    url(r'^/slats/(?P<id>\d+)$', SlatsDetail.as_view(), name='provider_slats_detail'),
    url(r'^/slats/(?P<id>\d+)/edit$', SlatsEdit.as_view(), name='provider_slats_edit'),
    url(r'^/slats/(?P<id>\d+)/remove$', SlatsRemove.as_view(), name='provider_slats_remove'),
    
    url(r'^/vms$', VmsList.as_view(), name='provider_vms'),
    url(r'^/vms/(?P<id>\d+)$', VmsDetail.as_view(), name='provider_vms_detail'),

    url(r'^/servers$', ServersList.as_view(), name='provider_servers'),
    url(r'^/servers/(?P<id>\d+)$', ServersDetail.as_view(), name='provider_servers_detail'),
    
    url(r'^/clusters$', ClustersList.as_view(), name='provider_clusters'),
    url(r'^/clusters/(?P<id>\d+)$', ClustersDetail.as_view(), name='provider_clusters_detail'),
    
    url(r'^/vos$', VosList.as_view(), name='provider_vos'),
    url(r'^/vos/(?P<id>\d+)$', VosDetail.as_view(), name='provider_vos_detail'),
)

urlpatterns = patterns('',
    url(r'^$', ProvidersList.as_view(), name='provider'),
    
    url(r'^/providers/(?P<provider_id>\d+)', include(providers_urlpatterns))
)
