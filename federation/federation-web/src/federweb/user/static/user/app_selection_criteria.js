function confirmCancel() {
    var answer = confirm("Are you sure you want to cancel?");
    if (answer === true) {
        window.location.replace("/user#/");
    }
}

function submitApplication() {
    $.jGrowl("Submitting application...");
    $.ajax({
        type: "POST",
        url: "/user/app-selection-criteria/save",
        data: $("#app_selection_criteria_form").serialize(),
        dataType: 'json'
    })
        .done(function (data, textStatus, jqXHR) {
            $.jGrowl("Application has been created successfully.");

            var postData = {
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
                'appId' : data.appId,
                'slatId' : data.slatId,
                'negotiationId' : data.negotiationId
            }
            $().redirect('/user/offers', postData);
        })
        .fail(function (jqXHR, textStatus) {
            $.jGrowl("Failed to create the application: " + jqXHR.status + " " + jqXHR.statusText);
        });
}