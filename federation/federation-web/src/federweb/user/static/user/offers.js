Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '../ux');

Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*'
]);

Ext.onReady(function () {
    Ext.QuickTips.init();

    for (var i in offers) {
        drawTreeGrid(offers[i]);
    }
});

function drawTreeGrid(offer) {
    var container = "treegrid-" + offer.offerId;

    // define model
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'name', type: 'string'},
            {name: 'qosTerm', type: 'string'},
            {name: 'operator', type: 'string'},
            {name: 'target', type: 'string'},
            {name: 'status', type: 'string'},
            {name: 'thresholdOld', type: 'string'},
            {name: 'thresholdNew', type: 'string'}
            /*{name: 'threshold', type: 'string', convert: function (v, r) {
             if (!r.data.qosTerm) {
             return "";
             }
             else {
             return r.data.thresholdOld + "/" + r.data.thresholdNew;
             }
             }}*/
        ]
    });

    // define store
    var store = Ext.create('Ext.data.TreeStore', {
        model: 'Task',
        proxy: {
            type: 'ajax',
            url: getProxiedUrl(
                sprintf('%s/users/%s/slats/%s/negotiation/%s/proposals/%s/getTreeGridComparison', fedApiAddress, userUuid, slatId, negotiationId, offer.offerId))
        },
        folderSort: true
    });

    // draw tree grid
    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Offer Details',
        width: 1000,
        height: 300,
        renderTo: Ext.get(container),
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: store,
        multiSelect: true,
        columns: [
            {
                xtype: 'treecolumn',
                text: 'Name',
                width: 250,
                sortable: true,
                dataIndex: 'name',
                locked: true
            },
            {
                header: 'Status',
                flex: 30,
                dataIndex: 'status',
                sortable: true,
                align: 'center',
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    if (value === 'NOT_CHANGED') {
                        return "<span style='font-size:20px; font-weight:900; color: green' title='No change'>&#10004;</span>";
                    }
                    else if (value === 'CHANGED') {
                        return "<span style='font-size:20px; font-weight:900; color: orange' title='Changed'>&#x2260;</span>";
                    }
                    else if (value === 'ADDED') {
                        return "<span style='font-size:20px; font-weight:900; color: orange' title='Added'>+</span>";
                    }
                    else if (value === 'REMOVED') {
                        return "<span style='font-size:20px; font-weight:900; color: red' title='Removed'>&times;</span>";
                    }
                    else {
                        return "";
                    }
                    return sprintf("<img src='%simg/%s' width='15' height='15' title='%s'/>", staticUrl, icon, title);
                }
            },
            {
                header: 'Target',
                flex: 100,
                dataIndex: 'target',
                sortable: true
            },
            {
                header: 'QoS Term',
                flex: 100,
                dataIndex: 'qosTerm',
                sortable: true
            },
            {
                header: 'Operator',
                flex: 50,
                dataIndex: 'operator',
                sortable: false
            },
            {
                xtype: 'templatecolumn',
                header: 'Proposal Value',
                flex: 75,
                dataIndex: 'thresholdOld',
                sortable: false,
                tpl: Ext.create('Ext.XTemplate', '{[this.format(values)]}', {
                    format: function (values) {
                        if (values.qosTerm) {
                            return (values.thresholdOld) ? values.thresholdOld : 'None'
                        }
                    }
                })
            },
            {
                xtype: 'templatecolumn',
                header: 'Offer Value',
                flex: 75,
                dataIndex: 'thresholdNew',
                sortable: false,
                tpl: Ext.create('Ext.XTemplate', '{[this.format(values)]}', {
                    format: function (values) {
                        if (values.qosTerm) {
                            return (values.thresholdNew) ? values.thresholdNew : 'None'
                        }
                    }
                })
            }
        ]
    });
}

function getProxiedUrl(url) {
    return '/user/jsonproxy/' + window.btoa(url).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, ',');
}

function acceptOffer(offerId) {
    $.jGrowl(sprintf("Accept SLA offer %d...", offerId));
    var url = sprintf("%s/users/%s/slats/%s/negotiation/%s/proposals/%d/createAgreement",
        fedApiAddress, userUuid, slatId, negotiationId, offerId);

    $.jGrowl("Calling createAgreement...");
    var data = {
        appUuid: appId
    }
    $.ajax({
        type: "POST",
        url: getProxiedUrl(url),
        contentType: "application/json",
        data: JSON.stringify(data),
        dataType: 'json'
    })
        .done(function (data, textStatus, jqXHR) {
            $.jGrowl("Negotiation has finished successfully. SLA agreement has been created.");
            $.jGrowl("SLA ID: " + data.slaId);
            var postData = {
                'csrfmiddlewaretoken': CRpc.get_cookie('csrftoken'),
                'slaId': data.slaId,
                'appId': appId
            }
            $().redirect('/user/sla-agreement', postData);
        })
        .fail(function (jqXHR, textStatus) {
            $.jGrowl("Failed to create SLA agreement. Unexpected response: " + jqXHR.status + " " + jqXHR.statusText);
        });

}

function renegotiate(offerId, offerNum) {
    $.jGrowl(sprintf("Renegotiating SLA offer %d...", offerId));
    var postData = {
        'csrfmiddlewaretoken': CRpc.get_cookie('csrftoken'),
        'appId': appId,
        'slatId': slatId,
        'negotiationId': negotiationId,
        'offerId': offerId,
        'offerNum': offerNum
    };
    $().redirect('/user/show-renegotiation', postData);
}

function confirmCancel() {
    var answer = confirm("Are you sure you want to cancel?");
    if (answer === true) {
        window.location.replace("/user#/");
    }
}
