/*global $, d3, active_driver*/

var format = d3.time.format("%Y-%m-%dT%H:%M:%S%Z");
var thresholds = {},
    providerUris = {},
    dialogHeight,
    dialogWidth,
    contextHeight = 120,
    transitionDuration = 250;


function urlToProxy(url) {
    "use strict";
    return active_driver['proxy-uri'] + window.btoa(url).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, ',');
}


// returns metrics selected by user
function getSelectedMetrics() {
    "use strict";
    var metrics = [];
    $('input:checked', $('#metrics')).each(function () {
        metrics.push($(this).attr('id'));
    });
    return metrics;
}


// calls callback when data is loaded
function loadReportData(request, callback) {
    "use strict";
    var openedRequests = [],
        timer;

    openedRequests.push(d3.xhr(urlToProxy(active_driver['feder-acc-uri'] + "/reports/host_metrics_history"))
        .header("Content-type", "application/json")
        .post(JSON.stringify(request), function (error, response) {
            if (error) {
                $.jGrowl("Error getting JSON data!");
                callback(error);
                console.error(error);
                return;
            }
            var statusUrl = JSON.parse(response.responseText).location;

            function checkIfStatusFinished() {
                $.jGrowl("Checking if report is finished...");

                openedRequests.push(d3.json(urlToProxy(statusUrl), function (error, json) {
                    if (error) {
                        $.jGrowl("Error getting JSON data!");
                        callback(error);
                        console.error(error);
                        return;
                    }

                    if (json.jobStatus === "RUNNING") {
                        timer = setTimeout(checkIfStatusFinished, 200);
                        return;
                    }
                    if (json.jobStatus === "SUCCESS") {
                        $.jGrowl("SUCCESS!!!!! :D");

                        var reportUrl = json.reportUri;


                        openedRequests.push(d3.json(urlToProxy(reportUrl), function (error, json) {
                            if (error) {
                                $.jGrowl("Error getting JSON data!");
                                callback(error);
                                console.error(error);
                                return;
                            }

                            callback(false, request, json);
                        }));

                        return;
                    }
                    if (json.jobStatus === "ERROR") {
                        $.jGrowl("Error: " + json.errorMsg);
                        return;
                    }
                    if (json.jobStatus === "FAILED") {
                        $.jGrowl("FAILED! :(");
                        return;
                    }
                    if (json.jobStatus === "QUEUED") {
                        timer = setTimeout(checkIfStatusFinished, 500);
                        return;
                    }

                    $.jGrowl("Unknown job status? :O");
                }));
            }

            checkIfStatusFinished();
        }));
    return {
        cancel: function () {
            clearTimeout(timer);
            $.each(openedRequests, function (key, value) {
                value.abort();
            });
        }
    };
}


// parse data for stacked chart
function parseOneMetricAllSids(intervals, sids, metric) {
    "use strict";
    var data = [],
        obj,
        index;
    $.each(intervals, function (key, value) {
        obj = {};
        obj.date = value;
        obj.sids = {};
        index = key;
        $.each(sids, function (key, value) {
            value = value.metricsData;
            if (value[metric][index] === null) {
                obj.sids[key] = 0;
            } else {
                obj.sids[key] = value[metric][index][1]; // 1 = average
            }
        });
        data.push(obj);
    });
    return data;
}

// draws chart for one metric and one server
function singleMetricChart() {
    "use strict";
    var margin = {top: 20, right: 100, bottom: 30 + contextHeight + 30, left: 70},
        contextMargin = {top: dialogHeight - contextHeight - 30, right: 100, bottom: 30, left: 70},
        width = dialogWidth,
        height = dialogHeight,
        xScale = d3.time.scale(),
        yScale = d3.scale.linear(),
        xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
        yAxis = d3.svg.axis().scale(yScale).orient("left").tickSubdivide(true),
        xScaleContext = d3.time.scale(),
        yScaleContext = d3.scale.linear(),
        xAxisContext = d3.svg.axis().scale(xScale).orient("bottom"),
        yAxisContext = d3.svg.axis().scale(yScaleContext).orient("left"),
        xValue = function (d) {
            return d[0];
        },
        minValue = function (d) {
            return +d[1];
        },
        avgValue = function (d) {
            return +d[2];
        },
        maxValue = function (d) {
            return +d[3];
        },
        lineMin = d3.svg.line(),
        lineAvg = d3.svg.line(),
        lineMax = d3.svg.line(),
        lineMinContext = d3.svg.line(),
        lineAvgContext = d3.svg.line(),
        lineMaxContext = d3.svg.line(),
        label = "",
        sid = "",
        request,
        metric,
        intervalLength;

    function X(d) {
        return xScale(d[0]);
    }

    function Ymin(d) {
        return yScale(d[1]);
    }

    function Yavg(d) {
        return yScale(d[2]);
    }

    function Ymax(d) {
        return yScale(d[3]);
    }

    function XContext(d) {
        return xScaleContext(d[0]);
    }

    function YminContext(d) {
        return yScaleContext(d[1]);
    }

    function YavgContext(d) {
        return yScaleContext(d[2]);
    }

    function YmaxContext(d) {
        return yScaleContext(d[3]);
    }


    lineMin.x(X).y(Ymin).defined(function (d) {
        return !isNaN(d[1]);
    });
    lineAvg.x(X).y(Yavg).defined(function (d) {
        return !isNaN(d[2]);
    });
    lineMax.x(X).y(Ymax).defined(function (d) {
        return !isNaN(d[3]);
    });

    lineMinContext.x(XContext).y(YminContext).defined(function (d) {
        return !isNaN(d[1]);
    });
    lineAvgContext.x(XContext).y(YavgContext).defined(function (d) {
        return !isNaN(d[2]);
    });
    lineMaxContext.x(XContext).y(YmaxContext).defined(function (d) {
        return !isNaN(d[3]);
    });


    function chartBig(selection) {
        selection.each(function (data) {
            var realWidth = width - margin.left - margin.right,
                realHeight = height - margin.top - margin.bottom,
                realHeightContext = height - contextMargin.top - contextMargin.bottom;

            yAxis.tickSize(-realWidth, 0);

            // x-scale.
            xScale
                .domain(d3.extent(data, function (d) {
                    return d[0];
                }))
                .range([0, realWidth]);

            // y-scale.
            var domain = [d3.min(data, function (d) {
                    return d[1]; // minimum
                }), d3.max(data, function (d) {
                    return d[3]; // maximum
                })];
            if (domain[0] === domain[1]) {
                domain[0] *= 0.99;
                domain[1] *= 1.01;
                if (domain[1] === 0) {
                    domain[1] = 0.001;
                }
            }
            yScale
                .domain(domain)
                .range([realHeight, 0]);


            // select the svg element
            var svg = d3.select(this)
                .attr("width", width)
                .attr("height", height)
                .attr("class", "single-metric-chart");

            svg.append("defs").append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", realWidth)
                .attr("height", realHeight);

            // set the inner dimensions.
            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


            /* thresholds warning/critical background */
            var thresholdBackgrounds = {};

            var provider = "xlab";
            if (thresholds[provider] && thresholds[provider][metric]) {
                $.each(thresholds[provider][metric], function (key, value) {
                    thresholdBackgrounds[key] = g.append("path")
                        .attr("class", "threshold " + key)
                        .attr("clip-path", "url(#clip)");
                });
            }

            function moveThresholds(transition) {
                if (thresholds[provider] && thresholds[provider][metric]) {
                    var domain = yScale.domain(), gt, lt, y0, y1, height, object;

                    $.each(thresholds[provider][metric], function (key, value) {
                        gt = domain[0];
                        if (value.length > 0) {
                            gt = value[0];
                        }
                        lt = domain[1];
                        if (value.length > 1) {
                            lt = value[1];
                        }
                        y0 = yScale(gt);
                        y1 = yScale(lt);
                        if (transition) {
                            object = transition.select(".threshold." + key);
                        } else {
                            object = thresholdBackgrounds[key];
                        }
                        object.attr("d", "m 0," + y1 + " h " + realWidth + " V " + y0 + " h " + -realWidth + " z");
                    });
                }
            }
            moveThresholds();


            // create the line paths
            var pathMin = g.append("path")
                .attr("class", "line lineMin")
                .attr("d", lineMin)
                .attr("clip-path", "url(#clip)");
            var pathAvg = g.append("path")
                .attr("class", "line lineAvg")
                .attr("d", lineAvg)
                .attr("clip-path", "url(#clip)");
            var pathMax = g.append("path")
                .attr("class", "line lineMax")
                .attr("d", lineMax)
                .attr("clip-path", "url(#clip)");

            // create x-axis
            g.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + yScale.range()[0] + ")")
                .call(xAxis);

            // create y-axis
            g.append("g")
                .attr("class", "y axis")
                .call(yAxis);


            /* legend */
            var legend = svg.append("g")
                .attr("transform", "translate(" + (margin.left + realWidth) + "," + (margin.top) + ")");

            // TODO: hardcoded?
            var legendData = [{
                name: "Maximum",
                color: "#ff0000"
            }, {
                name: "Average",
                color: "#00c000"
            }, {
                name: "Minimum",
                color: "#4f4fee"
            }];

            $.each(legendData, function (key, value) {
                var legendEntry = legend.append("g")
                    .attr("transform", "translate(5," + key * 20 + ")");
                legendEntry.append("circle")
                    .attr("cy", 4.5)
                    .attr("cx", 5)
                    .attr("r", 4.5)
                    .style("fill", value.color);

                legendEntry.append("text")
                    .text(value.name)
                    .attr("x", 10)
                    .attr("y", 10);
                legendEntry.append("title")
                    .text(value.name);
            });


            /* mouse over */
            var pointer = g.append("line")
                .attr("y2", realHeight)
                .style("stroke", "black")
                .style("stroke-width", "1px")
                .style("opacity", "0")
                .style("shape-rendering", "crispEdges")
                .style("stroke", "black")
                .style("stroke", "black");

            var mouseEventCatcher = g.append("rect").attr("width", realWidth).attr("height", realHeight)
                .style("shape-rendering", "crispEdges")
                .style("fill", "#ffffff")
                .style("opacity", 0);

            var tooltip = d3.select(".tooltip");

            var numberFormat = d3.format(".4r");

            function getYatX(data, x) {
                var domain = xScale.domain(),
                    time = xScale.invert(x),
                    index = Math.floor(data.length * ((+time - +domain[0]) / (+domain[1] - +domain[0]))),
                    factor,
                    values = [],
                    val;
                while (data[index][0] < time) {
                    index += 1;
                }
                while (data[index][0] > time) {
                    index -= 1;
                }
                factor = (+time - +data[index][0]) / (+data[index + 1][0] - +data[index][0]);
                var i;
                for (i = 1; i <= 3; i += 1) {
                    val = data[index][i] * (1 - factor) + data[index + 1][i] * factor;
//                    console.log(val);
                    if (val === null || isNaN(val)) {
                        val = "no value";
                    } else {
                        val = numberFormat(val);
                    }
                    values.push(val);
                }
                return values;
            }

            function mouseover() {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 1);
                pointer
                    .style("opacity", 1);
            }

            var mousemove = function () {
                var coordXY = d3.mouse(this),
                    x = coordXY[0],
                    date = xScale.invert(x),
                    values = getYatX(pathMax.data()[0], x);

                tooltip
                    .html("")
                    .style("left", (d3.event.pageX + 34) + "px")
                    .style("top", (d3.event.pageY - 35) + "px");

                tooltip.append("p").text(date);

                tooltip.append("p").text("Max: " + values[2]);
                tooltip.append("p").text("Avg: " + values[1]);
                tooltip.append("p").text("Min: " + values[0]);

                pointer.attr("x1", x).attr("x2", x);
            };

            function mouseout() {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
                pointer
                    .style("opacity", 0);
            }

            mouseEventCatcher
                .on("mouseover", mouseover)
                .on("mousemove", mousemove)
                .on("mouseout", mouseout);


            /* metric label */
            svg.append("text")
                .attr("transform", "translate(15," + (realHeight / 2 + margin.top) + ") rotate(-90)")
                .style("text-anchor", "middle")
                .text(label);

            /* chart title */
            svg.append("text")
                .attr("class", "title")
                .attr("transform", "translate(" + (margin.left + realWidth / 2) + ",10)")
                .style("text-anchor", "middle")
                .text(label + " for " + sid);



            /* SMALL CONTEXT CHART */
            // x-scale
            xScaleContext
                .domain(d3.extent(data, function (d) {
                    return d[0];
                }))
                .range([0, width - contextMargin.left - contextMargin.right]);


            // y-scale
            domain = [d3.min(data, function (d) {
                return d[1]; // minimum
            }), d3.max(data, function (d) {
                return d[3]; // maximum
            })];
            if (domain[0] === domain[1]) {
                domain[0] *= 0.99;
                domain[1] *= 1.01;
                if (domain[1] === 0) {
                    domain[1] = 0.001;
                }
            }
            yScaleContext
                .domain(domain)
                .range([height - contextMargin.top - contextMargin.bottom, 0]);


            var context = svg.append("g")
                .attr("transform", "translate(" + contextMargin.left + "," + contextMargin.top + ")");

            var originalData = [data];
            context.data(originalData);

            // create line paths
            var pathMinContext = context.append("path")
                .attr("class", "line lineMin")
                .attr("d", lineMinContext);
            var pathAvgContext = context.append("path")
                .attr("class", "line lineAvg")
                .attr("d", lineAvgContext);
            var pathMaxContext = context.append("path")
                .attr("class", "line lineMax")
                .attr("d", lineMaxContext);


            // x-axis
            context.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + yScaleContext.range()[0] + ")")
                .call(xAxisContext);

            // y-axis
            context.append("g").attr("class", "y axis")
                .call(yAxisContext);


            /* loading icon */
            var loading = g.append("g")
                .attr("class", "loading")
                .attr("transform", "translate(" + realWidth / 2 + "," + realHeight / 2 + ")");
            loading.append("path")
                .attr("d", "m -40,0 15,0 0,0 -15,0 z")
                .attr("class", "led one");
            loading.append("path")
                .attr("d", "m -20,0 15,0 0,0 -15,0 z")
                .attr("class", "led two");
            loading.append("path")
                .attr("d", "m 0,0 15,0 0,0 -15,0 z")
                .attr("class", "led three");
            loading.append("path")
                .attr("d", "m 20,0 15,0 0,0 -15,0 z")
                .attr("class", "led four");
            loading.style("display", "none");


            /* brush */
            var brush = d3.svg.brush()
                .x(xScaleContext);

            var lastRequest,
                moved = false;

            function cancleLastRequest() {
                if (lastRequest) {
                    lastRequest.cancel();
                    lastRequest = null;
                    loading.style("display", "none");
                }
            }

            function brushstart() {
                console.warn("Brush START!");
                cancleLastRequest();


                var t = g.transition().duration(0);
                g.data(originalData);
                t.select(".lineMin").attr("d", lineMin);
                t.select(".lineAvg").attr("d", lineAvg);
                t.select(".lineMax").attr("d", lineMax);
                moveThresholds(t);

                yScale
                    .domain(yScaleContext.domain());
                t = t.transition().duration(transitionDuration);
                t.select(".lineMin").attr("d", lineMin);
                t.select(".lineAvg").attr("d", lineAvg);
                t.select(".lineMax").attr("d", lineMax);
                t.select(".y.axis").call(yAxis);
                moveThresholds(t);
            }

            function brushmove() {
                console.warn("Brush MOVE!");
                cancleLastRequest();

                var t;

                var timeRange = brush.extent();
                if (Math.abs(timeRange[0] - timeRange[1]) < 10000) {
                    t = g.transition().duration(0);
                    xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                    yScale
                        .domain(yScaleContext.domain());
                    t.select(".y.axis").call(yAxis);
                    t.select(".lineMin").attr("d", lineMin);
                    t.select(".lineAvg").attr("d", lineAvg);
                    t.select(".lineMax").attr("d", lineMax);
                    moveThresholds(t);
                    g.select(".x.axis").call(xAxis);
                    return;
                }

                // cancel transition
                t = g.transition().duration(0);
                t.select(".lineMin").attr("d", lineMin);
                t.select(".lineAvg").attr("d", lineAvg);
                t.select(".lineMax").attr("d", lineMax);
                t.select(".y.axis").call(yAxis);
                moveThresholds(t);

                xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                g.select(".y.axis").call(yAxis);
                g.select(".lineMin").attr("d", lineMin);
                g.select(".lineAvg").attr("d", lineAvg);
                g.select(".lineMax").attr("d", lineMax);
                g.select(".y.axis").call(yAxis);
                g.select(".x.axis").call(xAxis);

                moved = true;
            }

            function brushend() {
                console.warn("Brush END!");

                function refreshChart(error, request, json) {
                    loading.style("display", "none");

                    var values = json.values[sid].metricsData[metric];
                    data = [];
                    var i;
                    for (i = 0; i < values.length; i += 1) {
                        if (values[i] === null) {
                            data.push([new Date(json.intervals[i])].concat([NaN, NaN, NaN]));
                        } else {
                            data.push([new Date(json.intervals[i])].concat(values[i]));
                        }
                    }
                    var focusData = [data];
                    g.data(focusData);
                    g.select(".lineMin").attr("d", lineMin);
                    g.select(".lineAvg").attr("d", lineAvg);
                    g.select(".lineMax").attr("d", lineMax);


                    // y-scale.
                    domain = [d3.min(focusData[0], function (d) {
                        return d[1]; // minimum
                    }), d3.max(focusData[0], function (d) {
                        return d[3]; // maximum
                    })];
                    if (domain[0] === domain[1]) {
                        domain[0] *= 0.99;
                        domain[1] *= 1.01;
                        if (domain[1] === 0) {
                            domain[1] = 0.001;
                        }
                    }
                    yScale
                        .domain(domain);

                    var t = g.transition().duration(transitionDuration);
                    t.select(".lineMin").attr("d", lineMin);
                    t.select(".lineAvg").attr("d", lineAvg);
                    t.select(".lineMax").attr("d", lineMax);
                    t.select(".y.axis").call(yAxis);

                    moveThresholds(t);
                }

                if (request) {
                    cancleLastRequest();
                    var timeRange = brush.extent();

                    var t = g.transition().duration(0);
                    xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                    t.select(".y.axis").call(yAxis);
                    context.data(originalData);
                    t.select(".lineMin").attr("d", lineMin);
                    t.select(".lineAvg").attr("d", lineAvg);
                    t.select(".lineMax").attr("d", lineMax);
                    moveThresholds(t);
                    g.select(".x.axis").call(xAxis);

                    if (Math.abs(timeRange[0] - timeRange[1]) < 10000) {
                        t = g.transition().duration(0);
                        xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                        yScale
                            .domain(yScaleContext.domain());
                        t.select(".y.axis").call(yAxis);
                        t.select(".lineMin").attr("d", lineMin);
                        t.select(".lineAvg").attr("d", lineAvg);
                        t.select(".lineMax").attr("d", lineMax);
                        moveThresholds(t);
                        g.select(".x.axis").call(xAxis);
                        return;
                    }

                    loading.style("display", null);

                    request.metrics = [metric];
                    request.startTime = format(timeRange[0]);
                    request.endTime = format(new Date(+timeRange[1] + 1000));
                    lastRequest = loadReportData(request, refreshChart);
                }

                moved = false;
            }

            brush
                .on("brushstart", brushstart)
                .on("brush", brushmove)
                .on("brushend", brushend);

            context.append("g")
                .attr("class", "x brush")
                .call(brush)
                .selectAll("rect")
                .attr("height", realHeightContext);
        });
    }

    function chart(selection) {
        selection.each(function (data) {
            var margin = {top: 20, right: 100, bottom: 30, left: 70},
                width = 400,
                height = 200;

            var realWidth = width - margin.left - margin.right,
                realHeight = height - margin.top - margin.bottom,
                xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
                yAxis = d3.svg.axis().scale(yScale).orient("left").tickSubdivide(true);

            // number of ticks
            xAxis.ticks(4);
            yAxis.ticks(5);

            yAxis.tickSize(-realWidth, 0);

            // x-scale
            xScale
                .domain(d3.extent(data, function (d) {
                    return d[0];
                }))
                .range([0, realWidth]);


            // y-scale
            var domain = [d3.min(data, function (d) {
                    return d[1]; // minimum
                }), d3.max(data, function (d) {
                    return d[3]; // maximum
                })];
            if (domain[0] === domain[1]) {
                domain[0] *= 0.99;
                domain[1] *= 1.01;
                if (domain[1] === 0) {
                    domain[1] = 0.001;
                }
            }
            yScale
                .domain(domain)
                .range([realHeight, 0]);


            // create svg element
            var svg = d3.select(this)
                .attr("width", width)
                .attr("height", height)
                .attr("class", "single-metric-chart");

            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // create line paths
            var pathMin = g.append("path")
                .attr("class", "line lineMin")
                .attr("d", lineMin);
            var pathAvg = g.append("path")
                .attr("class", "line lineAvg")
                .attr("d", lineAvg);
            var pathMax = g.append("path")
                .attr("class", "line lineMax")
                .attr("d", lineMax);

            // x-axis
            g.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + yScale.range()[0] + ")")
                .call(xAxis);

            // y-axis
            g.append("g")
                .attr("class", "y axis")
                .call(yAxis);


            /* legend */
            var legend = svg.append("g")
                .attr("transform", "translate(" + (margin.left + realWidth) + "," + (margin.top) + ")");

            // TODO: hardcoded?
            var legendData = [{
                name: "Maximum",
                color: "#ff0000"
            }, {
                name: "Average",
                color: "#00c000"
            }, {
                name: "Minimum",
                color: "#4f4fee"
            }];

            $.each(legendData, function (key, value) {
                var legendEntry = legend.append("g")
                    .attr("transform", "translate(5," + key * 20 + ")");
                legendEntry.append("circle")
                    .attr("cy", 4.5)
                    .attr("cx", 5)
                    .attr("r", 4.5)
                    .style("fill", value.color);

                legendEntry.append("text")
                    .text(value.name)
                    .attr("x", 10)
                    .attr("y", 10);
                legendEntry.append("title")
                    .text(value.name);
            });


            /* metric label */
            svg.append("text")
                .attr("transform", "translate(15," + (realHeight / 2 + margin.top) + ") rotate(-90)")
                .style("text-anchor", "middle")
                .text(label);

            /* chart title */
            svg.append("text")
                .attr("class", "title")
                .attr("transform", "translate(" + (margin.left + realWidth / 2) + ",10)")
                .style("text-anchor", "middle")
                .text(label + " for " + sid);
        });

        selection.style("cursor", "pointer");

        selection.on("click", function () {
            var dialog = d3.select("#dialog-modal");
            dialog.text("");
            dialog.append('svg').data(selection.data()).call(chartBig);
            $("#dialog-modal").dialog("open");
        });
    }

    chart.margin = function (_) {
        if (!arguments.length) {
            return margin;
        }
        margin = _;
        return chart;
    };

    chart.width = function (_) {
        if (!arguments.length) {
            return width;
        }
        width = _;
        return chart;
    };

    chart.height = function (_) {
        if (!arguments.length) {
            return height;
        }
        height = _;
        return chart;
    };

    chart.x = function (_) {
        if (!arguments.length) {
            return xValue;
        }
        xValue = _;
        return chart;
    };

    chart.label = function (_) {
        if (!arguments.length) {
            return label;
        }
        label = _;
        return chart;
    };

    chart.sid = function (_) {
        if (!arguments.length) {
            return sid;
        }
        sid = _;
        return chart;
    };

    chart.label = function (_) {
        if (!arguments.length) {
            return label;
        }
        label = _;
        return chart;
    };

    chart.request = function (_) {
        if (!arguments.length) {
            return request;
        }
        request = _;
        return chart;
    };

    chart.metric = function (_) {
        if (!arguments.length) {
            return metric;
        }
        metric = _;
        return chart;
    };

    chart.intervalLength = function (_) {
        if (!arguments.length) {
            return intervalLength;
        }
        intervalLength = _;
        return chart;
    };

    return chart;
}

// draws chart for one metric and all servers of one provider
function stackedChart() {
    "use strict";
    var margin = {top: 20, right: 100, bottom: 30 + contextHeight + 30, left: 70},
        contextMargin = {top: dialogHeight - contextHeight - 30, right: 100, bottom: 30, left: 70},
        width = dialogWidth,
        height = dialogHeight,
        xScale = d3.time.scale(),
        yScale = d3.scale.linear(),
        xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
        yAxis = d3.svg.axis().scale(yScale).orient("left"),
        xScaleContext = d3.time.scale(),
        yScaleContext = d3.scale.linear(),
        xAxisContext = d3.svg.axis().scale(xScale).orient("bottom"),
        yAxisContext = d3.svg.axis().scale(yScaleContext).orient("left"),
        color = d3.scale.category20(),
        area = d3.svg.area()
            .x(function (d) { return xScale(d.date); })
            .y0(function (d) { return yScale(d.y0); })
            .y1(function (d) { return yScale(d.y0 + d.y); })
            .defined(function (d) {
                return !isNaN(d.y);
            }),
        areaContext = d3.svg.area()
            .x(function (d) { return xScaleContext(d.date); })
            .y0(function (d) { return yScaleContext(d.y0); })
            .y1(function (d) { return yScaleContext(d.y0 + d.y); })
            .defined(function (d) {
                return !isNaN(d.y);
            }),
        stack = d3.layout.stack()
            .offset("zero")
            .values(function (d) { return d.values; }),
        label = "",
        request,
        metric,
        intervalLength;


    function chartBig(selection) {
        selection.each(function (data) {
            var realWidth = width - margin.left - margin.right,
                realHeight = height - margin.top - margin.bottom,
                realWidthContext = width - contextMargin.left - contextMargin.right,
                realHeightContext = height - contextMargin.top - contextMargin.bottom;

            // x-scale.
            xScale
                .domain(d3.extent(data, function (d) {
                    return d.date;
                }))
                .range([0, width - margin.left - margin.right]);

            // y-scale.
            var domain = [0, d3.max(data, function (d) {
                    var sum = 0;
                    $.each(d.sids, function (key, value) {
                        sum += value;
                    });
                    return sum;
                })];
            if (domain[1] === 0) {
                domain[1] = 0.01;
            }
            yScale
                .domain(domain)
                .range([height - margin.top - margin.bottom, 0]);


            color.domain(d3.keys(data[0].sids));

            var sids = stack(color.domain().map(function (name) {
                return {
                    name: name,
                    values: data.map(function (d) {
                        return {
                            date: d.date,
                            y: d.sids[name]
                        };
                    })
                };
            }));


            // select svg element
            var svg = d3.select(this)
                .attr("width", width)
                .attr("height", height)
                .attr("class", "stacked-chart");

            svg.append("defs").append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", realWidth)
                .attr("height", realHeight);

            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            /* thresholds warning/critical background */
            var thresholdBackgrounds = {};

            var provider = "xlab";
            if (thresholds[provider] && thresholds[provider][metric]) {
                $.each(thresholds[provider][metric], function (key, value) {
                    thresholdBackgrounds[key] = g.append("path")
                        .attr("class", "threshold " + key)
                        .attr("clip-path", "url(#clip)");
                });
            }

            function moveThresholds(transition) {
                if (thresholds[provider] && thresholds[provider][metric]) {
                    var domain = yScale.domain(), gt, lt, y0, y1, height, object;

                    $.each(thresholds[provider][metric], function (key, value) {
                        gt = domain[0];
                        if (value.length > 0) {
                            gt = value[0] * sids.length;
                        }
                        lt = domain[1];
                        if (value.length > 1) {
                            lt = value[1] * sids.length;
                        }
                        y0 = yScale(gt);
                        y1 = yScale(lt);
                        if (transition) {
                            object = transition.select(".threshold." + key);
                        } else {
                            object = thresholdBackgrounds[key];
                        }
                        object.attr("d", "m 0," + y1 + " h " + realWidth + " V " + y0 + " h " + -realWidth + " z");
                    });
                }
            }
            moveThresholds();


            var sid = g.selectAll(".sid")
                .data(sids)
                .enter().append("g")
                .attr("class", "sid");

            sid.append("title")
                .text(function (d, i) {
                    return d.name;
                });

            var path = sid.append("path")
                .attr("clip-path", "url(#clip)")
                .attr("class", "area")
                .attr("d", function (d) {
                    return area(d.values);
                })
                .style("fill", function (d) {
                    return color(d.name);
                });


            // x-axis
            g.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + yScale.range()[0] + ")")
                .call(xAxis);

            // y-axis
            g.append("g").attr("class", "y axis")
                .call(yAxis);


            /* legend */
            var legend = svg.append("g")
                .attr("transform", "translate(" + (margin.left + realWidth) + "," + (margin.top) + ")");

            $.each(d3.keys(data[0].sids).reverse(), function (key, value) {
                var legendEntry = legend.append("g")
                    .attr("transform", "translate(5," + key * 20 + ")");
                legendEntry.append("circle")
                    .attr("cy", 4.5)
                    .attr("cx", 5)
                    .attr("r", 4.5)
                    .style("fill", color(value));

                // text overflow
                var svgSwitch = legendEntry.append("switch");
                svgSwitch.append("foreignObject")
                    .attr("width", 85)
                    .attr("height", 19)
                    .attr("x", 10)
                    .attr("y", -3)
                    .append("xhtml:div")
                    .attr("title", value)
                    .text(value);
                // fallback if foreignObject not supported
                svgSwitch.append("text")
                    .text(value)
                    .attr("x", 10)
                    .attr("y", 10);
                legendEntry.append("title")
                    .text(value);
            });

            /* label */
            svg.append("text")
                .attr("transform", "translate(15," + (realHeight / 2 + margin.top) + ") rotate(-90)")
                .style("text-anchor", "middle")
                .text(label);


            /* SMALL CONTEXT CHART */
            // x-scale
            xScaleContext
                .domain(d3.extent(data, function (d) {
                    return d.date;
                }))
                .range([0, width - contextMargin.left - contextMargin.right]);

            // y-scale.
            domain = [0, d3.max(data, function (d) {
                var sum = 0;
                $.each(d.sids, function (key, value) {
                    sum += value;
                });
                return sum;
            })];
            if (domain[1] === 0) {
                domain[1] = 0.01;
            }
            yScaleContext
                .domain(domain)
                .range([height - contextMargin.top - contextMargin.bottom, 0]);

            var context = svg.append("g")
                .attr("transform", "translate(" + contextMargin.left + "," + contextMargin.top + ")");

            var contextSid = context.selectAll(".sid")
                .data(sids)
                .enter().append("g")
                .attr("class", "sid");

            contextSid.append("path")
                .attr("class", "area")
                .attr("d", function (d) {
                    return areaContext(d.values);
                })
                .style("fill", function (d) {
                    return color(d.name);
                });

            // x-axis
            context.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + yScaleContext.range()[0] + ")")
                .call(xAxisContext);

            // y-axis
            context.append("g").attr("class", "y axis")
                .call(yAxisContext);


            /* loading icon */
            var loading = g.append("g")
                .attr("class", "loading")
                .attr("transform", "translate(" + realWidth / 2 + "," + realHeight / 2 + ")");
            loading.append("path")
                .attr("d", "m -40,0 15,0 0,0 -15,0 z")
                .attr("class", "led one");
            loading.append("path")
                .attr("d", "m -20,0 15,0 0,0 -15,0 z")
                .attr("class", "led two");
            loading.append("path")
                .attr("d", "m 0,0 15,0 0,0 -15,0 z")
                .attr("class", "led three");
            loading.append("path")
                .attr("d", "m 20,0 15,0 0,0 -15,0 z")
                .attr("class", "led four");
            loading.style("display", "none");


            /* brush */
            var brush = d3.svg.brush()
                .x(xScaleContext);

            var lastRequest,
                moved = false;

            function cancleLastRequest() {
                if (lastRequest) {
                    lastRequest.cancel();
                    lastRequest = null;
                    loading.style("display", "none");
                }
            }

            function brushstart() {
                console.warn("Brush START!");
                cancleLastRequest();

                var t = g.transition().duration(0);
                sid.data(sids);
                t.selectAll(".sid").select("path")
                    .attr("d", function (d) {
                        return area(d.values);
                    })
                    .style("fill", function (d) {
                        return color(d.name);
                    });
                t.select(".y.axis").call(yAxis);
                moveThresholds(t);

                yScale
                    .domain(yScaleContext.domain());
                t = t.transition().duration(transitionDuration);
                t.selectAll(".sid").select("path")
                    .attr("d", function (d) {
                        return area(d.values);
                    })
                    .style("fill", function (d) {
                        return color(d.name);
                    }); // redraw paths;
                t.select(".y.axis").call(yAxis);

                moveThresholds(t);
            }

            function brushmove() {
                console.warn("Brush MOVE!");
                cancleLastRequest();

                var t,
                    timeRange = brush.extent();
                if (Math.abs(timeRange[0] - timeRange[1]) < 10000) {
                    t = g.transition().duration(0);
                    xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                    yScale
                        .domain(domain);
                    t.select(".y.axis").call(yAxis);
                    sid.data(sids);
                    t.selectAll(".sid").select("path")
                        .attr("d", function (d) {
                            return area(d.values);
                        })
                        .style("fill", function (d) {
                            return color(d.name);
                        });
                    moveThresholds(t);
                    g.select(".x.axis").call(xAxis);
                    return;
                }

                t = g.transition().duration(0);
                t.selectAll(".sid").select("path");
                t.select(".y.axis").call(yAxis);

                moveThresholds(t);

                xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                yScale
                    .domain(domain);
                g.select(".y.axis").call(yAxis);
                sid.data(sids);
                sid.select("path")
                    .attr("d", function (d) {
                        return area(d.values);
                    })
                    .style("fill", function (d) {
                        return color(d.name);
                    }); // redraw paths
                g.select(".x.axis").call(xAxis);

                moved = true;

            }

            function brushend() {
                console.warn("Brush END!");

                function refreshChart(error, request, json) {
                    loading.style("display", "none");
                    var data = json,
                        sids = json.values;
                    $.each(request.metrics, function (key, value) {
                        data = parseOneMetricAllSids(json.intervals, sids, value);
                        var focusData = stack(color.domain().map(function (name) {
                            return {
                                name: name,
                                values: data.map(function (d) {
                                    return {
                                        date: d.date,
                                        y: d.sids[name]
                                    };
                                })
                            };
                        }));

                        sid.data(focusData);
                        sid.select("path")
                            .attr("d", function (d) {
                                return area(d.values);
                            })
                            .style("fill", function (d) {
                                return color(d.name);
                            }); // redraw paths
                        var domain = [0, d3.max(data, function (d) {
                            var sum = 0;
                            $.each(d.sids, function (key, value) {
                                sum += value;
                            });
                            return sum;
                        })];
                        if (domain[1] === 0) {
                            domain[1] = 0.01;
                        }
                        yScale
                            .domain(domain);

                        var t = g.transition().duration(transitionDuration);
                        t.selectAll(".sid").select("path")
                            .attr("d", function (d) {
                                return area(d.values);
                            })
                            .style("fill", function (d) {
                                return color(d.name);
                            });
                        t.select(".y.axis").call(yAxis);

                        moveThresholds(t);
                    });
                }

                if (request) {
                    cancleLastRequest();
                    var timeRange = brush.extent();

                    var t = g.transition().duration(0);
                    xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                    yScale
                        .domain(domain);
                    t.select(".y.axis").call(yAxis);
                    sid.data(sids);
                    t.selectAll(".sid").select("path")
                        .attr("d", function (d) {
                            return area(d.values);
                        })
                        .style("fill", function (d) {
                            return color(d.name);
                        });
                    moveThresholds(t);
                    g.select(".x.axis").call(xAxis);

                    if (Math.abs(timeRange[0] - timeRange[1]) < 10000) {
                        t = g.transition().duration(0);
                        xScale.domain(brush.empty() ? xScaleContext.domain() : brush.extent());
                        yScale
                            .domain(domain);
                        t.select(".y.axis").call(yAxis);
                        sid.data(sids);
                        t.selectAll(".sid").select("path")
                            .attr("d", function (d) {
                                return area(d.values);
                            })
                            .style("fill", function (d) {
                                return color(d.name);
                            });
                        moveThresholds(t);
                        g.select(".x.axis").call(xAxis);
                        return;
                    }

                    loading.style("display", null);

                    request.metrics = [metric];
                    request.startTime = format(timeRange[0]);
                    request.endTime = format(new Date(+timeRange[1] + 1000));
                    console.warn(+timeRange[0]);
                    console.warn(request);
                    lastRequest = loadReportData(request, refreshChart);
                }

                moved = false;
            }

            brush
                .on("brushstart", brushstart)
                .on("brush", brushmove)
                .on("brushend", brushend);

            context.append("g")
                .attr("class", "x brush")
                .call(brush)
                .selectAll("rect")
                .attr("height", realHeightContext);
        });
    }

    function chart(selection) {
        selection.each(function (data) {

            var margin = {top: 20, right: 100, bottom: 30, left: 70},
                width = 400,
                height = 200,
                xScale = d3.time.scale(),
                yScale = d3.scale.linear(),
                xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
                yAxis = d3.svg.axis().scale(yScale).orient("left"),
                color = d3.scale.category20(),
                area = d3.svg.area()
                    .x(function (d) {
                        return xScale(d.date);
                    })
                    .y0(function (d) {
                        return yScale(d.y0);
                    })
                    .y1(function (d) {
                        return yScale(d.y0 + d.y);
                    })
                    .defined(function (d) {
                        return !isNaN(d.y);
                    }),
                stack = d3.layout.stack()
                    .offset("zero")
                    .values(function (d) {
                        return d.values;
                    });

            var realWidth = width - margin.left - margin.right,
                realHeight = height - margin.top - margin.bottom,
                realWidthContext = width - contextMargin.left - contextMargin.right,
                realHeightContext = height - contextMargin.top - contextMargin.bottom;

            // number of ticks
            xAxis.ticks(4);
            yAxis.ticks(5);

            // x-scale
            xScale
                .domain(d3.extent(data, function (d) {
                    return d.date;
                }))
                .range([0, width - margin.left - margin.right]);

            // y-scale
            var domain = [0, d3.max(data, function (d) {
                    var sum = 0;
                    $.each(d.sids, function (key, value) {
                        sum += value;
                    });
                    return sum;
                })];
            if (domain[1] === 0) {
                domain[1] = 0.01;
            }
            yScale
                .domain(domain)
                .range([height - margin.top - margin.bottom, 0]);


            color.domain(d3.keys(data[0].sids));

            var sids = stack(color.domain().map(function (name) {
                return {
                    name: name,
                    values: data.map(function (d) {
                        return {
                            date: d.date,
                            y: d.sids[name]
                        };
                    })
                };
            }));


            var svg = d3.select(this)
                .attr("width", width)
                .attr("height", height)
                .attr("class", "stacked-chart");

            svg.append("defs").append("clipPath")
                .attr("id", "clip-preview")
                .append("rect")
                .attr("width", realWidth)
                .attr("height", realHeight);

            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            /* thresholds warning/critical background */
            var thresholdBackgrounds = {};

            var provider = "xlab";
            if (thresholds[provider] && thresholds[provider][metric]) {
                $.each(thresholds[provider][metric], function (key, value) {
                    thresholdBackgrounds[key] = g.append("path")
                        .attr("class", "threshold " + key)
                        .attr("clip-path", "url(#clip-preview)");
                });
            }

            function moveThresholds(transition) {
                if (thresholds[provider] && thresholds[provider][metric]) {
                    var domain = yScale.domain(), gt, lt, y0, y1, height, object;

                    $.each(thresholds[provider][metric], function (key, value) {
                        gt = domain[0];
                        if (value.length > 0) {
                            gt = value[0];
                        }
                        lt = domain[1];
                        if (value.length > 1) {
                            lt = value[1];
                        }
                        y0 = yScale(gt);
                        y1 = yScale(lt);
                        if (transition) {
                            object = transition.select(".threshold." + key);
                        } else {
                            object = thresholdBackgrounds[key];
                        }
                        object.attr("d", "m 0," + y1 + " h " + realWidth + " V " + y0 + " h " + -realWidth + " z");
                    });
                }
            }
            moveThresholds();


            var sid = g.selectAll(".sid")
                .data(sids)
                .enter().append("g")
                .attr("class", "sid");

            sid.append("title")
                .text(function (d, i) {
                    return d.name;
                });

            var path = sid.append("path")
                .attr("class", "area")
                .attr("d", function (d) {
                    return area(d.values);
                })
                .style("fill", function (d) {
                    return color(d.name);
                });


            // x-axis
            g.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + yScale.range()[0] + ")")
                .call(xAxis);

            // y-axis
            g.append("g").attr("class", "y axis")
                .call(yAxis);


            /* legend */
            var legend = svg.append("g")
                .attr("transform", "translate(" + (margin.left + realWidth) + "," + (margin.top) + ")");

            $.each(d3.keys(data[0].sids).reverse(), function (key, value) {
                var legendEntry = legend.append("g")
                    .attr("transform", "translate(5," + key * 20 + ")");
                legendEntry.append("circle")
                    .attr("cy", 4.5)
                    .attr("cx", 5)
                    .attr("r", 4.5)
                    .style("fill", color(value));

                // text overflow
                var svgSwitch = legendEntry.append("switch");
                svgSwitch.append("foreignObject")
                    .attr("width", 85)
                    .attr("height", 19)
                    .attr("x", 10)
                    .attr("y", -3)
                    .append("xhtml:div")
                    .attr("title", value)
                    .text(value);
                // fallback if foreignObject not supported
                svgSwitch.append("text")
                    .text(value)
                    .attr("x", 10)
                    .attr("y", 10);
                legendEntry.append("title")
                    .text(value);
            });

            /* label */
            svg.append("text")
                .attr("transform", "translate(15," + (realHeight / 2 + margin.top) + ") rotate(-90)")
                .style("text-anchor", "middle")
                .text(label);
        });

        selection.style("cursor", "pointer");

        selection.on("click", function () {
            var dialog = d3.select("#dialog-modal");
            dialog.text("");
            dialog.append('svg').data(selection.data()).call(chartBig);
            $("#dialog-modal").dialog("open");
        });
    }

    chart.margin = function (_) {
        if (!arguments.length) {
            return margin;
        }
        margin = _;
        return chart;
    };

    chart.width = function (_) {
        if (!arguments.length) {
            return width;
        }
        width = _;
        return chart;
    };

    chart.height = function (_) {
        if (!arguments.length) {
            return height;
        }
        height = _;
        return chart;
    };

    chart.label = function (_) {
        if (!arguments.length) {
            return label;
        }
        label = _;
        return chart;
    };

    chart.request = function (_) {
        if (!arguments.length) {
            return request;
        }
        request = _;
        return chart;
    };

    chart.metric = function (_) {
        if (!arguments.length) {
            return metric;
        }
        metric = _;
        return chart;
    };

    chart.intervalLength = function (_) {
        if (!arguments.length) {
            return intervalLength;
        }
        intervalLength = _;
        return chart;
    };

    return chart;
}

// draws chart for one metric and all servers/providers
function sunburstChart() {
    "use strict";
    var margin = {top: 20, right: 20, bottom: 30, left: 50},
        width = dialogWidth,
        height = dialogHeight,
        label = "";


    function chartBig(selection, root) {
        var realWidth = width - margin.left - margin.right,
            realHeight = height - margin.top - margin.bottom,
            radius = Math.min(realWidth, realHeight) / 2,
            xScale = d3.scale.linear()
                .range([0, 2 * Math.PI]),
            yScale = d3.scale.sqrt()
                .range([0, radius]),
            color = d3.scale.category20(),
            svg = selection.attr("width", width)
                .attr("height", height)
                .attr("class", "sunburst-chart"),
            gContainer = svg.append("g")
                .attr("transform", "translate(" + (realWidth / 2 + margin.left) + "," + (realHeight / 2 + margin.top) + ")"),
            partition = d3.layout.partition()
                .value(function (d) {
                    return d.size;
                }),
            arc = d3.svg.arc()
                .startAngle(function (d) {
                    return Math.max(0, Math.min(2 * Math.PI, xScale(d.x)));
                })
                .endAngle(function (d) {
                    return Math.max(0, Math.min(2 * Math.PI, xScale(d.x + d.dx)));
                })
                .innerRadius(function (d) {
                    return Math.max(0, yScale(d.y));
                })
                .outerRadius(function (d) {
                    return Math.max(0, yScale(d.y + d.dy));
                }),
            g = gContainer.selectAll("g")
                .data(partition.nodes(root))
                .enter().append("g"),
            path = g.append("path")
                .attr("d", arc)
                .style("fill", function (d) {
                    if (!d.children) {
                        var colorScale = d3.scale.linear()
                            .domain([0, d.parent.children.length - 1])
                            .range([
                                d3.rgb(color(d.parent.name)).brighter(0.7).toString(),
                                d3.rgb(color(d.parent.name)).darker(0.6).toString()
                            ]);
                        var retColor = "grey";
                        $.each(d.parent.children, function (key, value) {
                            if (value.name === d.name) {
                                retColor = colorScale(key);
                            }
                        });
                        return retColor;
                    }
                    return color(d.name);
                }),
            text = g.append("text")
                .attr("class", "hoverLabel")
                .attr("transform", function (d) {
                    return "translate(" + arc.centroid(d) + ")";
                })
                .attr("dy", ".35em")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.name;
                });

        var tooltip = d3.select(".tooltip");

        var mouseover = function (d) {
            tooltip.transition()
                .duration(250)
                .style("opacity", 1);
            var self = d3.select(this);
            var color = self.attr("data-my-true-color");
            if (!color) {
                color = self.style("fill");
                self.attr("data-my-true-color", color);
            }
            color = d3.rgb(color).brighter(0.2).toString();
            self.style("fill", color);
        };

        var mousemove = function (d) {
            tooltip
                .html("")
                .style("left", (d3.event.pageX + 34) + "px")
                .style("top", (d3.event.pageY - 35) + "px");

            tooltip.append("p").text(d.name);
        };

        var mouseout = function (d) {
            tooltip.transition()
                .duration(250)
                .style("opacity", 0);
            var self = d3.select(this);
            self.style("fill", self.attr("data-my-true-color"));
        };

        path
            .on("mouseover", mouseover)
            .on("mousemove", mousemove)
            .on("mouseout", mouseout);


        /* label */
        svg.append("text")
            .attr("transform", "translate(15,15) rotate(-90)")
            .style("text-anchor", "end")
            .text(label);
    }


    function chart(selection, root) {
        var width = 400,
            height = 300;

        var realWidth = width - margin.left - margin.right,
            realHeight = height - margin.top - margin.bottom,
            radius = Math.min(realWidth, realHeight) / 2,
            xScale = d3.scale.linear()
                .range([0, 2 * Math.PI]),
            yScale = d3.scale.sqrt()
                .range([0, radius]),
            color = d3.scale.category20(),
            svg = selection.attr("width", width)
                .attr("height", height)
                .attr("class", "sunburst-chart"),
            gContainer = svg.append("g")
                .attr("transform", "translate(" + (realWidth / 2 + margin.left) + "," + (realHeight / 2 + margin.top) + ")"),
            partition = d3.layout.partition()
                .value(function (d) {
                    return d.size;
                }),
            arc = d3.svg.arc()
                .startAngle(function (d) {
                    return Math.max(0, Math.min(2 * Math.PI, xScale(d.x)));
                })
                .endAngle(function (d) {
                    return Math.max(0, Math.min(2 * Math.PI, xScale(d.x + d.dx)));
                })
                .innerRadius(function (d) {
                    return Math.max(0, yScale(d.y));
                })
                .outerRadius(function (d) {
                    return Math.max(0, yScale(d.y + d.dy));
                }),
            g = gContainer.selectAll("g")
                .data(partition.nodes(root))
                .enter().append("g"),
            path = g.append("path")
                .attr("d", arc)
                .style("fill", function (d) {
                    if (!d.children) {
                        var colorScale = d3.scale.linear()
                            .domain([0, d.parent.children.length - 1])
                            .range([
                                d3.rgb(color(d.parent.name)).brighter(0.7).toString(),
                                d3.rgb(color(d.parent.name)).darker(0.6).toString()
                            ]);
                        var retColor = "grey";
                        $.each(d.parent.children, function (key, value) {
                            if (value.name === d.name) {
                                retColor = colorScale(key);
                            }
                        });
                        return retColor;
                    }
                    return color(d.name);
                }),
            text = g.append("text")
                .attr("class", "hoverLabel")
                .attr("transform", function (d) {
                    return "translate(" + arc.centroid(d) + ")";
                })
                .attr("dy", ".35em")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.name;
                });

        /* label */
        svg.append("text")
            .attr("transform", "translate(15,15) rotate(-90)")
            .style("text-anchor", "end")
            .text(label);

        selection.style("cursor", "pointer");

        selection.on("click", function () {
            var dialog = d3.select("#dialog-modal");
            dialog.text("");
            dialog.append('svg').data(selection.data()).call(chartBig, root);
            $("#dialog-modal").dialog("open");
        });
    }

    chart.margin = function (_) {
        if (!arguments.length) {
            return margin;
        }
        margin = _;
        return chart;
    };

    chart.width = function (_) {
        if (!arguments.length) {
            return width;
        }
        width = _;
        return chart;
    };

    chart.height = function (_) {
        if (!arguments.length) {
            return height;
        }
        height = _;
        return chart;
    };

    chart.label = function (_) {
        if (!arguments.length) {
            return label;
        }
        label = _;
        return chart;
    };

    return chart;
}


var singleMetric = d3.select('#single-metric-charts'),
    stacked = d3.select('#stacked-charts'),
    sunburst = d3.select('#sunburst-charts'),
    lastRequest,
    sunburstData,
    sunburstProviderCounter;


// draws all charts for one provider
function drawCharts(error, request, json) {
    "use strict";
    if (error) {
        $.jGrowl("Error getting JSON data!");
        $('#ajax-loader').hide();
        console.error(error);
        return;
    }
    var intervals = json.intervals;

    var i;
    for (i = 0; i < intervals.length; i += 1) {
        intervals[i] = new Date(intervals[i]);
    }


    var data, sid, metric, metrics,
        sids = json.values;

    $.each(sids, function (key, value) {
        sid = key;
        metrics = value.metricsData;
        $.each(metrics, function (key, value) {
            data = [];
            for (i = 0; i < value.length; i += 1) {
                if (value[i] === null) {
                    data.push([intervals[i]].concat([NaN, NaN, NaN]));
                } else {
                    data.push([intervals[i]].concat(value[i]));
                }
            }
            data = [data];
            var chart = singleMetricChart().label(window.metrics[key]).sid(sid).request(request).metric(key).intervalLength(json.intervalLength);
            singleMetric.append('svg').data(data).call(chart);
        });
    });

    // one metric all sids
    var obj, index;
    $.each(request.metrics, function (key, value) {
        metric = value;
        data = parseOneMetricAllSids(intervals, sids, value);
        var chart = stackedChart().label(window.metrics[metric]).request(request).metric(metric).intervalLength(json.intervalLength);
        stacked.append('svg').datum(data).call(chart);
    });


    /* parse sunburst data */
    $.each(request.metrics, function (key, metric) {
        var sidsData = [];
        sunburstData[metric].children.push({
            name: request.providerUuid,
            children: sidsData
        });

        $.each(sids, function (key, value) {
            var size;
            if (value.metricsData[metric].slice(-1)[0] !== null) {
                size = value.metricsData[metric].slice(-1)[0][1]; // 1 = average
            } else {
                size = 0;
            }
            sidsData.push({
                name: key,
                size: size
            });
        });
    });

    sunburstProviderCounter -= 1;
    if (sunburstProviderCounter === 0) { // show sunburst charts when data of all providers is loaded
        $.each(sunburstData, function (key, value) {
            var chart = sunburstChart().label(window.metrics[key]);
            sunburst.append('svg').call(chart, value);
        });
        // all charts drawn... hide pacman
        $('#ajax-loader').hide();
    }


}


// sends all requests for data
function getMetricsAndDrawGraphs() {
    "use strict";
    var startTime = $('#from').datetimepicker('getDate'),
        endTime = $('#to').datetimepicker('getDate'),
        req = {
            metrics: getSelectedMetrics(),
            numberOfIntervals: Math.floor(dialogWidth / 5)
        },
        providers = $('#providers > div').has('.server :checkbox:checked');

    if (lastRequest) {
        $.each(lastRequest, function (key, value) {
            value.cancel();
        });
    }

    singleMetric.html("");
    stacked.html("");
    sunburst.html("");

    if (startTime === null || endTime === null) {
        $.jGrowl("Select time range.");
        return;
    }

    if (req.metrics.length === 0) {
        $.jGrowl("Select at least one metric.");
        return;
    }

    req.startTime = format(startTime);
    req.endTime = format(endTime);

    if (providers.length > 0) {
        lastRequest = [];
        sunburstData = {};
        sunburstProviderCounter = providers.length;

        $.each(req.metrics, function (key, value) {
            sunburstData[value] = {
                name: value,
                children: []
            };
        });
        providers.each(function (index, element) {
            req = JSON.parse(JSON.stringify(req));
            req.providerUuid = providerUris[$('.provider label', element).text()];
            req.sids = $('.server :checkbox:checked', element).map(function (index, element) {
                return this.id;
            }).get();
            lastRequest.push(loadReportData(req, drawCharts));
        });

        $('#ajax-loader').show();
    } else {
        $.jGrowl("Select at least one server.");
        return;
    }
}


var metrics = {
    "cpu.cores": "CPU number of cores",
    "cpu.speed": "CPU speed",
    "cpu.user": "CPU user",
    "cpu.system": "CPU system",
    "cpu.idle": "CPU idle",
    "cpu.load_one": "CPU load_one",
    "cpu.load_five": "CPU load_five",
    "cpu.load_one_norm": "CPU load_one_norm",
    "cpu.load_five_norm": "CPU load_five_norm",
    "memory.total": "memory total",
    "memory.free": "memory free",
    "memory.used": "memory used",
    "disk.available": "disk available",
    "disk.used": "disk used",
    "network.rx": "network rx",
    "network.tx": "network tx"
};

var defaultCheckedMetrics = [
    "cpu.load_one",
    "cpu.load_five"
];

function showMetricCheckboxes(metrics, checkedMetrics) {
    "use strict";
    if (checkedMetrics === undefined) {
        checkedMetrics = [];
    }

    var m,
        div,
        checkbox,
        checkboxContainer = $("#metrics", $("#abc"));

    $.each(metrics, function (key, value) {
        div = $('<div />');
        checkbox = $('<input type="checkbox" />').attr("id", key);
        div.append(checkbox);
        div.append($('<label />').attr("for", key).text(value));
        if ($.inArray(key, checkedMetrics) !== -1) {
            checkbox.attr("checked", "checked");
        }
        checkbox.change(getMetricsAndDrawGraphs);
        checkboxContainer.append(div);
    });
}

function showProviderCheckboxes() {
    "use strict";
    d3.json(urlToProxy(active_driver['feder-api-uri'] + "/providers"), function (error, json) {
        if (error) {
            $.jGrowl("Error getting JSON data!");
            console.error(error);
            return;
        }

        var providers = $("#providers"),
            div,
            checkbox;

        providers.html("");

        providers.append($("<h3 />").text("Providers:"));

        $.each(json, function (key, value) {
			console.warn(value);
			providerUris[value.name] = value.uri.split('/')[2];
            var provider = $("<div />");
            div = $('<div />').addClass("provider");
            checkbox = $('<input type="checkbox" />').attr("id", value.name);
            div.append(checkbox);
            div.append($('<label />').attr("for", value.name).text(value.name));
            provider.append(div);

            // load thresholds
            var providerThresholds = {};
            thresholds[value.name] = providerThresholds;
            d3.json(urlToProxy(active_driver['feder-acc-uri'] + "/metrics_thresholds" + value.uri), function (error, json) {
                if (error) {
                    console.error(error);
                    return;
                }
                $.each(json, function (key, value) {
                    var metricThresholds = {};
                    providerThresholds[key] = metricThresholds;
                    $.each(value, function (key, value) {
                        metricThresholds[key] = [];
                        if (value.GT !== undefined) {
                            metricThresholds[key] = [value.GT];
                        } else if (value.AND !== undefined) {
                            metricThresholds[key] = [value.AND.GT, value.AND.LTE];
                        }
                    });
                });
            });

            d3.json(urlToProxy(active_driver['feder-api-uri'] + value.uri), function (error, json) {
                if (error) {
                    $.jGrowl("Error getting JSON data!");
                    console.error(error);
                    return;
                }

                d3.json(urlToProxy(active_driver['feder-api-uri'] + json.servers), function (error, json) {
                    if (error) {
                        $.jGrowl("Error getting JSON data!");
                        console.error(error);
                        return;
                    }

                    $.each(json, function (key, value) {
                        div = $('<div />').addClass("server");
                        checkbox = $('<input type="checkbox" />').attr("id", value.name);
                        div.append(checkbox);
                        div.append($('<label />').attr("for", value.name).text(value.name));
                        provider.append(div);
                    });

                    $('.server input[type=checkbox]', provider).change(function () {
                        $('.provider input[type=checkbox]', provider).attr("checked",
                            $('.server input[type=checkbox]:not(:checked)', provider).length === 0);
                    });
                });
            });
            checkbox.change(function () {
                $('.server input[type=checkbox]', provider).attr("checked", this.checked);
            });

            providers.append(provider);
        });
    });
}


$(function () {
    "use strict";
    $('#ajax-loader').hide();
    d3.select(".tooltip").style("opacity", 0);

    var abc = $("#abc"),
        dateTime = $("#datetime", abc),
        dateTimePickerFrom = $("#from", dateTime),
        dateTimePickerTo = $("#to", dateTime),
        now = function () {
            return new Date();
        };


    // initializes datetimepickers
    dateTimePickerFrom.datetimepicker({
        changeMonth: true,
        numberOfMonths: 3,
        dateFormat: 'yy-mm-dd',
        separator: 'T',
        timeFormat: 'HH:mm:ssz',
        firstDay: 1,
        onClose: function (selectedDate) {
            dateTimePickerTo.datetimepicker("option", "minDate", selectedDate);
        }
    });
    dateTimePickerTo.datetimepicker({
        changeMonth: true,
        numberOfMonths: 3,
        dateFormat: 'yy-mm-dd',
        separator: 'T',
        timeFormat: 'HH:mm:ssz',
        firstDay: 1,
        onClose: function (selectedDate) {
            dateTimePickerFrom.datetimepicker("option", "maxDate", selectedDate);
        }
    });

    dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (8 * 60 * 60 * 1000)));
    dateTimePickerTo.datetimepicker('setDate', now());

    dialogHeight = $(window).height() * 0.8;
    dialogWidth = $(window).width() * 0.8;

    $("#dialog-modal").dialog({
        modal: true,
        resizable: false,
        autoOpen: false,
        height: dialogHeight + 60,
        width: dialogWidth + 30,
        close: function (event, ui) {
            $("#dialog-modal").text("");
            d3.select(".tooltip")
                .transition()
                .duration(0)
                .style("opacity", 0);
        }
    });

    // buttons for common time intervals selection
    dateTime.append($('<input type="button" value="last 5 minutes" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (5 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));
    dateTime.append($('<input type="button" value="last 15 minutes" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (15 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));
    dateTime.append($('<input type="button" value="last hour" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (60 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));
    dateTime.append($('<input type="button" value="last 8 hours" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (8 * 60 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));
    dateTime.append($('<input type="button" value="last 24 hours" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (24 * 60 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));
    dateTime.append($('<input type="button" value="last 30 days" />').click(function () {
        $.jGrowl("Click!");
        dateTimePickerFrom.datetimepicker('setDate', new Date(now().getTime() - (30 * 24 * 60 * 60 * 1000)));
        dateTimePickerTo.datetimepicker('setDate', now());
        getMetricsAndDrawGraphs();
    }));


    showMetricCheckboxes(metrics, defaultCheckedMetrics);

    showProviderCheckboxes();


    abc.append($('<input type="button" value="Show" />').click(function () {
        $.jGrowl("Click!");
        getMetricsAndDrawGraphs();
    }));
});
