window.Fed =
  invoke: (method, params, success, error) ->
    CRpc.call(RPC_BASE, method, success, params, error)

window.UnitFormat =
  size: (bytes, postxt) ->
    postxt or= 0
    units = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    
    return 'n/a' if bytes == 0
    
    while bytes >= 1024
      postxt++
      bytes /= 1024
    
    bytes.toFixed(1) + ' ' + units[postxt]
  
  cpu: (speed) ->
    units = ['MHz', 'GHz']
    
    return 'n/a' if speed == 0
    
    postxt = 0
    
    while speed >= 1000
      postxt++
      speed /= 1000
    
    speed.toFixed(1) + ' ' + units[postxt]

lt = (val) -> ['<', val]
lte = (val) -> ['<=', val]
gt = (val) -> ['>', val]
gte = (val) -> ['>=', val]

ko.bindingHandlers.slider =
  init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    options = valueAccessor()
    
    $(element).slider
      value: options.value()
      min: options.min
      max: options.max
      step: options.step
      slide: (event, ui) ->
        options.value(ui.value); return

ko.bindingHandlers.draggable =
  init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    options = valueAccessor()
    
    $(element)
      .data('draggable-data', options.data)
      .draggable
        revert: 'invalid'
        helper: 'clone'
        cursor: 'move'
        appendTo: 'body'
        start: (event, ui) ->
          ui.helper.addClass('form-el-draggable'); return

ko.bindingHandlers.droppable =
  init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    options = valueAccessor()
    $(element).droppable
      accept: options.accept
      activeClass: 'activated'
      hoverClass: 'over'
      drop: (event, ui) ->
        options.data(ui.draggable.data('draggable-data'))

ko.bindingHandlers.xmlmirror =
  init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    options = valueAccessor()
    valAccessor = options.value
    visibleAccessor = options.visible
    $el = $(element)
    
    cm = null
    
    if visibleAccessor?
      valAccessor.subscribe (newval) ->
        $el.val(newval)
    
      visibleAccessor.subscribe (newval) ->
        if newval
          cm = CodeMirror.fromTextArea(element,
            mode:
              name: 'xml'
            lineNumbers: true
          )
          setTimeout ->
            cm.refresh()
          , 500
        else
          cm.toTextArea()
          valAccessor($el.val())
    else
      $el.val(valAccessor())
      
      cm = CodeMirror.fromTextArea element,
        mode:
          name: 'xml'
        lineNumbers: true,
        onChange: ->
          valAccessor cm.getValue()
      
      valAccessor.subscribe (newval) ->
        cm.setValue newval

ko.bindingHandlers.bootmodal =
  init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    options = valueAccessor()
    showAccessor = options.visible
    visibleAccessor = options.visible
    $el = $(element)
    
    $el.on 'shown', ->
      visibleAccessor(yes)
    
    $el.on 'hidden', ->
      showAccessor(no)
      visibleAccessor(no)
    
    showAccessor.subscribe (val) ->
      if val
        $(element).modal('show')
        
ko.bindingHandlers.editable =
  update: (element, valueAccessor, allBindingsAccessor, viewModel) ->
    $(element).editable (value, settings) ->
      # TODO: Enable this after release 1.1
      #valueAccessor().value = value
      #value
      valueAccessor().value

class SlaNegotiateVm
  constructor: (data) ->
    self = this
    
    data or= {}
    
    @providers = ko.observableArray()
    @slats = ko.observableArray()
    
    @provider = ko.observableArray()
    @slat = ko.observableArray()
    @name = ko.observable(data.name)
    @content = ko.observable(data.content)
    
    @filters =
      ramTotalPow: ko.observable(Math.log(data.filters.ramTotal) / Math.log(2))
      ramFreePow: ko.observable(Math.log(data.filters.ramFree) / Math.log(2))
      cpuCores: ko.observable(data.filters.cpuCores)
      cpuSpeed: ko.observable(data.filters.cpuSpeed)
      cpuLoadOne: ko.observable(data.filters.cpuLoadOne)
      cpuLoadFive: ko.observable(data.filters.cpuLoadFive)
    
    @filters = _.extend @filters,
      ramTotal: ko.computed ->
        Math.pow 2, self.filters.ramTotalPow()
      , @
      ramFree: ko.computed ->
        Math.pow 2, self.filters.ramFreePow()
      , @
    
    @nameHasFocus = ko.observable(yes)
    
    ko.computed(->
      filters =
        ramTotal: gte self.filters.ramTotal()
        ramFree: gte self.filters.ramFree()
        cpuCores: gte self.filters.cpuCores()
        cpuSpeed: gte self.filters.cpuSpeed()
        cpuLoadOne: lte self.filters.cpuLoadOne()
        cpuLoadFive: lte self.filters.cpuLoadFive()
      
      Fed.invoke 'providers_list_servers', filters, (res) ->
        self.providers(res)
    , @).extend(throttle: 250)

class SlaVm
  constructor: (data) ->
    data or= {}
    @id = ko.observable data.id
    @name = ko.observable data.name
    @content = ko.observable data.content
    
class SlaOffer
  constructor: (data) ->
    self = this
    
    data or= {}
    
    #terms = _.map data.agreementTerms, (termVal, termId) ->
    #  id: termId
    #  guarantees: _.map(termVal.guarantees, (guaranteeVal, guaranteeId) ->
    #    id: guaranteeId
    #    guarantee: guaranteeVal
    #  )
      
    @agreementTerms = ko.observableArray(data.agreementTerms)
    @isOfferEdited = ko.observable(false)
    
    
class ApplicationRegisterVm
  constructor: (data) ->
    self = this
    
    data or= {}
    
    @slas = ko.observableArray()
    @name = ko.observable(data.name)
    @sla = ko.observable(data.sla or {})
    
    @nameHasFocus = ko.observable(yes)
    
    @slaPreviewShow = ko.observable(no)
    @slaPreviewVisible = ko.observable(no)
    @slaPreviewContent = ko.observable()
    
    @slaPreview = ->
      Fed.invoke 'slas_detail', id: self.sla().id, (res) ->
        xml = vkbeautify.xml(res.content, 2);
        self.slaPreviewContent xml
        self.slaPreviewVisible yes
    
    Fed.invoke 'slas_list', null, (res) ->
      self.slas(res)

class ApplicationVm
  constructor: (data) ->
    data or= {}
    
    @id = data.id
    @name = data.name
    @state = ko.observable(data.state or 'registered')
    @msg = data.msg
    @provider = data.provider 
    @sla = data.sla
    @vms = ko.observableArray(data.vms)
    @log = ko.observableArray([
      time: (new Date).toUTCString()
      msg: 'Application was successfully registered on provider.'
    ])

    @stateBoxStyle = ko.computed(->
      switch @state()
        when 'registered'
          ''
        when 'deployed'
          'alert-success'
        when 'error'
          'alert-error'
        when 'stopped'
          'alert-error'
    , this)
    
    @stateLabelStyle = ko.computed(->
      switch @state()
        when 'registered'
          'label label-info'
        when 'deployed'
          'label label-success'
        when 'error'
          'label label-important'
        when 'stopped'
          'label label-important'
    , this)

  deploy: ->
    @state 'deployed'
    @log.push
      time: '2012/02/27 18:40:02'
      msg: 'Application was successfully deployed.'
    return
 
  stop: ->
    @state 'stopped'
    @log.push
      time: '2012/02/27 18:40:02'
      msg: 'Application was successfully stopped.'
    return

class SlaCreateVm
  constructor: (data) ->
    self = this
    
    data or= {}
    
    @slats = ko.observableArray(data.slats)
    @slat = ko.observableArray(data.slat)
    @name = ko.observable(data.name)
    @sla = ko.observableArray(data.name)
    
    @nameHasFocus = ko.observable(yes)

formatDate = (date) ->
  dateStamp = [date.getFullYear(), (date.getMonth() + 1), date.getDate()].join("/")
  hourStamp = [date.getHours(), date.getMinutes(), date.getSeconds()].join(":")
  timeStamp = [dateStamp, hourStamp].join("; ")

  RE_findSingleDigits = /\b(\d)\b/g

  # Places a `0` in front of single digit numbers.
  timeStamp = timeStamp.replace( RE_findSingleDigits, "0$1" )
  timeStamp.replace /\s/g, ""

class Alert
  constructor: (parent, type, msg) ->
    self = this
    date = formatDate(new Date())
    @type = type
    @msg = date + "    "+msg
    @close = ->
      parent.remove self

federApp = Sammy('#main', ->
  base = this
  baseEl = @$element()[0]
  
  @raise_errors = yes
  
  @alerts = ko.observableArray()
  
  ko.applyBindings
    alerts: @alerts
  , $('#alerts')[0]
  
  @alert = (type, msg) ->
    base.alerts.push new Alert(base.alerts, type, msg)
  
  @growl = (msg) ->
    $.jGrowl(msg)
  
  @bindvm = (vm) ->
    ko.applyBindings vm, baseEl
  
  @bind 'run-route', ->
    $(document).scrollTop 0
  
  if hookbox?
    @hb = hookbox.connect(HOOKBOX_BASE)
    
    @hb.subscribe(HOOKBOX_CHANNEL)
    
    @hb.onSubscribed = (name, subscription) ->
      subscription.onPublish = (res) ->
        console.log('publish', arguments)
        
        if res.payload.status
          base.growl(res.payload.status)
      
      subscription.onSubscribe = ->
        console.log('subscribe', arguments)
      
      subscription.onUnsubscribe = ->
        console.log('unsubscribe', arguments)
  
  @get '#/', (ctx) ->
    @render('user/tpl/dashboard.html')
      .swap()
      .then ->
        vm =
          slas: ko.observableArray()
          apps: ko.observableArray()
        
        Fed.invoke 'applications_list', null, (res) ->
          vm.apps(res)
        
        Fed.invoke 'slas_list', null, (res) ->
          vm.slas(res)
        
        base.bindvm vm

  @get '#/slats/federation', (ctx) ->
    @render('user/tpl/federation-slats-details.html')
      .swap()
      .then ->
        feder_provider = null
        slaNeg = new SlaNegotiateVm
          filters:
            ramTotal: 1024
            ramFree: 512
            cpuCores: 1
            cpuSpeed: 1000
            cpuLoadOne: 0.80
            cpuLoadFive: 0.80

        Fed.invoke 'get_federation_provider', null, (provider) ->
          #feder_provider = provider.id
          console.log('provider ', provider)
          console.log('providerId ', provider.id)
          slaNeg.provider provider
          Fed.invoke 'slats_list', providerId:  slaNeg.provider().id, (res) ->
            console.log('calling slats_list with ', slaNeg.provider().id)
            slaNeg.slats(res)

        slaNeg.selectSlat = (slat) ->
          Fed.invoke 'slats_detail',
            providerId: slaNeg.provider().id
            slatId: slat.id
          , (res) ->
            slaNeg.slat slat
            slaNeg.content res.content

        slaNeg.create_user_slat = ->
          data =
            slatId: slaNeg.slat().id,
            name: slaNeg.name(),
            content: slaNeg.content()

          base.growl("Saving user SLAT...")
          Fed.invoke 'create_user_slat', data, (res) ->
            if res.success
              base.alert 'success', sprintf("User SLAT '%s' has been saved successfully.", slaNeg.name())
              ctx.redirect '#/'
            else
              base.alert 'error', 'Failed to save user SLAT.'

        slaNeg.cancel = ->
            ctx.redirect '#/'

        base.bindvm slaNeg

  @get '#/slas/negotiate', (ctx) ->
    @render('user/tpl/slas-negotiate.html')
      .swap()
      .then ->
        slaNeg = new SlaNegotiateVm
          filters:
            ramTotal: 1024
            ramFree: 512
            cpuCores: 1
            cpuSpeed: 1000
            cpuLoadOne: 0.80
            cpuLoadFive: 0.80
        
        slaNeg.selectProvider = (provider) ->
          Fed.invoke 'slats_list', providerId: provider.id, (res) ->
            slaNeg.slats(res)
          
          slaNeg.selectSlat = (slat) ->
            Fed.invoke 'slats_detail',
              providerId: provider.id
              slatId: slat.id
            , (res) ->
              slaNeg.slat slat
              slaNeg.content res.content
          
          ctx.render('user/tpl/slas-negotiate-details.html')
            .swap()
            .then ->
              slaNeg.provider provider              
              
              base.bindvm slaNeg
        
        # This is v1_1. After release, use the later method. 
        slaNeg.negotiate_v1_1 = ->
          sla =
            providerId: slaNeg.provider().id,
            name: slaNeg.name(),
            content: slaNeg.content()
          
          Fed.invoke 'slas_negotiate_v1_1', sla, (res) ->
            if res.success
              ctx.redirect '#/'
              base.growl """SLA "#{sla.name}" has been created."""
            else
              base.growl 'SLA negotiation failed: ' + res.error
        
        slaNeg.negotiate = ->
          sla =
            providerId: slaNeg.provider().id,
            name: slaNeg.name(),
            content: slaNeg.content(),
            provider_slat_id: "",
            slat_id: slaNeg.slat().id,
            negotiation_id: "",
            proposal_id: "",
            slat_abstract: "",
          
          offer = ""

          Fed.invoke 'slas_negotiate', sla, (res) ->
            if res.success   
              console.log('calling slas_negotiate')           
              offer = jQuery.parseJSON(res.content_slat_proposal)              
              sla.slat_id = res.slat_id
              sla.negotiation_id = res.negotiation_id
              sla.proposal_id = res.proposal_id
              console.log('slat proposal', offer)
              res                   			
 
            ctx.render('user/tpl/slas-negotiate-offer.html')
              .swap()
              .then ->
                console.log('slat offer to render', offer)
                slaOffer = new SlaOffer(offer)
            
            	# Enable this after Release 1.1
                #slaOffer.editNegotiatedValue = ->
                #  slaOffer.isOfferEdited(yes)
                
                # Disable this before Release 1.1
                slaOffer.editNegotiatedValue = ->
                  slaOffer.isOfferEdited(false)
                  
                slaOffer.cancelNegotiaion = ->
                  ctx.redirect '#/'
                  base.growl 'SLA negotiation canceled'
              
                slaOffer.renegotiate = ->
                  console.log offer
                  base.growl """Negotiating the offer"""
                  sla.slat_abstract = offer
                  Fed.invoke 'update_slat', sla, (res) ->                
                    if res.success
                      Fed.invoke 'renegotiate_slat', sla, (res) ->
                        if res.success              
                          offer = jQuery.parseJSON(res.content_slat_proposal)
                          slaOffer.agreementTerms(offer.agreementTerms)
                          slaOffer.isOfferEdited(no)
                          sla.slat_id = res.slat_id
                          sla.proposal_id=res.proposal_id
                          sla.slat_abstract = offer
                          console.log('slat proposal', offer)
                          base.growl """Finished negotiation"""
                          res                         
                        
                slaOffer.acceptAgreement = ->                                  
                  Fed.invoke 'create_agreement', sla, (res) -> 
                    if res.success                      
                      base.growl """Agreement accepted."""
                      base.growl """SLA "#{sla.name}" has been created."""
                      ctx.redirect '#/apps/register'
                    else
                      base.growl """Agreement failed"""
                      ctx.redirect '#/'
                base.bindvm slaOffer

        base.bindvm slaNeg

  @get '#/slas/:id', (ctx) ->
    slaId = ctx.params.id
    
    Fed.invoke 'slas_detail', id: slaId, (res) ->
      if res.content
        res.content = vkbeautify.xml(res.content, 2);
      sla = new SlaVm(res)
      
      ctx.render('user/tpl/slas-detail.html')
        .swap()
        .then ->
          base.bindvm sla

  @get '#/apps/register', (ctx) ->
    @render('user/tpl/apps-register.html')
      .swap()
      .then ->
        appReg = new ApplicationRegisterVm
        
        appReg.register = ->
          app =
            name: appReg.name()
            slaId: appReg.sla().id

          Fed.invoke 'applications_register', app, (res) ->
            if res.success
              ctx.redirect '#/'
              base.growl """Application "#{app.name}" has been registered."""
            else
              base.growl 'Application could not be registered: ' + res.error

        appReg.criteria = ->
          postData =
            appName: appReg.name()
            slatId: appReg.sla().id
            csrfmiddlewaretoken: CRpc.get_cookie('csrftoken')

          $().redirect('/user/app-selection-criteria', postData);

        appReg.cancel = ->
          base.growl """Application registration canceled."""
          window.location.replace('#/')

        base.bindvm appReg

  @get '#/apps/:id', (ctx) ->
    appId = ctx.params.id
    
    Fed.invoke 'applications_detail', id: appId, (res) ->
      console.log(res)
      app = new ApplicationVm(res)
      
      ctx.render('user/tpl/apps-detail.html')
        .swap()
        .then ->
          base.bindvm app

  @get '#/apps/:id/deploy', (ctx) ->
    appId = ctx.params.id
    
    Fed.invoke 'applications_deploy', id: appId, (res) ->
      if res.success
        base.alert 'success', 'Application was deployed'
        
        ctx.redirect '#', 'apps', appId
      else
        base.alert 'error', 'Application could not be deployed'
  
  @get '#/apps/:id/stop', (ctx) ->
    appId = ctx.params.id
    
    Fed.invoke 'applications_stop', id: appId, (res) ->
      if res.success
        base.alert 'success', 'Application was stopped'

        ctx.redirect '#', 'apps', appId
      else
        base.alert 'error', 'Application could not be stopped'  
  
  @get '#/apps/:id/start', (ctx) ->
    appId = ctx.params.id
    
    Fed.invoke 'applications_start', id: appId, (res) ->
      if res.success
        base.alert 'success', 'Application was started'
        
        ctx.redirect '#', 'apps', appId
      else
        base.alert 'error', 'Application could not be started'  

  @get '#/apps/:id/kill', (ctx) ->
    appId = ctx.params.id

    if confirm "Are you sure you want to kill the application? This can not be undone."
      Fed.invoke 'applications_kill', id: appId, (res) ->
        if res.success
          base.alert 'success', 'Application was killed successfully.'
        else
          base.alert 'error', 'Application could not be killed.'
    ctx.redirect '#/'

  @get '#/apps/:id/delete', (ctx) ->
    appId = ctx.params.id

    if confirm "Are you sure you want to delete the application? This can not be undone."
      Fed.invoke 'applications_delete', id: appId, (res) ->
        if res.success
          base.alert 'success', 'Application was deleted'
        else
          base.alert 'error', 'Application could not be deleted'
    ctx.redirect '#/'

  @get '#/slas/:id/delete', (ctx) ->
    slaId = ctx.params.id

    if confirm "Are you sure you want do delete the SLA? This can not be undone."
      Fed.invoke 'slas_delete', id: slaId, (res) ->
        if res.success
          base.alert 'success', 'SLA was deleted'
        else
          base.alert 'error', 'SLA could not be deleted'
    ctx.redirect '#/'

  return
)

federApp.run '#/'
