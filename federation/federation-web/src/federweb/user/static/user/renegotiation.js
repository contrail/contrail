Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '../ux');

Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*'
]);

Ext.onReady(function () {
    Ext.QuickTips.init();

    drawTreeGrid(offerId);
});

var currentRecord;
var store;

function drawTreeGrid(offerId) {
    var container = "treegrid-" + offerId;

    // define model
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'name', type: 'string'},
            {name: 'qosTerm', type: 'string'},
            {name: 'operator', type: 'string'},
            {name: 'target', type: 'string'},
            {name: 'status', type: 'string'},
            {name: 'thresholdOld', type: 'string'},
            {name: 'thresholdNew', type: 'string'}
        ]
    });

    // define store
    store = Ext.create('Ext.data.TreeStore', {
        model: 'Task',
        proxy: {
            type: 'ajax',
            url: getProxiedUrl(
                sprintf('%s/users/%s/slats/%s/negotiation/%s/proposals/%s/getTreeGridComparison', fedApiAddress, userUuid, slatId, negotiationId, offerId))
        },
        folderSort: true
    });

    // draw tree grid
    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Offer Details',
        width: 1000,
        height: 500,
        renderTo: Ext.get(container),
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: store,
        multiSelect: true,
        viewConfig: {
            listeners: {
                itemcontextmenu: viewContextMenu
            }
        },
        columns: [
            {
                xtype: 'treecolumn',
                text: 'Name',
                width: 250,
                sortable: true,
                dataIndex: 'name',
                locked: true
            },
            {
                header: 'Status',
                flex: 30,
                dataIndex: 'status',
                sortable: true,
                align: 'center',
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    if (value === 'NOT_CHANGED') {
                        return "<span style='font-size:20px; font-weight:900; color: green' title='No change'>&#10004;</span>";
                    }
                    else if (value === 'CHANGED') {
                        return "<span style='font-size:20px; font-weight:900; color: orange' title='Changed'>&#x2260;</span>";
                    }
                    else if (value === 'ADDED') {
                        return "<span style='font-size:20px; font-weight:900; color: orange' title='Added'>+</span>";
                    }
                    else if (value === 'REMOVED') {
                        return "<span style='font-size:20px; font-weight:900; color: red' title='Removed'>&times;</span>";
                    }
                    else {
                        return "";
                    }
                    return sprintf("<img src='%simg/%s' width='15' height='15' title='%s'/>", staticUrl, icon, title);
                }
            },
            {
                header: 'Target',
                flex: 100,
                dataIndex: 'target',
                sortable: true
            },
            {
                header: 'QoS Term',
                flex: 100,
                dataIndex: 'qosTerm',
                sortable: true
            },
            {
                header: 'Operator',
                flex: 50,
                dataIndex: 'operator',
                sortable: false
            },
            {
                xtype: 'templatecolumn',
                header: 'Proposal Value',
                flex: 75,
                dataIndex: 'thresholdOld',
                sortable: false,
                tpl: Ext.create('Ext.XTemplate', '{[this.format(values)]}', {
                    format: function (values) {
                        if (values.qosTerm) {
                            return (values.thresholdOld) ? values.thresholdOld : 'None'
                        }
                    }
                })
            },
            {
                xtype: 'templatecolumn',
                header: 'Offer Value',
                flex: 75,
                dataIndex: 'thresholdNew',
                sortable: false,
                tpl: Ext.create('Ext.XTemplate', '{[this.format(values)]}', {
                    format: function (values) {
                        if (values.qosTerm) {
                            return (values.thresholdNew) ? values.thresholdNew : 'None'
                        }
                    }
                })
            }
        ]
    });


    function viewContextMenu(dataview, record, item, index, e, eOpts) {
        //stop the default action
        e.stopEvent();
        //save the current selected record
        currentRecord = record;

        if (record.get('depth') === 1) {
            // agreement term
            if (!this.agrTermContextMenu) {
                this.agrTermContextMenu = Ext.create('TreeGrid.AgrTermContextMenu');
            }
            this.agrTermContextMenu.showAt(e.getXY());
        }

        if (record.get('depth') === 2) {
            // guarantee
            if (!this.guaranteeContextMenu) {
                this.guaranteeContextMenu = Ext.create('TreeGrid.GuaranteeContextMenu');
            }
            this.guaranteeContextMenu.showAt(e.getXY());
        }
        return false;
    }

    Ext.define('TreeGrid.GuaranteeContextMenu', {
        extend: 'Ext.menu.Menu',
        alias: 'widget.editMenu',
        width: 120,

        initComponent: function () {
            var me = this;

            Ext.applyIf(me, {
                items: [
                    {
                        xtype: 'menuitem',
                        text: 'Edit',
                        iconCls: 'icon-edit',
                        handler: editGuarantee
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Remove',
                        iconCls: 'icon-delete',
                        handler: deleteGuarantee
                    }
                ]
            });

            me.callParent(arguments);
        }
    });

    Ext.define('TreeGrid.AgrTermContextMenu', {
        extend: 'Ext.menu.Menu',
        alias: 'widget.editMenu',
        width: 120,

        initComponent: function () {
            var me = this;

            Ext.applyIf(me, {
                items: [
                    {
                        xtype: 'menuitem',
                        text: 'Remove',
                        iconCls: 'icon-delete',
                        handler: deleteAgrTerm
                    }
                ]
            });

            me.callParent(arguments);
        }
    });

    function editGuarantee() {
        var guaranteeEditWindow = Ext.create('GuaranteeEditWindow');
        guaranteeEditWindow.down('form').loadRecord(currentRecord);
        guaranteeEditWindow.show();
    }

    function deleteGuarantee() {
        currentRecord.destroy();
    }

    function deleteAgrTerm() {
        currentRecord.destroy();
    }

    var operatorsStore = Ext.create('Ext.data.Store', {
        fields: ['id', 'name'],
        data: [
            {"id": "equals", "name": "equals"},
            {"id": "not_equals", "name": "not equals"},
            {"id": "less_than", "name": "less than"},
            {"id": "less_than_or_equals", "name": "less than or equals"},
            {"id": "greater_than", "name": "greater than"},
            {"id": "greater_than_or_equals", "name": "greater than or equals"},
            {"id": "member_of", "name": "member of"}
        ]
    });

    Ext.define('GuaranteeEditWindow', {
        extend: 'Ext.window.Window',
        alias: 'widget.countryEdit',
        addMode: false,
        title: 'Edit Guarantee',
        layout: 'fit',

        initComponent: function () {
            this.items = this.buildItems();
            this.buttons = this.buildButtons();
            this.callParent(arguments);
        },
        buildItems: function () {
            return [
                {
                    xtype: 'form',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'QoS Term',
                            name: 'qosTerm'
                        },
                        {
                            xtype: 'textfield',
                            name: 'target',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldLabel: 'Target',
                            size: 31,
                            maxLength: 30
                        },
                        {
                            xtype: 'combo',
                            name: 'operator',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldLabel: 'Operator',
                            store: operatorsStore,
                            displayField: 'name',
                            valueField: 'id'
                        },
                        {
                            xtype: 'textfield',
                            name: 'thresholdOld',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldLabel: 'Value',
                            size: 31,
                            maxLength: 30
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'parentNodeId',
                            itemId: 'parentNodeId'
                        }
                    ]
                }
            ];
        },
        buildButtons: function () {
            return [
                {
                    text: 'Save',
                    action: 'save',
                    handler: saveGuarantee
                },
                {
                    text: 'Cancel',
                    scope: this,
                    handler: this.close
                }
            ];
        }

    });

    function saveGuarantee(button) {
        //get access to the window using the button reference
        var win = button.up('window');
        //get access to the form using the window reference
        form = win.down('form');

        //check if the form passed all validations
        if (form.getForm().isValid()) {
            currentRecord.set('target', form.getValues().target);
            currentRecord.set('operator', form.getValues().operator);
            currentRecord.set('thresholdOld', form.getValues().thresholdOld);

            win.close();
        }
    }

    function getProxiedUrl(url) {
        return '/user/jsonproxy/' + window.btoa(url).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, ',');
    }
}

function renegotiate() {
    if (store.getModifiedRecords().length == 0 &&
        store.getRemovedRecords().length == 0) {
        var answer = confirm("Nothing was changed. Are you sure you want to renegotiate?");
        if (answer === false) {
            return false;
        }
    }

    $.jGrowl(sprintf("Renegotiating SLA offer..."));
    var slatData = {
        modified: [],
        removed: []
    };

    store.getModifiedRecords().forEach(function (record) {
        var o = {
            id: record.data.id,
            type: record.data.depth,
            modified: record.getChanges()
        };
        slatData.modified.push(o);
    });

    store.getRemovedRecords().forEach(function (record) {
        var o = {
            id: record.data.id,
            type: record.data.depth
        };
        slatData.removed.push(o);
    });

    var postData = {
        'csrfmiddlewaretoken': CRpc.get_cookie('csrftoken'),
        'appId': appId,
        'slatId': slatId,
        'negotiationId': negotiationId,
        'offerId': offerId,
        'slatData': JSON.stringify(slatData)
    };

    $.ajax({
        type: "POST",
        url: "/user/renegotiate",
        contentType: "application/xml",
        data: postData,
        dataType: 'json'
    })
        .done(function (data, textStatus, jqXHR) {
            $.jGrowl("Renegotiation has finished successfully.");
            newSlatId = data.slatID
            $.jGrowl("New SLAT ID: " + newSlatId);

            var postData = {
                'csrfmiddlewaretoken': CRpc.get_cookie('csrftoken'),
                'appId': appId,
                'slatId': slatId,
                'negotiationId': negotiationId
            };
            $().redirect('/user/offers', postData);
        })
        .fail(function (jqXHR, textStatus) {
            $.jGrowl("Failed to renegotiate. Unexpected response: " + jqXHR.status + " " + jqXHR.statusText);
        });
}

function backToOffersList() {
    var postData = {
        'csrfmiddlewaretoken': CRpc.get_cookie('csrftoken'),
        'appId': appId,
        'slatId': slatId,
        'negotiationId': negotiationId
    };
    $().redirect('/user/offers', postData);
}

function confirmCancel() {
    var answer = confirm("Are you sure you want to cancel the negotiation?");
    if (answer === true) {
        window.location.replace("/user#/");
    }
}
