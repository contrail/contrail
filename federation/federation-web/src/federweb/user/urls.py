from django.conf.urls.defaults import patterns, url

from federweb.user.views.apps import Apps, AppsRpc, AppsTemplate, MonitorView
from federweb.user.views.authorization import AuthorizationTrustList, authorizationTrustModify

urlpatterns = patterns('',
    url(r'^$', Apps.as_view(), name='user'),
    url(r'^/monitor$', MonitorView.as_view(), name='user_monitor'),
    url(r'^/rpc$', AppsRpc.as_view(), name='user_apps_rpc'),
    url(r'^/tpl/(?P<tpl>.*)$', AppsTemplate.as_view(), name='user_apps_tpl'),
    url(r'^/jsonproxy/(?P<url>.+)$', 'federweb.user.views.apps.jsonProxy'),
    #authorization
    ####################################### url(r'^/authorization$', AuthorizationList.as_view(), name='federation_authorization'),

    #url(r'^/authorization/organizations$', AuthorizationOrganizationsList.as_view(), name='user_authorization_organizations'),
    #url(r'^/authorization/organizations/create$', AuthorizationOrganizationsCreate.as_view(), name='user_authorization_organizations_create'),
    #url(r'^/authorization/organizations/(?P<id>\d+)/edit$', AuthorizationOrganizationsEdit.as_view(), name='user_authorization_organizations_edit'),
    #url(r'^/authorization/organizations/(?P<id>\d+)/remove$', AuthorizationOrganizationsRemove.as_view(), name='user_authorization_organizations_remove'),

    #url(r'^/authorization/organizations/(?P<organization_id>\d+)/clients$', AuthorizationOrganizationsClientsList.as_view(), name='user_authorization_organizations_clients'),
    #url(r'^/authorization/organizations/(?P<organization_id>\d+)/clients/create$', AuthorizationOrganizationsClientsCreate.as_view(), name='user_authorization_organizations_clients_create'),
    #url(r'^/authorization/organizations/(?P<organization_id>\d+)/clients/(?P<id>\d+)/edit$', AuthorizationOrganizationsClientsEdit.as_view(), name='user_authorization_organizations_clients_edit'),
    #url(r'^/authorization/organizations/(?P<organization_id>\d+)/clients/(?P<id>\d+)/remove$', AuthorizationOrganizationsClientsRemove.as_view(), name='user_authorization_organizations_clients_remove'),

    url(r'^/authorization/trust$', AuthorizationTrustList.as_view(), name='user_authorization_trust'),
    url(r'^/authorization/trust/modify/(?P<organization_id>\d+)$', authorizationTrustModify, name='user_authorization_trust_modify_organizations'),
    url(r'^/authorization/trust/modify/(?P<organization_id>\d+)/clients/(?P<client_id>\d+)$', authorizationTrustModify, name='user_authorization_trust_modify_organizations_clients'),

    # Auditing
    url(r'^/auditing$', 'federweb.user.views.auditing.auditing_show_tab', name='user_auditing'),

    url(r'^/app-selection-criteria$', 'federweb.user.views.app_selection_criteria.show', name='app_selection_criteria_show'),
    url(r'^/app-selection-criteria/save', 'federweb.user.views.app_selection_criteria.save', name='app_selection_criteria_save'),
	
    url(r'^/offers$', 'federweb.user.views.offers.show_offers', name='show_offers'),
    url(r'^/show-renegotiation$', 'federweb.user.views.renegotiation.show_offer', name='show_offer'),
    url(r'^/renegotiate$', 'federweb.user.views.renegotiation.renegotiate', name='renegotiate'),
    url(r'^/sla-agreement$', 'federweb.user.views.sla_agreement.show', name='show_sla_agreement'),
)
