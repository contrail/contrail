import logging
import json

from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from federweb.base import utils


logger = logging.getLogger(__name__)

@login_required
def show_offer(request):
    logger.debug("show_offers() started.")

    appId = request.POST.get("appId")
    slatId = request.POST.get("slatId")
    negotiationId = request.POST.get("negotiationId")
    offerId = request.POST.get("offerId")
    offerNum = request.POST.get("offerNum")
    user = request.user
    token = request.session['monitoring_access_token']
    logger.debug("appId=%s, slatId=%s, negotiationId=%s, offerId=%s, offerNum=%s" %
                 (appId, slatId, negotiationId, offerId, offerNum))

    logger.debug("Retrieving SLA offer with id %s..." % offerId)
    url = "%s/users/%s/slats/%s/negotiation/%s/proposals/%s" %\
          (settings.FEDERATION_API_URL_SSL, user.uuid, slatId, negotiationId, offerId)
    response = utils.send_http_request("GET", url, token)
    offerData = json.loads(response.content)

    providerUuid = offerData["slatAbstract"]["providers"][0]
    logger.debug("Retrieving provider's %s data...")
    url = "%s/providers/%s" % (settings.FEDERATION_API_URL_SSL, providerUuid)
    providerResponse = utils.send_http_request("GET", url, token)
    providerData = json.loads(providerResponse.content)

    offer = dict(
        num=offerNum,
        offerId=offerId,
        created=offerData["created"],
        providerName=providerData["name"]
    )

    params = dict(
        fedApiAddress=settings.FEDERATION_API_URL_SSL,
        offerId=offerId,
        offer=offer,
        slatId=slatId,
        negotiationId=negotiationId,
        appId=appId
    )

    return render_to_response('user/apps/renegotiation.html', params, context_instance=RequestContext(request))

@login_required
def renegotiate(request):
    logger.debug("renegotiate() started.")

    appId = request.POST.get("appId")
    slatId = request.POST.get("slatId")
    negotiationId = request.POST.get("negotiationId")
    offerId = request.POST.get("offerId")
    slatData = request.POST.get("slatData")

    user = request.user
    token = request.session['monitoring_access_token']
    logger.debug("appId=%s, slatId=%s, negotiationId=%s, offerId=%s, slatData=%s" %
                 (appId, slatId, negotiationId, offerId, slatData))

    logger.debug("Creating modified user SLAT based on SLA offer %s..." % offerId)
    url = "%s/users/%s/slats/%s/negotiation/%s/proposals/%s/adapt" %\
          (settings.FEDERATION_API_URL_SSL, user.uuid, slatId, negotiationId, offerId)
    response = utils.send_http_request("POST", url, token, slatData)
    logger.debug(response)
    if response.status_code != 201:
        return HttpResponse(content="Failed to create new SLAT: %d" % response.status_code, status=400)
    responseJson = json.loads(response.content)
    newSlatId = responseJson["slatId"]

    response_data = dict(
        slatID=newSlatId
    )
    return HttpResponse(content=json.dumps(response_data), content_type="application/json")
