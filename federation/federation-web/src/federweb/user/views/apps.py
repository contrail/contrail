import base64
import os
import logging
import operator
import re
import traceback
from django.views.decorators.csrf import csrf_exempt

from django.views.generic.base import TemplateView
from django.utils import simplejson as json
from django.http import Http404, HttpResponse
from django.contrib.auth.views import redirect_to_login
from django.conf import settings

from federweb.base.views import AppNodeMixin, RpcView, rpc_method
from federweb.user.views import AppMixin
from federweb.base.models import federation, UserApplicationManagerError
from federweb.user.models import ProviderVep, Deployment
from federweb.hookbox.models import Pusher
import requests

logger = logging.getLogger(__name__)

class AppsMixin(AppMixin, AppNodeMixin):
    node_name = 'apps'

class Apps(AppsMixin, TemplateView):
    action_name = 'apps'

class AppsTemplate(AppsMixin, TemplateView):
    def get_template_names(self):
        tpl = os.path.basename(self.kwargs['tpl'])
        
        if not tpl.endswith('.html'):
            raise Http404
        
        return ['user/apps/tpls/%s' % tpl]

class AppsRpc(AppsMixin, RpcView):
    @rpc_method
    def applications_list(self, **data):
        apps = self.request.user.applications.full()
        
        for app in apps:
            yield dict(
                id=app.id,
                name=app.name,
                state=app.state or 'registered'
            )
    
    @rpc_method
    def applications_detail(self, id):
        #import pdb; pdb.set_trace()
        app = self.request.user.applications.get(id)

        attrs = json.loads(app.attributes)
        logger.debug("Application attributes: %s" % attrs)
        logger.debug("Application details: %s " % attrs['app_data'])

        if attrs.has_key("providerId"):
            provider = federation.providers.get(attrs['providerId'])
        else:
            provider = None

        if attrs.has_key("slaId"):
            sla = self.request.user.slas.get(attrs['slaId'])
        else:
            sla = None

        logger.debug("Retrieving VMs data: %s" % app.vms)
        vmArray = []
        for vmUri in app.vms:
            m = re.match(r"^/providers/[\w-]+/vms/(\d+)$", vmUri)
            if m:
                vmId = m.group(1)
                vm = provider.vms.get(vmId)
                vmData = dict(
                    name=vm.name,
                    vmVinAddress=vm.vmVinAddress,
                    netAppName=vm.NetAppName,
                    vinNetId=vm.VinNetId
                )
                vmArray.append(vmData)
            else:
                logger.error("Invalid VM URI: " + vmUri)

        return dict(
            id=app.id,
            name=app.name,
            state=app.state or 'registered',
            provider=provider.name if provider else "N/A",
            sla=sla.sla if sla else "N/A",
            app_data=attrs['app_data'],
            vms=vmArray
        )
    
    def provider_register_user(self, provider, user):
        provider_vep = ProviderVep(provider)
        
        return provider_vep.register_user(user)

    def _get_current_loa(self):
        attributes = self.request.user.attributes.all()
        for attribute in attributes:
            if attribute.name == "urn:contrail:names:federation:subject:current-loa":
                pass

    @rpc_method
    def applications_register(self, **data):        
        sla_id = data['slaId']
        sla = self.request.user.slas.get(sla_id)

        provider_id = sla.templateurl
        
        provider = federation.providers.get(provider_id)
        
        app = self.request.user.applications.new()
        app.name = data['name']
        app.deploymentDesc = ''
        app.applicationOvf = ''
        #app.attributes = json.dumps({'slaId': sla.id, 'providerId': provider.id, 'app_data':None, 'userSLATemplateUrl': sla.id})
        app.attributes = {'slaId': sla.id, 'providerId': provider.id, 'app_data': None, 'userSLATemplateUrl': sla.id}

        logger.debug("application: %s" % json.dumps(app.attributes))

        saved = app.save()
        
        app.state = 'registered'
        saved = app.save()

        ## Here commes call to Feder API / core -> submitApplication
        app = self.request.user.applications.get(app.id)
        logger.debug("Submiting application through federation core: %s" % json.dumps(app.attributes))
        ret = self.request.user.applications.submit_application(app.id)
        logger.debug("Got from the User Application Manager: %s" % ret)

        if saved:
            return dict(success=True, id=app.id)
        
        return dict(success=False, error="FAIL!")
    
    @rpc_method
    def applications_deploy_vep(self, id):
        """
        This was renamed to 'vep' since this calls VEP directly. Now,
        we are calling this through federation API. See method below
        """
        app = self.request.user.applications.get(id)
        
        if app.state != 'registered':
            return dict(success=False, error="Already deployed")
        
        attr = json.loads(app.attributes)
        
        provider = federation.providers.get(attr['providerId'])
        sla = self.request.user.slas.get(attr['slaId'])
        
        deployment = Deployment(self.request.user, provider)
        
        deployed = deployment.deploy(app, sla)
        #import pdb; pdb.set_trace()
        if deployed['success']:
            return dict(success=True, id=app.id)
        
        return dict(success=False, error="FAIL!")

    @rpc_method
    def applications_deploy(self, id):
        app = self.request.user.applications.get(id)
        if app.state != 'registered':
            return dict(success=False, error="Already deployed")
        attr = json.loads(app.attributes)
        #provider = federation.providers.get(attr['providerId'])
        #slat = self.request.user.slats.get(attr['slaId'])
        ## Here commes call to Feder API / core -> startApplication
        #app = self.request.user.applications.get(app.id)
        logger.debug("Starting application through federation core: %s" % json.dumps(app.attributes))
        ret = self.request.user.applications.start_application(app.id)
        logger.debug("Got from the User Application Manager: %s" % ret)
        app.state = 'deployed'
        logger.debug("Setting app state to DEPLOYED")
        saved = app.save()
        if saved and ret.status_code == 204:
            return dict(success=True, id=app.id)
        return dict(success=False, error="FAIL!")
    
    @rpc_method
    def applications_stop(self, id):
        try:
            app = self.request.user.applications.get(id)
            if app.state != 'running':
                return dict(success=False, error="Already stopped")
            logger.debug("Stopping application through federation core: %s" % json.dumps(app.attributes))
            resp = self.request.user.applications.stop_application(app.id)
            logger.debug("Got response from the User Application Manager: %s" % resp)
            return dict(success=True)
        except UserApplicationManagerError as e:
            return dict(success=False, error="Failed to stop the application: %d %s" % (e.response.status_code, e.response.content))
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())
            return dict(success=False, error="Failed to stop the application: %s" % str(e))

    @rpc_method
    def applications_stop_vep(self, id):
        try:
            app = self.request.user.applications.get(id)

            if app.state != 'deployed':
                return dict(success=False, error="Already stopped")

            attr = json.loads(app.attributes)

            provider = federation.providers.get(attr['providerId'])
            sla = self.request.user.slas.get(attr['slaId'])

            deployment = Deployment(self.request.user, provider)

            stopped = deployment.stop(app)
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())

        if stopped['success']:
            return dict(success=True)

        return dict(success=False, error="FAIL!")

    @rpc_method
    def applications_kill(self, id):
        try:
            app = self.request.user.applications.get(id)
            logger.debug("Killing application through federation core: %s" % json.dumps(app.attributes))
            resp = self.request.user.applications.kill_application(app.id)
            logger.debug("Got response from the User Application Manager: %s" % resp)
            return dict(success=True)
        except UserApplicationManagerError as e:
            return dict(success=False, error="Failed to kill the application: %d %s" % (e.response.status_code, e.response.content))
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())
            return dict(success=False, error="Failed to kill the application: %s" % str(e))

    @rpc_method
    def applications_start_vep(self, id):
        """
        Old way of deploying app through VEP
        """
        try:
            app = self.request.user.applications.get(id)
            
            if app.state == 'deployed':
                return dict(success=False, error="Already deployed")
            
            attr = json.loads(app.attributes)
            
            provider = federation.providers.get(attr['providerId'])
            sla = self.request.user.slas.get(attr['slaId'])
            
            deployment = Deployment(self.request.user, provider)
            
            started = deployment.start(app, sla)
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())

        if started['success']:
            return dict(success=True)
        
        return dict(success=False, error="FAIL!")    

    @rpc_method
    def applications_start(self, id):
        try:
            app = self.request.user.applications.get(id)
            if app.state != 'submitted':
                return dict(success=False, error="Application in wrong state.")
            logger.debug("Starting application through federation core: %s" % json.dumps(app.attributes))
            resp = self.request.user.applications.start_application(app.id)
            logger.debug("Got response from the User Application Manager: %s" % resp)
            return dict(success=True)
        except UserApplicationManagerError as e:
            return dict(success=False, error="Failed to start the application: %d %s" % (e.response.status_code, e.response.content))
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())
            return dict(success=False, error="Failed to start the application: %s" % str(e))

    @rpc_method
    def applications_delete(self, id):
        resp = None
        try:
            logger.debug("Deleting User Application")
            app = self.request.user.applications.get(id)
            if app.state == 'started':
                return dict(success=False, error="Already started!")
            if app.state == 'stopped':
                # we have to kill the application first
                logger.debug("Killing the application %s..." % id)
                try:
                    resp = self.request.user.applications.kill_application(app.id)
                    logger.debug("Application was killed successfully.")
                except UserApplicationManagerError as e:
                    logger.error("Failed to kill the application: %s" % e.response.content)
                    # ignore the error, kill the application still
            resp = app.delete()
            logger.debug("Got response from the User Application Manager: %s" % resp)
            return dict(success=True)
        except UserApplicationManagerError as e:
            return dict(success=False, error="Failed to delete the application: %d %s" % (e.response.status_code, e.response.content))
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())
            return dict(success=False, error="Failed to delete the application: %s" % str(e))

    SERVERS_KEY_MAP = {
        'name': 'name',
        'ram_total': 'ramTotal',
        'ram_used': 'ramUsed',
        'ram_free': 'ramFree',
        'cpu_cores': 'cpuCores',
        'cpu_speed': 'cpuSpeed',
        'cpu_load_one': 'cpuLoadOne',
        'cpu_load_five': 'cpuLoadFive'
    }
    
    EQ_SIGN_MAP = {
        '<': operator.lt,
        '<=': operator.le,
        '>': operator.gt,
        '>=': operator.ge,
    }
    
    def _server_filter(self, s, filters):
        vals = {}
        
        for k, v in self.SERVERS_KEY_MAP.items():
            vals[v] = getattr(s, k, None)
        
        for f, (fe, fv) in filters.items():
            if f in vals and vals[f] is not None:
                if not self.EQ_SIGN_MAP[fe](float(vals[f]), float(fv)):
                    return False
        
        return vals
    
    @rpc_method
    def providers_list_servers(self, **filters):
        providers = federation.providers.full()
        
        for provider in providers:
            def get_servers():
                for s in provider.servers.full():
                    try:
                        vals = self._server_filter(s, filters)
                        
                        if vals:
                            yield vals
                    except:
                        logger.error('Invalid server (id=%s) data' % s.id, exc_info=True)
            
            servers = list(get_servers())
            
            if servers:
                yield dict(
                    id=provider.id,
                    name=provider.name,
                    servers=servers
                )
    
    def _providers_slats(self):
        providers = federation.providers.full()
        
        for provider in providers:
            for slat in provider.slats.all():
                yield slat, provider
    
    @rpc_method
    def slats_list(self, **data):
        logger.debug("Inside slats_list")
        provider_id = data['providerId']
        
        provider = federation.providers.get(provider_id)
        
        for slat in provider.slats.all():
            yield dict(
                id=slat.id,
                name=slat.name,
                url=slat.url
            )

    @rpc_method
    def get_federation_provider(self, **data):
        """
        Gets FEDERATION provider
        """
        logger.debug("Inside get_federation_provider")
        provider = federation.providers.get(settings.FEDERATION_UUID)
        return dict(
            id=provider.id,
            name=provider.name
        )
        #yield dict(id=provider.id, name=provider.name, servers=None)

    @rpc_method
    def federation_provider_slats_list(self, **data):
        """
        Used to list FEDERAITON provider's SLATs
        """
        provider = None
        for provider in federation.providers.all():
            if provider.name == "FEDERATION":
                break

        for slat in provider.slats.all():
            yield dict(
                id=slat.id,
                name=slat.name,
                url=slat.url
            )
    
    @rpc_method
    def slats_detail(self, **data):
        provider_id = data['providerId']
        slat_id = data['slatId']
        
        provider = federation.providers.get(provider_id)
        slat = provider.slats.get(slat_id)
        
        return dict(
            id=slat.id,
            name=slat.name,
            content=slat.content,
            url=slat.url
        )
    
    @rpc_method
    def slas_list(self):
        for sla in self.request.user.slats.all():
            yield dict(
                id=sla.id,
                #name=sla.sla
                name=sla.name
            )
    
    @rpc_method
    def slas_detail(self, id):
        sla = self.request.user.slats.get(id)
        
        return dict(
            id=sla.id,
            #name=sla.sla,
            name=sla.name,
            content=sla.content,
            #url=sla.templateurl
            url=sla.url
        )

    @rpc_method
    def slas_delete(self, id):
        resp = None
        try:
            logger.debug("Deleting User SLA")
            sla = self.request.user.slats.get(id)
            resp = sla.delete()
            logger.debug("Got from the User SLA Manager: %s" % resp)
        except Exception as e:
            logger.error(e)
            logger.error(traceback.format_exc())
        if resp.status_code == 204:
            return dict(success=True)
        return dict(success=False, error="FAIL!")

    @rpc_method
    def slas_negotiate(self, **data):
        provider_id = data['providerId']
        name = data['name']
        content = data['content']

        logger.debug("Creating UserSLATemplate")
        user_slat = self.request.user.slats.new()
        user_slat.sla = name
        user_slat.name = name
        user_slat.content = content
        user_slat.userId = self.request.user.id
        user_slat.slatId = data['slat_id']
        saved_user_slat = user_slat.save()        
        logger.debug("Got user slat response headers: %s " % saved_user_slat.headers)
        saved_user_slat_id=saved_user_slat.headers['location'].split('/')[-1]
        if saved_user_slat:
            try:                        
                logger.debug("Starting the first negotiation")
                negotiation_res=self.request.user.slats.initiate_negotiation(saved_user_slat_id)
                logger.debug("Got negotiation_id %s " % negotiation_res['negotiationId'])
                logger.debug("Staring initial negotiation")
                slatproposals=self.request.user.slats.negotiate_proposal(saved_user_slat_id, negotiation_res['negotiationId'], 0)
                # TODO: can be more than one proposal!  iterate [0]
                userSLATemplateId=slatproposals['slatProposals'][0]['userSLATemplateId']
                proposalUri=slatproposals['slatProposals'][0]['proposalUri']
                proposalId=proposalUri.split('/')[-1]
                compared=self.request.user.slats.compare_proposal(saved_user_slat_id,negotiation_res['negotiationId'],  proposalId)                
                logger.debug("Got slatAbstract: %s" % compared)
                return dict(success=True, content_slat_proposal=json.dumps(compared), slat_id=userSLATemplateId, negotiation_id=negotiation_res['negotiationId'], proposal_id=proposalId)
            except Exception as e:
                logger.error(traceback.format_exc())
        logger.error("Error while starting the negotiation phase.")
        return dict(success=False)
    
    def _get_changed_terms (self, offer):
        """Detects changed guarantees in the offer (based on thresholds)
        """
        termsChanged={}
        for term in offer['agreementTerms']:
            for guarantee in term['guarantees']:
                if guarantee['threshold1'] != guarantee['threshold2']:
                    logger.debug( "Changed: %s " % guarantee['guaranteeId'] )
                    termsChanged[guarantee['guaranteeId']]=guarantee['threshold2']
        return termsChanged
    
    def _update_proposal(self, offer, terms):
        for changed_term_name in terms:            
            for term in offer['slatAbstract']['agreementTerms']:
                for guarantee in term['guarantees']:
                    if guarantee['guaranteeId'] == changed_term_name:
                        guarantee['threshold']['value']=terms[changed_term_name]
        return offer
    
    @rpc_method
    def slas_negotiate_v1_1(self, **data):
        """ This is the negotiation process until version 1.1.
            After 1.1 we use the later method. Here we just save
            the chosen SLATEmplate (SLA actually) under user SLA
            and we finish the negotiation process.
        """
        provider_id = data['providerId']
        name = data['name']
        content = data['content']
        
        provider = federation.providers.get(provider_id)
        logger.debug("Saving SLA")
        sla = self.request.user.slas.new()
        sla.sla = name
        sla.content = content
        sla.templateurl = provider.id
        saved_sla = sla.save()
        logger.debug("Saved: %s" % saved_sla)

        logger.debug("Saving SLAT")
        slat = self.request.user.slats.new()
        slat.url = provider.id
        slat.name = name
        slat.content = content
        saved_slat = slat.save()
        logger.debug("Saved: %s" % saved_slat)
        
        if saved_sla and saved_slat:
            return dict(success=True, id=sla.id)
        
        return dict(success=False)
    
    @rpc_method
    def create_user_slat(self, **data):
        logger.debug("create_user_slat() started.")
        slatId = data['slatId']
        name = data['name']
        content = data['content']

        logger.debug("Saving user SLAT: slatId=%s, name=%s" % (slatId, name))
        slat = self.request.user.slats.new()
        slat.name = name
        slat.content = content
        slat.slatId = slatId
        response = slat.save()

        if response:
            logger.debug("User SLAT has been saved successfully.")
            return dict(success=True)
        else:
            logger.debug("Response: %d %s" % (response.status_code, response.content))
            return dict(success=False)

    @rpc_method
    def update_slat(self, **data):
        """TODO: "Contrail-SLAT-All" UUID is hardcoded within the method. 
        """
        slat_id = data['slat_id']
        negotiation_id = data['negotiation_id']
        proposal_id = data['proposal_id']        
        slat_abstract=data['slat_abstract']
        try:
            changedTerms=self._get_changed_terms(slat_abstract)
            logger.debug("Getting the offer from the server")
            negotiation_proposal=self.request.user.slats.get_negotiation_proposal(slat_id,negotiation_id, proposal_id )
            changed_negotiation_proposal=self._update_proposal(negotiation_proposal, changedTerms)                    
            # TODO: this is hardcoded
            slat_abstract['slatUUID']='Contrail-SLAT-All'        
            logger.debug("Updating the offer")
            self.request.user.slats.update_proposal(slat_id,negotiation_id, proposal_id , changed_negotiation_proposal['slatAbstract'])
            return dict(success=True)
        except Exception as e:
            logger.error(traceback.format_exc())
        return dict(success=False)
    
    @rpc_method
    def renegotiate_slat(self, **data):
        slat_id = data['slat_id']
        negotiation_id = data['negotiation_id']
        proposal_id = data['proposal_id'] 
        try:
            logger.debug("Staring renegotiation")
            slatproposals=self.request.user.slats.negotiate_proposal(slat_id, negotiation_id, proposal_id)                
            userSLATemplateId=slatproposals['slatProposals'][0]['userSLATemplateId']
            proposalUri=slatproposals['slatProposals'][0]['proposalUri']
            proposalId=proposalUri.split('/')[-1]
            logger.debug("Comparing SLA proposals")
            compared=self.request.user.slats.compare_proposal(slat_id,negotiation_id,  proposalId)                
            logger.debug("Got slatAbstract: %s" % compared)
            return dict(success=True, content_slat_proposal=json.dumps(compared), proposal_id=proposalId, slat_id=userSLATemplateId)
        except Exception as e:
            logger.error(traceback.format_exc())
        logger.error("Error while starting the negotiation phase.")
        return dict(success=False)
    
    @rpc_method
    def create_agreement(self, **data):
                
        try:
            slat_id = data['slat_id']
            negotiation_id = data['negotiation_id']
            proposal_id = data['proposal_id']        
            logger.debug("Staring create_agreement")
            res=self.request.user.slats.create_agreement(slat_id, negotiation_id, proposal_id)                            
            logger.debug("Got response for create_agreement: %s" % json.dumps(res))
            return dict(success=True)
        except Exception as e:
            logger.error(traceback.format_exc())
        logger.error("Error while starting the negotiation phase.")
        return dict(success=False)
    
    @rpc_method
    def push_test(self):
        pusher = Pusher(self.request.user)
    
        success = pusher.push({'status': 'Test'})
        
        return dict(success=success)

class MonitorView(TemplateView):
    template_name = "user/monitor.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect_to_login(request.path)
        response = super(MonitorView, self).dispatch(request, *args, **kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(MonitorView, self).get_context_data(**kwargs)
        context['MON_DRIVER'] = settings.MON_DRIVER
        return context

@csrf_exempt
def jsonProxy(request, url):

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer %s" % request.session['monitoring_access_token']
    }

    url = base64.b64decode(url.replace("-", "+").replace("_", "/").replace(",", "="))

    client_cert = settings.OAUTH2_CONFIG['oauth_client']['client_cert']
    client_key = settings.OAUTH2_CONFIG['oauth_client']['client_key']
    r = requests.request(
        method=request.method,
        url=url,
        data=request.raw_post_data,
        headers=headers,
        cert=(client_cert, client_key),
        verify=False
    )

    response = HttpResponse(r.text)
    response.status_code = r.status_code
    if 'Content-Type' in r.headers:
        response['Content-Type'] = r.headers['Content-Type']
    if 'Location' in r.headers:
        response['Location'] = r.headers['Location']

    return response
