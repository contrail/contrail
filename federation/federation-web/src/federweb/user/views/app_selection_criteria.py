import logging

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.functional import lazy
from django.conf import settings
import requests
from federweb.base import utils
from federweb.base.models import federation
from federweb.user.views.apps import AppsRpc
from django.utils import simplejson as json

reverse_lazy = lambda name=None, *args : lazy(reverse, str)(name, args=args)
logger = logging.getLogger(__name__)

def show(request):
    user = request.user
    appName = request.POST.get('appName')
    slatId = request.POST.get('slatId')
    data = dict(
        user_selection_criteria=user.selection_criteria.all(),
        slatId=slatId,
        appName=appName
    )
    return render_to_response('user/apps/app_selection_criteria.html',
                              data,
                              context_instance=RequestContext(request))

def save(request):
    logger.debug("save() started.")

    slatId = request.POST.get("slatId")
    appName = request.POST.get("appName")
    slat = request.user.slats.get(slatId)
    user = request.user

    logger.debug("Creating application...")
    app = request.user.applications.new()
    app.name = appName
    app.deploymentDesc = ''
    app.applicationOvf = ''
    app.attributes = {
        'app_data': None,
        'userSLATemplateUrl': "users/%s/slats/%s" % (user.uuid, slat.id)}
    logger.debug("application: %s" % json.dumps(app.attributes))

    saved = app.save()
    logger.debug("Application has been created.")

    # call federation-api:submitApplication
    logger.debug("Calling federation-api:submitApplication...")
    token = request.session['monitoring_access_token']
    url = "%s/users/%s/applications/%s/submit" % (settings.FEDERATION_API_URL_SSL, user.uuid, app.id)
    response = utils.send_http_request("PUT", url, token)
    logger.debug("federation-api:submitApplication response: %d" % response.status_code)

    # store selection criteria for the created application
    logger.debug("Storing selection criteria...")
    asc = app.selection_criteria.new()
    data = []
    for key in request.POST.keys():
        keyString = str(key)
        if keyString.startswith("usc-"):
            criterionData = {}
            criterionData["name"] = keyString[4:]
            criterionData["value"] = request.POST[key]
            data.append(criterionData)

    asc.raw = data
    asc.save(True)
    logger.debug("Selection criteria have been stored successfully.")

    # retrieve application data and extract negotiationId
    logger.debug("Retrieving negotiationId...")
    url = "%s/users/%s/applications/%s" % (settings.FEDERATION_API_URL_SSL, user.uuid, app.id)
    response = utils.send_http_request("GET", url, token)
    logger.debug("Got response: %s" % response)
    if response.status_code != 200:
        return HttpResponse(content="Failed to retrieve application data: %d %s" % (response.status_code, response.content), status=500)
    appData = json.loads(response.content)
    appAttrs = json.loads(appData["attributes"])
    if not "negotiationIds" in appAttrs:
        raise Exception("Negotiation failed. The 'negotiationIds' attribute cannot be found.")
    negotiationIdArr = appAttrs["negotiationIds"]
    negotiationId = negotiationIdArr[0]
    logger.debug("negotiationId: " + negotiationId)

    if saved:
        respData = dict(
            slatId=slat.id,
            appId=app.id,
            negotiationId=negotiationId
        )
        return HttpResponse(json.dumps(respData))
    else:
        return HttpResponse(content="Failed to create the application.", status=500)
