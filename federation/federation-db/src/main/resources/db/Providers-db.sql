SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `contrail` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `contrail` ;

-- -----------------------------------------------------
-- Table `Provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Provider` ;

CREATE  TABLE IF NOT EXISTS `Provider` (
  `providerId` INT NOT NULL AUTO_INCREMENT ,
  `uuid` VARCHAR(36) NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `providerUri` VARCHAR(255) NOT NULL ,
  `typeId` INT NOT NULL ,
  PRIMARY KEY (`providerId`) ,
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User` ;

CREATE  TABLE IF NOT EXISTS `User` (
  `userId` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(45) NOT NULL ,
  `attributes` TEXT NULL ,
  `firstName` VARCHAR(255) NOT NULL ,
  `lastName` VARCHAR(255) NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `UUID` VARCHAR(36) NOT NULL ,
  PRIMARY KEY (`userId`) ,
  UNIQUE INDEX `UUID_UNIQUE` (`UUID` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VO` ;

CREATE  TABLE IF NOT EXISTS `VO` (
  `voId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`voId`) ,
  INDEX `fk_VirtualOrganization_Provider1_idx` (`providerId` ASC) ,
  CONSTRAINT `fk_VirtualOrganization_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DataCenter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DataCenter` ;

CREATE  TABLE IF NOT EXISTS `DataCenter` (
  `dcId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `Provider_providerId` INT NULL ,
  PRIMARY KEY (`dcId`) ,
  INDEX `fk_DataCenter_Provider1_idx` (`Provider_providerId` ASC) ,
  CONSTRAINT `fk_DataCenter_Provider1`
    FOREIGN KEY (`Provider_providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Storage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Storage` ;

CREATE  TABLE IF NOT EXISTS `Storage` (
  `storageId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  PRIMARY KEY (`storageId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Network`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Network` ;

CREATE  TABLE IF NOT EXISTS `Network` (
  `networkId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  PRIMARY KEY (`networkId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster` ;

CREATE  TABLE IF NOT EXISTS `Cluster` (
  `clusterId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`clusterId`) ,
  INDEX `fk_Cluster_Provider1_idx` (`providerId` ASC) ,
  CONSTRAINT `fk_Cluster_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DataCenter_has_Network`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DataCenter_has_Network` ;

CREATE  TABLE IF NOT EXISTS `DataCenter_has_Network` (
  `dcId` INT NOT NULL ,
  `networkId` INT NOT NULL ,
  PRIMARY KEY (`dcId`, `networkId`) ,
  INDEX `fk_DataCenter_has_Network1_Network1_idx` (`networkId` ASC) ,
  INDEX `fk_DataCenter_has_Network1_DataCenter1_idx` (`dcId` ASC) ,
  CONSTRAINT `fk_DataCenter_has_Network1_DataCenter1`
    FOREIGN KEY (`dcId` )
    REFERENCES `DataCenter` (`dcId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DataCenter_has_Network1_Network1`
    FOREIGN KEY (`networkId` )
    REFERENCES `Network` (`networkId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DataCenter_has_Storage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DataCenter_has_Storage` ;

CREATE  TABLE IF NOT EXISTS `DataCenter_has_Storage` (
  `dcId` INT NOT NULL ,
  `storageId` INT NOT NULL ,
  PRIMARY KEY (`dcId`, `storageId`) ,
  INDEX `fk_DataCenter_has_Storage_Storage1_idx` (`storageId` ASC) ,
  INDEX `fk_DataCenter_has_Storage_DataCenter1_idx` (`dcId` ASC) ,
  CONSTRAINT `fk_DataCenter_has_Storage_DataCenter1`
    FOREIGN KEY (`dcId` )
    REFERENCES `DataCenter` (`dcId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DataCenter_has_Storage_Storage1`
    FOREIGN KEY (`storageId` )
    REFERENCES `Storage` (`storageId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DataCenter_has_Cluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DataCenter_has_Cluster` ;

CREATE  TABLE IF NOT EXISTS `DataCenter_has_Cluster` (
  `dcId` INT NOT NULL ,
  `clusterId` INT NOT NULL ,
  PRIMARY KEY (`dcId`, `clusterId`) ,
  INDEX `fk_DataCenter_has_Cluster_Cluster1_idx` (`clusterId` ASC) ,
  INDEX `fk_DataCenter_has_Cluster_DataCenter1_idx` (`dcId` ASC) ,
  CONSTRAINT `fk_DataCenter_has_Cluster_DataCenter1`
    FOREIGN KEY (`dcId` )
    REFERENCES `DataCenter` (`dcId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DataCenter_has_Cluster_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cluster_has_Network`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster_has_Network` ;

CREATE  TABLE IF NOT EXISTS `Cluster_has_Network` (
  `clusterId` INT NOT NULL ,
  `networkId` INT NOT NULL ,
  PRIMARY KEY (`clusterId`, `networkId`) ,
  INDEX `fk_Cluster_has_Network_Network1_idx` (`networkId` ASC) ,
  INDEX `fk_Cluster_has_Network_Cluster1_idx` (`clusterId` ASC) ,
  CONSTRAINT `fk_Cluster_has_Network_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Cluster_has_Network_Network1`
    FOREIGN KEY (`networkId` )
    REFERENCES `Network` (`networkId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cluster_has_Storage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster_has_Storage` ;

CREATE  TABLE IF NOT EXISTS `Cluster_has_Storage` (
  `clusterId` INT NOT NULL ,
  `storageId` INT NOT NULL ,
  PRIMARY KEY (`clusterId`, `storageId`) ,
  INDEX `fk_Cluster_has_Storage_Storage1_idx` (`storageId` ASC) ,
  INDEX `fk_Cluster_has_Storage_Cluster1_idx` (`clusterId` ASC) ,
  CONSTRAINT `fk_Cluster_has_Storage_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Cluster_has_Storage_Storage1`
    FOREIGN KEY (`storageId` )
    REFERENCES `Storage` (`storageId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Server`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Server` ;

CREATE  TABLE IF NOT EXISTS `Server` (
  `serverId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`serverId`) ,
  INDEX `fk_Server_Provider1_idx` (`providerId` ASC) ,
  CONSTRAINT `fk_Server_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Application`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Application` ;

CREATE  TABLE IF NOT EXISTS `Application` (
  `applicationId` INT NOT NULL AUTO_INCREMENT ,
  `uuid` VARCHAR(36) NOT NULL ,
  `deploymentDesc` TEXT NULL ,
  `name` VARCHAR(255) NULL ,
  `applicationOvf` TEXT NULL ,
  `attributes` TEXT NULL ,
  `slaUrl` VARCHAR(255) NULL ,
  `state` VARCHAR(45) NULL ,
  PRIMARY KEY (`applicationId`) ,
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VM`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VM` ;

CREATE  TABLE IF NOT EXISTS `VM` (
  `vmId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `providerId` INT NOT NULL ,
  `applicationId` INT NULL ,
  PRIMARY KEY (`vmId`) ,
  INDEX `fk_VM_Provider1_idx` (`providerId` ASC) ,
  INDEX `fk_VM_Application1` (`applicationId` ASC) ,
  CONSTRAINT `fk_VM_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_VM_Application1`
    FOREIGN KEY (`applicationId` )
    REFERENCES `Application` (`applicationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CEE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CEE` ;

CREATE  TABLE IF NOT EXISTS `CEE` (
  `ceeId` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `voId` INT NOT NULL ,
  PRIMARY KEY (`ceeId`) ,
  INDEX `fk_CEE_VirtualOrganization1_idx` (`voId` ASC) ,
  CONSTRAINT `fk_CEE_VirtualOrganization1`
    FOREIGN KEY (`voId` )
    REFERENCES `VO` (`voId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EDC`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EDC` ;

CREATE  TABLE IF NOT EXISTS `EDC` (
  `edcId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` VARCHAR(45) NULL ,
  PRIMARY KEY (`edcId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VO_has_EDC`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VO_has_EDC` ;

CREATE  TABLE IF NOT EXISTS `VO_has_EDC` (
  `voId` INT NOT NULL ,
  `edcId` INT NOT NULL ,
  PRIMARY KEY (`voId`, `edcId`) ,
  INDEX `fk_VO_has_EDC_EDC1_idx` (`edcId` ASC) ,
  INDEX `fk_VO_has_EDC_VO1_idx` (`voId` ASC) ,
  CONSTRAINT `fk_VO_has_EDC_VO1`
    FOREIGN KEY (`voId` )
    REFERENCES `VO` (`voId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_VO_has_EDC_EDC1`
    FOREIGN KEY (`edcId` )
    REFERENCES `EDC` (`edcId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VO_has_Cluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VO_has_Cluster` ;

CREATE  TABLE IF NOT EXISTS `VO_has_Cluster` (
  `voId` INT NOT NULL ,
  `clusterId` INT NOT NULL ,
  PRIMARY KEY (`voId`, `clusterId`) ,
  INDEX `fk_VO_has_Cluster_Cluster1_idx` (`clusterId` ASC) ,
  INDEX `fk_VO_has_Cluster_VO1_idx` (`voId` ASC) ,
  CONSTRAINT `fk_VO_has_Cluster_VO1`
    FOREIGN KEY (`voId` )
    REFERENCES `VO` (`voId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_VO_has_Cluster_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ovf`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ovf` ;

CREATE  TABLE IF NOT EXISTS `Ovf` (
  `ovfId` INT NOT NULL AUTO_INCREMENT ,
  `providerId` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `content` TEXT NULL ,
  PRIMARY KEY (`ovfId`) ,
  INDEX `fk_Ovf_Provider1_idx` (`providerId` ASC) ,
  CONSTRAINT `fk_Ovf_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserOvf`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserOvf` ;

CREATE  TABLE IF NOT EXISTS `UserOvf` (
  `ovfId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `attributes` TEXT NULL ,
  `content` TEXT NULL ,
  `userId` INT NOT NULL ,
  `providerOvfId` INT ,
  PRIMARY KEY (`ovfId`) ,
  INDEX `fk_UserOvf_User1_idx` (`userId` ASC) ,
  INDEX `fk_UserOvf_Ovf1_idx` (`providerOvfId` ASC) ,
  CONSTRAINT `fk_UserOvf_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserOvf_Ovf1`
    FOREIGN KEY (`providerOvfId` )
    REFERENCES `Ovf` (`ovfId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cluster_has_Server`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster_has_Server` ;

CREATE  TABLE IF NOT EXISTS `Cluster_has_Server` (
  `clusterId` INT NOT NULL ,
  `serverId` INT NOT NULL ,
  PRIMARY KEY (`clusterId`, `serverId`) ,
  INDEX `fk_Cluster_has_Server_Server1_idx` (`serverId` ASC) ,
  INDEX `fk_Cluster_has_Server_Cluster1_idx` (`clusterId` ASC) ,
  CONSTRAINT `fk_Cluster_has_Server_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Cluster_has_Server_Server1`
    FOREIGN KEY (`serverId` )
    REFERENCES `Server` (`serverId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cluster_has_VM`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster_has_VM` ;

CREATE  TABLE IF NOT EXISTS `Cluster_has_VM` (
  `clusterId` INT NOT NULL ,
  `vmId` INT NOT NULL ,
  PRIMARY KEY (`clusterId`, `vmId`) ,
  INDEX `fk_Cluster_has_VM_VM1_idx` (`vmId` ASC) ,
  INDEX `fk_Cluster_has_VM_Cluster1_idx` (`clusterId` ASC) ,
  CONSTRAINT `fk_Cluster_has_VM_Cluster1`
    FOREIGN KEY (`clusterId` )
    REFERENCES `Cluster` (`clusterId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Cluster_has_VM_VM1`
    FOREIGN KEY (`vmId` )
    REFERENCES `VM` (`vmId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `URole`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `URole` ;

CREATE  TABLE IF NOT EXISTS `URole` (
  `roleId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `description` VARCHAR(255) NULL ,
  `acl` TEXT NULL ,
  PRIMARY KEY (`roleId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UGroup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UGroup` ;

CREATE  TABLE IF NOT EXISTS `UGroup` (
  `groupId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `description` VARCHAR(255) NULL ,
  PRIMARY KEY (`groupId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Attribute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Attribute` ;

CREATE  TABLE IF NOT EXISTS `Attribute` (
  `attributeUuid` VARCHAR(36) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `uri` VARCHAR(255) NULL ,
  `defaultValue` VARCHAR(255) NULL ,
  `reference` VARCHAR(45) NULL ,
  `description` VARCHAR(255) NULL ,
  PRIMARY KEY (`attributeUuid`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User_has_Attribute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User_has_Attribute` ;

CREATE  TABLE IF NOT EXISTS `User_has_Attribute` (
  `userId` INT NOT NULL ,
  `attributeUuid` VARCHAR(36) NOT NULL ,
  `value` VARCHAR(255) NULL ,
  `referenceId` INT NULL ,
  PRIMARY KEY (`userId`, `attributeUuid`) ,
  INDEX `fk_User_has_Attribute_Attribute1_idx` (`attributeUuid` ASC) ,
  INDEX `fk_User_has_Attribute_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_Attribute_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Attribute_Attribute1`
    FOREIGN KEY (`attributeUuid` )
    REFERENCES `Attribute` (`attributeUuid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `IdentityProvider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IdentityProvider` ;

CREATE  TABLE IF NOT EXISTS `IdentityProvider` (
  `idpUuid` VARCHAR(36) NOT NULL ,
  `providerURI` VARCHAR(255) NOT NULL ,
  `description` VARCHAR(255) NULL ,
  `providerName` VARCHAR(255) NULL ,
  `attributes` TEXT NULL ,
  PRIMARY KEY (`idpUuid`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User_has_identityProvider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User_has_identityProvider` ;

CREATE  TABLE IF NOT EXISTS `User_has_identityProvider` (
  `userId` INT NOT NULL ,
  `idpUuid` VARCHAR(36) NOT NULL ,
  `identity` VARCHAR(255) NOT NULL ,
  `attributes` TEXT NULL ,
  PRIMARY KEY (`userId`, `idpUuid`) ,
  INDEX `fk_User_has_identityProvider_identityProvider1_idx` (`idpUuid` ASC) ,
  INDEX `fk_User_has_identityProvider_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_identityProvider_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_identityProvider_identityProvider1`
    FOREIGN KEY (`idpUuid` )
    REFERENCES `IdentityProvider` (`idpUuid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User_has_Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User_has_Role` ;

CREATE  TABLE IF NOT EXISTS `User_has_Role` (
  `userId` INT NOT NULL ,
  `roleId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `roleId`) ,
  INDEX `fk_User_has_Role_Role1_idx` (`roleId` ASC) ,
  INDEX `fk_User_has_Role_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_Role_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Role_Role1`
    FOREIGN KEY (`roleId` )
    REFERENCES `URole` (`roleId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User_has_Group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User_has_Group` ;

CREATE  TABLE IF NOT EXISTS `User_has_Group` (
  `userId` INT NOT NULL ,
  `groupId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `groupId`) ,
  INDEX `fk_User_has_Group_Group1_idx` (`groupId` ASC) ,
  INDEX `fk_User_has_Group_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_Group_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Group_Group1`
    FOREIGN KEY (`groupId` )
    REFERENCES `UGroup` (`groupId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Account` ;

CREATE  TABLE IF NOT EXISTS `Account` (
  `userId` INT NOT NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `providerId`) ,
  INDEX `fk_User_has_Provider_Provider1_idx` (`providerId` ASC) ,
  INDEX `fk_User_has_Provider_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_Provider_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Provider_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User_has_Application`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User_has_Application` ;

CREATE  TABLE IF NOT EXISTS `User_has_Application` (
  `userId` INT NOT NULL ,
  `applicationId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `applicationId`) ,
  INDEX `fk_User_has_Application_Application1_idx` (`applicationId` ASC) ,
  INDEX `fk_User_has_Application_User1_idx` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_Application_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Application_Application1`
    FOREIGN KEY (`applicationId` )
    REFERENCES `Application` (`applicationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Application_has_Ovf`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Application_has_Ovf` ;

CREATE  TABLE IF NOT EXISTS `Application_has_Ovf` (
  `applicationId` INT NOT NULL ,
  `ovfId` INT NOT NULL ,
  PRIMARY KEY (`applicationId`, `ovfId`) ,
  INDEX `fk_Application_has_Ovf_Ovf1_idx` (`ovfId` ASC) ,
  INDEX `fk_Application_has_Ovf_Application1_idx` (`applicationId` ASC) ,
  CONSTRAINT `fk_Application_has_Ovf_Application1`
    FOREIGN KEY (`applicationId` )
    REFERENCES `Application` (`applicationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Application_has_Ovf_Ovf1`
    FOREIGN KEY (`ovfId` )
    REFERENCES `UserOvf` (`ovfId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SLATemplate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SLATemplate` ;

CREATE  TABLE IF NOT EXISTS `SLATemplate` (
  `slatId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `url` VARCHAR(255) NOT NULL ,
  `content` TEXT NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`slatId`) ,
  INDEX `fk_SLATemplate_Provider1_idx` (`providerId` ASC) ,
  CONSTRAINT `fk_SLATemplate_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserSLATemplate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserSLATemplate` ;

CREATE  TABLE IF NOT EXISTS `UserSLATemplate` (
  `SLATemplateId` INT NOT NULL AUTO_INCREMENT ,
  `url` VARCHAR(255) NULL ,
  `content` TEXT NULL ,
  `userId` INT NOT NULL ,
  `slatId` INT NULL ,
  `name` VARCHAR(45) NULL ,
  `ovfId` INT NOT NULL ,
  PRIMARY KEY (`SLATemplateId`) ,
  INDEX `fk_SLATemplate_User1_idx` (`userId` ASC) ,
  INDEX `fk_UserSLATemplate_SLATemplate1_idx` (`slatId` ASC) ,
  INDEX `fk_UserSLATemplate_UserOvf1` (`ovfId` ASC) ,
  CONSTRAINT `fk_SLATemplate_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserSLATemplate_SLATemplate1`
    FOREIGN KEY (`slatId` )
    REFERENCES `SLATemplate` (`slatId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserSLATemplate_UserOvf1`
    FOREIGN KEY (`ovfId` )
    REFERENCES `UserOvf` (`ovfId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserSLA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserSLA` ;

CREATE  TABLE IF NOT EXISTS `UserSLA` (
  `SLAId` INT NOT NULL AUTO_INCREMENT ,
  `sla` TEXT NULL ,
  `templateURL` VARCHAR(255) NULL ,
  `content` TEXT NULL ,
  `userId` INT NOT NULL ,
  `SLATemplateId` INT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`SLAId`) ,
  INDEX `fk_SLAs_User1_idx` (`userId` ASC) ,
  INDEX `fk_UserSLA_UserSLATemplate1_idx` (`SLATemplateId` ASC) ,
  CONSTRAINT `fk_SLAs_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserSLA_UserSLATemplate1`
    FOREIGN KEY (`SLATemplateId` )
    REFERENCES `UserSLATemplate` (`SLATemplateId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SLATemplateProposal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SLATemplateProposal` ;

CREATE  TABLE IF NOT EXISTS `SLATemplateProposal` (
  `proposalId` INT NOT NULL AUTO_INCREMENT ,
  `content` TEXT NOT NULL ,
  `created` DATETIME NOT NULL ,
  `negotiationId` VARCHAR(64) NOT NULL ,
  `SLATemplateId` INT NOT NULL ,
  PRIMARY KEY (`proposalId`) ,
  INDEX `fk_SLATemplateProposal_UserSLATemplate1_idx` (`SLATemplateId` ASC) ,
  CONSTRAINT `fk_SLATemplateProposal_UserSLATemplate1`
    FOREIGN KEY (`SLATemplateId` )
    REFERENCES `UserSLATemplate` (`SLATemplateId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Provider_has_Application`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Provider_has_Application` ;

CREATE  TABLE IF NOT EXISTS `Provider_has_Application` (
  `providerId` INT NOT NULL ,
  `applicationId` INT NOT NULL ,
  PRIMARY KEY (`providerId`, `applicationId`) ,
  INDEX `fk_Provider_has_Application_Application1` (`applicationId` ASC) ,
  INDEX `fk_Provider_has_Application_Provider1` (`providerId` ASC) ,
  CONSTRAINT `fk_Provider_has_Application_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Provider_has_Application_Application1`
    FOREIGN KEY (`applicationId` )
    REFERENCES `Application` (`applicationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Service`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Service` ;

CREATE  TABLE IF NOT EXISTS `Service` (
  `serviceId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(255) NULL ,
  `address` VARCHAR(255) NULL ,
  `providerId` INT NOT NULL ,
  PRIMARY KEY (`serviceId`) ,
  INDEX `fk_ProviderService_Provider1` (`providerId` ASC) ,
  UNIQUE INDEX `unique_service` (`name` ASC, `providerId` ASC) ,
  CONSTRAINT `fk_ProviderService_Provider1`
    FOREIGN KEY (`providerId` )
    REFERENCES `Provider` (`providerId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SelectionCriterion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SelectionCriterion` ;

CREATE  TABLE IF NOT EXISTS `SelectionCriterion` (
  `scId` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `defaultValue` DOUBLE NULL ,
  PRIMARY KEY (`scId`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserSelectionCriterion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserSelectionCriterion` ;

CREATE  TABLE IF NOT EXISTS `UserSelectionCriterion` (
  `userId` INT NOT NULL ,
  `scId` INT NOT NULL ,
  `value` DOUBLE NULL ,
  PRIMARY KEY (`userId`, `scId`) ,
  INDEX `fk_User_has_SelectionCriteria_SelectionCriteria1` (`scId` ASC) ,
  INDEX `fk_User_has_SelectionCriteria_User1` (`userId` ASC) ,
  CONSTRAINT `fk_User_has_SelectionCriteria_User1`
    FOREIGN KEY (`userId` )
    REFERENCES `User` (`userId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_SelectionCriteria_SelectionCriteria1`
    FOREIGN KEY (`scId` )
    REFERENCES `SelectionCriterion` (`scId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ApplicationSelectionCriterion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ApplicationSelectionCriterion` ;

CREATE  TABLE IF NOT EXISTS `ApplicationSelectionCriterion` (
  `applicationId` INT NOT NULL ,
  `scId` INT NOT NULL ,
  `value` DOUBLE NULL ,
  PRIMARY KEY (`applicationId`, `scId`) ,
  INDEX `fk_Application_has_SelectionCriteria_SelectionCriteria1` (`scId` ASC) ,
  INDEX `fk_Application_has_SelectionCriteria_Application1` (`applicationId` ASC) ,
  CONSTRAINT `fk_Application_has_SelectionCriteria_Application1`
    FOREIGN KEY (`applicationId` )
    REFERENCES `Application` (`applicationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Application_has_SelectionCriteria_SelectionCriteria1`
    FOREIGN KEY (`scId` )
    REFERENCES `SelectionCriterion` (`scId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
