/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author ales
 */
@Entity
@Table(name = "Application")
@NamedQueries({
        @NamedQuery(name = "Application.findAll", query = "SELECT a FROM Application a"),
        @NamedQuery(name = "Application.findByApplicationId", query = "SELECT a FROM Application a WHERE a.applicationId = :applicationId"),
        @NamedQuery(name = "Application.findByUuid", query = "SELECT a FROM Application a WHERE a.uuid = :uuid"),
        @NamedQuery(name = "Application.findByName", query = "SELECT a FROM Application a WHERE a.name = :name"),
        @NamedQuery(name = "Application.findBySlaUrl", query = "SELECT a FROM Application a WHERE a.slaUrl = :slaUrl"),
        @NamedQuery(name = "Application.findByState", query = "SELECT a FROM Application a WHERE a.state = :state")})
public class Application implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "applicationId")
    private Integer applicationId;
    @Basic(optional = false)
    @Column(name = "uuid", unique = true)
    private String uuid;
    @Lob
    @Column(name = "deploymentDesc")
    private String deploymentDesc;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "applicationOvf")
    private String applicationOvf;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @Column(name = "slaUrl")
    private String slaUrl;
    @Column(name = "state")
    private String state;
    @JoinTable(name = "Application_has_Ovf", joinColumns = {
            @JoinColumn(name = "applicationId", referencedColumnName = "applicationId")}, inverseJoinColumns = {
            @JoinColumn(name = "ovfId", referencedColumnName = "ovfId")})
    @ManyToMany
    private List<UserOvf> userOvfList;
    @ManyToMany(mappedBy = "applicationList")
    private List<User> userList;
    @ManyToMany(mappedBy = "applicationList")
    private List<Provider> providerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "applicationId")
    private List<Vm> vmList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "application", fetch = FetchType.LAZY)
    private List<ApplicationSelectionCriterion> applicationSelectionCriterionList;

    public Application() {
    }

    public Application(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDeploymentDesc() {
        return deploymentDesc;
    }

    public void setDeploymentDesc(String deploymentDesc) {
        this.deploymentDesc = deploymentDesc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApplicationOvf() {
        return applicationOvf;
    }

    public void setApplicationOvf(String applicationOvf) {
        this.applicationOvf = applicationOvf;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getSlaUrl() {
        return slaUrl;
    }

    public void setSlaUrl(String slaUrl) {
        this.slaUrl = slaUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<UserOvf> getUserOvfList() {
        return userOvfList;
    }

    public void setUserOvfList(List<UserOvf> userOvfList) {
        this.userOvfList = userOvfList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Provider> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<Provider> providerList) {
        this.providerList = providerList;
    }

    public List<Vm> getVmList() {
        return vmList;
    }

    public void setVmList(List<Vm> vmList) {
        this.vmList = vmList;
    }

    public List<ApplicationSelectionCriterion> getApplicationSelectionCriterionList() {
        return applicationSelectionCriterionList;
    }

    public void setApplicationSelectionCriterionList(List<ApplicationSelectionCriterion> applicationSelectionCriterionList) {
        this.applicationSelectionCriterionList = applicationSelectionCriterionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (applicationId != null ? applicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Application)) {
            return false;
        }
        Application other = (Application) object;
        if ((this.applicationId == null && other.applicationId != null) || (this.applicationId != null && !this.applicationId.equals(other.applicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Application[ applicationId=" + applicationId + " ]";
    }

}
