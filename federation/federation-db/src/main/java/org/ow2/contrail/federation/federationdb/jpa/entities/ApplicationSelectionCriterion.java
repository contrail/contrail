package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ApplicationSelectionCriterion")
@NamedNativeQuery(name = "ApplicationSelectionCriterion.findByAppIdUserId", query =
        "SELECT sc.name, apsc.value, usc.value, sc.defaultValue " +
        "FROM SelectionCriterion sc " +
        "LEFT JOIN UserSelectionCriterion usc ON (usc.scId = sc.scId) AND usc.userId = ? " +
        "LEFT JOIN ApplicationSelectionCriterion apsc ON (apsc.scId = sc.scId) AND apsc.applicationId = ?")
public class ApplicationSelectionCriterion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ApplicationSelectionCriterionPK applicationSelectionCriterionPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(precision = 22)
    private Double value;
    @JoinColumn(name = "scId", referencedColumnName = "scId", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SelectionCriterion selectionCriterion;
    @JoinColumn(name = "applicationId", referencedColumnName = "applicationId", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Application application;

    public ApplicationSelectionCriterion() {
    }

    public ApplicationSelectionCriterion(ApplicationSelectionCriterionPK applicationSelectionCriterionPK) {
        this.applicationSelectionCriterionPK = applicationSelectionCriterionPK;
    }

    public ApplicationSelectionCriterion(int applicationId, int scId) {
        this.applicationSelectionCriterionPK = new ApplicationSelectionCriterionPK(applicationId, scId);
    }

    public ApplicationSelectionCriterionPK getApplicationSelectionCriterionPK() {
        return applicationSelectionCriterionPK;
    }

    public void setApplicationSelectionCriterionPK(ApplicationSelectionCriterionPK applicationSelectionCriterionPK) {
        this.applicationSelectionCriterionPK = applicationSelectionCriterionPK;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public SelectionCriterion getSelectionCriterion() {
        return selectionCriterion;
    }

    public void setSelectionCriterion(SelectionCriterion selectionCriterion) {
        this.selectionCriterion = selectionCriterion;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (applicationSelectionCriterionPK != null ? applicationSelectionCriterionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationSelectionCriterion)) {
            return false;
        }
        ApplicationSelectionCriterion other = (ApplicationSelectionCriterion) object;
        if ((this.applicationSelectionCriterionPK == null && other.applicationSelectionCriterionPK != null) || (this.applicationSelectionCriterionPK != null && !this.applicationSelectionCriterionPK.equals(other.applicationSelectionCriterionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.ApplicationSelectionCriterion[ applicationSelectionCriterionPK=" + applicationSelectionCriterionPK + " ]";
    }
    
}
