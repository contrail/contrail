package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.URole;

import javax.persistence.EntityManager;

public class URoleDAO extends BaseDAO {

    public URoleDAO() {
    }

    public URoleDAO(EntityManager em) {
        super(em);
    }

    public URole findById(int roleId) {
        try {
            return em.find(URole.class, roleId);
        }
        finally {
            closeEntityManager();
        }
    }
}
