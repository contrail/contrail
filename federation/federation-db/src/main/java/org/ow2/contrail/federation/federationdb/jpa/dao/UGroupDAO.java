package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.UGroup;

import javax.persistence.EntityManager;

public class UGroupDAO extends BaseDAO {

    public UGroupDAO() {
    }

    public UGroupDAO(EntityManager em) {
        super(em);
    }

    public UGroup findById(int gId) {
        try {
            return em.find(UGroup.class, gId);
        }
        finally {
            closeEntityManager();
        }
    }
}
