package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UserDAO extends BaseDAO {

    public UserDAO() {
    }

    public UserDAO(EntityManager em) {
        super(em);
    }

    public User findById(int userId) {
        try {
            return em.find(User.class, userId);
        }
        finally {
            closeEntityManager();
        }
    }

    public User findByUuid(String uuid) {
        try {
            Query query = em.createNamedQuery("User.findByUuid");
            query.setParameter("uuid", uuid);
            return (User) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }

    public User findByEmail(String email) {
        try {
            Query query = em.createNamedQuery("User.findByEmail");
            query.setParameter("email", email);
            try{
            	// Get the first result from the list
            	return (User)query.getResultList().get(0);
            }
            catch(IndexOutOfBoundsException iobe){
            	// In case there is no result
            	return null;
            }
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }

    /**
     * Search the database for user with given user name.
     *
     * @param username
     * @return
     */
    public User findByUsername(String username) {
        try {
            Query query = em.createNamedQuery("User.findByUsername");
            query.setParameter("username", username);
            return (User) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }
}
