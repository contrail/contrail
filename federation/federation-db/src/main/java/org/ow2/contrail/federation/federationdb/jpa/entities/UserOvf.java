/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "UserOvf")
@NamedQueries({
    @NamedQuery(name = "UserOvf.findAll", query = "SELECT u FROM UserOvf u"),
    @NamedQuery(name = "UserOvf.findByOvfId", query = "SELECT u FROM UserOvf u WHERE u.ovfId = :ovfId"),
    @NamedQuery(name = "UserOvf.findByName", query = "SELECT u FROM UserOvf u WHERE u.name = :name")})
public class UserOvf implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ovfId")
    private Integer ovfId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @Lob
    @Column(name = "content")
    private String content;
    @ManyToMany(mappedBy = "userOvfList")
    private List<Application> applicationList;
    @JoinColumn(name = "providerOvfId", referencedColumnName = "ovfId")
    @ManyToOne(optional = false)
    private Ovf providerOvfId;
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private User userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userOvf")
    private List<UserSLATemplate> userSLATemplateList;

    public UserOvf() {
    }

    public UserOvf(Integer ovfId) {
        this.ovfId = ovfId;
    }

    public Integer getOvfId() {
        return ovfId;
    }

    public void setOvfId(Integer ovfId) {
        this.ovfId = ovfId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Application> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(List<Application> applicationList) {
        this.applicationList = applicationList;
    }

    public Ovf getProviderOvfId() {
        return providerOvfId;
    }

    public void setProviderOvfId(Ovf providerOvfId) {
        this.providerOvfId = providerOvfId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public List<UserSLATemplate> getUserSLATemplateList() {
        return userSLATemplateList;
    }

    public void setUserSLATemplateList(List<UserSLATemplate> userSLATemplateList) {
        this.userSLATemplateList = userSLATemplateList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ovfId != null ? ovfId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserOvf)) {
            return false;
        }
        UserOvf other = (UserOvf) object;
        if ((this.ovfId == null && other.ovfId != null) || (this.ovfId != null && !this.ovfId.equals(other.ovfId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserOvf[ ovfId=" + ovfId + " ]";
    }
    
}
