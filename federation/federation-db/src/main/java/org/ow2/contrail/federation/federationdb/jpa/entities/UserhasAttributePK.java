/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 *
 * @author ales
 */
@Embeddable
public class UserhasAttributePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "userId")
    private int userId;
    @Basic(optional = false)
    @Column(name = "attributeUuid ")
    private String attributeUuid ;

    public UserhasAttributePK() {
    }

    public UserhasAttributePK(int userId, String attributeUuid ) {
        this.userId = userId;
        this.attributeUuid  = attributeUuid ;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAttributeUuid () {
        return attributeUuid ;
    }

    public void setAttributeUuid (String attributeUuid ) {
        this.attributeUuid  = attributeUuid ;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (attributeUuid != null ? attributeUuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserhasAttributePK)) {
            return false;
        }
        UserhasAttributePK other = (UserhasAttributePK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.attributeUuid == null && other.attributeUuid != null) || (this.attributeUuid != null && !this.attributeUuid.equals(other.attributeUuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserhasAttributePK[ userId=" + userId + ", attributeUuid =" + attributeUuid  + " ]";
    }

}
