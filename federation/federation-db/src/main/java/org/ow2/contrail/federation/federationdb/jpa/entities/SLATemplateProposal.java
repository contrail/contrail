package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SLATemplateProposal")
@NamedQueries({
    @NamedQuery(name = "SLATemplateProposal.findAll", query = "SELECT s FROM SLATemplateProposal s"),
    @NamedQuery(name = "SLATemplateProposal.findByProposalId", query = "SELECT s FROM SLATemplateProposal s WHERE s.proposalId = :proposalId"),
    @NamedQuery(name = "SLATemplateProposal.findByCreated", query = "SELECT s FROM SLATemplateProposal s WHERE s.created = :created"),
    @NamedQuery(name = "SLATemplateProposal.findByNegotiationId", query = "SELECT s FROM SLATemplateProposal s WHERE s.negotiationId = :negotiationId")})
public class SLATemplateProposal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "proposalId")
    private Integer proposalId;
    @Basic(optional = false)
    @Column(name = "negotiationId")
    private String negotiationId;
    @Lob
    @Column(name = "content", columnDefinition = "TEXT")
    private String content;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created")
    private Date created;
    @JoinColumn(name = "SLATemplateId", referencedColumnName = "SLATemplateId")
    @ManyToOne(optional = false)
    private UserSLATemplate userSLATemplate;

    public SLATemplateProposal() {
    }

    public SLATemplateProposal(Integer proposalId) {
        this.proposalId = proposalId;
    }

    public SLATemplateProposal(Integer proposalId, String content, Date created, String negotiationId) {
        this.proposalId = proposalId;
        this.content = content;
        this.created = created;
        this.negotiationId = negotiationId;
    }

    public Integer getProposalId() {
        return proposalId;
    }

    public void setProposalId(Integer proposalId) {
        this.proposalId = proposalId;
    }

    public String getNegotiationId() {
        return negotiationId;
    }

    public void setNegotiationId(String negotiationId) {
        this.negotiationId = negotiationId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public UserSLATemplate getUserSLATemplate() {
        return userSLATemplate;
    }

    public void setUserSLATemplate(UserSLATemplate userSLATemplate) {
        this.userSLATemplate = userSLATemplate;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("proposalId", proposalId);
        json.put("content", content);
        json.put("created", created);
        json.put("userSLATemplateId", userSLATemplate.getSLATemplateId());
        return json;
    }

 public void setSLATemplateId(UserSLATemplate userSLATemplate) {
        this.userSLATemplate = userSLATemplate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proposalId != null ? proposalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SLATemplateProposal)) {
            return false;
        }
        SLATemplateProposal other = (SLATemplateProposal) object;
        if ((this.proposalId == null && other.proposalId != null) || (this.proposalId != null && !this.proposalId.equals(other.proposalId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplateProposal[ proposalId=" + proposalId + " ]";
    }
}

