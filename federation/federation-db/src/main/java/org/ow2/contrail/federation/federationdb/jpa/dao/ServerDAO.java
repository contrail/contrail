package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Server;

import javax.persistence.EntityManager;

public class ServerDAO extends BaseDAO {

    public ServerDAO() {
    }

    public ServerDAO(EntityManager em) {
        super(em);
    }

    public Server findById(int providerId, int serverId) {
        try {
            Server server = em.find(Server.class, serverId);
            if (server == null || !server.getProviderId().getProviderId().equals(providerId)) {
                return null;
            }
            else {
                return server;
            }
        }
        finally {
            closeEntityManager();
        }
    }

    public Server findById(String providerUuid, int serverId) {
        try {
            Server server = em.find(Server.class, serverId);
            if (server == null || !server.getProviderId().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return server;
            }
        }
        finally {
            closeEntityManager();
        }
    }
}
