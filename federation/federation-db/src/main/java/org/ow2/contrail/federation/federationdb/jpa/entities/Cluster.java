/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * @author ales
 */
@Entity
@Table(name = "Cluster", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "providerId"}))
@NamedQueries({
        @NamedQuery(name = "Cluster.findAll", query = "SELECT c FROM Cluster c"),
        @NamedQuery(name = "Cluster.findByClusterId", query = "SELECT c FROM Cluster c WHERE c.clusterId = :clusterId"),
        @NamedQuery(name = "Cluster.findByName", query = "SELECT c FROM Cluster c WHERE c.name = :name")})
public class Cluster implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "clusterId")
    private Integer clusterId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @JoinTable(name = "Cluster_has_Network", joinColumns = {
            @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")}, inverseJoinColumns = {
            @JoinColumn(name = "networkId", referencedColumnName = "networkId")})
    @ManyToMany
    private List<Network> networkList;
    @ManyToMany(mappedBy = "clusterList")
    private List<Vo> voList;
    @JoinTable(name = "Cluster_has_Server", joinColumns = {
            @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")}, inverseJoinColumns = {
            @JoinColumn(name = "serverId", referencedColumnName = "serverId")})
    @ManyToMany
    private List<Server> serverList;
    @JoinTable(name = "Cluster_has_VM", joinColumns = {
            @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")}, inverseJoinColumns = {
            @JoinColumn(name = "vmId", referencedColumnName = "vmId")})
    @ManyToMany
    private List<Vm> vmList;
    @ManyToMany(mappedBy = "clusterList")
    private List<DataCenter> dataCenterList;
    @JoinTable(name = "Cluster_has_Storage", joinColumns = {
            @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")}, inverseJoinColumns = {
            @JoinColumn(name = "storageId", referencedColumnName = "storageId")})
    @ManyToMany
    private List<Storage> storageList;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;

    public Cluster() {
    }

    public Cluster(JSONObject json) throws JSONException {
        String name = (String) json.remove("name");
        this.setName(name);
        this.setAttributes(json.toString());
    }


    public Cluster(Integer clusterId) {
        this.clusterId = clusterId;
    }

    public Integer getClusterId() {
        return clusterId;
    }

    public void setClusterId(Integer clusterId) {
        this.clusterId = clusterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(List<Network> networkList) {
        this.networkList = networkList;
    }

    public List<Vo> getVoList() {
        return voList;
    }

    public void setVoList(List<Vo> voList) {
        this.voList = voList;
    }

    public List<Server> getServerList() {
        return serverList;
    }

    public void setServerList(List<Server> serverList) {
        this.serverList = serverList;
    }

    public List<Vm> getVmList() {
        return vmList;
    }

    public void setVmList(List<Vm> vmList) {
        this.vmList = vmList;
    }

    public List<DataCenter> getDataCenterList() {
        return dataCenterList;
    }

    public void setDataCenterList(List<DataCenter> dataCenterList) {
        this.dataCenterList = dataCenterList;
    }

    public List<Storage> getStorageList() {
        return storageList;
    }

    public void setStorageList(List<Storage> storageList) {
        this.storageList = storageList;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clusterId != null ? clusterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cluster)) {
            return false;
        }
        Cluster other = (Cluster) object;
        if ((this.clusterId == null && other.clusterId != null) || (this.clusterId != null && !this.clusterId.equals(other.clusterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Cluster[ clusterId=" + clusterId + " ]";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject(attributes);
        json.put("name", name);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String) json.remove("name");
        }

        JSONObject attrs = new JSONObject(attributes);
        Iterator it = json.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = json.get(key);
            attrs.put(key, value);
        }
        attributes = attrs.toString();
    }
}
