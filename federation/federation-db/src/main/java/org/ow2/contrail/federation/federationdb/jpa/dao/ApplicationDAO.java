/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Application;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 * @author ales
 */
public class ApplicationDAO extends BaseDAO {

    public ApplicationDAO() {
    }

    public ApplicationDAO(EntityManager em) {
        super(em);
    }

    public Application findById(int appId) {
        try {
            return em.find(Application.class, appId);
        }
        finally {
            closeEntityManager();
        }
    }

    public Application findByUuid(String uuid) {
        try {
            Query q = em.createNamedQuery("Application.findByUuid");
            q.setParameter("uuid", uuid);
            return (Application) q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }
}
