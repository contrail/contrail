/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 *
 * @author ales
 */
@Embeddable
public class UserhasidentityProviderPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "userId")
    private int userId;
    @Basic(optional = false)
    @Column(name = "idpUuid")
    private String idpUuid;

    public UserhasidentityProviderPK() {
    }

    public UserhasidentityProviderPK(int userId, String idpUuid) {
        this.userId = userId;
        this.idpUuid = idpUuid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getIdpUuid() {
        return idpUuid;
    }

    public void setIdpUuid(String idpUuid) {
        this.idpUuid = idpUuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (idpUuid != null ? idpUuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserhasidentityProviderPK)) {
            return false;
        }
        UserhasidentityProviderPK other = (UserhasidentityProviderPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.idpUuid == null && other.idpUuid != null) || (this.idpUuid != null && !this.idpUuid.equals(other.idpUuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProviderPK[ userId=" + userId + ", idpUuid=" + idpUuid + " ]";
    }
    
}
