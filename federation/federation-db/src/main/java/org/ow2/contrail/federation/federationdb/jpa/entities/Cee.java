/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "CEE")
@NamedQueries({
    @NamedQuery(name = "Cee.findAll", query = "SELECT c FROM Cee c"),
    @NamedQuery(name = "Cee.findByCeeId", query = "SELECT c FROM Cee c WHERE c.ceeId = :ceeId"),
    @NamedQuery(name = "Cee.findByName", query = "SELECT c FROM Cee c WHERE c.name = :name")})
public class Cee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ceeId")
    private Integer ceeId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @JoinColumn(name = "voId", referencedColumnName = "voId")
    @ManyToOne(optional = false)
    private Vo voId;

    public Cee() {
    }

    public Cee(Integer ceeId) {
        this.ceeId = ceeId;
    }

    public Integer getCeeId() {
        return ceeId;
    }

    public void setCeeId(Integer ceeId) {
        this.ceeId = ceeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public Vo getVoId() {
        return voId;
    }

    public void setVoId(Vo voId) {
        this.voId = voId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ceeId != null ? ceeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cee)) {
            return false;
        }
        Cee other = (Cee) object;
        if ((this.ceeId == null && other.ceeId != null) || (this.ceeId != null && !this.ceeId.equals(other.ceeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Cee[ ceeId=" + ceeId + " ]";
    }
    
}
