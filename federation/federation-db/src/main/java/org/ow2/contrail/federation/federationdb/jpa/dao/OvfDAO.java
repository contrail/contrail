package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Ovf;

import javax.persistence.EntityManager;

public class OvfDAO extends BaseDAO {

    public OvfDAO() {
    }

    public OvfDAO(EntityManager em) {
        super(em);
    }

    public Ovf findById(int ovfId) {
        try {
            return em.find(Ovf.class, ovfId);
        }
        finally {
            closeEntityManager();
        }
    }
}
