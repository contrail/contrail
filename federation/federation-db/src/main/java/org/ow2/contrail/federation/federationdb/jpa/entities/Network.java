/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "Network")
@NamedQueries({
    @NamedQuery(name = "Network.findAll", query = "SELECT n FROM Network n"),
    @NamedQuery(name = "Network.findByNetworkId", query = "SELECT n FROM Network n WHERE n.networkId = :networkId"),
    @NamedQuery(name = "Network.findByName", query = "SELECT n FROM Network n WHERE n.name = :name")})
public class Network implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "networkId")
    private Integer networkId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @ManyToMany(mappedBy = "networkList")
    private List<Cluster> clusterList;
    @ManyToMany(mappedBy = "networkList")
    private List<DataCenter> dataCenterList;

    public Network() {
    }

    public Network(Integer networkId) {
        this.networkId = networkId;
    }

    public Integer getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Integer networkId) {
        this.networkId = networkId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public List<DataCenter> getDataCenterList() {
        return dataCenterList;
    }

    public void setDataCenterList(List<DataCenter> dataCenterList) {
        this.dataCenterList = dataCenterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (networkId != null ? networkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Network)) {
            return false;
        }
        Network other = (Network) object;
        if ((this.networkId == null && other.networkId != null) || (this.networkId != null && !this.networkId.equals(other.networkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Network[ networkId=" + networkId + " ]";
    }
    
}
