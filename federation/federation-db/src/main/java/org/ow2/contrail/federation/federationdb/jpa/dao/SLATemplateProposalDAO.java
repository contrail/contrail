package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplateProposal;

import javax.persistence.EntityManager;
import java.util.List;

public class SLATemplateProposalDAO extends BaseDAO {

    public SLATemplateProposalDAO() {
    }

    public SLATemplateProposalDAO(EntityManager em) {
        super(em);
    }

    public SLATemplateProposal find(int slatID, String negotiationId, int proposalId) {
        try {
            SLATemplateProposal proposal = em.find(SLATemplateProposal.class, proposalId);
            if (proposal == null || proposal.getUserSLATemplate().getSLATemplateId() != slatID ||
                    !proposal.getNegotiationId().equals(negotiationId)) {
                return null;
            }
            else {
                return proposal;
            }
        }
        finally {
            closeEntityManager();
        }
    }

    public List<SLATemplateProposal> findByNegotiationId(String negotiationId) {
        try {
            @SuppressWarnings("unchecked")
            List<SLATemplateProposal> proposals = em.createNamedQuery("SLATemplateProposal.findByNegotiationId")
                    .setParameter("negotiationId", negotiationId)
                    .getResultList();
            return proposals;
        }
        finally {
            closeEntityManager();
        }
    }
}
