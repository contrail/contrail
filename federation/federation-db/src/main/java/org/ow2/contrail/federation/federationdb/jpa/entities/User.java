/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "\"User\"")
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByUserId", query = "SELECT u FROM User u WHERE u.userId = :userId"),
    @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
    @NamedQuery(name = "User.findByFirstName", query = "SELECT u FROM User u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "User.findByLastName", query = "SELECT u FROM User u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByUuid", query = "SELECT u FROM User u WHERE u.uuid = :uuid")})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userId")
    private Integer userId;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @Basic(optional = false)
    @Column(name = "firstName")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "lastName")
    private String lastName;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "UUID", unique = true)
    private String uuid;
    @JoinTable(name = "Account", joinColumns = {
        @JoinColumn(name = "userId", referencedColumnName = "userId")}, inverseJoinColumns = {
        @JoinColumn(name = "providerId", referencedColumnName = "providerId")})
    @ManyToMany
    private List<Provider> providerList;
    @JoinTable(name = "User_has_Group", joinColumns = {
        @JoinColumn(name = "userId", referencedColumnName = "userId")}, inverseJoinColumns = {
        @JoinColumn(name = "groupId", referencedColumnName = "groupId")})
    @ManyToMany
    private List<UGroup> uGroupList;
    @JoinTable(name = "User_has_Application", joinColumns = {
        @JoinColumn(name = "userId", referencedColumnName = "userId")}, inverseJoinColumns = {
        @JoinColumn(name = "applicationId", referencedColumnName = "applicationId")})
    @ManyToMany
    private List<Application> applicationList;
    @JoinTable(name = "User_has_Role", joinColumns = {
        @JoinColumn(name = "userId", referencedColumnName = "userId")}, inverseJoinColumns = {
        @JoinColumn(name = "roleId", referencedColumnName = "roleId")})
    @ManyToMany
    private List<URole> uRoleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<UserOvf> userOvfList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserhasidentityProvider> userhasidentityProviderList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<UserSLATemplate> userSLATemplateList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<UserSLA> userSLAList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserhasAttribute> userhasAttributeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserSelectionCriterion> userSelectionCriterionList;

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(Integer userId, String username, String firstName, String lastName, String email, String password, String uuid) {
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.uuid = uuid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Provider> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<Provider> providerList) {
        this.providerList = providerList;
    }

    public List<UGroup> getUGroupList() {
        return uGroupList;
    }

    public void setUGroupList(List<UGroup> uGroupList) {
        this.uGroupList = uGroupList;
    }

    public List<Application> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(List<Application> applicationList) {
        this.applicationList = applicationList;
    }

    public List<URole> getURoleList() {
        return uRoleList;
    }

    public void setURoleList(List<URole> uRoleList) {
        this.uRoleList = uRoleList;
    }

    public List<UserOvf> getUserOvfList() {
        return userOvfList;
    }

    public void setUserOvfList(List<UserOvf> userOvfList) {
        this.userOvfList = userOvfList;
    }

    public List<UserhasidentityProvider> getUserhasidentityProviderList() {
        return userhasidentityProviderList;
    }

    public void setUserhasidentityProviderList(List<UserhasidentityProvider> userhasidentityProviderList) {
        this.userhasidentityProviderList = userhasidentityProviderList;
    }

    public List<UserSLATemplate> getUserSLATemplateList() {
        return userSLATemplateList;
    }

    public void setUserSLATemplateList(List<UserSLATemplate> userSLATemplateList) {
        this.userSLATemplateList = userSLATemplateList;
    }

    public List<UserSLA> getUserSLAList() {
        return userSLAList;
    }

    public void setUserSLAList(List<UserSLA> userSLAList) {
        this.userSLAList = userSLAList;
    }

    public List<UserhasAttribute> getUserhasAttributeList() {
        return userhasAttributeList;
    }

    public void setUserhasAttributeList(List<UserhasAttribute> userhasAttributeList) {
        this.userhasAttributeList = userhasAttributeList;
    }

    public List<UserSelectionCriterion> getUserSelectionCriterionList() {
        return userSelectionCriterionList;
    }

    public void setUserSelectionCriterionList(List<UserSelectionCriterion> userSelectionCriterionList) {
        this.userSelectionCriterionList = userSelectionCriterionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.User[ userId=" + userId + " ]";
    }
    
}
