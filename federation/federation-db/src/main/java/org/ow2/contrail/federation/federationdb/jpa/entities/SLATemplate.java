/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author Damjan
 */
@Entity
@Table(name = "SLATemplate", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "providerId"}))
@NamedQueries({
    @NamedQuery(name = "SLATemplate.findAll", query = "SELECT s FROM SLATemplate s"),
    @NamedQuery(name = "SLATemplate.findBySlatId", query = "SELECT s FROM SLATemplate s WHERE s.slatId = :slatId"),
    @NamedQuery(name = "SLATemplate.findByName", query = "SELECT s FROM SLATemplate s WHERE s.name = :name"),
    @NamedQuery(name = "SLATemplate.findByUrl", query = "SELECT s FROM SLATemplate s WHERE s.url = :url")})
public class SLATemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "slatId")
    private Integer slatId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @Lob
    @Column(name = "content")
    private String content;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;
    @OneToMany(mappedBy = "slatId")
    private List<UserSLATemplate> userSLATemplateList;

    public SLATemplate() {
    }

    public SLATemplate(JSONObject json) throws JSONException {
        this.setName((String) json.get("name"));
        this.setUrl((String) json.get("url"));
    }

    public SLATemplate(Integer slatId) {
        this.slatId = slatId;
    }

    public SLATemplate(Integer slatId, String name, String url) {
        this.slatId = slatId;
        this.name = name;
        this.url = url;
    }

    public Integer getSlatId() {
        return slatId;
    }

    public void setSlatId(Integer slatId) {
        this.slatId = slatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    public List<UserSLATemplate> getUserSLATemplateList() {
        return userSLATemplateList;
    }

    public void setUserSLATemplateList(List<UserSLATemplate> userSLATemplateList) {
        this.userSLATemplateList = userSLATemplateList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (slatId != null ? slatId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SLATemplate)) {
            return false;
        }
        SLATemplate other = (SLATemplate) object;
        if ((this.slatId == null && other.slatId != null) || (this.slatId != null && !this.slatId.equals(other.slatId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplate[ slatId=" + slatId + " ]";
    }
    
    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("url", url);
        json.put("content", content);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String)json.get("name");
        }
        if (json.has("url")) {
            url = (String)json.get("url");
        }
        if (json.has("content")) {
            content = (String)json.get("content");
        }
    }
}
