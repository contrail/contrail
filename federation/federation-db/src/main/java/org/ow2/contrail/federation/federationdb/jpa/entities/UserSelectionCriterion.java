package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UserSelectionCriterion")
@NamedNativeQuery(name = "UserSelectionCriterion.findByUserIdJoinSC", query =
        "SELECT sc.name, sc.defaultValue, usc.value FROM UserSelectionCriterion usc RIGHT JOIN " +
                "SelectionCriterion sc ON (usc.scId = sc.scId) AND usc.userId = ?")
public class UserSelectionCriterion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserSelectionCriterionPK userSelectionCriterionPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(precision = 22)
    private Double value;
    @JoinColumn(name = "scId", referencedColumnName = "scId", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SelectionCriterion selectionCriterion;
    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserSelectionCriterion() {
    }

    public UserSelectionCriterion(UserSelectionCriterionPK userSelectionCriterionPK) {
        this.userSelectionCriterionPK = userSelectionCriterionPK;
    }

    public UserSelectionCriterion(int userId, int scId) {
        this.userSelectionCriterionPK = new UserSelectionCriterionPK(userId, scId);
    }

    public UserSelectionCriterionPK getUserSelectionCriterionPK() {
        return userSelectionCriterionPK;
    }

    public void setUserSelectionCriterionPK(UserSelectionCriterionPK userSelectionCriterionPK) {
        this.userSelectionCriterionPK = userSelectionCriterionPK;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public SelectionCriterion getSelectionCriterion() {
        return selectionCriterion;
    }

    public void setSelectionCriterion(SelectionCriterion selectionCriterion) {
        this.selectionCriterion = selectionCriterion;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userSelectionCriterionPK != null ? userSelectionCriterionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSelectionCriterion)) {
            return false;
        }
        UserSelectionCriterion other = (UserSelectionCriterion) object;
        if ((this.userSelectionCriterionPK == null && other.userSelectionCriterionPK != null) || (this.userSelectionCriterionPK != null && !this.userSelectionCriterionPK.equals(other.userSelectionCriterionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserSelectionCriterion[ userSelectionCriterionPK=" + userSelectionCriterionPK + " ]";
    }

}
