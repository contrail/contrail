/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * @author ales
 */
@Entity
@Table(name = "Server", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "providerId"}))
@NamedQueries({
        @NamedQuery(name = "Server.findAll", query = "SELECT s FROM Server s"),
        @NamedQuery(name = "Server.findByServerId", query = "SELECT s FROM Server s WHERE s.serverId = :serverId"),
        @NamedQuery(name = "Server.findByName", query = "SELECT s FROM Server s WHERE s.name = :name")})
public class Server implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "serverId")
    private Integer serverId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @ManyToMany(mappedBy = "serverList")
    private List<Cluster> clusterList;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;

    public Server() {
    }

    public Server(JSONObject json) throws JSONException {
        String name = (String) json.remove("name");
        this.setName(name);
        this.setAttributes(json.toString());
    }

    public Server(Integer serverId) {
        this.serverId = serverId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serverId != null ? serverId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Server)) {
            return false;
        }
        Server other = (Server) object;
        if ((this.serverId == null && other.serverId != null) || (this.serverId != null && !this.serverId.equals(other.serverId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Server[ serverId=" + serverId + " ]";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject(attributes);
        json.put("name", name);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String) json.remove("name");
        }

        JSONObject attrs = new JSONObject(attributes);
        Iterator it = json.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = json.get(key);
            attrs.put(key, value);
        }
        attributes = attrs.toString();
    }
}
