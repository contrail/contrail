/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.IdentityProvider;

import javax.persistence.EntityManager;

/**
 * @author ales
 */
public class IdentityProviderDAO extends BaseDAO {

    public IdentityProviderDAO() {
    }

    public IdentityProviderDAO(EntityManager em) {
        super(em);
    }

    public IdentityProvider findByUuid(String idpUuid) {
        try {
            return em.find(IdentityProvider.class, idpUuid);
        }
        finally {
            closeEntityManager();
        }
    }

}
