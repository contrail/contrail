/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.UserOvf;

import javax.persistence.EntityManager;

/**
 * @author ales
 */
public class UserOvfDAO extends BaseDAO {

    public UserOvfDAO() {
    }

    public UserOvfDAO(EntityManager em) {
        super(em);
    }

    public UserOvf findById(int ovfid) {
        try {
            return em.find(UserOvf.class, ovfid);
        }
        finally {
            closeEntityManager();
        }
    }

    public UserOvf findById(String userUuid, int ovfid) {
        try {
            UserOvf ovf = em.find(UserOvf.class, ovfid);
            if (ovf.getUserId().getUuid().equals(userUuid)) {
                return ovf;
            }
            else {
                return null;
            }
        }
        finally {
            closeEntityManager();
        }
    }
}
