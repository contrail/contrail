/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "Storage")
@NamedQueries({
    @NamedQuery(name = "Storage.findAll", query = "SELECT s FROM Storage s"),
    @NamedQuery(name = "Storage.findByStorageId", query = "SELECT s FROM Storage s WHERE s.storageId = :storageId"),
    @NamedQuery(name = "Storage.findByName", query = "SELECT s FROM Storage s WHERE s.name = :name")})
public class Storage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "storageId")
    private Integer storageId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @ManyToMany(mappedBy = "storageList")
    private List<DataCenter> dataCenterList;
    @ManyToMany(mappedBy = "storageList")
    private List<Cluster> clusterList;

    public Storage() {
    }

    public Storage(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<DataCenter> getDataCenterList() {
        return dataCenterList;
    }

    public void setDataCenterList(List<DataCenter> dataCenterList) {
        this.dataCenterList = dataCenterList;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (storageId != null ? storageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Storage)) {
            return false;
        }
        Storage other = (Storage) object;
        if ((this.storageId == null && other.storageId != null) || (this.storageId != null && !this.storageId.equals(other.storageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Storage[ storageId=" + storageId + " ]";
    }
    
}
