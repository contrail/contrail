/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasAttribute;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasAttributePK;

import javax.persistence.EntityManager;

/**
 * @author ales
 */
public class UserhasAttributeDAO extends BaseDAO {

    public UserhasAttributeDAO() {
    }

    public UserhasAttributeDAO(EntityManager em) {
        super(em);
    }

    public UserhasAttribute findByUuid(int usrId, String attributeUuid) {
        try {
            return em.find(UserhasAttribute.class, new UserhasAttributePK(usrId, attributeUuid));
        }
        finally {
            closeEntityManager();
        }
    }
}
