package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLA;

import javax.persistence.EntityManager;

public class UserSLADAO extends BaseDAO {

    public UserSLADAO() {
    }

    public UserSLADAO(EntityManager em) {
        super(em);
    }

    public UserSLA findById(int slaid) {
        try {
            return em.find(UserSLA.class, slaid);
        }
        finally {
            closeEntityManager();
        }
    }

}
