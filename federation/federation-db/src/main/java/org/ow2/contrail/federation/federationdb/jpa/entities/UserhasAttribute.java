/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "User_has_Attribute")
@NamedQueries({
    @NamedQuery(name = "UserhasAttribute.findAll", query = "SELECT u FROM UserhasAttribute u"),
    @NamedQuery(name = "UserhasAttribute.findByUserId", query = "SELECT u FROM UserhasAttribute u WHERE u.userhasAttributePK.userId = :userId"),
    @NamedQuery(name = "UserhasAttribute.findByAttributeUuid", query = "SELECT u FROM UserhasAttribute u WHERE u.userhasAttributePK.attributeUuid = :attributeUuid"),
    @NamedQuery(name = "UserhasAttribute.findByValue", query = "SELECT u FROM UserhasAttribute u WHERE u.value = :value"),
    @NamedQuery(name = "UserhasAttribute.findByReferenceId", query = "SELECT u FROM UserhasAttribute u WHERE u.referenceId = :referenceId")})
public class UserhasAttribute implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserhasAttributePK userhasAttributePK;
    @Column(name = "value")
    private String value;
    @Column(name = "referenceId")
    private Integer referenceId;
    @JoinColumn(name = "userId", referencedColumnName = "userId", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;
    @JoinColumn(name = "attributeUuid", referencedColumnName = "attributeUuid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Attribute attribute;

    public UserhasAttribute() {
    }

    public UserhasAttribute(UserhasAttributePK userhasAttributePK) {
        this.userhasAttributePK = userhasAttributePK;
    }

    public UserhasAttribute(int userId, String attributeUuid) {
        this.userhasAttributePK = new UserhasAttributePK(userId, attributeUuid);
    }

    public UserhasAttributePK getUserhasAttributePK() {
        return userhasAttributePK;
    }

    public void setUserhasAttributePK(UserhasAttributePK userhasAttributePK) {
        this.userhasAttributePK = userhasAttributePK;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }


    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userhasAttributePK != null ? userhasAttributePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserhasAttribute)) {
            return false;
        }
        UserhasAttribute other = (UserhasAttribute) object;
        if ((this.userhasAttributePK == null && other.userhasAttributePK != null) || (this.userhasAttributePK != null && !this.userhasAttributePK.equals(other.userhasAttributePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserhasAttribute[ userhasAttributePK=" + userhasAttributePK + " ]";
    }
    
}
