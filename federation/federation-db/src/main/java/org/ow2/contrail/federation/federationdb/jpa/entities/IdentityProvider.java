/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "IdentityProvider")
@NamedQueries({
    @NamedQuery(name = "IdentityProvider.findAll", query = "SELECT i FROM IdentityProvider i"),
    @NamedQuery(name = "IdentityProvider.findByIdpUuid", query = "SELECT i FROM IdentityProvider i WHERE i.idpUuid = :idpUuid"),
    @NamedQuery(name = "IdentityProvider.findByProviderURI", query = "SELECT i FROM IdentityProvider i WHERE i.providerURI = :providerURI"),
    @NamedQuery(name = "IdentityProvider.findByDescription", query = "SELECT i FROM IdentityProvider i WHERE i.description = :description"),
    @NamedQuery(name = "IdentityProvider.findByProviderName", query = "SELECT i FROM IdentityProvider i WHERE i.providerName = :providerName")})
public class IdentityProvider implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idpUuid")
    private String idpUuid;
    @Basic(optional = false)
    @Column(name = "providerURI")
    private String providerURI;
    @Column(name = "description")
    private String description;
    @Column(name = "providerName")
    private String providerName;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "identityProvider")
    private List<UserhasidentityProvider> userhasidentityProviderList;

    public IdentityProvider() {
    }

    public IdentityProvider(String idpUuid) {
        this.idpUuid = idpUuid;
    }

    public IdentityProvider(String idpUuid, String providerURI) {
        this.idpUuid = idpUuid;
        this.providerURI = providerURI;
    }

    public String getIdpUuid() {
        return idpUuid;
    }

    public void setIdpUuid(String idpUuid) {
        this.idpUuid = idpUuid;
    }

    public String getProviderURI() {
        return providerURI;
    }

    public void setProviderURI(String providerURI) {
        this.providerURI = providerURI;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<UserhasidentityProvider> getUserhasidentityProviderList() {
        return userhasidentityProviderList;
    }

    public void setUserhasidentityProviderList(List<UserhasidentityProvider> userhasidentityProviderList) {
        this.userhasidentityProviderList = userhasidentityProviderList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpUuid != null ? idpUuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdentityProvider)) {
            return false;
        }
        IdentityProvider other = (IdentityProvider) object;
        if ((this.idpUuid == null && other.idpUuid != null) || (this.idpUuid != null && !this.idpUuid.equals(other.idpUuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.IdentityProvider[ idpUuid=" + idpUuid + " ]";
    }
}
