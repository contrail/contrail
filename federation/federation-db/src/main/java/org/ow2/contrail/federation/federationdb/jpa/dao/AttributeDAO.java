/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Attribute;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * @author ales
 */
public class AttributeDAO extends BaseDAO {

    public AttributeDAO() {
    }

    public AttributeDAO(EntityManager em) {
        super(em);
    }

    public Attribute findByUuid(String attributeUuid) {
        try {
            return em.find(Attribute.class, attributeUuid);
        }
        finally {
            closeEntityManager();
        }
    }

    /**
     * Find attribute by name.
     *
     * @param name of the attribute
     * @return
     */
    public Attribute findByName(String name) {
        try {
            Query query = em.createNamedQuery("Attribute.findByName");
            query.setParameter("name", name);
            return (Attribute) query.getSingleResult();
        }
        finally {
            closeEntityManager();
        }
    }
}
