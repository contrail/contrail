package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Cluster;

import javax.persistence.EntityManager;

public class ClusterDAO extends BaseDAO {

    public ClusterDAO() {
    }

    public ClusterDAO(EntityManager em) {
        super(em);
    }

    public Cluster findById(int providerId, int clusterId) {
        try {
            Cluster cluster = em.find(Cluster.class, clusterId);
            if (cluster == null || !cluster.getProviderId().getProviderId().equals(providerId)) {
                return null;
            }
            else {
                return cluster;
            }
        }
        finally {
            closeEntityManager();
        }
    }

    public Cluster findById(String providerUuid, int clusterId) {
        try {
            Cluster cluster = em.find(Cluster.class, clusterId);
            if (cluster == null || !cluster.getProviderId().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return cluster;
            }
        }
        finally {
            closeEntityManager();
        }
    }
}
