/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * @author ales
 */
@Entity
@Table(name = "VM", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "providerId"}))
@NamedQueries({
        @NamedQuery(name = "Vm.findAll", query = "SELECT v FROM Vm v"),
        @NamedQuery(name = "Vm.findByVmId", query = "SELECT v FROM Vm v WHERE v.vmId = :vmId"),
        @NamedQuery(name = "Vm.findByName", query = "SELECT v FROM Vm v WHERE v.name = :name")})
public class Vm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vmId")
    private Integer vmId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @ManyToMany(mappedBy = "vmList")
    private List<Cluster> clusterList;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;
    @JoinColumn(name = "applicationId", referencedColumnName = "applicationId")
    @ManyToOne(optional = true)
    private Application applicationId;

    public Vm() {
    }

    public Vm(JSONObject json) throws JSONException {
        String name = (String) json.remove("name");
        this.setName(name);
        this.setAttributes(json.toString());
    }

    public Vm(Integer vmId) {
        this.vmId = vmId;
    }

    public Integer getVmId() {
        return vmId;
    }

    public void setVmId(Integer vmId) {
        this.vmId = vmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    public Application getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Application applicationId) {
        this.applicationId = applicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vmId != null ? vmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vm)) {
            return false;
        }
        Vm other = (Vm) object;
        if ((this.vmId == null && other.vmId != null) || (this.vmId != null && !this.vmId.equals(other.vmId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Vm[ vmId=" + vmId + " ]";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject(attributes);
        json.put("name", name);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String)json.remove("name");
        }

        JSONObject attrs = new JSONObject(attributes);
        Iterator it = json.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = json.get(key);
            attrs.put(key, value);
        }
        attributes = attrs.toString();
    }
}
