/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "Ovf")
@NamedQueries({
    @NamedQuery(name = "Ovf.findAll", query = "SELECT o FROM Ovf o"),
    @NamedQuery(name = "Ovf.findByOvfId", query = "SELECT o FROM Ovf o WHERE o.ovfId = :ovfId"),
    @NamedQuery(name = "Ovf.findByName", query = "SELECT o FROM Ovf o WHERE o.name = :name")})
public class Ovf implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ovfId")
    private Integer ovfId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @Lob
    @Column(name = "content")
    private String content;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerOvfId")
    private List<UserOvf> userOvfList;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;

    public Ovf() {
    }

    public Ovf(JSONObject json) throws JSONException {
        String name = (String) json.remove("name");
        this.setName(name);
        this.setAttributes(json.toString());
    }

    public Ovf(Integer ovfId) {
        this.ovfId = ovfId;
    }

    public Integer getOvfId() {
        return ovfId;
    }

    public void setOvfId(Integer ovfId) {
        this.ovfId = ovfId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<UserOvf> getUserOvfList() {
        return userOvfList;
    }

    public void setUserOvfList(List<UserOvf> userOvfList) {
        this.userOvfList = userOvfList;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ovfId != null ? ovfId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ovf)) {
            return false;
        }
        Ovf other = (Ovf) object;
        if ((this.ovfId == null && other.ovfId != null) || (this.ovfId != null && !this.ovfId.equals(other.ovfId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Ovf[ ovfId=" + ovfId + " ]";
    }
    
}
