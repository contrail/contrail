/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "UserSLATemplate")
@NamedQueries({
    @NamedQuery(name = "UserSLATemplate.findAll", query = "SELECT u FROM UserSLATemplate u"),
    @NamedQuery(name = "UserSLATemplate.findBySLATemplateId", query = "SELECT u FROM UserSLATemplate u WHERE u.sLATemplateId = :sLATemplateId"),
    @NamedQuery(name = "UserSLATemplate.findByUrl", query = "SELECT u FROM UserSLATemplate u WHERE u.url = :url")})
public class UserSLATemplate implements Serializable {
    @Column(name = "name")
    private String name;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SLATemplateId")
    private Integer sLATemplateId;
    @Column(name = "url")
    private String url;
    @Lob
    @Column(name = "content")
    private String content;
    @JoinColumn(name = "slatId", referencedColumnName = "slatId")
    @ManyToOne
    private SLATemplate slatId;
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private User userId;
    @OneToMany(mappedBy = "sLATemplateId")
    private List<UserSLA> userSLAList;
    @OneToMany(mappedBy = "userSLATemplate")
    private List<SLATemplateProposal> slaTemplateProposals;
    @JoinColumn(name = "ovfId", referencedColumnName = "ovfId", nullable = false)
    @ManyToOne(optional = false)
    private UserOvf userOvf;

    public UserSLATemplate() {
    }

    public UserSLATemplate(Integer sLATemplateId) {
        this.sLATemplateId = sLATemplateId;
    }

    public Integer getSLATemplateId() {
        return sLATemplateId;
    }

    public void setSLATemplateId(Integer sLATemplateId) {
        this.sLATemplateId = sLATemplateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public SLATemplate getSlatId() {
        return slatId;
    }

    public void setSlatId(SLATemplate slatId) {
        this.slatId = slatId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public List<UserSLA> getUserSLAList() {
        return userSLAList;
    }

    public void setUserSLAList(List<UserSLA> userSLAList) {
        this.userSLAList = userSLAList;
    }

    public List<SLATemplateProposal> getSlaTemplateProposals() {
        return slaTemplateProposals;
    }

    public void setSlaTemplateProposals(List<SLATemplateProposal> slaTemplateProposals) {
        this.slaTemplateProposals = slaTemplateProposals;
    }

    public UserOvf getUserOvf() {
        return userOvf;
    }

    public void setUserOvf(UserOvf userOvf) {
        this.userOvf = userOvf;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sLATemplateId != null ? sLATemplateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSLATemplate)) {
            return false;
        }
        UserSLATemplate other = (UserSLATemplate) object;
        if ((this.sLATemplateId == null && other.sLATemplateId != null) || (this.sLATemplateId != null && !this.sLATemplateId.equals(other.sLATemplateId))) {
            return false;
        }
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate[ sLATemplateId=" + sLATemplateId + " ]";
    }
    
}
