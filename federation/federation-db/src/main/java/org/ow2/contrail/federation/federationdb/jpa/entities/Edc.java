/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "EDC")
@NamedQueries({
    @NamedQuery(name = "Edc.findAll", query = "SELECT e FROM Edc e"),
    @NamedQuery(name = "Edc.findByEdcId", query = "SELECT e FROM Edc e WHERE e.edcId = :edcId"),
    @NamedQuery(name = "Edc.findByName", query = "SELECT e FROM Edc e WHERE e.name = :name"),
    @NamedQuery(name = "Edc.findByAttributes", query = "SELECT e FROM Edc e WHERE e.attributes = :attributes")})
public class Edc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "edcId")
    private Integer edcId;
    @Column(name = "name")
    private String name;
    @Column(name = "attributes")
    private String attributes;
    @ManyToMany(mappedBy = "edcList")
    private List<Vo> voList;

    public Edc() {
    }

    public Edc(Integer edcId) {
        this.edcId = edcId;
    }

    public Integer getEdcId() {
        return edcId;
    }

    public void setEdcId(Integer edcId) {
        this.edcId = edcId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Vo> getVoList() {
        return voList;
    }

    public void setVoList(List<Vo> voList) {
        this.voList = voList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (edcId != null ? edcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Edc)) {
            return false;
        }
        Edc other = (Edc) object;
        if ((this.edcId == null && other.edcId != null) || (this.edcId != null && !this.edcId.equals(other.edcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Edc[ edcId=" + edcId + " ]";
    }
    
}
