package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserSelectionCriterionPK implements Serializable {
    @Basic(optional = false)
    @Column(nullable = false)
    private int userId;
    @Basic(optional = false)
    @Column(nullable = false)
    private int scId;

    public UserSelectionCriterionPK() {
    }

    public UserSelectionCriterionPK(int userId, int scId) {
        this.userId = userId;
        this.scId = scId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getScId() {
        return scId;
    }

    public void setScId(int scId) {
        this.scId = scId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) scId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSelectionCriterionPK)) {
            return false;
        }
        UserSelectionCriterionPK other = (UserSelectionCriterionPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.scId != other.scId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserSelectionCriterionPK[ userId=" + userId + ", scId=" + scId + " ]";
    }
    
}
