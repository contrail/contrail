/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "User_has_identityProvider")
@NamedQueries({
    @NamedQuery(name = "UserhasidentityProvider.findAll", query = "SELECT u FROM UserhasidentityProvider u"),
    @NamedQuery(name = "UserhasidentityProvider.findByUserId", query = "SELECT u FROM UserhasidentityProvider u WHERE u.userhasidentityProviderPK.userId = :userId"),
    @NamedQuery(name = "UserhasidentityProvider.findByIdpUuid", query = "SELECT u FROM UserhasidentityProvider u WHERE u.userhasidentityProviderPK.idpUuid = :idpUuid"),
    @NamedQuery(name = "UserhasidentityProvider.findByIdentity", query = "SELECT u FROM UserhasidentityProvider u WHERE u.identity = :identity")})
public class UserhasidentityProvider implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserhasidentityProviderPK userhasidentityProviderPK;
    @Basic(optional = false)
    @Column(name = "\"identity\"")
    private String identity;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @JoinColumn(name = "userId", referencedColumnName = "userId", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;
    @JoinColumn(name = "idpUuid", referencedColumnName = "idpUuid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private IdentityProvider identityProvider;

    public UserhasidentityProvider() {
    }

    public UserhasidentityProvider(UserhasidentityProviderPK userhasidentityProviderPK) {
        this.userhasidentityProviderPK = userhasidentityProviderPK;
    }

    public UserhasidentityProvider(UserhasidentityProviderPK userhasidentityProviderPK, String identity) {
        this.userhasidentityProviderPK = userhasidentityProviderPK;
        this.identity = identity;
    }

    public UserhasidentityProvider(int userId, String idpUuid) {
        this.userhasidentityProviderPK = new UserhasidentityProviderPK(userId, idpUuid);
    }

    public UserhasidentityProviderPK getUserhasidentityProviderPK() {
        return userhasidentityProviderPK;
    }

    public void setUserhasidentityProviderPK(UserhasidentityProviderPK userhasidentityProviderPK) {
        this.userhasidentityProviderPK = userhasidentityProviderPK;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public IdentityProvider getIdentityProvider() {
        return identityProvider;
    }

    public void setIdentityProvider(IdentityProvider identityProvider) {
        this.identityProvider = identityProvider;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userhasidentityProviderPK != null ? userhasidentityProviderPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserhasidentityProvider)) {
            return false;
        }
        UserhasidentityProvider other = (UserhasidentityProvider) object;
        if ((this.userhasidentityProviderPK == null && other.userhasidentityProviderPK != null) || (this.userhasidentityProviderPK != null && !this.userhasidentityProviderPK.equals(other.userhasidentityProviderPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProvider[ userhasidentityProviderPK=" + userhasidentityProviderPK + " ]";
    }
    
}
