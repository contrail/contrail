package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Vo;

import javax.persistence.EntityManager;

public class VoDAO extends BaseDAO {

    public VoDAO() {
    }

    public VoDAO(EntityManager em) {
        super(em);
    }

    public Vo findById(int voId) {
        try {
            return em.find(Vo.class, voId);
        }
        finally {
            closeEntityManager();
        }
    }
}
