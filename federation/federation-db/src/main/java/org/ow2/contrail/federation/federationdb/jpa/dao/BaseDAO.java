package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;

public abstract class BaseDAO {
    protected EntityManager em;
    private boolean hasToBeClosed;

    protected BaseDAO() {
        em = PersistenceUtils.getInstance().getEntityManager();
        hasToBeClosed = true;
    }

    protected BaseDAO(EntityManager em) {
        this.em = em;
        hasToBeClosed = false;
    }

    protected void closeEntityManager() {
        if (hasToBeClosed) {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
