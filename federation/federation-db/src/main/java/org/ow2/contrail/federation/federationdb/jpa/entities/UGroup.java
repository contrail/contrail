/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "UGroup")
@NamedQueries({
    @NamedQuery(name = "UGroup.findAll", query = "SELECT u FROM UGroup u"),
    @NamedQuery(name = "UGroup.findByGroupId", query = "SELECT u FROM UGroup u WHERE u.groupId = :groupId"),
    @NamedQuery(name = "UGroup.findByName", query = "SELECT u FROM UGroup u WHERE u.name = :name"),
    @NamedQuery(name = "UGroup.findByDescription", query = "SELECT u FROM UGroup u WHERE u.description = :description")})
public class UGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "groupId")
    private Integer groupId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToMany(mappedBy = "uGroupList")
    private List<User> userList;

    public UGroup() {
    }

    public UGroup(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UGroup)) {
            return false;
        }
        UGroup other = (UGroup) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UGroup[ groupId=" + groupId + " ]";
    }
    
}
