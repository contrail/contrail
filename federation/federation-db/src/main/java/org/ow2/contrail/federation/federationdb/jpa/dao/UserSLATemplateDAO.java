package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class UserSLATemplateDAO extends BaseDAO {

    public UserSLATemplateDAO() {
    }

    public UserSLATemplateDAO(EntityManager em) {
        super(em);
    }

    public UserSLATemplate findById(int slatemplateid) {
        try {
            return em.find(UserSLATemplate.class, slatemplateid);
        }
        finally {
            closeEntityManager();
        }
    }

    public UserSLATemplate findByUrl(String url) {
        try {
            TypedQuery<UserSLATemplate> q = em.createNamedQuery("UserSLATemplate.findByUrl", UserSLATemplate.class);
            q.setParameter("url", url);
            return q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }
}
