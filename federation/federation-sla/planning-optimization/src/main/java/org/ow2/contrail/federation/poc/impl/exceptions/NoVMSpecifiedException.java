package org.ow2.contrail.federation.poc.impl.exceptions;

/**
 * The <code>NoVMSpecifiedException</code> exception class represents that there
 * is not VM specified.
 * 
 * @author Kuan Lu
 */
public class NoVMSpecifiedException extends Exception
{

    private static final long serialVersionUID = 1L;

    public NoVMSpecifiedException()
    {
        // TODO Auto-generated constructor stub
    }

    public NoVMSpecifiedException(
                                   String message )
    {
        super( message );
        // TODO Auto-generated constructor stub
    }

    public NoVMSpecifiedException(
                                   Throwable cause )
    {
        super( cause );
        // TODO Auto-generated constructor stub
    }

    public NoVMSpecifiedException(
                                   String message,
                                   Throwable cause )
    {
        super( message, cause );
        // TODO Auto-generated constructor stub
    }

}
