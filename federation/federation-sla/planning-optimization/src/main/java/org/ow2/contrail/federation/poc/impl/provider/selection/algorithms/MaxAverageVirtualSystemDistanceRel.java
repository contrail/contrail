/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.federation.poc.impl.provider.selection.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.contrail.federation.poc.impl.exceptions.SelectionException;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.ow2.contrail.federation.poc.impl.provider.selection.OfferSelectorAbstractImpl;
import org.ow2.contrail.federation.poc.impl.provider.selection.WeightedSlat;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplate;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplateParser;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailVirtualSystem;
import org.ow2.contrail.federation.poc.impl.slaparser.MeasurableAgreementTerm;
import org.slasoi.slamodel.sla.SLATemplate;

public class MaxAverageVirtualSystemDistanceRel extends OfferSelectorAbstractImpl {

	
	static void weightVirtualSystems(List<ContrailSlaTemplate> offers, Criterion[] criteria) {
		HashMap<String, Double> vsSumDistance = new HashMap<String, Double>();
		
		for (ContrailSlaTemplate offer : offers) {
			for (ContrailVirtualSystem vs : offer.getVirtualSystems()) {
				if (0 == vs.getAgreementTerms().size()) 
					continue;
				double sqrDistance = 0;
				for (Criterion c : criteria) {
					MeasurableAgreementTerm mat = 
						(MeasurableAgreementTerm) vs.getAgreementTerm(c.getName());
					if (mat != null)
						sqrDistance += Math.pow(mat.getWeight(), 2);
				}
				double distance = Math.sqrt(sqrDistance);
				vs.setWeight(distance);
				Double sum = vsSumDistance.get(vs.getId()) ;
				if (null == sum) sum = (double) 0;
				vsSumDistance.put(vs.getId(), sum + distance);
			}
		}
		
		for (ContrailSlaTemplate offer : offers) {
			for (ContrailVirtualSystem vs : offer.getVirtualSystems()) {
				if (0 == vs.getAgreementTerms().size()) 
					continue;
				Double sumDistance = vsSumDistance.get(vs.getId());
				vs.setWeight( (offers.size()-1)*vs.getWeight() / (sumDistance-vs.getWeight()));
			}
		}
	}
	
	

	@Override
	public List<WeightedSlat> evaluate(List<SLATemplate> ssoffs,
			SLATemplate ssprop, Criterion[] criteria)
			throws SelectionException {

		ContrailSlaTemplate proposal = 
			ContrailSlaTemplateParser.getContrailSlat(ssprop, criteria);

		Map<ContrailSlaTemplate, SLATemplate> offersMap = new HashMap<ContrailSlaTemplate, SLATemplate>();
		for (SLATemplate ssoff : ssoffs) {
			ContrailSlaTemplate offer = ContrailSlaTemplateParser.getContrailSlat(ssoff, criteria);
			offersMap.put(offer, ssoff);
		}

		List<ContrailSlaTemplate> offers = 
				new ArrayList<ContrailSlaTemplate>(offersMap.keySet());
		
		normalize(offers, proposal);
		print("Normalized offers", offers);
		
		weight(offers, criteria);
		weightVirtualSystems(offers, criteria);
		print("Weighted offers", offers);
		
		List<WeightedSlat> weightedSlats = new ArrayList<WeightedSlat>();
		for (ContrailSlaTemplate offer : offers) {
			Double weight = getAverageWeight(offer.getVirtualSystems());
			WeightedSlat wslat = new WeightedSlat(offersMap.get(offer), weight);
			weightedSlats.add(wslat);
		}
		
		return weightedSlats;
	}

}
