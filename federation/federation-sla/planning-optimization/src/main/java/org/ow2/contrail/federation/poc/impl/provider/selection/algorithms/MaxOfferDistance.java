/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.federation.poc.impl.provider.selection.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.poc.impl.exceptions.SelectionException;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.ow2.contrail.federation.poc.impl.provider.selection.OfferSelectorAbstractImpl;
import org.ow2.contrail.federation.poc.impl.provider.selection.WeightedSlat;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplate;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplateParser;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailVirtualSystem;
import org.ow2.contrail.federation.poc.impl.slaparser.MeasurableAgreementTerm;
import org.slasoi.slamodel.sla.SLATemplate;

public class MaxOfferDistance extends OfferSelectorAbstractImpl {

	
	static double getAbsoluteDistance(ContrailSlaTemplate offer, Criterion[] criteria) {
		double sqrDist = 0;
		for (ContrailVirtualSystem vs : offer.getVirtualSystems()) {
			if (0 == vs.getAgreementTerms().size()) 
				continue;
			for (Criterion c : criteria) {
				MeasurableAgreementTerm mat = 
					(MeasurableAgreementTerm) vs.getAgreementTerm(c.getName());
				if (mat != null)
					sqrDist += Math.pow(mat.getWeight(), 2);
			}
		}
		LOGGER.debug("Distance=" + Math.sqrt(sqrDist));
		return  Math.sqrt(sqrDist);
	}

	
	
	@Override
	public List<WeightedSlat> evaluate(List<SLATemplate> ssoffs,
			SLATemplate ssprop, Criterion[] criteria)
			throws SelectionException {

		ContrailSlaTemplate proposal = 
			ContrailSlaTemplateParser.getContrailSlat(ssprop, criteria);

		Map<ContrailSlaTemplate, SLATemplate> offersMap = new HashMap<ContrailSlaTemplate, SLATemplate>();
		for (SLATemplate ssoff : ssoffs) {
			ContrailSlaTemplate offer = ContrailSlaTemplateParser.getContrailSlat(ssoff, criteria);
			offersMap.put(offer, ssoff);
		}

		List<ContrailSlaTemplate> offers = 
				new ArrayList<ContrailSlaTemplate>(offersMap.keySet());
		
		normalize(offers, proposal);
		weight(offers, criteria);
		print("============ WEIGHTED OFFERS ============", offers);
		
		List<WeightedSlat> weightedSlats = new ArrayList<WeightedSlat>();
		for (ContrailSlaTemplate offer : offers) {
			Double distance = getAbsoluteDistance(offer, criteria);
			WeightedSlat wslat = new WeightedSlat(offersMap.get(offer), distance);
			weightedSlats.add(wslat);
		}
		
		return weightedSlats;
	}

	private static final Logger LOGGER = Logger.getLogger(MaxOfferDistance.class);
}
