/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.federation.poc.impl.provider.selection.algorithms;

import java.util.List;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.poc.impl.exceptions.SelectionException;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.ow2.contrail.federation.poc.impl.provider.selection.OfferSelectorAbstractImpl;
import org.ow2.contrail.federation.poc.impl.provider.selection.WeightedSlat;
import org.slasoi.slamodel.sla.SLATemplate;


public class ConfigurableAlg extends OfferSelectorAbstractImpl {

	private static OfferSelectorAbstractImpl selector;
	
	
	public ConfigurableAlg() {
		try {
			selector =  (OfferSelectorAbstractImpl) Class.forName(algClass).newInstance();
			LOGGER.info("Algorithm selected: " + selector.getClass().getName());
		} catch (InstantiationException e) {
			LOGGER.error("Could not instantiate algorithm class" + algClass);
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			LOGGER.error("Could not access class" + algClass);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			LOGGER.error("Could not find class" + algClass);
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public List<WeightedSlat> evaluate(List<SLATemplate> ssoffs,
			SLATemplate ssprop, Criterion[] criteria) throws SelectionException {
		
		return selector.evaluate(ssoffs, ssprop, criteria);
	}

	
	private static final Logger LOGGER = Logger.getLogger(ConfigurableAlg.class);
	
}
