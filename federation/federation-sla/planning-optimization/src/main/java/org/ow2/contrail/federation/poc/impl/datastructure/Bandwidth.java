package org.ow2.contrail.federation.poc.impl.datastructure;

import org.ow2.contrail.federation.poc.impl.utils.Constant;

/**
 * <code>Bandwidth</code> represents Bandwidth in resource request.
 */
public class Bandwidth extends Resource
{
    public Bandwidth()
    {
        this.setResourceName( Constant.Bandwidth );
    }
}
