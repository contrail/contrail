package org.ow2.contrail.federation.poc.impl.provision;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.commons.plan.Task;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class PlanHandlerImpl
{
    public static SLAManagerContext context;
    private SLA sla;
    private ArrayList<Task> taksList;
    private static final String PLAN_ID = "MyPlan";
    public static final STND PLAN_ID_SLA = new STND( PLAN_ID );
    public static final String propertiesFile = "generic-slamanager" + System.getProperty( "file.separator" ) + "provisioning-adjustment" + System.getProperty( "file.separator" )
    + "provisioning_adjustment.properties";

    private static final Logger LOGGER = Logger.getLogger( PlanHandlerImpl.class );

    public PlanHandlerImpl(SLATemplate slat)  {
        super();
        if ( slat instanceof SLA ) {
            this.sla = (SLA)slat;
            this.taksList = new ArrayList<Task>();
        }
        else {
            LOGGER.error( "In order to make a plan, SLA object is a reasonable parameter." );
        }
    }

    
    public Plan planMaker() {
    	return null;
    }
    
 
 }
