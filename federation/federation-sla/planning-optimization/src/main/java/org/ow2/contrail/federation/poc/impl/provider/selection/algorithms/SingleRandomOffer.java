package org.ow2.contrail.federation.poc.impl.provider.selection.algorithms;

import java.util.ArrayList;
import java.util.List;

import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.ow2.contrail.federation.poc.impl.provider.selection.OfferSelectorAbstractImpl;
import org.ow2.contrail.federation.poc.impl.provider.selection.WeightedSlat;
import org.slasoi.slamodel.sla.SLATemplate;


public class SingleRandomOffer extends OfferSelectorAbstractImpl {

	@Override
	public List<WeightedSlat> evaluate(List<SLATemplate> offers,
			SLATemplate proposal, Criterion[] criteria) {
		
		List<WeightedSlat> weightedSlats = new ArrayList<WeightedSlat>();
		weightedSlats.add(new WeightedSlat(offers.get(0), 1));
		return weightedSlats;
	}

}
