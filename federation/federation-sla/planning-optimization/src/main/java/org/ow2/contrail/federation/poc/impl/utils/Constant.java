package org.ow2.contrail.federation.poc.impl.utils;

/**
 * The class of <code>Constant</code> includes all the constants that needed by
 * optimization processes.
 */
public class Constant
{
    public final static String $cup_architecture = "cpu_architecture";
    public final static String $memory_redundancy = "memory_redundancy";

    public final static String VM0 = "VM_TYPE_0";
    public final static String VM1 = "VM_TYPE_1";

    // for resource of request
    public final static String startTtime = "START_TIME_VAR";
    public final static String endTime = "END_TIME_VAR";
    public final static String Memory = "Memory";
    public final static String CPU = "CPU";
    public final static String Harddisk = "Harddisk";
    public final static String Bandwidth = "Bandwidth";

    public final static float CPU_Max_Speed_VM0 = 4.0f;
    public final static float CPU_Max_Speed_VM1 = 4.0f;
    public final static float CPU_Mini_Speed_VM0 = 1.0f;
    public final static float CPU_Mini_Speed_VM1 = 1.0f;
    public final static double CPU_Core_Max_Nr_VM0 = 16;
    public final static double CPU_Core_Max_Nr_VM1 = 16;
    public final static double CPU_Core_Mini_Nr_VM0 = 1;
    public final static double CPU_Core_Mini_Nr_VM1 = 1;
    public final static double Memory_Max_Size = 2048;
    public final static double Memory_Mini_Size = 128;
    // the price is the basic price of 128M
    public final static double Memory_Basic_Price = 2;
    // the price is the basic price of 1G
    public final static double Harddisk_Basic_Price = 2;
    // the price is the basic price of 1mb/s
    public final static double Bandwidth_Basic_Price = 2;
    // the price is the basic price of 1GHz
    public final static double CPU_Basic_Price = 50;
    // the increased price per 0.1GHz
    public final static double CPU_Price_Increasement = 5;

    /*
     * General Config
     */
    public final static double maxAvailability = 99;
    public final static double basicAvailability = 95;
    public final static double innerProfitRate = 0.3;
    public final static double externalProfitRate = 0.05;
    public final static double maxFailureRate = 0.15;
    public final static double minimalH = 0.01;
    public final static double maxBeta = 0.2;

    public final static double goldCustomerDiscount = 0.7;
    public final static double silverCustomerDiscount = 0.8;
    public final static double bronzeCustomerDiscount = 0.9;

    public final static String CPU_1GHz_1Gb_Name = "CPU_1GHz_1Gb";
    public final static String CPU_1GHz_2Gb_Name = "CPU_1GHz_2Gb";
    public final static String CPU_2GHz_1Gb_Name = "CPU_2GHz_1Gb";
    public final static String CPU_2GHz_2Gb_Name = "CPU_2GHz_2Gb";

    // Here we assume all the providers have the same failure rate
    // public final static double failureRate = 0.2;

    // Main Provider
    public final static String Main_Provider = "Main_Provider";

    public final static double Main_FailureRate = 0.2;

    public final static int Main_CPU_1GHz_1Gb_Price = 50;
    public final static int Main_CPU_1GHz_2Gb_Price = 100;
    public final static int Main_CPU_2GHz_1Gb_Price = 100;
    public final static int Main_CPU_2GHz_2Gb_Price = 150;
    public final static int Main_Memory_Price = 2;

    public final static int Main_CPU_1GHz_1Gb_Nr = 200;
    public final static int Main_CPU_1GHz_2Gb_Nr = 100;
    public final static int Main_CPU_2GHz_1Gb_Nr = 100;
    public final static int Main_CPU_2GHz_2Gb_Nr = 150;
    public final static int Main_Memory_Nr = 20;

    // 1st Sub Provider
    public final static String Sub_Provider_1 = "Sub_Provider_1";

    public final static double Sub_Provider_1_FailureRate = 0.22;

    public final static int Sub1_CPU_1GHz_1Gb_Price = 45;
    public final static int Sub1_CPU_1GHz_2Gb_Price = 110;
    public final static int Sub1_CPU_2GHz_1Gb_Price = 90;
    public final static int Sub1_CPU_2GHz_2Gb_Price = 155;
    public final static int Sub1_Memory_Price = 2;

    public final static int Sub1_CPU_1GHz_1Gb_Nr = 100;
    public final static int Sub1_CPU_1GHz_2Gb_Nr = 90;
    public final static int Sub1_CPU_2GHz_1Gb_Nr = 90;
    public final static int Sub1_CPU_2GHz_2Gb_Nr = 135;
    public final static int Sub1_Memory_Nr = 30;

    // 2nd Sub Provider
    public final static String Sub_Provider_2 = "Sub_Provider_2";

    public final static double Sub_Provider_2_FailureRate = 0.18;

    public final static int Sub2_CPU_1GHz_1Gb_Price = 55;
    public final static int Sub2_CPU_1GHz_2Gb_Price = 90;
    public final static int Sub2_CPU_2GHz_1Gb_Price = 100;
    public final static int Sub2_CPU_2GHz_2Gb_Price = 160;
    public final static int Sub2_Memory_Price = 2;

    public final static int Sub2_CPU_1GHz_1Gb_Nr = 150;
    public final static int Sub2_CPU_1GHz_2Gb_Nr = 110;
    public final static int Sub2_CPU_2GHz_1Gb_Nr = 100;
    public final static int Sub2_CPU_2GHz_2Gb_Nr = 160;
    public final static int Sub2_Memory_Nr = 25;

    // 3rd Sub Provider
    public final static String Sub_Provider_3 = "Sub_Provider_3";

    public final static double Sub_Provider_3_FailureRate = 0.2;

    public final static int Sub3_CPU_1GHz_1Gb_Price = 50;
    public final static int Sub3_CPU_1GHz_2Gb_Price = 110;
    public final static int Sub3_CPU_2GHz_1Gb_Price = 110;
    public final static int Sub3_CPU_2GHz_2Gb_Price = 165;
    public final static int Sub3_Memory_Price = 2;

    public final static int Sub3_CPU_1GHz_1Gb_Nr = 50;
    public final static int Sub3_CPU_1GHz_2Gb_Nr = 110;
    public final static int Sub3_CPU_2GHz_1Gb_Nr = 110;
    public final static int Sub3_CPU_2GHz_2Gb_Nr = 135;
    public final static int Sub3_Memory_Nr = 18;

    // Client Type , we assume the main provider is a gold customer to its
    // sub-providers
    public final static String Client_Type_Gold = "gold";
    public final static String Client_Type_Silver = "silver";
    public final static String Client_Type_Bronze = "bronze";

    // Client requests simulations.
    public final static int Client_Request_CPU_1GHz_1Gb_Nr = 300;
    public final static int Client_Request_CPU_1GHz_2Gb_Nr = 300;
    public final static int Client_Request_CPU_2GHz_1Gb_Nr = 200;
    public final static int Client_Request_CPU_2GHz_2Gb_Nr = 200;

    public final static int Client_Request_Memory_Nr = 90;

    public final static double Client_Request_QoS_Availability = 0.98;
    public final static boolean Client_Request_QoS_Isolation = false;

}
