package org.ow2.contrail.federation.poc.impl.negotiation;


/**
 * The implementation of interface <code>ServiceManagerHandler</code>
 * 
 * @see org.slasoi.isslam.poc.servicesmanager.ServiceManagerHandler
 * @author Kuan Lu
 */
public class ServiceManagerHandlerImpl
{
    public static ServiceManagerHandlerImpl instance;

    /**
     * Gets the instance of infrastructure service manager handler.
     */
    public static ServiceManagerHandlerImpl getInstance()
    {
        if ( ServiceManagerHandlerImpl.instance == null ) {
            ServiceManagerHandlerImpl.instance = new ServiceManagerHandlerImpl();
            return ServiceManagerHandlerImpl.instance;
        }
        else return ServiceManagerHandlerImpl.instance;
    }

}
