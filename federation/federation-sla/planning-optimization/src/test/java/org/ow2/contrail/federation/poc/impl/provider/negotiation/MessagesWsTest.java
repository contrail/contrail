package org.ow2.contrail.federation.poc.impl.provider.negotiation;

import java.io.File;
import java.io.IOException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.commons.io.FileUtils;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.ow2.contrail.federation.poc.impl.provider.negotiation.ws.BZNegotiationStub.InitiateNegotiation;
import org.ow2.contrail.federation.poc.impl.provider.negotiation.ws.BZNegotiationStub.Negotiate;
import org.ow2.contrail.federation.poc.impl.provider.translation.SlaTranslator;
import org.ow2.contrail.federation.poc.impl.provider.translation.SlaTranslatorImplNoOsgi;
import org.slasoi.slamodel.sla.SLATemplate;

public class MessagesWsTest {

	private static SLATemplate slaTemplate1;
	private static final String negotiationId = "7cf8339b-c040-4d96-8c6b-4bf6e4774a36";
	private static NegotiationWsClient negotiationClient;
	
	@BeforeClass
	public static void setUp() throws Exception {
		
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setNormalizeWhitespace(true);

		String slatXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/contrail-basic-slat.xml"));
	    SLASOITemplateParser tp = new SLASOITemplateParser();
	    slaTemplate1 = tp.parseTemplate(slatXml);		
	    negotiationClient = new NegotiationWsClient();
	    SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
	    negotiationClient.setSlaTranslator(slaTranslator);
	}

	
	
	//@Test
	public void initiateNegotiationRequest() throws Exception {
 		InitiateNegotiation doc = negotiationClient.getInitiateNegotiationDoc(slaTemplate1);
		String initNegotMsg = FileUtils.readFileToString(
    			new File("src/test/resources/soap/initiateNegotiation.xml"));
		String generatedMsg = doc.getOMElement(null, OMAbstractFactory.getOMFactory()).toString();
		System.out.println("initiateNegotiation req=\n" + generatedMsg);
		XMLAssert.assertXMLEqual(initNegotMsg, generatedMsg);
	}

	
	//@Test
	public void negotiationRequest() throws Exception {
		Negotiate doc = negotiationClient.getNegotiationDoc(negotiationId, slaTemplate1);
		String negotiationMsg = FileUtils.readFileToString(
    			new File("src/test/resources/soap/negotiation.xml"));
		String generatedMsg = doc.getOMElement(null, OMAbstractFactory.getOMFactory()).toString();
		System.out.println("negotiation req=\n" + generatedMsg);
		XMLAssert.assertXMLEqual(negotiationMsg, generatedMsg);
	}

	
	@AfterClass
	public static void tearDown() throws IOException {
	}

}
