package org.ow2.contrail.federation.poc.impl.provider.selection;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxOfferLinearCombination;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxVirtualSystemDistance;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxAverageVirtualSystemDistance;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.ConfigurableAlg;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxOfferDistance;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.SingleRandomOffer;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxAverageVirtualSystemDistanceRel;
import org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxVirtualSystemDistanceRel;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplate;
import org.ow2.contrail.federation.poc.impl.slaparser.ContrailSlaTemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;


public class SelectionTest {

	OfferSelector selectionAlgorithm;
	static SLATemplate proposal, proposalDummy, proposalFull;
	static List<SLATemplate> offersList, offersListDummy;
	static Criterion[] criteria;
	
	
	
	@Before
	public void setUp() throws Exception {
		List<Criterion> cc = new ArrayList<Criterion>();
		cc.add(new Criterion("cpu_speed", 0.8));
		cc.add(new Criterion("vm_cores", 0.5));
		cc.add(new Criterion("memory", 0.7));
		cc.add(new Criterion("price", 0.9));
		criteria = cc.toArray(new Criterion[cc.size()]);
		
	    SLASOITemplateParser tp = new SLASOITemplateParser();

		String xmlProposal = FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/SlatProposal.xml"));

	    proposal = tp.parseTemplate(xmlProposal);
		
	    offersList = new ArrayList<SLATemplate>();
		for (int i = 0; i < 4; i++) {
			String xmlOffer = FileUtils.readFileToString(
	    			new File("src/test/resources/slats/selection/SlatOffer" + (i+1) + ".xml"));
			offersList.add(tp.parseTemplate(xmlOffer));
		}

	    proposalDummy = tp.parseTemplate(FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/DummySlatProposalFederation.xml")));

	    offersListDummy = new ArrayList<SLATemplate>();
	    offersListDummy.add(tp.parseTemplate(FileUtils.readFileToString(
	    	new File("src/test/resources/slats/selection/DummySlatOfferFederation.xml"))));
	    
		String xmlProposalFull = FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/SlatProposalFull.xml"));

	    proposalFull = tp.parseTemplate(xmlProposalFull);


	}

	
	@After
	public void tearDown() throws Exception {
		System.out.println("\n---- end of test ----");	
	}

	
	@Test
	public void parseProposalSimple() throws Exception {
		String xmlProposal = FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/SlatProposalSimple.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
	    proposal = tp.parseTemplate(xmlProposal);
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(proposal, criteria);
		System.out.println("PROPOSAL: " + cst);
		Assert.assertNotNull(cst.getVirtualSystems());
	}


	@Test
	public void parseProposalDummy() throws Exception {
		String xmlProposal = FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/DummySlatProposalFederation.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
	    proposal = tp.parseTemplate(xmlProposal);
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(proposal, criteria);
		System.out.println("PROPOSAL: " + cst);
		Assert.assertNotNull(cst.getVirtualSystems());
	}


	@Test
	public void parseOfferDummy() throws Exception {
		String xmlProposal = FileUtils.readFileToString(
    			new File("src/test/resources/slats/selection/DummySlatOfferFederation.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
	    proposal = tp.parseTemplate(xmlProposal);
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(proposal, criteria);
		System.out.println("OFFER: " + cst);
		Assert.assertNotNull(cst.getVirtualSystems());
	}

	
	@Test
	public void parseOfferIntegrationTest() throws Exception {
		String slat = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatOfferIntegrationTest.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
	    proposal = tp.parseTemplate(slat);
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(proposal, criteria);
		System.out.println("PROPOSAL: " + cst);
		Assert.assertNotNull(cst.getVirtualSystems());
	}

	
	@Test
	public void parseProposal() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(proposal, criteria);
		System.out.println("PROPOSAL: " + cst);
		Assert.assertNotNull(cst.getVirtualSystems());
	}

	
	@Test
	public void parseOffers() {
		int i = 0;
		for (SLATemplate offer : offersList) {
			ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(offer, criteria);
			System.out.println("\n----- OFFER[" + i++ + "] ----- " + cst);
			Assert.assertNotNull(cst.getVirtualSystems());
		}
	}

	
	@Test
	public void orderTest() throws SecurityException, NoSuchFieldException {
		List<WeightedSlat> weightedSlats = new ArrayList<WeightedSlat>();
		weightedSlats.add(new WeightedSlat(null, 20));
		weightedSlats.add(new WeightedSlat(null, 5));
		weightedSlats.add(new WeightedSlat(null, 1));
		weightedSlats.add(new WeightedSlat(null, 3));
		weightedSlats.add(new WeightedSlat(null, 10));
		Collections.sort(weightedSlats, new WeightedSlat.WeightComparator());
		Assert.assertEquals((int) weightedSlats.get(0).getWeight(), 20);
		Assert.assertEquals((int) weightedSlats.get(4).getWeight(), 1);
	}
	
	
	@Test
	public void singleRandomOffer() {
		selectionAlgorithm = new SingleRandomOffer();
		List<SLATemplate> list = new ArrayList<SLATemplate>();
		list.add(offersList.get(0));
		SLATemplate[] ss = selectionAlgorithm.selectOptimaSlaTemplates(list, proposal, criteria);
		Assert.assertEquals(offersList.get(0), ss[0]);
		System.out.println("Best SLA offer=" + ss[0]);
	}


	@Test
	public void singleRandomOfferNoCriteria() {
		selectionAlgorithm = new SingleRandomOffer();
		List<SLATemplate> list = new ArrayList<SLATemplate>();
		list.add(offersList.get(0));
		SLATemplate[] ss = selectionAlgorithm.selectOptimaSlaTemplates(list, proposal, null);
		Assert.assertEquals(offersList.get(0), ss[0]);
		System.out.println("Best SLA offer=" + ss[0]);
	}

	
	@Test
	public void singleRandomOfferPartialCriteria() {
		Criterion[] crit = new Criterion[1];
		crit[0] = new Criterion("cpu_speed", 0.8);
		selectionAlgorithm = new SingleRandomOffer();
		List<SLATemplate> list = new ArrayList<SLATemplate>();
		list.add(offersList.get(0));
		SLATemplate[] ss = selectionAlgorithm.selectOptimaSlaTemplates(list, proposal, crit);
		Assert.assertEquals(offersList.get(0), ss[0]);
		System.out.println("Best SLA offer=" + ss[0]);
	}

	
	@Test
	public void maxVirtualSystemDistance() {
		selectionAlgorithm = new MaxVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm
				.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(2), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}


	@Test
	public void maxVirtualSystemDistanceNoCriteria() {
		selectionAlgorithm = new MaxVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(2), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	//@Test
	public void maxVirtualSystemDistancePartialCriteria() {
		Criterion[] crit = new Criterion[2];
		crit[0] = new Criterion("cpu_speed", 0.8);
		crit[1] = new Criterion("memory", 0.7);
		selectionAlgorithm = new MaxVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, crit);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(2), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}
	
	
	//@Test
	public void maxAverageVirtualSystemDistance() {
		selectionAlgorithm = new MaxAverageVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxAverageVirtualSystemDistanceNoCriteria() {
		selectionAlgorithm = new MaxAverageVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxOfferDistance() {
		selectionAlgorithm = new MaxOfferDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}


	@Test
	public void maxOfferDistanceFullProposal() {
		selectionAlgorithm = new MaxOfferDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposalFull, criteria);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	
	@Test
	public void maxOfferDistanceNoCriteria() {
		selectionAlgorithm = new MaxOfferDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxOfferLinearCombination() {
		selectionAlgorithm = new MaxOfferLinearCombination();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxOfferLinearCombinationNoCriteria() {
		selectionAlgorithm = new MaxOfferLinearCombination();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	
	@Test
	public void maxVirtualSystemDistanceRel() {
		selectionAlgorithm = new MaxVirtualSystemDistanceRel();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(2), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxVirtualSystemDistanceRelNoCriteria() {
		selectionAlgorithm = new MaxVirtualSystemDistanceRel();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(0), bestSlats[0]);
		Assert.assertEquals(offersList.get(2), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void maxAverageVirtualSystemDistanceRel() {
		selectionAlgorithm = new MaxAverageVirtualSystemDistanceRel();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}
	

	@Test
	public void maxAverageVirtualSystemDistanceRelNoCriteria() {
		selectionAlgorithm = new MaxAverageVirtualSystemDistanceRel();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertEquals(offersList.get(2), bestSlats[0]);
		Assert.assertEquals(offersList.get(0), bestSlats[1]);
		Assert.assertEquals(offersList.get(1), bestSlats[2]);
		Assert.assertEquals(offersList.get(3), bestSlats[3]);
	}

	
	@Test
	public void configurableAlgorithmTest() {
		selectionAlgorithm = new ConfigurableAlg();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, criteria);
		Assert.assertNotNull(bestSlats[0]);
	}
	

	@Test
	public void configurableAlgorithmTestProposalFull() {
		selectionAlgorithm = new ConfigurableAlg();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposalFull, criteria);
		Assert.assertNotNull(bestSlats[0]);
	}

	
	@Test
	public void configurableAlgorithmTestNoCriteria() {
		selectionAlgorithm = new ConfigurableAlg();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersList, proposal, null);
		Assert.assertNotNull(bestSlats[0]);
	}

	
	@Test
	public void maxVirtualSystemDistanceDummySlats() {
		selectionAlgorithm = new MaxVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm.selectOptimaSlaTemplates(offersListDummy, proposalDummy, criteria);
		Assert.assertEquals(offersListDummy.get(0), bestSlats[0]);
	}


	@Test
	public void maxVirtualSystemDistanceDummySlatsNoCriteria() {
		selectionAlgorithm = new MaxVirtualSystemDistance();
		SLATemplate[] bestSlats = selectionAlgorithm
				.selectOptimaSlaTemplates(offersListDummy, proposalDummy, null);
		Assert.assertEquals(offersListDummy.get(0), bestSlats[0]);
	}

	
}
