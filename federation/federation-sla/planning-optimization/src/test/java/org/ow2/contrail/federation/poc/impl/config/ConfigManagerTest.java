package org.ow2.contrail.federation.poc.impl.config;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class ConfigManagerTest {

	private static ConfigManager config;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		config = ConfigManager.getInstance();
	}

		
	@Test
	public void getConfigAlgorithm() throws Exception {
		String alg = config.getAlgorithmClass();
		Assert.assertEquals("org.ow2.contrail.federation.poc.impl.provider.selection.algorithms.MaxOfferDistance", alg);
	}


	@Test
	public void getSupportedTerms() throws Exception {
		String[] terms = config.getSupportedTerms();
		Assert.assertEquals("cpu_speed", terms[0]);
	}


	@Test
	public void getMaxOffers() throws Exception {
		int maxOffers = config.getMaxOffers();
		Assert.assertEquals(4, maxOffers);
	}

	
	@AfterClass
	public static void tearDown() throws IOException {
	}
	

}
