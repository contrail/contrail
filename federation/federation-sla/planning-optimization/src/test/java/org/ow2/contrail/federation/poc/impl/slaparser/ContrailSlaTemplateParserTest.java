package org.ow2.contrail.federation.poc.impl.slaparser;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;

public class ContrailSlaTemplateParserTest {

	private static SLATemplate fedPropSlat, fedOfferSlat;
	private static SLATemplate propMinLoa, propAllTerms;
	Criterion[] criteria;
	
	@Before
	public void setUp() throws Exception {
		List<Criterion> cc = new ArrayList<Criterion>();
		cc.add(new Criterion("vm_cores", 1));
		cc.add(new Criterion("memory", 1));
		cc.add(new Criterion("cpu_speed", 1));
		criteria = cc.toArray(new Criterion[cc.size()]);
		
		String proposalXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatProposalFederation.xml"));
		String offerXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatOfferFederation.xml"));
	    SLASOITemplateParser tp = new SLASOITemplateParser();
	    fedPropSlat = tp.parseTemplate(proposalXml);	
	    fedOfferSlat = tp.parseTemplate(offerXml);
	    
	    
		String xmlProposalAllTerms = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatProposalAllTerms.xml"));

		propAllTerms = tp.parseTemplate(xmlProposalAllTerms);		

		String xmlProposalMinLoa = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatProposalMinimumLoa.xml"));

		propMinLoa = tp.parseTemplate(xmlProposalMinLoa);		

		
	    System.out.println("\n---------------");
	}



	@Test
	public void testParsingProposalWithoutCriteria() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(fedPropSlat, null);
		System.out.println("PROPOSAL: " + cst);
	}

	

	@Test
	public void testParsingProposalMininimumLoa() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(propMinLoa, criteria);
		System.out.println("PROPOSAL: " + cst);
	}

	
	@Test
	public void testParsingProposalAllTerms() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(propAllTerms, criteria);
		System.out.println("PROPOSAL: " + cst);
	}


	@Test
	public void testParsingProposal() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(fedPropSlat, criteria);
		System.out.println("PROPOSAL: " + cst);
	}
	
	
	@Test
	public void testParsingOfferWithoutCriteria() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(fedOfferSlat, null);
		String unit = cst.getVirtualSystem("AppServer").getAgreementTerm("cpu_speed").getUnit();
		System.out.println("OFFER: " + cst);
		Assert.assertEquals("GHz", unit);
	}
	

	@Test
	public void testParsingOffer() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(fedOfferSlat, criteria);
		String unit = cst.getVirtualSystem("AppServer").getAgreementTerm("cpu_speed").getUnit();
		System.out.println("OFFER: " + cst);
		Assert.assertEquals("GHz", unit);
	}

	
	@Test
	public void testGetPropertyValue() {
		String userid = SlaTemplateEntitiesParser.getRootPropertyValue(propAllTerms, "UserUUID");
		Assert.assertEquals("caa6e102-8ff0-400f-a120-23149326a936", userid);
	}

	
	@Test
	public void testGetMinimumLoA() {
		int loa = SlaTemplateEntitiesParser.getMinimumLoAValue(propAllTerms);
		Assert.assertEquals(3, loa);
	}
	
	
	@After
	public void tearDown() throws Exception {
	}
}
