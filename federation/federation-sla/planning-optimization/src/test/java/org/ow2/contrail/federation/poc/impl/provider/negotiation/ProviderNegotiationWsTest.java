package org.ow2.contrail.federation.poc.impl.provider.negotiation;

import java.io.File;
import java.io.IOException;


import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.wink.client.MockHttpServer;
import org.apache.wink.client.MockHttpServer.MockHttpServerResponse;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.contrail.federation.poc.impl.provider.translation.SlaTranslator;
import org.ow2.contrail.federation.poc.impl.provider.translation.SlaTranslatorImplNoOsgi;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.slamodel.sla.SLATemplate;

public class ProviderNegotiationWsTest {

	private static NegotiationWsClient nc;
	private static MockHttpServer mockServer;
	private static final int serverPort = 8080;
	private static final String requestUrl = "/services/contrailNegotiation";
	private static final String negId = "7cf8339b-c040-4d96-8c6b-4bf6e4774a36";
	
	
	@BeforeClass
	public static void configXMLunit() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setNormalizeWhitespace(true);
	}
	

	@Before
	public void setUp() throws Exception {
		nc = new NegotiationWsClient();
		SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
		nc.setSlaTranslator(slaTranslator);
		mockServer = new MockHttpServer(serverPort-1);
    	mockServer.startServer();
		System.out.println("\nHTTP server started at port " + mockServer.getServerPort());
	}

	
	
    //@Test
    public void testInitiateNegotiationWs() throws Exception {
		
		String xmlResp = FileUtils.readFileToString(
    			new File("src/test/resources/soap/initiateNegotiationSoapResponse.xml"));

		MockHttpServerResponse mockResponse = new MockHttpServerResponse();
		mockResponse.setMockResponseContent(xmlResp);
		mockServer.setMockHttpServerResponses(mockResponse);
		
    	String slatXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/contrail-basic-slat.xml")); 
    			
	    SLASOITemplateParser slasoieTemplatParser = new SLASOITemplateParser();
	    SLATemplate slat = slasoieTemplatParser.parseTemplate(slatXml);		
	    System.out.println("Sending initiateNegotiation SOAP request...");
	    String hexCode = nc.initiateNegotiation("http://localhost:" + serverPort + requestUrl, slat);
	    Assert.assertEquals(negId, hexCode);

	    String xmlGenReq = mockServer.getRequestContentAsString();
	    System.out.println("Message received by server=" + mockServer.getRequestContentAsString());
    	
    	String xmlExpReq = FileUtils.readFileToString(
    			new File("src/test/resources/soap/initiateNegotiationSoapRequest.xml"));
    	
    	XMLAssert.assertXMLEqual(xmlExpReq, xmlGenReq);
    }
    
    
    
    //@Test
    public void testNegotiationWs() throws Exception {
		
		String xmlResp = FileUtils.readFileToString(
    			new File("src/test/resources/soap/negotiationSoapResponse.xml"));

    	String xmlExpReq = FileUtils.readFileToString(
    			new File("src/test/resources/soap/negotiationSoapRequest.xml"));

		MockHttpServerResponse mockResponse = new MockHttpServerResponse();
		mockResponse.setMockResponseContent(xmlResp);
		mockServer.setMockHttpServerResponses(mockResponse);
		
    	String slatXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/contrail-basic-slat.xml")); 
    			
	    SLASOITemplateParser parser = new SLASOITemplateParser();
	    SLATemplate slat = parser.parseTemplate(slatXml);		
	    System.out.println("Sending negotiate SOAP request...");
	    SLATemplate[] slats = nc.negotiate("http://localhost:" + serverPort + requestUrl, slat, negId);

	    String xmlGenReq = mockServer.getRequestContentAsString();
	    System.out.println("Message received by server=" + mockServer.getRequestContentAsString());

    	XMLAssert.assertXMLEqual(xmlExpReq, xmlGenReq);

	    SLASOITemplateRenderer rend = new SLASOITemplateRenderer();
	    String xmlRetSlat = rend.renderSLATemplate(slats[0]);
	    
	    Assert.assertNotNull(xmlRetSlat);
    }
    
    
 	
	@After
	public void tearDown() throws IOException {
		mockServer.stopServer();
		System.out.println("HTTP server stopped");
	}

}
