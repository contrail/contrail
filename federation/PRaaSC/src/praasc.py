import os
import json
import urllib
import atexit
import logging
import tempfile
import eventlet
from eventlet import wsgi
from eventlet.greenio import shutdown_safe
from eventlet.hubs import hub
from eventlet.green import select
from eventlet.green import subprocess
from ordereddict import OrderedDict
from flask import (Flask, Response, jsonify, redirect, url_for,
                   json, abort, request)
import colander as cl

class DefaultSettings:
    DEBUG = False
    DATA_FILE = os.path.join(tempfile.gettempdir(), 'praasc.json')
    LOGGING_LEVEL = 'ERROR'
    LOGGING_FORMAT = '%(asctime)s %(message)s'
    PUBLIC_KEY = '~/.ssh/id_rsa.pub'
    PRIVATE_KEY = '~/.ssh/id_rsa'
    LOCALTUNNEL_OPEN = 'http://open.localtunnel.com/'
    LOCALTUNNEL_SSH_PORT = 22
    PUBLIC_ADDRESS = ''
    SERVER_HOST = '0.0.0.0'
    SERVER_PORT = 7722

app = Flask(__name__)
app.config.from_object(DefaultSettings)
app.config.from_pyfile('praasc.conf', silent=True)
app.config.from_pyfile('/etc/praasc.conf', silent=True)
app.config.from_envvar('PRAASC_SETTINGS', silent=True)

logger = logging.getLogger(__name__)

eventlet.monkey_patch()

pool = eventlet.GreenPool()

kill_me = []

@atexit.register
def killer():
    for process in kill_me:
        try:
            logger.info('Killing %s' % process.pid)
            process.kill()
        except:
            pass

def is_port(node, val):
    cl.Range(0, 65535)(node, val)

def expand_path(path):
    path = os.path.expandvars(path)
    path = os.path.expanduser(path)
    path = os.path.abspath(path)
    return path

def rest_response(code, location=None):
    headers = {}
    
    if location:
        headers['Location'] = location
    
    return Response(status=code, headers=headers)

class EntrySchema(cl.Schema):
    name = cl.SchemaNode(cl.Str())
    local_port = cl.SchemaNode(cl.Int(), validator=is_port, missing=None)
    remote_host = cl.SchemaNode(cl.Str())
    remote_port = cl.SchemaNode(cl.Int(), validator=is_port)

class BaseProxy(object):
    def __init__(self, remote_host, remote_port):
        self.remote = (remote_host, remote_port)
        self.sockets = []
        self.closed = False
    
    def handle(self, client_sock):
        remote_sock = None
        
        self.sockets.append(client_sock)
        
        try:
            remote_sock = eventlet.connect(self.remote)
            self.sockets.append(remote_sock)
            
            while True:
                if self.closed:
                    break
                
                r, _, _ = select.select([client_sock, remote_sock], [], [])
                
                if client_sock in r:
                    data = client_sock.recv(34816)
                    if not data:
                        break
                    remote_sock.sendall(data)
                
                if remote_sock in r:
                    data = remote_sock.recv(34816)
                    if not data:
                        break
                    client_sock.sendall(data)
            
        except Exception, e:
            logger.error(e, exc_info=True)
        finally:
            try:
                if remote_sock is not None:
                    logger.debug('Closing remote_sock %r' % remote_sock)
                    shutdown_safe(remote_sock)
                    self.sockets.remove(client_sock)
            except:
                pass
            
            try:
                logger.debug('Closing client_sock %r' % remote_sock)
                shutdown_safe(client_sock)
                self.sockets.remove(client_sock)
            except:
                pass

class Proxy(BaseProxy):
    def __init__(self, local_port, remote_host, remote_port):
        self.local_port = local_port
        super(Proxy, self).__init__(remote_host, remote_port)
    
    def listen(self, server):
        logger.info('Proxy server listening on port %s' % self.local_port)
        
        while True:
            try:
                client_sock, address = server.accept()
                
                if self.closed:
                    shutdown_safe(client_sock)
                    break
                
                logger.info('Accepted %s:%d' % address)
                pool.spawn_n(self.handle, client_sock)
            except Exception, e:
                logger.error(e, exc_info=True)
                break
        
        logger.debug('Closing proxy server %r' % server)
        
        shutdown_safe(server)
        server.close()
    
    def proxy(self):
        try:
            server = eventlet.listen(('0.0.0.0', self.local_port))
        except Exception, e:
            logger.error(e, exc_info=True)
            return
        
        self.server_sock = server
        self.sockets.append(server)
        self.main_thread = pool.spawn(self.listen, server)
        
        return True
    
    def close(self):
        self.closed = True
        
        for sock in self.sockets:
            shutdown_safe(sock)
        
        self.sockets = []
        
        self.main_thread.cancel()

class LocalTunnel(BaseProxy):
    def __init__(self, remote_host, remote_port, public_key,
                 private_key, localtunnel_open, localtunnel_ssh_port):
        
        self.public_key = public_key
        self.private_key = private_key
        self.localtunnel_open = localtunnel_open
        self.localtunnel_ssh_port = localtunnel_ssh_port
        super(LocalTunnel, self).__init__(remote_host, remote_port)
    
    def _get_tunnel_data(self):
        for i in range(3):
            data = urllib.urlencode({'key': self.public_key})
            resp = urllib.urlopen(self.localtunnel_open, data=data)
            out = resp.read()
        
            if 'No port available' in out:
                continue
            
            return json.loads(out)
    
    def proxy(self):
        try:
            data = self._get_tunnel_data()
            
            if not data:
                return
            
            logger.info('LocalTunnel proxy: http://%s' % data['host'])
            
            cmd = ['ssh', '-N', '-o', 'StrictHostKeychecking=no']
            cmd += ['-l', data['user']]
            cmd += ['-R', '127.0.0.1:%s:%s:%s' % (data['through_port'],
                                            self.remote[0], self.remote[1])]
            cmd += ['-p', str(self.localtunnel_ssh_port)]
            cmd += ['-i', self.private_key]
            cmd += [data['host']]
            
            logger.debug('Running cmd: %s' % ' '.join(cmd))
            
            self.process = subprocess.Popen(cmd)
            
            kill_me.append(self.process)
            
            return data['host']
        except Exception, e:
            logger.error(e, exc_info=True)
    
    def close(self):
        try:
            self.process.kill()
        except Exception, e:
            logger.error(e, exc_info=True)
        
        kill_me.remove(self.process)

class PersistentProxyManager(object):
    def __init__(self, file_path=None):
        self.file_path = file_path
        self.data = OrderedDict()
        self.proxies = {}
        self.schema = EntrySchema()
    
    def create_proxy(self, id, entry):
        local_port = entry['local_port']
        remote_host = entry['remote_host']
        remote_port = entry['remote_port']
        
        if local_port == '' or local_port is None:
            proxy = LocalTunnel(remote_host, remote_port, self.public_key,
                                self.private_key, self.localtunnel_open,
                                self.localtunnel_ssh_port)
            
            address = proxy.proxy()
            
            if address:
                self.proxies[id] = proxy
                
                entry['public_address'] = 'http://%s/' % address
                
                return True
        else:
            local_ports = [x['local_port'] for x in self.data.values()
                                            if str(x['id']) != id]
            
            if local_port in local_ports:
                return
            
            proxy = Proxy(local_port, remote_host, remote_port)
            
            if proxy.proxy():
                self.proxies[id] = proxy
                
                if self.public_address:
                    entry['public_address'] = '%s:%d' % (self.public_address,
                                                         local_port)
                else:
                    entry['public_address'] = ':%d' % local_port
                
                return True
        
    def destroy_proxy(self, id):
        proxy = self.proxies.pop(id, None)
        
        if proxy:
            try:
                proxy.close()
            except Exception, e:
                logger.error(e, exc_info=True)
            
            return True
        
        return
    
    def load(self):
        if os.path.exists(self.file_path):
            with open(self.file_path) as f:
                src = f.read()
                data = json.loads(src) if src else {}
                data = OrderedDict(sorted(data.items(), key=lambda x: int(x[0])))
            
            for id, entry in data.items():
                if self.create_proxy(id, entry):
                    self.data[id] = entry
                    self.save()
    
    def save(self):
        with open(self.file_path, 'w') as f:
            json.dump(self.data, f)
    
    def gen_id(self):
        return max(map(int, self.data.keys()) or [0]) + 1
    
    def index(self):
        return self.data.values()
    
    def create(self, entry):
        try:
            entry = self.schema.deserialize(entry)
        except Exception, e:
            logger.error(e, exc_info=True)
            return
        
        id = str(self.gen_id())
        entry['id'] = int(id)
        
        if not self.create_proxy(id, entry):
            return
        
        self.data[id] = entry
        self.save()
        
        return entry
    
    def show(self, id):
        return self.data.get(id, None)
    
    def delete(self, id):
        entry = self.data.pop(id, None)
        
        if entry:
            self.destroy_proxy(id)
            self.save()
            
            return True
        
        return

manager = PersistentProxyManager()

@app.route('/', methods=['GET'])
def index():
    return jsonify({'objects': manager.index()})

@app.route('/', methods=['POST'])
def create():
    entry = manager.create(json.loads(request.data))
    
    if entry:
        return rest_response(201, url_for('show', id=entry['id']))
    else:
        abort(400)

@app.route('/<int:id>', methods=['GET'])
def show(id):
    entry = manager.show(str(id))
    
    if entry:
        return jsonify(entry)
    else:
        abort(404)

@app.route('/<int:id>', methods=['DELETE'])
def delete(id):
    if manager.delete(str(id)):
        return rest_response(204)
    else:
        abort(400)

def configure():
    logging_level = logging.getLevelName(app.config['LOGGING_LEVEL'])
    logging_format = app.config['LOGGING_FORMAT']
    
    logging.basicConfig(format=logging_format, level=logging_level)
    
    public_key = expand_path(app.config['PUBLIC_KEY'])
    
    if not os.path.exists(public_key):
        raise Exception('Public key not found.')
    
    manager.public_key = open(public_key).read()
    
    manager.private_key = expand_path(app.config['PRIVATE_KEY'])
    
    if not os.path.exists(manager.private_key):
        raise Exception('Public key not found.')
    
    manager.file_path = app.config['DATA_FILE']
    manager.localtunnel_open = app.config['LOCALTUNNEL_OPEN']
    manager.localtunnel_ssh_port = int(app.config['LOCALTUNNEL_SSH_PORT'])
    manager.public_address = app.config['PUBLIC_ADDRESS']
    manager.load()

def main():
    configure()
    server = eventlet.listen((
                    app.config['SERVER_HOST'], app.config['SERVER_PORT']))
    pool.spawn_n(wsgi.server, server, app)
    
    while True:
        eventlet.sleep(0.5)

if __name__ == '__main__':
    main()
