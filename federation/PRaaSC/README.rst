PRaaSC
------

PRoxy As A Service for Contrail is simple TCP proxy with RESTful interface.

Dependencies
------------

PRaaSC needs setuptools and python-dev package on Linux to install
successfully::

    # apt-get install python-setuptools python-dev

Installing
----------

Execute following to install package from source directory::


    # python setup.py install

Or execute following to install package source distribution::

    # easy_install praasc.zip

Running application
-------------------

Package creates executable file to use directly from shell::

    $ praasc

This will execute PRaaSC with default settings (0.0.0.0:7722). To use
custom settings, you can create your own config file::

    # /path/to/praasc.conf
    DATA_FILE = '/path/to/praasc.json'
    LOGGING_LEVEL = 'INFO'
    SERVER_HOST = '127.0.0.1'
    SERVER_PORT = 7722

Then you can run following command::

    $ PRAASC_SETTINGS="/path/to/praasc.conf" praasc

Or you can put your config file to `/etc/praasc.conf` and just run::

    $ praasc
