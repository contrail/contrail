from setuptools import setup, find_packages

console_scripts = [
    'praasc = praasc:main',
]

setup(
    name = 'praasc',
    version = '1.0',
    description = 'PRoxy As A Service for Contrail',
    author = 'Luka Zakrajsek',
    author_email = 'luka.zakrajsek@xlab.si',
    package_dir = {'': 'src'},
    py_modules = ['praasc'],
    license = 'MIT',
    classifiers = ['Topic :: Internet :: Proxy Servers',
                 'License :: OSI Approved :: MIT License',
                 'Operating System :: OS Independent',
                 'Programming Language :: Python'],
    zip_safe = False,
    long_description = open('README.rst').read(),
    entry_points = {
        'console_scripts': console_scripts,
    },
    install_requires = [
        'Flask',
        'colander',
        'eventlet',
        'ordereddict'
    ]
)
