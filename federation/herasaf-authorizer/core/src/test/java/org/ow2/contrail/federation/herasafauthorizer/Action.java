package org.ow2.contrail.federation.herasafauthorizer;

public class Action {
    public static final String READ="READ";
    public static final String WRITE="WRITE";
}
