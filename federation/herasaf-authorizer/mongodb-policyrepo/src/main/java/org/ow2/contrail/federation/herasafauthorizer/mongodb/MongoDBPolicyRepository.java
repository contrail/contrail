package org.ow2.contrail.federation.herasafauthorizer.mongodb;

import com.mongodb.*;
import org.apache.log4j.Logger;
import org.herasaf.xacml.core.PolicyRepositoryException;
import org.herasaf.xacml.core.SyntaxException;
import org.herasaf.xacml.core.WritingException;
import org.herasaf.xacml.core.api.PolicyRetrievalPoint;
import org.herasaf.xacml.core.api.UnorderedPolicyRepository;
import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.impl.AttributeType;
import org.herasaf.xacml.core.context.impl.RequestType;
import org.herasaf.xacml.core.policy.Evaluatable;
import org.herasaf.xacml.core.policy.EvaluatableID;
import org.herasaf.xacml.core.policy.PolicyMarshaller;
import org.herasaf.xacml.core.policy.impl.*;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MongoDBPolicyRepository implements
        UnorderedPolicyRepository, PolicyRetrievalPoint {

    private static Logger log = Logger.getLogger(MongoDBPolicyRepository.class);
    private static String collectionName = "policies";

    private static String ID_ATTR = "policyId";
    private static String EMPTY_SUBJECT_ATTR = "anySubject";
    private static String EMPTY_RESOURCE_ATTR = "anyResource";
    private static String EMPTY_ACTION_ATTR = "anyAction";
    private static String XACML_ATTR = "xacmlContent";

    private MatchingMode matchingMode;
    private Mongo mongo;
    private DBCollection collection;

    public MongoDBPolicyRepository(String host, int port, String databaseName, MatchingMode matchingMode,
                                   List<String> indexes) throws UnknownHostException {
        this.matchingMode = matchingMode;
        mongo = new Mongo(host, port);
        DB db = mongo.getDB(databaseName);
        collection = db.getCollection(collectionName);

        if (indexes != null) {
            for (String index : indexes) {
                collection.createIndex(new BasicDBObject(index, 1));
            }
        }
        // add default indexes
        collection.createIndex(new BasicDBObject(EMPTY_SUBJECT_ATTR, 1));
        collection.createIndex(new BasicDBObject(EMPTY_RESOURCE_ATTR, 1));
    }

    @Override
    public void deploy(Evaluatable evaluatable) {
        try {
            if (evaluatable instanceof PolicyType) {
                deploy((PolicyType) evaluatable);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void deploy(PolicyType policy) throws WritingException {
        BasicDBObject document = new BasicDBObject();
        document.put(ID_ATTR, policy.getId().getId());

        // subject
        if (policy.getTarget().getSubjects() != null) {
            for (SubjectType subject : policy.getTarget().getSubjects().getSubjects()) {
                for (SubjectMatchType subjectMatch : subject.getSubjectMatches()) {
                    String key = subjectMatch.getAttributeDesignator().getAttributeId();
                    String value = subjectMatch.getAttributeValue().getContent().get(0).toString();
                    value = value.trim();
                    BasicDBList valuesList = (BasicDBList) document.get(key);
                    if (valuesList == null) {
                        valuesList = new BasicDBList();
                        document.put(key, valuesList);
                    }
                    valuesList.add(value);
                }
            }
        }
        else {
            document.put(EMPTY_SUBJECT_ATTR, true);
        }

        // resource
        if (policy.getTarget().getResources() != null) {
            for (ResourceType resource : policy.getTarget().getResources().getResources()) {
                for (ResourceMatchType resourceMatch : resource.getResourceMatches()) {
                    String key = resourceMatch.getAttributeDesignator().getAttributeId();
                    String value = resourceMatch.getAttributeValue().getContent().get(0).toString();
                    value = value.trim();
                    BasicDBList valuesList = (BasicDBList) document.get(key);
                    if (valuesList == null) {
                        valuesList = new BasicDBList();
                        document.put(key, valuesList);
                    }
                    valuesList.add(value);
                }
            }
        }
        else {
            document.put(EMPTY_RESOURCE_ATTR, true);
        }

        // action
        if (policy.getTarget().getActions() != null) {
            for (ActionType action : policy.getTarget().getActions().getActions()) {
                for (ActionMatchType actionMatch : action.getActionMatches()) {
                    String key = actionMatch.getAttributeDesignator().getAttributeId();
                    String value = actionMatch.getAttributeValue().getContent().get(0).toString();
                    value = value.trim();
                    BasicDBList valuesList = (BasicDBList) document.get(key);
                    if (valuesList == null) {
                        valuesList = new BasicDBList();
                        document.put(key, valuesList);
                    }
                    valuesList.add(value);
                }
            }
        }
        else {
            document.put(EMPTY_ACTION_ATTR, true);
        }


        StringWriter writer = new StringWriter();
        PolicyMarshaller.marshal(policy, writer);
        document.put(XACML_ATTR, writer.toString());
        collection.insert(document);
    }

    @Override
    public void deploy(Collection<Evaluatable> evaluatables) {
        for (Evaluatable evaluatable : evaluatables) {
            deploy(evaluatable);
        }
    }

    @Override
    public void undeploy(EvaluatableID evaluatableID) {
        DBObject document = findEvaluatable(evaluatableID);
        collection.remove(document);
    }

    @Override
    public void undeploy(Collection<EvaluatableID> evaluatableIDs) {
        for (EvaluatableID evaluatableID : evaluatableIDs) {
            undeploy(evaluatableID);
        }
    }

    @Override
    public List<Evaluatable> getDeployment() {
        DBCursor cursor = collection.find();
        List<Evaluatable> evaluatables = new ArrayList<Evaluatable>();
        try {
            while (cursor.hasNext()) {
                Evaluatable evaluatable = convertDBObject(cursor.next());
                evaluatables.add(evaluatable);
            }
        }
        finally {
            if (cursor != null)
                cursor.close();
        }
        return evaluatables;
    }

    @Override
    public Evaluatable getEvaluatable(EvaluatableID evaluatableID) {
        DBObject document = findEvaluatable(evaluatableID);
        if (document != null) {
            return convertDBObject(document);
        }
        else {
            throw new PolicyRepositoryException("No Evaluatable with ID "
                    + evaluatableID.getId() + " found.");
        }
    }

    @Override
    public List<Evaluatable> getEvaluatables(RequestCtx requestCtx) {
        RequestType request = requestCtx.getRequest();

        // subject conditions
        BasicDBList subjectConditions = new BasicDBList();
        if (request.getSubjects() != null) {
            assert request.getSubjects().size() == 1;
            org.herasaf.xacml.core.context.impl.SubjectType subject = request.getSubjects().get(0);
            for (AttributeType attribute : subject.getAttributes()) {
                String key = attribute.getAttributeId();
                String value = attribute.getAttributeValues().get(0).getContent().get(0).toString();
                BasicDBObject obj = new BasicDBObject();
                obj.put(key, value);
                subjectConditions.add(obj);
            }
            BasicDBObject emptySubjectExpr = new BasicDBObject();
            emptySubjectExpr.put(EMPTY_SUBJECT_ATTR, true);
            subjectConditions.add(emptySubjectExpr);
        }

        // resource conditions
        BasicDBList resourceConditions = new BasicDBList();
        if (request.getResources() != null) {
            assert request.getSubjects().size() == 1;
            org.herasaf.xacml.core.context.impl.ResourceType resource = request.getResources().get(0);
            for (AttributeType attribute : resource.getAttributes()) {
                String key = attribute.getAttributeId();
                String value = attribute.getAttributeValues().get(0).getContent().get(0).toString();
                BasicDBObject obj = new BasicDBObject();
                obj.put(key, value);
                resourceConditions.add(obj);
            }
            BasicDBObject emptyResourceExpr = new BasicDBObject();
            emptyResourceExpr.put(EMPTY_RESOURCE_ATTR, true);
            resourceConditions.add(emptyResourceExpr);
        }

        // create search query
        BasicDBObject searchQuery = new BasicDBObject();
        if (matchingMode == MatchingMode.MIXED) {
            BasicDBList andOperands = new BasicDBList();

            BasicDBObject subjOrExpr = new BasicDBObject();
            subjOrExpr.put("$or", subjectConditions);
            andOperands.add(subjOrExpr);

            BasicDBObject resOrExpr = new BasicDBObject();
            resOrExpr.put("$or", resourceConditions);
            andOperands.add(resOrExpr);

            searchQuery.put("$and", andOperands);
        }
        else if (matchingMode == MatchingMode.SUBJECT) {
            searchQuery.put("$or", subjectConditions);
        }
        else if (matchingMode == MatchingMode.RESOURCE) {
            searchQuery.put("$or", resourceConditions);
        }
        else {
            throw new UnsupportedOperationException();
        }


        DBCursor cursor = collection.find(searchQuery);
        List<Evaluatable> evaluatables = new ArrayList<Evaluatable>();
        try {
            while (cursor.hasNext()) {
                Evaluatable evaluatable = convertDBObject(cursor.next());
                evaluatables.add(evaluatable);
            }
        }
        finally {
            if (cursor != null)
                cursor.close();
        }

        return evaluatables;
    }

    private DBObject findEvaluatable(EvaluatableID evaluatableID) throws PolicyRepositoryException {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put(ID_ATTR, evaluatableID.getId());
        DBCursor cursor = collection.find(searchQuery);
        try {
            if (cursor.count() == 1) {
                return cursor.next();
            }
            else if (cursor.count() > 1) {
                throw new PolicyRepositoryException(
                        String.format("More than one policy with ID '%s' were found.", evaluatableID.getId()));
            }
            else {  // count == 0
                return null;
            }
        }
        finally {
            if (cursor != null)
                cursor.close();
        }
    }

    private Evaluatable convertDBObject(DBObject document) throws PolicyRepositoryException {
        String xacmlString = (String) document.get(XACML_ATTR);
        StringReader reader = new StringReader(xacmlString);
        try {
            return PolicyMarshaller.unmarshal(reader);
        }
        catch (SyntaxException e) {
            throw new PolicyRepositoryException("Failed to unmarshal XACML document from string.");
        }
    }

    protected void clearRepository(String databaseName) {
        mongo.dropDatabase(databaseName);
    }

    public static enum MatchingMode {
        SUBJECT,
        RESOURCE,
        MIXED
    }
}
