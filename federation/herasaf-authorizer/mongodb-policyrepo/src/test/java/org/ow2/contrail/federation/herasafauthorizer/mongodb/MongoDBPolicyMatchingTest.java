package org.ow2.contrail.federation.herasafauthorizer.mongodb;

import org.apache.log4j.Logger;
import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.RequestCtxFactory;
import org.herasaf.xacml.core.policy.Evaluatable;
import org.herasaf.xacml.core.policy.EvaluatableID;
import org.herasaf.xacml.core.policy.PolicyMarshaller;
import org.herasaf.xacml.core.policy.impl.EvaluatableIDImpl;
import org.herasaf.xacml.core.policy.impl.PolicyType;
import org.herasaf.xacml.core.simplePDP.SimplePDPFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Ignore  // requires MongoDB running on local machine
public class MongoDBPolicyMatchingTest {
    private static Logger log = Logger.getLogger(MongoDBPolicyMatchingTest.class);
    private static String databaseName = "xacml-policy-repository-test";
    private MongoDBPolicyRepository repository;

    @Before
    public void setUp() throws UnknownHostException {
        SimplePDPFactory.getSimplePDP(); // This line is needed that the JAXB stuff is initialized.
        String[] indexes = {"subject:username", "subject:role", "resource:resource-id"};
        List<String> indexList = Arrays.asList(indexes);
        repository = new MongoDBPolicyRepository("localhost", 27017, databaseName,
                MongoDBPolicyRepository.MatchingMode.MIXED, indexList);
    }

    @Test
    public void testPolicyMatching() throws Exception {
        log.trace("testPolicyMatching() started.");

        String[] policyFiles = {"policy1.xml", "policy2.xml", "policy3.xml", "policy4.xml", "policy5.xml",
                "policy6.xml", "policy7.xml"};

        // deploy policies
        for (String fileName : policyFiles) {
            File f = new File("src/test/resources/" + fileName);
            Evaluatable evaluatable = PolicyMarshaller.unmarshal(f);
            repository.deploy(evaluatable);
        }

        List<Evaluatable> deployment = repository.getDeployment();
        assertEquals(deployment.size(), 7);

        EvaluatableID evaluatableID = new EvaluatableIDImpl("policy3");
        PolicyType policy1 = (PolicyType) repository.getEvaluatable(evaluatableID);
        assertEquals(policy1.getPolicyId(), "policy3");

        RequestCtx request;
        List<Evaluatable> evaluatables;

        // request1
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request1.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy1", "policy4", "policy7"});

        // request2
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request2.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy1", "policy3", "policy4", "policy7"});

        // request3
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request3.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy2", "policy4", "policy7"});

        // request4
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request4.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy2", "policy4", "policy5", "policy7"});

        // request5
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request5.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy6", "policy7"});

        // request6
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request6.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy1", "policy4", "policy6", "policy7"});

        // request7
        request = RequestCtxFactory.unmarshal(new File("src/test/resources/request7.xml"));
        evaluatables = repository.getEvaluatables(request);
        checkMatchedPolicies(evaluatables, new String[]{"policy1", "policy3", "policy4", "policy6", "policy7"});

        log.trace("testPolicyMatching() finished successfully.");
    }

    @After
    public void tearDown() {
        repository.clearRepository(databaseName);
    }

    private String readTextFile(String filePath) throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(filePath));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = in.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        in.close();
        return sb.toString();
    }

    private void checkMatchedPolicies(List<Evaluatable> evaluatables, String[] policyIDs) {
        assertEquals(evaluatables.size(), policyIDs.length);
        List<String> policyIDList = Arrays.asList(policyIDs);
        for (Evaluatable evaluatable : evaluatables) {
            assertTrue(policyIDList.contains(
                    evaluatable.getId().getId()
            ));
        }
    }
}
