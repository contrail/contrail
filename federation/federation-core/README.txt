				-------------------------------------------
						Federation Core
				-------------------------------------------
						2011-12-12

Definition

  To include Federation Core as dependency of one maven project you need to be sure to add a "dependency
  tag" in your pom.xml of your project:

  ---------------------------------------------------------------------------------------
  <dependency>
    <groupId>org.ow2.contrail.federation</groupId>
    <artifactId>federation-core</artifactId>
    <version>0.1-SNAPSHOT</version> 
    <scope>compile</scope>
  </dependency>
  ---------------------------------------------------------------------------------------
  
  Maven will retrieve the federation-core's pom from the remote artifact downloading it in your 
  local repository.


Development

  The easiest way to import the project into Eclipse is to use m2eclipse.
  
  IMPORTANT: To correctly setup the project environment in eclipse first you have to compile the 
  maven project and after build the ".project" resource of federation-core. See "Compiling" section.


Dependencies

  The Federation Core depends from several libraries:
  
  * json-rpc 1.0
  * junit 4.10
  * hamcrest-core 1.1
  * log4j 1.2.16
  * cnr-contrail-pep 0.0.1-SNAPSHOT
  * wink-client 1.1.3-incubating
  * wink-common 1.1.3-incubating
  * jsr311-api 1.1.1
  * jetty 6.1.7
  * jetty-util 6.1.7
  * servlet-api-2.5 6.1.7
  * cglib-nodep 2.2
  * geronimo-spec-j2ee-jacc 1.0-rc4
  * jsr250-api 1.0
  * axiom-impl 1.2.7
  * axiom-api 1.2.7
  * neethi 3.0.1
  * tuscany-sca-all 1.6.2
  * jgrapht 0.8.2 
  * ovf-parser 0.1-snapshot
  * overlayweaver 0.10.3 

  The dependencies are automatically resolved by Maven compiling the code.

 
Resources

  Resides under src/main/java/resources. It contains:
  
  * internal library, mandatory for a correct compilation phase
  * settings file, mandatory for a correct execution
  * misc files


Parent

  Federation Core is compilable as separated maven project. Its location is under:
 
  - trunk/federation/src/federation-core
  
  and its direclty parent is located under:

  - trunk/federation/pom.xml

  The parent TAG in its own pom.xml is:

  ---------------------------------------------------------------------------------------
  <parent>
    <groupId>org.ow2.contrail</groupId>
    <artifactId>contrail</artifactId>
    <version>0.2-SNAPSHOT</version>
  </parent>  
  ---------------------------------------------------------------------------------------

  
Compiling
  
  To simply compile the maven project go in federation-core folder and type:

  ---------------------------------------------------------------------------------------
  $ mvn clean compile
  ---------------------------------------------------------------------------------------

  If you also need to create the ".project" resource for eclipse, after compiled with maven, type:

  ---------------------------------------------------------------------------------------
  $ mvn clean eclipse:eclipse
  ---------------------------------------------------------------------------------------


Running application

  TO DO


Running unit tests

  TO DO


Generating documentation

  TO DO
