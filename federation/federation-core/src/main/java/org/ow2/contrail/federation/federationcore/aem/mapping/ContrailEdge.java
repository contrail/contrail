package org.ow2.contrail.federation.federationcore.aem.mapping;

import org.jgrapht.graph.DefaultEdge;

@SuppressWarnings("serial")
public class ContrailEdge extends DefaultEdge{

	private String edgeName = "";
	private int weight = 1;

	public ContrailEdge(int w, String n) {
		super();
		this.weight = w;
		this.edgeName = n;		
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void setEdgeName(String edgeName) {
		this.edgeName = edgeName;
	}

	public String getEdgeName() {
		return edgeName;
	}

}
