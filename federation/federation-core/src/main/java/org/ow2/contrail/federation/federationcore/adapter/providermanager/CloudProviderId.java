package org.ow2.contrail.federation.federationcore.adapter.providermanager;

public class CloudProviderId {
	String providerId;
	
	public CloudProviderId(String p_id)
	{
		providerId = p_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((providerId == null) ? 0 : providerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CloudProviderId other = (CloudProviderId) obj;
		if (providerId == null) {
			if (other.providerId != null)
				return false;
		} else if (!providerId.equals(other.providerId))
			return false;
		return true;
	}
	
	public String toString(){
		return providerId;
	}
	
	/*
	public int toInteger(){
		return Integer.parseInt(providerId);
	}
	*/
	
}
