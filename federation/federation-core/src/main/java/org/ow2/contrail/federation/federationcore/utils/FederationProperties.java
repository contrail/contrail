package org.ow2.contrail.federation.federationcore.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.exception.ExistingFederationPropertiesException;

/**
 * Implementation of the FederationProperties using java properties.
 */
public class FederationProperties
{
	private static Logger logger = Logger.getLogger(FederationProperties.class);
	private Properties p;
	private String filename;
	private static FederationProperties instance = null;

	/**
	 * Creates the instance of the Federation Properties
	 * @param path
	 * @throws ExistingFederationPropertiesException
	 */
	public static void createFederationProperties(String path) // throws ExistingFederationPropertiesException
	{
		if ((instance == null) && (path != null))
		{
			logger.info("Core is loading configuration file from " + path);
			instance = new FederationProperties(path);
		}
		else
		{
			logger.warn("Federation Properties: cannot create a different federation properties instance.");
			//throw new ExistingFederationPropertiesException("Federation Properties: can not create a different federation properties instance");
		}

	}
	
	public static void createFederationProperties() {
		if ((instance == null)) {
			instance = new FederationProperties();
		}
		else {
			logger.warn("Federation Properties: cannot create a different federation properties instance.");
		}
	}

	private FederationProperties(){
		p = createDefaultProperties();
	}
	
	private Properties createDefaultProperties() {
		Properties p = new Properties();
		p.setProperty("pdp_path", "http://146.48.96.76:8080/axis2/services/UconWs");
		p.setProperty("authz-enabled", "true");
		p.setProperty("vin-agent-context", "host");
		return p;
	}

	private FederationProperties(String path) {
		try {
			load(path);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the unique instance of the class.
	 */
	public static FederationProperties getInstance() {
		return instance;
	}

	/**
	 * Loads the properties file.
	 * 
	 * @param filename
	 *            , the name of the file that contains the properties.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void load(String filename) throws FileNotFoundException, IOException
	{
		this.filename = filename;

		p = new Properties();
		// String env = System.getenv().get(COREPATH);
		p.setProperty("vin-agent-context", "host");
		p.setProperty("gafs-server-ip", "");
		p.setProperty("gafs-server-port", "");
		p.setProperty("fedapi-port", "8085");
		
		try {
			if (new File(filename).exists())
			{
				p.load(new FileReader(new File(filename)));
				logger.info("Federation Settings: settings loaded from " + filename);
			}
			else
			{
				if (new File(System.getProperty("user.dir") + "/" + "src/main/resources/" + filename).exists())
				{
					p.load(new FileReader(new File(System.getProperty("user.dir") + "/" + "src/main/resources/" + filename)));
					logger.warn("Federation Settings: (WARNING) settings loaded from \"testing configuration\" under "
							+ System.getProperty("user.dir") + "/" + "src/main/resources/" + filename + ". Check out your installation");
				}
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves the properties to disk.
	 */

	public void save()
	{
		try
		{
			p.store(new FileOutputStream(filename), "Federation properties:");
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Gets the value of the specified property.
	 * 
	 * @param propertyName
	 *            , the name of the property to get.
	 * @return the property value as a <code>String</code>.
	 */

	public String getProperty(String propertyName)
	{
		if (p.getProperty(propertyName) != null){
			return p.getProperty(propertyName).trim();
		}
		else 
			return "";
	}

	/**
	 * Sets the value of the specified property.
	 * 
	 * @param propertyName
	 *            , the name of the property to set.
	 * @param propertyValue
	 *            , the value to set the property.
	 */
	public void setProperty(String propertyName, String propertyValue)
	{
		p.setProperty(propertyName, propertyValue);
	}
}