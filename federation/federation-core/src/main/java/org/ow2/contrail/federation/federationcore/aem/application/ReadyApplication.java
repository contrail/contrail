package org.ow2.contrail.federation.federationcore.aem.application;

import java.util.Collection;
import java.util.Set;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.CloudProviderId;
import org.ow2.contrail.federation.federationcore.aem.mapping.MappingPlan;


public class ReadyApplication {
	private MappingPlan[] orderedMappingPlans;
	private Collection<ApplianceStartupDescriptor> startupDesc;
	private String name;
	private String id;

	public ReadyApplication(MappingPlan[] mappings, Collection<ApplianceStartupDescriptor> startupDesc, String name, String fedId) {
		orderedMappingPlans = mappings;
		this.startupDesc = startupDesc;
		this.name = name;
		this.id = fedId;
	}

	
	public MappingPlan[] getOrderedMappingPlans() {
		return orderedMappingPlans;
	}


	public void setOrderedMappingPlans(MappingPlan[] orderedMappingPlans) {
		this.orderedMappingPlans = orderedMappingPlans;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	/**
	 * In case when the application is not splitted, it get the mapping plan related to 
	 * that provider
	 * @return
	 */
	public MappingPlan getMappingPlanForSingleProvider(CloudProviderId id){
		MappingPlan m1 = null;
		for (MappingPlan m : orderedMappingPlans){
			Set<CloudProviderId> ks = m.getMapping().keySet();
			if (ks.contains(id)){
				m1 = m;
				break;
			}
		}
		return m1;
	}
}
