package org.ow2.contrail.federation.federationcore.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;

import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;

import org.ow2.contrail.federation.federationcore.utils.GraphUtils;
import org.ow2.contrail.federation.federationcore.utils.OvfDom;
import org.w3c.dom.Document;

/**
 * @author gae
 *
 */
public class GraphManipulation {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String ovf = readOvfFromFile();
		ApplicationDescriptor appDesc = OVFParser.ParseOVF(ovf);
		ApplicationGraph<String, ContrailEdge> appGraph = GraphUtils.MakeGraph(-1, appDesc);
		System.out.println(appGraph.toString());
		modifyOvfPerVirtualSystem(ovf);
	}

	static String readOvfFromFile() throws IOException{
		String appPath = System.getProperty("user.dir");
		// FileReader fr = new FileReader(appPath + "/src/main/resources/ovf-MultipleVM-cnr-ubntServer.xml");
		FileReader fr = new FileReader(appPath + "/src/main/resources/dsl-test-xlab.xml");
		BufferedReader br = new BufferedReader(fr);
		StringBuffer sb = new StringBuffer();
		while (br.ready()) {
			sb.append(br.readLine());
		}
		return sb.toString();
	}
	
	static void modifyOvfPerVirtualSystem(String ovf) throws FileNotFoundException{
		OvfDom ovfd = new OvfDom(ovf);
		
		if (ovfd != null){
			ovfd.addVINProperty("VIN_IBIS_SERVER", "localhost");
			ovfd.addVINProperty("VIN_POOL_NAME", "ibis");
			ovfd.addVINProperty("VIN_AGENT_CONTEXT", "vm");
			ovf = ovfd.toString();
		}
		
		PrintWriter out = new PrintWriter("/tmp/news.xml");
		out.println(ovf);
		out.close();
	}
}
