package org.ow2.contrail.federation.federationcore.monitoring;

public enum VMMetrics 
{	
	disk,
	memory,
	common,
	cpu,
	network
}
