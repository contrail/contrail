package org.ow2.contrail.federation.federationcore.authz.oauth;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class CertificatesCache {
	
	private static Logger logger = Logger.getLogger(CertificatesCache.class);
	private static String loggerTag = "[oauth.cache] ";

	private final int initialUsers = 2;
	Map<String, X509Certificate> cache = null;
	private static CertificatesCache instance = null;
	
	public static boolean DISABLED = true;
	
	private CertificatesCache (){
		cache = new HashMap<String, X509Certificate>(initialUsers);
	}

	public static synchronized CertificatesCache getInstance() {
        if (instance == null) 
            instance = new CertificatesCache();
        return instance;
    }
	
	public void put(String uuid, X509Certificate c){
		cache.put(uuid, c);
	}
	
	public X509Certificate getCertificate(String uuid){
		X509Certificate value = (X509Certificate) cache.get(uuid);
		return value;
	}
	
	/**
	 * Returns a void string if the certificate is not present for the user
	 * @param uuid
	 * @return
	 */
	public String getSerializedCertificate(String uuid){
		if (CertificatesCache.DISABLED) return null;
		String s = null;
		try {
			X509Certificate value = (X509Certificate) cache.get(uuid);
			s = new String(Base64.encodeBase64(value.getEncoded()));
			logger.debug(loggerTag + "getting from cache");
		} 
		catch (CertificateEncodingException e) {
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			// System.out.println("Not in cache");
		}
		return s;
	}
	
	public static void main(String[] args) throws Exception {
		CertificatesCache.getInstance();
	}
}
