package org.ow2.contrail.federation.federationcore.test;

import java.io.BufferedReader;

import org.codehaus.jettison.json.JSONArray;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.federation.federationcore.adapter.gafs.GlobalFileSystemRegistry;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistrator;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistry;
import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;

public class VinInteraction {
	private static Logger logger = Logger.getLogger(VinInteraction.class);
	
	static void printVinIds(String appName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(appName);
		logger.info("*** Associations between VIN VMs and Appliance names ***");
		for (Map.Entry<String, String> e : m.entrySet()){
			System.out.println(e.getKey() + ": " + e.getValue());
		}
	}
	
	private static void printVinIdsAsJSON(String applicationName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(applicationName);
		JSONObject jsonVin2OVF = new JSONObject(m);
		logger.info("*** JSON Associations between VIN VMs and Appliance names ***");
		System.out.println(jsonVin2OVF);
	}
	
	static void printVinIdsAsJSONArray(String applicationName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(applicationName);
		Set<String> k = m.keySet();
		
		ArrayList<String> coll = new ArrayList<String>(k.size());
		for(String s : k ){
			coll.add(m.get(s));
		}
		JSONArray vs = new JSONArray(coll);
		JSONArray jsonVin2OVF = new JSONArray(k);
		logger.info("*** JSON Associations between VIN VMs and Appliance names ***");
		System.out.println(jsonVin2OVF);
		System.out.println(vs);
	}
	
	static void printVinAddress(String applicationName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(applicationName);
		Set<String> vmIds = m.keySet();
		for (String vmId : vmIds){
			String address = registry.getVmAddressByVmId(vmId);
			System.out.println(address);
		}
		
	}
	
	public static ApplicationGraph<String, ContrailEdge> setUpApp(String ovfName) throws Exception{
		ApplicationGraph<String, ContrailEdge> appGraph = null;
		String appPath = System.getProperty("user.dir");
		System.out.println(appPath);

		FileReader fr = new FileReader(appPath + "/src/main/resources/" + ovfName);

		BufferedReader br = new BufferedReader(fr);
		StringBuffer sb = new StringBuffer();
		while (br.ready()) {
			sb.append(br.readLine());
		}
		br.close();
		String ovf = sb.toString();
		ApplicationDescriptor appDesc = OVFParser.ParseOVF(ovf);
		appGraph = GraphUtils.MakeGraph(-1, appDesc);
		return appGraph;
	}
	
	public static void main(String[] args) throws Exception {
		
		String appUuid = "ffffffffff";
		
		/*
		System.out.println("First App");
		ApplicationGraph<String, ContrailEdge> appGraph1 = setUpApp("ovf-2vms-vep2.0_shared.xml");
		printVinIdsAsJSON(appGraph1.getName());

		VirtualNetworkRegistrator vinApp1 = new VirtualNetworkRegistrator(appGraph1, appUuid, "sharedVolume");
		logger.info("(Main) Ending registration of VIN for application "+ appGraph1.getName() + "*****************");
		List<String> s = VirtualNetworkRegistrator.getVinConnParams();
		logger.info("(Main) " + s);
		logger.info("(Main) Ending getting params of VIN for application "+ appGraph1.getName() + "+++++++++++++++");
		
		logger.info("(Main) ");
		printVinIdsAsJSON(appGraph1.getName());
		
		*/

		/*
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		registry.addApplication("Pippo", appGraph1.getName(), "1");
		
		// String dep_id = registry.getAppDeployIdByName(appGraph.getName(), "Pippo");
		String appName = registry.getAppNameByDeployId("1", "Pippo");
		if (!appName.equals(appGraph1.getName()))
			logger.error("(Main) Get app deploy id NOT WORK");
		else
			logger.info("(Main) Get app deploy id WORK");
		
		*/
		
		
		System.out.println();
		System.out.println("Second app");
		VirtualNetworkRegistry reg = VirtualNetworkRegistry.getInstance();
		// ApplicationGraph<String, ContrailEdge> appGraph2 = setUpApp("ovfs/ovf-MultipleVM-cnr-ubntServer.xml");
		ApplicationGraph<String, ContrailEdge> appGraph2 = setUpApp("ovf-2vms-vep2.0.ovf");
		
		VirtualNetworkRegistrator vinApp2 = new VirtualNetworkRegistrator(appGraph2, appUuid);
		logger.info("(Main)");
		reg.printNetIds();
		
		// VirtualNetworkRegistrator vinApp2 = new VirtualNetworkRegistrator(appGraph2);
		logger.info("(Main) Ending registration of VIN for application "+ appGraph2.getName() + "*****************");
		List<String> s1 = VirtualNetworkRegistrator.getVinConnParams();
		logger.info("(Main) " + s1);
		// assert (s1.equals(s));
		logger.info("(Main) Ending getting params of VIN for application "+ appGraph2.getName() + "+++++++++++++++");
		
		logger.info("(Main) ");
		printVinIdsAsJSON(appUuid);
		printVinAddress(appUuid);
	}
}
