package org.ow2.contrail.federation.federationcore.providermgmt;

import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;

public interface IProviderManagement 
{
	/**
	 * This method must be called for notifying the federation core that a new cloud provider is available. It is assumed that the provider object has
	 * been already registered in the federation-db.
	 * @param provider the provider to be registered
	 * @throws UnknownProviderTypeException
	 * @throws ProviderTypeNotYetSupportedException
	 */
	public void registerProvider(Provider provider) throws UnknownProviderTypeException, ProviderTypeNotYetSupportedException;
}
