package org.ow2.contrail.federation.federationcore.aem.mapping;

import java.util.Comparator;

public class MappingPlanComparator implements Comparator<MappingPlan>{

	@Override
	public int compare(MappingPlan arg0, MappingPlan arg1) {
		Double o1 = arg0.getWeight();
		Double o2 = arg1.getWeight();
		return (o1>o2 ? -1 : (o1==o2 ? 0 : 1));
	}

}
