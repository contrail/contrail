package org.ow2.contrail.federation.federationcore.test;


import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.sla.soap.SoapSlamInterface;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslator;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslatorImplNoOsgi;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.*;
import org.w3c.dom.Document;

public class SOAPClient {
	
	private final static STND GSLAM_EPR = org.slasoi.slamodel.vocab.sla.gslam_epr;
	private final static String fSlamEndpoint = "http://146.48.81.248:8080/services/federationNegotiation?wsdl";
	public static void main (String[] args) throws Exception{
		
		/* TEST HASHMAP ACCESS
		String xmlSlat = Statics.serializeXml("src/main/resources/SlaTOfferFederation2.xml");
		SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
		SLATemplate slaTemplate = slaTranslator.parseSlaTemplate(xmlSlat);
		String negotiationId ="123";
		FederationProposal fedProposal = new FederationProposal(slaTemplate, negotiationId, null);
		HashMap<String, FederationProposal> returnProposal = new HashMap<String, FederationProposal>();
		returnProposal.put(negotiationId, fedProposal);
		System.out.println(returnProposal.get(returnProposal.keySet().iterator().next()).getNegotiationId());
		/*/
		//initiate negotiotion1
		
	    String xmlSlat = Statics.serializeXml("src/main/resources/SlaTOfferFederationWithoutProviders.xml");
	    SoapSlamInterface slam = new SoapSlamInterface();
	    SLATemplate slaTemplate = slam.parseSLATemplate(xmlSlat);
	    SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
	    STND ovfProperty = new STND("OVF_URL");
	    System.out.println(slaTemplate.getInterfaceDeclrs()[0].getPropertyValue(ovfProperty));
		//SLATemplate slaTemplate = slaTranslator.parseSlaTemplate(xmlSlat);
	    String providerList = "{\"ProvidersList\": [{\"provider-uuid\":\"42\", \"p-slam-url\":\"http://146.48.81.255:8080/services/contrailNegotiation?wsdl\"}]}";
	    STND property = new STND("ProvidersList");
	    //System.out.println(xmlSlat);
	    //System.out.println(slaTemplate.getPropertyValue(property));
	    slaTemplate.setPropertyValue(property, providerList);
	    //System.out.println(slaTranslator.renderSlaTemplate(slaTemplate));
		//System.out.println(slaTemplate.getPropertyValue(property));
	    
	    System.out.println(slaTemplate.getPropertyValue(ovfProperty));
	    Party slaTemplateParty = getProviderParty(slaTemplate);
	    Party slaTemplateParty2 = getCunsumerParty(slaTemplate);
	    
	    if (slaTemplateParty2 == null) System.out.println("is null");
	    slaTemplateParty.setPropertyValue(GSLAM_EPR, fSlamEndpoint);
	    slaTemplateParty2.setId(new ID("asda-asda-asda-asda"));
	    Party[] slaTemplateParties = new Party[]{slaTemplateParty, slaTemplateParty2};
	    slaTemplate.setParties(slaTemplateParties);
	    System.out.println(slaTranslator.renderSlaTemplate(slaTemplate));
	    
	   
	    /*
	    String xmlSlat2 = Statics.serializeXml("src/main/resources/SlaTOfferFederationWithoutProviders.xml");
	    String negotiationId = slam.initiateNegotiation(slaTemplate);
	    System.out.println("Negotiation Id in ret: "+ negotiationId);
	    
	    //String xmlOfferSlat = Statics.serializeXml("src/main/resources/SlaTProposalFederation.xml");
	    
	    //initiate negotiotion2
	    String negotiationId2 = slam.initiateNegotiation(slaTemplate);
	    System.out.println("Negotiation Id in ret: "+ negotiationId2);
	    
	    //negotiate1
	    SLATemplate[] offers = slam.negotiate(slaTemplate,negotiationId);
	    System.out.println("OFFERS TO NEGOTIATION 1");
	    for(int i=0; i<offers.length;i++){
	    	if(offers[i]==null) System.out.println("offers "+i+" is null");
	    	else System.out.println(offers[i].toString());
	    }
	    
	    //negotiate2
	    SLATemplate[] offers2 = slam.negotiate(slaTemplate,negotiationId2);
	    System.out.println("OFFERS TO NEGOTIATION 2");
	    for(int i=0; i<offers2.length;i++){
	    	if(offers2[i]==null) System.out.println("offers2 "+i+" is null");
	    	else System.out.println(offers2[i].toString());
	    }
	    //cancel negotiation1
	    boolean cancelNeg1 = slam.cancelNegotiation(negotiationId);
	    System.out.println("cancel return: "+cancelNeg1);
	    
	    //cancel negotiation2
	    //boolean cancelNeg2 = slam.cancelNegotiation(negotiationId2);
	    //System.out.println("cancel return: "+cancelNeg2);
	    //Create Agreement negotiation2
	    
	    SLA sla = slam.createAgreement(offers2[0], negotiationId2);
	    
	    //Retrieve Cee-Id from SLA
	    STND property2 = new STND("CEE-ID");
	    String[] ceeIdpath= sla.getPropertyValue(property2).split("/");
	    String ceeId = ceeIdpath[ceeIdpath.length - 1];
	    System.out.println(sla.getPropertyValue(property2));
	    System.out.println(ceeId);
	    System.out.println("********************************************************************");
	    System.out.println(sla.toString());
	    System.out.println("********************************************************************");
	    System.out.println(slaTranslator.renderSla(sla));
		*/
	}
		
	public static Party getProviderParty(SLATemplate slaTemplate) {
	  Party[] parties = slaTemplate.getParties();
	  for (Party party : parties) {
		   STND partyRole = (STND) party.getAgreementRole();
		   if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
		    return party;
		   }
	  }
	  return null;
	}
	
	public static Party getCunsumerParty(SLATemplate slaTemplate) {
		  Party[] parties = slaTemplate.getParties();
		  for (Party party : parties) {
			   STND partyRole = (STND) party.getAgreementRole();
			   if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
			    return party;
			   }
		  }
		  return null;
		}
	public static class Statics 
	{
		public String slatoffer = "";
				
		static String serializeXml(String path)
		{
			String slat ="";
			try
			{
				
				DocumentBuilderFactoryImpl dbf = new DocumentBuilderFactoryImpl();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(path);
				
				TransformerFactory transfac = TransformerFactory.newInstance();
				Transformer trans = transfac.newTransformer();
				trans.setOutputProperty(OutputKeys.METHOD, "xml");
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", Integer.toString(2));

				StringWriter sw = new StringWriter();
				StreamResult result = new StreamResult(sw);
				DOMSource source = new DOMSource(doc.getDocumentElement());

				trans.transform(source, result);
				slat = sw.toString();
				
			}
			catch (Exception e) {e.printStackTrace();}
			return slat;
		}
		
	}
	
}