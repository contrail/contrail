package org.ow2.contrail.federation.federationcore.aem.mapping;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualHardware;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualSystem;
import org.ow2.contrail.common.implementation.ovf.virtualhardware.OVFVirtualHwCpu;
import org.ow2.contrail.common.implementation.ovf.virtualhardware.OVFVirtualHwMemory;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.*;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

/**
 * Maps an appliance with a list of suitable providers
 */
class ApplianceMapping {
	private Map<CloudProviderId, Double> feasibleSet = null;
	private ApplianceDescriptor appliance = null;
	private Collection<Provider> providers = new ArrayList<Provider>();
	boolean[] allocation_possible = null; //for avoiding iteration among the whole set of providers when checking for a different resource 
	double[] w = null;
	
	ApplianceMapping(ApplianceDescriptor ap, Collection<Provider> cp){
		feasibleSet = new HashMap<CloudProviderId, Double>();
		appliance = ap;
		providers = cp;
		allocation_possible = new boolean[cp.size()];
		w = new double[cp.size()];
		for (int i=0; i<allocation_possible.length; i++){
			allocation_possible[i] = true;
			w[i]=Double.NEGATIVE_INFINITY;
		}
	}
	
	/**
	 * @return An array of integers representing the virtual memory 
	 * 	       required by each VM contained in the appliance
	 */
	int[] getVirtualMemoryRequired(ApplianceDescriptor appl){
		Collection<OVFVirtualSystem> vsc = appl.getVirtualSystems();
		int[] virtualMem = new int[vsc.size()];
		int j=0;
		for (OVFVirtualSystem vs: vsc){
			Collection<OVFVirtualHardware> vh_c = vs.getRequiredHardware();
			Iterator<OVFVirtualHardware> i = vh_c.iterator();
			OVFVirtualHwCpu cpu = (OVFVirtualHwCpu) i.next();
			OVFVirtualHwMemory mem = (OVFVirtualHwMemory) i.next();
			virtualMem[j] = (int) mem.getVirtualQuantity();
			j++;
		}
		return virtualMem;
	}
		
	
	int retrieveServerRamFreeFromDB(Server s){
		int ram_free = -1;
		String att = s.getAttributes();
		JSONObject obj;
		try {
			obj = new JSONObject(att);
			ram_free = obj.getInt("ram_free");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ram_free;
	}
	
	void checkRamFree(){
		int[] reqRams = this.getVirtualMemoryRequired(appliance);
		int reqRam = reqRams[0];
		int i = 0;
		for (Provider p : providers){
			if (allocation_possible[i] == true){
				// probably federation should have aggregated info, so I wouldn't care too much about servers.
				// i.e. do not mark servers and do not iterate for all servers
				// Correct the behavior if such assumption is not true.
				Collection<Server> cs = p.getServerList();
				if (cs != null){
					Iterator<Server> s_i = cs.iterator();
					if (s_i.hasNext()){
						Server s = s_i.next();
						int serverRam = this.retrieveServerRamFreeFromDB(s);
						// System.out.println(applianceName + ": "+  reqRam + "/" + serverRam);
						if (reqRam <= serverRam){
							w[i] =  (serverRam - reqRam) / (double) serverRam;
							// System.out.println(w[i]);
							// break;
						}
						else{
							allocation_possible[i] = false;
						}
					}
				}
			}
			i++;
		}
	}
	
	/** When some checks have been performed for the given appliance, this method return the set of cloud providers
	 * that are suitable for the allocation, i.e. they satisfy the requirements for this appliance. For each cloud providers
	 * also the weight is returned.
	 * 
	 * @return the ids of feasible cloud provider and the corresponding weight
	 */
	Map<CloudProviderId, Double> computeFeasibleSet(){
		int i = 0;
		for (Provider p : providers){
			if (allocation_possible[i] == true){
				feasibleSet.put(new CloudProviderId(p.getUuid()), new Double(w[i]));
			}
			i++;
		}
		return feasibleSet;
	}
	
	public double[] getScores(){
		return w;
	}
	
	@SuppressWarnings("unchecked")
	Collection<Provider> retrieveProvidersFromDB(){
		PersistenceUtils.createInstance("fedPU");
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		List<Provider> providerList = null;
		Query query = (Query) em.createNamedQuery("Provider.findAll");
		providerList = query.getResultList();

		return providerList;
	}
}

public class Mapping{

	private static Logger logger = Logger.getLogger(Mapping.class);
	private static String loggerTag = "[core.aem.mapping] ";

	/**
	 * Checks for each appliance (in isolation) the suitable providers among the providers an user can exploit
	 */
	private static ApplianceMapping checkRequirements(ApplianceDescriptor sd, Collection<Provider> providers ){
		ApplianceMapping ap = new ApplianceMapping(sd, providers);
		ap.checkRamFree();
		return ap;
	}
	
	/***
	 * Calculates a set of possible mapping plan for an application to submit
	 * @param user A <code>User</code>
	 * @param appDesc An <code>ApplicationGraph<String, ContrailEdge></code>
	 * @return A new <code>Collection<MappingPlan></code> or null otherwise 
	 * 
	 */
	public static Collection<MappingPlan> computeMappingPlan(User user, ApplicationGraph<String, ContrailEdge> appDesc){
		/*
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		
		try {
			Query query = em.createNamedQuery("Provider.findAll");
			List<Provider> providerList = query.getResultList();

			for (Provider provider : providerList) {
				for (SLATemplate slaTemplate : provider.getSLATemplateList()) {
				}
			}
		}catch(Exception e){
			
		}*/
		
		// logger.info("\t\t---------------------------------------------------");
		
		// we need to navigate the graph structure to build a proper mapping
		Collection<ApplianceDescriptor> applDescriptors = appDesc.getAppliances();
		
		// Retrieving the root appliance descriptor
		ApplianceDescriptor[] applDescriptorsArray = applDescriptors.toArray(new ApplianceDescriptor[applDescriptors.size()]);
		if (applDescriptorsArray.length == 0){
			logger.warn(loggerTag + "No appliance found!");
			return null;
		}
		
		// Retrieving information about all cloud providers (should we need to discriminate among providers of a particular user?)
		Collection<CloudProviderId> allCloudProviders = ProviderContainer.getProviderList();
		Collection<Provider> dbProviders = getRegisteredProviderObjects(allCloudProviders);
		if (dbProviders.size() == 0){
			logger.warn(loggerTag + "No provider found!");
			return null;
		}
		
		logger.debug( loggerTag + "computing mapping plan for application: " + appDesc.getName());
		ArrayList<MappingPlan> mappingPlans = new ArrayList<MappingPlan>();
		//MappingPlan mp = roundRobinAllocation(allCloudProviders, applDescriptorsArray);
		mappingPlans = intersectionFeasibleAllocation(dbProviders, applDescriptorsArray);
		
		if(mappingPlans.size() == 0)
			logger.warn(loggerTag + "Impossible to compute a plan for " + appDesc.getName());
		
		return mappingPlans;
		
	}

	
	private static Collection<Provider> getRegisteredProviderObjects(Collection<CloudProviderId> cloudDescriptors){
		Collection<Provider> dbProviders = new ArrayList<Provider>();
		for (CloudProviderId id : cloudDescriptors){
			BaseProviderManager cpd;
			try {
				cpd = (BaseProviderManager) ProviderContainer.getInstance(id);
				dbProviders.add(cpd.getProvider());
			}
			catch (UnknownProviderErrorException e) {
				logger.warn("Mapping Module: No provider found" + e.getMessage());
			}
		}
		return dbProviders;
	}
	
	private static ArrayList<MappingPlan> intersectionFeasibleAllocation(Collection<Provider> userProviders, ApplianceDescriptor[] applDescriptorsArray){
		// initializing the weights map for each providers 
		int provNumb = userProviders.size();
		Map<CloudProviderId, Double> weight_map = new HashMap<CloudProviderId, Double>(provNumb);
		for (Provider p : userProviders){
			CloudProviderId id = new CloudProviderId(p.getUuid());
			weight_map.put(id, 0.0);
		}
		
		// constructing the list of sets, with each set representing the mapping between an appliance and the providers
		Collection<Set<CloudProviderId>> s2p_sets = new ArrayList<Set<CloudProviderId>>(applDescriptorsArray.length); 
		for (ApplianceDescriptor s : applDescriptorsArray){
			ApplianceMapping s2p = checkRequirements(s, userProviders);
			Map<CloudProviderId, Double> m = s2p.computeFeasibleSet();
			Set<CloudProviderId> plist = m.keySet();
			s2p_sets.add(plist);
			
			for(CloudProviderId p : plist){
				Double tmp = weight_map.get(p);
				tmp += m.get(p);
				weight_map.put(p, tmp);
			}
		}

		// intersection between the list of sets
		Iterator<Set<CloudProviderId>> clid = s2p_sets.iterator();
		Set<CloudProviderId> first = clid.next();
		Set<CloudProviderId> intersection = new HashSet<CloudProviderId>(first);
		while (clid.hasNext()){
			intersection.retainAll(clid.next());
		}
		
		ArrayList<MappingPlan> mappingPlans = new ArrayList<MappingPlan>();
		for (CloudProviderId p : intersection){
			MappingPlan mp = new MappingPlan();
			for (int i = 0; i < applDescriptorsArray.length; i++){
				mp.addMap(applDescriptorsArray[i], p);	
			}
			mp.setProviderWeight(p, weight_map.get(p));
			logger.debug(p + " weight:" + mp.getProviderWeight(p));
			mappingPlans.add(mp);
		}
		
		return mappingPlans;
		/*

		int index = 0; 
		CloudProviderId[] cloudDescriptorsArray = new CloudProviderId[intersection.size()];
		intersection.toArray(cloudDescriptorsArray);
		
		// getting provider with bigger weight (less load)
		if (cloudDescriptorsArray.length > 1){
			Double max = 0.0;
			for (int i=0; i<cloudDescriptorsArray.length; i++){
				if (max < weight_map.get(cloudDescriptorsArray[i]))
					index = i;
			}
		}
		else {
			return null;
		}
		*/
	}
	
	private static MappingPlan roundRobinAllocation(Collection<CloudProviderId> cloudDescriptors, ApplianceDescriptor[] applDescriptorsArray){
		CloudProviderId[] cloudDescriptorsArray = new CloudProviderId[cloudDescriptors.size()];
		cloudDescriptors.toArray(cloudDescriptorsArray);
		MappingPlan mp = new MappingPlan();
		for (int i = 0; i < applDescriptorsArray.length; i++){
			int index = i % cloudDescriptorsArray.length;
			CloudProviderId cid =  cloudDescriptorsArray[index];
			mp.addMap(applDescriptorsArray[i], cid);
		}
		return mp;
	}
}
