package org.ow2.contrail.federation.federationcore.adapter.providermanager;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.monitoring.BaseApplicationMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.BaseProviderMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public abstract class BaseProviderManager implements ICloudProviderManager
{
	private static Logger logger = Logger.getLogger(BaseProviderManager.class);

	protected ProviderWatcherManager providerWatcherManager;
	protected BaseProviderMonitoring provider_monitor;
	protected BaseApplicationMonitoring app_mon;

	protected ProviderType providerType;

	protected CloudProviderId providerID;
	protected String providerAddress;
	protected String providerName;
	protected Provider providerDbEntity = null;
	
	/**
	 * The enumeration for the kinds of provider involved in the Contrail project.
	 */
	public enum ProviderType
	{
		CONTRAIL, AMAZON;
	}

	BaseProviderManager(ProviderWatcherManager manager, CloudProviderId cpi, Provider provider, ProviderType providerType)
	{
		this.providerID = cpi;
		this.providerAddress = provider.getProviderUri();
		this.providerType = providerType;
		this.providerName = provider.getName();
		this.providerWatcherManager = manager;
		this.providerDbEntity = provider;
		createMonitoring(manager);
		provider_monitor.start();
	}

	public Provider getProvider(){return providerDbEntity;}
	
	/**
	 * Creates and starts the federation-level monitoring system for the provider. 
	 */
	protected abstract void createMonitoring(ProviderWatcherManager manager);
	
	@Override
	public ProviderType getType()
	{
		return providerType;
	}
	
	@Override
	public String getProviderId(){
		return providerID.toString();
	}

	@Override
	public void setProviderID(CloudProviderId ID){
		providerID = ID;
	}

	@Override
	public String getProviderAddress(){
		return providerAddress;
	}

	@Override
	public void setProviderAddress(String address){
		this.providerAddress = address;
	}

	@Override
	public void setProviderName(String name){
		this.providerName = name;
	}

	@Override
	public String getProviderName(){
		return this.providerName;
	}

	/**
	 * Starts an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public Collection<String> start(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException, UnknownProviderErrorException
	{
		// NB: we're assuming that prior to start application-level monitoring, the application has to be started.
		// In this way the subclasses have only to define atomic operation "internalStart" and "startApplicationLevelMonitoring".
		// The base class sequentializes the calls.

		Collection<String> result = internalStart(appDesc, user, appId);

		if (!startApplicationLevelMonitoring(result))
		{
			logger.error("Base Provider Manager: Unable to start application-level monitoring");
			throw new ProviderComunicationException("Contrail Provider Manager: Unable to start application-level monitoring");
		}

		return result;
	}

	/**
	 * Stops an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public boolean stop(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
			UnknownProviderErrorException
	{
		boolean result = internalStop(appDesc, user, appId);

		/*
		if (!stopApplicationLevelMonitoring())
		{
			logger.error("Base Provider Manager: Unable to stop application-level monitoring");
			throw new ProviderComunicationException("Contrail Provider Manager: Unable to stop application-level monitoring");
		}
	    */

		return result;
	}
	
	/**
	 * Performs the actual start of appliances on the provider. Each kind of provider has to implement this method.
	 * @throws UnknownProviderErrorException 
	 */
	protected abstract Collection<String> internalStart(Collection<ApplianceDescriptor> appDesc, User user, String appId)
			throws ProviderComunicationException, UnknownProviderErrorException;

	/**
	 * Performs the actual stop of appliances on the provider. Each kind of provider has to implement this method.
	 * @throws UnknownProviderErrorException 
	 */
	protected abstract boolean internalStop(Collection<ApplianceDescriptor> appDesc, User user, String appId) 
			throws ProviderComunicationException, UnknownProviderErrorException;
	
	/**
	 * Performs the actual kill of appliances on the provider. Each kind of provider has to implement this method.
	 * @throws UnknownProviderErrorException 
	 */
	public abstract boolean internalKill(Collection<ApplianceDescriptor> appDesc, User user, String appId) 
			throws ProviderComunicationException, UnknownProviderErrorException;
	/**
	 * Starts application-level monitoring.
	 */
	protected boolean startApplicationLevelMonitoring(Collection<String> result)
	{
		return true;
	}
	
	/**
	 * Stops application-level monitoring.
	 */
	protected boolean stopApplicationLevelMonitoring()
	{
		return true;
	}
}
