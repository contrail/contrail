package org.ow2.contrail.federation.federationcore.providermgmt;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.ProviderContainer;
import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationcore.usermgmt.UserManagement;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.osoa.sca.annotations.Service;

@Service(IProviderManagement.class)
public class ProviderManagement implements IProviderManagement
{
	private static Logger logger = Logger.getLogger(ProviderManagement.class);

	/**
	 * This method must be called for notifying the federation core that a new cloud provider is available. The provider object should be already in 
	 * the federation-db.
	 */
	@Override
	public void registerProvider(Provider provider) throws UnknownProviderTypeException, ProviderTypeNotYetSupportedException
	{
		// We can assume that every provider we receive is different from the previously registered
		logger.info("[Provider Management] registering " + provider.getName() + " on federation adapters...");

		ProviderContainer.addProvider(provider);

		logger.info("[Provider Management] \"" + provider.getName() + "\" has been registered");
	}
}
