package org.ow2.contrail.federation.federationcore.adapter.vin;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
@NamedQueries({
	@NamedQuery(name = "VinApps.findAll", query = "SELECT t FROM VinApps t"),
	@NamedQuery(name = "VinApps.findNameByDeployId", query = "SELECT t.appIdFedCore FROM VinApps t WHERE t.appIdProvider = :appDeployId AND t.userId = :userName"),
    @NamedQuery(name = "VinApps.findDeployIdByName", query = "SELECT t.appIdProvider FROM VinApps t WHERE t.appIdFedCore = :appName AND t.userId = :userName"),
    @NamedQuery(name = "VinApps.findDbIdByDeployId", query = "SELECT t.id FROM VinApps t WHERE t.appIdProvider = :appDeployId AND t.userId = :userName"),
})
public class VinApps {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private int appIdProvider;
	private String appIdFedCore;
	private String userId;
	
	public int getAppIdProvider() {
		return appIdProvider;
	}
	public void setAppIdProvider(int appIdProvider) {
		this.appIdProvider = appIdProvider;
	}
	public String getAppIdFedCore() {
		return appIdFedCore;
	}
	public void setAppIdFedCore(String appIdFedCore) {
		this.appIdFedCore = appIdFedCore;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
