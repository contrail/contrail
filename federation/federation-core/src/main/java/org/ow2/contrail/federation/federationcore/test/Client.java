package org.ow2.contrail.federation.federationcore.test;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.Logger;
import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.federation.federationcore.FederationCore;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualInfrastructureNetwork;
import org.ow2.contrail.federation.federationcore.aem.application.IApplicationManagement;
import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;
import org.ow2.contrail.federation.federationcore.exception.ApplicationNotFoundException;
import org.ow2.contrail.federation.federationcore.exception.ApplicationOperationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationcore.providermgmt.IProviderManagement;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.sla.soap.Statics;
import org.ow2.contrail.federation.federationcore.usermgmt.IUserManagement;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.Server;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

/**
 * Contains some demo use cases of the federation for the preliminary scenario.
 */
public class Client
{
	private static SCADomain scaDomain;

	private static Logger logger = Logger.getLogger(Client.class);

	static int increment = 0;

	private static final boolean PROVIDER_REGISTRATION = true;
	private static final boolean USER_CREATION = true;
	private static final boolean APP_SUBMISSION = true;
	private static final boolean APP_START = true;
	private static final boolean APP_STOP = true;
	
	IUserManagement userManagement = null;
	IProviderManagement provManagement = null;
	IApplicationManagement appManagement = null;

	private static int app_id = 9;
	
	public Client(){
		scaDomain = FederationCore.initCore("FederationCore.settings"); // using default values inside working directory
		FederationProperties.getInstance().setProperty("sla-disabled", "false");
		FederationProperties.getInstance().setProperty("authz-disabled", "true");
		userManagement = scaDomain.getService(IUserManagement.class, "UserManagementServiceComponent/IUserManagement");
		provManagement = scaDomain.getService(IProviderManagement.class, "ProviderManagementServiceComponent/IProviderManagement");
		appManagement = scaDomain.getService(IApplicationManagement.class, "ApplicationManagementServiceComponent/IApplicationManagement");
	}

	/**
	 * Shows the user registration to the federation.
	 */
	public static List<String> serviceUserRegistration(IUserManagement userManagement, User user){
		List<String> providerList = new ArrayList<String>();
		List<Provider> pList = createDemoProviders();
		try {
			providerList = userManagement.propagateUser(user, pList);
		}
		catch (ProviderComunicationException e) {
			logger.error("Client: impossible to comunicate with the provider ");
			e.printStackTrace();
		}
		catch (UnknownProviderErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return providerList;
	}
	
	public static void serviceProviderRegistration(IProviderManagement prov, Provider provider){
		try {
			prov.registerProvider(provider);
		}
		catch (UnknownProviderTypeException e) {
			logger.error("Client: error while testing provider registration");
			e.printStackTrace();
		}
		catch (ProviderTypeNotYetSupportedException e) {
			logger.error("Client: error while testing provider registration");
			e.printStackTrace();
		}
	}

	/**
	 * Shows the application submission feature of the federation.
	 * 
	 * @throws IOException
	 */
	boolean demoSubmitApplication(User user, Application app) throws IOException{
		boolean returnStatus = false;
		IApplicationManagement federation = scaDomain.getService(IApplicationManagement.class,
				"ApplicationManagementServiceComponent/IApplicationManagement");
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		List userSlaTemplateList = (List) em.createNamedQuery("UserSLATemplate.findAll").getResultList();	
		UserSLATemplate userSlaTemplate = null;
		Iterator ituserSLAT = userSlaTemplateList.iterator(); 
		while(ituserSLAT.hasNext()){
			UserSLATemplate userSlaTemplateTemp = ((UserSLATemplate)ituserSLAT.next());
			if(userSlaTemplateTemp.getName().equals("FEDUSERSLA")){
				userSlaTemplate = userSlaTemplateTemp;
				logger.info("find UserSLATemplate {SLATemplateId: "+userSlaTemplate.getSLATemplateId()+" SlaTid: " + userSlaTemplate.getSlatId()+"}");
			}
		}
       
		//HashMap<String, FederationProposal> orderedP = new HashMap<String, FederationProposal>();
		try {
			if (userSlaTemplate == null) throw new ApplicationOperationException();
			returnStatus = federation.submitApp(user, app, userSlaTemplate);
			/*
			System.out.println("Client: Providers suitable for app " + app.getName() + " are (in order of preference):");
			for (int i=0; i<orderedP.length; i++)
				System.out.println("\t " + "Provider:  " + orderedP[i].getName() + " with id " + orderedP[i].getProviderId() + " and uiid "+orderedP[i].getUuid());
			 */
		}
		catch (ApplicationOperationException e){
			logger.error("Client: application not submitted");
			e.printStackTrace();
		}
		catch (UnknownProviderErrorException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnStatus;
	}

	public static User createDemoUser(String username, String uuid, List<Application> appList){
		// sla section
		//UserSLA sla = new UserSLA();
		//sla.setSla("SLASOI.xml");
		//List<UserSLA> slaList = new ArrayList<UserSLA>();
		//slaList.add(sla);
		
		User user = new User();
		user.setFirstName("John");
		user.setLastName("TheRipper");
		user.setEmail("NASCOSTO");
		user.setPassword("hpcisti");
		user.setUserId(101205);
		user.setUuid("fff");
		user.setUsername(username);
		user.setApplicationList(appList);
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		/*
		em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
		*/// user.setProviderList(providers);
		// user.setUserSLAList(slaList);

		/*
		ArrayList<UGroup> uglist = new ArrayList<UGroup>();
		UGroup ug = new UGroup();
		ug.setName(user.getUsername());
		uglist.add(ug);
		ug = new UGroup();
		ug.setName("user");
		uglist.add(ug);
		user.setUGroupList(uglist);
		*/
		return user;
	}
	
	
	public static <T> void addObjectToDB(T u){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		em.getTransaction().begin();
		em.persist(u);
		em.getTransaction().commit();
		em.close();
	}

	void addProviderToDB(Provider p){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		em.close();
	}

	public static String loadOvf(String path) throws IOException{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream s = classLoader.getResourceAsStream(path);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(s));
		StringBuilder sb = new StringBuilder();
		while (br.ready()){
			sb.append(br.readLine());
		}
		br.close();
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public Collection<Provider> retrieveProvidersFromDB(){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		List<Provider> providerList = null;
		Query query = (Query) em.createNamedQuery("Provider.findAll");
		providerList = query.getResultList();

		return providerList;
	}

	
	public void demoProvidersRegistration(Collection<Provider> providers){
		if (PROVIDER_REGISTRATION) {
			logger.info("*** Client: starting registration for " + providers.size() + " providers ***");
			for (Provider p : providers){
				serviceProviderRegistration(provManagement, p);
			}
			logger.info("*** Client: providers created successfully ***");	
		}
	}



	public static void main(String[] args) throws IOException, NumberFormatException, XPathExpressionException, DOMException,
	ParserConfigurationException, SAXException, URISyntaxException, MalformedOVFException
	{

		
		//logger.setLevel(Level.WARN);

		Client c = new Client();
		System.out.println("Test client started");


		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		Collection<Provider> providers = c.retrieveProvidersFromDB();
		if (providers.size() == 0){ 
			c.populate_FedDb_With_Providers();
		}
		c.demoProvidersRegistration(providers);
		*/
		
		Provider p = Client.createProviderObject("localhost", 1, "http://192.168.57.12:8080/services/contrailNegotiation?wsdl", null);
		serviceProviderRegistration(c.provManagement, p);

		Application app = null;
		try {
			app = Client.createApplication("ovf-2vms-vep2.0.ovf",app_id, Integer.toString(app_id)+"rr");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Application> appList = new ArrayList<Application>();
		appList.add(app);

		//User user = createBarabbaUser(appList);
		User contrailuser = Client.createDemoUser("contrailuser", "caa6e102-8ff0-400f-a120-23149326a936", appList);
		User user = contrailuser;
		
		// Start demo
		logger.info("Client: starting test client");
		logger.info("-------------------------------------------------\n");

		
		if (USER_CREATION){
			logger.info("Client: starting user registration procedure");
			List<String> providerUser = c.demoUserRegistration(user);
			logger.info("Client: user registered on " + providerUser.size());
			logger.info("-------------------------------------------------\n");
		}
		
		if (APP_SUBMISSION){
			logger.info("Client: starting app-submission procedure");
			c.demoSubmitApplication(user, app);
			logger.info("Client: application submission procedure done");
			logger.info("-------------------------------------------------\n");
		}

		if (APP_START){
			logger.info("Client: starting app-start procedure");
			c.demoStartApplication(user, app);
			logger.info("Client: application start procedure done");
			logger.info("-------------------------------------------------\n");
		}
		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		

		if (APP_STOP){
			logger.info("- - - Client: starting app-stop procedure");
			demoStopApplication(user, app);
			logger.info("- - - Client: application stop procedure done");
			logger.info("-------------------------------------------------\n");
		}
		
		VirtualInfrastructureNetwork.stop();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (VirtualInfrastructureNetwork.isTerminated()){
			logger.info("- - - Client: VIN terminated");
		}
		
		scaDomain.close();
		logger.info("- - - Client: federation demo terminated");

	}

	private List<String> demoUserRegistration(User user) {
		return Client.serviceUserRegistration(userManagement, user);
	}

	public static Application createApplication(String ovfPath, int id, String uuid) throws Exception {
		ParserManager pm = null;;
		String ovf = null;
		try {
			ovf = Client.loadOvf(ovfPath);
			pm = new ParserManager(ovf, false);
		} catch (Exception e) {
			throw new Exception("Dove sai tu");
		}
		Application app = new Application();
		app.setApplicationId(id);
		app.setUuid(uuid);
		String appName = "";
		try {
			appName = pm.getApplication().getName();
		}
		catch (NullPointerException e){
			logger.error("Error on getting the name");
		}
		app.setName(appName);
		app.setApplicationOvf(ovf);
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		em.getTransaction().begin();
        em.persist(app);
        em.getTransaction().commit();
		return app;
	}
	
	

	private static void demoStopApplication(User user, Application app){
		IApplicationManagement federation = scaDomain.getService(IApplicationManagement.class,
				"ApplicationManagementServiceComponent/IApplicationManagement");
		logger.info("Client: stopping the application.");
		try
		{
			federation.stopApp(user, app);
		}
		catch (ApplicationNotFoundException e)
		{
			logger.error("Client: application not stopped");
			e.printStackTrace();
		}
		catch (ProviderComunicationException e)
		{
			logger.error("Client: impossible to communicate with the provider");
			e.printStackTrace();
		}
		catch (ApplicationOperationException e)
		{
			logger.error("Client: application not stopped");
			logger.error(e);
		}
		catch (UnknownProviderErrorException e)
		{
			logger.error("Client: application not stopped");
			e.printStackTrace();
		}

	}

	public void demoStartApplication(User user, Application app){
		try {
			appManagement.startApp(user, app);
		}
		catch (ApplicationNotFoundException e) {
			logger.error(e.getClass() + e.getMessage());
			e.printStackTrace();
		}
		catch (ProviderComunicationException e) {
			logger.error("Client: impossible to comunicate with the provider");
			e.printStackTrace();
		}
		catch (ApplicationOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (UnknownProviderErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Provider createProviderObject(String name, int id, String uriPM, String serverAttr){
		String fake_uuid = "fake" + increment;
		List<Server> ls2 = new ArrayList<Server>();

		Provider provider2 = new Provider();
		provider2.setName(name);
		provider2.setProviderId(id);
		provider2.setProviderUri(uriPM);
		provider2.setTypeId(0);
		provider2.setUserList(new ArrayList<User>());
		provider2.setUuid(fake_uuid);

		if (serverAttr != null){
			Server s2 = new Server();
			s2.setProviderId(provider2);
			s2.setAttributes(serverAttr);
			ls2.add(s2);
			provider2.setServerList(ls2);
		}

		increment++;
		return provider2;
	}

	public static List<Provider> createDemoProviders(){
		String attr = "{\"cpu_speed\":\"2494.276\",\"cpu_cores\":\"4\",\"ram_total\":\"3915\",\"ram_free\":\"1024\",\"cpu_load_one\":\"0.09\",\"cpu_load_five\":\"0.04\",\"ram_used\":\"1152\"}";
		String attr1 = "{\"cpu_speed\":\"2494.276\",\"cpu_cores\":\"4\",\"ram_total\":\"3915\",\"ram_free\":\"512\",\"cpu_load_one\":\"0.09\",\"cpu_load_five\":\"0.04\",\"ram_used\":\"1152\"}";
		String attr4 = "{\"cpu_speed\":\"2494.276\",\"cpu_cores\":\"4\",\"ram_total\":\"3915\",\"ram_free\":\"496\",\"cpu_load_one\":\"0.09\",\"cpu_load_five\":\"0.04\",\"ram_used\":\"1152\"}";

		Provider greco = createProviderObject("greco", 22, "http://146.48.82.169:4242", null);
		Provider provider1 = createProviderObject("CloudProvider2", 1, "http://sangiovese:4242", attr);
		Provider provider3 = createProviderObject("CloudProvider3", 3, "http://conero:4242", attr1);
		Provider provider4 = createProviderObject("CloudProvider4", 3, "http://192.168.57.12:8080/services/contrailNegotiation?wsdl", attr4);

		List<Provider> pr = new ArrayList<Provider>(4);
		pr.add(greco);
		pr.add(provider1);
		pr.add(provider3);
		pr.add(provider4);
		return pr;
	}
	
	public void populate_FedDb_With_Providers(){
		List<Provider> pr = createDemoProviders();
		for (Provider prov : pr)
			this.addProviderToDB(prov);

		this.demoProvidersRegistration(pr);
	}

	public static User createBarabbaUser(List<Application> appList) {
		return Client.createDemoUser("barabba", "101205", appList);
	}

	protected void demoProviderRegistration(Provider provider1) {
		Client.serviceProviderRegistration(provManagement, provider1);
	}

	public ApplicationGraph<String, ContrailEdge> getApplicationGraphPath(String ovfPath) throws ApplicationOperationException{
		ParserManager parser;
		try {
			parser = new ParserManager(ovfPath);
		}
		catch (Exception e){
			String string = "";
			for (StackTraceElement s : e.getStackTrace())
				string += s.toString()+ "\n";
			logger.error(string);
			logger.error("Application Management: application is NOT submitted");
			throw new ApplicationOperationException("Internal error while parsing the OVF document");
		}

		ApplicationGraph<String, ContrailEdge> appGraphDesc = GraphUtils.MakeGraph(-1, parser.getApplication());
		return appGraphDesc;
	}
}
