package org.ow2.contrail.federation.federationcore.test.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistrator;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistry;
import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;
import org.ow2.contrail.resource.vin.common.VinError;

public class VIN_MultipleVms_Test {

	final static String ovfResource = "ovf-2vms-vep2.0_shared.xml";
	static ApplicationGraph<String, ContrailEdge> appGraph = null;
	final static String appUuid = "FFFF";
	VirtualNetworkRegistrator vinApp1 = null;
	
	void printVinIdsAsJSON(String applicationName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(applicationName);
		JSONObject jsonVin2OVF = new JSONObject(m);
		System.out.println("*** JSON Associations between VIN VMs and Appliance names ***");
		System.out.println(jsonVin2OVF);
	}
	
	void printVinIds(String appName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(appName);
		System.out.println("*** Associations between VIN VMs and Appliance names ***");
		for (Map.Entry<String, String> e : m.entrySet()){
			System.out.println(e.getKey() + ": " + e.getValue());
		}
	}
	
	static void settingVmNumberToAllVirtualSystem(int vmNumber){
		Collection<ApplianceDescriptor> appl = appGraph.getAppliances();
		Iterator<ApplianceDescriptor> i = appl.iterator();
		while (i.hasNext()){
			i.next().setDefaultVmNumber(vmNumber);
		}
	}
	
	@BeforeClass
	public static void setUp() throws Exception{
		String appPath = System.getProperty("user.dir");
		System.out.println(appPath);

		FileReader fr = new FileReader(appPath + "/src/main/resources/" + ovfResource);

		BufferedReader br = new BufferedReader(fr);
		StringBuffer sb = new StringBuffer();
		while (br.ready()) {
			sb.append(br.readLine());
		}
		br.close();
		String ovf = sb.toString();
		ApplicationDescriptor appDesc = OVFParser.ParseOVF(ovf);
		appGraph = GraphUtils.MakeGraph(-1, appDesc);
		settingVmNumberToAllVirtualSystem(2);
	}
	
	@Test
	public void testCreation() {
		try {
			vinApp1 = new VirtualNetworkRegistrator(appGraph, appUuid);
		} catch (VinError e) {
			fail("Vin Controller not created");
			e.printStackTrace();
		}
		assertNotNull(vinApp1);
		
		List<String> s = VirtualNetworkRegistrator.getVinConnParams();
		assertTrue(s.size() == 2);
		
	}
	
	@Test
	public void testCreationNullApp() {
		System.out.println("\n [test] Creating null application");
		try {
			vinApp1 = new VirtualNetworkRegistrator(null, null);
		} catch (VinError e) {
			fail("Vin Controller not created");
			e.printStackTrace();
		}
		assertNotNull(vinApp1);
		List<String> s = VirtualNetworkRegistrator.getVinConnParams();
		assertTrue(s.size() == 2);
	}
	
	@Test
	public void testRegistry(){
		System.out.println("\n [test] Testing registry");
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(appGraph.getName());
		printVinIds(appGraph.getName());
		
		assertEquals(m.get("vm0"), "VirtualSystem1");
		assertEquals(m.get("vm1"), "VirtualSystem1");
		assertEquals(m.get("vm2"), "VirtualSystem2");
		assertEquals(m.get("vm3"), "VirtualSystem2");
	}

}
