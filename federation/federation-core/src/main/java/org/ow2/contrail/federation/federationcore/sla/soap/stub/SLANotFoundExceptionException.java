
/**
 * SLANotFoundExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

package org.ow2.contrail.federation.federationcore.sla.soap.stub;

public class SLANotFoundExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLANotFoundExceptionE faultMessage;

    
        public SLANotFoundExceptionException() {
            super("SLANotFoundExceptionException");
        }

        public SLANotFoundExceptionException(java.lang.String s) {
           super(s);
        }

        public SLANotFoundExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SLANotFoundExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLANotFoundExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLANotFoundExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    