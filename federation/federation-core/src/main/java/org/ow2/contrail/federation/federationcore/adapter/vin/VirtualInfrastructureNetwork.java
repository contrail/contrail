package org.ow2.contrail.federation.federationcore.adapter.vin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.controller.VinAPI;
import org.ow2.contrail.resource.vin.controller.VinAdministratorAPI;
import org.ow2.contrail.resource.vin.controller.VinAdministratorFactory;

/**
 * VirtualInfrastructureNetwork
 * @author Gaetano F. Anastasi (ISTI-CNR)
 * 
 * VirtualInfrastrucutureNetwork is in charge of analyzing the configuration file and creating the VIN controller.
 * 
 */
public class VirtualInfrastructureNetwork{
	private static Logger logger = Logger.getLogger(VirtualInfrastructureNetwork.class);
	private static VinAPI controller = null;
	private static Properties vnetProp;
	
	private static final String appPath = System.getProperty("user.dir");
	// private static final String propFilePath = appPath + "/src/main/resources/";
	private static final String propFilePath = "/usr/contrail/vin/";
	private static final String propFileName = "vinAdmin.cfg";
	
	private static void createVinController() throws VinError{
		vnetProp = new Properties();
		try {
			FileInputStream in = new FileInputStream(getPropFromFedSettings());
			logger.debug("[VIN Adapter] Loading settings from " + getPropFromFedSettings());
	    	vnetProp.load(in);
        }
	    catch (Exception e){
	    	logger.debug("[VIN Adapter] Error: " + e.getMessage());
	    	logger.debug("[VIN Adapter] Creating " + propFileName + " with defaults values");
	    	vnetProp = createVinProperties();
	    	save_props(vnetProp);
	    }
		VinAdministratorAPI administrator = null;
		try {
			administrator = VinAdministratorFactory.createVinAdministrator(vnetProp);
			controller = administrator.createController();
		} catch (VinError e) {
			logger.error(e.getMessage());
			throw e;
		} catch (OutOfEntriesError e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info(" Virtual infrastructure network: driver created");
	}

	private static void save_props(Properties p){
		FileOutputStream out;
		try {
			out = new FileOutputStream(propFilePath + propFileName);
			p.store(out, "VIN administration properties. Please refer to the VIN API for documentation");
			out.close();
		} catch (Exception e) {
			logger.debug("Cannot save properties " + e.getMessage());
		}
    }
	
	private static String getPropFromFedSettings() throws Exception{
		String vinSettings = FederationProperties.getInstance().getProperty("vin-configuration");
		return vinSettings;
	}
	
	
	@SuppressWarnings("unused")
	private static String getProp() throws Exception{
		URL url = VirtualInfrastructureNetwork.class.getResource("/" + propFileName);
		return url.getFile();
	}
	
	private static Properties createVinProperties(){
		Properties prop = new Properties();
		prop.setProperty("controller.ports.start", "13000");
		prop.setProperty("controller.ports.end", "20000");// determine the number of concurrent apps that can use a VIN
		prop.setProperty("controller.localaddresses.start", "10.0.0.0");
		prop.setProperty("controller.localaddresses.end", "10.255.255.255");
		prop.setProperty("controller.macaddresses.start", "00:16:3E:00:00:00");
		prop.setProperty("controller.macaddresses.end", "00:16:3E:FF:FF:FF");
		prop.setProperty("hostnetworks", "131.254.201.0/24");
		prop.setProperty("controller.verbose", "true");
		// prop.setProperty("ca.uri.cafactory", "");
		return prop;
	}
	
	// Note: this constructor must be public, otherwise tuscany complains about it
	public VirtualInfrastructureNetwork(){}
	
	/*
	private VirtualInfrastructureNetwork() throws VinError{
		if (controller == null){
			createVinController();
		}
	}
	*/
	
	public static VinAPI getController() throws VinError {
		if (controller == null){
			createVinController();
		}
		return controller;
	}
	
	public static void stop(){
		if (controller != null){
			controller.setStopped();
		}
		else {
			logger.error("Error: Vin Controller cannot be stopped");
		}
	}
	
	public static boolean isTerminated(){
		if (controller != null){
			return controller.getTerminatedNormally();
		}
		else
			return true;
	}
}