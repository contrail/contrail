package org.ow2.contrail.federation.federationcore.aem.application;

import org.ow2.contrail.federation.federationdb.jpa.entities.ApplicationSelectionCriterion;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSelectionCriterion;

public class CoreSelectionCriterion {
	private String name;
	private Double value;
	
	CoreSelectionCriterion(String name, Double value){
		this.setName(name);
		this.setValue(value);
	}
	
	CoreSelectionCriterion(UserSelectionCriterion c){
		this.setName(c.getSelectionCriterion().getName());
		this.setValue(c.getValue());
	}
	
	CoreSelectionCriterion(ApplicationSelectionCriterion c){
		this.setName(c.getSelectionCriterion().getName());
		this.setValue(c.getValue());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
