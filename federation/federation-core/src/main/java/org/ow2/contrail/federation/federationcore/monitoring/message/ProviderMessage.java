package org.ow2.contrail.federation.federationcore.monitoring.message;

import org.ow2.contrail.federation.federationcore.monitoring.HostMetrics;

public class ProviderMessage extends BaseMessage
{
	String providerID;
	HostMetrics metric;
	String value;
	
	public ProviderMessage(String providerID, HostMetrics metric, String value)
	{
		this.providerID = providerID;
		this.metric = metric;
		this.value = value;
	}
}
