package org.ow2.contrail.federation.federationcore.usermgmt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.osoa.sca.annotations.Service;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.*;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

@Service(IUserManagement.class)
public class UserManagement implements IUserManagement
{
	private static Logger logger = Logger.getLogger(UserManagement.class);

	// the second parameter is not redundant. We may want to register the user on a subset of providers.
	@Override
	public List<String> propagateUser(User user, List<Provider> providers) throws ProviderComunicationException, UnknownProviderErrorException
	{
		logger.debug("User Management: propagating the user " + user.getUsername() + " for registration on all providers");

		for (Provider p: providers){
			try {
				ProviderContainer.addProvider(p);
			} catch (UnknownProviderTypeException e) {
				logger.error("User Management: Cannot handle provider " + p.getName());
			} catch (ProviderTypeNotYetSupportedException e) {
				logger.error("User Management:  Cannot handle provider " + p.getName());
			}
		}
		
		ArrayList<String> success = new ArrayList<String>();		
		ArrayList<CloudProviderId> providerList = ProviderContainer.getProviderList();

		for (CloudProviderId cprovider : providerList) {
			ICloudProviderManager provider = ProviderContainer.getInstance(cprovider);
			if (provider.propagateUser(user)) {
				logger.debug("User Management: user propagated in: " + provider.getProviderAddress());
				success.add(provider.getProviderId());
			}
		}
		if (success.isEmpty()) {
			logger.error("User Management: propagation procedure failed. User has not been registered on any provider");
		}
		else {
			String successProviders = new String();
			for (String s: success)
				successProviders +="\'" + s + "\' \t";
			logger.debug("User Management: propagation done on " + successProviders);
			logger.info("User Management: propagation procedure successfully done on " + success.size() + " over " + providerList.size()
					+ " provider ");
		}
		return success;
	}
}
