package org.ow2.contrail.federation.federationcore.test.junit;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.ow2.contrail.federation.federationcore.*;
import org.ow2.contrail.federation.federationcore.aem.application.ApplicationManagement;
import org.ow2.contrail.federation.federationcore.aem.application.IApplicationManagement;
import org.ow2.contrail.federation.federationcore.exception.ApplicationNotFoundException;
import org.ow2.contrail.federation.federationcore.exception.ApplicationOperationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.providermgmt.IProviderManagement;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.test.Client;
import org.ow2.contrail.federation.federationcore.usermgmt.IUserManagement;
import org.ow2.contrail.federation.federationcore.usermgmt.UserManagement;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.junit.*;


public class FederationCoreTest {
	
	private static SCADomain scaDomain;
	static User barabbauser;
	static User contrailuser;
	static List<Application> appList; 
	static IApplicationManagement federationAemService = null;
	static IProviderManagement provService = null;
	static IUserManagement userService = null;
	
	static int appId = 1;
	
	@BeforeClass
	public static void setUp() throws Exception{
		scaDomain = FederationCore.initCore("FederationCore.settings"); 
		
		FederationProperties.getInstance().setProperty("sla-disabled", "true");
		FederationProperties.getInstance().setProperty("authz-enabled", "true");
		
		Application app = Client.createApplication("./src/main/resources/ovf-2vms-vep2.0.xml", appId, Integer.toString(appId));
		Application app1 = Client.createApplication("./src/main/resources/ovf-2vms-vep2.0.xml", appId++, Integer.toString(appId++));
		appList = new ArrayList<Application>();
		appList.add(app);
		appList.add(app1);
		barabbauser = Client.createBarabbaUser(appList);
		contrailuser = Client.createDemoUser("contrailuser", "caa6e102-8ff0-400f-a120-23149326a936", appList);
		
		System.out.println("here");
		federationAemService = scaDomain.getService(IApplicationManagement.class, "ApplicationManagementServiceComponent/IApplicationManagement");
		Provider p = Client.createProviderObject("localhost", 1, "http://sangiovese:4242", null);
		provService = scaDomain.getService(IProviderManagement.class, "ProviderManagementServiceComponent/IProviderManagement");
		Client.serviceProviderRegistration(provService, p);
		
	}

	boolean submittingApp(IApplicationManagement federation, int index){
		boolean returnStatus = false;
		Application app = appList.get(index);
		UserSLATemplate slaTemplate= new UserSLATemplate();
		//HashMap<String, FederationProposal> orderedP = new HashMap<String, FederationProposal>();
		try {
			returnStatus = federation.submitApp(contrailuser, app, slaTemplate);
		}
		catch (ApplicationOperationException e){
			System.out.println("Error on app: " + e.getMessage());
			e.printStackTrace();
		}
		catch (UnknownProviderErrorException e){
			e.printStackTrace();
		}
		return returnStatus;
	}
	
	@Test
	public void testUserRegistration(){
		userService = scaDomain.getService(IUserManagement.class, "UserManagementServiceComponent/IUserManagement");
		Client.serviceUserRegistration(userService, barabbauser);
		Client.serviceUserRegistration(userService, contrailuser);
	}
	
	@Test
	public void testSubmitFirstApplication() throws IOException{ 
		System.out.println("\n Testing Submit First App");
		submittingApp(federationAemService, 0);
	}
	

	@Test
	public void testStartingFirstApplication() throws IOException{
		System.out.println("\n TestingStartingApp 0");
		//FederationProposal chosenProposal = new FederationProposal(null, null, null, null);
		Application app = appList.get(0);
		startingApp(federationAemService, contrailuser, app);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	@Test
	public void testStartingApplicationDifferentUser() throws IOException{
		System.out.println("\n TestingStartDifferentUser");
		//FederationProposal chosenProposal = new FederationProposal(null, null, null, null);
		Application app = appList.get(0);
		try {
			federationAemService.startApp(barabbauser, app);
		}
		catch (Exception e){
			assertEquals(e.getMessage(), "Impossible to find some application submitted related to the user " + barabbauser.getUsername());
		}
	}
	

	@Test
	public void testStopApplication() throws IOException{
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n Testing Stop App 0 ");
		Application app = appList.get(0);
		try {
			federationAemService.stopApp(contrailuser, app);
		}
		catch (Exception e){
			// assertEquals(e.getMessage(), "Impossible to find some application started related to the user " + contrailuser.getUsername());
		}
		
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	private void startingApp(IApplicationManagement federation, User user, Application app){
		try {
			federation.startApp(user, app);
		}
		catch (ApplicationNotFoundException e) {
			e.printStackTrace();
		}
		catch (ProviderComunicationException e) {
			System.out.println("Client: impossible to comunicate with the provider");
			e.printStackTrace();
		}
		catch (ApplicationOperationException e) {
			e.printStackTrace();
		}
		catch (UnknownProviderErrorException e) {
			e.printStackTrace();
		}
	}
}
