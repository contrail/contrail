package org.ow2.contrail.federation.federationcore.authz.oauth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.CCFlowClient;
import org.ow2.contrail.common.oauth.client.CertRetriever;
import org.ow2.contrail.common.oauth.client.KeyAndCertificate;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;


public class OauthFedClient {

	private static Logger logger = Logger.getLogger(OauthFedClient.class);
	private static String loggerTag = "[Oauth Fed Client] ";
	
	private static final String CONF_FILE = "/etc/contrail/federation-api/federation-api.cfg";
	private static final String TEST_CONF_FILE = "oauth-java-client-demo/oauth-java-client-demo.properties";
	
	private static String getOauthPropFromFedSettings() throws Exception{
		String oauthSettings = FederationProperties.getInstance().getProperty("oauth-configuration");
		if (oauthSettings.equals(""))
			oauthSettings = CONF_FILE;
		return oauthSettings;
	}
	/**
	 * This method queries the OAuth server for receiving the user certificate from uuid.
	 * @param resource_owner_uuid
	 * @return
	 * @throws Exception
	 */
	public static X509Certificate getCertificateFromUuid(String resource_owner_uuid) throws Exception {
		String resourceOwner = null;
		if (resource_owner_uuid == null || resource_owner_uuid.equals(""))
			resourceOwner = "caa6e102-8ff0-400f-a120-23149326a936";// contrail-user
		else
			resourceOwner = resource_owner_uuid;

		try {
			Conf.getInstance().load(new File(getOauthPropFromFedSettings()));
			logger.debug(loggerTag + "Loading settings from " + getOauthPropFromFedSettings());
        }
	    catch (Exception e){
	    	File f = null;
	    	try {
	    		URL url = OauthFedClient.class.getResource("/" + TEST_CONF_FILE);
	    		f = new File(url.toURI());
	    	}
	    	catch (NullPointerException n){
		    	f = new File("src/main/resources/" + TEST_CONF_FILE);
		    }
	    	finally{
	    		Conf.getInstance().load(f);
	    		logger.error(loggerTag + "Error " + e.getLocalizedMessage());
	    		logger.debug(loggerTag + "Loading settings from " + TEST_CONF_FILE);
	    	}
	    	
	    }
		
		String uname = null;
		try {
			EntityManager em = PersistenceUtils.getInstance().getEntityManager();
			User user = new UserDAO(em).findByUuid(resourceOwner);

			if (user != null) {
				uname = user.getUsername();
			}
			PersistenceUtils.getInstance().closeEntityManager(em);
		}
		catch (NullPointerException e){
			uname = resourceOwner;
		}
        
		X509Certificate cert = null;
		logger.info(String.format(
				"Requesting OAuth access token from AS %s on behalf of user %s.",
				Conf.getInstance().getASAccessTokenUri(), uname));
		AccessToken accessToken = null;
		try {
			accessToken = getToken(resourceOwner, null);
			logger.info("Received access token: " + accessToken.getValue());
		}
		catch (Exception e) {
			String message = "Failed to obtain an access token ";
			String st = "";
			for (StackTraceElement elem : e.getStackTrace())
				st += elem.toString() +"\n";
			logger.error(message + st);
			throw new Exception(message);
		}

		String accessTok = accessToken.getValue();
		logger.info(loggerTag + String.format(
				"Requesting delegated user certificate from CA %s using access token %s.",
				Conf.getInstance().getCAUserCertUri(), accessTok));
		try {
			cert = getCert(accessTok);
			logger.info(loggerTag + "Received user certificate!");
			CertificatesCache.getInstance().put(resource_owner_uuid, cert);
			// saveCertificateToFile(cert, file); 
		}
		catch (Exception e) {
			throw new Exception("Failed to obtain user certificate: " + e.getMessage());
		}
		return cert;
	}

	protected static void saveCert(X509Certificate cert, String file){
		FileOutputStream os;
		try {
			os = new FileOutputStream(file);
			os.write("-----BEGIN CERTIFICATE-----\n".getBytes("US-ASCII"));  
			os.write(Base64.encodeBase64(cert.getEncoded(), true));  
			os.write("-----END CERTIFICATE-----\n".getBytes("US-ASCII"));  
			os.close(); 
		} 
		catch (Exception e) {
			e.printStackTrace();
		}  
	}
	
	
	public static String serializeCert(X509Certificate cert) {
		String s = null;
		try {
			s = new String(Base64.encodeBase64(cert.getEncoded()));
		} catch (CertificateEncodingException e) {
			logger.error(loggerTag + "Some error");
		}
		return s;
	}
	
	
	
	public static X509Certificate getCertificateFromFile(String fileName){
		X509Certificate cert = null;
		try {
			InputStream inStream = Class.class.getResourceAsStream("/" + fileName);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			cert = (X509Certificate)cf.generateCertificate(inStream);
			inStream.close();
		} catch (Exception e1) {
			logger.error("Exception getting certificate from file");
		}
		return cert;
	}
	
	protected static AccessToken getToken(String resourceOwner, String scope) {
		URI tokenEndpointUri = null;
		try {
			tokenEndpointUri = new URI(Conf.getInstance().getASAccessTokenUri());
		} catch (URISyntaxException e) {
			logger.error("Syntax URI Exception");
		}
		CCFlowClient ccFlowClient = new CCFlowClient(tokenEndpointUri,
				Conf.getInstance().getClientKeystoreFile(), 
				Conf.getInstance().getClientKeystorePass(),
				Conf.getInstance().getClientTruststoreFile(), 
				Conf.getInstance().getClientTruststorePass());
		ccFlowClient.setClientId(Conf.getInstance().getClientId());
		ccFlowClient.setClientSecret(Conf.getInstance().getClientSecret());

		AccessToken at = null;
		try {
			at = ccFlowClient.requestAccessToken(resourceOwner, scope);
		} catch (Exception e) {
			logger.error("Exception requesting acces token");
		}
		return at;
	}

	protected static X509Certificate getCert(String accessToken) throws Exception {
		URI userCertEndpointUri = new URI(Conf.getInstance().getCAUserCertUri());
		CertRetriever certRetriever = new CertRetriever(userCertEndpointUri,
				Conf.getInstance().getClientKeystoreFile(), Conf.getInstance().getClientKeystorePass(),
				Conf.getInstance().getClientTruststoreFile(), Conf.getInstance().getClientTruststorePass());

		KeyAndCertificate keyAndCertificate = certRetriever.retrieveCert(accessToken);
        return keyAndCertificate.getCertificate();
	}
	
	public static void main(String[] args) throws Exception {
		OauthFedClient.getCertificateFromUuid(null);
	}
}
