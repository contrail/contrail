package org.ow2.contrail.federation.federationcore.authz;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.ow2.contrail.authorization.cnr.pep.PEP;
import org.ow2.contrail.authorization.cnr.pep.PEP_callout;
import org.ow2.contrail.authorization.cnr.pep.PepCallback;
import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;
import org.ow2.contrail.authorization.cnr.utils.pep.PepRequestAttribute;
import org.ow2.contrail.federation.federationcore.aem.application.AemCallback;
import org.ow2.contrail.federation.federationcore.authz.oauth.OauthFedClient;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;

/**
 * 
 * @author Anastasi, Carlini
 *
 */
public class AuthzHub {
	private static Logger logger = Logger.getLogger(AuthzHub.class);
	private static String loggerTag = "[core.authzHub] ";
	
	private PEP_callout pep_callout;
	private List<PepRequestAttribute> request;
	private boolean authorization = false;
	// private PepCallback accessRevokeEndCallBack;
	private String resourceId = null;
	private static final String TEST_CERTIFICATE = "oauth-java-client-demo/cert.cer"; //use relative path as it will be loaded by classloader
	private PEP pep = null;
	
	// keys are appIds
	private static HashMap<String, String> authorizations = new HashMap<String, String>();
	
	private boolean isEnabled(){
		return !Boolean.parseBoolean(FederationProperties.getInstance().getProperty("authz-disabled"));
	}
	
	public AuthzHub(){
		if (isEnabled()){
			pep = FederationPEP.getPEPInstance();
		}
		else {
			logger.error(loggerTag + "Ucon-authz is disabled");
			authorization = true;
		}
		return;
	}
	
	@Deprecated
	// It seems to not work inside servlet containers
	/**
	 * This method allows for waiting msec milliseconds in the case of immediate revocation
	 * of the startaccess. In this way the programmer can avoid to start the application 
	 * and then using the callback to stop it. 
	 * @param p the callback that handles the revocation
	 * @param msec the milliseconds to wait
	 * @return
	 */
	public boolean myWait(AemCallback p, int msec) {
		logger.info(loggerTag + "I'm waiting in the case of immediate revoke");
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			if (p.isRevoked()){
				p.setHandled();
				authorizations.remove(p.getAppFedId());
				return false;
			}
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return true;
	}
		
	public boolean requestAuthz(String UUID, String appId) {
		logger.info(loggerTag + "Asking for authorization for the user ");
		if (!isEnabled()) {
			authorization = true;
			return authorization;
		}
		
		try  {
			pep_callout = new PEP_callout(pep);
		} 
		catch (AxisFault e) {
			logger.error("Unable to setup PEP_callout");
			e.printStackTrace();
		}
		
		// creating the request
		X509Certificate cert = null;
		try {
			cert = OauthFedClient.getCertificateFromUuid(UUID);
		} catch (Exception e2) {
			logger.error("Unable to retrieve the certificate from OAuth");
			cert = OauthFedClient.getCertificateFromFile(TEST_CERTIFICATE);
			logger.error("Using test certificate!");
			// e2.printStackTrace();
		}
		
		try {
			request = pep_callout.getAttributesFromCertificate(cert);
			PepRequestAttribute res = new PepRequestAttribute(RequestsFactory.RESOURCE_ATTR_ID, RequestsFactory.type, 
					appId, RequestsFactory.issuer, Category.RESOURCE);
			request.add(res);
			PepRequestAttribute act = new PepRequestAttribute(RequestsFactory.ACTION_ATTR_ID, RequestsFactory.type, 
					RequestsFactory.action_value, RequestsFactory.issuer, Category.ACTION);
			request.add(act);
			logger.debug(loggerTag + "Pep_callout getAttribute From Certificate success");
			
		} catch (Exception e2) {
			logger.error("Building fake request");
			request = RequestsFactory.getFakeRequest();
			e2.printStackTrace();
		}
		
		// get the decision
		try {
			logger.debug(loggerTag + "Pep_callout calling tryaccess...");
			authorization = pep_callout.tryaccess(request);
			logger.debug(loggerTag + "Pep_callout authorization is: " + authorization);
			if (authorization == true) {
				pep_callout.mapId(appId);
				authorizations.put(appId, UUID);
			}
		} 
		catch (AxisFault e1) 
		{
			logger.error(loggerTag + "Unable to retrieve the state of authorization");
			authorization = false;
			e1.printStackTrace();
		}
		
		logger.debug(loggerTag + "User is authorized");
		return authorization;
	}
	
	/**
	 * Invokes the startaccess action for a new PEP callout called with the appId as key
	 * @param appId
	 * @return
	 */
	public boolean startAccess(String uuid, String appId, PepCallback call){
		if (!isEnabled()) return true;
		
		if (!checkAuthz(uuid, appId)){
			logger.error(loggerTag + "... no authorization found to perform startaccess");
			return false;
		}
		
		logger.info(loggerTag + "Trying startaccess for authorization");
		try  {
			pep_callout = new PEP_callout(pep, appId);
		} 
		catch (AxisFault e) {
			logger.error(loggerTag + "Unable to setup PEP_callout");
			e.printStackTrace();
		}
		catch (NullPointerException e){
			logger.error(loggerTag + "Null pointer");
			e.printStackTrace();
		}	
		
		// starting the access
		// the PDP remotely check the mutable attribute (may take several seconds)
		// try { Thread.sleep(1000);} 
		// catch (InterruptedException e) {e.printStackTrace();}
		
		try {
			String fedapi = getFedApiUrl();
			logger.info(loggerTag + " I will contact Fed-API on  URL " + getFedApiUrl());
			String complete = "/federation-api/users/caa6e102-8ff0-400f-a120-23149326a936/applications/6d2ebd66-a430-4aef-b2d0-ad0fb117df83/stop/";
			URL url = null;
			try {
				url = new URL(fedapi + "/federation-api/federation/manager/users/" + uuid + "/applications/" + appId + "/revoke");
			} catch (MalformedURLException e) {
				logger.error(loggerTag + "Malformed URL");
				e.printStackTrace();
			}
			pep_callout.startaccess(url);
		} 
		catch (AxisFault e){
			logger.error(loggerTag + "Unable to start the authorization");
			e.printStackTrace();
		}	
		catch (NullPointerException e){
			logger.error(loggerTag + "Null pointer");
			e.printStackTrace();
		}		
		return true;
	}
	
	private static String getPublicAddress(){
		InetAddress ip = null;
		String ipS = null;
		try {
			ip = InetAddress.getLocalHost();
			ipS = ip.getHostAddress();
		} catch (UnknownHostException e) {
			ipS = "146.48.81.249"; // temporary fed node
		}
		return ipS;
	}
	
	private String getFedApiUrl(){
		return "http://" + getPublicAddress() + ":" + FederationProperties.getInstance().getProperty("fedapi-port");
	}
	
	public boolean checkAuthz(String uuid, String appId) {
		logger.info(loggerTag + "Checking for previous authorization...");
		
		if (!isEnabled()) {
			logger.info(loggerTag + "...default!");
			return true;
		}
		
		String saved_uuid = authorizations.get(appId);
		if (saved_uuid.equals(uuid)){
			logger.info(loggerTag + "...ok!");
			return true;
		}
		else {
			logger.info(loggerTag + "...no!");
			return false;
		}
	}
	
	/**
	 * Invokes the stopAccess for a new PEP callout called with appId as key
	 * @param appId
	 */
	public boolean stopAccess(String uuid, String appId) {
		boolean success = true;
		if (!isEnabled()) {
			return success;
		}
		
		if (!checkAuthz(uuid, appId)){
			logger.error(loggerTag + "... no authorization found to perform endaccess");
			return false;
		}
		else
			logger.error(loggerTag + "... ok");
		
		logger.info(loggerTag + " Trying to perform endaccess");
		try  {
			pep_callout = new PEP_callout(pep, appId);
		} 
		catch (AxisFault e) {
			logger.error("Unable to setup PEP_callout");
			e.printStackTrace();
		}
		
		logger.info(loggerTag + " Releasing PEP_callout");
		
		// kill the access
		try {
			pep_callout.endaccess();
			authorizations.remove(appId);
		} 
		catch (AxisFault e) {
			logger.error(loggerTag + "Unable to release PEP_callout");
			e.printStackTrace();
			success = false;
		}
		
		return success;
	}
	
}
