package org.ow2.contrail.federation.federationcore.adapter.vin;

import javax.persistence.*;


@Entity
@NamedQueries({
    @NamedQuery(name = "VinNet.findAll", query = "SELECT t FROM VinNet t"),
    @NamedQuery(name = "VinNet.findIdByApp", query = "SELECT t.vinNetId FROM VinNet t WHERE t.appId = :applicationID"),
    @NamedQuery(name = "VinNet.findIdByEdge", query = "SELECT t.vinNetId FROM VinNet t WHERE t.appId = :applicationID AND t.appNetId = :edgeID")})
public class VinNet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String appId;
	private String appNetId;
	private String vinNetId;
	
	public Long getId() {
		return id;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppNetId() {
		return appNetId;
	}
	public void setAppNetId(String appNId) {
		this.appNetId = appNId;
	}
	public String getVinNetId() {
		return vinNetId;
	}
	public void setVinNetId(String vinVmId) {
		this.vinNetId = vinVmId;
	}
	public String toString(){
		return this.getAppId() + " | " + this.getAppNetId() + " | " + this.getVinNetId();
	}
}
