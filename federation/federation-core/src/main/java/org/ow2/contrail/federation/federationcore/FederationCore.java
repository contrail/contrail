package org.ow2.contrail.federation.federationcore;

import org.apache.log4j.Logger;
import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

public class FederationCore {
	
	private static Logger logger = Logger.getLogger(FederationCore.class);
	public static String COMPOSITE_NAME="Federation-core.composite";
	private static final String DEFAULT_PU = "fedPU";
		
	//static SCADomain sca = null;
	
	public static SCADomain initCore(String configuration_path){
		logger.debug("Entering initCore");
		
		// Tuscany start-up
		SCADomain scaDomain = SCADomain.newInstance(COMPOSITE_NAME);	
		//sca = scaDomain;
		
		// Initialize configuration
		FederationProperties.createFederationProperties(configuration_path);
		
		logger.info("Using " + DEFAULT_PU + " persistence unit for federation-db");
		try {
			PersistenceUtils.createInstance(DEFAULT_PU); // Initializing database persistence
		}
		catch (java.lang.RuntimeException e){
			logger.info("Persistence unit already defined");
		}
		
		logger.debug("Exiting initCore");
		return scaDomain;
	}
	
}
