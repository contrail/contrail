package org.ow2.contrail.federation.federationcore.monitoring.message;

import org.ow2.contrail.federation.federationcore.monitoring.VMMetrics;

public class ApplicationMessage extends BaseMessage
{
	String appID;
	String vmID;
	VMMetrics metric;
	String value;
	
	public ApplicationMessage(String appID, String vmID, VMMetrics metric, String value)
	{
		this.appID = appID;
		this.vmID = vmID;
		this.metric = metric;
		this.value = value;
	}
}
