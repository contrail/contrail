package org.ow2.contrail.federation.federationcore.aem.application;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents a group of appliances with the same startup order.  
 */
public class ApplianceStartupDescriptor
{
	private Collection<ApplianceStartupDescriptor> startupDescriptors;
	
	private int delay;
	
	/**
	 * Creates a new instance of ApplianceStartupDescriptor type.
	 */
	public ApplianceStartupDescriptor(int delay)
	{
		startupDescriptors = new ArrayList<ApplianceStartupDescriptor>();
		this.delay = delay;
	}
	
	/**
	 * Gets the group of related appliances.
	 */
	public Collection<ApplianceStartupDescriptor> getStartupDescriptors() 
	{
		return startupDescriptors;
	}

	/**
	 * Gets the time delay with respect to the next ApplianceStartupDescriptor.
	 */
	public int getDelay() 
	{
		return delay;
	}

}
