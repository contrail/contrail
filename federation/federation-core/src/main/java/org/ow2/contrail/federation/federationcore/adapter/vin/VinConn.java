package org.ow2.contrail.federation.federationcore.adapter.vin;

import javax.persistence.*;

@Entity
@NamedQueries({
	@NamedQuery(name = "VinConn.findAll", query = "SELECT t FROM VinConn t"),
	@NamedQuery(name = "VinConn.findVinConnId", query = "SELECT t.vinConnId FROM VinConn t WHERE t.vinNetId = :vinNetworkID AND t.vinVmId = :vinNodeID"),
    @NamedQuery(name = "VinConn.findVMsByNet", query = "SELECT t.vinVmId FROM VinConn t WHERE t.vinNetId = :vinNetworkID")})
public class VinConn {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String vinConnId;
	private String vinNetId;
	private String vinVmId;
	
	public String getVinNetId() {
		return vinNetId;
	}
	public void setVinNetId(String vinVmId) {
		this.vinNetId = vinVmId;
	}
	public String getVinVmId() {
		return vinVmId;
	}
	public void setVinVmId(String vinVmId) {
		this.vinVmId = vinVmId;
	}
	public String getVinConnId() {
		return vinConnId;
	}
	public void setVinConnId(String vinConnId) {
		this.vinConnId = vinConnId;
	}

	
	public String toString(){
		String tmp = this.vinConnId + " | " + this.getVinNetId() + " | ";
		tmp += this.getVinVmId();
		return tmp;
	}
}