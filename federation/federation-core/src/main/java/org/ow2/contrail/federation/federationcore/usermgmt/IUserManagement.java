package org.ow2.contrail.federation.federationcore.usermgmt;

import java.util.List;

import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public interface IUserManagement
{
	public List<String> propagateUser(User user, List<Provider> providers) throws ProviderComunicationException, UnknownProviderErrorException;
}
