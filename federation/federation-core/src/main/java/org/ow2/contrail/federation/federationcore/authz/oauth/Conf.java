package org.ow2.contrail.federation.federationcore.authz.oauth;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class Conf {
    private static Conf instance = new Conf();
    private static Logger log = Logger.getLogger(Conf.class);
    private Properties props;

    private static String propPrefix = "oauthClient."; 
    public static Conf getInstance() {
        return instance;
    }

    private Conf() {
    }

    public void load(File confFile) throws IOException {
        log.debug(String.format("Loading configuration from file '%s'.", confFile.getAbsolutePath()));
        this.props = new Properties();
        try {
            this.props.load(new FileInputStream(confFile));
            log.debug("Configuration loaded successfully.");
        }
        catch (IOException e) {
            throw new IOException(String.format("Failed to read configuration file '%s'.", confFile.getAbsolutePath()));
        }
    }

    public String getClientId() {
        String prop = props.getProperty(propPrefix + "id");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientSecret() {
        String prop = props.getProperty(propPrefix + "secret");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientOauth2CallbackUri() {
        String prop = props.getProperty(propPrefix + "oauth2callbackUri");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientKeystoreFile() {
        String prop = props.getProperty(propPrefix + "keystore.file");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientKeystorePass() {
        String prop =  props.getProperty(propPrefix + "keystore.pass");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientTruststoreFile() {
        String prop =  props.getProperty(propPrefix + "truststore.file");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    public String getClientTruststorePass() {
        String prop = props.getProperty(propPrefix + "truststore.pass");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    private String getOAuthASTokenEndpoint() {
        String prop = props.getProperty("oauthClient.tokenEndpoint");
        if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
    }

    private URI getAddressOAuthAS() throws URISyntaxException {
        String address = props.getProperty("address.oauth-as");
        if (!address.endsWith("/")) {
            address += "/";
        }
        return new URI(address);
    }
    
    public String getASAuthorizationUri() {
        // return props.getProperty("authzserver.authorizationEndpointUri");
    	URI uri = null;
		try {
			uri = this.getAddressOAuthAS();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return uri.toString();
    }

    public String getASAccessTokenUri() {
        // return props.getProperty("authzserver.accessTokenEndpointUri");
    	String prop = this.getOAuthASTokenEndpoint();
    	if (prop.equals("") | prop == null)
        	return null;
        else 
        	return prop;
        
    }

    /*
    public String getASAccessTokenValidationUri() {
        return props.getProperty("authzserver.accessTokenValidationEndpointUri");
    }
    */

    public String getCAUserCertUri() {
        return props.getProperty("caserver.userCertUri");
    }

    public String getScope() {
        return props.getProperty("scope");
    }
}
