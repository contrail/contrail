package org.ow2.contrail.federation.federationcore.test.junit;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.federationcore.authz.oauth.CertificatesCache;
import org.ow2.contrail.federation.federationcore.authz.oauth.OauthFedClient;
import java.security.cert.X509Certificate;

public class OauthClientTest {

	private static final String TEST_CERTIFICATE = "oauth-java-client-demo/cert.cer";
	X509Certificate cert;
	String uuid = "444";
	String contrailuserUuid = "caa6e102-8ff0-400f-a120-23149326a936";
	
	@Before
	public void setUp() throws Exception {
		CertificatesCache.getInstance();
		cert = OauthFedClient.getCertificateFromFile(TEST_CERTIFICATE);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPut() {
		
		CertificatesCache.getInstance().put(uuid, cert);
	}

	@Test
	public void testGetCertificate() {
		X509Certificate c = CertificatesCache.getInstance().getCertificate(uuid);
		assertTrue(c.equals(cert));
	}

	@Test
	public void testGetCertificateFromUUid(){
		try {
			OauthFedClient.getCertificateFromUuid(contrailuserUuid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetEncodedCertificate() {
		String c = CertificatesCache.getInstance().getSerializedCertificate(uuid);
		String s_cert = OauthFedClient.serializeCert(cert);
		assertTrue(c.equals(s_cert));
	}
	
	@Test
	public void testNullGetEncodedCertificate() {
		String c = CertificatesCache.getInstance().getSerializedCertificate("4489");
		assertNull(c);
	}

}
