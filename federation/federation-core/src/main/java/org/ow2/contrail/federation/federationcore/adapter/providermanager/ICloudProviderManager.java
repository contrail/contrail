package org.ow2.contrail.federation.federationcore.adapter.providermanager;

import java.util.Collection;

import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager.ProviderType;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public interface ICloudProviderManager
{
	/**
	 * Gets the provider Id.
	 * 
	 * @return A <code>String</code>.
	 */
	public String getProviderId();

	/**
	 * Sets the provider Id.
	 * 
	 * @param ID The provider Id.
	 */
	public void setProviderID(CloudProviderId ID);
	
	/**
	 * Gets the provider name.
	 * 
	 * @return A <code>String</code>
	 */
	public String getProviderName();

	/**
	 * Sets the provider name.
	 * 
	 * @param name The provider name.
	 */
	public void setProviderName(String name);

	/**
	 * Gets the provider type.
	 * 
	 * @return <code>ProviderType</code>
	 */
	public ProviderType getType();

	// FIXME: not compliant with our architecture.
	public String getProviderAddress();

	// FIXME: not compliant with our architecture.
	public void setProviderAddress(String vepAddress);

	/**
	 * Creates an account for each cloud provider registered in Contrail.
	 * @throws UnknownProviderErrorException 
	 */
	public boolean propagateUser(User user) throws ProviderComunicationException, UnknownProviderErrorException;

	/**
	 * Deploys the application (a set of appliances) on the provider.
	 * @param ovfName 
	 * @param slaName 
	 * @throws UnknownProviderErrorException 
	 */
	public String submit(Collection<ApplianceDescriptor> appDesc, User user, String ovf, String ovfName, String ceeId) throws ProviderComunicationException, UnknownProviderErrorException;

	/**
	 * Starts an application (a set of appliances) on the provider.
	 * @throws UnknownProviderErrorException 
	 */
	public Collection<String> start(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException, UnknownProviderErrorException;

	/**
	 * Stops an application (a set of appliances) on the provider.
	 * @throws UnknownProviderErrorException 
	 */
	public boolean stop(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException, UnknownProviderErrorException;

	/**
	 * Sends to the provider a notification for the new connection of the federation.
	 */
	public String notifyFederationConnection();

	/**
	 * Kill an application (a set of appliances) on the provider.
	 * @throws UnknownProviderErrorException 
	 */
	boolean internalKill(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException, UnknownProviderErrorException;
}
