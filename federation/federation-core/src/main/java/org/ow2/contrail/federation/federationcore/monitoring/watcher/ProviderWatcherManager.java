package org.ow2.contrail.federation.federationcore.monitoring.watcher;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;

public class ProviderWatcherManager
{
	// private ExecutorService exec;
	// private int numberThreadActive;
	private ConcurrentLinkedQueue<BaseMessage> monitoringQueue;

	/**
	 * Defines the providerwatcher manager component which instantiate a thread
	 * pool to handle the queue message
	 */
	private ProviderWatcherManager() 
	{
		// exec = Executors.newCachedThreadPool();
		monitoringQueue = new ConcurrentLinkedQueue<BaseMessage>();
		// numberThreadActive = 0;
	}

	private static ProviderWatcherManager instance;
	
	public static ProviderWatcherManager getInstance()
	{
		if (instance == null)
		{
			instance = new ProviderWatcherManager();
		}
		
		return instance;
	}
	
	/**
	 * Gets the monitoring queue of the provider.
	 */
	public ConcurrentLinkedQueue<BaseMessage> getMonitoringQueue()
	{
		return monitoringQueue;
	}
	
	/**
	 * Queue a monitoring event on the provider queue.
	 */
	public void addMonitoringMessage(BaseMessage message)
	{
		monitoringQueue.add(message);
	}
}
