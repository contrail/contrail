package org.ow2.contrail.federation.federationcore.monitoring.rest;

import org.ow2.contrail.federation.federationcore.monitoring.BaseMonitoringThread;
import org.ow2.contrail.federation.federationcore.monitoring.HostMetrics;
import org.ow2.contrail.federation.federationcore.monitoring.VMMetrics;
import org.ow2.contrail.federation.federationcore.monitoring.message.ApplicationMessage;
import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;
import org.ow2.contrail.federation.federationcore.monitoring.message.ProviderMessage;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

import com.sun.jersey.api.client.WebResource;

public class RESTMetricFetcher extends BaseMonitoringThread
{
	private long interval;
	private WebResource service;
	private String resturl;
	private ProviderWatcherManager manager;
	private BaseMessage.MessageType messagetype;

	public RESTMetricFetcher(String resturl, long interval, WebResource service, BaseMessage.MessageType mtype)
	{
		execution = true;
		this.resturl = resturl;
		this.service = service;
		this.interval = interval;
		this.manager = ProviderWatcherManager.getInstance();
		this.messagetype = mtype;
	}

	@Override
	public void run()
	{
		while (execution)
		{
			try
			{
				Thread.sleep(interval);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String response = service.path(resturl).get(String.class);
			BaseMessage message = null;

			if (messagetype == BaseMessage.MessageType.APPLICATION)
			{
				// TODO: String appID, String vmID, VMMetrics metric, String value
				message = new ApplicationMessage("", "", VMMetrics.cpu, response);
			}
			else
				if (messagetype == BaseMessage.MessageType.PROVIDER)
				{
					// TODO: String providerID, HostMetrics metric, String value
					message = new ProviderMessage("", HostMetrics.common, response);
				}

			manager.addMonitoringMessage(message);
		}
	}
}
