package org.ow2.contrail.federation.federationcore.test;

import org.ow2.contrail.federation.federationcore.utils.FederationProperties;

public class PropertiesParsing {

	public static void main(String[] args) {
		
		String veps = FederationProperties.getInstance().getProperty("veps");
		
		for(String vep : veps.split(",")){
			
			System.out.println(vep.trim());
			
		}
	}

}
