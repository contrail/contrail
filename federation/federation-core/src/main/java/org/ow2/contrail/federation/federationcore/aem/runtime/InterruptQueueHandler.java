package org.ow2.contrail.federation.federationcore.aem.runtime;

import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;

public class InterruptQueueHandler implements Runnable 
{
	@Override
	public void run() 
	{
		// check the interrupt queue
		while (true)
		{
			if (!RuntimeManagement.interruptQueue.isEmpty()) 
			{
				BaseMessage message = RuntimeManagement.interruptQueue.poll();
				
				// Check the type of message
				// Check the body of the message
				
				//FIXME: if body = something then doSomething()
			} 
			else
			{
				try 
				{	
					Thread.sleep(5000);
				} 
				catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
