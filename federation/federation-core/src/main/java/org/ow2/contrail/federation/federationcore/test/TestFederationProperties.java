package org.ow2.contrail.federation.federationcore.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;

import org.ow2.contrail.federation.federationcore.utils.FederationProperties;

public class TestFederationProperties 
{
	/**
	 * @param args
	 * @throws IOException x
	 * @throws FileNotFoundException 
	 */
	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		Client c = new Client();
		
		FederationProperties properties = FederationProperties.getInstance();
		
		//properties.load("properties");
		
		String foo = properties.getProperty("vin-configuration");
		String boo = properties.getProperty("boo");
		
		System.out.println(URLEncoder.encode(foo, "UTF-8"));
		System.out.println(boo);
		
		File f = new File(foo);
		FileInputStream in = new FileInputStream(f);
		
		properties.setProperty("foo", "newfoo");
		properties.setProperty("boo", "newboo");
		
	}
}
