package org.ow2.contrail.federation.federationcore.adapter.providermanager;


import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERGROUPS;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERNAME;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERROLE;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERVID;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.XUSERNAME;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.APP_ID;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.ovf.SharedDisk;
import org.ow2.contrail.federation.federationcore.adapter.gafs.GlobalFileSystemRegistry;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistrator;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistry;
import org.ow2.contrail.federation.federationcore.authz.AuthzHub;
import org.ow2.contrail.federation.federationcore.authz.oauth.CertificatesCache;
import org.ow2.contrail.federation.federationcore.authz.oauth.OauthFedClient;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.monitoring.rest.ApplicationRestMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.rest.ContrailRestMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UGroup;
import org.ow2.contrail.federation.federationdb.jpa.entities.URole;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.multipart.MultiPart;
/**
 * Contrail Provider driver
 * @author Emanuele Carlini, Gaetano Anastasi, Marco Distefano
 *
 */
public class ContrailProviderManager extends BaseProviderManager
{
	private static Logger logger = Logger.getLogger(ContrailProviderManager.class);
	static String loggerTag = "[core.adapter.providermanager] (CPM) ";
	private static ClientRequest.Builder requestBuilder = ClientRequest.create();
	private static Client client = Client.create();
	private static ClientRequest clientRequest;
	private static final String truststore_path = "src/main/resources/truststore_client";
	private static final String truststore_password = "contrail";
	private static final String keystore_path = "src/main/resources/keystore_client";
	private static final String keystore_password = "contrail";
	private static HTTPSProperties prop = null;

	ContrailProviderManager(ProviderWatcherManager manager, CloudProviderId cpi, Provider provider, ProviderType type)
	{
		super(manager, cpi, provider, type);  
		if(provider.getProviderUri().startsWith("https")){
			logger.info(loggerTag +" setting https connection with provider: " + provider.getProviderUri());
			this.settingHttpsConnection();
		}
		UriBuilder uriBuilder = UriBuilder.fromUri(provider.getProviderUri());
		clientRequest = requestBuilder.build(uriBuilder.build(), "GET");

		logger.debug(loggerTag + "Creating provider \"" + provider.getName() + "\" with id \"" + providerID
				+ "\" with address \"" + provider.getProviderUri() + "\"");

		logger.debug(loggerTag + "provider created");
	}
	
	private void settingHttpsConnection(){
		try {
			//setting client truststore and keystore 
	        TrustManager mytm[] = null;
	        KeyManager mykm[] = null;
	
	        try {
	            mytm = new TrustManager[]{new MyX509TrustManager(truststore_path, truststore_password.toCharArray())};
	            mykm = new KeyManager[]{new MyX509KeyManager(keystore_path, keystore_password.toCharArray())};
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	
	        //setting SSL context
	        SSLContext context = null;
	
	        try {
	            context = SSLContext.getInstance("SSL");
	            context.init(mykm, mytm, null);
	        } catch (NoSuchAlgorithmException nae) {
	            nae.printStackTrace();
	        } catch (KeyManagementException kme) {
	            kme.printStackTrace();
	        }
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					System.out.println("Warning: URL Host: " + urlHostName + " vs. "
		                    + session.getPeerHost());
					return true;
				}
	        };
	        
	        prop = new HTTPSProperties(allHostsValid, context);
		} catch (Exception e) {
	        e.printStackTrace();
	    }
		
	}

	@Override
	protected void createMonitoring(ProviderWatcherManager manager) {
		provider_monitor = new ContrailRestMonitoring(manager, this);
	}

	private String getCertificateForPM(User user){
		String uuid = user.getUuid();
		String userCert = CertificatesCache.getInstance().getSerializedCertificate(uuid);
		if (userCert == null){
			X509Certificate cert = null;
			try {
				cert = OauthFedClient.getCertificateFromUuid(uuid); //this method also update the cache
				userCert = OauthFedClient.serializeCert(cert); 
			} catch (Exception e2) {
				userCert = "";
				logger.error(loggerTag + "Unable to retrieve certificate");
			}
		}
		return userCert;
	}

	/**
	 * Deploys the application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public String submit(Collection<ApplianceDescriptor> appDesc, User user, String ovf, String appUUid, String ceeId)
			throws ProviderComunicationException, UnknownProviderErrorException {
		String submittedId = "-1";

		// already requested authorization on aem.submitApplication

		logger.debug("Set up the clients for the communication with provisioning manager");

		String userCert = getCertificateForPM(user);

		// creates request
		try {
			MultiPart mp = new MultiPart();
			//final String temp_ceeId = "126"; 
			// String slaID = "SLASOI.xml";

			// mp.bodyPart(ovfName, MediaType.TEXT_PLAIN_TYPE).bodyPart(slaName, MediaType.TEXT_PLAIN_TYPE).bodyPart(ovf, MediaType.TEXT_XML_TYPE)
			//		.bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE).bodyPart(jsonVin2OVF, MediaType.APPLICATION_JSON_TYPE);

			//mp.bodyPart(ovfName, MediaType.TEXT_PLAIN_TYPE).bodyPart(ovf, MediaType.TEXT_XML_TYPE)
			//.bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE).bodyPart(user.getUserId().toString(), MediaType.TEXT_PLAIN_TYPE);

			mp.bodyPart(appUUid, MediaType.TEXT_PLAIN_TYPE)
			.bodyPart(ovf, MediaType.TEXT_XML_TYPE)
			.bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE)
			.bodyPart(Integer.toString(user.getUserId()), MediaType.TEXT_PLAIN_TYPE)
			.bodyPart(ceeId, MediaType.TEXT_PLAIN_TYPE); // This is the CEE ID, in a remote future may be the slaId 

			//build uri for request
			UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress()+"/application-mgmt/submit");
			requestBuilder.type("multipart/mixed");
			requestBuilder.accept("application/json");
			requestBuilder.header("X-Certificate", userCert);

			requestBuilder.entity(mp);

			//create request
			clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
			if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			//Can't use these two methods to reuse ClientRequest instance
			//clientRequest.setURI(uriBuilder.build());
			//clientRequest.setMethod("PUT");

			//execute the request
			ClientResponse answer = client.handle(clientRequest);
			JSONObject obj = answer.getEntity(JSONObject.class); //get answer body
			parseReturnStatus(obj); // internal codes of the PM
			boolean result = parseReturnStatus(answer); // codes of the REST interface
			if (result) {
				submittedId = obj.getString(APP_ID);
				logger.info(loggerTag + "Appliances successfully submitted with APP-ID \"" + submittedId + "\"");
			}
			else {
				logger.info(loggerTag + "Appliances unsubmitted for application " + submittedId);
			}
		}
		catch (JSONException e) {
			String error = "JSON object is incorrect while submitting a new appliance";
			logger.error(loggerTag +"Exception: " + e);
			e.printStackTrace();
			throw new ProviderComunicationException(error);
		}
		catch (UniformInterfaceException e) {
			String error = "Maybe the VEP has noot been started for provider " + this.getProviderName() + "\n";
			logger.error(loggerTag +"Exception: " + e.toString());
			throw new ProviderComunicationException(error);
		}
		catch (ClientHandlerException e1) {
			String error = "Unable to contact the provisioning manager for provider " + this.getProviderName() + "\n";
			logger.debug(loggerTag +"Exception: " + e1.toString());
			logger.error(error + "on address " + this.getProviderAddress());
			throw new ProviderComunicationException(error);
		}
		catch (Exception e1) {
			logger.error(loggerTag +"Exception: " + e1);
			logger.error(loggerTag + " user \"" + user.getUsername() + "\" error on submit APP-ID "+ submittedId);

		}
		
		if (!submittedId.equals("-1")){
			VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
			registry.addApplication(user.getUsername(), appUUid, submittedId);
			// registry.addApplication(user.getUsername(), " ", submittedId); // only for test, sorry about that
		}

		return submittedId;
	}

	/**
	 * Starts an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public Collection<String> internalStart(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
	UnknownProviderErrorException {
		logger.info(loggerTag + " **** Trying to start app ****");
		Collection<String> vmId = null;
		String userCert = getCertificateForPM(user);

		/* moved to aem
		AuthzHub ad = new AuthzHub();
		String appFedId = VirtualNetworkRegistry.getInstance().getAppNameByDeployId(appId, user.getUsername());
		AemCallback accessRevokeCallback = new AemCallback(FederationPEP.getPEPInstance(), appFedId, user.getUuid());
		boolean tryaccess = ad.startAccess(user.getUuid(), appFedId, accessRevokeCallback);
		if (!tryaccess) {
			logger.error(loggerTag + " user " + user.getUsername() + " not authorized to start app " + appId + " on " + this.providerID + " provider");
			return vmId;
		}
		boolean revokeaccess = ad.myWait(accessRevokeCallback, 1000);
		if (!revokeaccess) {
			logger.error(loggerTag + " user " + user.getUsername() + " not authorized to start app " + appId + " on " + this.providerID + " provider");
			return vmId;
		}
		logger.info(loggerTag + " user " + user.getUsername() + " IS authorized to start app " + appId + " on provider " + this.providerID + " provider");
		 */

		// creates request
		JSONObject jobject = createJSONRequest(user, appDesc);
		try {
			//build uri for request
			UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress() +"/application-mgmt/start/"+ appId);
			requestBuilder.type("application/json");
			requestBuilder.accept("application/json");
			requestBuilder.header("X-Certificate", userCert);
			requestBuilder.entity(jobject);

			//create request
			clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
			if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			//execute the request
			ClientResponse answer = client.handle(clientRequest);
			boolean result = this.parseReturnStatus(answer);
			if (result) {
				//get answer body
				JSONObject obj = answer.getEntity(JSONObject.class);
				JSONArray array = obj.getJSONArray(VM_IDS);

				logger.info(loggerTag + " appliances successfully started with APP-ID " + appId + " on:\n");

				//TODO: to comment what this piece of code does
				vmId = new ArrayList<String>();
				for (int i = 0; i < array.length(); i++) {
					logger.info("\tvm - " + array.getString(i) + "\n");
					vmId.add(array.getString(i));
				}
			}
			else {
				logger.error(loggerTag + "user \"" + user.getUsername() + "\" error in perform application start");
			}
		}
		catch (JSONException e) {
			logger.error(loggerTag + " Json object is incorrect while submitting a new appliance");
			logger.error(e);
			throw new ProviderComunicationException(e.toString());
		}
		catch (ClientHandlerException e1)
		{
			logger.error(loggerTag + " Unable to contact the provisioning manager");
			throw new ProviderComunicationException(e1.toString());
		}

		return vmId;
	}

	public Collection<String> internalDeploy(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
	UnknownProviderErrorException
	{
		Collection<String> vmId = null;
		logger.info(loggerTag + " **** Trying to deploy app ****");

		//Second hook for the integration with VIN 
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		String appName = registry.getAppNameByDeployId(appId, user.getUsername());
		System.out.println(" appName is " + appName);
		
		Map<String,String> mapVin2OVF = registry.getVmIdsByApplication(appName); //in case there is no ids, it seems the map is empty but it is created
		Set<String> vinIds = mapVin2OVF.keySet();
		int numVms = vinIds.size();
		int numAppliances = appDesc.size();

		/*
		 *Modifying the ovf should not be necessary as we are using DeploymentDocument
		ArrayList<String> coll = new ArrayList<String>(numVms);
		for(String s : vinIds ){
			coll.add(mapVin2OVF.get(s));
		}
		 */

		int vmNumbFromAppl = 0;
		ArrayList<String> coll = new ArrayList<String>(numAppliances);
		ArrayList<String> collVmNumber = new ArrayList<String>(numAppliances);
		for (ApplianceDescriptor i1 : appDesc){
			coll.add(i1.getID());
			collVmNumber.add(Integer.toString(i1.getDefaultVmNumber()));
			vmNumbFromAppl += i1.getDefaultVmNumber();
		}
		
		if (numVms == 0) {
			numVms = vmNumbFromAppl;
		}
		
		Collection<String> gafsUrls = new ArrayList<String>(numVms);
		for (ApplianceDescriptor a: appDesc){ // FIXME: we should iterate on vms (here still on the assumption one vm per vsys - only for sharedDisks not VIN)
			// ASSUMPTION: the provider can support one shared disk per appliance (they can be different one each other) 
			try { 
				Collection<SharedDisk> sd = a.getSharedDisks();
				Iterator<SharedDisk> i = sd.iterator();
				SharedDisk s = i.next();
				String gafsVolumeName = s.getFile().getId();
				System.out.println(gafsVolumeName);
				long volId = GlobalFileSystemRegistry.getInstance().getVolumeId(appName, gafsVolumeName);
				String gafsDIR = GlobalFileSystemRegistry.getInstance().get_DIR_Server(volId);
				gafsUrls.add(gafsDIR);
			}
			catch (Exception e){
				gafsUrls.add("");
			}
		}
		JSONArray virtualSystems = new JSONArray(coll);
		JSONArray vmNumber = new JSONArray(collVmNumber);
		JSONArray ibisPool =  new JSONArray();
		JSONArray ibisServer =  new JSONArray();
		JSONArray vinAgents =  new JSONArray();
		JSONArray gafsVolumes =  new JSONArray(gafsUrls);
		JSONArray jsonVinIds = null; 
		
		JSONObject jobject = createJSONRequest(user, appDesc); // creates request
		
		if (vinIds.size() == 0){
			jsonVinIds = new JSONArray();
			for (int i=0 ; i<numVms; i++){
				try {
					jsonVinIds.put(i,"");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		else {
			jsonVinIds = new JSONArray(vinIds);
		}
		
		try {
			String ibisPoolStr = "", ibisServerStr = "";
			ibisPoolStr = VirtualNetworkRegistrator.getIBisPoolName();
			ibisServerStr = VirtualNetworkRegistrator.getIbisServerAddress();
			logger.debug("VIN AGENT: " + FederationProperties.getInstance().getProperty("vin-agent-context"));
			System.out.println("        " + ibisPoolStr);
			for (int i=0 ; i< numVms; i++){
				ibisPool.put(i,ibisPoolStr);
				ibisServer.put(i,ibisServerStr);
				vinAgents.put(i, FederationProperties.getInstance().getProperty("vin-agent-context"));
			}

		} catch (Exception e) {
			logger.equals(loggerTag + e);
		}
		
		try {
			jobject.put("appliances", virtualSystems);
			jobject.put("vmNumberForAppliance", vmNumber);
			jobject.put("vin_ids", jsonVinIds);
			jobject.put("vin_ibis_servers", ibisServer);
			jobject.put("vin_pool_names", ibisPool);
			jobject.put("vin_agent_context", vinAgents);
			jobject.put("gafs_volume_urls", gafsVolumes);
		} catch (Exception e2) {
			logger.info(loggerTag + " impossible to send VIN Ids to Provisioning Manager -" + e2);
		}

		System.out.println(jobject);

		String userCert = getCertificateForPM(user);
		try {
			//build uri for request
			UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress()+"/application-mgmt/deploy/"+ appId);
			requestBuilder.type("application/json");
			requestBuilder.accept("application/json");
			requestBuilder.header("X-Certificate", userCert);
			requestBuilder.entity(jobject);

			//create request
			clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
			if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			//execute the request
			ClientResponse answer = client.handle(clientRequest);
			boolean result = this.parseReturnStatus(answer);

			if (result){
				logger.info(loggerTag + " appliances successfully deployed with Provider APP-ID " + appId + "\n");
			}
			else {
				logger.error(loggerTag +" user \"" + user.getUsername() + "\" error in performing deployment");
			}
		}
		catch (ClientHandlerException e1) {
			logger.error(loggerTag + " Unable to contact the provisioning manager");
			throw new ProviderComunicationException(e1.toString());
		}

		return vmId;
	}

	@Override
	public boolean propagateUser(User user) throws ProviderComunicationException, UnknownProviderErrorException
	{
		boolean result = false;

		// setup the authz classes
		/* TODO check
		AuthzHub ad = new AuthzHub();
		ad.init();
		ad.requestAuthz(user.getUuid());

		if (ad.checkAuthz() == false) {
			logger.error(loggerTag +" user \"" + user.getUsername() + "\" cannot be propagated on provider \""
					+ this.getProviderName() + "\" beacuse missing authorization by PDP.");
			return result;
		}
		

		logger.info("Contrail Provider Manager: user \"" + user.getUsername() + "\" can be propagated on provider \"" + this.getProviderName()
				+ "\" it is authorized by PDP.");
		 */
		logger.debug("User propagation: Creating service for REST invocation");

		// creating the JSON object
		JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields
		try {
			jobject.put(USERNAME, user.getUsername() + "");
			String uuid = user.getUuid();
			if (uuid == null) {
				logger.warn("Invalid or null users uuid, using: -1");
				uuid = "-1";
			}
			jobject.put(USERVID, uuid);
			
			JSONArray rolesArray = new JSONArray();
			if (user.getUGroupList() != null) {
				for (URole urole : user.getURoleList()) {
					rolesArray.put(urole.getName());
				}
			}
			//jobject.put(USERROLE, rolesArray); // user's role
			jobject.put(USERROLE, "user");
			
			jobject.put(XUSERNAME, FederationProperties.getInstance().getProperty("xuser.id"));// Xusernam

			JSONArray jsonArray = new JSONArray();
			if (user.getUGroupList() != null) {
				for (UGroup uGroup : user.getUGroupList()) {
					jsonArray.put(uGroup.getName());
				}
			}
			jobject.put(USERGROUPS, jsonArray);
			logger.debug("User propagation: sending REST invocation");
			// creates request

			String userCert = this.getCertificateForPM(user);
			
			UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress()+"/user-mgmt/propagate");
			requestBuilder.type("application/json");
			requestBuilder.accept("application/json");
			requestBuilder.header("X-Certificate", userCert);
			requestBuilder.entity(jobject);
			clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
			if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			//execute the request
			ClientResponse answer = client.handle(clientRequest);
			result = this.parseReturnStatus(answer);	
			logger.debug("User propagation: response received " + answer);
			
			if (result) {
				logger.debug("Contrail Provider Manager: user " + user.getUsername() + " successfully propagated");
			}
			else {
				logger.error("Contrail Provider Manager: user " + user.getUsername()+ " has not been propagated");
			}

		}
		catch (ClientHandlerException e1)
		{
			logger.debug(e1.getMessage());
			logger.error("(CPM) Unable to contact the provisioning manager at address " + this.getProviderAddress());
			// throw new ProviderComunicationException(e1);
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: json object is incorrect while propagating a new user");
			throw new ProviderComunicationException(e);
		}
		finally
		{
			//ad.releaseAuthz();
		}

		return result;
	}

	/**
	 * Stops an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public boolean internalStop(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
	UnknownProviderErrorException
	{
		boolean result = false;

		// setup the authz classes
		AuthzHub ad = new AuthzHub();
		String appFedId = VirtualNetworkRegistry.getInstance().getAppNameByDeployId(appId, user.getUsername());
		boolean tryaccess = ad.stopAccess(user.getUuid(), appFedId);
		if (!tryaccess) {
			logger.error(loggerTag + "user " + user.getUsername() + " not authorized to stop app on provider " + this.providerID);
			return result;
		}

		String userCert = getCertificateForPM(user);

		// creating the JSON object.
		JSONObject jobject = createJSONRequest(user, appDesc);

		// creates uri
		UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress()+"/application-mgmt/stop/" + appId);
		requestBuilder.type("application/json");
		requestBuilder.accept("application/json");
		requestBuilder.header("X-Certificate", userCert);
		requestBuilder.entity(jobject);
		clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
		if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
		//execute the request
		ClientResponse answer = client.handle(clientRequest);
		result = this.parseReturnStatus(answer);
		if (result){
			logger.info(loggerTag + " appliances successfully stopped with APP-ID " + appId);
		}
		else {
			logger.error(loggerTag + " user \"" + user.getUsername() + "\" not authorized to perform this action");
		}
		return result;

	}

	/**
	 * Kills an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public boolean internalKill(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
	UnknownProviderErrorException
	{
		boolean result = false;

		// setup the authz classes
		AuthzHub ad = new AuthzHub();
		String appFedId = VirtualNetworkRegistry.getInstance().getAppNameByDeployId(appId, user.getUsername());
		boolean tryaccess = ad.stopAccess(user.getUuid(), appFedId);
		if (!tryaccess) {
			logger.error(loggerTag + "user " + user.getUsername() + " not authorized to kill app on provider " + this.providerID);
			return result;
		}

		String userCert = getCertificateForPM(user);

		// creating the JSON object.
		JSONObject jobject = createJSONRequest(user, appDesc);

		// creates uri
		UriBuilder uriBuilder = UriBuilder.fromUri(getProviderAddress()+"/application-mgmt/delete/" + appId);
		requestBuilder.type("application/json");
		requestBuilder.accept("application/json");
		requestBuilder.header("X-Certificate", userCert);
		requestBuilder.entity(jobject);
		clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
		if(prop != null) clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
		//execute the request
		ClientResponse answer = client.handle(clientRequest);
		result = this.parseReturnStatus(answer);
		if (result){
			logger.info(loggerTag + " appliances successfully kill with APP-ID " + appId);
		}
		else {
			logger.error(loggerTag + " user \"" + user.getUsername() + "\" not authorized to perform this action");
		}
		return result;

	}
	
	private static JSONObject getVinIds(String appName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> mapVin2OVF = registry.getVmIdsByApplication(appName); //in case there is no ids, it seems the map is empty but it is created
		JSONObject jsonVin2OVF = new JSONObject(mapVin2OVF);
		return jsonVin2OVF;
	}

	private JSONObject createJSONRequest(User user, Collection<ApplianceDescriptor> appDesc) throws ProviderComunicationException {
		// creating the JSON object.
		JSONObject jobject = new JSONObject();

		try {
			jobject.put("name", user.getUsername() + "");

			// for each appliance add its the name in the request to provisioning manager.
			JSONArray jsonArray = new JSONArray();
			for (ApplianceDescriptor app : appDesc) {
				jsonArray.put(app.getID());
			}
			jobject.put("appliances", jsonArray);
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: incorrect json object.");
			throw new ProviderComunicationException(e.toString());
		}

		return jobject;
	}

	/**
	 * Starts application-level monitoring.
	 */
	@Override
	protected boolean startApplicationLevelMonitoring(Collection<String> result)
	{
		app_mon = new ApplicationRestMonitoring(this.providerWatcherManager, result, this);

		return true;
	}

	/**
	 * Stops application-level monitoring.
	 */
	protected boolean stopApplicationLevelMonitoring()
	{
		app_mon.stop();

		return true;
	}

	@Override
	public String notifyFederationConnection()
	{
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * This method check the return status from the provisioning manager.
	 * 
	 * @param answer
	 * @return
	 * @throws ProviderComunicationException 
	 * @throws UnknownProviderErrorException 
	 */
	private boolean parseReturnStatus(ClientResponse answer) throws ProviderComunicationException, UnknownProviderErrorException {
		boolean result = false;
		Status s = answer.getClientResponseStatus();
		if (s.equals(Status.OK)){
			result = true;
		}
		else if (s.equals(Status.UNAUTHORIZED)){
			logger.error(loggerTag + " PM returned: 401 UNAUTHORIZED");
		}
		else if (s.equals(Status.NOT_FOUND)){
			logger.error(loggerTag + " PM returned: 404 NOT FOUND");
		}
		else if (s.equals(Status.UNSUPPORTED_MEDIA_TYPE)){
			logger.error(loggerTag + " PM returned 415 UNSUPPORTED_MEDIA_TYPE");
		}
		else if (s.equals(Status.INTERNAL_SERVER_ERROR)){
			String message = " PM returned 500 INTERNAL SERVER ERROR";
			logger.error(loggerTag + message);
			throw new UnknownProviderErrorException(message);
		}
		else {
			logger.error(loggerTag + " PM returned" + s);
		}

		return result;

	}

	private boolean parseReturnStatus(JSONObject object) throws ProviderComunicationException, UnknownProviderErrorException, JSONException {
		boolean result = false;
		String s = (String) object.get(RETURN_STATUS);

		if (s.equals(Status.OK)){
			result = true;
		}
		else if (s.equals(Status.UNAUTHORIZED)){
			logger.error(loggerTag + " PM Status returned: 401 UNAUTHORIZED");
		}
		else if (s.equals(Status.NOT_FOUND)){
			logger.error(loggerTag + " PM status returned: 404 NOT FOUND");
		}
		else if (s.equals(Status.UNSUPPORTED_MEDIA_TYPE)){
			logger.error(loggerTag + " PM status returned 415 UNSUPPORTED_MEDIA_TYPE");
		}
		else if (s.equals(Status.INTERNAL_SERVER_ERROR)){
			logger.error(loggerTag + " PM status returned 500 INTERNAL SERVER ERROR");
		}
		else {
			logger.error(loggerTag + " PM returned " + s);
		}

		return result;

	}
	
	
	 static class MyX509TrustManager implements X509TrustManager {

         /*
          * The default PKIX X509TrustManager9.  We'll delegate
          * decisions to it, and fall back to the logic in this class if the
          * default X509TrustManager doesn't trust it.
          */
         X509TrustManager pkixTrustManager;

         MyX509TrustManager(String trustStore, char[] password) throws Exception {
             this(new File(trustStore), password);
         }

         MyX509TrustManager(File trustStore, char[] password) throws Exception {
             
        	 // create a "default" JSSE X509TrustManager.
             KeyStore ks = KeyStore.getInstance("JKS");
             ks.load(new FileInputStream(trustStore), password);
             TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
             tmf.init(ks);
             return;
         }


         public X509Certificate[] getAcceptedIssuers() { return null; }
         public void checkClientTrusted(X509Certificate[] certs, String authType) {}
         public void checkServerTrusted(X509Certificate[] certs, String authType) {}

    }

    /**
     * Inspired from http://java.sun.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html
     *
     */
    static class MyX509KeyManager implements X509KeyManager {

         /*
          * The default PKIX X509KeyManager.  We'll delegate
          * decisions to it, and fall back to the logic in this class if the
          * default X509KeyManager doesn't trust it.
          */
         X509KeyManager pkixKeyManager;

         MyX509KeyManager(String keyStore, char[] password) throws Exception {
             this(new File(keyStore), password);
         }

         MyX509KeyManager(File keyStore, char[] password) throws Exception {
             // create a "default" JSSE X509KeyManager.

             KeyStore ks = KeyStore.getInstance("JKS");
             ks.load(new FileInputStream(keyStore), password);

             KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509", "SunJSSE");
             kmf.init(ks, password);

             KeyManager kms[] = kmf.getKeyManagers();

             /*
              * Iterate over the returned keymanagers, look
              * for an instance of X509KeyManager.  If found,
              * use that as our "default" key manager.
              */
             for (int i = 0; i < kms.length; i++) {
                 if (kms[i] instanceof X509KeyManager) {
                     pkixKeyManager = (X509KeyManager) kms[i];
                     return;
                 }
             }

             /*
              * Find some other way to initialize, or else we have to fail the
              * constructor.
              */
             throw new Exception("Couldn't initialize");
         }
         
        public PrivateKey getPrivateKey(String arg0) {
            return pkixKeyManager.getPrivateKey(arg0);
        }

        public X509Certificate[] getCertificateChain(String arg0) {
            return pkixKeyManager.getCertificateChain(arg0);
        }

        public String[] getClientAliases(String arg0, Principal[] arg1) {
            return pkixKeyManager.getClientAliases(arg0, arg1);
        }

        public String chooseClientAlias(String[] arg0, Principal[] arg1, Socket arg2) {
            return pkixKeyManager.chooseClientAlias(arg0, arg1, arg2);
        }

        public String[] getServerAliases(String arg0, Principal[] arg1) {
            return pkixKeyManager.getServerAliases(arg0, arg1);
        }

        public String chooseServerAlias(String arg0, Principal[] arg1, Socket arg2) {
            return pkixKeyManager.chooseServerAlias(arg0, arg1, arg2);
        }
    }

}

