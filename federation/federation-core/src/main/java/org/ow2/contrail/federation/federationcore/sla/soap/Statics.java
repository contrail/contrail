package org.ow2.contrail.federation.federationcore.sla.soap;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.w3c.dom.Document;

public class Statics 
{
	public static String slatoffer = "";
			
	public static String serializeXml(String path)
	{
		String slat ="";
		try
		{
			
			DocumentBuilderFactoryImpl dbf = new DocumentBuilderFactoryImpl();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(path);
			
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", Integer.toString(2));

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc.getDocumentElement());

			trans.transform(source, result);
			slat = sw.toString();
			
		}
		catch (Exception e) {e.printStackTrace();}
		return slat;
	}
	
}
