package org.ow2.contrail.federation.federationcore.exception;

public class UnknownProviderTypeException extends Exception
{

	public UnknownProviderTypeException()
	{
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderTypeException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderTypeException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderTypeException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
