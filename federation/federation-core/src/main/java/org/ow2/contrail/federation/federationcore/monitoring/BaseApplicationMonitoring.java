package org.ow2.contrail.federation.federationcore.monitoring;

import java.util.Collection;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

public abstract class BaseApplicationMonitoring extends BaseMonitoring 
{
	protected Collection<String> vmids;
	protected BaseProviderManager provider;
	
	public BaseApplicationMonitoring(ProviderWatcherManager manager, Collection<String> vmids, BaseProviderManager provider) 
	{
		super(manager);
		
		this.vmids = vmids;
		this.provider = provider;
	}
}
