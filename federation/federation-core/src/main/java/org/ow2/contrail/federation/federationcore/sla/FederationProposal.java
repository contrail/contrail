package org.ow2.contrail.federation.federationcore.sla;

import java.util.List;

import javax.persistence.EntityManager;

import org.ow2.contrail.federation.federationdb.jpa.dao.UserSLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLA;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Class to aggregate informations to return to Federation API for confirm Negotiation
 */
public class FederationProposal {

	/**
	 * SLATemplateProposal to return to Federation-API
	 */
	private SLATemplate slaProposal;
	
	/**
	 * slaTemplateId of the Template associated with this proposal
	 */
	private Integer slaTemplateId;

	/**
	 * The aggregate Data of the proposal to show in Federation GUI
	 */
	private String aggregateData;
	
	/**
	 * Negotiation ID associated to SLATemplateProposal
	 */
	private String negotiationId;
	
	public FederationProposal(SLATemplate slaP, Integer slaTid, String negId, String aggData){
		this.slaProposal = slaP;
		this.slaTemplateId = slaTid;
		this.negotiationId=negId;
		this.aggregateData = aggData;
		
	}

	public SLATemplate getSlaProposal() {
		return slaProposal;
	}
	public void setSlaProposal(SLATemplate slaProposal) {
		this.slaProposal = slaProposal;
	}
	
	public Integer getSlaTemplateId() {
		return slaTemplateId;
	}
	public void setSlaTemplateId(Integer slaTemplateId) {
		this.slaTemplateId = slaTemplateId;
	}
	
	public String getNegotiationId() {
		return negotiationId;
	}
	public void setNegotiationId(String negotiationId) {
		this.negotiationId = negotiationId;
	}
	
	public String getAggregateData() {
		return aggregateData;
	}
	public void setAggregateData(String aggregateData) {
		this.aggregateData = aggregateData;
	}
	
	/**
	 * getUserSLAObject: method for create 
	 * @param slaContent
	 * @return
	 */
	public UserSLA getUserSLAObject(String slaContent){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		
		//Retrieve UserSLATemplate associated with the proposal accepted
		UserSLATemplate slat = new UserSLATemplateDAO(em).findById(this.slaTemplateId);
		UserSLA userSLA =null;
		// If slat existing in DB we have to save the SLA retrieved in DB
		if (slat != null){
			userSLA = new UserSLA();
			userSLA.setName(slat.getName());
			userSLA.setSla(slat.getName());
			//userSLA.setTemplateURL(slat.getSlatId().getProviderId().getProviderId().toString());
			userSLA.setSLATemplateId(slat);
			userSLA.setUserId(slat.getUserId());
			userSLA.setContent(slaContent);
		}
		
		em.close();
		return userSLA;
	}
	
	
	
}
