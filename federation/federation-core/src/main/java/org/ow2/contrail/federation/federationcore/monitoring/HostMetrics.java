package org.ow2.contrail.federation.federationcore.monitoring;

public enum HostMetrics 
{
	common,
	memory,
	cpu,
	disk,
	network,
	cluster
}
