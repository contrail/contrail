package org.ow2.contrail.federation.federationcore.aem.application;

import java.util.HashMap;

import org.ow2.contrail.federation.federationcore.exception.ApplicationNotFoundException;
import org.ow2.contrail.federation.federationcore.exception.ApplicationOperationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;

public interface IApplicationManagement
{

	public boolean startApp(User user, Application app) throws ApplicationNotFoundException, ProviderComunicationException,  UnknownProviderErrorException, ApplicationOperationException;

	public void stopApp(User user, Application app) throws ApplicationNotFoundException, ProviderComunicationException, UnknownProviderErrorException, ApplicationOperationException;

	public boolean cancelSubmission(User user, Application app) throws ApplicationOperationException;
	
	public boolean submitApp(User user, Application app, UserSLATemplate userSLATemplate) throws UnknownProviderErrorException, ApplicationOperationException;

	public boolean killApp(User user, Application app) throws ApplicationNotFoundException, ProviderComunicationException, UnknownProviderErrorException, ApplicationOperationException;

	public boolean createAgreement(User user, Application app, int proposalId);

}
