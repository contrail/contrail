package org.ow2.contrail.federation.federationcore.exception;

/**
 * Exception for notifying that the provider has not been registered inside the federation.
 *
 */

public class UnknownProviderErrorException extends Exception
{
	private static final long serialVersionUID = 4994081880636476849L;

	public UnknownProviderErrorException()
	{
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderErrorException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderErrorException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnknownProviderErrorException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
