package org.ow2.contrail.federation.federationcore.adapter.gafs;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.resource.vin.controller.VinAPI;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public class GlobalAutonomousFileSystem {
	private static Logger logger = Logger.getLogger(GlobalAutonomousFileSystem.class);
	private static String loggerTag = "[GAFS Adapter] ";
	private static int requestId = 0;
	private static URL serverURL = null;
	private static GlobalAutonomousFileSystem instance = new GlobalAutonomousFileSystem();

	private static String GAFS_SERVER_IP = "146.48.81.251";
	private static String GAFS_SERVER_PORT = "8080";

	public static GlobalAutonomousFileSystem getInstance() {
		return instance;
	}

	// tuscany complains about it
	public GlobalAutonomousFileSystem() {
		try {
			if (!FederationProperties.getInstance().getProperty("gafs-server-ip").equals(""))
				GAFS_SERVER_IP = FederationProperties.getInstance().getProperty("gafs-server-ip");
			if (!FederationProperties.getInstance().getProperty("gafs-server-port").equals(""))
				GAFS_SERVER_PORT = FederationProperties.getInstance().getProperty("gafs-server-port");
		}
		catch(NullPointerException e){
			logger.info(loggerTag + " Gafs config parameters are not set, using defaults");
		}

		try {
			serverURL = new URL("http://" + GAFS_SERVER_IP +":"+ GAFS_SERVER_PORT + "/xtreemfs-jsonrpc/executeMethod");
		} catch (MalformedURLException e) {
			logger.error(loggerTag + "Error in specifying the URL");
		}
		logger.info(loggerTag + " Driver created, connected with " + GAFS_SERVER_IP + ":" + GAFS_SERVER_PORT);
	}

	public static JSONRPC2Response invokeMethod(String methodName, Map<String, Object> parameters){
		JSONRPC2Session mySession = new JSONRPC2Session(serverURL);
		String id = "id-"+(++requestId);
		JSONRPC2Request req = new JSONRPC2Request(methodName, parameters, id);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(req);
		} catch (JSONRPC2SessionException e) {
			logger.error(loggerTag + "Internal error" + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public List<String> listVolumes(){
		List<String> volumes = null;
		JSONRPC2Response response = invokeMethod("listVolumes", null);
		logger.debug("[GAFS] listVolumes response: " + response);
		// System.out.println(response);
		if (response != null && response.indicatesSuccess()){
			JSONArray res = (JSONArray) response.getResult();
			int lenght = res.size();
			volumes = new ArrayList<String>(lenght);
			for (int i=0; i < lenght; i++){
				JSONObject j = (JSONObject) res.get(i);
				String tmp = (String) j.get("name");
				volumes.add(tmp);
			}
		}
		return volumes;
	}

	private List<String> JSONArray2List(JSONArray res){
		List<String> servers = null;
		int lenght = res.size();
		servers = new ArrayList<String>(lenght);

		for (int i=0; i < lenght; i++){
			String tmp = (String) res.get(i);
			servers.add(tmp);
		}
		return servers;
	}

	public List<String> getServers(String vol_name){
		List<String> servers = null;
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("volume_name", vol_name);
		JSONRPC2Response response = GlobalAutonomousFileSystem.invokeMethod("getServers", parametersMap);

		if (response != null && response.indicatesSuccess()){
			JSONArray res = (JSONArray) response.getResult();
			servers = JSONArray2List(res);
		}
		return servers;
	}

	public List<String> startVinAgents(List<String> servers, List<String> vids, String pool_name, String server_address){
		List<String> servers_joined = null;
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("server_ips", servers);
		parametersMap.put("vin_agent_identifiers", vids);
		parametersMap.put("ibis_pool_name", pool_name);
		parametersMap.put("ibis_server_address", server_address);

		JSONRPC2Response response = GlobalAutonomousFileSystem.invokeMethod("startVINAgents", parametersMap);
		if (response != null && response.indicatesSuccess()){
			JSONArray res = (JSONArray) response.getResult();
			servers_joined = JSONArray2List(res);
		}

		return servers_joined;
	}

	public static void main(String[] args){
		GlobalAutonomousFileSystem gafs = new GlobalAutonomousFileSystem();
		List<String> response = gafs.listVolumes();
		if (response != null ){
			System.out.println(response);
		}
		else {
			System.out.println("Error on listVolumes");
		}

		response = gafs.getServers("myVolume");
		if (response != null ){
			System.out.println(response);
		}
		else {
			System.out.println("Error on getServers");
		}

		response = gafs.startVinAgents(response, null, null, null);
		if (response != null ){
			System.out.println(response);
		}
		else {
			System.out.println("Error on startVinAgents");
		}



	}


}
