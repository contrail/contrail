package org.ow2.contrail.federation.federationcore.aem.mapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.*;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public class MappingPlan
{
	private static final String APP_NOT_SUBMITTED = "-1";
	private static Logger logger = Logger.getLogger(MappingPlan.class);
	private static String loggerTag = "[core.aem.mapping] ";
	private HashMap<CloudProviderId, Collection<ApplianceDescriptor>> mapping = new HashMap<CloudProviderId, Collection<ApplianceDescriptor>>();
	private Map<CloudProviderId, Double> providerWeight = new HashMap<CloudProviderId, Double>();
	private Double weight = Double.NEGATIVE_INFINITY;
	
	public MappingPlan(){
		super();
	}
	
	public MappingPlan(HashMap<CloudProviderId, Collection<ApplianceDescriptor>> appMapping){
		mapping = appMapping;
	}


	public HashMap<CloudProviderId, Collection<ApplianceDescriptor>> getMapping(){
		return mapping;
	}
	
	/**
	 * In the case where each mapping plan refers to one provider, it returns the provider 
	 * where all the appliances have been allocated.
	 */
	public CloudProviderId getAllocationProvider(){
		Set<CloudProviderId> id_set = mapping.keySet();
		Iterator<CloudProviderId> i = id_set.iterator();
		if (id_set.size() == 1 && i.hasNext()){
			return i.next();
		}
		return null;
	}
	
	public Map<CloudProviderId, Double> getProviderMapWeight(){ return providerWeight; }
	
	public void setProviderWeight(CloudProviderId id, Double w){
		providerWeight.put(id, w); //TODO: some checks
		weight = w;
	}
	
	public Double getProviderWeight(CloudProviderId id){
		return providerWeight.get(id); //TODO: some checks
	}
	
	public Double getWeight(){return weight; }
	
	public void addMap(ApplianceDescriptor applianceDescriptor, CloudProviderId cloudProviderDescriptor){
		Collection<ApplianceDescriptor> applianceCollection = mapping.get(cloudProviderDescriptor);

		if (applianceCollection == null){
			applianceCollection = new Vector<ApplianceDescriptor>();
		}

		applianceCollection.add(applianceDescriptor);
		mapping.put(cloudProviderDescriptor, applianceCollection);
	}

	private String deployOnSingleProvider(User fedUser, String ovf, String ovfName, String ceeId, CloudProviderId cp_id) 
			throws ProviderComunicationException, UnknownProviderErrorException {
		String appId = APP_NOT_SUBMITTED;
		boolean submitted = false;
		logger.debug(loggerTag + " getting mapping for Provider: " + cp_id.toString());
		//for debug
		Iterator it = mapping.entrySet().iterator();
		while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        logger.debug(pairs.getKey() + " = " + pairs.getValue());
		}
		Collection<ApplianceDescriptor> applianceColl = mapping.get(cp_id);
		ICloudProviderManager cpd = ProviderContainer.getInstance(cp_id);
		appId = cpd.submit(applianceColl, fedUser, ovf, ovfName, ceeId);
		logger.debug(loggerTag + "appId: " + appId);
		if (!appId.equals(APP_NOT_SUBMITTED)){
			logger.debug(loggerTag + "successfully submit with appId: " + appId);
			submitted = true;
		}
		if (submitted){
			if (applianceColl == null) logger.debug(loggerTag + "ApplianceCollector is null: ERROR");
			logger.info(loggerTag + applianceColl.size() + " appliances of app #" + ovfName + " submitted with provider " + cpd.getProviderId());
			ContrailProviderManager m = (ContrailProviderManager) cpd;
			m.internalDeploy(applianceColl, fedUser, appId);
		}
		else{
			logger.error(loggerTag + " IMPOSSIBLE TO SUBMIT " + applianceColl.size() + " appliances on provider " + cpd.getProviderId());
		}
		return appId;
	}
	
	public String deploy(User fedUser, String ovf, String ovfName, String ceeId, CloudProviderId pId) throws ProviderComunicationException,
	UnknownProviderErrorException
	{
		String appId = APP_NOT_SUBMITTED;
		if (pId != null){
			// providerId is retrieved by SLA in this case
			appId = deployOnSingleProvider(fedUser, ovf, ovfName, ceeId, pId);
		}
		else {
			Set<CloudProviderId> cloud_set = this.mapping.keySet();
			Iterator<CloudProviderId> c = cloud_set.iterator();
			while (c.hasNext() && appId.equals(APP_NOT_SUBMITTED)){
				CloudProviderId cp_id = c.next();
				appId = deployOnSingleProvider(fedUser, ovf, ovfName, ceeId, cp_id);
			}
		}
		return appId;
	}

	public Collection<String> start(User user, String appfedId , String appDeployId) throws ProviderComunicationException, UnknownProviderErrorException
	{
		Collection<String> success = null;
		for (CloudProviderId cp_id : this.mapping.keySet())
		{
			Collection<ApplianceDescriptor> applianceColl = mapping.get(cp_id);
			ICloudProviderManager cpd = ProviderContainer.getInstance(cp_id);
			success = cpd.start(applianceColl, user, appDeployId);
			if (success != null){
				logger.info(loggerTag + applianceColl.size() + " appliances of app #" + appfedId + " started on provider " + cpd.getProviderId());
			}
			else{
				logger.error(loggerTag + " IMPOSSIBLE TO START " + applianceColl.size() + " appliances on provider " + cpd.getProviderId());
			}
		}
		return success;
	}

	public boolean stop(User user, String appFedId, String appDeployId) throws ProviderComunicationException, UnknownProviderErrorException
	{
		boolean success = false;
		for (CloudProviderId cp_id : this.mapping.keySet())
		{
			Collection<ApplianceDescriptor> applianceColl = mapping.get(cp_id);
			ICloudProviderManager cpd = ProviderContainer.getInstance(cp_id);
			success = cpd.stop(applianceColl, user, appDeployId);
			if (success)
			{
				logger.info(loggerTag + applianceColl.size() + " appliances of app #" + appFedId + " stopped on provider " + cpd.getProviderId());
			}
			else
			{
				logger.error(loggerTag + " IMPOSSIBLE TO STOP " + applianceColl.size() + " appliances on provider " + cpd.getProviderId());
			}
		}
		return success;
	}
	
	public boolean kill(User user, String appFedId, String appDeployId) throws ProviderComunicationException, UnknownProviderErrorException
	{
		boolean success = false;
		for (CloudProviderId cp_id : this.mapping.keySet())
		{
			Collection<ApplianceDescriptor> applianceColl = mapping.get(cp_id);
			ICloudProviderManager cpd = ProviderContainer.getInstance(cp_id);
			success = cpd.internalKill(applianceColl, user, appDeployId);
			if (success)
			{
				logger.info(loggerTag + applianceColl.size() + " appliances of app #" + appFedId + " stopped on provider " + cpd.getProviderId());
			}
			else
			{
				logger.error(loggerTag + " IMPOSSIBLE TO STOP " + applianceColl.size() + " appliances on provider " + cpd.getProviderId());
			}
		}
		return success;
	}
	
	public String toString(){
		String s = new String("MappingPlan related to providers: ");
		Set<CloudProviderId> clouds = providerWeight.keySet();
		Iterator<CloudProviderId> i = clouds.iterator();
		while (i.hasNext()){
			s += " ";
			s += i.next();
		}
		return s;
	}
}