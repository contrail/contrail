package org.ow2.contrail.federation.federationcore.exception;

public class ApplicationNotFoundException extends Exception
{

	public ApplicationNotFoundException()
	{
		// TODO Auto-generated constructor stub
	}

	public ApplicationNotFoundException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ApplicationNotFoundException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ApplicationNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
