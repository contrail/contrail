package org.ow2.contrail.federation.federationcore.monitoring.rest;

import java.util.Collection;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager;
import org.ow2.contrail.federation.federationcore.monitoring.BaseApplicationMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.VMMetrics;
import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

import com.sun.jersey.api.client.WebResource;

public class ApplicationRestMonitoring extends BaseApplicationMonitoring
{


	public ApplicationRestMonitoring(ProviderWatcherManager manager,
			Collection<String> vmids, BaseProviderManager provider)
	{
		super(manager, vmids, provider);
	}

	private WebResource service;

	@Override
	public void start() 
	{
		for (VMMetrics metric: VMMetrics.values())
		{
			// TODO subscription
			String host = "???????";
			String substring = 
					this.provider.getProviderAddress()+"/offers/subscribe/hub:offer:"+"#"+":"+host+metric;
			service.path(substring).put();
			
			// TODO handling
			String request = "";
			
			RESTMetricFetcher fetcher = new RESTMetricFetcher(request, 5000, service, BaseMessage.MessageType.APPLICATION);
			fetcher.start();
			this.pool.add(fetcher);
		}
		
	}
	
}
