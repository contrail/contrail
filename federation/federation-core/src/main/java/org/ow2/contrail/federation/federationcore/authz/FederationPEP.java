package org.ow2.contrail.federation.federationcore.authz;

import java.net.InetAddress;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.transport.http.SimpleHTTPServer;
import org.apache.log4j.Logger;
import org.ow2.contrail.authorization.cnr.pep.PEP;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;


/**
 * @author carlini
 * This class creates a PEP (i.e. a http server) intendend
 * to be common to the whole federation-core.
 * Host and port of the server are read from the configuration 
 * file FederatioCore.settings
 */

public class FederationPEP 
{
	private static PEP pep = null;
	private static Logger logger = Logger.getLogger(FederationPEP.class);
	private static String loggerTag = "[core.federationpep]";
	
	public static PEP getPEPInstance()
	{
		if (pep == null) {
			logger.info(loggerTag + " Creating PEP");
			String pdp_endpoint = null;
			String pep_port = null;
			try {
				pdp_endpoint = FederationProperties.getInstance().getProperty("pdp_path");
				pep_port = FederationProperties.getInstance().getProperty("pep-port"); 
				if (pdp_endpoint.equals("") || pep_port.equals(""))throw new Exception();
				logger.debug("Connecting to " + pdp_endpoint + " /" + pep_port);
			}
			catch (Exception e){
				pdp_endpoint = "http://146.48.96.76:8080/axis2/services/UconWs";
				pep_port = "3030";
				logger.debug("Connecting to " + pdp_endpoint + " port:" + pep_port);
			}
			String myIP = "";
			
			try { 
				//myIP = InetAddress.getLocalHost().getHostAddress();
				myIP = "146.48.82.58";
				myIP.trim();
			}
			catch (Exception e) { 
				myIP = "146.48.82.12";
				e.printStackTrace();
			}
			
			logger.debug("my IP address is " + myIP);
			try 
			{
				pep = new PEP(pdp_endpoint, pep_port, myIP);
			} 
			catch (AxisFault e) 
			{
				logger.error("Unable to setup PEP on federation");
				pep = null;
				e.printStackTrace();
			}
			
			logger.info("PEP initialized");
		}
		else
			logger.info(loggerTag + " already created");
		return pep;
	}

}
