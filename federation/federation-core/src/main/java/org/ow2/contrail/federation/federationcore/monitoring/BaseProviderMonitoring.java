package org.ow2.contrail.federation.federationcore.monitoring;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

public abstract class BaseProviderMonitoring extends BaseMonitoring 
{
	protected BaseProviderManager provider;

	public BaseProviderMonitoring(ProviderWatcherManager manager, BaseProviderManager provider) 
	{
		super(manager);
		this.provider = provider;
	}
}
