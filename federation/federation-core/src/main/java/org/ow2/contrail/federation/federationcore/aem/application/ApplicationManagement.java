package org.ow2.contrail.federation.federationcore.aem.application;

import java.io.StringWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONArray;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.implementation.ovf.File;
import org.ow2.contrail.common.implementation.ovf.SharedDisk;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.CloudProviderId;
import org.ow2.contrail.federation.federationcore.adapter.vin.VinNet;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistrator;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistry;
import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;
import org.ow2.contrail.federation.federationcore.aem.mapping.Mapping;
import org.ow2.contrail.federation.federationcore.aem.mapping.MappingPlan;
import org.ow2.contrail.federation.federationcore.aem.mapping.MappingPlanComparator;
import org.ow2.contrail.federation.federationcore.authz.AuthzHub;
import org.ow2.contrail.federation.federationcore.authz.FederationPEP;
import org.ow2.contrail.federation.federationcore.exception.ApplicationNotFoundException;
import org.ow2.contrail.federation.federationcore.exception.ApplicationOperationException;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.sla.soap.SoapSlamInterface;
import org.ow2.contrail.federation.federationcore.sla.soap.Statics;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;
import org.ow2.contrail.federation.federationcore.utils.OvfDom;
import org.ow2.contrail.federation.federationdb.jpa.dao.SLATemplateProposalDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserOvfDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserSLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.ApplicationSelectionCriterion;
import org.ow2.contrail.federation.federationdb.jpa.entities.Ovf;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplateProposal;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserOvf;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLA;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSelectionCriterion;
import org.ow2.contrail.federation.federationdb.jpa.entities.Vm;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.w3c.dom.Document;
//import org.ow2.contrail.federation.federationdb.jpa.entities.ApplicationSelectionCriterion;


@Scope("COMPOSITE")
@Service(IApplicationManagement.class)
public class ApplicationManagement implements IApplicationManagement{

	private HashMap<String, Collection<ReadyApplication>> userReadyApp = new HashMap<String, Collection<ReadyApplication>>();
	private HashMap<String, Collection<RunningApplication>> userRunningApp = new HashMap<String, Collection<RunningApplication>>();
	private HashMap<String, Collection<SuspendedApplication>> userSuspendedApp = new HashMap<String, Collection<SuspendedApplication>>();
	private static Logger logger = Logger.getLogger(ApplicationManagement.class);
	private static String loggerTag = "[core.aem] ";

	/**
	 * Add 2 Vin parameters to the OVF file as properties. The original OVF is not modified 
	 * if the interaction with the VIn controller cannot be performed.
	 * @return the string object representing the ovf
	 */
	@Deprecated
	String addVinParams2Ovf(String ovfOrig, ApplicationGraph<String, ContrailEdge> appDesc){
		String ovf = null;
		VirtualNetworkRegistrator vinApp1 = null;
		try {
			vinApp1 = new VirtualNetworkRegistrator(appDesc, "");
			List<String> s = vinApp1.getVinConnParams();

			OvfDom ovfd = new OvfDom(ovfOrig);
			if (ovfd != null){
				ovfd.addVINProperty("VIN_IBIS_SERVER", s.get(1));
				ovfd.addVINProperty("VIN_POOL_NAME", s.get(0));
				ovfd.addVINProperty("VIN_AGENT_CONTEXT", "vm");
				ovf = ovfd.toString();	
			}
		} catch (VinError e1) {
			ovf = ovfOrig; 			// the ovf is not modified if there is a VIN error
			logger.error(loggerTag +"Vin Error: "+ e1.getMessage());
			e1.printStackTrace();
		}
		return ovf;
	}

	private String startVin(String ovfOrig, String appUuid){
		String ovf = ovfOrig;
		VirtualNetworkRegistrator vinApp1 = null;
		ParserManager pm = null;
		ApplicationGraph<String, ContrailEdge> appDesc = null;
		try {
			appDesc = this.getApplicationGraph(pm, ovf);
		} catch (ApplicationOperationException e2) {
			e2.printStackTrace();
		}
		Collection<SharedDisk> cs = null;
		try {
			pm = new ParserManager(ovfOrig, false);
			cs = pm.getAllSharedDisks();
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.info(loggerTag + "Error parsing the ovf");
		}
		try {
			boolean vinStarted = false;
			if (cs != null ){
				Iterator<SharedDisk> i = cs.iterator();
				while (i.hasNext() && !vinStarted ){
					logger.info(loggerTag +" Finding a SharedDisk volume");
					SharedDisk s = i.next();
					System.out.println(s.getFormat());
					if (s.isVirtualNetworked()){
						File f = s.getFile();
						if (f != null && f.getUri()!= null){
							logger.info(loggerTag +" Initialising SharedDisk with Virtual Networks");
							String gafsUri = f.getUri();
							vinApp1 = new VirtualNetworkRegistrator(appDesc, appUuid, gafsUri);
							vinStarted = true;
						}
					}
				}
			}

			if (!vinStarted){
				logger.info(loggerTag +" Starting VIN");
				vinApp1 = new VirtualNetworkRegistrator(appDesc, appUuid);
			}
		} catch (VinError e1) {
			ovf = ovfOrig; 			// the ovf is not modified if there is a VIN error
			logger.error(loggerTag +"Vin Error: "+ e1.getMessage());
			e1.printStackTrace();
		}

		return ovf;
	}


	/**
	 * Orders the mapping plans.
	 */
	private MappingPlan[] rankMappings(Collection<MappingPlan> mappingPlans){
		if (mappingPlans == null) 
			return null;

		List<MappingPlan> listPlans = new ArrayList<MappingPlan>();
		for (MappingPlan mp : mappingPlans){
			listPlans.add(mp);
		}

		// getting provider with bigger weight (less load) maybe-to check
		Collections.sort(listPlans, new MappingPlanComparator());
		// for (MappingPlan mp : listPlans){ System.out.println(mp + " " + mp.getWeight()); }
		MappingPlan[] arrayPlans = listPlans.toArray(new MappingPlan[mappingPlans.size()]);
		return arrayPlans;
	}

	private ReadyApplication constructReadyApp(User username, Application app, MappingPlan[] mp) throws UnknownProviderErrorException{
		ReadyApplication result = null;
		result = new ReadyApplication(mp, null, app.getName(), app.getUuid());
		return result;
	}

	private ApplicationGraph<String, ContrailEdge> getApplicationGraph(ParserManager parser, String ovf) throws ApplicationOperationException{
		// ParserManager parser;
		try {
			parser = new ParserManager(ovf, false);
		}
		catch (Exception e){
			e.printStackTrace();
			logger.error("Application Management: application is NOT submitted");
			throw new ApplicationOperationException("Internal error while parsing the OVF document");
		}

		ApplicationGraph<String, ContrailEdge> appGraphDesc = GraphUtils.MakeGraph(-1, parser.getApplication());
		return appGraphDesc;
	}

	private ApplicationGraph<String, ContrailEdge> getApplicationGraph(String ovf) throws ApplicationOperationException{
		ParserManager parser;
		try {
			parser = new ParserManager(ovf, false);
		}
		catch (Exception e){
			String string = "";
			for (StackTraceElement s : e.getStackTrace())
				string += s.toString()+ "\n";
			logger.error(string);
			logger.error("Application Management: application is NOT submitted");
			throw new ApplicationOperationException("Internal error while parsing the OVF document");
		}

		ApplicationGraph<String, ContrailEdge> appGraphDesc = GraphUtils.MakeGraph(-1, parser.getApplication());
		return appGraphDesc;
	}
	

	private void authorization(User user, Application app) throws ApplicationOperationException{
		String appId = app.getUuid();
		AuthzHub ad = new AuthzHub();
		boolean status = ad.requestAuthz(user.getUuid(), appId);

		if (status == false) {
			String error = " Authorization error: user " + user.getUsername() + " not authorized to submit " + app.getName() + "\n";
			logger.error(loggerTag + error);
			throw new ApplicationOperationException(error);
		}
		else {
			logger.info(loggerTag + " Authorization: user " + user.getUsername() + " IS authorized to submit " + app.getName());
		}
	}
	
	/**
	 * 
	 * Method exposed by the federation-core for receiving requests of application submission. It analyzes the application for determining
	 * the suitable providers for deploying it. The set of providers is ordered by preferring the less loaded ones and it is returned to the caller as 
	 * an array. 
	 * Actually, this method relies on the server attributes stored in the federation-db.
	 *
	 * @param user
	 * @param app
	 * @return the ordered list of suitable providers for allocating the application.
	 * @throws UnknownProviderErrorException
	 * @throws ApplicationOperationException
	 */
	@Override public boolean submitApp(User user, Application app, UserSLATemplate userSLATemplate) throws ApplicationOperationException, UnknownProviderErrorException {
		boolean status = false;
		
		UserOvf appOvf = getUserOvfFromUri(app.getApplicationOvf());
		if(appOvf == null){
			logger.error(loggerTag + " error on retrieving ovf from application");
			throw new ApplicationOperationException("Error on retrieving ovf from application");
		}
		String ovf = appOvf.getContent();
		
		ApplicationGraph<String, ContrailEdge> appGraphDesc = this.getApplicationGraph(ovf);
		
		Collection<MappingPlan> mappingPlans = Mapping.computeMappingPlan(user, appGraphDesc);
		MappingPlan[] orderedMappingPlans = rankMappings(mappingPlans);
		try {
			for (int i=0; i<orderedMappingPlans.length; i++){
				logger.debug(loggerTag + "\t" + "Provider:  " + orderedMappingPlans[i].getAllocationProvider());
			}
		}
		catch (Exception e){
			orderedMappingPlans = new MappingPlan [0];
		}
		
		MappingPlan[] mps = orderedMappingPlans;
		
		Provider[] feasibleProviders = new Provider[mps.length];
		for (int i=0; i< mps.length; i++){
			Provider p = getProviderInTheDB(mps[i].getAllocationProvider());
			feasibleProviders[i] = p;
		}
		
		// putting authorization tryaccess here, as soon as possible in the execution flow
		authorization(user, app);
		
		SLATemplate offer = null;
		Integer slaTemplateId = null;
		String negotiationId = null;
		
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		
		// if the property is not defined it goes inside the control block
		if (!Boolean.parseBoolean(FederationProperties.getInstance().getProperty("sla-disabled"))){
			if (userSLATemplate != null){
				//if (userSLATemplate.getContent() == null)
				
				//creating FSlam for soap calls
				SoapSlamInterface fslam = new SoapSlamInterface();
				offer = new SLATemplate();
				
				try {
					
					//add provider list in SLATemplate
					SLATemplate slaT = fslam.parseSLATemplate(userSLATemplate.getContent());
					
					slaT = fslam.updateFslamEndpoint(slaT, user.getUuid());
					
					//adding userUuid and AppUuid to SlaTemplate for negotiation
					slaT = fslam.addUserUuid(slaT, user.getUuid());
					
					slaT = fslam.addAppUuid(slaT, app.getUuid());
					
					
					logger.info(loggerTag + "Adding provider to SLATemplate");
					slaT = fslam.addProviderList(slaT, feasibleProviders);
					//add criteria in SLATemplate
					CoreSelectionCriterion crit[] = null;
					try {
						List<ApplicationSelectionCriterion> appCriteria = app.getApplicationSelectionCriterionList();
						if (appCriteria == null || appCriteria.size() == 0) throw new ApplicationOperationException();
						crit = new CoreSelectionCriterion[appCriteria.size()];
						int i = 0;
						for (ApplicationSelectionCriterion u : appCriteria){
							crit[i] = new CoreSelectionCriterion(u);
							i++;
						}
						logger.info(loggerTag + "Using application defined criteria");
					}
					catch (ApplicationOperationException e){
						List<UserSelectionCriterion> userCriteria = user.getUserSelectionCriterionList();
						boolean chekUserCriteria = userCriteria == null;
						if (userCriteria != null){
							crit = new CoreSelectionCriterion[userCriteria.size()];
							int i = 0;
							for (UserSelectionCriterion u : userCriteria){
								crit[i] = new CoreSelectionCriterion(u);
								i++;
							}
							logger.debug(loggerTag + "No application specific criteria, using user defined criteria");
						}
						else {
							logger.debug(loggerTag + "No criteria found, neither for application nor for the user");
						}
					}
					
					if(crit.length != 0){
						slaT = fslam.addCriteria(slaT, crit);
						logger.debug("criteria found and inserted in SlaTemplate");
					} else logger.debug("criteria not found");
					
					userSLATemplate.setContent(fslam.renderSLATemplate(slaT));
					
					em.getTransaction().begin();
		            em.merge(userSLATemplate);
		            em.getTransaction().commit();
		            //for test purpose
		            //SoapSlamInterface.writeIntoFile(fslam.renderSLATemplate(slaT),"/var/log/contrail/slatForInitiateNegotiation.xml");
		            negotiationId = fslam.initiateNegotiation(slaT);
					SLATemplate[] offers = fslam.negotiate(slaT, negotiationId);

					/*
					 * EXAMPLE CODE WHEN THE FSLAM IS DISABLED
					negotiationId="asdad-asdads-asdasd-asdasd";
					SLATemplate[] offers = new SLATemplate[1];
					SLATemplate offerta = fslam.parseSLATemplate(Statics.serializeXml("src/main/resources/SlaTOfferFederationWithoutProviders.xml"));
					offers[0] = offerta;
					*/
					logger.info("Retrieve "+offers.length+" SlaTemplateProposals");
					for(int i=0; i<offers.length; i++){
						logger.debug("slaTemplateProposal #"+i+" creating...");
						SLATemplateProposal slaProposal = new SLATemplateProposal();
						slaProposal.setNegotiationId(negotiationId);
						slaProposal.setUserSLATemplate(userSLATemplate);
						slaProposal.setCreated(new Date(System.currentTimeMillis()));
						slaProposal.setContent(fslam.renderSLATemplate(offers[i]));
						logger.debug("slaTemplateProposal #"+i+" is storing in the db");
						em.getTransaction().begin();
			            em.persist(slaProposal);
			            em.getTransaction().commit();
						
					}
					
					try {
						em.getTransaction().begin();
						JSONObject appAttributes = new JSONObject(app.getAttributes());
						JSONArray negotiationIds = new JSONArray();
						negotiationIds.put(negotiationId);
						appAttributes.put("negotiationIds",negotiationIds );
						app.setAttributes(appAttributes.toString());
						app.setState("submitted");
						em.merge(app);
			            em.getTransaction().commit();
						
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				}
				catch (Exception e) {
					logger.info(loggerTag + "Error in negotiation process " + e);
					for( StackTraceElement elem : e.getStackTrace()){
						logger.info(loggerTag + "message: " + elem);
					}
					e.printStackTrace();
					offer = null;
					return status;
				}
			}
		}

		String userKey = user.getUuid();
		ReadyApplication ready = constructReadyApp(user, app, orderedMappingPlans);
		if (ready == null){
			logger.error(loggerTag + " Impossible to submit this application");
			throw new ApplicationOperationException("Impossible to submit this application");
		}
		
		if (!userReadyApp.containsKey(userKey)){
			logger.debug(loggerTag + " Create readyApps and add collection of the user");
			Collection<ReadyApplication> readyColl = new ArrayList<ReadyApplication>();
			readyColl.add(ready);
			userReadyApp.put(userKey, readyColl);
		}
		else {
			logger.debug(loggerTag + " Add collection of the user to ReadyApps");
			userReadyApp.get(userKey).add(ready);
			
		}
		status = true;
		em.close();
		return status;
	}

	private String retrieveSlaFromFile(){
		final String path= "src/main/resources/SlaTOfferFederationWithoutPrividers.xml";
		DocumentBuilderFactoryImpl dbf = new DocumentBuilderFactoryImpl();
		DocumentBuilder db;
		String slaTemplate;
		try {
			db = dbf.newDocumentBuilder();

			Document doc = db.parse(path);

			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", Integer.toString(2));

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc.getDocumentElement());

			trans.transform(source, result);
			slaTemplate = sw.toString();
		} catch (Exception e) {
			throw new Error("FILE NOT FUOND");
		}

		return slaTemplate;
	}

	private Provider getProviderInTheDB(CloudProviderId id){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		Query query = (Query) em.createNamedQuery("Provider.findByUuid").setParameter("uuid", id.toString());
		try {
			Provider p = (Provider) query.getSingleResult();
			return p;
		}
		catch (NoResultException e){
			logger.error(e.getMessage());
			return null;
		}
	}
	
	private Provider getProviderByInteger(int id){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		Query query = (Query) em.createNamedQuery("Provider.findByProviderId").setParameter("providerId", id);
		try {
			Provider p = (Provider) query.getSingleResult();
			return p;
		}
		catch (NoResultException e){
			logger.error(e.getMessage());
			return null;
		}
	}


	/**
	 * It deploys a ready application (i.e. application has been already submitted to the core and plans have been constructed) by
	 * trying different plans in an ordered way. 
	 * @param u
	 * @param a
	 * @param ovf
	 * @param ceeId
	 * @return an application deployed
	 */
	private RunningApplication deployReadyApp(User user, ReadyApplication ready, String ovf, String ceeId, CloudProviderId pID) throws ProviderComunicationException, UnknownProviderErrorException {
		RunningApplication running = null;

		// MappingPlan[] orderedMappingPlans = ready.getOrderedMappingPlans();
		MappingPlan[] orderedMappingPlans = new MappingPlan[1]; 
		MappingPlan m = ready.getMappingPlanForSingleProvider(pID);
		orderedMappingPlans[0] = m;
		logger.debug(loggerTag + "MappingPlan selected: " + orderedMappingPlans[0]);
		String appName = ready.getName();
		String appUuid = ready.getId();

		// Iteratively trying to deploy the application
		MappingPlan used = null;
		String submittedId = "-1";
		int orderOfPlan = 0;
		while (submittedId.equals("-1") && (orderOfPlan < orderedMappingPlans.length)){
			MappingPlan selectedPlan = orderedMappingPlans[orderOfPlan];
			try{
				submittedId = selectedPlan.deploy(user, ovf, appUuid, ceeId, pID);

				if (!submittedId.equals("-1")){
					logger.info(loggerTag + "Application: " + appName + "has been submitted by using plan \"" + selectedPlan);
					used =  selectedPlan;
				}
				orderOfPlan++;
			}
			catch (ProviderComunicationException e){
				logger.error(loggerTag + "Error while submitting the appliances with plan " + orderOfPlan + ", trying the next plan");
				e.printStackTrace();
				orderOfPlan++;
			} catch (UnknownProviderErrorException e) {
				logger.error(loggerTag + "Error while submitting the appliances with plan " + orderOfPlan + ", trying the next plan");
				e.printStackTrace();
				orderOfPlan++;
			}
		}
		
		boolean started = (used == null) ? false : true;
		if (started == true) {
			logger.info(loggerTag + "Application: " + ready.getName() + "has been deployed on provider " + used.getAllocationProvider());

			// remembering the map between the appName and the appDeploymentId (already done in previous hooks)
			/*
			VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
			registry.addApplication(user.getUsername(), ready.getName(), submittedId);
			 */

			userReadyApp.get(user.getUuid()).remove(ready);
			if (userReadyApp.get(user.getUuid()).isEmpty()){
				userReadyApp.remove(user.getUuid());
			}
			running = new RunningApplication(used, null, ready.getName(), ready.getId(), submittedId);
		}
		else {
			logger.info(loggerTag + "The start of the application " + ready.getName() + " cannot be completed");
		}
		return running;
	}

	/**
	 * This method verifies if the given application has been already submitted by the user and returns a ReadyApplication 
	 * object representing the application to start. 
	 * 
	 * @param u
	 * @param a
	 * @return the application ready to be started
	 * @throws ApplicationNotFoundException
	 */
	private ReadyApplication getSubmittedApplication(User u, Application a) throws ApplicationNotFoundException{
		String userKey = u.getUuid();
		logger.info(loggerTag + " searching for application submitted ("+a.getUuid()+") to start for user " + userKey);
		if (!userReadyApp.containsKey(userKey)){
			throw new ApplicationNotFoundException("Impossible to find some application submitted related to the user " + u.getUsername());
		}
		
		Collection<ReadyApplication> readyApp = userReadyApp.get(userKey);
		ReadyApplication readyToStart = null;
		// FIXME: change this routine!! : search for appUuid instead of name
		for (ReadyApplication ready : readyApp){
			logger.debug(loggerTag + "application submitted with id: " + ready.getId());
			if (ready.getId().equals(a.getUuid())) {
				readyToStart = ready;
				logger.info(loggerTag + " application \"" + a.getName() + "\" found");
				break;
			}
		}
		if (readyToStart == null) 
			throw new ApplicationNotFoundException("Impossible to start application \"" + a.getName() + "\", it has not been submitted.");
		return readyToStart;
	}
	
	/**
	 * Method exposed by the federation-core for conclude negotiation given a proposalID.
	 * The proposal that has to be use for conclude negotiation is retrieved from DB with the proposalID passed as parameter.
	 * The Proposal retrieved is used for conclude negotiation with FSLAM. 
	 * The SLA agreed returned by F-SLAM is stored in the UserSLA db table and its id is associated with the application that is ready to start. 
	 * 
	 * @param user
	 * @param app
	 * @param proposalId
	 * @return
	 */
	@Override
	public boolean createAgreement(User user, Application app, int proposalId){
		boolean status = false;
		SoapSlamInterface fslam = new SoapSlamInterface();
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		SLATemplateProposal proposal = null;
		
		//Retrive Proposal from DB
		try {
			logger.info(loggerTag + "Retrieve proposal choice by user");
			proposal = em.find(SLATemplateProposal.class, proposalId);
			logger.info("SLATemplateProposal id retrieved: " + proposal.getProposalId()+ " with negotiationId: " + proposal.getNegotiationId());
		} catch (Exception e) {
			logger.error(loggerTag + "Error on retrieve Proposal");
			e.printStackTrace();
			return false;
		}
		
		//Parse SLATemplateProposal and conclude negotiation (createAgreement call)
		if (!Boolean.parseBoolean(FederationProperties.getInstance().getProperty("sla-disabled"))){
			String sla = null;
			SLATemplate prop = null;
			try {
				//test log
				//logger.debug("retrieved proposal content:\n" + proposal.getContent());
				
				prop = fslam.parseSLATemplate(proposal.getContent());
			} catch (Exception e1) {
				logger.error(loggerTag + "Error on parse SLATemplateProposal");
				String stackTrace ="";
				for(StackTraceElement st: e1.getStackTrace())
					stackTrace += st +"\n";
				logger.error(stackTrace);
				return false;
			}
			//test log
			logger.debug("execute createAgreement on FSLAM");
			SLA slaAgreed = fslam.createAgreement(prop, proposal.getNegotiationId());
			try {
				
				sla = fslam.renderSLA(slaAgreed);
				
				
			} catch (Exception e1) {
				logger.error(loggerTag + "Error on parse SLA received from FSLAM");
				String stackTrace ="";
				for(StackTraceElement st: e1.getStackTrace())
					stackTrace += st +"\n";
				logger.error(stackTrace);
				return false;
			}
			
			// Store SLA agreed in DB
			UserSLA userSLA =null;
			try {
				logger.debug("serch for SLATemplate whit id: " + proposal.getUserSLATemplate().getSLATemplateId());
				UserSLATemplate slat = new UserSLATemplateDAO(em).findById(proposal.getUserSLATemplate().getSLATemplateId());
				
				// If slat existing in DB we have to save the SLA retrieved in DB
				if (slat != null){
					//logger.debug(loggerTag + "slatemplate retrieved not null");
					userSLA = new UserSLA();
					userSLA.setName(slat.getName());
					userSLA.setSla(slat.getName());
					userSLA.setSLATemplateId(slat);
					userSLA.setUserId(slat.getUserId());
					userSLA.setContent(sla);
				} else {
					logger.debug(loggerTag + "no slatemplate retrieved ");
				}
				logger.debug(loggerTag + "Storing UserSLA Object...");
				em.getTransaction().begin();
				em.persist(userSLA);
				em.getTransaction().commit();
				userSLA.getSLAId();
				logger.info(loggerTag + "Adding sla to UserSLAList...");
				List<UserSLA> l = user.getUserSLAList();
				if (l != null){
					user.getUserSLAList().add(userSLA);
				}
			} catch (Exception e){
				logger.error(loggerTag + " Error in SLA storing - DB error: SLA is not stored in the DB");
				/*
					String stackTrace ="";
					for(StackTraceElement st: e.getStackTrace())
						stackTrace += st +"\n";
					logger.error(stackTrace);
				 */
			}
			logger.info(loggerTag + "Updating Application Object with SLAID: "+ userSLA.getSLAId());
			// UPDATE Application Object with SLAID
			try {
				em.getTransaction().begin();
				JSONObject appAttributes = new JSONObject(app.getAttributes());
				appAttributes.put("slaId",userSLA.getSLAId());
				app.setAttributes(appAttributes.toString());
				em.merge(app);
	            em.getTransaction().commit();
			} catch (Exception e) {
				logger.error(loggerTag + " Error on Update Application Object");
				String stackTrace ="";
				for(StackTraceElement st: e.getStackTrace())
					stackTrace += st +"\n";
				logger.error(stackTrace);
				return false;
			}
			status = true;
			em.close();
		}
		return status;
	}

	/**
	 * Method exposed by the federation-core for receiving requests of starting an application. It requires that the application has been already submitted 
	 * by using the {@link submitApp} method. This method tries to allocate and start the application by using the mapping plans already computed 
	 * in the submission phase. The mapping plans are iteratively tried by respecting the computed order.
	 * @param user
	 * @param app
	 * @param proposal
	 * @throws ApplicationNotFoundException
	 * @throws ProviderComunicationException
	 * @throws UnknownProviderErrorException
	 * @throws ApplicationOperationException
	 */
	@Override public boolean startApp(User user, Application app) throws ApplicationNotFoundException, ProviderComunicationException, UnknownProviderErrorException, ApplicationOperationException {
		
		String negotiationId=null;
		boolean status = false;
		/* FOR TEST THE CREATE AGREEMENT IS CALLED IMPLICITY IN STARTAPP
		
		try {
			logger.info("Choice first Proposal");
			JSONObject appAttributes = new JSONObject(app.getAttributes());
			negotiationId = (String)((JSONArray) appAttributes.get("negotiationIds")).get(0);
			logger.debug("searching for proposal with negotioationId: "+negotiationId);
			SLATemplateProposal prop = new SLATemplateProposalDAO().findByNegotiationId(negotiationId).get(0);
			logger.debug(loggerTag + "found proposalId: "+ prop.getProposalId().toString());
			if (createAgreement(user,app,prop.getProposalId()) == false) throw new Exception();
			logger.debug(loggerTag + "exit create Agreement");
		} catch (Exception e){
			logger.error(loggerTag + "Error in calling the createAgreement");
			return status;
		}
		*/
		
		UserOvf appOvf = getUserOvfFromUri(app.getApplicationOvf());
		if(appOvf == null){
			logger.error(loggerTag + " error on retrieving ovf from application");
			throw new ApplicationOperationException("Error on retrieving ovf from application");
		}
		String ovf = appOvf.getContent();
		
		SLA slaAgreed = null;
		String ceeId = null;
		CloudProviderId cid = null;
		Provider p = null;
		SoapSlamInterface fslam = new SoapSlamInterface();
		
		// a ReadyApplication is a submitted one, it is retrieved from internal data structures
		ReadyApplication readyToStart = getSubmittedApplication(user, app);
		String userKey = user.getUuid();
		//retrieve SLAID from Application and retrieve UserSLA
		JSONObject appAttributes = null;
		int slaId;
		try {
			appAttributes = new JSONObject(app.getAttributes());
			slaId = (Integer) appAttributes.get("slaId");
		} catch (JSONException e) {
			logger.error(loggerTag + " Error in retrieve SLAID from Application Object");
			e.printStackTrace();
			return status;
		}
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		
		//Retrieve ProviderID and CeeID from SLA for starting application
		try {
			UserSLA userSLA = (UserSLA) em.createNamedQuery("UserSLA.findBySLAId").setParameter("sLAId", slaId).getSingleResult();
			if (userSLA != null){
				logger.debug(loggerTag + "find UserSLA with id: " +slaId);
				logger.debug(loggerTag + "SLAContent: " + userSLA.getContent());
				//logger.debug(loggerTag + "find UserSLA content: " +userSLA.getContent());
				slaAgreed = fslam.parseSLA(userSLA.getContent());
				STND property = new STND("ProviderUUid");
				int providerId= Integer.parseInt(slaAgreed.getPropertyValue(property));
				p = getProviderByInteger(providerId);
				cid = new CloudProviderId(p.getUuid());
				logger.info(loggerTag + " Retrieved ProviderID: " +providerId+" and Uuid: "+p.getUuid()+" from SLAagreed");
				try {
					STND property2 = new STND("CEE-ID");
					String[] ceeIdpath= slaAgreed.getPropertyValue(property2).split("/");
					ceeId = ceeIdpath[ceeIdpath.length - 1];
					logger.info(loggerTag + " Retrieved ceeID: " +ceeId+" from SLAagreed");
				} catch (Exception e){
					logger.info(loggerTag + " ceeID not retrived from SLA - VEPMOCK in P-SLAM");
					logger.warn(loggerTag + " setting standard ceeId = 1");
					ceeId = "1";
				}
			}
		} catch (Exception e){
			logger.error(loggerTag + " Error in SLA rendering - Cannot retrieve ProviderId and CeeId");
			String stackTrace ="";
			for(StackTraceElement st: e.getStackTrace())
				stackTrace += st +"\n";
			logger.error(stackTrace);
			return status;
		}
		
		// ApplicationGraph<String, ContrailEdge> appGraphDesc = this.getApplicationGraph(ovf);
		// the ovf is modified for the provisioning adding vin properties
		// String modOvf = this.addVinParams2Ovf(ovf, appGraphDesc);

		String appUuid = app.getUuid();
		String modOvf = startVin(ovf, appUuid);

		
		AuthzHub ad = new AuthzHub();
		String appFedId = app.getUuid();
		
		AemCallback call = new AemCallback(FederationPEP.getPEPInstance());
		boolean tryaccess = ad.startAccess(user.getUuid(), appFedId, call);
		if (!tryaccess) {
			logger.error(loggerTag + " user " + user.getUsername() + " not authorized to start app " + appFedId);
			return false;
		}
		
		
		logger.info(loggerTag + " user " + user.getUsername() + " IS authorized to start app " + appFedId );
		
		// the submitted application is deployed by trying its plans in an ordered way (a RunningApplication is a deployed one)
		RunningApplication running = deployReadyApp(user, readyToStart, modOvf, ceeId, cid);

		// the deployed application is started according to its mapping plan
		if (running == null) {
			logger.error("Application Management: application is NOT started");
			throw new ApplicationOperationException("Impossible to start this application");
		}
		else {
			MappingPlan mapping = running.getMappingPlan();
			mapping.start(user, running.getId(), running.getDeployId());
		}

		// the application is registered in the internal data structure
		if (!userRunningApp.containsKey(userKey)) {
			// we can assume that there is a single ovf with a single name (maybe not, check it)
			Collection<RunningApplication> runningColl = new ArrayList<RunningApplication>();
			runningColl.add(running);
			userRunningApp.put(userKey, runningColl);
		}
		else {
			userRunningApp.get(userKey).add(running);
		}
		
		List<Vm> vmList = createVmObjectsForApplication(app, p);
		
		try {
			em.getTransaction().begin();
			app.setState("running");
			app.setVmList(vmList);
			em.merge(app);
            em.getTransaction().commit();
            em.close();
		} catch (Exception e) {
			logger.error(loggerTag + " Error on Update Application Object State");
			String stackTrace ="";
			for(StackTraceElement st: e.getStackTrace())
				stackTrace += st +"\n";
			logger.error(stackTrace);
			return false;
		}
		
		// add the code for the monitoring
		// ArrayList<String> vmids = (ArrayList<String>) running.getVmId();
		
		status = true;
		return status;
	}

	/**
	 * Get UserOvf Object from Rest resource Uri
	 * @param userOvfUri
	 * @return UserOvf
	 */
	private UserOvf getUserOvfFromUri(String userOvfUri) {
		String[] ovfUrisplitted= userOvfUri.split("/");
		String ovfId = ovfUrisplitted[ovfUrisplitted.length - 1];
		UserOvf ovf = null;
		try {
			EntityManager em = PersistenceUtils.getInstance().getEntityManager();
			ovf = new UserOvfDAO(em).findById(Integer.parseInt(ovfId));
			logger.debug(loggerTag + "Retrieved ovf with id \"" + ovfId +"\" from application");
		} catch (Exception e) {
			logger.error(loggerTag + "Error on retrieve UserOvf: NOT FOUND");
			String stackTrace ="";
			for(StackTraceElement st: e.getStackTrace())
				stackTrace += st +"\n";
			logger.error(stackTrace);
		}
		return ovf;
	}

	private List<Vm> createVmObjectsForApplication(Application app, Provider prov) {
		String appUuid = app.getUuid();
		List<Vm> toReturn = new ArrayList<Vm>();
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> m = registry.getVmIdsByApplication(appUuid);
		List<VinNet> vinNets = registry.getNetIds();
		
		Set<String> vmIds = m.keySet();
		for (String vmId : vmIds){
			String address = registry.getVmAddressByVmId(vmId);
			String netAppId = null;
			String vinNetId = null;
			for (VinNet vNet : vinNets){
				if(vNet.getAppId() == app.getUuid()){
					netAppId = vNet.getAppNetId();
					vinNetId= vNet.getId().toString();
					logger.debug(loggerTag + "[createVm] NetAppId: " + vNet.getAppNetId());
					logger.debug(loggerTag + "[createVm] Id: " + vNet.getId());
				}
			}
			JSONObject attributes = new JSONObject();
			try {
				attributes.put("vmVinAddress", address+"");
				attributes.put("NetAppName", netAppId+"");
				attributes.put("VinNetId", vinNetId+"");
			} catch (JSONException e) {
				logger.error(loggerTag + "Error in creating address object");
			}
			logger.debug(loggerTag + address);
			logger.debug(loggerTag + attributes.toString());
			
			Vm vm = new Vm();
			vm.setName(vmId);
			vm.setProviderId(prov);
			vm.setApplicationId(app);
			vm.setAttributes(attributes.toString());
			
			try {
				EntityManager em = PersistenceUtils.getInstance().getEntityManager();
				em.getTransaction().begin();
				em.persist(vm);
				em.getTransaction().commit();
				em.close();
			} catch (Exception e) {
				logger.error(loggerTag + "Error in storing vm object");
			}
			
			toReturn.add(vm);
		}
		return toReturn;
	}

	@Override public boolean cancelSubmission(User user, Application app) throws ApplicationOperationException{
		String negotiationId=null;
		boolean status = false;
		try {
			logger.info("Retrieve Negotiation Id to cancel negotiation");
			JSONObject appAttributes = new JSONObject(app.getAttributes());
			negotiationId = (String)((JSONArray) appAttributes.get("negotiationIds")).get(0);
		} catch (Exception e){
			logger.error(loggerTag + "Error retireving nogotiationId from Application Object");
			return status;
		}
		
		SoapSlamInterface fslam = new SoapSlamInterface();
		try {
			status = fslam.cancelNegotiation(negotiationId);
		} catch (Exception e) {
			logger.error(loggerTag + "Error on cancelling the negotiation ");
			e.printStackTrace();
			throw new ApplicationOperationException("Impossible to cancel negotiation");
		}
		return status;
	}

	private boolean stopAppById(User user, String appId) throws ProviderComunicationException, UnknownProviderErrorException, ApplicationNotFoundException {
		logger.error(loggerTag + "Stopping application by id");
		String userKey ;
		try {
			userKey = user.getUuid();
		}
		catch (NullPointerException e){
			userKey = "caa6e102-8ff0-400f-a120-23149326a936";
		}
		RunningApplication running = getRunningApplication(userKey, appId);

		if (running == null) {
			throw new ApplicationNotFoundException("Impossible to find the application with id \"" + appId + "\" for user ");
		}

		String appKey = running.getId();
		MappingPlan selectedPlan = running.getMappingPlan();
		boolean stopped = selectedPlan.stop(user, appKey, running.getDeployId());
		if (stopped) {
			logger.info(loggerTag + "Application: " + running.getName() + " has been stopped ");
			
			//remove application from application running collection
			try{
				userRunningApp.get(userKey).remove(running);
				if (userRunningApp.get(userKey).isEmpty()) {
				userRunningApp.remove(userKey);
				}
			}
			catch(Exception e) {
				logger.error(loggerTag + "Application not found in runningApplication");
				return false;
			}
			//Change status of Application to "SUSPENDED" and add to userSuspendedApplication collection
			try{
				SuspendedApplication suspended = new SuspendedApplication(running.getMappingPlan(),null, running.getName(), running.getId(), running.getDeployId());

				if (!userRunningApp.containsKey(userKey)) {
					// we can assume that there is a single ovf with a single name (maybe not, check it)
					Collection<SuspendedApplication> suspendedColl = new ArrayList<SuspendedApplication>();
					suspendedColl.add(suspended);
					userSuspendedApp.put(userKey, suspendedColl);
				} else {
					userSuspendedApp.get(userKey).add(suspended);
				}
			}
			catch(Exception e){
				logger.error(loggerTag + "Error on add Application in suspended Applications collection");
				return false;
			}
			VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
			try {
				registry.removeApplication(userKey, appKey, running.getDeployId());
			}
			catch (NoResultException e){
				logger.error(loggerTag + "Internal vin registry has not found the app, maybe already deleted or different Fed-AP");
			}
		}
		else {
			logger.info(loggerTag + " the start of the application can not be completed because user has not been authorized.");
		}
		return stopped;
	}
	
	private RunningApplication getRunningApplication(String userKey, String appKey){
		logger.info("getRunningApplication: " + userKey + " " + appKey);
		Collection<RunningApplication> runningApp = userRunningApp.get(userKey);
		if (runningApp == null){
			logger.error("Something wrong");
			return null;
		}
		else {
			logger.info("Found " + runningApp.size());
		}
		RunningApplication runningToStop = null;
		for (RunningApplication running : runningApp) {
			if (running.getId().equals(appKey)) {
				runningToStop = running;
				logger.error("Application Management: application \"" + running.getName() + "\" found");
			}
		}
		return runningToStop;
	}
	
	/**
	 * Get Application suspended from userSuspendedApp Collection
	 * 
	 * @param userKey
	 * @param appKey
	 * @return
	 */
	private SuspendedApplication getSuspendedApplication(String userKey, String appKey){
		logger.info("getSuspendedApplication: " + userKey + " " + appKey);
		Collection<SuspendedApplication> suspendedApp = userSuspendedApp.get(userKey);
		if (suspendedApp == null){
			logger.error("Something wrong");
			return null;
		}
		else {
			logger.info("Found " + suspendedApp.size());
		}
		SuspendedApplication suspendedApplication = null;
		for (SuspendedApplication suspended : suspendedApp) {
			if (suspended.getId().equals(appKey)) {
				suspendedApplication = suspended;
				logger.error("Application Management: application \"" + suspended.getName() + "\" found");
			}
		}
		return suspendedApplication;
	}

	@Override
	public void stopApp(User user, Application app) throws ApplicationNotFoundException, ApplicationOperationException,
															ProviderComunicationException, UnknownProviderErrorException {
		String userKey = user.getUuid();
		String appKey = app.getUuid();
		
		logger.info(loggerTag + "Searching for application started to stop...");
		logger.info(loggerTag + "userKey "+userKey+" appKey "+appKey);
		if (!userRunningApp.containsKey(userKey)){
			logger.error(loggerTag + "None application found for user " + user.getUsername());
			throw new ApplicationNotFoundException("Impossible to find some application started related to the user " + user.getUsername());
		}
		
		boolean status = this.stopAppById(user, appKey);
		if (!status) {
			logger.error("Application Management: application is NOT stopped");
			throw new ApplicationOperationException("Impossible to stop this application");
		}
		logger.info("Application Management: application has been stopped");
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		try {
			em.getTransaction().begin();
			app.setState("stopped");
			em.merge(app);
            em.getTransaction().commit();
            em.close();
		} catch (Exception e) {
			logger.error(loggerTag + " Error on Update Application Object State");
			String stackTrace ="";
			for(StackTraceElement st: e.getStackTrace())
				stackTrace += st +"\n";
			logger.error(stackTrace);
		}
	}

	private boolean killAppById(User user, Application app) throws ProviderComunicationException, UnknownProviderErrorException, ApplicationNotFoundException, ApplicationOperationException {
		logger.info(loggerTag + "Killing application by id");
		String userKey = user.getUuid();
		String appKey = app.getUuid();
		logger.info(loggerTag + "Searching for application suspended kill...");
		SuspendedApplication suspended = getSuspendedApplication(userKey, appKey);
		
		if (suspended == null) {
			throw new ApplicationNotFoundException("Impossible to find the application with id \"" + app.getUuid() + "\" for user ");
		} else {
			logger.info(loggerTag + "found suspended application with id \"" + app.getUuid() + "\" for user ");
		}
		
		MappingPlan selectedPlan = suspended.getMappingPlan();
		boolean killed = selectedPlan.kill(user, appKey, suspended.getDeployId());
		if (killed) {
			logger.info(loggerTag + "Application: " + suspended.getName() + " has been killed ");
			
			userSuspendedApp.get(userKey).remove(suspended);
			if (userSuspendedApp.get(userKey).isEmpty()) {
				userSuspendedApp.remove(userKey);
			}
		} else {
			logger.error("Application Management: application is NOT killed");
			throw new ApplicationOperationException("Impossible to kill this application");
		}
		
		
		return true;
	}
	@Override
	public boolean killApp(User user, Application app)
			throws ApplicationNotFoundException, ProviderComunicationException, UnknownProviderErrorException, ApplicationOperationException {
		return this.killAppById(user, app);
	}

	
}
