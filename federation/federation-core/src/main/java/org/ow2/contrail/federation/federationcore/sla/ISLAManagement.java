package org.ow2.contrail.federation.federationcore.sla;

import org.ow2.contrail.federation.federationdb.jpa.entities.User;

public interface ISLAManagement 
{
	public void getSLATemplate(User user);

	public void negotiate(User user, String sla_neg_id, String sla_proposal);
	
	public void createAgreement(User user, String sla_neg_id, String sla_proposal);
}
