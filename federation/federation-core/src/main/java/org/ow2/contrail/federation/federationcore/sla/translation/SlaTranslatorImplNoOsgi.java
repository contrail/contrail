package org.ow2.contrail.federation.federationcore.sla.translation;

import org.slasoi.gslam.syntaxconverter.SLAParser;
import org.slasoi.gslam.syntaxconverter.SLARenderer;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOIRenderer;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.gslam.syntaxconverter.SLATemplateRenderer;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class SlaTranslatorImplNoOsgi implements SlaTranslator {

	@Override
	public SLATemplate parseSlaTemplate(String xmlSlat) throws Exception {
		return (SLATemplate) parser.parseTemplate(xmlSlat);
	}

	
	@Override
	public String renderSlaTemplate(SLATemplate slat) throws Exception {
		return renderer.renderSLATemplate(slat);
	}

	
	@Override
	public SLA parseSla(String xmlSla) throws Exception {
		return (SLA) slaParser.parseSLA(xmlSla);
	}

	
	@Override
	public String renderSla(SLA sla) throws Exception {
		return slaRenderer.renderSLA(sla);
	}


	public SlaTranslatorImplNoOsgi() {
		slaParser = new SLASOIParser();
		slaRenderer = new SLASOIRenderer();
		parser = new SLASOITemplateParser();
		renderer = new SLASOITemplateRenderer();
	}
	
	private static SLATemplateParser parser;
	private static SLAParser slaParser;
	private static SLARenderer slaRenderer;
	private static SLATemplateRenderer renderer;
	
}
