package org.ow2.contrail.federation.federationcore.test;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationcore.sla.soap.Statics;
import org.ow2.contrail.federation.federationdb.jpa.dao.ApplicationDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.SLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplate;
import org.ow2.contrail.federation.federationdb.jpa.entities.Server;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;


public class FedCoreClient {

	private static Logger logger = Logger.getLogger(FedCoreClient.class);
	private static int appId = 2;
	
	public static void main(String[] args) throws Exception {
		Client core = new Client();
		
		User user = new UserDAO().findByUsername("ContrailUser");
		logger.info(user.getUsername());
		
		
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		List slaTemplateList = (List) em.createNamedQuery("SLATemplate.findAll").getResultList();
		Iterator it = slaTemplateList.iterator();
		SLATemplate slaTemplateFed = null;
		while(it.hasNext()){
			SLATemplate slaTemplateFedTemp = ((SLATemplate)it.next());
			if(slaTemplateFedTemp.getName().equals("FED_SLA")){
				slaTemplateFed = slaTemplateFedTemp;
				logger.info("find SLATemplateFED :" + slaTemplateFed.getSlatId());
			}
		}
		if(slaTemplateFed == null) slaTemplateFed=registerSLATemplateFed();
		
		
		List userSlaTemplateList = (List) em.createNamedQuery("UserSLATemplate.findAll").getResultList();	
		UserSLATemplate userSlaTemplate = null;
		Iterator ituserSLAT = userSlaTemplateList.iterator(); 
		while(ituserSLAT.hasNext()){
			UserSLATemplate userSlaTemplateTemp = ((UserSLATemplate)ituserSLAT.next());
			if(userSlaTemplateTemp.getName().equals("FEDUSERSLA")){
				userSlaTemplate = userSlaTemplateTemp;
				logger.info("find UserSLATemplate {SLATemplateId: "+userSlaTemplate.getSLATemplateId()+" SlaTid: " + userSlaTemplate.getSlatId()+"}");
			}
		}
		if(userSlaTemplate == null) userSlaTemplate = registerUserSLATemplate(user,slaTemplateFed);
				
		

		Application app = null;
		
		if (!user.getApplicationList().isEmpty()){
			app=user.getApplicationList().get(0);

		} else {
			app = createApplication("ovf-2vms-vep2.0.ovf",appId, Integer.toString(appId),user,userSlaTemplate);
		}
		
		Provider p = createProviderObject("localhost", "fake4", 1, "http://192.168.57.12:8080/services/contrailNegotiation?wsdl", null);
		
		core.serviceProviderRegistration(core.provManagement, p);
		//core.populate_FedDb_With_Providers();
		logger.info("****************** EXECUTE SUBMIT APP ***********************");
		core.demoSubmitApplication(user, app);
		logger.info("****************** EXECUTE START APP ***********************");
		core.demoStartApplication(user, app);
		
	}

	private static SLATemplate registerSLATemplateFed(){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		SLATemplate slaTemplateFed = new SLATemplate();
		slaTemplateFed.setName("FED_SLA");
		slaTemplateFed.setUrl("/url");
		Provider providerFed = new ProviderDAO().findById(1);
		if (providerFed == null){
			logger.error("PROVIDER FEDERATION NOT FOUND");
			System.exit(1);
		}
		slaTemplateFed.setProviderId(providerFed);
		em.getTransaction().begin();
		em.persist(slaTemplateFed);
        em.getTransaction().commit();
        return slaTemplateFed;
	}
	
	private static UserSLATemplate registerUserSLATemplate(User user, SLATemplate slaTemplateFed){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		UserSLATemplate userSLATemplate= new UserSLATemplate();
		userSLATemplate.setContent(Statics.serializeXml("src/main/resources/SlaTOfferFederationWithoutProviders.xml"));
		userSLATemplate.setName("FEDUSERSLA");
		userSLATemplate.setUserId(user);
		userSLATemplate.setSlatId(slaTemplateFed);
		em.getTransaction().begin();
		em.persist(userSLATemplate);
        em.getTransaction().commit();
		return userSLATemplate;
	}
	
	private static Application createApplication(String ovfPath, int id, String uuid,User user, UserSLATemplate userSlaTemplate) throws Exception {
		ParserManager pm = null;;
		String ovf = null;
		try {
			ovf = Client.loadOvf(ovfPath);
			pm = new ParserManager(ovf, false);
		} catch (Exception e) {
			throw new Exception("Dove sai tu");
		}
		Application app = new Application();
		app.setApplicationId(id);
		app.setUuid(uuid);
		String appName = "";
		try {
			appName = pm.getApplication().getName();
		}
		catch (NullPointerException e){
			logger.error("Error on getting the name");
		}
		app.setName(appName);
		app.setApplicationOvf(ovf);
		
		app.setAttributes("{\"userSLATemplate\":\"/users/"+user.getUuid()+"/slats/"+userSlaTemplate.getSLATemplateId()+"\"}");
		
		List<Application> listApplicationUser = user.getApplicationList();
		listApplicationUser.add(app);
		user.setApplicationList(listApplicationUser);
		logger.info("storing Application");
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		em.getTransaction().begin();
		
		em.merge(user);
		em.persist(app);
        em.getTransaction().commit();
		return app;
	}
	
	private static Provider createProviderObject(String name, String uuid, int id, String uriPM, String serverAttr){
		
		List<Server> ls2 = new ArrayList<Server>();

		Provider provider = new Provider();
		provider.setName(name);
		provider.setProviderId(id);
		provider.setProviderUri(uriPM);
		provider.setTypeId(0);
		provider.setUserList(new ArrayList<User>());
		provider.setUuid(uuid);

		if (serverAttr != null){
			Server s2 = new Server();
			s2.setProviderId(provider);
			s2.setAttributes(serverAttr);
			ls2.add(s2);
			provider.setServerList(ls2);
		}

		return provider;
	}
}
