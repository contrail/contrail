package org.ow2.contrail.federation.federationcore.adapter.gafs;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * @author Gaetano F. Anastasi (ISTI-CNR)
 * 
 * GafsApp entity.
 */

@Entity
@NamedQueries({
	@NamedQuery(name = "GafsDIR.findAll", query = "SELECT t FROM GafsDIR t"),
	@NamedQuery(name = "GafsDIR.findDirById", query = "SELECT t.addressDIR FROM GafsDIR t WHERE t.volumeId = :volumeID")
})
public class GafsDIR {

	@Id
	private Long volumeId;
	
	private String addressDIR; // the ip address of the GAFS DIR server

	public Long getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(Long volumeId) {
		this.volumeId = volumeId;
	}

	public String getAddressDIR() {
		return addressDIR;
	}

	public void setAddressDIR(String addressDIR) {
		this.addressDIR = addressDIR;
	}
	

}
