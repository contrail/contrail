package org.ow2.contrail.federation.federationcore.adapter.vin;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
@NamedQueries({
    @NamedQuery(name = "VinHost.findAll", query = "SELECT t FROM VinHost t"),
   @NamedQuery(name = "VinHost.findIdByApplication", query = "SELECT t.vinHostId FROM VinHost t WHERE t.appId = :applicationID")})
public class VinHost {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String appId;
	// private String applianceId;
	private String vinHostId;
	
	public VinHost(String appID, String vinHostID){
		super();
		this.appId = appID;
		// this.applianceId = applID;
		this.vinHostId = vinHostID;
	}
	
	public VinHost(){}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getVinHostId() {
		return vinHostId;
	}

	public void setVinHostId(String vinHostId) {
		this.vinHostId = vinHostId;
	}
	
}