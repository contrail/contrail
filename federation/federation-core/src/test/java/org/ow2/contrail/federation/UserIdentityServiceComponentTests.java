package org.ow2.contrail.federation;

import static org.junit.Assert.fail;

import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserIdentityServiceComponentTests
{

	SCADomain scaDomain = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{

	}

	@Before
	public void setUp() throws Exception
	{
		// scaDomain= SCADomain.newInstance("", "src/main/resources/tuscany", "Federation.composite");
		// federation = scaDomain.getService(RegistrationAndLogin.class, "UserIdentityServiceComponent/RegistrationAndLogin");
	}

	@After
	public void tearDown() throws Exception
	{
		// scaDomain.close();
	}

	@Test
	public void testRegisterBase()
	{
		// FederationUserDescriptor result = federation.register("carlini", "pippo", "ricercatore", "spezzini");
		// assertNotNull(result);

		// boolean result = federation.register("carlini", "pippo", "ricercatore", "spezzini");
		// assertTrue(result);
	}

	@Test
	public void testRegisterCheckForDuplicates()
	{
		/*
		 * FederationUserDescriptor result = federation.register("carlini", "pippo", "ricercatore", "spezzini"); assertNotNull(result);
		 * FederationUserDescriptor result2 = federation.register("carlini", "pippo", "ricercatore", "spezzini"); assertNotNull(result2);
		 */

		// boolean result = federation.register("carlini", "pippo", "ricercatore", "spezzini");
		// assertTrue(result);

		// boolean result2 = federation.register("carlini", "pippo", "ricercatore", "spezzini");
		// assertTrue(result2);
	}

	@Test
	public void testRegisterCheckCredentialsValidity()
	{
		fail("Not Yet Implemented");
	}

}
