#!/bin/bash

if ! [ $# -eq 3 ]; then
	echo "Missing Parameters: use with uuid ovfFilename and slatFilename"
	exit 0
fi
userUuid=$1
ovfName='OvfDemo'
ovfContent=`cat $2`
providerId=1
slatName='slaTDemo'
slatUrl='/federation/slat'
slatContent=`cat $3`


mysql -u contrail -pcontrail contrail<<EOF
insert into Ovf (name, providerId) values('$ovfName', '$providerId');
EOF
mysql -u contrail -pcontrail contrail<<EOF
insert into SLATemplate (name, url, providerId) values('$slatName', '$slatUrl',1);
EOF

ovfSel=`mysql -u contrail contrail -pcontrail -e "select ovfId from Ovf where name='$ovfName'"`
ovfId=$(echo $ovfSel|awk '{print $2}')
userSel=`mysql -u contrail contrail -pcontrail -e "select userId from User where UUID='$userUuid'"`
userId=$(echo $userSel|awk '{print $2}')
mysql -u contrail -pcontrail contrail<<EOF
insert into UserOvf (name, providerOvfId, content, userId) values('$ovfName', '$ovfId', '$ovfContent', '$userId');
EOF
userOvfSel=`mysql -u contrail contrail -pcontrail -e "select ovfId from UserOvf where name='$ovfName'"`
userOvfId=$(echo $userOvfSel|awk '{print $2}')
#echo "*************************************************************************"
#echo "USER OVF REGISTERED:  /users/$userUuid/ovfs/$userOvfId"
#echo "*************************************************************************"
slatSel=`mysql -u contrail contrail -pcontrail -e "select slatId from SLATemplate where name='$slatName'"`
slatId=$(echo $slatSel|awk '{print $2}')
slatUrl=`echo /federation/stats/$slatId` 
mysql -u contrail -pcontrail contrail<<EOF
insert into UserSLATemplate (name, url, content, userId, slatId) values('$slatName', '$slatUrl', '$slatContent', '$userId', '$slatId');
EOF

userSLATSel=`mysql -u contrail contrail -pcontrail -e "select SLATemplateId from UserSLATemplate where name='$slatName'"`
userSlatId=$(echo $userSLATSel|awk '{print $2}')
#echo "*************************************************************************"
#echo "USER SLATemplate REGISTERED:  /users/$userUuid/slats/$userSlatId"
#echo "*************************************************************************"
#echo
echo "/users/$userUuid/ovfs/$userOvfId"
echo "/users/$userUuid/slats/$userSlatId"
