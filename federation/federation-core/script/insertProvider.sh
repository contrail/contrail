#!/bin/bash

function valid_ip {
    local  ipPort=$1
    local  stat=1
    if [[ $ipPort =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\:[0-9]+$ ]]; then
        OIFS=$IFS
	IFS=':'
	Ipsplitted=($ipPort)
	IFS=$OIFS
	ip=${Ipsplitted[0]}
	port=${Ipsplitted[1]}
	OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}


if ! [ $# -eq 2 ]; then
	echo "Missing Parameters: use with providerUri providerPSLAMuri"
	exit 0
fi

if ! valid_ip $1; then 
	echo $1 is not valid IP address; 
	exit 0
fi
if ! valid_ip $2; then 
	echo $2 is not valid IP address; 
	exit 0
fi

providerUri=$1
providerPSLAMuri=$2



uuid=`uuidgen`
suffix=${uuid:0:5}
name="ProviderTest-$suffix"
attributes="{\"p-slam-url\":\"http://$providerPSLAMuri/services/contrailNegotiation?wsdl\"}"
providerUri="http://$providerUri"
mysql -u contrail -pcontrail contrail<<EOF
insert into Provider (uuid, name, attributes, providerUri, typeId) values('$uuid', '$name', '$attributes', '$providerUri', 0);
EOF
providerSel=`mysql -u contrail contrail -pcontrail -e "select providerId from Provider where uuid='$uuid'"`
providerId=$(echo $providerSel|awk '{print $2}')

echo "*************************************************************************"
echo "Provider inserted in Federation-db"
echo Id = $providerId
echo uuid = $uuid
echo name = $name
echo uri = $providerUri
echo p-slam-uri= http://$providerPSLAMuri/services/contrailNegotiation?wsdl
echo "*************************************************************************"



