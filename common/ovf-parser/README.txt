				-------------------------------------------
						       OVF Parser
				-------------------------------------------
						       2011-12-09

Support

  For any question about the integration send an email to:
  
  * iacoperi@gmail.com


Definition

  To include OVF-parser as dependency of one maven project you need to be sure to add a "dependency
  tag" in your pom.xml of your project:

  ---------------------------------------------------------------------------------------
  <dependency>
    <groupId>org.ow2.contrail.common</groupId>
    <artifactId>ovf-parser</artifactId>
    <version>0.1-SNAPSHOT</version> 
    <scope>compile</scope>
  </dependency>
  ---------------------------------------------------------------------------------------
  
  Maven will retrieve the ovf-parser's pom from the remote artifact downloading it in your 
  local repository.


Development

  The easiest way to import the project into Eclipse is to use m2eclipse.
  
  IMPORTANT: To correctly setup the project environment in eclipse first you have to compile the 
  maven project and after build the ".project" resource of ovf-parser. See "Compiling" section.


Dependencies

  The OVF parser depends from several libraries:
  
  * junit 4.10 

  The dependencies are automatically resolved by Maven compiling the code.

 
Sample ovf

  Resides under src/main/java/resources.


Parent

  OVF parser is compilable as separated maven project. Its location is under:
 
  - trunk/common/
  
  and its direclty parent is located under:

  - trunk/common/pom.xml

  The parent TAG in its own pom.xml is:

  ---------------------------------------------------------------------------------------
  <parent>
    <groupId>org.ow2.contrail</groupId>
    <artifactId>contrail</artifactId>
    <version>0.2-SNAPSHOT</version>
  </parent>  
  ---------------------------------------------------------------------------------------

  
Compiling
  
  To simply compile the maven project go in ovf-parser folder and type:

  ---------------------------------------------------------------------------------------
  $ mvn clean compile
  ---------------------------------------------------------------------------------------

  If you also need to create the ".project" resource for eclipse, after compiled with maven, type:

  ---------------------------------------------------------------------------------------
  $ mvn clean eclipse:eclipse
  ---------------------------------------------------------------------------------------


Running application

  TO DO


Running unit tests

  TO DO


Generating documentation

  TO DO