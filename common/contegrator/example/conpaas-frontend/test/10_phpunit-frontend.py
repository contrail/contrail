# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to
#                 host names
# - ct_nodeman:   a NodeManager instance that can be used to issue ssh commands
#                 to the nodes

def test_conpaas_frontend(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    try:
        ct_nodeman.sshrun("root", head, "mysql -u root -e \"create database conpaas_test character set = 'utf8';\"")
        ct_nodeman.sshrun("root", head, "mysql -u root -e \"grant all on conpaas_test.* to 'test'@'localhost' identified by 'test';\"")
        ct_nodeman.sshrun("root", head, "mysql -u test --password=test </opt/conpaas/frontend/scripts/frontend-db.sql")
        ct_nodeman.sshrun("root", head, "php /opt/conpaas/frontend/tests/run.php")
    finally:
        pass
