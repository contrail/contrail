# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to
#                 host names
# - ct_nodeman:   a NodeManager instance that can be used to issue ssh commands
#                 to the nodes

# instal mysql server, outside of chroot env
def test_01_install(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    try:
        ct_nodeman.sshrun("root", head, "apt-get update")
        ct_nodeman.sshrun("root", head, "DEBIAN_FRONTEND=noninteractive apt-get -q -y --force-yes install mysql-server ")
        ct_nodeman.sshrun("root", head, "cp -f /etc/init/mysql.conf.conpaas-frontend /etc/init/mysql.conf")
        ct_nodeman.sshrun("root", head, "service mysql stop")
        ct_nodeman.sshrun("root", head, "service mysql start")
    finally:
        pass
