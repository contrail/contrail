# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

def test_stress(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    try:
        ct_nodeman.sshrun("root", head, "sleep 30")
        ct_nodeman.sshrun("root", head, "service opennebula restart")
        ct_nodeman.sshrun("root", head, "sleep 30")
        # ct_nodeman.sshrun("root", head, "onehost delete " + head + " im_kvm vmm_kvm tm_nfs")
        # ct_nodeman.sshrun("root", head, "onehost delete " + worker + " im_kvm vmm_kvm tm_nfs")
        ct_nodeman.sshrun("root", head, "onehost create " + head + " im_kvm vmm_kvm tm_nfs")
        ct_nodeman.sshrun("root", head, "onehost create " + worker + " im_kvm vmm_kvm tm_nfs")
        ct_nodeman.sshrun("root", head, "onevnet publish 0")
        ct_nodeman.sshrun("root", head, "/usr/bin/one-test-run.sh")
        ct_nodeman.sshrun("root", head, "/srv/contegrator-test/wait-before-shutdown.sh 300")
    finally:
        pass
