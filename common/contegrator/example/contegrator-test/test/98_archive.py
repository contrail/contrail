# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

import subprocess

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_archive(ct_node_list, ct_nodeman):
    global jenkins_archive
    worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    try:
        ct_nodeman.getfile(worker, "/var/log/syslog", "./archive/worker/syslog" )
        ct_nodeman.getfile(worker, "/root/timestamp.txt", "./archive/worker/" )
        ct_nodeman.getfile(worker, "/root/bigfile-1", "./archive/worker/" )
        ct_nodeman.getfile(worker, "/root/smallfile-1", "./archive/worker/" )
        ct_nodeman.getfile(worker, "/var/log/dmesg", "./archive/worker/" )
        ct_nodeman.getfile(worker, "/var/log/contrail/", "./archive/worker/var-log-contrail-1/" )
        ct_nodeman.getfile(worker, "/var/log/contrail/", "./archive/worker/var/log/" ) # will create subdir
    except:
        print "Test failed"
        raise
    finally:
        pass

#

