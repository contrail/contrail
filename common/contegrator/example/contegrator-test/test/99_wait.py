# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_99_wait(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    try:
        ct_nodeman.testrun("root", worker, "/srv/contegrator-test/wait-before-shutdown.sh 1800")
        # ct_nodeman.sshrun("root", worker, "/usr/bin/one-test-run.sh")
    except:
        print "Test failed"
        raise
    finally:
        pass

#

