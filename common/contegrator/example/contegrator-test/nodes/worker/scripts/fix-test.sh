#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# rm /var/lib/one-nfsshare/remotes/im/kvm.d/freespace.sh

echo "VNFS_NAME = $VNFS_NAME"
HEAD_VNFS_NAME=`echo $VNFS_NAME | sed -e 's/worker$/head/'`
cp /etc/fstab /etc/fstab.orig
# sed -e "s/one-2.2-head-jc.aufs/$HEAD_VNFS_NAME/g" </etc/fstab.orig >/etc/fstab
[ ! -z "$BASE_VNFS_NAME_head" ] && sed -e "s/$BASE_VNFS_NAME_head/$VNFS_NAME_head/g" </etc/fstab.orig >/etc/fstab

# Check env variables
set +e
echo "NODE_ID = $NODE_ID"
echo "HOST_NAME = $HOST_NAME"
echo "VNFS_NAME = $VNFS_NAME"
echo "BASE_VNFS_NAME = $BASE_VNFS_NAME"
#
echo "NODE_ID_worker = $NODE_ID_worker"
echo "HOST_NAME_worker = $HOST_NAME_worker"
echo "VNFS_NAME_worker = $VNFS_NAME_worker"
echo "BASE_VNFS_NAME_worker = $BASE_VNFS_NAME_worker"


