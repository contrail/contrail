testbed_node_list = [
    "n0006",
    "n0007",
    "n0008",
]

node_passwords = {
    "root@n0004": "root",
    "root@n0005": "root",
    "root@n0006": "root",
    "root@n0007": "root",
    "root@n0008": "root",
}

node_ssh_keys = [
    "/var/lib/jenkins/.ssh/id_rsa_jenkins_perceus_xc1",
]

ct_perceus_util_node = "perceus"
ct_perceus_util_user = "jenkins"

# set to true if test nodes are set-up already and all you want to do
# is run the test code. useful for debugging to avoid lengthy staging
# of test nodes on each contegrator run.
ct_prestaged = False
#ct_prestaged = True
# if you set above to true, make sure you fix the map below,
# mapping node names (node dir names) to the corresponding hosts
ct_prestaged_nodes = {
    "head": "n0007",
    "worker": "n0007",
}
