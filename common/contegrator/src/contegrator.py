'''
contegrator.py

integration test driver for Contrail
- manage VNFS capsules for test machines
- provision and boot test machines via perceus
- run test cases

@author: jaka
'''

import sys
import logging
import os
import types
import traceback
import argparse
import datetime

from image.imager import Imager
from nodes.nodeman import NodeMan, Node, ResourceError

from testbedconfig import testbed_node_list, ct_prestaged, ct_prestaged_nodes
from conf import ct_conf

usage = "contegrator <release-tag> <test-suite-path> <vnfsname-tag>"

ct_node_list = {}
ct_nodeman = None
tc_fail_total = -1
tc_failed_name = []
cmdline_args = None
ct_failed_test_wait_count = 0

def prepare_nodes(release_tag, nodes_dir, test_dir, vnfsname_tag):
    global ct_node_list, ct_nodeman    
    print "Preparing images for release ", release_tag
    print "from configuration directory", nodes_dir

    imager = Imager(release_tag, nodes_dir, vnfsname_tag, testbed_node_list)
    images = imager.prepare_nodes(ct_prestaged)
    if images == None:
        return False, images, imager
    for image in images:
        if images[image]:
            print "Node '", image, "' image name", images[image].image_name, "image type", images[image].get_image_type()
    print "Images prepared (stage 1)."


    print "Acquiring physical nodes and preparing images (stage 2)"
    nodeman = NodeMan(testbed_node_list)
    ct_nodeman = nodeman
    if not ct_prestaged:
        try:
            nodes = nodeman.add_phy_nodes(images)

            images = imager.prepare_nodes_run_scripts(images, nodes, ct_prestaged)
            if images == None:
                return False, images, imager
            for image in images:
                if images[image]:
                    print "Node '", image, "' image name", images[image].image_name, "image type", images[image].get_image_type(), "run_script"
            print "Images prepared (stage 2)."
                
            print "Booting images ..."
            if not boot_nodes(nodeman, imager, nodes, ct_node_list):
                print "Error: nodes not booted in time."
                if not cmdline_args.leave_nodes_alive:
                    imager.purge_nodes(images, ct_prestaged)
                images = {}
                return False, images, imager
        except ResourceError as ex:
            logging.exception("Failed to add physical node.")
            print "No more physical nodes to work with."
            if not cmdline_args.leave_nodes_alive:
                imager.purge_nodes(images, ct_prestaged)
            images = {}
            return False, images, imager
        except Exception as ex:
            logging.exception(ex)
            if not cmdline_args.leave_nodes_alive:
                imager.purge_nodes(images, ct_prestaged)
            images = {}
            return False, images, imager
    else:
        ct_node_list = ct_prestaged_nodes

    for node in ct_node_list:
        if node:
            print "Node", node, "running on host", ct_node_list[node]

    print "Nodes are up and running."
    return True, images, imager

def boot_nodes(nodeman, imager, nodes, ct_node_list):
    #  release_tag, nodes_dir, test_dir, vnfsname_tag):
    print "Booting images ..."
    for image in nodes:
        node = nodes[image]
        if node:
            nodeman.reboot_phy_node(node)
            ct_node_list[image] = node.get_host_name() 
        else:
            # node was listed in ignore-nodes
            ct_node_list[image] = None
    # If one node does not boot in 300 sec, reboot only that node.
    loop_retry = 3  
    while loop_retry > 0:
        status, not_booted_nodes = nodeman.wait_for_phy_nodes(10, 30)
        loop_retry -= 1
        if status:
            print "Nodes booted."
            return True, ct_node_list
        else:
            print "Warning: nodes not booted in time, failed nodes count is : " + str(len(not_booted_nodes)) + "."
            for node in not_booted_nodes:
                print "    not_booted_node: " + str(node)
            for node in not_booted_nodes:
                nodeman.reboot_phy_node(node)
        
    print "Error: nodes not booted in time."
    return False

def run_tests():
    global tc_fail_total
    global tc_failed_name
    tc_fail_total = 0
    # at this point, test scripts have hostnames of nodes (named after their respective directories)
    # readily available in dict ct_node_list, key is node name (aka node dir name), value is
    # host name

    # NOTE: we currently only support test cases coded in python
    tmp_test_cases = os.listdir(test_dir)
    test_cases = []
    for tc in tmp_test_cases:
        if tc.endswith(".py"):
            tc = tc.replace(".py", "")
            test_cases.append(tc)
    sys.path.insert(0, test_dir)
    test_cases.sort()
    for tc in test_cases:
        print "Executing test module:", tc
        # import test case as a module
        tc_pass = 0
        tc_fail = 0
        tc_total = 0
        try:
            tc_mod = __import__(tc, globals(), locals(), ['*'])
        except:
            print "Test case __import__", tc, "failed."
            traceback.print_exc(None, sys.stdout)
            tc_fail += 1
            tc_total += 1
            continue
        fnc_names = dir(tc_mod)
        fnc_names.sort()
        for i in fnc_names:
            if i.startswith("test_"):
                f = tc_mod.__dict__.get(i)
                if isinstance(f, types.FunctionType):
                    tc_name = tc + "." + i
                    print "Running test case", tc_name
                    tc_total += 1
                    try:
                        f(ct_node_list, ct_nodeman)
                        print "Test case", tc_name, "passed."
                        tc_pass += 1
                    except:
                        print "Test case", tc_name, "failed."
                        traceback.print_exc(None, sys.stdout)
                        tc_fail += 1
                        tc_fail_total += 1
                        tc_failed_name.append(tc_name)
                        try:
                            wait_failed_test(ct_node_list, ct_nodeman)
                        except:
                            print 'neki sem zaj..'
                            traceback.print_exc(None, sys.stdout)
        print "Passed / Failed / Total:", tc_pass, "/", tc_fail, "/", tc_total
    
# after test fails, offer option to manually fix (typo in path/filename etc)
def wait_failed_test(ct_node_list, ct_nodeman):
    global ct_failed_test_wait_count;
    tf_conf = ct_conf['test_fail']
    wait_time = tf_conf['wait_time']
    count_limit = tf_conf['count_limit']
    wait_file = tf_conf['wait_file']
    wait_nodes = tf_conf['wait_nodes']
    print 'wait_failed_test, AAA ct_failed_test_wait_count ' + str(ct_failed_test_wait_count) + '/' + str(count_limit)
    if wait_time <= 0 or ct_failed_test_wait_count >= count_limit:
        return
    # get first valid node
    for node_key in ct_node_list:
        if ct_node_list[node_key]:
            node = ct_node_list[node_key]
            break
    else:
        print 'No valid node found in ' + str(ct_node_list)
        return
    print 'wait_failed_test, using node ' + node
    # if 
    rv = ct_nodeman.sshrun("root", node, "/srv/contegrator-test/common/wait-failed-test.sh " + str(wait_time) + " " + wait_file)
    print 'wait_failed_test, sshrun -> ' + str(rv)
    if rv.return_code == 0:
        # user did interact
        pass
    else:
        ct_failed_test_wait_count += 1
    print 'wait_failed_test, BBB ct_failed_test_wait_count ' + str(ct_failed_test_wait_count) + '/' + str(count_limit)

   
if __name__ == '__main__':
    # takes three arguments:
    # 1. release tag to test (should equal the tag used by bamboo to
    # store binary packages)
    # 2. path to test suite dir (containing path to nodes and test directories)
    # 3. vnfsname tag (job name), to generate unique/sefl-describing VNFS names
    parser = argparse.ArgumentParser(description="Contegrator integration test")
    parser.add_argument("release_tag", type=str, help="release tag")
    parser.add_argument("testsuite_dir", type=str, help="path to test suite")
    parser.add_argument("vnfsname_tag", type=str, help="vnfsname tag (job name)")
    parser.add_argument("--leave-nodes-alive", dest='leave_nodes_alive', action="store_true",
                        help = "Do not shutdown nodes when done" )
    cmdline_args = parser.parse_args()
    print cmdline_args
    
    release_tag = cmdline_args.release_tag
    nodes_dir = os.path.join(cmdline_args.testsuite_dir, "nodes")
    test_dir = os.path.join(cmdline_args.testsuite_dir, "test")
    vnfsname_tag = cmdline_args.vnfsname_tag
    
    logging.basicConfig(level = logging.DEBUG)
  
    logging.debug("contegrator starting.")
    status, images, imager = prepare_nodes(release_tag, nodes_dir, test_dir, vnfsname_tag)

    if status:
        run_tests()

    if not ct_prestaged and not cmdline_args.leave_nodes_alive:
        # finally, shut down nodes and purge test vnfses
        print "Shutting nodes down ..."
        ct_nodeman.shutdown_phy_nodes_halt()
        print "Nodes shut down."
        print "Purging test VNFS capsules ..."
        imager.purge_nodes(images, ct_prestaged)
        print "VNFS capsules purged."
    else:
        print "Leaving nodes alive..."

    print "Have a nice day."
    now = datetime.datetime.now()
    print "Finished at: " + now.strftime("%Y-%m-%d %H:%M:%S")
    if tc_fail_total != 0:
        print "Failed test case count: ", tc_fail_total
        for tc_name in tc_failed_name:
            print "    " + tc_name
        sys.exit(1)
