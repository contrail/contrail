'''
Created on Aug 30, 2011

@author: jaka
'''

import subprocess
from subprocess import CalledProcessError
from fabric.api import env
from fabric.operations import run, _AttributeString

import re
import os
import logging
import string

from testbedconfig import node_ssh_keys, ct_perceus_util_node, ct_perceus_util_user

class Perceus(object):

    _debug = True
    
    def __init__(self, available_nodes = None,
                 perceus_cmd = "perceus"):
        self._sudo_cmd = 'sudo'
        self._perceus_cmd = perceus_cmd
        self._available_nodes = available_nodes
        if self._available_nodes is None:
            self._available_nodes = self.list_nodes()
        self._allocated_nodes = []

    def setup_fabric_env(self):
        env.shell = "/bin/bash -l -i -c"
        env.key_filename = node_ssh_keys
        env.warn_only = True
        env.disable_known_hosts = True

    def _run_via_ssh(self, node, cmd, **kwargs):
        args = { "user": ct_perceus_util_user, #"root",
                 "timeout": 30 }
        args.update(kwargs)
        self.setup_fabric_env()
        env.host_string = args["user"] + "@" + node
        cmd_str = " ".join(cmd)
        logging.debug("Running [" + cmd_str + "] on " + env.host_string)
        try:
            rv = run(cmd_str)
        except SystemExit as e:
            rv = _AttributeString("")
            rv.succeeded = False
            rv.failed = True
            rv.return_code = -1
        logging.debug("Success:     " + str(rv.succeeded))
        logging.debug("Return code: " + str(rv.return_code))
        logging.debug("Output:      " + rv)
        return rv

    def _log_cmd(self, cmd_list):
        logging.debug("Command: " + " ".join(cmd_list))

    def _pcmd(self, args):
        cmd = [self._sudo_cmd, self._perceus_cmd, "-y"]
        if not Perceus._debug:
            cmd.append("-q")
        cmd.extend(args)
        self._log_cmd(cmd)
        return cmd

    # try to refresh NFS mount, before using it locally
    def _refresh_nfs(self, vnfs=None):
        try:
            subprocess.call(["ls", "/usr/var/lib/perceus/"], stdout=subprocess.PIPE)
            subprocess.call(["ls", "/usr/var/lib/perceus/vnfs/"], stdout=subprocess.PIPE)
            if vnfs:
                subprocess.call(["ls", "/usr/var/lib/perceus/vnfs/" + vnfs], stdout=subprocess.PIPE)
                subprocess.call(["ls", "/usr/var/lib/perceus/vnfs/" + vnfs + "/rootfs/"], stdout=subprocess.PIPE)
        except:
            pass

    def clone_vnfs(self, src_vnfs, tgt_vnfs):
        rv = self._run_via_ssh("perceus", self._pcmd(["vnfs", "clone", src_vnfs, tgt_vnfs]))
        self._refresh_nfs(src_vnfs)
        if rv.failed:
            logging.error("Failed to clone VNFS " + src_vnfs + " " + tgt_vnfs)
            return False
        self._refresh_nfs(tgt_vnfs)
        return True

    def mount_vnfs(self, vnfs):
        self._refresh_nfs(vnfs)
        try:
            output = subprocess.check_output(self._pcmd(["vnfs", "mount", vnfs]))
            logging.debug("--- output start ---")
            logging.debug(output)
            logging.debug("--- output end ---")
            # The VNFS can be found at: /mnt/test-vnfs-X-node-1
            match = re.search("The VNFS can be found at: (\S+)", output)
            logging.debug("Match: " + str(match))
            if match is None:
                return None
            # fix /etc/mtab
            subprocess.check_call("sudo chroot /mnt/" + vnfs + " /bin/sh -c \"echo '' > /etc/mtab\"", shell = True)
            return match.group(1)
        except CalledProcessError as error:
            logging.error("Failed to mount VNFS: " + str(error.returncode))
            return None
        
    def umount_vnfs(self, vnfs, force=False):
        self._refresh_nfs()
        try:
            vnfs_mount =  "/mnt/" + vnfs
            if os.path.isdir(vnfs_mount) or force:
                subprocess.check_call(self._pcmd(["vnfs", "umount", vnfs]))
            return True
        except CalledProcessError as error:
            logging.error("Failed to unmount VNFS: " + str(error.returncode))
            return False

    def exist_vnfs(self, vnfs):
        self._refresh_nfs()
        vnfs_dir =  "/usr/var/lib/perceus/vnfs/" + vnfs
        return os.path.isdir(vnfs_dir)

    def delete_vnfs(self, vnfs):
        rv = self._run_via_ssh("perceus", self._pcmd(["vnfs", "delete", vnfs]))
        self._refresh_nfs(vnfs)
        if rv.failed:
            logging.error("Failed to delete VNFS " + vnfs)
            return False
        return True

    def acquire_node(self, vnfs):
        try:
            if len(self._available_nodes) == 0:
                return None
            node = self._available_nodes.pop()
            subprocess.check_call(self._pcmd(["node", "set", "vnfs", vnfs, node]))
            self._allocated_nodes.append(node)
            return node
        except CalledProcessError as error:
            self._available_nodes.push(node)
            logging.error("Failed to set node vnfs: " + str(error.returncode))
            return None   

    def release_node(self, node):
        try:
            free_vnfs_name='free-node'
            subprocess.check_call(self._pcmd(["node", "set", "vnfs", free_vnfs_name, node]))
            self._allocated_nodes.remove(node)
            self._available_nodes.append(node)
            return True
        except ValueError:
            return False

    def release_all_nodes(self):
        for node in self._allocated_nodes:
            self._available_nodes.append(node)
        self._allocated_nodes = []

    def list_nodes(self):
        output = subprocess.check_output(self._pcmd(["node", "list"]))
        logging.debug("--- output start ---")
        logging.debug(output)
        logging.debug("--- output end ---")
        nodes = string.split(output)
        i = 0
        for node in nodes:
            i += 1
            logging.debug("Node " + str(i) + " [" + node + "]")
        return nodes

