# ignore-nodes
# Set to ['worker'], and nothing will be done for node defined by directory TEST-NAME/nodes/worker/
# In test script, ct_node_list['worker'] will be set to None.
# Calling ct_nodeman.testrun("user", None, ...) is ignored.
ct_conf={
    'ignore-nodes': [],

    # testing on which OS
    # used to match repository from repository.xml
    'os': {
        'name': 'ubuntu',
        'version': '12.04',
    },
    
    # which repository to use
    'repository': {
         # mode:
         # contrail - use contrail .tar.gz packages
         # binary - use binary packages (deb, rpm)
        'mode': 'contrail',
        # release: used to match repository from repository.xml
        'release': 'testing',
    },

    # failed test manual intervention
    'test_fail': {
        'wait_time': 300,
        # max num of unhandled failed tests
        'count_limit': 5,
        'wait_file': '/tmp/wait_file',
        # on which node to wait ? The first valid will be used.
        'wait_nodes': ['federation', 'head']
    },
    # additional, configurable environment variables, are put in /srv/contegrator-test/common/env_var.sh
    'env': {},
}

#ct_conf['repository']['mode']='binary'
#ct_conf['repository']['release']='staging'
#ct_conf['env']['CONTRAIL_ENV_FLAG']='value'
