'''
Created on Aug 30, 2011

@author: jaka
'''

from xml.etree import ElementTree

import logging
import string
import tempfile
import urllib
import shutil

class RepoDefError(Exception):
    def __init__(self, msg):
        self._msg = msg
    
    def get_error_message(self):
        return self._msg

class RepoDefElement(object):
    '''
    Define one (binary) repository
    '''
    TAG_REPO_BINARY = "binary"

    ATTR_TYPE = "type"
    ATTR_RELEASE = "release"
    ATTR_OS_NAME = "os_name"
    ATTR_OS_VERSION = "os_version"
    ATTR_URL = "url"

    def __init__(self):
        '''
        Constructor
        '''
        self.type = None
        self.release = None
        self.os_name = None
        self.os_version = None
        self.url = None
    def __str__(self):
        return str( (self.type, self.release, self.os_name, self.os_version, self.url) )

    def _init_from_elem(self, root):
        if root.tag != RepoDefElement.TAG_REPO_BINARY:
            raise RepoDefError("Root element is not " + RepoDefElement.TAG_REPO_BINARY)
        self.type = root.get(RepoDefElement.ATTR_TYPE)
        self.release = root.get(RepoDefElement.ATTR_RELEASE)
        self.os_name = root.get(RepoDefElement.ATTR_OS_NAME)
        self.os_version = root.get(RepoDefElement.ATTR_OS_VERSION)
        self.url = root.get(RepoDefElement.ATTR_URL)
        logging.debug("Repository binary: " + str(self))
        
class RepoDef(object):
    '''
    Define all available repositories
    '''
    def __init__(self):
        self.repo_binary = []

    def _init_from_elem(self, root):
        for repo_root in root.iter(RepoDefElement.TAG_REPO_BINARY):
            repo_elem = RepoDefElement();
            repo_elem._init_from_elem(repo_root)
            self.repo_binary.append(repo_elem);    

    def parse_file(self, path):
        logging.debug("Parsing file " + path)
        root = ElementTree.parse(path)
        self._init_from_elem(root.getroot())

    # return only binary repositories, selected by (release, os_name, os_version) parameters
    def get_binary_repositories(self, release, os_name, os_version):
        repo_selected = []
        for repo in self.repo_binary:
            if  repo.release == release and \
                repo.os_name == os_name and \
                repo.os_version == os_version:
                logging.debug("Add repository " + str(repo))
                repo_selected.append(repo)
            else:
                logging.debug("Skip repository " + str(repo))
        return repo_selected
        
    

    