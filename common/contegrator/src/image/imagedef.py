'''
Created on Aug 30, 2011

@author: jaka
'''

from xml.etree import ElementTree

import logging
import string
import tempfile
import urllib
import shutil

from image.repodef import RepoDef
from conf import ct_conf

class ImageDefError(Exception):
    def __init__(self, msg):
        self._msg = msg
    
    def get_error_message(self):
        return self._msg

    
class ImageDef(object):
    '''
    Example image definition XML:
    <?xml version="1.0"?>
    <node type="vnfs" base="base-image-vnfs-name"
          repository="http://some.server.net/contrail">
          xmlns="http://contrail-project.eu/schema/2011/08/install">
      <upstream>upstream-package-name-1</upstream>
      <upstream>upstream-package-name-2</upstream>
      <upstream>upstream-package-name-3</upstream>
      <contrail>contrail-package-name-1</contrail>
      <contrail>contrail-package-name-2</contrail>
    </node>
    '''

    TAG_NODE = "node"
    TAG_UPSTREAM = "upstream"
    TAG_CONTRAIL = "contrail"
    TAG_TARGZ = "targz"
    
    ATTR_TYPE = "type"
    ATTR_BASE = "base"
    # OW2/bamboo/nexus repository. 
    ATTR_REPO = "repository_contrail"
    # repo for manually prepared tar.gz. Should contain dir structure /var/lib/... 
    ATTR_REPO_TARGZ = "repository_targz"

    IMAGE_TYPE_VNFS = "vnfs"
    IMAGE_TYPE_VM   = "vm"

    def __init__(self):
        '''
        Constructor
        '''
        self.syspackages = []
        self.ctrpackages = []
        self.tgzpackages = []
        self.repo = None
        self.repo_targz = ""
        self.type = None
        self.base = None
        self._release_tag = None
        #
        self.repo_binary = []
        # imager add this
        self.image_name = ""
        self.mount_path = ""
    
    def _init_from_elem(self, root, release_tag):
        self._release_tag = release_tag
        logging.debug("Parsing image definition")
        if root.tag != ImageDef.TAG_NODE:
            raise ImageDefError("Root element is not " + ImageDef.TAG_NODE)
        # mandatory attributes
        all_attr_defined = ImageDef.ATTR_TYPE in root.attrib and \
                           ImageDef.ATTR_BASE in root.attrib and \
                           ImageDef.ATTR_REPO in root.attrib
        non_attr_defined = not ImageDef.ATTR_TYPE in root.attrib and \
                           not ImageDef.ATTR_BASE in root.attrib and \
                           not ImageDef.ATTR_REPO in root.attrib
        if not all_attr_defined and not non_attr_defined:
            raise ImageDefError("Node must specify type, base image and repository URL (in exactly one install*.xml)")
        repo_targz = root.get(ImageDef.ATTR_REPO_TARGZ)
        if all_attr_defined:
            self_any_attr_defined = self.repo or self.repo_targz or self.base or self.type
            if self_any_attr_defined:
                # do not overwrite existing attributes
                raise ImageDefError("Mandatory attributes were already defined")
            self.repo = root.get(ImageDef.ATTR_REPO)
            self.base = root.get(ImageDef.ATTR_BASE)
            self.type = root.get(ImageDef.ATTR_TYPE)
            # ATTR_REPO_TARGZ is optional, but has to be in same file as mandatory attributes
            if repo_targz:
                self.repo_targz = repo_targz
        if repo_targz and not all_attr_defined:
            raise ImageDefError("targz repository defined without other mandatory attributes")
                  
        for pkg in root.getiterator(ImageDef.TAG_UPSTREAM):
            self.syspackages.append(string.strip(pkg.text))
        for pkg in root.getiterator(ImageDef.TAG_CONTRAIL):
            element = {}
            element['path2'] = string.strip(pkg.get("path"))
            element['file1'] = string.strip(pkg.get("file"))
            element['release_tag'] = string.strip(pkg.get("release_tag") or self._release_tag)
            self.ctrpackages.append(element)
        for pkg in root.getiterator(ImageDef.TAG_TARGZ):
            element = {}
            element['path2'] = string.strip(pkg.get("path"))
            element['filename'] = string.strip(pkg.get("file"))
            element['release_tag'] = string.strip(pkg.get("release_tag") or self._release_tag)
            self.tgzpackages.append(element)

    # download xml from nexus, find latest .tar.gz
    # save filename relative to repo/release_tag/
    # tag: bamboo tag, like "0.2-SNAPSHOT"
    def _init_contrail_filenames(self):
        xml_dir = tempfile.mkdtemp(".tmp", "contrail-xml-")
        logging.debug("Fetching XML files to " + xml_dir )
        for pkg in self.ctrpackages:
            try:
                xml_url = self.repo + "/" + pkg["path2"] + "/" + pkg["release_tag"] + "/"
                local_xml_path = xml_dir + "/" + pkg["file1"] + ".xml"
                logging.debug("  Fetching " + xml_url + " to file " + local_xml_path)
                (fname, headers) = urllib.urlretrieve(xml_url, local_xml_path)
                filename = self._parse_nexus_xml(local_xml_path, pkg["file1"])
                pkg["filename"] = filename
            except:
                logging.exception("Failed to retrieve XML for Contrail package: " + pkg["file1"])
                shutil.rmtree(xml_dir)
                return None
        shutil.rmtree(xml_dir)

    # search for latest .tar.gz, starting with filter.
    def _parse_nexus_xml(self, xml_path, filter):
        tree = ElementTree.parse(xml_path)
        root = tree.getroot()
        logging.debug("Parsing Nexus XML: " + xml_path)
        if root.tag != "content":
            raise ImageDefError("Nexus XML, root element is not content")
        data = root.find("data")
        latest_content = None
        latest_content_date = "1970-01-01 00:00:00.0 GMT"
        for content in data.iter("content-item"):
            #print content.find("lastModified").text
            text = content.find("text").text
            last_modified = content.find("lastModified").text
            # filter
            if text[-7:] != '.tar.gz':
                continue
            if text.find(filter) != 0:
                continue
            # OK, tar.gz with correct filename
            if last_modified > latest_content_date:
                latest_content_date = last_modified
                latest_content = content
        filename = latest_content.find("text").text
        logging.debug("LastModified filename: " + filename)
        return filename

    # check xml result, after all xml files (for a single node) were parsed
    def _post_parse_xml(self, release_tag):
        if self.tgzpackages and self.repo_targz == "":
            raise ImageDefError("Node must specify tar.gz repository")
        self._init_contrail_filenames()

    def parse_string(self, deftext, release_tag):
        root = ElementTree.fromstring(deftext)
        self._init_from_elem(root.getroot(), release_tag)
        self._post_parse_xml(release_tag)

    def parse_files(self, paths, release_tag):
        for path in paths:
            logging.debug("Parsing file " + path)
            root = ElementTree.parse(path)
            self._init_from_elem(root.getroot(), release_tag)
        if ct_conf['repository']['mode'] == 'contrail':
            self._post_parse_xml(release_tag)

    def get_sys_packages(self):
        return self.syspackages

    # return only filenames
    def get_contrail_packages(self):
        ctrpackages_filename = []
        for pkg in self.ctrpackages:
            ctrpackages_filename.append(pkg["filename"])
        return ctrpackages_filename

    # return full URL
    def get_contrail_packages_url(self):
        ctrpackages_url = []
        for pkg in self.ctrpackages:
            url = self.repo + "/" + pkg["path2"] + "/" + pkg["release_tag"] + "/" + pkg["filename"]
            ctrpackages_url.append(url)
        return ctrpackages_url

    # return only filenames
    def get_targz_packages(self):
        tgzpackages_filename = []
        for pkg in self.tgzpackages:
            tgzpackages_filename.append(pkg["filename"])
        return tgzpackages_filename

    # return full URL
    def get_targz_packages_url(self):
        tgzpackages_url = []
        for pkg in self.tgzpackages:
            url = self.repo_targz + "/" + pkg["path2"] + "/" + pkg["release_tag"] + "/" + pkg["filename"]
            tgzpackages_url.append(url)
        return tgzpackages_url

    def get_repository(self):
        return self.repo

    def get_image_type(self):
        return self.type

    def get_base_image_name(self):
        return self.base
    

    
