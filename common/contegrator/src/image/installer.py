'''
Created on Aug 30, 2011

@author: jaka
'''

import subprocess
from subprocess import CalledProcessError

import logging
import os
import shutil

class Installer(object):
    '''
    classdocs
    '''

    def __init__(self, mount_point):
        '''
        Constructor
        '''
        self._mount_point = mount_point
        self._tar_cmd = "tar"
        self._apt_cmd = "apt-get"
        self._chroot_cmd = "chroot"
        self._sudo_cmd = "sudo"

    def install(self, package_list):
        logging.debug("Installing system packages: " + " ".join(package_list))
        if None is package_list or len(package_list) == 0:
            return True
        # first, update apt repo
        arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                    self._apt_cmd, "-q", "-y", "--force-yes", "update"]
        try:
            subprocess.check_call(arg_list)
        except CalledProcessError as error:
            logging.exception("Failed to update VNFS apt repo: " + str(error.returncode))
            return False
        except OSError as error:
            logging.exception("Failed to update VNFS apt repo.")
            return False
        arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                    self._apt_cmd, "-q", "-y", "--force-yes", "install"]
        arg_list.extend(package_list)
        logging.debug("Install cmd: " + " ".join(arg_list))
        try:
            new_env = os.environ.copy()
            new_env['DEBIAN_FRONTEND'] = "noninteractive"
            subprocess.check_call(arg_list, env = new_env)
        except CalledProcessError as error:
            logging.exception("Failed to install packages to VNFS: " + str(error.returncode))
            return False
        except OSError as error:
            logging.exception("Failed to install packages to VNFS.")
            return False
        return True

    # tarball_list - relative to chroot dir
    def unpack(self, tarball_list):
        for tarball in tarball_list:
            try:
                logging.debug(tarball + ":")
                subprocess.check_call([self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                                       self._tar_cmd, "xzvf", tarball, "-C", "/"])
            except CalledProcessError as error:
                logging.exception("Failed to install packages to VNFS: " + str(error.returncode))
                return False
        return True

    # tarball_list - relative to chroot dir
    def unpack_bamboo(self, tarball_list):
        pkg_dir = "/var/tmp/contegrator/bamboo"
        pkg_dir_abs = self._mount_point + "/" + pkg_dir
        subprocess.check_call([self._sudo_cmd, self._chroot_cmd, self._mount_point, "mkdir", pkg_dir])
        for tarball in tarball_list:
            try:
                logging.debug(tarball + ":")
                arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                            self._tar_cmd, "xzvf", tarball, "-C", pkg_dir]
                subprocess.check_call(arg_list)
            except CalledProcessError as error:
                logging.exception("Failed to install packages to VNFS: " + str(error.returncode))
                return False
        for pkg_subdir_rel in os.listdir(pkg_dir_abs):
            # abs path in chroot
            pkg_subdir = os.path.join(pkg_dir, pkg_subdir_rel)
            # abs path outside of chroot
            pkg_subdir_abs = self._mount_point + "/" + pkg_subdir
            if os.path.isdir(pkg_subdir_abs):
                logging.debug("Tar/untar " + pkg_subdir_abs)
                try:
                    arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                                self._tar_cmd, 'cf', pkg_dir+"/temp.tar", "-C", pkg_subdir, "./"]
                    subprocess.check_call(arg_list)
                    arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                                self._tar_cmd, 'xf', pkg_dir+"/temp.tar", "-C", "/"]
                    subprocess.check_call(arg_list)
                except CalledProcessError as error:
                    logging.exception("Failed to install packages to VNFS (tar/untar): " + str(error.returncode))
                    return False
            else:
                logging.debug("Skip " + pkg_subdir_abs)
                continue
        return True        

    def chroot(self, cmd_args, env = {}):
        arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point]
        arg_list.extend(cmd_args)
        logging.debug("Chroot cmd: " + " ".join(arg_list))
        try:
            new_env = os.environ.copy()
            new_env.update(env)
            subprocess.check_call(arg_list, env = new_env)
            return True
        except CalledProcessError as error:
            logging.exception("Failed to execute chrooted script: " + str(error.returncode))
            return False

    def delete(self, local_path):
        abs_path = os.path.join(self._mount_point, local_path)
        arg_list = [self._sudo_cmd, self._chroot_cmd, self._mount_point, \
                    "rm", "-fr", local_path]
        logging.debug("Deleting " + abs_path)
        try:
            subprocess.check_call(arg_list)
            return True
        except:
            logging.exception("Failed to remove " + abs_path)
            return False
