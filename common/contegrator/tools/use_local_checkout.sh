#!/bin/bash

# Instead of using tests from svn, use a local svn checkout.
# E.g. replace existing svn checkouts (made by jenkins SVN plugin) with files from local filesystem (local svn checkout, owned by arbitraty user)
# $1 - path to local svn checkout
# $2 - path to base dir where jenkins puts his SVN co.
# $3... - individual subdirs from $1 to copy to $2

# if
# $1=/aa/bb/cc
# $2=/var/lib/jenkins/jobs/job-name/workspace/ (where jenkins creates subdirs xx, yy etc)
# then
# $3=ee/ff/xx, to replace $2/xx with $1/$3

set -xeu

LOCAL_DIR="$1"
shift
JENKINS_DIR="$1"
shift
TARGET_DIRS=$@

replace_one_dir() {
TARGET_BASENAME=`basename "$TARGET_DIR"`
echo "  Replacing $TARGET_BASENAME ($LOCAL_DIR/$TARGET_DIR -> $JENKINS_DIR/$TARGET_BASENAME)"
rm -fr "$JENKINS_DIR/$TARGET_BASENAME"
cp -a "$LOCAL_DIR/$TARGET_DIR" "$JENKINS_DIR"
}

echo "Replacing tests in $JENKINS_DIR ..."
for TARGET_DIR in $TARGET_DIRS
do
	replace_one_dir
done
#
