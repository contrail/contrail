#!/bin/bash

# Take jenkins parameters from environment, and append lines to contegrator conf.py file

CONF=$1
if [ $# -ne 1 ] || [ ! -f "$CONF" ]
then
	echo "Usage: $0 <conf.py>"
	exit 1
fi

# PRM_REPOSITORY_RELEASE
echo " # Added by $0" >> $CONF
echo "Info: Jenkins param value PRM_REPOSITORY_RELEASE=$PRM_REPOSITORY_RELEASE"
case "$PRM_REPOSITORY_RELEASE" in
	targz)
		echo "ct_conf['repository']['mode']='contrail'" >> $CONF
		# an additional apt-get repo will be still added to sources.list - xtreemfs debs
		echo "ct_conf['repository']['release']='testing'" >> $CONF
		;;
	staging|testing|release|obs_staging|obs_testing|obs_release)
		echo "ct_conf['repository']['mode']='binary'" >> $CONF
		echo "ct_conf['repository']['release']='$PRM_REPOSITORY_RELEASE'" >> $CONF
		;;
	*)
		echo "Error: Jenkins param value unknown PRM_REPOSITORY_RELEASE=$PRM_REPOSITORY_RELEASE"
		exit 1
esac

echo "Info: Jenkins param value PRM_OS_NAME_VERSION=$PRM_OS_NAME_VERSION"
case "$PRM_OS_NAME_VERSION" in
	'ubuntu-11.10')
		PRM_OS_NAME="ubuntu"
		PRM_OS_VERSION="11.10"
		;;
	'ubuntu-12.04')
		PRM_OS_NAME="ubuntu"
		PRM_OS_VERSION="12.04"
		;;
	*)
		echo "Error: Jenkins param value unknown PRM_OS_NAME_VERSION=$PRM_OS_NAME_VERSION"
		# exit 1
		;;
esac

echo "Info: Jenkins param value PRM_OS_NAME=$PRM_OS_NAME"
case "$PRM_OS_NAME" in
	ubuntu)
		echo "ct_conf['os']['name']='$PRM_OS_NAME'" >> $CONF
		;;
	*)
		echo "Error: Jenkins param value unknown PRM_OS_NAME=$PRM_OS_NAME"
		# exit 1
		;;
esac

echo "Info: Jenkins param value PRM_OS_VERSION=$PRM_OS_VERSION"
case "$PRM_OS_VERSION" in
	'11.10' | '12.04')
		echo "ct_conf['os']['version']='$PRM_OS_VERSION'" >> $CONF
		;;
	*)
		echo "Error: Jenkins param value unknown PRM_OS_VERSION=$PRM_OS_VERSION"
		# exit 1
		;;
esac

CONTRAIL_ENV_VARS=`set | grep '^CONTRAIL_ENV_' | sed 's/=.*$//'`
echo TTRT CONTRAIL_ENV_VARS = $CONTRAIL_ENV_VARS
for v_name in $CONTRAIL_ENV_VARS
do
	echo TTRT v_name = $v_name
	eval v_value=\$${v_name}
	echo TTRT v_value = $v_value
	echo "ct_conf['env']['$v_name']='$v_value'" >> $CONF
done

