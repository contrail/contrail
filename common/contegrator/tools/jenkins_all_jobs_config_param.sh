#!/bin/bash
# run as root...

JOBS="contegrator-test one-stress-test \
 federation-api federation-cli federation-web \
 monitoring-core monitoring-hub monitoring-infrastructure \
 provisioning-manager vep-cli vep-gui vin xtreemfs zookeeper \
 federation-ca-server provider-accounting \
 partner---vep-etc uros---federation-api"

PPWD=$PWD/`dirname $0`

for jj in $JOBS
do
	echo ''
	echo "Job: $jj"
	sudo -u jenkins $PPWD/jenkins_replace_xml_block.sh /var/lib/jenkins/jobs/$jj/config.xml $PPWD/config-param.xml hudson.model.ParametersDefinitionProperty
done

