#!/bin/bash

# Take multiple directories with individual contegratoer tests and merge them into one test.
# $1 - final test dir
# $2... - src tests

set -xeu

DEST_DIR="$1"
shift
SRC_DIRS=$@

merge_one_dir() {
echo "  Merging files from $SRC_DIR..."
(cd "$SRC_DIR" && tar cf - --exclude-vcs ./) | (cd "$DEST_DIR" && tar xvf -)
}

echo "Merging tests to $DEST_DIR..."
rm -fr "$DEST_DIR"
mkdir "$DEST_DIR"
for SRC_DIR in $SRC_DIRS
do
	merge_one_dir
done
echo "Merging tests to $DEST_DIR done"
#
