#!/bin/bash

# Detect obviously mistyped filenames in test .py files.

check_file() {
	ff=$1
	files=`cat $ff | grep -v '^[[:space:]]*#' | grep 'contegrator-test' | sed 's/[ "]/\n/g' | grep 'contegrator-test'`
	for file in $files
	do
		file_abs=/home/root2/ow2-contrail/trunk/common/integration-test/scripts-post-boot/$file
		if [ ! -f $file_abs ]
		then
			echo "ERROR: $ff, missing  $file_abs ( $file )"
		else
			echo "OK, $ff ( $file )"
		fi
	done
}

# check_file vin/test/02_vin_setup.py
main_dir="./"
[ -n "$1" ] && main_dir="$1"
files=`find "$main_dir" -wholename '*/test/*.py'`
for ff in $files
do
	check_file $ff
done
