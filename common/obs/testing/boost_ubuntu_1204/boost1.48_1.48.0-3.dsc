-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: boost1.48
Binary: libboost1.48-dbg, libboost1.48-dev, libboost1.48-all-dev, libboost1.48-doc, libboost-chrono1.48.0, libboost-chrono1.48-dev, libboost-date-time1.48.0, libboost-date-time1.48-dev, libboost-filesystem1.48.0, libboost-filesystem1.48-dev, libboost-graph1.48.0, libboost-graph1.48-dev, libboost-graph-parallel1.48.0, libboost-graph-parallel1.48-dev, libboost-iostreams1.48.0, libboost-iostreams1.48-dev, libboost-locale1.48.0, libboost-locale1.48-dev, libboost-math1.48.0, libboost-math1.48-dev, libboost-program-options1.48.0, libboost-program-options1.48-dev, libboost-python1.48.0, libboost-python1.48-dev, libboost-random1.48.0, libboost-random1.48-dev, libboost-regex1.48.0, libboost-regex1.48-dev, libboost-serialization1.48.0, libboost-serialization1.48-dev, libboost-signals1.48.0, libboost-signals1.48-dev, libboost-system1.48.0, libboost-system1.48-dev, libboost-test1.48.0,
 libboost-test1.48-dev, libboost-thread1.48.0, libboost-thread1.48-dev, libboost-timer1.48.0, libboost-timer1.48-dev, libboost-wave1.48.0,
 libboost-wave1.48-dev
Architecture: any all
Version: 1.48.0-3
Maintainer: Debian Boost Team <pkg-boost-devel@lists.alioth.debian.org>
Uploaders: Steve M. Robbins <smr@debian.org>, Domenico Andreoli <cavok@debian.org>
Homepage: http://www.boost.org/
Standards-Version: 3.9.2
Vcs-Browser: http://svn.debian.org/wsvn/pkg-boost/boost/trunk/
Vcs-Svn: svn://svn.debian.org/svn/pkg-boost/boost/trunk
Build-Depends: debhelper (>= 8), zlib1g-dev, libbz2-dev, libicu-dev, bison, flex, docbook-to-man, help2man, xsltproc, doxygen, python, python-all-dev, python3, python3-all-dev (>= 3.1)
Package-List: 
 libboost-chrono1.48-dev deb libdevel optional
 libboost-chrono1.48.0 deb libs optional
 libboost-date-time1.48-dev deb libdevel optional
 libboost-date-time1.48.0 deb libs optional
 libboost-filesystem1.48-dev deb libdevel optional
 libboost-filesystem1.48.0 deb libs optional
 libboost-graph-parallel1.48-dev deb libdevel optional
 libboost-graph-parallel1.48.0 deb libs optional
 libboost-graph1.48-dev deb libdevel optional
 libboost-graph1.48.0 deb libs optional
 libboost-iostreams1.48-dev deb libdevel optional
 libboost-iostreams1.48.0 deb libs important
 libboost-locale1.48-dev deb libdevel optional
 libboost-locale1.48.0 deb libs optional
 libboost-math1.48-dev deb libdevel optional
 libboost-math1.48.0 deb libs optional
 libboost-program-options1.48-dev deb libdevel optional
 libboost-program-options1.48.0 deb libs optional
 libboost-python1.48-dev deb libdevel optional
 libboost-python1.48.0 deb python optional
 libboost-random1.48-dev deb libdevel optional
 libboost-random1.48.0 deb libs optional
 libboost-regex1.48-dev deb libdevel optional
 libboost-regex1.48.0 deb libs optional
 libboost-serialization1.48-dev deb libdevel optional
 libboost-serialization1.48.0 deb libs optional
 libboost-signals1.48-dev deb libdevel optional
 libboost-signals1.48.0 deb libs optional
 libboost-system1.48-dev deb libdevel optional
 libboost-system1.48.0 deb libs optional
 libboost-test1.48-dev deb libdevel optional
 libboost-test1.48.0 deb libs optional
 libboost-thread1.48-dev deb libdevel optional
 libboost-thread1.48.0 deb libs optional
 libboost-timer1.48-dev deb libdevel optional
 libboost-timer1.48.0 deb libs optional
 libboost-wave1.48-dev deb libdevel optional
 libboost-wave1.48.0 deb libs optional
 libboost1.48-all-dev deb libdevel optional
 libboost1.48-dbg deb debug extra
 libboost1.48-dev deb libdevel optional
 libboost1.48-doc deb doc optional
Checksums-Sha1: 
 27aced5086e96c6f7b2b684bda2bd515e115da35 48236989 boost1.48_1.48.0.orig.tar.bz2
 8c55de43dff18d61215143a000206cd32f5507ba 106324 boost1.48_1.48.0-3.debian.tar.gz
Checksums-Sha256: 
 1bf254b2d69393ccd57a3cdd30a2f80318a005de8883a0792ed2f5e2598e5ada 48236989 boost1.48_1.48.0.orig.tar.bz2
 1352dd094a95d4d199839634190446564ae13ca39883bc07bd667778760e75b7 106324 boost1.48_1.48.0-3.debian.tar.gz
Files: 
 d1e9a7a7f532bb031a3c175d86688d95 48236989 boost1.48_1.48.0.orig.tar.bz2
 f69515df9151b4c84b853c3e41cd21f9 106324 boost1.48_1.48.0-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.18 (GNU/Linux)

iEYEARECAAYFAk+j5X0ACgkQIPJyS9rqnllWYQCdHUHg84weeS7/TTDO7ASEVcFi
fU4AoILHTVNVV6C3wBhLUSovZWvvWUlS
=pc3s
-----END PGP SIGNATURE-----