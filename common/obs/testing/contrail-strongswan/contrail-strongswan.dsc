Format: 1.0
Source: contrail-strongswan
Version: 0+2954
Binary: contrail-strongswan
Maintainer: Contrail Administrators <contrail-admin@ow2.org>
Architecture: any
Build-Depends: debhelper (>= 5), build-essential (>=11), g++, flex, bison, gperf, automake, libgmp3-dev, libreadline-dev, autoconf, libtool
Files:
 3d135cdf72469abddd108e8764fadf0e 5519288 contrail-strongswan_0+2954.orig.tar.gz
