Format: 1.0
Source: contrail-federation-api
Version: 0+2954
Binary: contrail-federation-api
Maintainer: Contrail Administrators <contrail-admin@ow2.org>
Architecture: any
Build-Depends: debhelper (>= 5),
  build-essential (>=11),
  ant (>= 1.6.5), ant-optional (>= 1.6.5),
  java6-sdk
Files:
 22bd7851a4bad55a278396717f4c35c5 61322650 contrail-federation-api_0+2954.orig.tar.gz
