Format: 1.0
Source: contrail-security-commons
Version: 0+2954
Binary: contrail-security-commons
Maintainer: Contrail Administrators <contrail-admin@ow2.org>
Architecture: any
Build-Depends: debhelper (>= 5),
  build-essential (>=11),
  ant (>= 1.6.5), ant-optional (>= 1.6.5),
  java6-sdk
Files:
 ddc6366ab1585a8d739a1b85978ccaa8 10785572 contrail-security-commons_0+2954.orig.tar.gz
