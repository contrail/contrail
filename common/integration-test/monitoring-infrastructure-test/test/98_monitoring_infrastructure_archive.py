# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_archive(ct_node_list, ct_nodeman):
    # federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    #
    ct_nodeman.getfile(head, "/tmp/contrail-one-monitor.log", "archive/head/" )
    ct_nodeman.getfile(head, "/tmp/contrail-one-sensor.log", "archive/head/" )
    ct_nodeman.getfile(worker, "/tmp/contrail-one-sensor.log", "archive/worker/" )

#
