# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

def test_01_xtreemfs(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    #ct_nodeman.sshrun("root", worker, "sleep 30")
    ct_nodeman.testrun("root", federation, "ls -la  /srv/contrail/images/")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/xtreemfs/test-2-copy-files.sh")
    ct_nodeman.testrun("root", head, "sleep 20 || true")

    for key in ct_node_list:
        node = ct_node_list[key]
        ct_nodeman.testrun("root", node, "/srv/contegrator-test/xtreemfs/test-3-list-files.sh")
#
