#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# test.sh uses curl.
install_binary_always curl

do_binary() {
install_binary_always contrail-federation-id-prov
}

do_targz() {
install_targz_dependency tomcat6
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

