#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

cd /srv/contegrator-test/federation-ca-server/files/

# Create truststore
cat cert/contrail-id-prov-server.key cert/contrail-id-prov-server.cert > cert/contrail-id-prov-server.certkey
openssl pkcs12 -export -in cert/contrail-id-prov-server.certkey -out cert/ks-id-prov.p12 -name contrail -noiter -nomaciter -passout pass:contrail

mkdir -p /var/lib/contrail/federation-id-prov
cd /srv/contegrator-test/federation-ca-server/files
cp cert/ks-id-prov.p12 /var/lib/contrail/contrail-id-prov/
cp cert/contrailTrustStore /var/lib/contrail/contrail-id-prov/
ls -la /var/lib/contrail/contrail-id-prov/

cp /var/lib/tomcat6/conf/server.xml /var/lib/tomcat6/conf/server.xml.back-`date +%s`
sed -i -e '/<Service name="Catalina">/r /srv/contegrator-test/federation-id-prov/tomcat-connector.xml'  /etc/tomcat6/server.xml

