#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# setup vep-cli via telnet interface

# add '\n' between cmd line params
function telnet_cmd() {	
STR=""
for ss in "$@"
do
	STR+="${ss}\n"
done
echo -e "$STR\n" | nc localhost 10555
}

function vep_telnet() {
# first call to telnet interface - question to setup local admin (ladmin, p1234)
telnet_cmd '' '' 'yes' 'ladmin' 'l1234' 'exit'
telnet_cmd 'ladmin' 'l1234' 'add admin' 'fedadmin' 'y'

telnet_cmd 'ladmin' 'l1234' 'add datacenter' 'datac1' 'SI' 'dc-1-desc' 'y'
telnet_cmd 'ladmin' 'l1234' 'add cluster' 'cluster1' '001' '1' 'cl-1-desc' 'y' # add to datacenter id=1
telnet_cmd 'ladmin' 'l1234' 'add rack' 'rack1' '1' 'rack-1-desc' 'y' # to rack id=1
# add new host - but first delete existing host
onecluster list
onehost list
#### onehost delete "$HOST_NAME_head" || true
telnet_cmd 'ladmin' 'l1234' 'add host' "$HOST_NAME_head" 'im_kvm' 'vmm_kvm' 'tm_nfs' 'y' 'exit'
onecluster list
onehost list
# seems that host is not added/moved to contrail cluster
# onecluster addhost "$HOST_NAME_head" contrail # ONE 2.2
onecluster addhost contrail "$HOST_NAME_head" # ONE 3.6
if [ ! -z "$HOST_NAME_worker" ]; then
	#### onehost delete "$HOST_NAME_worker"
	telnet_cmd 'ladmin' 'l1234' 'add host' "$HOST_NAME_worker" 'im_kvm' 'vmm_kvm' 'tm_nfs' 'y' 'exit'
	onecluster addhost contrail "$HOST_NAME_worker"
fi
if [ ! -z "$HOST_NAME_worker1" ]; then
	#### onehost delete "$HOST_NAME_worker1"
	telnet_cmd 'ladmin' 'l1234' 'add host' "$HOST_NAME_worker1" 'im_kvm' 'vmm_kvm' 'tm_nfs' 'y' 'exit'
	onecluster addhost contrail "$HOST_NAME_worker1"
fi
if [ ! -z "$HOST_NAME_worker2" ]; then
	#### onehost delete "$HOST_NAME_worker2"
	telnet_cmd 'ladmin' 'l1234' 'add host' "$HOST_NAME_worker2" 'im_kvm' 'vmm_kvm' 'tm_nfs' 'y' 'exit'
	onecluster addhost contrail "$HOST_NAME_worker2"
fi

onehost list
onecluster list
}

vep_telnet

#
