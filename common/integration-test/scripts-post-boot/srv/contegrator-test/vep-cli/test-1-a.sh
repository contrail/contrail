#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

#	one.ip=$HOST_NAME_head \

function deploy_ovf() {
# param
# OVF_ID

# VEP create app
OVF_FILE=/srv/contegrator-test/vep-cli/dsl-test-xlab.ovf
OVF="`cat $OVF_FILE`"
curl -i -v -X PUT \
	--header "X-Username: gregorb" \
	--header "Accept: application/json" \
	--header "Content-type: application/xml" \
	--data "$OVF" \
	http://localhost:10500/ovf/myapp1
# {"message":"SUCCESS_ACCEPTED","title":"New OVF application submit request","sno":1}

# VEP initialize
curl -i -v -X PUT \
	--header "X-Username: gregorb" \
	 --header "Accept: application/json" \
	http://localhost:10500/ovf/id/$OVF_ID/action/initialize

# VEP deploy - oneimage with wrong attributes etc
curl -i -v -X PUT \
	--header "X-Username: gregorb" \
	--header "Accept: application/json" \
	http://localhost:10500/ovf/id/$OVF_ID/action/deploy
# Error submitting VM to ONE: [VirtualMachineAllocate] Error trying to CREATE VM Could not get disk image for VM.
}

# VEP user
curl -i -X PUT \
	--header "X-Username: fedadmin" \
	--header "Accept: application/json" \
	--header "Content-type: application/json" \
	--data '{"role":"admin", "vid":"-1", "groups":["user", "admin"]}' \
	http://localhost:10500/user/gregorb
# {"message":"SUCCESS_CREATED"}

OVF_ID=1
deploy_ovf

#

