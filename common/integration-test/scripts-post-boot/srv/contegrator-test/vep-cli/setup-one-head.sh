#!/bin/bash

# executed on ONE head

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# ONE setup - contrail cluster
onecluster create contrail
if [ ! -z "$HOST_NAME_head" ]; then
#	onecluster addhost $HOST_NAME_head contrail # ONE 2.2
	onecluster addhost contrail $HOST_NAME_head # ONE 3.6

fi
if [ ! -z "$HOST_NAME_worker" ]; then
	onecluster addhost contrail $HOST_NAME_worker
fi
if [ ! -z "$HOST_NAME_worker1" ]; then
	onecluster addhost contrail $HOST_NAME_worker1
fi
if [ ! -z "$HOST_NAME_worker2" ]; then
	onecluster addhost contrail $HOST_NAME_worker2
fi

#

