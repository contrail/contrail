#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# get build tools and other dependencies
install_binary_always libtool bzip2 libssl-dev libcurl4-openssl-dev libldap2-dev libpam0g-dev libkrb5-dev po-debconf hardening-wrapper network-manager-dev libfcgi-dev clearsilver-dev libreadline-dev bison flex gperf libgmp3-dev libjansson-dev libxml2-dev libsqlite3-dev libnm-glib-dev libnm-util-dev automake strongswan

# make and install
cd /srv/contegrator-test/contrail-strongswan/
mkdir contrail-strongswan-src
pushd contrail-strongswan-src
tar xzvf ../contrail-strongswan-src.tar.gz
cd strongswan
sh ./autogen.sh
./configure --prefix=/usr --sysconfdir=/etc --enable-styx
make all
#make test
sudo make install


# config certs
CERT_SRC="/srv/contegrator-test/vin/strongswan-ca"
cp ${CERT_SRC}/ca-cert.der /etc/ipsec.d/cacerts/
#cp ${CERT_SRC}/one-${NODE_NAME}-cert.der /etc/ipsec.d/certs/
#cp ${CERT_SRC}/one-${NODE_NAME}-key.der /etc/ipsec.d/private/
cp ${CERT_SRC}/dummy-cert.der /etc/ipsec.d/certs/
cp ${CERT_SRC}/dummy-key.der /etc/ipsec.d/private/
echo ": RSA dummy-key.der" > /var/lib/strongswan/ipsec.secrets.inc

/usr/sbin/ipsec restart
popd
# ls -l /var/run/styx.sock
#
