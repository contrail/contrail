#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Create a volume, for oneadmin:cloud user/group
# On federation node
addgroup --gid=4000 cloud
useradd --uid=4000 --gid=4000 oneadmin
mkfs.xtreemfs -u oneadmin -g cloud localhost/contrail-images
