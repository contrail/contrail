#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# The metadata-cache-ttl-s option cannot be specified in /etc/fstab
# mount /srv/contrail/images
mount.xtreemfs --metadata-cache-ttl-s 10 ${HOST_NAME_head}/contrail-images /srv/contrail/images

