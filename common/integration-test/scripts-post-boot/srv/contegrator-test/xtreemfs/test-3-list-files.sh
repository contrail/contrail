#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Check files on the client node
ls -la /srv/
ls -la /srv/contrail/
ls -la /srv/contrail/images/
ls -la /srv/contrail/images/test-file
