#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Install apt packages"
install_binary_always daemon
install_binary_always xtreemfs-server xtreemfs-backend xtreemfs-tools
# create additional directories, needed by modified /etc/init.d scripts
mkdir /var/run/xtreemfs/
chown xtreemfs /var/run/xtreemfs/
