#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# additional group for oneadmin
# usermod -a -G fuse oneadmin

modprobe fuse
echo -e '\n# Required for xtreemfs\nfuse\n' >> /etc/modules

# --metadata-cache-ttl-s 10
mkdir -p /srv/contrail/images
echo "${HOST_NAME_head}/contrail-images /srv/contrail/images xtreemfs defaults,allow_other,noauto 0 0" >> /etc/fstab

