#!/usr/bin/python2.7

import json
import os
import sys

if len(sys.argv) < 2:
    print "Usage: <prog> <file.json> <key1,key2,=new_value> ..."
    sys.exit(1)

fin_name = sys.argv[1]

fin = open(fin_name)
val = json.load(fin)
fin.close()

for arg in sys.argv[2:]:
    [keys, new_value] = arg.split('=')
    keys = keys.split(',')
    val_tmp = []
    val_tmp.append(val)

    for key in keys[:-1]:
        val_sub = val_tmp[-1][key]
	val_tmp.append(val_sub)

    # appened edited last value
    val_tmp.append(new_value)

    val_prev = new_value
    for (val_sub, key) in reversed(zip(val_tmp, keys)):
        val_sub[key] = val_prev
        val_prev = val_sub
        val = val_sub

# print json.dumps(val, sort_keys=True, indent=4)
fout = open(fin_name, "w")
json.dump(val, fout, indent=4)
fout.close()
