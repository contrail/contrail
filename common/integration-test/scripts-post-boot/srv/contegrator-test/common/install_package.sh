#!/bin/bash

# Install binary contrail packages, if testing binary contrail packages.
# Functions p_* are private.

# exit on error
set -e
# echo cmd
set -x

p_install_deb() {
export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
echo "Install apt packages"
apt-get -q -y --force-yes install $@
}

install_binary_always() {
# TODO: select OS
p_install_deb $@
}

# install binary package, only if binary mode is selected
install_binary() {
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	install_binary_always $@
fi
}

# If package was install from .tag.gz, install prerequisites
install_targz_dependency()
{
if [ "$CONF_REPOSITORY_MODE" == "contrail" ]
then
	install_binary_always $@
fi
}

