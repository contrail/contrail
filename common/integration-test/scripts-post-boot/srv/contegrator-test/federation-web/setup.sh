#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

CONF="/etc/contrail/contrail-federation-web/federation-web.conf"
cp $CONF $CONF.`date +%s`
sed -i -e '/SLA_EXTRACTOR_BASE/d' $CONF
sed -i -e '/MONITORING_BASE/d' $CONF
echo "" >> $CONF
echo "SLA_EXTRACTOR_BASE = 'http://${HOST_NAME_head}:8080/rest-monitoring/sla/slaextractor'" >> $CONF
echo "MONITORING_BASE = 'http://${HOST_NAME_head}:8080/rest-monitoring/monitoring'" >> $CONF

#
