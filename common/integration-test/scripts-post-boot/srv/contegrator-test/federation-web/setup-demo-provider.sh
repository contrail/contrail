#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

if [ -z "${HOST_NAME_head}" ]
then
	# feder-web test without VEP, rest-monitoring etc.
	echo "ERROR - HOST_NAME_head is empty, not executing $0"
	exit 0
fi

export FEDERATION_IP=${HOST_NAME_federation}
export IP=`host ${HOST_NAME_head} | awk '{print $4}'`
#export HOSTNAME=n0004
export FEDERATION_CLI_URL="http://${HOST_NAME_federation}:8080/federation-api"

PROVIDER_ADD_DATA=$(contrail-federation-cli add-provider -data "{'name':\
        'CloudProvider2','email':'cloudprovider2.com','country':'UK','typeId':42,\
        'providerUri':'http://$IP:10500'}")

export PROVIDER_ID=$(echo $PROVIDER_ADD_DATA | python -c "import sys; import \
        json; print json.loads(sys.stdin.read())['headers']['Location']\
        .split('/')[-1]")

contrail-federation-cli add-server -providerId $PROVIDER_ID \
        -data "{'name': '$HOSTNAME', 'ram_total': '3915', 'ram_used': '1152', \
        'ram_free': '2763', 'cpu_cores': '4', 'cpu_speed': '2494.276', \
        'cpu_load_one': '0.09', 'cpu_load_five': '0.04'}"

# ? ? ?
contrail-federation-cli add-slat -providerId $PROVIDER_ID -data \
        "{'name': 'XLAB SLAT', 'url': 'http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml'}"

