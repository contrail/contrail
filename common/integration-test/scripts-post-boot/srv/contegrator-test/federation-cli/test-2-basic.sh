#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# based on svn://svn.forge.objectweb.org/svnroot/contrail/trunk/federation/federation-cli/docs/examples.txt
# Test for basic federation-cli functionality - add, remove, update etc.

# federation-api REST server URL
export FEDERATION_CLI_URL=http://localhost:8080/federation-api

# federation api url
URL="http://localhost:8080/federation-api"
CMD=contrail-federation-cli
federation-cli() {
	set +x
        arg1="$1"
        shift
	set -x
        str=`$CMD $arg1 -url $URL "$@"`
        (echo "$str" | grep -q '"status": "SUCCESS"') || return 2
}

# operations on provider
federation-cli add-provider -data "{'name':'CloudProvider','email':'cloudprovider.com','country':'UK','typeId':3,'providerUri':'contrail://CloudProvider'}"
federation-cli add-provider -data "{'name':'TestProvider','typeId':3,'providerUri':'contrail://TestProvider'}"
federation-cli getAll-provider
federation-cli get-provider -providerId 1
federation-cli update-provider -providerId 1 -data "{'country':'SI'}"
federation-cli delete-provider -providerId 2

# operations on server
federation-cli add-server -providerId 1 -data "{'name':'server1.cloudprovider.com', 'RAM':'16 GB'}"
federation-cli add-server -providerId 1 -data "{'name':'server2.cloudprovider.com', 'RAM':'32 GB'}"
federation-cli add-server -providerId 1 -data "{'name':'server3.cloudprovider.com', 'RAM':'32 GB'}"
federation-cli getAll-server -providerId 1
federation-cli update-server -providerId 1 -serverId 1 -data "{'RAM':'8 GB'}"
federation-cli get-server -providerId 1 -serverId 1
federation-cli delete-server -providerId 1 -serverId 1

# operations on VM
federation-cli add-vm -providerId 1 -data "{'name':'vm1', 'OS':'Ubuntu 11.10'}"
federation-cli add-vm -providerId 1 -data "{'name':'vm2', 'OS':'Ubuntu 11.10'}"
federation-cli add-vm -providerId 1 -data "{'name':'vm3', 'OS':'Debian 6.0.4'}"
federation-cli getAll-vm -providerId 1
federation-cli update-vm -providerId 1 -vmId 1 -data "{'OS':'Ubuntu 11.04'}"
federation-cli get-vm -providerId 1 -vmId 1
federation-cli delete-vm -providerId 1 -vmId 1

# operations on cluster
federation-cli add-cluster -providerId 1 -data "{'name':'Cluster1'}"
federation-cli add-cluster -providerId 1 -data "{'name':'Cluster2'}"
federation-cli add-cluster -providerId 1 -data "{'name':'Cluster3'}"
federation-cli getAll-cluster -providerId 1
federation-cli update-cluster -providerId 1 -clusterId 2 -data "{'newKey':'value'}"
federation-cli get-cluster -providerId 1 -clusterId 2
federation-cli delete-cluster -providerId 1 -clusterId 1

# linking/unlinking server to/from cluster
federation-cli linkServer-cluster -providerId 1 -clusterId 2 -serverId 2
federation-cli linkServer-cluster -providerId 1 -clusterId 2 -serverId 3
federation-cli getLinkedServers-cluster -providerId 1 -clusterId 2
federation-cli getLinkedServer-cluster -providerId 1 -clusterId 2 -serverId 3
federation-cli unlinkServer-cluster -providerId 1 -clusterId 2 -serverId 3

# linking/unlinking VM to/from cluster
federation-cli linkVm-cluster -providerId 1 -clusterId 2 -vmId 2
federation-cli linkVm-cluster -providerId 1 -clusterId 2 -vmId 3
federation-cli getLinkedVms-cluster -providerId 1 -clusterId 2
federation-cli getLinkedVm-cluster -providerId 1 -clusterId 2 -vmId 3
federation-cli unlinkVm-cluster -providerId 1 -clusterId 2 -vmId 3

# operations with virtual organization
federation-cli add-vo -providerId 1 -data "{'name':'Virtual Organization 1', 'email':'info@vo1.com'}"
federation-cli add-vo -providerId 1 -data "{'name':'Virtual Organization 2', 'email':'info@vo2.com'}"
federation-cli getAll-vo -providerId 1
federation-cli update-vo -providerId 1 -voId 1 -data "{'newKey':'value'}"
federation-cli get-vo -providerId 1 -voId 1
federation-cli delete-vo -providerId 1 -voId 1

# linking/unlinking cluster to/from virtual organization
federation-cli linkCluster-vo -providerId 1 -voId 2 -clusterId 2
federation-cli linkCluster-vo -providerId 1 -voId 2 -clusterId 3
federation-cli getLinkedClusters-vo -providerId 1 -voId 2
federation-cli getLinkedCluster-vo -providerId 1 -voId 2 -clusterId 2
federation-cli unlinkCluster-vo -providerId 1 -voId 2 -clusterId 3

# operations with OVFs
federation-cli add-ovf -providerId 1 -data "{'name':'Test OVF 1'}"
federation-cli add-ovf -providerId 1 -data "{'name':'Test OVF 2'}"
federation-cli getAll-ovf -providerId 1
federation-cli get-ovf -providerId 1 -ovfId 1
federation-cli delete-ovf -providerId 1 -ovfId 2

# operations with SLA templates (SLATs)
federation-cli add-slat -providerId 1 -data "{'url':'http://contrail.xlab.si/slats/GoldSLAT.xml','name':'Gold SLA Template'}"
federation-cli add-slat -providerId 1 -data "{'url':'http://contrail.xlab.si/slats/SilverSLAT.xml','name':'Silver SLA Template'}"
federation-cli getAll-slat -providerId 1
federation-cli update-slat -providerId 1 -slatId 2 -data "{'url':'http://contrail.xlab.si/slats/SilverSLAT-1.xml'}"
federation-cli get-slat -providerId 1 -slatId 2
federation-cli delete-slat -providerId 1 -slatId 1


# TODO:
# https protocol and certificate-based authentication
export FEDERATION_CLI_URL=https://localhost:8443/federation-api
export FEDERATION_CLI_CACERT=/path/to/cacerts.jks:contrail
export FEDERATION_CLI_CLIENTCERT=/path/to/client.jks:contrail

#$CMD getAll-provider
