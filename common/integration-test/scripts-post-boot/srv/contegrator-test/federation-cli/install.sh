#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
install_binary_always contrail-federation-cli
}

do_targz() {
# unpacking tar.gz
cat > /usr/bin/contrail-federation-cli <<EOF
#!/bin/sh
exec java -jar /usr/lib/contrail/federation-cli/federation-cli-0.1-SNAPSHOT-jar-with-dependencies.jar "\$@"
EOF
chmod a+x /usr/bin/contrail-federation-cli
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

