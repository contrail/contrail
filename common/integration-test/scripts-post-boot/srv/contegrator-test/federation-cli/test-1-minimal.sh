#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# federatin api url
URL="http://localhost:8080/federation-api"
CMD=contrail-federation-cli

# usage example
$CMD getAll-provider -url $URL
$CMD getAll-provider -url $URL | grep SUCCESS

