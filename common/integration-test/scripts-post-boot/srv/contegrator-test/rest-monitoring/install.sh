#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
install_binary_always contrail-rest-monitoring
}

do_targz() {
install_binary_always contrail-rest-monitoring
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
