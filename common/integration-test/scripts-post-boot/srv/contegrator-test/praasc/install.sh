#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
echo "Install PRaaSC to ONE head"

apt-get -q -y --force-yes install python-setuptools python-dev

mkdir -p /usr/share/contrail/federation
mkdir -p /var/log/contrail/
mkdir -p /var/lib/contrail/

cd /usr/share/contrail/federation
svn co svn://svn.forge.objectweb.org/svnroot/contrail/trunk/federation/PRaaSC
cd PRaaSC
python setup.py install

