#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# SSH key already copied to proxy2.contrail.xlab.si

cat > /etc/praasc.conf <<EOF
DATA_FILE = '/var/lib/contrail/praasc.json'
LOGGING_LEVEL = 'DEBUG'
PUBLIC_ADDRESS = "${HOST_NAME}.xc2.xlab.lan"
LOCALTUNNEL_OPEN = 'http://open.proxy2.contrail.xlab.si/'
EOF

