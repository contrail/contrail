#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Setup rabbitmq - federation"
# rabbitmqctl change_password guest 123
rabbitmqctl change_password guest guest
# add user if required
rabbitmqctl list_users | grep '^fedadmin[[:space:]]' || \
	rabbitmqctl add_user fedadmin fed1234
rabbitmqctl set_permissions fedadmin ".*"  ".*"  ".*"
rabbitmqctl list_permissions

#
