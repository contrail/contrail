#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Setup parameters for federation-api - provisioning-manager communication
CONFIG=/etc/contrail/federation-api/federation-api.cfg

if [ "$CONTRAIL_ENV_USE_PDP" == "false" ]
then
	federation_core_class_string='FederationCoreBasic'
else
	federation_core_class_string='FederationCoreImpl'
fi

# mkdir /usr/local/usersdb, /usr/local/imagestore ?
/srv/contegrator-test/edit-conf-file.sh $CONFIG \
	federation-core-class=$federation_core_class_string \
	pdp_path=http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap \
	user_db_path=/usr/local/usersdb \
	image_store_path=/usr/local/imagestore \
	xuser.id=gregorb \
	sla.negotiation.engine=MOCK \
	sla.negotiation.endpoint=http://localhost:8080/services/contrailNegotiation


mkdir /usr/local/imagestore
mkdir /usr/local/usersdb
