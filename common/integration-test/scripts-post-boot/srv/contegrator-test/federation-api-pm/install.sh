#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# test.sh uses curl. shasum comes with perl
install_binary_always curl perl

install_binary contrail-federation-api
#
install_targz_dependency tomcat6

