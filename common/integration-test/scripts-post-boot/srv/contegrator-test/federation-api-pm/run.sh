#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Run deamons"
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	# installing deb
	echo -n ''
	# service tomcat6 restart
else
	# unpacking tar.gz
	service tomcat6 restart
fi

