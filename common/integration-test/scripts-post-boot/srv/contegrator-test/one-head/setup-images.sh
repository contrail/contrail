#!/bin/bash
# Param - image templates to register

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

#
IMAGES="$@"
for img in $IMAGES
do
	echo "Image template: $img"
	oneimage register "$img"
done
