#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

#
install_binary_always dnsmasq
/etc/init.d/dnsmasq stop
