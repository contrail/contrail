#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

#
/etc/init.d/dnsmasq start
sleep 5
netstat -plane | grep -e ':53[[:space:]]' -e ':67[[:space:]]'
