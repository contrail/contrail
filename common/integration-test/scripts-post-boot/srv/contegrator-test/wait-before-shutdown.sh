#!/bin/bash

# exit on error
set -e
# echo cmd
#set -x

WAIT_TIME=3600

if [ x${JENKINS_WAIT_AFTER_RUN:-yes} = xno ]; then
    echo "Environment variable JENKINS_WAIT_AFTER_RUN has been set to $JENKINS_WAIT_AFTER_RUN, so no waiting"
    exit 0
fi

SLEEP_TIME=10

# optional param
[ $# -ge 1 ] && WAIT_TIME=$1

FILE1="/tmp/remove_to_end_test.$$"
FILE2="/tmp/remove_to_end_test"
cat <<EOF

`date`
Waiting on someone to
rm $FILE1
Do
touch $FILE2
to wait longer than $WAIT_TIME seconds.


EOF

touch $FILE1
while [ -f $FILE1 ] && [ $WAIT_TIME -gt 0 ]
do
	sleep $SLEEP_TIME
	WAIT_TIME=$(($WAIT_TIME - $SLEEP_TIME))
done
[ -f $FILE2 ] && echo "File $FILE2 exists, waiting up to inf secondes..."
while [ -f $FILE2 ]
do
	sleep 11
done

#
