#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
/etc/init.d/contrail-one-monitor stop
sleep 10 # required, restart is not working
/etc/init.d/contrail-one-monitor start
sleep 10
}

do_targz() {
echo "Run one-monitor.jar"
MYCP=`find /usr/lib/contrail/monitoring/one-monitor/lib -iname '*.jar' -exec echo -n {}: \;`
nohup java -cp $MYCP eu.contrail.infrastructure_monitoring.OneMonitor >> /var/log/contrail/one-monitor.nohup.log &
sleep 10 # nohup needs some time ?
echo "  done"
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi
#
