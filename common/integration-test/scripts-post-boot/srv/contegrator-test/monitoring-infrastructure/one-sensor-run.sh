#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
/etc/init.d/contrail-one-sensor stop
sleep 10
/etc/init.d/contrail-one-sensor start
sleep 10
}

do_targz() {
echo "Run one-sensor.jar"
MYCP=`find /usr/lib/contrail/monitoring/one-sensor/lib -iname '*.jar' -exec echo -n {}: \;`
nohup java -cp $MYCP eu.contrail.infrastructure_monitoring.OneSensor >> /var/log/contrail/one-sensor.nohup.log &
sleep 10 # nohup needs some time ?
echo "  done"
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi
#
