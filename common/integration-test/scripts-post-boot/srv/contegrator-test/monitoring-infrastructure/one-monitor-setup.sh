#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Setup rabbitmq"
# rabbitmqctl change_password guest 123
rabbitmqctl change_password guest guest
# add user if required
rabbitmqctl list_users | grep '^contrail[[:space:]]' || \
	rabbitmqctl add_user contrail contrail
rabbitmqctl set_permissions contrail ".*"  ".*"  ".*"
rabbitmqctl list_permissions

echo "Setup ONE monitor config"
CONF="/etc/contrail/contrail-one-monitor/one-monitor.config"
/srv/contegrator-test/edit-conf-file.sh $CONF \
	rabbit_mq_host=$HOST_NAME_head \
	log4j_log_level=info \
	one_admin_user=oneadmin \
	one_admin_password=310d133ccfaa2c1fbb0462c2beaa73cd

# We might have federation node, ow it can be a stand-alone ONE head + ONE worker test.
if [ -z "$HOST_NAME_federation" ]
then
	# no federation node
	/srv/contegrator-test/edit-conf-file.sh $CONF \
		send_to_monitoring_hub=0 \
		federation_finagle_host=localhost
else
	# we have federation node
	/srv/contegrator-test/edit-conf-file.sh $CONF \
		send_to_monitoring_hub=1 \
		federation_finagle_host=$HOST_NAME_federation
fi


#
