#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

BIN=/usr/bin/safeconfig

# get build tools
install_binary_always build-essential autoconf

# Copy binary safeconfig from tar.gz to /usr/local/bin
cd /srv/contegrator-test/safeconfig/
mkdir safeconfig-src
pushd safeconfig-src
tar xzvf ../contrail-safeconfig.tar.gz
./configure
make all
make test
cp ./src/safeconfig $BIN
popd

chown root:root $BIN
chmod 6755 $BIN

#
