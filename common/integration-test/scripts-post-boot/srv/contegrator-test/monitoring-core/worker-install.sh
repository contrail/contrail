#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# ruby1.9 etc installed in chroot
export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
echo "Install apt packages"
# apt-get -q -y --force-yes install sun-java6-jre # partner repo
apt-get -q -y --force-yes install rabbitmq-server
echo "Install rubygems"
# gem, gem1.9, gem1.9.1
GEM_INSTALL='gem install -d --no-rdoc --no-ri'
$GEM_INSTALL json
$GEM_INSTALL xml-simple
if [ "$CONF_OS_VERSION" == "12.04" ]
then
	# (needs g++, sudo aptitude install g++)
	$GEM_INSTALL amqp --version 0.9.3
else
	$GEM_INSTALL amqp
fi
$GEM_INSTALL sinatra
$GEM_INSTALL thin
$GEM_INSTALL sourcify

# fix message:
# Invalid gemspec in [/var/lib/gems/1.8/specifications/tilt-1.3.3.gemspec]: invalid date format in specification: "2011-08-25 00:00:00.000000000Z"
if [ -d /var/lib/gems/1.8/specifications/ ];
then
	[ -n "`find /var/lib/gems/1.8/specifications/ -type f`" ] && \
	sed -i 's/ 00:00:00.000000000Z//' /var/lib/gems/1.8/specifications/*
fi
#
