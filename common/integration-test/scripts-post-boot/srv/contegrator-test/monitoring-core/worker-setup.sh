#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Setup rabbitmq"
rabbitmqctl change_password guest 123
# add user if required
rabbitmqctl list_users | grep '^contrail[[:space:]]' || \
	rabbitmqctl add_user contrail contrail
rabbitmqctl set_permissions contrail ".*"  ".*"  ".*"
rabbitmqctl list_permissions

# echo "Setup mysql - TODO"

echo "Copy config files"
# cp /usr/share/contrail/monitoring/one-sensor/config where ?
# cp /usr/share/contrail/monitoring/one-sensor/hostConfig where ?

#
