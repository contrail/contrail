#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
install_binary_always contrail-provider-accounting
#if [ ! -f /etc/contrail/contrail-provider-accounting/accounting.cfg ]
#then
#    mkdir -p /etc/contrail/contrail-provider-accounting/
#    echo "ERORR ERORR ERORR no /etc/contrail/contrail-provider-accounting/accounting.cfg"
#    cd /etc/contrail/contrail-provider-accounting
#    svn export svn://svn.forge.objectweb.org/svnroot/contrail/trunk/provider/accounting/src/main/config/accounting.cfg
#fi
}

do_targz() {
echo ""
}

# main
install_binary_always tomcat6
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
