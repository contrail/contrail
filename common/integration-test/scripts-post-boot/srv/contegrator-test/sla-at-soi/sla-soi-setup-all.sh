!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

PATH=$PATH:/srv/pax-runner-1.7.1/bin
export PATH
export SLASOI_HOME="/srv/contrail-2/trunk/provider/sla-at-soi-platform/common/osgi-config/"

# mysql
cd /srv/contrail-2/trunk/provider
mysql --user=root --password="" < sla-at-soi-platform/generic-slamanager/sla-registry/db/model.sql
mysql --user=root --password="" < sla-at-soi-platform/common/osgi-config/Integration/templates/db/business/dbmodel.sql
mysql --user=root --password="" < sla-at-soi-platform/common/osgi-config/Integration/templates/db/business/datamodel.sql
mysql --user=root --password="" < sla-at-soi-platform/business-manager/DB/bm-datamodel.sql

mysql --user=root --password="" << EOF
  create user 'slasoi'@'localhost' identified by 'slasoi';
  grant all privileges on *.* to 'slasoi'@'%' with grant option;
  grant all privileges on slasoi.* to 'slasoi'@'%' with grant option;
  grant all privileges on *.* to 'slasoi'@'%' identified by 'slasoi'
EOF

# rabbit
rabbitmqctl add_user slasoi slasoi
rabbitmqctl list_users
#
rabbitmqctl add_vhost /slasoi
rabbitmqctl list_vhosts
#
rabbitmqctl set_permissions  slasoi  -p /slasoi ".*" ".*" ".*"
rabbitmqctl set_permissions  slasoi  -p / ".*" ".*" ".*"
rabbitmqctl list_user_permissions slasoi
#

# edit / check
# common/osgi-config/generic-slamanager/sla-registry/db/gslam-slaregistry.cfg.xml
# common/osgi-config/generic-slamanager/template-registry/db.properties
# common/osgi-config/BusinessManager/db.properties
# common/osgi-config/bmanager-postsale-reporting/DB/db.properties

sed -i 's/10.15.5.52/localhost/g' common/osgi-config/contrail-slamanager/provisioning-adjustment/eventbus.properties
sed -i 's|ovf_repo_path=/home/adcontrail/contrail-svn/sla-at-soi/platform/trunk/common/osgi-config/|ovf_repo_path=/srv/contrail-2/trunk/provider/sla-at-soi-platform/common/osgi-config/|' common/osgi-config/contrail-slamanager/provisioning-adjustment/provisioning_adjustment.properties
sed -i 's/10.15.5.52/localhost/g' common/osgi-config/contrail-slamanager/provisioning-adjustment/provisioning_adjustment.properties

#common/osgi-config/slams/contrail.instance1.cfg
#  localhost is ok ?

