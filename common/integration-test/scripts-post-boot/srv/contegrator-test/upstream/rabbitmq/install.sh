#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# ruby1.9 etc installed in chroot
export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
echo "Install apt packages"
apt-get -q -y --force-yes install rabbitmq-server

#
