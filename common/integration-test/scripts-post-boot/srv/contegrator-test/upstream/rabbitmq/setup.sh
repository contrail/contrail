#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Currently, we have same setup for federation and ONE head node.
echo "Setup rabbitmq"
# rabbitmqctl change_password guest 123
rabbitmqctl change_password guest guest
# add user if required
rabbitmqctl list_users | grep '^contrail[[:space:]]' || \
	rabbitmqctl add_user contrail contrail
rabbitmqctl set_permissions contrail ".*"  ".*"  ".*"
rabbitmqctl list_permissions

#
