#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Install galera load balancer

cd /srv/contegrator-test/upstream/mysql-galera/
cp glbd.init.d /etc/init.d/glbd
(cd /etc/rc2.d && ln -s ../init.d/glbd S80gldb)
install_binary_always build-essential

wget www.codership.com/files/glb/glb-1.0.0rc1.tar.gz
tar xzvf glb-1.0.0rc1.tar.gz
ln -s glb-1.0.0rc1 glb
cd glb
./configure && make && make install
which glbd
