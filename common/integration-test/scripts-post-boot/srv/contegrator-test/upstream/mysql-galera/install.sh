#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Install mysql-galera 

cd /srv/contegrator-test/upstream/mysql-galera/
install_binary_always libaio1 libssl0.9.8
install_binary_always psmisc libdbi-perl libdbd-mysql-perl perl
install_binary_always mysql-client # standard client and libs.
install_binary_always rsync

# wget https://launchpad.net/codership-mysql/5.5/5.5.29-23.7.3/+download/mysql-server-wsrep-5.5.29-23.7.3-amd64.deb
# wget https://launchpad.net/galera/2.x/23.2.4/+download/galera-23.2.4-amd64.deb
# dpkg -i mysql-server-wsrep-5.5.29-23.7.3-amd64.deb
# dpkg -i galera-23.2.4-amd64.deb
#
wget https://launchpad.net/codership-mysql/5.6/5.6.14-25.1/+download/mysql-server-wsrep-5.6.14-25.1-amd64.deb
wget https://launchpad.net/galera/3.x/25.3.2/+download/galera-25.3.2-amd64.deb
dpkg -i mysql-server-wsrep-5.6.14-25.1-amd64.deb
dpkg -i galera-25.3.2-amd64.deb

cp /etc/init.d/mysql /etc/init.d/mysql.orig
cp -f /srv/contegrator-test/upstream/mysql-galera/mysql.init.d /etc/init.d/mysql
# log dir used by deafult, but missing
mkdir -p /var/log/mysql/
chmod 777 /var/log/mysql/
