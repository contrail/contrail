#!/bin/bash

# $1 - dns name of peer node

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

PEERNAME=$1
if [ -z "$PEERNAME" ] ; then
	echo "PEERNAME / S1 is empty"
	exit 1
fi
MYIP=`dig +short $HOST_NAME`
echo "INFO: mysql-galera me: $HOST_NAME ($MYIP)"
PEERNAME=`eval echo '$'"HOST_NAME_$1"`
PEERIP=`dig +short $PEERNAME`
echo "INFO: mysql-galera peer: $PEERNAME ($1: $PEERIP)"

function prim_ip_addr() {
PRIM_ETH=`ip route | grep '^default' | head -n1 | awk '{print $5}'`
PRIM_IP=`ip addr show dev $PRIM_ETH | grep '[[:space:]]inet[[:space:]].*global*' | awk '{print $2}' | sed 's|/.*$||'`
}

# Setup wsrep node.
DATE=`date +%Y%m%d-%H%M%S`

CFG=/etc/mysql/my.cnf
cp $CFG $CFG.$DATE
sed -i 's/^query_cache_limit/#### query_cache_limit/g' $CFG
sed -i 's/^query_cache_size/#### query_cache_size/g' $CFG

CFG=/etc/mysql/conf.d/wsrep.cnf
cp $CFG $CFG.$DATE
sed -i \
 -e 's/#wsrep_cluster_address=/wsrep_cluster_address=/' \
 -e 's/#wsrep_node_name/wsrep_node_name/' \
 -e 's/#wsrep_node_address/wsrep_node_address/' \
 -e 's/#wsrep_node_incoming_address/wsrep_node_incoming_address/' \
 -e 's/#wsrep_sst_receive_address/wsrep_sst_receive_address/' \
 $CFG
sed -i \
 -e 's/#wsrep_provider_options=/wsrep_provider_options="gcache.size=100M; gcache.page_size=10M" /' \
 $CFG
/srv/contegrator-test/edit-conf-file.sh $CFG \
 wsrep_provider=/usr/lib/galera/libgalera_smm.so \
 wsrep_cluster_name='blah_cluster' \
 wsrep_cluster_address=gcomm://${PEERIP} \
 wsrep_sst_method=rsync \
 wsrep_node_name=$HOST_NAME \
 wsrep_node_address=$MYIP \
 wsrep_node_incoming_address=${MYIP} \
 wsrep_sst_receive_address=${MYIP} 

