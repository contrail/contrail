#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Setup galera load balancer
echo "SQL_SERVERS=\"$HOST_NAME_federation:3306 $HOST_NAME_head:3306\"" >> /etc/default/glbd
