#!/bin/bash
# chkconfig: 2345 55 25

SQL_SERVERS="localhost:3306"
if [ -f /etc/default/glbd ]
then
	source /etc/default/glbd
fi

STARTCMD="glbd 0.0.0.0:3307 $SQL_SERVERS -v -d"
SEARCHCMD='glbd 0.0.0.0:3307'

case $1 in
    start)
        echo "starting $0..."
        $STARTCMD
        echo -e 'done.'
    ;;
    stop)
        echo "stopping $0..."
        pkill -f "$SEARCHCMD"
        (
        MYPID=`pgrep -f "$SEARCHCMD"` && (echo -n '.'; sleep 1; \
        MYPID=`pgrep -f "$SEARCHCMD"` && (echo -n '.'; sleep 1; \
        MYPID=`pgrep -f "$SEARCHCMD"` && (echo -n '.'; sleep 2; \
        MYPID=`pgrep -f "$SEARCHCMD"` && (echo -n '.'; sleep 5; \
        MYPID=`pgrep -f "$SEARCHCMD"` && (echo -n '.'; sleep 5; \
        ))))) ; echo 'x'; )
        echo -e 'done.'
    ;;
    restart)
        $0 stop
        sleep 2
        $0 start
    ;;
#    reload)
#        MYPID=`pgrep -f "$SEARCHCMD"`
#        #echo 'pid $MYPID'
#        #ps afx | grep "^$MYPID[[:space:]]"
#        kill -USR2 $MYPID
#    ;;
    status)
        # ps afxu | grep glbd
        pgrep -f "$SEARCHCMD"
    ;;
    *)
        echo "usage: $0 (start|stop|restart)"
    ;;
esac

