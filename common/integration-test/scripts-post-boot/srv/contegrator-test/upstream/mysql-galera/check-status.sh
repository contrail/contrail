#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

mysql -e "SHOW STATUS LIKE 'wsrep%'"
mysql -e "SHOW STATUS LIKE 'wsrep%'" | grep -e 'wsrep_cluster_size' -e 'wsrep_ready'
