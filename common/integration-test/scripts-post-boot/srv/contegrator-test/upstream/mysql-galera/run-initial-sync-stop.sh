#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# 
echo "Stopping mysql-galera PID `pgrep mysqld`"
killall mysqld || true
echo "Stopping rsync PID `pgrep -f 'rsync --daemon --port 4444 --config'`"
pkill -f 'rsync --daemon --port 4444 --config' || true
sleep 10
