#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
echo "Install apt packages"
apt-get -q -y --force-yes install git daemon unzip

echo "Install kestrel..."
# mkdir /usr/local/kestrel # used in kestrel.sh
cd /usr/local/
wget http://robey.github.com/kestrel/download/kestrel-2.1.5.zip
unzip kestrel-2.1.5.zip
ln -s kestrel-2.1.5 kestrel
cd kestrel
# run
# java -jar kestrel-2.1.6-SNAPSHOT.jar
# or
chmod a+x ./scripts/*.sh
ln -s . current
(cd /etc/init.d && cp /usr/local/kestrel/scripts/kestrel.sh kestrel)
sed -i 's|^HEAP_OPTS=.*$|HEAP_OPTS="-Xmx1024m -Xms1024m -XX:NewSize=256m"|' /etc/init.d/kestrel
(cd /bin && ln -s `which java`)
(cd /usr/local/bin && ln -s `which daemon`)
mkdir -p /var/run/kestrel
mkdir -p /var/lib/kestrel
mkdir -p /var/log/kestrel
# /etc/init.d/kestrel start
# on shutdown
# ERR [20120315-20:11:39.515] kestrel: Background process background-expiration died with unexpected exception: java.lang.NullPointerException
#
