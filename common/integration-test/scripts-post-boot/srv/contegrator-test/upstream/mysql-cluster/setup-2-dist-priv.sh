#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Setup distributed privileges.
# Execute script on each SQL node.
# http://dev.mysql.com/doc/refman/5.5/en/mysql-cluster-privilege-distribution.html
cd /usr/local/mysql/

mysql -uroot < share/ndb_dist_priv.sql
mysql -e "CALL mysql.mysql_cluster_move_privileges();"
mysql -e "SELECT CONCAT('Conversion ', IF(mysql.mysql_cluster_privileges_are_distributed(), 'succeeded', 'failed'), '.') AS Result;"
