#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Setup mgmt node.
cd /usr/local/mysql/

cp /srv/contegrator-test/upstream/mysql-cluster/mysql.mgmd /etc/init.d/
chmod a+x /etc/init.d/mysql.mgmd
(cd /etc/rc2.d/ && ln -s ../init.d/mysql.mgmd S80mysql.mgmd )


cat <<EOF > /usr/local/mysql/config.ini
[ndb_mgmd]
hostname=${HOST_NAME_federation}
datadir=  /var/lib/mysql-cluster/
NodeId=1

[ndbd default]
noofreplicas=2
datadir=/var/lib/mysql-cluster/

[ndbd]
hostname=${HOST_NAME_federation}
NodeId=3

[ndbd]
hostname=${HOST_NAME_head}
NodeId=4

[mysqld]
hostname=${HOST_NAME_federation}
NodeId=5

[mysqld]
hostname=${HOST_NAME_head}
NodeId=6
EOF

