#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Common for mgmt, ndbd and sql node.

cd /srv/contegrator-test/upstream/mysql-cluster/
# dummy mysql-server - to satisfy federation-db prerequisite
wget http://contrail.xlab.si/mysql-server-dummy/mysql-server-dummy-0.1.deb
dpkg -i mysql-server-dummy-0.1.deb
install_binary_always mysql-client # standard client and libs.

apt-get install libaio1 # required for mysql-cluster
apt-get install daemon # init.d/mysql.ndbd script

cd /usr/local/
wget http://cdn.mysql.com/Downloads/MySQL-Cluster-7.2/mysql-cluster-gpl-7.2.10-linux2.6-x86_64.tar.gz
tar xzvf mysql-cluster-gpl-7.2.10-linux2.6-x86_64.tar.gz
ln -s mysql-cluster-gpl-7.2.10-linux2.6-x86_64 mysql

# for init.d/mysql.server from the tar ball
(cd /usr/bin/ && ln -sf /usr/local/mysql/bin/mysqld_safe)
mkdir /usr/share/mysql/ && cp /usr/local/mysql/share/english/errmsg.sys /usr/share/mysql/

mkdir /var/lib/mysql-cluster
echo '/usr/local/mysql/lib/' > /etc/ld.so.conf.d/mysql-cluster.conf
ldconfig -v
# echo 'PATH=$PATH:/usr/local/mysql/bin/; export PATH' >> ~/.bashrc
# . ~/.bashrc

groupadd --system mysql
useradd --system -g mysql mysql

