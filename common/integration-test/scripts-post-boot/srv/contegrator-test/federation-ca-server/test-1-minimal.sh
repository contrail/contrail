#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Both versions wants to read stdin...
# And jar compiled on ubuntu 11.04 works on that machine only?
# old version
#get-user-cert -T /etc/ssl/certs/java/cacerts -U changeit -u coordinator -p '$2a$06$DCq7YPn5Rq63x1Lad4cll.kD3zZ845LsvMekyowTQk2VNmbDdQsWO' https://contrail-ca-server:8443/ca/user
# new version
#get-user-cert -T /etc/ssl/certs/java/cacerts -U changeit -u coordinator -p password https://contrail-ca-server:8443/ca/user
