#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# get get-user-cert utility
cd /srv/contegrator-test/federation-ca-server/files
tar -xvzf contrail-security-commons.tar.gz --strip 1 -C /
