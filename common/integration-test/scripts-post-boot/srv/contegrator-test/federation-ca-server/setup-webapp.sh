#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# replace password
sed -i 's/changeme/tomcat/g' /var/lib/tomcat6/webapps/ca/WEB-INF/web.xml
# add connector for 8443 port
cp /var/lib/tomcat6/conf/server.xml /var/lib/tomcat6/conf/server.xml.orig
patch /var/lib/tomcat6/conf/server.xml < /srv/contegrator-test/federation-ca-server/files/server.xml.patch
# password again
cp /var/lib/tomcat6/webapps/ca/WEB-INF/classes/META-INF/persistence.xml /var/lib/tomcat6/webapps/ca/WEB-INF/classes/META-INF/persistence.xml.orig
sed -i 's/\*\*\*\*\*\*\*\*/contrail/g' /var/lib/tomcat6/webapps/ca/WEB-INF/classes/META-INF/persistence.xml
