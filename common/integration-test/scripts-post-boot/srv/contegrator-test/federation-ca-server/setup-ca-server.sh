#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Create CA’s and ca-server’s certificate and key
cd /srv/contegrator-test/federation-ca-server/files
  # rm -f cert/*
  # ./openssl-gen-cert
# Create truststore
cat cert/contrail-ca-server.key cert/contrail-ca-server.cert > cert/contrail-ca-server.certkey
openssl pkcs12 -export -in cert/contrail-ca-server.certkey -out cert/ks.p12 -name contrail -noiter -nomaciter -passout pass:contrail
# keytool -import -file cert/ca.cer -alias contrailCA -keystore cert/contrailTrustStore -noprompt -storepass contrail -keypass contrail

# Create ContrailRoot CA Cert
rm -fr ca-server
rm -fr .svn
svn co svn://svn.forge.objectweb.org/svnroot/contrail/trunk/common/ca-server
cd ca-server/src/main/resources
mkdir -p /etc/contrail/ca-server/
mkdir -p /var/lib/contrail/ca-server/
chmod +x add-trusted-ca
chmod a+x create-rootca-files
cp add-trusted-ca /usr/local/sbin
cp ./create-rootca-files.conf /etc/contrail/ca-server/
./create-rootca-files /DC=Slovenia/DC=Contrail/DC=ca/DC=users
ls -la /var/lib/contrail/ca-server/

cd /srv/contegrator-test/federation-ca-server/files
cp cert/ks.p12 /var/lib/contrail/ca-server/
cp cert/contrailTrustStore /var/lib/contrail/ca-server/
ls -la /var/lib/contrail/ca-server/
