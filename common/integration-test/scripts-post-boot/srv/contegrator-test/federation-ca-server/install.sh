#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
install_binary_always contrail-ca-server
}

do_targz() {
do_binary
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

