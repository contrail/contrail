#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd 
set -x

echo "Run provider-storage-manager"

do_binary() {
/etc/init.d/contrail-provider-storage-manager stop
sleep 5
/etc/init.d/contrail-provider-storage-manager start
sleep 5
}

do_targz() {
nohup java -cp /usr/share/contrail/provider/storage-manager/storage-manager-0.1-SNAPSHOT-jar-with-dependencies.jar:/usr/share/contrail/provider/storage-manager/lib org.ow2.contrail.provider.storagemanager.Main --config /etc/contrail/contrail-provider-storage-manager/storage-manager.cfg 1>/var/log/contrail/storage-manager.nohup.log 2>&1 &
sleep 10
}

# main
/etc/init.d/mongodb restart # changed conf bind ip
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
