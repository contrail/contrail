#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
install_binary_always mongodb
install_binary_always contrail-provider-storage-manager
}

do_targz() {
install_binary_always mongodb
install_binary_always contrail-provider-storage-manager
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
