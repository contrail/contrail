#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
# use mysql cluster if we have two nodes in test (ONE head in addition to federation node)
# 
if [ -f "/usr/local/mysql/config.ini" ]
then
	echo "Federation DB will use MySQL cluster"
	export CONTRAIL_FEDERATION_DB_USE_MYSQL_CLUSTER=1
	install_binary_always contrail-federation-db
else
	echo "Federation DB will use standard MySQL server"
	install_binary_always contrail-federation-db
fi
}

do_targz() {
install_binary_always mysql-server
export DEBIAN_FRONTEND=noninteractive
/usr/share/contrail/federation-db/mysql-setup.sh
}

if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	# installing deb
	do_binary
else
	# unpacking tar.gz
	do_targz
fi
