#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Run monitoring-hub"

do_binary() {
/etc/init.d/contrail-monitoring-hub restart
}

do_targz() {
nohup /usr/share/contrail/federation/monitoring-hub/run >> /var/log/contrail/monitoring-hub.nohup.log 2>&1 &
sleep 10
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
