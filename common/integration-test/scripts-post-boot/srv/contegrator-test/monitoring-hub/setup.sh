#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# sbt update
CONFIG=/etc/contrail/contrail-monitoring-hub/config.json
cp $CONFIG $CONFIG.orig

/srv/contegrator-test/common/edit-json.sh $CONFIG \
	rabbit,enabled=true \
	rabbit,host=$HOST_NAME_federation

# fix init.d
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	sed -i 's|.jar:."  org.ow2.contrail.monitoring.hub.HubServer|.jar:." -Dhub.config=/etc/contrail/contrail-monitoring-hub/config.json org.ow2.contrail.monitoring.hub.HubServer|' /usr/bin/contrail-monitoring-hub
fi
#

