#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

if [ -z $1 ]
then
	echo "Usage $0 'node1 node2' "
	echo "node1 etc are contegrator node names - head for ct_node_list[\"head\"] etc"
	exit 1
fi

# TODO:
HOST_NAME_FQDN="$HOST_NAME.xc2.xlab.lan"
# SERVERS="$HOST_NAME_head.xc2.xlab.lan "
SERVERS=""
NODES=$1
for NODE in $NODES
do
	var_name=HOST_NAME_$NODE
	srv_name=`eval echo '$'$var_name`
	srv_name=${srv_name}.xc2.xlab.lan
	SERVERS+=" $srv_name"
done

rm /etc/zookeeper/conf -f # only a symlink
cp -a /etc/zookeeper/conf_example /etc/zookeeper/conf
echo "bla" > /etc/zookeeper/conf/myid
ID=1
for SERVER in $SERVERS
do
	echo "server.$ID=$SERVER:2888:3888" >> /etc/zookeeper/conf/zoo.cfg
	if [ "$SERVER" == "$HOST_NAME_FQDN" ]
	then
		echo $ID > /etc/zookeeper/conf/myid
	fi
	ID=$(($ID + 1))
done

#

