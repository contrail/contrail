#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# test.sh uses curl. shasum comes with perl
install_binary_always curl perl

do_binary() {
install_binary_always contrail-federation-api
}

do_targz() {
install_targz_dependency tomcat6
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

