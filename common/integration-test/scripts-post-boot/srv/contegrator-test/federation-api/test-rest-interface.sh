#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

cd /srv/contegrator-test/federation-api/
svn export svn://svn.forge.objectweb.org/svnroot/contrail/trunk/federation/federation-api/src/main/resources/scripts/rest_example.sh

# john would get id 3 - not 1, roles are already present...
# ./rest_example.sh $HOST_NAME_federation 8080

#

