#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# to avoid error: pci_add_option_rom: failed to find romfile "pxe-virtio.bin"
if [ "$CONF_OS_NAME" == "ubuntu" ] && [ "$CONF_OS_VERSION" \< "12.04" ]
then
	install_binary_always kvm-pxe
fi

# get required jar/class files
cd / && tar -xzvf /srv/contegrator-test/vin/vin-support.tar.gz

INTERFACESCRIPTSDIR=/usr/share/contrail/support/vin/interface-scripts
CONFIGDIR=/usr/share/contrail/support/vin/interface-configs
ONEHOOKSDIR=/usr/share/contrail/support/vin/one-scripts

chown root $INTERFACESCRIPTSDIR/*
chmod 4755 $INTERFACESCRIPTSDIR/*
chmod +x $ONEHOOKSDIR/*
chown root.root $CONFIGDIR/*

modprobe -v dummy
modprobe -v ip_gre
#
