#!/bin/bash

# -------------------------------------------------------------------------- #
# Copyright 2002-2011, OpenNebula Project Leads (OpenNebula.org)             #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
#--------------------------------------------------------------------------- #

echo "Starting VM contextualization" > /var/log/context.log
set -x

if [ -f /mnt/context.sh ]
then
  . /mnt/context.sh
fi


echo $HOSTNAME > /etc/hostname
hostname $HOSTNAME
sed -i "/127.0.1.1/s/ubuntu/$HOSTNAME/" /etc/hosts

if [ -n "$IP_PUBLIC" ]; then
	ifconfig eth0 $IP_PUBLIC
fi
 
if [ -n "$NETMASK" ]; then
	ifconfig eth0 netmask $NETMASK
fi


if [ -f /mnt/$ROOT_PUBKEY ]; then
	mkdir -p /root/.ssh
	cat /mnt/$ROOT_PUBKEY >> /root/.ssh/authorized_keys
	#chmod -R 600 /root/.ssh/
	chmod 600 /root/.ssh/authorized_keys
	chmod 700 /root/.ssh
fi

if [ -n "$USERNAME" ]; then
	useradd -s /bin/bash -m $USERNAME
	if [ -f /mnt/$USER_PUBKEY ]; then
		mkdir -p /home/$USERNAME/.ssh/
		cat /mnt/$USER_PUBKEY >> /home/$USERNAME/.ssh/authorized_keys
		chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
		chmod 600 /home/$USERNAME/.ssh/authorized_keys
	fi
fi

# added:
if [ -n "$GATEWAY" ]; then
        # Change gateway from default; note eth0 is already active
        ( echo Orig network:
          ifconfig -a
          netstat -r
          echo Now set GATEWAY to $GATEWAY
        ) >> /var/log/context.log 2>&1

        ifdown eth0
	# Update GATEWAY and NETMASK configs in ifcfg-eth0
	# CentOS
        # mv /etc/sysconfig/network-scripts/ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0.orig
        # sed -e "s/ GATEWAY=.*/ GATEWAY=$GATEWAY/" -e "s/ NETMASK=.*/ NETMASK=$NETMASK/" < /etc/sysconfig/network-scripts/ifcfg-eth0.orig > /etc/sysconfig/network-scripts/ifcfg-eth0
        # cp /etc/sysconfig/network-scripts/ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0.sav
	#
	# Debian with single eth0, so sed can replace all lines
	cp /etc/network/interfaces /etc/network/interfaces.orig
	sed -i -e "s/gateway.*/gateway $GATEWAY/" -e "s/netmask.*/netmask $NETMASK/" /etc/network/interfaces
	cp /etc/network/interfaces /etc/network/interfaces.sav
        ifup eth0

        ( echo current network:
          ifconfig -a
          netstat -r
        ) >> /var/log/context.log 2>&1
fi

# added:
if [ -n "$DNS" ]; then
        echo "Setting DNS server to $DNS" >>/var/log/context.log
        echo "nameserver $DNS" >/etc/resolv.conf
fi

# justinc
export DEBIAN_FRONTEND=noninteractive
apt-get update 1>/dev/null
apt-get -q -y --force-yes install pciutils
apt-get -q -y --force-yes install kvm-pxe
modprobe -v ipip
modprobe -v gre
modprobe -v ip_gre
# modules required for network interface hotplug
modprobe -v acpiphp
modprobe -v pci_hotplug
lsmod

if [ -f /mnt/vin-support.tar.gz ]; then
    VIN_DIRECTORY=/usr/share/contrail/support/vin
    (cd / && tar xvzf /mnt/vin-support.tar.gz)

    INTERFACEDIR=$VIN_DIRECTORY/interface-configs
    # Make the whole lot non-executable and SUID root
    chmod 6755 $INTERFACEDIR/*

    SCRIPTSDIR=$VIN_DIRECTORY/interface-scripts
    # Make the whole lot executable 
    chmod a+x $SCRIPTSDIR/*

apt-get -q -y --force-yes install strongSwan openjdk-6-jre-headless
echo "include /var/lib/contrail/vin/strongSwan/*.conf" >> /etc/ipsec.conf
#
mkdir /var/log/contrail
#
cp /mnt/strongswan-ca/ca-cert.der /etc/ipsec.d/cacerts
cp /mnt/strongswan-ca/one-worker1-cert.der /etc/ipsec.d/certs/
cp /mnt/strongswan-ca/one-worker1-key.der /etc/ipsec.d/private/
cp /mnt/strongswan-ca/one-worker2-cert.der /etc/ipsec.d/certs/
cp /mnt/strongswan-ca/one-worker2-key.der /etc/ipsec.d/private/
# one cert for multiple clients
echo ": RSA /etc/ipsec.d/private/one-worker1-key.der" >> /etc/ipsec.secrets
ipsec restart # load new certs
#
(cd / && ln -sf /usr/share/contrail)
# Jar-files from library.
LIBCLASSPATH="${VIN_DIRECTORY}/logconfig"
add_to_libclasspath () {
    JARFILES=`cd "$1" && ls *.jar 2>/dev/null`
    for i in ${JARFILES} ; do
        if [ -z "$LIBCLASSPATH" ] ; then
            LIBCLASSPATH="$1/$i"
        else
            LIBCLASSPATH="$LIBCLASSPATH:$1/$i"
        fi
    done
}
# Add the jar files in the VideoPlayer lib dir to the classpath.
#add_to_libclasspath "${VIN_DIRECTORY}"/used-jars/ipl
#add_to_libclasspath "${VIN_DIRECTORY}"/used-jars
add_to_libclasspath "${VIN_DIRECTORY}"/jars
#pushd "${VIN_DIRECTORY}"

    echo 1 > /proc/sys/net/ipv4/ip_forward
    echo "Starting VIN agent: VIN_POOL_NAME=$VIN_POOL_NAME VIN_IBIS_SERVER=$VIN_IBIS_SERVER VIN_VMID=$VIN_VMID" >>/var/log/context.log
    # FIXME: should run as non-root
    /usr/bin/java \
        -server \
        -Dibis.pool.name=$VIN_POOL_NAME \
        -Dibis.server.address=$VIN_IBIS_SERVER \
        -enableassertions \
        -classpath "${CLASSPATH-""}:$LIBCLASSPATH" \
        org.ow2.contrail.resource.vin.agent.VinAgent \
        -configDir=$INTERFACEDIR $VIN_VMID INVM >> /var/log/contrail/vin-agent-`hostname`.log 2>&1 &
fi
#popd
echo "Completed VM contextualization" >> /var/log/context.log
