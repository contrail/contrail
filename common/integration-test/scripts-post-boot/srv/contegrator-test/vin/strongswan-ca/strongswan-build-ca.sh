#!/bin/bash

# exit on error
set -e
# echo cmd
set -x

# justinc, recreate sample strongswan certs
exit 0 # requires strongswan package, which includes daemon...

create_peer() {
PEER=$1
	ipsec pki --gen > ${PEER}-key.der
	ipsec pki --pub --in ${PEER}-key.der | ipsec pki --issue --cacert ca-cert.der --cakey ca-key.der --dn "C=CH, O=strongSwan, CN=${PEER}" > ${PEER}-cert.der
}

TARGET_DIR="/srv/contegrator-test/vin/strongswan-ca"
mkdir ${TARGET_DIR}
cd ${TARGET_DIR}
ipsec pki --gen > ca-key.der
ipsec pki --self --in ca-key.der --dn "C=CH, O=strongSwan, CN=strongSwan CA" --ca > ca-cert.der

create_peer one-head
create_peer one-worker1
create_peer one-worker2

#

