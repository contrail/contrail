#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# whoami, get VNFS name
# "10.31.1.253:/usr/var/lib/perceus//vnfs/test-vnfs-vin-0.2-SNAPSHOT-head/rootfs on / type nfs ... "
# head, worker1, worker2 
NODE_NAME=`mount | grep ' on / type' | sed "s|/|\n|g" | grep '^test-vnfs-' | sed "s/-/\n/g" | tail -n1`

#
