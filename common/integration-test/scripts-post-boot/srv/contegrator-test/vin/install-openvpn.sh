#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -q -y --force-yes install openvpn

# Make sure the executable is there
ls -l /usr/sbin/openvpn
