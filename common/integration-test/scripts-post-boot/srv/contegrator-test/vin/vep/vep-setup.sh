#!/bin/bash
#arguments should be the list of host ids to add to the database.
#example: vep-setup.sh 0 1 2
source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x


#Is it really localhost for open nebula front end IP? Is there a global variable for this machine I could put there?
#ONE frontend ip
ONEIP=localhost
#VEP Ip, in case one wants to modify this script to use on a distant vep
VEPIP=$HOST_NAME_head:10500
#VEP Admin login
VEPADMIN=admin
#ONE admin username
ONEADMIN=oneadmin
#ONE admin password
ONEPASSWORD=310d133ccfaa2c1fbb0462c2beaa73cd

#Untar vep
tar xvfz vep.tar.gz

#Update vep.properties with correct values
echo "one.user=$ONEADMIN" >> vep.properties
echo "one.pass=$ONEPASSWORD" >> vep.properties
echo "one.ip=$ONEIP" >> vep.properties
sed -i '/contrail.cluster/d' vep.properties
echo "contrail.cluster=100" >> vep.properties

md5sum vep.properties

#Copy the vep.properties file at the right place
if [ -d "$HOME/.vep-gui/" ]; then
	/bin/cp -vf ./vep.properties $HOME/.vep-gui
else
	mkdir $HOME/.vep-gui/
	/bin/cp -vf ./vep.properties $HOME/.vep-gui
fi
md5sum ./vep.properties $HOME/.vep-gui/vep.properties

#Test if script already launched before and vep is still there, if so, kill it.
if [ -f "jid.log" ]; then
	JID=$(head -1 jid.log)
	kill $JID || true
	rm jid.log
fi

# Seems ONE hosts should be already in contrail cluster.
/srv/contegrator-test/vep-cli/setup-one-head.sh

#Launch VEP
#java -jar VEPController.jar -t &
nohup java -jar VEPController.jar -t &
md5sum ./vep.properties $HOME/.vep-gui/vep.properties

#Save processus id to file
echo $! > jid.log

#Wait for VEP init
sleep 13

#Host addition to VEP db, use param $1, will fail if any error, could use expect package but unsure if installed
#Adds host id 1, belonging to rack id 1, using vorg id -1 (none), host of type 1 (opennebula), description is FirstHost, y to confirm. Hosts are static, there are only 3 hosts id 0, 1 and 2.
#IDs come from the two latest VIN tests console output I could find, can they be different?

echo 'md5sum nc-0'
nc localhost 10555 <<EOF
admin
password
list hosts
1
2
q
exit
EOF

echo 'md5sum nc-1'
md5sum ./vep.properties $HOME/.vep-gui/vep.properties

#sleep 3600 || true && exit 0

for hid in "$@"
do
nc localhost 10555 <<EOF
admin
password
add fedadmin
$hid
1
-1
1
Host1
y
exit
exit
y
EOF
done

echo 'md5sum nc-2'
md5sum ./vep.properties $HOME/.vep-gui/vep.properties

nc localhost 10555 <<EOF
admin
password
list hosts
1
2
q
exit
EOF

onecluster list
onehost list

echo 'md5sum nc-3'
md5sum ./vep.properties $HOME/.vep-gui/vep.properties

#ACL for Users to Use all Networks and Images, may give error (which is on stdout) if already exists
if [ "$ONEIP" = "localhost" ]; then
	# error 'already exists'
	su oneadmin -l -c "oneacl create \"@1 IMAGE+NET/* USE\"" || true
else
	ssh root@$ONEIP 'su oneadmin -l -c "oneacl create \"@1 IMAGE+NET/* USE\"" 1>/dev/null'
fi

#curl to create a user via Rest
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -H "X-Username: $VEPADMIN" -X PUT -d "{\"role\":\"admin\", \"vid\":\"-1\", \"groups\":[\"admin\"]}" http://$VEPIP/user/vin

# vep-cli test uses fedadmin user to add gregorb
curl -i -X PUT \
        --header "X-Username: $VEPADMIN" \
        --header "Accept: application/json" \
        --header "Content-type: application/json" \
        --data '{"role":"admin", "vid":"-1", "groups":["user", "admin"]}' \
        http://localhost:10500/user/gregorb
# federation-web expects fedadmin user ...
curl -i -X PUT \
        --header "X-Username: $VEPADMIN" \
        --header "Accept: application/json" \
        --header "Content-type: application/json" \
        --data '{"role":"admin", "vid":"-1", "groups":["user", "admin"]}' \
        http://localhost:10500/user/fedadmin
