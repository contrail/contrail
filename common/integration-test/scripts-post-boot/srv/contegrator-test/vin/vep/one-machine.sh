#!/bin/sh
set -e -u
set -x
VIN_HOME=/usr/share/contrail/support/vin/
TEST_FILES_DIR=/srv/contegrator-test/vin/vep

# Jar-files from library.
LIBCLASSPATH="$TEST_FILES_DIR/logconfig"
add_to_libclasspath () {
    JARFILES=`cd "$1" && ls *.jar 2>/dev/null`
    for i in ${JARFILES} ; do
        LIBCLASSPATH="$LIBCLASSPATH:$1/$i"
    done
}

add_to_libclasspath "${VIN_HOME}"/jars

# And finally, run ...
echo "Starting VIN controller for VEP test"
java \
    -server \
    -classpath bin:"${CLASSPATH-""}:$LIBCLASSPATH" \
    -enableassertions \
     org.ow2.contrail.resource.vin.controller.tester.CommandLine -p=$TEST_FILES_DIR/controller.properties $TEST_FILES_DIR/$*
echo "VIN controller has stopped"
