#!/bin/bash
#
# install libvirt-bin, because the VIN interface scripts need 'virsh'.

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

export DEBIAN_FRONTEND=noninteractive
apt-get update
if [ -z "`which virsh`" ]
then
	apt-get -q -y --force-yes install libvirt-bin
fi
if [ "$CONF_OS_NAME" == "ubuntu" ] && [ "$CONF_OS_VERSION" \< "12.04" ]
then
	apt-get -q -y --force-yes install kvm-pxe
fi

