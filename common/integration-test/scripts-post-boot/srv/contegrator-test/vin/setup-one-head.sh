#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# add hook to start VIN agent

ONEHOOKSDIR=/usr/share/contrail/support/vin/one-scripts

cat >> /etc/one/oned.conf <<EOF

# Contrail hooks, appended by $0
VM_HOOK = [
    name = "VINHookScript",
    on = "RUNNING",
    remote = "yes",
    command = "$ONEHOOKSDIR/onehook-start-agent",
    arguments = "\$VMID \$TEMPLATE"
]

EOF

echo "/etc/one/oned.conf is now:"
echo "-----------------------------"
cat /etc/one/oned.conf
echo "-----------------------------"

# one VIN VM per host
cp -a /etc/one/sched.conf /etc/one/sched.conf.back
sed -i 's/^[[:space:]]policy = 1$/policy = 3, rank   = "- (RUNNING_VMS * 50  + FREE_CPU * 0)"/' /etc/one/sched.conf
echo "/etc/one/sched.conf is now:"
echo "-----------------------------"
cat /etc/one/sched.conf
echo "-----------------------------"

#
