#!/bin/bash

# $1 - File
# Line in conf file is "rabbit_mq_host = n0003"
# Set $2 = 'rabbit_mq_host=asdf' - no spaces!

# exit on error
set -e
# echo cmd
# set -x

DATE=`date +%Y%m%d-%H%M%S`

FILE=$1
shift

echo "Modify config file $FILE"
cp $FILE $FILE.back-$DATE
for PARAM in $@
do
	KEY=`echo $PARAM | sed 's/=.*$//'`
	NEW_VALUE=`echo $PARAM | sed "s/$KEY=//"`
        NEW_LINE="$KEY = $NEW_VALUE"
	echo "  $NEW_LINE"
	sed -i "s|^$KEY[[:space:]]*=.*$|$NEW_LINE|" $FILE
        grep -q "^$NEW_LINE\$" $FILE  || echo "Error: line replace failed"
done


#
