# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_setup_hosts(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/federation-ca-server/setup-hosts.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/federation-ca-server/setup-hosts.sh")
    ct_nodeman.testrun("root", worker, "/srv/contegrator-test/federation-ca-server/setup-hosts.sh")

def test_02_setup_ca_cert(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/federation-ca-server/setup-ca-server.sh")

def test_03_setup_webapp(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/federation-ca-server/setup-webapp.sh")

def test_04_setup_security_commons(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/federation-ca-server/setup-security-commons.sh")


#

