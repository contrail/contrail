# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_01_provider_storage_manager(ct_node_list, ct_nodeman):
    # federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/provider-storage-manager/install.sh")

#

