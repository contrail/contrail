# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_redis(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/redis/setup.sh")

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def dis_test_02_sbt(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/sbt/setup.sh")

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_03_kestrel(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/kestrel/setup.sh")

def test_04_monitoring_hub(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    # head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/monitoring-hub/setup.sh")

#

