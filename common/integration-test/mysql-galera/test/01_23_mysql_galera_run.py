# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
# restart to normal state
def test_03_run(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-galera/run-initial-sync-stop.sh")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-galera/run.sh")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-galera/check-status.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-galera/run-initial-sync-stop.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-galera/run.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-galera/check-status.sh")

#
