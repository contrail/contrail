# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_10_install_vep(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vep-cli/install.sh")

#

