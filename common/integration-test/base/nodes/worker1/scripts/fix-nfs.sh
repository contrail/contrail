#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# rm /var/lib/one-nfsshare/remotes/im/kvm.d/freespace.sh

# required for ONE worker node
echo "VNFS_NAME = $VNFS_NAME"
cp /etc/fstab /etc/fstab.orig
[ ! -z "$BASE_VNFS_NAME_head" ] && sed -e "s/$BASE_VNFS_NAME_head/$VNFS_NAME_head/g" </etc/fstab.orig >/etc/fstab

