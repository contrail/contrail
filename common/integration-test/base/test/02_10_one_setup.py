# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_02_setup_one(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    #worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/one-head/setup.sh")

def test_03_setup_one_dnsmasq(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    #worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/one-head/setup-one-dnsmasq.sh")


#

