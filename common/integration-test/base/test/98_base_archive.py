# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_archive(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    #worker1 = ct_node_list["worker1"]
    #worker2 = ct_node_list["worker2"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    for node_name in ct_node_list:
        node = ct_node_list[node_name] 
        ct_nodeman.getfile(node, "/var/log/syslog", "./archive/" + node_name + "/syslog.txt")
        ct_nodeman.getfile(node, "/var/log/dmesg", "./archive/" + node_name + "/dmesg.txt")
        ct_nodeman.getfile(node, "/var/log/contrail/", "./archive/" + node_name + "/var-log-contrail/")

    ct_nodeman.getfile(head, "/var/log/one/", "./archive/head/var-log-one/" )

#
