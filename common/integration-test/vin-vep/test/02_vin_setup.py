# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_03_edit_cofig(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/setup.sh")
    #
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/setup-worker.sh")
    ct_nodeman.testrun("root", worker1, "/srv/contegrator-test/vin/setup-worker.sh")
    ct_nodeman.testrun("root", worker2, "/srv/contegrator-test/vin/setup-worker.sh")

def test_03_edit_oned_conf(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    #worker1 = ct_node_list["worker1"]
    #worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/setup-one-head.sh")
#    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/setup-vep-head.sh")

#

