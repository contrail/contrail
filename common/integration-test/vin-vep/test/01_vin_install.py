# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_01_setup_linux(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    #worker1 = ct_node_list["worker1"]
    #worker2 = ct_node_list["worker2"]
    pass

def test_02_install_strongswan(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    # Choose between straigth strongswan or enhanced version.
    #installer = "/srv/contegrator-test/vin/install-strongswan.sh"
    installer = "/srv/contegrator-test/contrail-strongswan/install.sh"
    ct_nodeman.testrun("root", head, installer)
    ct_nodeman.testrun("root", worker1, installer)
    ct_nodeman.testrun("root", worker2, installer)

def test_03_install_libvirt(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/install-libvirt.sh")

def test_04_install_safeconfig(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/safeconfig/install.sh")
    ct_nodeman.testrun("root", worker1, "/srv/contegrator-test/safeconfig/install.sh")
    ct_nodeman.testrun("root", worker2, "/srv/contegrator-test/safeconfig/install.sh")

def test_05_install_vin(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/install-worker.sh")
    ct_nodeman.testrun("root", worker1, "/srv/contegrator-test/vin/install-worker.sh")
    ct_nodeman.testrun("root", worker2, "/srv/contegrator-test/vin/install-worker.sh")

def test_06_install_openvpn(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/install-openvpn.sh")
    ct_nodeman.testrun("root", worker1, "/srv/contegrator-test/vin/install-openvpn.sh")
    ct_nodeman.testrun("root", worker2, "/srv/contegrator-test/vin/install-openvpn.sh")

#

