# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_01_run_vin(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    #worker1 = ct_node_list["worker1"]
    #worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/vin/run.sh")

#

