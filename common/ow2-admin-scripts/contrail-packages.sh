#!/bin/bash
#
#    Manage Contrail binary packages
#
# Usage
# -----
#    contrail-packages [options] <command> [<args>...]
#
# Versions
# --------
#    2012-06-01 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Fixed ignored --email option
#    2012-04-29 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Fixed list_subprojects() to retry after 1st network timeout
#    2012-04-02 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Added the clone command to copy links and aggregates
#        * Fixed website URL in debian.control files
#    2012-03-27 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Copy only project packages, not links or aggregates
#        * Improved usage message with updated examples
#    2012-03-23 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Implemented the copy command
#        * Simplified command line interface
#    2012-03-22 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Skip linked and aggregated packages when updating
#        * Changed interactive shell prompt
#    2012-03-06 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Added DATADIR in variables set in debian.rules
#    2012-03-05 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Now set variables PACKAGE, LIBDIR, BINDIR in debian.rules
#        * Improved the interactive shell interface
#    2012-03-01 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Added notice message when starting the interactive subshell
#        * Fixed problem processing non versioned osc packages
#    2012-02-29 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Replaced option --revision (SVN) by --build-id (Bamboo)
#        * Replaced option --user by --maintainer
#        * Changed packages version incrementation method
#    2012-02-28 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Added --user, --email and --interactive options
#        * Implemented automatic update of debian configuration files
#    2012-02-27 Sebastien Andre <sebastien.andre@petalslink.com>
#        * Initial version of update-contrail-packages
#
set -o nounset -o pipefail -o errexit


declare -r NAME='contrail-packages'
declare -r VERSION='2012-06-01'


declare -r SVN_URL='svn://svn.forge.objectweb.org/svnroot/contrail'
declare -r BAMBOO_URL='http://bamboo.ow2.org/browse/CONTRAIL-TRUNK-%ID%/artifact/JOB1/Packages'
declare -r BAMBOO_RSS='http://bamboo.ow2.org/rss/createAllBuildsRssFeed.action?feedType=rssAll&buildKey=CONTRAIL-TRUNK&os_authType=basic'
declare -r OBS_PRJ='home:contrail'
declare -r CACHE_DIR="$(echo ~)/.cache/$NAME"

declare MAINTAINER='Contrail Administrators' EMAIL='contrail-admin@ow2.org'

declare -r USAGE="Usage: $NAME [options] <command> [<args>...]

Options:

   -v, --version STR
        Set the version string of the packages
   -b, --build-id NUMBER
        Select Bamboo build NUMBER
   -c, --cleanup
        Empty the local cache directory before starting.
   -m, --maintainer NAME
        Set the maintainer's name (default ${MAINTAINER})
   -e, --email EMAIL
        Set the maintainer's email (default ${EMAIL})
   -i, --interactive
        Allow manual modification of files before commit


Commands:

   update <subproject> [<package>...]
       Rebuild every <package> (default all) from
       <subproject> for the latest successful Bamboo build.

   copy <source> <target> [<package>...]
       Copy <package> (default all) from <source> subproject
       to <target>, ignoring linked and aggregated packages.

   clone <source> <target> [<package>...]
       Copy <package> (default all) from <source> subproject
       to <target>, including linked and aggregated packages.

Examples:

   # running nightly builds
   $NAME update testing

   # staging release 1.0~RC2
   $NAME copy testing staging
   $NAME -b 840 -v 1.0~RC2 update staging

   # publishing a staged release
   $NAME copy staging release

   # staging only bug fixes of package conpaas-web 1.0.0
   $NAME -v 1.0.1 -b 844 update staging conpaas-web
"



# Print an info message
function info () {
	echo ":: $(date '+%F %T') [$NAME] $*" >&2
}

# Print a warning message
function warn () {
	info "WARNING: $*" >&2
}

# Terminate the script with an error message
function fail () {
	info "ERROR: ${1:-failed}" >&2
	exit ${2:-1}
}


# Overwrite a file with content read from STDIN for inplace updates
function ow () {
	local f="${1?}"
	info "$f: rewrite file"
	cat > "$f.tmp" && mv -f "$f.tmp" "$f"
}


# List packages in subproject $1
function list_packages () {
	local prj="${OBS_PRJ}${1:+:$1}"

	# FIXME: set the network timeout threshold instead of retrying
	if ! osc list "$prj" 2>/dev/null
	then
		sleep 1
		osc list "$prj"
	fi
}


# Print the Bamboo build id of the latest successful build if
# $1 empty, print $1 otherwise
function print_build_id () {
	info "retrieve bamboo build id"
	local bid="${1:-$((
		wget -nv -O - --post-data "${BAMBOO_RSS#*\?}" "${BAMBOO_RSS%%\?*}" \
			| tr '>' '\n' \
			| awk '
				sub(/^CONTRAIL-TRUNK-/, "") && sub(/ was SUCCESSFUL.*/, "") {
				print
				exit
			}'
		# TODO: fix broken pipe error
	) 2>/dev/null)}"
	echo "$bid"
	test ! -z "$bid"
}

# Print and check the Bamboo URL of artifacts for build id $1
function print_build_url () {
	local bid="${1?}"
	local url="$(sed -e "s/%ID%/$bid/g" <<<"$BAMBOO_URL")"
	if wget --spider "$url" >&2
	then
		info "build id #$bid has been found on Bamboo"
		echo "$url"
	else
		info "build id #$bid was NOT found on Bamboo"
		return 1
	fi
}


# Print cache directory for package $2 in subproject $1
function print_cache_dir () {
	local dir="$CACHE_DIR/$OBS_PRJ${1:+:$1}${2:+/$2}"
	echo "$dir"
}

# Update or checkout OBS files for subproject $1
# and print the resulting directory path
function update_cache () {
	local sub="$OBS_PRJ:${1?}"
	local dir="$CACHE_DIR/$sub"

	mkdir -vp "$CACHE_DIR"
	if [ -d "$dir" ]
	then
		info "update cache for repository \`$sub'"
		( cd "$dir" && osc update -u )
	else
		info "checkout repository \`$sub' in local cache"
		( cd "$CACHE_DIR" && osc checkout -u "$sub" )
	fi
}

# List packages in cache for subproject $1
function list_cache () {
	local sub="${1?}"
	( cd "$CACHE_DIR/$OBS_PRJ:$sub" && echo * )
}


# Tests whether package $2 from subproject $1 is a project package
# and not a link, an aggregate or a simple directory
function is_project_package () {
	local dir="$(print_cache_dir "${1?}" "${2?}")" pkg="$2"

	if [ ! -d "$dir" ]
	then
		warn "$pkg: package not found in cache"

	elif [ ! -d "$dir/.osc" ]
	then
		warn "$pkg: package is not under version control"

	elif [ -f "$dir/_link" ]
	then
		info "$pkg: $(sed '1!d' < "$dir/_link")"

	elif [ -f "$dir/_aggregate" ]
	then
		info "$pkg: $(sed '2!d' "$dir/_aggregate")"

	else
		return 0
	fi

	return 1
}





# Run an interactive shell in directory $1
function run_interactive_shell () {
	local dir="${1:-.}"

	echo '
::::::::::::::::: Interactive Shell ::::::::::::::::::::
:: Type "exit" when all modifications are done.       ::
:: To discard all changes, exit with a non-zero code. ::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
	( cd "$dir" && PS1="${PWD##*/}> " sh )
}


# Ask confirmation for message $1
# Return 0 for 'y', 1 for 'n' and loop otherwise
function ask_configuration () {
	local prompt="$1"

	while true
	do
		read -p "$prompt? [Y/n] "
		case "${REPLY:-y}" in
			y|Y) return 0 ;;
			n|N) return 1 ;;
			*) echo 'Please answer "y" for yes or "n" for no'
		esac
	done
}

# Update debian dsc for package $1 and version $2
function update_debian_dsc () {
	local pkg="${1?}" ver="${2?}"

	info "$pkg: update debian descriptor"
	echo "Format: 1.0
Source: $pkg
Version: $ver
Binary: $pkg
Maintainer: $MAINTAINER <$EMAIL>
Architecture: any
Build-Depends: $(awk '
	/^Files:/ { exit }
	sw { print }
	sub(/^Build-Depends: */, "") { if ($0) print; sw=1 }
' $pkg.dsc)
Files:
 $(
	md5sum $pkg.tar.gz | awk '{ print $1 }'
) $(
	stat -c %s $pkg.tar.gz
) ${pkg}_${ver}.orig.tar.gz" \
		| ow "$pkg.dsc"
}

# Print the next version string for build id $1 and version $2
function print_next_version () {
	local bid="${1?}" ver="$2"
	echo "${ver:-0}+$bid"
}


# Update debian rules for package $1 and build url $2
function update_debian_rules () {
	local pkg="${1?}" url="${2?}"

	info "$pkg: update debian rules file"
	sed -i "
		s|^\(UPSTREAM_URL=\).*|\1$url|
		s|^\(PACKAGE=\).*|\1$pkg|
		s|^\(LIBDIR=\).*|\1/usr/lib/contrail/$pkg|
		s|^\(DATADIR=\).*|\1/usr/share/contrail/$pkg|
		s|^\(ETCDIR=\).*|\1/etc/contrail/$pkg|
		s|^\(BINDIR=\).*|\1/usr/bin|
	" "debian.rules"
}

# Update debian changelog for package $1, version $2 and build id $3
function update_debian_changelog () {
	local pkg="${1?}" ver="${2?}" bid="${3?}"

	info "$pkg: update debian changelog file"
	echo "$pkg ($ver) unstable; urgency=low

  * updated with artifacts from bamboo build #$bid

 -- $MAINTAINER <$EMAIL>  $(date -R)
" \
		| ow "debian.changelog"
}

# Update debian control file for package $1 and version $2
function update_debian_control () {
	local pkg="${1?}" ver="${2?}"

	info "$pkg: update debian control file"
	sed -i -e "
		s/^Source:.*/Source: $pkg/
		s/^Maintainer:.*/Maintainer: $MAINTAINER <$EMAIL>/
		s/^Homepage:.*/Homepage: http:\/\/contrail.ow2.org/
		s/^Version:.*/Version: $ver/
	" "debian.control"
}


# Update debian package $2 in subproject $1 for build id $3, new version $4
# and build url $5. All files are expected to be in the current directory
function update_debian_package () {
	local sub="${1?}" pkg="${2?}" bid="${3?}" ver="$4" url="${5?}"

	if is_project_package "$sub" "$pkg"
	then
		update_debian_changelog "$pkg" "$ver" "$bid"
		update_debian_rules "$pkg" "$url"

		info "$pkg: get upstream source"
		make -f debian.rules get-orig-source

		update_debian_dsc "$pkg" "$ver"
		update_debian_control "$pkg" "$ver"
	else
		info "$pkg: skipped"
	fi
}





#############################################################################



if [ $# -eq 0 ]
then
	echo "$USAGE" >&2
	exit 2
fi


declare command= opts= build_id= version=
declare -i interactive=0 clean=0

opts="$(POSIXLY_CORRECT='' getopt --options "b:v:cm:e:i" \
		--longoptions "build-id:,version:,clean,maintainer:,email:,interactive" \
		--name "$NAME" --shell 'bash' -- "$@")" \
	|| exit 2

eval set -- "$opts"

while true
do
	case "$1" in
	-b|--build-id)
		egrep -q '^[0-9]+$' <<<"$2" \
			|| fail "$2: invalid bamboo build id"
		build_id="$2"
		shift 2
	;;
	-v|--version)
		version="$2"
		egrep -q '^[0-9A-Z\.\+\~\-]+' <<<"$2" \
			|| fail "$2: invalid version number"
		shift 2
	;;
	-c|--clean)
		clean=1
		shift
	;;
	-i|--interactive)
		interactive=1
		shift
	;;
	-m|--maintainer)
		MAINTAINER="$2"
		shift 2
	;;
	-e|--email)
		EMAIL="$2"
		shift 2
	;;
	--)
		shift
		break
	;;
	*)
		fail "internal error" 127
	esac
done


declare command="$1"
shift

info "start $command"

case "$command" in
update)
	declare pkg subprj build_url
	declare -a pkgs

	if [ $# -eq 0 ]
	then
		fail "$#: bad number of arguments"

	elif ! list_packages "$1" > /dev/null
	then
		fail "$1: subproject not found"

	else
		subprj="$1"
		shift
	fi
		
	if ! build_id="$(print_build_id "$build_id")"
	then
		fail "failed to retrieve bamboo build id"

	elif ! build_url="$(print_build_url "$build_id")"
	then
		fail "failed to reach build artifacts on Bamboo"
	fi

	if (( clean ))
	then
		info "$(print_cache_dir "$subprj"): clean cache directory"
		rm -fr "$(print_cache_dir "$subprj")"
	fi

	update_cache "$subprj" \
		|| fail "$subprj: failed to update local cache"

	if [ $# -eq 0 ]
	then
		pkgs=( $(list_packages "$subprj") )
	else
		pkgs=( "$@" )
	fi

	info "print Open Build Service packages list"
	tr ' ' '\n' <<<"${pkgs[*]}" | nl

	version="${version:-0}+$build_id"
	info "update to version $version"

	for pkg in "${pkgs[@]}"
	do
		pushd .
		cd "$(print_cache_dir "$subprj" "$pkg")"

		update_debian_package "$subprj" "$pkg" "$build_id" "$version" "$build_url"
		osc status

		if (( ! interactive ))
		then
			osc commit -m "updated by $NAME for version $version"

		elif run_interactive_shell . \
			|| ask_confirmation "Discard these changes"
		then
			osc commit 

		else
			warn "$pkg: changes in cache have NOT been saved"
		fi

		popd
	done
;;
copy)
	declare source target pkg
	declare -a pkgs

	if [ $# -lt 2 ]
	then
		fail "$#: bad number of arguments"

	elif ! list_packages "$1" > /dev/null
	then
		fail "$1: source subproject not found"

	elif ! list_packages "$2" > /dev/null
	then
		fail "$2: target subproject not found"

	else
		source="$1"
		target="$2"
		shift 2
	fi

	if (( clean ))
	then
		info "clean cache directories"
		rm -fr "$(print_cache_dir "$source")" "$(print_cache_dir "$target")"
	fi

	if ! update_cache "$source" || ! update_cache "$target"
	then
		fail "failed to update local cache"
	fi

	if [ $# -eq 0 ]
	then
		pkgs=( $(list_packages "$source") )
	else
		pkgs=( "$@" )
	fi

	for pkg in "${pkgs[@]}"
	do
		if is_project_package "$source" "$pkg"
		then
			info "$pkg: copy package from $source to $target"
			osc copypac "$OBS_PRJ:$source" "$pkg" "$OBS_PRJ:$target"
		else
			info "$pkg: skipped"
		fi
	done
;;
clone)
        declare source target pkg
        declare -a pkgs

        if [ $# -lt 2 ]
        then
                fail "$#: bad number of arguments"

        elif ! list_packages "$1" > /dev/null
        then
                fail "$1: source subproject not found"

        elif ! list_packages "$2" &> /dev/null
        then
                fail "$2: target subproject not found"

        else
                source="$1"
                target="$2"
                shift 2
        fi

	info "$target: remove existing packages"
	for pkg in $(list_packages "$target")
	do
		osc del "$OBS_PRJ:$target" "$pkg"
		info "$pkg: removed"
	done

	info "$source: clone into $target"
	for pkg in $(list_packages "$source")
	do
		osc copypac "$OBS_PRJ:$source" "$pkg" "$OBS_PRJ:$target"
		info "$pkg: copied"
	done
;;
*)
	fail "$command: unknown command"
esac

info 'terminate'
exit 0


