#!/bin/bash
#
#    Manage validation tests for the Contrail project
#
#    This script uses libvirt along with a set of preconfigured
#    virtual machines templates to install/uninstall all binary
#    packages automatically and independently. It aims at improving
#    the softwares quality by detecting packaging and configuration
#    issues before publication.
#
# Usage
# -----
#    contrail-validation [options] <command> [<args>...]
#
# Versions
# --------
#    2012-04-02 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added domain ubuntu-11-10-64
#    2012-03-30 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Improved the os and domains filter
#        * Added custom command when starting/stopping VMs
#    2012-03-28 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added new domains and IP of the ESX server
#        * Implemented the --oses option
#    2012-03-26 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Changed default directory for key files
#        * Added staging repository
#    2012-03-22 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added interactive terminal to virtual hosts
#    2012-03-20 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added start/stop/copy-id commands
#        * Added create/revert snapshots during tests
#        * Added test script to install/remove/install/purge packages
#    2012-03-19 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Initial version
#
set -o pipefail +o errexit

declare -r NAME='contrail-validation'
declare -r VERSION='2012-04-02'


# URL of the binaries repository
declare REPO_URL='http://contrail.ow2.org/repositories'

# Max time in seconds for a VM to start
declare -i STARTUP_TIMEOUT=50

# Identity File to connect cluster and VMs
declare KEY_FILE="./validation_rsa"

# IP address of domain 0
declare DOM0_IP='192.168.1.23'

# Repositories per OBS subproject
declare -A REPO_OS=(
	[testing]='Debian_6.0 xUbuntu_11.04 xUbuntu_11.10'
	[staging]='Debian_6.0 xUbuntu_11.04 xUbuntu_11.10'
	[release]='Debian_6.0 xUbuntu_11.04 xUbuntu_11.10'
)

# Compatible domains per os
declare -A OS_DOMS=(
	[Debian_6.0]='debian-6-64 debian-6-32'
	[xUbuntu_11.04]='ubuntu-11-04-32 ubuntu-11-04-64'
	[xUbuntu_11.10]='ubuntu-11-10-32 ubuntu-11-10-64'
)

# Ip address of domains
declare -A DOM_IPS=(
	[debian-6-32]='192.168.1.200'
	[debian-6-64]='192.168.1.201'
	[ubuntu-11-04-32]='192.168.1.202'
	[ubuntu-11-04-64]='192.168.1.203'
	[ubuntu-11-10-32]='192.168.1.204'
	[ubuntu-11-10-64]='192.168.1.205'
)


# Default list of repositories
declare -a REPOS=(
	"${!REPO_OS[@]}"
)

# Default list of OS
declare -a OSES=( 
	"${!OS_DOMS[@]}"
)

# Default list of domains
declare -a DOMAINS=(
	"${!DOM_IPS[@]}"
)

# Default list of packages
declare -a PACKAGES=( 
conpaas-frontend
conpaas-frontend-db
conpaas-mysql
conpaas-scalarix-one-client
conpaas-scalarix-one-frontend
conpaas-scalarix-one-manager
conpaas-taskfarm
conpaas-web
contrail-ca-server
contrail-federation-api
contrail-federation-cli
contrail-federation-db
contrail-federation-id-prov
contrail-monitoring-hub
contrail-one-monitor
contrail-one-sensor
contrail-provider-accounting
contrail-provisioning-manager
contrail-rest-monitoring
contrail-safeconfig
contrail-vep-cli
contrail-vep-gui
contrail-vin
scalaris
scalaris-doc
xtreemfs-backend
xtreemfs-client
xtreemfs-server
xtreemfs-tools
)


declare -r USAGE="$NAME [options] <command>

Options:
   -r, --repositories LIST
             Names of repositories of binary packages to be tested.
   -o, --oses LIST
             Names of the operating systems to be tested.
   -d, --domains LIST
             Names of libvirt domains (Virtual Machines).
   -p, --packages LIST
             Names of packages to be tests.

   -k, --key-file PATH
             File from which the user's DSA, ECDSA or DSA
             authentication identity is read. (default $KEY_FILE)

   -i, --interactive
             Run an intercative shell once tests have finished

Commands:
   copy-id   Copy Key ID file on every machine
   run       Install and uninstall every package in separated machines.
   list      List selected domains and tasks.
   errors    Print errors from log files

   start     Start domains
   stop      Stop domains

Manage validation tests for the Contrail project.

Default behavior is to test all packages from each repository on every
domain. Using options can restrict tests to specific packages or repositories.

Elements in LIST are all comma separated.


Examples:
   # Test install/uninstall of the contrail-vin nightly build on debian
   $NAME -r testing -p contrail-vin -d debian6-64,debian6-32 run

"



# Terminate the script with an error message
function fail () {
	echo "ERROR: ${1:-failed}" >&2
	exit ${2:-1}
}

function warn () {
	echo "WARNING: $*" >&2
}

# Overwrite a file with content read from STDIN allowing inplace updates
function ow () {
	cat > "${1?}.tmp" && mv -f "$1.tmp" "$1"
}

# Print the intersection of $@ -- $@
function inter () {
	local -a stack=( ) result=( )

	while (( $# ))
	do
		case "$1" in
		--)
			break ;;
		*)
			stack+=( "$1" )
			shift
		esac
	done

	shift

	while (( $# ))
	do
		for item in "${stack[@]}"
		do
			case "$1" in
			$item)
				result+=( "$1" )
				shift
				continue 2
			esac
		done
		shift
	done

	echo "${result[*]}"
}

				
# Print all selected combinations of (repository, os, domain, package)
function print_tasks () {
	local repo os dom pkg
	local -a repos oses doms

	for repo in "${REPOS[@]}"
	do
		oses=( ${REPO_OS[$repo]} )

		if [ ${#oses[@]} -eq 0 ]
		then
			warn "$repo: no OS compatible with this repository"
			continue
		fi

		for os in $(inter "${oses[@]}" -- "${OSES[@]}")
		do
			doms=( ${OS_DOMS[$os]} )

			if [ ${#doms[@]} -eq 0 ]
                        then
                                warn "$os: no valid domain found for this operating system" >&2
                                continue
                        fi

			for dom in $(inter "${doms[@]}" -- "${DOMAINS[@]}")
			do
				if [ -z "${DOM_IPS[$dom]}" ]
				then
					warn "$dom: no ip address configured for domain"
					continue
				fi

				for pkg in "${PACKAGES[@]}"
				do
					echo "$repo|$os|$dom|$pkg"
				done
			done
		done
	done
}

# Print the test script for package $2 from repository $1
function print_test_script() {
	local url="${1?}" pkg="${2?}"
	echo "
echo "deb $url ./" >> /etc/apt/sources.list
wget -O - $REPO_URL/contrail.pub | apt-key add -
aptitude update
aptitude search contrail conpaas scalaris xtreemfs
aptitude -y install $pkg
aptitude -y remove $pkg
aptitude -y install $pkg
aptitude -y purge $pkg
"
}


# Run command $@ on dom0
function exec_dom0 () {
	ssh -i "$KEY_FILE" -l root "$DOM0_IP" "$@"
}

# Run command $@ on domain $1
function exec_dom () {
	local dom="${1?}"
	shift
	ssh -i "$KEY_FILE" -l root "${DOM_IPS[$dom]}" "$@" 
}


# Wait until port $2 on machine $1 is in $3 state ("up" or "down")
# within $4 seconds (default 30)
function wait_updown () {
	local ip="${1?}" port="${2?}" state="${3?}"
	local -i threshold=$(( $(date +%s) + ${4:-${STARTUP_TIMEOUT}} )) ret=0

	case "$state" in
	up|down)		
		while true
		do
			( exec <> "/dev/tcp/$ip/$port" ) &> /dev/null
			ret=$?
	
			if [ $ret -eq 0 -a "$state" = "up" ] \
				|| [ $ret -ne 0 -a "$state" = "down" ]
			then
				return 0
		
			elif (( $(date +%s) > threshold ))
			then
				return 1
	
			else
				sleep 1
			fi
		done
	;;
	*)
		fail "$state: invalid state"
	esac
}



#############################################################################
declare opts prog pkg name

opts="$(POSIXLY_CORRECT="" getopt --options "r:p:o:d:k:i" \
  --longoptions "key-file:,packages:,oses:,domains:,repositories:,interactive" \
        --name "$NAME" --shell 'bash' -- "$@")" \
    || exit 2
eval set -- "$opts"

while true
do
	case "$1" in
	-k|--key-file)
		KEY_FILE="$2"
		shift 2
	;;
	-o|--oses)
		OSES=( ${2//,/ } )
		shift 2
	;;
	-p|--packages)
		PACKAGES=( ${2//,/ } )
		shift 2
	;;
	-d|--domains)
		DOMAINS=( ${2//,/ } )
        	shift 2
	;;
	-r|--repositories)
		REPOS=( ${2//,/ } )
        	shift 2
	;;
	-i|--interactive)
		INTERACTIVE='yes'
		shift
	;;
	--)
		shift
		break
	;;
	*)
		fail "internal error" 127
	esac
done


declare cmd repo os dom pkg

if [ $# -eq 0 ]
then
	echo "$USAGE" >&2
	exit 2

elif [ ! -f "$KEY_FILE" ]
then
	fail "$KEY_FILE: key file not found"

else
	cmd="$1"
	shift
fi



case "$cmd" in
copy-id)
	for ip in "$DOM0_IP" "${DOM_IPS[@]}"
	do
		ssh-copy-id -i "$KEY_FILE.pub" "root@$ip"
	done
;;
list)
	exec_dom0 virsh list --all
	echo "
Repository OS Domain Package
---------- -- ------ -------
$(print_tasks | tr '|' ' ')
" \
		| column -t
;;
start)
	for dom in $(print_tasks | cut -d '|' -f 3 | sort -u)
	do
		echo ":: Start domain $dom"
		if exec_dom0 virsh list | grep -qwF "$dom"
		then
			echo "Domain $dom is already running"
		else
			exec_dom0 virsh start "$dom"
			echo "Boot in progress..."
			wait_updown "${DOM_IPS[$dom]}" 22 up
		fi
	done
	exec_dom0 virsh list --all
	if [ $# -gt 0 ]
	then
		echo ":: Execute custom command - $*"
		ssh -i "$KEY_FILE" -l root "${DOM_IPS[$dom]}" "$@"
		echo ":: Returned $?"
	fi
;;
stop)
	if [ $# -gt 0 ]
	then
		echo ":: Execute custom command - $*"
		ssh -i "$KEY_FILE" -l root "${DOM_IPS[$dom]}" "$@"
		echo ":: Returned $?"
	fi
	for dom in $(print_tasks | cut -d '|' -f 3 | sort -u)
	do
		echo ":: Stop domain $dom"
		if exec_dom0 virsh list | grep -qwF "$dom"
		then
			#exec_dom "$dom" bash -c 'poweroff &'
			#wait_updown "${DOM_IPS[$dom]}" 22 down
			exec_dom0 virsh destroy "$dom"
			sleep 2
		else
			echo "Domain $dom was already shut off"
		fi
	done
	exec_dom0 virsh list --all
;;
run)
	for v in $(print_tasks)
	do
		repo="$(cut -d '|' -f 1 <<<"$v")"
		os="$(cut -d '|' -f 2 <<<"$v")"
		dom="$(cut -d '|' -f 3 <<<"$v")"
		pkg="$(cut -d '|' -f 4 <<<"$v")"

		"$0" -k "$KEY_FILE" -d "$dom" start

		echo ":: Create snapshot for domain $dom"
		exec_dom0 virsh snapshot-create "$dom"
		exec_dom0 virsh snapshot-list "$dom"

		echo ":: Start of validation tests - $(date -R)"
		print_test_script "$REPO_URL/binaries/$repo/$os" "$pkg" \
			| exec_dom "$dom" bash -l \
			| tee "$(date +%F.%T).$repo.$os.$dom.$pkg.log"
		echo ":: End of validation tests - $(date -R)"

		if [ "$INTERACTIVE" = 'yes' ]
		then
			echo ":: Run interactive shell"
			ssh -i "$KEY_FILE" -l root "${DOM_IPS[$dom]}"
			echo ":: Returned $?"
		fi

		echo ":: Restore snapshot $snapshot for domain $dom"
		snapshot=$(exec_dom0 virsh snapshot-list "$dom" \
				| awk 'NF > 3 { t=$1 } END { print t }')
		exec_dom0 virsh snapshot-revert "$dom" "$snapshot"
		exec_dom0 virsh snapshot-delete "$dom" "$snapshot"

		"$0" -k "$KEY_FILE" -d "$dom" stop
	done
;;
errors)
	for v in $(print_tasks)
	do
		for f in *."${v//|/.}.log"
		do
			if [ -f "$f" ]
			then
				echo "$f"
			fi
		done
	done \
		| xargs -r grep --color=auto -C 2 -iwHE '(E:|error|fatal|fail)'
;;
*)
	fail "$1: unknown command"
esac

exit 0

