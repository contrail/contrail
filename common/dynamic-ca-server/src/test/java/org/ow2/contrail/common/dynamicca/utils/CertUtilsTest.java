package org.ow2.contrail.common.dynamicca.utils;

import eu.contrail.security.SecurityCommons;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.common.dynamicca.Utils;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.jpa.entities.Ca;
import org.ow2.contrail.common.dynamicca.jpa.entities.Vin;

import javax.persistence.EntityManager;
import java.io.*;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CertUtilsTest {

    @Before
    public void setUp() throws Exception {
        EMF.init("testPersistenceUnit");
        Conf.getInstance().load(new File("src/test/resources/dynamic-ca-server.properties"));
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @After
    public void tearDown() throws Exception {
        EMF.close();
        Utils.dropTestDatabase();
    }

    @Test
    public void testCreateCaCert() throws Exception {
        EntityManager em = EMF.createEntityManager();
        CertUtils certUtils = new CertUtils(em);

        Vin vin = new Vin();
        vin.setUid("myvin");
        Ca ca1 = new Ca();
        ca1.setUid("myca1");
        ca1.setVin(vin);
        Ca ca2 = new Ca();
        ca2.setUid("myca2");
        ca2.setVin(vin);

        em.getTransaction().begin();
        em.persist(vin);
        em.persist(ca1);
        em.persist(ca2);
        em.getTransaction().commit();

        assertEquals(ca1.getSeqNum(), 1);
        assertEquals(ca2.getSeqNum(), 2);
        assertEquals(ca1.getCertSerialNumberCounter(), 1);

        certUtils.createCACertificate(ca1);

        PEMReader pemReader = new PEMReader(new StringReader(ca1.getPrivateKey()));
        KeyPair keyPair = (KeyPair) pemReader.readObject();
        assertEquals(keyPair.getPrivate().getAlgorithm(), "RSA");

        pemReader = new PEMReader(new StringReader(ca1.getCertificate()));
        X509Certificate cert = (X509Certificate) pemReader.readObject();
        assertEquals(cert.getType(), "X.509");
        assertEquals(cert.getSerialNumber().intValue(), 1);
        assertEquals(cert.getIssuerDN().getName(), "DC=users,DC=ca,DC=contrail-project,DC=eu");
        assertEquals(cert.getPublicKey().getAlgorithm(), "RSA");
        assertEquals(cert.getSigAlgName(), "SHA1WithRSAEncryption");
        assertEquals(cert.getSubjectDN().getName(), Conf.getInstance().getCACertDN("myca1"));

        // verify that this certificate was signed using the root-ca private key
        SecurityCommons securityCommons = new SecurityCommons();
        X509Certificate rootCACertificate = securityCommons.readCertificate(new InputStreamReader(
                new FileInputStream(Conf.getInstance().getRootCACertFile())));
        cert.verify(rootCACertificate.getPublicKey());
    }

    @Test
    public void testCreateVinAgentCert() throws Exception {
        EntityManager em = EMF.createEntityManager();
        CertUtils certUtils = new CertUtils(em);

        Vin vin = new Vin();
        vin.setUid("myvin");
        Ca ca = new Ca();
        ca.setUid("myca");
        ca.setVin(vin);

        em.getTransaction().begin();
        em.persist(vin);
        em.persist(ca);
        certUtils.createCACertificate(ca);
        em.getTransaction().commit();

        assertEquals(ca.getSeqNum(), 1);
        assertEquals(ca.getCertSerialNumberCounter(), 1);

        // create certificate signing request
        SecurityCommons securityCommons = new SecurityCommons();
        KeyPair keyPair = securityCommons.generateKeyPair("RSA", 2048);
        String subject = "CN=vin-agent";
        PKCS10CertificationRequest csr = securityCommons.createCSR(keyPair, subject, "SHA256withRSA");

        X509Certificate vinAgentCert = certUtils.createVinAgentCertificate(csr, ca);

        // CA certificate
        PEMReader pemReader = new PEMReader(new StringReader(ca.getCertificate()));
        X509Certificate caCert = (X509Certificate) pemReader.readObject();

        // check VIN agent certificate
        assertEquals(vinAgentCert.getType(), "X.509");
        assertEquals(vinAgentCert.getSerialNumber().intValue(), 1);
        assertEquals(vinAgentCert.getIssuerDN().getName(), caCert.getSubjectDN().getName());
        assertEquals(vinAgentCert.getPublicKey().getAlgorithm(), "RSA");
        assertEquals(vinAgentCert.getSigAlgName(), "SHA1WithRSAEncryption");
        assertEquals(vinAgentCert.getSubjectDN().getName(), subject);

        // verify that this certificate was signed using the ca's private key
        vinAgentCert.verify(caCert.getPublicKey());

        // check that cert counter was increased
        assertEquals(ca.getCertSerialNumberCounter(), 2);
    }

    @Test
    public void testCreateCRL() throws Exception {
        EntityManager em = EMF.createEntityManager();
        CertUtils certUtils = new CertUtils(em);

        // create CA
        Vin vin = new Vin();
        vin.setUid("myvin");
        Ca ca = new Ca();
        ca.setUid("myca");
        ca.setVin(vin);

        em.getTransaction().begin();
        em.persist(vin);
        em.persist(ca);
        certUtils.createCACertificate(ca);
        em.getTransaction().commit();

        // create CRL
        X509CRLHolder crlHolder = certUtils.createCRL(ca);

        // check CRL number
        ASN1Integer crlNumber = (ASN1Integer) crlHolder.getExtension(X509Extension.cRLNumber).getParsedValue();
        assertEquals(crlNumber.getValue().intValue(), 1);
        assertEquals(ca.getCrlCounter(), 2);

        // check issuer
        X509Certificate caCert = certUtils.getCaCert(ca);
        assertEquals(crlHolder.getIssuer().toString(), caCert.getSubjectDN().getName());

        assertTrue(crlHolder.isSignatureValid(
                new JcaContentVerifierProviderBuilder().setProvider("BC").build(caCert)));

        // create another CRL, check CRL number
        crlHolder = certUtils.createCRL(ca);
        crlNumber = (ASN1Integer) crlHolder.getExtension(X509Extension.cRLNumber).getParsedValue();
        assertEquals(crlNumber.getValue().intValue(), 2);
        assertEquals(ca.getCrlCounter(), 3);
    }
}
