package org.ow2.contrail.common.dynamicca;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import eu.contrail.security.SecurityCommons;
import org.bouncycastle.cert.X509CRLEntryHolder;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.utils.CertUtils;
import org.ow2.contrail.common.dynamicca.utils.Conf;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.StringReader;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CaTest extends JerseyTest {

    public CaTest() throws Exception {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.common.dynamicca.rest")
                .contextPath("dynca").build());
    }

    @Before
    public void setUp() throws Exception {
        EMF.init("testPersistenceUnit");
        Conf.getInstance().load(new File("src/test/resources/dynamic-ca-server.properties"));
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @After
    public void tearDown() throws Exception {
       EMF.close();
       Utils.dropTestDatabase();
    }

    @Test
    public void testIssueVinAgentCert() throws Exception {
        EntityManager em = EMF.createEntityManager();
        WebResource webResource = resource();

        // create VIN controller and CA
        JSONObject vinData = new JSONObject();
        vinData.put("uid", "myvin");
        webResource.path("/vins").post(vinData);

        JSONObject caData = new JSONObject();
        caData.put("uid", "myca");
        webResource.path("/vins/myvin/cas").post(caData);

        // get CA certificate
        String caCertPem = webResource.path("/vins/myvin/cas/myca/cacert").get(String.class);
        PEMReader pemReader = new PEMReader(new StringReader(caCertPem));
        X509Certificate caCert = (X509Certificate) pemReader.readObject();

        // create certificate signing request
        SecurityCommons securityCommons = new SecurityCommons();
        KeyPair keyPair = securityCommons.generateKeyPair("RSA", 2048);
        String subject = "CN=vin-agent";
        PKCS10CertificationRequest csr = securityCommons.createCSR(keyPair, subject, "SHA256withRSA");

        // convert CSR to PEM format
        CertUtils certUtils = new CertUtils(em);
        String csrPem = certUtils.convertToPem(csr);

        // send CSR to CA server
        String vinAgentCertPem = webResource.path("/vins/myvin/cas/myca/certs").post(String.class, csrPem);

        // decode PEM encoded VIN agent certificate
        pemReader = new PEMReader(new StringReader(vinAgentCertPem));
        X509Certificate vinAgentCert1 = (X509Certificate) pemReader.readObject();
        assertEquals(vinAgentCert1.getSubjectDN().getName(), subject);
        assertEquals(vinAgentCert1.getSerialNumber().intValue(), 1);

        // verify that VIN agent certificate is signed by the myca CA
        vinAgentCert1.verify(caCert.getPublicKey());

        // request another VIN agent certificate and check serial number
        String vinAgentCertPem1 = webResource.path("/vins/myvin/cas/myca/certs").post(String.class, csrPem);
        pemReader = new PEMReader(new StringReader(vinAgentCertPem1));
        X509Certificate vinAgentCert2 = (X509Certificate) pemReader.readObject();
        assertEquals(vinAgentCert2.getSerialNumber().intValue(), 2);

        // revoke certificates 1 and 2
        Date revocationDate = new Date();
        webResource.path("/vins/myvin/cas/myca/certs/1").delete();
        webResource.path("/vins/myvin/cas/myca/certs/2").delete();

        // get CRL
        byte[] crlEncoded = webResource.path("/vins/myvin/cas/myca/crl").get(byte[].class);

        // create X509CRLHolder
        X509CRLHolder crlHolder = new X509CRLHolder(crlEncoded);

        // check CRL
        assertEquals(crlHolder.getIssuer().toString(), caCert.getSubjectDN().getName());
        assertTrue(crlHolder.isSignatureValid(
                new JcaContentVerifierProviderBuilder().setProvider("BC").build(caCert)));

        // check CRL entries
        X509CRLEntryHolder crlEntry1 = (X509CRLEntryHolder) crlHolder.getRevokedCertificates().toArray()[0];
        X509CRLEntryHolder crlEntry2 = (X509CRLEntryHolder) crlHolder.getRevokedCertificates().toArray()[1];
        assertEquals(crlEntry1.getSerialNumber().intValue(), 1);
        assertEquals(crlEntry2.getSerialNumber().intValue(), 2);
        assertTrue(Math.abs(crlEntry1.getRevocationDate().getTime() - revocationDate.getTime()) < 3000);
        assertTrue(Math.abs(crlEntry2.getRevocationDate().getTime() - revocationDate.getTime()) < 3000);

        /*FileOutputStream fos = new FileOutputStream(new File("dynamic-ca-test.crl"));
        fos.write(crlEncoded);
        fos.close();*/
    }
}
