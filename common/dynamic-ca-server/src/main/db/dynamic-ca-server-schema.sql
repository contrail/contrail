SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `dynamic_ca_server` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `dynamic_ca_server` ;

-- -----------------------------------------------------
-- Table `vin`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `vin` (
  `uid` VARCHAR(36) NOT NULL ,
  PRIMARY KEY (`uid`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ca`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ca` (
  `uid` VARCHAR(36) NOT NULL ,
  `seq_num` INT NOT NULL ,
  `vin_uid` VARCHAR(36) NOT NULL ,
  `private_key` TEXT NULL ,
  `certificate` TEXT NULL ,
  `cert_sn_counter` INT NULL DEFAULT 1 ,
  `crl_counter` INT NULL DEFAULT 1 ,
  PRIMARY KEY (`uid`) ,
  INDEX `fk_ca_vin` (`vin_uid` ASC) ,
  CONSTRAINT `fk_ca_vin`
    FOREIGN KEY (`vin_uid` )
    REFERENCES `vin` (`uid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cert`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cert` (
  `sn` INT NOT NULL ,
  `ca_uid` VARCHAR(36) NOT NULL ,
  `content` TEXT NULL ,
  `revoked` TINYINT(1)  NOT NULL DEFAULT false ,
  `revocation_date` TIMESTAMP NULL ,
  PRIMARY KEY (`sn`, `ca_uid`) ,
  INDEX `fk_cert_ca1` (`ca_uid` ASC) ,
  CONSTRAINT `fk_cert_ca1`
    FOREIGN KEY (`ca_uid` )
    REFERENCES `ca` (`uid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `SEQUENCE` VALUES ('SEQ_GEN',50);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
