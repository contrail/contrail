package org.ow2.contrail.common.dynamicca.rest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.jpa.entities.Vin;
import org.ow2.contrail.common.dynamicca.utils.DBUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/vins")
public class VinRootResource {
    protected static Logger log = Logger.getLogger(VinRootResource.class);

    @Context
    UriInfo uriInfo;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getVinControllers() {
        EntityManager em = EMF.createEntityManager();
        try {
            TypedQuery<Vin> q = em.createNamedQuery("Vin.findAll", Vin.class);
            List<Vin> vinList = q.getResultList();
            JSONArray jsonArray = new JSONArray();
            for (Vin vin : vinList) {
                UriBuilder ub = uriInfo.getAbsolutePathBuilder();
                URI vinUri = ub.path(vin.getUid()).build();
                jsonArray.put(vinUri);
            }
            return jsonArray;
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addVinController(JSONObject data) throws URISyntaxException, JSONException {
        String vcUid;
        try {
            vcUid = data.getString("uid");
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }

        EntityManager em = EMF.createEntityManager();
        try {
            Vin vin = new Vin();
            vin.setUid(vcUid);

            em.getTransaction().begin();
            em.persist(vin);
            em.getTransaction().commit();

            URI location = new URI(vin.getUid());
            return Response.created(location).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }
}
