package org.ow2.contrail.common.dynamicca.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Conf {
    private static Conf instance = new Conf();
    private static Logger log = Logger.getLogger(Conf.class);
    private Properties props;

    public static Conf getInstance() {
        return instance;
    }

    private Conf() {
    }

    public void load(File confFile) throws Exception {
        props = new Properties();
        try {
            props.load(new FileInputStream(confFile));
            log.info(String.format("Configuration loaded successfully from file '%s'.", confFile));
        }
        catch (IOException e) {
            throw new Exception(String.format("Failed to read configuration file '%s': %s", confFile, e.getMessage()));
        }
    }

    public String getRootCAPrivateKeyFile() {
        return props.getProperty("rootca.privateKeyFile");
    }

    public String getRootCAPrivateKeyPass() {
        return props.getProperty("rootca.privateKeyPass");
    }

    public String getRootCACertFile() {
        return props.getProperty("rootca.certificateFile");
    }

    public int getRootCACertLifetimeDays() {
        return Integer.parseInt(props.getProperty("rootca.cert.lifetimeDays"));
    }

    public String getCACertDN(String uid) {
        String format = props.getProperty("rootca.cert.DN");
        return String.format(format, uid);
    }

    public int getVinAgentCertLifetimeDays() {
        return Integer.parseInt(props.getProperty("dynamicca.cert.vinagent.lifetimeDays"));
    }
}
