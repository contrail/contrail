package org.ow2.contrail.common.dynamicca.utils;

import eu.contrail.security.SecurityCommons;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.ow2.contrail.common.dynamicca.jpa.entities.Ca;
import org.ow2.contrail.common.dynamicca.jpa.entities.Cert;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.*;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

public class CertUtils {
    private SecurityCommons securityCommons;
    private EntityManager em;

    public CertUtils(EntityManager em) {
        securityCommons = new SecurityCommons();
        this.em = em;
    }

    public void createCACertificate(Ca ca) throws Exception {

        PrivateKey rootCAPrivateKey = securityCommons.readPrivateKey(Conf.getInstance().getRootCAPrivateKeyFile(),
                Conf.getInstance().getRootCAPrivateKeyPass().toCharArray());
        X509Certificate rootCACert = securityCommons.readCertificate(new InputStreamReader(
                new FileInputStream(Conf.getInstance().getRootCACertFile())));
        KeyPair issuerKeyPair = new KeyPair(rootCACert.getPublicKey(), rootCAPrivateKey);
        String issuerName = rootCACert.getSubjectDN().getName();
        issuerName = securityCommons.reverse(issuerName, ",");

        KeyPair caKeyPair = securityCommons.generateKeyPair("RSA", 2048);
        String subjectDN = Conf.getInstance().getCACertDN(ca.getUid());

        X509Certificate caCert = securityCommons.createCertificate(
                caKeyPair.getPublic(),
                subjectDN,
                ca.getSeqNum(),
                issuerName,
                issuerKeyPair,
                "SHA1withRSA",
                true,
                new Date(),
                Conf.getInstance().getRootCACertLifetimeDays(),
                0,
                0);

        String pemEncodedPrivateKey = convertToPem(caKeyPair.getPrivate());
        ca.setPrivateKey(pemEncodedPrivateKey);
        String pemEncodedCert = convertToPem(caCert);
        ca.setCertificate(pemEncodedCert);
    }

    public X509Certificate createVinAgentCertificate(PKCS10CertificationRequest csr, Ca ca) throws Exception {

        PrivateKey caPrivateKey = securityCommons.readPrivateKey(
                new ByteArrayInputStream(ca.getPrivateKey().getBytes()),
                null
        );
        X509Certificate caCert = securityCommons.readCertificate(
                new InputStreamReader(new ByteArrayInputStream(ca.getCertificate().getBytes())));
        KeyPair issuerKeyPair = new KeyPair(caCert.getPublicKey(), caPrivateKey);
        String issuerName = caCert.getSubjectDN().getName();

        int serialNumber;
        synchronized (CertUtils.class) {
            em.getTransaction().begin();
            serialNumber = ca.getCertSerialNumberCounter();
            ca.setCertSerialNumberCounter(serialNumber + 1);
            em.getTransaction().commit();
        }

        X509Certificate vinAgentCert = securityCommons.createCertificate(
                csr.getPublicKey(),
                csr.getCertificationRequestInfo().getSubject().toString(),
                serialNumber,
                issuerName,
                issuerKeyPair,
                "SHA1withRSA",
                false,
                new Date(),
                Conf.getInstance().getRootCACertLifetimeDays(),
                0,
                0);

        return vinAgentCert;
    }

    public String convertToPem(Object o) throws IOException {
        StringWriter writer = new StringWriter();
        PEMWriter pemWriter = new PEMWriter(writer);
        pemWriter.writeObject(o);
        pemWriter.flush();
        pemWriter.close();
        return writer.toString();
    }

    public X509Certificate getCaCert(Ca ca) throws IOException {
        return securityCommons.readCertificate(
                new InputStreamReader(new ByteArrayInputStream(ca.getCertificate().getBytes())));
    }

    public PrivateKey getCaPrivateKey(Ca ca) throws IOException, NoSuchAlgorithmException {
        return securityCommons.readPrivateKey(
                new ByteArrayInputStream(ca.getPrivateKey().getBytes()),
                null);
    }

    public X509CRLHolder createCRL(Ca ca) throws Exception {

        X509Certificate caCert = getCaCert(ca);
        PrivateKey caPrivateKey = getCaPrivateKey(ca);

        Date now = new Date();
        X509v2CRLBuilder crlGen = new X509v2CRLBuilder(new X500Name(caCert.getSubjectDN().getName()), now);

        Date nextUpdate = new Date(now.getTime() + 24 * 3600 * 1000); // Every day
        crlGen.setNextUpdate(nextUpdate);

        // get CRL number and increase counter
        int crlNumber;
        synchronized (CertUtils.class) {
            em.getTransaction().begin();
            crlNumber = ca.getCrlCounter();
            ca.setCrlCounter(crlNumber + 1);
            em.getTransaction().commit();
        }

        // set CRL number
        crlGen.addExtension(X509Extension.cRLNumber, false, new CRLNumber(BigInteger.valueOf(crlNumber)));

        // set authority key identifier
        crlGen.addExtension(X509Extension.authorityKeyIdentifier, false,
                new AuthorityKeyIdentifierStructure(caCert));

        // add revoked certificates
        TypedQuery<Cert> q = em.createNamedQuery("Cert.getRevokedCerts", Cert.class);
        q.setParameter("caUid", ca.getUid());
        List<Cert> certList = q.getResultList();
        for (Cert cert : certList) {
            BigInteger sn = BigInteger.valueOf(cert.getCertPK().getSn());
            crlGen.addCRLEntry(sn, cert.getRevocationDate(), CRLReason.unspecified);
        }

        // sign with CA private key
        ContentSigner contentSigner = new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC").build(caPrivateKey);
        X509CRLHolder crlHolder = crlGen.build(contentSigner);

        return crlHolder;
    }
}
