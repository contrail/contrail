package org.ow2.contrail.common.dynamicca.rest;

import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.openssl.PEMReader;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.jpa.entities.Ca;
import org.ow2.contrail.common.dynamicca.jpa.entities.Cert;
import org.ow2.contrail.common.dynamicca.utils.CertUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.StringReader;
import java.security.cert.X509Certificate;

@Path("/vins/{vcUid}/cas/{caUid}")
public class CaResource {
    private String vcUid;
    private String caUid;

    @Context
    UriInfo uriInfo;

    public CaResource(@PathParam("vcUid") String vcUid, @PathParam("caUid") String caUid) {
        this.vcUid = vcUid;
        this.caUid = caUid;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getCa() throws JSONException {
        EntityManager em = EMF.createEntityManager();
        try {
            Ca ca = em.find(Ca.class, caUid);
            if (ca == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject o = new JSONObject();
            o.put("uid", ca.getUid());
            o.put("uri", uriInfo.getAbsolutePath());
            o.put("revocation_list", uriInfo.getAbsolutePathBuilder().path("revocation_list").build());
            o.put("cacert", uriInfo.getAbsolutePathBuilder().path("cacert").build());
            return o;
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/cacert")
    public String getCaCert() throws JSONException {
        EntityManager em = EMF.createEntityManager();
        try {
            Ca ca = em.find(Ca.class, caUid);
            if (ca == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            return ca.getCertificate();
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("certs")
    public String generateCert(String csrPem) throws Exception {
        EntityManager em = EMF.createEntityManager();
        try {
            Ca ca = em.find(Ca.class, caUid);
            if (ca == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            PKCS10CertificationRequest csr;
            try {
                PEMReader pemReader = new PEMReader(new StringReader(csrPem));
                csr = (PKCS10CertificationRequest) pemReader.readObject();
                csr.verify();
            }
            catch (Exception e) {
                throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                        .entity("Invalid certificate signing request: " + e.getMessage()).build());
            }

            CertUtils certUtils = new CertUtils(em);
            X509Certificate x509Cert = certUtils.createVinAgentCertificate(csr, ca);
            Cert cert = new Cert(x509Cert.getSerialNumber().intValue(), ca.getUid());
            cert.setCa(ca);

            em.getTransaction().begin();
            em.persist(cert);
            em.getTransaction().commit();

            return certUtils.convertToPem(x509Cert);
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/pkix-crl")
    @Path("crl")
    public byte[] getCertRevocationList() throws Exception {
        EntityManager em = EMF.createEntityManager();
        try {
            Ca ca = em.find(Ca.class, caUid);
            if (ca == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            CertUtils certUtils = new CertUtils(em);
            X509CRLHolder crlHolder = certUtils.createCRL(ca);

            return crlHolder.getEncoded();
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }
}
