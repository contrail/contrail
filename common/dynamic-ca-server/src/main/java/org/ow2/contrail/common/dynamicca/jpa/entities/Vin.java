package org.ow2.contrail.common.dynamicca.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "vin")
@NamedQueries({
    @NamedQuery(name = "Vin.findAll", query = "SELECT v FROM Vin v"),
    @NamedQuery(name = "Vin.findByUid", query = "SELECT v FROM Vin v WHERE v.uid = :uid")})
public class Vin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "uid", nullable = false, length = 36)
    private String uid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vin")
    private List<Ca> caList;

    public Vin() {
    }

    public Vin(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<Ca> getCaList() {
        return caList;
    }

    public void setCaList(List<Ca> caList) {
        this.caList = caList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (uid != null ? uid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vin)) {
            return false;
        }
        Vin other = (Vin) object;
        if ((this.uid == null && other.uid != null) || (this.uid != null && !this.uid.equals(other.uid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.common.dynamicca.jpa.entities.Vin[ uid=" + uid + " ]";
    }
    
}
