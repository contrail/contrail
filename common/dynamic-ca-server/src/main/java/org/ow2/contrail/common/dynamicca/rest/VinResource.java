package org.ow2.contrail.common.dynamicca.rest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.jpa.entities.Ca;
import org.ow2.contrail.common.dynamicca.jpa.entities.Vin;
import org.ow2.contrail.common.dynamicca.utils.CertUtils;
import org.ow2.contrail.common.dynamicca.utils.DBUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/vins/{vcUid}")
public class VinResource {
    protected static Logger log = Logger.getLogger(VinResource.class);

    private String vcUid;

    @Context
    UriInfo uriInfo;

    public VinResource(@PathParam("vcUid") String vcUid) {
        this.vcUid = vcUid;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getVin() throws JSONException {
        EntityManager em = EMF.createEntityManager();
        try {
            Vin vin = em.find(Vin.class, vcUid);
            if (vin == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject o = new JSONObject();
            o.put("uid", vin.getUid());
            o.put("uri", uriInfo.getAbsolutePath());
            o.put("cas", uriInfo.getAbsolutePathBuilder().path("cas").build());
            return o;
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cas")
    public JSONArray getCas() throws URISyntaxException {
        EntityManager em = EMF.createEntityManager();
        try {
            Vin vin = em.find(Vin.class, vcUid);
            if (vin == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray jsonArray = new JSONArray();
            for (Ca ca : vin.getCaList()) {
                UriBuilder ub = uriInfo.getAbsolutePathBuilder();
                URI caUri = ub.path(ca.getUid()).build();
                jsonArray.put(caUri);
            }
            return jsonArray;
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("cas")
    public Response addCa(JSONObject data) throws Exception {
        String caUid;
        try {
            caUid = data.getString("uid");
        }
        catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }

        EntityManager em = EMF.createEntityManager();
        try {
            Vin vin = em.find(Vin.class, vcUid);
            if (vin == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Ca ca = new Ca();
            ca.setUid(caUid);
            ca.setVin(vin);

            EntityTransaction t = em.getTransaction();
            t.begin();
            em.persist(ca);
            vin.getCaList().add(ca);
            try {
                CertUtils certUtils = new CertUtils(em);
                certUtils.createCACertificate(ca);
                t.commit();
            }
            catch (Exception e) {
                if (t.isActive()) {
                    em.getTransaction().rollback();
                }
                if (DBUtils.isIntegrityConstraintException(e)) {
                    return Response.status(Response.Status.CONFLICT).build();
                }
                log.error("Failed to create CA certificate: " + e.getMessage(), e);
                throw new Exception("Failed to create CA certificate: " + e.getMessage());
            }

            URI location = new URI(ca.getUid());
            return Response.created(location).build();
        }

        finally

        {
            EMF.closeEntityManager(em);
        }
    }
}
