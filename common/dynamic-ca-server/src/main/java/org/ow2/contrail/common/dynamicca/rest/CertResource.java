package org.ow2.contrail.common.dynamicca.rest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.jpa.entities.Cert;
import org.ow2.contrail.common.dynamicca.jpa.entities.CertPK;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Date;

@Path("/vins/{vcUid}/cas/{caUid}/certs/{sn}")
public class CertResource {
    private String vcUid;
    private String caUid;
    private int sn;

    @Context
    UriInfo uriInfo;

    public CertResource(@PathParam("vcUid") String vcUid, @PathParam("caUid") String caUid,
                        @PathParam("sn") int sn) {
        this.vcUid = vcUid;
        this.caUid = caUid;
        this.sn = sn;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public JSONObject getCert() throws JSONException {
        EntityManager em = EMF.createEntityManager();
        try {
            CertPK certPK = new CertPK(sn, caUid);
            Cert cert = em.find(Cert.class, certPK);
            if (cert == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            return null;
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }

    @DELETE
    public Response revokeCert() {
        EntityManager em = EMF.createEntityManager();
        try {
            CertPK certPK = new CertPK(sn, caUid);
            Cert cert = em.find(Cert.class, certPK);
            if (cert == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            if (cert.isRevoked()) {
                throw new WebApplicationException(Response.Status.NOT_MODIFIED);
            }

            em.getTransaction().begin();
            cert.setRevoked(true);
            cert.setRevocationDate(new Date());
            em.getTransaction().commit();

            String message = String.format("The certificate with serial number %d has been revoked successfully.", sn);
            return Response.status(Response.Status.NO_CONTENT).entity(message).build();
        }
        finally {
            EMF.closeEntityManager(em);
        }
    }
}