package org.ow2.contrail.common.dynamicca.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "cert")
@NamedQueries({
        @NamedQuery(name = "Cert.find",
                query = "SELECT c FROM Cert c WHERE c.certPK.caUid = :caUid AND c.certPK.sn = :sn"),
        @NamedQuery(name = "Cert.getRevokedCerts",
                query = "SELECT c FROM Cert c WHERE c.certPK.caUid = :caUid AND c.revoked = true")})
public class Cert implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CertPK certPK;
    @Lob
    @Column(name = "content", length = 65535)
    private String content;
    @Basic(optional = false)
    @Column(name = "revoked", nullable = false)
    private boolean revoked;
    @Basic(optional = false)
    @Column(name = "revocation_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date revocationDate;
    @JoinColumn(name = "ca_uid", referencedColumnName = "uid", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ca ca;

    public Cert() {
    }

    public Cert(CertPK certPK) {
        this.certPK = certPK;
    }

    public Cert(int sn, String caUid) {
        this.certPK = new CertPK(sn, caUid);
    }

    public CertPK getCertPK() {
        return certPK;
    }

    public void setCertPK(CertPK certPK) {
        this.certPK = certPK;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Ca getCa() {
        return ca;
    }

    public void setCa(Ca ca) {
        this.ca = ca;
    }

    public boolean isRevoked() {
        return revoked;
    }

    public void setRevoked(boolean revoked) {
        this.revoked = revoked;
    }

    public Date getRevocationDate() {
        return revocationDate;
    }

    public void setRevocationDate(Date revocationDate) {
        this.revocationDate = revocationDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certPK != null ? certPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cert)) {
            return false;
        }
        Cert other = (Cert) object;
        if ((this.certPK == null && other.certPK != null) || (this.certPK != null && !this.certPK.equals(other.certPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.common.dynamicca.jpa.entities.Cert[ certPK=" + certPK + " ]";
    }

}
