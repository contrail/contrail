/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;



import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocketFactory;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.security.auth.x500.X500Principal;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;


import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;


//import org.bouncycastle.util.encoders.Base64;

//import org.bouncycastle.x509.X509V3CertificateGenerator;

import org.junit.*;
import static org.junit.Assert.*;
import org.mindrot.jbcrypt.BCrypt;


/**
 *
 * @author ianjohnson
 */
public class SecurityCommonsTest {

  /*
   * Values for testGetIntegerProperty
   *
   */
  final static String GIP_KEY = "key";
  final static String GIP_VALUE = "1";
  
  
  private static KeyPair eeKeyPair = null;
  private static KeyPair hostKeyPair = null;  
  
  private static X509Certificate eeCert = null;
  
  private static X509Certificate hostCert = null;
  
  
  static String caIssuer = null;
  static String caSubject = null;
  
  static X500Principal issuerOrderedX500P;
  
  static X500Name issuerOrderedX500Name;
  
  
  private static KeyPair caKeyPair = null;
  
  static PrivateKey caPrivate = null;
  static X509Certificate caCert = null;
  

 
  static PKCS10CertificationRequest request = null;
  
  final String fakeCert = "-----BEGIN CERTIFICATE-----\n"
    + "MIIDFjCCAf6gAwIBAgIBAjANBgkqhkiG9w0BAQUFADAbMRkwFwYDVQQDDBBDb250\n"
    + "cmFpbCBSb290IENBMB4XDTEyMDIxNTE0NTI0N1oXDTEyMDMxNjE1MDI0N1owDjEM\n"
    + "MAoGA1UEAwwDaWpqMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhu6F\n"
    + "7ej5yKZ9s9n6L0cNYvlG8lEKZbmnLM+SK9nSawboPe2tRY6lrkF/NG99GdD0CapC\n"
    + "j9MKdYXRWVPYq/pHjFjc18NjxqvNtOcgSQ8vQHBxeH3JMnyu9wa3vyuqqTkrAUDn\n"
    + "sMuzW9zKVI0pg5dIBWxxuK44gz35PwZwQqj/yaUgWw98uF/eXLjXPKf5X/SaKq5m\n"
    + "03JgH8Beky8bK62hob6LNQm0A6g2UOPIMrd5xoFRmwTSr3Yt01+6/hqsEqMIyF14\n"
    + "S9hH/9sA8GVPOv7i3RPmuVaLMWc61uNt/gIeK2yvVUFrzZWYPnnhtpJtyjudoR1u\n"
    + "3L1b2z084u44OWicEQIDAQABo3IwcDAfBgNVHSMEGDAWgBQXUsTf+vUrFFb+JIGr\n"
    + "Y1g2XOYTDzAdBgNVHQ4EFgQU+wEiNodlAts5l5V8elsZEuo1ce8wDAYDVR0TAQH/\n"
    + "BAIwADAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwDQYJKoZIhvcN\n"
    + "AQEFBQADggEBAHPzQ0KA0DX7oF+At3Db7F472cVcBCzxvNHiZWyx15YQ8+e8siqQ\n"
    + "9v0ou8RA2P2+aBAox66VrDiadJhGYSdyetuCHvnz9sVZ+4npa9OphwU6JmgrXOxQ\n"
    + "rSQQAd46iIwao80sbdqhN/i9CBuZc2JzJ1nMj/QilwHwzUHqXlExrHcj42Lk5I8Z\n"
    + "FN2BSqAnOorTRXRPqTROdrZql1sh65jA4J/nzxKG7vtPGu63Pb/bZS8GLdgbbGLS\n"
    + "EXDJkRn5yGZTae3/qF0bXhNCqNX8dxMiO2T9dZkhVVGxNVflpsOFf+eAmmCFoNG2\n"
    + "bmCOdw696/uvCj8TfexN1H7gLbQ7EFXm96g=\n"
    + "-----END CERTIFICATE-----\n";
  final String fakeCSR =
    "-----BEGIN CERTIFICATE REQUEST-----\n"
    + "MIICUTCCATkCAQAwDjEMMAoGA1UEAxMDaWpqMIIBIjANBgkqhkiG9w0BAQEFAAOC\n"
    + "AQ8AMIIBCgKCAQEAph95NgnO/ewdTTJvsUqwSRgWA7dDCzBEj3hAcXRnHD2S0SG7\n"
    + "XkOyM4QDT5nT6f0E7rD1d6xXOgQ8pP6vNMbs+M6O6cSvSWx0uiWSfd4Iy/DLjR6/\n"
    + "+WPDkYcpDftFUgbon10S+NGG569ZrVln6tLUL+MIVebuN9mRd4ZOCJgwKmvX4yPx\n"
    + "T9GgKe/cvIqWIZPJnsDbyP9vUKgF3IISPVn3NrtDIvCjc4mQJXWG1syWmDImVqcp\n"
    + "zviVitiSqanDXLLnMO6N//Rmq7zMcM5I8CrZHihpBkeIW4HwYECuKEmYd9YU2lxy\n"
    + "833F7EOe3VvsUYcMfEW3zMW0w/4F0uOzW96wMQIDAQABMA0GCSqGSIb3DQEBCwUA\n"
    + "A4IBAQAlwQgcR7utM7OqGhAlYqQC4nLqcoJ69loYaFpnhfMwD+a4vzKYwGbapldg\n"
    + "0OWc28ND3l8TQ7cfEKTlPK2DPQOz4wcSa+i+NyijU343QNI5Gg1stBe7H4QNgOq+\n"
    + "/FfTgm9EoruogRpIV4RuFOAwRJI0ksRVpQxpO02lNQGR6MKO9GkwjFJkfcNtSK/P\n"
    + "03SH+h0ANXKYf2tjfwyTi+uWoT2fMpysYQSzo1plpa2+dZ7p0UZYzwvDkTwT321u\n"
    + "Lny/2zdge3HyCP1yHmFvXQRLTX58nwAEFGk8S5dSMddNqJE0C98f1WbM2mORzNWk\n"
    + "lMPz4dtWSfu6SAzEqQsMArVxmkK5\n"
    + "-----END CERTIFICATE REQUEST-----\n";
  
  
  
  private SecurityCommons sc = new SecurityCommons();
  
  public SecurityCommonsTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {

    System.getProperties().put(GIP_KEY, GIP_VALUE);

    Security.addProvider(new BouncyCastleProvider());
    


  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
    
    System.out.println("\nsetup\n");
    
    String filename = "src/main/resources/rootca-key.pem";
//    KeyPair caKeyPair = null;
    
    caPrivate = null;
    try {
      caPrivate = sc.readPrivateKey(filename, null);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }  
    
    filename = "src/main/resources/rootca-cert.pem";
    try {

      caCert = sc.readCertificate(new InputStreamReader(new FileInputStream(filename)));

    } catch (FileNotFoundException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
      
    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
       
  }

  @After
  public void tearDown() {
  }
  
  
  public void testGetUserAttributes() throws Exception {
    
      PersistenceUtils pu = PersistenceUtils.getInstance();

      if (pu == null) {
        PersistenceUtils.createInstance("appPU"); // Ideally, should be a getinstance call which does 
                                                  // createInstance if no instance constructed yet 
        
        // TODO - change name of persistence unit to read from config file
      }      
  
    User user = null;
    EntityManager em = PersistenceUtils.getInstance().getEntityManager();

    if (em == null) {

 

        System.err.println("Couldn't create EntityManager");

      }




    String queryString;
    
//    queryString = "SELECT u FROM User u WHERE u.username = :username AND u.password = :password";
    
    queryString = "SELECT u FROM User u WHERE u.username = :username";
    
    
    Query query = em.createQuery(queryString);

    if (query == null) {

      System.err.println("Couldn't create Query");

      }

 
    final String username = "admin";
    
    query.setParameter("username", username);
//    query.setParameter("password", password);

    boolean badMatch = false;
    try {

      user = (User) query.getSingleResult();

      if (user == null) {

        System.err.println("User object from query.getSingleResult is NULL");

        }

      
 //     badMatch = !BCrypt.checkpw(password, user.getPassword());
      
      System.err.println(String.format("BCrypt.checkpw returns false"));
        
 

    } catch (NoResultException ex) {

    System.err.println(String.format("No entry in database for %s.", username));
      
      badMatch = true;

    } catch (NonUniqueResultException ex) {

      System.err.println(String.format("Multiple entries in database for %s.", username));
      
      badMatch = true;
    }      
      
    
  }
  
 @Test
 public  void testBigInteger()
   throws Exception {
   
   System.out.println("bigInteger");
   BigInteger bi = BigInteger.valueOf(-1L);
   
 }

@Test
public void testFor() throws Exception {
  
  int[] ia = new int[0];
  
  for (int i: ia) {
    
  }
}

@Test
public void testBCrypt() throws Exception {
  

  final String password = "password";
  
  String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
  
  System.out.printf("BCrypt of %s is %s .\n", password, hashed);
  
  
  if (!BCrypt.checkpw(password, hashed)) {
    throw new Exception("BCrypt test failed");
  }
  
  hashed = "$2a$12$WIhQdMz46ebR1/...vLYb.DlGUVa1q/VntTyt6NiR7FxmaUd1.1uy";
  
  System.out.printf("BCrypt of %s is %s .\n", password, hashed);



  if (!BCrypt.checkpw(password, hashed)) {
    throw new Exception("BCrypt test failed");
  }
  
}
  

  
//  @Test
  public void testDigest() {

    System.out.println("digest");

    String message = "Hello, World";
    try {
      
 //     final char[] expectedDigest = "907d14fb3af2b0d4f18c2d46abe8aedce17367bd".toCharArray(); // SHA-1
      
      final char[] expectedDigest = "03675ac53ff9cd1535ccc7dfcdfa2c458c5218371f418dc136f2d19ac1fbe8a5".toCharArray(); // SHA-356
      
      
      
      
      final byte[] md1 = sc.sha256digest(message.getBytes());
      
 //     System.out.println(Hex.encodeHex(expectedDigest)); 
      
 //     System.out.println(Hex.encodeHex(md1));
      
      char[] gendCharArr = Hex.encodeHex(md1);
      
      String genDigest = new String(md1);
      
      
      if (!Arrays.equals(gendCharArr, expectedDigest)) {
        
        System.err.println("Expected");
        System.err.println((expectedDigest));
        System.err.println(gendCharArr);
        
        int i = 0;
        for (char c: gendCharArr) {
          
          if (c != expectedDigest[i]) {
            
            System.err.printf("Exp: %c, got %c.\n", expectedDigest[i], c );
          }
          ++i;
        }
//        char c = expectedDigest[0];
 
      }
      

      
//      System.out.println("Digests are equal: " + MessageDigest.isEqual(md1, expectedDigest));
      
      
//      assertEquals(md1, expectedDigest);

    } catch (NoSuchAlgorithmException ex) {
      ;
    } catch (NoSuchProviderException ex) {
      ;
    } 
    
  }
  
  
//  @Test
//  public void testGetUser() {
//    
//    System.out.println("getUser");
//    
//    final String username = "bertfiggis";    
//    
//    User nullUser = sc.getUserNyName(username, "badpass");
//    
//    assertNull(nullUser);
//    
//    
//    User goodUser = sc.getUserNyName(username, "goodpass");
//    
//    assertNotNull(goodUser);
//    
//    assertEquals(goodUser.getUsername(), username);
//    
//    assertEquals((Object)goodUser.getUserId(), 99);
//    
//    assertEquals(goodUser.getEmail(), "bert.figgis@contrail.eu");
//      
//    
//    
//    
//  }
  

  
  @Test
  public void testLoadPrivateKeyEntry() {

    System.out.println("loadPrivateKeyEntry");
    try {

      KeyStore client = KeyStore.getInstance("PKCS12");
      char[] password = "client".toCharArray();
      final String filename = "./src/test/resources/keystore.p12";

      FileInputStream fis = new FileInputStream(filename);

      client.load(fis, password);
      
      fis.close();
      
      final String alias = "client";

      KeyStore.PrivateKeyEntry pkEntry = 
        (KeyStore.PrivateKeyEntry) client.getEntry(alias, new KeyStore.PasswordProtection(password));
      
      
      X509Certificate[] certs = (X509Certificate[])pkEntry.getCertificateChain();
      
      Assert.assertNotNull(certs);
      Assert.assertTrue(certs.length > 0);
      
      X509Certificate peerCert = certs[0];
      
      Assert.assertNotNull(peerCert);
      
      final String subjectName = peerCert.getSubjectDN().getName();
      
      final String[] CNs = sc.getRDNs(subjectName, BCStyle.CN); // returns RDN values w/o tag e.g. 'CN='
      
      Assert.assertNotNull(CNs);
      Assert.assertTrue(CNs.length > 0);
      
      String CN = CNs[0];
      
      Assert.assertEquals(CN, "99");
      
      
    } catch (UnrecoverableEntryException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);

    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (CertificateException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);


    } catch (KeyStoreException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }

  }


  
  @Test
  public void testReadCertFromString() {
    
    System.out.println("readCertFromString");


    X509Certificate cert = null;

    try {

      cert = sc.getCertFromStream(
      (new ByteArrayInputStream(fakeCert.getBytes())));      
      
//      cert = sc.readCertificate(
//        new InputStreamReader(new ByteArrayInputStream(
//        fakeCert.getBytes())));
      
      final String issuerName = cert.getIssuerDN().getName();
      
      assertEquals(issuerName, "CN=Contrail Root CA");
      
      final String subjectName = cert.getSubjectDN().getName();
      
      final String expectedCN = "CN=ijj";
      
      assertEquals(subjectName, expectedCN);
      
      Assert.assertTrue(sc.authorise(new X509Certificate[] { cert }, expectedCN));
      
//      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      
//      OutputStreamWriter osw = new OutputStreamWriter(baos);
      
      
    } catch (CertificateException ex) {
      System.err.println(ex);

    } 


  }
  
  @Test
  public void testAuthorisedCN() throws Exception {
    
    System.out.println("authorisedCN");
    
    
    final String host1 = "CN=one-test.contrail.rl.ac.uk";
    
    final String CN1 = host1;
    Assert.assertTrue(sc.authorisedCN(CN1, host1));

   
    final String host2 = "/C=GB"+ "/" + host1;      

    
    final String CN2 = host1 + "," + host2;
    
    Assert.assertTrue(sc.authorisedCN(CN2, host2));    
    
  }

  
  @Test
  public void testCertFactory() throws Exception {
    
    System.out.println("certFactory");
    
    ByteArrayInputStream is = new ByteArrayInputStream(fakeCert.getBytes());
    
    CertificateFactory cf;
    X509Certificate cert;
    
    try {
      cf = CertificateFactory.getInstance("X.509");
      
      cert = (X509Certificate)cf.generateCertificate(is);
      
      assertNotNull("Certificate is NULL", cert);
      
      final String issuerName = cert.getIssuerDN().getName();
      final String expectedIssuerName = "CN=Contrail Root CA";
      assertEquals(String.format("Issuer name isn't expected ('%s')", expectedIssuerName), 
        issuerName, expectedIssuerName);
      
      final String subjectName = cert.getSubjectDN().getName();
      final String expectedSubjectName = "CN=ijj";
      assertEquals(String.format("Subject name isn't expected ('%s')", expectedSubjectName), 
        subjectName, expectedSubjectName);      
      
      
    } catch (CertificateException ex) {
      ;
    }
    
  }
//  @Test
  public void testSetCertValidity() {
    
    System.out.println("setCertValidity");
    
    final int days = 30;
    
    final int hours = 0;
    
    final int minutes = 0;
    
    Date[] beforeAndAfter = sc.getValidityRange(days, hours, minutes);
    
//    System.err.printf("notBefore = %s, notAfter = %s.\n", 
//      beforeAndAfter[0].toString(), beforeAndAfter[1].toString());
    
    
  }
  


  private  void createRootCertificate(final KeyPair caKeyPair, final String p_caIssuer, final String signature, final int lifetime) {  

    caIssuer = p_caIssuer;
    caSubject = caIssuer;
    
    final int serial = 1;

    try {

      caCert = sc.createRootCertificate(
        caKeyPair, caIssuer, signature,lifetime, serial);

    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
    }
    
    
    try {

      System.out.println("CA Root Cert:\n");
      sc.writeCertificate(System.out, caCert);
      
      System.out.println("CA Keypair\n");
      sc.writeKey(System.out, caKeyPair.getPrivate());

    } catch (IOException ex) {
      System.err.println(ex);
    }    

    String generatedIssuer = caCert.getIssuerDN().getName();
    
    
    X500Principal x5p = caCert.getIssuerX500Principal(); // This is returned in reverse order
    String orderedX500Principal = sc.reverse(x5p.toString(), ",");
    
    issuerOrderedX500Name = new X500Name(orderedX500Principal);
    
    issuerOrderedX500P = new X500Principal(orderedX500Principal); 
    
      
//    System.out.printf("\nX500Principal = %s.\n", x5p);
//    
//    System.out.printf("Ordered X500Principal String = %s.\n", orderedX500Principal);  
//    System.out.printf("\nissuerOrderedX500Name = %s.\n", issuerOrderedX500Name);
//    
//    System.out.printf("issuerOrderedX500P = %s.\n\n", issuerOrderedX500P);
     

    String generatedSubject = caCert.getSubjectDN().getName();

    assertEquals(generatedIssuer, generatedSubject);

    try {
      
      String subjKIDStr = sc.getExtensionValueAsOctetString(caCert, "2.5.29.14");
      String authKIDStr = sc.getExtensionValueAsOctetString(caCert, "2.5.29.35");      
      
    } catch (IOException ex) {
      System.err.println(ex);
    }




//    System.err.printf("subjIDStr = %s.\n", subjKIDStr);
//    System.err.printf("authIDStr = %s.\n", authKIDStr);

    try {

      caCert.verify(caKeyPair.getPublic());

//      System.err.println("Root cert verified OK");

      boolean[] keysBits = caCert.getKeyUsage();

      if (keysBits != null) {

        for (int i = 0; i < keysBits.length; i++) {
//          System.err.printf("%b\t", keysBits[i]);
        }
        //       System.err.println();

      }

      List<String> ekus = caCert.getExtendedKeyUsage();

      if (ekus != null) {

        for (String ku : ekus) {
//          System.err.printf("%s\t", ku);
        }

      }


    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
    } catch (NoSuchProviderException ex) {
      System.err.println(ex);
    } catch (SignatureException ex) {
      System.err.println(ex);
    }

  }
    
  private void createEEKeyPair(
    final String cipher, 
    final int keylen) {

    try {

      eeKeyPair = sc.generateKeyPair(cipher, keylen);

    } catch (NoSuchAlgorithmException ex) {
      ;
    }
  }
  
  
  private void createHostKeyPair(
    final String cipher, 
    final int keylen) {

    try {

      hostKeyPair = sc.generateKeyPair(cipher, keylen);

    } catch (NoSuchAlgorithmException ex) {
      ;
    }
  }
  
  
//  private PKCS10CertificationRequest createCSRFromString() {
//    
//    PKCS10CertificationRequest csr = null;
//    
//
//    
//    try {
//      
//      csr = sc.readCSR(new ByteArrayInputStream(fakeCSR.getBytes()));
//      
//      final String expectedSubjectName = "CN=i";
//      
//      final String subjectNameFromCSR = csr.getCertificationRequestInfo().getSubject().toString();
//      
//      assertEquals( String.format("subjectName %s from CSR doesn't match",expectedSubjectName), 
//        expectedSubjectName, subjectNameFromCSR);
//    
//    } catch (IOException ex) {
//      System.err.println(ex.getLocalizedMessage());
//    }
//    
//    return csr;
//    
//    
//  }
  
  private void createUserEECSR() {

    try {  
      
      final String CN = "CN";
      final String expectedName = "1";
      
      final String eeSubjectName = CN + "=" + expectedName;
      
      request =
        new PKCS10CertificationRequest("SHA256withRSA", new X500Principal(eeSubjectName), 
        eeKeyPair.getPublic(), null, eeKeyPair.getPrivate());

      final PublicKey publicKey = request.getPublicKey();
     
      assertEquals("Didn't find expected public key", publicKey, eeKeyPair.getPublic());
      
      @SuppressWarnings("deprecation")
      final String subjectNameFromCSR = request.getCertificationRequestInfo().getSubject().toString();
      
      assertEquals( String.format("subject name '%s' from CSR is wrong", subjectNameFromCSR),
        eeSubjectName, subjectNameFromCSR);  
      
      String name = eeSubjectName.split("=")[1];
      
      assertEquals(expectedName, name);

      
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (NoSuchProviderException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (InvalidKeyException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (SignatureException ex) {
      System.err.println(ex.getLocalizedMessage());
    }
      
    
  }
    
  
  private void createHostEECSR() {


    try {  
      
      final String eeHostName = "one-test.contrail.rl.ac.uk";
      
      request =
        new PKCS10CertificationRequest("SHA256withRSA", new X500Principal("CN=" + eeHostName), hostKeyPair.getPublic(), null, hostKeyPair.getPrivate());

      final PublicKey publicKey = request.getPublicKey();
     
      assertEquals("Didn't find expected public key", publicKey, hostKeyPair.getPublic());
      
      
      
      
      
      @SuppressWarnings("deprecation")
      
      
      final X509Name subjectX509Name = request.getCertificationRequestInfo().getSubject();
      
      final String subjectNameFromCSR = subjectX509Name.toString();
      
      assertEquals( String.format("subject name '%s' from CSR is wrong", subjectNameFromCSR),
        "CN="+eeHostName, subjectNameFromCSR);  
      
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (NoSuchProviderException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (InvalidKeyException ex) {
      System.err.println(ex.getLocalizedMessage());
    } catch (SignatureException ex) {
      System.err.println(ex.getLocalizedMessage());
    }
      
    
  }  
  
  

  /**
   * 
   * Test of certificate creation
   */
  
  @Test
  public void testCreateUserCertificate() {
    
    String before = "DC=eu,DC=not-contrail,DC=ca";
    
    String after = StringUtils.replace(before, "," , ", ");
    
    System.out.printf("Before = %s.", before);
    System.out.printf("After = %s.", after);


    System.out.println("createUserCertificate");
    try {
      caKeyPair = sc.generateKeyPair("RSA", 2048);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }

    createRootCertificate(caKeyPair, 
      "DC=eu,DC=not-contrail,DC=ca",
      "SHA256withRSA", 5*365);

    createEEKeyPair("RSA", 2048);
    
    createUserEECSR();
    
//    createCSRFromString();


    try {
      
      request.verify();
      
      System.out.println("created User EE CSR:");
      sc.writeCSR(System.out, request);

      String subjectNameStr = "CN=ijj, CN=42";

      final BigInteger serial = BigInteger.valueOf(2);

      PublicKey eePK = eeKeyPair.getPublic();
      
      String issuerName = caCert.getSubjectDN().getName();
      
      System.out.printf("issuerName = %s.\n", issuerName);

      eeCert = sc.createUserCertificate(
        eePK, new X500Principal(issuerName + "," + subjectNameStr), UUID.randomUUID().toString(), serial, caCert, 
        caKeyPair.getPrivate(), "SHA256withRSA", 
        30, 0, 0);
             
      final String generatedIssuer =  eeCert.getIssuerDN().toString();
      
      String generatedSubject = eeCert.getSubjectDN().toString();
      
      final BigInteger generatedSerial = eeCert.getSerialNumber();
       
      
      assertEquals(generatedIssuer, caIssuer);
      
//      assertEquals(generatedSubject, subjectNameStr);
      
      assertEquals(generatedSerial.intValue(), serial.intValue());
      
      eeCert.verify(caKeyPair.getPublic());
     
      
      boolean[] keysBits = eeCert.getKeyUsage();

//      if (keysBits != null) {
//
//        for (int i = 0; i < keysBits.length; i++) {
//          System.err.printf("%b\t", keysBits[i]);
//        }
//
//      }

      
//      List<String> ekus = eeCert.getExtendedKeyUsage();
//
//      for (String ku: ekus) {
//        
//        System.err.printf("%s\t", ku);
//      }
      


      try {
        
        System.out.println("User EE Certificate:");
        sc.writeCertificate(System.out, eeCert);    

      } catch (IOException ex) {
        System.err.println(ex);
      }

    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
    } catch (NoSuchProviderException ex) {
      System.err.println(ex);
    } catch (SignatureException ex) {
      System.err.println(ex);
    } catch (OperatorCreationException ex) {
      System.err.println(ex);
    } catch (IOException ex) {
      System.err.println(ex.getLocalizedMessage());
    }
    
  }
  
  
  @Test
  public void testDNappending() {
    
    System.out.println("DNappending");
    
    X500Principal root = new X500Principal("DC=eu,DC=contrail-project,DC=ca,DC=users");
    
    System.out.println(root);
    
    X500Principal user = new X500Principal(root + "," + "CN=UUID,CN=bert figgis");
    
    System.out.println(user);
    
//    X500Principal rootPrincipalFromCert = caCert.getSubjectX500Principal();
    
//    System.out.println("rootPrincipalFromCert:");
//    System.out.println(rootPrincipalFromCert);
    
//    System.out.println("rootPrincipalFromCert.getName():");
//    System.out.println(rootPrincipalFromCert.getName());
        
    
//    System.out.println("Now let's see that reversed:");
//    System.out.println(sc.reverse(rootPrincipalFromCert.getName(), ","));
    
    
    String filename = "src/main/resources/rootca-key.pem";
//    KeyPair caKeyPair = null;
    
    PrivateKey caPrivate = null;
    try {
      caPrivate = sc.readPrivateKey(filename, null);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
  
    X509Certificate newCACert = null;
    
    filename = "src/main/resources/rootca-cert.pem";
    try {

      newCACert = sc.readCertificate(new InputStreamReader(new FileInputStream(filename)));

    } catch (FileNotFoundException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
      
    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    caCert = newCACert;
    
    Principal rootcaSubject = newCACert.getSubjectDN();
    
    final byte[] authKeyID = newCACert.getExtensionValue("2.5.29.35");
    
    System.out.println("\nrootcaSubject");
    System.out.println(rootcaSubject);
    
    caIssuer = newCACert.getIssuerDN().getName();
    caSubject = caIssuer;    
    
    String rootcaSubjectAsString = rootcaSubject.toString();
    System.out.println("rootcaSubjectAsString:");
    System.out.println(rootcaSubjectAsString + "\n");
    
    String rootcaSubjectGetName = rootcaSubject.getName();
    System.out.println("rootcaSubject.getName():");
    System.out.println(rootcaSubjectGetName + "\n");
 
    final String cnString = "CN=1,CN=user name";
    
    X500Principal cnPrincipal = new X500Principal(cnString);
    
    System.out.println("cnPrincipal");
    System.out.println(cnPrincipal + "\n");
    
    user = new X500Principal(rootcaSubjectGetName + "," + cnString );
    
//    System.out.println("X500Principal = Concatenated rootcaSubjectGetName + , + (String) CN=UUID,CN=user name");
    
    System.out.println("user");
    System.out.println(user + "\n");    
    
    System.out.println("user.toString()");
    System.out.println(user.toString() + "\n");
    
    System.out.println("\nstring concat of rootcaSubjectAsString + \",\" + cnString");
    
    System.out.println(rootcaSubjectAsString + "," + cnString + "\n");
    
    X500Name x500name = new X500Name(user.toString());
    
    System.out.println("X500Name");
    System.out.println(x500name);
    
    KeyPair eePK = null;
    
    BigInteger serial = new BigInteger("1");
    
    
    try {
      eePK = sc.generateKeyPair("RSA", 2048);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
      System.out.println("NSAE");
    }
    
    if (eePK == null) {
      
      System.out.println("eepK is NULL");
    }
    
    PrivateKey priv = eePK.getPrivate();
    
    if (priv == null) {
      
      System.out.println("Private part is null");
      
    }
    
    UUID randomUUID = UUID.randomUUID();
    try {    
    
      eeCert = sc.createUserCertificate(
        eePK.getPublic(), user, 
        randomUUID.toString(), /* new X500Principal(subjectNameStr) */ serial, newCACert, 
         priv,  /*  caKeyPair */  "SHA256withRSA", 
        30, 0, 0);    
    } catch (CertificateException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (InvalidKeyException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (OperatorCreationException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    String[] dcArr = sc.getRDNs(user.toString(), BCStyle.DC);
    
    
    for (String s: dcArr) {
//      System.out.println(s);
    }
        
    
    Principal prinFromCert = eeCert.getSubjectDN();
    
    String prinString = prinFromCert.toString();
    
//    System.out.println("prinString here");
//    System.out.println(prinString);
    
    dcArr = sc.getRDNs(prinString, BCStyle.DC);
    
    System.out.println("DCs");
    
    for (String s: dcArr) {
//      System.out.println(s);
    }
    
    String[] cnArr = sc.getRDNs(prinString, BCStyle.CN);
    
    System.out.println("CNs");
    for (String cn: cnArr) {
//      System.out.println(cn);
    }
    
    String[] newCN = null;
    
    newCN = sc.findRDNs(prinString, BCStyle.CN, BCStyle.STREET  );
    
//    System.out.println("newCN with desired RDN first");
    
//    assertEquals("CN=1,CN=user name", newCN);
    
    newCN = sc.findRDNs(prinString,  BCStyle.STREET, BCStyle.CN  );
    
//    System.out.println("newCN, with Desired RDN last");
    
//    assertEquals("user name", newCN);
    
    newCN = sc.findRDNs(user.toString(), 
      BCStyle.STREET, BCStyle.COUNTRY_OF_CITIZENSHIP, BCStyle.GENERATION );
    
//    System.out.println("newCN, with Desired RDN type not in list");
    
    assertNull("user name", newCN);    
     
     
     
     
      
      Collection subjectAltNames = null;
      
      
      
      try {
        
        subjectAltNames = eeCert.getSubjectAlternativeNames();
      } catch (CertificateParsingException ex) {
        
        System.err.println(ex);
        
      }
      
      

      assertNotNull(subjectAltNames);

      String userUUID = 
          sc.findSubjectAltName(subjectAltNames, GeneralName.uniformResourceIdentifier);
 
      assertEquals("urn:uuid:" + randomUUID, userUUID);     
     
     
     if (sc.isUserId("1") ) {
       ;
     } else {
       fail("Digit 1 is definitely a UserID");
     }
     
  
     if (sc.isUserId("A") ) {
       fail("Letter A is definitely NOT a UserId");
     }
     
     


    if (sc.isUUID("782026d7-bd7d-45c9-875c-081d6f534709") ) {
      ;
    } else {
      fail("Should have parsed 782026d7-bd7d-45c9-875c-081d6f534709 as an UUID");
    }    
 
    
    if (sc.isUUID("782026d7-bd7d-45c9-875c-081d6f534709XXX")) {
      fail("Should have failed: 'wobblers' is not a UUID");
    }  
//    782026d7-bd7d-45c9-875c-081d6f534709
//    assertEquals(rootcaSubjectAsString + "," + cnString, user.toString());
    
    System.out.println("eeCert.getSubjectDN().toString():");
    System.out.println(eeCert.getSubjectDN().toString());
    
    System.out.println("user Cert:");
    try {    
      sc.writeCertificate(System.out, eeCert);
    } catch (IOException ex) {
      Logger.getLogger(SecurityCommonsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
   
    
  }  
  
  /**
   * 
   * Test of certificate creation
   */
  
  @Test
  public void testCreateHostCertificate() {


    System.out.println("createHostCertificate");

//   testDNappending();
//
//    createCACertificate();
//
    createHostKeyPair("RSA", 2048);

    createHostEECSR();
    
    try {
      
      request.verify();
      
      System.out.println("Generated Host CSR:");
      sc.writeCSR(System.out, request);

      String hostNameStr = "client-99.contrail.rl.ac.uk";

      final int serial = 169;

      PublicKey hostPK = hostKeyPair.getPublic();
      
      X509Certificate prodCAcert = 
        sc.getCertFromStream(new FileInputStream("src/test/resources/rootca-cert.pem"));
      PrivateKey prodCAkey = sc.readPrivateKey("src/test/resources/rootca-key.pem", null);
      
 //     caIssuer = prodCAcert.getSubjectDN(); // .toString();
      
      Principal issuer = prodCAcert.getSubjectDN();

      hostCert = sc.createHostCertificate(
        hostPK, hostNameStr, BigInteger.valueOf(serial), prodCAcert,
        prodCAkey, "SHA256withRSA", 
        new Date(), 30, 0, 0);

      final String generatedIssuer = hostCert.getIssuerDN().toString();
      
      Principal issuerFromCert = hostCert.getIssuerDN();
      
//      assertEquals(issuer, issuerFromCert);

      String generatedSubject = hostCert.getSubjectDN().toString();

      final BigInteger generatedSerial = hostCert.getSerialNumber();

//      assertEquals(generatedIssuer, sc.reverse(caIssuer, ","));

      assertEquals(generatedSubject, "CN=" + hostNameStr);

      assertEquals(generatedSerial.intValue(), serial);

      hostCert.verify(prodCAcert.getPublicKey());

      boolean[] keysBits = hostCert.getKeyUsage();
      
      

      
      Collection subjectAltNames = hostCert.getSubjectAlternativeNames();
      
      

      assertNotNull(subjectAltNames);

      String hostDnsAltName = 
          sc.findSubjectAltName(subjectAltNames, GeneralName.dNSName);
 
      assertEquals(hostNameStr, hostDnsAltName);
      
      
      System.out.print("Host Private Key\n");
      
      sc.writeKey(System.out, hostKeyPair.getPrivate());
    
    
      System.out.printf("\nHost Certificate:\n");

      sc.writeCertificate(System.out, hostCert);
      
    


    } catch (IOException ex) {
      System.err.println(ex);


    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
      ex.printStackTrace();
    } catch (NoSuchProviderException ex) {
      System.err.println(ex);
    } catch (SignatureException ex) {
      System.err.println(ex);
    } catch (OperatorCreationException ex) {
      System.err.println(ex);
    }
   
  }


  /**
   * Test of addStringExtension method, of class sc.
   */
//  @Test
//  public void testAddStringExtension() {
//    System.out.println("addStringExtension");
//    X509V3CertificateGenerator certGen = null;
//    String oid = "";
//    boolean critical = false;
//    String value = "";
//    sc.addStringExtension(certGen, oid, critical, value);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }

  /**
   * Test of getIntegerProperty method, of class sc.
   */
//  @Test
  public void testGetIntegerPropertyExpectedValueMatchesRetrievedValue() {
    
    System.out.println("getIntegerPropertyExpectedValueMatchesRetrievedValue");
    Properties props = new Properties();
    
    String key = GIP_KEY;
    

    
    int defaultValue = 0;
    int expResult = 1;
    int result = sc.getIntegerProperty(System.getProperties(), key, defaultValue);

    
    assertEquals(expResult, result);


  }

  
  /**
   * Test of getIntegerProperty method, of class sc. 
   * Supply an invalid key in order to get the 'defaultValue' returned
   * 
   */
//  @Test
  public void testGetIntegerPropertyMissingKey() {
    
    System.out.println("getIntegerPropertyMissingKey");
    Properties props = new Properties();
    
    String key = GIP_KEY + "_absent";
    
    int defaultValue = Integer.valueOf(GIP_VALUE);
    int expResult = Integer.valueOf(GIP_VALUE);
    
    int result = sc.getIntegerProperty(System.getProperties(), key, defaultValue);

    assertEquals (expResult, result);
    
  }  
  /**
   * Test of generateKeyPair method, of class sc.
   */
//  @Test
  public void testGenerateKeyPair() throws Exception {
    System.out.println("generateKeyPair");
    String algorithm = "RSA";
    int keylen = 2048;
    KeyPair expResult = null;
    KeyPair result = sc.generateKeyPair(algorithm, keylen);
    
   
    int privateLength = result.getPrivate().getEncoded().length;
    
 //   System.out.printf("Length of private key encoding is %d.\n", privateLength);
      
    assertEquals(result.getPrivate().getAlgorithm(), algorithm);
  
    // TODO review the generated test code and remove the default call to fail.

  }

//  /**
//   * Test of getRequestedDuration method, of class sc.
//   */
//  @Test
//  public void testGetRequestedDuration() {
//    System.out.println("getRequestedDuration");
//    String requestedPeriod = "";
//    String periodType = "";
//    int expResult = 0;
//    int result = sc.getRequestedDuration(requestedPeriod, periodType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getDays method, of class sc.
//   */
//  @Test
//  public void testGetDays() {
//    System.out.println("getDays");
//    X509Extensions extensions = null;
//    int expResult = 0;
//    int result = sc.getDays(extensions);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of expandTilde method, of class sc.
//   */
//  @Test
//  public void testExpandTilde() throws Exception {
//    System.out.println("expandTilde");
//    String filename = "";
//    String expResult = "";
//    String result = sc.expandTilde(filename);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readLine method, of class sc.
//   */
//  @Test
//  public void testReadLine() throws Exception {
//    System.out.println("readLine");
//    String filename = "";
//    String expResult = "";
//    String result = sc.readLine(filename);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getExtensionValueByOID method, of class sc.
//   */
//  @Test
//  public void testGetExtensionValueByOID() {
//    System.out.println("getExtensionValueByOID");
//    X509Extensions extensions = null;
//    String oid = "";
//    String expResult = "";
//    String result = sc.getExtensionValueByOID(extensions, oid);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getExtensionValue method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetExtensionValue() {
//    System.out.println("getExtensionValue");
//    X509Extension ext = null;
//    String expResult = "";
//    String result = SecurityUtils.getExtensionValue(ext);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getPass method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetPass() throws Exception {
//    System.out.println("getPass");
//    String passSource = "";
//    char[] expResult = null;
//    char[] result = SecurityUtils.getPass(passSource);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeKey method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteKey_3args() throws Exception {
//    System.out.println("writeKey");
//    String userKeyFilename = "";
//    PrivateKey priv = null;
//    char[] keyPassphrase = null;
//    SecurityUtils.writeKey(userKeyFilename, priv, keyPassphrase);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeKey method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteKey_4args_1() throws Exception {
//    System.out.println("writeKey");
//    String userKeyFilename = "";
//    PrivateKey priv = null;
//    char[] keyPassphrase = null;
//    String alg = "";
//    SecurityUtils.writeKey(userKeyFilename, priv, keyPassphrase, alg);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeCertificate method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteCertificate_X509Certificate_String() throws Exception {
//    System.out.println("writeCertificate");
//    X509Certificate userCert = null;
//    String userCertFilename = "";
//    SecurityUtils.writeCertificate(userCert, userCertFilename);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of createCRLDistPoint method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetCRLDistPoint() {
//    System.out.println("createCRLDistPoint");
//    String[] crlURIs = null;
//    CRLDistPoint expResult = null;
//    CRLDistPoint result = SecurityUtils.createCRLDistPoint(crlURIs);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeCertificateChain method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteCertificateChain() throws Exception {
//    System.out.println("writeCertificateChain");
//    X509Certificate[] certChain = null;
//    String userCertFilename = "";
//    SecurityUtils.writeCertificateChain(certChain, userCertFilename);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeCSR method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteCSR_PKCS10CertificationRequest() throws Exception {
//    System.out.println("writeCSR");
//    PKCS10CertificationRequest request = null;
//    String expResult = "";
//    String result = SecurityUtils.writeCSR(request);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeCSR method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteCSR_PKCS10CertificationRequest_String() throws Exception {
//    System.out.println("writeCSR");
//    PKCS10CertificationRequest request = null;
//    String csrFilename = "";
//    SecurityUtils.writeCSR(request, csrFilename);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getUsername method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetUsername() {
//    System.out.println("getUsername");
//    Console console = null;
//    String expResult = "";
//    String result = SecurityUtils.getUsername(console);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getPassphrase method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetPassphrase() {
//    System.out.println("getPassphrase");
//    Console console = null;
//    int minLength = 0;
//    char[] expResult = null;
//    char[] result = SecurityUtils.getPassphrase(console, minLength);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getExtensions method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetExtensions() {
//    System.out.println("getExtensions");
//    ASN1Set attributes = null;
//    X509Extensions expResult = null;
//    X509Extensions result = SecurityUtils.getExtensions(attributes);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of findSubjectAltName method, of class SecurityCommons.
//   */
//  @Test
//  public void testFindSubjectAltName() {
//    System.out.println("findSubjectAltName");
//    Collection subjectAltNames = null;
//    int nameType = 0;
//    String expResult = "";
//    String result = SecurityUtils.findSubjectAltName(subjectAltNames, nameType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getAltName method, of class SecurityCommons.
//   */
//  @Test
//  public void testGetAltName() {
//    System.out.println("getAltName");
//    GeneralName[] names = null;
//    int tagNo = 0;
//    String expResult = "";
//    String result = SecurityUtils.getAltName(names, tagNo);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readX509Certificate method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadX509Certificate() throws Exception {
//    System.out.println("readX509Certificate");
//    String keyFilename = "";
//    char[] password = null;
//    X509Certificate expResult = null;
//    X509Certificate result = SecurityUtils.readX509Certificate(keyFilename, password);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
  /**
   * Test of readKeyPair method, of class SecurityCommons.
   */
  @Test
  public void testPrivateKey() throws Exception {
    System.out.println("readPrivateKey");
    String keyFilename = "src/test/resources/rootca-key.pem";;
    char[] password = null;
    KeyPair expResult = null;
    PrivateKey result = sc.readPrivateKey(keyFilename, password);
    
    System.err.printf("\nAlgorithm: %s\n", result.getAlgorithm());
       
    
    

 //   assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
 //   fail("The test case is a prototype.");
  }
  
 
  
  @Test
  public void testReadDefaultTruststore() throws Exception {
    
    System.out.println("readDefaultTruststore");
    
    SSLSocketFactory sslFact = null;
    
    sslFact = (SSLSocketFactory)SSLSocketFactory.getDefault();
    
    
  }
//
//  /**
//   * Test of readPEM method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadPEM_3args() throws Exception {
//    System.out.println("readPEM");
//    String keyFilename = "";
//    char[] password = null;
//    String type = "";
//    Object expResult = null;
//    Object result = SecurityUtils.readPEM(keyFilename, password, type);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readCSR method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadCSR_InputStream() throws Exception {
//    System.out.println("readCSR");
//    InputStream is = null;
//    PKCS10CertificationRequest expResult = null;
//    PKCS10CertificationRequest result = SecurityUtils.readCSR(is);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readCSR method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadCSR_String() throws Exception {
//    System.out.println("readCSR");
//    String csrInFilename = "";
//    PKCS10CertificationRequest expResult = null;
//    PKCS10CertificationRequest result = SecurityUtils.readCSR(csrInFilename);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readCertificate method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadCertificate() throws Exception {
//    System.out.println("readCertificate");
//    InputStreamReader isr = null;
//    X509Certificate expResult = null;
//    X509Certificate result = SecurityUtils.readCertificate(isr);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readPEM method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadPEM_InputStreamReader() throws Exception {
//    System.out.println("readPEM");
//    InputStreamReader isr = null;
//    Object expResult = null;
//    Object result = SecurityUtils.readPEM(isr);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeKey method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteKey_4args_2() throws Exception {
//    System.out.println("writeKey");
//    OutputStream os = null;
//    PrivateKey key = null;
//    String alg = "";
//    char[] password = null;
//    SecurityUtils.writeKey(os, key, alg, password);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeKey method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteKey_OutputStream_PrivateKey() throws Exception {
//    System.out.println("writeKey");
//    OutputStream os = null;
//    PrivateKey key = null;
//    SecurityUtils.writeKey(os, key);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of writeCertificate method, of class SecurityCommons.
//   */
//  @Test
//  public void testWriteCertificate_OutputStreamWriter_X509Certificate() throws Exception {
//    System.out.println("writeCertificate");
//    OutputStreamWriter osw = null;
//    X509Certificate cert = null;
//    SecurityUtils.writeCertificate(osw, cert);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of readPrivateKey method, of class SecurityCommons.
//   */
//  @Test
//  public void testReadPrivateKey() throws Exception {
//    System.out.println("readPrivateKey");
//    String keyFilename = "";
//    char[] keyPassphrase = null;
//    KeyPair expResult = null;
//    KeyPair result = SecurityUtils.readPrivateKey(keyFilename, keyPassphrase);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
}
