/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

/**
 *
 * @author ijj
 */
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertPath;

import java.security.cert.CertPathValidator;

import java.security.cert.CertPathValidatorException;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
 
import java.security.cert.X509Certificate;
import java.util.*;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//import javax.security.auth.x500.X500Principal;
/**
 * Basic example of certificate path validation
 */
public class CertificatePathValidator {
  
  private List list;
  
  PKIXParameters param;
  
  Set<TrustAnchor> trust;
  
  CertPath certPath;
    
  CertPathValidator validator;
  

  
  public CertificatePathValidator(
    final X509Certificate rootCert, 
/*    final PrivateKey rootKey, */
    final X509CRL rootCRL, 
//    X509Certificate[] interCerts, 
//    X509CRL[] interCRLs,
    final X509Certificate endCert) 
    
  throws 
    InvalidAlgorithmParameterException,
    NoSuchAlgorithmException, 
    NoSuchProviderException, 
    CertificateException { 
    
    // create CertStore to support validation

    list = new ArrayList();
    list.add(rootCert);

//    for (X509Certificate c: interCerts) {
//      list.add(c);
//    }

    list.add(endCert);
    list.add(rootCRL);

//    for (X509CRL c: interCRLs) {
//      list.add(c);
//    } 

    CollectionCertStoreParameters params = new CollectionCertStoreParameters(list);
    CertStore store = CertStore.getInstance("Collection", params, "BC");

    // create certificate path

    CertificateFactory fact = CertificateFactory.getInstance("X.509", "BC");
    List<X509Certificate> certChain = new ArrayList<X509Certificate>();

    certChain.add(endCert);

//    for (X509Certificate c: interCerts) {
//      certChain.add(c);
//    }


    certPath = fact.generateCertPath(certChain);
    Set<TrustAnchor> trust = Collections.singleton(new TrustAnchor(rootCert, null));

    // prepare for validation
    validator = CertPathValidator.getInstance("PKIX", "BC");
    param = new PKIXParameters(trust);

    param.addCertStore(store);

  }

  
  public void setTargetCertConstraints(final X509CertSelector selector) {
    
    param.setTargetCertConstraints(selector);       
    
  }
  
  public void validate() throws CertPathValidatorException, InvalidAlgorithmParameterException {
    
    param.setDate(new Date());    
    validator.validate(certPath, param);
    
  }

  public static void main(
    String[] args)
    throws Exception {

    
    if (args.length != 4) {
      
      System.err.println("Usage: validate-cert rootCert endCert");
      System.exit(-1);
      
    }
    final String rootCertName = args[0];
    
//    final String rootKeyName = args[1];
    
//   final String passwordString = args[2];
    
    
//    char[] password;
//    if ("none".equals(passwordString.trim())) {
//      System.out.println("Found empty password String");
//      password = null;
//    } else {
//      password = passwordString.toCharArray();
//    }

    final String endCertName = args[1];

    Security.addProvider(new BouncyCastleProvider());

    final X509Certificate rootCert = SecurityUtils.readCertificate(
      new InputStreamReader(new FileInputStream(rootCertName)));
    
//    final KeyPair rootKeyPair = SecurityUtils.readKeyPair(rootKeyName, password);
//    
//    System.err.println(rootKeyPair.getPrivate().getAlgorithm());
//    
//    X509CRL rootCRL = SecurityUtils.createCRL(rootCert, rootKeyPair.getPrivate(), 
//      BigInteger.valueOf(44), CRLReason.privilegeWithdrawn);
    
    try {
      
      rootCert.verify(rootCert.getPublicKey());
      
    }
    
//    catch (Exception ex) {
//      System.err.println("Verify rootCert with rootCert.getPublicKey()");
//      
//      System.err.println(ex);
//    }
// 
//    
//    try {
//      
//      rootCert.verify(rootKeyPair.getPublic());
//      
//    }
//    catch (Exception ex) {
//      
//      System.err.println("Verify rootCert with rootKeyPair.getPublic()");
//      
//      System.err.println(ex);
//    }
//     
//      
//    
//    try {
//      
//      
//      rootCRL.verify(rootKeyPair.getPublic());
//
//    }
//    
//    catch (Exception ex) {
//      
//      System.err.println("Verify rootCRL with rootKeyPair.getPublic()");
//      
//      
//      System.err.println(ex);
//    }
//    
//
//    try {
//      rootCRL.verify(rootCert.getPublicKey());
//
//    }
    
    catch (Exception ex) {
      System.err.println("Verify rootCRL with rootKeyPair.getPublic()");
      
      System.err.println(ex);
    }
        
    
    
//    System.out.printf("Root CRL issuer = %s.%n", rootCRL.getIssuerX500Principal());
//    
//    SecurityUtils.writeCRL(new FileOutputStream("/tmp/root.crl"), rootCRL);
//    
    
    
    X509Certificate endCert = SecurityUtils.readCertificate(
      new InputStreamReader(new FileInputStream(endCertName)));   

    // create CertStore to support validation
    
    List list = new ArrayList();

    list.add(rootCert);

    list.add(endCert);
    
//    list.add(rootCRL);
    
    


    CollectionCertStoreParameters params = new CollectionCertStoreParameters(list);
    CertStore store = CertStore.getInstance("Collection", params, "BC");

    
    // create certificate path
    CertificateFactory fact = CertificateFactory.getInstance("X.509", "BC");
    List<X509Certificate> certChain = new ArrayList<X509Certificate>();

    certChain.add(endCert);
    
//    certChain.add(rootCert);


    CertPath certPath = fact.generateCertPath(certChain);
    Set<TrustAnchor> trust = Collections.singleton(new TrustAnchor(rootCert, null));
    
    

    // perform validation
    CertPathValidator validator = CertPathValidator.getInstance("PKIX", "BC");
    PKIXParameters param = new PKIXParameters(trust);
    
    param.addCertStore(store);
    param.setDate(new Date());
    
    param.setRevocationEnabled(false);  // For now, disable checking for CRLs
                                        // Otherwise, we are forced to include CRLs in the CollectionCertStore

//        X509CertSelector selector = new X509CertSelector();
//        
//        selector.setSubject(new X500Principal("CN=No Match"));
//        param.setTargetCertConstraints(selector);

    try {
      /* CertPathValidatorResult result = */ validator.validate(certPath, param);
      
      

      System.out.println("certificate path validated");
    } catch (CertPathValidatorException e) {
      System.out.println("validation failed on certificate number " + e.getIndex() + ", details: " + e.getMessage());
      
      
      
      
    }
  }
}
