package eu.contrail.security;


import gnu.getopt.Getopt;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.UnknownHostException;

import java.security.*;
import java.security.cert.CertificateException;

//import java.security.cert.CertificateFactory;

import java.security.cert.X509Certificate;

import java.util.ArrayList;

//import java.util.HashMap;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//import java.util.Vector;

import javax.security.auth.x500.X500Principal;

//import org.apache.http.*;


import org.apache.http.*;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;


import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;

import org.apache.http.impl.auth.BasicScheme;

import org.apache.http.impl.client.BasicAuthCache;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;

//import org.bouncycastle.asn1.DERSet;


//import org.bouncycastle.asn1.*;
//import org.bouncycastle.asn1.pkcs.Attribute;
//import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

//import org.bouncycastle.asn1.x509.CRLDistPoint;
//import org.bouncycastle.asn1.x509.DistributionPoint;
//import org.bouncycastle.asn1.x509.DistributionPointName;

//import org.bouncycastle.asn1.x509.GeneralName;
//import org.bouncycastle.asn1.x509.GeneralNames;

//import org.bouncycastle.asn1.x509.X509Extension;
//import org.bouncycastle.asn1.x509.X509Extensions;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


public class DelegatedHostCertClient {

  private DefaultHttpClient httpClient;
  private URI uri;
  private HttpPost httpPost = null;
  private HttpResponse response;
  private StatusLine statusLine;
  private int status;
  
  SecurityCommons sc  = new SecurityCommons();

  public DelegatedHostCertClient(
    final String uriSpec, final boolean useProxy,
    final String keystorePath, final String keystorePass,
    final String truststorePath, final String truststorePass)
    throws URISyntaxException, KeyStoreException,
    FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException,
    UnrecoverableKeyException, KeyManagementException {

    uri = new URI(uriSpec);

    int port = uri.getPort();

    httpClient = null;

    KeyStore keystore = null;

    KeyStore truststore = null;


    keystore = KeyStore.getInstance("PKCS12");

    InputStream keystoreInput = new FileInputStream(keystorePath);
    keystore.load(keystoreInput, keystorePass.toCharArray());

    truststore = KeyStore.getInstance("JKS");
    keystoreInput = new FileInputStream(truststorePath);
    truststore.load(keystoreInput, truststorePass.toCharArray());

    SchemeRegistry schemeRegistry = new SchemeRegistry();
    SSLSocketFactory aSchemeSocketFactory = new SSLSocketFactory(keystore, keystorePass, truststore);

    schemeRegistry.register(new Scheme("https", port, aSchemeSocketFactory));

    final HttpParams httpParams = new BasicHttpParams();
    httpClient = new DefaultHttpClient(new SingleClientConnManager(schemeRegistry), httpParams);

    if (useProxy) {

      final String proxyHost = System.getProperty("http.proxyHost");
      String proxyScheme = null;
      String proxyPortSpec = null;

      if (proxyHost != null) {

        proxyPortSpec = System.getProperty("http.proxyPort");
        proxyScheme = "http"; // Set to HTTP for now 
        setProxy(proxyHost, proxyPortSpec, proxyScheme);

      }

    }



  }

    


  public DefaultHttpClient getHttpClient(final int httpsPort) {


    try {
      KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
      trustStore.load(null, null);

      SSLSocketFactory sf = new /* My */ SSLSocketFactory(trustStore);
             sf.setHostnameVerifier (SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

      HttpParams params = new BasicHttpParams();
      HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
      HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

      SchemeRegistry registry = new SchemeRegistry();

      registry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
      registry.register(new Scheme("https", httpsPort, MySSLSocketFactory.getSocketFactory()));

      ClientConnectionManager ccm = new ThreadSafeClientConnManager(registry); // SingleClientConnManager(registry);

      return new DefaultHttpClient(ccm, params);
    } catch (Exception e) {
      return new DefaultHttpClient();
    }

  }

  public void shutdownConnection() {

    httpClient.getConnectionManager().shutdown();

  }

  public URI getUri() {

    return uri;

  }

  public BasicHttpContext setAuth(final String username, final String password) {
    
    final HttpHost target = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());

    AuthScope authScope = new AuthScope(target.getHostName(), target.getPort());
    
    ;
    
    httpClient.getCredentialsProvider().setCredentials(authScope,
      new UsernamePasswordCredentials(username, password));


    final BasicScheme basicScheme = new BasicScheme();
    final AuthCache authCache = new BasicAuthCache();
    authCache.put(target, basicScheme);

    final BasicHttpContext localContext = new BasicHttpContext();
    localContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
    
    
    return localContext;

  }

  public void setFormData(final String name, final String content)
    throws UnsupportedEncodingException {

    httpPost = new HttpPost(uri);

    final List<NameValuePair> formParams = new ArrayList<NameValuePair>();

    formParams.add(new BasicNameValuePair(name, content));

    httpPost.setEntity(new UrlEncodedFormEntity(formParams, "UTF-8"));

  }

  public int executePost(final BasicHttpContext context)
    throws HttpException, IOException {


    try {
      
      
      
      response = httpClient.execute(httpPost, context);

      statusLine = response.getStatusLine();

      return statusLine.getStatusCode();


    } catch (ClientProtocolException ex) {

      System.err.println(ex);
      throw new HttpException("CPE");
    }
    
  }

  public int executePost()
    throws HttpException, IOException {

    response = httpClient.execute(httpPost);
      
    statusLine = response.getStatusLine();

    return statusLine.getStatusCode();

  }  

  public StatusLine getStatusLine() {

    return statusLine;

  }

  public void setStatus(final int status) {
    
    this.status = status;
    
  }

  public int getStatus() {
    
    return status;
    
  }

  public InputStream getStream(final HttpResponse response)
    throws IOException {
    
    HttpEntity entity = response.getEntity();
    
    long len = entity.getContentLength();
    
    if (len == 0) {
      throw new IOException("Input stream is empty");
    }

    return entity.getContent();
    
  }

  public void setProxy(
    final String proxyHost, 
    final String proxyPortSpec, 
    final String proxySchemeSpec) {
    
    final int PROXYPORTDEFAULT = 8080;
    
    String proxyScheme;
    
    if (proxyHost != null && proxyPortSpec != null) {
      
      int proxyPort = -1; //NOPMD

      try {
        
        proxyPort = Integer.valueOf(proxyPortSpec);
        if (proxyPort <= 0) {
          throw new NumberFormatException();
          
        }
        /*
         * If the proxyPortSpec was malformed, or <= 0, set proxyPort to the
         * default
         *
         */
      } catch (NumberFormatException ex) {
        
        proxyPort = PROXYPORTDEFAULT;
        
      }
 
      proxyScheme = (proxySchemeSpec == null) ? "http" : proxySchemeSpec;

      final HttpHost proxy = new HttpHost(proxyHost, proxyPort, proxyScheme);
      
 //     httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
    }

  }

//  public static X509Certificate getTrustAnchorFor(final X509Certificate cert, final String rootCertsDir)
//    throws IOException, FileNotFoundException {
//
//
//    X509Certificate trustAnchor;
//
//    /*
//     * Currenlty, use entity hard-wired filename
//     *
//     * To-Do: find the trust anchor in the rootCertsDir by one of the following
//     * methods:
//     *
//     * Find entity certificate where the Subject CN == Issuer CN of receiveed user
//     * certificate
//     *
//     * or
//     *
//     * Find entity certificate where the Subject Key Identifier == the Authority Key
//     * Identifier of user certificate
//     *
//     * Migh want to extend this to cater for entity signing chain with more than one
//     * entry in it (that is, not entity single signing certificate)
//     *
//     *
//     *
//     */
//
//    final String trustAnchorFilename = "/tmp/roots/f790ee95.0";
//
//    /*
//     * Here is entity certificate guaranteed to not verify any use certificates -
//     * "/tmp/roots/ff783690.0";
//     */
//
//    try {
//
//      trustAnchor = sc.getCertFromStream(new  FileInputStream(trustAnchorFilename));
//
//    } catch (CertificateException ex) {
//      System.err.printf("CertificateError from file %s.%n", trustAnchorFilename);
//      trustAnchor = null; // Signal to not to verify      
//    }
//    
//    catch (FileNotFoundException ex) {
//      System.err.printf("Cannot open file %s.%n", trustAnchorFilename);
//      trustAnchor = null; // Signal to not to verify
//
//    } catch (IOException ex) {
//      System.err.printf("Cannot read from file %s.%n", trustAnchorFilename);
//      trustAnchor = null; // Signal to not to verify
//    }
//
//    return trustAnchor;
//
//  }

  public X509Certificate getCert(
    final KeyPair keyPair,
    final String signatureAlgorithm,
    final String userID,
    boolean keepConnection)
    throws HttpException, IOException {

    X509Certificate cert = null;
    
    final PKCS10CertificationRequest request = 
      sc.createCSR(keyPair, String.format("CN=%s", userID), signatureAlgorithm);

    InputStream is = null;

    try {

//      setProxy(proxyHost, proxyPortSpec, proxyScheme);

//      final BasicHttpContext context = setAuth(username, password);

      final String csrSpec = sc.writeCSR(request);

      setFormData("certificate_request", csrSpec);
      
//      status = executePost();    // w/o context, entity 401 is returned from server
                                   
      
      status = executePost();    
      
      if (status == HttpStatus.SC_OK) {

        is = getStream(response);
       
        cert = sc.getCertFromStream(is);

      } else {
        
//        System.err.println("Throwing an HttpException " + statusLine.getReasonPhrase());
        
        throw new HttpException("Code: " + statusLine.getStatusCode() + ", Reason: " + statusLine.getReasonPhrase());
      }
      
    } catch (UnsupportedEncodingException ex) {
      
      throw new IllegalArgumentException(ex);
      
    } catch (CertificateException ex) {
      
      throw new IllegalArgumentException(ex);
      
    } 
    
    
    finally {

      if (is != null) {

        try {
          is.close();

        } catch (IOException ex) {
          ;
        }
      }
      if (!keepConnection) {
        shutdownConnection();
      }
    }

    return cert;

  }


  
  public static void writeStream(
    final InputStream is, 
    final OutputStream fos) 
    throws IOException, FileNotFoundException {   
    
    BufferedReader br = null;
    
    try {

      br = new BufferedReader(new InputStreamReader(is));
      String line;
      final PrintStream prs = new PrintStream(fos);

      while ((line = br.readLine()) != null) {
        prs.println(line);
      }    
    
    }
    finally {

      if (br != null) {
        try {
          br.close();
        } catch (Exception ex) {
          ; // Can't do owt...
        }
    
      }
    }

  }
  public static void printHeaders(HttpMessage httpMessage) {

    for (Header h : httpMessage.getAllHeaders()) {
      String headerName = h.getName();
      String headerValue = h.getValue();
      System.err.printf("Name=%s, Value=%s.%n", headerName, headerValue);

    }

  }
  
  
  /* 
   * 
   * Move caught exceptions into throws clause
   * 
   */



  public static String getUsername(final Console console) {

    return console.readLine("Enter username: ");

  }

  
//  public static void main(String[] args) {
//    
//    SecurityCommons sc  = new SecurityCommons();
//
//    String rootCertDirName = null;
//
//    String keyOutFilename = null;
//    String certOutFilename = null;
//    String csrOutFilename = null;
//
//    String keyInFilename = null;
//    String csrInFilename = null;
//
//    String username = null;
//
//    char[] keyInPassphrase = null;
////    char[] password = null;
//
//    String password = null;
//    char[] keyOutPassphrase = null;
//
//    boolean saveCSR = false;
//
//    String days = null;
//    int nUserFilenames = 0;
//    boolean verbose = false;
//
//    boolean exitAfterCSR = false;
//// 
////
////          String[] options = new String[args.length - NREQUIRED_ARGS];
////
////          System.arraycopy(args, NREQUIRED_ARGS, options, 0, args.length - NREQUIRED_ARGS);
//    
//    
//    
//    /*
//     * 
//     * Process the command-line arguments
//     * 
//     * 
//     * 
//     * 
//     */
//
//    int idx = -1;
//    try {
//
//      Getopt g = new Getopt("CA Client", args, "c:hk:u:p:r:R:vs:S:C:D:K:P:Q:");
//
//      int c;
//
//      while ((c = g.getopt()) != -1) {
//
//        switch (c) {
//
//          case 'c':
//            certOutFilename = g.getOptarg();
//            ++nUserFilenames;
//            break;
//
//          case 'h':
//            System.err.println("Usage: GetCert  URI");
//            System.exit(0);
//            break;
//
//          case 'k':
//            keyOutFilename = g.getOptarg();
//            ++nUserFilenames;
//            break;
//
//          case 'u':
//            username = g.getOptarg();
//            break;
//
//          case 'p':
//            password = new String(sc.getPass(g.getOptarg()));
//            break;
//
//          case 'r':
//            rootCertDirName = g.getOptarg();
//            break;
//
//          case 'v':
//            verbose = true;
//            break;
//
//          case 'C':
//            csrInFilename = g.getOptarg();
//            break;
//
//          case 'D':
//            days = g.getOptarg();
//            break;
//
//          case 'K':
//            keyInFilename = g.getOptarg();
//            break;
//
//          case 'P':
//            keyInPassphrase = sc.getPass(g.getOptarg());
//            break;
//
//          /*
//           * To-do: allow passphrase specifier of e.g. 'pass:nopass' to specify
//           * that key will not be protected by entity passphrase.
//           *
//           *
//           */
//          case 'Q':
//            keyOutPassphrase = sc.getPass(g.getOptarg());
//
//            if (keyOutPassphrase.length < 8) {
//
//              throw new IllegalArgumentException("key pass-phrase is too short");
//
//            }
//            break;
//
//          case 's':
//            if (csrInFilename == null) {
//              saveCSR = true;
//              csrOutFilename = g.getOptarg();
//              exitAfterCSR = true;
//            }
//            break;
//
//          case 'S':
//            if (csrInFilename == null) {
//              saveCSR = true;
//              csrOutFilename = g.getOptarg();
//            }
//            break;
//
//          default:
//            break;
//        }
//
//      } // end while
//
//      idx = g.getOptind();
//
//    } 
//    
//    catch (FileNotFoundException ex) {
//
//      System.err.printf("Cannot find file " + ex.getMessage() + " - exiting.%n");
//      System.exit(-1);
//
//    } 
//    
//    catch (IOException ex) {
//
//      System.err.printf("Cannot open/read from file." + ex.getMessage() + "%n");
//      System.exit(-1);
//
//    }
//    
//    
//    /*
//     * 
//     * 
//     * Act on the command-line options
//     * 
//     * 
//     */
//
//
//    if (keyInFilename != null && keyOutFilename != null) {
//
//      System.err.println("Cannot provide both a filename for an existing private key and an output filename for a new private key..%n");
//      System.exit(-1);
//
//    }
//
//    Console console = System.console();
//
//    if (username == null) {
//      username = getUsername(console);
//    }
//
//    if (password == null) {
//      password = new String(console.readPassword("Enter password:"));
//    }
//
//    if (rootCertDirName == null) { // wasn't set on command-line with '-r'
//      rootCertDirName = System.getProperty("rootCertDir", "/etc/grid-security/certificates");
//      System.err.printf("Using %s for trustroots.%n", rootCertDirName);
//
//    }
//
//
//    String homeDir = System.getProperty("user.home");
//
//
//
//    /*
//     * Process required arguments
//     *
//     */
//
//
//
//    if (nUserFilenames == 0 && keyInFilename == null) { // No filenames provided, so generate filenames for user private key and XOS-Certificate
//
//
//      String userKeyFilePath = System.getProperty("userKeyPath", homeDir);
//      String userCertFilePath = System.getProperty("userKeyPath", homeDir);
//
//
//      keyOutFilename = userKeyFilePath + "/" + "user.key";
//      certOutFilename = userCertFilePath + "/" + "user.crt";
//
//    }
//
//    if (nUserFilenames == 1 && keyInFilename == null) {
//
//      System.err.println("Must supply either both output filenames, or no output filenames.");
//      System.err.printf("You supplied: key output file %s, certificate output file %s.%n", keyOutFilename, certOutFilename);
//
//      System.exit(-1);
//
//    }
//
//    if (keyInFilename != null) {   // Don't save keypair if we are reading from an existing file
//
//      keyOutFilename = null;
//
//    }
//
//    if (keyInPassphrase != null && keyInFilename == null) {
//
//      System.err.println("Must supply a filename for an existing private key ('-K key') if you specify a passphrase with '-P'. %n");
//
//      System.exit(-1);
//
//    }
//
//    if (keyInFilename == null) { // Need to create keypair, prompt user for private key passphrase
//
//      if (keyOutPassphrase == null) {
//        boolean phrasesMatch = false;
//
//        while (!phrasesMatch) {
//
//          try {
//            keyOutPassphrase = sc.getPassphrase(console, 8);
//            phrasesMatch = true;
//          } catch (IllegalArgumentException ex) {
//            System.err.println("Error: passphrases do not match.%n");
//            ;
//          }
//
//        }
//
//      }
//
//    }
//    
//    
//    /*
//     * 
//     * Create CSR and format it as entity String
//     * 
//     * 
//     */
//    
//    String uriSpec = args[idx];
//    InputStream inputStream = null;
//    
//    PKCS10CertificationRequest certRequest = null;    
//
//    X509Certificate cert = null;
//    KeyPair kp = null;
//    DelegatedHostCertClient client = null;
//
//    try {
//
//      Security.addProvider(new BouncyCastleProvider());
//      
//      if (csrInFilename != null) {
//
//        certRequest = sc.readCSR(csrInFilename);
//
//      } else {
//
//        if (keyInFilename != null) {
//
//          kp = sc.readKeyPair(keyInFilename, keyInPassphrase);
//
//        } else {
//
//          kp = sc.generateKeyPair("RSA", 2048);
//
//        }
//
//        certRequest = sc.createCSR(kp, "", "SHA256withRSA");
//
//      }
//
//  
//      if (certRequest == null) {
//        System.err.println("Couldn't create CSR.%n");
//        System.exit(-1);
//      }
//
//
//      String csrSpec = null;
//
//      csrSpec = sc.writeCSR(certRequest);
//
//      if (saveCSR) {
//        sc.writeCSR(certRequest, csrOutFilename);
//      }
//
//
//      if (exitAfterCSR) {
//        System.out.printf("Saved CSR file to %s%n", csrOutFilename);
//        System.exit(0);
//      }
//
//      
//      /*
//       * 
//       * Create client connection, send CSR to server, receive certificate back
//       * 
//       * 
//       */
//      
//      
//
//      client = new DelegatedHostCertClient(uriSpec, true,       
//        "./src/test/resources/keystore.p12", "client",
//      "./src/test/resources/caserver.jks", "caserver");
//
////      final String proxyHost = System.getProperty("http.proxyHost");
////      String proxyScheme = null;
////      String proxyPortSpec = null;
////
////      if (proxyHost != null) {
////
////        proxyPortSpec = System.getProperty("http.proxyPort");
////        proxyScheme = "http"; // Set to HTTP for now 
////        client.setProxy(proxyHost, proxyPortSpec, proxyScheme);
////
////      }    
//
//
//      BasicHttpContext basicContext = client.setAuth(username, password);
//
//      client.setFormData("certificate_request", csrSpec);
//
//
//
//      /*
//       * Send the request and deal with any error
//       */
//
//      final int status = client.executePost(basicContext);
//
//      if (status != HttpStatus.SC_OK) {        
//
//        final StatusLine statusLine = client.getStatusLine();
//
//        System.err.println("Status line is " + statusLine);
//
//        client.shutdownConnection();
//        System.exit(-1);
//
//      }
//
//  //    inputStream = client.getStream();
//
//      cert = sc.getCertFromStream(inputStream);
//      
//      if (cert == null) {
//        
//        System.err.println("Problem reading certificate from server.");
//        System.exit(-1);
//        
//      }
//      
//    } catch (KeyStoreException ex) {
//      
//      System.err.println(ex);
//
//    } catch (KeyManagementException ex) {
//      
//      System.err.println(ex);
//      
//    }
//    
//    catch (UnrecoverableKeyException ex) {
//      
//      System.err.println(ex);
//      
//    }
//    
//    catch (URISyntaxException ex) {
//
//      System.err.printf("Error: URI %s is not in the correct format. Exiting.%n", uriSpec);
//      System.exit(-1);
//
//    } catch (NoSuchAlgorithmException ex) {
//      
//      System.err.println(ex);
//      System.exit(-1);
//      
//    } catch (CertificateException ex) {
//      
//      System.err.println(ex);
//      System.exit(-1);
//      
//    } catch (UnknownHostException ex) {
//      
//      System.err.println(ex);
//      System.exit(-1);
//      
//    } catch (UnsupportedEncodingException ex) {
//
//      System.err.println("Can't encode form parameters.");
//      System.exit(-1);
//
//    } catch (HttpException ex) {
//      
//      System.err.println(ex);
//    }
//    
//    catch (IOException ex) {
//
//      System.err.println("Cannot read certificate from remote server. Exiting");//NOPMD
//      System.exit(-1);//NOPMD
//
//    } finally {
//
//      if (inputStream != null) {
//
//        try {
//          inputStream.close();
//
//        } catch (IOException ex) {
//          ;
//        }
//      }
//    }
//
//    
//    /*
//     * 
//     * Verify the received certificate, save certificate and private key if necessary
//     * 
//     * 
//     * 
//     */
//    
//    try {
//      /*
//       * Save the certificate (and private key, if we created one)
//       */
//
//      if (keyOutFilename != null && kp != null) {
//
//        sc.writeKey(keyOutFilename, kp.getPrivate(), keyOutPassphrase);
//        System.out.printf("Wrote private key to %s%n", keyOutFilename);
//
//      }
//
//      X509Certificate trustAnchor = null;
//
////      trustAnchor = getTrustAnchorFor(cert, rootCertDirName);
//
//      if (trustAnchor != null) {
//
//        cert.verify(trustAnchor.getPublicKey());
//
//      }
//
//      sc.writeCertificate(cert, certOutFilename);
//      System.out.printf("Wrote certificate to %s%n", certOutFilename);//NOPMD
//      System.exit(0);
//
//
//
//    } catch (VerifyError ex) {
//
//      System.err.printf("Fatal: Cannot verify certificate.%n");  //NOPMD
//      System.exit(-1); //NOPMD
//
//    } catch (SignatureException ex) {
//
//      System.err.printf("Error: signature on received certificate doesn't match trustroot certificate.%n");//NOPMD
//      System.exit(-1); //NOPMD
//      
//    } catch (FileNotFoundException ex) {
//
//      System.err.println(ex);
//      System.exit(-1);
// 
//    } catch (IOException ex) {
//
//      System.err.println(ex.getMessage());
//      System.exit(-1);
//
//    } catch (Exception ex) {
//
//      System.err.println(ex);
//      System.exit(-1);
//
//    } finally {
//
//      client.shutdownConnection();
//
//    }
//
//  }
}
