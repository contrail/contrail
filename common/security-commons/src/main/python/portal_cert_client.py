import unittest

from HTTPSV3Utils import HTTPSHandlerV3
import urllib, urllib2, ssl, httplib
from OpenSSL import crypto

import time

key_file  = 'client.key'
cert_file = 'client.crt'
ca_certs_file = 'one-test-ssl-chain.pem'
global client 

class PortalCAClientTestCase(unittest.TestCase):


    def setUp(self):
        host1 = "https://one-test.contrail.rl.ac.uk:8443/ca/portaluser"
        host2 ="https://lcg0710.gridpp.rl.ac.uk:8443/ca/portaluser" 
        self.client = portal_cert_client(host1, 
                              key_file, cert_file) #, ca_certs_file) 

# When called from Django, problems passing ca_certs_file,
# but this works OK from command line

    def tearDown(self):
        pass



    def testUserId_1_returns_1(self):

	print 'testUserId_1_returns_1'

        userId = "1"
 
        t0 = time.clock()
        (cert, pkey) = self.client.get_certAndKeypair(userId, 2048)
        t1 = time.clock()

        print t1-t0, 'Seconds process time'

        assert cert.get_subject().commonName == userId

#        open('user1.crt', 'w').write(crypto.dump_certificate(crypto.FILETYPE_PEM,cert))
#        open('user1.key', 'w').write(crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey))



    def testUserId_2_returns_2(self):

        print 'testUserId_2_returns_2'
    
        userId = "2"
        
        t0 = time.clock()
        (cert, pkey) = self.client.get_certAndKeypair(userId, 2048)
        t1 = time.clock()

        print t1-t0, 'Seconds process time'

        assert cert.get_subject().commonName == userId

    def testUserId_Invalid_gets_exception(self):

        print 'testUserID_Invalid_gets_exception'
        userId = "-1"

        t0 = time.clock() 

        try:
            (cert, pkey) = self.client.get_certAndKeypair(userId, 2048)
        except urllib2.HTTPError, e:
            print 'Got an HTTP Error'
            print str(e.code)
        except urllib2.URLError, e:
            print 'Got a URL Error'
            print str(e.reason)
        except httplib.HTTPException, e:
            print 'Got an HTTP exception'

            print e
        t1 = time.clock()

        print t1-t0, 'Seconds process time'

    def testUserId_NaN_gets_exception(self):

        print 'testUserId_NaN_gets_exception'

        userId = "NaN"

        t0 = time.clock() 

        try:
            (cert, pkey) = self.client.get_certAndKeypair(userId, 2048)
        except urllib2.HTTPError, e:
            print 'Got an HTTP Error'
            print str(e.code)
        except urllib2.URLError, e:
            print 'Got a URL Error'
            print str(e.reason)
        except httplib.HTTPException, e:
            print 'Got an HTTP exception'
            print e
        else:
            print("Expected an Error")

        t1 = time.clock()



def createKeyPair(keylen):
    
    pkey = crypto.PKey()
    pkey.generate_key(crypto.TYPE_RSA, keylen)
    
    return pkey

def createCertReq(pkey, commonName, sigAlgorithm):

    cert_req = crypto.X509Req()
    subject = cert_req.get_subject()
    setattr(subject, "CN", commonName)
    cert_req.set_pubkey(pkey)
    cert_req.sign(pkey, sigAlgorithm)

    return cert_req

class portal_cert_client(object):

    def __init__(self, uri, key_file, cert_file, ca_certs_file=None):

        self.uri = uri
        self.key_file = key_file
        self.cert_file = cert_file
        self.ca_certs_file = ca_certs_file

        urllib2.install_opener(urllib2.build_opener(
            HTTPSHandlerV3(keyfile=self.key_file, certfile=self.cert_file,
            cert_reqs=ssl.CERT_NONE )))

    def get_certAndKeypair(self, userId, keylen):

        pkey = createKeyPair(keylen)
        cert_req  = createCertReq(pkey, commonName=userId, sigAlgorithm='SHA256')

        cert_req_buf = crypto.dump_certificate_request(crypto.FILETYPE_PEM, cert_req)      
        data = urllib.urlencode( {'certificate_request': cert_req_buf })

        try:
            r = urllib2.urlopen(self.uri, data)
       
	    cert_buf = r.read()
        except Exception, e:
            print str(e)
            return (None, None)

        if len(cert_buf) == 0:
            raise urllib2.URLError("No data")
        else:
	    cert = crypto.load_certificate(crypto.FILETYPE_PEM, cert_buf)

        return (cert, pkey)

if __name__ == '__main__':
    unittest.main()

