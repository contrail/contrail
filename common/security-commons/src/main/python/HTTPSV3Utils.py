import httplib, ssl, urllib2, socket, urllib


class HTTPSConnectionV3(httplib.HTTPSConnection):
    def __init__(self, *args, **kwargs):
        httplib.HTTPSConnection.__init__(self, *args, **kwargs)
        
    def connect(self):
        sock = socket.create_connection((self.host, self.port), self.timeout)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()

        key_file  = HTTPSHandlerV3.key_file


        cert_file = HTTPSHandlerV3.cert_file
        cert_reqs = HTTPSHandlerV3.cert_reqs
        ca_certs  = HTTPSHandlerV3.ca_certs
        
        self.sock = ssl.wrap_socket(sock , key_file, cert_file,
            cert_reqs=cert_reqs, ca_certs=ca_certs, ssl_version=ssl.PROTOCOL_SSLv3)
           
class HTTPSHandlerV3(urllib2.HTTPSHandler):

#
# After providing a constructor for HTTPSHandlerV3, we need to add the attribute _debuglevel,
# oherwise we get the following erorr message:
#
# AttributeError: HTTPSHandlerV3 instance has no attribute '_debuglevel'
#
    _debuglevel = 0

    key_file = None

    def __init__(self, keyfile, certfile, cert_reqs, ca_certs=None):
#
# Save key_file, cert_file, cert_reqs and ca_certs in static variables
#
#
        HTTPSHandlerV3.key_file  = keyfile
        HTTPSHandlerV3.cert_file = certfile
        HTTPSHandlerV3.cert_reqs = cert_reqs
        HTTPSHandlerV3.ca_certs  = ca_certs

        print self.key_file
        
    def https_open(self, req):
        return self.do_open(HTTPSConnectionV3, req)

