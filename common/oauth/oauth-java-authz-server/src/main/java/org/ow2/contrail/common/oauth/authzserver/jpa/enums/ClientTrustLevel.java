package org.ow2.contrail.common.oauth.authzserver.jpa.enums;

public enum ClientTrustLevel {
    TRUSTED,
    NOT_TRUSTED
}
