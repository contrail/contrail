package org.ow2.contrail.common.oauth.authzserver.jpa.enums;

public enum AuthorizedGrantType {
    AUTHORIZATION_CODE,
    CLIENT_CREDENTIALS
}
