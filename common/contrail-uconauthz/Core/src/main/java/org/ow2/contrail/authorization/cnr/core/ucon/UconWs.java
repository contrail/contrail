package org.ow2.contrail.authorization.cnr.core.ucon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import javax.ws.rs.core.MediaType;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.engine.Phase;
import org.apache.axis2.phaseresolver.PhaseException;
import org.apache.axis2.service.Lifecycle;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.authorization.cnr.core.pdp.BalanaPdp;
import org.ow2.contrail.authorization.cnr.core.pdp.UconPdp;
import org.ow2.contrail.authorization.cnr.utils.UconConstants;
import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;
import org.ow2.contrail.authorization.cnr.utils.core.OpenSamlCore;
import org.ow2.contrail.authorization.cnr.utils.core.UconAttribute;
import org.ow2.contrail.authorization.cnr.utils.core.UconConstantsCore;
import org.ow2.contrail.authorization.cnr.utils.core.UconPhase;
import org.ow2.contrail.authorization.cnr.utils.core.UconRequestContext;
import org.ow2.contrail.authorization.cnr.utils.core.UconResponseContext;
import org.ow2.contrail.authorization.cnr.utils.core.UconSessionContext;
import org.ow2.contrail.authorization.cnr.utils.core.XacmlSamlCoreUtils;
import com.mysql.jdbc.Driver;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import static org.ow2.contrail.authorization.cnr.utils.UconConstants.VERBOSE_NONE;
import static org.ow2.contrail.authorization.cnr.utils.UconConstants.VERBOSE_LOW;
import static org.ow2.contrail.authorization.cnr.utils.UconConstants.VERBOSE_HIGH;

public class UconWs implements Lifecycle {

	private static String verboseTableEntry = "VERBOSITY";

	// this is to create a service state, a data shared between all service instances
	// here there should be a connection to DB and a thread which enforces attribute retrieval
	public void init(ServiceContext serviceContext) throws AxisFault {
		System.out.println("[UCON]: service init");
		// load MySql driver
		try {
			DriverManager.registerDriver(new Driver());
		} catch (SQLException e) {
			LOG(0, "Initialization failed (Unable to load MySql driver manager)", VERBOSE_NONE);
			return;
		}
		// initialize ucon state
		if (!initState(serviceContext))
			return;
		// instead of adding module manually, we add it in code
		addResponseAbortHandler(serviceContext);
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, "Init completed", VERBOSE_LOW);
	}

	// //LOAD FROM JAR code
	// String configFile = "config.properties";
	// ClassLoader classLoader = getClass().getClassLoader();
	// InputStream in = classLoader.getResourceAsStream(configFile);
	// properties = new Properties();
	// try {
	// properties.load(in);
	// } catch (Exception e) {
	// e.printStackTrace();
	// LOG(verbosity, "didn't find config file in " + configFile, VERBOSE_NONE);
	// return;
	// }

	private boolean initState(ServiceContext serviceContext) {
		System.out.println("[UCON]: service init");
		// default values
		String policyPath = UconConstantsCore.POLICY_DIR;
		long cycle_pause_duration = 20000;
		int access_db_parallelism = 32, parallel_thread_number = 32;
		// load config file
		Properties properties = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream(UconConstantsCore.configFile);
			properties.load(in);
		} catch (FileNotFoundException e) {
			LOG(0, " ERROR: unable to find configuration file in " + UconConstantsCore.configFile, VERBOSE_NONE);
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			LOG(0, " ERROR: while reading configuration file in " + UconConstantsCore.configFile, VERBOSE_NONE);
			return false;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
				}
		}

		// value read from configuration file or set to default
		String temp = "";
		if ((temp = properties.getProperty(UconConstantsCore.CYCLE_PAUSE_DURATION)) != null)
			try {
				cycle_pause_duration = Long.parseLong(temp);
			} catch (NumberFormatException e) {
				LOG(0, "Invalid value for "+UconConstantsCore.CYCLE_PAUSE_DURATION+". Please, check the configuration file "+UconConstantsCore.configFile, VERBOSE_NONE);
			}
		serviceContext.setProperty(UconConstantsCore.CYCLE_PAUSE_DURATION, cycle_pause_duration);
		if ((temp = properties.getProperty(UconConstantsCore.ACCESS_DB_PARALLELISM)) != null)
			try {
				access_db_parallelism = Integer.parseInt(temp);
			} catch (NumberFormatException e) {
				LOG(0, "Invalid value for "+UconConstantsCore.ACCESS_DB_PARALLELISM+". Please, check the configuration file "+UconConstantsCore.configFile, VERBOSE_NONE);
			}
		serviceContext.setProperty(UconConstantsCore.ACCESS_DB_PARALLELISM, access_db_parallelism);
		if ((temp = properties.getProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER)) != null)
			try {
				parallel_thread_number = Integer.parseInt(temp);
			} catch (NumberFormatException e) {
				LOG(0, "Invalid value for "+UconConstantsCore.PARALLEL_THREAD_NUMBER+". Please, check the configuration file "+UconConstantsCore.configFile, VERBOSE_NONE);
			}
		serviceContext.setProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER, parallel_thread_number);

		// looking for debug mode
		String newPolicyPath = debug(serviceContext);
		int verbosity = 0;
		if (newPolicyPath != null) {
			verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
			policyPath = newPolicyPath;
			LOG(verbosity, "Debug mode ON:\n- verbose set to: " + verbosity + "\n- Policies path: " + policyPath
					+ "\n- cycle pause duration: " + serviceContext.getProperty(UconConstantsCore.CYCLE_PAUSE_DURATION)
					+ "\n- access db parallelism: " + serviceContext.getProperty(UconConstantsCore.ACCESS_DB_PARALLELISM)
					+ "\n- parallel thread number: " + serviceContext.getProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER), VERBOSE_NONE);
		}

		// init OpenSaml
		XacmlSamlCoreUtils utils = null;
		try {
			utils = new OpenSamlCore();
			LOG(verbosity, "OpenSaml initialization", VERBOSE_LOW);
			serviceContext.setProperty(UconConstantsCore.OPENSAML_UTILS, utils);
		} catch (XacmlSamlException e) {
			LOG(verbosity, " " + e.getMessage(), VERBOSE_NONE);
			LOG(0, "Initialization failed (Unable to initialize OpenSaml library)", VERBOSE_NONE);
			return false;
		}
		SessionManager sm;
		try {
			sm = new SessionManager(serviceContext, utils, properties, verbosity);
		} catch (Exception e) { // it catches InvalidPropertiesFormatException, SQLException and AxisFault
			e.printStackTrace();
			LOG(0, "Session manager creation error: "+e.getMessage(), VERBOSE_NONE); // all this exceptions have textual info in getMessage
			return false;
		}
		serviceContext.setProperty(UconConstantsCore.SESSION_MANAGER, sm);

		int concurrency = Integer.parseInt(properties.getProperty(UconConstantsCore.ACCESS_DB_PARALLELISM));
		LinkedBlockingQueue<UconPdp> prePdp = new LinkedBlockingQueue<UconPdp>(concurrency);
		for (int i = 0; i < concurrency; i++)
			prePdp.add(new BalanaPdp(UconPhase.PRE, policyPath));
		serviceContext.setProperty(UconConstantsCore.PRE_PDP, prePdp);

		LinkedBlockingQueue<UconPdp> postPdp = new LinkedBlockingQueue<UconPdp>(concurrency);
		for (int i = 0; i < concurrency; i++)
			postPdp.add(new BalanaPdp(UconPhase.POST, policyPath));
		serviceContext.setProperty(UconConstantsCore.POST_PDP, postPdp);

		concurrency = Integer.parseInt(properties.getProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER));
		LinkedBlockingQueue<UconPdp> onPdp = new LinkedBlockingQueue<UconPdp>(concurrency);
		for (int i = 0; i < concurrency; i++)
			onPdp.add(new BalanaPdp(UconPhase.ON, policyPath));
		serviceContext.setProperty(UconConstantsCore.ON_PDP, onPdp);

		return true;
	}

	/**
	 * Read values from ~/testmanagerconfig.properties. If file exist, it could set - verbosity (log information mode) - policy path
	 * (different location for policy) - cycle pause duration (the time between two attribute update) - access db parallelism (how many
	 * thread can access to database concurrently - parallel thread number (how many thread use for attribute update/ongoing re-evaluation
	 * phase)
	 */
	private String debug(ServiceContext serviceContext) {
		File configFile = new File(System.getProperty("user.home") + "/testmanagerconfig.properties");
		if (!configFile.exists()) { return null; // It means no debug mode
		}
		int verbosity = 0;
		String policyPath = null;
		Properties properties = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream(configFile);
			properties.load(in);
			String temp = properties.getProperty("policyPath");
			if (temp != null)
				policyPath = System.getProperty("user.home") + "/" + temp;
			if ((temp = properties.getProperty("verbosity")) != null)
				verbosity = Integer.parseInt(temp);
			if ((temp = properties.getProperty(UconConstantsCore.CYCLE_PAUSE_DURATION)) != null)
				serviceContext.setProperty(UconConstantsCore.CYCLE_PAUSE_DURATION, Long.parseLong(temp));
			if ((temp = properties.getProperty(UconConstantsCore.ACCESS_DB_PARALLELISM)) != null)
				serviceContext.setProperty(UconConstantsCore.ACCESS_DB_PARALLELISM, Integer.parseInt(temp));
			if ((temp = properties.getProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER)) != null)
				serviceContext.setProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER, Integer.parseInt(temp));

		} catch (FileNotFoundException e) {
			return null; // It means no debug mode
		} catch (NumberFormatException e) {
			LOG(0, " ERROR: while reading debug configuration file in " + configFile + ". Found invalid number ( properties are set to "
							+ properties.getProperty("verbosity") + ", " + properties.getProperty(UconConstantsCore.CYCLE_PAUSE_DURATION)
							+ ", " + properties.getProperty(UconConstantsCore.ACCESS_DB_PARALLELISM) + ", "
							+ properties.getProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER) + ").\n" +
									"Default values will be used.", VERBOSE_NONE);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			LOG(0, " ERROR: while reading debug configuration file in " + configFile, VERBOSE_NONE);
			return null;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
				}
			serviceContext.setProperty(verboseTableEntry, verbosity);
		}
		return policyPath;
	}

	public void destroy(ServiceContext serviceContext) {
		destroyAll(serviceContext);
	}

	private void destroyAll(ServiceContext serviceContext) {
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, "destroying service", VERBOSE_NONE);
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm != null) sm.destroy();
		serviceContext.setProperty(UconConstantsCore.SESSION_MANAGER, null);
		serviceContext.setProperty(UconConstantsCore.OPENSAML_UTILS, null);
		serviceContext.setProperty(UconConstantsCore.PRE_PDP, null);
		serviceContext.setProperty(UconConstantsCore.ON_PDP, null);
		serviceContext.setProperty(UconConstantsCore.POST_PDP, null);
		serviceContext.setProperty(UconConstantsCore.CYCLE_PAUSE_DURATION, null);
		serviceContext.setProperty(UconConstantsCore.ACCESS_DB_PARALLELISM, null);
		serviceContext.setProperty(UconConstantsCore.PARALLEL_THREAD_NUMBER, null);

		LOG(verbosity, "service destroyed", VERBOSE_NONE);
	}

	public String restart() {
		MessageContext messageContext = MessageContext.getCurrentMessageContext();
		ServiceContext serviceContext = messageContext.getServiceContext();
		destroyAll(serviceContext);
		initState(serviceContext);
		System.gc();
		return "done";
	}

	// ucon ws implementation
	public String tryaccess(String request) {
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);

		LOG(verbosity, UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "tryaccess received", VERBOSE_LOW);
		XacmlSamlCoreUtils utils = (XacmlSamlCoreUtils) serviceContext.getProperty(UconConstantsCore.OPENSAML_UTILS);
		if(utils == null) {
			LOG(verbosity, "Tryaccess: open saml utils is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;			
		}	
		ContextHandler chPre = new ContextHandler(serviceContext, UconPhase.PRE, utils, verbosity);
		LOG(verbosity, "request content:\n" + request, VERBOSE_HIGH);

		UconRequestContext reqPre;
		try {
			reqPre = chPre.createRequestContext(request);
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			// this is the only case when I have to allert PEP
			return UconConstants.ERROR_INPUT_MESSAGE;
		}

		if (reqPre.getSessionId() == null || reqPre.getSessionId().startsWith(UconConstantsCore.UCON_SESSION_ID_PREFIX)) {
			LOG(verbosity, "Invalid id found in tryaccess: " + reqPre.getSessionId(), VERBOSE_LOW);
			return UconConstants.ERROR_ID_INVALID_MESSAGE;
		}
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm == null) {
			LOG(verbosity, "Tryaccess: session manager is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;			
		}			
		try {
			List<UconAttribute> attr = sm.pullAttributesFromPip(reqPre.getSubjectId(), reqPre.getObjectId());
			UconResponseContext respPre = chPre.runPdpEvaluation(reqPre, attr);
			String sessionId = UconConstants.NO_SESSION_ID;
			if (respPre.getAccessDecision()) {
				// insert request into dataBase (I do it only if it's permit)
				UconSessionContext newSessionContext = new UconSessionContext();
				newSessionContext.setSessionID(reqPre.getSessionId());
				newSessionContext.setInitialRequestContext(reqPre);
				newSessionContext.setReplyTo(""); // no replyto in tryaccess!!

				sessionId = sm.insertSessionInDB(newSessionContext);

				if (sessionId.equals(UconConstants.NO_SESSION_ID)) { throw new SQLException("Unable"); }
			}
			respPre = chPre.setSamlResponse(respPre, sessionId);
			LOG(verbosity, "\n" + UconConstants.PRINTLINE, VERBOSE_LOW);
			String result = (respPre.getAccessDecision()) ? "Permit" : "Not applicable";
			LOG(verbosity, "tryaccess response: " + result, VERBOSE_LOW);
			LOG(verbosity, "tryaccess response:\n" + respPre.getSAMLResponse(), VERBOSE_HIGH);
			// CHECKME: in RunPreAuthorization I set UconResponseContext field but i use only the saml response
			return respPre.getSAMLResponse();
		} catch(NullPointerException e) {
			e.printStackTrace();
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (ConnectException e) {
			LOG(verbosity, "Connect Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("tryaccess", e.getMessage());
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("tryaccess", e.getMessage());
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (InterruptedException e) {
			LOG(verbosity, "Interrupted Exception: " + e.getMessage(), VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			errorLog("tryaccess", e.getMessage() + "\n" + request);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
	}// */

	// long-standing action. it starts attribute retrieval when the first
	// request comes and ends when the last request ends/is revoked
	public String startaccessAsynch(String ackAssertion) {
		
		//set the environvment
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "startaccess received", VERBOSE_LOW);
				
		XacmlSamlCoreUtils utils = (XacmlSamlCoreUtils) serviceContext.getProperty(UconConstantsCore.OPENSAML_UTILS);
		if(utils == null) {
			LOG(verbosity, "Startaccess: open saml utils is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm == null) {
			LOG(verbosity, "Startaccess: session manager is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}	
		// process the ackAssertion by Context Handler
		ContextHandler chOn = new ContextHandler(serviceContext, UconPhase.ON, utils, verbosity);
		// if the message isn't well-formed, an exception will be thrown
		UconRequestContext start;
		try {
			start = chOn.createRequestContext(ackAssertion);
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			// this is a case when I have to alert PEP
			return UconConstants.ERROR_INPUT_MESSAGE;
		}

		LOG(verbosity, "for the session with id: " + start.getSessionId(), VERBOSE_LOW);
		try {
			// get a session from DB
			UconSessionContext sessionContext = sm.getSessionFromDB(start);
			if (sessionContext == null) { // no session found
				LOG(verbosity, "(startaccess) no session in db with id " + start.getSessionId(), VERBOSE_LOW);
				// this is a case when I have to alert PEP
				return UconConstants.ERROR_INPUT_MESSAGE;
			}

			if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_PRE)) {
				// TODO: (Sasha) find all mutable attributes needed for this session. have no idea how to implement this function
				// TODO: get all mutable attribute which are needed to service the request (usage session)
				// TODO: search it in database before asking it to pip
				List<UconAttribute> attrNeededbySession = sm.pullAttributesFromPip(
						sessionContext.getInitialRequestContext().getSubjectId(), sessionContext.getInitialRequestContext().getObjectId());
				UconResponseContext resp = chOn.runPdpEvaluation(sessionContext.getInitialRequestContext(), attrNeededbySession);
				if (resp.getAccessDecision()) {
					LOG(verbosity, "the session " + start.getSessionId() + " can starts!", VERBOSE_LOW);
					// update status of the session and in DB also
					sessionContext.setStatus(UconConstantsCore.SESSION_ON);
					sessionContext.setReplyTo(getCurrentReplyTo());
					sessionContext.setMessageId(getCurrentMessageId());

					// sessions and attributes will be updated in db in usage control cycle
					sm.addSessionToUpdateList(sessionContext);
					sm.addAttrPerSession(attrNeededbySession, sessionContext);

					// run usage control cycle, if there isn't one already
					// if there's already a cycle running this call return quickly
					// otherwise it will end when the last session end
					sm.startUsageControlCycle();

					// LOG(verbosity, "\nSTART request content:\n" + ackAssertion, VERBOSE_NONE);
					// LOG(verbosity, "\nSTART response (revoke) content:\n" + UconConstantsCore.REVOKE_MESSAGE, VERBOSE_NONE);

					// with the help of the ResponseAbortHandler this response will go nowhere
					return UconConstantsCore.ABORT_MESSAGE;
				} else {
					LOG(verbosity, "the session " + start.getSessionId() + " can't starts! The ongoing policy is violated", VERBOSE_LOW);
					sessionContext.setStatus(UconConstantsCore.SESSION_POST);
					sm.addSessionToUpdateList(sessionContext);
					return UconConstantsCore.REVOKE_MESSAGE;
				}
			} else {
				if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_ON)) {
					LOG(verbosity, "(Startaccess) Session found in db was already started", VERBOSE_LOW);
					return UconConstants.SESSION_ALREADY_STARTED;
				}
				if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_POST)) {
					LOG(verbosity, "(Startaccess) Session found in db was already stopped or revoked", VERBOSE_LOW);
					return UconConstants.SESSION_ALREADY_STOPPED_REVOKED;
				}
				LOG(verbosity, "(Startaccess) Session found in db has a corrupted status value " + sessionContext.getStatus(), VERBOSE_NONE);
				return UconConstants.ERROR_GENERIC_MESSAGE;
			}
		} catch (ConnectException e) {
			LOG(verbosity, "Connect Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage());
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage(), e);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (InterruptedException e) {
			LOG(verbosity, "Interrupted Exception: " + e.getMessage(), VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			errorLog("startaccess", e.getMessage() + "\n" + ackAssertion);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
	}// */

	public String endaccess(String endAssertion) {
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "endaccess received", VERBOSE_LOW);

		XacmlSamlCoreUtils utils = (XacmlSamlCoreUtils) serviceContext.getProperty(UconConstantsCore.OPENSAML_UTILS);
		if(utils == null) {
			LOG(verbosity, "Endaccess: open saml utils is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;			
		}	
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm == null) {
			LOG(verbosity, "Endaccess: session manager is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;			
		}	
		// process the endAssertion by Context Handler
		ContextHandler chEnd = new ContextHandler(serviceContext, UconPhase.POST, utils, verbosity);
		UconRequestContext end = null;
		try {
			end = chEnd.createRequestContext(endAssertion);
		} catch (XacmlSamlException e) {
			return UconConstants.ERROR_INPUT_MESSAGE;
		}
		try {
			// get a session from DB
			UconSessionContext sessionContext = sm.getSessionFromDB(end);
			if (sessionContext == null) { // no session found
				LOG(verbosity, "(Endaccess) no session in db with id " + end.getSessionId(), VERBOSE_LOW);
				return UconConstants.ERROR_INPUT_MESSAGE;
			}
			// check if some post-updates are needed?
			if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_ON)
					|| sessionContext.getStatus().equals(UconConstantsCore.SESSION_PRE)) { // if endaccess arrived before startaccess
				chEnd.runPdpEvaluation(sessionContext.getInitialRequestContext());
				// UconResponseContext respEnd = chEnd.runPdpEvaluation(sessionContext.getInitialRequestContext());
				// if (respEnd.getAccessDecision()) { //CHECKME: Why do I run pdp post evalutation if I don't check it?

				// update DB appropriately.
				sessionContext.setStatus(UconConstantsCore.SESSION_POST);
				sm.addSessionToUpdateList(sessionContext);

				LOG(verbosity, "endaccess response is being sent", VERBOSE_LOW);
			} else {
				if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_POST)) {
					LOG(verbosity, "(Endaccess) Session found in db was already stopped or revoked", VERBOSE_LOW);
					return UconConstants.SESSION_ALREADY_STOPPED_REVOKED;
				}
				LOG(verbosity, "(Endaccess) Session found in db has a corrupted status value " + sessionContext.getStatus(), VERBOSE_NONE);
			}

			// LOG(verbosity, "\nEND request content:\n" + endAssertion, VERBOSE_NONE);
			// LOG(verbosity, "\nEND response content:\n" + UconConstants.END_MESSAGE, VERBOSE_NONE);

			return UconConstants.END_MESSAGE;
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("endaccess", e.getMessage());
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (InterruptedException e) {
			LOG(verbosity, "Interrupted Exception: " + e.getMessage(), VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (XacmlSamlException e) {
			errorLog("endaccess", e.getMessage() + "\n" + endAssertion);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
	}

	public void mapId(String old_id, String ovf_id) {
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, "\n" + UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "mapid received", VERBOSE_LOW);
		LOG(verbosity, "old id: " + old_id + " ovf_id: " + ovf_id, VERBOSE_LOW);
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		// update session in DB
		try {
			sm.mapId(old_id, ovf_id);
		} catch (InterruptedException e) {
			LOG(verbosity, "Interrupted Exception: " + e.getMessage(), VERBOSE_NONE);
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("endaccess", e.getMessage());
		}
		return;
	}

	public String subscription(String message) {
		LOG(0,"messaggio ricevuto: "+message, VERBOSE_NONE);
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		
		try {
			sm.updateSubscriptedAttribute(message);
		} catch (XacmlSamlException e) {
			LOG(0, "bad message received as pip update: "+e.getMessage(), VERBOSE_NONE);
			return "err";
		}
		return "done";
	}
	
	
	private String getCurrentMessageId() {
		MessageContext messageContext = MessageContext.getCurrentMessageContext();
		return messageContext.getMessageID();
	}

//	private String getCurrentReplyTo(String addr) {
//		MessageContext messageContext = MessageContext.getCurrentMessageContext();
//		String replyTo = messageContext.getReplyTo().getAddress().toString();
//		System.out.println("[UCON] REPLYTO value: " + replyTo);
//		messageContext.setReplyTo(new EndpointReference(addr));
//		System.out.println("[UCON] REPLYTO real value: " + messageContext.getReplyTo().getAddress().toString());
//		// return "http://localhost:7070";
//		return replyTo;
//	}
	
	private String getCurrentReplyTo() {
		MessageContext messageContext = MessageContext.getCurrentMessageContext();
		// System.out.println("[UCON] REPLYTO value: " + messageContext.getReplyTo().getAddress().toString());

		// return "http://localhost:7070";
		return messageContext.getReplyTo().getAddress().toString();
	}

	// UCON note: this is needed to accomplish the step 10
	// adds a handler to the out flow which does abortion of the message
	private void addResponseAbortHandler(ServiceContext serviceContext) {

		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		AxisConfiguration config = null;
		config = serviceContext.getAxisService().getAxisConfiguration();

		List<Phase> phasesOut = null;
		phasesOut = config.getOutFlowPhases();
		// for (Iterator<Phase> iterator = phasesOut.iterator(); iterator.hasNext();) {
		// Phase phase = (Phase) iterator.next();
		for (Phase phase : phasesOut) { // we don't need iterator anymore :)
			if (UconConstantsCore.RESPONSE_ABORT_PHASE.equals(phase.getPhaseName())) {
				ResponseAbortHandler handler = new ResponseAbortHandler();
				try {
					// This will be the last handler under RESPONSE_ABORT_PHASE phase
					phase.setPhaseLast(handler);
					LOG(verbosity, "ResponseAbortHandler was engaged", VERBOSE_LOW);
				} catch (PhaseException e) {
					// if the handler is already engagged, this exception will be throw!
					LOG(verbosity, "UconWs.addResponseAbortHandled (the handler is already engagged)", VERBOSE_LOW);
				}
			}
		}
	}

	// not used, but may be could be useful
	@SuppressWarnings("unused")
	private void howToChangeWSAReplyToFromCode(MessageContext messageContext) {
		Options opts = messageContext.getOptions();

		opts.setProperty(org.apache.axis2.addressing.AddressingConstants.REPLACE_ADDRESSING_HEADERS, Boolean.TRUE);
		EndpointReference replyToEPR = opts.getReplyTo();

		String newReplyTo = "http://localhost:7070";
		replyToEPR.setAddress(newReplyTo);
		opts.setReplyTo(replyToEPR);

		messageContext.setOptionsExplicit(opts);
	}

	private void errorLog(String method, String message) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(System.getProperty("user.home") + "/ERROR_LOG", true));
			SimpleDateFormat a = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");
			out.println("*********INIT*******");
			out.println("An error occour at " + a.format(new Date()) + " executing method " + method);
			out.println("This is the message received:\n" + message);
			out.println("*********END********");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void errorLog(String method, String message, Exception e) {
		StringWriter str = new StringWriter();
		e.printStackTrace(new PrintWriter(str));
		errorLog(method, message + "\n" + str.toString());
	}

	private void LOG(int verbosity, String text, int mode) {
		if (mode <= verbosity) {
			System.out.println("[UCON]: " + text);
		}
	}

	public String startaccess(String message) {
		
		//set the environvment
		MessageContext messageContext = MessageContext.getCurrentMessageContext();
		ServiceContext serviceContext = messageContext.getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "startaccess received", VERBOSE_LOW);

		LOG(verbosity,"message: "+message,VERBOSE_NONE);
		
		XacmlSamlCoreUtils utils = (XacmlSamlCoreUtils) serviceContext.getProperty(UconConstantsCore.OPENSAML_UTILS);
		if(utils == null) {
			LOG(verbosity, "Startaccess: open saml utils is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm == null) {
			LOG(verbosity, "Startaccess: session manager is null", VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
		// process the ackAssertion by Context Handler
		ContextHandler chOn = new ContextHandler(serviceContext, UconPhase.ON, utils, verbosity);
		// if the message isn't well-formed, an exception will be thrown
		String[] str = message.split("%");
		if(str.length != 2) {
			LOG(verbosity, "Invalid startaccess message:\n"+message, VERBOSE_NONE);
			return UconConstants.ERROR_INPUT_MESSAGE;
		}
		
		//TODO: should be removed, once the fed core knows its address
		String ackAssertion = str[0];
		String replyTo = str[1];
		String ipAddr = (String) messageContext.getProperty(MessageContext.REMOTE_ADDR); //the actual client addr
		String[] split = replyTo.split(":",3);
		replyTo = "http://" + ipAddr + ":" + split[2];
		
//		System.out.println("ack "+ackAssertion);
//		System.out.println("rT "+replyTo);
		UconRequestContext start;
		try {
			start = chOn.createRequestContext(ackAssertion);			
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			// this is a case when I have to alert PEP
			return UconConstants.ERROR_INPUT_MESSAGE;
		}

		LOG(verbosity, "for the session with id: " + start.getSessionId(), VERBOSE_LOW);
		try {
			// get a session from DB
			UconSessionContext sessionContext = sm.getSessionFromDB(start);
			if (sessionContext == null) { // no session found
				LOG(verbosity, "(startaccess) no session in db with id " + start.getSessionId(), VERBOSE_LOW);
				// this is a case when I have to alert PEP
				return UconConstants.ERROR_INPUT_MESSAGE;
			}
						
			if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_PRE)) {
//				LOG(verbosity, "the session " + start.getSessionId() + " can starts!", VERBOSE_LOW);
				// update status of the session and in DB also
				sessionContext.setStatus(UconConstantsCore.SESSION_NEW);
				sessionContext.setReplyTo(replyTo);
				sessionContext.setMessageId(getCurrentMessageId());
			
				List<UconAttribute> attrNeededbySession = sm.pullAttributesFromPip(
						sessionContext.getInitialRequestContext().getSubjectId(), sessionContext.getInitialRequestContext().getObjectId());
				
				// sessions and attributes will be updated in db NOW!
				sm.addAttrPerSession(attrNeededbySession, sessionContext);
				sm.addSessionToUpdateList(sessionContext);
				sm.updateSessionInDB(); //TODO: HOW IS THE DB ACCESS IN THAT FUNCTION?
				
				return "OK";
			} else {
				if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_ON)) {
					LOG(verbosity, "(Startaccess) Session found in db was already started", VERBOSE_LOW);
					return UconConstants.SESSION_ALREADY_STARTED;
				}
				if (sessionContext.getStatus().equals(UconConstantsCore.SESSION_POST)) {
					LOG(verbosity, "(Startaccess) Session found in db was already stopped or revoked", VERBOSE_LOW);
					return UconConstants.SESSION_ALREADY_STOPPED_REVOKED;
				}
				LOG(verbosity, "(Startaccess) Session found in db has a corrupted status value " + sessionContext.getStatus(), VERBOSE_NONE);
				return UconConstants.ERROR_GENERIC_MESSAGE;
			}
		} catch (ConnectException e) {
			LOG(verbosity, "Connect Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage());
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage(), e);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (InterruptedException e) {
			LOG(verbosity, "Interrupted Exception: " + e.getMessage(), VERBOSE_NONE);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		} catch (XacmlSamlException e) {
			LOG(verbosity, e.getMessage(), VERBOSE_LOW);
			errorLog("startaccess", e.getMessage() + "\n" + ackAssertion);
			return UconConstants.ERROR_GENERIC_MESSAGE;
		}
	}
	
	public void reevaluation() {
//		String uuid = "af22bc00-3f0b-480b-9da6-05110892e7ab";
//		String appUuid = "b0bebfe7-343e-4a33-b586-3e7db3b9cc43";
//		String urlBase = "http://146.48.81.249:8080/federation-api/";
		
		//set the environvment
		ServiceContext serviceContext = MessageContext.getCurrentMessageContext().getServiceContext();
		int verbosity = (Integer) serviceContext.getProperty(verboseTableEntry);
		LOG(verbosity, UconConstants.PRINTSTAR, VERBOSE_LOW);
		LOG(verbosity, "reevaluation received", VERBOSE_LOW);
				
		SessionManager sm = (SessionManager) serviceContext.getProperty(UconConstantsCore.SESSION_MANAGER);
		if(sm == null) {
			LOG(verbosity, "reevaluation: session manager is null", VERBOSE_NONE);
		}
		
		List<UconSessionContext> sessionsToRevoke = null;
		try {
			sessionsToRevoke = sm.usageControl();
		} catch (ConnectException e) {
			LOG(verbosity, "Connect Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage());
		} catch (SQLException e) {
			LOG(verbosity, "Database Exception: " + e.getMessage(), VERBOSE_NONE);
			errorLog("startaccess", e.getMessage(), e);
		} 
		
		for(UconSessionContext s: sessionsToRevoke) {
			JSONObject j = new JSONObject();
			ClientConfig cc = new DefaultClientConfig();
			Client client = Client.create(cc);
			WebResource r = client.resource(s.getReplyTo());
			try {
				String entity = r.accept(
				MediaType.APPLICATION_JSON_TYPE, 
				MediaType.APPLICATION_XML_TYPE).entity(j, MediaType.APPLICATION_JSON_TYPE).put(String.class);
				System.out.println("Response " + entity);
			} catch (UniformInterfaceException ue) {
				ClientResponse response = ue.getResponse();
				System.out.println("Response " + response.getStatus() + " " +  response.getClientResponseStatus());
			}
		}
	}
	
}