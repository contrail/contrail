package org.ow2.contrail.authorization.cnr.core.pdp;

import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;
import org.ow2.contrail.authorization.cnr.utils.core.UconPhase;

public abstract class UconPdp {

	protected UconPhase policyType;
	protected String policyPath;

	public UconPdp(UconPhase policyType, String policyPath) {
		this.policyType = policyType;
		this.policyPath = policyPath;
	}

	public abstract String evaluate(String xacmlRequest) throws XacmlSamlException;

}
