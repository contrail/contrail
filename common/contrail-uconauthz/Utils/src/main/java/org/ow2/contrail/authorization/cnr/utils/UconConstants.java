package org.ow2.contrail.authorization.cnr.utils;
public class UconConstants {

	public static enum Category {
		SUBJECT, RESOURCE, ACTION, ENVIRONMENT
	}

	public static final String XML_STRING = "http://www.w3.org/2001/XMLSchema#string";
	public static final String XML_INT = "http://www.w3.org/2001/XMLSchema#integer";

	public static final String UCON_NAMESPACE = "http://ucon.core.cnr.authorization.contrail.ow2.org";
	
	public static final String REVOKE_MESSAGE = "revoke";
	public static final String END_MESSAGE = "end";
//	public static final String SESSION_NEVER_START = "session never started";
	public static final String SESSION_ALREADY_STARTED = "session already started";
	public static final String SESSION_ALREADY_STOPPED_REVOKED = "session already stopped or revoked";
	public static final String ERROR_GENERIC_MESSAGE = "An error occur on Ucon service. Contact the administrator.";
	public static final String ERROR_ID_INVALID_MESSAGE = "The request id is not valid";
	public static final String ERROR_INPUT_MESSAGE = "Input message is not valid";

    public static final String NO_SESSION_ID = "-1";
    
    public static final String PRINTLINE = "-------------------------------------------------------------------------------------------------------------------------------------------------------------";
    public static final String PRINTSTAR = "*************************************************************************************************************************************************************";

    public static final int VERBOSE_NONE = 0, VERBOSE_LOW = 1, VERBOSE_HIGH = 2;
    
}
