package org.ow2.contrail.authorization.cnr.utils.pep;

import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;

public class PepRequestAttribute {

	private String xacml_attribute_id, type, value, issuer;
	private Category category;

	public PepRequestAttribute(String xacmlid, String type, String value, String issuer, Category category) {
		this.xacml_attribute_id = xacmlid;
		this.type = type;
		this.value = value;
		this.issuer = issuer;
		this.category = category;
	}

	public String getXacmlId() {
		return xacml_attribute_id;
	}

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public String getIssuer() {
		return issuer;
	}

	public Category getCategory() {
		return category;
	}

}
