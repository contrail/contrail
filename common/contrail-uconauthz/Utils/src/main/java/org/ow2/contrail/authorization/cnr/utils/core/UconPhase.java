package org.ow2.contrail.authorization.cnr.utils.core;

public enum UconPhase {
	PRE, ON, POST
}
