package org.ow2.contrail.authorization.cnr.utils.core;

import org.opensaml.xacml.ctx.RequestType;
import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;

public class UconXacmlRequest {
	private RequestType req = null;
	private OpenSamlCore utils = null;

	public UconXacmlRequest(String req, OpenSamlCore utils) throws XacmlSamlException {
		this(utils.convertXacmlRequestToObject(req), utils);
	}
	
	
	public UconXacmlRequest(RequestType req, OpenSamlCore utils) {
		this.req = req;
		this.utils = utils;
	}
	
	public RequestType getObject() {
		return req;
	}
	
	public void setObject(RequestType req) {
		this.req = req;
	}
	
	public String getString() throws XacmlSamlException {
		return utils.convertXacmlRequestToString(req);
	}
	
	public void setString(String str) throws XacmlSamlException {
		this.req = utils.convertXacmlRequestToObject(str);
	}
}
