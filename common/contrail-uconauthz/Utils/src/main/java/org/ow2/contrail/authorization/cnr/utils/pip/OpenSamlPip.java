package org.ow2.contrail.authorization.cnr.utils.pip;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeQuery;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.impl.AttributeBuilder;
import org.opensaml.saml2.core.impl.AttributeQueryBuilder;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SubjectBuilder;
import org.opensaml.ws.soap.soap11.Body;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.ws.soap.soap11.impl.BodyBuilder;
import org.opensaml.ws.soap.soap11.impl.EnvelopeBuilder;
import org.opensaml.xacml.ctx.AttributeType;
import org.opensaml.xacml.ctx.AttributeValueType;
import org.opensaml.xacml.ctx.ResourceType;
import org.opensaml.xacml.ctx.SubjectType;
import org.opensaml.xacml.ctx.impl.AttributeTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.AttributeValueTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.ResourceTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.SubjectTypeImplBuilder;
import org.opensaml.xml.XMLObject;
import org.ow2.contrail.authorization.cnr.utils.OpenSamlUtils;
import org.ow2.contrail.authorization.cnr.utils.UconConstants;
import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;
import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;
import org.w3c.dom.Element;

public class OpenSamlPip extends OpenSamlUtils implements XacmlSamlPipUtils {

	public OpenSamlPip() throws XacmlSamlException {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public Object formSAMLAttributeQuery(String name) {
		return formSAMLAttributeQuery(name, new ArrayList<String>());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public AttributeQuery formSAMLAttributeQuery(String name, List<String> attributes) {

		AttributeQueryBuilder builderAttrQuery = (AttributeQueryBuilder) builderFactory.getBuilder(AttributeQuery.DEFAULT_ELEMENT_NAME);
		AttributeQuery attrQuery = builderAttrQuery.buildObject();
		attrQuery.setID("AttrQuery12345789");// CHECKME: ID of QUERY?
		attrQuery.setIssueInstant(new DateTime());
		attrQuery.setVersion(SAMLVersion.VERSION_20);

		IssuerBuilder builderIssuer = (IssuerBuilder) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = builderIssuer.buildObject();
		// issuer.setFormat(Issuer.ENTITY); //CHECKME: what's it? it seems not necessary...
		issuer.setValue("http://Attribute authority address");
		attrQuery.setIssuer(issuer);

		SubjectBuilder builderSubject = (SubjectBuilder) builderFactory.getBuilder(Subject.DEFAULT_ELEMENT_NAME);
		Subject subject = builderSubject.buildObject();
		NameIDBuilder builderNameID = (NameIDBuilder) builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME);
		NameID nameID = builderNameID.buildObject();
		// nameID.setFormat(NameID.UNSPECIFIED); //CHECKME: typical use but again refer oasis saml doc
		nameID.setValue(name); // ID of principal subject of assertion
		subject.setNameID(nameID); // associate name id to subject
		attrQuery.setSubject(subject); // finally set main subject for attribute query

		// Add attribute to request spec:
		// "if no attributes are specified, it indicates that all attribute allowed by policy are requested"
		for (String attributeName : attributes) {
			Attribute samlAttr = createSAMLAttribute(attributeName);
			attrQuery.getAttributes().add(samlAttr);
		}
		return attrQuery;
	}

	/**
	 * {@inheritDoc}
	 */
	public Attribute createSAMLAttribute(String attributeName) {
		AttributeBuilder builderAttribute = (AttributeBuilder) builderFactory.getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
		Attribute attribute = builderAttribute.buildObject();
		attribute.setName(attributeName);
		return attribute;
	}

	/**
	 * {@inheritDoc}
	 */
	public String formSOAPMessage(Object bodyContentObj) throws XacmlSamlException {
		XMLObject bodyContent = (XMLObject) bodyContentObj;
		EnvelopeBuilder builderEnv = (EnvelopeBuilder) builderFactory.getBuilder(Envelope.DEFAULT_ELEMENT_NAME);
		Envelope env = builderEnv.buildObject();
		BodyBuilder builderBody = (BodyBuilder) builderFactory.getBuilder(Body.DEFAULT_ELEMENT_NAME);
		Body body = builderBody.buildObject();
		body.getUnknownXMLObjects().add(bodyContent);
		env.setBody(body);
		String result = marshalling(env);
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<String> getAttributeNameList(String queryResponse) throws XacmlSamlException {
		Response samlResponse = (Response) getSOAPContent(queryResponse);
		List<String> attributes = new LinkedList<String>();
		for (Assertion samlAssertion : samlResponse.getAssertions()) {
			for (AttributeStatement samlAttributeStatement : samlAssertion.getAttributeStatements()) {
				for (Attribute samlAttribute : samlAttributeStatement.getAttributes()) {
					attributes.add(samlAttribute.getName());
				}
			}
		}
		return attributes;
	}

	public String formXACMLResponseMessage(String queryResponse, Category type) throws XacmlSamlException {
		Response samlResponse = (Response) getSOAPContent(queryResponse);
		String response = "";
		switch(type) {
		case SUBJECT:
			response = formXACMLSubjectResponseMessage(samlResponse);
			break;
		case RESOURCE:
			response = formXACMLResourceResponseMessage(samlResponse);
			break;
		default:
			throw new XacmlSamlException("Invalid type "+type);
		}
		return response;
	}
	
	private List<AttributeType> getXacmlAttributeList(Response samlResponse) throws XacmlSamlException {
		List<AttributeType> list = new ArrayList<AttributeType>();
	
		for (Assertion samlAssertion : samlResponse.getAssertions()) {
			for (AttributeStatement samlAttributeStatement : samlAssertion.getAttributeStatements()) {
				List<Attribute> samlAttributeList = samlAttributeStatement.getAttributes();
				for (Attribute samlAttribute : samlAttributeList) {

					AttributeTypeImplBuilder attributeBuilder = (AttributeTypeImplBuilder) builderFactory
							.getBuilder(AttributeType.DEFAULT_ELEMENT_NAME);
					// create xacml attribute
					AttributeType xacmlAttribute = attributeBuilder.buildObject();
					// set xacml attribute id
					xacmlAttribute.setAttributeID(samlAttribute.getName());
					// set xacml attribute datatype

					Element elem = samlAttribute.getDOM();
					// System.out.println("\n******[UTILS PIP] "+samlAttribute.getName()+" value: " + elem.getAttribute("DataType") + "\n");
					if (elem.hasAttribute("DataType")) // CHECKME: is datatype the attribute name that define the attribute type?
						xacmlAttribute.setDataType(elem.getAttribute("DataType"));
					else
						xacmlAttribute.setDataType(UconConstants.XML_STRING);

					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
					// set xacml attribute value
					for (XMLObject samlAttributeValue : samlAttribute.getAttributeValues()) {
						AttributeValueTypeImplBuilder xacmlAttrValueBuilder = (AttributeValueTypeImplBuilder) builderFactory
								.getBuilder(AttributeValueType.DEFAULT_ELEMENT_NAME);
						AttributeValueType xacmlAttributeValue = xacmlAttrValueBuilder.buildObject();
						xacmlAttributeValue.setValue(samlAttributeValue.getDOM().getTextContent());
						xacmlAttribute.getAttributeValues().add(xacmlAttributeValue);
					}
					// set xacml issuer
					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
					// add attribute to subject
					list.add(xacmlAttribute);
				}
			}
		}
		return list;
	}
	
	private String formXACMLResourceResponseMessage(Response samlResponse) throws XacmlSamlException {
		ResourceTypeImplBuilder resourceBuilder = (ResourceTypeImplBuilder) builderFactory.getBuilder(ResourceType.DEFAULT_ELEMENT_NAME);
		ResourceType xacmlResource = resourceBuilder.buildObject();

		for(AttributeType xacmlAttribute: getXacmlAttributeList(samlResponse)) {
			xacmlResource.getAttributes().add(xacmlAttribute);
		}
		
		return marshalling(xacmlResource);
	}
	
	
	private String formXACMLSubjectResponseMessage(Response samlResponse) throws XacmlSamlException {
		SubjectTypeImplBuilder subjectBuilder = (SubjectTypeImplBuilder) builderFactory.getBuilder(SubjectType.DEFAULT_ELEMENT_NAME);
		SubjectType xacmlSubject = subjectBuilder.buildObject();

		for(AttributeType xacmlAttribute: getXacmlAttributeList(samlResponse)) {
			xacmlSubject.getAttributes().add(xacmlAttribute);
		}
		
		return marshalling(xacmlSubject);
	}
	
//	/**
//	 * {@inheritDoc}
//	 */
//	public String formXACMLResponseMessage(String queryResponse) throws XacmlSamlException {
//		Response samlResponse = (Response) getSOAPContent(queryResponse);
//		SubjectTypeImplBuilder subjectBuilder = (SubjectTypeImplBuilder) builderFactory.getBuilder(SubjectType.DEFAULT_ELEMENT_NAME);
//		SubjectType xacmlSubject = subjectBuilder.buildObject();
//
//		for (Assertion samlAssertion : samlResponse.getAssertions()) {
//			for (AttributeStatement samlAttributeStatement : samlAssertion.getAttributeStatements()) {
//				List<Attribute> samlAttributeList = samlAttributeStatement.getAttributes();
//				for (Attribute samlAttribute : samlAttributeList) {
//
//					AttributeTypeImplBuilder attributeBuilder = (AttributeTypeImplBuilder) builderFactory
//							.getBuilder(AttributeType.DEFAULT_ELEMENT_NAME);
//					// create xacml attribute
//					AttributeType xacmlAttribute = attributeBuilder.buildObject();
//					// set xacml attribute id
//					xacmlAttribute.setAttributeID(samlAttribute.getName());
//					// set xacml attribute datatype
//
//					Element elem = samlAttribute.getDOM();
//					// System.out.println("\n******[UTILS PIP] "+samlAttribute.getName()+" value: " + elem.getAttribute("DataType") + "\n");
//					if (elem.hasAttribute("DataType")) // CHECKME: is datatype the attribute name that define the attribute type?
//						xacmlAttribute.setDataType(elem.getAttribute("DataType"));
//					else
//						xacmlAttribute.setDataType(UconConstants.XML_STRING);
//
//					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
//					// set xacml attribute value
//					for (XMLObject samlAttributeValue : samlAttribute.getAttributeValues()) {
//						AttributeValueTypeImplBuilder xacmlAttrValueBuilder = (AttributeValueTypeImplBuilder) builderFactory
//								.getBuilder(AttributeValueType.DEFAULT_ELEMENT_NAME);
//						AttributeValueType xacmlAttributeValue = xacmlAttrValueBuilder.buildObject();
//						xacmlAttributeValue.setValue(samlAttributeValue.getDOM().getTextContent());
//						xacmlAttribute.getAttributeValues().add(xacmlAttributeValue);
//					}
//					// set xacml issuer
//					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
//					// add attribute to subject
//					xacmlSubject.getAttributes().add(xacmlAttribute);
//				}
//			}
//		}
//		return marshalling(xacmlSubject);// .replace("<xacml-context:", "<").replace("</xacml-context:", "</");
//	}

	public String formXACMLUpdateMessage(String updateMessage) throws XacmlSamlException{
		Response samlResponse = (Response) getSOAPContent(updateMessage);
		SubjectTypeImplBuilder subjectBuilder = (SubjectTypeImplBuilder) builderFactory.getBuilder(SubjectType.DEFAULT_ELEMENT_NAME);
		SubjectType xacmlSubject = subjectBuilder.buildObject();
	

		for (Assertion samlAssertion : samlResponse.getAssertions()) {
			AttributeTypeImplBuilder attributeBuilder = (AttributeTypeImplBuilder) builderFactory
				.getBuilder(AttributeType.DEFAULT_ELEMENT_NAME);
			AttributeValueTypeImplBuilder xacmlAttrValueBuilder = (AttributeValueTypeImplBuilder) builderFactory
					.getBuilder(AttributeValueType.DEFAULT_ELEMENT_NAME);
			
			// create xacml attribute for holder
			AttributeType xacmlAttributeHolder = attributeBuilder.buildObject();
			
			xacmlAttributeHolder.setAttributeID("HOLDER_ATTRIBUTE");
			xacmlAttributeHolder.setIssuer(samlAssertion.getIssuer().getValue());			
			xacmlAttributeHolder.setDataType(UconConstants.XML_STRING);
			AttributeValueType xacmlAttributeHolderValue = xacmlAttrValueBuilder.buildObject();
			xacmlAttributeHolderValue.setValue(samlAssertion.getSubject().getDOM().getTextContent());
			xacmlAttributeHolder.getAttributeValues().add(xacmlAttributeHolderValue);
			xacmlSubject.getAttributes().add(xacmlAttributeHolder);
			//add the other attribute
			
			for (AttributeStatement samlAttributeStatement : samlAssertion.getAttributeStatements()) {
				List<Attribute> samlAttributeList = samlAttributeStatement.getAttributes();
				for (Attribute samlAttribute : samlAttributeList) {
					
					// create xacml attribute
					AttributeType xacmlAttribute = attributeBuilder.buildObject();
					// set xacml attribute id
					xacmlAttribute.setAttributeID(samlAttribute.getName());
					// set xacml attribute datatype

					Element elem = samlAttribute.getDOM();
					// System.out.println("\n******[UTILS PIP] "+samlAttribute.getName()+" value: " + elem.getAttribute("DataType") + "\n");
					if (elem.hasAttribute("DataType")) // CHECKME: is datatype the attribute name that define the attribute type?
						xacmlAttribute.setDataType(elem.getAttribute("DataType"));
					else
						xacmlAttribute.setDataType(UconConstants.XML_STRING);

					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
					// set xacml attribute value
					for (XMLObject samlAttributeValue : samlAttribute.getAttributeValues()) {
						
						AttributeValueType xacmlAttributeValue = xacmlAttrValueBuilder.buildObject();
						xacmlAttributeValue.setValue(samlAttributeValue.getDOM().getTextContent());
						xacmlAttribute.getAttributeValues().add(xacmlAttributeValue);
					}
					// set xacml issuer
					xacmlAttribute.setIssuer(samlAssertion.getIssuer().getValue());
					// add attribute to subject
					xacmlSubject.getAttributes().add(xacmlAttribute);
				}
			}
		}
		return marshalling(xacmlSubject);// .replace("<xacml-context:", "<").replace("</xacml-context:", "</");
	}
	
	private Object getSOAPContent(String message) throws XacmlSamlException {
		Envelope env = (Envelope) unmarshalling(message);
		Body body = env.getBody(); //TODO: IF SUBJECT NOT EXIST IT RETURN EMPTY (OR NULL)
		if (body.getUnknownXMLObjects().size() < 0)
			throw new XacmlSamlException("Unable to get SOAP content from this message:\n" + message);
		else
			return body.getUnknownXMLObjects().get(0);
	}
}
