package org.ow2.contrail.authorization.cnr.utils.core;


public class UconSessionContext {
	
	private String sessionIDstring = null;
	private UconRequestContext requestContext = null; 
	private String replyTo;
	private String messageId; //soap id!!!
	private String session_status;
	private String sessionKey; //db primary key
	
	public UconSessionContext() {
		session_status =  UconConstantsCore.SESSION_PRE;
		replyTo = "";
	}
	
	public String getSessionID(){
		return sessionIDstring;
	}
	
	public void setSessionID(String sessionID){
		sessionIDstring = sessionID;
	}
	
	public String getSessionKey(){
		return sessionKey;
	}
	
	public void setSessionKey(String sessionKey){
		this.sessionKey = sessionKey;
	}

	public String getReplyTo(){
		return replyTo;
	}
	
	public void setReplyTo(String replyAddress){
		replyTo = replyAddress;
	}

	public String getMessageId(){
		return messageId;
	}
	
	public void setMessageId(String messageId){
		this.messageId = messageId;
	}
	
	
	public UconRequestContext getInitialRequestContext(){
		return requestContext;
	}
	
	public void setInitialRequestContext(UconRequestContext uconRequestContext){
		requestContext = uconRequestContext;
	}
	
	public String getStatus(){
		return session_status;
	}
	
	public void setStatus(String status){
		session_status = status;
	}

}
