import sys

import libxml2
from libxml2 import xmlNode

import numpy as np


def getExptValues(ctxt, namespacePrefix, I, Q, Idev):
    
    def selector(namespacePrefix, elt):
        return "//" + namespacePrefix + ":" + elt
    
    I = ctxt.xpathEval( selector(namespacePrefix, I) )
    Q = ctxt.xpathEval( selector(namespacePrefix, Q) )   
    Idev = ctxt.xpathEval( selector(namespacePrefix, Idev) )

    return I, Q, Idev 

import string
import json

def getModel(jd):

    return jd['model']

def getRadiusLength(pi):
    
    for i in pi:
        if i['paramname'] == 'radius':
            radius = i['value']
            continue
        if i['paramname'] == 'length':
            length = i['value']
    
    return radius, length
        
 
def getSimulatedValues(jd):
    

    #===========================================================================
    # Remove the brackets around the string representation of the I-values,
    # then split the values into an array  
    #===========================================================================
    
    
    i = str( jd["data_out"]["i"] )     
    i = string.translate(i, None, '[]').split(',')

    q = str( jd["data_out"]["q"] )
    q = string.translate(q, None, '[]').split(',')
        
    iv = map(float, i)
    
    qv = map(float, q)

    
    return iv, qv


def getFloatArrs(I, Q, Idev):

    i  = map( float, map(xmlNode.getContent, I ) )
    q  = map( float, map(xmlNode.getContent, Q ) ) 
    idev  = map( float, map(xmlNode.getContent, Idev ) )
    
    return i, q, idev
    
def processSimFile(simulationOutputFilename):
        
        
    print 'Processing %s' % simulationOutputFilename      
    try:
        jd = open(simulationOutputFilename).read()
    except IOError as e:
        raise e
        
    jd = json.loads(jd)
    
    parametersIn = jd["parameters_in"]
    
    radius, length = getRadiusLength(parametersIn)
    
    simI, simQ = getSimulatedValues(jd)
    
    return getModel(jd), radius, length, simI, simQ      
    
def main():    
    
    namespacePrefix = "cansas1d"
    schemaLocation = "http://svn.smallangles.net/svn/canSAS/1dwg/trunk/cansas1d.xsd"
    
    exptDataFilename = "pin_a.xml"
    
    simulationOutputFilename = "000000.json"

    try:
        xd = open(exptDataFilename).read()
    except IOError as e:
        print e
        sys.exit(-1)
        
        
    doc = libxml2.parseDoc( xd )
    ctxt = doc.xpathNewContext()
    ctxt.xpathRegisterNs(namespacePrefix, schemaLocation)
        
    
    I, Q, Idev = getExptValues(ctxt, namespacePrefix, "I", "Q", "Idev")
    
    
    print 'Length of I = %d' % len(I)
        
    i, q, idev = getFloatArrs(I, Q, Idev)     
    
    
    
    import matplotlib
     
    matplotlib.use("Agg")
     
    import matplotlib.pyplot as plt
     
#    fig = plt.figure(num=model)
    
    fig = plt.figure()         


     
    ax = fig.add_subplot(111)
    
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
     
     
#     plt.errorbar(q, i, yerr=idev, ls='-', label='Experimental')
    
    axes = fig.gca()
    
    print 'Axes: ', axes

    legendString = ['Experimental']

   #============================================================================
   # Now for the JSON files of simulated scattering
   #============================================================================
   
    files = ['000000.json', '000005.json']
    
    for f in files:
        
        try:
            model, radius, length, simulatedI, simulatedQ = processSimFile(f)
        except IOError:
            continue   
        
        print 'Length of simulatedI = %d' % len( simulatedI )
        
        print "SumI, SumQ = %f, %f" % ( np.sum(simulatedI), np.sum(simulatedQ) )
    
        print 'Model = %s' % model
        
        print 'radius %f, length %f' % (radius, length)
     
        fig.canvas.set_window_title(model)    
      
#         axN = fig.add_axes(axes)

        print 'Plotting %s' % f
        plt.plot(simulatedQ, simulatedI, label='l=%.1f, r=%.1f' % (length, radius))
    
    #===========================================================================
    # Order of labels in legendString should relate to order of plotting operations...
    #===========================================================================
    
#        legendString.append('l=%.1f, r=%.1f' % (length, radius))
     
#        ax.legendString(['Experimental', 'l=%.2f, r=%.2f' % (length, radius)], loc='upper right')
    
#    ax.set_xlabel(r'$\mathrm{ Q/Angstrom^{-1} }$')


    plt.legend(loc='upper right')
    
    ax.set_xlabel(ur'ZZZZ $\mathrm{ Q/\u00c5^{-1} }$')

    
    ax.set_ylabel(r'$\mathrm{ I/cm^{-1} }$')
    
    
    ax.set_xscale('log')

    ax.set_yscale('log')
    
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    
    print 'x range ', xlim
    print 'y range', ylim
    
    xmin = sorted(q)[0]
    
    print 'xmin from expt Q = %f' % xmin
    
    xmax = sorted(q)[-1]
    
    print 'xmax from expt Q = %f' % xmax
    
    
    xminsimq = sorted(simulatedQ)[0] 
    
    print 'xmin from Sim q = %f' % xminsimq
    
    
    
    xmaxsimq = sorted(simulatedQ)[-1]
    
    print 'xmax from Sim q = %f' % xmaxsimq
    
    xmin = min( xmin, xminsimq )
    xmax = max( xmax, xmaxsimq ) 
    
    
    ymin = sorted(i)[0]
    ymax = sorted(i)[-1]
    
    ymin = min( ymin, sorted(simulatedI)[0]  ) 
    ymax = max( ymax, sorted(simulatedI)[-1] )
    
    print 'xmin is %f, xmax is %f' % (xmin, xmax)
    
    print 'ymin is %f, ymax is %f' % (ymin, ymax)
    
    ax.set_ylim(ymin, ymax)
    
    ax.set_xlim( xmin * 0.9, xmax * 1.1)
    
    plt.show()
     
    fig.savefig('isis.png', bbox_inches='tight')
    
if __name__ == "__main__":
    
    main()    
    
    