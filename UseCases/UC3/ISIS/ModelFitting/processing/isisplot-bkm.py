import sys, os
from os.path import basename, splitext
import xml.etree.ElementTree as ET
import numpy as np
import pybiosas.models as models
REGISTERED_MODELS = models.models
import json
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

def getStem(name):
    return splitext(basename(name))[0]

def getPlotParams(model):
     pparams = []
     if model == 'cylinder':
        pparams= ['radius','length']
     elif model == 'ellipse':
        pparams=['radius_a','radius_b']
     elif model == 'sphere':
        pparams=['radius']
     elif model == 'coreShellBicell':
        pparams= ['radius','length','rim_sld']
     elif model == 'ellipticalCylinder':
	pparams = ['r_minor','r_ratio','length']
     return pparams

def getParamNames(model):
    params = []
    if model in REGISTERED_MODELS:
         allparams = [p['paramname'] for p in \
                     REGISTERED_MODELS[model]['exp_vals']]
    else:       
         raise Exception('Cannot yet cope with model=' + model)
    return params

def getExptValuesXml(file):
    
    # Parse the xml file and find the root element
    tree = ET.parse(file)
    elem = tree.getroot()

    # return a list of all the <Q> tags and get the Q values
    q_tags = elem.getiterator("{cansas1d/1.0}Q")

    q_list = []

    for elements in q_tags:
        q_list.append(float(elements.text)) # need to convert text to float

    # then do the same for the <I> tags and values
    i_tags = elem.getiterator("{cansas1d/1.0}I") 
    i_list = []

    for elements in i_tags:
        i_list.append(float(elements.text))

   # And the Idev values
    idev_list = []
    idev_tags = elem.getiterator("{cansas1d/1.0}Idev") 
    for elements in idev_tags:
        idev_list.append(float(elements.text))

    # check everything is ok with q_list and i_list
    assert len(q_list) == len(i_list), 'different number of q and i values?'
    assert len(q_list) != 0, 'appear to be no q values'
    assert len(i_list) != 0, 'appear to be no i values'
    assert q_list[0] < q_list[-1], 'q values not in order?'
    return i_list, q_list, idev_list
    return I, Q, Idev 
pass

def getExptValuesCols(file):

    data= np.loadtxt(file,skiprows=1)
    Q=[]
    I=[]
    Err=[]
    for l in range(len(data)):
         Q.append(float(data[l][0]))
         I.append(float(data[l][1]))
         Err.append(float(data[l][2]))
    return I, Q, Err
pass

class SimData:
    
    def __init__(self,json_output, file):
         self.file = [file]
         self.count = 1
         self.model = [json_output['model']]
         self.params_in = [json_output['parameters_in']]
         self.params_out = [json_output['fit']]
         self.data_out = json_output['data_out']
    pass
    
    def getModel(self,index=0):
         return self.model[index]

    def getFileName(self, index=0):
         return self.file[index]

    def getParamsIn(self,index=0):
         pi = self.params_in[index]
         pnames = getPlotParams(self.model[index])
         params = {}
         for p in pnames:
             for i in pi:
                if i['paramname'] == p:
                   params[p] = i['value']
                   continue
         return params

    def getParamsOut(self,index=0):
         po=self.params_out[index]
         pnames = getPlotParams(self.model[index])
         params={}
         for p in pnames:
           params[p] = po[p]
         return params

    def getSimIQ(self):
       do = self.data_out
       i = json.loads(do['i'])
       q = json.loads(do['q'])
       return i,q

    def getChi2(self):
       return self.params_out[0]['chi2']['value'] 

    def addSimData(self, simdata):
       self.file.append(simdata.file[0])
       self.model.append(simdata.model[0])
       self.params_in.append(simdata.params_in[0])
       self.params_out.append(simdata.params_out[0])
       self.count += 1

    def __str__(self,index=0):
        params = getParamNames(self.model[index])
        string='{'
        for key in params + ['chi2']:
           string += "'"+ key + "':" + str(self.params_out[0][key]['value']) +','
        string +=  "'file':" + self.file[0]
        string += '}'
        return string
    pass
pass
 
def processSimOutput(directory, files):

    def compare_chi2(first_fit, second_fit):
        diff = first_fit.getChi2()*1e10 - second_fit.getChi2()*1e10
        if diff < 0: # Put chi2 as first parameter
            return -1
        elif diff == 0:
            return 0

        else:
            return 1

    def approx_equal(x, y, tol=1e-18, rel=1e-7):
        if tol is rel is None:
            raise TypeError('cannot specify both absolute and relative errors are None')
        tests = []
        if tol is not None: tests.append(tol)
        if rel is not None: tests.append(rel*abs(x))
        assert tests
        return abs(x - y) <= max(tests)

        
    fit_list = []

    for file in files:

        fit = {}
        path = os.path.join(directory, file)
        with open(path, 'r') as f:
             output = json.load(f)
        params_in = output['parameters_in']
        model = output['model']
        print directory, file,"Model=",model
        
        
        outf ="fixed params: "
        outv="variable params: "
        for p in params_in:
            if not(p['fixed']):
               outv += " "+p['paramname']+"="+str(p['value'])
            else:
               outf += " "+p['paramname']+"="+str(p['value'])
        print outv
        print outf
        if 'fit' in output:
            model = output['model']
            params = [p['paramname'] for p in \
                       REGISTERED_MODELS[model]['exp_vals']]
            params.insert(0, 'chi2')

            for param in params:
                param_value = float(output['fit'][param]['value'])
                fit[param] = param_value 
            fit['file'] = file
            fit_list.append(SimData(output,file))
        else:
	     print "no Fit"
    pass
    print "Number of valid fits=",len(fit_list)
    fit_list.sort(compare_chi2)


    concatenated = [fit_list[0]]
    count = 1
    i=0
    while i < len(fit_list)-1:
        if approx_equal(fit_list[i].getChi2(), fit_list[i+1].getChi2(), rel=1e-6):
            concatenated[-1].addSimData(fit_list[i+1])
            count+=1
        else:
            concatenated.append(fit_list[i+1])
            count = 1
        i+=1
    print "Number of distinct fits=",len(concatenated)
    for fit in concatenated:
        print fit

    return concatenated
pass   

def plotFit(idata,qdata,idev,fit):
    
    fig = plt.figure()         
    ax = fig.add_subplot(111)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    params= {'legend.fontsize':10} 
    plt.rcParams.update(params)
    plt.errorbar(qdata, idata, yerr=idev, ls='.', label='Experimental', capsize=0)
    
    axes = fig.gca()

    legendString = ['Experimental']
    print 'SimFile: ' +  str(fit.file)
    count = len(fit.file)

    pnames=getPlotParams(fit.getModel())

    p0vals=[]
    p1vals=[]
    p2vals=[]

    nparams = len(pnames)
    
    for idx in range(count):
        params = fit.getParamsIn(idx)
        keys = params.keys()
        for i in range(len(params)):
            s='p' + str(i) +'vals.append(params["' + keys[i] + '"])'
            exec(s)
    pass

    minvals=[]
    maxvals=[]
            
    if(len(p0vals) > 0):
    	minvals.append(min(p0vals))
	maxvals.append(max(p0vals))
    if(len(p1vals) > 0):
    	minvals.append(min(p1vals))
	maxvals.append(max(p1vals))
    if(len(p2vals) > 0):
    	minvals.append(min(p2vals))
	maxvals.append(max(p2vals))
      
    chi2 = fit.getChi2()
   
    legend0= "Model - %s\nSimulated response from %d fits. Chi2=%e\n" \
       % (fit.getModel(), count, chi2)
    legend1 = legend0 + "Parameter ranges:\n"
    for i in range(len(pnames)):
        legend1 += '%s from %s to %s\n' \
            % (pnames[i],minvals[i], maxvals[i])

    pout = fit.getParamsOut()
    legend1+='Fitted values:\n'
    for i in range(len(pnames)):
        #r,l = fit.getRLOut(param1,param2)
        pname = pnames[i]
        legend1 += '%s=%.3f\n' % (pname,pout[pname]['value'])

    print legend1


    model = fit.getModel()
#    radius,length = fit.getParamsIn(param1,param2)
    simulatedI, simulatedQ = fit.getSimIQ()
 
    fig.canvas.set_window_title(model)    
  
#         axN = fig.add_axes(axes)

#    plt.plot(simulatedQ, simulatedI, label='l=%.1f, r=%.1f' % (length, radius))
    plt.plot(simulatedQ, simulatedI, label=legend1)
    
    #===========================================================================
    # Order of labels in legendString should relate to order of plotting operations...
    #===========================================================================
    
#        legendString.append('l=%.1f, r=%.1f' % (length, radius))
     
#        ax.legendString(['Experimental', 'l=%.2f, r=%.2f' % (length, radius)], loc='upper right')
    
#    ax.set_xlabel(r'$\mathrm{ Q/Angstrom^{-1} }$')

    plt.legend(loc='lower left')
    
    ax.set_xlabel(ur'$\mathrm{ Q/\u00c5^{-1} }$')
    ax.set_ylabel(r'$\mathrm{ I/cm^{-1} }$')
    ax.set_xscale('log')
    ax.set_yscale('log')
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
  
    xmin = sorted(qdata)[0]
    xmax = sorted(qdata)[-1]
   
    xminsimq = sorted(simulatedQ)[0] 
    xmaxsimq = sorted(simulatedQ)[-1]
        
    xmin = min( xmin, xminsimq )
    xmax = max( xmax, xmaxsimq ) 
    
    ymin = sorted(idata)[0]
    ymax = sorted(idata)[-1]
    
    ymin = min( ymin, sorted(simulatedI)[0]  ) 
    ymax = max( ymax, sorted(simulatedI)[-1] )
    
    ax.set_ylim(ymin, ymax)
    ax.set_xlim( xmin * 0.9, xmax * 1.1)
    
    plt.show()
    
    return fig

def createPlots(exptDataFilename, simulationOutputDirectory, plotOutDir):    
   
    try:
        expt_file = open(exptDataFilename)
    except IOError as e:
        print e
        return
    if splitext(basename(exptDataFilename))[1]  == '.xml':
        i, q, idev = getExptValuesXml(expt_file)
    else:
        i, q, idev = getExptValuesCols(expt_file) 
    
    
    print 'Number of experimental data points = %d' % len(i)

    if not os.path.isdir(simulationOutputDirectory):
        print simulationOutputDirectory + ' is not a directory'
        return
    json_files = []
    for file in os.listdir(simulationOutputDirectory):
        if file[len(file)-4:len(file)] == 'json':
             json_files.append(file)
    print str(len(json_files)) + " simulation output file(s) found"
    json_files.sort()

    fits = processSimOutput(simulationOutputDirectory, json_files)
    print str(len(fits)) + " fits to plot"
     
#    fig = plt.figure(num=model)
#    for plot in concatenated:
 
    for fit in fits:
         fig = plotFit(i,q,idev,fit)
         pngFile = getStem(fit.getFileName()) + '.png'
         pngFile = os.path.join(plotOutDir, pngFile)
         fig.savefig(pngFile, bbox_inches='tight')
    
if __name__ == "__main__":
    argc = len(sys.argv)
    
    if (argc < 4):
        print 'Usage: isisplot exptData simOutFile plotoutDir'
        sys.exit(-1) 
        
    exptDataFilename = sys.argv[1]
    simulationOutputDirectory = sys.argv[2]
    plotOutDir = sys.argv[3]
    
    createPlots(exptDataFilename, simulationOutputDirectory, plotOutDir)    

    
