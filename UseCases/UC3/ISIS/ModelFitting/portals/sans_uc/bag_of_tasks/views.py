"""Contains the business logic for the pages of the bag_of_tasks app.

Each view must return an HttpResponse object containing the content for the
requested page, or raise an exception, such as Http404. Generally, a view
retrieves data according to the parameters, loads a template and renders the
template with the retrieved data.

Official docs: https://docs.djangoproject.com/en/1.4/topics/http/views/

"""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012-2013 The Science and Technology Facilities Council
#===============================================================================

import uuid

import time

import stat

import subprocess

import sans_uc.settings as settings

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext, loader

from django.contrib.auth.models import User

from subprocess import Popen

from django.core.urlresolvers import reverse

import forms
import models
import utils

import logging

import os
import sys


import httplib

import urllib
import urllib2

import StringIO

import json
from subprocess import CalledProcessError

from cps import taskfarm


home = ""

try:   
    home = os.environ['HOME']
except KeyError:
    
    #=============================================================================
    # When running as user 'www-data', I found that HOME wasn't in the environment
    # Set it explicitly
    #=============================================================================
    
    home = '/var/www'

    #===============================================================================
    # Who needs os.environ['HOME'] ???
    #===============================================================================    
    
    os.environ['HOME'] = home
    


PASSWORD = ""
USERNAME = ""
ENDPOINT = ""

CONFDIR = os.path.join(home, '.conpaas')
try:
    
    #=====================================================================================
    # The files cert.pem and key.pem will be read later, so let's check that they exist...
    #=====================================================================================
    
    f = file( os.path.join( CONFDIR, 'cert.pem'), 'r')
    f.close()
    
    f = file( os.path.join( CONFDIR, 'key.pem'), 'r')   
    f.close()
            
    f = file( os.path.join( CONFDIR, 'password'), 'r')
    PASSWORD = f.read()   
    f.close()
    
    f = file( os.path.join( CONFDIR, 'target'), 'r') 
    ENDPOINT = f.read()
    f.close()
    
    f = file( os.path.join( CONFDIR, 'username'), 'r')
    USERNAME = f.read()              
    f.close()
    
except IOError as e:
    print 'Cannot open or read file: reason %s' % e
    sys.exit(0)

    
from conpaas.core import https
https.client.conpaas_init_ssl_ctx(CONFDIR, 'user')

ipaddr = "0.0.0.0"

SID = "9"


nTot = 0
nDone = -1
nErrs = 0

logger = logging.getLogger(__name__)



def http_jsonrpc_post(hostname, port, uri, method, params):
    url = "http://%s:%s%s" % (hostname, port, uri)

    data = json.dumps({ 'method': method, 'params': params, 
        'jsonrpc': '2.0', 'id': 1 })

    req = urllib2.Request(url, data, {'Content-Type': 'application/json'})

    res = urllib2.urlopen(req).read()
    return res


def http_file_upload_post(host, port, uri, params={}, files=[]):
    content_type, body = (None, None) # https.client._encode_multipart_formdata(params, files)
    h = httplib.HTTP(host, port)
    h.putrequest('POST', uri)
    h.putheader('content-type', content_type)
    h.putheader('content-length', str(len(body)))
    h.endheaders()
    h.send(body)
    errcode, errmsg, headers = h.getreply()
    return h.file.read()

def http_file_upload_post_h(host, port, uri, params={}, files=[]):
    content_type, body =  (None, None) # https.client._encode_multipart_formdata(params, files)
    h = httplib.HTTP(host, port)
    h.putrequest('POST', uri)
    h.putheader('content-type', content_type)
    h.putheader('content-length', str(len(body)))
    h.endheaders()
    h.send(body)
    errcode, errmsg, headers = h.getreply()
    return h




def add_data(request):
    """View for uploading data files and associating with user."""
    if request.method == 'POST':
        form = forms.FileForm(request.POST, request.FILES)
        if form.is_valid():
            new_data = models.DataFile(data_file=request.FILES['ul_file'])                
            new_data.owner = request.user 
            new_data.save()
#           messages.add_message(request, messages.SUCCESS,
#                                'Uploaded datafile')
            return HttpResponseRedirect('/uc3/bot/creation')
    else:
        form = forms.FileForm()
    return render_to_response('bag_of_tasks/add_data.html',
                              {'form': form},
                              context_instance=RequestContext(request))




# def upload(request):
#     """View for basic file upload page."""
#     
#     if True:
#         return HttpResponseRedirect('/uc3/bot/creation')
# 
#     nDone, nTot, nErrs = getJobsDone(SID)
# 
#     if nDone > 0 and nDone  < nTot:
#         return HttpResponse('Task Farm is already running')
# 
#     ul_file = ""
#     xtfs_loc = ""
# 
#     message = ""
#     if request.method == 'POST':
# 
#         postdict = request.POST
#         
#         if not postdict:
#             message = message + "No POST Params"
#             
#         else:
# 
#             if "xtfs_loc" in postdict:
#                 xtfs_loc = postdict["xtfs_loc"]
#             
#             
#         filesdict = request.FILES
#         if not filesdict:
#             message = message + " No FILES Params"
#         
#         else:
#                 
#             if "ul_file" in filesdict:
#                 ul_file = filesdict["ul_file"]
#             
#             
#         
# 
#         form = forms.BaTFileForm(request.POST, request.FILES)
#         if form.is_valid():
# #           newdoc = models.BoTFile(task_file=request.FILES['ul_file'])
# #           newdoc.save()
#             messages.add_message(request, messages.SUCCESS, message)
#             service = service_dict(SID)
#             ip = service['manager']
#             port = 8475
# 
#             h = upload_bag_of_tasks(ip, port, ul_file, xtfs_loc)
#             errcode, errmsg, headers = h.getreply()
#     
#             if errcode != 200:
#                 return HttpResponse('Problem submitting Bag-of-Tasks:\n%s' % (errmsg,))
#     
#             else: 
#                 reply = h.getfile().read()
# 
#                 return HttpResponseRedirect('/uc3/bot/monitor')
# #                return HttpResponse('Submit Bag-of-Tasks OK: reply:\n%s' % (reply,))
# 
# 
#         
#         
#     else:
#         form = forms.BaTFileForm()
#         return render_to_response('bag_of_tasks/BaTupload.html', {'form': form},
#                               context_instance=RequestContext(request))


   
def upload_bag_of_tasks(host, port, filename, xtreemfs_location):
    """eg: upload_bag_of_tasks(host="192.168.122.5", 
                               port="8475",
                               filename="/var/lib/outsideTester/contrail3/test.bot", 
                               xtreemfs_location="131.254.201.4/xtfs")
    """
    params = { 'uriLocation': xtreemfs_location, 'method': 'start_sampling' }
    filecontents = filename.read()
    h =  http_file_upload_post_h(host, port, '/', params, files=[('botFile', filename.name, filecontents)])
    return h 


def executeOneShot(request):

    """
    Purpose:

    """

    import shlex
    
    if request.method == 'GET':
        form = forms.ExecuteOneShotForm()
           
        return render_to_response('bag_of_tasks/executeOneShot.html',
                              {'form': form}, 
                              context_instance=RequestContext(request))
    else:    

#         uuid = request.session['uuid'] # We do not now set this in creation()

#        counter = os.path.join ( settings.MEDIA_ROOT, 'progress-%s' % uuid )
        
        filename = request.session['botfile']       
        filename = os.path.join (settings.MEDIA_ROOT, filename)
         
        f = None
        try:
            f = open(filename, 'r')
        except IOError as e:
            print 'IOError %s' % e.strerror
                        
        i = 0
        for command in f:    

            utils.updateCounter(settings.PROGRESS_COUNTER, i)
            
            if i % 10 == 0:                 
                print 'Command %d' % i               
            i += 1
            if (i == 1):
                print 'Command = %s' % command
                        
            cmdAndArgs = shlex.split(command)
                
            try:
                subprocess.check_output(cmdAndArgs, stderr=subprocess.STDOUT)
                #messages.add_message(request, messages.SUCCESS, output) # If we have assigned 'output' in previous line...
         
        # NB With subprocess.check_output(command, stderr=subprocess.STDOUT) we still get a stack traceback in Django
        # What we wanted to get was the error message from the command placed into the exception .output field.
        #
            except CalledProcessError as e:
                messages.add_message(request, messages.ERROR, e.output)
                
        print 'Command %d' % i
        
        utils.destroyCounter(settings.PROGRESS_COUNTER)
            
        return HttpResponseRedirect('')      
  
def executeOneShotSpawn(request):

    """
    Purpose:

    """

    import shlex
    
    if request.method == 'GET':
        form = forms.ExecuteOneShotForm()
           
        return render_to_response('bag_of_tasks/executeOneShot.html',
                              {'form': form}, 
                              context_instance=RequestContext(request))
    else:    

#         uuid = request.session['uuid']  # We do not now set this in creation

#        counter = os.path.join ( settings.MEDIA_ROOT, 'progress-%s' % uuid )
        
        filename = request.session['botfile']      # We do not now set this in creation() 
        filename = os.path.join (settings.MEDIA_ROOT, filename)
         
        proc = subprocess.Popen(filename)
        
        request.session['process'] = proc
        
        f = None
        try:
            f = open(filename, 'r')
        except IOError as e:
            print 'IOError %s' % e.strerror
                        
        i = 0
        for command in f:    

            utils.updateCounter(settings.PROGRESS_COUNTER, i)
            
            if i % 10 == 0:                 
                print 'Command %d' % i               
            i += 1
            if (i == 1):
                print 'Command = %s' % command
                        
            cmdAndArgs = shlex.split(command)
                
            try:
                subprocess.check_output(cmdAndArgs, stderr=subprocess.STDOUT)
                #messages.add_message(request, messages.SUCCESS, output) # If we have assigned 'output' in previous line...
         
        # NB With subprocess.check_output(command, stderr=subprocess.STDOUT) we still get a stack traceback in Django
        # What we wanted to get was the error message from the command placed into the exception .output field.
        #
            except CalledProcessError as e:
                messages.add_message(request, messages.ERROR, e.output)
                
        print 'Command %d' % i
        
        utils.destroyCounter(settings.PROGRESS_COUNTER)
            
        return HttpResponseRedirect('')      
    





def results(request):
    """View for full task monitoring and results test."""
    refresh = False
    files = None
    if request.session.get('processes', default=[]):
        refresh = True
        polls = [p.poll() for p in request.session['processes']]
        
        # If None doesn't appear in the list, all processes have terminated
        if None not in polls:   
            del request.session['processes']
            messages.add_message(request, messages.INFO,
                                 'Processes finished!')
        else:
            messages.add_message(request, messages.INFO,
                '{0} of {1} processes complete.'.format(
                    len(polls) - polls.count(None), len(polls))
                )
            messages.add_message(request, messages.INFO,
                                 'Processes running...')
        # Need to tell django that session variable is modified as it
        # doesn't detect modifications of individual variable elements.
        request.session.modified = True
    else:
        path = os.path.join(settings.MEDIA_ROOT)
        files = [{'name': name, 'url': '/media/' + name} for
                 name in os.listdir(path) if name.endswith('.txt')]

    return render_to_response('bag_of_tasks/results.html',
                              {'files': files, 'refresh': refresh},
                              context_instance=RequestContext(request))


def monitor(request):
    
    print 'In monitor method now...'
    
    completed = utils.readCounter(settings.PROGRESS_COUNTER)
    
    total = request.session['total_tasks']
    
    return render_to_response('bag_of_tasks/monitor.html', { 'completed': completed, 'total': total}, 
                                context_instance=RequestContext(request))



def monitorLocalTaskfarm(request):

    output_dir = request.session['output_dir']
    completed = int( utils.getCompleted(output_dir) )
    
    total = int( request.session['total_tasks'] )
    
    process = request.session['process']


    try:
        client = request.session['taskfarm_client'] 
    except KeyError:
        print 'setting taskfarm_client object in list_services'
        client = taskfarm.Client()
        request.session['taskfarm_client'] = client
    else:
        print 'taskfarm_client object already set' 
        
        
    
    if process.poll() == None: 
        
        print 'monitorLocalProcess: poll() reports processes are still running'
        # Processes are still running
       
        return render_to_response('bag_of_tasks/monitorLocalProcess.html', { 'completed': completed, 'total': total}, 
                                context_instance=RequestContext(request))        
    else: 
        #=======================================================================
        # Process has completed
        #=======================================================================
        
        print 'monitorLocalProcess: poll() reports process has finished '
        
        del request.session['process']
        
        # Processes have completed, check to see if all results have been written
        
        limit = settings.OUTPUT_FILE_WRITE_DELAY
        output_settle_time = 0
        
        print 'Waiting for remaining output files to be written..'
        
        while ( int( utils.getCompleted( output_dir )  ) < total  
               and output_settle_time < limit ):  
            
            # Wait for output files to be written and visible 
            # This is to allow for latency in distributed filesystems                                                   
            time.sleep(1)
            output_settle_time += 1
            
        print '... done waiting'
        
        if  output_settle_time != 0:
            print 'Output Settle Time: %d' % output_settle_time
        
        #===================================================================
        #
        # Sanity check: might still be some processes which didn't complete 
        #
        #===================================================================            
            
        tasks_remaining = total - int ( utils.getCompleted( output_dir ) )
        
        if tasks_remaining != 0:
            print 'Tasks Remaining: %d' % tasks_remaining
            
        return render_to_response('bag_of_tasks/completed.html', 
                                  { 'completed': completed, 'total': total, 
                                   'output_settle_time': output_settle_time, 'tasks_remaining': tasks_remaining},
                                  context_instance=RequestContext(request))  
    
def monitorLocalProcess(request):

    output_dir = request.session['output_dir']
    completed = int( utils.getCompleted(output_dir) )
    total = int( request.session['total_tasks'] )
    
    process = request.session['process']
    
    if process.poll() == None: 
        
        print 'monitorLocalProcess: poll() reports processes are still running'
        # Processes are still running
       
        return render_to_response('bag_of_tasks/monitorLocalProcess.html', { 'completed': completed, 'total': total}, 
                                context_instance=RequestContext(request))        
    else: 
        #=======================================================================
        # Process has completed
        #=======================================================================
        
        print 'monitorLocalProcess: poll() reports process has finished '
        
        del request.session['process']
        
        # Processes have completed, check to see if all results have been written
        
        limit = settings.OUTPUT_FILE_WRITE_DELAY
        output_settle_time = 0
        
        print 'Waiting for remaining output files to be written..'
        
        while ( int( utils.getCompleted( output_dir )  ) < total  
               and output_settle_time < limit ):  
            
            # Wait for output files to be written and visible 
            # This is to allow for latency in distributed filesystems                                                   
            time.sleep(1)
            output_settle_time += 1
            
        print '... done waiting'
        
        if  output_settle_time != 0:
            print 'Output Settle Time: %d' % output_settle_time
        
        #===================================================================
        #
        # Sanity check: might still be some processes which didn't complete 
        #
        #===================================================================            
            
        tasks_remaining = total - int ( utils.getCompleted( output_dir ) )
        
        if tasks_remaining != 0:
            print 'Tasks Remaining: %d' % tasks_remaining
            
        return render_to_response('bag_of_tasks/completed.html', 
                                  { 'completed': completed, 'total': total, 
                                   'output_settle_time': output_settle_time, 'tasks_remaining': tasks_remaining,
                                    'output_dir': output_dir},
                                  context_instance=RequestContext(request))        

def monitorLocalProcessBar(request):

    output_dir = request.session['output_dir']
    completed = int( utils.getCompleted(output_dir) )
    total = int( request.session['total_tasks'] )
    
    process = request.session['process']     
        
    if completed != total: 
        
        #=======================================================================
        # Tasks are still running
        #=======================================================================
        
        if process.poll != None:
            
            print 'Process is %d' % process.pid
        
            if completed == 0:
                percent = 0.00
            else:
                
                percent = "{0:.2f}".format( 1.0 -  float ( total - completed ) / float( total ) ) 
                         
            return render_to_response('bag_of_tasks/monitorLocalProcessBar.html', { 'percent': percent,
                                                                                   'completed': completed, 'total': total}, 
                                    context_instance=RequestContext(request))    
        else:
            print 'Some tasks have not completed, but the process %d has finished' % process.pid
            
            output_settle_time = 0
            
            tasks_remaining = total - completed
            
            del request.session['process']
            
            return render_to_response('bag_of_tasks/completed.html', 
                                      { 'completed': completed, 'total': total, 
                                       'output_settle_time': output_settle_time, 'tasks_remaining': tasks_remaining,
                                        'output_dir': output_dir},
                                      context_instance=RequestContext(request))              
                     
    else: 

        #=======================================================================
        # Tasks have completed
        #=======================================================================
        
        if process.poll == None:
            print 'Child Process PID %d is still running' % process.pid

        else:
            del request.session['process']
        
        # Processes have completed, check to see if all results have been written
        
        limit = settings.OUTPUT_FILE_WRITE_DELAY
        output_settle_time = 0
        
        print 'Waiting for remaining output files to be written..'
        
        while ( int( utils.getCompleted( output_dir )  ) < total  
               and output_settle_time < limit ):  
            
            # Wait for output files to be written and visible 
            # This is to allow for latency in distributed filesystems                                                   
            time.sleep(1)
            output_settle_time += 1
            
        print '... done waiting'
        
        if  output_settle_time != 0:
            print 'Output Settle Time: %d' % output_settle_time
        
        #===================================================================
        #
        # Sanity check: might still be some tasks which didn't complete 
        #
        #===================================================================            
            
        tasks_remaining = total - int ( utils.getCompleted( output_dir ) )
        
        if tasks_remaining != 0:
            print 'Tasks Remaining: %d' % tasks_remaining
            
        return render_to_response('bag_of_tasks/completed.html', 
                                  { 'completed': completed, 'total': total, 
                                   'output_settle_time': output_settle_time, 'tasks_remaining': tasks_remaining,
                                    'output_dir': output_dir},
                                  context_instance=RequestContext(request))        


@login_required
def creation(request):
    """View for showing files and models for creating BoT files.

    This is the central interface that users will be presented with.

    """
# TODO: Combine sweep and single model runs into one add_sweep/model interface?
    # Default unbound selection form.
    selection = forms.ModelSelection()
    if request.method == 'POST':
        
        if request.POST.get('delete_file'):
            models.DataFile.objects.filter(
                pk__in=request.POST.getlist('delete_file')).delete()
                
        elif request.POST.get('delete_model'):
            for item in request.POST.getlist('delete_model'):
                mod, ident = item.split('_')
#                messages.add_message(request, messages.INFO, item) # Don't bother to list models which have just been deleted
                
                getattr(models, mod).objects.get(pk=ident).delete()
                
        elif request.POST.get('edit_model'):
# TODO: Add model editing
# TODO: Look into using <button> element to hide value
            pass
        
        elif request.POST.get('create'):
            
            try:
                client = request.session['taskfarm_client'] 
            except KeyError:
                print 'Setting taskfarm_client object'
                client = taskfarm.Client()
                request.session['taskfarm_client'] = client
            else:
                print 'taskfarm_client object already set'

                
            
            outdir = utils.get_OutputDir('output-%s')
            
            if settings.VARIABLE_DEBUG:
                print 'Output dir is %s' % outdir
            
            try:
                os.mkdir(outdir)
              
            except IOError as e:
                print 'IOError when making %s: %s' % (outdir, e.strerror)
                
                messages.add_message(request, messages.ERROR,
                                     'Could not create output directory ' + outdir)                  
                return HttpResponseRedirect('/bot/creation')
            
            #=======================================================
            # 
            #=======================================================
                        
                        
            modelset = utils.get_user_models(request, (models.Cylinder,
                                            models.Sphere,
                                            models.Ellipse))
# ijj                                            
#                                            models.CoreShellBicelle
                                             
                                             
            sweepset = utils.get_user_models(request, (models.CylinderSweep,
                                            models.SphereSweep,
                                            models.EllipseSweep))
# ijj                                            
#                                            models.CoreShellBicelleSweep)


            datums = utils.get_user_models(request, (models.DataFile,))
            
            datadict = []
            for d in datums:
                                   
                datadict.append( str(d.data_file) )

#                 if settings.VARIABLE_DEBUG:
#                     print 'datums[i].datafile %s' % d.data_file
            
#             if settings.VARIABLE_DEBUG:    
#                 for e in datadict:
#                     print 'e = %s' % e
           
#            uuidN = uuid.uuid1()
      
#            outdir = os.path.join ( settings.OUTPUT_ROOT, 'output-%s' % str (uuidN) )
            
 
            

            #===================================================================
            # 
            #===================================================================
            
            botData = utils.create_bot_with_sweep(sweepset, datadict, outdir, settings.BAG_OF_TASKS_MAXLENGTH)

            botLength = botData.count("\n") # Count before possibly adding shell magic number string
                        
            if settings.SPAWN_LOCAL_SHELL:
                botData = settings.SHELL_MAGIC + botData
            
            newdoc, file_obj = utils.createAndSaveDocument(botData, 'isis.bot', request.user)
                        
            botFilename = os.path.join(settings.MEDIA_ROOT, str( newdoc ) )
 
            print 'Path to BoT file: %s' % botFilename   
            
            request.session['total_tasks'] = str( botLength )
            
            #===================================================================
            # 
            #===================================================================
            
            if settings.SPAWN_LOCAL_SHELL:
                
                request.session['output_dir'] = outdir 
                
                      
                try:
                    os.chmod(botFilename, stat.S_IXUSR | stat.S_IRUSR)
                except IOError as e:
                    print "Can't change %s to be executable" % botFilename 
                    messages.add_message(request, messages.ERROR,
                                         'Could not change executable' + botFilename)                  
                    return HttpResponseRedirect('/uc3/bot/creation')                           
    
                popenFailed = False
                try:
                    proc = Popen(botFilename, shell=True) 
                             
                except IOError as e:
                    print 'Cannot Popen: %s' % e.strerror
                    popenFailed = True
                    
                except ValueError as e:
                    print 'ValueError in Popen'
                    popenFailed = True
                    
                else:
                    request.session['process'] = proc
                    
                if popenFailed:
                    messages.add_message(request, messages.ERROR,
                                         'Could not Popen ' + botFilename)                  
                    return HttpResponseRedirect('/bot/creation')               
                    
                    
                
                #===================================================================
                # All is well!
                #===================================================================
                
                monitorURL = '/uc3/bot/monitorLocalProcessBar'
                print 'redirecting to %s' % monitorURL
                return HttpResponseRedirect( reverse('bag_of_tasks.views.monitorLocalProcessBar') )
            
            
            elif settings.CONPAAS_TASKFARM:
                
                #===============================================================
                # 
                #===============================================================
                

                sid = settings.CONPAAS_SID
                       
                client.check_service_id(sid)
    
                if not os.path.isfile(botFilename):
                    print "E: %s is not a file" % botFilename
                    sys.exit(0)
    
                res = client.upload_bag_of_tasks(sid, botFilename, settings.XTREEMFS_LOCATION)
                if 'error' in res['result']:
                    print res['result']['error']
                elif 'message' in res['result']:
                    print res['result']['message']
                else:
                    print res   
                    
                return HttpResponseRedirect('/uc3/bot/monitorLTaskFarm')                 
            
            else:
                pass
            
                     
#             return render_to_response('bag_of_tasks/submitBoT.html', {'botLength': botLength}, context_instance=RequestContext(request))

#            return HttpResponseRedirect('/uc3/bot/upload')
        
        #
        # Do we still need to add the selections of a single model, when most users wnat to define a sweep?
        #
        
        elif request.POST.get('add_model'):
            selection = forms.ModelSelection(request.POST)
            
        elif request.POST.get('add_sweep'):
            selection = forms.ModelSelection(request.POST)
            
    #
    # Do if request isn't a POST, or if we haven't already returned from processing the POST
    #         

    files = models.DataFile.objects.filter(owner=request.user)

    modelset = utils.get_user_models(request, (models.Cylinder,
                                               models.Sphere,
                                               models.Ellipse))
# ijj    
#                                                models.CoreShellBicelle))
 
    sweepset = utils.get_user_models(request, (models.CylinderSweep,
                                               models.SphereSweep,
                                               models.EllipseSweep))
# ijj    
#                                                models.CoreShellBicelleSweep))
                                               
    # Calc total number of commands that will be added to BoT.
    runs = len(modelset)
    for sweep in sweepset:
        runs += sweep.runs

    return render_to_response('bag_of_tasks/creation.html',
                              {'files': files,
                               'models': modelset,
                               'sweeps': sweepset,
                               'selection': selection,
                               'runs': runs},
                              context_instance=RequestContext(request))




#@login_required
def add_model(request):
    """View for adding sans models and associating with user."""
    # Pull out selected model from request, default to Sphere.
    model = request.GET.get('select_models', 'Sphere')
    # Get form object from user selection, but don't instantiate
    form_object = getattr(forms, str(model) + 'Form')
    if request.method == 'POST':
        form = form_object(request.POST)
        if form.is_valid():
            # Two stage save used, otherwise request.user is added as a
            # LazyObject wrapper (ambiguous until used). Also used for adding
            # fixed_field data.
            new_model = form.save(commit=False)
            new_model.fixed_fields = str(request.POST.getlist('fixed'))
            new_model.owner = request.user
            new_model.save()

            messages.add_message(request, messages.SUCCESS,
                                 'Model run added!')
            return HttpResponseRedirect('/bot/creation')
    else:
        form = form_object()
    return render_to_response('bag_of_tasks/add_model.html',
                              {'form': form},
                              context_instance=RequestContext(request))


#@login_required
def add_parameter_sweep(request):
    """View for adding a sans model with a parameter sweep."""
    # Pull out selected model from request, default to Sphere.
    model = request.GET.get('select_models', 'Sphere')
    # Get form object from user selection, but don't instantiate
    form_object = getattr(forms, str(model) + 'SweepForm')
    if request.method == 'POST':
        form = form_object(request.POST)
        if form.is_valid():
            # Calculate number of lines that will be added to BoT
            fields = form.cleaned_data
            lines = 1
            for field in fields:
                if len(fields[field].split(',')) == 3:
                    floats = [float(x) for x in fields[field].split(',')]
                    lines *= len(list(utils.frange(*floats)))

            # Two stage save used, otherwise request.user is added as a
            # LazyObject wrapper (ambiguous until used). Also used for adding
            # fixed_field data.
            new_model = form.save(commit=False)
            new_model.fixed_fields = str(request.POST.getlist('fixed'))
            new_model.owner = request.user
            new_model.runs = lines
            new_model.save()

#             messages.add_message(request, messages.SUCCESS,
#                                  'Parameter sweep added!')

            return HttpResponseRedirect('/uc3/bot/creation')
    else:
        form = form_object()
    return render_to_response('bag_of_tasks/add_sweep.html',
                              {'form': form},
                              context_instance=RequestContext(request))


#@login_required
def file_paths(request):
    """View for displaying the paths of uploaded data files and created BoT."""
    data_files = models.DataFile.objects.filter(owner=request.user.id)
    bot_files = models.BoTFile.objects.filter(owner=request.user.id)

    data_files = [{'name': f, 'url': settings.MEDIA_URL + str(f)} for
        f in data_files]

    bot_files = [{'name': f, 'url': settings.MEDIA_URL + str(f)} for
    f in bot_files]
    
    
    print bot_files
       
    return render_to_response('bag_of_tasks/file_paths.html',
                              {'data_paths': data_files,
                               'bot_paths': bot_files},
                              context_instance=RequestContext(request))

   
def list_services(request):
    
    print 'In list_services'
    
    try:
        client = request.session['taskfarm_client'] 
    except KeyError:
        print 'setting taskfarm_client object in list_services'
        client = taskfarm.Client()
        request.session['taskfarm_client'] = client
    else:
        print 'taskfarm_client object already set'  
                  
    services = client.callapi("list", True, {})
    
    services = [ {'name': s['name'], 'sid': s['sid'], 'vmid': s['vmid']} for s in services]
    
    return render_to_response('bag_of_tasks/ConPaaS-service-list.html',
                              {'services': services},
                              context_instance=RequestContext(request))
    
            
