"""The URL declarations specific to the bag_of_tasks app.

Official docs: https://docs.djangoproject.com/en/1.4/topics/http/urls/

"""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012 The Science and Technology Facilities Council
#===============================================================================

 
from django.conf import settings
from django.conf.urls import patterns, url
# from django.views.generic.simple import direct_to_template


urlpatterns = patterns('bag_of_tasks.views',

    url(r'^creation/$', 'creation'),

    url(r'^creation/add_data/$', 'add_data'),

    url(r'^creation/add_model/$', 'add_model'),

    url(r'^creation/add_sweep/$', 'add_parameter_sweep'),

#     url(r'^plain_bot/$', TemplateView.as_view(template_name= 'plain_bot.html')),

    url(r'^file_paths/$', 'file_paths'),
    
    url(r'^monitor/$', 'monitor'),
    url(r'^monitorLocalProcess/$', 'monitorLocalProcess'),
    url(r'^monitorLocalProcessBar/$', 'monitorLocalProcessBar'),    
    url(r'listServices/$', 'list_services')         
)

# Only allow access to unsafe views if in DEBUG mode.
if settings.DEBUG:
    urlpatterns += patterns('bag_of_tasks.views',

# This caused an error when a new user logged in... 
#    url(r'^upload/$', 'upload'),

        url(r'^monitor/$', 'monitor'),

        url(r'^results/$', 'results'),

    )
