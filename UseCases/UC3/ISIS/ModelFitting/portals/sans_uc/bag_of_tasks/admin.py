"""Registers models with the admin interface.

Official docs: https://docs.djangoproject.com/en/1.4/ref/contrib/admin/

"""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012-2013 The Science and Technology Facilities Council
#===============================================================================


from django.contrib import admin

import models

class BoTFileAdmin(admin.ModelAdmin):
    """Class for data file display in the admin UI."""
    list_display = ('task_file', 'owner',)
    # Force display of read only field.
    readonly_fields = ('owner',)


class DataFileAdmin(admin.ModelAdmin):
    """Class for data file display in the admin UI."""
    list_display = ('data_file', 'owner',)
    # Force display of read only field.
    readonly_fields = ('owner',)


class ModelAdmin(admin.ModelAdmin):
    """Class for SansView model display in the admin UI."""
    list_display = ('__unicode__', 'owner', 'background', 'scale')
    # Force display of read only fields.
    readonly_fields = ('owner', 'fixed_fields',)


admin.site.register(models.BoTFile, BoTFileAdmin)
admin.site.register(models.DataFile, DataFileAdmin)

admin.site.register(models.Cylinder, ModelAdmin)
admin.site.register(models.Sphere, ModelAdmin)
admin.site.register(models.Ellipse, ModelAdmin)

# ijj
#admin.site.register(models.CoreShellBicelle, ModelAdmin)

admin.site.register(models.CylinderSweep, ModelAdmin)
admin.site.register(models.SphereSweep, ModelAdmin)
admin.site.register(models.EllipseSweep, ModelAdmin)

# ijj
#admin.site.register(models.CoreShellBicelleSweep, ModelAdmin)
