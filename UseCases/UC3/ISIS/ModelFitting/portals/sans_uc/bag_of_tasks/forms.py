"""Defines form objects used in views.

A Form object encapsulates a sequence of form fields and a collection of
validation rules that must be fulfilled in order for the form to be accepted.

Official docs: https://docs.djangoproject.com/en/1.4/topics/forms/

"""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012-2013 The Science and Technology Facilities Council
#===============================================================================


from django import forms

import models


class FileForm(forms.Form):
    """Form used for uploading files."""
    ul_file = forms.FileField(label='Select a file')

 
class BaTFileForm(forms.Form):
    """Form used for uploading files."""
    ul_file = forms.FileField(label='Select BaT file')
    xtfs_loc = forms.CharField(widget=forms.TextInput(attrs={'size': 30}), label='XtreemFS Volume', initial='myriads-serv2.irisa.fr/xtfs')


class ExecuteForm(forms.Form):
    """Form used for taking text input to be execued as a shell command."""
    command = forms.CharField()
    
class ExecuteOneShotForm(forms.Form):  
    """"Do not need any input areas on this form"""  
    pass
    
class MonitorForm(forms.Form):
    """Form used for taking text input to be execued as a shell command."""
    sid = forms.CharField()

class ModelSelection(forms.Form):
    """Form used for selecting a model from the available SansView models."""
    select_models = forms.MultipleChoiceField(
        choices=(('Cylinder', 'Cylinder'),
                 ('Sphere', 'Sphere'),
                 ('Ellipse', 'Ellipse'),
# ijj                 
#                 ('CoreShellBicelle', 'Core Shell Bicelle')),
        ),
        label='Select model',
        widget=forms.Select())


class CylinderForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.Cylinder


class SphereForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.Sphere


class EllipseForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.Ellipse

# ijj
# class CoreShellBicelleForm(forms.ModelForm):
#     """Shortcut class that creates a form directly from a model."""
#     class Meta:
#         model = models.CoreShellBicelle


# Forms for paramter sweeps
class CylinderSweepForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.CylinderSweep


class SphereSweepForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.SphereSweep


class EllipseSweepForm(forms.ModelForm):
    """Shortcut class that creates a form directly from a model."""
    class Meta:
        model = models.EllipseSweep

# ijj
# class CoreShellBicelleSweepForm(forms.ModelForm):
#     """Shortcut class that creates a form directly from a model."""
#     class Meta:
#         model = models.CoreShellBicelleSweep
