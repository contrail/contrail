"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

#===============================================================================
# Test suite for ISIS SANS_UC Portal
# Author: Ian Johnson
# Copyright: STFC 2012-2013
#===============================================================================

from django.test import TestCase
import sans_uc.settings as settings

from django.test.client import Client

import bag_of_tasks

import uuid

import stat

from time import sleep

from cps import taskfarm


from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

import libcloud

import libcloud.compute.drivers.opennebula

from bag_of_tasks.utils import create_bot_with_sweep
from bag_of_tasks.models import CylinderSweep

import bag_of_tasks.utils as utils
import os, sys

from time import time, sleep


from subprocess import Popen

from string import Template

# 
# Should contain tests for individual methods in utils
#
class SimpleTest(TestCase):
    
    def setUp(self):
        self.modelSweep = self.create_modelSweep()
        
        username = 'uc3user'
        password = 'uc3pass'
        
        self.createUser(username, password)
        
        print 'In setUp - Logged In: %s' % self.client.login(username=username, password=password)
    
    def create_modelSweep(self):
        modelSweep = CylinderSweep(radius = '2,20,2')
#        model.radius = 0.01,10.0, 0.05
        modelSweep.background = '1'
        modelSweep.length = '4,40,2'
        modelSweep.scale = '0.01'
        modelSweep.sldCyl = '3.22e-6'
        modelSweep.sldSolv = '6.33e-6'
        
        modelSweep.fixed_fields = ('background', 'scale', 'sldCyl', 'sldSolv')
        
#       print modelSweep.radius
        
#        print modelSweep
        
        return modelSweep
    
    
    def createUser(self, username, password):

        from django.contrib.auth.models import User
        self.user = User.objects.create_user(username, username +'@gmail.com', password)
        self.user.save()

        
#     def test_client_login(self):
#         
#         from django.contrib.auth.models import User
#         user = User.objects.create_user('uc3user', 'luc3user@gmail.com', 'uc3user')
#         user.save()
#         
#         c = Client()
#         loggedIn = c.login(username='uc3user', password='uc3user')
#         
#         print 'Logged in: %s' % loggedIn
#         
      
    def test_client_get_login_page(self):

        
        response = self.client.get('/accounts/login/')
        
        self.client.login(username='uc3', password='n00blers')
        assert response.status_code != 404  
        
#         print 'response.content'
#         print response.content 
#          
#         print 'response.templates'
#         print response.templates
#          
#         print response
 
#         print 'Iterate over response'
#         i = 0
#         for item in response:
#             print i
#             i += 1
         
        print "response['Last-Modified']"
#         print response['Last-Modified']
         
        print 'response.context'
#         print response.context
        
    def test_get_creation_page(self):
        
        print 'test_get_creation_page'
        response = self.client.get('/bot/creation/')
        
        assert response.status_code != 404
        
#        print response.content
        
    def test_generate_bot(self):
        
        data = settings.TESTDATA_DIR + '/pin_a.xml'
        
        #
        # Need to cast arguments as List elements
        #
        
        bot = create_bot_with_sweep([self.modelSweep], [data], limit=settings.BAG_OF_TASKS_MAXLENGTH)
        
        if settings.SPAWN_LOCAL_SHELL:
            bot = settings.SHELL_MAGIC + bot
            
        
        lines = bot.splitlines()
        
        print 'Len( lines ) = %d' % len ( lines )
               
        print len(lines), '\n', lines[0], '\n', lines[1], '\n', lines[-1]
               
        newdoc, file_obj = utils.createAndSaveDocument(bot, 'test.bot', self.user)
  
        botFilename = file_obj._get_name() 
               
        print 'BoT Document model is: %s ' % newdoc        
        print 'Name of latest BoT file: %s' % botFilename
        
        
        botFilename = os.path.join(settings.MEDIA_ROOT, str( newdoc ) )
        
        print 'Real path is %s' % botFilename
                       
        try:
            utils.setExecutableBit(botFilename)
            
        except IOError as e:
            print 'Could not change %s to be executable: reason %s' % (botFilename, e.strerror)        
        
#        BoT_file = bag_of_tasks.models.BoTFile(newdoc)
        
#        print 'Name from BotFile(newdoc).task_file.name() = %s' % str( BoT_file.task_file.get_filename() )

                    
    def test_newline_count(self):
        botFile = "\n\n" 
        assert botFile.count("\n") == len(botFile.splitlines())      
          

       
    def test_Popen(self):
 
        return
    
        uuidp = uuid.uuid1()
        
        filename = os.path.join( settings.TESTDATA_DIR, 'test-%s.bot' % str ( uuidp ))
        
        print 'BoT Filename = %s' % filename
         
#         uuidp = str( uuid.uuid1() )
         
#         outdir = os.path.join(settings.TESTDATA_DIR, os.path.join('output', uuidp) )
        
        outdir, cmd = self.createBot()
        
        
        
#         temp_name = os.tmpnam()
        try:
#             fd = os.open(temp_name, os.O_APPEND)

            self.f = file(filename, 'w')
        except IOError as e:
            print 'Cannot open %s: reason: %s' % (filename, e.strerror)
        except ValueError as e:
            print 'ValueError: %s' % e.strerror
        
        try:
            
            self.f.write(cmd)
            
            self.f.flush()
            
            self.f.close()
            
            os.chmod(filename, stat.S_IXUSR | stat.S_IRUSR)
            
        except IOError as e:
            print 'IOError: %s' % e.strerror
            
            
#             nbytes = os.write(fd, cmd)
#             assert nbytes == len(cmd)
        except IOError as e:
            print "Can't open or write %s: %s" % (filename, e.strerror)
         
        try:      
            
            os.mkdir(outdir)
        except IOError as e:
            print 'IOError when creating directory: %s' % e.strerror
            return
         
        proc = Popen(filename, shell=True)
         
        while proc.poll() == None:
            num_done = utils.getCompleted(outdir)
            print 'Completed %d' % num_done
            sleep(1)
             
        num_done = utils.getCompleted(outdir)
        print 'Completed %d' % num_done   
         
         
        

    def test_execute_single_model(self):
        
        import subprocess, sys        
        
        import shlex
        
        filename = os.path.join( settings.TESTDATA_DIR, 'testmf.bot')
        line1 = None
        
        print filename
        
        sys.stdout.flush()
        
        
        try:
            with open(filename, 'r') as f:
                
                i = 0
                for line1 in f:
                    print 'line %d: %s' % (i, line1)  # line1 = f.next() 
                    i += 1
        except IOError as e:
            print e
            return
                    
        print 'line1=%s\n' % line1
        
        cmdAndArgs = shlex.split(line1)
        
        print 'Line1 via shlex.split()'
        for c in cmdAndArgs:
            print c

        output = None
        try:
            output = subprocess.check_output(cmdAndArgs, stderr=subprocess.STDOUT)

            
        # NB With subprocess.check_output(command, stderr=subprocess.STDOUT) we still get a stack traceback in Django
        # What we wanted to get was the error message from the command placed into the exception .output field.
        #
        except subprocess.CalledProcessError as e: 
            print 'CalledProcessError %s' % e.output 
        else:
            print 'Success: output\n%s' % output      
        
        
    def test_UpdateCounter(self):
        
        filename = '/tmp/99'
        
        import random
        
        value = random.randint(0, 9999)
        utils.updateCounter(filename, value)
        
        assert open(filename, 'r').read() == str(value)
        
        
                    
    def test_CalcDirLen(self):
        
        dirname = '/etc' # os.path.join( os.path.expanduser('~ijj'), 'sans_uc/sans_uc/media/task_files/')
        
        dirlist = os.listdir(dirname)
        
#         print dirlist
        
        dirlen = len(dirlist)
        print 'Number of directory entries in %s is %d' % ( dirname, dirlen )
        
        os.sys.stdout.flush()
        
#         assert dirlen == 96
        

                    
#     def test_SpawnShell(self):
#             
#            
#         cmdAndArgs = os.path.join( settings.TESTDATA_DIR, 'test-20binsh.bot' )
#         
#         print cmdAndArgs
#         try:
#             p = Popen(cmdAndArgs)
#         except OSError as e:
#             print 'OSError: error is %s' % e.strerror
#             return
#         except ValueError as v:
#             print 'ValueError: error is %s' % v.strerror 
#             return
#         
#         t1 = time()
#         while True:
#             pstat = p.poll()
#             if pstat != None:
#                 break
#             print 'Still working...'
#             sleep(5)
#             
#         print 'Command Status = %d' % pstat
#         print 'Command took %d seconds' % (time() - t1)


        
    def test_PrintOutDirName(self):
        
        uuidp = uuid.uuid1()
        
        outdir = utils.get_OutputDir( 'output-%s' )
        
        print 'Output dir is %s ' % outdir
        
        assert utils.getCompleted('/etc/') == 97
        
    def createBot(self):
        
        
        cmd_template = \
        Template('${progpath} ${delay}  >>  ${outpath} ')
        cmd = ''        
        
        count = 20
        
        uuidp = uuid.uuid1()
        
        
        output_dir = utils.get_OutputDir( 'output-%s' )
        cmd_template = \
        Template('${progpath} ${delay} >> ${outpath} ')
                                
        cmd = ''
        
        for i in range(count):
            
            outpath = os.path.join( output_dir,   '%d.json' % i)

            cmd += cmd_template.substitute(
                                           progpath='/bin/sleep',
                                           delay='3 && echo hi',
                                   outpath=outpath) + '\n'
                                   
        return output_dir, cmd



                    
           
    def test_dictify(self):
        
        s = { 'a': 'a', 'b': 'b'}
        
        print 'Dictionary s:', s
        w = [ 'b', 'a']
        t = utils.dictify(s, w)
        
        assert len(t) == len(w) 
        print 'Results of dictify:',  t
        
        

    def list_services(self):
        
        print 'test_list_services'
        
        sid = 1 
        
        import json
        
        try:
            t = taskfarm.Client(False)
        except Exception as e:
            print e
            return

        print 'Now from the TaskFarm'
        
        services = t.callapi("list", True, {})
        
        for s in services:
            print s['sid'], s['name'], s['vmid']
            
        print '\n\n'
            
            
        services = [ {'name': s['name'], 'sid': s['sid']} for s in services]
        
        print 'iterate over services variable...'
        for s in services:
            pass
#            print 'Name: %s, SID: %s' % (s['name'], s['sid'])
            
        print '\n\n'
            
        try:
            data = t.info(sid)
        except AttributeError as e:
            print 'error (service might not be running): %s' % e
            return
        
#        print 'Data = %s' % data
        
        noCompletedTasks, noTotalTasks = utils.getTasks(t, sid)
        
        print 'noCompletedTasks = %d' % noCompletedTasks       
        
        print 'noTotalTasks = %d' % noTotalTasks
        
        i = 0
        while noCompletedTasks == 0:
            
            i += 1
            
            if i % 40 == 0:
                print '\n'
            sys.stdout.write('.')
            sleep(5)
            noCompletedTasks, noTotalTasks = utils.getTasks(t, sid)
            
        print '\nTasks are now running'  
          
        while noCompletedTasks < noTotalTasks:
            noCompletedTasks, noTotalTasks = utils.getTasks(t, sid)
            
            print '%d/%d'
        
        return
    
            
#         from cStringIO import StringIO
#         
#         import string
#         
#         old_stdout = sys.stdout
#         sys.stdout = mystdout = StringIO()
#         
#         t.info(1)
#         
#         sys.stdout = old_stdout
#         
#         raw = mystdout.getvalue()    
#         
#         print 'RAW\n\n\n%s\n\n\n'% raw
#         
#         jstring = string.replace(raw, '\n', '')
#         
#         try:
#             d = json.loads(jstring)
#         except ValueError as e:
#             print e
#             d = raw
#             
#         print 'Subnet: %s' % d['subnet']
#             
    def reassign_stdout(self):
        
        print 'test_reassign_stdout'
        
        from cStringIO import StringIO
        import sys
        
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        aString = 'A string'
        print 'aString = %s' % aString
        
        sys.stdout = old_stdout
        
        print 'mystdout.getValue() %s' %  mystdout.getvalue()
#        assert aString == mystdout.getvalue()            
            
            
    def getDriver(self):


        USERNAME = "oneadmin"
        PASSWORD = "oneadmin"
        HOST = "130.246.180.224"
        PORT = 4567
        SIZE = "small"
        IMAGE_ID = "9"
        
        Driver = get_driver(Provider.OPENNEBULA)


#         driver = libcloud.compute.drivers.opennebula.OpenNebula_3_2_NodeDriver
#         driver = libcloud.compute.drivers.opennebula.OpenNebula_2_0_NodeDriver
#         
#         print 'driver.secure: ' % driver.secure 

        
        
        conn = Driver(USERNAME, PASSWORD, secure=False,
                     host=HOST, port=PORT)
        if conn is None:
            print 'Connection is None'
            sys.exit(0)
        else:
            print conn

        
        
        class size:
            id = SIZE
            name = SIZE
        
        class image:
            id = IMAGE_ID
        
        print conn.list_nodes()
        print conn.create_node(image=image, size=size, name='conpaas')
        print conn.list_nodes()
