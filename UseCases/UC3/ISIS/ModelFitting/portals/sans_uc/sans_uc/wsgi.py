import os, sys
sys.path.append('/opt/sans_uc')
os.environ['DJANGO_SETTINGS_MODULE'] = 'sans_uc.settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()

