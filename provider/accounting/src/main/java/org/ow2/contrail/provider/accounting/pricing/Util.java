package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import java.util.*;

public class Util {
    static Logger logger = Logger.getLogger(Util.class);

    public static class PassThroughException extends Exception {
        public PassThroughException(String message) {
            super(message);
        }
    }


    public static boolean checkFields(JSONObject obj, String allowedFields[]) {
        List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(allowedFields));
        Collections.sort(sortedAllowedFields);

        Iterator<String> it = obj.keys();
        while (it.hasNext()) {
            String key = it.next();

            if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                System.out.println("ERROR IN KEY :: " + key);
                return false;
            }
        }

        return true;
    }


    public static double unitConvert(IPriceNode.TimeUnit unitName) {
        System.out.println("\t\t\tCONVERT :: " + unitName);
        switch (unitName) {
            case SECOND:
                return 1000;
            case MINUTE:
                return 60000;
            case HOUR:
                return 60 * (double) 60000;
            case DAY:
                return 24 * 60 * (double) 60000;
            case WEEK:
                return 7 * 24 * 60 * (double) 60000;
            case MONTH:
                return 30 * (double) 86400000;
            case YEAR:
                return 365 * (double) 86400000;
            default:
                return 1;
        }
    }

    public static double unitConvert(IPriceNode.SizeUnit unitName) {
        switch (unitName) {
            case BYTE:
                return 1;
            case KILOBYTE:
                return 1024;
            case MEGABYTE:
                return 1024 * 1024;
            case GIGABYTE:
                return 1024 * 1024 * 1024;
            default:
                return 1;
        }
    }

    public static TimeInterval intervalOverlap(Date start1, Date end1, Date start2, Date end2) {
        if ((start1.compareTo(end1) <= 0) && (start2.compareTo(end2) <= 0)) {
            TimeInterval t = new Util.TimeInterval();
            t.setStatus(false);
            return t;
        }
        else if ((start1.compareTo(end2) <= 0) && (start2.compareTo(end1) <= 0)) {
            TimeInterval t = new Util.TimeInterval();
            // the two intervals overlap
            if (start1.compareTo(start2) <= 0)
                t.setStart(start2);
            else
                t.setStart(start1);
            if (end1.compareTo(end2) <= 0)
                t.setEnd(end1);
            else
                t.setEnd(end2);

            t.setStatus(true);

            return t;
        }
        else {
            TimeInterval t = new Util.TimeInterval();
            t.setStatus(false);

            return t;
        }


    }

    //
    // TIME INTERVAL
    //
    public static class TimeInterval {
        private Date _start;
        private Date _end;

        private boolean _status;

        public TimeInterval() {
            _start = new Date();
            _end = new Date();
            _status = true;
        }

        public TimeInterval(Date d0, Date d1) {
            _start = d0;
            _end = d1;

            _status = true;
        }

        public void setStart(Date d0) {
            _start = d0;
        }

        public void setEnd(Date d1) {
            _end = d1;
        }

        public Date getStart() {
            return _start;
        }

        public Date getEnd() {
            return _end;
        }

        public void setStatus(boolean tf) {
            _status = tf;
        }

        public boolean getStatus() {
            return _status;
        }
    }

    //
    // JSON TIME COMPARATOR
    //
    public static class JSONTimeComparator implements Comparator<JSONObject> {
        @Override
        public int compare(JSONObject arg0, JSONObject arg1) {
            Date a0 = new Date();
            Date a1 = new Date();

            try {
                if (arg0.has("time"))
                    a0 = (Date) arg0.get("time");
                if (arg1.has("time"))
                    a1 = (Date) arg1.get("time");

                return a0.compareTo(a1);
            }
            catch (JSONException ex) {
                ex.printStackTrace();
            }

            return 0;
        }

    }

    //
    // PASS THROUGH FUNCTIONS
    //

    // PASS THROUGH || FROM DATE ||
    public static Object passThroughFromDate(Date fromDate, Date defFromDate) {
        if ((fromDate != null) && (defFromDate != null))
            if (fromDate.compareTo(defFromDate) <= 0)
                return defFromDate;
            else
                return fromDate;
        else if (fromDate != null)
            return fromDate;
        else if (defFromDate != null)
            return defFromDate;
        else
            return null;
    }


    // PASS THROUGH || TO DATE ||
    public static Object passThroughToDate(Date toDate, Date defToDate) {
        if ((toDate != null) && (defToDate != null))
            if (toDate.compareTo(defToDate) <= 0)
                return toDate;
            else
                return defToDate;
        else if (toDate != null)
            return toDate;
        else if (defToDate != null)
            return defToDate;
        else
            return null;
    }

    // PASS THROUGH || ENTITIES ||
    public static Object passThroughEnitites(ArrayList<String> entities, Object defEntities) throws PassThroughException {
        try {
            // if any entities are defined in the pricing model then we need to find the potential intersection with the entities defined in the query..
            if ((entities != null) && (entities.size() > 0)) {
                if (defEntities != null) {
                    // find intersection of both sets
                    JSONArray dentities = (JSONArray) defEntities;
                    HashSet<String> setDEntities = new HashSet<String>();
                    for (int i = 0; i < dentities.length(); i++) {
                        setDEntities.add(dentities.getString(i));
                    }

                    HashSet<String> setEntities = new HashSet<String>();
                    setEntities.addAll(entities);

                    // the actual intersection
                    setEntities.retainAll(setDEntities);

                    if (setEntities.size() == 0) {
                        // no common entities.. the pricing model is NOT defined for the entities defined in the query
                        throw new PassThroughException("The pricing model is not defined for entities specified in the query.");
                    }
                    else {
                        // there are some common entities between the pricing model and the query.. Pass there on to the pricing submodels
                        Iterator<String> it = setEntities.iterator();
                        JSONArray ientities = new JSONArray();
                        while (it.hasNext()) {
                            ientities.put(it.next());
                        }
                        return ientities;
                    }

                }
                else {
                    // use entities defined by the pricing model
                    JSONArray ientities = new JSONArray().put(entities);

                    for (int i = 0; i < entities.size(); i++)
                        ientities.put(entities.get(i));

                    return ientities;
                }
            }
            else // if no entities are defined in the pricing model
            {
                if (defEntities != null) {
                    return defEntities;
                }
                else {
                    // no entities defined.. the pricing model and the query defined for all possible entities -> defined just to cover all possibilities
                    return null;
                }
            }
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }

        throw new PassThroughException("Entities :: Undefined situation.");
    }

    // PASS THROUGH || RESOURCE ||
    public static Object passThroughMetric(String metric, Object defMetric) throws PassThroughException {
        try {
            logger.debug("PASS THROUGH METRIC :: METRIC 1 :: " + metric);
            logger.debug("PASS THROUGH METRIC :: METRIC 2 ::" + defMetric);

            if (defMetric != null) {
                if (metric != null) {
                    if (defMetric instanceof JSONArray) {
                        JSONArray defMetrics = (JSONArray) defMetric;

                        boolean isIncluded = false;
                        for (int i = 0; i < defMetrics.length(); i++) {
                            if (defMetrics.getString(i).compareTo(metric) == 0) {
                                isIncluded = true;
                                break;
                            }
                        }

                        // if the resource for which this pricing model is defined is not included in definitions then it is not relevant to the query -> return 0
                        if (isIncluded == false)
                            throw new PassThroughException("This pricing model is not defined for metrics specified in the query");
                        else
                            return metric;
                    }
                    else if (defMetric instanceof String) {
                        String defMetricS = (String) defMetric;

                        if (defMetricS != null) {
                            if (defMetricS.compareTo(metric) != 0)
                                throw new PassThroughException("This pricing model is not defined for metrics specified in the query");
                            else
                                return metric;
                        }
                        else
                            return metric;

                    }

                }
                else if (metric != null)
                    return metric;

            }
            else
                return metric;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;
        //throw new PassThroughException("Metric :: Undefined situation.");
    }

    // PASS THROUGH || RESOURCE ||
    public static Object passThroughResource(String resource, Object defResources) throws PassThroughException {
        try {
            if (resource == null)
                return defResources;

            if (defResources == null)
                return resource;

            if (defResources instanceof JSONArray) {
                JSONArray definitions = (JSONArray) defResources;

                boolean isIncluded = false;
                for (int i = 0; i < definitions.length(); i++) {
                    if (definitions.getString(i).compareTo(resource) == 0) {
                        isIncluded = true;
                        break;
                    }
                }
                // if the resource for which this pricing model is defined is not included in defintions then it is not relevant to the query -> return 0
                if (isIncluded == false) {
                    throw new PassThroughException("The pricing model is not defined for the resource specified ");
                }
                else
                    return resource;

            }
            else if (defResources instanceof String) {
                String defResource = (String) defResources;

                if (defResource.compareTo(resource) != 0) {
                    throw new PassThroughException("The pricing model is not defined for the resource specified ");
                }
                else
                    return resource;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static JSONObject copyJSONwithMetric(JSONObject obj, double metric) {
        JSONObject json = new JSONObject();
        try {
            Iterator it = obj.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                if (key.compareTo("metrics") == 0) {
                    JSONObject metrics = obj.getJSONObject("metrics");

                    Iterator mit = metrics.keys();

                    String mkey = (String) mit.next();

                    json.put("metrics", new JSONObject().put(mkey, metric));
                }
                else if (key.compareTo("time") == 0) {
                    json.put("time", DBConnector.formatter.format((Date) obj.get("time")));
                }
                else {
                    json.put(key, obj.get(key));
                }

            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return json;
    }

    public static String parseString(String key, JSONObject input) {
        try {
            if (input.has(key))
                return input.getString(key);
            else
                return null;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Date parseDate(String key, JSONObject input) {
        try {
            if (input.has(key)) {
                Object o = input.get(key);

                if (o instanceof Date) {
                    Date d = (Date) o;

                    return d;
                }
                else {
                    String def = input.getString(key);

                    if (def.length() > 0)
                        return (Date) DBConnector.formatter.parse(input.getString(key));
                    else
                        return null;
                }
            }
            else
                return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
