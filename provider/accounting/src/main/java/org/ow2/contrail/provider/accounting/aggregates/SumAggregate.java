package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

public class SumAggregate implements IAggregate {
    private double sum;

    public SumAggregate() {
        sum = 0;
    }

    @Override
    public void addValue(DateValue dv) {
        sum = sum + (Double) dv.value();
    }

    @Override
    public JSONObject getValue() {
        try {
            return new JSONObject().put("sum", sum);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
