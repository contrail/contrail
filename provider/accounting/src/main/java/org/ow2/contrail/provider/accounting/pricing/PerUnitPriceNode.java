package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import java.util.*;

public class PerUnitPriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(PerUnitPriceNode.class);

    private static PriceNodeType _type = PriceNodeType.PER_UNIT;
    private static String[] _allowedFields = {"type", "fromDate", "toDate", "entities", "unit", "unitCost"};
    // "unit" == {"timeUnit":timeunit,"resourceUnit":resourceunit}

    private Date _fromDate;
    private Date _toDate;
    private double _resourceUnit;
    private double _timeUnit;
    private double _unitCost;
    private ArrayList<String> _entities;

    public String parse(JSONObject input) {
        logger.debug("PER UNIT NODE :: Parsing string input ... ");
        logger.debug(input);

        if (!Util.checkFields(input, _allowedFields)) {
            logger.debug("PER UNIT NODE :: Node structure contains fields that are not allowed.");

            return "{error:price node structure contains fields that are not allowed.}";
        }

        try {
            // if specified store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("PER UNIT NODE :: _fromDate = " + _fromDate);

            // if specified store the "to date"
            _toDate = Util.parseDate("toDate", input);
            logger.debug("PER UNIT NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("PER UNIT NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }

            // unit needs to be defined for the metrics..
            if (input.has("unit")) {
                JSONObject unit = input.getJSONObject("unit");

                _timeUnit = Util.unitConvert(IPriceNode.TimeUnit.valueOf(unit.getString("timeUnit")));

                _resourceUnit = Util.unitConvert(IPriceNode.SizeUnit.valueOf(unit.getString("resourceUnit")));
            }
            else {
                _timeUnit = 1;
                _resourceUnit = 1;
            }
            logger.debug("PER UNIT NODE :: _timeUnit = " + _timeUnit + " _resourceUnit = " + _resourceUnit);

            // unit price needs to be defined!
            if (input.has("unitCost"))
                _unitCost = input.getDouble("unitCost");
            else
                _unitCost = Double.NaN;
            logger.debug("PER UNIT NODE :: _unitCost = " + _unitCost);

            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("PER UNIT NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);

                    logger.debug("PER UNIT NODE :: _entities[0]=" + (String) o);
                }

            }
            else {
                _entities = null;
                logger.debug("PER UNIT NODE :: _entities=null");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("PER UNIT NODE :: An exception was raised :: " + e.getLocalizedMessage());

            return "{error:An exception has been raised during the parsing of a per-unit price node.}";
        }

        return "{status:OK}";
    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        logger.debug("PER UNIT NODE :: Starting price calculation \n \t Price query " + definitions);

        // if no input data, then we return 0.s
        if (input.length() == 0) {
            logger.warn("PER UNIT NODE :: No documents received from the database.");
            return 0;
        }

        try {
            // copy data into an array list
            ArrayList<JSONObject> inputArray = new ArrayList<JSONObject>();

            logger.debug("PER UNIT NODE :: Number of documents received from the database :: " + input.length());

            for (int i = 0; i < input.length(); i++) {
                JSONObject o = input.getJSONObject(i);

                boolean add = true;
                // check if the document's time falls within the range defined by the from and to dates
                if ((_fromDate != null) && (o.has("time"))) {
                    Date time = (Date) o.get("time");

                    if (time.compareTo(_fromDate) < 0)
                        add = false;
                }
                if ((_toDate != null) && (o.has("time"))) {
                    Date time = (Date) o.get("time");

                    if (time.compareTo(_toDate) > 0)
                        add = false;
                }

                // check if any of the document's entities matches any of the entities defined by the node/pricing model
                if ((_entities != null) && (o.has("entities"))) {
                    HashSet<String> setEntities = new HashSet<String>();
                    setEntities.addAll(_entities);

                    Object objEntities = o.get("entities");

                    if (objEntities instanceof String) {
                        if (!_entities.contains((String) objEntities))
                            add = false;
                    }
                    else if (objEntities instanceof JSONArray) {
                        JSONArray dentities = (JSONArray) objEntities;
                        HashSet<String> setDEntities = new HashSet<String>();
                        for (int j = 0; j < dentities.length(); j++) {
                            setDEntities.add(dentities.getString(j));
                        }

                        // the actual intersection
                        setDEntities.retainAll(setEntities);

                        if (setDEntities.size() == 0) {
                            add = false;
                        }
                    }
                }

                if (add)
                    inputArray.add(o);

            }

            logger.debug("PER UNIT NODE :: Number of documents satisfying all criteria :: " + inputArray.size());

            if (inputArray.size() < 2) {
                logger.warn("PER UNIT NODE :: No document matches all criteria! [Check the monitoring frequency toleracne.]");
                return 0;
            }

            // sort data time-wise
            logger.debug("PER UNIT NODE :: Sorting documents ...");
            Collections.sort(inputArray, new Util.JSONTimeComparator());

            logger.debug("PER UNIT NODE :: Calculating the up-time and prices");
            double cost = 0;
            for (int i = 1; i < inputArray.size(); i++) {
                //logger.debug("PER UNIT NODE :: array object :: " + inputArray.get(i).toString());
                Date dFirst = (Date) inputArray.get(i - 1).get("time");
                Date dLast = (Date) inputArray.get(i).get("time");

                double timeDiff = dLast.getTime() - dFirst.getTime();

                double mFirst, mLast;
                if (definitions.has("metrics")) {
                    String metric = definitions.getString("metrics");

                    mFirst = inputArray.get(i - 1).getJSONObject("metrics").getDouble(metric);
                    mLast = inputArray.get(i).getJSONObject("metrics").getDouble(metric);
                }
                else {
                    Iterator it1 = inputArray.get(i - 1).getJSONObject("metrics").keys();
                    Iterator it2 = inputArray.get(i).getJSONObject("metrics").keys();

                    String k1 = (String) it1.next();
                    String k2 = (String) it2.next();

                    mFirst = inputArray.get(i - 1).getJSONObject("metrics").getDouble(k1);
                    mLast = inputArray.get(i).getJSONObject("metrics").getDouble(k2);
                }

                double avgMetric = 0.5 * (mFirst + mLast);

                if (timeDiff < IPriceNode.MONITORING_FREQUENCY_TOLERANCE) {
                    cost += _unitCost * (avgMetric / _resourceUnit) * (timeDiff / _timeUnit);
                }
            }

            logger.debug("PER UNIT NODE :: PRICE :: " + cost);
            return cost;
        }
        catch (JSONException ex) {
            logger.warn("PER UNIT NODE :: An exception was raised :: " + ex.getLocalizedMessage());
            return 0;
        }
    }

    @Override
    public PriceNodeType getType() {
        return _type;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }

    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        // TODO Auto-generated method stub
        return 0;
    }


    public JSONObject toJSON() {
        JSONObject output = new JSONObject();
        //{"type", "fromDate", "toDate", "entities", "unit", "unitCost"};

        try {
            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            output.put("type", _type);

            if ((_timeUnit != Double.NaN) && (_resourceUnit != Double.NaN)) {
                output.put("unit", new JSONObject().put("timeUnit", _timeUnit).put("resourceUnit", _resourceUnit));
            }

            if (_unitCost != Double.NaN)
                output.put("unitCost", _unitCost);

            if (_entities != null) {
                JSONArray entities = new JSONArray();
                for (int i = 0; i < _entities.size(); i++) {
                    entities.put(_entities.get(i));
                }
                output.put("entities", entities);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
}
