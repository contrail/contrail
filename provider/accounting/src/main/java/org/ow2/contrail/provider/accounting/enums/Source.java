package org.ow2.contrail.provider.accounting.enums;

public enum Source {
    HOST,
    VM;

    public static Source fromString(String text) throws Exception {
        try {
            return Source.valueOf(text.toUpperCase());
        }
        catch (Exception e) {
            throw new Exception("Invalid Source value: " + text);
        }
    }
}
