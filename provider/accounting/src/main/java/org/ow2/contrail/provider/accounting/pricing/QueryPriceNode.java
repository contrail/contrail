package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import java.util.*;

public class QueryPriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(QueryPriceNode.class);

    private static PriceNodeType _type = PriceNodeType.QUERY;
    private static String[] _allowedFields = {"type", "fromDate", "toDate", "entities", "resource", "metrics", "submodel", "$parent"};

    private Date _fromDate;
    private Date _toDate;

    private ArrayList<String> _entities;

    private String _resource;
    private String _metric;

    // only for use in conditional price node to determine which of the query nodes is the parent
    private boolean _parent;

    private ArrayList<IPriceNode> _childNodes;

    public QueryPriceNode() {
        _fromDate = null;
        _toDate = null;
        _entities = null;
        _resource = null;
        _metric = null;
        _parent = false;
    }

    @Override
    public String parse(JSONObject input) {
        logger.debug("QUERY NODE :: Parsing string input ... ");
        logger.debug(input);

        if (!Util.checkFields(input, _allowedFields)) {
            logger.debug("QUERY NODE :: Node structure contains fields that are not allowed.");
            return "{error:query node structure contains fields that are not allowed.}";
        }

        try {
            // if specified, store the "$parent"
            if (input.has("$parent"))
                _parent = true;
            else
                _parent = false;

            // if specified, store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("QUERY NODE :: _fromDate = " + _fromDate);

            _toDate = Util.parseDate("toDate", input);
            logger.debug("QUERY NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("QUERY NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }

            // if specified, store the "resource"
            _resource = Util.parseString("resource", input);
            logger.debug("QUERY NODE :: _resource = " + _resource);

            // if specified, store the "metrics"
            _metric = Util.parseString("metrics", input);
            logger.debug("QUERY NODE :: _metric = " + _metric);

            // if specified, store the "entities"
            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("QUERY NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);

                    logger.debug("QUERY NODE :: _entities[0]=" + (String) o);
                }

            }
            else {
                _entities = null;
                logger.debug("QUERY NODE :: _entities=null");
            }

            if (input.has("submodel")) {
                logger.debug("QUERY NODE :: Parsing child nodes.");

                // initialize child nodes
                _childNodes = new ArrayList<IPriceNode>();

                JSONArray submodel = input.getJSONArray("submodel");

                String returnMessage = "";
                for (int i = 0; i < submodel.length(); i++) {
                    JSONObject o = (JSONObject) submodel.get(i);

                    if (o.has("type")) {
                        switch (IPriceNode.PriceNodeType.valueOf(o.getString("type"))) {
                            case FIXED_PRICE: {
                                IPriceNode p = new FlatRatePriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            case PER_UNIT: {
                                IPriceNode p = new PerUnitPriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            case PER_USE: {
                                IPriceNode p = new PerUsePriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            default:
                                //unknown pricing type
                                logger.error("QUERY NODE :: Unknown pricing type." + IPriceNode.PriceNodeType.valueOf(o.getString("type")));
                                return "{error:Unknown or incorrect node type.}";
                        }
                    }
                    else {
                        // invalid structure
                        logger.error("QUERY NODE :: Unknown child node type :: " + o.getString("type"));
                        return "{error: QUERY NODE, unknown submodel node.}";
                    }
                }
            }
            else {
                logger.warn("QUERY NODE :: this query node has no children.");

                _childNodes = null;
                return "{error:Empty pricing model}";
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("QUERY NODE :: An exception was raised :: " + e.toString());

            return "{error:An exception has been raised during the parsing of a query node.}";
        }

        return "{status:OK}";
    }

    public JSONArray getResults(DBConnector db, JSONObject definitions) {
        logger.debug("QUERY NODE :: Starting query execution \n \t Price query " + definitions);

        try {
            Date date1 = Util.parseDate("fromDate", definitions);
            Date ifromDate = (Date) Util.passThroughFromDate(_fromDate, date1);
            logger.debug("QUERY NODE :: fromDate resolution => " + date1 + " & " + _fromDate + " -> " + ifromDate);

            Date date2 = Util.parseDate("toDate", definitions);
            Date itoDate = (Date) Util.passThroughToDate(_toDate, date2);
            logger.debug("QUERY NODE :: toDate resolution => " + date2 + " & " + _toDate + " -> " + itoDate);

            // check if the from and to date range is valid -> if not return 0
            if ((itoDate != null) && (ifromDate != null) && (ifromDate.compareTo(itoDate) > 0)) {
                logger.error("QUERY NODE :: fromDate larger than toDate.");
                return null;
            }

            Object resource = Util.passThroughResource(_resource, definitions.opt("resource"));
            logger.debug("QUERY NODE :: resource resolution => " + definitions.opt("resource") + " & " + _resource + " -> " + resource);

            Object metric = Util.passThroughMetric(_metric, definitions.opt("metrics"));
            logger.debug("QUERY NODE :: metric resolution => " + definitions.opt("metrics") + " & " + _metric + " -> " + metric);

            Object entities = Util.passThroughEnitites(_entities, definitions.opt("entities"));
            logger.debug("QUERY NODE :: entities resolution => " + definitions.opt("entities") + " & " + _entities + " -> " + entities);

            // Construct the query
            logger.debug("QUERY NODE :: Query construction started ... ");
            JSONObject query = new JSONObject();
            JSONObject returnFields = new JSONObject();


            if (metric != null) {
                returnFields.put("time", 1);
                returnFields.put("metrics." + metric, 1);
            }

            if ((ifromDate != null) || (itoDate != null)) {
                JSONObject dateRange = new JSONObject();

                if (ifromDate != null)
                    dateRange.put("$gt", ifromDate);

                if (itoDate != null)
                    dateRange.put("$lt", itoDate);

                query.put("time", dateRange);
            }

            if (resource != null)
                query.put("group", resource);

            if (entities != null) {
                if (entities instanceof String)
                    query.put("sid", entities);
                else if (entities instanceof JSONArray) {
                    query.put("sid", new JSONObject().put("$in", entities));
                }
            }

            List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(_allowedFields));
            Collections.sort(sortedAllowedFields);

            Iterator it = definitions.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                // check if the key is one of the allowed keys
                //	- if yes, then it has already been taken care of in the above
                // 	- if no, then pass it to the children
                if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                    query.put(key, definitions.get(key));
                }
            }

            logger.debug("QUERY NODE :: Query constructed :: " + query + " with return fields " + returnFields);
            logger.debug("QUERY NODE :: Executing query");

            JSONArray queryResults;
            if (returnFields.length() > 0)
                queryResults = db.queryDirect(query, returnFields);
            else
                queryResults = db.queryDirect(query, null);

            logger.debug("QUERY NODE :: Received " + queryResults.length() + " results.");

            return queryResults;

        }
        catch (Exception e) {
            logger.error("QUERY NODE :: An exception has been raised :: " + e.toString());
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        logger.debug("QUERY NODE :: Starting price calculation \n \t Price query " + definitions);

        try {
            Date date1 = Util.parseDate("fromDate", definitions);
            Date ifromDate = (Date) Util.passThroughFromDate(_fromDate, date1);
            logger.debug("QUERY NODE :: fromDate resolution => " + date1 + " & " + _fromDate + " -> " + ifromDate);

            Date date2 = Util.parseDate("toDate", definitions);
            Date itoDate = (Date) Util.passThroughToDate(_toDate, date2);
            logger.debug("QUERY NODE :: toDate resolution => " + date2 + " & " + _toDate + " -> " + itoDate);

            // check if the from and to date range is valid -> if not return 0
            if ((itoDate != null) && (ifromDate != null) && (ifromDate.compareTo(itoDate) > 0)) {
                logger.error("QUERY NODE :: fromDate larger than toDate.");
                return 0;
            }

            Object resource = Util.passThroughResource(_resource, definitions.opt("resource"));
            logger.debug("QUERY NODE :: resource resolution => " + definitions.opt("resource") + " & " + _resource + " -> " + resource);

            Object metric = Util.passThroughMetric(_metric, definitions.opt("metrics"));
            logger.debug("QUERY NODE :: metric resolution => " + definitions.opt("metrics") + " & " + _metric + " -> " + metric);

            Object entities = Util.passThroughEnitites(_entities, definitions.opt("entities"));
            logger.debug("QUERY NODE :: entities resolution => " + definitions.opt("entities") + " & " + _entities + " -> " + entities);

            // Construct the query
            logger.debug("QUERY NODE :: Query construction started ... ");
            JSONObject query = new JSONObject();
            JSONObject returnFields = new JSONObject();


            if (metric != null) {
                returnFields.put("time", 1);
                returnFields.put("metrics." + metric, 1);
            }

            if ((ifromDate != null) || (itoDate != null)) {
                JSONObject dateRange = new JSONObject();

                if (ifromDate != null)
                    dateRange.put("$gt", ifromDate);

                if (itoDate != null)
                    dateRange.put("$lt", itoDate);

                query.put("time", dateRange);
            }

            if (resource != null)
                query.put("group", resource);

            if (entities != null) {
                if (entities instanceof String)
                    query.put("sid", entities);
                else if (entities instanceof JSONArray) {
                    query.put("sid", new JSONObject().put("$in", entities));
                }
            }

            List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(_allowedFields));
            Collections.sort(sortedAllowedFields);

            Iterator it = definitions.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                // check if the key is one of the allowed keys
                //	- if yes, then it has already been taken care of in the above
                // 	- if no, then pass it to the children
                if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                    query.put(key, definitions.get(key));
                }
            }

            logger.debug("QUERY NODE :: Query constructed :: " + query + " with return fields " + returnFields);
            logger.debug("QUERY NODE :: Executing query");

            JSONArray queryResults;
            if (returnFields.length() > 0)
                queryResults = db.queryDirect(query, returnFields);
            else
                queryResults = db.queryDirect(query, null);

            logger.debug("QUERY NODE :: Received " + queryResults.length() + " results.");

            logger.debug("QUERY NODE :: Passing results to children ...");
            double price = 0;
            for (int i = 0; i < _childNodes.size(); i++) {
                price += _childNodes.get(i).getPrice(queryResults, definitions);
            }

            logger.debug("QUERY NODE :: PRICE :: " + price);
            return price;

        }
        catch (Exception e) {
            logger.error("QUERY NODE :: An exception has been raised :: " + e.toString());
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        return 0;
    }

    @Override
    public PriceNodeType getType() {
        return IPriceNode.PriceNodeType.QUERY;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }

    public boolean isParent() {
        return _parent;
    }

    public void setParent() {
        _parent = true;
    }

    public JSONObject toJSON() {
        //{"type", "fromDate", "toDate", "entities", "resource", "metrics", "submodel", "$parent"};
        JSONObject output = new JSONObject();

        try {
            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            if (_metric != null)
                output.put("metrics", _metric);

            if (_resource != null)
                output.put("resource", _resource);

            output.put("type", _type);

            if (_entities != null) {
                JSONArray entities = new JSONArray();
                for (int i = 0; i < _entities.size(); i++) {
                    entities.put(_entities.get(i));
                }
                output.put("entities", entities);
            }

            if (this._childNodes != null) {
                JSONArray children = new JSONArray();
                for (int i = 0; i < _childNodes.size(); i++) {
                    children.put(_childNodes.get(i).toJSON());
                }
                output.put("submodel", children);
            }
            else
                System.out.println("\tQUERY NODE:: no submodel defined.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
}
