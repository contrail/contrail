package org.ow2.contrail.provider.accounting.billing;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.MongoDBConnector;
import org.ow2.contrail.provider.accounting.db.StorageManagerConnector;
import org.ow2.contrail.provider.accounting.pricing.IPriceNode;
import org.ow2.contrail.provider.accounting.pricing.RootPriceNode;

public class Billing {
    private static Logger log = Logger.getLogger(Billing.class);

    public String calculatePrice(JSONObject model, JSONObject query) throws Exception {
        log.trace("calculatePrice() started.");
        IPriceNode pricingModel = new RootPriceNode();

        String returnValue = pricingModel.parse(model);
        if (returnValue.compareTo("{status:OK}") != 0) {
            System.out.println("Error parsing the tree");
            return returnValue;
        }

        /*MongoDBConnector db = new MongoDBConnector();
        db.connect("localhost", "monitoring-billing-test", 27017);
        db.useCollection("rawData");*/

        StorageManagerConnector db = new StorageManagerConnector();

        System.out.println("Calculating price...");
        double price = pricingModel.getPrice(db, query);
        System.out.println("COMPUTED PRICE :: " + price);

        log.trace("calculatePrice() finished successfully.");
        return "{price:" + price + "}";
    }
}
