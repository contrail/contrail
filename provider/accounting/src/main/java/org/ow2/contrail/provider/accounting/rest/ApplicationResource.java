package org.ow2.contrail.provider.accounting.rest;

import com.mongodb.DB;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;
import org.ow2.contrail.provider.accounting.utils.OAuthUtils;
import org.ow2.contrail.provider.storagemanager.DataRetriever;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/applications/{appUuid}")
public class ApplicationResource {

    private String appUuid;

    @Context
    private HttpServletRequest httpRequest;

    public ApplicationResource(@PathParam("appUuid") String appUuid) {
        this.appUuid = appUuid;
    }

    @GET
    @Path("/events")
    @Produces("application/json")
    public Response getMonitoringEvents() throws IOException, JSONException {
        DB db = MongoDBConnection.getDB();
        DataRetriever dataRetriever = new DataRetriever(db);
        String userUuid = OAuthUtils.getOwnerUuid(httpRequest);
        JSONArray events = dataRetriever.getEvents(appUuid, userUuid);
        return Response.ok(events.toString()).build();
    }
}
