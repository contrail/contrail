package org.ow2.contrail.provider.accounting.db;

import com.mongodb.*;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.aggregates.*;

import java.net.UnknownHostException;
import java.util.*;

public class MongoDBConnector implements DBConnector {
    private static Logger log = Logger.getLogger(MongoDBConnector.class);

    private Mongo mongoDatabase;
    private DB database;
    private DBCollection collection;


    public void connect(String host, String db, int port) throws UnknownHostException, MongoException {
        mongoDatabase = new Mongo(host, port);

        database = mongoDatabase.getDB(db);
    }

    public void disconnect() {
        collection = null;

        database = null;

        mongoDatabase.close();
    }

    public void write(JSONObject obj) {
        Object o = com.mongodb.util.JSON.parse(obj.toString());
        DBObject dbObj = (DBObject) o;
        System.out.println(collection.count());
        collection.insert(dbObj);
    }

    public void write(DBObject obj) {
        collection.insert(obj);
    }

    public void useCollection(String col) {
        if (database.collectionExists(col))
            collection = database.getCollection(col);
        else
            collection = database.createCollection(col, null);
    }

    public List<String> listCollections() {
        Set<String> collections = database.getCollectionNames();
        ArrayList<String> cnames = new ArrayList<String>();

        Iterator<String> it = collections.iterator();

        while (it.hasNext()) {
            String cname = it.next();
            if (cname.compareTo("system.indexes") != 0)
                cnames.add(cname);
        }
        return cnames;
    }

    @Override
    public JSONObject query(JSONObject request) {
        if (log.isTraceEnabled()) {
            log.trace("query() started.");
            log.trace("Query to execute: " + request.toString());
        }
        JSONObject returnValue = new JSONObject();
        try {
            if (!request.has("resource"))
                return new JSONObject().put("Error", "Resource not specified").put("Request", request.toString());

            String resource = request.getString("resource");


            DBCursor cursor = null;

            BasicDBObject query = new BasicDBObject();
            query.put("group", resource);

            if (request.has("filter")) {
                JSONArray inputFilter = request.getJSONArray("filter");


                for (int i = 0; i < inputFilter.length(); i++) {
                    JSONObject f = inputFilter.getJSONObject(i);
                    BasicDBObject filter = new BasicDBObject();

                    if (!f.has("field")) {
                        return new JSONObject().put("Error", "Query filter must contain a field specification.");
                    }
                    String field = f.getString("field");

                    if (!f.has("conditions")) {
                        return new JSONObject().put("Error", "Query filter must contain a field-specific conditions.");
                    }

                    Object conditionObject = f.get("conditions");
                    if (conditionObject instanceof JSONArray) {
                        JSONArray conditions = f.getJSONArray("conditions");
                        for (int j = 0; j < conditions.length(); j++) {
                            JSONObject c = conditions.getJSONObject(j);
                            Object data = null;
                            if (field.compareTo("time") != 0)
                                data = c.get("limit");
                            else {
                                System.out.println("Date time found!");
                                data = (Date) formatter.parse((String) c.get("limit"));
                                System.out.println((Date) data);
                            }

                            if (c.getString("operator").compareTo("gt") == 0) {
                                filter.put("$gt", data);
                            }
                            if (c.getString("operator").compareTo("lt") == 0) {
                                filter.put("$lt", data);
                            }
                            if (c.getString("operator").compareTo("lte") == 0) {
                                filter.put("$lte", data);
                            }
                            if (c.getString("operator").compareTo("gte") == 0) {
                                filter.put("$gte", data);
                            }
                        }
                        query.put(field, filter);
                    }
                    else if (conditionObject instanceof String) {
                        query.put(field, conditionObject);
                    }

                }
            }

            cursor = collection.find(query);

            JSONObject returnField = null;
            String returnString = null;
            IAggregate returnAggregate = null;

            if (request.has("return")) {
                returnField = request.getJSONObject("return");

                Iterator it = returnField.keys();
                while (it.hasNext()) {
                    returnString = (String) it.next();
                    String value = returnField.getString(returnString);

                    if (value.compareTo("sum") == 0) {
                        returnAggregate = new SumAggregate();
                    }
                    if (value.compareTo("list") == 0) {
                        returnAggregate = new ListAggregate();
                    }
                    if (value.compareTo("mean") == 0) {
                        returnAggregate = new MeanAggregate();
                    }
                    if (value.compareTo("stddev") == 0) {
                        returnAggregate = new StdDevAggregate();
                    }
                    if (value.compareTo("max") == 0) {
                        returnAggregate = new MaxAggregate();
                    }
                    if (value.compareTo("min") == 0) {
                        returnAggregate = new MinAggregate();
                    }
                    if (value.compareTo("availability") == 0) {
                        returnAggregate = new AvailabilityAggregate();
                    }
                }

                if (returnAggregate == null)
                    returnAggregate = new ListAggregate();

            }
            else {
                return new JSONObject().put("Error", "Must refine a return value");
            }

            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                JSONObject json = new JSONObject();

                Date d = (Date) o.get("time");

                JSONObject entry = new JSONObject((String) (o.get("metrics").toString()));

                Iterator it = entry.keys();

                while (it.hasNext()) {
                    String key = (String) it.next();
                    Object value = entry.get(key);

                    if (returnField != null) {
                        if ((returnString != null) && (returnString.compareTo(key) == 0)) {
                            if (returnAggregate != null) {
                                returnAggregate.addValue(new DateValue(d, value));
                            }
                        }
                    }
                }
            }

            if (returnAggregate != null)
                returnValue.put("value", returnAggregate.getValue());
            else
                returnValue.put("value", "null");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        log.trace("query() finished successfully.");
        return returnValue;

    }

    @Override
    public JSONArray queryDirect(JSONObject query, JSONObject fields) {
        if (log.isTraceEnabled()) {
            log.trace("query() started.");
            log.trace("Query to execute: " + query.toString() + "  Fields: " + fields.toString());
        }

        if (fields != null) {
            DBCursor cursor = collection.find(Util.encode(query), Util.encode(fields));
            JSONArray result = new JSONArray();
            while (cursor.hasNext()) {
                result.put(Util.encode(cursor.next()));
            }
            log.trace("query() finished successfully.");
            return result;
        }
        else {
            DBCursor cursor = collection.find(Util.encode(query));

            JSONArray result = new JSONArray();
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                result.put(Util.encode(o));
            }
            log.trace("query() finished successfully.");
            return result;
        }
    }

    public DBCursor queryRaw() {
        return collection.find();
    }
}
