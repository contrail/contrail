package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;
import org.ow2.contrail.provider.accounting.pricing.Util.PassThroughException;

import java.util.*;

public class PriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(PriceNode.class);

    private static String[] _allowedFields = {"type", "fromDate", "toDate", "entities", "resource", "metrics", "submodel"};

    private static PriceNodeType _type = PriceNodeType.PRICE;
    private Date _fromDate;
    private Date _toDate;
    private String _resource;
    private String _metric;
    private ArrayList<String> _entities;

    private ArrayList<IPriceNode> _childNodes;

    // db.data.find({"sid":/^[\.-]*ENTITY_ID[\.-]/})

    public PriceNode() {
        _fromDate = null;
        _toDate = null;
        _resource = null;
        _metric = null;
        _childNodes = null;
        _entities = null;
    }

    //
    // PARSE THE PRICING MODEL
    //
    @Override
    public String parse(JSONObject input) {
        logger.debug("PRICE NODE :: Parsing string input ... ");
        logger.debug(input);

        if (!Util.checkFields(input, _allowedFields)) {
            logger.debug("PRICE NODE :: Node structure contains fields that are not allowed.");

            return "{error:price node structure contains fields that are not allowed.}";
        }

        try {
            // if specified store the "resource"
            _resource = Util.parseString("resource", input);
            logger.debug("PRICE NODE :: _resource = " + _resource);


            // if specified store the "from date"
            _metric = Util.parseString("metrics", input);
            logger.debug("PRICE NODE :: _metric = " + _metric);

            // if specified store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("PRICE NODE :: _fromDate = " + _fromDate);

            // if specified store the "to date"
            _toDate = Util.parseDate("toDate", input);
            logger.debug("PRICE NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("PRICE NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }

            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("PRICE NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);

                    logger.debug("PRICE NODE :: _entities[0]=" + (String) o);
                }

            }
            else {
                _entities = null;
                logger.debug("PRICE NODE :: _entities=null");
            }

            if (input.has("submodel")) {
                logger.debug("PRICE NODE :: Parsing child nodes.");

                // initialize child nodes
                _childNodes = new ArrayList<IPriceNode>();

                JSONArray submodel = input.getJSONArray("submodel");

                for (int i = 0; i < submodel.length(); i++) {
                    JSONObject o = (JSONObject) submodel.get(i);

                    if (o.has("type")) {
                        switch (IPriceNode.PriceNodeType.valueOf(o.getString("type"))) {
                            case PRICE: {
                                IPriceNode p = new PriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            case QUERY: {
                                IPriceNode p = new QueryPriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            case CONDITIONAL: {
                                IPriceNode p = new ConditionalPriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;

                            default:
                                //unknown pricing type
                                logger.error("PRICE NODE :: Unknown pricing type." + IPriceNode.PriceNodeType.valueOf(o.getString("type")));
                                return "{error:Unknown or incorrect node type.}";
                        }
                    }
                    else {
                        // invalid structure
                        logger.error("PRICE NODE :: Unknown child node type :: " + o.getString("type"));

                        // invalid structure
                        return "{error:Invalid pricing model structure.}";
                    }
                }
            }
            else {
                logger.error("PRICE NODE :: this price node has no children.");

                _childNodes = null;
                return "{error:Empty pricing model}";
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("PRICE NODE :: An exception was raised :: " + e.toString());

            return "{error:An exception has been raised during the parsing of a price node.}";
        }

        return "{status:OK}";
    }

    //
    // GET THE PRICE FROM THE PRICING MODEL
    //
    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        JSONObject new_definitions = new JSONObject();

        logger.debug("PRICE NODE :: Starting price calculation \n \t Price query " + definitions);


        try {
            Date date1 = Util.parseDate("fromDate", definitions);
            Date ifromDate = (Date) Util.passThroughFromDate(_fromDate, date1);
            logger.debug("PRICE NODE :: fromDate resolution => " + date1 + " & " + _fromDate + " -> " + ifromDate);

            Date date2 = Util.parseDate("toDate", definitions);
            Date itoDate = (Date) Util.passThroughToDate(_toDate, date2);
            logger.debug("PRICE NODE :: toDate resolution => " + date2 + " & " + _toDate + " -> " + itoDate);

            // check if the from and to date range is valid -> if not return 0
            if ((itoDate != null) && (ifromDate != null) && (ifromDate.compareTo(itoDate) > 0)) {
                logger.error("PRICE NODE :: fromDate larger than toDate.");
                return 0;
            }

            Object resource = Util.passThroughResource(_resource, definitions.opt("resource"));
            logger.debug("PRICE NODE :: resource resolution => " + definitions.opt("resource") + " & " + _resource + " -> " + resource);

            Object metric = Util.passThroughMetric(_metric, definitions.opt("metrics"));
            logger.debug("PRICE NODE :: metric resolution => " + definitions.opt("metrics") + " & " + _metric + " -> " + metric);

            Object entities = Util.passThroughEnitites(_entities, definitions.opt("entities"));
            logger.debug("PRICE NODE :: entities resolution => " + definitions.opt("entities") + " & " + _entities + " -> " + entities);

            // now we neatly package everything into new_definitions and send it to children, collect the prices and sum the results.
            if (ifromDate != null)
                new_definitions.put("fromDate", ifromDate);

            if (itoDate != null)
                new_definitions.put("toDate", itoDate);

            if (resource != null)
                new_definitions.put("resource", resource);

            if (metric != null)
                new_definitions.put("metrics", metric);

            if (entities != null)
                new_definitions.put("entities", entities);

            List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(_allowedFields));
            Collections.sort(sortedAllowedFields);

            Iterator it = definitions.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                // check if the key is one of the allowed keys
                //	- if yes, then it has already been taken care of in the above
                // 	- if no, then pass it to the children
                if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                    new_definitions.put(key, definitions.get(key));
                }
            }

            logger.debug("PRICE NODE :: pass to children => " + new_definitions);

            // right now.. the new definitions are ready! GO!
            double price = 0;
            for (int i = 0; i < _childNodes.size(); i++) {
                price += _childNodes.get(i).getPrice(db, new_definitions);
            }

            logger.debug("PRICE NODE :: PRICE :: " + price);

            return price;
        }
        catch (JSONException ex) {
            logger.error("PRICE NODE :: An exception has been raised :: " + ex.getLocalizedMessage());
            ex.printStackTrace();
            return 0;
        }
        catch (PassThroughException e) {
            logger.error("PRICE NODE :: An exception has been raised :: " + e.getLocalizedMessage());
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public PriceNodeType getType() {
        return _type;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        // TODO Auto-generated method stub
        return 0;
    }

    public JSONObject toJSON() {
        JSONObject output = new JSONObject();

        try {
            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            if (_metric != null)
                output.put("metrics", _metric);

            if (_resource != null)
                output.put("resource", _resource);

            output.put("type", _type);

            if (_entities != null) {
                JSONArray entities = new JSONArray();
                for (int i = 0; i < _entities.size(); i++) {
                    entities.put(_entities.get(i));
                }
                output.put("entities", entities);
            }

            if (this._childNodes != null) {
                JSONArray children = new JSONArray();
                for (int i = 0; i < _childNodes.size(); i++) {
                    children.put(_childNodes.get(i).toJSON());
                }
                output.put("submodel", children);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }

}
