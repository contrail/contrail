package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

public class MeanAggregate implements IAggregate {
    private double mean;
    private int numberOfSamples;

    public MeanAggregate() {
        mean = 0;
        numberOfSamples = 0;
    }

    @Override
    public void addValue(DateValue dv) {
        mean = (mean * numberOfSamples + (Double) dv.value()) / (numberOfSamples + 1);
        numberOfSamples++;
    }

    @Override
    public JSONObject getValue() {
        try {
            return new JSONObject().put("mean", mean);
        }
        catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
