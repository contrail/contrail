package org.ow2.contrail.provider.accounting.utils;

import org.ow2.contrail.common.oauth.client.TokenInfo;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class OAuthUtils {

    public static String getOwnerUuid(HttpServletRequest request) {
        TokenInfo tokenInfo = (TokenInfo) request.getAttribute(Constants.OAUTH_ACCESS_TOKEN);
        if (tokenInfo == null) {
            throw new WebApplicationException(
                    Response.status(Response.Status.UNAUTHORIZED).entity("Missing OAuth access token.").build());
        }
        else {
            return tokenInfo.getOwnerUuid();
        }
    }
}
