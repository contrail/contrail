package org.ow2.contrail.provider.accounting.db;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Iterator;
import java.util.Set;

public class Util {
    public static byte[] toByteArray(Object obj) {
        byte[] bytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            bos.close();
            bytes = bos.toByteArray();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return bytes;
    }

    public static Object toObject(byte[] bytes) {
        Object obj = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return obj;
    }

    public static DBObject encode(JSONArray a) {
        if (a == null)
            return null;

        BasicDBList result = new BasicDBList();
        try {
            for (int i = 0; i < a.length(); ++i) {
                Object o = a.get(i);
                if (o instanceof JSONObject) {
                    result.add(encode((JSONObject) o));
                }
                else if (o instanceof JSONArray) {
                    result.add(encode((JSONArray) o));
                }
                else {
                    result.add(o);
                }
            }
            return result;
        }
        catch (JSONException je) {
            return null;
        }
    }

    public static DBObject encode(JSONObject o) {
        if (o == null)
            return null;

        BasicDBObject result = new BasicDBObject();
        try {
            Iterator i = o.keys();
            while (i.hasNext()) {
                String k = (String) i.next();
                Object v = o.get(k);
                if (v instanceof JSONArray) {
                    result.put(k, encode((JSONArray) v));
                }
                else if (v instanceof JSONObject) {
                    result.put(k, encode((JSONObject) v));
                }
                else {
                    result.put(k, v);
                }
            }
            return result;
        }
        catch (JSONException je) {
            return null;
        }
    }

    public static JSONObject encode(DBObject o) {
        JSONObject result = new JSONObject();
        try {
            Set<String> keyset = o.keySet();
            Iterator i = keyset.iterator();
            while (i.hasNext()) {
                String k = (String) i.next();
                Object v = o.get(k);
                if (v instanceof BasicDBList) {
                    result.put(k, encode((BasicDBList) v));
                }
                else if (v instanceof DBObject) {
                    result.put(k, encode((DBObject) v));
                }
                else {
                    result.put(k, v);
                }
            }
            return result;
        }
        catch (Exception je) {
            return null;
        }
    }

    public static JSONArray encode(BasicDBList a) {
        JSONArray result = new JSONArray();
        Set<String> keyset = a.keySet();
        Iterator i = keyset.iterator();
        while (i.hasNext()) {
            String k = (String) i.next();
            Object o = a.get(k);
            if (o instanceof JSONObject) {
                result.put(encode((JSONObject) o));
            }
            else if (o instanceof JSONArray) {
                result.put(encode((JSONArray) o));
            }
            else {
                result.put(o);
            }
        }
        return result;
    }

}

