package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

public class FlatRatePriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(FlatRatePriceNode.class);

    private static PriceNodeType _type = PriceNodeType.QUERY;
    private static String[] _allowedFields = {"type", "fromDate", "toDate", "entities", "unit", "unitCost"};

    private Date _fromDate;
    private Date _toDate;
    private double _unit;
    private double _unitCost;
    private ArrayList<String> _entities;

    public FlatRatePriceNode() {
        _fromDate = null;
        _toDate = null;
        _unit = Double.NaN;
        _unitCost = Double.NaN;
        _entities = null;
    }

    public String parse(JSONObject input) {
        logger.debug("FLAT RATE NODE :: Parsing string input ... ");
        logger.debug(input);

        if (!Util.checkFields(input, _allowedFields)) {
            logger.debug("FLAT RATE NODE :: Node structure contains fields that are not allowed.");

            return "{error:price node structure contains fields that are not allowed.}";
        }

        try {
            // if specified store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("FLAT RATE NODE :: _fromDate = " + _fromDate);

            // if specified store the "to date"
            _toDate = Util.parseDate("toDate", input);
            logger.debug("FLAT RATE NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("FLAT RATE NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }


            // unit needs to be defined for the metrics..
            if (input.has("unit"))
                _unit = Util.unitConvert(IPriceNode.TimeUnit.valueOf(input.getString("unit")));
            else
                _unit = Double.NaN;
            logger.debug("FLAT RATE NODE :: _unit = " + _unit);

            // unit price needs to be defined!
            if (input.has("unitCost"))
                _unitCost = input.getDouble("unitCost");
            else
                _unitCost = Double.NaN;
            logger.debug("FLAT RATE NODE :: _unitCost = " + _unitCost);

            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("FLAT RATE NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);

                    logger.debug("FLAT RATE NODE :: _entities[0]=" + (String) o);
                }

            }
            else {
                _entities = null;
                logger.debug("FLAT RATE NODE :: _entities=null");
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            logger.error("PER UNIT NODE :: An exception was raised :: " + ex.getLocalizedMessage());

            return "{error:An exception has been raised during the parsing of a flat rate price node.}";
        }

        return "{status:OK}";
    }

    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public PriceNodeType getType() {
        return _type;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        logger.debug("FLAT RATE NODE :: Starting price calculation \n \t Price query " + definitions);

        // if no input data, then we return 0.s
        if (input.length() == 0) {
            logger.warn("FLAT RATE NODE :: No documents received from the database.");
            return 0;
        }

        try {
            // copy data into an array list
            ArrayList<JSONObject> inputArray = new ArrayList<JSONObject>();

            logger.debug("FLAT RATE NODE :: Number of documents received from the database :: " + input.length());

            for (int i = 0; i < input.length(); i++) {
                JSONObject o = input.getJSONObject(i);

                boolean add = true;

                // check if the document's time falls within the range defined by the from and to dates
                if ((_fromDate != null) && (o.has("time"))) {
                    Date time = DBConnector.formatter.parse(o.getString("time"));

                    if (time.compareTo(_fromDate) < 0)
                        add = false;
                }
                if ((_toDate != null) && (o.has("time"))) {
                    Date time = DBConnector.formatter.parse(o.getString("time"));

                    if (time.compareTo(_toDate) > 0)
                        add = false;
                }

                // check if any of the document's entities matches any of the entities defined by the node/pricing model
                if ((_entities != null) && (o.has("entities"))) {
                    HashSet<String> setEntities = new HashSet<String>();
                    setEntities.addAll(_entities);

                    Object objEntities = o.get("entities");

                    if (objEntities instanceof String) {
                        if (!_entities.contains((String) objEntities))
                            add = false;
                    }
                    else if (objEntities instanceof JSONArray) {
                        JSONArray dentities = (JSONArray) objEntities;
                        HashSet<String> setDEntities = new HashSet<String>();
                        for (int j = 0; j < dentities.length(); j++) {
                            setDEntities.add(dentities.getString(j));
                        }

                        // the actual intersection
                        setDEntities.retainAll(setEntities);

                        if (setDEntities.size() == 0) {
                            add = false;
                        }
                    }
                }
                if (add)
                    inputArray.add(o);
            }

            logger.debug("FLAT RATE NODE :: Number of documents satisfying all criteria :: " + inputArray.size());

            if (inputArray.size() < 2) {
                logger.warn("FLAT RATE NODE :: No document matches all criteria! [Check the monitoring frequency toleracne.]");
                return 0;
            }

            // sort data time-wise
            logger.debug("FLAT RATE NODE :: Sorting documents ...");
            Collections.sort(inputArray, new Util.JSONTimeComparator());

            Date dFirst = (Date) inputArray.get(0).get("time");
            Date dLast = (Date) inputArray.get(inputArray.size() - 1).get("time");

            double timeDiff = dLast.getTime() - dFirst.getTime();

            double price = _unitCost * timeDiff / _unit;

            logger.debug("FLAT RATE NODE :: Time diference :: " + timeDiff);

            logger.debug("FLAT RATE NODE :: PRICE :: " + price);

            return price;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }


    public JSONObject toJSON() {
        JSONObject output = new JSONObject();
        //{"type", "fromDate", "toDate", "entities", "unit", "unitCost"};

        try {
            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            output.put("type", _type);

            if (_unit != Double.NaN) {
                output.put("unit", _unit);
            }

            if (_unitCost != Double.NaN) {
                output.put("unitCost", _unitCost);
            }

            if (_entities != null) {
                JSONArray entities = new JSONArray();
                for (int i = 0; i < _entities.size(); i++) {
                    entities.put(_entities.get(i));
                }
                output.put("entities", entities);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
}
