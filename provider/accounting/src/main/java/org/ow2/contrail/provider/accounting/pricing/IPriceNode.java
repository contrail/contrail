package org.ow2.contrail.provider.accounting.pricing;


import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

public interface IPriceNode {
    public enum TimeUnit {
        SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR
    }

    ;

    public enum SizeUnit {
        BYTE, KILOBYTE, MEGABYTE, GIGABYTE
    }


    public enum PriceNodeType {
        ROOT, PRICE, QUERY, CONDITIONAL, FIXED_PRICE, PER_UNIT, PER_USE
    }

    ;

    public static double MONITORING_FREQUENCY_TOLERANCE = 3600 * 600;

    public String parse(JSONObject input);

    public double getPrice(DBConnector db, JSONObject definitions);

    public double getPrice(JSONArray input, JSONObject definitions);

    public PriceNodeType getType();

    public boolean validation(JSONObject obj);

    public JSONObject toJSON();
}
