package org.ow2.contrail.provider.accounting.aggregates;

import java.util.Date;

public class DateValue implements Comparable<DateValue> {
    private Date date;
    private Object value;

    public DateValue(Date d, Object v) {
        date = d;
        value = v;
    }

    public Date date() {
        return date;
    }

    public Object value() {
        return value;
    }

    @Override
    public int compareTo(DateValue arg0) {
        return this.date.compareTo(arg0.date);
    }

}
