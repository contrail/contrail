package org.ow2.contrail.provider.accounting.rest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.billing.Billing;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/billing")
public class BillingResource {
    private static Logger log = Logger.getLogger(BillingResource.class);

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response calculatePrice(String request) {
        if (log.isTraceEnabled()) {
            log.trace("calculatePrice() started. Request: " + request);
        }

        try {
            JSONObject requestJSON = new JSONObject(request);

            JSONObject model = requestJSON.getJSONObject("model");
            JSONObject query = requestJSON.getJSONObject("query");

            Billing billing = new Billing();
            String result = billing.calculatePrice(model, query);
            log.trace("calculatePrice() finished successfully.");
            return Response.ok(result).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
    }
}
