package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONObject;

public interface IAggregate {
    public void addValue(DateValue dv);

    public JSONObject getValue();
}
