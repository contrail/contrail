package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

public class MinAggregate implements IAggregate {

    private double min;

    public MinAggregate() {
        min = Double.MAX_VALUE;
    }

    @Override
    public void addValue(DateValue dv) {
        if ((Double) dv.value() < min)
            min = (Double) dv.value();
    }

    @Override
    public JSONObject getValue() {
        try {
            return new JSONObject().put("min", min);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
