package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AvailabilityAggregate implements IAggregate {
    List<DateValue> list;

    public AvailabilityAggregate() {
        list = new ArrayList<DateValue>();
    }

    @Override
    public void addValue(DateValue dv) {
        list.add(dv);
    }

    @Override
    public JSONObject getValue() {
        Collections.sort(list);
        long available = 0;

        System.out.println("List size :: " + list.size());

        for (int i = 0; i < list.size() - 1; i++) {
            if (((Double) list.get(i + 1).value() == 1) && ((Double) list.get(i).value() == 1)) {
                available += list.get(i + 1).date().getTime() - list.get(i).date().getTime();
            }
            else if (((Double) list.get(i + 1).value() != 0) || ((Double) list.get(i).value() != 0)) {
                available += (long) (0.5 * (list.get(i + 1).date().getTime() - list.get(i).date().getTime()));
            }
        }
        long totalDiff = list.get(list.size() - 1).date().getTime() - list.get(0).date().getTime();

        double availability = ((double) available) / ((double) totalDiff);

        try {
            return new JSONObject().put("availability", availability);
        }
        catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
