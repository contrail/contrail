package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StdDevAggregate implements IAggregate {
    private double mean;
    private double numberOfSamples;
    private List<Double> list;

    public StdDevAggregate() {
        mean = 0;
        numberOfSamples = 0;
        list = new ArrayList<Double>();
    }

    @Override
    public void addValue(DateValue dv) {
        mean = (mean * numberOfSamples + (Double) dv.value()) / (numberOfSamples + 1);
        numberOfSamples++;
        list.add((Double) dv.value());
    }

    @Override
    public JSONObject getValue() {
        if (numberOfSamples == 0) {
            try {
                return new JSONObject().put("stddev", -1).put("mean", 0);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            double stddev = 0;
            for (int i = 0; i < numberOfSamples; i++) {
                stddev += Math.pow(list.get(i) - mean, 2);
            }
            stddev /= numberOfSamples;
            stddev = Math.sqrt(stddev);

            try {
                return new JSONObject().put("stddev", stddev).put("mean", mean);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        return new JSONObject();
    }


}
