package org.ow2.contrail.provider.accounting.db;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public interface DBConnector {
    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    public abstract JSONObject query(JSONObject request) throws Exception;

    public abstract JSONArray queryDirect(JSONObject o, JSONObject fields) throws Exception;
}
