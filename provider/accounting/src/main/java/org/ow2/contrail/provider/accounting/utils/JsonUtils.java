package org.ow2.contrail.provider.accounting.utils;

import com.google.gson.*;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;
import java.util.Date;

public class JsonUtils {
    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static Logger log = Logger.getLogger(JsonUtils.class);

    private static JsonUtils instance;
    private Gson gson;

    private JsonUtils() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

        gsonBuilder.serializeSpecialFloatingPointValues(); // Double.NaN is valid double value

        gson = gsonBuilder.create();
        log.info("Gson created.");

    }

    public static JsonUtils getInstance() {
        if (instance == null) {
            instance = new JsonUtils();
        }
        return instance;
    }

    public Gson getGson() {
        return gson;
    }

    private static class DateSerializer implements JsonSerializer<Date> {

        public JsonElement serialize(Date date, Type typeOfSrc,
                                     JsonSerializationContext context) {
            JsonPrimitive dateprim = null;
            try {
                dateprim = new JsonPrimitive(date.getTime());
            }
            catch (Exception e) {
                e.printStackTrace();
                log.error("DateDeserializer failed for dateString ", e);

                throw new JsonParseException(e.getMessage());
            }
            return dateprim;
        }
    }

    private static class DateDeserializer implements JsonDeserializer<Date> {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            long millis = json.getAsJsonPrimitive().getAsLong();
            try {
                return new Date(millis);
            }
            catch (Exception e) {

                e.printStackTrace();
                log.error("DateDeserializer failed for date " + millis, e);

                throw new JsonParseException(e.getMessage());
            }
        }
    }
}
