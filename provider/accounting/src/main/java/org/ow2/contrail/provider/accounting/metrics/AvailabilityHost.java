package org.ow2.contrail.provider.accounting.metrics;

import com.mongodb.DB;
import org.apache.log4j.Logger;
import org.ow2.contrail.provider.accounting.enums.Source;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;
import org.ow2.contrail.provider.storagemanager.HistoryRetriever;
import org.ow2.contrail.provider.storagemanager.MetricsHistoryData;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AvailabilityHost extends Metric {

    private static Logger log = Logger.getLogger(AvailabilityHost.class);
    private List<String> sids;

    public AvailabilityHost(List<String> sids) {
        this.sids = sids;
    }

    @Override
    public Map<String, Object> getValue(Date startTime, Date endTime) throws Exception {
        if (sids.size() == 1) {
            return calculateHostAvailability(sids.get(0), startTime, endTime);
        }
        else {
            double sum = 0;
            for (String sid : sids) {
                Map<String, Object> hostAvailData = calculateHostAvailability(sid, startTime, endTime);
                double hostAvail = (Double) hostAvailData.get("availability");
                sum += hostAvail;
            }
            double availability = sum / sids.size();
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("availability", availability);
            return result;
        }
    }

    private Map<String, Object> calculateHostAvailability(String sid, Date startTime, Date endTime) throws Exception {
        log.trace(String.format(
                "Retrieving 'availabilityStatus' metric history from storage-manager for the host %s ...", sid));
        org.ow2.contrail.provider.storagemanager.common.Metric availMetric =
                new org.ow2.contrail.provider.storagemanager.common.Metric("common", "availability");
        DB db = MongoDBConnection.getDB();
        HistoryRetriever historyRetriever = new HistoryRetriever(db);
        MetricsHistoryData history = historyRetriever.getHistory(
                availMetric, Source.HOST.name(), sid, startTime, endTime);
        log.trace("Metric history retrieved successfully.");

        long upTime = 0;
        long downTime = 0;
        long naTime = 0;
        Date actualStartTime = null;
        Date actualEndTime = null;

        for (int i = 0; i < history.size(); i++) {
            int value = (Integer) history.getMetricValues("availability").get(i);
            boolean isAvailable = (value != 0);

            // calculate start time of current interval
            long t1 = history.getTimeValues().get(i).getTime();
            if (startTime != null && t1 < startTime.getTime()) {
                t1 = startTime.getTime();
            }

            // calculate end time of current interval
            long t2;
            if (i < history.size() - 1) {
                // this is not the last interval in history
                t2 = history.getTimeValues().get(i + 1).getTime();
                if (t2 > endTime.getTime()) {
                    t2 = endTime.getTime();
                }
            }
            else {
                // this is the last interval in history
                if (history.size() >= 2) {
                    // assume that length of current interval is the same as the length of previous interval
                    long tPrevious = history.getTimeValues().get(i - 1).getTime();
                    t2 = 2 * t1 - tPrevious;
                }
                else {
                    // use default interval length
                    t2 = t1 + 5000;
                }
                if (t2 > endTime.getTime()) {
                    t2 = endTime.getTime();
                }
            }

            long dt = t2 - t1;
            if (isAvailable) {
                upTime += dt;
            }
            else {
                downTime += dt;
            }

            if (i == 0) {
                actualStartTime = new Date(t1);
            }
            if (i == history.size() - 1) {
                actualEndTime = new Date(t2);
            }
        }

        Map<String, Object> result = new HashMap<String, Object>();
        long totalTime = upTime + downTime;
        result.put("uptime", upTime / 1000);
        result.put("downtime", downTime / 1000);
        result.put("NAtime", naTime / 1000);
        double availability;
        if (totalTime > 0) {
            availability = 100 * (double) upTime / totalTime;
        }
        else {
            availability = Double.NaN;
        }
        result.put("availability", availability);

        result.put("startTime", actualStartTime);
        result.put("endTime", actualEndTime);

        return result;
    }
}
