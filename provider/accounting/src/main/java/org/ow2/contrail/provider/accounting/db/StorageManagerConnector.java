package org.ow2.contrail.provider.accounting.db;

import com.mongodb.DB;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;
import org.ow2.contrail.provider.storagemanager.DataRetriever;
import org.ow2.contrail.provider.storagemanager.aggregates.HistoryJsonAggregate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StorageManagerConnector implements DBConnector {
    private static Logger log = Logger.getLogger(StorageManagerConnector.class);
    private DataRetriever dataRetriever;

    public StorageManagerConnector() throws IOException {
        DB db = MongoDBConnection.getDB();
        dataRetriever = new DataRetriever(db);
    }

    @Override
    public JSONObject query(JSONObject request) {
        throw new UnsupportedOperationException();
    }

    @Override
    public JSONArray queryDirect(JSONObject query, JSONObject fields) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("query() started.");
            log.trace("Query to execute: " + query.toString() + "  Fields: " + fields.toString());
        }

        List<String> sids = null;
        Date startTime = null;
        Date endTime = null;
        String group = null;
        List<String> metrics = null;
        try {
            sids = new ArrayList<String>();
            JSONArray sidArr = query.getJSONObject("sid").getJSONArray("$in");
            for (int i = 0; i < ((JSONArray) sidArr).length(); i++) {
                String sid = ((JSONArray) sidArr).getString(i);
                sids.add(sid);
            }

            startTime = (Date) query.getJSONObject("time").get("$gt");
            endTime = (Date) query.getJSONObject("time").get("$lt");
            group = query.getString("group");

            Iterator iterator = fields.keys();
            metrics = new ArrayList<String>();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                if (key.startsWith("metrics.")) {
                    String metric = key.substring(key.indexOf(".") + 1);
                    metrics.add(metric);
                }
            }
        }
        catch (JSONException e) {
            throw new Exception("Invalid query received: " + e.getMessage(), e);
        }

        log.trace("Calling storage-manager...");
        HistoryJsonAggregate historyJsonAggregate = new HistoryJsonAggregate(metrics);
        dataRetriever.calculateAggregateValue(group, metrics, "host", sids, startTime, endTime, historyJsonAggregate);
        log.trace("Data retrieved.");

        log.trace("query() finished successfully.");
        return historyJsonAggregate.getAggregateValue();
    }
}
