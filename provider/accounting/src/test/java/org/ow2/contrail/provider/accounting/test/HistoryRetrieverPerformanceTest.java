package org.ow2.contrail.provider.accounting.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.accounting.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class HistoryRetrieverPerformanceTest {
    private static final int[] TEST_DURATIONS = {1, 10, 60, 120, 300, 720, 1440, 1440*2, 1440*3, 1440*5, 1440*7};
    private static final String URL_CONDENSED_DATA = "http://localhost:8080/accounting";
    private static final GregorianCalendar startTime = new GregorianCalendar(2013, 5, 1, 0, 0, 0);
    private static final String HOST = "host001.test.com";
    private static final String METRIC = "cpu.load_one";

    public static void main(String[] args) throws Exception {
        new HistoryRetrieverPerformanceTest().execute();
    }

    public void execute() throws Exception {
        System.out.println("Condensed history data:");
        System.out.println("duration[min]\tcontent-length[bytes]\ttime[s]");
        for (int i = 0; i < TEST_DURATIONS.length; i++) {
            testRetrieveCondensedHistoryData(TEST_DURATIONS[i]);
        }

        System.out.println();
        System.out.println("Raw history data:");
        System.out.println("duration[min]\tcontent-length[bytes]\ttime[s]");
        for (int i = 0; i < TEST_DURATIONS.length; i++) {
            testRetrieveRawHistoryData(TEST_DURATIONS[i]);
        }
    }

    private void testRetrieveCondensedHistoryData(int duration) throws Exception {
        Client client = Client.create();
        WebResource webResource = client.resource(URL_CONDENSED_DATA);

        JSONObject postData = new JSONObject();
        postData.put("source", "host");
        JSONArray sids = new JSONArray().put(HOST);
        postData.put("sids", sids);
        JSONArray metrics = new JSONArray().put(METRIC);
        postData.put("metrics", metrics);
        postData.put("startTime", DateUtils.format(startTime.getTime()));
        GregorianCalendar endTime = (GregorianCalendar) startTime.clone();
        endTime.add(Calendar.MINUTE, duration);
        postData.put("endTime", DateUtils.format(endTime.getTime()));
        postData.put("numberOfIntervals", 1000);

        Date t0 = new Date();
        ClientResponse response = webResource.path("/metrics_history/condensed_data")
                .post(ClientResponse.class, postData);
        Date t1 = new Date();
        double time = (t1.getTime() - t0.getTime()) / 1000.0;
        if (response.getStatus() != 200) {
            throw new Exception("Invalid response received from the accounting: "
                    + response.getClientResponseStatus());
        }
        String content = response.getEntity(String.class);
        int contentLength = content.length();
        System.out.println(duration + "\t" +
                contentLength + "\t" +
                time);
    }

    private void testRetrieveRawHistoryData(int duration) throws Exception {
        Client client = Client.create();
        WebResource webResource = client.resource(URL_CONDENSED_DATA);

        JSONObject postData = new JSONObject();
        postData.put("source", "host");
        postData.put("sid", HOST);
        JSONArray metrics = new JSONArray().put(METRIC);
        postData.put("metrics", metrics);
        postData.put("startTime", DateUtils.format(startTime.getTime()));
        GregorianCalendar endTime = (GregorianCalendar) startTime.clone();
        endTime.add(Calendar.MINUTE, duration);
        postData.put("endTime", DateUtils.format(endTime.getTime()));

        Date t0 = new Date();
        ClientResponse response = webResource.path("/accounting/metrics_history")
                .post(ClientResponse.class, postData);
        Date t1 = new Date();
        double time = (t1.getTime() - t0.getTime()) / 1000.0;
        if (response.getStatus() != 200) {
            throw new Exception("Invalid response received from the accounting: "
                    + response.getClientResponseStatus());
        }
        String content = response.getEntity(String.class);
        int contentLength = content.length();
        System.out.println(duration + "\t" +
                contentLength + "\t" +
                time);
    }
}