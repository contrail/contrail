package org.ow2.contrail.provider.accounting.test;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.provider.accounting.billing.Billing;
import org.ow2.contrail.provider.accounting.db.DBConnector;
import org.ow2.contrail.provider.storagemanager.Conf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

@Ignore // requires local MongoDB database
public class TestBilling {
    private static Logger log = Logger.getLogger(TestBilling.class);

    @Before
    public void setUp() throws IOException {
        String configFilePath = "src/main/config/provider-accounting.cfg";
        Conf.getInstance().load(configFilePath);
    }

    @Test
    public void testPriceCalculation() throws Exception {
        log.trace("testPriceCalculation() started.");
        String modelJson = readFileAsString("src/test/resources/price-model1.json");
        JSONObject model = new JSONObject(modelJson);

        Calendar cal1 = new GregorianCalendar();
        cal1.set(2012, 1, 1, 0, 0, 0);
        Calendar cal2 = new GregorianCalendar();
        cal2.set(2012, 1, 10, 23, 59, 59);

        JSONObject query = new JSONObject();
        query.put("toDate", DBConnector.formatter.format(cal2.getTime()));
        query.put("fromDate", DBConnector.formatter.format(cal1.getTime()));
        query.put("entities", new JSONArray().put("n0001.xc1.xlab.lan"));

        Date t1 = new Date();
        Billing billing = new Billing();
        String result = billing.calculatePrice(model, query);
        Date t2 = new Date();
        long dt = t2.getTime() - t1.getTime();
        log.info("Price: " + result);
        log.info(String.format("Time: %.2f seconds", dt/1000.0));

        log.trace("testPriceCalculation() finished successfully.");
    }

    private String readFileAsString(String filePath) throws FileNotFoundException {
        return new Scanner(new File(filePath)).useDelimiter("\\Z").next();
    }
}
