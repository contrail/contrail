package org.ow2.contrail.provider.accounting.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;
// COMMAND LINE EXAMPLE:
//		curl -i -X POST -H Accept:application/json -H Content-Type:application/json -d '{"host":"n00013.xc1.xlab.lan","command":"availability","from":"2011.07.20.13.49.19","to":"2011.07.21.13.49.19"}' https://localhost:8443/Accounting/auditor --cacert ~/.ssh/ca.crt --cert ~/.ssh/client.crt --key ~/.ssh/client.key 

public class TestClient {

    // TODO: Modify tests so that are not machine dependent and remove blankTest

    @Test
    public void blankTest() {
        assertTrue(true);
    }

    //@Test
    public void test() {
        SSLContext ctx = null;
        try {
            KeyStore trustStore;
            KeyStore keyStore;

            trustStore = KeyStore.getInstance("JKS");
            trustStore.load(new FileInputStream("/Users/ales/.ssh/cacerts.jks"), "contrail".toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(trustStore);

            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("/Users/ales/.ssh/client.jks"), "contrail".toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keyStore, "contrail".toCharArray());

            ctx = SSLContext.getInstance("SSL");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            System.out.println("SSL context initialized;");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ClientConfig config = new DefaultClientConfig();
            config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(null, ctx));


            WebResource webResource = Client.create(config).resource("https://localhost:8443/contrail/accounting");
            //service.addFilter(new HTTPBasicAuthFilter(username,password));

            //Client client = Client.create();
            //WebResource webResource = client.resource("http://localhost:8080/Accounting/auditor");

            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();
                JSONArray conditions = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                conditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(cal.getTime())));

                metrics.put(new JSONObject().put("field", "time").put("conditions", conditions));

                filter.put("resource", "cpu");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("idle", "stddev"));
                rGet.put("get", filter);
                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);


                metrics.put(new JSONObject().put("field", "sid").put("conditions", "n00013.xc1.xlab.lan"));

                filter.put("resource", "cpu");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("idle", "stddev"));
                rGet.put("get", filter);
                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();
                JSONArray conditions = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                conditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(cal.getTime())));

                metrics.put(new JSONObject().put("field", "time").put("conditions", conditions));
                metrics.put(new JSONObject().put("field", "sid").put("conditions", "n00013.xc1.xlab.lan"));

                filter.put("resource", "cpu");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("idle", "list"));
                rGet.put("get", filter);
                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();
                JSONArray conditions = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                conditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(cal.getTime())));

                metrics.put(new JSONObject().put("field", "time").put("conditions", conditions));
                metrics.put(new JSONObject().put("field", "sid").put("conditions", "n00013.xc1.xlab.lan"));

                filter.put("resource", "memory");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("free", "stddev"));
                rGet.put("get", filter);
                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();
                JSONArray conditions = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                conditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(cal.getTime())));

                metrics.put(new JSONObject().put("field", "time").put("conditions", conditions));
                metrics.put(new JSONObject().put("field", "sid").put("conditions", "n00013.xc1.xlab.lan"));

                filter.put("resource", "disk");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("available", "stddev"));
                rGet.put("get", filter);
                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();
                JSONObject rGet = new JSONObject();
                JSONObject filter = new JSONObject();
                JSONArray metrics = new JSONArray();
                JSONArray conditions = new JSONArray();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                conditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(cal.getTime())));

                metrics.put(new JSONObject().put("field", "time").put("conditions", conditions));
                metrics.put(new JSONObject().put("field", "sid").put("conditions", "n00013.xc1.xlab.lan"));

                filter.put("resource", "info");
                filter.put("filter", metrics);
                filter.put("return", new JSONObject().put("available", "availability"));
                rGet.put("get", filter);

                rObj.put("command", rGet);

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }
            {
                JSONObject rObj = new JSONObject();

                Calendar cal = new GregorianCalendar();
                cal.set(2011, 6, 20);

                rObj.put("command", "availability").put("host", "n00013.xc1.xlab.lan").put("from", DBConnector.formatter.format(cal.getTime()));

                String responseString = webResource.accept("application/json").type("application/json").post(String.class, rObj.toString());

                System.out.println(responseString);
                System.out.print("\n\n\n");
            }


        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        assertTrue(true);
    }

}
