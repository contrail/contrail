package org.ow2.contrail.provider.accounting.test;

import com.mongodb.BasicDBObject;
import org.ow2.contrail.provider.accounting.db.MongoDBConnector;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FillDatabase {
    private static String DATABASE_NAME = "monitoring-billing-test";
    private static String COLLECTION_NAME = "rawData";
    private static int NUM_OF_HOSTS = 8;

    public static void main(String[] args) throws Exception {
        MongoDBConnector db = new MongoDBConnector();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 1); // Feb
        cal.set(Calendar.YEAR, 2012);

        Calendar calEnd = (Calendar) cal.clone();
        calEnd.add(Calendar.MONTH, 1);

        try {
            db.connect("127.0.0.1", DATABASE_NAME, 27017);

            db.useCollection(COLLECTION_NAME);

            System.out.println("Connected.");

            Random rand = new Random();

            System.out.println("Writing...");

            int cores[] = new int[NUM_OF_HOSTS];
            for (int i = 0; i < NUM_OF_HOSTS; i++) {
                cores[i] = 2 + (int) (Math.random() * ((16 - 2) + 1));
            }

            while (cal.before(calEnd)) {
                Date d = cal.getTime();

                for (int j = 0; j < NUM_OF_HOSTS; j++) {
                    BasicDBObject obj = new BasicDBObject();
                    obj.put("time", d);
                    obj.put("group", "cpu");
                    obj.put("source", "host");
                    obj.put("sid", "n000" + j + ".xc1.xlab.lan");

                    BasicDBObject metrics = new BasicDBObject();
                    double a = rand.nextDouble();
                    double b = (1 - a) * rand.nextDouble();

                    metrics.put("cores", cores[j]);
                    metrics.put("system", new Double(a));
                    metrics.put("speed", 800 + (int) (Math.random() * ((3600 - 800) + 1)));
                    metrics.put("user", new Double(b));
                    metrics.put("idle", new Double(1 - a - b));

                    obj.put("metrics", metrics);

                    db.write(obj);
                }

                for (int j = 0; j < NUM_OF_HOSTS; j++) {
                    BasicDBObject obj = new BasicDBObject();
                    obj.put("time", d);
                    obj.put("group", "memory");
                    obj.put("source", "host");
                    obj.put("sid", "n000" + j + ".xc1.xlab.lan");

                    BasicDBObject metrics = new BasicDBObject();
                    double a = rand.nextDouble();
                    double b = (1 - a);
                    double total = 4096 + (int) (Math.random() * ((65536 - 4096) + 1));

                    metrics.put("total", total);
                    metrics.put("free", new Double(Math.round(a * total)));
                    metrics.put("used", new Double(Math.round(b * total)));

                    obj.put("metrics", metrics);

                    db.write(obj);
                }

                for (int j = 0; j < NUM_OF_HOSTS; j++) {
                    BasicDBObject obj = new BasicDBObject();
                    obj.put("time", d);
                    obj.put("group", "disk");
                    obj.put("source", "host");
                    obj.put("sid", "n000" + j + ".xc1.xlab.lan");

                    BasicDBObject metrics = new BasicDBObject();
                    double a = rand.nextDouble();
                    double b = (1 - a);
                    double total = 4096 + (int) (Math.random() * ((16777216 - 4096) + 1));

                    metrics.put("total", total);
                    metrics.put("available", new Double(Math.round(a * total)));
                    metrics.put("used", new Double(Math.round(b * total)));

                    obj.put("metrics", metrics);

                    db.write(obj);
                }

                for (int j = 0; j < NUM_OF_HOSTS; j++) {
                    BasicDBObject obj = new BasicDBObject();
                    obj.put("time", d);
                    obj.put("group", "info");
                    obj.put("source", "host");
                    obj.put("sid", "n000" + j + ".xc1.xlab.lan");

                    BasicDBObject metrics = new BasicDBObject();
                    double a = rand.nextDouble();
                    double b;
                    if (a > 0.5)
                        b = 1;
                    else
                        b = 0;

                    metrics.put("available", new Double(b));

                    obj.put("metrics", metrics);

                    db.write(obj);
                }

                cal.add(Calendar.MINUTE, 1);
            }
            System.out.println("Done.");
        }
        finally {
            db.disconnect();
        }
    }
}
