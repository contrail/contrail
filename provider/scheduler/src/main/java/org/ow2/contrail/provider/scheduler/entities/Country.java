/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fgaudenz
 */
@Entity
@Table(name="COUNTRYLIST")
@NamedQueries({
        @NamedQuery(name = "Country.findAll", query = "SELECT d FROM Country d"),
        @NamedQuery(name="Country.findByCode",query="SELECT c FROM Country c WHERE c.code = :code")
        })
public class Country {
    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;
    private String name;
   private String code;
    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
    
    
    
    
    public Integer getId() {
        return id;
    }
    
    

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Country)) {
            return false;
        }
        Country other = (Country) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.provider.scheduler.entities.Country[ id=" + id + " ]";
    }
    
}
