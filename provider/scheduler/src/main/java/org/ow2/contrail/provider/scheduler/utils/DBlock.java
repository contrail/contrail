/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.utils;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import org.ow2.contrail.provider.scheduler.entities.*;

/**
 *
 * @author fgaudenz
 */
public class DBlock {
   public static synchronized void  deleteHost(Host host){
       EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            host = em.merge(host);
            em.remove(host);
            em.getTransaction().commit();
            //return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
   }

    static void deleteDatacenter(Datacenter datacenter) {
       EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            datacenter = em.merge(datacenter);
            em.remove(datacenter);
            em.getTransaction().commit();
            //return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    static void deleteCluster(Cluster cluster) {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            cluster = em.merge(cluster);
            em.remove(cluster);
            em.getTransaction().commit();
            //return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    static void deleteRack(Rack rack) {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            rack = em.merge(rack);
            em.remove(rack);
            em.getTransaction().commit();
            //return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
    static void updateDatacenter(Datacenter datacenter){
        //TODO
        
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Datacenter newData=em.find(Datacenter.class, datacenter.getId());
            em.getTransaction().begin();
            //updateDatacenter
            em.persist(datacenter);
            em.getTransaction().commit();
            PersistenceUtils.getInstance().closeEntityManager(em);
    }
    
    public static void deleteVM(VmSlot vm){
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            vm = em.merge(vm);
            em.remove(vm);
            em.getTransaction().commit();
            //return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    static void storeReservation(Reservation reservation) {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager(); 
        em.getTransaction().begin();
        em.persist(reservation);
        em.getTransaction().commit();
        PersistenceUtils.getInstance().closeEntityManager(em);
    }

    static void updateSingleReservation(SingleVMRes single) {
            EntityManager em = PersistenceUtils.getInstance().getEntityManager();
           
            em.createQuery("update SingleVMRes set vmslot="+single.getVm()+" where id="+single.getId()).executeUpdate();
            
            PersistenceUtils.getInstance().closeEntityManager(em);
    }
    
    
    
}
