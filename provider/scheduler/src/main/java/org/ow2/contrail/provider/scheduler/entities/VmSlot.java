package org.ow2.contrail.provider.scheduler.entities;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import org.json.JSONArray;

@Entity
@Table(name="VMSLOT")
@NamedQueries({
        //@NamedQuery(name = "VmSlot.findAllHosted", query = "SELECT d FROM VmSlot d WHERE c.status='DPY' or c.status='RNG'"),
        @NamedQuery(name="VmSlot.findAll",query="SELECT v FROM VmSlot v")
        })
public class VmSlot {

    public static VmReservationSlot[] toVMReservationSlot(VmSlot[] vms, VmReservationSlot[] vmsR) {

        ArrayList<VmReservationSlot> res = new ArrayList<VmReservationSlot>();

        for (VmSlot vm : vms) {
            if (vm.getHost() != null) {
                boolean found = false;
                for (int i = 0; i < res.size(); i++) {
                    if (vm.getReservationBelongsTo() == res.get(i).getId()) {
                        //aggiungi
                        res.get(i).addSingleReservation(vm.host);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    //System.out.println("CREATE NEW RESERVATION WITHOUT ENDTIME");
                    res.add(vm.toVmReservationSlot());
                }
            }
        }

        

        System.out.println("CREAZIONE ARRAY DI:" + res.size());
        VmReservationSlot[] result = new VmReservationSlot[res.size()];
        for (int i = 0; i < result.length; i++) {
            int k=0;
            while((k<vmsR.length)&&(res.get(i).getId()!=vmsR[k].getId())){
                k++;
            }
            System.out.println("index:"+k+"--"+i);
            result[i] = res.get(i);
            result[i].setEndTime(vmsR[k].getEndTime());
            result[i].setStartTime(vmsR[k].getStartTime());
        }
        return result;
    }

    
    final static String COUNTRY="country";
    final static String HOST="host";
    final static String CLUSTER="cluster";
    final static String RACK="rack";
    
    
    public static void processConstraintsforReservation(ArrayList<VmSlot> vms, JSONArray constraintArray) throws JSONException {
        System.out.println("number of constraints for Reservation:"+constraintArray.length());
        for(int i=0;i<constraintArray.length();i++){
                JSONObject constraint=constraintArray.getJSONObject(i);
                String typeConstraint=constraint.getString("id");
                System.out.println("constraint about:"+typeConstraint);
                if(typeConstraint.equalsIgnoreCase(COUNTRY)){
                   /*
                   // COUNTRY CONSTRAINTS
                   //
                   */
                    String operator=constraint.getString("operator");
                    String country=constraint.getString("value");
                    System.out.println("COUNTRY & OPERATION:"+country+" "+operator);
                    JSONArray vmsID=(JSONArray) constraint.get("VMs");
                    for(int k=0;k<vmsID.length();k++){
                      int vmID=vmsID.getJSONObject(k).getInt("id");
                      for(int j=0;j<vms.size();j++){
                          VmSlot vm=vms.get(j);
                          
                          if(vm.getReservationBelongsTo()==vmID){
                              System.out.println(vm.getReservationBelongsTo()+" vs "+vmID);
                              if(operator.equalsIgnoreCase("NOT")){
                                vm.addNotCountry(country);
                              }else{
                                 vm.addInCountry(country);
                              }
                              System.out.println("CHECK IN COUNTRY"+vm.inCountry.size());
                          }
                          
                      }
                       
                       
                   }
                }
                if (typeConstraint.equalsIgnoreCase(HOST)) {             
                     /*
                     // HOST CONSTRAINTS (<operator> same host of - operator in|not
                     //
                     */
                    String operator = constraint.getString("operator");
                    VmSlot vmtTOT=new VmSlot();
                    //System.out.println("HOST & OPERATION:" + country + " " + operator);
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmHost=new ArrayList<VmSlot>();
                   
                    
                    for (int k = 0; k < vmsID.length(); k++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            if (vm.getReservationBelongsTo() == vmID)    
                                vmHost.add(vm); 
                        }//TODO:if not found look on the database
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                             if (operator.equalsIgnoreCase("NOT")) {
                                    vm.addNotHostList(vmHost);
                                } else {
                                    //TODO:manage sameHOST
                         }
                        }
                        
                    }


                    }
                
                 if (typeConstraint.equalsIgnoreCase(RACK)) {             
                     /*
                     // co_location_RACK 
                     //
                     */
                    System.out.println("RACK WORKING");
                    System.out.println("vms.size:"+vms.size());
                    String operator = constraint.getString("operator");
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmRack=new ArrayList<VmSlot>();
                   
                    for (int k = 0; k < vmsID.length(); k++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        System.out.println("FIRST LOOP:"+vmID);
                        boolean found=false;
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            System.out.println(vmID +"vs" +vm.getReservationBelongsTo()+"-"+vm.getId());
                            if (vm.getReservationBelongsTo() == vmID){    
                                vmRack.add(vm);
                                found=true;
                            }
                        }
                        if(!found){
                          //TODO:if not found look on the database  
                        }
                    System.out.println("VMRACK PRINT:"+vmRack.size());   
                    }
                    for(VmSlot appo:vmRack){
                        System.out.println("ADDED:"+appo.getId());
                    }
                        for (int j = 0; j < vmRack.size(); j++) {
                            VmSlot vm = vmRack.get(j);
                            vm.addSameRackList(vmRack);
                        }
                        
                    


                    }
                
                
                
                
                
                
                
                
                
                
                if (typeConstraint.equalsIgnoreCase(CLUSTER)) {             
                     /*
                     // HOST CONSTRAINTS (<operator> same host of - operator in|not
                     //
                     */
                    String operator = constraint.getString("operator");
                    
                    //System.out.println("HOST & OPERATION:" + country + " " + operator);
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmHost=new ArrayList<VmSlot>();
                    for (int k = 0; i < vmsID.length(); i++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            if (vm.getId() == vmID)    
                                vmHost.add(vm); 
                        }//TODO:if not found look on the database
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                             if (operator.equalsIgnoreCase("NOT")) {
                                    vm.addNotClusterList(vmHost);
                                } else {
                                    //TODO:manage sameHOST
                         }
                        }
                        
                    }


                    }
                
            }




        }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static void processConstraints(ArrayList<VmSlot> vms, JSONArray constraintArray) throws JSONException {
        System.out.println("number of constraints:"+constraintArray.length());
        for(int i=0;i<constraintArray.length();i++){
                JSONObject constraint=constraintArray.getJSONObject(i);
                String typeConstraint=constraint.getString("id");
                System.out.println("constraint about:"+typeConstraint);
                if(typeConstraint.equalsIgnoreCase(COUNTRY)){
                   /*
                   // COUNTRY CONSTRAINTS
                   //
                   */
                    String operator=constraint.getString("operator");
                    String country=constraint.getString("value");
                    System.out.println("COUNTRY & OPERATION:"+country+" "+operator);
                    JSONArray vmsID=(JSONArray) constraint.get("VMs");
                    for(int k=0;k<vmsID.length();k++){
                      int vmID=vmsID.getJSONObject(k).getInt("id");
                      for(int j=0;j<vms.size();j++){
                          VmSlot vm=vms.get(j);
                          
                          if(vm.getId()==vmID){
                              System.out.println(vm.getId()+" vs "+vmID);
                              if(operator.equalsIgnoreCase("NOT")){
                                vm.addNotCountry(country);
                              }else{
                                 vm.addInCountry(country);
                              }
                              System.out.println("CHECK IN COUNTRY"+vm.inCountry.size());
                          }
                          
                      }
                       
                       
                   }
                }
                if (typeConstraint.equalsIgnoreCase(HOST)) {             
                     /*
                     // HOST CONSTRAINTS (<operator> same host of - operator in|not
                     //
                     */
                    String operator = constraint.getString("operator");
                    VmSlot vmtTOT=new VmSlot();
                    //System.out.println("HOST & OPERATION:" + country + " " + operator);
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmHost=new ArrayList<VmSlot>();
                   
                    
                    for (int k = 0; k < vmsID.length(); k++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            if (vm.getId() == vmID)    
                                vmHost.add(vm); 
                        }//TODO:if not found look on the database
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                             if (operator.equalsIgnoreCase("NOT")) {
                                    vm.addNotHostList(vmHost);
                                } else {
                                    //TODO:manage sameHOST
                         }
                        }
                        
                    }


                    }
                
                 if (typeConstraint.equalsIgnoreCase(RACK)) {             
                     /*
                     // co_location_RACK 
                     //
                     */
                    System.out.println("RACK WORKING");
                    System.out.println("vms.size:"+vms.size());
                    String operator = constraint.getString("operator");
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmRack=new ArrayList<VmSlot>();
                   
                    for (int k = 0; k < vmsID.length(); k++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        System.out.println("FIRST LOOP:"+vmID);
                        boolean found=false;
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            //System.out.println(vmID);
                            if (vm.getId() == vmID){    
                                vmRack.add(vm);
                                found=true;
                            }
                        }
                        if(!found){
                          //TODO:if not found look on the database  
                        }
                    System.out.println("VMRACK PRINT");   
                    }
                    for(VmSlot appo:vmRack){
                        System.out.println("ADDED:"+appo.getId());
                    }
                        for (int j = 0; j < vmRack.size(); j++) {
                            VmSlot vm = vmRack.get(j);
                            vm.addSameRackList(vmRack);
                        }
                        
                    


                    }
                
                
                
                
                
                
                
                
                
                
                if (typeConstraint.equalsIgnoreCase(CLUSTER)) {             
                     /*
                     // HOST CONSTRAINTS (<operator> same host of - operator in|not
                     //
                     */
                    String operator = constraint.getString("operator");
                    
                    //System.out.println("HOST & OPERATION:" + country + " " + operator);
                    JSONArray vmsID = (JSONArray) constraint.get("VMs");
                    ArrayList<VmSlot> vmHost=new ArrayList<VmSlot>();
                    for (int k = 0; i < vmsID.length(); i++) {
                        int vmID = vmsID.getJSONObject(k).getInt("id");
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                            if (vm.getId() == vmID)    
                                vmHost.add(vm); 
                        }//TODO:if not found look on the database
                        for (int j = 0; j < vms.size(); j++) {
                            VmSlot vm = vms.get(j);
                             if (operator.equalsIgnoreCase("NOT")) {
                                    vm.addNotClusterList(vmHost);
                                } else {
                                    //TODO:manage sameHOST
                         }
                        }
                        
                    }


                    }
                
            }




        }
    
    
    
    @Id
    private Integer id;
    private Integer cpuCount;
    private Double cpuSpeed;
    private Double memorySize;
    private Double diskSize;
    //private String status;
    @ManyToOne(optional = true)
    private Host host;
    @OneToMany(mappedBy = "vmslot", cascade = CascadeType.PERSIST)
    private List<SingleVMRes> reservations;
    @Transient
    int reservationCEE=-1;
    @Transient
    int reservationID=-1;
    @Transient
    ArrayList<String> inCountry;
    @Transient
    ArrayList<String> notCountry;
    @Transient
    List<VmSlot> vmNotSameHost;
    @Transient
    List<VmSlot> vmSameRack;
    @Transient
    List<VmSlot> vmNotSameCluster;
    @Transient
    int sameHost_id;
    @Transient
    int resBelongTo;
    
    public void addReservation(SingleVMRes single){
        if(reservations==null){
            reservations=new ArrayList<SingleVMRes>();
        }
        reservations.add(single);  
    }
    public void freeReservation(){
        reservations=null;
    }

    public List<SingleVMRes> getReservations() {
        return reservations;
    }
    
    public VmSlot() {
        inCountry=new ArrayList<String>();
        notCountry=new ArrayList<String>();
       vmSameRack=new ArrayList<VmSlot>();
       vmNotSameHost=new ArrayList<VmSlot>();
    }

    public VmSlot(JSONObject json) throws JSONException {
        this.id = json.getInt("id");
        this.cpuCount = json.getInt("nCpu");
        this.cpuSpeed = json.getDouble("fCpu");
        this.memorySize = json.getDouble("mem");
        this.diskSize = json.getDouble("disk");
        if (json.has("reservation_CEE")) {
            reservationCEE=json.getInt("reservation_CEE");
            reservationID=json.getInt("reservation_id");
        }else{
            reservationCEE=-1;
            reservationID=-1;
        }
        host=null;
        inCountry=new ArrayList<String>();
        notCountry=new ArrayList<String>();
        vmSameRack=new ArrayList<VmSlot>();
        vmNotSameHost=new ArrayList<VmSlot>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

   

    public Double getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(Double cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public Double getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(Double memorySize) {
        this.memorySize = memorySize;
    }

    public Host getHost() {
        return host;
    }

    public Integer getCpuCount() {
        return cpuCount;
    }

    public Double getDiskSize() {
        return diskSize;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public void setCpuCount(Integer cpuCount) {
        this.cpuCount = cpuCount;
    }

    public void setDiskSize(Double diskSize) {
        this.diskSize = diskSize;
    }

    public int getReservationCEE() {
        return reservationCEE;
    }

    public int getReservationID() {
        return reservationID;
    }

    /*public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }*/

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id.toString());
        o.put("nCpu", cpuCount.toString());
        o.put("fCpu", cpuSpeed.toString());
        o.put("mem", memorySize.toString());
        o.put("disk", diskSize.toString());
        
        o.put("host", host.getId());
        return o;
    }
    
    public boolean equals(VmSlot obj){
        if(obj==null){
            return false;
        }
        //System.out.println("1:"+(this.id==obj.getId()));
        //System.out.println("2:"+(obj.getCpuCount()==this.cpuCount));
        //System.out.println("3:"+(obj.getMemorySize()==this.memorySize));
        //System.out.println("4:"+(obj.getDiskSize()==this.diskSize));
        //System.out.println("5:"+(obj.getCpuSpeed()==this.cpuSpeed));
        if(((((this.id==obj.getId())&&
                (obj.getCpuCount()==this.cpuCount))
                &&
                (obj.getMemorySize()==this.memorySize))
                &&
                (obj.getDiskSize()==this.diskSize))
                &&
               (obj.getCpuSpeed()==this.cpuSpeed)){
                    return true;
        }else{
            
           return false;
        }
    }

    public VmReservationSlot toVmReservationSlot() {
        VmReservationSlot vm;
        vm=new VmReservationSlot();
        vm.setCpuCount(cpuCount);
        vm.setCpuSpeed(cpuSpeed);
        vm.setMemorySize(memorySize);
        vm.setDiskSize(diskSize);
        vm.setId(this.resBelongTo);
        vm.addSingleReservation(host);
        
        //vm.setHost(host);
        return vm;
        //throw new UnsupportedOperationException("Not yet implemented");
    }

    public void addNotCountry(String country) {
        notCountry.add(country);
    }
     public void addInCountry(String country) {
        inCountry.add(country);
    }
     public boolean checkCountry(String country){
        boolean check=true;
        //System.out.println("NOT COUNTRY:"+notCountry.size());
         //System.out.println("IN COUNTRY:"+inCountry.size());
        if(notCountry.size()!=0){
            for(String ctr:notCountry){
                if(ctr.equalsIgnoreCase(country))
                    return false;
            }
        }
         if(inCountry.size()!=0){
             check=false;
            for(String ctr:inCountry){
                if(ctr.equalsIgnoreCase(country))
                    return true;
            }
        }
        
        return check; 
     }

    public void addNotHostList(ArrayList<VmSlot> vmHost) {
         vmNotSameHost=vmHost;
    }
    public boolean checkHost(Host host){
        if(vmNotSameHost!=null){
            for(VmSlot vms:vmNotSameHost){
                if(vms.hasHost()){
                    if(host.getId()==vms.host.getId()){
                        return false;
                    }
                }
            }
        }
        return true;
    }

     private void addSameRackList(ArrayList<VmSlot> vmRack) {
        vmSameRack=vmRack;
    }
    
    public void addNotClusterList(ArrayList<VmSlot> vmHost) {
         vmNotSameHost=vmHost;
    }
    public boolean checkCluster(Host host){
        if(vmNotSameHost!=null){
            for(VmSlot vms:vmNotSameHost){
                if(vms.hasHost()){
                    if(host.getRack().getCluster().getId()==vms.host.getRack().getCluster().getId()){
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public boolean rackConstraint(){
        if((vmSameRack.size()==0)||(vmSameRack==null)){
            return false;
        }
        return true;
    }
    
    public boolean hasHost() {
        if(host!=null){
            return true;
        }else{
            return false;
        }
    }

    public VmSlot[] getSameRackVMs() {
        VmSlot[] vms=new VmSlot[vmSameRack.size()];
        for(int i=0;i<vmSameRack.size();i++){
            vms[i]=vmSameRack.get(i);
        }
        return vms;
    }

    public String getCountry() {
        String countryString="COUNTRY_SAVED: ";
       if(notCountry.size()!=0){
           countryString+="___NOTINCOUNTRY_____";
            for(String ctr:notCountry){
                countryString+="/"+ctr;
            }
        }
         if(inCountry.size()!=0){
             countryString+="___INCOUNTRY_____";
            for(String ctr:inCountry){
                countryString+="/"+ctr;
            }
        }
        return countryString;
    }    
    void setReservationBelongsTo(int idR) {
        this.resBelongTo=idR;
    }
    int getReservationBelongsTo() {
        return this.resBelongTo;
    }


   
         
 }
