package org.ow2.contrail.provider.scheduler.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.utils.DateUtils;

import javax.persistence.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

@Entity
@Table(name="RESERVATION")
@NamedQueries({
        @NamedQuery(name = "Reservation.findAll", query = "SELECT r FROM Reservation r"),
        @NamedQuery(name = "Reservation.maxID", query = "SELECT max(r.id) FROM Reservation r"),
 //       @NamedQuery(name = "Reservation.activeReservation", query = "SELECT r FROM Reservation r WHERE r.endTime > :time")
        
})
public class Reservation {
    
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date submitTime;
  /*  @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;*/
    @OneToMany(mappedBy = "reservation", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private Collection<VmReservationSlot> vmReservations;
    
    @Transient
    private JSONArray constraints;

    public JSONArray getConstraints() {
        return constraints;
    }

    public void setConstraints(JSONArray constraints) {
        this.constraints = constraints;
    }

    public Reservation() {
    }

    public Reservation(JSONObject json) throws JSONException {
        try {
            
          /*  EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Reservation.maxID");
           System.out.println("GETTING ID");
            List<Integer> idList = query.getResultList();
            System.out.println("ID:"+idList.size());
            if(idList.size()==0){
               id=1L;    
            }else{
                id=idList.get(0)+1L;
            } 
            PersistenceUtils.getInstance().closeEntityManager(em);*/
           // System.out.println(json.getString("id"));
            this.constraints=json.getJSONArray("constraints");
            this.id=new Long(json.getString("id"));
            if (json.has("submitTime")) {
                String submitTimeString = json.getString("submitTime");
                this.submitTime = DateUtils.deserialize(submitTimeString);
            }
            else {
                this.submitTime = new Date();
            }
            
          /*  String startTimeString = json.getString("startdate");
            this.startTime = DateUtils.deserialize(startTimeString);
            String endTimeString = json.getString("enddate");
            this.endTime = DateUtils.deserialize(endTimeString);*/

            JSONArray vmArray = json.getJSONArray("reservations");
            this.vmReservations = new ArrayList<VmReservationSlot>();
            for (int i = 0; i < vmArray.length(); i++) {
                JSONObject vmSpec = vmArray.getJSONObject(i);
                //int numberOFReservation;
             /*   int numberOfReservation = Integer.parseInt((String)vmSpec.get("count"));
                for(int k=0;k<numberOfReservation;k++){*/
                VmReservationSlot vmReservation = new VmReservationSlot(vmSpec);
                vmReservation.setReservation(this);
                this.vmReservations.add(vmReservation);
               // }
                
            }
        }
        catch (ParseException e) {
            throw new JSONException(e);
        }

    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    

    public Collection<VmReservationSlot> getVmReservations() {
        return vmReservations;
    }

    public void setVmReservations(Collection<VmReservationSlot> vmReservations) {
        this.vmReservations = vmReservations;
        for(VmReservationSlot elem: vmReservations){
            elem.setReservation(this);
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("submitTime", DateUtils.serialize(submitTime));
        //o.put("startTime", DateUtils.serialize(startTime));
        //o.put("endTime", DateUtils.serialize(endTime));

        JSONArray vmArray = new JSONArray();
        for (VmReservationSlot vmReservation : this.vmReservations) {
            vmArray.put(vmReservation.toJSON());
        }
        o.put("reservations", vmArray);
        return o;
    }

  
}
