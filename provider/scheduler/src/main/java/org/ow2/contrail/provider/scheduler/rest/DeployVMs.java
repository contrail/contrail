/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Reservation;
import org.ow2.contrail.provider.scheduler.entities.VmReservationSlot;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import org.ow2.contrail.provider.scheduler.entities.VmSlot;

/**
 *
 * @author fgaudenz
 */

@Path("deploy/vms")
public class DeployVMs {
    

  /*  @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getVmReservation(@PathParam("id") int vmId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        System.out.println("GET OF VM "+vmId);
        VmSlot vm = em.find(VmSlot.class, vmId);
        if (vm == null) {
            PersistenceUtils.getInstance().closeEntityManager(em);
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        JSONObject json = vm.toJSON();
        PersistenceUtils.getInstance().closeEntityManager(em);
        return Response.ok(json).build();
       }*/
      
          	  
       
 

    @DELETE
    @Path("/{deploymentId}")
    public Response removeVmReservation(@PathParam("deploymentId") int vmId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        VmSlot vm = em.find(VmSlot.class, vmId);
        if (vm == null) {
            PersistenceUtils.getInstance().closeEntityManager(em);
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        System.out.println("DELETING VM:"+vmId);
        em.getTransaction().begin();
        em.remove(vm);
        em.getTransaction().commit();
        //JSONObject json = vm.toJSON();
        PersistenceUtils.getInstance().closeEntityManager(em);
        return Response.ok().build();
   }
    
}