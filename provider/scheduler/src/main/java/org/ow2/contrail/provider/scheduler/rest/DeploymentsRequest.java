package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.entities.VmSlot;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.ow2.contrail.provider.scheduler.entities.Cluster;
import org.ow2.contrail.provider.scheduler.entities.Host;
import org.ow2.contrail.provider.scheduler.entities.Rack;
import org.ow2.contrail.provider.scheduler.entities.SingleVMRes;
import org.ow2.contrail.provider.scheduler.utils.DBlock;
import org.ow2.contrail.provider.scheduler.utils.Scheduler;

@Path("/deploy")
public class DeploymentsRequest {

    @GET
    @Produces("application/json")
    public Response getReservations() throws JSONException {
        /*EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createNamedQuery("Reservation.findAll");
            List<VMSlot> reservationList = query.getResultList();

            JSONArray jsonArray = new JSONArray();
            for (Reservation reservation : reservationList) {
                URI uri = UriBuilder.fromResource(ReservationsResource.class)
                        .path(reservation.getId().toString())
                        .build();
                JSONObject o = reservation.toJSON();
                o.put("uri", uri);
                jsonArray.put(o);
            }
            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }*/return Response.ok().build();
    }
    
    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response addDeployment(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            //EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            JSONObject json = new JSONObject(request);
            JSONArray vmArray = json.getJSONArray("VMs");
            //System.out.println("NUMERO VMS:"+vmArray.length());
            ArrayList<VmSlot> vmScheduled=new ArrayList<VmSlot>();
            ArrayList<VmSlot> vms=new ArrayList<VmSlot>();
                    //new VmSlot[vmArray.length()];//change to arraylist
            
            for(int i=0;i<vmArray.length();i++){
                VmSlot appVM=new VmSlot(vmArray.getJSONObject(i));
                
                VmSlot confVM=em.find(VmSlot.class, appVM.getId());                             
                if(appVM.equals(confVM)){
                    //System.out.println("UGUALE");
                    vmScheduled.add(appVM);
                }else{
                    //System.out.println("non uguale");
                    if(confVM!=null){
                        //System.out.println("cancellata");
                        DBlock.deleteVM(confVM);
                    }
                    
                    //      em.find(VmSlot.class, )
                vms.add(appVM);//new VmSlot(vmArray.getJSONObject(i));
            
                }
            }
            
            
            
            if(json.has("constraints"))
                VmSlot.processConstraints(vms,json.getJSONArray("constraints"));
            
            
            
            VmSlot[] VMs=new VmSlot[vms.size()];
            for(int i=0;i<VMs.length;i++)
                 VMs[i]=vms.get(i);
            VMs=(VmSlot[]) Scheduler.executeCommand(Scheduler.DEPLOY, VMs);
            //System.out.println("Scheduled :"+VMs.length);
            JSONArray array = new JSONArray();
            for (VmSlot vm : VMs) {
                URI vmuri = UriBuilder.fromResource(DeployVMs.class)
                        .path(vm.getId().toString())
                        .build();
                Host host=vm.getHost();
                String uriH=null;
                if(host!=null){
                Rack rack=host.getRack();
                Cluster cluster = rack.getCluster(); 
                Datacenter datacenter=cluster.getDatacenter();  
                uriH="datacenters/"+datacenter.getId()+"/clusters/"+cluster.getId()+"/racks/"+rack.getId()+"/host/"+host.getId();
                //System.out.println("STORING THE VMS");
                try{ 
                    SingleVMRes single=null;
                    if(vm.getReservations()!=null){
                    single=em.find(SingleVMRes.class,vm.getReservations().get(0).getId());
                    }
                    vm.freeReservation();
                    em.getTransaction().begin();
                    if(single!=null)
                        single.setVm(vm);
                    em.persist(vm);
                    em.getTransaction().commit();
                }catch(Exception ex){
                    System.out.println(ex);   
                }
                
                }
                JSONObject o = new JSONObject();
                o.put("vm", vmuri);
                o.put("host", uriH);
                //o.put("uri", uri);
                
                //o.put("country", datacenter.getCountry());
                array.put(o);
                //PersistenceUtils.getInstance().closeEntityManager(em);
            }
            for(int i=0;i<vmScheduled.size();i++){
                VmSlot vm=vmScheduled.get(i); 
                URI vmuri = UriBuilder.fromResource(DeployVMs.class)
                        .path(vm.getId().toString())
                        .build();
                Host host=vm.getHost();
                Rack rack=host.getRack();
                Cluster cluster = rack.getCluster(); 
                Datacenter datacenter=cluster.getDatacenter();  
                String uriH="datacenters/"+datacenter.getId()+"/clusters/"+cluster.getId()+"/racks/"+rack.getId()+"/host/"+host.getId();
                JSONObject o = new JSONObject();
                o.put("vm", vmuri);
                o.put("host", uriH);
                array.put(o);
            }   
            return Response.ok(array.toString()).build();
            //prepare json
            
            
            
            
            
            
            
            
            
            
           
            
            /*VmSlot vmList = new VmSlot(json);

            em.getTransaction().begin();
            em.persist(reservation);
            em.getTransaction().commit();
*/
            //URI resourceUri = new URI("bello bello");
           // return Response.status(Response.Status.OK).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
      
    /*
    @Path("/{reservationId}")
    public ReservationResource findReservation(@PathParam("reservationId") long reservationId) {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Reservation reservation = em.find(Reservation.class, reservationId);
            if (reservation == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            else {
                return new ReservationResource(reservation);
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
*/
}
