/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.entities;

/**
 *
 * @author fgaudenz
 */
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="SINGLERESERVATIONSLOT")
public class SingleVMRes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @ManyToOne(fetch = FetchType.LAZY)
    Host host;
    @ManyToOne(fetch = FetchType.LAZY)
    VmReservationSlot vmreservation;
    @ManyToOne(fetch = FetchType.LAZY)
    VmSlot vmslot=null;

    public int getId() {
        return id;
    }

    public VmReservationSlot getVmreservation() {
        return vmreservation;
    }

    
    public VmSlot getVmslot() {
        return vmslot;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public VmReservationSlot getReservation() {
        return vmreservation;
    }

    public void setReservation(VmReservationSlot reservation) {
        this.vmreservation = reservation;
    }

    public VmSlot getVm() {
        return vmslot;
    }

    public void setVm(VmSlot vm) {
        this.vmslot = vm;
    }
    public VmSlot toVmSlot(){
        VmSlot vm=new VmSlot();
        vm.setId((vmreservation.getId()*-1));
        vm.setCpuSpeed(vmreservation.getCpuSpeed());
        vm.setCpuCount(vmreservation.getCpuCount());
        vm.setMemorySize(vmreservation.getMemorySize());
        vm.setDiskSize(vmreservation.getDiskSize());
        vm.setHost(host);
        return vm;
    }
    
}