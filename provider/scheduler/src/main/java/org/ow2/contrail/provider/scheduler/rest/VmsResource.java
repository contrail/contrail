package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Reservation;
import org.ow2.contrail.provider.scheduler.entities.VmReservationSlot;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/vms")
public class VmsResource {

    @GET
    @Path("/{vmId}")
    @Produces("application/json")
    public Response getVmReservation(@PathParam("vmId") int vmId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            VmReservationSlot vmReservation = em.find(VmReservationSlot.class, vmId);
            if (vmReservation == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = vmReservation.toJSON();
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    @Path("/{vmId}")
    public Response removeVmReservation(@PathParam("vmId") int vmId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            VmReservationSlot vmReservation = em.find(VmReservationSlot.class, vmId);
            if (vmReservation == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            Reservation reservation = vmReservation.getReservation();
            if (reservation.getVmReservations().size() == 1) {
                em.remove(reservation);
            }
            else {
                em.remove(vmReservation);
            }
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
