package org.ow2.contrail.provider.scheduler.entities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import org.json.JSONArray;
import org.ow2.contrail.provider.scheduler.utils.DateUtils;

@Entity
@Table(name="VMRESERVATION")
@NamedQueries({
        @NamedQuery(name = "VmReservationSlot.findAll", query = "SELECT r FROM VmReservationSlot r"),
        @NamedQuery(name = "VmReservationSlot.activeReservation", query = "SELECT r FROM VmReservationSlot r WHERE r.endTime > :time")
        })
public class VmReservationSlot {
    @Id
    private Integer id;
    private Integer cpuCount;
    private Double cpuSpeed;
    private Double memorySize;
    private Double diskSize;
    private int slots=0;
    @Column(name = "STARTTIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Column(name = "ENDTIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @ManyToOne(optional = false)
    private Reservation reservation;
    @OneToMany(mappedBy = "vmreservation", cascade = CascadeType.PERSIST)
    private List<SingleVMRes> vmslots;
    
    
    public VmReservationSlot() {
        vmslots=new ArrayList<SingleVMRes>();
    }

    public VmReservationSlot(JSONObject json) throws JSONException, ParseException {
        String endTimeString = json.getString("enddate");
        this.endTime = DateUtils.deserialize(endTimeString);
        System.out.println("DATE "+endTimeString+" "+this.endTime);
        String startTimeString = json.getString("startdate");
        this.startTime = DateUtils.deserialize(startTimeString);
        this.id = json.getInt("id");
        this.cpuCount = json.getInt("nCpu");
        this.cpuSpeed = json.getDouble("fCpu");
        this.memorySize = json.getDouble("mem");
        this.diskSize = json.getDouble("diskSize");
        this.slots=json.getInt("count");
        
        
        
        vmslots=new ArrayList<SingleVMRes>();
        for(int i=0;i<slots;i++){
            SingleVMRes app=new SingleVMRes();
            app.setReservation(this);
            vmslots.add(app);
            
            
        }
    }
    
    public void addSingleReservation(Host host){
            slots++;
            SingleVMRes app=new SingleVMRes();
            app.setReservation(this);
            app.setHost(host);
            vmslots.add(app);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(Integer cpuCount) {
        this.cpuCount = cpuCount;
    }

    public Double getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(Double cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public Double getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(Double memorySize) {
        this.memorySize = memorySize;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public void setDiskSize(Double diskSize) {
        this.diskSize = diskSize;
    }


    public Double getDiskSize() {
        return diskSize;
    }

    public int getSlots() {
        return slots;
    }

    public List<SingleVMRes> getVmslots() {
        return vmslots;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }


    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("nCpu", cpuCount);
        o.put("fCpu", cpuSpeed);
        o.put("mem", memorySize);
        o.put("diskSize", diskSize);
        //o.put("reservationId", reservation.getId());
        o.put("count",slots);
        //JSONArray vmArray = new JSONArray();
        int i=0;
        for (SingleVMRes vm : this.vmslots) {
            if(vm.vmslot==null)
                i++;
        }
        o.put("free", i);
        
        
        return o;
    }

    public VmSlot[] toVmSlot(int starID) {
       // System.out.println("NSLOTS:"+slots);
       // System.out.println("ID:"+this.id);
        VmSlot[] vms=new VmSlot[slots];
        for(int i=0;i<slots;i++){
        //VmSlot vm=new VmSlot();
        vms[i]=new VmSlot();
        vms[i].setId(starID+i);
        vms[i].setCpuSpeed(cpuSpeed);
        vms[i].setCpuCount(cpuCount);
        vms[i].setMemorySize(memorySize);
        vms[i].setDiskSize(diskSize);
        vms[i].setHost(null);
        vms[i].setReservationBelongsTo(id);
        }
        return vms;
    }
    


   
}
