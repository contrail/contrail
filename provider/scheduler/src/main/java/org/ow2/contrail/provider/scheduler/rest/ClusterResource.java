package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Cluster;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.entities.Rack;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

public class ClusterResource {
    private static String URI_TEMPLATE = "/datacenters/%d/clusters/%d";

    private Datacenter datacenter;
    private Cluster cluster;
    String uri;

    public ClusterResource(Datacenter datacenter, Cluster cluster) {
        this.datacenter = datacenter;
        this.cluster = cluster;
        this.uri = String.format(URI_TEMPLATE, datacenter.getId(), cluster.getId());
    }

    @GET
    @Produces("application/json")
    public Response getCluster() throws JSONException {
        //System.out.println("clusterCall");
        JSONObject json = cluster.toJSON();
        return Response.ok(json.toString()).build();
    }

    @DELETE
    public Response removeCluster() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            cluster = em.merge(cluster);
            em.remove(cluster);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Path("/racks")
    @Produces("application/json")
    public Response getRacks() throws JSONException {
        JSONArray array = new JSONArray();
        for (Rack rack : cluster.getRacks()) {
            JSONObject o = new JSONObject();
            o.put("name", rack.getName());
            o.put("uri", uri + "/racks/" + rack.getId());
            array.put(o);
        }
        return Response.ok(array.toString()).build();
    }

    @POST
    @Path("/racks")
    @Consumes("application/json")
    public Response addRack(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            JSONObject json = new JSONObject(request);
            Rack rack = new Rack(json);
            rack.setCluster(cluster);

            em.getTransaction().begin();
            em.persist(rack);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", rack.getId()));
            return Response.created(resourceUri).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @Path("/racks/{rackId}")
    public RackResource findRack(@PathParam("rackId") int rackId) {
        for (Rack rack : cluster.getRacks()) {
            if (rack.getId() == rackId) {
                return new RackResource(datacenter, cluster, rack);
            }
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

}
