package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Cluster;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.entities.Host;
import org.ow2.contrail.provider.scheduler.entities.Rack;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ow2.contrail.provider.scheduler.utils.DBManagement;
import org.ow2.contrail.provider.scheduler.utils.DBlock;

public class HostResource {
    private Datacenter datacenter;
    private Cluster cluster;
    private Rack rack;
    private Host host;

    public HostResource(Datacenter datacenter, Cluster cluster, Rack rack, Host host) {
        this.datacenter = datacenter;
        this.cluster = cluster;
        this.rack = rack;
        this.host = host;
    }

    @GET
    @Produces("application/json")
    public Response getHost() throws JSONException {
        JSONObject json = host.toJSON();
        return Response.ok(json.toString()).build();
    }

    @DELETE
    public Response removeHost() throws JSONException {
        DBManagement.deleteHost(host);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

}
