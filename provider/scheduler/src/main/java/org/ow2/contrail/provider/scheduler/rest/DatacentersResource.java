package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.LockModeType;

@Path("/datacenters")
public class DatacentersResource {

    @GET
    @Produces("application/json")
    public Response getDatacenters() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            
            Query query = em.createNamedQuery("Datacenter.findAll");
          // em.getTransaction().begin();
           // query.setLockMode(LockModeType.READ);
            List<Datacenter> datacenterList = query.getResultList();
            
            
            
            
            /*try {
                //em.lock(datacenterList, LockModeType.PESSIMISTIC_READ);
                Thread.sleep(100000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(DatacentersResource.class.getName()).log(Level.SEVERE, null, ex);
                * 
                * <property name="eclipselink.ddl-generation" value="create-tables"/>
                * 
            }*/
           // em.getTransaction().commit();
            JSONArray array = new JSONArray();
            for (Datacenter datacenter : datacenterList) {
                URI uri = UriBuilder.fromResource(DatacentersResource.class)
                        .path(datacenter.getId().toString())
                        .build();
                JSONObject o = new JSONObject();
                o.put("name", datacenter.getName());
                o.put("country", datacenter.getCountry());
                o.put("uri", uri);
                
                //o.put("country", datacenter.getCountry());
                array.put(o);
                
            }
            return Response.ok(array.toString()).build();
        }finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    public Response addDatacenter(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            JSONObject json = new JSONObject(request);
            Datacenter datacenter = new Datacenter(json);

            em.getTransaction().begin();
            em.persist(datacenter);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", datacenter.getId()));
            return Response.created(resourceUri).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @Path("/{datacenterId}")
    public DatacenterResource findDatacenter(@PathParam("datacenterId") int datacenterId) {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Datacenter datacenter = em.find(Datacenter.class, datacenterId);
            if (datacenter == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            else {
                return new DatacenterResource(datacenter);
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
