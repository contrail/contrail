package org.ow2.contrail.provider.scheduler.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Cluster {
    @Id
    private Integer id;
    private String name;
    @ManyToOne
    private Datacenter datacenter;
    @OneToMany(mappedBy = "cluster", fetch = FetchType.LAZY)
    private Collection<Rack> racks;
    private String interconnect_id;

    public Cluster() {
    }

    public Cluster(JSONObject o) throws JSONException {
        this.id = o.getInt("id");
        this.name = o.getString("name");
        this.interconnect_id=o.getString("interconnect");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Datacenter getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(Datacenter datacenter) {
        this.datacenter = datacenter;
    }

    public Collection<Rack> getRacks() {
        return racks;
    }

    public void setRacks(Collection<Rack> racks) {
        this.racks = racks;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("name", name);
        o.put("interconnect", interconnect_id);
        return o;
    }
}
