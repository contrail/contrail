package org.ow2.contrail.provider.scheduler.entities;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Rack.findAll", query = "SELECT d FROM Rack d")
})
public class Rack {
    @Id
    private Integer id;
    private String name;
    @ManyToOne
    private Cluster cluster;
    @OneToMany(mappedBy = "rack", fetch = FetchType.LAZY)
    private List<Host> hosts;

    public Rack() {
    }

    public Rack(JSONObject o) throws JSONException {
        this.id = o.getInt("id");
        this.name = o.getString("name");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public List<Host> getHosts() {
        return hosts;
    }

    public void setHosts(List<Host> hosts) {
        this.hosts = hosts;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("name", name);
        return o;
    }
}
