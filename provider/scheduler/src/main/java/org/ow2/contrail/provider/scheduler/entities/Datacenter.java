package org.ow2.contrail.provider.scheduler.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

@Entity
@NamedQueries({
        @NamedQuery(name = "Datacenter.findAll", query = "SELECT d FROM Datacenter d"),
        @NamedQuery(name = "Datacenter.maxID", query = "SELECT max(d.id) FROM Datacenter d")
})
public class Datacenter {
    @Id
    private Integer id;
    private String name; 
    //private String country_id;
     @ManyToOne
    private Country country;
    @OneToMany(mappedBy = "datacenter", fetch = FetchType.LAZY)
    private Collection<Cluster> clusters;

    @Version
    private int version;
    
    
    public Datacenter() {
    }

    public Datacenter(JSONObject o) throws JSONException {
       // System.out.println(o.getString("id"));
        if(o.getString("id")==null){
            EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Datacenter.maxID");
            List<Integer> idList = query.getResultList();
            if(idList.size()==0){
                this.id=1;    
            }else{
                this.id=idList.get(0)+1;
            }
        }else{    
            this.id = o.getInt("id");
        }
        this.name = o.getString("name");
        if(o.getString("country")!=null){
            String country_id=o.getString("country");
            EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Country.findByCode");
            query.setParameter("code", country_id);
            List<Country> countryList = query.getResultList();
            country=countryList.get(0);
        }else{
            country=null;
        }
        
        //TODO add value
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public String getCountry(){
        return country.getCode();
    }
    public void setName(String name) {
        this.name = name;
    }

    public Collection<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(Collection<Cluster> clusters) {
        this.clusters = clusters;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("name", name);
        o.put("country",this.getCountry());
        return o;
    }
}
