package eu.contrail.reservations.simple.base;

public class VirtualMachine {
    public String vmId;
    public String address;
    public double memoryMB;

    public VirtualMachine(String vmId, String address, double memoryMB) {
	super();
	this.vmId = vmId;
	this.address = address;
	this.memoryMB = memoryMB;
    }
}
