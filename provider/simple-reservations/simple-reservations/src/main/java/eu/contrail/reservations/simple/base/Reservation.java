package eu.contrail.reservations.simple.base;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class Reservation {
    private static Logger logger = Logger.getLogger(Reservation.class);

    String userId;
    GregorianCalendar startTime;
    GregorianCalendar endTime;
    VirtualMachine vm;

    public Reservation(String userId, GregorianCalendar startTime,
	    GregorianCalendar endTime, VirtualMachine vm) throws Exception {
	super();
	this.userId = userId;
	this.startTime = startTime;
	this.endTime = endTime;
	this.vm = vm;
	if (!startTime.before(endTime))
	    throw new Exception("Start time should be before end time.");
    }

    public boolean disjoint(Reservation reservation) {
	logger.trace("Reservation 1: " + this.startTime.getTimeInMillis()
		+ " - " + this.endTime.getTimeInMillis());
	logger.trace("Reservation 2: "
		+ reservation.startTime.getTimeInMillis() + " - "
		+ reservation.endTime.getTimeInMillis());
	ArrayList<GregorianCalendar> times = new ArrayList<GregorianCalendar>();

	return (this.startTime.before(reservation.startTime)
		&& this.endTime.before(reservation.startTime) || reservation.startTime
		.before(this.startTime)
		&& reservation.endTime.before(this.startTime));
    }

}
