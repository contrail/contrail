package eu.contrail.reservations.simple.core;

import java.util.ArrayList;
import java.util.Hashtable;

import eu.contrail.reservations.simple.base.Host;
import eu.contrail.reservations.simple.base.Reservation;

public class Timetable {
    private final Hashtable<Host, ArrayList<Reservation>> reservations;

    public Timetable() {
	this.reservations = new Hashtable<Host, ArrayList<Reservation>>();
    }

    public void addHost(Host host) {
	if (!this.reservations.contains(host)) {
	    this.reservations.put(host, new ArrayList<Reservation>());
	}
    }

    public boolean addReservation(Host host, Reservation reservation) {
	ArrayList<Reservation> resOnHost = this.reservations.get(host);
	if (resOnHost != null) {
	    for (Reservation resIn : resOnHost) {
		if (!resIn.disjoint(reservation)) {
		    return false;
		}
	    }
	}
	resOnHost.add(reservation);
	return true;
    }

    public int getSlotCount(Host host) {
	int retval = -1;

	ArrayList<Reservation> hostRes = this.reservations.get(host);
	if (hostRes != null) {
	    retval = hostRes.size();
	}

	return retval;
    }
}
