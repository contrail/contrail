package eu.contrail.reservations.simple.base;

public class Host {
    public String hostId;
    public String address;
    public double memoryGB;
    public int cpuCount;

    public Host(String hostId, String address, double memoryGB, int cpuCount) {
	super();
	this.hostId = hostId;
	this.address = address;
	this.memoryGB = memoryGB;
	this.cpuCount = cpuCount;
    }
}
