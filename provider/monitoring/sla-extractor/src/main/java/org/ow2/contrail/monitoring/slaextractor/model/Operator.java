package org.ow2.contrail.monitoring.slaextractor.model;

public enum Operator {
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN,
    LESS_THAN_OR_EQUAL,
    EQUAL,
    NOT_EQUAL,
    AND,
    OR
}
