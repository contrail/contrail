package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;

public class Quantity implements Operand {
    @Element
    private String value;

    @Element
    private String unit;

    public Quantity(String value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
