package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

public class OVF implements SLOTarget {
    @Element
    private String url;

    public OVF(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
