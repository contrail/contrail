package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Root
public class MonitoringConfiguration {
    @Element
    String uuid;

    @Element
    Date effectiveFrom;

    @Element
    Date effectiveUntil;

    @ElementList()
    ArrayList<ServiceLevelObjective> serviceLevelObjectives;

    public MonitoringConfiguration() {
        serviceLevelObjectives = new ArrayList<ServiceLevelObjective>();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Date getEffectiveUntil() {
        return effectiveUntil;
    }

    public void setEffectiveUntil(Date effectiveUntil) {
        this.effectiveUntil = effectiveUntil;
    }

    public List<ServiceLevelObjective> getServiceLevelObjectives() {
        return serviceLevelObjectives;
    }

    public void setServiceLevelObjectives(ArrayList<ServiceLevelObjective> serviceLevelObjectives) {
        this.serviceLevelObjectives = serviceLevelObjectives;
    }

    public void addServiceLevelObjective(ServiceLevelObjective slo) {
        this.serviceLevelObjectives.add(slo);
    }

    public String toXml() throws Exception {
        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        serializer.write(this, writer);

        return writer.toString();
    }
}
