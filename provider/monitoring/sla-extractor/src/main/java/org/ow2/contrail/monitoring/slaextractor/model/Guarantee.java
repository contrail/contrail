package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class Guarantee implements Obligation {
    @Attribute
    private String id;

    //@Element
    SLOValidity validity;

    @Element
    Constraint constraint;

    public Guarantee(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SLOValidity getValidity() {
        return validity;
    }

    public void setValidity(SLOValidity validity) {
        this.validity = validity;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }
}
