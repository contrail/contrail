package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementUnion;
import org.simpleframework.xml.Path;

public class Expression implements Operand {
    @Element
    private Operator operator;

    @Element
    @ElementUnion({
            @Element(name = "metric", type = Metric.class),
            @Element(name = "expression", type = Expression.class),
            @Element(name = "quantity", type = Quantity.class)
    })
    @Path("operand[1]")
    private Operand op1;

    @Element
    @Path("operand[2]")
    @ElementUnion({
            @Element(name = "metric", type = Metric.class),
            @Element(name = "expression", type = Expression.class),
            @Element(name = "quantity", type = Quantity.class)
    })
    private Operand op2;

    public Expression(Operator operator, Operand op1, Operand op2) {
        this.operator = operator;
        this.op1 = op1;
        this.op2 = op2;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Operand getOp1() {
        return op1;
    }

    public void setOp1(Operand op1) {
        this.op1 = op1;
    }

    public Operand getOp2() {
        return op2;
    }

    public void setOp2(Operand op2) {
        this.op2 = op2;
    }
}
