package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;

public class Constraint {
    @Element
    private Expression expression;

    public Constraint(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
