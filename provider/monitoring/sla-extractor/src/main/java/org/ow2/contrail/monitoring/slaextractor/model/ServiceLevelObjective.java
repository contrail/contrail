package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;
import java.util.List;

public class ServiceLevelObjective {
    @Attribute
    private String id;

    @ElementList
    private ArrayList<Guarantee> guarantees;

    public ServiceLevelObjective(String id) {
        this.id = id;
        guarantees = new ArrayList<Guarantee>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Guarantee> getGuarantees() {
        return guarantees;
    }

    public void setGuarantees(ArrayList<Guarantee> guarantees) {
        this.guarantees = guarantees;
    }

    public void addGuaranteed(Guarantee guarantee) {
        this.guarantees.add(guarantee);
    }
}
