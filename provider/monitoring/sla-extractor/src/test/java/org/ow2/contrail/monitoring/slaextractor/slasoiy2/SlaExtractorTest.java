package org.ow2.contrail.monitoring.slaextractor.slasoiy2;

import eu.slaatsoi.slamodel.SLADocument;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.ow2.contrail.monitoring.slaextractor.model.*;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class SlaExtractorTest {
    private static Logger log = Logger.getLogger(SlaExtractorTest.class);

    @Test
    public void testSyntaxConverter() throws Exception {
        log.info("testSyntaxConverter() started.");
        String xml = readFileAsString("src/test/resources/A4-SLA-1.xml");
        SLADocument slaDoc = SLADocument.Factory.parse(xml);

        SLASOIParser slasoiParser = new SLASOIParser();
        SLA sla = slasoiParser.parseSLA(slaDoc.xmlText());

        assertEquals(sla.getUuid().toString(), "OneVMTypeSLAForSimulation");
        assertEquals(sla.getParty("ID-OF-PROVIDER-PARTY-GOES-HERE").getAgreementRole().toString(),
                "provider");
        assertEquals(((org.slasoi.slamodel.sla.Guaranteed.State) sla.getAgreementTerm("OverAllAvailability").getGuaranteed("AvailabilityState")).getState().toString(),
                "availability(VM_Access_Point) > \"90\" percentage");
        log.info("testSyntaxConverter() finished successfully.");
    }

    @Test
    public void testExtractingInfrastructureSla() throws Exception {
        /*log.info("testExtractingInfrastructureSla() started.");
        String xml = readFileAsString("src/test/resources/A4-SLA-1.xml");
        SLADocument slaDoc = SLADocument.Factory.parse(xml);

        SlaExtractor slaExtractor = new SlaExtractor(slaDoc.xmlText());
        MonitoringConfiguration monConf = slaExtractor.extractQoSConstraints();

        assertEquals(monConf.getUuid(), "OneVMTypeSLAForSimulation");
        assertEquals(monConf.getServiceLevelObjectives().size(), 2);
        assertNotNull(monConf.getEffectiveFrom());
        assertNotNull(monConf.getEffectiveUntil());

        ServiceLevelObjective slo1 = monConf.getServiceLevelObjectives().get(0);
        assertEquals(slo1.getId(), "OverAllAvailability");
        assertEquals(slo1.getGuarantees().size(), 1);

        Guarantee guaranteed1 =
                slo1.getGuarantees().get(0);
        assertEquals(guaranteed1.getId(), "AvailabilityState");

        SLOValidity sloValidity = guaranteed1.getValidity();
        assertNotNull(sloValidity.getEffectiveFrom());
        assertNotNull(sloValidity.getEffectiveUntil());
        assertEquals(sloValidity.getSchedule(), SLOValidity.SLOValidityType.NON_STOP);

        Constraint expression = guaranteed1.getConstraint();
        assertEquals(expression.getUnit(), "percentage");

        Predicate predicate = expression.getPredicate();
        assertEquals(predicate.getType(), "greater_than");
        assertEquals(predicate.getValue(), "90");

        Metric metric = predicate.getMetric();
        assertEquals(metric.getName(), "availability");

        SLOTarget target = metric.getTarget();
        assertEquals(target.getType(), SLOTarget.Type.INFRASTRUCTURE_SERVICE);
        assertEquals(target.getSid(), "VM_Access_Point");

        log.trace(monConf.toXml());
        log.info("testExtractingInfrastructureSla() finished successfully.");*/
    }

    @Test
    public void testGetOVFs() throws Exception {
        log.info("testGetOVFs() started.");
        String xml = readFileAsString("src/test/resources/lamp-SLA.xml");
        SlaExtractor slaExtractor = new SlaExtractor(xml);

        List<String> ovfList = slaExtractor.getOVFs();
        assertEquals(ovfList.size(), 1);
        assertEquals(ovfList.get(0), "http://contrail.xlab.si/ovfs/lamp.ovf");

        log.info("testGetOVFs() finished successfully.");
    }

    @Test
    public void testExtractQoSConstraints() throws Exception {
        log.info("testExtractQoSConstraints() started.");
        String xml = readFileAsString("src/test/resources/ubuntu-test-xlab-SLA.xml");
        SlaExtractor slaExtractor = new SlaExtractor(xml);

        MonitoringConfiguration monConf = slaExtractor.extractQoSConstraints();
        log.trace(monConf.toXml());

        assertEquals(monConf.getUuid(), "Ubuntu-test-xlab-SLA");
        assertEquals(monConf.getServiceLevelObjectives().size(), 3);
        assertNotNull(monConf.getEffectiveFrom());
        assertNotNull(monConf.getEffectiveUntil());

        log.info("testExtractQoSConstraints() finished successfully.");
    }

    private static String readFileAsString(String filePath) throws java.io.IOException {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        FileInputStream f = new FileInputStream(filePath);
        f.read(buffer);
        return new String(buffer);
    }
}
