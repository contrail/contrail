require 'rubygems'
require 'amqp'
require 'pp'
require 'ostruct'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
    
    strListeners = {
      "registration.metrics" => "registration.metrics.#",
      "slaextractor.constraints" => "slaextractor.constraints.#",
      "input.host" => "input.host.#",
      "test" => "test.tests.#"
    }

    msgListeners = {
      "alerts.host" => "alerts.host.#"
    }
    
    strListeners.each {
      |topic, routingKey|
      exchange = channel.topic(topic, :auto_delete => true)
      channel.queue().bind(exchange, :routing_key => routingKey).subscribe do
        |metadata, msg|
        puts "on #{metadata.routing_key}"
        puts "got #{msg}"
      end      
    }
    
    msgListeners.each {
      |topic, routingKey|
      exchange = channel.topic(topic, :auto_delete => true)
      channel.queue().bind(exchange, :routing_key => routingKey).subscribe do
        |metadata, msg|
        puts "on #{metadata.routing_key}"
        obj = Marshal.load(msg)
        puts "got #{obj.body}"
      end      
    }
    
    
    ##exchange = channel.topic("input.opennebula", :auto_delete => true)
    #exchange = channel.topic("registration.metrics", :auto_delete => true)
    #channel.queue().bind(exchange, :routing_key => "registration.metrics.#").subscribe do
    #  |metadata, msg|
    #  puts "on #{metadata.routing_key}"
    #  puts "got #{msg}"
    #end
    #
    #exchange = channel.topic("slaextractor.constraints", :auto_delete => true)
    #channel.queue().bind(exchange, :routing_key => "slaextractor.constraints.#").subscribe do
    #  |metadata, msg|
    #  puts "on #{metadata.routing_key}"
    #  puts "got #{msg}"
    #end
    #
    #exchange = channel.topic("alerts.host", :auto_delete => true)
    #channel.queue().bind(exchange, :routing_key => "alerts.host.#").subscribe do
    #  |metadata, msg|
    #  puts "on #{metadata.routing_key}"
    #  obj = Marshal.load(msg)
    #  puts "got #{obj.body}"
    #end
    #
    #exchange = channel.topic("input.opennebula", :auto_delete => true)
    #channel.queue().bind(exchange, :routing_key => "input.opennebula.#").subscribe do
    #  |metadata, msg|
    #  puts "on #{metadata.routing_key}"
    #  puts "got #{msg}"
    #end
    #
    #channel.queue().bind(exchange, :routing_key => "input.vm.opennebula.#").subscribe do
    #  |metadata, msg|
    #  puts "VM INFO on #{metadata.routing_key}"
    #  puts "got #{msg}"
    #end
    #
    #exchange = channel.topic("input.opennebula", :auto_delete => true)
    #channel.queue().bind(exchange, :routing_key => "input.host.opennebula.#").subscribe do
    #  |metadata, msg|
    #  puts "HOST INFO on #{metadata.routing_key}"
    #  puts "got #{msg}"
    #end

    #exchange = channel.topic("resource.cpu")
    #channel.queue("resource.cpu.#").bind(exchange, :routing_key => "resource.cpu.#").subscribe do 
    #  |metadata, msg|
    #  puts "result in cpu queue is #{msg}"
    #end
    #
    #exchange = channel.topic("unit-tests.sink")
    #channel.queue("unit-tests.sink").bind(exchange, :routing_key => "unit-tests.sink").subscribe do 
    #  |metadata, msg|
    #  puts "result in UT queue is #{msg}"
    #end
            
    #let's wrap it up
    #showStopper = Proc.new { connection.close { EventMachine.stop} }
    #Signal.trap "TERM", showStopper
    #EM.add_timer(5, showStopper)      
  end
end