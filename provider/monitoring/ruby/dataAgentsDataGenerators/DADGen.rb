require 'rubygems'
require 'amqp'
require 'pp'

# ===============================================================
# = Any variable that needs to be set is set in the config file =
# ===============================================================

# ========
# = Code =
# ========

def generateValues(metrics, params)
  vals = Hash.new
  
  #set parameter values
  params.each { |p, val| vals[p] = val }
  
  #set other values, by order
  order = metrics[:order]
  order.each {
    |param|
    instruction = metrics[:elms][param]
    instruction.each {
      |params, block|
      result = nil
      if(params.empty?) 
        result = block.call
      else
        p = params.map { |p| vals[p] }
        result = block.call(p)
      end
      vals[param] = result
    }
  }
  vals
end

randomHostMetrics = {
  "common" => {
    :order => [],
    :elms => {
      "hostname" => :fixed,
      "availiability" => :fixed
    }
  },
  "memory" => {
    #order of evaluation
    :order => ["used", "free"],
    :elms => {
      "total" => :fixed,
      #used is a random number modulo total memory size
      "used" => { ["total"] => lambda { |x| Random.rand(x[0]) } },
      #free is total - used
      "free" => { ["total", "used"] => lambda { |x| x[0] - x[1]} }
    }
  },
  "cpu" => {
    :order => ["user", "idle", "system"], 
    :elms => {
      "cores" => :fixed, 
      "speed" => :fixed, 
      #set user percentage for user load
      "user" => { [] => lambda { Random.rand(100).to_f / 100.0 } },
      #idle is random modulo 1 -  user_load
      "idle" => { ["user"] => lambda { 
        |x| 
        i = (100*x[0]).to_i 
        u = Random.rand(100 - i)
        u.to_f / 100.0
        }
      },
      #system is 1 - user - idle
      "system" => { ["idle", "user"] => lambda { |x| 1 - x[0] - x[1] } }
    }
  },
  "disk" => {
    :order => ["used", "available"],
    :elms => {
      "total" => :fixed,
      "used" => { ["total"] => lambda { |x| Random.rand(x[0]) } },
      "available" => { ["total", "used"] => lambda {|x| x[0] - x[1] }}
    }
  }
}

def generateMetricsForAllHosts(hosts)
  data = []
  hosts.each {
    |sid, host|
    host.each {
      |group, params|
      data << {
        :source => "host",
        :sid => sid,
        :group => group,
        :metrics => generateValues(@hostMetrics[group], params)
      }
    }
  }
  data
end

def addTime(metricsData, secInterval)
  time = Time.new
  timedData = metricsData.map { 
    |x| 
    x[:time] = time.to_s 
    time += secInterval
    x
  }
end

def metricsToXML(metrics)
  s = "<Message time=\"#{metrics[:time]}\">\n"
  metrics[:metrics].each {
    |id, value|
    s += "\t<Value id=\"#{id}\">#{value}</Value>\n"
  }
  s += "</Message>\n"

  s
end

# ==================================
# = Test parameters and execution  =
# ==================================
@hostMetrics = randomHostMetrics

load "SourceConfig.rb"

data = []
@@N_OUTPUT.times {
  data += generateMetricsForAllHosts(@hosts)
}

data = addTime(data, 0)
xmls = data.map { |d| 
  d[:xml] = metricsToXML(d) 
  d
}

#pp xmls

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
    
    xmls.each {
      |info|
      exchangeName = "input.#{info[:source]}"
      routingKey = "input.#{info[:source]}.#{info[:sid].gsub(".", "-")}.#{info[:group]}"
      
      pp exchangeName, routingKey, info[:xml]
      
      #exchange = channel.topic(exchangeName, :auto_delete => true)
      #exchange.publish(info[:xml], :routing_key => routingKey)
    }

    #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end