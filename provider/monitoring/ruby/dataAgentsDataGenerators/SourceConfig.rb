@@N_OUTPUT = 10

host1 = {
#  "common" => { "hostname" => "n0008", "availiability" => true },
  "memory" => { "total" => 4096 }, 
#  "cpu" => { "cores" => 4, "speed" => 2600 },
#  "disk" => { "total" => 2000 }
}

host2 = {
#  "common" => { "hostname" => "n0013", "availiability" => true },
  "memory" => { "total" => 8192 }, 
#  "cpu" => { "cores" => 8, "speed" => 3066 },
#  "disk" => { "total" => 2000 }
}

@hosts = {
  "n0008.xc1.xlab.lan" => host1, 
  "n0013.xc1.xlab.lan" => host2
}
