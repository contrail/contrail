require 'rubygems'
require 'pp'
require 'date'
require 'amqp'

load 'xml2ostruct2json.rb'

include XMLTranslations

def checkSLAconsistency(metricMsg, metric, exprType, exprValue)
  targetMetric = metric.downcase
  operator = exprType.downcase

  metrics = metricMsg.body
  msgobj = XMLTranslations::xml2ostruct(metrics)
  #pp msgobj
  msgobj.message.value = [ msg.message.valu ] if msgobj.message.value.class == OpenStruct
  msgobj.message.value.each {
    |val|
    #pp val
    id = val.id.downcase
    if (targetMetric == id) 
      puts "found metric"
      #check operators
      #convert value to float        
      slaVal = exprValue.to_f
      measured = val.content.to_f        
      msg = OpenStruct.new        
      msg.time = Time.new if msg.time.nil?
      case operator
      when "greater_than"
      when "greater"
        if not measured > slaVal
          msg.body = "#{measured} #{operator} #{slaVal} is false"
        end
        puts "SLA condition is met"
      when "smaller_than"
      when "smaller"
        if not measured < slaVal
          msg.body = "#{measured} #{operator} #{slaVal} is false"
          return msg
        end
        puts "SLA condition is met"
      when "equal"
        if not measured == slaVal
          msg.body = "#{measured} #{operator} #{slaVal} is false"
          return msg
        end
        puts "SLA condition is met"            
      when "different"
        if not measured != slaVal
          msg.body = "#{measured} #{operator} #{slaVal} is false"
          return msg
        end
        puts "SLA condition is met"                    
      end
    end
  }
  #return nil so that the flow is no longer propagated further on the queues
  nil
end

def get_sla_blocks(obj)
  blocks = []
  slobjective = obj.monitoringconfiguration.servicelevelobjectives.servicelevelobjective
  slobjective = [ slobjective ] if slobjective.class == OpenStruct
  slobjective.each {
    |slob|
    id = slob.id
    slob.guarantees.guaranteed = [ slob.guarantees.guaranteed ] if slob.guarantees.guaranteed.class == OpenStruct
    slob.guarantees.guaranteed.each {
      |garnte|
      #pp garnte
      grId = garnte.id
      grFrom = DateTime.parse(garnte.validity.effectivefrom)
      grTo = DateTime.parse(garnte.validity.effectiveuntil)
      grSchedule = garnte.validity.schedule        
      expr = garnte.constraintexpression
      exprType = expr.predicate.type
      exprValue = expr.predicate.value
      metric = expr.predicate.metric.name
      targetSource = expr.predicate.metric.target.source
      targetGroup = expr.predicate.metric.target.group
      targetSid = expr.predicate.metric.target.sid
      patternTargetSid = targetSid.gsub(".", "-")

      #construct pattern to response to
      #the pattern used is only the group channel
      qPattern = "#{targetSource}.#{patternTargetSid}.#{targetGroup}"
      pp qPattern
      #if metrics are sent to separate metric channels, use this one instead of the group
      #channel that is used in previous setting
      #qMetricPattern = "#{qPattern}.#{metric}"
      #pp qMetricPattern
    
      #construct string block with values wihing a function call
      strBlock = "{\n\t|x|\n"
      strBlock += "\tcheckSLAconsistency(x, \"#{metric}\", \"#{exprType}\", \"#{exprValue}\")\n"
      strBlock += "}\n"
    
      #puts strBlock
      
      #set the group channel and the block
      blocks << [qPattern, strBlock]     
    }
  }
  blocks
end

def test_sla_blocks
  slexml = File.open("SLAs/slaex-simple-example.xml").read
  obj = XMLTranslations::xml2ostruct(slexml)
  
  slobjective = obj.monitoringconfiguration.servicelevelobjectives.servicelevelobjective
  slobjective = [ slobjective ] if slobjective.class == OpenStruct
  slobjective.each {
    |slob|
    id = slob.id
    slob.guarantees.guaranteed = [ slob.guarantees.guaranteed ] if slob.guarantees.guaranteed.class == OpenStruct
    slob.guarantees.guaranteed.each {
      |garnte|
      #pp garnte
      grId = garnte.id
      grFrom = DateTime.parse(garnte.validity.effectivefrom)
      grTo = DateTime.parse(garnte.validity.effectiveuntil)
      grSchedule = garnte.validity.schedule        
      expr = garnte.constraintexpression
      exprType = expr.predicate.type
      exprValue = expr.predicate.value
      metric = expr.predicate.metric.name
      targetSource = expr.predicate.metric.target.source
      targetGroup = expr.predicate.metric.target.group
      targetSid = expr.predicate.metric.target.sid
      patternTargetSid = targetSid.gsub(".", "-")

      #construct pattern to response to
      qPattern = "#{targetSource}.#{patternTargetSid}.#{targetGroup}"
      qMetricPattern = "#{qPattern}.#{metric}"
      pp qPattern
      pp qMetricPattern
    
      #construct string block with values wihing a function call
      strBlock = "{\n\t|x|\n"
      strBlock += "\tcheckSLAconsistency(x, \"#{metric}\", \"#{exprType}\", \"#{exprValue}\")\n"
      strBlock += "}\n"
    
      puts strBlock
    
      block = eval("lambda #{strBlock}")
    
      metricMessage = File.open("metrics/opennebula-common-example.xml").read
    
      msg = OpenStruct.new
      msg.body = metricMessage
    
      result = block.call(msg)
    
      pp result
    
      puts
      exit(0)
    }
  }
end

#test_sla_blocks

#slexml = File.open("SLAs/slaex-simple-example.xml").read

@@metricsCount = 0

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)

    exchangeBlock = channel.topic("blocks", :auto_delete => true)
    exchangeOutput = channel.topic("outputs", :auto_delete => true)    
    
    exchange = channel.topic("slaextractor.constraints", :auto_delete => true)
    channel.queue().bind(exchange, :routing_key => "slaextractor.constraints.#").subscribe do
      |metadata, slexml|
      obj = XMLTranslations::xml2ostruct(slexml)
      blocks = get_sla_blocks(obj)
      pp blocks
      blocks.each {
        |elm|
        pattern = elm[0]
        block = elm[1]
        
        puts "publishing #{block} \non #{pattern}"
        exchangeBlock.publish(block, :routing_key => "blocks.slaex#{@@metricsCount}.input.#{pattern}.#")
        #output to alerts.pattern.#slaid! #must get slaid
        exchangeOutput.publish("alerts.#{pattern}.#{obj.monitoringconfiguration.uuid}", 
          :routing_key => "outputs.slaex#{@@metricsCount}.input.#{pattern}.#")            
        
        @@metricsCount += 1
      }
    end
  end
end