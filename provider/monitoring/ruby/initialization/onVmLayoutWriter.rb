require 'rubygems'
require 'amqp'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
              
    exchange2 = channel.topic("input.opennebula", :auto_delete => true)
    metricRegMsg = %{<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <Message time="2011-10-07T16:39:05+0200" type="VmLayout">
    	<NetworkEntities>
    		<NetworkEntity cpu_cores="4" cpu_speed="2494.648" disk_available="0.0" disk_used="0.0" fqdn="n0008.xc1.xlab.lan" load_one="0.29" memory="3915" name="n0008" type="host" up="1" used_cpu_cores="4" used_cpu_speed="2494.648" used_memory="3915">
    			<NetworkEntity cpu_cores="2" cpu_load="0.00" cpu_share="1" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-6-stress.qcow2', 'persistent': true}]" fqdn="vm-10-1-0-11.xc1.xlab.lan" mem_host="150.961" mem_usage="67" memory="119" name="vm-10-1-0-11" state="active" type="vm" userId="0"/>
    			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="0" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-12.xc1.xlab.lan" mem_host="143.609" mem_usage="115" memory="119" name="vm-10-1-0-12" state="active" type="vm" userId="0"/>
    			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="2" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-13.xc1.xlab.lan" mem_host="149.641" mem_usage="109" memory="245" name="vm-10-1-0-13" state="active" type="vm" userId="0"/>
    			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="0" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-17.xc1.xlab.lan" mem_host="132.828" mem_usage="92" memory="119" name="vm-10-1-0-17" state="active" type="vm" userId="0"/>
    		</NetworkEntity>
    		<NetworkEntity cpu_cores="4" cpu_speed="2494.203" disk_available="0.0" disk_used="0.0" fqdn="n0007.xc1.xlab.lan" load_one="0.03" memory="3915" name="n0007" type="host" up="1" used_cpu_cores="4" used_cpu_speed="2494.203" used_memory="3915"/>
    	</NetworkEntities>
    </Message>}
    exchange2.publish(metricRegMsg, :routing_key => "input.opennebula.vmlayout")
    
   #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end