require 'rubygems'
require 'amqp'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
        
        
    exchange2 = channel.topic("registration.metrics", :auto_delete => true)
    
    metricFiles = [ 
      "metrics/opennebula-common-register.xml", 
      "metrics/opennebula-memory-register.xml"
      ]
    
    
    metricFiles.each {
      |fileName|
      metricRegMsg = File.open(fileName, "r").read
      exchange2.publish(metricRegMsg, :routing_key => "registration.metrics.#")
    }
    
   #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end