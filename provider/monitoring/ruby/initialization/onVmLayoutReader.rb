require 'rubygems'
require 'pp'
require 'amqp'

load 'xml2ostruct2json.rb'

include XMLTranslations

=begin
Messages that come into "input.opennebula.vmlayout" queue.

The messages with vmLayout are dissected for every possible 
metric and then sent again, using a more detailed pattern
on which the QoS terms can be registered.
=end

def constructRabbitMQpattern(type, uri, metrics)
  "#{type}.opennebula.#{uri}.#{metrics}"
end

def exampleONvmLayoutXML
  %q{<?xml version="1.0" encoding="UTF-8" standalone="no"?>
  <Message time="2011-10-07T16:39:05+0200" type="VmLayout">
  	<NetworkEntities>
  		<NetworkEntity cpu_cores="4" cpu_speed="2494.648" disk_available="0.0" disk_used="0.0" fqdn="n0008.xc1.xlab.lan" load_one="0.29" memory="3915" name="n0008" type="host" up="1" used_cpu_cores="4" used_cpu_speed="2494.648" used_memory="3915">
  			<NetworkEntity cpu_cores="2" cpu_load="0.00" cpu_share="1" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-6-stress.qcow2', 'persistent': true}]" fqdn="vm-10-1-0-11.xc1.xlab.lan" mem_host="150.961" mem_usage="67" memory="119" name="vm-10-1-0-11" state="active" type="vm" userId="0"/>
  			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="0" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-12.xc1.xlab.lan" mem_host="143.609" mem_usage="115" memory="119" name="vm-10-1-0-12" state="active" type="vm" userId="0"/>
  			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="2" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-13.xc1.xlab.lan" mem_host="149.641" mem_usage="109" memory="245" name="vm-10-1-0-13" state="active" type="vm" userId="0"/>
  			<NetworkEntity cpu_cores="1" cpu_load="0.00" cpu_share="0" cpu_speed="2494.332" disks="[{'uri': '/nebula/images/debian-600.qcow2.z', 'persistent': true}]" fqdn="vm-10-1-0-17.xc1.xlab.lan" mem_host="132.828" mem_usage="92" memory="119" name="vm-10-1-0-17" state="active" type="vm" userId="0"/>
  		</NetworkEntity>
  		<NetworkEntity cpu_cores="4" cpu_speed="2494.203" disk_available="0.0" disk_used="0.0" fqdn="n0007.xc1.xlab.lan" load_one="0.03" memory="3915" name="n0007" type="host" up="1" used_cpu_cores="4" used_cpu_speed="2494.203" used_memory="3915"/>
  	</NetworkEntities>
  </Message>}
end

xmlONvmLayout = exampleONvmLayoutXML

def extractMetricPatternsAndValues(xmlONvmLayout)
  objONvmLayout = XMLTranslations::xml2ostruct(xmlONvmLayout)

  #pp objONvmLayout

  rabbitmqPatterns = Hash.new
  otherKeys = [:type, :fqdn, :name, :networkentity]

  objONvmLayout.message.networkentities.networkentity.each {
    |hostNetEnt|
    vms = hostNetEnt.networkentity

    #translate hosts
    ht = hostNetEnt.marshal_dump
    #pp ht
    type = hostNetEnt.type
    fqdn = hostNetEnt.fqdn
    otherKeys.each { |key| ht.delete(key) }
    ht.each {
      |key, value|
      rabbitmqPattern = constructRabbitMQpattern(type, fqdn, key)
      rabbitmqPatterns[rabbitmqPattern] = value
    }
      
    #now deal with VMs
    if not vms.nil?
      #puts "got #{vms.size} VMs"
      vms.each {
        |vmNetEnt|
        ht = vmNetEnt.marshal_dump
        #pp ht
        #now change everything into rabbitmq patterns
        type = vmNetEnt.type
        fqdn = vmNetEnt.fqdn.gsub(".", "-")
      
        #clean nonmetric stuff
        otherKeys.each { |key| ht.delete(key) }
      
        ht.each {
          |key, value|
          #pp key.to_s
          rabbitmqPattern = constructRabbitMQpattern(type, fqdn, key)
          rabbitmqPatterns[rabbitmqPattern] = value
        }
      }
    else
      #puts "no VMs defined"
    end
  }
  
  #pp rabbitmqPatterns
  rabbitmqPatterns
end

#msgValues = extractMetricPatternsAndValues(xmlONvmLayout)
#pp msgValues


#create a listener on the "input.opennebula.vmlayout"
#the listener proceesses the the message, creates a list of
#used metrics and redistributes the messages using the same 
#exchange, but with a different pattern.
EventMachine.run do  
  AMQP.connect(:host => 'localhost') do |connection|
    channel = AMQP::Channel.new(connection)
    
    exchange = channel.topic("input.opennebula", :auto_delete => true)
    channel.queue().bind(exchange, :routing_key => "input.opennebula.vmlayout").subscribe do
      |metadata, vmlayoutMsg|
      begin
        pp vmlayoutMsg
        msgValues = extractMetricPatternsAndValues(vmlayoutMsg)
        msgValues.each {
          |pattern, metricValue|
          puts "publishing #{metricValue} on input.#{pattern}"
          exchange.publish(metricValue, :routing_key => "input.#{pattern}")
        }
      rescue
        #send on some alerting queue
        puts "ERROR! while parsing "
        puts "TBD: send on some alerting queue"
      end
    end
          
    #showStopper = Proc.new { connection.close { EventMachine.stop} }
    #Signal.trap "TERM", showStopper
    #EM.add_timer(10, showStopper)    
  end
end          
