require 'rubygems'
require 'amqp'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
        
        
    exchange2 = channel.topic("slaextractor.constraints", :auto_delete => true)
    metricRegMsg = File.open("SLAs/slaex-simple-example.xml").read
    exchange2.publish(metricRegMsg, :routing_key => "slaextractor.constraints.#")
    
   #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end