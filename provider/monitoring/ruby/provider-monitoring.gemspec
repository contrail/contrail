Gem::Specification.new do |s|
  s.name        = 'contrail-provider-monitoring'
  s.version     = '1.0.0'
  s.date        = '2012-03-14'
  s.summary     = "Contrail-Monitoring"
  s.description = "Contrail monitoring infrastructure"
  s.authors     = ["Uros Jovanovic"]
  s.email       = 'uros.jovanovic@xlab.si'
  s.files       = ["AccountingStorage.rb", "QueueManager2.rb", "Listeners.rb"]
  s.homepage    =
    'http://contrail.xlab.si/gems/contrail-monitoring'
end
