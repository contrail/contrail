require 'rubygems'
require 'amqp'
require 'ostruct'
require 'pp'

@@storage = []

require 'rubygems'
require 'pp'
require 'mongoid'
 
Mongoid.configure do |config|
  name = "mongoid_test_db"
  host = "localhost"
  port = 27017
  config.database = Mongo::Connection.new.db(name)
end

class BasicMessageEntry
  include Mongoid::Document
  
  field :body, :type => String
  field :time, :type => Time
  field :resourceSource, :type => String
  field :traceQueues, :type => Array
  field :bodyType, :type => String
  field :sourceIp, :type => String
end
 
#-----

def filterToBlock(filters)
  properFilters = filters.map {
    |x|
    key = x[0].to_s.gsub(":", "")
    fil = x[1].gsub(key, "x.#{key}")
    "( #{fil} )"
  }  
  
  code = properFilters.join(" and ")
  #strBlock = "{\n|x|\n  pp x\n  res = nil\n"
  #strBlock += "res = x if ( #{code}  )\n"
  #strBlock += "res\n}"
  strBlock = %q{
    |x|
    res = nil
    pp x.marshal_dump
    res = x if ( } + code + %q{ )
    res
  }

  pp code
  pp strBlock

  filterBlock = eval("lambda {#{strBlock}}")
  pp filterBlock
  filterBlock
end

def filter(array, filters)  
  block = filterToBlock(filters)
  res = array.map { |x| block.call(x) }
  res.compact!
  #pp res
  res
end

#-----

require 'rubygems'
require 'pp'

class FlatRateVMPricing
 attr_reader :pricePerHour

 def initialize(price)
   @pricePerHour = price
   @pricePerSecond = price.to_f / 3600.0
 end

 def calculatePrice(startTime, endTime)
   secs = endTime - startTime
   @pricePerSecond * secs
 end

 def price(vmEvents)
   price = 0

   sTime = nil
   eTime = nil

   vmEvents.each {
     |eventMsg|
     event = eventMsg.body
     time = eventMsg.time
     puts "event #{event} occured at #{time}"
     
     #can handle just one VM at the time
     if event == "up" or event == "awake" 
       sTime = time
     end
     
     if event == "down" or event == "pause" or event == "migrate"
       if not sTime.nil?
         eTime = time
         
         duration = eTime - sTime
         price += @pricePerSecond * duration
         
         eTime = nil
         sTime = nil
       end
     end
   }
   
   if not sTime.nil? 
     #VM still running
     puts "still running!!!"
     eTime = Time.new
     duration = eTime - sTime
     price += @pricePerSecond * duration     
   end
   
   price
 end
end

@@billing = Hash.new
@@billing["FlatRateVMPricing-3600"] = FlatRateVMPricing.new(3600) #on euro per second :D

#-----

EventMachine.run do
  AMQP.connect(:host => '127.0.0.1') do |connection|

    channel = AMQP::Channel.new(connection)

    exchange = channel.topic("accounting.input")
    channel.queue("accounting.input.events").bind(exchange, :routing_key => "accounting.input.#").subscribe do 
      |metadata, msg|
      message = Marshal.load(msg)
      puts "accounting storing msg: #{message.marshal_dump}"
      @@storage << message
      
      entry = BasicMessageEntry.create({
        :body => message.body,
        :time => message.time,
        :resourceSource => message.resourceSource,
        :traceQueues => message.traceQueues,
        :bodyType => message.bodyType,
        :sourceIp => message.sourceIp
      })      
      
    end
    
    exchange = channel.topic("accounting.storage")
    channel.queue("accounting.storage.filters").bind(exchange, :routing_key => "accounting.storage.filters.#").subscribe do 
      |metadata, msg|
      message = Marshal.load(msg)
      filters = message.filters
      puts "accounting filters: #{filters}"

      filteredMsgs = filter(@@storage, filters)
      #filteredMsgs.each { |fm| pp fm.marshal_dump }
      puts "Filtered results: (count = #{filteredMsgs.size})"
      
    end
    
    exchange = channel.topic("accounting.billing")
    channel.queue("accounting.billing").bind(exchange, :routing_key => "accounting.billing.#").subscribe do
      |metadata, msg|
      message = Marshal.load(msg)
      filters = message.filters
      puts "accounting-billing filters: #{filters}"
      
      billingType = message.billingType
      billingRate = @@billing[billingType]

      #filteredMsgs = filter(@@storage, filters)
      
      filteredMsgs = []
      res = BasicMessageEntry.where(filters).each {
        |entry|
        
        msg = OpenStruct.new
        msg.body = entry.body
        msg.time = entry.time
        msg.resourceSource = entry.resourceSource
        msg.traceQueues = entry.resourceSource
        msg.bodyType = entry.bodyType
        msg.sourceIp = entry.sourceIp
        
        filteredMsgs << msg
      }
      
      price = billingRate.price(filteredMsgs)
      puts "calculated price: #{price}"
    end
    
  end
end