INTRODUCTION
============

Basic REST access URL:
----------------------
http://server/API/federation/R/A

where:
- API is a set of supported APIs
- R is a set of available resources
- A_{resource} is a set of available attributes for a resource

and:
API = {OCCI, CTRLAPI, \ldots}
R = {SLA, SLA template, user, provider, appliance, application, monitoring}

SLA attributes:
A_{sla} = {id, status, consumer, provider, valid, valid_from, valid_to}
A_{sla\ template} = A_{sla}

User attributes:
A_{user} = {id, id_{attr}, id_{R}, id_{R_{id}}

Semantics:
----------
- an attribute \emph{id} is a parameter. 
- an attribute \emph{id_{attr}} is translated into http://server/API/federation/user/:id/:attr 
- an attribute \emph{id_{R}} is translated into http://server/API/federation/user/:id/:R where all resources of type \emph{R} are returned for the user with an ID \emph{:id}.
- an attribute \emph{id_{R_{id}} is translated into http://server/API/federation/user/:id/:R/:rid (TODO: possible duplication with http://server/API/federation/R/:rid?)


http://server/API/federation/R
- GET: returns the list of all resources of type R that the user has access to
- POST: adds a resource where the body of the post contains an XML with the resource description

http://server/API/federation/R/:id
- GET: returns the XML representation of the resource with a given ID as a parameter

http://server/API/federation/R/:id/:attr
- GET: returns the value of the attribute of a resource with a given ID
- POST: sets the value of the attribute of the resource with a given ID

INSTALLATION
============
Requirements:
- ruby (>= 1.8.x)
- sinatra gem (1.2.6)
- xml-simple gem (1.0.12)

To install the two gems, use the following commands:
- sudo gem install sinatra
- sudo gem install xml-simple

USAGE
=====
To run the script use:
- ruby fedeR.rb

Sinatra runs on port 4567 by default. To access the service use the following base URL from the browser:
- http://localhost:4567/CTRLAPI/federation

The current version of service uses internal caching mechanism and is therefore empty when initially ran - needs to be filled with data before use. This is done by posting XML files to the selected resource. Posting can be done by using \emph{curl} command, i.e. like this:

curl -d "xml=<?xml version="1.0" encoding="utf-8" ?>
<mysla>
<user>John Doe</user>
<from>today</from>
<expire>never</expire>
</mysla>
" http://localhost:4567/CTRLAPI/federation/sla

The given command posts a \emph{SLA} to the list of available SLAs and returns the ID of a new post. The list of all available SLAs can be retrieved by visiting the following URL in the browser: http://localhost:4567/CTRLAPI/federation/sla. 

To access some SLA, visit the following URL in the browser: http://localhost:4567/CTRLAPI/federation/sla/1 (to access the SLA with an ID=1).

To access some attribute within a SLA (defined by the posted XML), just extend the URL to access the SLA with the attribute name. For example, to access \emph{from} attribute in the example above, direct the browser to the following URL: http://localhost:4567/CTRLAPI/federation/sla/1/from

To change the value of an attribute, one needs to post the new value to the URL of an attribute. For example, to change the value of the \emph{from} attribute, use the following culr command from the command line: curl -d "xml=tomorrow" http://localhost:4567/CTRLAPI/federation/sla/1/from

REMARKS
=======
- the idea of the prototype is to see what resources are used, what description is needed for given resource and how the resources interact/connect to determine the proper workflow for federation
- all posts are stored only in RAM, when service is terminated, the data is lost
- there is no checking of form of XMLs that are posted, any XML will do (current prototype is used for testing purposes only!)

TODO
====
- add new attribute to the existing post
- introduce links between resource instances to
- federation functionality