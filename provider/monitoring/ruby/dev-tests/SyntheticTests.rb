require 'rubygems'
require 'amqp'
require 'sourcify'
require 'pp'

=begin
Producers make periodic messages. Producers are for different domains
(type/format of information). Workflow for monitoring this information
is established. All information is sent to the accounting input queue 
in the end, where it is stored. Alerts are monitored.

Should include
- numeric data for measurements
- events
- alerts

Tests should include:
- proper flow of messages
- proper management/calculation of message content
- proper alerting management

Use-cases:
- events for VM-s statuses
- swap size of the host should not exceed amount M
=end

EventMachine.run do  
AMQP.connect(:host => 'localhost') do |connection|
  @@channel = AMQP::Channel.new(connection)
  
  def initialize_registry
      sourceIp = 'localhost'
    
      puts "setting up blocks"
      exchange = @@channel.topic("blocks", :auto_delete => true)
      exchange2 = @@channel.topic("outputs", :auto_delete => true)

      #block construction that gets values from outside
      #will have to be dealt differently (concat strings).
      #works for static values. dynamic values are supposed
      #to be sent by queues
      #block = %q{ {
      #  |x| 
      #  puts "Got event: #{x}"
      #  
      #  msg = OpenStruct.new
      #  msg.body = x
      #  msg.sourceIp = '127.0.0.1'
      #  msg.bodyType = 'event'
      #  
      #  msg
      #  } 
      #}
      
      block = lambda {
        |x| 
        #this is just a demo
        def bork
          "XXX Hi there, bork bork bork!!!"
        end
        
        puts "Got event: #{x}"
        puts bork
        
        x.sourceIp = '127.0.0.1'
        x.bodyType = 'event'
        
        x
      }
      blockStr = block.to_source
      pp blockStr
            
      exchange.publish(blockStr, :routing_key => "blocks.events.input.events.#")
      exchange2.publish("accounting.input.events", :routing_key => "outputs.events.input.events.#")
      exchange2.publish("processed.input.events", :routing_key => "outputs.events.input.events.#")
    
      block = lambda {
        |x|
        puts "eventcounter got msg with body: #{x.body}"
        @eventCounter = 0 if @eventCounter.nil?
        @eventCounter += 1
        puts "current event count: #{@eventCounter}"

        x.body = @eventCounter
        x
      }
      exchange.publish(block.to_source, :routing_key => "blocks.eventcounter.processed.input.events.#")    
      exchange2.publish("monitoring.events.counter.eventcounter", :routing_key => "outputs.eventcounter.processed.input.events.#")
    
      block = lambda {
        |x|
        c = x.body.to_i
        puts "eventalerter got count at: #{c}"
        result = nil
        if c > 50
          result = "VM event count too big at #{c}!!!"
        end
        x.body = result
        x
      }
      exchange.publish(block.to_source, :routing_key => "blocks.eventalerter.monitoring.events.counter.eventcounter.#")    
      exchange2.publish("monitoring.alerts.counter", :routing_key => "outputs.eventalerter.monitoring.events.counter.eventcounter.#")
      
      block = lambda {
        |x|
        puts "vmcounter got msg with body: #{x.body}"
        @vmCounter = 0 if @vmCounter.nil?
        @vmCounter += 1 if x.body == "up" or x.body == "awake" 
        @vmCounter -= 1 if x.body == "down" or x.body == "pause" or x.body == "migrate"
        puts "current vm count: #{@vmCounter}"

        x.body = @vmCounter
        x
      }
      exchange.publish(block.to_source, :routing_key => "blocks.vmcounter.processed.input.events.#")
      exchange2.publish("monitoring.events.counter.vmcounter", :routing_key => "outputs.vmcounter.processed.input.events.#")   

      block = lambda {
        |x|
        c = x.body.to_i
        puts "vmcount alerter got count at: #{c}"
        result = nil
        if c > 1
          result = "VM count too big at #{c}!!!"
        end
        x.body = result
        x
      }
      exchange.publish(block.to_source, :routing_key => "blocks.vmcalerter.monitoring.events.counter.vmcounter.#")    
      exchange2.publish("monitoring.alerts.vmcounter", :routing_key => "outputs.vmcalerter.monitoring.events.counter.vmcounter.#")      
      
      
      block = %q{{
        |x|
        str = "ALERT: #{x.body}"
        puts str
        x
      }}
      exchange.publish(block, :routing_key => "blocks.alerts.monitoring.alerts.#")    

  
      block = %q{ {
        |x|
        puts "Got msg for accounting: #{x.body}"
        x
      }}
      exchange.publish(block, :routing_key => "blocks.out4acc.accounting.input.#")
    
      puts "setting up output"      
  end

  def event_producer(events, resource, &sleepBlock)
    exchange = @@channel.topic("input.events", :auto_delete => true)
    events.each {
      |event|
      exchange.publish(event, :routing_key => "input.events.#{resource}")
      puts "event published"
      sleepBlock.call
    }
  end
  
  t1 = Thread.new do
    initialize_registry
  end
  
  t2 = Thread.new do
    events = [:up, :up, :up, :up, :pause, :awake, :migrate, :down, :down]
    #events1 = [:up, :pause, :awake, :down, :up, :down]
    event_producer(events, "vm1") { 
      sleep(10) 
      puts "was sleeping for a while"
    }
  end

  #t3 = Thread.new do
  #  events2 = [:up, :pause, :awake, :down, :up]
  #  event_producer(events2, "vm2") { 
  #    sleep(30) 
  #    puts "was sleeping for a while"
  #  }
  #end
  #
  #t4 = Thread.new do
  #  events3 = [:nop, :down]
  #  event_producer(events3, "vm2") { 
  #    sleep(140) 
  #    puts "cleaning up vm2"
  #  }
  #end    
  
  #let's wrap it up
   showStopper = Proc.new { connection.close { EventMachine.stop} }
   Signal.trap "TERM", showStopper
   EM.add_timer(150, showStopper)    
end
end
