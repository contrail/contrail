require 'rubygems'
require 'sinatra'
require 'amqp'
require 'pp'

@@CTRL_BASE = '/CTRLAPI/monitoring'

@@XML_HEADER = '<?xml version="1.0" encoding="UTF-8"?>'

get @@CTRL_BASE+'/:id' do
  id = params[:id]
  
  parts = id.split(".")
  parts[parts.size - 1] = ["#"] if parts[parts.size-1].length == 0
  
  topic = "#{parts[0]}.#{parts[1]}"
  
  routingKey = parts.join(".")
  
  result = @@XML_HEADER
  result += "<channel>#{params[:id]}</channel>\n"
  result += "<messages>\n"
  EventMachine.run do  
    AMQP.connect(:host => '127.0.0.1') do |connection|
      channel = AMQP::Channel.new(connection)

      exchange = channel.topic(topic)
      channel.queue(routingKey).bind(exchange, :routing_key => routingKey).subscribe do 
        |metadata, msg|
        result += "\t<msg>#{msg}</msg>\n"
      end

      #let's wrap it up
      #showStopper = Proc.new { connection.close { EventMachine.stop} }
      #Signal.trap "TERM", showStopper
      #EM.add_timer(10, showStopper)      
    end
  end  
  result += "</messages>"
  return result
end
