require 'rubygems'
require 'pp'
require 'set'
require 'amqp'

#t1 = Thread.new do
#  EM.run do 
#   q1 = MQ.new 
#   q1.queue('iq1.bla').subscribe do |msg| 
#     puts msg 
#   end 
#  end
#end
#t1.join

EventMachine.run do
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
    
    exchange = channel.topic("resource.cpu", :auto_delete => true)
    
    channel.queue("cpu1").bind(exchange, :routing_key => "resource.cpu.1.#").subscribe do 
      |metadata, msg|
      puts "CPU1: Got information #{msg} for #{metadata.routing_key}"
      puts "CPU1: other metadata: "
      #puts "My metadata: #{metadata.my_metadata}"
      #pp metadata
    end
    
    channel.queue("cpu2").bind(exchange, :routing_key => "resource.cpu.2.#").subscribe do
      |metadata, msg|
      puts "CPU2: Got information #{msg} for #{metadata.routing_key}"
      puts "CPU2: other metadata: "
      #pp metadata
    end
    
    channel.queue("cpu").bind(exchange, :routing_key => "resource.cpu.#").subscribe do 
      |metadata, msg|
      puts "CPU: Got information #{msg} for #{metadata.routing_key}"
      puts "CPU: other metadata: "
    end
    
    exchangeEvents = channel.topic("events", :auto_delete => true)
    
    channel.queue("eventsQ").bind(exchangeEvents, :routing_key => "events.#").subscribe do
      |metadata, msg|
      puts "EVENT: Got event #{msg} for #{metadata.routing_key}"
    end
    
    channel.queue("blocks").bind(exchangeEvents, :routing_key => "blocks.#").subscribe do
      |metadata, msg|
      puts msg
      puts msg.class
      pp metadata.routing_key
      block = eval("lambda #{msg}")
      block.call("hi there from out of space!")
    end
    
    #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(60, showStopper)
  end
end