require 'rubygems'
require 'test/unit'
require 'amqp'
require 'pp'

class SimpleTest < Test::Unit::TestCase
  
  def test_queue
    EventMachine.run do  
      AMQP.connect(:host => '127.0.0.1') do |connection|
        channel = AMQP::Channel.new(connection)
      
        exchange2 = channel.topic("blocks", :auto_delete => true)
        block = %q{ {
          |x| 
          puts x
          puts "Yeah! #{x}"
          "block b1 result"
          } 
        }
        exchange2.publish(block, :routing_key => "blocks.b1.resource.cpu.#")

        block = %q{ {
          |x| 
          puts "From another block -> #{x}"
          "block b2 result"
          } 
        }
        exchange2.publish(block, :routing_key => "blocks.b2.resource.cpu.#")

        block = %q{ {
          |x| 
          puts "Sink got: #{x}"
          } 
        }
        exchange2.publish(block, :routing_key => "blocks.b3.unit-tests.sink.#")


        exchange2 = channel.topic("outputs", :auto_delete => true)
        exchange2.publish("unit-tests.sink.1", :routing_key => "outputs.b1.resource.cpu.#")    

        puts "sleeping"
        sleep(2)
        puts "sending msg for cpu"

        exchange = channel.topic("resource.cpu", :auto_delete => true)
        exchange.publish("metrics for cpu1", :routing_key => "resource.cpu.1")
        
        msgRead = false
        exchange = channel.topic("unit-tests.sink")
        channel.queue("ms1").bind(exchange, :routing_key => "unit-tests.sink.#").subscribe do 
          |metadata, msg|
          puts "sink from UT got #{msg}"
          assert_equal msg, "block b1 result"
          msgRead = true
        end
        
        #assert msgRead
                
        #let's wrap it up
        showStopper = Proc.new { connection.close { EventMachine.stop} }
        Signal.trap "TERM", showStopper
        EM.add_timer(20, showStopper)      
      end
    end
  end
end