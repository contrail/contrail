require 'rubygems'
require 'amqp'

=begin
What we do here is set the following infrastructure. There are two blocks 
of code, b1 and b2, that are set to listen on the "resource.cpu.#" channel.

To set this we need to send a message to the "blocks" exchange whose body
is a string representing the code block that we want to execute. To set the
name of the block and to which channel it resposnds, we set the 
routing key with the following syntax:

blocks.{block name}.{routing key to response to}

For example:

blocks.b1.resoruce.cpu.# is decoded as follows:
- blocks is notification, that we are sending it the internal queue
that response to the source blocks of code
- b1 is the name of the block given
- the rest of the identifier (resource.cpu.#) is the routing key pattern
of the messages that the block is used to process

Now we need to set the output of the block, the queue where it is
sent after processing. This is done by sending a message to the 
"outputs" exchange. The routing key of the message is used to decode
which block and for wich queue the output is set. The body of the 
message represents the destination.

For example:
.publish("monitoring.sink", :routing_key => "outputs.b1.resource.cpu.#")    
is decoded as follows:
- block with name b1 that listens to resource.cpu.# pattern
- sends the results to the "monitoring.sink", where "monitoring" is the
topic of the cannel and sink the queue.

When the user sends the message, for example:
- publish("metrics for cpu1", :routing_key => "resource.cpu.1")
it is recognised by the resources.cpu.# pattern, where the b1 code block
listens to the incomming messages. When the block b1 finished, it sends
the result to the monitoring.sink message.

=end
EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
        
    exchange2 = channel.topic("blocks", :auto_delete => true)
    block = %q{ {
      |x| 
      puts x
      puts "Yeah! #{x}"
      "block b1 result" if not x.nil?
      } 
    }
    exchange2.publish(block, :routing_key => "blocks.b1.resource.cpu.#")
    
    block = %q{ {
      |x| 
      puts "From another block -> #{x}"
      "block b2 result"
      } 
    }
    exchange2.publish(block, :routing_key => "blocks.b2.resource.cpu.#")

    block = %q{ {
      |x| 
      puts "Monitoring sink got: #{x}"
      } 
    }
    exchange2.publish(block, :routing_key => "blocks.b3.monitoring.sink")

    
    exchange2 = channel.topic("outputs", :auto_delete => true)
    exchange2.publish("monitoring.sink", :routing_key => "outputs.b1.resource.cpu.#")    
    
    puts "sleeping"
    sleep(2)
    puts "sending msg for cpu"
    
    exchange = channel.topic("resource.cpu", :auto_delete => true)
    
    exchange.publish("metrics for cpu1", :routing_key => "resource.cpu.1")
    
    
    #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(15, showStopper)
  end
end