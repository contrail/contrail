require 'rubygems'
require 'amqp'
require 'ostruct'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
    
    #exchange = channel.topic("accounting.storage", :auto_delete => true)
    exchange = channel.topic("accounting.billing", :auto_delete => true)

    msg = OpenStruct.new
    msg.billingType = "FlatRateVMPricing-3600"
    #msg.filters = {
    #  :sourceIp => 'sourceIp == "127.0.0.1"',
    #  :bodyType => 'bodyType == "event"',
    #  :resourceSource => 'resourceSource.include?("vm1")'
    #}
    msg.filters = {
      :sourceIp => "127.0.0.1",
      :bodyType => "event",
      :resourceSource => /vm1/
    }

    #exchange.publish(Marshal.dump(msg), :routing_key => "accounting.storage.filters.test")
    exchange.publish(Marshal.dump(msg), :routing_key => "accounting.billing.test")

    #msg.filters = {
    #  :sourceIp => 'sourceIp == "127.0.0.1"',
    #  :bodyType => 'bodyType == "event"',
    #  :resourceSource => 'resourceSource.include?("vm2")'
    #}
    msg.filters = {
      :resourceSource => /vm2/      
    }
    exchange.publish(Marshal.dump(msg), :routing_key => "accounting.billing.test")

    #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(15, showStopper)
  end
end    