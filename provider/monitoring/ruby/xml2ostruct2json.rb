require 'rubygems'
require 'pp'
require 'xmlsimple'
require 'ostruct'
require 'json'

module XMLTranslations
  
=begin
changes the hash from XML-Simple to a oStruct, handling
hash, array and object types. 
=end
def hashes2ostruct(object)
  return case object
  when Hash
    object = object.clone
    #added and keys needed to handle the "bug/feature" of ruby 1.9.2
    keys = []
    added = Hash.new
    object.each do |key, value|
      #puts "key: #{key.to_s}"
      key2 = key.to_s.downcase.gsub(":", "_")
      #puts "newkey: #{key2}"
      #object[key2] = hashes2ostruct(value)
      added[key2] = hashes2ostruct(value)
      #object.delete(key) if key != key2
      keys << key if key != key2
      #puts 
      #puts object.to_s
    end
    
    keys.each { |k| object.delete(k) }
    
    object.merge!(added)
    
    OpenStruct.new(object)
  when Array
    object = object.clone
    object.map! { |i| hashes2ostruct(i) }
    return object[0] if object.size == 1
    object
  else
    #puts "pure object"
    #pp object
    #puts 
    object
  end
end

=begin
adds a special attribute "orig_hash_value" to the generated
oStruct, that holds the original hash created by the XML-Simple.
The hash is used to convert the oStruct to JSON used by MongoDB.
This gives the object "a bit" bigger memory footprint, however, 
it can be used for other purposes (not only for MongoDB).
=end
def hash2ostruct(hash)
  ostrObj = hashes2ostruct(hash)
  ostrObj.orig_hash_value = hash
  ostrObj
end

=begin
direct conversion of the xml into oStruct with the special 
"orig_hash_value" hash.
=end
def xml2ostruct(xml)
  hash = XmlSimple.xml_in(xml, { 'KeepRoot' => true } )
  hash2ostruct(hash)
end

@template = %{<Message time="" type="ClusterConfiguration">
  <ClusterConfiguration fqdn="">
    <Host fqdn="" id="">
      <Auditability></Auditability>
      <Location>
        <CountryCode></CountryCode>
      </Location>
      <SAS70></SAS70>
      <CCR></CCR>
      <DataClassification></DataClassification>
      <HWRedundancyLevel></HWRedundancyLevel>
      <DiskThroughput></DiskThroughput>
      <NetThroughput></NetThroughput>
      <DataEncryption></DataEncryption>
    </Host>
  </ClusterConfiguration>
</Message>}

=begin
takes hash from XML and finds all attributes with 
nesting and makes a list of all available XML paths
in the tree.
=end
def hash2keys(hash, level="", keys=[])
  case hash
  when Hash
    #added and keys needed to handle the "bug/feature" of ruby 1.9.2
    hash.each do |key, value|
      if level.size() == 0 
        nlevel = key
      else 
        nlevel = "#{level}.#{key}"
      end
      keys << nlevel
      hash2keys(value, nlevel, keys)
    end
    #keys
  when Array
    hash.each { 
      |i| 
      hash2keys(i, level, keys) 
    }
  else
    #what to do?!?
  end
end

=begin
non-recursive call with automatic conversion from XML
into the tree paths.
=end
def xml2keys(xml)
  hash = XmlSimple.xml_in(xml, { 'KeepRoot' => true } )
  #pp hash
  keys = []
  hash2keys(hash, "", keys)
  keys
end

=begin
returns true if all the paths in the XML are also
present in the template XML. If not, false is returned
since we've created a path that is not allowed.

The function is used as validation test.
=end
def conform2template?(template, xml)
  templateKeys = xml2keys(template)
  inputKeys = xml2keys(xml)

  #puts "keys"
  #pp inputKeys  
  #inputKeys.each {
  #  |key|
  #  return false if not templateKeys.include?(key)
  #}
  conformKeys2templateKeys(templateKeys, inputKeys)
  true
end

def conformKeys2templateKeys(templateKeys, xmlKeys)
  xmlKeys.each {
    |key|
    return false if not templateKeys.include?(key)
  }
  true
end

=begin
Simple registry class that links the xml templates 
with the patterns used in the QueueManager. For each
pattern, we can register templates and check if the
incomming xml conforms to any of them. 
=end
class TemplateRegistry
  attr_reader :templates  #name templates
  attr_reader :links      #link rabbitMQ patterns to template names
  attr_reader :keys
  
  def initialize
    @templates = Hash.new
    @links = Hash.new
    @keys = Hash.new
  end
  
  def addTemplate(id, template)
    @templates[id] = template
    @keys[id] = xml2keys(template)
  end
  
  def addPattern(pattern, templateId)
    @links[pattern] = [] if not @links.has_key?(pattern)
    @links[pattern] << templateId
  end
  
  def removePattern(pattern)
    @links.delete(pattern)
  end
  
  def conforms?(pattern, xml)
    return true if @links[pattern].nil? #if there are no templates, everything is ok
    
    xmlKeys = xml2keys(xml)
    @links[pattern].each {
      |templateId|
      tKeys = @keys[templateId]
      return true if conformKeys2templateKeys(tKeys, xmlKeys)
    }
    return false
  end
end

end 

#@input = %{<Message time="2011-10-11T15:30:59+0200" type="ClusterConfiguration">
#  <ClusterConfiguration fqdn="opennebula.xc1.xlab.si">
#    <Host fqdn="n0008.xc1.xlab.lan" id="n0008">
#      <Auditability>0</Auditability>
#      <Location>
#        <CountryCode>FR</CountryCode>
#      </Location>
#      <SAS70>1</SAS70>
#      <CCR>0</CCR>
#      <DataClassification>Public</DataClassification>
#      <HWRedundancyLevel>Best</HWRedundancyLevel>
#      <DiskThroughput>BEST</DiskThroughput>
#      <NetThroughput>BEST</NetThroughput>
#      <DataEncryption>False</DataEncryption>
#    </Host>
#    <Host fqdn="n0007.xc1.xlab.lan" id="n0007">
#      <Auditability>1</Auditability>
#      <Location>
#        <CountryCode>SI</CountryCode>
#      </Location>
#      <SAS70>0</SAS70>
#      <CCR>1</CCR>
#      <DataClassification>Confidential</DataClassification>
#      <HWRedundancyLevel>Better</HWRedundancyLevel>
#      <DiskThroughput>STANDARD</DiskThroughput>
#      <NetThroughput>STANDARD</NetThroughput>
#      <DataEncryption>True</DataEncryption>
#    </Host>
#  </ClusterConfiguration>
#</Message>}
#
#conformed = conform2template?(@template, @input)
#puts "conformed? #{conformed}"
#obj = xml2ostruct(@input)
#@jsonInput = obj.orig_hash_value.to_json
#puts @jsonInput