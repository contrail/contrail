require 'rubygems'
require 'json'
require 'yaml'
require 'xmlsimple'
require 'amqp'
require 'set'
require 'pp'
require 'sinatra/base'
require 'thin'
require 'ostruct'
require 'sourcify'

load 'xml2ostruct2json.rb'

include XMLTranslations

#NOTE: for now, all is based on a localhost connection!

=begin
The registry adds code that responds to the incomming messages in
run-time. This is done as follows. The user sends the string 
representing the code block to the "blocks" exchange. The registry 
opens the listener on the "blocks.#" pattern and listens to the
incomming messages. The body of the message is translated into
Proc object. The routing key as part of the metadata is used to 
decode the name of the block and to which pattern of routing keys
it responds to. 

When the message is sent to the "outputs" topic, the connection 
between the results of the blocks for the queueing pattern and 
between the outgoing queue pattern described in the body is made.

Quick note of how routing keys are used:
The routing key is composed of at least two parts, separated by
the dot, as a normal AMQP naming system. The first part represents
the name for which new channel is qreated. The second part is the
name of the topic. To be precise, the topic is actualy composition
of the two parts with a dot.

Now that we have a channel and the exchange, we can set the routing
key which is the given input key.

The channels, exchanges and listeners are created automatically by
either sending the routing_key to the "blocks" or "outputs" topics.
=end

=begin
Explanation of the automatic pub-sub system by example:

We want to set the code block b1 to listen to the resource.cpu.# and 
send the results to another queue.

To set this we need to send a message to the "blocks" exchange whose body
is a string representing the code block that we want to execute. To set the
name of the block and to which channel it resposnds, we set the 
routing key with the following syntax:

blocks.{block name}.{routing key to response to}

For example:

blocks.b1.resoruce.cpu.# is decoded as follows:
- blocks is notification, that we are sending it the internal queue
that response to the source blocks of code
- b1 is the name of the block given
- the rest of the identifier (resource.cpu.#) is the routing key pattern
of the messages that the block is used to process

Now we need to set the output of the block, the queue where it is
sent after processing. This is done by sending a message to the 
"outputs" exchange. The routing key of the message is used to decode
which block and for wich queue the output is set. The body of the 
message represents the destination.

For example:
.publish("monitoring.sink", :routing_key => "outputs.b1.resource.cpu.#")    
is decoded as follows:
- block with name b1 that listens to resource.cpu.# pattern
- sends the results to the "monitoring.sink", where "monitoring" is the
topic of the cannel and sink the queue.

When the user sends the message, for example:
- publish("metrics for cpu1", :routing_key => "resource.cpu.1")
it is recognised by the resources.cpu.# pattern, where the b1 code block
listens to the incomming messages. When the block b1 finished, it sends
the result to the monitoring.sink message.
=end

=begin
For reference of complete list of functionality needed, please look at the QueueManagerCommandsCreator class.
Copied for local reference, might not be in sync:
List of commands
  - add category (multicast exchange)
  - register code block by id to subcategory
  - set output of code block by id based on subcategory
  - remove category
  - list categories
  - list all subcategories of category
  
Categories are hash tables storing the names of the channels.
Each channel has many exchanges. Each exchange has many listeners.

Code blocks is a hash table with names or IDs of the blocks and
the code itself. Each code block gets two parameters, metadata
and the body of the message. 

Bindings or listeners are binded with code blocks. It is a 
hash table, with the name of the input queue, holding a hash table
of code blocks ids mapped to a hash table with categories and their
output queues names.
=end
class QueueManager
  attr_accessor :connection
  attr_accessor :cahnnels
  attr_accessor :exchanges
  attr_accessor :initialized
  
  attr_accessor :categories #info about
  attr_accessor :blocks
  attr_accessor :bindings
  
  def checkSLAconsistency(metricMsg, metric, exprType, exprValue)
    targetMetric = metric.downcase
    operator = exprType.downcase

    metrics = metricMsg.body
    msgobj = XMLTranslations::xml2ostruct(metrics)
    #pp msgobj
    msgobj.message.value = [ msg.message.valu ] if msgobj.message.value.class == OpenStruct
    msgobj.message.value.each {
      |val|
      #pp val
      id = val.id.downcase
      if (targetMetric == id) 
        puts "found metric"
        #check operators
        #convert value to float        
        slaVal = exprValue.to_f
        measured = val.content.to_f        
        msg = OpenStruct.new        
        msg.time = Time.new if msg.time.nil?
        case operator
        when "greater_than"
        when "greater"
          if not measured > slaVal
            msg.body = "#{measured} #{operator} #{slaVal} is false"
          end
          puts "SLA condition is met"
        when "smaller_than"
        when "smaller"
          if not measured < slaVal
            msg.body = "#{measured} #{operator} #{slaVal} is false"
            return msg
          end
          puts "SLA condition is met"
        when "equal"
          if not measured == slaVal
            msg.body = "#{measured} #{operator} #{slaVal} is false"
            return msg
          end
          puts "SLA condition is met"            
        when "different"
          if not measured != slaVal
            msg.body = "#{measured} #{operator} #{slaVal} is false"
            return msg
          end
          puts "SLA condition is met"            
        end
      end
    }
    #return nil so that the flow is no longer propagated further on the queues
    nil
  end
  
  def init_blocks_channel
    #create channel for blocks
    blockChannel = AMQP::Channel.new(@connection)
    blockExchange = blockChannel.topic("blocks", :auto_delete => true)
    @channels["blocks"] = blockChannel
    @exchanges["blocks"] = blockExchange
    blockChannel.queue("blocks").bind(blockExchange, :routing_key => "blocks.#").subscribe do
      |metadata, msg|
      block = string2block(msg)
      
      key = metadata.routing_key
      parts = key.split(".")
      blockname = parts[1]
      categorization = parts[2]
      category = parts[3]
      parts.delete_at(1)      
      parts.delete_at(0)
      subcategory = parts.join(".")
      
      puts "block from #{categorization}/#{category} with key: #{subcategory}"
      addCategorization(categorization)
      addCategory(categorization, category)
      addBlock(blockname, block)
      bindBlock(categorization, category, subcategory, blockname)
      if not @initialized.has_key?(subcategory) 
        puts "new block: #{msg}"
        initQueue(categorization, category, subcategory) 
      end
    end
  end
  
  def init_blocks_remove_channel
  
  end
  
  def init_outputs_channel
    #create channel for outputs
    outputsChannel = AMQP::Channel.new(@connection)
    outputsExchange = outputsChannel.topic("outputs", :auto_delete => true)
    @channels["outputs"] = outputsChannel
    @exchanges["outputs"] = outputsExchange
    outputsChannel.queue("outputs").bind(outputsExchange, :routing_key => "outputs.#").subscribe do
      |metadata, msg|
      key = metadata.routing_key
      parts = key.split(".")
      parts.delete_at(0)
      blockid = parts[0]
      parts.delete_at(0)
      subcategory = parts.join(".")
      
      msgParts = msg.split(".")
      oCategorization = msgParts[0]
      oCategory = msgParts[1]
      oSubcategory = msg
      
      addCategorization(oCategorization)
      addCategory(oCategorization, oCategory)
      
      bindBlockOutput(blockid, subcategory, oCategorization, oCategory, oSubcategory)
    end    
  end
  
  def initialize(connection)
    @connection = connection
    @channels = Hash.new
    @exchanges = Hash.new
    
    @categories = Hash.new
    @blocks = Hash.new
    @bindings = Hash.new
    
    @initialized = Hash.new #track of queues that have been initialized
    
    init_blocks_channel
    init_outputs_channel
    #TBD: init_commands_channel
  end
  
  private 
  
  #adds another channel to the connection
  def addCategorization(categorizationName)
    return if @categories.has_key?(categorizationName)
    @categories[categorizationName] = Hash.new
    channel = AMQP::Channel.new(@connection)
    @channels[categorizationName] = channel
  end
  
  #adds a exchange to the channel
  def addCategory(categorizationName, categoryName)
    return if not @categories.has_key?(categorizationName)
    return if not @channels.has_key?(categorizationName)
    return if @categories[categorizationName].has_key?(categoryName)
    @categories[categorizationName][categoryName] = Hash.new
    exchange = @channels[categorizationName].topic("#{categorizationName}.#{categoryName}", :auto_delete => true)
    @exchanges["#{categorizationName}.#{categoryName}"] = exchange    
    puts "registered exchange at: #{categorizationName}.#{categoryName}"
  end
  
  def addBlock(blockID, block)
    return if @blocks.has_key?(blockID)
    @blocks[blockID] = block
  end
  
  #ads block
  def bindBlock(categorization, category, subcategory, blockID)
    #set up categories
    #@categories[categorization][category][subcategory] = Set.new if not @categories[categorization][category].has_key?(subcategory)
    #@categories[categorization][category][subcategory].add(blockID)
    @categories[categorization][category][subcategory] = [] if not @categories[categorization][category].has_key?(subcategory)
    @categories[categorization][category][subcategory] << blockID if not @categories[categorization][category][subcategory].include?(blockID)
  end

  #adds routing key to the exchange and links block to it
  #same as:
  #    channel.queue("qmQueue").bind(qMngExchange, :routing_key => "QueueManager.#").subscribe do 
  #      |metadata, msg|
  def initQueue(categorization, category, subcategory)
    #set up bindings
    channel = @channels[categorization]
    exchange = @exchanges["#{categorization}.#{category}"]
    #get set of all blocks, call each block
    #for each block, get result
    #publish results over result channel
    channel.queue(subcategory).bind(exchange, :routing_key => subcategory).subscribe do 
      |metadata, msgObj|
      msg = nil
      begin 
        msg = Marshal.load(msgObj)
      rescue
        msg = OpenStruct.new
        msg.body = msgObj
      end
      msg.time = Time.new if msg.time.nil?
      msg.resourceSource = metadata.routing_key if msg.resourceSource.nil?
      msg.traceQueues = [] if msg.traceQueues.nil?
      msg.traceQueues << metadata.routing_key
            
      @categories[categorization][category][subcategory].each {
        |blockName|
        block = @blocks[blockName]
        #clone = msg
        #begin
          clone = OpenStruct.new( msg.marshal_dump )
        #rescue
        #  clone = String.new(msg)
        #end
        result = block.call(clone)
        if not (result.nil? or result.body.nil?)
          msgResult = Marshal.dump(result)
          #TBD: publish the result as bindings are configured
          if @bindings.has_key?(blockName)
            if @bindings[blockName].has_key?(subcategory)
              @bindings[blockName][subcategory].each {
                |triple|
                puts "found result ouput at: "
                pp triple
                oCategorization = triple[0]
                oCategory = triple[1]
                oSubcategory = triple[2]
                #open channel and exchange, then send it over key defined in subcategory
                ochannel = @channels[oCategorization]
                oexchange = channel.topic("#{oCategorization}.#{oCategory}", :auto_delete => true)
                oexchange.publish(msgResult, :routing_key => oSubcategory)
                puts "result published to #{oSubcategory}"
              }
            end
          end
        end
      }
    end
    
    @initialized[subcategory] = true
  end
  
  #subcategory represents the routing_key
  #oCategorization represents output exchange
  #oCompleteCategoryName represetns the string used for rouitng_keys
  #def bindBlockOutput(categorization, category, subcategory, blockID, 
  #                    oCategorization, oCompleteCategoryName)
  #  #tbd
  #end
  def bindBlockOutput(blockID, subcategory, oCategorization, oCategory, oSubcategory)
    puts "binding #{blockID} on #{subcategory} with #{oCategorization}/#{oCategory} with key #{oSubcategory}"
    @bindings[blockID] = Hash.new if not @bindings.has_key?(blockID)
    #@bindings[blockID][subcategory] = Set.new if not @bindings[blockID].has_key?(subcategory)
    #@bindings[blockID][subcategory].add([oCategorization, oCategory, oSubcategory])
    @bindings[blockID][subcategory] = [] if not @bindings[blockID].has_key?(subcategory)
    @bindings[blockID][subcategory] << [oCategorization, oCategory, oSubcategory] if not @bindings[blockID][subcategory].include?([oCategorization, oCategory, oSubcategory])
  end
  
  def string2block(strBlock)
    #puts strBlock
    block = nil
    if strBlock.start_with?("proc")
      block = eval( strBlock )
    else  
      block = eval("lambda #{strBlock}")
    end
    return block
  end
  
  def addBlock(blockID, block)
    return if @blocks.has_key?(blockID)
    @blocks[blockID] = block
  end
  
  #binds block with routing key for some exchange
  def bindBlock(categorization, category, subcategory, blockID)
    #set up categories
    #@categories[categorization][category][subcategory] = Set.new if not @categories[categorization][category].has_key?(subcategory)
    #@categories[categorization][category][subcategory].add(blockID)
    @categories[categorization][category][subcategory] = [] if not @categories[categorization][category].has_key?(subcategory)
    @categories[categorization][category][subcategory] << blockID if not @categories[categorization][category][subcategory].include?(blockID)   
  end  
  
end

@@CTRL_BASE = '/CTRLAPI/monitoring'

@@XML_HEADER = '<?xml version="1.0" encoding="UTF-8"?>'

EventMachine.run do
  AMQP.connect(:host => '127.0.0.1') do |connection|
    @@qManager = QueueManager.new(connection)    
    @@alerts = []
    
    channel = AMQP::Channel.new(connection)
    exchange = channel.topic("monitoring.alerts")
    channel.queue("monitoringal").bind(exchange, :routing_key => "monitoring.alerts.#").subscribe do 
      |metadata, msg|
      mymsg = Marshal.load(msg)
      str = mymsg.to_s
      str = str.delete("<>")
      puts "ALETS for WEB: " + str
      @@alerts << str
    end    
    #pp @@qManager.instance_variables.map! {|x| x.gsub("@", "")}
    #exit(0)
    
    #in odrer to get the access to the state of the registry, 
    #we create REST get methods to all instance variables 
    #and give access in JSON, XML and YAML.
    #the links are available at:
    #http://{url}:{port}/CTRLAPI/monitoring/{format}/{variable}
    #where format is either json or yaml. when format is not present, xml 
    #is returned.
    #variable is the name of the instance variable of the QueueManager, 
    #for example blocks, bindings, etc.
    class App < Sinatra::Base
      
        
      @@qManager.instance_variables.map! {|x| x.to_s.gsub("@", "")}.each {
        |var|
        pp "setting REST get for YAML #{var}"
        get @@CTRL_BASE+"/yaml/#{var}" do
          YAML::dump(@@qManager.instance_variable_get("@#{var}"))
        end
      }

      @@qManager.instance_variables.map! {|x| x.to_s.gsub("@", "")}.each {
        |var|
        pp "setting REST get for JSON #{var}"
        get @@CTRL_BASE+"/json/#{var}" do
          (@@qManager.instance_variable_get("@#{var}")).to_json(true)
        end
      }

      @@qManager.instance_variables.map! {|x| x.to_s.gsub("@", "")}.each {
        |var|
        pp "setting REST get for XML #{var}"
        get @@CTRL_BASE+"/#{var}" do
          XmlSimple.xml_out(@@qManager.instance_variable_get("@#{var}"), 'AttrPrefix' => true)
        end
      }

      get @@CTRL_BASE+"/alerts" do
        body = "No alerts."
        body = @@alerts.join("<p>") if not @@alerts.nil?
        "<html>"+body+"</html>"
      end
      
      get @@CTRL_BASE+'/topics' do 
        result = @@XML_HEADER
        result += "<topics>\n"
        @@qManager.exchanges.each {
          |name, exchange|
          result += "\t<exchange>#{name}</exchange>"
        }
        result += "<topics>\n"
        return result
      end
      
      
#      get @@CTRL_BASE+"/blocks" do
#        @@qManager.blocks.to_json
#      end
#      
#      get @@CTRL_BASE+"/categories" do
#        @@qManager.categories.to_json
#      end
#      
#      get @@CTRL_BASE+"/bindings" do
#        @@qManager.to_json
#      end
    end
    
    
    App.run!({:port => 3210})
  end  
end
