#!/bin/sh

OT_JAR_PATH=/usr/share/java/${project.artifactId}
OT_CP=$CLASSPATH
for F in $OT_JAR_PATH/*.jar; do
	OT_CP=$F:$OT_CP
done

echo Classpath: $OT_CP

java -cp $OT_CP eu.contrail.infrastructure_monitoring.OpenNebulaStressTest
