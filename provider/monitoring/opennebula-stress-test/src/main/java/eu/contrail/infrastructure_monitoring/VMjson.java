package eu.contrail.infrastructure_monitoring;

public class VMjson {
	private String name;
	private String fqdn;
	private String clusterName;

	public VMjson(String h, String f, String c) {
		this.setName(h);
		this.setFqdn(f);
		this.setClusterName(c);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFqdn() {
		return fqdn;
	}

	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
}

