package eu.contrail.infrastructure_monitoring;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;
import org.opennebula.client.vm.VirtualMachinePool;

/**
 * 
 * @author gregor.beslic@cloud.si
 *
 */

public class StressTest {
	
	private static final String PROPS_FILE_DEFAULT = "./one-stress-test.config";
	public static Client oneClient;
	private static final String VM_STATUS_RUNNING = "runn";
	private static final String VM_STATUS_FAIL = "fail";
	private static int TEST_SECONDS_MAX;
	private static String PROPS_FILE;
	private static String ONE_ADMIN_USERNAME;
    private static String ONE_ADMIN_PASSWORD;
    private static String ONE_RPC_HOST;
    private static String ONE_RPC_PORT;
    private static int NUM_VMS;
    private static int START_DELAY_MILLIS;
    private static int SHUTDOWN_DELAY_MILLIS;
    private static int SHUTDOWN_AFTER_SECONDS;
    private static String STRESS_PARAMS;
    private static String ONE_IMAGE_ID;
    private static String LOG4J_LOG_LEVEL;
    private static String VM_CPU;
    private static String VM_MEMORY;
    
    private static Logger log = Logger.getLogger(StressTest.class);
	
    public int getShutdownDelay() {
    	return SHUTDOWN_DELAY_MILLIS;
    }
    
    public static String getImageId() {
    	return ONE_IMAGE_ID;
    }
    
    public static String getCpu() {
    	return VM_CPU;
    }
    
    public static String getMemory() {
    	return VM_MEMORY;
    }
    
	public void testSynchronizedShutdown(String propsFile) {
		try {
			Properties properties = new Properties();
			PROPS_FILE = propsFile == null || propsFile.isEmpty() ? PROPS_FILE_DEFAULT : propsFile;
			properties.load(new FileInputStream(PROPS_FILE));
			ONE_RPC_HOST = properties.getProperty("one_rpc_host");
			ONE_RPC_PORT = properties.getProperty("one_rpc_port");
			ONE_ADMIN_USERNAME = properties.getProperty("one_admin_username");
			ONE_ADMIN_PASSWORD = properties.getProperty("one_admin_password");
			ONE_IMAGE_ID = properties.getProperty("one_image_id");
			SHUTDOWN_DELAY_MILLIS = Integer.parseInt(properties.getProperty("shutdown_delay_ms"));
			START_DELAY_MILLIS = Integer.parseInt(properties.getProperty("start_delay_ms"));
			VM_CPU = properties.getProperty("vm_cpu");
			VM_MEMORY = properties.getProperty("vm_memory");
			
			TEST_SECONDS_MAX = Integer.parseInt(properties.getProperty("test_seconds_max"));
			if(TEST_SECONDS_MAX < 1)
				TEST_SECONDS_MAX = 3600;
			
			NUM_VMS = Integer.parseInt(properties.getProperty("num_vms"));
			if(NUM_VMS < 1)
				NUM_VMS = 5;
			
			SHUTDOWN_AFTER_SECONDS = Integer.parseInt(properties.getProperty("shutdown_after_s"));
			if(SHUTDOWN_AFTER_SECONDS < 1)
				SHUTDOWN_AFTER_SECONDS = 180;
			
			STRESS_PARAMS = properties.getProperty("stress_params");
			if(STRESS_PARAMS.isEmpty())
				STRESS_PARAMS = "-c 4";
			
			LOG4J_LOG_LEVEL = properties.getProperty("log4j_log_level");
			if(!LOG4J_LOG_LEVEL.isEmpty()) {
				if(LOG4J_LOG_LEVEL.equalsIgnoreCase("info")) {
					LogManager.getRootLogger().setLevel((Level)Level.INFO);
				}
				else if(LOG4J_LOG_LEVEL.equalsIgnoreCase("debug")) {
					LogManager.getRootLogger().setLevel((Level)Level.DEBUG);
				}
				else if(LOG4J_LOG_LEVEL.equalsIgnoreCase("trace")) {
					LogManager.getRootLogger().setLevel((Level)Level.TRACE);
				}
			}
			
			oneClient = new Client(ONE_ADMIN_USERNAME + ":" + ONE_ADMIN_PASSWORD, "http://" + ONE_RPC_HOST + ":" + ONE_RPC_PORT + "/RPC2");
	
			StressMachinePool stressPool = new StressMachinePool();
			ArrayList<String> startupErrors = new ArrayList<String>();
			String startupError = null;
			for(int i = 1; i <= NUM_VMS; i++) {
				Thread.sleep(START_DELAY_MILLIS);
				startupError = stressPool.startNewVm("vm-" + i, STRESS_PARAMS);
				if(startupError != null)
					startupErrors.add(startupError);
			}
			log.info("\n\nTest is in progress. After all " + NUM_VMS + " VMs are fully booted, 'stress " + STRESS_PARAMS + "' command will be executed and continue to run for "  + SHUTDOWN_AFTER_SECONDS + " seconds when they simultaneously receive the shutdown signal.\n");
			
			VirtualMachinePool vmPool = new VirtualMachinePool(oneClient);
			long start = System.currentTimeMillis();
			long testStarted = 0;
			long testRunning = 0;
			int loggingStep = 5;
			int i = 0;
			while(true) {
				OneResponse response = vmPool.info();
				if (response.getErrorMessage() != "null") {
					Iterator<VirtualMachine> iterator = vmPool.iterator();
					while (iterator.hasNext()) {
						VirtualMachine vm = iterator.next();
						for(StressMachine sm : stressPool.getStressMachines()) {
							// Current VirtualMachine is also in the Stress pool
							if(
								sm.getId() != null
								&& vm.getId() != null
								&& sm.getId().equalsIgnoreCase(vm.getId())
								&& vm.status() != null
								&& (
									vm.status().equalsIgnoreCase(VM_STATUS_RUNNING)
									|| vm.status().equalsIgnoreCase(VM_STATUS_FAIL)
								)
							) {
								sm.setVm(vm);
								OneVm oneVm = new OneVm(vm);
								sm.setRunning(true);
								sm.setIpAddress(oneVm.getVmIpAddress());
								log.debug("Found stress VM: " + vm.getId() + " with IP " + sm.getIpAddress());
								// Wait for all machines to become running before executing the test...
								if(stressPool.areAllRunning()) {
									if(!stressPool.isTestStarted()) {
										stressPool.setTestStarted(true);
										testStarted = System.currentTimeMillis();
									}
									sm.check();
								}
								else
									log.debug("Waiting for all StressMachines to start running...");
							}
						}
					}
					long end = System.currentTimeMillis();
					long running = (end - start) / 1000;
					if(testStarted > 0)
						testRunning = (end - testStarted) / 1000;
					
					if(i++ % loggingStep == 0)
						log.info("Total time: " + running + " seconds, time running stress tests: " + testRunning + " seconds.\n");
					
					ArrayList<String> shutdownErrors = new ArrayList<String>();
					if(testRunning > SHUTDOWN_AFTER_SECONDS) {
						if(!stressPool.shutdownAll()) {
							shutdownErrors = stressPool.getErrors();
						}
					
						int exitStatus = 0;
						String errors = "";
						log.info("----------------------- TEST COMPLETED -----------------------\n");
						if(startupErrors.isEmpty()) {
							log.info("Test started without errors - all VMs were successfully provisioned.");
						}
						else {
							log.error("Test started with errors!");
							for(String err : startupErrors) {
								errors += err + "\n";
							}
							log.warn(errors);
							exitStatus++;
						}
						
						if(shutdownErrors.isEmpty()) {
							log.info("Test finished without errors - all VMs were successfully shut down.");
							System.exit(exitStatus);
						}
						
						errors = "";
						log.error("Test finished with errors!");
						for(String err : shutdownErrors) {
							errors += err + "\n";
						}
						log.warn(errors);
						exitStatus++;
						System.exit(exitStatus);
					}
					
					// Global timeout was reached
					if(running > TEST_SECONDS_MAX) {
						log.warn("Test finished due to global timelimit of " + TEST_SECONDS_MAX + "s");
						break;
					}

					Thread.sleep(1000);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(42);
	}
}