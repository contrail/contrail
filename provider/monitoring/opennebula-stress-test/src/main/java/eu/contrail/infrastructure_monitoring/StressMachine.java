package eu.contrail.infrastructure_monitoring;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;

import eu.contrail.infrastructure_monitoring.StressTest;
import eu.contrail.infrastructure_monitoring.StressThread;

class StressMachine implements Comparable<StressMachine> {
	private VirtualMachine vm;
	private String name;
	private String stressParams;
	private String id;
	private String hostname;
	private ArrayList<Float> loadAvgs;
	private boolean running;
	private String ipAddress;
	private StressThread st;
	private long start;
	private boolean testFinished = false;
	public static final String VM_NAME_PFX = "stress-";
	
	private static Logger log = Logger.getLogger(StressMachine.class);
	
	private static final String VM_TEMPLATE = "NAME =  \"" + VM_NAME_PFX + "<NAME>\" CPU = " + StressTest.getCpu() + " MEMORY = " + StressTest.getMemory() + "\n" +
			"OS = [ boot=\"hd\", arch=\"x86_64\" ]\n" +
			"DISK = [\n" +
				"image_id   = " + StressTest.getImageId() + "\n" +
			"]\n" +
			"NIC = [\n" +
				"NETWORK = \"private-lan\"\n" +
			"]\n" +
			"GRAPHICS = [\n" +
				"type=\"vnc\",\n" +
				"listen=\"localhost\"\n" +
			"]\n";
	
	public StressMachine(String name, String stressParams) {
		this.name = name;
		this.stressParams = stressParams;
		this.setRunning(false);
		this.loadAvgs = new ArrayList<Float>();
	}
	
	public OneResponse provision() {
		OneResponse rc = null;
		try {
			rc = VirtualMachine.allocate(StressTest.oneClient, VM_TEMPLATE.replaceAll("<NAME>", this.name));
			if(rc.isError()) {
				throw new Exception(rc.getErrorMessage());
		    }
			this.setId(rc.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rc;
	}
	
	public void check() {
		if(this.running && !this.testFinished) {
			if(this.ipAddress == null) {
				log.debug("no IP address for " + getName());
				return;
			}
			
			if(this.st == null) {
				this.st = new StressThread(this, this.stressParams);
				st.start();
			}
		
			if(st.stressed) {
				log.trace("Getting metrics for " + getName());
				st.storeLoadAvg();
				long end = System.currentTimeMillis();
				long running = (end - start) / 1000;
				log.debug(this.name + " up & running for " + running + "s");
			}
		}
	}
	
	public String getHostname() {
		if(hostname == null && this.ipAddress != null) {
			hostname = "vm-" + ipAddress;
		}
		return hostname;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public float getLoadAvg() {
		float la = 0;
		for (Float f : this.loadAvgs) {
			la += f.floatValue();
		}
		return la / this.loadAvgs.size();
	}
	
	public String getLoadAvgs() {
		String la = "";
		boolean first = true;
		for (Float f : this.loadAvgs) {
			if(first)
				first = false;
			else
				la += ", ";
			la += String.valueOf(f);
		}
		return la;
	}

	public void addLoadAvg(float loadAvg) {
		this.loadAvgs.add(loadAvg);
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public OneResponse destroy() {
		this.st = null;
		if(this.vm != null)
			return this.vm.finalizeVM();
		
		return null;
	}

	public VirtualMachine getVm() {
		return vm;
	}

	public void setVm(VirtualMachine vm) {
		this.vm = vm;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}	
	
	public boolean isTestFinished() {
		return this.testFinished;
	}
	
	public int compareTo(StressMachine otherMachine) {
		if(otherMachine instanceof StressMachine) {
			if(this.getLoadAvg() < otherMachine.getLoadAvg())
				return -1;
			else if(this.getLoadAvg() > otherMachine.getLoadAvg())
				return 1;
			return 0;
		}
		else
			throw new ClassCastException("Can only compare StressMachines");
	}
}
