package eu.contrail.infrastructure_monitoring;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.opennebula.client.vm.VirtualMachine;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class OneVm {
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private DocumentBuilder db;
	private Node docEle = null;
	private XPathFactory factory = XPathFactory.newInstance();
    private XPath xpath = factory.newXPath();
	
    public OneVm(VirtualMachine vm) {
		if(vm != null) {
			try {
				db = dbf.newDocumentBuilder();
				if(vm != null && vm.info() != null) {
					Document dom = db.parse(new ByteArrayInputStream(vm.info().getMessage().getBytes()));
					this.docEle = dom.getDocumentElement();	
				}
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getVmInfo(String xPath) {
		try {
			if(this.docEle != null)
				return this.xpath.evaluate(xPath, docEle);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getVmHostname() {
		return getVmInfo("HISTORY/HOSTNAME");
	}

	public String getVmOwner() {
		return getVmInfo("UID");
	}
	
	public String getVmSource() {
		return getVmInfo("TEMPLATE/DISK/SOURCE");
	}
	
	public String getVmReadOnly() {
		return getVmInfo("TEMPLATE/DISK/READONLY");
	}
	
	public String getVmIpAddress() {
		return getVmInfo("TEMPLATE/NIC/IP");
	}
}
