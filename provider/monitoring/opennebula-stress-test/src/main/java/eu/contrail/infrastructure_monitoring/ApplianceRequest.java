package eu.contrail.infrastructure_monitoring;

import java.util.List;

import eu.contrail.infrastructure_monitoring.VMjson;

public class ApplianceRequest {
	private String applianceName;
	private List<VMjson> vmList;
	public String getApplianceName() {
		return applianceName;
	}
	public void setApplianceName(String applianceName) {
		this.applianceName = applianceName;
	}
	public List<VMjson> getVmList() {
		return vmList;
	}
	public void setVmList(List<VMjson> vmList) {
		this.vmList = vmList;
	}
}
