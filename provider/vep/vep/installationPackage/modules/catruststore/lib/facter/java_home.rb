# java_home.rb

Facter.add("java_home") do
  setcode do
    Facter::Util::Resolution.exec('echo $(readlink -f /usr/bin/java | sed "s:bin/java::")')
  end
end
