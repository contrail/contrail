class deploywar::params {

  ### Application related parameters

  # Let's deal with versions madness
  $pkgver = $::operatingsystem ? {
    ubuntu                          => 'tomcat6',
    debian                          => $::lsbmajdistrelease ? {
      5       => 'tomcat5.5',
      6       => 'tomcat6',
      7       => 'tomcat7',
      default => 'tomcat6',
    },
    /(?i:CentOS|RedHat|Scientific)/ => $::lsbmajdistrelease ? {
      5       => 'tomcat5',
      6       => 'tomcat6',
      default => 'tomcat6',
    },
    /(?i:Amazon)/ => $::lsbmajdistrelease ? {
      3       => 'tomcat6',
      default => 'tomcat6',
    },
    /(?i:SLES|OpenSuSe)/            => 'tomcat6',
    default                         => 'tomcat',
  }

  # Todo: What to do when $pkgver = 'tomcat' without any number?
  $version = inline_template("<%= '${pkgver}'.scan(/\d/).first %>")
  # Application related parameters
  $manager_package = $::operatingsystem ? {
    /(?i:Debian|Ubuntu)/            => "tomcat${version}-admin",
    /(?i:CentOS|RedHat|Scientific)/ => "tomcat${version}-admin-webapps",
    default                         => undef,
  }

  $manager_dir = $::operatingsystem ? {
    /(?i:Debian|Ubuntu)/            => "/usr/share/${pkgver}-admin/manager",
    /(?i:CentOS|RedHat|Scientific)/ => "/var/lib/${pkgver}/webapps/manager",
    default                         => undef,
  }

  $webappdir = $::operatingsystem ? {
    /(?i:Debian|Ubuntu)/            => "/var/lib/${pkgver}/webapps/",
    /(?i:CentOS|RedHat|Scientific)/ => "/var/lib/${pkgver}/webapps/",
    default                         => undef,
  }
}
