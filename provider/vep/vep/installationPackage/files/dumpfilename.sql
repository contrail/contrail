-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: schedulerEmpty
-- ------------------------------------------------------
-- Server version	5.1.63-0+squeeze1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CLOUDTYPE`
--

DROP TABLE IF EXISTS `CLOUDTYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CLOUDTYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(128) DEFAULT NULL,
  `VERSION` varchar(32) DEFAULT NULL,
  `SUPPORTEDCLOUD_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLOUDTYPE`
--
--
-- Table structure for table `COUNTRYLIST`
--

DROP TABLE IF EXISTS `COUNTRYLIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COUNTRYLIST` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(128) DEFAULT NULL,
  `CODE` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COUNTRYLIST`
--


INSERT INTO COUNTRYLIST VALUES (1, 'Afghanistan', 'AF');
               
INSERT INTO COUNTRYLIST VALUES (2, 'Aland Islands', 'AX');
               
INSERT INTO COUNTRYLIST VALUES (3, 'Albania', 'AL');
               
INSERT INTO COUNTRYLIST VALUES (4, 'Algeria', 'DZ');
               
INSERT INTO COUNTRYLIST VALUES (5, 'American Samoa', 'AS');
               
INSERT INTO COUNTRYLIST VALUES (6, 'Andorra', 'AD');
               
INSERT INTO COUNTRYLIST VALUES (7, 'Angola', 'AO');
               
INSERT INTO COUNTRYLIST VALUES (8, 'Anguilla', 'AI');
               
INSERT INTO COUNTRYLIST VALUES (9, 'Antarctica', 'AQ');
               
INSERT INTO COUNTRYLIST VALUES (10, 'Antigua and Barbuda', 'AG');
               
INSERT INTO COUNTRYLIST VALUES (11, 'Argentina', 'AR');
               
INSERT INTO COUNTRYLIST VALUES (12, 'Armenia', 'AM');
               
INSERT INTO COUNTRYLIST VALUES (13, 'Aruba', 'AW');
               
INSERT INTO COUNTRYLIST VALUES (14, 'Australia', 'AU');
               
INSERT INTO COUNTRYLIST VALUES (15, 'Austria', 'AT');
               
INSERT INTO COUNTRYLIST VALUES (16, 'Azerbaijan', 'AZ');
               
INSERT INTO COUNTRYLIST VALUES (17, 'Bahamas', 'BS');
               
INSERT INTO COUNTRYLIST VALUES (18, 'Bahrain', 'BH');
               
INSERT INTO COUNTRYLIST VALUES (19, 'Bangladesh', 'BD');
               
INSERT INTO COUNTRYLIST VALUES (20, 'Barbados', 'BB');
               
INSERT INTO COUNTRYLIST VALUES (21, 'Belarus', 'BY');
               
INSERT INTO COUNTRYLIST VALUES (22, 'Belgium', 'BE');
               
INSERT INTO COUNTRYLIST VALUES (23, 'Belize', 'BZ');
               
INSERT INTO COUNTRYLIST VALUES (24, 'Benin', 'BJ');
               
INSERT INTO COUNTRYLIST VALUES (25, 'Bermuda', 'BM');
               
INSERT INTO COUNTRYLIST VALUES (26, 'Bhutan', 'BT');
               
INSERT INTO COUNTRYLIST VALUES (27, 'Bolivia, Plurinational State of', 'BO');
               
INSERT INTO COUNTRYLIST VALUES (28, 'Bonaire, Sint Eustatius and Saba', 'BQ');
               
INSERT INTO COUNTRYLIST VALUES (29, 'Bosnia and Herzegovina', 'BA');
               
INSERT INTO COUNTRYLIST VALUES (30, 'Botswana', 'BW');
               
INSERT INTO COUNTRYLIST VALUES (31, 'Bouvet Island', 'BV');
               
INSERT INTO COUNTRYLIST VALUES (32, 'Brazil', 'BR');
               
INSERT INTO COUNTRYLIST VALUES (33, 'British Indian Ocean Territory', 'IO');
               
INSERT INTO COUNTRYLIST VALUES (34, 'Brunei Darussalam', 'BN');
               
INSERT INTO COUNTRYLIST VALUES (35, 'Bulgaria', 'BG');
               
INSERT INTO COUNTRYLIST VALUES (36, 'Burkina Faso', 'BF');
               
INSERT INTO COUNTRYLIST VALUES (37, 'Burundi', 'BI');
               
INSERT INTO COUNTRYLIST VALUES (38, 'Cambodia', 'KH');
               
INSERT INTO COUNTRYLIST VALUES (39, 'Cameroon', 'CM');
               
INSERT INTO COUNTRYLIST VALUES (40, 'Canada', 'CA');
               
INSERT INTO COUNTRYLIST VALUES (41, 'Cape Verde', 'CV');
               
INSERT INTO COUNTRYLIST VALUES (42, 'Cayman Islands', 'KY');
               
INSERT INTO COUNTRYLIST VALUES (43, 'Central African Republic', 'CF');
               
INSERT INTO COUNTRYLIST VALUES (44, 'Chad', 'TD');
               
INSERT INTO COUNTRYLIST VALUES (45, 'Chile', 'CL');
               
INSERT INTO COUNTRYLIST VALUES (46, 'China', 'CN');
               
INSERT INTO COUNTRYLIST VALUES (47, 'Christmas Island', 'CX');
               
INSERT INTO COUNTRYLIST VALUES (48, 'Cocos (Keeling); Islands', 'CC');
               
INSERT INTO COUNTRYLIST VALUES (49, 'Colombia', 'CO');
               
INSERT INTO COUNTRYLIST VALUES (50, 'Comoros', 'KM');
               
INSERT INTO COUNTRYLIST VALUES (51, 'Congo', 'CG');
               
INSERT INTO COUNTRYLIST VALUES (52, 'Congo, the Democratic Republic of the', 'CD');
               
INSERT INTO COUNTRYLIST VALUES (53, 'Cook Islands', 'CK');
               
INSERT INTO COUNTRYLIST VALUES (54, 'Costa Rica', 'CR');
               
INSERT INTO COUNTRYLIST VALUES (55, 'Côte d-Ivoire', 'CI');
               
INSERT INTO COUNTRYLIST VALUES (56, 'Croatia', 'HR');
               
INSERT INTO COUNTRYLIST VALUES (57, 'Cuba', 'CU');
               
INSERT INTO COUNTRYLIST VALUES (58, 'Curaçao', 'CW');
               
INSERT INTO COUNTRYLIST VALUES (59, 'Cyprus', 'CY');
               
INSERT INTO COUNTRYLIST VALUES (60, 'Czech Republic', 'CZ');
               
INSERT INTO COUNTRYLIST VALUES (61, 'Denmark', 'DK');
               
INSERT INTO COUNTRYLIST VALUES (62, 'Djibouti', 'DJ');
               
INSERT INTO COUNTRYLIST VALUES (63, 'Dominica', 'DM');
               
INSERT INTO COUNTRYLIST VALUES (64, 'Dominican Republic', 'DO');
               
INSERT INTO COUNTRYLIST VALUES (65, 'Ecuador', 'EC');
               
INSERT INTO COUNTRYLIST VALUES (66, 'Egypt', 'EG');
               
INSERT INTO COUNTRYLIST VALUES (67, 'El Salvador', 'SV');
               
INSERT INTO COUNTRYLIST VALUES (68, 'Equitorial Guinea', 'GQ');
               
INSERT INTO COUNTRYLIST VALUES (69, 'Eritrea', 'ER');
               
INSERT INTO COUNTRYLIST VALUES (70, 'Estonia', 'EE');
               
INSERT INTO COUNTRYLIST VALUES (71, 'Ethiopia', 'ET');
               
INSERT INTO COUNTRYLIST VALUES (72, 'Falkland Islands (Malvinas);', 'FK');
               
INSERT INTO COUNTRYLIST VALUES (73, 'Faroe Islands', 'FO');
               
INSERT INTO COUNTRYLIST VALUES (74, 'Fiji', 'FJ');
               
INSERT INTO COUNTRYLIST VALUES (75, 'Finland', 'FI');
               
INSERT INTO COUNTRYLIST VALUES (76, 'France', 'FR');
               
INSERT INTO COUNTRYLIST VALUES (77, 'French Guiana', 'GF');
               
INSERT INTO COUNTRYLIST VALUES (78, 'French Polynesia', 'PF');
               
INSERT INTO COUNTRYLIST VALUES (79, 'French Southern Territories', 'TF');
               
INSERT INTO COUNTRYLIST VALUES (80, 'Gabon', 'GA');
               
INSERT INTO COUNTRYLIST VALUES (81, 'Gambia', 'GM');
               
INSERT INTO COUNTRYLIST VALUES (82, 'Georgia', 'GE');
               
INSERT INTO COUNTRYLIST VALUES (83, 'Germany', 'DE');
               
INSERT INTO COUNTRYLIST VALUES (84, 'Ghana', 'GH');
               
INSERT INTO COUNTRYLIST VALUES (85, 'Gibraltar', 'GI');
               
INSERT INTO COUNTRYLIST VALUES (86, 'Greece', 'GR');
               
INSERT INTO COUNTRYLIST VALUES (87, 'Greenland', 'GL');
               
INSERT INTO COUNTRYLIST VALUES (88, 'Grenada', 'GD');
               
INSERT INTO COUNTRYLIST VALUES (89, 'Guadeloupe', 'GP');
               
INSERT INTO COUNTRYLIST VALUES (90, 'Guam', 'GU');
               
INSERT INTO COUNTRYLIST VALUES (91, 'Guatemala', 'GT');
               
INSERT INTO COUNTRYLIST VALUES (92, 'Guernsey', 'GG');
               
INSERT INTO COUNTRYLIST VALUES (93, 'Guinea', 'GN');
               
INSERT INTO COUNTRYLIST VALUES (94, 'Guinea-Bissau', 'GW');
               
INSERT INTO COUNTRYLIST VALUES (95, 'Guyana', 'GY');
               
INSERT INTO COUNTRYLIST VALUES (96, 'Haiti', 'HT');
               
INSERT INTO COUNTRYLIST VALUES (97, 'Heard Island and McDonald Islands', 'HM');
               
INSERT INTO COUNTRYLIST VALUES (98, 'Holy See (Vatican City State);', 'VA');
               
INSERT INTO COUNTRYLIST VALUES (99, 'Honduras', 'HN');
               
INSERT INTO COUNTRYLIST VALUES (100, 'Hong Kong', 'HK');
    
INSERT INTO COUNTRYLIST VALUES (101, 'Hungary', 'HU');
               
INSERT INTO COUNTRYLIST VALUES (102, 'Iceland', 'IS');
               
INSERT INTO COUNTRYLIST VALUES (103, 'India', 'IN');
               
INSERT INTO COUNTRYLIST VALUES (104, 'Indonesia', 'ID');
               
INSERT INTO COUNTRYLIST VALUES (105, 'Iran, Islamic Republic of', 'IR');
               
INSERT INTO COUNTRYLIST VALUES (106, 'Iraq', 'IQ');
               
INSERT INTO COUNTRYLIST VALUES (107, 'Ireland', 'IE');
               
INSERT INTO COUNTRYLIST VALUES (108, 'Isle of Man', 'IM');
               
INSERT INTO COUNTRYLIST VALUES (109, 'Israel', 'IL');
               
INSERT INTO COUNTRYLIST VALUES (110, 'Italy', 'IT');
               
INSERT INTO COUNTRYLIST VALUES (111, 'Jamaica', 'JM');
               
INSERT INTO COUNTRYLIST VALUES (112, 'Japan', 'JP');
               
INSERT INTO COUNTRYLIST VALUES (113, 'Jersey', 'JE');
               
INSERT INTO COUNTRYLIST VALUES (114, 'Jordan', 'JO');
               
INSERT INTO COUNTRYLIST VALUES (115, 'Kazakhstan', 'KZ');
               
INSERT INTO COUNTRYLIST VALUES (116, 'Kenya', 'KE');
               
INSERT INTO COUNTRYLIST VALUES (117, 'Kiribati', 'KI');
               
INSERT INTO COUNTRYLIST VALUES (118, 'Korea, Democratic People-s Republic of', 'KP');
               
INSERT INTO COUNTRYLIST VALUES (119, 'Korea, Republic of', 'KR');
               
INSERT INTO COUNTRYLIST VALUES (120, 'Kuwait', 'KW');
               
INSERT INTO COUNTRYLIST VALUES (121, 'Kyrgyzstan', 'KG');
               
INSERT INTO COUNTRYLIST VALUES (122, 'Lao People-s Democratic Republic', 'LA');
               
INSERT INTO COUNTRYLIST VALUES (123, 'Latvia', 'LV');
               
INSERT INTO COUNTRYLIST VALUES (124, 'Lebanon', 'LB');
               
INSERT INTO COUNTRYLIST VALUES (125, 'Lesotho', 'LS');
  
INSERT INTO COUNTRYLIST VALUES (126, 'Liberia', 'LR');
               
INSERT INTO COUNTRYLIST VALUES (127, 'Libya', 'LY');
               
INSERT INTO COUNTRYLIST VALUES (128, 'Liechtenstein', 'LI');
               
INSERT INTO COUNTRYLIST VALUES (129, 'Lithuania', 'LT');
               
INSERT INTO COUNTRYLIST VALUES (130, 'Luxembourg', 'LU');
               
INSERT INTO COUNTRYLIST VALUES (131, 'Macao', 'MO');
               
INSERT INTO COUNTRYLIST VALUES (132, 'Macedonia, the former Yugoslav Republic of', 'MK');
               
INSERT INTO COUNTRYLIST VALUES (133, 'Madagascar', 'MG');
               
INSERT INTO COUNTRYLIST VALUES (134, 'Malawi', 'MW');
               
INSERT INTO COUNTRYLIST VALUES (135, 'Malaysia', 'MY');
               
INSERT INTO COUNTRYLIST VALUES (136, 'Maldives', 'MV');
               
INSERT INTO COUNTRYLIST VALUES (137, 'Mali', 'ML');
               
INSERT INTO COUNTRYLIST VALUES (138, 'Malta', 'MT');
               
INSERT INTO COUNTRYLIST VALUES (139, 'Marshall Islands', 'MH');
               
INSERT INTO COUNTRYLIST VALUES (140, 'Martinique', 'MQ');
               
INSERT INTO COUNTRYLIST VALUES (141, 'Mauritania', 'MR');
               
INSERT INTO COUNTRYLIST VALUES (142, 'Mauritius', 'MU');
               
INSERT INTO COUNTRYLIST VALUES (143, 'Mayotte', 'YT');
               
INSERT INTO COUNTRYLIST VALUES (144, 'Mexico', 'MX');
               
INSERT INTO COUNTRYLIST VALUES (145, 'Micronesia, Federated States of', 'FM');
               
INSERT INTO COUNTRYLIST VALUES (146, 'Moldova, Republic of', 'MD');
               
INSERT INTO COUNTRYLIST VALUES (147, 'Monaco', 'MC');
               
INSERT INTO COUNTRYLIST VALUES (148, 'Mongolia', 'MN');
               
INSERT INTO COUNTRYLIST VALUES (149, 'Montenegro', 'ME');
               
INSERT INTO COUNTRYLIST VALUES (150, 'Montserrat', 'MS');

INSERT INTO COUNTRYLIST VALUES (151, 'Morocco', 'MA');
               
INSERT INTO COUNTRYLIST VALUES (152, 'Mozambique', 'MZ');
               
INSERT INTO COUNTRYLIST VALUES (153, 'Myanmar', 'MM');
               
INSERT INTO COUNTRYLIST VALUES (154, 'Namibia', 'NA');
               
INSERT INTO COUNTRYLIST VALUES (155, 'Nauru', 'NR');
               
INSERT INTO COUNTRYLIST VALUES (156, 'Nepal', 'NP');
               
INSERT INTO COUNTRYLIST VALUES (157, 'Netherlands', 'NL');
               
INSERT INTO COUNTRYLIST VALUES (158, 'New Caledonia', 'NC');
               
INSERT INTO COUNTRYLIST VALUES (159, 'New Zealand', 'NZ');
               
INSERT INTO COUNTRYLIST VALUES (160, 'Nicaragua', 'NI');
               
INSERT INTO COUNTRYLIST VALUES (161, 'Niger', 'NE');
               
INSERT INTO COUNTRYLIST VALUES (162, 'Nigeria', 'NG');
               
INSERT INTO COUNTRYLIST VALUES (163, 'Niue', 'NU');
               
INSERT INTO COUNTRYLIST VALUES (164, 'Norfolk Island', 'NF');
               
INSERT INTO COUNTRYLIST VALUES (165, 'Northern Mariana Islands', 'MP');
               
INSERT INTO COUNTRYLIST VALUES (166, 'Norway', 'NO');
               
INSERT INTO COUNTRYLIST VALUES (167, 'Oman', 'OM');
               
INSERT INTO COUNTRYLIST VALUES (168, 'Pakistan', 'PK');
               
INSERT INTO COUNTRYLIST VALUES (169, 'Palau', 'PW');
               
INSERT INTO COUNTRYLIST VALUES (170, 'Palestinian Territory, Occupied', 'PS');
               
INSERT INTO COUNTRYLIST VALUES (171, 'Panama', 'PA');
               
INSERT INTO COUNTRYLIST VALUES (172, 'Papua New Guinea', 'PG');
               
INSERT INTO COUNTRYLIST VALUES (173, 'Paraguay', 'PY');
               
INSERT INTO COUNTRYLIST VALUES (174, 'Peru', 'PE');
               
INSERT INTO COUNTRYLIST VALUES (175, 'Philippines', 'PH');
               
INSERT INTO COUNTRYLIST VALUES (176, 'Pitcairn', 'PN');
               
INSERT INTO COUNTRYLIST VALUES (177, 'Poland', 'PL');
               
INSERT INTO COUNTRYLIST VALUES (178, 'Portugal', 'PT');
               
INSERT INTO COUNTRYLIST VALUES (179, 'Puerto Rico', 'PR');
               
INSERT INTO COUNTRYLIST VALUES (180, 'Qatar', 'QA');
               
INSERT INTO COUNTRYLIST VALUES (181, 'Réunion', 'RE');
               
INSERT INTO COUNTRYLIST VALUES (182, 'Romania', 'RO');
               
INSERT INTO COUNTRYLIST VALUES (183, 'Russian Federation', 'RU');
               
INSERT INTO COUNTRYLIST VALUES (184, 'Rwanda', 'RW');
               
INSERT INTO COUNTRYLIST VALUES (185, 'Saint Barthélemy', 'BL');
               
INSERT INTO COUNTRYLIST VALUES (186, 'Saint Helena, Ascension and Tristan da Cunha', 'SH');
               
INSERT INTO COUNTRYLIST VALUES (187, 'Saint Kitts and Nevis', 'KN');
               
INSERT INTO COUNTRYLIST VALUES (188, 'Saint Lucia', 'LC');
               
INSERT INTO COUNTRYLIST VALUES (189, 'Saint Martin (French part);', 'MF');
               
INSERT INTO COUNTRYLIST VALUES (190, 'Saint Pierre and Miquelon', 'PM');
               
INSERT INTO COUNTRYLIST VALUES (191, 'Saint Vincent and the Grenadines', 'VC');
               
INSERT INTO COUNTRYLIST VALUES (192, 'Samoa', 'WS');
               
INSERT INTO COUNTRYLIST VALUES (193, 'San Marino', 'SM');
               
INSERT INTO COUNTRYLIST VALUES (194, 'Sao Tome and Principe', 'ST');
               
INSERT INTO COUNTRYLIST VALUES (195, 'Saudi Arabia', 'SA');
               
INSERT INTO COUNTRYLIST VALUES (196, 'Senegal', 'SN');
               
INSERT INTO COUNTRYLIST VALUES (197, 'Serbia', 'RS');
               
INSERT INTO COUNTRYLIST VALUES (198, 'Seychelles', 'SC');
               
INSERT INTO COUNTRYLIST VALUES (199, 'Sierra Leone', 'SL');
               
INSERT INTO COUNTRYLIST VALUES (200, 'Singapore', 'SG');
               
INSERT INTO COUNTRYLIST VALUES (201, 'Sint Maarten (Dutch part);', 'SX');
               
INSERT INTO COUNTRYLIST VALUES (202, 'Slovakia', 'SK');
               
INSERT INTO COUNTRYLIST VALUES (203, 'Slovenia', 'SI');
               
INSERT INTO COUNTRYLIST VALUES (204, 'Solomon Islands', 'SB');
               
INSERT INTO COUNTRYLIST VALUES (205, 'Somalia', 'SO');
               
INSERT INTO COUNTRYLIST VALUES (206, 'South Africa', 'ZA');
               
INSERT INTO COUNTRYLIST VALUES (207, 'South Georgia and the South Sandwich Islands', 'GS');
               
INSERT INTO COUNTRYLIST VALUES (208, 'South Sudan', 'SS');
               
INSERT INTO COUNTRYLIST VALUES (209, 'Spain', 'ES');
               
INSERT INTO COUNTRYLIST VALUES (210, 'Sri Lanka', 'LK');
               
INSERT INTO COUNTRYLIST VALUES (211, 'Sudan', 'SD');
               
INSERT INTO COUNTRYLIST VALUES (212, 'Suriname', 'SR');
               
INSERT INTO COUNTRYLIST VALUES (213, 'Svalbard and Jan Mayen', 'SJ');
               
INSERT INTO COUNTRYLIST VALUES (214, 'Swaziland', 'SZ');
               
INSERT INTO COUNTRYLIST VALUES (215, 'Sweden', 'SE');
               
INSERT INTO COUNTRYLIST VALUES (216, 'Switzerland', 'CH');
               
INSERT INTO COUNTRYLIST VALUES (217, 'Syrian Arab Republic', 'SY');
               
INSERT INTO COUNTRYLIST VALUES (218, 'Taiwan, Province of China', 'TW');
               
INSERT INTO COUNTRYLIST VALUES (219, 'Tajikistan', 'TJ');
               
INSERT INTO COUNTRYLIST VALUES (220, 'Tanzania, United Republic of', 'TZ');
               
INSERT INTO COUNTRYLIST VALUES (221, 'Thailand', 'TH');
               
INSERT INTO COUNTRYLIST VALUES (222, 'Timor-Leste', 'TL');
               
INSERT INTO COUNTRYLIST VALUES (223, 'Togo', 'TG');
               
INSERT INTO COUNTRYLIST VALUES (224, 'Tokelau', 'TK');
               
INSERT INTO COUNTRYLIST VALUES (225, 'Tonga', 'TO');
               
INSERT INTO COUNTRYLIST VALUES (226, 'Trinidad and Tobago', 'TT');
               
INSERT INTO COUNTRYLIST VALUES (227, 'Tunisia', 'TN');
               
INSERT INTO COUNTRYLIST VALUES (228, 'Turkey', 'TR');
               
INSERT INTO COUNTRYLIST VALUES (229, 'Turkmenistan', 'TM');
               
INSERT INTO COUNTRYLIST VALUES (230, 'Turks and Caicos Islands', 'TC');
               
INSERT INTO COUNTRYLIST VALUES (231, 'Tuvalu', 'TV');
               
INSERT INTO COUNTRYLIST VALUES (232, 'Uganda', 'UG');
               
INSERT INTO COUNTRYLIST VALUES (233, 'Ukraine', 'UA');
               
INSERT INTO COUNTRYLIST VALUES (234, 'United Arab Emirates', 'AE');
               
INSERT INTO COUNTRYLIST VALUES (235, 'United Kingdom', 'GB');
               
INSERT INTO COUNTRYLIST VALUES (236, 'United States', 'US');
               
INSERT INTO COUNTRYLIST VALUES (237, 'United States Minor Outlying Islands', 'UM');
               
INSERT INTO COUNTRYLIST VALUES (238, 'Uruguay', 'UY');
               
INSERT INTO COUNTRYLIST VALUES (239, 'Uzbekistan', 'UZ');
               
INSERT INTO COUNTRYLIST VALUES (240, 'Vanuatu', 'VU');
               
INSERT INTO COUNTRYLIST VALUES (241, 'Venezuela, Bolivarian Republic of', 'VE');
               
INSERT INTO COUNTRYLIST VALUES (242, 'Viet Nam', 'VN');
               
INSERT INTO COUNTRYLIST VALUES (243, 'Virgin Islands, British', 'VG');
               
INSERT INTO COUNTRYLIST VALUES (244, 'Virgin Islands, U.S.', 'VI');
               
INSERT INTO COUNTRYLIST VALUES (245, 'Wallis and Futuna', 'WF');
               
INSERT INTO COUNTRYLIST VALUES (246, 'Western Sahara', 'EH');
               
INSERT INTO COUNTRYLIST VALUES (247, 'Yemen', 'YE');
               
INSERT INTO COUNTRYLIST VALUES (248, 'Zambia', 'ZM');
               
INSERT INTO COUNTRYLIST VALUES (249, 'Zimbabwe', 'ZW');




















--
-- Table structure for table `DATACENTER`
--


DROP TABLE IF EXISTS `DATACENTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATACENTER` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `VERSION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_DATACENTER_COUNTRY_ID` (`COUNTRY_ID`),
  CONSTRAINT `FK_DATACENTER_COUNTRYLIST` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `COUNTRYLIST` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `FK_DATACENTER_COUNTRY_ID` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `COUNTRYLIST` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATACENTER`
--



--
-- Table structure for table `CLUSTER`
--

DROP TABLE IF EXISTS `CLUSTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CLUSTER` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DATACENTER_ID` int(11) DEFAULT NULL,
  `INTERCONNECT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_CLUSTER_DATACENTER_ID` (`DATACENTER_ID`),
  CONSTRAINT `FK_CLUSTER_DATACENTER_ID` FOREIGN KEY (`DATACENTER_ID`) REFERENCES `DATACENTER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLUSTER`
--

--
-- Table structure for table `RACK`
--

DROP TABLE IF EXISTS `RACK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RACK` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CLUSTER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RACK_CLUSTER_ID` (`CLUSTER_ID`),
  CONSTRAINT `FK_RACK_CLUSTER_ID` FOREIGN KEY (`CLUSTER_ID`) REFERENCES `CLUSTER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RACK`
--



--
-- Table structure for table `HOST`
--

DROP TABLE IF EXISTS `HOST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST` (
  `ID` int(11) NOT NULL,
  `CORECOUNT` int(11) DEFAULT NULL,
  `CPUARCH` varchar(255) DEFAULT NULL,
  `DISKSIZE` double DEFAULT NULL,
  `MEMORYSIZE` double DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RACK_ID` int(11) DEFAULT NULL,
  `CPUFREQ` int(11) DEFAULT NULL,
  `L2SWITCH_ID` int(11) DEFAULT NULL,
  `CLOUDTYPE_ID` int(11) DEFAULT NULL,
  `IAASID` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_HOST_RACK_ID` (`RACK_ID`),
  KEY `FK_HOST_CLOUDTYPE` (`CLOUDTYPE_ID`),
  CONSTRAINT `FK_HOST_CLOUDTYPE` FOREIGN KEY (`CLOUDTYPE_ID`) REFERENCES `CLOUDTYPE` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_HOST_RACK_ID` FOREIGN KEY (`RACK_ID`) REFERENCES `RACK` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST`
--


DROP TABLE IF EXISTS `VMSLOT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VMSLOT` (
  `ID` int(11) NOT NULL,
  `CPUSPEED` double DEFAULT '0',
  `DISKSIZE` double DEFAULT '0',
  `MEMORYSIZE` double DEFAULT '0',
  `CPUCOUNT` int(11) DEFAULT NULL,
  `HOST_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_VMSLOT_HOST` (`HOST_ID`),
  CONSTRAINT `FK_VMSLOT_HOST` FOREIGN KEY (`HOST_ID`) REFERENCES `HOST` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VMSLOT`
--

/*!40000 ALTER TABLE `VMSLOT` DISABLE KEYS */;
INSERT INTO `VMSLOT` VALUES (1,0,0,0,0,1);


--
-- Table structure for table `RESERVATION`
--

DROP TABLE IF EXISTS `RESERVATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RESERVATION` (
  `ID` bigint(20) NOT NULL,
  `SUBMITTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESERVATION`
--

--
-- Table structure for table `VMRESERVATION`
--

DROP TABLE IF EXISTS `VMRESERVATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VMRESERVATION` (
  `ID` int(11) NOT NULL,
  `CPUSPEED` double DEFAULT NULL,
  `DISKSIZE` double DEFAULT NULL,
  `MEMORYSIZE` double DEFAULT NULL,
  `RESERVATION_ID` bigint(20) DEFAULT NULL,
  `CPUCOUNT` int(11) DEFAULT NULL,
  `SLOTS` int(11) DEFAULT NULL,
  `ENDTIME` datetime DEFAULT NULL,
  `STARTTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_VMRESERVATION_RESERVATION_ID` (`RESERVATION_ID`),
  CONSTRAINT `FK_VMRESERVATION_RESERVATION_ID` FOREIGN KEY (`RESERVATION_ID`) REFERENCES `RESERVATION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VMRESERVATION`
--




--
-- Table structure for table `SINGLERESERVATIONSLOT`
--

DROP TABLE IF EXISTS `SINGLERESERVATIONSLOT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SINGLERESERVATIONSLOT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HOST_ID` int(11) DEFAULT NULL,
  `VMRESERVATION_ID` int(11) DEFAULT NULL,
  `VMSLOT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `VMRESERVATION_ID` (`VMRESERVATION_ID`),
  KEY `VMSLOT_ID` (`VMSLOT_ID`),
  CONSTRAINT `SINGLERESERVATIONSLOT_ibfk_1` FOREIGN KEY (`VMRESERVATION_ID`) REFERENCES `VMRESERVATION` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `SINGLERESERVATIONSLOT_ibfk_2` FOREIGN KEY (`VMSLOT_ID`) REFERENCES `VMSLOT` (`ID`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SINGLERESERVATIONSLOT`
--
-- Table structure for table `STORAGE`
--

DROP TABLE IF EXISTS `STORAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORAGE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(128) DEFAULT NULL,
  `STORAGETYPE_ID` int(11) DEFAULT NULL,
  `ENDPOINT` varchar(256) DEFAULT NULL,
  `RACK_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_STORAGE_RACK` (`RACK_ID`),
  KEY `FK_STORAGE_STORAGETYPE` (`STORAGETYPE_ID`),
  CONSTRAINT `FK_STORAGE_RACK` FOREIGN KEY (`RACK_ID`) REFERENCES `RACK` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_STORAGE_STORAGETYPE` FOREIGN KEY (`STORAGETYPE_ID`) REFERENCES `STORAGETYPE` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGE`
--


--
-- Table structure for table `STORAGETYPE`
--

DROP TABLE IF EXISTS `STORAGETYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORAGETYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGETYPE`
--



insert into CLOUDTYPE values(0,"c1","0",0);
insert into CLOUDTYPE values(1,"c1","0",0);
insert into CLOUDTYPE values(2,"c2","0",0);
insert into CLOUDTYPE values(3,"c3","0",0);

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-11 14:47:07
