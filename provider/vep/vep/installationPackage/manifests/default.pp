$installation_dir=hiera("installation_dir")
$dumpFile="${installation_dir}/files/dumpfilename.sql"
$warpath="${installation_dir}/files/scheduler.war"
$dbpath=hiera("dbvep",undef)
$vep_target='/etc/VEPRestKeyStore.jks'
$private_keyInst="${installation_dir}/files/veplocal.key"
$public_keyInst="${installation_dir}/files/veplocal.crt"
$running_dirInst="${installation_dir}/vep/"
$capath="${installation_dir}/files/ca.crt"

class{"tomcat":}

class{"deploywar":
     war=>$warpath,
     namewar=>'scheduler.war',
     }


class{"mysql":
    root_password=>'root',
    }
    
mysql::grant { 'schedulerEmpty':
  mysql_privileges => 'ALL',
  mysql_password => 'password',
  mysql_db => 'schedulerEmpty',
  mysql_user => 'scheduler',
  mysql_host => 'localhost',
  mysql_db_init_query_file => $dumpFile,
}

java_ks { 'vep':
  ensure       => present,
  private_key  => $private_keyInst,
  certificate  => $public_keyInst,
  target       => $vep_target,
  password     => 'pass1234',
  require => Class[tomcat],
  before => Class["vep"];
}

class{"vep":
   db=>$dbpath,
   running_dir=>$running_dirInst,
   vepkeystore=>$vep_target,
}

class{"catruststore":
ca_path=>$capath,
ca_name=>"vep_CA",
}
