/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;

public class Authenticator {
	
	private static Logger logger = Logger.getLogger("VEP.Authenticator");;
	private static DBHandler db = new DBHandler("User", VEPHelperMethods.getProperty("vepdb.choice", logger));

	public static boolean checkResourceAccess(String requesterUname, String resourceId, String resourceType) throws SQLException {
		boolean access = false;
		
		if(resourceType.equalsIgnoreCase("CEE"))
		{
			access = checkCEEAccess(requesterUname, resourceId);
		}else{
                   if(resourceType.equalsIgnoreCase("User")){
                       access=true;
                   }else{
                       if(resourceType.equalsIgnoreCase("ProviderHandlersConstraints"))
                           access=true;
                   }    
                }
                //logger.info("ACCESS:"+access);
		return access;
		//return true;//TODO:Test Hack
	}

	private static boolean checkCEEAccess(String requesterUname, String resourceId) throws SQLException {
		boolean access = false;
		//retrieve userid and role
		ResultSet rs = db.query("select", "id, role", "user", "where username = '"+requesterUname+"'");
		if (rs.next())
		{
			
			int requesterId = rs.getInt("id");
			String role = rs.getString("role");
			
			if(resourceId == null)
			{
				//resource creation is ok for all users
				access = true;
			} else
			{
				//Check if user is an admin
				if (role.contains("administrator")) {
					access = true;
				} else {
					//If not, check if user is owner of CEE
					rs = db.query("select", "userid", "cee", "where id = "+resourceId);
					int ceeUserId = rs.getInt("userid");
					if (requesterId == ceeUserId)
					{
						access = true;
					} else {
						//If not check permission (later)
					}
				}	
			}	
		}	
		return access;
	}

	/**
	 * used to auth users to do get/post/put queries on specific resources
	 * Example : authorize(username, resource)
	 */
	
}
