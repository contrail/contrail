/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.util.ajax.JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


import org.ow2.contrail.provider.vep.objects.Application;
import org.ow2.contrail.provider.vep.objects.Application.Reservation;
import org.ow2.contrail.provider.vep.objects.ApplicationCollection;
import org.ow2.contrail.provider.vep.objects.Authenticator;
import org.ow2.contrail.provider.vep.objects.CEE;
import org.ow2.contrail.provider.vep.objects.CEE.ConstraintMap;
import org.ow2.contrail.provider.vep.objects.CEE.DefaultMap;
import org.ow2.contrail.provider.vep.objects.CEECollection;
import org.ow2.contrail.provider.vep.objects.Constraint;
import org.ow2.contrail.provider.vep.objects.ConstraintCollection;
import org.ow2.contrail.provider.vep.objects.NetHandler;
import org.ow2.contrail.provider.vep.objects.NetHandlerCollection;
import org.ow2.contrail.provider.vep.objects.OVF;
import org.ow2.contrail.provider.vep.objects.StaticConstraint;
import org.ow2.contrail.provider.vep.objects.StaticConstraintCollection;
import org.ow2.contrail.provider.vep.objects.StorageHandler;
import org.ow2.contrail.provider.vep.objects.StorageHandlerCollection;
import org.ow2.contrail.provider.vep.objects.User;
import org.ow2.contrail.provider.vep.objects.VEPCollection;
import org.ow2.contrail.provider.vep.objects.VEPVirtualMachine;
import org.ow2.contrail.provider.vep.objects.VMHandler;
import org.ow2.contrail.provider.vep.objects.VMHandlerCollection;
import org.apache.log4j.Logger;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.StringRepresentation;

public class CIMIParser {
	
	/**
	 * Parses the JSON from a PUT request and creates the user
	 * @param data JSON data from the request
	 * @return a User to store in the DB
	 */
	public static User userCreate(String requesterUname, String data){		
		User user = null;
		JSONObject json = (JSONObject) JSONValue.parse(data);
		//TODO: Parse the JSON
		String uname = (String)json.get("name");
		JSONObject properties = (JSONObject)json.get("properties");
		String email = (String)properties.get("email");
		String role = (String)properties.get("role");
		String password = (String)properties.get("password");
		if(uname != null && email != null && role != null)
		{
			user = new User(true);
			user.setUsername(uname);
			user.setEmail(email);
			user.setRole(role);
			user.setPassword(password);
			// Are there any CEEs? If yes add them to the user
//			JSONArray cees = (JSONArray)json.get("CEEs");
//			for (int i = 0; i<cees.size(); i++)
//			{
//				JSONObject cee = (JSONObject) cees.get(i);
//				CEE c = CIMIParser.ceeCreate(requesterUname, cee.toJSONString(),null);
//				if (c != null)
//					user.getCees().add(c);
//			}
			// Are there any CEETemplate?
			// Are there any OVFTemplate?
		}
		
		return user;
	}
	
	/**
	 * Parses the JSON from a PUT request and updates the user
	 * @param userid id of the user to be updated
	 * @param data JSON data from the request
	 * @return a User to store in the DB
	 */
	public static User userUpdate(String requesterUname, String userid, String data){
		User user = new User(true);
		JSONObject json = (JSONObject) JSONValue.parse(data);
		//TODO: Parse the JSON
		
		return user;
	}

	/**
	 * Retrieves the JSONObject containing the values extracted from a User object
	 * @param userid the id of the user to inspect
	 * @return A JSONObject
	 * @throws SQLException if the user does not exist
	 */
	@SuppressWarnings("unchecked") // Suppressed warning due to SimpleJson extending hashmap and no way to parameterize it
	public static JSONObject userRetrieve(String requesterUname, String userid) throws SQLException{
		JSONObject json = new JSONObject();
		User user = new User(userid);
		//TODO: put data from user into json
		boolean ok = user.retrieveFullUser();
		if (ok){
			json.put("id", "/user/" + userid); // add complete link?
			//retrieve User properties
			json.put("name", user.getUsername());
			JSONObject properties = new JSONObject();
			properties.put("email", user.getEmail());
			properties.put("role", user.getRole());
			json.put("properties", properties);
			
			//Only get a link to a collection of cees, CEE object does NOT contain any CEE, they were not retrieved from the DB to save time
			JSONObject collectionCee = new JSONObject();
			collectionCee.put("href", "/user/" + userid + "/cees");
			json.put("CEEs", collectionCee);
			/*
			//retrieve CEEs array
			JSONArray arrayCees = new JSONArray();
			for (CEE cee : user.getCees()){
				JSONObject jCee = new JSONObject();
				jCee.put("name", cee.getName());
				jCee.put("href", cee.getHref());
				arrayCees.add(jCee);
			}
			json.put("CEEs", arrayCees);*/
			
		}
		return json;
	}
	
	public static CEE ceeCreate(String requesterUname, String data, String ceeId) throws SQLException, UnauthorizedRestAccessException{
		CEE cee = null;
		boolean authorized = Authenticator.checkResourceAccess(requesterUname, ceeId, "CEE");
		if(authorized)
		{
			if(ceeId == null)
				cee = new CEE();
			else
				cee = new CEE(ceeId);
			JSONObject json = (JSONObject) JSONValue.parse(data);
			String state = (String)json.get("state");
			String name = (String)json.get("name");
			JSONArray vmhs = (JSONArray)json.get("VMHandlers");
			JSONArray shs = (JSONArray)json.get("storageHandlers");
			JSONArray nhs = (JSONArray)json.get("networkHandlers");
			JSONArray apps = (JSONArray)json.get("applications");
			JSONArray cms = (JSONArray)json.get("constraintsMapping");
			JSONArray dms = (JSONArray)json.get("defaultMapping");
			
			//register requesterUname as future CEE author
			cee.setUserName(requesterUname);
			//cee.setUserId("1");
			
			
			if (name != null) cee.setName(name);
			if (state != null) cee.setStatus(state);
			//VM Handlers
			if (vmhs != null)
			{
				ArrayList<String> vmhids = new ArrayList<String>(); 
				for(int i = 0; i<vmhs.size(); i++)
				{
					String href = (String)((JSONObject)vmhs.get(i)).get("href");
					String vmhId = getLastURI(href);
					vmhids.add(vmhId);
				}
				cee.updateVMHandlers(vmhids);
			}
			
			//Network Handlers
			if (nhs != null)
			{
				ArrayList<String> nhids = new ArrayList<String>(); 
				for(int i = 0; i<nhs.size(); i++)
				{
					String href = (String)((JSONObject)nhs.get(i)).get("href");
					String nhId = getLastURI(href);
					nhids.add(nhId);
				}
				cee.updateNetworkHandlers(nhids);
			}
			
			//Storage Handlers
			if (shs != null)
			{
				ArrayList<String> shids = new ArrayList<String>(); 
				for(int i = 0; i<shs.size(); i++)
				{
					String href = (String)((JSONObject)shs.get(i)).get("href");
					String shId = getLastURI(href);
					shids.add(shId);
				}
				cee.updateStorageHandlers(shids);
			}
			
			//Applications
			if(apps != null)
			{
				ArrayList<Application> appsids = new ArrayList<Application>();
				for(int i = 0; i<apps.size(); i++)
				{
					Application ap = CIMIParser.appCreate(requesterUname, ((JSONObject)apps.get(i)).toJSONString(),null,null);
					appsids.add(ap);
					
				}
				cee.updateApplications(appsids);
			}

			//constraints mapping
			if(cms != null)
			{
				ArrayList<ConstraintMap> cmids = new ArrayList<CEE.ConstraintMap>();
				ArrayList<ConstraintMap> storagecmids = new ArrayList<CEE.ConstraintMap>();
				for(int i = 0; i<cms.size(); i++)
				{
					
					JSONObject jcm  = (JSONObject)cms.get(i);
					JSONArray virtualSystems = (JSONArray)jcm.get("virtualSystem");
					if(virtualSystems != null)
					{
						ArrayList<String> vss = new ArrayList<String>();
						for(int j=0;j<virtualSystems.size();j++)
						{
							vss.add(((String)((JSONObject)virtualSystems.get(j)).get("href")).substring(1));
						}
						CEE.ConstraintMap cm = cee.new ConstraintMap(getLastURI((String)((JSONObject)jcm.get("constraint")).get("href")), (String)jcm.get("parameter"), vss);
						cmids.add(cm);
					} else 
					{
						JSONArray storage = (JSONArray)jcm.get("storage");
						ArrayList<String> vss = new ArrayList<String>();
						for(int j=0;j<storage.size();j++)
						{
							vss.add(((String)((JSONObject)storage.get(j)).get("href")).substring(1));
						}
						CEE.ConstraintMap cm = cee.new ConstraintMap(getLastURI((String)((JSONObject)jcm.get("constraint")).get("href")), (String)jcm.get("parameter"), vss);
						storagecmids.add(cm);
					}
					
					
				}
				cee.updateConstraints(cmids);
				cee.updateStorageConstraints(storagecmids);
			}
			//default mapping
			if(dms!=null)
			{
				ArrayList<DefaultMap> dmids = new ArrayList<DefaultMap>();
				for(int i = 0; i<dms.size(); i++)
				{
					JSONObject jdm  = (JSONObject)dms.get(i);
					CEE.DefaultMap dm = cee.new DefaultMap((String)jdm.get("type"), ((String)((JSONObject)jdm.get("virtualSystem")).get("href")).substring(1), getLastURI((String)((JSONObject)jdm.get("handler")).get("href")));
					if(jdm.containsKey("crecount"))
                                        dm.cpuCores = (Integer) ((Long)jdm.get("corecount")).intValue();
                                        if(jdm.containsKey("ram"))
					dm.memory = (Integer) ((Long)jdm.get("ram")).intValue();
                                        if(jdm.containsKey("cpuFreq"))
					dm.cpuFreq = (Integer) ((Long) jdm.get("cpuFreq")).intValue();
					dmids.add(dm);
				}
				cee.updateDefaultMap(dmids);
			}
		}else
                        throw new UnauthorizedRestAccessException("cee");
		return cee;
	}

	public static Application appCreate(String requesterUname, String jsonString, String ceeId, String applicationId) throws SQLException, UnauthorizedRestAccessException {
		Logger logger = Logger.getLogger("VEP.CIMIPARSERappCREATE");
                Application app = null;
		boolean authorized = Authenticator.checkResourceAccess(requesterUname, ceeId, "CEE");
		if(authorized)
		{
			if(ceeId == null && applicationId == null)
			{
				app = new Application();
			} else if (ceeId!=null && applicationId != null)
			{
				app = new Application(ceeId, applicationId);
			}
			if(jsonString != null)
			{
				JSONObject json = (JSONObject) JSONValue.parse(jsonString);
				
				String jAppId = (String)json.get("id");
				if(jAppId != null)
				{
					app.setApplicationId(getLastURI(jAppId));
				}
				
				String name = (String)json.get("name");
				app.setName(name);
				
				JSONArray ovfdescriptors = (JSONArray)json.get("OVFDescriptors");
				if(ovfdescriptors != null)
				{
					for(int i=0; i<ovfdescriptors.size();i++)
					{
						JSONObject jovf = (JSONObject)(ovfdescriptors.get(i));
						String ovf = (String) jovf.get("OVFFile");
						app.getOvfs().add(ovf);
					}
				}
				JSONArray reservations = (JSONArray)json.get("reservations");
				if(reservations != null)
				{       
					for(int i=0; i<reservations.size();i++)
					{
						JSONObject jres = (JSONObject)(reservations.get(i));
						Reservation r = app.new Reservation(((String)((JSONObject)jres.get("virtualSystem")).get("href")).substring(1),((Long)jres.get("count")).intValue(),(String)jres.get("end-date"));
						app.getReservations().add(r);
					}
				}
				JSONArray vms = (JSONArray)json.get("VMs");
				if(vms != null)
				{
					for(int i=0; i<vms.size();i++)
					{
						JSONObject jvm = (JSONObject)(vms.get(i));
						VEPVirtualMachine vm = new VEPVirtualMachine(true);
						String vmId = (String)jvm.get("id");
						if(vmId != null)
						{
							vm.setVmId(getLastURI(vmId));
						}
						vm.setName((String)jvm.get("name"));
						vm.setOvfmapname(((String)((JSONObject)jvm.get("virtualSystem")).get("href")).substring(1));
						//context parsing
						JSONObject jContext = (JSONObject)jvm.get("contextualization");
						if(jContext != null)
						{
							for(Map.Entry<String, String> contextKeyVal : (Set<Map.Entry<String, String>>)jContext.entrySet())
							{
								vm.getContext().put(contextKeyVal.getKey(), contextKeyVal.getValue());
							}
						}
						app.getVms().add(vm);
					}
				}
			}
		}else
                        throw new UnauthorizedRestAccessException("cee");
		return app;
	}

	private static String getLastURI(String uri){return uri.replaceFirst(".*/([^/?]+).*", "$1");}
	
	@SuppressWarnings("unchecked")
	public static JSONObject ceeRetrieve(String requesterUname, String ceeId) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(requesterUname, ceeId, "CEE");
		if (authorized) {
			CEE cee = new CEE(ceeId);
			cee.retrieveCEE();
			if (!cee.isError())
			{
				json.put("resourceURI", cee.getResourceUri());
				json.put("id", cee.getId());
				json.put("name", cee.getName());
				json.put("state", cee.getStatus());
				JSONArray ops = new JSONArray();
				JSONObject edit = new JSONObject();
				edit.put("rel", "edit");
				edit.put("href", "/cee/"+ceeId);
				ops.add(edit);
				JSONObject delete = new JSONObject();
				delete.put("rel", "delete");
				delete.put("href", "/cee/"+ceeId);
				ops.add(delete);
				JSONObject extend = new JSONObject();
				extend.put("rel", "/VEP/actions/extend");
				extend.put("href", "/cee/"+ceeId+"/action/extend");
				ops.add(extend);
				JSONObject addApp = new JSONObject();
				addApp.put("rel", "/VEP/actions/addApplication");
				addApp.put("href", "/cee/"+ceeId+"/action/addApplication");
				ops.add(addApp);
				JSONObject stop = new JSONObject();
				stop.put("rel", "/VEP/actions/stop");
				stop.put("href", "/cee/"+ceeId+"/action/stop");
				ops.add(stop);
				//add other actions here
				json.put("operations", ops);
				JSONObject vmH = new JSONObject();
				vmH.put("href", "/cee/"+ceeId+"/VMHandlers");
				json.put("VMHandlers", vmH);
				JSONObject cons = new JSONObject();
				cons.put("href", "/cee/"+ceeId+"/constraints");
				json.put("constraints", cons);
				JSONObject netH = new JSONObject();
				netH.put("href", "/cee/"+ceeId+"/networkHandlers");
				json.put("networkHandlers", netH);
				JSONObject sH = new JSONObject();
				sH.put("href", "/cee/"+ceeId+"/storageHandlers");
				json.put("storageHandlers", sH);
				JSONObject apps = new JSONObject();
				apps.put("href", "/cee/"+ceeId+"/applications");
				json.put("applications", apps);
			} else {
				json.put("error",cee.getError());
			}
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject collectionFill(VEPCollection v)
	{
		JSONObject json = new JSONObject();
		json.put("resourceURI", v.getResourceUri());
		json.put("id", v.getId());
		json.put("count", v.getCount());
		return json;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject ceeCollectionRetrieve(String requesterUname, String uid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(requesterUname, uid, "User");
		if(authorized)
		{
			CEECollection cCol = new CEECollection(uid);
			cCol.retrieveCEECollection();

			json = collectionFill(cCol);
			
			JSONArray cees = new JSONArray();
			for (CEE c : cCol.getCees()) 
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", c.getId());
				cJson.put("name", c.getName());
				cees.add(cJson);
			}
			json.put("CEEs", cees);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject constraintsCollectionRetrieve(String userName,
			String ceeid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			ConstraintCollection c = new ConstraintCollection(ceeid);
			c.retrieveConstraintCollection();
			json = collectionFill(c);
			JSONArray constraints = new JSONArray();
			for (Constraint cn : c.getConstraints())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", cn.getId());
				cJson.put("name", cn.getName());
				constraints.add(cJson);
			}
			json.put("constraints", constraints);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject VMHandlerCollectionRetrieve(String userName,
			String ceeid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			VMHandlerCollection c = new VMHandlerCollection(ceeid);
			c.retrieveVMHandlerCollection();
			json = collectionFill(c);
			JSONArray vmhs = new JSONArray();
			for (VMHandler vmh : c.getVmhs())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", vmh.getId());
				cJson.put("name", vmh.getName());
				vmhs.add(cJson);
			}
			json.put("vmhandlers", vmhs);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject VMHandlerCollectionRetrieve(String userName) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if (authorized) {
			VMHandlerCollection c = new VMHandlerCollection();
			c.retrieveVMHandlerCollection();
			json = collectionFill(c);
			JSONArray vmhs = new JSONArray();
			for (VMHandler vmh : c.getVmhs())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", vmh.getId());
				cJson.put("name", vmh.getName());
				vmhs.add(cJson);
			}
			json.put("vmhandlers", vmhs);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject NetHandlerCollectionRetrieve(String userName,
			String ceeid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			NetHandlerCollection c = new NetHandlerCollection(ceeid);
			c.retrieveNetHandlerCollection();
			json = collectionFill(c);
			JSONArray nhs = new JSONArray();
			for (NetHandler n : c.getNeths())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", n.getId());
				cJson.put("name", n.getName());
				nhs.add(cJson);
			}
			json.put("nethandlers", nhs);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject NetHandlerCollectionRetrieve(String userName) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints"); //will check if user exists and that's it
		if (authorized) {
			NetHandlerCollection c = new NetHandlerCollection();
			c.retrieveNetHandlerCollection();
			json = collectionFill(c);
			JSONArray nhs = new JSONArray();
			for (NetHandler n : c.getNeths())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", n.getId());
				cJson.put("name", n.getName());
				nhs.add(cJson);
			}
			json.put("nethandlers", nhs);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject ApplicationCollectionRetrieve(String userName,
			String ceeid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			ApplicationCollection c = new ApplicationCollection(ceeid);
			c.retrieveAppCollection();
			json = collectionFill(c);
			JSONArray nhs = new JSONArray();
			for (Application n : c.getApps())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", n.getId());
				cJson.put("name", n.getName());
				nhs.add(cJson);
			}
			json.put("applications", nhs);
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject ApplicationRetrieve(String userName,
			String ceeid, String applicationid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			Application c = new Application(ceeid, applicationid);
			authorized = c.checkRelation();
			if(authorized)
			{
				boolean ok = c.retrieveApplication();
				if(ok)
				{
					json.put("resourceURI", c.getResourceUri());
					json.put("id", c.getId());
					json.put("name", c.getName());
					json.put("state", c.getState());


					//show only ovf's href
					JSONArray ovfs = new JSONArray();
					for(Integer oid : c.getOvfDBIDs())
					{
						JSONObject ovf = new JSONObject();
						ovf.put("href", c.getId()+"/ovf/"+oid);
						ovfs.add(ovf);
					}
					json.put("OVFs", ovfs);

					//Add currently registered vms
					JSONArray vms = new JSONArray();
					for(int i=0;i<c.getVms().size();i++)
					{
						VEPVirtualMachine v = c.getVms().get(i);
						JSONObject vm = new JSONObject();
						vm.put("href", v.getId());
						vm.put("state", v.getState());
						vms.add(vm);
					}
					json.put("VMs", vms);

					//Add currently saved reservations
					JSONObject reservations = new JSONObject();
					reservations.put("reservationId", applicationid);//We use the applicationId as the reservationId as there should be only one reservation per application
					JSONArray reservationsArray = new JSONArray();
					for(Reservation r : c.getReservationFromScheduler())
					{
                                            
						JSONObject vs = new JSONObject();
						JSONObject href = new JSONObject();
						href.put("href", "#"+r.ovfmapname);
						vs.put("virtualSystem", href);
						vs.put("count", r.count);
						vs.put("free", r.free);
						reservationsArray.add(vs);
					}
					reservations.put("reservations", reservationsArray);
					json.put("reservation", reservations);

					//ADD OPERATIONS

					//edit,delete
					JSONArray ops = new JSONArray();
					JSONObject edit = new JSONObject();
					edit.put("rel", "edit");
					edit.put("href", "/cee/"+ceeid+"/application/"+applicationid);
					ops.add(edit);
					JSONObject delete = new JSONObject();
					delete.put("rel", "delete");
					delete.put("href", "/cee/"+ceeid+"/application/"+applicationid);
					ops.add(delete);
					//AddVM, Net...
					/*JSONObject addVM = new JSONObject();
					addVM.put("rel", "/VEP/actions/addVM");
					addVM.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/addVM");
					ops.add(addVM);
					JSONObject addNet = new JSONObject();
					addNet.put("rel", "/VEP/actions/addNet");
					addNet.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/addNet");
					ops.add(addNet);
					JSONObject addVolume = new JSONObject();
					addVolume.put("rel", "/VEP/actions/addVolume");
					addVolume.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/addVolume");
					ops.add(addVolume);*/
					//start, stop,...
					JSONObject start = new JSONObject();
					start.put("rel", "/VEP/actions/start");
					start.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/start");
					ops.add(start);
					JSONObject stop = new JSONObject();
					stop.put("rel", "/VEP/actions/stop");
					stop.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/stop");
					ops.add(stop);
					/*JSONObject extend = new JSONObject();
					extend.put("rel", "/VEP/actions/extend");
					extend.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/extend");
					ops.add(extend);
					JSONObject restart = new JSONObject();
					restart.put("rel", "/VEP/actions/restart");
					restart.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/restart");
					ops.add(restart);
					JSONObject pause = new JSONObject();
					pause.put("rel", "/VEP/actions/pause");
					pause.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/pause");
					ops.add(pause);
					JSONObject suspend = new JSONObject();
					suspend.put("rel", "/VEP/actions/suspend");
					suspend.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/action/suspend");
					ops.add(suspend);*/

					//add other actions here
					json.put("operations", ops);
					//ADD COLLECTIONS
					/*JSONObject ovfs = new JSONObject();
					ovfs.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/OVFs");
					json.put("OVFDescriptors", ovfs);
					JSONObject vms = new JSONObject();
					vms.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/VMs");
					json.put("virtualMachines", vms);
					JSONObject vols = new JSONObject();
					vols.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/volumes");
					json.put("volumes", vols);
					JSONObject nets = new JSONObject();
					nets.put("href", "/cee/"+ceeid+"/application/"+applicationid+"/networks");
					json.put("applicationNetworks", nets);*/
				}
			}			
		} 
		if(!authorized) {
			throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	
	@SuppressWarnings("unchecked")
	public static JSONObject retrieveConstraint(String userName,
			String ceeid, String constraintid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if(authorized)
		{
			Constraint c = new Constraint(ceeid, constraintid);
			boolean ok = c.retrieveConstraint();
			if (ok)
			{
				json.put("resourceURI", c.getResourceUri());
				json.put("id", c.getId());
				json.put("type", c.getType());
			}
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}		
		return json;
	}
	@SuppressWarnings("unchecked")
	public static JSONObject VMHandlerRetrieve(String userName, String vid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if(authorized)
		{
			VMHandler v = new VMHandler(vid);
			boolean ok = v.retrieveVMHandler();
			if (ok)
			{
				json.put("resourceURI", v.getResourceUri());
				json.put("id", v.getId());
				json.put("name", v.getName());
				json.put("corecount_low", v.getCorecount_l());
				json.put("corecount_high", v.getCorecount_h());
				json.put("disksize_low", v.getDisksize_l());
				json.put("disksize_high", v.getDisksize_h());
				json.put("cpufreq_low", v.getCpu_l());
				json.put("cpufreq_high", v.getCpu_h());
				json.put("ram_low", v.getRam_l());
				json.put("ram_high", v.getRam_h());
			}
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}		
		return json;
	}
	@SuppressWarnings("unchecked")
	public static JSONObject NetHandlerRetrieve(String userName, String nid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if(authorized)
		{
			NetHandler v = new NetHandler(nid);
			boolean ok = v.retrieveNetHandler();
			if (ok)
			{
				json.put("resourceURI", v.getResourceUri());
				json.put("id", v.getId());
				json.put("name", v.getName());
				json.put("publicIpSupport", v.isPubipsupport());
				json.put("dhcpSupport", v.isDhcpsupport());
			}
		} else {
			
                        throw new UnauthorizedRestAccessException("cee");
		}		
		return json;
	}

	public static JSONObject constraintsCollectionRetrieve(String userName) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if (authorized) {
			StaticConstraintCollection c = new StaticConstraintCollection();
			c.retrieveStaticConstraintCollection();
			json = collectionFill(c);
			JSONArray scs = new JSONArray();
			for (StaticConstraint sc : c.getScs())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", sc.getId());
				cJson.put("name", sc.getName());
				cJson.put("type", sc.getType());
				scs.add(cJson);
			}
			json.put("constraints", scs);
		} else {
			 throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	public static JSONObject constraintRetrieve(String userName, String cid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if(authorized)
		{
			StaticConstraint v = new StaticConstraint(cid);
			boolean ok = v.retrieveStaticConstraint();
			if (ok)
			{
				json.put("resourceURI", v.getResourceUri());
				json.put("id", v.getId());
				json.put("name", v.getName());
			}
		} else {
			 throw new UnauthorizedRestAccessException("cee");
		}		
		return json;
	}


	public static JSONObject parseCheckResponse(Boolean reserve, ArrayList<VEPVirtualMachine> vmList) {
		JSONObject jObj = new JSONObject();
		JSONArray vss = new JSONArray();
		JSONArray reservations = new JSONArray();
		for(VEPVirtualMachine v : vmList)
		{
			JSONObject vs = new JSONObject();
			JSONObject href = new JSONObject();
			href.put("href", "#"+v.getOvfmapname());
			vs.put("virtualSystem", href);
			if(v.countToDeploy >= 1)
				vs.put("deployability", true);
			else
				vs.put("deployability", false);
			vss.add(vs);
			if(v.endDateOfReservation != null)
			{
				//if there is a date there is a reservation
				JSONObject resa = new JSONObject();
				JSONObject rHref = new JSONObject();
				rHref.put("href", "#"+v.getOvfmapname());
				resa.put("virtualSystem", rHref);
				resa.put("count", v.countToDeploy);
				reservations.add(resa);
			}
		}
		jObj.put("virtualSystems", vss);
		if(reserve)
			jObj.put("reservations", reservations);
		
		return jObj;
	}
public static JSONObject parseActiveResponse(Boolean reserve, ArrayList<VEPVirtualMachine> vmList) {
		JSONObject jObj = new JSONObject();
		JSONArray vss = new JSONArray();
		JSONArray reservations = new JSONArray();
		for(VEPVirtualMachine v : vmList)
		{
			JSONObject vs = new JSONObject();
			JSONObject href = new JSONObject();
			/*href.put("href", "#"+v.getOvfmapname());
			vs.put("virtualSystem", href);
			if(v.countToDeploy >= 1)
				vs.put("deployability", true);
			else
				vs.put("deployability", false);
			vss.add(vs);*/
			if(v.endDateOfReservation != null)
			{
				//if there is a date there is a reservation
				JSONObject resa = new JSONObject();
				JSONObject rHref = new JSONObject();
				rHref.put("href", "#"+v.getOvfmapname());
				resa.put("virtualSystem", rHref);
				resa.put("count", v.countToDeploy);
				reservations.add(resa);
			}
		}
		//jObj.put("virtualSystems", vss);
		if(reserve)
			jObj.put("reservations", reservations);
		
		return jObj;
}

	//retrieve ovf as a single string
	public static String OVFRetrieve(String userName, String ceeid, String appid, String ovfid) throws SQLException, UnauthorizedRestAccessException {
		String ovf = "";
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if(authorized)
		{
			OVF v = new OVF(ceeid, appid, ovfid);
			boolean ok = v.retrieveOVF();
			if (ok)
			{
				ovf = v.getOvf();
			}
		}else{
                     throw new UnauthorizedRestAccessException("cee");
                }
		return ovf;
        }

	/** 
	 * applications/{appid}/vms [incomplete]
	 * @param userName
	 * @param ceeid
	 * @param appid
	 * @return
	 * @throws SQLException
	 */
	public static JSONObject VMCollectionRetrieve(String userName,
			String ceeid, String appid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			
		} else {
			 throw new UnauthorizedRestAccessException("vmCollection");
		}
		return json;
	}
	/**
	 * applications/{appid}/vm/{vmid}
	 * @param userName
	 * @param ceeid
	 * @param appid
	 * @param vmid
	 * @return
	 * @throws SQLException
	 */
	public static JSONObject VMRetrieve(String userName, String ceeid,
			String appid, String vmid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			VEPVirtualMachine v = new VEPVirtualMachine(ceeid, appid, vmid);
			v.retrieveVirtualMachine();
			v.retrieveVmInfo(userName);
			if(!v.isError())
			{
				json.put("resourceURI", v.getResourceUri());
				json.put("id", v.getId());
				json.put("name", v.getName());
				json.put("state", v.getState());
				JSONObject ovfmap = new JSONObject();
				ovfmap.put("href", "#"+v.getOvfmapname());
				json.put("virtualSystem", ovfmap);
				JSONObject handler = new JSONObject();
				handler.put("href", "/vmhandler/"+v.getCeevmhandlerid());
				json.put("vmhandler", handler);
				if (v.getIp() != null)
					json.put("ip", v.getIp());
					
			}
		} else {
			 throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	public static JSONObject StorageHandlerCollectionRetrieve(String userName,
			String ceeid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			StorageHandlerCollection shs = new StorageHandlerCollection(ceeid);
			shs.retrieveStorageHandlerCollection();
			json = collectionFill(shs);
			JSONArray nhs = new JSONArray();
			for (StorageHandler sh : shs.getShs())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", sh.getId());
				cJson.put("name", sh.getName());
				nhs.add(cJson);
			}
			json.put("storagehandlers", nhs);
		} else {
            throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	public static JSONObject StorageHandlerCollectionRetrieve(String userName) throws UnauthorizedRestAccessException, SQLException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if (authorized) {
			StorageHandlerCollection c = new StorageHandlerCollection();
			c.retrieveStorageHandlerCollection();
			json = collectionFill(c);
			JSONArray shs = new JSONArray();
			for (StorageHandler sh : c.getShs())
			{
				JSONObject cJson = new JSONObject();
				cJson.put("href", sh.getId());
				cJson.put("name", sh.getName());
				shs.add(cJson);
			}
			json.put("storagehandlers", shs);
		} else {
            throw new UnauthorizedRestAccessException("cee");
		}
		return json;
	}

	public static JSONObject StorageHandlerRetrieve(String userName, String sid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, "ProviderHandlersConstraints", "ProviderHandlersConstraints");
		if(authorized)
		{
			StorageHandler sh = new StorageHandler(sid);
			sh.retrieveStorageHandler();
			if (!sh.isError())
			{
				json.put("resourceURI", sh.getResourceUri());
				json.put("id", sh.getId());
				json.put("name", sh.getName());
				json.put("type", sh.getType());
				if(sh.getEndPoint() != null) 
				{
					json.put("endpoint", sh.getEndPoint());
				}
//				json.put("rack", sh.getRack());
			} else {
				json.put("error", sh.getError());
			}
		} else {
			throw new UnauthorizedRestAccessException("cee");
		}		
		return json;
	}

	public static JSONObject VMDelete(String userName, String ceeid,
			String appid, String vmid) throws SQLException, UnauthorizedRestAccessException {
		JSONObject json = new JSONObject();
		boolean authorized = Authenticator.checkResourceAccess(userName, ceeid, "CEE");
		if (authorized) {
			VEPVirtualMachine v = new VEPVirtualMachine(ceeid, appid, vmid);
			v.retrieveVirtualMachine();
			
		}
		return json;
	}

}
