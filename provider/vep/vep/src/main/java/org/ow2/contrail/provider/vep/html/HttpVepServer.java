/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.html;

/**
 *
 * @author fgaudenz
 */
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;

public class HttpVepServer {

    static final String[] mainFiles = {"colorbox.css", "countries.js", "create_cee.js", "edit_cee.js", "jquery.colorbox.js", "jquery.js", "list_cee.js", "navigation.js", "vep.css","newAccount.js"};
     static final String[] bootstrapCSS = {"bootstrap.css"};
    static final String[] bootstrapJS={"bootstrap.js"};
    static final String[] bootstrapIMG={};
    static final String[] images={ "add48.png", "add_main_menu.png", "add.png", "border.png", "controls.png", "delete.png", "edit.png", "header.jpg", "info.png", "list48.png", "list.png", "loading_background.png", "loading.gif", "logout48.png", "logout.png", "LOGOVEP.png"};
    static final String[] context = {"/newAccount.js","/countries.js", "/jquery.js", "/bootstrap/css/bootstrap.css", "/bootstrap/js/bootstrap.js", "/navigation.js", "/edit_cee.js", "/jquery.colorbox.js", "/colorbox.css",
        "/vep.css", "/list_cee.js", "/create_cee.js",
        "/images/add48.png", "/images/add_main_menu.png", "/images/add.png", "/images/border.png", "/images/controls.png", "/images/delete.png", "/images/edit.png", "/images/header.jpg", "/images/info.png", "/images/list48.png", "/images/list.png", "/images/loading_background.png", "/images/loading.gif", "/images/logout48.png", "/images/logout.png", "/images/LOGOVEP.png"};

    public static void main(String[] args) throws Exception {

//      String enable=VEPHelperMethods.getProperty("webuser-interface.enable", null);
//      String httpDir=VEPHelperMethods.getProperty("webuser-interface.path", null);
//      String httpPort=VEPHelperMethods.getProperty("webuser-interface.port", null);
        String httpPort = "8000";
        String httpDir = "/home/fgaudenz/integration/app/asset";
        new HttpVepServer(httpPort, httpDir,false);
    }

    public HttpVepServer(String port, String root,boolean check) throws IOException {
        if(check)
            checkResources();
        HttpServer server = HttpServer.create(new InetSocketAddress(Integer.parseInt(port)), 0);
        for (String url : context) {
            //System.out.println(url+" "+root);
            server.createContext(url, new MyHandler(root + url));
        }
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    private void checkResources() throws FileNotFoundException, IOException {
        String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator")
                + ".vep" + System.getProperty("file.separator") + "webuserInterface" + System.getProperty("file.separator");

        File dir = new File(defaultPath);
        if ((dir != null) && dir.exists() && dir.isDirectory()) {
            System.out.println("webuserInterface exist");
        } else {

            dir.mkdir();
            System.out.println("webuserInterface folder created");
        }
          dir = new File(defaultPath+"bootstrap"+System.getProperty("file.separator"));
        if ((dir != null) && dir.exists() && dir.isDirectory()) {
           
        } else {

            dir.mkdir();
           
        }
        dir = new File(defaultPath+"bootstrap"+System.getProperty("file.separator")+"css"+System.getProperty("file.separator"));
        if ((dir != null) && dir.exists() && dir.isDirectory()) {
           
        } else {

            dir.mkdir();
           
        }
        dir = new File(defaultPath+"bootstrap"+System.getProperty("file.separator")+"js"+System.getProperty("file.separator"));
         if ((dir != null) && dir.exists() && dir.isDirectory()) {
           
        } else {

            dir.mkdir();
           
        }
         dir = new File(defaultPath+"bootstrap"+System.getProperty("file.separator")+"img"+System.getProperty("file.separator"));
         if ((dir != null) && dir.exists() && dir.isDirectory()) {
           
        } else {

            dir.mkdir();
           
        }
        dir = new File(defaultPath+System.getProperty("file.separator")+"images"+System.getProperty("file.separator"));
         if ((dir != null) && dir.exists() && dir.isDirectory()) {
           
        } else {

            dir.mkdir();
           
        }
        
        
        
        
        
        
        
        
        
        
        
        for (int i = 0; i < mainFiles.length; i++) {
            String file = mainFiles[i];
            File f = new File(defaultPath + file);
           
            InputStream myInputStream = this.getClass().getClassLoader().getResourceAsStream(file);


            
            OutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = myInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
           
            myInputStream.close();
               System.out.println(f.getAbsoluteFile()+ "      created"); 
        }
        
        
      
        for (int i = 0; i < bootstrapCSS.length; i++) {
            String file = bootstrapCSS[i];
            File f = new File(defaultPath+"bootstrap"+System.getProperty("file.separator")+"css"+System.getProperty("file.separator")+file);
          
            InputStream myInputStream = this.getClass().getClassLoader().getResourceAsStream(file);
            OutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = myInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            myInputStream.close();
            System.out.println(f.getAbsoluteFile()+ "      created");
        }
        
        
        
        
        
        
        
        
        
        for (int i = 0; i < bootstrapIMG.length; i++) {
            String file = bootstrapIMG[i];
            
             File f = new File(defaultPath + "file");
            
            
            
            InputStream myInputStream = this.getClass().getClassLoader().getResourceAsStream( file);


           
            OutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = myInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            myInputStream.close();
            System.out.println(f.getAbsoluteFile()+ "      created");
        }
        
        for (int i = 0; i < bootstrapJS.length; i++) {
            String file = bootstrapJS[i];
            InputStream myInputStream = this.getClass().getClassLoader().getResourceAsStream( file);


            File f = new File(defaultPath+"bootstrap"+System.getProperty("file.separator")+"js"+System.getProperty("file.separator")+file);
            OutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = myInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            myInputStream.close();
            System.out.println(f.getAbsoluteFile()+ "      created");
        }
        
        
        
        
        
        
        
        for (int i = 0; i < images.length; i++) {
            String file = images[i];
            InputStream myInputStream = this.getClass().getClassLoader().getResourceAsStream( file);
            File f = new File(defaultPath+"images"+System.getProperty("file.separator")+file);
            //System.out.println(defaultPath+"images"+System.getProperty("file.separator")+file);
            OutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = myInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            myInputStream.close();
            System.out.println(f.getAbsoluteFile()+ "      created");
        }
        
        
        
        
        
        
    }

    private class MyHandler implements HttpHandler {

        private String file;

        public MyHandler(String file) {
            this.file = file;
        }

        public void handle(HttpExchange t) throws IOException {
            //String response  = FileUtils.readFileToString(new File(file));


            String[] app = file.split("./");
            String mime = app[app.length - 1];
            if ((mime.equalsIgnoreCase("css")) || (mime.equalsIgnoreCase("js"))) {
                String response = FileUtils.readFileToString(new File(file));
                t.sendResponseHeaders(200, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } else {
                t.sendResponseHeaders(200, 0);


                OutputStream os = t.getResponseBody();
                FileInputStream fs = new FileInputStream(file);
                final byte[] buffer = new byte[0x10000];
                int count = 0;
                while ((count = fs.read(buffer)) >= 0) {
                    os.write(buffer, 0, count);
                }
                fs.close();
                os.close();
            }
            /*
             OutputStream os = t.getResponseBody();
             os.write(response.getBytes());
             os.close();*/
        }
    }
}
