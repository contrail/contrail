/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.SchedulerClient;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class SchedulerSync {

    static public void syncScheduler() {
        DBHandler db;
        ResultSet rs;
        Logger logger = Logger.getLogger("VEP.SchedulerSyng");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("SchedulerSync", dbType);
        System.out.println("SYNC WITH SCHEDULER");
        /*rs=db.query("select", "*", "cloudtype", "");
        try {
        while (rs.next()) {
              SchedulerClient.addCloud(rs.getString("id"),rs.getString("name"),rs.getString("version"),rs.getString("typeid"));
            }*/
        
        try {
        
        
        
        
        
        
        rs = db.query("select", "datacenter.name as dname,datacenter.id as did,countrylist.code as ccode ", "datacenter,countrylist", "where datacenter.countryid=countrylist.id");
        ArrayList<String> datacenters = new ArrayList<String>();
        
            while (rs.next()) {
                String name = rs.getString("dname");
                String id = rs.getString("did");
                String countryid = rs.getString("ccode");
                SchedulerClient.addDatacenter(id, name, countryid);
                datacenters.add(id);
            }
            for (int i = 0; i < datacenters.size(); i++) {
                ArrayList<String> clusters = new ArrayList<String>();
                rs = db.query("select", "*", "cluster", "where datacenterid="+datacenters.get(i));
                while (rs.next()) {
                    String name = rs.getString("name");
                    String id = rs.getString("id");
                    SchedulerClient.addCluster(id, name, datacenters.get(i));
                    clusters.add(id);
                }
                for(int j=0;j<clusters.size();j++){
                    ArrayList<String> racks = new ArrayList<String>();
                    rs = db.query("select", "*", "rack", "where clusterid="+clusters.get(j));
                    while (rs.next()) {
                    String name = rs.getString("name");
                    String id = rs.getString("id");
                    SchedulerClient.addRack(id, name, datacenters.get(i), clusters.get(j));
                    racks.add(id);
                    }
                    for(int k=0;k<racks.size();k++){ 
                      
                    rs = db.query("select", "*", "host", "where rackid="+racks.get(k));
                    while (rs.next()) {
                    String id = rs.getString("id");
                    String cpuFreq = rs.getString("cpufreq");
                    String coreCount=rs.getString("corecount");
                    /*rs.getString("ram");
                    rs.getString("disksize");
                    rs.getString("cpuArch");
                    rs.getString("");*/
                    SchedulerClient.addHost(id, rs.getString("hostname"), cpuFreq, coreCount, rs.getString("ram"), rs.getString("disksize"),  rs.getString("cpuArch"), rs.getString("iaasid"), datacenters.get(i), clusters.get(j), racks.get(k));
                    //racks.add(id);
                    System.out.println("Syncronization DONE");
                   }
                }
            }
          }
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
        }

    }
}
