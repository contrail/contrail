/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.fixImage2_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class ImageFIXClient
{
 
   private Socket connection;     
   private BufferedReader fromServer;
   PrintStream toServer;
   Logger logger;
public ImageFIXClient(String addressIP,String port)
 {
            try {
                // Apre una connessione verso un server in ascolto
                // sulla porta 7777. In questo caso utilizziamo localhost
                // che corrisponde all’indirizzo IP 127.0.0.1
                logger = Logger.getLogger("VEP.RestUserDoAction");

                // Ricava lo stream di input dal socket s1
                // ed utilizza un oggetto wrapper di classe BufferedReader
                // per semplificare le operazioni di lettura
                        
                connection = new Socket(addressIP, Integer.parseInt(port));      
                fromServer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                toServer = new PrintStream(connection.getOutputStream());
            } catch (IOException ex) {
                logger.info("Exception"+ex);
            }
   
   
 }

        
 
public void closeClient(){
            try {
                fromServer.close();
                toServer.close();
                connection.close();
            } catch (IOException ex) {
                
            }
 }
 
 public String downloadImage(String msg, String user) throws IOException{
     toServer.println("hello");
     
     String getIN;
     getIN= fromServer.readLine();
     if(getIN.compareTo("hello")==0){
         toServer.println("oneadminContrail");
         getIN= fromServer.readLine();
         if(getIN.compareTo("ok")==0){
             toServer.println("oneadmin");
             getIN= fromServer.readLine();
             if(getIN.compareTo("ok")==0){
                 toServer.println("SRC="+msg+" USR="+user);
                 getIN= fromServer.readLine();
                  String[] app=getIN.split(" ");
                 if(app[0].compareTo("ok")==0){
                        String[] app2=app[1].split("PATH=");
                        return app2[1];
                 }
                 
             }
         }
     }
     return null;
 }
 


        public boolean setImage(String idPathImage) throws IOException {
          toServer.println("DST="+idPathImage);
          String getIN= fromServer.readLine();
          if(getIN.compareTo("ok")==0){
              getIN= fromServer.readLine();
              if(getIN.compareTo("quit")==0){
                  this.closeClient();
                  return true;
              }
              
          }
          return false;
        }
    }
