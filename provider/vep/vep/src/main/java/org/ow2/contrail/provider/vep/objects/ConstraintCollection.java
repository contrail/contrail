/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ConstraintCollection extends VEPCollection {

	private String ceeId;
	private ArrayList<Constraint> constraints = new ArrayList<Constraint>();
	
	public ConstraintCollection(String ceeId)
	{
		super("ConstraintCollection", true);
		this.ceeId = ceeId;
	}
	
	public void retrieveConstraintCollection() throws SQLException
	{
		ResultSet rs;
		logger.debug("Retrieving Constraints for cee " + ceeId);
		ArrayList<Integer> constraintsCEE = new ArrayList<Integer>();
		rs = db.query("select", "id", "ceeconstraints", "where ceeid = " + ceeId);
		while (rs.next()){
			constraintsCEE.add(rs.getInt("id"));
		}
		//avoid nested rs
		for(int ceeconstraintsid : constraintsCEE)
		{
			rs = db.query("select", "id", "ceeconstraintslist", "where ceeconstraintsid = " + ceeconstraintsid);
			while (rs.next()){
				Constraint c = new Constraint(false);
				c.setName("N/A");
				c.setId("/cee/" + ceeId + "/constraint/" + rs.getInt("id"));
				constraints.add(c);
				this.incrementCount();
			}
		}
	}

	public ArrayList<Constraint> getConstraints() {
		return constraints;
	}
	
}
