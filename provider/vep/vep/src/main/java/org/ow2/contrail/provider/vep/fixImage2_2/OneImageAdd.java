/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.fixImage2_2;

import java.util.LinkedList;
import javax.swing.JLabel;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.JLabel;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.Pool;
import org.opennebula.client.cluster.Cluster;
import org.opennebula.client.cluster.ClusterPool;
import org.opennebula.client.host.Host;
import org.opennebula.client.image.Image;
import org.opennebula.client.image.ImagePool;
import org.opennebula.client.user.User;
import org.opennebula.client.vm.VirtualMachine;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.ow2.contrail.provider.vep.One2XMLRPCHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.ow2.contrail.provider.vep.DBHandler;

/**
 *
 * @author fgaudenz
 */
public class OneImageAdd {
        
    private String oneIP;
    private String onePort;
    private String oneUser;
    private String onePass;
    private Client oneClient;
    private String tempVal;
    
    private String hostId;
    private JLabel output;
    private int actionType;
  
    private Logger logger;
    private boolean imageParse;
    private boolean vmParse;
    private boolean graphicsData;
    private boolean clusterParse;
    private boolean singleHostInfo;
   
    public static void main(String[] args) throws Exception {
        String oneIP = "127.0.0.1";
        String onePort = "2633";
       String  oneUser = "oneadmin";
      String   onePass = "onecloud";
          OneImageAdd  cl= new OneImageAdd(oneIP, onePort, oneUser, onePass);
          
          String app=cl.addImage("NAME=\"userProva@suseVM-NEW\" \nPATH=/home/fgaudenz/tempVEP//userProva_ubuServerpure.img \nPUBLIC=YES");
          //System.out.println(cl.getSource(app));
    }
   
    public OneImageAdd (String oneIP,String onePort, String oneUser,String onePass){
        
       
        
        
        
        this.oneIP = oneIP;
        this.onePort = onePort;
        this.oneUser = oneUser;
        this.onePass = onePass ;
        tempVal = "";
        hostId = "";
        actionType = -1;
        output = null;
        imageParse = false;
        vmParse = false;
        graphicsData = false;
        clusterParse = false;
        logger = Logger.getLogger("VEP.ONEADDIMAGE");
        try
        {
            logger.debug("Creating one2lient: new Client(" + oneUser + ":" + onePass + ", \"http://" + oneIP + ":" + onePort + "/RPC2\")");
            oneClient  = new Client(oneUser + ":" + onePass, "http://" + oneIP + ":" + onePort + "/RPC2");
            
            //System.out.println(oneClient);
        }
        catch(Exception ex)
        {
            logger.error("OpenNebula3 XML RPC connection could not be established. Check system settings and restart the application.");
            oneClient = null;
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    public String addImage(String tmp) throws ParserConfigurationException, IOException, SAXException
    {
        int value = -1;
        if(oneClient != null)
        {
            String template=tmp;
            
            logger.debug("Received this image template:\n" + template);
            OneResponse val = Image.allocate(oneClient, template);
            
            logger.debug("RegiRestering image with ONE - got response: " + val.getMessage());
           
            if(!val.isError()){
                value = Integer.parseInt(val.getMessage());
                Image.enable(oneClient,value , true);
            }else
            {
                
                logger.warn("Error submitting Image to ONE: " + val.getErrorMessage());
                One2XMLRPCHandler onehandle = new One2XMLRPCHandler(oneIP, onePort, oneUser, onePass, "oneImageAdd:addImages()");
                LinkedList<ONEImage> imageList = null;
                if(onehandle != null)
                         imageList = onehandle.getImageList();
               
                String[] appImg=tmp.split("NAME=\"");
                 
                 String imgName=(appImg[1].split("\""))[0];
                
                 for(int i=0; imageList != null && i< imageList.size(); i++){
                                            ONEImage img = imageList.get(i);
                                            logger.debug("Got ONE Image Details: " + img.imageName + ", " + img.localPath + ", " + img.state);
                                            if(img.imageName.equalsIgnoreCase(imgName))
                                                    value=img.id;
                                         }
                 
            }
            
        }
        return getSource(value);
    }

    private String getSource(int id) throws ParserConfigurationException, IOException, SAXException {
        int value = -1;
        if(oneClient != null)
        {  
            OneResponse val = Image.info(oneClient, id);
          
            logger.debug("Registering image with ONE - got response: " + val.getMessage());
            if(!val.isError()){
                 
                 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                 DocumentBuilder db = dbf.newDocumentBuilder();
                 InputSource is = new InputSource();
                 is.setCharacterStream(new StringReader(val.getMessage()));
                 Document doc = db.parse(is);
                
                 return gettingText(doc);

                
            }else
            {
               
                logger.warn("Error submitting Image to ONE: " + val.getErrorMessage());
            }
            
        }
        return null;
    }
    
    
    
    
     public String gettingText(Document doc) {
       Element e = doc.getDocumentElement();
        NodeList nodeList = doc.getElementsByTagName("IMAGE");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                NodeList nodelist = element.getElementsByTagName("SOURCE");
                Element element1 = (Element) nodelist.item(0);
                NodeList fstNm = element1.getChildNodes();
                return fstNm.item(0).getNodeValue();
               }
        }
        return null;
     }
     

    
}
