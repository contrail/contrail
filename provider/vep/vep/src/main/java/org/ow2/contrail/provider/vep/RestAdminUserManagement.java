/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.apache.commons.fileupload.*;
import org.apache.commons.io.IOUtils;

public class RestAdminUserManagement extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	
	public RestAdminUserManagement()
	{
		logger = Logger.getLogger("VEP.RestAdminUserManagement");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestAdminUserAccount", dbType);
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
		Form form = null;
		boolean proceed = false;
		boolean certStored = false;
		boolean roleUpdated = false;
		String actionType = "";
		String username = "";
		String password = "";
		String message = "";
		String action = ((String) getRequest().getAttributes().get("action"));
		if(MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true) && action.equalsIgnoreCase("addcert")) 
		{
			int userId = -1;
			actionType = "addcert";
			certStored = false;
			roleUpdated = false;
			DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1000240);
            RestletFileUpload upload = new RestletFileUpload(factory);
            List<FileItem> items;
            try
            {
            	items = upload.parseRequest(getRequest());
            	for(final Iterator<FileItem> it = items.iterator(); it.hasNext() && !certStored;) 
            	{
                    FileItem fi = it.next();
                    if(fi.isFormField()) 
                    {
                        if (fi.getFieldName().equals("userid"))
                        {
                        	userId = Integer.parseInt(fi.getString());
                        }
                        if (fi.getFieldName().equals("username"))
                        {
                        	username =fi.getString().trim();
                        }
                        if (fi.getFieldName().equals("password"))
                        {
                        	password = fi.getString().trim();
                        	try
                	        {
                		        if(!VEPHelperMethods.testDBconsistency())
                		        {
                		        	//only grant temporary access for the predetermined user
                		        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
                		        	{
                		        		proceed = true;
                		        	}
                		        }
                		        else
                		        {
                		        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                		        	if(rs.next())
                		        	{
                		        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")) && rs.getString("role").equalsIgnoreCase("administrator"))
                		        			proceed = true;
                		        	}
                		        }
                	        }
                	        catch(Exception ex)
                	        {
                	        	logger.warn("Admin authentication resulted in exception.");
                	        	//if(logger.isDebugEnabled())
                	        	//	ex.printStackTrace(System.err);
                	        	logger.debug("Exception Caught: ", ex);
                	        	proceed = false;
                	        }
                        }
                    }
                    if(fi.getFieldName().equals("certfile") && userId != -1 && proceed)
                    {
                    	logger.info("AddCert: trying to store certificate into table for userid = " + userId);
                    	try
                    	{
                    		InputStream instream = fi.getInputStream();
                    		if(instream.available() > 0)
                    		{
	                    		PreparedStatement pstmt = DBHandler.dbHandle.prepareStatement("UPDATE user SET certificate = ? WHERE id = " + userId);
	                    		byte[] temp = IOUtils.toByteArray(instream);
	                    		pstmt.setBytes(1, temp);
	                    		pstmt.execute();
	                    		certStored = true;
                    		}
                    	}
                    	catch(Exception ex)
                    	{
                    		certStored = false;
                    		logger.warn("AddCert Routine: Certificate storage resulted in exception.");
                    		//if(logger.isDebugEnabled())
                    		//	ex.printStackTrace(System.err);
                    		logger.debug("Exception Caught: ", ex);
                    	}
                    }
                }
            	if(userId == -1 || !certStored || !proceed)
            		message += "Improper data provided. Check user selection and certificate file choice. ";
            }
            catch(Exception ex)
            {
            	//if(logger.isDebugEnabled())
            	//	ex.printStackTrace(System.err);
            	logger.debug("Exception Caught: ", ex);
            }
            proceed = true;
		}
		else if(MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true) && action.equalsIgnoreCase("updateuser")) 
		{
			int userId = -1;
			String role = "";
			actionType = "updateuser";
			certStored = false;
			roleUpdated = false;
			DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1000240);
            RestletFileUpload upload = new RestletFileUpload(factory);
            List<FileItem> items;
            try
            {
            	items = upload.parseRequest(getRequest());
            	for(final Iterator<FileItem> it = items.iterator(); it.hasNext();) 
            	{
                    FileItem fi = it.next();
                    if(fi.isFormField()) 
                    {
                        if (fi.getFieldName().equals("userid"))
                        {
                        	userId = Integer.parseInt(fi.getString());
                        }
                        if (fi.getFieldName().equals("username"))
                        {
                        	username =fi.getString().trim();
                        }
                        if (fi.getFieldName().equals("password"))
                        {
                        	password = fi.getString().trim();
                        	try
                	        {
                		        if(!VEPHelperMethods.testDBconsistency())
                		        {
                		        	//only grant temporary access for the predetermined user
                		        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
                		        	{
                		        		proceed = true;
                		        	}
                		        }
                		        else
                		        {
                		        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                		        	if(rs.next())
                		        	{
                		        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")) && rs.getString("role").equalsIgnoreCase("administrator"))
                		        			proceed = true;
                		        	}
                		        }
                	        }
                	        catch(Exception ex)
                	        {
                	        	logger.warn("Admin authentication resulted in exception.");
                	        	//if(logger.isDebugEnabled())
                	        	//	ex.printStackTrace(System.err);
                	        	logger.debug("Exception Caught: ", ex);
                	        	proceed = false;
                	        }
                        }
                        if (fi.getFieldName().equals("role"))
                        {
                        	role = fi.getString().trim();
                        }
                    }
                    if(fi.getFieldName().equals("certfile") && userId != -1 && proceed)
                    {
                    	logger.info("UpdateUser: trying to update certificate into table for userid = " + userId);
                    	try
                    	{
                    		InputStream instream = fi.getInputStream();
                    		if(instream.available() > 0)
                    		{
	                    		PreparedStatement pstmt = DBHandler.dbHandle.prepareStatement("UPDATE user SET certificate = ? WHERE id = " + userId);
	                    		byte[] temp = IOUtils.toByteArray(instream);
	                    		pstmt.setBytes(1, temp);
	                    		pstmt.execute();
	                    		certStored = true;
                    		}
                    	}
                    	catch(Exception ex)
                    	{
                    		certStored = false;
                    		logger.warn("UpdateUser Routine: Certificate storage resulted in exception.");
                    		//if(logger.isDebugEnabled())
                    		//	ex.printStackTrace(System.err);
                    		logger.debug("Exception Caught: ", ex);
                    	}
                    }
                    if(fi.getFieldName().equals("role") && userId != -1 && proceed)
                    {
                    	logger.info("UpdateUser: trying to update role into table for userid = " + userId);
                    	try
                    	{
                    		if(role.length() > 0)
                    		{
                    			PreparedStatement pstmt = DBHandler.dbHandle.prepareStatement("UPDATE user SET role = ? WHERE id = " + userId);
                    			pstmt.setString(1, role);
                    			pstmt.execute();
                    			roleUpdated = true;
                    		}
                    	}
                    	catch(Exception ex)
                    	{
                    		logger.warn("AddCert Routine: Role update process resulted in exception.");
                    		roleUpdated = false;
                    		//if(logger.isDebugEnabled())
                    		//	ex.printStackTrace(System.err);
                    		logger.debug("Exception Caught: ", ex);
                    	}
                    }
                }
            	if(userId == -1 || (!certStored && !roleUpdated))
            		message += "Improper data provided. Check user selection and certificate file or role value. ";
            }
            catch(Exception ex)
            {
            	//if(logger.isDebugEnabled())
            	//	ex.printStackTrace(System.err);
            	logger.debug("Exception Caught: ", ex);
            }
		}
		else
		{
			form = new Form(entity);
			Map<String, String> values = form.getValuesMap();
			Set<String> keys = values.keySet();
			Collection<String> keyval = values.values();
			Iterator<String> keyIter = keys.iterator();
			Iterator<String> valIter = keyval.iterator();
			while(keyIter.hasNext() && valIter.hasNext())
			{
				String key = keyIter.next();
				String value = valIter.next();
				if(key.equalsIgnoreCase("username")) username = value;
				if(key.equalsIgnoreCase("password")) password = value;
			}
	        try
	        {
		        if(!VEPHelperMethods.testDBconsistency())
		        {
		        	//only grant temporary access for the predetermined user
		        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
		        	{
		        		proceed = true;
		        	}
		        }
		        else
		        {
		        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
		        	if(rs.next())
		        	{
		        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")) && rs.getString("role").equalsIgnoreCase("administrator"))
		        			proceed = true;
		        	}
		        }
	        }
	        catch(Exception ex)
	        {
	        	logger.warn("Admin authentication resulted in exception.");
	        	//if(logger.isDebugEnabled())
	        	//	ex.printStackTrace(System.err);
	        	logger.debug("Exception Caught: ", ex);
	        	proceed = false;
	        }
		}
        
        if(proceed)
        {
        	stringBuilder.append("<b>Welcome to VEP Service Management Page</b><br>");
        	stringBuilder.append(username).append(", you can manage individual user's account in this page!<hr>");
        	stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana;font-size:9pt;'>");
        	if(action == null)
        	{
        		
                    HashMap<Integer,String> users=new HashMap<Integer,String>();
                        try
        		{
        			ResultSet rs = db.query("select", "*", "user", "WHERE certificate IS '' OR certificate IS null");
                                
        			while(rs.next())
					{
						users.put(rs.getInt("id"),rs.getString("username"));
					}
        		}
        		catch(Exception ex)
        		{
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
                    /////////////displaying user accounts with pending requests
        		stringBuilder.append("For any newly created user account, you can generate a user certificate and upload to the server. Please select from the drop down list of newly created ")
        			.append("account that you wish to act on:<br><br>");
        		stringBuilder.append("<form  enctype='multipart/form-data' name='editnewuser' action='../admin/usermanagement/addcert' method='post' style='font-family:Verdana;font-size:9pt;color:white;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;background:white;color:black;border-style:dotted;border-color:green;border-width:1px;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td>Select the user account<td align='left'><select name='userid'><option value='-1'>Not selected</option>");
        		
                         for(Map.Entry<Integer,String> entry:users.entrySet())
                               
					{
						stringBuilder.append("<option value='").append(entry.getKey()).append("'>").append(entry.getValue()).append("</option>");
					}
        		
                        
                        
        		stringBuilder.append("</select><td>");
        		stringBuilder.append("<tr><td>Certificate Path (.pfx)<td align='right'><input type='file' name='certfile' size='40'>");
        		stringBuilder.append("<td align='right'><input type='submit' value='add certificate'>");
		        stringBuilder.append("</table></form><br>");
		        stringBuilder.append("You can change roles and update user certificate for existing users below:<br><br>");
		        stringBuilder.append("<form  enctype='multipart/form-data' name='updateuser' action='../admin/usermanagement/updateuser' method='post' style='font-family:Verdana;font-size:9pt;color:white;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;background:white;color:black;border-style:dotted;border-color:brown;border-width:1px;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td>Select the user account<td align='left'><select name='userid'><option value='-1'>Not selected</option>");
        		users=new HashMap<Integer,String>();
                        try
        		{
        			ResultSet rs = db.query("select", "*", "user", "");
                                
        			while(rs.next())
					{
						users.put(rs.getInt("id"),rs.getString("username"));
					}
        		}
        		catch(Exception ex)
        		{
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        			
        			  for(Map.Entry<Integer,String> entry:users.entrySet())
                               
					{
						stringBuilder.append("<option value='").append(entry.getKey()).append("'>").append(entry.getValue()).append("</option>");
					}
        	
        		
        		stringBuilder.append("</select>");
        		stringBuilder.append("<tr><td>Certificate Path (.pfx)<td align='left'><input type='file' name='certfile' size='39'>");
        		stringBuilder.append("<tr><td>Role (leave empty to keep the existing role)<td align='left'><input type='text' name='role' size='39'>(administrator or user)");
        		stringBuilder.append("<tr><td><td align='left'><input type='submit' value='update account'>");
		        stringBuilder.append("</table></form><br>");
		        
				////////////////////////////////////////////////////
				stringBuilder.append("<td valign='top'>");
				stringBuilder.append("<form name='goback' action='../admin/dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
				stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
				stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td><td align='right'><input type='submit' value='cancel and go back'>");
				stringBuilder.append("</table></form>");
        	}
        	else
        	{
        		if(actionType.equalsIgnoreCase("addcert"))
        		{
        			if(!certStored)
    	        	{
    	        		stringBuilder.append("<br><br><font color='red'>").append(message).append("</font><br><br>");
    	        		stringBuilder.append("<form name='goback' action='../usermanagement' method='post'>")
    	        			.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='go back'></form>");
    	        	}
    	        	else
    	        	{
    	        		stringBuilder.append("<font color='green'>The user certificate was uploaded successfully!</font><br><br>");
    		        	stringBuilder.append("<form name='proceed' action='../usermanagement' method='post'>")
    						.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='proceed'></form>");
    	        	}
        		}
        		else if(actionType.equalsIgnoreCase("updateuser"))
        		{
        			if(!certStored && !roleUpdated)
    	        	{
        				message += "User role and the certificate were left unchanged. ";
    	        		stringBuilder.append("<br><br><font color='red'>").append(message).append("</font><br><br>");
    	        		stringBuilder.append("<form name='goback' action='../usermanagement' method='post'>")
    	        			.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='go back'></form>");
    	        	}
    	        	else if(!certStored && roleUpdated)
    	        	{
    	        		stringBuilder.append("<font color='green'>The user certificate was left unchanged and the role was updated successfully!</font><br><br>");
    		        	stringBuilder.append("<form name='proceed' action='../usermanagement' method='post'>")
    						.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='proceed'></form>");
    	        	}
    	        	else if(certStored && !roleUpdated)
    	        	{
    	        		stringBuilder.append("<font color='green'>The user certificate was updated and the role was left unchanged!</font><br><br>");
    		        	stringBuilder.append("<form name='proceed' action='../usermanagement' method='post'>")
    						.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='proceed'></form>");
    	        	}
    	        	else if(certStored && roleUpdated)
    	        	{
    	        		stringBuilder.append("<font color='green'>The user certificate and the role were updated successfully!</font><br><br>");
    		        	stringBuilder.append("<form name='proceed' action='../usermanagement' method='post'>")
    						.append("<input type='hidden' name='username' value='").append(username).append("'>")
    						.append("<input type='hidden' name='password' value='").append(password).append("'>")
    						.append("<input type='submit' value='proceed'></form>");
    	        	}
        		}
        	}
			stringBuilder.append("</table>");
        }
        else
        {
    		stringBuilder.append("<br><br><font color='red'>Unauthorized access, login and try again,</font><br><br>");
    		stringBuilder.append("<form name='goback' action='../admin/' method='get'>")
				.append("<input type='submit' value='go back'></form>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
