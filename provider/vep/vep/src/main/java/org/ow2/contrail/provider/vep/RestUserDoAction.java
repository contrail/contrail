/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.ow2.contrail.provider.vep.fixImage2_2.ImageFIXClient;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser;

public class RestUserDoAction extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	private SSLCertHandler certHandler;
	
    private String webHost, webPort;
    
	public RestUserDoAction()
	{
		logger = Logger.getLogger("VEP.RestUserDoAction");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        webHost = VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger);
        webPort = VEPHelperMethods.getProperty("webuser-interface.port", logger);
        db = new DBHandler("RestUserDoAction", dbType);
        certHandler = new SSLCertHandler(true); //bypasses the creation of CA service objects
	}
	
	@Post
	public Representation showConsole(Representation entity) throws ResourceException
	{
		Form form = new Form(entity);
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String username = "";
		String password = "";
		String requesttype = "";
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			if(key.equalsIgnoreCase("username")) username = value;
			if(key.equalsIgnoreCase("password")) password = value;
			if(key.equalsIgnoreCase("requesttype")) requesttype = value;
		}
		
		//first verify whether the username and password are correct, then proceed for certificate verification
		boolean proceed = false;
		int userId = -1;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	proceed = false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        		{
	        			userId = rs.getInt("id");
	        			proceed = true;
	        		}
	        		else
	        			proceed = false;
	        	}
	        	else
	        	{
	        		proceed = false;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception caught ", ex);
        	proceed = false;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	//now verify the certificate information
        	String certUser = "";
        	@SuppressWarnings("unchecked")
			List<X509Certificate> certs = (List<X509Certificate>)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
            for(int i=0; certs != null && i < certs.size() && proceed; i++)
            {
                X509Certificate Cert = certs.get(i);
                proceed = SSLCertHandler.isCertValid(Cert);
                certUser = SSLCertHandler.getCertDetails(Cert, "CN");
                logger.debug("Certificate User name: " + certUser + " Validity [" + proceed + "]");
            }
            
            if(certUser.equalsIgnoreCase(username))
            {
            	String action = ((String) getRequest().getAttributes().get("action"));
            	if(action == null)
            		displayAppConsole(stringBuilder, username, password, requesttype, userId, form);
            	else
            	{
            		doAction(stringBuilder, username, password, requesttype, userId, form, action);
            	}
            }
            else
            {
            	stringBuilder.append("<b>Welcome to VEP Application Management Tool</b><br>").append(username).append(", access to this page <b>has been blocked</b> due to certificate error!<hr>");
    	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
    	        
    	        stringBuilder.append("<font color='red'>Invalid Certificate! Please re-install your certificate and try again. You will be forced to logout. Please re-login to download your user certificate.</font><br><br>");
    	        
    	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
    	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
    	        stringBuilder.append("<tr><td align='left'><input type='submit' value='logout'>");
    	        stringBuilder.append("</table></form>");
    	        stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;background:#FFD1B7;;padding:5px;'>It is advised to close your browser after you logout in order to clear the old certificate selection from the browser cache.</p>");
    	        stringBuilder.append("</table>");
            }
        }
        else
        {
        	stringBuilder.append("<b>Welcome to VEP Application Management Tool</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
		return value;
	}
	
	private int[] findBestVMHandlerMatch(LinkedList<vmHandlerElement> handlerMatrix, String ovfDesc)
	{
		int[] bestIndex = null;
		try
		{
			OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovfDesc, "2.2.1", "oneadmin");
			String[] ovfIds = parser.getIDs();
			bestIndex = new int[ovfIds.length];
			for(int i=0; i<bestIndex.length; i++) bestIndex[i] = -1;
			//now processing for each Virtual System
			for(int i=0; i<ovfIds.length; i++)
			{
				logger.info("Now processing the Virtual System: " + ovfIds[i]);
				HashMap<String, Integer> temp = parser.getVSSpecs(ovfIds[i]);
				int cpuFreq = -1; //ignore this parameter for the moment
				int diskSize = -1; //ignore this parameter for the moment
				int ram = temp.get("ram");
				int cpuCores = temp.get("rcores");
				int score = 0;
				for(int j=0; j < handlerMatrix.size(); j++)
				{
					if(cpuFreq == -1) score += 1*100;
					else
					{
						
					}

					if(ram <= handlerMatrix.get(j).ram_high && ram >= handlerMatrix.get(j).ram_low) score += 1*100;
					else
					{
						if(ram < handlerMatrix.get(j).ram_low) score += -1 * (int)(((float)(handlerMatrix.get(j).ram_low - ram) / (float)ram) * 100.0);
						else
							score += -1 * (int)(((float)(ram - handlerMatrix.get(j).ram_high) / (float)ram) * 100.0);
					}
					
					if(cpuCores <= handlerMatrix.get(j).corecount_high && cpuCores >= handlerMatrix.get(j).corecount_low) score += 1*100;
					else
					{
						if(cpuCores < handlerMatrix.get(j).corecount_low) score += -1 * (int)(((float)(handlerMatrix.get(j).corecount_low - cpuCores) / (float)cpuCores) * 100.0);
						else
							score += -1 * (int)(((float)(cpuCores - handlerMatrix.get(j).corecount_high) / (float)cpuCores) * 100.0);
					}
					
					if(diskSize == -1) score += 1*100;
					else
					{
						
					}
					
					handlerMatrix.get(j).score = score;
					logger.info("Calculated Score for VirtualSystem " + ovfIds[i] + " for VMHandler Id: " + handlerMatrix.get(j).ceeVMHandlerId + " [Score=" + handlerMatrix.get(j).score + "]");
				}
				//now for this OVF Virtual System find the best score
				int bIndex = -1;
				for(int j=0; j<handlerMatrix.size(); j++)
				{
					if(handlerMatrix.get(j).score > 0 && j==0) bIndex = 0;
					else if(handlerMatrix.get(j).score > 0 && bIndex != -1 && handlerMatrix.get(j).score > handlerMatrix.get(bIndex).score) bIndex = j;
					else if(handlerMatrix.get(j).score > 0 && bIndex == -1) bIndex = j;
				}
				if(bIndex != -1)
					bestIndex[i] = handlerMatrix.get(bIndex).ceeVMHandlerId;
				else
					bestIndex[i] = -1;
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while finding the best match for OVF belonging to CEE.");
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			bestIndex = null;
		}
		return bestIndex;
	}
	
	private int[] findBestVNetHandlerMatch(String ovfId, String ovfDesc, int ceeId, LinkedList<netHandlerElement> handlerMatrix)
	{
		int[] bestIndex = null;
		boolean status = true;
		try
		{
			OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovfDesc, "2.2.1", "oneadmin");
			ArrayList<HashMap> temp = parser.getNetworks(ovfId);
			//computing the size of bestIndex array
			int indexSize = 0;
			for(int i=0; i<temp.size() && status; i++)
			{
				HashMap<String, Object> netVal = temp.get(i);
				ArrayList<String> nics = (ArrayList<String>)netVal.get("nics");
				indexSize += nics.size();
			}
			bestIndex = new int[indexSize];
			int currentIndex = 0;
			for(int i=0; i<temp.size() && status; i++)
			{
				HashMap<String, Object> netVal = temp.get(i);
				//System.err.println("Data received for virtual system: " + ovfId + ": " + netVal.get("name") + ", type=" + netVal.get("type") + "\nNow finding NICs list:");
				String ovfNetName = (String)netVal.get("name");
				String netType = ((String)netVal.get("type")).trim().toLowerCase(); //this will be private or public
				ArrayList<String> nics = (ArrayList<String>)netVal.get("nics");
				for(int j=0; j< nics.size() && status; j++)
				{
					//System.err.println(nics.get(j));
					String ip = nics.get(j).trim().toLowerCase();
					if(ip.equalsIgnoreCase("dhcp") || VEPHelperMethods.isIpAddress(ip))
					{
						int score = 0;
						for(int k=0; k < handlerMatrix.size(); k++)
						{
							if(netType.equalsIgnoreCase("public") && handlerMatrix.get(k).publicIpSupport == 1) score += 1;
							else if(netType.equalsIgnoreCase("private") && handlerMatrix.get(k).publicIpSupport != 1) score += 1;
							else if(ip.equalsIgnoreCase("dhcp") && handlerMatrix.get(k).dhcpSupport == 1) score += 1;
							else if(!netType.equalsIgnoreCase("dhcp") && handlerMatrix.get(k).dhcpSupport != 1) score += 1;
							handlerMatrix.get(k).score = score;
							logger.info("Calculated Score for VirtualSystem " + ovfId + " for netHandler Id: " + handlerMatrix.get(k).ceeNetHandlerId + " [Score=" + handlerMatrix.get(k).score + "]");
						}
						//now finding the best matching vnethandler index for this NIC
						int bIndex = -1;
						for(int k=0; k<handlerMatrix.size(); k++)
						{
							if(handlerMatrix.get(k).score > 0 && k==0) bIndex = 0;
							else if(handlerMatrix.get(k).score > 0 && bIndex != -1 && handlerMatrix.get(k).score > handlerMatrix.get(bIndex).score) bIndex = k;
							else if(handlerMatrix.get(k).score > 0 && bIndex == -1) bIndex = k;
						}
						if(bIndex != -1)
							bestIndex[currentIndex] = handlerMatrix.get(bIndex).ceeNetHandlerId;
						else
							bestIndex[currentIndex] = -1;
						currentIndex++;
					}
					else
					{
						logger.info("Invalid IP address detected for Virtual System " + ovfId + " for CEE " + ceeId);
						status = false;
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while finding the best match for OVF networks belonging to CEE.");
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			bestIndex = null;
		}
		if(!status) bestIndex = null;
		
		return bestIndex;
	}
	
	private int[] findBestStorageHandlerMatch(String ovfId, String ovfDesc, int ceeId)
	{
		int[] bestIndex = null;
		
		return bestIndex;
	}
	
	private boolean update_context_staticip(OpenNebulaOVFParser parser, String ovfId, int ovfmapId)
	{
		boolean status = true;
		
		//in this function you update the CONTEXT and static IP fields of the ovfmap entry
		String context = parser.getContext(ovfId);
		if(context.trim().length() == 0)
			db.updateContext(ovfmapId, "", "ovfmap");
		else
			db.updateContext(ovfmapId, context.trim(), "ovfmap");
		
		//if the static IP field is not present, make sure to change the status back from LCK to RDY state
		
		return status;
	}
	
	private boolean update_vmslot_diskmap(int vmslotId)
	{
		boolean status = true;
		try
		{
			//get ovfmapid from vmslots
			ResultSet rs = db.query("select", "ovfmapid, hostid", "vmslots", "WHERE id=" + vmslotId);
			if(rs.next())
			{
				int ovfmapId = rs.getInt("ovfmapid");
				int hostId = rs.getInt("hostid");
				//now use this to find out osdiskid from ovfmap table
				rs = db.query("select", "osdiskid", "ovfmap", "WHERE id=" + ovfmapId);
				if(rs.next())
				{
					int osdiskId = rs.getInt("osdiskid");
					//now use this to find if a suitable osdiskmap entry exists, if not then create one
					//first determine the cloud controller type managing the host
					rs = db.query("select", "cloudtypeid", "host", "where id=" + hostId);
					if(rs.next())
					{
						int cloudtypeId = rs.getInt("cloudtypeid");
						//now find if a osdiskma entry for this cloudtype exists
						rs = db.query("select", "id", "osdiskmap", "WHERE osdiskid=" + osdiskId + " AND cloudtypeid=" + cloudtypeId);
						if(rs.next())
						{
							//add the reference to vmslot entry
							int osdiskmapId = rs.getInt("id");
							status = db.update("vmslots", "osdiskmapid=" + osdiskmapId, "WHERE id=" + vmslotId);
						}
						else
						{
							//add image
                            rs=db.query("select", "hostid,localpath,ovfimagename,osdiskid", "vmslots,ovfmap,osdisk", "WHERE vmslots.id=" + vmslotId+" and osdisk.id=ovfmap.osdiskid and ovfmap.id=vmslots.ovfmapid");
                            String imageName=null;
                            String imagePath=null;
                            int hostid=-1;
                            int diskID;
                            if(rs.next())
                            {
                                imageName=rs.getString("ovfimagename");
                                imagePath=rs.getString("localpath");
                                hostid=rs.getInt("hostid");
                                diskID=rs.getInt("osdiskid");
                            }
                            else
                            {
                            	logger.debug("Error while adding image for vmslot:"+vmslotId);
                                //ERROR
                                return false;
                            }
                            logger.debug(imageName + " " + imagePath);
                            rs.close();
                            //check cloud provider type from host
                            rs=db.query("select", "cloudtypeid,headip,headport,version,sharedfolder", "cloudtype,host", "WHERE host.id=" + hostid+" and host.cloudtypeid=cloudtype.id");
                            String cloudIP,cloudFolder,version;
                            int cloudPort,cloudid;
                            if(rs.next())
                            {
                                cloudIP=rs.getString("headip");
                                cloudPort=rs.getInt("headport");
                                cloudFolder=rs.getString("sharedfolder");
                                cloudid=rs.getInt("cloudtypeid");
                                version=rs.getString("version");
                            }
                            else
                            {
                                return false;
                            }
                            logger.debug(cloudIP + " " + cloudPort);
                            //administrator
                            rs.close();
                            rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudid+" and accountmap.vepuser=user.id");
                            String iaasUser,iaasPass;
                            if(rs.next())
                            {
                                iaasUser=rs.getString("iaasuser");
                                iaasPass=rs.getString("iaaspass");
                            }
                            else
                            {
                                return false;
                            }
                            logger.debug(iaasUser + " " + iaasPass);
                            rs.close();     
                            rs=db.query("select", "ceeid", "vmslots", "WHERE vmslots.id="+vmslotId);
                            int ceeid;
                            if(rs.next())
                            {
                                ceeid=rs.getInt("ceeid");
                            }
                            else
                            {
                            	return false;
                            }
                            String template=null;      
                            if(version.startsWith("2."))
                            {
		                         ImageFIXClient fixImg;
		                         fixImg = new ImageFIXClient(cloudIP, "10556");
		                         String idPathImage = fixImg.downloadImage(imagePath,Integer.toString(ceeid)); 
		                         fixImg.setImage(idPathImage);
		                         fixImg.closeClient();     
                            }
                            //call right client based on cloud provider type
                            //create and register a new entry on osdiskmap (physical and database)
                            //version 3.4
                            if(version.startsWith("3."))
                            {
                            	One3XMLRPCHandler one3handle = new One3XMLRPCHandler(cloudIP, Integer.toString(cloudPort), iaasUser, iaasPass, "image managing");
                            	template = "NAME="+ceeid+"@"+imageName+"\n" +"PATH="+imagePath+"\n" +"TYPE=OS \n" +"PUBLIC=YES";                          
	                            logger.info(template);
	                            //unifique image:TODO
	                            //getCCE id and add to image
	                            //String template="NAME='"+providerImageName+"' \nPATH="+imagePath+" \nTYPE=OS \n PUBLIC=YES ";
	                            int response=one3handle.addImage(template);
	                            if(response==-1)
	                            {
	                            	logger.debug("Could not add image name:"+imageName);
	                                return false;
	                            //get response if it is fine update dabase
	                            }
	                            else
	                            {
	                            	rs = db.query("select", "max(id)", "osdiskmap", "");
	                            	int osdiskmapid = 1;
	                            	if(rs.next()) 
	                            	   osdiskmapid = rs.getInt(1) + 1;    
	                            	db.insert("osdiskmap (id,cloudtypeid,iaasname,iaastemplate,osdiskid,iaasid,status) ", "("+osdiskmapid+","+cloudid+",'"+(ceeid+"@"+imageName)+"','"+template+"',"+diskID+",'"+response+"', 'LCK')");
	                            	//now link this diskmap entry to this osdiskmap entry
	                            	db.update("vmslots", "osdiskmapid=" + osdiskmapid, "WHERE id=" + vmslotId);
	                            	return true;
	                            }
                            }
                            else if(version.startsWith("2."))
                            {
                            	throw new Exception("not implemented yet.");
                            	
                            }
						}
					}
					else
					{
						status = false;
					}
				}
				else
				{
					status = false;
				}
			}
			else
			{
				status = false;
			}
		}
		catch(Exception ex)
		{
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			logger.info("Exception caught inside update_vmslot_diskmap().");
			status = false;
		}
		return status;
	}
	
	private void deleteCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			int userId = -1;
			ResultSet rs = db.query("select", "id", "user", "WHERE username='" + username + "'");
			if(rs.next())
				userId = rs.getInt("id");
			if(userId == -1)
			{
				status = false;
				message += "Could not locate the VEP user record. ";
			}
			//get the list of all VM slots belonging to this CEE and remove them one by one
			rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId);
			while(rs.next() && status)
			{
				int vmslotId = rs.getInt("id");
				int hostId = rs.getInt("hostid");
				if(hostId == -1) 
				{
					message += "VM slot " + vmslotId + " VM is not scheduled on any host. ";
				}
				else
				{
					int iaasId = -1;
					try
					{
						iaasId = Integer.parseInt(rs.getString("iaasid"));
					}
					catch(Exception ex)
					{
						iaasId = -1; //VM not running or some thing else went wrong
					}
					if(iaasId == -1)
					{
						message += "VM slot " + vmslotId + " VM cloud ID was not found. ";
						//not running so no need to stop the associated VM
					}
					else
					{
						rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
						if(rs.next())
						{
							int cloudTypeId = rs.getInt("cloudtypeid");
							rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
							if(rs.next())
							{
								String headIP = rs.getString("headip");
								int headPort = rs.getInt("headport");
								String cloudVersion = rs.getString("version");
								int type = rs.getInt("typeid");
								rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
								if(rs.next())
								{
									String cloudType = rs.getString("name");
									rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userId + " AND cloudtype=" + cloudTypeId);
									if(rs.next())
									{
										String iaasUser = rs.getString("iaasuser");
										String iaasPass = rs.getString("iaaspass");
										if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
										{
											One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
											try
											{
												status = one3.shutdownVM(iaasId);
												if(!status)
												{
													message += "Probably the VM state does not permit SHUTDOWN operation. Or maybe the IaaS connection was refused. ";
													try
													{
														int msgid = 1;
														rs = db.query("select", "max(id)", "errormessagelist", "");
														if(rs.next())
														{
															msgid = rs.getInt(1) + 1;
														}
														db.insert("errormessagelist", "(" + msgid + ", 'Possible Runaway VM', 'The VM IaaS Id is " + iaasId + " in cloud with id " + cloudTypeId + "', '" + username + "', 'WARN', 'vmslots', '" + System.currentTimeMillis() + "', 0)");
													}
													catch(Exception ex1)
													{
														
													}
												}
											}
											catch(Exception ex)
											{
												//warn system admin of a potential runaway VM
												try
												{
													int msgid = 1;
													rs = db.query("select", "max(id)", "errormessagelist", "");
													if(rs.next())
													{
														msgid = rs.getInt(1) + 1;
													}
													db.insert("errormessagelist", "(" + msgid + ", 'Possible Runaway VM', 'The VM IaaS Id is " + iaasId + " in cloud with id " + cloudTypeId + ". Exception message: " + ex.getMessage() + "', '" + username + "', 'WARN', 'vmslots', '" + System.currentTimeMillis() + "', 0)");
												}
												catch(Exception ex1)
												{
													
												}
												status = false;
												message += "Exception caught during VM shutdown operation. ";
												logger.info("Exception caught during VM shutdown operation for VM slot: " + vmslotId);
												//if(logger.isDebugEnabled())
												//	ex.printStackTrace(System.err);
												logger.debug("Exception caught ", ex);
											}
										}
										else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
										{
											status = false;
											message += "This cloud type is currently not supported. ";
											
											
										}
									}
									else
									{
										status = false;
										message += "Corresponding cloud account for this user is not found. ";
									}
								}
								else
								{
									
								}
							}
							else
							{
								
							}
						}
						else
						{
							
						}
					}
				}
				//now delete all associated data for this vmslot
				status = db.delete("connectioninfo", "WHERE vmslotid=" + vmslotId);
				status = db.delete("vmslotconstraint", "WHERE vmslotid=" + vmslotId);
				status = db.delete("vmslots", "WHERE id=" + vmslotId);
				//now resetting the query
				rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId);
			}
			//now delete all ovfmap entries for this cee
			//first finding the application id of default application
			rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
			int appId = -1;
			if(rs.next()) appId = rs.getInt("id");
			rs.close();
			
			rs = db.query("select", "id", "ovf", "WHERE applicationid=" + appId);
			while(rs.next())
			{
				int ovfId = rs.getInt("id");
				rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + ovfId);
				while(rs.next())
				{
					int ovfmapId = rs.getInt("id");
					int osdiskId = rs.getInt("osdiskid");
					status = db.delete("ceenethandlerlist", "WHERE ovfmapid=" + ovfmapId);
					if(!status)
					{
						message += "Possible orphan entries in ceenethandlerlist table for this cee. ";
					}
					if(status) status = db.delete("ceestoragehandlerlist", "WHERE ovfmapid=" + ovfmapId);
					if(!status)
					{
						message += "Possible orphan entries in ceestoragehandlerlist table for this cee. ";
					}
					if(osdiskId != -1)
					{
						rs = db.query("select", "id, usecount", "osdisk", "WHERE id=" + osdiskId);
						if(rs.next())
						{
							int useCount = rs.getInt("usecount");
							if(useCount > 0) useCount--;
							if(status) status = db.update("osdisk", "usecount=" + useCount, "WHERE id=" + osdiskId);
							if(!status)
							{
								message += "Possible issue while updating the use count value for the osdisk entry. ";
							}
						}
					}
					if(status) status = db.delete("ovfmap", "WHERE id=" + ovfmapId);
					if(!status)
					{
						message += "Possible orphan entries in ovfmap table for this cee. ";
					}
					//now resetting the query
					rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + ovfId);
				}
				//all ovfmap for this ovf has been removed now, we can delete the OVF entry
				if(status) status = db.delete("ovf", "WHERE id=" + ovfId);
				if(!status)
				{
					message += "Possible orphan entries in ovf table for this cee. ";
				}
				//now resetting the query
				rs = db.query("select", "id", "ovf", "WHERE applicationid=" + appId);
			}
			//all OVFs have been deleted , now delete the default application
			if(status) status = db.delete("application", "WHERE id=" + appId);
			
			//now deleting all ceenethandlers entries
			if(status) status = db.delete("ceenethandlers", "WHERE ceeid=" + ceeId);
			if(!status)
			{
				message += "Possible orphan entries in ceenethandlers table for this cee. ";
			}
			//now deleting all ceestoragehandlers entries
			if(status) status = db.delete("ceestoragehandlers", "WHERE ceeid=" + ceeId);
			if(!status)
			{
				message += "Possible orphan entries in ceestoragehandlers table for this cee. ";
			}
			//now deleting all ceevmhandlers entries
			rs = db.query("select", "id", "ceevmhandlers", "WHERE ceeid=" + ceeId);
			while(rs.next())
			{
				int ceevmhandlerId = rs.getInt("id");
				//deleting all ceevmhandlerconstraint
				if(status) status = db.delete("ceevmhandlerconstraint", "WHERE ceevmhandlerid=" + ceevmhandlerId);
				if(!status)
				{
					message += "Possible orphan entries in ceevmhandlerconstraint table for this cee. ";
				}
				//now deleting this ceevmhandler entry
				if(status) status = db.delete("ceevmhandlers", "WHERE id=" + ceevmhandlerId);
				if(!status)
				{
					message += "Possible orphan entries in ceevmhandlers table for this cee. ";
				}
				//now resetting the query
				rs = db.query("select", "id", "ceevmhandlers", "WHERE ceeid=" + ceeId);
			}
			//now deleting all ceeconstraints
			rs = db.query("select", "id", "ceeconstraints", "WHERE ceeid=" + ceeId);
			while(rs.next())
			{
				int ceeConstraintId = rs.getInt("id");
				//deleting entries from ceeconstraintslist
				if(status) status = db.delete("ceeconstraintslist", "WHERE ceeconstraintsid=" + ceeConstraintId);
				if(!status)
				{
					message += "Possible orphan entries in ceeconstraintslist table for this cee. ";
				}
				//now deleting the cee constraint entry
				if(status) status = db.delete("ceeconstraints", "WHERE id=" + ceeConstraintId);
				if(!status)
				{
					message += "Possible orphan entries in ceeconstraints table for this cee. ";
				}
				//now resetting the query
				rs = db.query("select", "id", "ceeconstraints", "WHERE ceeid=" + ceeId);
			}
			//deleting all OSdisk entries for this CEE
			rs = db.query("select", "id", "osdisk", "WHERE ceeid=" + ceeId);
			while(rs.next())
			{
				int osdiskId = rs.getInt("id");
				rs = db.query("select", "*", "osdiskmap", "WHERE osdiskid=" + osdiskId);
				while(rs.next())
				{
					int diskmapId = rs.getInt("id");
					int cloudtypeId = rs.getInt("cloudtypeid");
					//use the administrator account for the cloud to delete the image
					String iaasName = rs.getString("iaasname");
					int iaasId = -1;
					try
					{
						iaasId = Integer.parseInt(rs.getString("iaasid"));
					}
					catch(Exception ex)
					{
						iaasId = -1;
					}
					if(iaasId != -1)
					{
						rs = db.query("select", "headip, headport, version, typeid", "cloudtype", "WHERE id=" + cloudtypeId);
						if(rs.next())
						{
							String headIp = rs.getString("headip");
							int headPort = rs.getInt("headport");
							int typeId = rs.getInt("typeid");
							String version = rs.getString("version");
							rs = db.query("select", "name", "supportedcloud", "WHERE id=" + typeId);
							if(rs.next())
							{
								String cloudType = rs.getString("name");
								if(cloudType.equalsIgnoreCase("OpenNebula") && version.startsWith("3."))
								{
									rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype=" + cloudtypeId + " and accountmap.vepuser=user.id");
		                            String iaasUser, iaasPass;
		                            if(rs.next())
		                            {
		                            	try
		                            	{
		                            		iaasUser = rs.getString("iaasuser");
		                            		iaasPass = rs.getString("iaaspass");
		                            		One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "image removal");
		                            		status = one3handle.removeImage(iaasId);
		                            		if(!status)
		                            		{
		                            			//warn system admin of a potential orphan IaaS OS disk
												try
												{
													int msgid = 1;
													rs = db.query("select", "max(id)", "errormessagelist", "");
													if(rs.next())
													{
														msgid = rs.getInt(1) + 1;
													}
													db.insert("errormessagelist", "(" + msgid + ", 'Orphan IaaS Image', 'The Image IaaS Id is " + iaasId + " in cloud with id " + cloudtypeId + "', '" + username + "', 'WARN', 'osdiskmap', '" + System.currentTimeMillis() + "', 0)");
												}
												catch(Exception ex1)
												{
													
												}
												status = true;
		                            		}
		                            	}
		                            	catch(Exception ex)
		                            	{
		                            		//warn system admin of a potential orphan IaaS OS disk
											try
											{
												int msgid = 1;
												rs = db.query("select", "max(id)", "errormessagelist", "");
												if(rs.next())
												{
													msgid = rs.getInt(1) + 1;
												}
												db.insert("errormessagelist", "(" + msgid + ", 'Orphan IaaS Image', 'The Image IaaS Id is " + iaasId + " in cloud with id " + cloudtypeId + ". Exception message: " + ex.getMessage() + "', '" + username + "', 'WARN', 'osdiskmap', '" + System.currentTimeMillis() + "', 0)");
											}
											catch(Exception ex1)
											{
												
											}
		                            	}
		                            }
		                            else
		                            {
		                            	message += "No valid cloud account map found for an administrator account. ";
		                            }
								}
								else
								{
									throw new Exception("Cloud type not supported yet.");
								}
							}
						}
					}
					else
					{
						message += "Invalid IaaS id for associated OS image. ";
					}
					
					//now delete this osmap entry
					if(status) status = db.delete("osdiskmap", "WHERE id=" + diskmapId);
					if(!status)
					{
						message += "Possible orphan entries in osdiskmap table for this cee. ";
					}
					//now resetting the query
					rs = db.query("select", "*", "osdiskmap", "WHERE osdiskid=" + osdiskId);
				}
				//now deleting this entry
				if(status) status = db.delete("osdisk", "WHERE id=" + osdiskId);
				if(!status)
				{
					message += "Possible orphan entries in osdisk table for this cee. ";
				}
				//now resetting the query
				rs = db.query("select", "id", "osdisk", "WHERE ceeid=" + ceeId);
			}
			//now deleteing the CEE entry
			if(status) status = db.delete("cee", "WHERE id=" + ceeId);
		}
		catch(Exception ex)
		{
			message += "Exception caught while trying to delete this CEE. ";
			logger.info("Exception caught while deleting the CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The delete CEE action was successful!</font><br><br>");
			stringBuilder.append("<font color='orange'>Warning messages (if any): ").append(message).append("</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			db.update("cee", "status='ERR'", "WHERE id=" + ceeId);
			stringBuilder.append("<font color='red'>The stop CEE action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void stopCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			int userId = -1;
			ResultSet rs = db.query("select", "id", "user", "WHERE username='" + username + "'");
			if(rs.next())
				userId = rs.getInt("id");
			if(userId == -1)
			{
				status = false;
				message += "Could not locate the VEP user record. ";
			}
			//get the list of all VM slots belonging to this CEE that are in state DEP=deployed
			rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId + " AND status='DEP'");
			int count = 0;
			while(rs.next() && status)
			{
				int vmslotId = rs.getInt("id");
				int hostId = rs.getInt("hostid");
				if(hostId == -1) 
				{
					message += "VM slot " + vmslotId + " VM is not scheduled on any host. ";
				}
				else
				{
					int iaasId = -1;
					try
					{
						iaasId = Integer.parseInt(rs.getString("iaasid"));
					}
					catch(Exception ex)
					{
						iaasId = -1; //VM not running or some thing else went wrong
					}
					if(iaasId == -1)
					{
						message += "VM slot " + vmslotId + " VM cloud ID was not found. ";
						//not running so no need to stop the associated VM
					}
					else
					{
						rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
						if(rs.next())
						{
							int cloudTypeId = rs.getInt("cloudtypeid");
							rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
							if(rs.next())
							{
								String headIP = rs.getString("headip");
								int headPort = rs.getInt("headport");
								String cloudVersion = rs.getString("version");
								int type = rs.getInt("typeid");
								rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
								if(rs.next())
								{
									String cloudType = rs.getString("name");
									rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userId + " AND cloudtype=" + cloudTypeId);
									if(rs.next())
									{
										String iaasUser = rs.getString("iaasuser");
										String iaasPass = rs.getString("iaaspass");
										if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
										{
											One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
											try
											{
												status = one3.shutdownVM(iaasId);
												if(!status)
												{
													message += "Probably the VM state does not permit SHUTDOWN operation. ";
												}
											}
											catch(Exception ex)
											{
												//warn system admin of a potential runaway VM
												try
												{
													int msgid = 1;
													rs = db.query("select", "max(id)", "errormessagelist", "");
													if(rs.next())
													{
														msgid = rs.getInt(1) + 1;
													}
													db.insert("errormessagelist", "(" + msgid + ", 'Possible Runaway VM', 'The VM IaaS Id is " + iaasId + " in cloud with id " + cloudTypeId + ". Exception message: " + ex.getMessage() + "', '" + username + "', 'WARN', 'vmslots', '" + System.currentTimeMillis() + "', 0)");
												}
												catch(Exception ex1)
												{
													
												}
												status = false;
												message += "Exception caught during VM shutdown operation. ";
												logger.info("Exception caught during VM shutdown operation for VM slot: " + vmslotId);
												//if(logger.isDebugEnabled())
												//	ex.printStackTrace(System.err);
												logger.debug("Exception caught ", ex);
											}
										}
										else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
										{
											status = false;
											message += "This cloud type is currently not supported. ";
											
											
										}
									}
									else
									{
										status = false;
										message += "Corresponding cloud account for this user is not found. ";
									}
								}
								else
								{
									
								}
							}
							else
							{
								
							}
						}
						else
						{
							
						}
					}
				}
				status = db.update("vmslots", "status='INI', hostid=-1", "WHERE id=" + vmslotId);
				//now resetting the query
				rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId + " AND status='DEP'");
			}
		}
		catch(Exception ex)
		{

			message += "Exception caught while trying to stop this CEE. ";
			logger.info("Exception caught while stopping the CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The stop CEE action was successful!</font><br><br>");
			stringBuilder.append("<font color='orange'>Warning messages (if any): ").append(message).append("</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The stop CEE action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}

	private void deployCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		Date now = new Date();
		
		int ceeId = -1;
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		logger.info("Deploy CEE action for CEE " + ceeId + " received at: " + now);
		boolean status = true;
		String message = "";
		String log = "";
		try
		{
			ResultSet rs = db.query("select", "status", "cee", "WHERE id=" + ceeId);
			String ceeStatus = "ERR";
			if(rs.next()) ceeStatus = rs.getString("status");
			if(ceeStatus.equalsIgnoreCase("RDY"))
			{
				//ok so here get the list of all vmslots for this cee which has status INI or STP or ERR
				//then send the scheduler with vmslotsid list and list of constraints
				rs = db.query("select", "*", "vmslots", "WHERE ceeid=" + ceeId + " AND status='INI' OR status='STP' OR status='ERR'");
				ArrayList<Integer> vmSlotList = new ArrayList<Integer>();
				while(rs.next())
				{
					int slotId = rs.getInt("id");
					vmSlotList.add(new Integer(slotId));
				}
				//System.err.println("Sending the arraylist with size: " + vmSlotList.size());
				Map<String, String> schedule = Scheduler.requestDeploy(vmSlotList,Scheduler.FIRSTFIT);
				if(schedule == null)
				{
					status = false;
					message += "It is not possible to schedule that many VM slots. Free up resources and try again. ";
					logger.info("Scheduler Response NULL: It is not possible to schedule.");
				}
				else
				{
					//display the received schedule for debug reasons
					Set<String> mapKeys = schedule.keySet();
					Collection<String> mapKeyval = schedule.values();
					Iterator<String> mapKeyIter = mapKeys.iterator();
					Iterator<String> mapValIter = mapKeyval.iterator();
					while(mapKeyIter.hasNext() && mapValIter.hasNext() && status)
					{
						String key = mapKeyIter.next();
						String value = mapValIter.next();
						
						logger.info("+++ For VM Slot ID: " + key + ", Host ID to be scheduled on: " + value);
						
						//now update the host id for the scheduled VM slots
						int slotId = Integer.parseInt(key);
						int hostId = Integer.parseInt(value);
						status = db.update("vmslots", "status='SCH', hostid=" + hostId, "WHERE id=" + slotId);
						if(status)
							logger.info("Updated vmslot " + slotId + ", changed status to SCH and host scheduled on to " + hostId);
						else
							logger.warn("Error in updating the parameters of vmslot id=" + slotId);
						try
						{
							if(status) status = update_vmslot_diskmap(slotId);	//update the osdiskmap link for this vmslot	
							if(!status) message = "Could not register the image on provider for vmslot "+slotId;
						}
						catch(Exception ex)
						{
							message += "Exception caught while processing your request. ";
							logger.info("Exception caught while updating osdiskmap");
							//if(logger.isDebugEnabled())
							//	ex.printStackTrace(System.err);
							logger.debug("Exception caught ", ex);
							status = false;
						}
					}
				}
				
				if(status)
				{
					//Deploy method, update status and message with answer 
					deployCEEElements(ceeId, username);
				}
				else
				{
					// send message describing that deployment could not start
				}
			}
			else
			{
				message += "CEE is not in READY state. This action can not be processed. Try re-initializing your CEE. ";
				status = false;
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while deploying the CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void deployCEEElements(int ceeId, String username) throws SQLException
	{
		ResultSet rs = db.query("select", "*", "vmslots", "WHERE ceeid=" + ceeId + " AND status='SCH'");
		ArrayList<Integer> vmSlotsList = new ArrayList<Integer>(); 
		while(rs.next())
		{
			vmSlotsList.add(rs.getInt("id"));
		}
		CEEDeploymentThread dep = new CEEDeploymentThread(vmSlotsList, username);
		dep.start();
	}
	
	/*private HashMap<String, Object> generateTemplate(int slotId, String username)
	{
		//System.err.println("Trying to generate VM template for slotId: " + slotId);
		HashMap<String, Object> params = new HashMap<String, Object>();
		HashMap<String, Object> cloudSpecs = new HashMap<String, Object>();
		String template = "";
		try
		{
			ResultSet rs = db.query("select", "*", "vmslots", "where id="+slotId);
			if(rs.next())
			{
				String ceevmhandlerid = String.valueOf(rs.getInt("ceevmhandlerid"));
				//String iaasid = rs.getString("iaasid");
				String osdiskmapid = String.valueOf(rs.getInt("osdiskmapid"));
				String ovfmapid = String.valueOf(rs.getInt("ovfmapid"));
				
				//retrieve cpu/ram/cores
				rs = db.query("select", "vmhandlerid", "ceevmhandlers", "where id="+ceevmhandlerid);
				if(rs.next())
				{
					int vmhandlerid = rs.getInt("vmhandlerid");
					rs = db.query("select", "*", "vmhandler", "where id="+vmhandlerid);
					if(rs.next())
					{
						int vcpu = rs.getInt("cpufreq_low");
						params.put("vcpu", vcpu);
						int ram = rs.getInt("ram_low");
						params.put("ram", ram);
						int cores = rs.getInt("corecount_low");
						params.put("cores", cores);
						int diskSize = rs.getInt("disksize_low");
						params.put("diskSize", diskSize);
					}
				}
			
				//retrieve ovfmap infos
				rs = db.query("select","*","ovfmap","where id="+ovfmapid);
				if(rs.next())
				{
					String vmName = rs.getString("ovfid");
					params.put("name", vmName);
				}
					
				//retrieve networks as a List of Maps("name:value", "ip:[dhcp||address]")
				ArrayList<HashMap<String, String>> networks = new ArrayList<HashMap<String,String>>(); 
				rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid); 
				int count = 0;
				while(rs.next())
				{
					count++;
					HashMap<String, String> network = new HashMap<String, String>();
					int ceenethandlerid = rs.getInt("ceenethandlerid");
					rs = db.query("select", "vnethandlerid", "ceenethandlers", "where id="+ceenethandlerid);
					if(rs.next())
					{
						int vnethandlerid = rs.getInt("vnethandlerid");
						rs = db.query("select", "cloudnetworkid", "vnethandler", "where id="+vnethandlerid);
						if(rs.next())
						{
							int cloudnetworkid = rs.getInt("cloudnetworkid");
							rs = db.query("select", "*", "cloudnetwork", "where id="+cloudnetworkid);
							if(rs.next())
							{
								String name = rs.getString("name");
								network.put("name", name);
								network.put("ip", "dhcp");// /!\hard coded dhcp
								networks.add(network);
							}
						}
					}
					//now resetting the sql query back
					rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid);
					int counter = 0;
					while(counter < count) { rs.next(); counter++; }
				}	
				params.put("networks", networks);
				//retrieve OSDisk Name as a Map<"name:value"> (other informations to be added later on if necessary)
				rs = db.query("select", "*", "osdiskmap", "where id="+osdiskmapid);
				int typeId = 0;
				String cloudVersion = null;
				if(rs.next())
				{
					HashMap<String, String> osDisk = new HashMap<String, String>();
					String iaasDiskName = rs.getString("iaasname");
					osDisk.put("name", iaasDiskName);
					params.put("osDisk", osDisk);
					int cloudtypeid = rs.getInt("cloudtypeid");
				
					//retrieve name of cloud provider and version
					rs = db.query("select", "typeid,version,headip,headport", "cloudtype", "where id="+cloudtypeid);
					if(rs.next())
					{
						typeId = rs.getInt("typeid");
						cloudVersion = rs.getString("version");
						cloudSpecs.put("headip", rs.getString("headip"));
						cloudSpecs.put("headport", rs.getInt("headport"));
					}
	
					//retrieve vep user's id from username
					logger.debug("Using VEP Username: " + username);
					rs = db.query("select", "id", "user", "where username=\""+username+"\"");
					if(rs.next())
					{
						int userid = rs.getInt("id");
	
						//retrieve IaaS UserName
						rs = db.query("select", "iaasuser,iaaspass", "accountmap", "where vepuser="+userid);
						try 
						{
							if(rs.next())
							{
								String iaasuser = rs.getString("iaasuser");
								String iaaspass = rs.getString("iaaspass");
								params.put("username", iaasuser);
								cloudSpecs.put("username", iaasuser);
								cloudSpecs.put("password", iaaspass);
							}
						} 
						catch(SQLException ex)
						{
							logger.info("No account linked to this user on the predefined cloud");
							//if(logger.isDebugEnabled())
							//	ex.printStackTrace(System.err);
							logger.debug("Exception caught ", ex);
						}
					}
					//retrieve admin user name on the cloud to id resources
			        rs = db.query("select", "iaasuser", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudtypeid+" and accountmap.vepuser=user.id");
			        String adminIaaSUname = "";
			        if(rs.next())
			        {
			        	adminIaaSUname=rs.getString("iaasuser");
			        	params.put("adminIaaSUname", adminIaaSUname);
			        }
			        rs.close();
				}
				
				//temp hack for ovfId and ovfVSId, may later be used to retrieve context from the generator
				params.put("ovfId", "myOvfId");
				params.put("ovfVSId", "myOvfVSId");
				
				//Generate template
				TemplateGenerator tg = new TemplateGenerator(typeId, cloudVersion);
				
				template = tg.getTemplate(params);

				if(!template.equals(""))
				{
					//db.update("vmslots", "template="+template, "where id="+slotId);
					db.updateTemplate(slotId, template);
					
					cloudSpecs.put("typeId", typeId);
					cloudSpecs.put("cloudVersion", cloudVersion);
					return cloudSpecs;
				}
				else	
					return null;
			}
		}
		catch(Exception ex)
		{
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			return null;
		}
		return null;
	}*/

	private void allocateCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		String log = "";
		try
		{
			ResultSet rs = db.query("select", "status", "cee", "WHERE id=" + ceeId);
			String ceeStatus = "ERR";
			if(rs.next()) ceeStatus = rs.getString("status");
			if(ceeStatus.equalsIgnoreCase("RDY"))
			{
				//get the list of ovfmap entry for this ceeid and create one vmslot for each
				//first get the default application id
				rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
				int appId = -1;
				if(rs.next()) appId = rs.getInt("id");
				rs.close();
				
				rs = db.query("select", "*", "ovf", "WHERE applicationid=" + appId);
				int count=0;
				while(rs.next())
				{
					count++;
					int ovfContainerId = rs.getInt("id");
					rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + ovfContainerId);
					int count1 = 0;
					while(rs.next() && status)
					{
						count1++;
						int ovfmapId = rs.getInt("id");
						int ceehandlerId = rs.getInt("ceevmhandlerid");
						int hostId = -1;
						String staticIP = rs.getString("staticip");
						String currStatus = rs.getString("status");
						//creating a new vmslots entry next
						int vmslotId = 1;
						rs = db.query("select", "max(id)", "vmslots", "");
	                    if(rs.next()) vmslotId = rs.getInt(1) + 1;
	                    rs.close();
						
	                    //now checking if the ovfmap is in locked state or not
	                    if(staticIP.length() > 6)
	                    {
	                    	if(currStatus.equalsIgnoreCase("lck"))
	                    	{
	                    		message += "Can not allocate a VM slot as one instance has already been created. This restriction is due to the fact that it contains static IP. ";
	                    		status = false;
	                    		logger.warn("Could not create a new vm slot entry for OVFMAPID = " + ovfmapId + ", status was locked.");
	                    	}
	                    	else
	                    	{
	                    		status = db.insert("vmslots", "(" + vmslotId + ", " + ceeId + ", " + ceehandlerId + ", " + hostId + ", " + ovfmapId + ", 'INI', '', '', -1, -1, 0, -1)");
	                    		db.update("ovfmap", "status='LCK'", "WHERE id=" + ovfmapId);
	                    		logger.info("Created a new vm slot entry for OVFMAPID = " + ovfmapId);
	                    	}
	                    }
	                    else
	                    {
	                    	status = db.insert("vmslots", "(" + vmslotId + ", " + ceeId + ", " + ceehandlerId + ", " + hostId + ", " + ovfmapId + ", 'INI', '', '', -1, -1, 0, -1)");
	                    	logger.info("Created a new vm slot entry for OVFMAPID = " + ovfmapId);
	                    }
						//now resetting the sql query
						rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + ovfContainerId);
						int counter=0;
						while(counter < count1) { rs.next(); counter++; }
					}
					//now resetting the sql query back
					rs = db.query("select", "*", "ovf", "WHERE applicationid=" + appId);
					int counter = 0;
					while(counter < count) { rs.next(); counter++; }
				}
			}
			else
			{
				message += "CEE is not in READY state. This action cannot proceed. Try to re-initialize the CEE. ";
				status = false;
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while allocating the CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			//stringBuilder.append("<font color='navy'>Partial log of actions undertaken:<hr><p style='font-family:Verdana;font-size:8pt;padding:10px;background:#EEEEEE;color:black;'>").append(log).append("</p><hr></font>");
			//stringBuilder.append("<div style='padding:10px;background:#F5BCA9;font-family:Verdana;font-size:9pt;'>INFO: It may some time before you can deploy your application depending on how much time the background disk-copy process will take to move the remote OS disk images into the local storage. Please be patient ...</div><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void initializeCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		String log = "";
		try
		{
			//building the VMhandlers Matrix for matching algorithm
			LinkedList<vmHandlerElement> vmhandlerMatrix;
			vmhandlerMatrix = new LinkedList<vmHandlerElement>();
			
			LinkedList<netHandlerElement> nethandlerMatrix;
			nethandlerMatrix = new LinkedList<netHandlerElement>();
			
			ResultSet rs = db.query("select", "*", "ceevmhandlers", "WHERE ceeid=" + ceeId);
			int count=0;
			while(rs.next())
			{
				count++;
				vmHandlerElement temp = new vmHandlerElement();
				
				temp.ceeVMHandlerId = rs.getInt("id"); 
				rs = db.query("select", "*", "vmhandler", "WHERE id=" + rs.getInt("vmhandlerid"));
				if(rs.next())
				{
					temp.cpufreq_low = rs.getInt("cpufreq_low"); 
					temp.cpufreq_high = rs.getInt("cpufreq_high"); 
					temp.ram_low = rs.getInt("ram_low"); 
					temp.ram_high = rs.getInt("ram_high"); 
					temp.corecount_low = rs.getInt("corecount_low"); 
					temp.corecount_high = rs.getInt("corecount_high"); 
					temp.disksize_low = rs.getInt("disksize_low"); 
					temp.disksize_high = rs.getInt("disksize_high"); 
					temp.score = 0; 
					vmhandlerMatrix.add(temp);
				}
				//now resetting the sql query
				rs = db.query("select", "*", "ceevmhandlers", "WHERE ceeid=" + ceeId);
				int counter = 0;
				while(counter < count) {rs.next(); counter++; }
			}
			
			rs = db.query("select", "*", "ceenethandlers", "WHERE ceeid=" + ceeId);
			count=0;
			while(rs.next())
			{
				count++;
				netHandlerElement temp = new netHandlerElement();
				
				temp.ceeNetHandlerId = rs.getInt("id"); 
				rs = db.query("select", "*", "vnethandler", "WHERE id=" + rs.getInt("vnethandlerid"));
				if(rs.next())
				{
					temp.name = rs.getString("name"); 
					temp.dhcpSupport = rs.getInt("dhcpsupport"); 
					temp.vlanSupport = rs.getInt("vlantagsupport"); 
					temp.publicIpSupport = rs.getInt("pubipsupport"); 
					rs = db.query("select", "*", "cloudnetwork", "WHERE id=" + rs.getInt("cloudnetworkid"));
					if(rs.next())
					{
						temp.cloudTypeId = rs.getInt("cloudtypeid");
					}
					temp.score = 0; 
					nethandlerMatrix.add(temp);
				}
				//now resetting the sql query
				rs = db.query("select", "*", "ceenethandlers", "WHERE ceeid=" + ceeId);
				int counter = 0;
				while(counter < count) {rs.next(); counter++; }
			}
			
			//now getting the list of all OVFs and processing them one by one
				//first get the default application id
			rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
			int appId = -1;
			if(rs.next())
			{
				appId = rs.getInt("id");
			}
			rs.close();
			
			rs = db.query("select", "*", "ovf", "WHERE applicationid=" + appId);
			count = 0;
			while(rs.next() && status)
			{
				count++;
				int id = rs.getInt("id");
				String ovfDesc = rs.getString("descp");
				OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovfDesc, "2.2.1", "oneadmin");
				
				//matching the vmhandlers and vnethandlers next
				String[] ovfIds = parser.getIDs();
				int[] bestMatchIndex = findBestVMHandlerMatch(vmhandlerMatrix, ovfDesc);
				//the bestMatchIndex corresponds to the order of virtual systems in the OVF
				//before adding a new OVF map entry, if one exist already just update it ...
				if(bestMatchIndex != null)
				{
					//find the ovfmap index corresponding to various virtual systems and if necessary update the VM handlers index
					for(int i=0; i<bestMatchIndex.length && status; i++) if(bestMatchIndex[i] < 0) {status = false; message += "Some Virtual Systems can not be matched to any handler. Erring system id: " + ovfIds[i];}
					
					if(status) status = db.update("ovfmap", "status='OR'", "WHERE ovfcontainerid=" + id); //setting all entries to OR
					
					for(int i=0; i<ovfIds.length && status; i++)
					{
						//getting the OS disk details for this virtual system
						String[] diskIds = parser.getVMDisksId(ovfIds[i]);
						int osDiskId = -1;
						for(int j=0; j<diskIds.length; j++)
						{
							String ovfDiskPath = parser.getImageDiskPath(ovfIds[i], diskIds[j]);
							String ovfDiskName = diskIds[j];
							//System.err.println("For virtual system: " + ovfIds[i] + " - disk name: " + ovfDiskName + ", path=" + ovfDiskPath);
							//now checking if this disk entry is in the osdisk table
							rs = db.query("select", "*", "osdisk", "WHERE ceeid=" + ceeId + " AND ovfimagename='" + ovfDiskName + "' AND ovfimagepath='" + ovfDiskPath + "'");
							if(rs.next())
							{
								osDiskId = rs.getInt("id");
								//if status = OR then set status as INI
								String stat = rs.getString("status");
								if(stat.equalsIgnoreCase("OR"))
									db.update("osdisk", "status='INI', usecount=0", "WHERE id=" + osDiskId);
							}
							else
							{
								//create a placeholder entry in the osdisk table
								rs = db.query("select", "max(id)", "osdisk", "");
			                    osDiskId = 1;
			                    if(rs.next()) osDiskId = rs.getInt(1) + 1;
			                    rs.close();
			                    status = db.insert("osdisk", "(" + osDiskId + ", '', '" + ovfDiskPath + "', '" + ovfDiskName + "', 'INI', 0, " + ceeId + ")"); //for the moment setting diskcount to 0
			                    logger.info("Added a new OS Disk entry for VirtualSystem: " + ovfIds[i] + ", OVFDiskPath=" + ovfDiskPath);
							}
							break; //to prevent processing of more than 1 OS disk per v-system
						}
						//the above code will break if a virtual system actually contains two OS disks, so a fix has to be designed in the parser
						
						rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + id + " AND ovfid='" + ovfIds[i] + "'");
						int ovfmapId = -1;
						if(rs.next())
						{
							//update the ceevmhandler index to the one matched ... if new index is -1 return warning message
							ovfmapId = rs.getInt("id");
							int currOsDiskId = rs.getInt("osdiskid");
							String staticIP = rs.getString("staticip");
							String currStatus = rs.getString("status");
							if(rs.getInt("ceevmhandlerid") != bestMatchIndex[i])
							{
								if(!(staticIP.length() > 6 && currStatus.equalsIgnoreCase("lck")))
									status = db.update("ovfmap", "ceevmhandlerid=" + bestMatchIndex[i] + ", status='RDY'", "WHERE id=" + ovfmapId);
								else
								{
									//not changing the locked state as it will be reset when the corresponding VMslot is deleted
									status = db.update("ovfmap", "ceevmhandlerid=" + bestMatchIndex[i], "WHERE id=" + ovfmapId);
								}
//								update_context_staticip(ovfmapId);
								logger.info("Updated OVF Map entry for VirtualSystem: " + ovfIds[i] + ", new VMHanlderId=" + bestMatchIndex[i]);
								log += "Updated OVF Map entry for VirtualSystem: " + ovfIds[i] + ", with new VMHanlderId=" + bestMatchIndex[i] + "<br>";
								if(currOsDiskId == osDiskId)
								{
									//nothing to be done
									logger.info("No change needed for the OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId);
									log += "No change needed for the OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + "<br>";
								}
								else
								{
									//for the currdiskid = reduce use count by 1 and increment use count by 1 for the new diskid and make ovfmap point to the new diskid
									rs = db.query("select", "*", "osdisk", "WHERE id=" + currOsDiskId);
									if(rs.next())
									{
										int usecount = rs.getInt("usecount") - 1;
										if(usecount <= 0)
										{
											status = db.update("osdisk", "status='OR', usecount=0", "WHERE id=" + currOsDiskId);
										}
										else
										{
											status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + currOsDiskId);
										}
									}
									rs = db.query("select", "*", "osdisk", "WHERE id=" + osDiskId);
									if(rs.next())
									{
										int usecount = rs.getInt("usecount") + 1;
										status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + osDiskId);
									}
									status = db.update("ovfmap", "osdiskid=" + osDiskId, "WHERE id=" + ovfmapId);
									logger.info("OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.");
									log += "OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.<br>";
								}
							}
							else
							{
								//status = db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
								if(!(staticIP.length() > 6 && currStatus.equalsIgnoreCase("lck")))
									status = db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
								else
								{
									//not changing the locked state as it will be reset when the corresponding VMslot is deleted
									
								}
//								update_context_staticip(ovfmapId);
								logger.info("OVF Map entry for VirtualSystem: " + ovfIds[i] + " was not updated, already matched to VMHanlderId=" + bestMatchIndex[i]);
								log += "OVF Map entry for VirtualSystem: " + ovfIds[i] + " was not updated, already matched to VMHanlderId=" + bestMatchIndex[i] + "<br>";
								if(currOsDiskId == osDiskId)
								{
									//nothing to be done
									logger.info("No change needed for the OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId);
									log += "No change needed for the OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + "<br>";
								}
								else
								{
									//for the currdiskid = reduce use count by 1 and increment use count by 1 for the new diskid and make ovfmap point to the new diskid
									rs = db.query("select", "*", "osdisk", "WHERE id=" + currOsDiskId);
									if(rs.next())
									{
										int usecount = rs.getInt("usecount") - 1;
										if(usecount <= 0)
										{
											status = db.update("osdisk", "status='OR', usecount=0", "WHERE id=" + currOsDiskId);
										}
										else
										{
											status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + currOsDiskId);
										}
									}
									rs = db.query("select", "*", "osdisk", "WHERE id=" + osDiskId);
									if(rs.next())
									{
										int usecount = rs.getInt("usecount") + 1;
										status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + osDiskId);
									}
									status = db.update("ovfmap", "osdiskid=" + osDiskId, "WHERE id=" + ovfmapId);
									logger.info("OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.");
									log += "OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.<br>";
								}
							}
						}
						else
						{
							//create a new OVFmap entry
							rs = db.query("select", "max(id)", "ovfmap", "");
		                    ovfmapId = 1;
		                    if(rs.next()) ovfmapId = rs.getInt(1) + 1;
		                    rs.close();
		                    
		                    status = db.insert("ovfmap", "(" + ovfmapId + ", " + id + ", '" + ovfIds[i] + "', " + bestMatchIndex[i] + ", -1, 'RDY', '', '')"); //for the moment setting osdisk id to -1 FIX LATER
		                    //call a function here to update the CONTEXT and static IP address fields if any
//		                    update_context_staticip(ovfmapId);
		                    
		                    logger.info("Added a new OVF Map entry for VirtualSystem: " + ovfIds[i] + ", VMHanlderId=" + bestMatchIndex[i]);
		                    log += "Added a new OVF Map entry for VirtualSystem: " + ovfIds[i] + ", VMHanlderId=" + bestMatchIndex[i] + "<br>";
		                    //make this new ovfmap entry point to the osdiskid
		                    rs = db.query("select", "*", "osdisk", "WHERE id=" + osDiskId);
							if(rs.next())
							{
								int usecount = rs.getInt("usecount") + 1;
								status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + osDiskId);
							}
							status = db.update("ovfmap", "osdiskid=" + osDiskId, "WHERE id=" + ovfmapId);
							logger.info("OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.");
							log += "OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.<br>";
						}
						update_context_staticip(parser, ovfIds[i], ovfmapId);
						
						//now find the corresponding network and storage handlers for this OVFID system and link or update those db entries
						//also update and verify the OS image for the ovfmap entry ...
						int[] vnetHandlersIdList = findBestVNetHandlerMatch(ovfIds[i], ovfDesc, ceeId, nethandlerMatrix);
						//first delete all old ceenethandlerlist entries for this ovfmap
						status = db.delete("ceenethandlerlist", "WHERE ovfmapid=" + ovfmapId);
						for(int j=0; status && vnetHandlersIdList!=null && j<vnetHandlersIdList.length; j++)
						{
							//System.err.println("received: " + vnetHandlersIdList[j] + " for VirtualSystem " + ovfIds[i]);
							if(vnetHandlersIdList[j] != -1)
							{	
								//create a new ceenethandlerlist entry for this OVFmap entry
								rs = db.query("select", "max(id)", "ceenethandlerlist", "");
			                    int netId = 1;
			                    if(rs.next()) netId = rs.getInt(1) + 1;
			                    rs.close();
			                    status = db.insert("ceenethandlerlist", "(" + netId + ", " + ovfmapId + ", " + vnetHandlersIdList[j] + ")");
			                    logger.info("Added a new CEENetHandlerList entry for VirtualSystem: " + ovfIds[i] + ", VNetHanlderId=" + vnetHandlersIdList[j]);
			                    log += "Added a new CEENetHandlerList entry for VirtualSystem: " + ovfIds[i] + ", VNetHanlderId=" + vnetHandlersIdList[j] + "<br>";
							}
						}
						int[] storageHandlersIdList = findBestStorageHandlerMatch(ovfIds[i], ovfDesc, ceeId);
						
					}
					
					//now for all the entries whose status = OR delete them, remove corresponding ceenethandlerlist, ceestoragehandlerlist, osdisk entries, and set corresponding VMSLOT entries link to -1
					rs = db.query("select", "id, osdiskid", "ovfmap", "WHERE ovfcontainerid=" + id + " AND status='OR'");
					int count1 = 0;
					while(rs.next())
					{
						count1++;
						int ovfmapID = rs.getInt("id");
						int diskID = rs.getInt("osdiskid");
						if(status && diskID != -1)
						{
							rs = db.query("select", "usecount", "osdisk", "WHERE id=" + diskID);
							int newCount = rs.getInt("usecount") - 1;
							if(newCount == 0) status = db.update("osdisk", "status='OR', usecount=0", "WHERE id=" + diskID); //setting this entry status to OR
							else
								status = db.update("osdisk", "usecount=" + newCount, "WHERE id=" + diskID); //reducing the usecount value by 1
						}
						
						//delete all vnethandlerlist entries linked to this entry
						if(status) status = db.delete("ceenethandlerlist", "WHERE ovfmapid=" + ovfmapID);
						//similarly clear the cee storagehandlerslist entries if any
						if(status) status = db.delete("ceestoragehandlerlist", "WHERE ovfmapid=" + ovfmapID);
						//set VMslot ovfmapid to -1 for this entry
						if(status) status = db.update("vmslots", "ovfmapid=-1", "WHERE ovfmapid=" + ovfmapID);
						
						//now resetting the sql query
						rs = db.query("select", "id, osdiskid", "ovfmap", "WHERE ovfcontainerid=" + id + " AND status='OR'");
						int counter=0;
						while(counter < count1) { rs.next(); counter++; }
					}
					//now simply delete all OR entries
					if(status) status = db.delete("ovfmap", "WHERE ovfcontainerid=" + id + " AND status='OR'");
					
					//now call the disk management thread to manage the disk download and storage in local folder
					DiskManagement dprocess = new DiskManagement(ceeId, username + "-" + ceeId + "-diskmove", username);
					dprocess.start();
					VEPHelperMethods.addService(username + "-" + ceeId + "-diskmove", dprocess);	//so that the thread is not killed when this process goes out of scope
				}
				else
				{
					status = false;
					message += "Something went wrong while finding the suitable VM handlers for Virtual Systems in your CEE. ";
				}
				
				//reseting the SQL query index
				rs = db.query("select", "*", "ovf", "WHERE applicationid=" + appId);
				int counter = 0;
				while(counter < count) { rs.next(); counter++; }
			}
			if(count == 0)
			{
				status = false;
				message += "There is no valid OVF attached to this CEE. ";
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while initializing the CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			db.update("cee", "status='RDY'", "WHERE id=" + ceeId);
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<font color='navy'>Partial log of actions undertaken:<hr><p style='font-family:Verdana;font-size:8pt;padding:10px;background:#EEEEEE;color:black;'>").append(log).append("</p><hr></font>");
			stringBuilder.append("<div style='padding:10px;background:#F5BCA9;font-family:Verdana;font-size:9pt;'>INFO: It may some time before you can deploy your application depending on how much time the background disk-copy process will take to move the remote OS disk images into the local storage. Please be patient ...</div><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			db.update("cee", "status='ERR'", "WHERE id=" + ceeId);
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void removeNetHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int nethandlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("ceenethandlerid")) nethandlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			//first set all ceenethandlerlist entries for this nethandler to point to -1
			status = db.update("ceenethandlerlist", "ceenethandlerid=-1", "WHERE ceenethandlerid=" + nethandlerId);
			if(status)
				status = db.delete("ceenethandlers", "WHERE id=" + nethandlerId);
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while deleting the network handler belonging to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void addVNetHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int nethandlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("vnethandlerid")) nethandlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		//now checking if the network handler has already been added to the CEE
		try
		{
			ResultSet rs = db.query("select", "id, vnethandlerid", "ceenethandlers", "WHERE ceeid=" + ceeId + " AND vnethandlerid=" + nethandlerId);
			if(rs.next())
			{
				status = false;
				message += "This network handler is already added in your CEE. ";
			}
			rs.close();
			if(status)
			{
				rs = db.query("select", "max(id)", "ceenethandlers", "");
                int id = 1;
                if(rs.next()) id = rs.getInt(1) + 1;
                rs.close();
                status = db.insert("ceenethandlers", "(" + id + ", " + nethandlerId + ", " + ceeId + ")");
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught during adding the network handler to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void removeStorageHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int storagehandlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("ceestoragehandlerid")) storagehandlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			//first make all entries in ceestoragehandlerlist to -1 that points to this ceestoragehandler
			status = db.update("ceestoragehandlerlist", "ceestoragehandlerid=-1", "WHERE ceestoragehandlerid=" + storagehandlerId);
			if(status)
				status = db.delete("ceestoragehandlers", "WHERE id=" + storagehandlerId);
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while deleting the storage handler belonging to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void addStorageHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int storagehandlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("storagehandlerid")) storagehandlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		//now checking if the storage handler has already been added to the CEE
		try
		{
			ResultSet rs = db.query("select", "id, storagehandlerid", "ceestoragehandlers", "WHERE ceeid=" + ceeId + " AND storagehandlerid=" + storagehandlerId);
			if(rs.next())
			{
				status = false;
				message += "This storage handler is already added in your CEE. ";
			}
			rs.close();
			if(status)
			{
				rs = db.query("select", "max(id)", "ceestoragehandlers", "");
                int id = 1;
                if(rs.next()) id = rs.getInt(1) + 1;
                rs.close();
                status = db.insert("ceestoragehandlers", "(" + id + ", " + storagehandlerId + ", " + ceeId + ")");
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught during adding the storage handler to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void removeVMHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int handlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("ceevmhandlerid")) handlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			//first make all ceevmhandlerconstraint entry point to -1 that points to this vm handler
			status = db.update("ceevmhandlerconstraint", "ceevmhandlerid=-1", "WHERE ceevmhandlerid=" + handlerId);
			if(status)
				status = db.delete("ceevmhandlers", "WHERE id=" + handlerId);
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while deleting the VM handler belonging to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void addVMHandler(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int vmhandlerId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("vmhandlerid")) vmhandlerId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		//now checking if the vm handler has already been added to the CEE
		try
		{
			ResultSet rs = db.query("select", "id, vmhandlerid", "ceevmhandlers", "WHERE ceeid=" + ceeId + " AND vmhandlerid=" + vmhandlerId);
			if(rs.next())
			{
				status = false;
				message += "This handler is already added in your CEE. ";
			}
			rs.close();
			if(status)
			{
				rs = db.query("select", "max(id)", "ceevmhandlers", "");
                int id = 1;
                if(rs.next()) id = rs.getInt(1) + 1;
                rs.close();
                status = db.insert("ceevmhandlers", "(" + id + ", " + vmhandlerId + ", " + ceeId + ")");
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught during adding the VM handler to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void removeConstraint(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int constraintId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("constraintid")) constraintId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		try
		{
			//first make all constraints in cee constraintslist = -1 that use this constraint
			status = db.update("ceeconstraintslist", "ceeconstraintsid=-1", "WHERE ceeconstraintsid=" + constraintId);
			if(status)
				status = db.delete("ceeconstraints", "WHERE id=" + constraintId);
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught while deleting the constraint belonging to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void addConstraint(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		int constraintId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("constraintid")) constraintId = Integer.parseInt(value);
		}
		boolean status = true;
		String message = "";
		//now checking if the constraint has already been added to the CEE
		try
		{
			ResultSet rs = db.query("select", "id, constrainttype", "ceeconstraints", "WHERE ceeid=" + ceeId + " AND constrainttype=" + constraintId);
			if(rs.next())
			{
				status = false;
				message += "This constraint is already added in your CEE. ";
			}
			rs.close();
			if(status)
			{
				rs = db.query("select", "max(id)", "ceeconstraints", "");
                int id = 1;
                if(rs.next()) id = rs.getInt(1) + 1;
                rs.close();
                status = db.insert("ceeconstraints", "(" + id + ", " + constraintId + ", " + ceeId + ")");
			}
		}
		catch(Exception ex)
		{
			message += "Exception caught while processing your request. ";
			logger.info("Exception caught during addition of a constraints to CEE " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			status = false;
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void editAddOVF2CEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password)
	{
		int ceeId = -1;
		String actionType = null;
		int ovfId = -1;
		String ovfDescp = "";
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("request")) actionType = value;
			if(key.equalsIgnoreCase("ovfid")) ovfId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("ovfdesc")) ovfDescp = value;
		}
		if(actionType == null || (actionType != null && actionType.equalsIgnoreCase("loadovf")))
		{
			stringBuilder.append("<table style='width:1000px;font-family:Verdana;font-size:10pt;'>")
				.append("<tr><td style='width:750px;font-family:Verdana;font-size:10pt;'>");
			///////////////////////// OVF management pane goes here
			stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;padding:10px;'>");
			stringBuilder.append("Welcome to the OVF management console for CEE ").append(ceeId).append(". You can select an existing OVF from the drop-down list to modify ")
				.append("or you can add a new OVF document to your CEE.<hr>");
			stringBuilder.append("<table cellpadding='2' style='font-family:Verdana;font-size:9pt;background:#DEEEFF;border-width:1px;border-style:solid;'>")
				.append("<tr><th colspan='2' align='left'>If you wish to add a new OVF, leave the OVF drop-down list to <i>Not selected</i>, otherwise the chosen OVF entry will be updated. To add simply paste the OVF content in the box.");
			
			stringBuilder.append("<script type=\"text/javascript\">")
	        	.append("function submitForm() {")
	        	.append("document.forms['editovf'].elements['request'].value = 'loadovf';")
	        	.append("document.editovf.submit();")
	        	.append("} </script>");
			
			stringBuilder.append("<form name='editovf' method='post' action='../doaction/editaddovf2cee' style='font-family:Verdana;font-size:9pt;'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='request' value='commitovf'>")
				.append("<tr><td>Select an OVF entry to modify from the list: <td align='right'><select name='ovfid'><option value='-1'>Not selected</option>");
			String ovfContent = "";
			try
			{
				//finding the application id of default application
				ResultSet rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
				int appId = -1;
				if(rs.next()) appId = rs.getInt("id");
				rs.close();
				
				rs = db.query("select", "id, descp", "ovf", "WHERE applicationid=" + appId);
				while(rs.next())
				{
					int ovftempid = rs.getInt("id");
					if(ovftempid == ovfId)
						stringBuilder.append("<option value='").append(ovftempid).append("' selected>").append(rs.getInt("id")).append("</option>");
					else
						stringBuilder.append("<option value='").append(ovftempid).append("'>").append(rs.getInt("id")).append("</option>");
					if(ovfId != -1 && ovfId == ovftempid) ovfContent = rs.getString("descp");
				}
			}
			catch(Exception ex)
			{
				logger.info("Exception caught during generation of list of existing OVFs belonging to CEE " + ceeId);
				//if(logger.isDebugEnabled())
				//	ex.printStackTrace(System.err);
				logger.debug("Exception caught ", ex);
			}
			stringBuilder.append("</select> <a href='javascript:void(0);' onClick='Javascript:submitForm();'>load OVF</a>");
			stringBuilder.append("<tr><td colspan='2'><textarea name='ovfdesc' rows='30' cols='100'>");
			if(ovfId != -1) stringBuilder.append(ovfContent);
			stringBuilder.append("</textarea>");
			stringBuilder.append("<tr><td>Modify/Paste the contents of the OVF in the box above<td align='right'><input type='submit' value='commit changes'>");
			stringBuilder.append("</form></table></div>");
			stringBuilder.append("<td valign='top' style='font-family:Verdana;font-size:10pt;'>");
			///////////////////////// navigation buttons go here
			stringBuilder.append("<form name='goback' action='../doaction' method='post' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
			stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
			stringBuilder.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>");
			stringBuilder.append("<input type='hidden' name='requesttype' value='loadcee'>");
	        stringBuilder.append("<input type='submit' value='cancel and go back'></form>");
	        
			stringBuilder.append("</table>");
		}
		else
		{
			if(actionType.equalsIgnoreCase("commitovf"))
			{
				if(ovfDescp != null)
					ovfDescp = ovfDescp.trim();
				else
					ovfDescp = "";
				boolean status = true;
				String message = "";
				if(ovfDescp.length() == 0)
				{
					message += "Empty OVF detected, can not add/update. ";
					status = false;
				}
				else
				{
					if(ovfId == -1) //add a new OVF
					{
						try
						{
							ResultSet rs = db.query("select", "max(id)", "ovf", "");
	                        int id = 1;
	                        if(rs.next()) id = rs.getInt(1) + 1;
	                        rs.close();
	                        //find the application id of the default application this OVF gets add to
	                        rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
	                        int appId = -1;
	                        if(rs.next())
	                        {
	                        	appId = rs.getInt("id");
	                        }
	                        rs.close();
	                        status = db.insert("ovf", "(" + id + ", " + appId + ", '" + ovfDescp + "')");
						}
						catch(Exception ex)
						{
							logger.warn("Exception caught while adding a new ovf entry to the default application of the CEE " + ceeId);
							//if(logger.isDebugEnabled())
							//	ex.printStackTrace(System.err);
							logger.debug("Exception caught ", ex);
							status = false;
						}
					}
					else //update an existing OVF
					{
						try
    					{
    						status = db.update("ovf", "descp='" + ovfDescp + "'", "where id=" + ovfId);
    					}
    					catch(Exception ex)
    					{
    						logger.warn("Exception caught while updating an existing ovf (ovf id=" + ovfId + ").");
    						//if(logger.isDebugEnabled())
    						//	ex.printStackTrace(System.err);
    						logger.debug("Exception caught ", ex);
    						status = false;
    					}
					}
				}
				if(status)
				{
					stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
					stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='requesttype' value='loadcee'>")
    					.append("<input type='submit' value='proceed'></form>");
				}
				else
				{
					stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
    	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
    	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='requesttype' value='loadcee'>")
    					.append("<input type='submit' value='go back'></form>");
				}
			}
		}
	}
	
	private void addCEE(StringBuilder stringBuilder, Iterator<String> keyIter, Iterator<String> valIter, String username, String password, int userid)
	{
		String ceeName = "";
		String ceePerm = "";
		int resId = -1;
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("ceename")) ceeName = value;
			if(key.equalsIgnoreCase("ceeperm")) ceePerm = value;
			if(key.equalsIgnoreCase("reservationid"))
			{
				try
				{
					resId = Integer.parseInt(value);
				}
				catch(Exception ex)
				{
					resId = -1;
				}
			}
		}
		boolean status = true;
		String message = "";
		
		if(ceeName == null || (ceeName != null && ceeName.trim().length() == 0))
		{
			status = false;
			message += "You must provide a valid name to your constrained execution environment. ";
		}
		if(ceePerm == null || (ceePerm != null && ceePerm.trim().length() == 0)) ceePerm = "700";
		if(status)
		{
			//check and make sure that a CEE for this user does not exist with the same name
			try
			{
				ResultSet rs = db.query("select", "id, name", "cee", "WHERE userid=" + userid + " AND name='" + ceeName + "'");
				if(rs.next())
				{
					status = false;
					message += "A CEE with the same name already exists, please choose a new name and try again. ";
				}
			}
			catch(Exception ex)
			{
				status = false;
				message += "There was an exception caught while accessing the internal database. Please contact your administrator. ";
				logger.info("Exception caught while checking the list of existing CEEs");
				//if(logger.isDebugEnabled())
				//	ex.printStackTrace(System.err);
				logger.debug("Exception caught ", ex);
			}
			
			if(status)
			{
				//add this cee for this user
				try
				{
					ResultSet rs = db.query("select", "max(id)", "cee", "");
                    int ceeid = 1;
                    if(rs.next()) ceeid = rs.getInt(1) + 1;
                    rs.close();
                    rs = db.query("select", "max(id)", "application", "");
                    int appid = 1;
                    if(rs.next()) appid = rs.getInt(1) + 1;
                    rs.close();
                    
                    status = db.insert("cee", "(" + ceeid + ", '" + ceeName + "', " + userid + ", '" + ceePerm + "', " + resId + ", 'INI')");
                    //create a default application for this cee
                    status = db.insert("application", "(" + appid + ", 'default', " + ceeid + ")");
				}
				catch(Exception ex)
				{
					logger.warn("Exception caught while adding a new cee entry.");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
					status = false;
				}
			}
			
			if(status)
	        {
	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='submit' value='proceed'></form>");
	        }
	        else
	        {
	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='submit' value='proceed'></form>");
	        }
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='submit' value='proceed'></form>");
		}
	}
	
	private void doAction(StringBuilder stringBuilder, String username, String password, String requesttype, int userid, Form form, String action)
	{
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		if(action.equalsIgnoreCase("deletecee"))
		{
			deleteCEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("stopcee"))
		{
			stopCEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("deploycee"))
		{
			deployCEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("allocatecee"))
		{
			allocateCEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("initializecee"))
		{
			initializeCEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("removenethandler"))
		{
			removeNetHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("addvnethandler"))
		{
			addVNetHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("removestoragehandler"))
		{
			removeStorageHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("addstoragehandler"))
		{
			addStorageHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("removevmhandler"))
		{
			removeVMHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("addvmhandler"))
		{
			addVMHandler(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("removeconstraint"))
		{
			removeConstraint(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("addconstraint"))
		{
			addConstraint(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("editaddovf2cee"))
		{
			editAddOVF2CEE(stringBuilder, keyIter, valIter, username, password);
		}
		else if(action.equalsIgnoreCase("addcee"))
		{
			addCEE(stringBuilder, keyIter, valIter, username, password, userid);
		}
	}
	
	private void displayAppConsole(StringBuilder stringBuilder, String username, String password, String requesttype, int userid, Form form)
	{
		stringBuilder.append("Welcome <i><font color='green'>").append(username).append("</font></i> to VEP <b>Constrained Execution Environment</b> (CEE) Management Portal. ")
			.append("You can manage your deployed applications and update existing CEE elements from this page. If you are not sure, use the <i>default</i> CEE mapping rules for deployment!<br><hr>");
		stringBuilder.append("<table style='width:1000px;font-family:Verdana;font-size:10pt;'>")
			.append("<tr><td style='width:800px;font-family:Verdana;font-size:10pt;'>");
		///////////////////////// main CEE management elements go next
		stringBuilder.append("<table style='padding:0px;font-family:Verdana;font-size:9pt;'><tr>");
		stringBuilder.append("<td valign='top'><form name='new-cee' action='../doaction' method='post' style='font-family:Verdana;font-size:9pt;'>")
			.append("<input type='hidden' name='username' value='").append(username).append("'>")
			.append("<input type='hidden' name='password' value='").append(password).append("'>")
			.append("<input type='hidden' name='requesttype' value='").append("addcee").append("'>")
			.append("You can <input type='submit' value='add a new CEE'></form>");
		stringBuilder.append("<td valign='top'><form name='load-cee' action='../doaction' method='post' style='font-family:Verdana;font-size:9pt;'>")
			.append("<input type='hidden' name='username' value='").append(username).append("'>")
			.append("<input type='hidden' name='password' value='").append(password).append("'>")
			.append("<input type='hidden' name='requesttype' value='").append("loadcee").append("'>")
			.append("or you can select an existing CEE from this list <select name='ceeid'><option value='-1'>Not selected</option>");
		try
		{
			ResultSet rs = db.query("select", "id, name", "cee", "WHERE userid=" + userid);
			while(rs.next())
			{
				stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append(": ").append(rs.getString("name")).append("</option>");
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught during generation of list of existing CEEs");
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
		stringBuilder.append("</select><input type='submit' value='load this CEE'></form></table>");
		
		stringBuilder.append("<div style='width:780px;padding:10px;background:white;font-color:#265983;font-family:Verdana;font-size:10pt;text-align:left;'>");
		///////////// depending on the requesttype the content of this DIV changes
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		
		if(requesttype.equalsIgnoreCase("addcee"))
		{
			//generate the simple form to add an empty CEE for this user
			stringBuilder.append("<table style='background:#E0E2E4;font-family:Verdana;font-size:9pt;border-style:dashed;border-width:1px;'>");
			stringBuilder.append("<form name='addcee' action='../doaction/addcee' method='post' style='font-family:Verdana;font-size:9pt;'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='requesttype' value='").append("addcee").append("'>")
				.append("<tr><td>Give a name to your new CEE <td><input type='text' name='ceename' size='32'>")
				.append("<tr><td>Set the permissions (defaults to 700) <td><input type='text' name='ceeperm' size='3'>")
				.append("<tr><td style='width:400px;'>If you know a valid reservation ID to associate with this CEE please enter here (leave empty if you do not have one) <td valign='top'><input type='text' name='reservationid' size='32'>")
				.append("<tr><td><td align='right'><input type='submit' value='click to add this CEE'></form></table>");
		}
		else if(requesttype.equalsIgnoreCase("loadcee"))
		{
			int ceeId = -1;
			while(keyIter.hasNext() && valIter.hasNext())
			{
				String key = keyIter.next();
				String value = valIter.next();
				if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			}
			stringBuilder.append("\n<script type=\"text/javascript\">\n")
				.append("function clearContents(element) {\n")
				.append("element.value = '';\n")
				.append("}\n")
				.append("</script>\n");
			if(ceeId == -1)
			{
				stringBuilder.append("<p style='color:red;font-family:Verdana;font-size:9pt;'>You must first select a valid CEE to load.</p>");
			}
			else
			{
				
				stringBuilder.append("<table id='cee-panel' style='font-family:Verdana;font-size:9pt;border-style:dotted;border-width:0px;padding:0px;'>");
				stringBuilder.append("<tr>");
				stringBuilder.append("<td><form method='post' action='../doaction/editaddovf2cee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='edit/add OVF'></form>");
				stringBuilder.append("<td><form method='post' action='../doaction/initializecee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='initialize' title='this will initialize the CEE using default matching rules, new VM slots will be created as necessary'></form>");
				stringBuilder.append("<td><form method='post' action='../doaction/allocatecee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='allocate' title='this will allocate the VM slots using default rules for the CEE'></form>");
				stringBuilder.append("<td><form method='post' action='../doaction/deploycee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='deploy' title='this will deploy the CEE using default matching rules' onClick='alert(\"This may take some time. Please be patient!\"); startSpin();'></form>");
				stringBuilder.append("<td><form method='post' action='../doaction/stopcee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='stop' title='this will stop all running VMs and free their hosts' onClick='return confirm(\"Do you really want to stop this CEE?\");'></form>");
				stringBuilder.append("<td><form method='post' action='../doaction/deletecee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='delete' title='this will delete this CEE and free all resources' onClick='return confirm(\"Do you really want to delete this CEE?\");'></form>");
				stringBuilder.append("<td><form method='post' action='../constraints' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<input type='submit' value='manage constraints'> <b>controls for CEE ").append(ceeId).append("</b></form>");
				stringBuilder.append("</table>");
				
				stringBuilder.append("<script type='text/javascript'>\n")
				.append("var opts = {\n")
				.append("lines: 13, // The number of lines to draw\n")
				.append("length: 11, // The length of each line\n")
				.append("width: 4, // The line thickness\n")
				.append("radius: 10, // The radius of the inner circle\n")
				.append("corners: 1, // Corner roundness (0..1)\n")
				.append("rotate: 0, // The rotation offset\n")
				.append("color: '#000', // #rgb or #rrggbb\n")
				.append("speed: 1, // Rounds per second\n")
				.append("trail: 60, // Afterglow percentage\n")
				.append("shadow: false, // Whether to render a shadow\n")
				.append("hwaccel: false, // Whether to use hardware acceleration\n")
				.append("className: 'spinner', // The CSS class to assign to the spinner\n")
				.append("zIndex: 2e9, // The z-index (defaults to 2000000000)\n")
				.append("top: 'auto', // Top position relative to parent in px\n")
				.append("left: 'auto' // Left position relative to parent in px\n")
				.append("};\n")
				.append("function startSpin() {\n")
				.append("var target = document.getElementById('cee-panel');\n")
				.append("var spinner = new Spinner(opts).spin(target);\n")
				.append("}\n</script>\n");
				
				stringBuilder.append("<table cellspacing='0' cellpadding='1' style='width:770px;padding:0px;'><tr>");
				stringBuilder.append("<td valign='top' style='background:#E9C9B6;padding:5px;'><div style='background:#F8EADB;width:570px;height:400px;'>");
				//the current CEE contents are displayed here
				stringBuilder.append("<table cellspacing='1' style='font-family:Verdana;font-size:9pt;background:white;'>");
				stringBuilder.append("<tr><td>"); 
				
				stringBuilder.append("<div style='width:96px;height:300px;background:#FFD396;border-style:dashed;border-width:1px;border-color:#F77F5E;overflow-x:hidden;overflow-y:auto;'>");
				//the list of OVF apps goes here vertically
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'>");
				try
				{
					ResultSet rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
					int appId = -1;
					if(rs.next()) appId = rs.getInt("id");
					rs.close();
					
					rs = db.query("select", "*", "ovf", "WHERE applicationid=" + appId);
					int count = 0;
                    while(rs.next())
                    {
                    	String tooltip = "OVF ID: " + rs.getInt("id");
                    	try
                    	{
                    		OpenNebulaOVFParser ovfparse = new OpenNebulaOVFParser(rs.getString("descp"), "2.2.1", "");
                    		tooltip += "\nApplication Name: " + ovfparse.getApplicationName() + "\n---------------------------\nList of key elements:\n";
                    		String[] ovfIds = ovfparse.getIDs();
                    		for(int i=0; i<ovfIds.length; i++) tooltip += "OVFID: " + ovfIds[i] + "\n";
                    	}
                    	catch(Exception ex)
                    	{
                    		logger.info("Exception caught while parsing the OVFs for elements for CEE: " + ceeId);
        					//if(logger.isDebugEnabled())
        					//	ex.printStackTrace(System.err);
                    		logger.debug("Exception caught ", ex);
                    	}
                    	stringBuilder.append("<tr><td><form method='post' action='../doaction/editaddovf2cee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='ovfid' value='").append(rs.getInt("id")).append("'>")
	    					.append("<input type='hidden' name='request' value='loadovf'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cee-ovf.png' title='").append(tooltip).append("' width='75' height='75'></form>");
                    	count++;
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No OVF files found</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the OVFs for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("<td>");
				//the rest of the elements goes in this part of the table
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;'><tr><td>");
				
				stringBuilder.append("<div style='width:357px;height:95px;background:#FFE0B4;border-style:dashed;border-width:1px;border-color:#3E8BCA;overflow-x:auto;overflow-y:hidden;'>");
				//VM handlers elements goes here
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'><tr>");
				try
				{
					ResultSet rs = db.query("select", "*", "ceevmhandlers", "WHERE ceeid=" + ceeId);
					int count = 0;
                    while(rs.next())
                    {
                    	count++;
                    	String tooltip = "VM Handler Type ID: " + rs.getInt("vmhandlerid");
                    	
                    	int vmhandlerType = rs.getInt("vmhandlerid");
                    	int ceevmhandlerId = rs.getInt("id");
                    	tooltip += "\nCEE VM Handler ID: " + ceevmhandlerId;
                    	rs = db.query("select", "*", "vmhandler", "WHERE id=" + vmhandlerType);
                    	if(rs.next())
                    	{
                    		tooltip += "\nHandler Name: " + rs.getString("name") + "\n---------------------------------------\nCPU-Freq Range (in MHz): " + rs.getString("cpufreq_low") + "-" + rs.getString("cpufreq_high") + "\n";
                        	tooltip += "RAM Range (in MB): " + rs.getString("ram_low") + "-" + rs.getString("ram_high") + "\n";
                        	tooltip += "CPU Cores: " + rs.getString("corecount_low") + "-" + rs.getString("corecount_high") + "\n";
                        	tooltip += "Disk Space Range (in MB): " + rs.getString("disksize_low") + "-" + rs.getString("disksize_high");
                    	}
                    	
                    	stringBuilder.append("<td><form method='post' action='../doaction/removevmhandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='ceevmhandlerid' value='").append(ceevmhandlerId).append("'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-computer.png' title='").append(tooltip).append("' width='75' height='70' onClick='return confirm(\"Do you really want to delete?\");'></form>");
                    	/////resetting the sql object
                    	rs = db.query("select", "*", "ceevmhandlers", "WHERE ceeid=" + ceeId);
                    	int counter = 0;
                    	while(counter < count){ rs.next(); counter++; }
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No VM handlers found</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the VM handlers for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("<td rowspan='3'>"); 
				
				stringBuilder.append("<div style='width:96px;height:300px;background:#FFE5BF;border-style:dashed;border-width:1px;border-color:red;overflow-x:hidden;overflow-y:auto;'>");
				//constraints list goes here vertically
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'>");
				try
				{
					ResultSet rs = db.query("select", "*", "ceeconstraints", "WHERE ceeid=" + ceeId);
					int count = 0;
                    while(rs.next())
                    {
                    	count++;
                    	String tooltip = "Constraint Type: " + rs.getInt("constrainttype");
                    	int constraintType = rs.getInt("constrainttype");
                    	int constraintId = rs.getInt("id");
                    	rs = db.query("select", "descp", "constraints", "WHERE id=" + constraintType);
                    	if(rs.next()) tooltip += "\nConstraint: " + rs.getString("descp");
                    	
                    	stringBuilder.append("<tr><td><form method='post' action='../doaction/removeconstraint' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='constraintid' value='").append(constraintId).append("'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-constraint.png' title='").append(tooltip).append("' width='75' height='70' onClick='return confirm(\"Do you really want to delete?\");'></form>");
                    	/////resetting the sql object
                    	rs = db.query("select", "*", "ceeconstraints", "WHERE ceeid=" + ceeId);
                    	int counter = 0;
                    	while(counter < count){ rs.next(); counter++; }
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No constraints found</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the OVFs for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("<tr><td><div style='width:357px;height:95px;background:#FFE9C8;border-style:dashed;border-width:1px;border-color:#2B608C;overflow-x:auto;overflow-y:hidden;'>");
				//storage element goes here
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'><tr>");
				try
				{
					ResultSet rs = db.query("select", "*", "ceestoragehandlers", "WHERE ceeid=" + ceeId);
					int count = 0;
                    while(rs.next())
                    {
                    	count++;
                    	String tooltip = "Storage Handler Type ID: " + rs.getInt("storagehandlerid");
                    	int storageType = rs.getInt("storagehandlerid");
                    	int ceestoragehandlerId = rs.getInt("id");
                    	rs = db.query("select", "*", "storagehandler", "WHERE id=" + storageType);
                    	if(rs.next())
                    	{
                    		tooltip += "\nHandler Name: " + rs.getString("name");
                    	}
                    	
                    	stringBuilder.append("<td><form method='post' action='../doaction/removestoragehandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='ceestoragehandlerid' value='").append(ceestoragehandlerId).append("'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-storage.png' title='").append(tooltip).append("' width='75' height='70' onClick='return confirm(\"Do you really want to delete?\");'></form>");
                    	/////resetting the sql object
                    	rs = db.query("select", "*", "ceestoragehandlers", "WHERE ceeid=" + ceeId);
                    	int counter = 0;
                    	while(counter < count){ rs.next(); counter++; }
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No storage handlers found</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the storage handlers for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("<tr><td><div style='width:357px;height:95px;background:#FFEFD6;border-style:dashed;border-width:1px;border-color:#2B608C;overflow-x:auto;overflow-y:hidden;'>");
				//VNet elements goes here
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'><tr>");
				try
				{
					ResultSet rs = db.query("select", "*", "ceenethandlers", "WHERE ceeid=" + ceeId);
					int count = 0;
                    while(rs.next())
                    {
                    	count++;
                    	String tooltip = "Network Handler Type ID: " + rs.getInt("vnethandlerid");
                    	int networkType = rs.getInt("vnethandlerid");
                    	int ceenethandlerId = rs.getInt("id");
                    	rs = db.query("select", "*", "vnethandler", "WHERE id=" + networkType);
                    	if(rs.next())
                    	{
                    		tooltip += "\nNetwork Handler Name: " + rs.getString("name") + "\n";
                    		tooltip += "Public IP Support (1=Yes): " + rs.getInt("pubipsupport") + "\n";
                    		tooltip += "vLan Tag Support (1=Yes): " + rs.getInt("vlantagsupport") + "\n";
                    		tooltip += "DHCP Support (1=Yes): " + rs.getInt("dhcpsupport") + "\n";
                    	}
                    	
                    	stringBuilder.append("<td><form method='post' action='../doaction/removenethandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='ceenethandlerid' value='").append(ceenethandlerId).append("'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-network.png' title='").append(tooltip).append("' width='75' height='70' onClick='return confirm(\"Do you really want to delete?\");'></form>");
                    	/////resetting the sql object
                    	rs = db.query("select", "*", "ceenethandlers", "WHERE ceeid=" + ceeId);
                    	int counter = 0;
                    	while(counter < count){ rs.next(); counter++; }
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No network handlers found</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the network handlers for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("</table>");
				
				stringBuilder.append("<tr><td colspan='2'>");
				
				stringBuilder.append("<div style='width:564px;height:84px;background:#FAD4B8;border-style:dashed;border-width:1px;border-color:#F16A6A;overflow-x:auto;overflow-y:hidden;'>");
				//the VM slots goes here horizontally
				stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;' cellspacing='2' cellpadding='2'><tr>");
				try
				{
					ResultSet rs = db.query("select", "*", "vmslots", "WHERE ceeid=" + ceeId);
					int count = 0;
                    while(rs.next())
                    {
                    	count++;
                    	int vmslotId = rs.getInt("id");
                    	String tooltip = "VM Slot ID: " + vmslotId;
                    	String slotstatus = rs.getString("status");
                    	int hostId = rs.getInt("hostid");
                    	int vmHandlerId = rs.getInt("ceevmhandlerid");
                    	tooltip += "\nVM status: " + slotstatus + "\n";
                    	if(hostId == -1) tooltip += "Host: Not allocated yet.\n";
                    	else tooltip += "Host ID: " + hostId + "\n";
                    	tooltip += "Linked to CEE VM Hander ID: " + vmHandlerId + "\n----------------------------\nConstraints linked to this slot:\n";
                    	
                    	//getting the list of constraints associated with this VM
                    	rs = db.query("select", "*", "vmslotconstraint", "WHERE vmslotid=" + vmslotId);
                    	int count1=0;
                    	while(rs.next())
                    	{
                    		count1++;
                    		String param = rs.getString("param");
                    		int ceeConsListId = rs.getInt("ceeconstraintlistid");
                    		rs = db.query("select", "ceeconstraintsid", "ceeconstraintslist", "WHERE id=" + ceeConsListId);
                    		if(rs.next())
                    		{
                    			int ceeConsId = rs.getInt("ceeconstraintsid");
                    			rs = db.query("select", "constrainttype", "ceeconstraints", "WHERE id=" + ceeConsId);
                    			if(rs.next())
                    			{
                    				int consType = rs.getInt("constrainttype");
                    				rs = db.query("select", "descp", "constraints", "WHERE id=" + consType);
                    				if(rs.next())
                    				{
                    					String consDescp = rs.getString("descp");
                    					tooltip += consDescp + " " + param + "\n";
                    				}
                    			}
                    		}
                    		//resetting the SQL query
                    		rs = db.query("select", "*", "vmslotconstraint", "WHERE vmslotid=" + vmslotId);
                    		int counter = 0;
                    		while(counter < count1){ rs.next(); counter++; }
                    	}
                    	
                    	//getting the list of constraints associated with the attached vm handler on this VM
                    	rs = db.query("select", "*", "ceevmhandlerconstraint", "WHERE ceevmhandlerid=" + vmHandlerId);
                    	count1=0;
                    	while(rs.next())
                    	{
                    		count1++;
                    		String param = rs.getString("param");
                    		int ceeConsListId = rs.getInt("ceeconstraintlistid");
                    		rs = db.query("select", "ceeconstraintsid", "ceeconstraintslist", "WHERE id=" + ceeConsListId);
                    		if(rs.next())
                    		{
                    			int ceeConsId = rs.getInt("ceeconstraintsid");
                    			rs = db.query("select", "constrainttype", "ceeconstraints", "WHERE id=" + ceeConsId);
                    			if(rs.next())
                    			{
                    				int consType = rs.getInt("constrainttype");
                    				rs = db.query("select", "descp", "constraints", "WHERE id=" + consType);
                    				if(rs.next())
                    				{
                    					String consDescp = rs.getString("descp");
                    					tooltip += consDescp + " " + param + "\n";
                    				}
                    			}
                    		}
                    		//resetting the SQL query
                    		rs = db.query("select", "*", "ceevmhandlerconstraint", "WHERE ceevmhandlerid=" + vmHandlerId);
                    		int counter = 0;
                    		while(counter < count1){ rs.next(); counter++; }
                    	}
                    	
                    	stringBuilder.append("<td><form method='post' action='../appstatus/getvmstatus' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
	    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
	    					.append("<input type='hidden' name='vmslotid' value='").append(vmslotId).append("'>")
	    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-machine.png' title='").append(tooltip).append("' width='65' height='65' onClick='return confirm(\"Do you really want to navigate away from this page?\n You will be taken to the VM status page ...\");'></form>");
                    	//resetting the SQL query
                    	int counter = 0;
                    	rs = db.query("select", "*", "vmslots", "WHERE ceeid=" + ceeId);
                    	while(counter < count) { rs.next(); counter++; }
                    }
                    if(count == 0)
                    	stringBuilder.append("<tr><td><p style='font-family:Verdana;font-size:8pt;'>No VM slot found for this CEE</p>");
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while listing the VM slots for current CEE: " + ceeId);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				stringBuilder.append("</div>");
				
				stringBuilder.append("</table>");
				stringBuilder.append("</div><br>")
					.append("<form name='deploymentForm' method='post' action='../doaction/updatecee' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
					.append("<input type='hidden' name='username' value='").append(username).append("'>")
					.append("<input type='hidden' name='password' value='").append(password).append("'>")
					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
					.append("<textarea name='deploymentdoc' cols='68' rows='3' onfocus='clearContents(this);'>paste your deployment instructions here</textarea>")
					.append("<input type='submit' value='process and update'></form>");
				
				//////////////////list of available CEE elements goes next
				stringBuilder.append("<td valign='top'><div style='background:#F2E1CB;width:190px;font-family:Verdana;font-size:10pt;padding:5px;'><b><u>Available CEE elements</u></b>");
				stringBuilder.append("<table cellspacing='5' cellpadding='0' style='font-family:Verdana;font-size:9pt;'><tr><th colspan='3' align='left'>VM Handlers</th></tr>");
				try
				{
					int count = 0;
					ResultSet rs = db.query("select", "*", "vmhandler", "");
                    while(rs.next())
                    {
                    	int typeId = rs.getInt("id");
                    	String tooltip = "VM Handler Type: " + typeId + "\n";
                    	tooltip += "Handler Name: " + rs.getString("name") + "\n---------------------------------------\nCPU-Freq Range (in MHz): " + rs.getString("cpufreq_low") + "-" + rs.getString("cpufreq_high") + "\n";
                    	tooltip += "RAM Range (in MB): " + rs.getString("ram_low") + "-" + rs.getString("ram_high") + "\n";
                    	tooltip += "CPU Cores: " + rs.getString("corecount_low") + "-" + rs.getString("corecount_high") + "\n";
                    	tooltip += "Disk Space Range (in MB): " + rs.getString("disksize_low") + "-" + rs.getString("disksize_high");
                    	if(count%3 == 0) stringBuilder.append("<tr>");
                    	stringBuilder.append("<td><form method='post' action='../doaction/addvmhandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='vmhandlerid' value='").append(rs.getInt("id")).append("'>")
    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-computer.png' title='").append(tooltip).append("' width='50' height='45'></form>");
                    	count++;
                    }
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while adding icons for vm handlers");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				
				stringBuilder.append("<table cellspacing='5' cellpadding='0' style='font-family:Verdana;font-size:9pt;'><tr><th colspan='3' align='left'>Storage Handlers</th></tr>");
				try
				{
					int count = 0;
					ResultSet rs = db.query("select", "*", "storagehandler", "");
                    while(rs.next())
                    {
                    	String tooltip = rs.getString("name");
                    	if(count%3 == 0) stringBuilder.append("<tr>");
                    	stringBuilder.append("<td><form method='post' action='../doaction/addstoragehandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='storagehandlerid' value='").append(rs.getInt("id")).append("'>")
    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-storage.png' title='").append(tooltip).append("' width='50' height='45'></form>");
                    	count++;
                    }
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while adding icons for storage handlers");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				
				stringBuilder.append("<table cellspacing='5' cellpadding='0' style='font-family:Verdana;font-size:9pt;'><tr><th colspan='3' align='left'>VNet Handlers</th></tr>");
				try
				{
					int count = 0;
					ResultSet rs = db.query("select", "*", "vnethandler", "");
                    while(rs.next())
                    {
                    	String tooltip = rs.getString("name");
                    	if(count%3 == 0) stringBuilder.append("<tr>");
                    	stringBuilder.append("<td><form method='post' action='../doaction/addvnethandler' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='vnethandlerid' value='").append(rs.getInt("id")).append("'>")
    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-network.png' title='").append(tooltip).append("' width='50' height='45'></form>");
                    	count++;
                    }
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while adding icons for vnet handlers");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				
				stringBuilder.append("<table cellspacing='5' cellpadding='0' style='font-family:Verdana;font-size:9pt;'><tr><th colspan='3' align='left'>Cloud Constraints</th></tr>");
				try
				{
					int count = 0;
					ResultSet rs = db.query("select", "*", "constraints", "");
                    while(rs.next())
                    {
                    	String tooltip = rs.getString("descp");
                    	if(count%3 == 0) stringBuilder.append("<tr>");
                    	stringBuilder.append("<td><form method='post' action='../doaction/addconstraint' style='font-family:Verdana;font-size:9pt;padding:0px;'>")
    					.append("<input type='hidden' name='username' value='").append(username).append("'>")
    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    					.append("<input type='hidden' name='constraintid' value='").append(rs.getInt("id")).append("'>")
    					.append("<input type='image' src='http://"+webHost+":"+webPort+"/cloud-constraint.png' title='").append(tooltip).append("' width='50' height='45'></form>");
                    	count++;
                    }
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while adding icons for cloud constraints");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				stringBuilder.append("</table>");
				
				stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;text-align:left;'>Click on the listed icons above to add that element to your current CEE. Hover your mouse pointer over the element to see more details.</p>");
				stringBuilder.append("</div>");
				stringBuilder.append("</table>");
			}
		}
		stringBuilder.append("</div>");
		
		stringBuilder.append("<td valign='top' style='font-family:Verdana;font-size:10pt;'>");
		///////////////////////// navigation buttons go here
		stringBuilder.append("<form name='goback' action='../dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
		stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
        stringBuilder.append("<input type='submit' value='cancel and go back'></form>");
        
		stringBuilder.append("</table>");
	}
}
