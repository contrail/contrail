/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.ChallengeScheme;
import org.restlet.routing.Router;
import org.restlet.security.ChallengeAuthenticator;
import org.restlet.security.MapVerifier;

public class RestServerRouter extends Application
{
	@Override
    public synchronized Restlet createInboundRoot() 
    //public synchronized Restlet createRoot() 
    {
        // Create a router Restlet that routes each call to a new instance of HelloWorldResource.
        Router router = new Router(getContext());
        
        // Defines only one route
        MapVerifier verifier = new MapVerifier();
        verifier.getLocalSecrets().put("admin", "pass1234".toCharArray());
        ChallengeAuthenticator guard = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Interface Access");
        guard.setVerifier(verifier);
        router.attach("/", RestRootResource.class);
        
        //todelete
        //router.attach("/semen",Semen.class);
        
        router.attach("/newaccount", RestNewAccount.class);
        router.attach("/newaccount/{action}", RestNewAccount.class);
        router.attach("/dologin", RestUserDoLogin.class);
        router.attach("/dologin/{action}", RestUserDoLogin.class);
        router.attach("/doaction", RestUserDoAction.class);
        router.attach("/doaction/{action}", RestUserDoAction.class);
        router.attach("/constraints", RestUserManageConstraints.class);
        router.attach("/constraints/{action}", RestUserManageConstraints.class);
        router.attach("/appstatus", RestAppStatus.class);
        router.attach("/appstatus/getvmstatus", RestAppStatusVMStatus.class);
        router.attach("/appstatus/getvmstatus/{action}", RestAppStatusVMStatus.class);
        //router.attach("/api/cimi/", RestApiAuth.class);
        router.attach("/api/cimi/user", RestCIMIUserResource.class);
        router.attach("/api/cimi/user/{id}", RestCIMIUserResource.class);
        router.attach("/api/cimi/user/{id}/cees", RestCIMICEEResource.class);
        //router.attach("/api/cimi/user/cees", RestCIMICEEResource.class); TODO
        router.attach("/api/cimi/cee", RestCIMICEEResource.class);
        router.attach("/api/cimi/cee/{ceeid}", RestCIMICEEResource.class);
        router.attach("/api/cimi/cee/{ceeid}/constraints", RestCIMIConstraintResource.class);
        router.attach("/api/cimi/cee/{ceeid}/VMHandlers", RestCIMIVMHandlerResource.class);
        router.attach("/api/cimi/cee/{ceeid}/networkHandlers", RestCIMINetHandlerResource.class);
        router.attach("/api/cimi/cee/{ceeid}/storageHandlers", RestCIMIStorageHandlerResource.class);
        router.attach("/api/cimi/constraints", RestCIMIConstraintResource.class);
        router.attach("/api/cimi/VMHandlers", RestCIMIVMHandlerResource.class);
        router.attach("/api/cimi/networkHandlers", RestCIMINetHandlerResource.class);
        router.attach("/api/cimi/storageHandlers", RestCIMIStorageHandlerResource.class);
        router.attach("/api/cimi/constraints", RestCIMIConstraintResource.class);
        router.attach("/api/cimi/constraint/{cid}", RestCIMIConstraintResource.class);
        router.attach("/api/cimi/VMHandler/{vid}", RestCIMIVMHandlerResource.class);
        router.attach("/api/cimi/vmhandler/{vid}", RestCIMIVMHandlerResource.class);
        router.attach("/api/cimi/networkHandler/{nid}", RestCIMINetHandlerResource.class);
        router.attach("/api/cimi/storageHandler/{sid}", RestCIMIStorageHandlerResource.class);
        router.attach("/api/cimi/cee/{ceeid}/applications", RestCIMIApplicationResource.class);
        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}", RestCIMIApplicationResource.class);
        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}/vm/{vmid}", RestCIMIVMResource.class);
        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}/action/{action}", RestCIMIApplicationAction.class);
        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}/ovf/{ovfid}", RestCIMIOVFResource.class);
        //router.attach("/user/{id}/getCertificate",RestUserGetCertificate.class);
        //router.attach("/user/{id}/getCertificate/",RestUserGetCertificate.class);
        
        //        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}/VM/{vmid}", RestCIMIVMResource.class);
//        router.attach("/api/cimi/cee/{ceeid}/application/{applicationid}/VM/{vmid}/action/{action}", RestCIMIVMAction.class);
        //router.attach("/api/reservation/", RestApiReservation.class);
        //router.attach("/api/old/", RestApiOld.class);
        
        ChallengeAuthenticator guard1 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        ChallengeAuthenticator guard2 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        ChallengeAuthenticator guard3 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        ChallengeAuthenticator guard4 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        ChallengeAuthenticator guard5 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        MapVerifier verifier1 = new MapVerifier();
        verifier1.getLocalSecrets().put("admin", "pass1234".toCharArray());
        guard1.setVerifier(verifier1);
        guard2.setVerifier(verifier1);
        guard3.setVerifier(verifier1);
        guard4.setVerifier(verifier1);
        guard5.setVerifier(verifier1);
        router.attach("/admin/", guard1);
        guard1.setNext(RestAdminControl.class);
        router.attach("/admin/dologin", guard2);
        router.attach("/admin/dologin/{action}", guard2);
        guard2.setNext(RestAdminDoLogin.class);
        router.attach("/admin/doaction", guard3);
        router.attach("/admin/doaction/{action}", guard3);
        guard3.setNext(RestAdminDoAction.class);
        router.attach("/admin/managedatacenter", guard4);
        router.attach("/admin/managedatacenter/{action}", guard4);
        guard4.setNext(RestAdminManageDatacenter.class);
        router.attach("/admin/usermanagement", guard5);
        router.attach("/admin/usermanagement/{action}", guard5);
        guard5.setNext(RestAdminUserManagement.class);
        return router;
    }
}
