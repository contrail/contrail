/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.util.Series;
import org.apache.log4j.Logger;

public class RestServer implements Runnable
{
	private Component component;
    private int restPort;
    private int restHTTPSPort;
    private Thread t;
    private Logger logger;
    private boolean state;
    private boolean clientAuthEnabled;
    private String keyStoreFile;
    private String keyStorePass; //key for the JKS
    private String keyPass; //key for the server certificate
    private Server server;
    
    public RestServer(int port, int secureport, boolean clientAuth, String keyPath, String storePass, String key)
    {
        state = true;
        restPort = port;
        restHTTPSPort = secureport;
        clientAuthEnabled = clientAuth;
        keyStoreFile = keyPath;
        keyStorePass = storePass;
        keyPass = key;
        logger = Logger.getLogger("VEP.RestServer");
        component = new Component();
        component.getDefaultHost().attach(new RestServerRouter());
        
        server = null;
        try
        {
            if(restHTTPSPort != -1)
            {
                server = component.getServers().add(Protocol.HTTPS, restHTTPSPort);

                Series<Parameter> parameters = server.getContext().getParameters();

                parameters.add("sslContextFactory", "org.restlet.ext.ssl.PkixSslContextFactory");

                if(keyStoreFile != null)
                    parameters.add("keystorePath", keyStoreFile);
                else
                {
                    parameters.add("keystorePath", "restServer.jks"); //using default keystore value
                    logger.warn("REST server keystore file has not been specified. Trying default value.");
                }

                if(keyStorePass != null)
                    parameters.add("keystorePassword", keyStorePass);
                else
                {
                    parameters.add("keystorePassword", "pass1234");
                    logger.warn("REST server keystore password has not been specified. Trying default value.");
                }

                if(keyPass != null)
                    parameters.add("keyPassword", keyPass);
                else
                {
                    parameters.add("keyPassword", "pass1234");
                    logger.warn("REST server certificate password has not been specified. Trying default value.");
                }

                parameters.add("keystoreType", "JKS");
                if(clientAuthEnabled)
                {
                    parameters.add("needClientAuthentication", "true");
                    parameters.add("wantClientAuthentication", "false");
                }
                else
                {
                    parameters.add("needClientAuthentication", "false");
                    parameters.add("wantClientAuthentication", "true");
                }
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while trying to start the HTTPS REST server.");
            server = null;
            logger.debug("Exception Caught: ", ex);
            logger.fatal(ex.getMessage());
        }

        if(restPort != -1)
        {
            if(server != null)
                component.getServers().add(Protocol.HTTP, restPort);
            else
                server = component.getServers().add(Protocol.HTTP, restPort);
        }
        t = new Thread(this);
    }
    
    public void run() 
    {
        try
        {
            component.start();
            if(server != null)
                state = true;
            else
                state = false;
        }
        catch (Exception ex)
        {
            state = false;
            logger.error("REST server could not be started. Exception caught: " + ex.getMessage() + ".");
            logger.error("REST server could not be started. Try setting correct parameters under Edit -> Settings and try again.");
        }
    }
    
    public boolean getState()
    {
        return state;
    }
    
    public void start()
    {
        t.start();
        if(server != null)
            logger.trace("VEP REST server started.");
        else
            logger.trace("Problem starting VEP REST server.");
    }
    
    public void stop()
    {
        try
        {
        	//server.stop();
        	while(component.getServers().size() > 0)
        	{
        		try
        		{
        			component.getServers().get(0).stop();
        		}
        		catch(Exception ex)
        		{
        			
        		}
        		try
        		{
        			component.getServers().remove(0);
        		}
        		catch(Exception ex)
        		{
        			
        		}
        	}
            component.stop();
            logger.trace("VEP REST server stopped.");
        }
        catch (Exception ex)
        {
            logger.warn("Exception caught while stopping the Rest server. " + ex.toString());
        }
    }
}
