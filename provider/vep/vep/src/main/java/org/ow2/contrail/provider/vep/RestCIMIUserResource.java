/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.opensaml.ws.wssecurity.Username;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import org.ow2.contrail.provider.vep.objects.User;

public class RestCIMIUserResource extends ServerResource {

	//TODO: no authentication yet

	private Logger logger;
	private DBHandler db;
	private SSLCertHandler certHandler;

	public RestCIMIUserResource() {
		logger = Logger.getLogger("VEP.RestUserDoAction");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
		db = new DBHandler("RestUserDoAction", dbType);
		//        certHandler = new SSLCertHandler(true);
	}

	@Get("json")
	public Representation getValue() throws ResourceException
	{
		Representation response = null;
		JSONObject json = new JSONObject();
		ResultSet rs;
		//TODO: No Certificate hack for testing
		//		String userName = "admin";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		//retrieve Accept/Content-Type here if needed

		String uid = ((String) getRequest().getAttributes().get("id"));
		if (uid != null) {
			try {
				// returns data of a specific user
				json = CIMIParser.userRetrieve(userName, uid);

			} catch (SQLException e)
			{
				//TODO: add some errors to status and response if user does not exist
			}
		} else {
			//TODO:List all users - admin only
		}
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}

	@Put("json")
	public Representation storeValue(String value)
	{
		//TODO: No Certificate hack for testing
		//		String userName = "admin";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		Representation response = null;
		JSONObject jResponse = new JSONObject();
		String uid = ((String) getRequest().getAttributes().get("id"));
		if (uid != null) {
			this.setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
			// Update request
		} else {
			// Create request
			try {
				User user = CIMIParser.userCreate(userName, value);
				if ( user != null )
				{
					boolean success = user.storeUser();
					if (success) 
					{       
						user.storeUserOnCloud();
						this.setStatus(Status.SUCCESS_CREATED);                                            
					} else 
					{
						this.setStatus(Status.CLIENT_ERROR_CONFLICT);
						jResponse.put("error", "Could not register user into the database.");//check api if error message are possible 
					}
				} else {
					this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					jResponse.put("error", "JSON Parsing failed.");//check api if error message are possible
				}

			} catch (SQLException e)
			{
				//TODO: add some errors to status and response if user does not exist
			}

		}
		response = new StringRepresentation(jResponse.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}

	@Delete("json")
	public Representation deleteValue()
	{
		Representation response = null;

		return response;
	}





}
