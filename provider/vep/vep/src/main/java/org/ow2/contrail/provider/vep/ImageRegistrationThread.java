/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.IOException;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.jclouds.openstack.glance.v1_0.domain.ContainerFormat;
import org.jclouds.openstack.glance.v1_0.domain.DiskFormat;
import org.json.simple.parser.ParseException;

import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;


public class ImageRegistrationThread {
//extends Thread{

	private OpenStackConnector osc;
	private String name;
	private DiskFormat diskFormat;
	private String region;
	private ContainerFormat bare;
	private String imagePath;
	private Logger logger;
	private DBHandler db;
	private String imageName;
	private int cloudtypeId;
	private String ceeId;
	private int diskID;
	private String vmId;


	public ImageRegistrationThread(OpenStackConnector osc, String name,
			String imagePath, String region, DiskFormat diskFormat, String imageName, int cloudtypeId, 
			String ceeId, int diskID, String vmId, Logger logger, DBHandler db) {
				this.osc = osc;
				this.name = name;
				this.imagePath = imagePath;
				this.region = region;
				this.diskFormat = diskFormat;
				this.imageName = imageName;
				this.cloudtypeId = cloudtypeId;
				this.ceeId = ceeId;
				this.diskID = diskID;
				this.vmId = vmId;
				this.logger = logger;
				this.db = db;
	}

	public void run()
	{
		logger.debug("Started Image registration thread for a new OpenStack image");
                String iaasMapId=null;
                String status;
                int newOsDiskMapId = db.insertAndGetKey("osdiskmap (cloudtypeid,iaasname,osdiskid,iaasid,status) ", "("+cloudtypeId+",'"+(ceeId+"@"+imageName)+"',"+diskID+",'"+iaasMapId+"', '"+"LCK"+"')");
                db.update("vmslots", "osdiskmapid=" + newOsDiskMapId, "WHERE id=" + vmId);
        try {
            iaasMapId = osc.addImage(name, imagePath, region, diskFormat, ContainerFormat.BARE);
            status = osc.imageStatus(iaasMapId, region);
        } catch (Exception ex) {
           status="err";
        }
		
		if(status.equals("UNK")||status.equals("err"))
		{
			logger.error("Could not add image name:"+imageName);
                        db.delete("osdiskmap", "where id="+newOsDiskMapId);
                        db.update("vmslots", "osdiskmapid=null", "WHERE id=" + vmId);
		}
		else
		{ 
                        db.update("osdiskmap", "iaasid='"+iaasMapId+"'","where id="+newOsDiskMapId);
                        db.update("osdiskmap","status='"+status+"'","where id="+newOsDiskMapId);
			//int newOsDiskMapId = db.insertAndGetKey("osdiskmap (cloudtypeid,iaasname,osdiskid,iaasid,status) ", "("+cloudtypeId+",'"+(ceeId+"@"+imageName)+"',"+diskID+",'"+iaasMapId+"', '"+"UP"+"')");
			//now link this diskmap entry to this osdiskmap entry
			//
		}
	}
}
