/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Disposition;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author fgaudenz
 */
public class RestUserGetCertificate extends ServerResource {

    private Logger logger;
    private DBHandler db;

    public RestUserGetCertificate() {
        logger = Logger.getLogger("VEP.RestGetCertificate");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestNewAccount", dbType);
    }

    @Post
    public Representation getResult(Representation entity) throws ResourceException {
        
        
        
                Form form = new Form(entity);
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String userName = "";
		String password = "";
		String requesttype = "";
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			if(key.equalsIgnoreCase("username")) userName = value;
			if(key.equalsIgnoreCase("password")) password = value;
			if(key.equalsIgnoreCase("requesttype")) requesttype = value;
		}
		String uid = ((String) getRequest().getAttributes().get("id"));
		//first verify whether the username and password are correct, then proceed for certificate verification
		boolean auth = false;
		int userId = -1;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
                        auth= false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where id='" + uid + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        		{
	        			userId = rs.getInt("id");
	        			auth = true;
	        		}
	        		else
                                        auth = false;
	        	}
	        	else
	        	{
	        		auth = false;
	        	}
                          rs.close();
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception caught ", ex);
        	auth = false;
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
        
      
               
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
 
        
        
        
        

       
        
        
        
        if (auth) {
            logger.info("Certificate download request received for user: " + userName);
            try {
                String scratchSpace = VEPHelperMethods.getProperty("vep.scratch", logger);
                if (!scratchSpace.endsWith(System.getProperty("file.separator"))) {
                    scratchSpace += System.getProperty("file.separator");
                }
                File vepScratch = new File(scratchSpace);
                File toSend = new File(scratchSpace + userName + ".pfx");
                if(toSend.exists()){
                FileRepresentation rep = new FileRepresentation(toSend, MediaType.APPLICATION_ALL);
                Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
                disp.setFilename(toSend.getName());
                disp.setSize(toSend.length());
                rep.setDisposition(disp);
                //toSend.delete();
                return rep;
                }else{
                    return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                }
            } catch (Exception ex) {
                logger.warn("Exception caught while sending the certificate file.");
                //if(logger.isDebugEnabled())
                //	ex.printStackTrace(System.err);
                logger.debug("Exception Caught: ", ex);
                return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
            }

        } else {
            setStatus(Status.CLIENT_ERROR_FORBIDDEN);
            return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
        }

      
    }
}
