/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import eu.contrail.security.SecurityCommons;
import java.security.PrivateKey;

import org.ow2.contrail.provider.vep.OAuthClient.OAuthClientFactory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import org.ow2.contrail.provider.vep.objects.VEPVirtualMachine;
import java.security.cert.X509Certificate;
import org.ow2.contrail.common.oauth.client.KeyAndCertificate;


public class CEEDeploymentThread extends Thread {
	
	private static final int periodicity = 10000;
	private ArrayList<Integer> vmSlotsList; //if deploying the vms with infos retrieved from the db (for the gui)
	private ArrayList<VEPVirtualMachine> vms; //if deploying the vms with infos retrieved from the objects (for the rest api)
	private HashMap<Integer, VEPVirtualMachine> vmMap; //if using VEPVM objects, this map is here to quickly retrieve the object's from their id (faster than searching the collection)
	private boolean fromdb = true; // differentiate between the two cases above as only used at the same time withtout checking the == null of one of the two each time
	private Logger logger;
	private DBHandler db;
	public String username;
	public int userid = 0;
	
	public CEEDeploymentThread(ArrayList<Integer> vmSlotsList, String username)
	{
		this();
		this.vmSlotsList = vmSlotsList;
		this.username = username;
	}
	
	public CEEDeploymentThread(VEPVirtualMachine vm)
	{
		this();
		ArrayList<VEPVirtualMachine> vms = new ArrayList<VEPVirtualMachine>();
		vms.add(vm);
		this.vms = vms;
        fromdb = false;
	}
	
	public CEEDeploymentThread(ArrayList<VEPVirtualMachine> vms)
	{
		this();
		this.vms = vms;
        fromdb = false;
	}
	public CEEDeploymentThread()
	{
		logger = Logger.getLogger("VEP.CEEDeployment");
		String dbType = VEPHelperMethods.getProperty("vepdbVEPHelper.choice", logger);
        db = new DBHandler("CEEDeployment", dbType);
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	/**
	 * Generates a template for a VM Slot
	 * @param slotId id of the VM Slot
	 * @param username Username of the user owning this VMSlot
	 * @return A Map object containing [headip (string), headport(int), username(string), password(string), typeId(int), cloudVersion(string)]
	 */
	private HashMap<String, HashMap<String, Object>> generateTemplate(int slotId, String username)
	{
		//System.err.println("Trying to generate VM template for slotId: " + slotId);
		HashMap<String,HashMap<String, Object>> maps = new HashMap<String, HashMap<String,Object>>(); // contains both below maps
		HashMap<String, Object> params = new HashMap<String, Object>();
		HashMap<String, Object> cloudSpecs = new HashMap<String, Object>();

		params.put("vmslotId",Integer.toString(slotId));
		params.put("uuid",username);
		try{

			//certificate creation
			//this fails when OAuth returns an invalid request HTTP 400 (like unknown resource owner)
			//should either loop over it until it succeeds, or exists gracefully
			//temporary fix: no oAuth
			//TODO:uncomment for production use
			OAuthClientFactory oauth=new OAuthClientFactory(username);
			if(oauth.isActive()){
               KeyAndCertificate certs=oauth.getDelegateCertificate();
               X509Certificate cert=certs.getCertificate();
               PrivateKey pk=certs.getPrivateKey();
                params.put("cert", cert);
                params.put("privateKey",pk);
                }else{
                    params.put("cert",null);
                }
			
			//end certificate creation

			String template = "";	
			ResultSet rs = db.query("select", "*", "vmslots", "where id="+slotId);
			if(rs.next())
			{
				String ceevmhandlerid = String.valueOf(rs.getInt("ceevmhandlerid"));
				//String iaasid = rs.getString("iaasid");
				String osdiskmapid = String.valueOf(rs.getInt("osdiskmapid"));
				String ovfmapid = String.valueOf(rs.getInt("ovfmapid"));
				if (!fromdb) {
					logger.debug("retrieving vm values from object");
					VEPVirtualMachine v = vmMap.get(slotId);
					//Fill params with : vcpu, ram, cores, diskSize, name, context
					int vcpu = v.getCpufreq();
					int ram = v.getRam();
					int cores = v.getCorecount();
					String name = v.getName();
					String context = v.getContextString();
					params.put("vcpu", vcpu);
					params.put("ram", ram);
					params.put("cores", cores);
					params.put("name", name);
					params.put("context", context);
					//As all VMs spawned from the API share the same physical network, with VIN on top, we need the cloudtypeid of the cloud where the VM will be registered.
					//This id is retrieved at the osdisk retrieval step, so we fill network at this point.
					
					
				} else {
					//retrieve cpu/ram/cores
					rs = db.query("select", "vmhandlerid", "ceevmhandlers", "where id="+ceevmhandlerid);
					if(rs.next())
					{
						int vmhandlerid = rs.getInt("vmhandlerid");
						rs = db.query("select", "*", "vmhandler", "where id="+vmhandlerid);
						if(rs.next())
						{
							int vcpu = rs.getInt("cpufreq_low");
							params.put("vcpu", vcpu);
							int ram = rs.getInt("ram_low");
							params.put("ram", ram);
							int cores = rs.getInt("corecount_low");
							params.put("cores", cores);
							int diskSize = rs.getInt("disksize_low");
							params.put("diskSize", diskSize);
						}
					}
				
					//retrieve ovfmap infos
					rs = db.query("select","*","ovfmap","where id="+ovfmapid);
					if(rs.next())
					{
						String vmName = rs.getString("ovfid");
						params.put("name", vmName);
						String context = rs.getString("context");
						params.put("context", context);
					}
					
					//retrieve networks as a List of Maps("name:value", "ip:[dhcp||address]")
					ArrayList<HashMap<String, String>> networks = new ArrayList<HashMap<String,String>>(); 
					rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid); 
					int count = 0;
					while(rs.next())
					{
						count++;
						HashMap<String, String> network = new HashMap<String, String>();
						int ceenethandlerid = rs.getInt("ceenethandlerid");
						rs.close();
						rs = db.query("select", "vnethandlerid", "ceenethandlers", "where id="+ceenethandlerid);
						if(rs.next())
						{
							int vnethandlerid = rs.getInt("vnethandlerid");
							rs = db.query("select", "cloudnetworkid", "vnethandler", "where id="+vnethandlerid);
							if(rs.next())
							{
								int cloudnetworkid = rs.getInt("cloudnetworkid");
								rs = db.query("select", "*", "cloudnetwork", "where id="+cloudnetworkid);
								if(rs.next())
								{
									String name = rs.getString("name");
									network.put("name", name);
									network.put("ip", "dhcp");// /!\hard coded dhcp
									networks.add(network);
								}
							}
						}
						//now resetting the sql query back
						rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid);
						int counter = 0;
						while(counter < count) { rs.next(); counter++; }
					}	
					params.put("networks", networks);
				}
				//retrieve OSDisk Name as a Map<"name:value"> (other informations to be added later on if necessary)
				rs = db.query("select", "*", "osdiskmap", "where id="+osdiskmapid);
				int typeId = 0;
				String cloudVersion = null;
				if(rs.next())
				{
					HashMap<String, String> osDisk = new HashMap<String, String>();
					String iaasDiskName = rs.getString("iaasname");
					osDisk.put("name", iaasDiskName);
					//Next two lines needed for OS
					String iaasDiskId = rs.getString("iaasid");
					osDisk.put("id", iaasDiskId);
					params.put("osDisk", osDisk);
					int cloudtypeid = rs.getInt("cloudtypeid");
				
					//fill networks if !fromdb
					if(!fromdb)
					{
						//retrieve networks as a List of Maps("name:value", "ip:[dhcp||address]")
						ArrayList<HashMap<String, String>> networks = new ArrayList<HashMap<String,String>>(); 
						rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid); 
						int count = 0;
						while(rs.next())
						{
							count++;
							HashMap<String, String> network = new HashMap<String, String>();
							int ceenethandlerid = rs.getInt("ceenethandlerid");
							rs.close();
							rs = db.query("select", "vnethandlerid", "ceenethandlers", "where id="+ceenethandlerid);
							if(rs.next())
							{
								int vnethandlerid = rs.getInt("vnethandlerid");
								rs = db.query("select", "cloudnetworkid", "vnethandler", "where id="+vnethandlerid);
								if(rs.next())
								{
									int cloudnetworkid = rs.getInt("cloudnetworkid");
									//cloudtypeid must conform to the one or the osdisk
									rs = db.query("select", "*", "cloudnetwork", "where id="+cloudnetworkid+" and cloudtypeid="+cloudtypeid);
									if(rs.next())
									{
										String name = rs.getString("name");
										network.put("name", name);
										network.put("ip", "dhcp");// /!\hard coded dhcp
										networks.add(network);
									}
								}
							}
							//now resetting the sql query back
							rs = db.query("select", "ceenethandlerid", "ceenethandlerlist", "where ovfmapid="+ovfmapid);
							int counter = 0;
							while(counter < count) { rs.next(); counter++; }
						}	
						params.put("networks", networks);
					}
					
					//retrieve name of cloud provider and version
					rs = db.query("select", "typeid,version,headip,headport", "cloudtype", "where id="+cloudtypeid);
					if(rs.next())
					{
						typeId = rs.getInt("typeid");
						cloudVersion = rs.getString("version");
						if(typeId == App.OPENNEBULA3_HOST)
						{
							cloudSpecs.put("headip", rs.getString("headip"));
							cloudSpecs.put("headport", rs.getInt("headport"));
						} else if (typeId == App.OPENSTACK_HOST)
							//some OpenStack specific data needed to deploy on a OpenStack cloud
						{
							rs = db.query("select", "*", "openstackcloud", "where id="+cloudtypeid);
							if(rs.next())
							{
								cloudSpecs.put("keystoneadminip", rs.getString("keystoneadminip"));
								cloudSpecs.put("keystoneadminport", rs.getString("keystoneadminport"));
								cloudSpecs.put("keystoneuserip", rs.getString("keystoneuserip"));
								cloudSpecs.put("keystoneuserport", rs.getString("keystoneuserport"));
								cloudSpecs.put("region", rs.getString("region"));
								//next two lines may not be used
								cloudSpecs.put("tenantid", rs.getString("tenantid"));
								cloudSpecs.put("tenantname", rs.getString("tenantname"));
								
							}
						}
					}
	
					if(userid  == 0)
					{
						//retrieve vep user's id from username
						logger.debug("Using VEP Username: " + username);
						
						rs = db.query("select", "id", "user", "where username=\""+username+"\"");
						if(rs.next())
						{
							userid = rs.getInt("id");
						}
					} 
					if(userid != 0)
					{
							
						//retrieve IaaS UserName
						rs = db.query("select", "iaasuser,iaaspass", "accountmap", "where vepuser="+userid);
						try 
						{
							if(rs.next())
							{
								String iaasuser = rs.getString("iaasuser");
								String iaaspass = rs.getString("iaaspass");
								params.put("username", iaasuser);
								cloudSpecs.put("username", iaasuser);
								cloudSpecs.put("password", iaaspass);
							}
						} 
						catch(SQLException ex)
						{
							logger.info("No account linked to this user on the predefined cloud");
							//if(logger.isDebugEnabled())
							//	ex.printStackTrace(System.err);
							logger.debug("Exception caught ", ex);
						}
					}
					//retrieve admin user name on the cloud to id resources
			        rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudtypeid+" and accountmap.vepuser=user.id");
			        String adminIaaSUname = "";
			        if(rs.next())
			        {
			        	adminIaaSUname=rs.getString("iaasuser");
			        	params.put("adminIaaSUname", adminIaaSUname);
			        	//OpenStack needs admin credentials for the connector
			        	cloudSpecs.put("adminIaaSUname", adminIaaSUname);
			        	cloudSpecs.put("adminIaaSPassword", rs.getString("iaaspass"));
			        }
			        rs.close();
				}
				
				//temp hack for ovfId and ovfVSId, may later be used to retrieve context from the generator
				params.put("ovfId", "myOvfId");
				params.put("ovfVSId", "myOvfVSId");
				
				
				boolean success = false;
				//Generate template
				//this should only be useful for OpenNebula
				TemplateGenerator tg = new TemplateGenerator(typeId, cloudVersion);
				switch(typeId){
					case App.OPENNEBULA3_HOST:
						template = tg.getTemplate(params);
						if(!template.equals(""))
						{
							db.updateTemplate(slotId, template);
							success = true;
						}
						maps.put("params", null);
						break;
					case App.OPENSTACK_HOST:
						if(params.get("cert")!=null)
						{
							X509Certificate pubk = (X509Certificate)params.get("cert");
							PrivateKey pk = (PrivateKey)params.get("privateKey");
							
							ByteArrayOutputStream pri = new ByteArrayOutputStream();
							ByteArrayOutputStream pub = new ByteArrayOutputStream();
							SecurityCommons sc = new SecurityCommons();
							
							sc.writeKey(pri, pk);
							sc.writeCertificate(pub, pubk);
							
							String privateKey = pri.toString();
							String publicKey = pub.toString();
							
							params.put("privateKey", privateKey);
							params.put("publicKey", publicKey);
						}
						//db.updateTemplate(slotId, (String)params.get("context"));
						maps.put("params",params);
						success = true;
						break;
					default:
						break;
				}
									
				if(success)
				{
					cloudSpecs.put("typeId", typeId);
					cloudSpecs.put("cloudVersion", cloudVersion);
					maps.put("cloudSpecs", cloudSpecs);
					return maps;
				}
				else	
					return null;
			}
		}
		catch(Exception ex)
		{
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
                        
			return null;
		}
		return null;
	}
	
	/**
	 * Checks the status of all Images used by the scheduled VMslots of the cee, when all of them are ready, deploy. 
	 * If one of them is in error, cancel deployment and put VMSlots in ERR state
	 */
	public void run(){
		try
		{
			//If using the objects, fill vmSlotsList with the id of each VEPVirtualMachine object from vms
			// generate vmSlots for retrocompatibility with older code
			if(!fromdb)
			{
				vmSlotsList = new ArrayList<Integer>();
				vmMap = new HashMap<Integer, VEPVirtualMachine>();
				for(VEPVirtualMachine v : vms)
				{
					vmSlotsList.add(Integer.parseInt(v.getVmId()));
					vmMap.put(Integer.parseInt(v.getVmId()), v);
				}
			}
			
			//set vmSlot statuses to DEP
			for(int vmSlot : vmSlotsList)
			{
				db.update("vmslots", "status='DEP'", "where id="+vmSlot);
			}
		
			//now test images readiness
			boolean error = false;
			boolean status = false; 
			while(!error && !status)
			{
				for(int vmSlot : vmSlotsList)
				{
					ResultSet rs = db.query("select", "*", "vmslots", "WHERE id=" + vmSlot);
					if(rs.next())
					{
						int osdiskmapid = rs.getInt("osdiskmapid");
						if(osdiskmapid != 0)
						{
							rs.close();
							rs = db.query("select", "status", "osdiskmap", "WHERE id=" + osdiskmapid);
							if(rs.next())
							{
								String imState = rs.getString("status");//throws an exception if there is no result
								logger.debug("Image status check for vmSlot id:"+vmSlot+" is:"+imState);
								if(imState.equals("ERR"))
								{
									//exit loop, something went wrong
									error = true;
									status = false;
									logger.error("Image for vmSlot "+vmSlot+" is on error state. Cancelling deployment.");
									break;
								} else if(imState.equals("UNK"))
								{
									//exit loop, something went wrong
									error = true;
									status = false;
									logger.error("Image for vmSlot "+vmSlot+" is on unknown state, provider may be down. Cancelling deployment.");
									break;
								} else if(imState.equals("LCK"))
								{
									//sleep before retry
									logger.debug("Image for vmSlot "+vmSlot+" is on locked state, retrying in " + periodicity/1000 + " seconds");
									try {
										Thread.sleep(periodicity);
									} catch (InterruptedException e) {}
									status = false;
									break;
								} else {
									//image ready for this vmSlot, try the next one
									status = true;
								}
							}
						} else {
							logger.debug("No registered osdiskmap entry yet, retrying in "+periodicity/1000+ " second(s).");
							try {
								Thread.sleep(periodicity);
							} catch (InterruptedException e) {}
							status = false;
							break;
						}
					}
				}
			}
			
			//now deploys if error != true (happens if one of the images got in err or unk state) 
			HashMap<String, Object> cloudSpecs;
			if(!error)
			{
				for(int vmSlot : vmSlotsList)
				{
					ResultSet rs = db.query("select", "*", "vmslots", "WHERE id=" + vmSlot);
					int slotId = rs.getInt("id");
					int hostId = rs.getInt("hostid");
					//get iaad host id
					rs = db.query("select", "iaasid,hostname", "host", "where id="+hostId);
					if(rs.next())
					{
						int iaasHostId = rs.getInt("iaasid");
						String hostname = rs.getString("hostname");
						// application can now be deployed
						//first generate and fill the template column of the vmSlot entry
						HashMap<String, HashMap<String, Object>> maps = generateTemplate(slotId, username);
						cloudSpecs = maps.get("cloudSpecs");
						HashMap<String, Object> params = maps.get("params");
						rs = db.query("select", "template", "vmslots", "where id="+slotId);
						if(rs.next())
						{
							String template = rs.getString("template");
							try 
							{
								//deployment
								if(cloudSpecs != null)
								{
									int typeId = (Integer) cloudSpecs.get("typeId");
									TemplateDeployer td = null;
									switch(typeId){
									case App.OPENNEBULA3_HOST:
										td = new TemplateDeployer(template, iaasHostId, cloudSpecs);
										if(td == null) throw new Exception("Could not create TemplateDeployer");
										int vmid = (Integer) td.deployTemplate();
										status = db.update("vmslots", "iaasid="+vmid, "WHERE id=" + slotId);
										logger.info("Successfully deployed vm n° "+vmid+" from vmSlot "+slotId+" on host "+hostId+".");
										break;
									case App.OPENSTACK_HOST:
										td = new TemplateDeployer(hostname, cloudSpecs, params);
										if(td == null) throw new Exception("Could not create TemplateDeployer");
										String openstackid = (String) td.deployTemplate();
										status = db.update("vmslots", "iaasid='"+openstackid+"'", "WHERE id=" + slotId);
										logger.info("Successfully deployed vm n° "+openstackid+" from vmSlot "+slotId+" on host "+hostId+".");
										break;
									}
								}
								else
								{
									logger.info("Error deploying vmSlot " + slotId + " on host " + hostId + ".");
									status = db.update("vmslots", "iaasid='-1', status='ERR'", "WHERE id=" + slotId);
									status = false;
								}
								//in periodic update thread -- connection info should be updated
							}
							catch(Exception e)
							{
								logger.info("Exception caught while deploying template from slot "+slotId+" on host "+hostId+". Message is: "+e.getMessage());
								status = false;
								logger.debug("Exception caught ", e);
								break;
							}
						}
					}
				}	
			} else {
				//an error happened, switch vmSlots statuses to ERR
				for(int vmSlot : vmSlotsList)
				{
					db.update("vmslots", "status='ERR'", "where id="+vmSlot);
				}
			}
		} catch (SQLException e)
		{
			logger.error("SQL exception during Deployment", e);
		}
		logger.debug("CEEDeployment end.");
	}
}
