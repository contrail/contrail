/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import org.ow2.contrail.provider.vep.objects.CEE;
import org.ow2.contrail.provider.vep.objects.User;
import org.ow2.contrail.provider.vep.objects.VEPVirtualMachine;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import org.apache.commons.lang.RandomStringUtils;

public class RestCIMICEEResource extends ServerResource {

    private Logger logger;
    private DBHandler db;

    public RestCIMICEEResource() {
        logger = Logger.getLogger("VEP.CEEResource");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestCEEResource", dbType);
    }

    @Post("json")
    public Representation getResult(String value) throws ResourceException {
        //TODO: No Certificate hack for testing
//		String userName = "admin";


        Representation response = null;
        JSONObject jResponse = new JSONObject();
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String userName = requestHeaders.getFirstValue("X-Username");


        //Authentication
        boolean auth = false;
        String requestor = VEPHelperMethods.checkCert((List) getRequest().getAttributes().get("org.restlet.https.clientCertificates"));
        if (requestor.equalsIgnoreCase(userName)) {
            ResultSet rs = db.query("select", "id", "user", "where username='" + requestor + "'");
            try {

                if (rs.next()) {
                    auth = true;
                }
                rs.close();
            } catch (SQLException ex) {
                logger.error("error in user creation for" + userName);
                //return not allowed

            }

        } else {
            String role = "";
            ResultSet rs = db.query("select", "role", "user", "where username='" + requestor + "'");
            try {

                if (rs.next()) {
                    role = rs.getString("role");
                }
                rs.close();
                if (role.equalsIgnoreCase("administrator")) {
                    auth=true;
                    rs = db.query("select", "id,role", "user", "where username='" + userName + "'");
               
                        if (!rs.next()) {

                            rs.close();
                            //create the user!
                            //vep user
                            User usr = new User(true);
                            usr.setUsername(userName);
                            usr.setPassword("pass1234");
                            usr.setEmail(userName + "@contrail.eu");
                            usr.setRole("user");
                            usr.storeUser();
                            usr.storeUserOnCloud();
                            //cloud user
                            
                            
                            
                            
                            
                        }
                }
            } catch (SQLException ex) {
                logger.error("error in user creation for" + userName);
                //return not allowed

            }
        }
       if(auth){
           
       }else{
           setStatus(Status.CLIENT_ERROR_FORBIDDEN);
           return new StringRepresentation("{}",MediaType.APPLICATION_JSON);
       }

        String cid = ((String) getRequest().getAttributes().get("ceeid"));        
        //CEE Creation
        try {
            //TODO:Remove test code
//			StringWriter stw  = new StringWriter();
//			try {
//				IOUtils.copy(new FileInputStream(new File("/home/fdudouet/Documents/TestJSONVEP-API/test1VM.ovf")), stw);
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			String ovf = stw.toString();
//			String ovf3 = StringEscapeUtils.escapeJava(ovf);
//			String ovf4 = StringEscapeUtils.escapeJavaScript(ovf3);
//			value = value.replaceAll("ovfovfovfovf",StringEscapeUtils.escapeJavaScript(StringEscapeUtils.escapeJavaScript(ovf))); 
            //End test code

            //tell people to use StringEscapeUtils.escapeJavaScript(ovf) to put the ovf inside the json
            CEE c=null;
            try{
            c = CIMIParser.ceeCreate(userName, value, cid);
            }catch(UnauthorizedRestAccessException e){
                setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
            }
            
            if (c.isError()) {
                this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                logger.debug("CEE initialization failed, error is: " + c.getError());
            } else {
                c.checkParameters();
                if (c.isError()) {
                    this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                    logger.error("Check parameters failed, error is: " + c.getError());
                } else {
                    if (c.getStatus().equalsIgnoreCase("check")) {
                        HashMap<String, Object> check = c.checkSchedulerReservation();
                        c.checkStorage();
                        if (c.isError()) {
                            this.setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
                            jResponse.put("error", "error during the check: " + c.getError());
                        } else if (check.containsKey("reservation") && check.containsKey("vmList")) {
                            jResponse = CIMIParser.parseCheckResponse((Boolean) check.get("reservation"), (ArrayList<VEPVirtualMachine>) check.get("vmList"));
                            this.setStatus(Status.SUCCESS_OK);
                        } else {
                            this.setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
                            jResponse.put("error", "error during the check");
                        }
                    } else {
                        c.checkCEErules();
                        if (!c.isError()) {
                            HashMap<String, Object> reservationResponse = c.registerToDb();
                            if(c.isError()){
                                this.setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
                                jResponse.put("error", "error during the cee creation: " + c.getError());
                                logger.debug("Error is: " + c.getError());
                            }else{
                            if (reservationResponse.containsKey("reservation") && reservationResponse.containsKey("vmList")) {
                                jResponse = CIMIParser.parseActiveResponse((Boolean) reservationResponse.get("reservation"), (ArrayList<VEPVirtualMachine>) reservationResponse.get("vmList"));
                                //this.setStatus(Status.SUCCESS_OK);
                            }
                            jResponse.put("id", c.getId());
                            this.setStatus(Status.SUCCESS_CREATED);
                            }
                        } else {
                            this.setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
                            jResponse.put("error", "error during the cee creation: " + c.getError());
                            logger.debug("Error is: " + c.getError());
                        }
                    }
                }
            }
        } catch (SQLException e) {
            logger.debug("SQLException: ", e);
            setStatus(Status.SERVER_ERROR_INTERNAL);
        }
        response = new StringRepresentation(jResponse.toJSONString(), MediaType.APPLICATION_JSON);
        return response;
    }

    @Get("json")
    public Representation getValue() throws ResourceException {
        Representation response = null;
        JSONObject json = new JSONObject();
        ResultSet rs;
        //TODO: No Certificate hack for testing
//		String userName = "ghostwheel";
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String userName = requestHeaders.getFirstValue("X-Username");
        String uid = ((String) getRequest().getAttributes().get("id"));
        String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
        if (uid != null) {
            if (ceeid == null) {
                try {
                    // returns data of a specific user cees
                    json = CIMIParser.ceeCollectionRetrieve(userName, uid);
                
                } catch (SQLException e) {
                    //TODO: add some errors to status and response if SQL request fails
                }catch(UnauthorizedRestAccessException e){
                                         setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                         return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                                    }
            }
        } else if (ceeid != null) {
            try {
                // returns data of a specific cee, will have to check if said user has rights to do it
                json = CIMIParser.ceeRetrieve(userName, ceeid);
            } catch (SQLException e) {
                //TODO: add some errors to status and response if SQL request fails
            }catch(UnauthorizedRestAccessException e){
                                         setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                         return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                                    }
        } else {
            //error, request malformed
        }

        response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
        return response;
    }
    
  
   

    

    
    
}
