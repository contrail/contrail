/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.log4j.Logger;

public class RestAdminDoLogin extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	private String webHost, webPort;
    
	public RestAdminDoLogin()
	{
		logger = Logger.getLogger("VEP.RestAdminDoLogin");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestAdminDoLogin", dbType);
        webHost = VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger);
        webPort = VEPHelperMethods.getProperty("webuser-interface.port", logger);
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
		Form form = new Form(entity);
		String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        String actionType = ((String) getRequest().getAttributes().get("action"));
        
        boolean proceed = false;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
	        	{
	        		proceed = true;
	        	}
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")) && rs.getString("role").equalsIgnoreCase("administrator"))
	        			proceed = true;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("Admin authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception Caught: ", ex);
        	proceed = false;
        }
        
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(proceed, username, password, actionType);
            else if(acceptType.contains("json"))
            {
                //response = toJson(proceed);
            }
        }
        else
        {
            //default rendering ...
            response = toHtml(proceed, username, password, actionType);
        }
        return response;
    }
	
	public Representation toHtml(boolean proceed, String username, String password, String actionType) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
	        stringBuilder.append("<b>Welcome to VEP Service Management Tool</b><br>").append(username).append(", you are now logged in.<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        if(actionType != null && actionType.equalsIgnoreCase("initializedb"))
	        {
	        	boolean status = VEPHelperMethods.initializeDB();
	        	if(!status)
	        	{
	        		stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
	        	}
	        	else
	        	{
	        		stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
	        	}
	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
    				.append("<input type='hidden' name='username' value='").append(username).append("'>")
    				.append("<input type='hidden' name='password' value='").append(password).append("'>")
    				.append("<input type='submit' value='proceed'></form>");
	        }
	        else if(actionType != null && actionType.equalsIgnoreCase("restartrest"))
	        {   
	        	Start.startRestServer();
	        	RestServer temp = (RestServer) VEPHelperMethods.getService("rest");
	        	if(temp != null && temp.getState())
	        	{
	        		stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
	        	}
	        	else
	        	{
	        		stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
	        	}
	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
    				.append("<input type='hidden' name='username' value='").append(username).append("'>")
    				.append("<input type='hidden' name='password' value='").append(password).append("'>")
    				.append("<input type='submit' value='proceed'></form>");
	        }
	        else
	        {
		        boolean dbCheck = VEPHelperMethods.testDBconsistency();
		        if(dbCheck)
		        {
		        	stringBuilder.append("<table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
		        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/database.png' width='48'><td>");
		        	stringBuilder.append("<td valign='center' align='left'><i>The system database is consistent.</i>.</td></tr></table>");
		        }
		        else
		        {
		        	stringBuilder.append("<table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
		        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/database.png' width='48'><td>");
		        	stringBuilder.append("<td valign='center' align='left'>The system database is inconsistent. Use the initialize button to fix it! Your installation may still work fine depending on the database you are using.")
		        			.append("<br><i>Beware! All old data values will be lost after re-initialization.</i></td><td valign='center'>");
		        	if(actionType == null)
		        	{
		        		stringBuilder.append("<form name='initializedb' action='dologin/initializedb' method='post'>")
		        			.append("<input type='hidden' name='username' value='").append(username).append("'>")
		        			.append("<input type='hidden' name='password' value='").append(password).append("'>")
		        			.append("<input type='submit' value='initialize db' onclick='this.disabled=true;return true;'></form>");
		        	}
		        	stringBuilder.append("</td></tr></table>");
		        }
		        //now do the same for rest service
		        RestServer temp = (RestServer) VEPHelperMethods.getService("rest");
	        	if(temp != null && temp.getState())
	        	{
	        		stringBuilder.append("<br><table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
	            	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/rest.png' width='48'><td>");
	            	stringBuilder.append("<td valign='center' align='left'><i>The REST server is operational.</i> If you restart the service, you will get a connection reset page. It is normal as the REST server shuts down breaking your connection. Just log back into your admin console after a few seconds.</td><td valign='center'>");
	            	stringBuilder.append("<form name='restartrest' action='dologin/restartrest' method='post'>")
        				.append("<input type='hidden' name='username' value='").append(username).append("'>")
        				.append("<input type='hidden' name='password' value='").append(password).append("'>")
        				.append("<input type='submit' value='restart rest'></form>");
	        	}
	        	else
	        	{
	        		stringBuilder.append("<br><table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
	            	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/rest.png' width='48'><td>");
	            	stringBuilder.append("<td valign='center' align='left'><i>The REST server is not-operational.</i>.</td><td valign='center'>");
	            	stringBuilder.append("<form name='restartrest' action='dologin/restartrest' method='post'>")
        				.append("<input type='hidden' name='username' value='").append(username).append("'>")
        				.append("<input type='hidden' name='password' value='").append(password).append("'>")
        				.append("<input type='submit' value='restart rest'></form>");
	        	}
	        	stringBuilder.append("</td></tr></table>");
	        	try
	            {
	            	/*String copyServerIp = VEPHelperMethods.getProperty("copyserver.ip", logger);
	            	int copyServerPort = Integer.parseInt(VEPHelperMethods.getProperty("copyserver.port", logger));
	            	
	            	Socket sock = new Socket(copyServerIp, copyServerPort);
	            	PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
	            	BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	            	out.println("hello");*/
	            	//String response = in.readLine();
                        String response="hello";
	            	if(response.equalsIgnoreCase("hello"))
	            	{
	            		stringBuilder.append("<br><table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
	                	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/copy.png' width='48'><td>");
	                	stringBuilder.append("<td valign='center' align='left'><i>The COPY service is operational.</i>.</td></tr></table>");
	            	}
	            	try
	            	{
	            		//sock.close();
	            	}
	            	catch(Exception ex)
	            	{
	            		
	            	}
	            }
	            catch(Exception ex)
	            {
	            	logger.warn("Copy service is not running probably.");
	            	stringBuilder.append("<br><table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
	            	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/copy.png' width='48'><td>");
	            	stringBuilder.append("<td valign='center' align='left'><i>The COPY service status can not be determined. Check the server settings or if the service has been started or not.</i>.</td></tr></table>");
	            }
	        }
	        if(actionType == null)
	        {
		        stringBuilder.append("<div style='font-family:Courier;font-size:10pt;'>");
		    	stringBuilder.append("<br><b>Current VEP Configuration Parameters</b><br><br>");
		    	try
		    	{
		    		FileReader fr = new FileReader(VEPHelperMethods.getPropertyFile());
		    		BufferedReader br = new BufferedReader(fr);
		    		String s;
		    		while((s = br.readLine()) != null) 
		    		{
		    			stringBuilder.append(s).append("<br>");
		    		}
		    		fr.close(); 
		    	}
		    	catch(Exception ex)
		    	{
		    		stringBuilder.append("<p style='color:red;'>Error trying to read the system configuration file.</p>");
		    	}
		    	stringBuilder.append("</div>");
	    	
		        stringBuilder.append("<td valign='top'>");
		        stringBuilder.append("<form name='dologout' action='../admin/' method='get' style='font-family:Verdana;font-size:8pt;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='logout'>");
		        stringBuilder.append("</table></form>");
		        //now generating the navigation panel
		        stringBuilder.append("<script type=\"text/javascript\">")
		        	.append("function submitForm(val) {")
		        	.append("document.forms['sidebar'].action = '../admin/doaction';")
		        	.append("if(val == 1) document.forms['sidebar'].elements['action'].value = 'changeaccount';")
		        	.append("if(val == 2) { ")
		        	.append("document.forms['sidebar'].action = '../admin/managedatacenter';")
		        	.append("}")
		        	.append("if(val == 3) document.forms['sidebar'].elements['action'].value = 'changeconfig';")
		        	.append("if(val == 4) document.forms['sidebar'].elements['action'].value = 'managecloud';")
		        	.append("if(val == 5) document.forms['sidebar'].elements['action'].value = 'checkstatus';")
		        	.append("if(val == 6) document.forms['sidebar'].action = '../admin/usermanagement';")
		        	.append("document.sidebar.submit();")
		        	.append("} </script>");
		        stringBuilder.append("<form name='sidebar' method='post' action='../admin/doaction'>")
		        	.append("<input type='hidden' name='username' value='").append(username).append("'>")
        			.append("<input type='hidden' name='password' value='").append(password).append("'>")
        			.append("<input type='hidden' name='action' value='nop'>")
		        	.append("</form>");
		        stringBuilder.append("<table cellpadding='2' style='font-family:Verdana;color:#333366;font-size:9pt;'>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(1);'>Manage Account</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(2);'>Manage Datacenter</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(3);'>Edit Configuration</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(4);'>Manage Cloud Parameters</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(5);'>Check System Status</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(6);'>User Management</a>")
		        	.append("</table>");
	        }
	        stringBuilder.append("</table>");
        }
        else
        {
        	stringBuilder.append("<b>Welcome to VEP Service Management Tool</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../admin/' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
