/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.IOUtils;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;

public class RestNewAccount extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	
	public RestNewAccount()
	{
		logger = Logger.getLogger("VEP.RestNewAccount");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestNewAccount", dbType);
	}
	

        
    /*    
         @Post("text")
    public Representation getResult(Representation entity) throws ResourceException {
        logger.info("HTML");
        Form form = new Form(entity);
        String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        String action=null;
       
            action=form.getFirstValue("action");
            
        String userid = null;
        //logger.info(password);
        //String actionType = ((String) getRequest().getAttributes().get("action"));
        boolean proceed = false;
        try {
            ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
            if (rs.next()) {
                //logger.info(VEPHelperMethods.makeSHA1Hash(password) + "VS" + rs.getString("password"));
                if (VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password"))) {
                    userid = rs.getString("id");
                    proceed = true;
                } else {
                    proceed = false;
                }
            } else {
                proceed = false;
            }
        } catch (Exception ex) {
            logger.warn("User authentication resulted in exception.");
            //if(logger.isDebugEnabled())
            //	ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
            proceed = false;
        }

        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");

        Representation response = null;

        if (acceptType != null) {
            if (acceptType.contains("html")) {
                response = toHtml(proceed, userid, username, password, action);
            } else if (acceptType.contains("json")) {
                //response = toJson(proceed);
            }
        } else {
            //default rendering ...
            response = toHtml(proceed, userid, username, password, "listCEE");
        }
        return response;
    }

    public Representation toHtml(boolean proceed, String userid, String username, String password, String action) throws ResourceException {
        String result = "";
        //stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        int userId = -1;
        if (proceed) {
            byte[] temp = null;
            String str = "";
            if (action.equalsIgnoreCase("createCEE")) {
                try {
                   // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/create_cee.html"));
                   InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("create_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                } catch (Exception ex) {
                    logger.error("create CEE page missing");
                }
                result = str;
            } else {
                if (action.equalsIgnoreCase("listCEE")) {
                    try {
                        logger.info("SHOW list-cee.html");
                        //str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/list_cee.html"));
                        InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("list_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                    } catch (Exception ex) {
                        logger.error("list CEE page missing");
                    }
                }else{
                    if(action.equalsIgnoreCase("editCEE")){
                      try {
//                        str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/edit_cee.html"));
                  
                          InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("edit_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                    } catch (Exception ex) {
                        logger.error("create CEE page missing");
                    }  
                    }
                }
            }
            str = str.replace("%USERNAME%", username);
            str = str.replace("%PASSWORD%", password);
            str = str.replace("%USERID%", userid);
            result=str;
            //FILIPPO:AUTHORIZED
        } else {
            String str = "";
            try {
               // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/wrongLogin.html"));
                InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("wrongLogin.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
            } catch (IOException ex) {
            }
            logger.info(str);
            
            result = str;
        }

        result=result.replaceAll("%WEBUSERHOST%", VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        result=result.replaceAll("%WEBUSERPORT%", VEPHelperMethods.getProperty("webuser-interface.port", logger));
        StringRepresentation value = new StringRepresentation(result, MediaType.TEXT_HTML);
        return value;
    }*/
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        @Get
	public Representation displayForm() throws ResourceException
	{
            
            StringBuilder stringBuilder = new StringBuilder();
       /* stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>");
        stringBuilder.append("Welcome to Contrail Virtual Execution Platform access point. Before you can proceed, you must login.<br>");
        stringBuilder.append("<form name='login-form' method='post' action='./dologin' style='font-family:Verdana;font-size:9pt;'>");
        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>").append("<tr><td>Username<td><input name='username' type='text' size='20'>")
        	.append("<tr><td>Password<td><input name='password' type='password' size='20'><tr><td><td align='right'><input type='submit' value='login'></form>")
        	.append("</table>If you do not have an account, you can create one <a href='./newaccount'>here</a>.");
        stringBuilder.append("</div>");*/
        String str="";
        try {
           
           InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("newAccount.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
           // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/login.html"));
        } catch (IOException ex) {
            
        }
        logger.info(VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        str=str.replace("%WEBUSERHOST%", VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        str=str.replace("%WEBUSERPORT%", VEPHelperMethods.getProperty("webuser-interface.port", logger));
        //tringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(str, MediaType.TEXT_HTML);
        return value;
        
            
            
            
            
            
            
            
	/*	StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>");
        stringBuilder.append("Welcome to the new user account creation page. All fields are mandatory.<br>");
        stringBuilder.append("<form name='newaccount-form' method='post' action='../newaccount/create' style='font-family:Verdana;font-size:9pt;'>");
        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;color:black;padding:5px;'>").append("<tr><td>Username<td><input name='username' type='text' size='20'>")
        	.append("<tr><td>Choose your password (minumum 8 characters)<td><input name='password' type='password' size='20'>")
        	.append("<tr><td>Retype password<td><input name='passwordcheck' type='password' size='20'>")
        	.append("<tr><td>Email Address<td><input name='email' type='text' size='35'>")
        	.append("<tr><td>Choose your password reset question<td><select name='resetid'><option value='-1'>Not selected</option>");
			try
			{
				ResultSet rs = db.query("select", "id, question", "resetquestion", "");
				while(rs.next())
				{
					stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("question")).append("</option>");
				}
			}
			catch(Exception ex)
			{
				//if(logger.isDebugEnabled())
				//	ex.printStackTrace(System.err);
				logger.debug("Exception Caught: ", ex);
			}
			stringBuilder.append("</select>");
        stringBuilder.append("<tr><td><td align='left'><input type='submit' value='create'></form>")
        	.append("</table>A default role of <i>user</i> will be assigned to any newly created account. Please contact your system administrator in case you need to change your role.");
        stringBuilder.append("</div>");
        
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;*/
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
		Form form = new Form(entity);
        String action = ((String) getRequest().getAttributes().get("action"));
        if(action.equalsIgnoreCase("create"))
        {
	        boolean status = true;
			Map<String, String> values = form.getValuesMap();
			Set<String> keys = values.keySet();
			Collection<String> keyval = values.values();
			Iterator<String> keyIter = keys.iterator();
			Iterator<String> valIter = keyval.iterator();
			String username = "";
			String password = "";
			String password1 = "";
			String email = "";
			int resetId = -1;
			while(keyIter.hasNext() && valIter.hasNext())
			{
				String key = keyIter.next();
				String value = valIter.next();
				if(key.equalsIgnoreCase("username")) username = value;
				if(key.equalsIgnoreCase("password")) password = value;
				if(key.equalsIgnoreCase("passwordcheck")) password1 = value;
				if(key.equalsIgnoreCase("email")) email = value;
				if(key.equalsIgnoreCase("resetid")) resetId = Integer.parseInt(value);
			}
	        
			String message = "";
			if(username == null || password == null || password1 == null || email == null || resetId == -1)
			{
				status = false;
				message += "You must provide value for all the fields. No field can be left empty. ";
			}
			if(status)
			{
				username = username.trim();
				password = password.trim();
				password1 = password1.trim();
				email = email.trim();
				if(username.length() == 0 || password.length() == 0 || password1.length() == 0 || email.length() == 0)
				{
					status = false;
					message += "You must provide value for all the fields. No field can be left empty. ";
				}
				if(status)
				{
					if(!password.equals(password1))
					{
						status = false;
						message += "Your passwords do not match. ";
					}
				}
			}
			if(status)
			{
				try
				{
					ResultSet rs = db.query("select", "count(*)", "user", "WHERE username='" + username + "'");
					if(rs.next())
					{
						if(rs.getInt(1) > 0)
						{
							status = false;
							message += "Another account with this username exists. Pick another one and try again. ";
						}
					}
					rs.close();
				}
				catch(Exception ex)
				{
					ex.printStackTrace(System.err);
				}
			}
			if(status)
			{
				ResultSet rs;
				try
				{
					rs = db.query("select", "max(id)", "user", "");
                    int id = 1;
                    if(rs.next()) id = rs.getInt(1) + 1;
                    rs.close();
                    status = db.insert("user", "(" + id + ", '" + username + "', '" + VEPHelperMethods.makeSHA1Hash(password) + "', '" + email + "', " + resetId + ", '', 'user')");
				}
				catch(Exception ex)
				{
					logger.warn("Exception caught while creating a new vep user account.");
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception Caught: ", ex);
					status = false;
				}
				
			}
			if(status)
	        {
	        	stringBuilder.append("<font color='green'>Your user account was created successfully!</font><br><br>")
	        		.append("System administrators will be notified and they will upload a user certificate for your account. Once available you should istall the certificate in your browser. ")
	        		.append("You can also associate your account with cloud user account (if you have one), or perform an automatic mapping to a system created cloud account for this account from the ")
	        		.append("accounts management page after you login.<br><br>");
	        	stringBuilder.append("<form name='goback' action='../' method='get'>")
					.append("<input type='submit' value='continue'></form>");
	        }
	        else
	        {
	        	stringBuilder.append("<font color='red'>Your account could not be created!</font><br><br>");
	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
	        	stringBuilder.append("<form name='goback' action='../newaccount' method='get'>")
					.append("<input type='submit' value='go back'></form>");
	        }
        }
        
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
