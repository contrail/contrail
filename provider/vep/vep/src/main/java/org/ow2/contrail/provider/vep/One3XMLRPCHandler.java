/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.opennebula3.client.Client;
import org.opennebula3.client.OneResponse;
import org.opennebula3.client.image.Image;
import org.opennebula3.client.user.User;
import org.opennebula3.client.vm.VirtualMachine;

public class One3XMLRPCHandler 
{
	private String oneIP;
    private String onePort;
    private String oneUser;
    private String onePass;
    private Client oneClient;
    private Logger logger;
    private XMLParser parser;
    
    public One3XMLRPCHandler(String IP, String port, String user, String pass, String entryPoint)
    {
		oneIP = IP;
        onePort = port;
        oneUser = user;
        onePass = pass;
        logger = Logger.getLogger("VEP.One3XMLRPC");
        try
        {
            logger.debug("Creating one3Client: new Client(" + oneUser + ":" + onePass + ", \"http://" + oneIP + ":" + onePort + "/RPC2\") - Entrypoint: " + entryPoint);
            oneClient  = new Client(oneUser + ":" + onePass, "http://" + oneIP + ":" + onePort + "/RPC2");
        }
        catch(Exception ex)
        {
            logger.error("OpenNebula 3 XML RPC connection could not be established. Check system settings and restart the application.");
            oneClient = null;
            //if(logger.isDebugEnabled())
            //    ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
        }
    }
    
    @SuppressWarnings("unchecked")
	public LinkedList<OpenNebulaHost> getHostList()
	{
		OneResponse val = org.opennebula3.client.host.HostPool.info(oneClient);
		String input = "";
		LinkedList<OpenNebulaHost> hostPool = new LinkedList<OpenNebulaHost>();
		if(!val.isError())
        {
            input = val.getMessage();
        }
        else
        {
            logger.warn("OpenNebula 3 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
        }
		if(input.length() > 0)
		{
			parser = new XMLParser(input, 1, hostPool, "3.4.1");
			hostPool = (LinkedList<OpenNebulaHost>)parser.parse();
		}
		return hostPool;
	}
	
    public int addUser(String name, String password) //in ONE 3 provide the normal password
    {
        int value = -1;
        if(oneClient != null)
        {
            OneResponse val = User.allocate(oneClient, name, password);
            logger.debug("Adding OpenNebula 3 user " + name + ": Got response: " + val.getMessage());
            if(val.getMessage() != null)
            {
            	value = Integer.parseInt(val.getMessage());
            	//set new user's group to oneadmin group's id (default 0)
            	User.chgrp(oneClient, value, 0);
            }
        }
        return value;
    }
    
	@SuppressWarnings("unchecked")
	public LinkedList<OpenNebulaNetwork> getVirtualNetworkList()
	{
		OneResponse val = org.opennebula3.client.vnet.VirtualNetworkPool.info(oneClient, -2);
		String input = "";
		LinkedList<OpenNebulaNetwork> virtualNetworkPool = new LinkedList<OpenNebulaNetwork>();
		if(!val.isError())
        {
            input = val.getMessage();
        }
        else
        {
            logger.warn("OpenNebula 3 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
        }
		if(input.length() > 0)
		{
			parser = new XMLParser(input, 2, virtualNetworkPool, "3.4.1");
			virtualNetworkPool = (LinkedList<OpenNebulaNetwork>)parser.parse();
		}
		return virtualNetworkPool;
	}

        
    public int addImage(String template)
    {
        int value = -1;
        if(oneClient != null)
        {
            logger.debug("Received this image template:\n" + template);
            OneResponse val = Image.allocate(oneClient, template, 1); //default datastore id is 1 assumed
            logger.debug("Registering image with ONE - got response: " + val.getMessage());
            if(!val.isError())
                value = Integer.parseInt(val.getMessage());
            else
            {
                logger.warn("Error submitting Image to ONE: " + val.getErrorMessage());
            }
        }
        return value;
    }
    
    public String imageStatus(int id)
    {
    	String status = "UNK";
        if(oneClient != null)
        {
            Image img = new Image(id, oneClient);
            OneResponse val = img.info();
            if(!val.isError())
            {
            	status = img.shortStateStr();
            	logger.debug("Image status for IaaS Image ID: " + id + ", is: " + status);
            }
            else
            {
            	logger.warn("Error retrieving Image status from ONE: " + val.getErrorMessage());
            }
        }
        return status;
    }
    
    public boolean removeImage(int id)
    {
    	boolean status = false;
        if(oneClient != null)
        {
            Image img = new Image(id, oneClient);
            OneResponse val = img.delete();
			if(!val.isError())
			{
			    logger.debug("Image with ONE id: " + id + " was sent delete command: ONE response - " + val.getMessage());
			    status = true;
			}
			else
			{
			    logger.warn("Image with ONE id: " + id + " was sent delete command: ONE response - " + val.getErrorMessage());
			    status = false;
			}            
        }
        return status;
    }

    public int addVM(String template)
    {
        int value = -1;
        if(oneClient != null)
        {
            OneResponse val = VirtualMachine.allocate(oneClient, template);
            logger.debug("Allocating VM in ONE - got response: " + val.getMessage());
            if(!val.isError())
                value = Integer.parseInt(val.getMessage());
            else
            {
                logger.warn("Error submitting VM to ONE: " + val.getErrorMessage());
            }
        }
        return value;
    }
    
    public boolean shutdownVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            
            
            OneResponse val = vm.shutdown();
			if(!val.isError())
			{
                           
			    logger.debug("VM with ONE id: " + id + " was sent shutdown command: ONE response - " + val.getMessage());
			    vm.finalizeVM();
			    status = true;
                           
			}
			else
			{
                             val=vm.finalizeVM();
                            if(val.isError()){
				String errorMsg = val.getErrorMessage();
			    logger.warn("VM with ONE id: " + id + " was sent shutdown command: ONE response - " + errorMsg);
			    status = false;
                            }else{
                                 logger.debug("VM with ONE id: " + id + " was sent shutdown command: ONE response - " + val.getMessage());
			    vm.finalizeVM();
			    status = true;
                            }
			}            
        }
        return status;
    }
    
    public boolean restartVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            OneResponse val = vm.restart();
			if(!val.isError())
			{
			    logger.debug("VM with ONE id: " + id + " was sent restart command: ONE response - " + val.getMessage());
			    status = true;
			}
			else
			{
			    logger.warn("VM with ONE id: " + id + " was sent restart command: ONE response - " + val.getErrorMessage());
			    status = false;
			}            
        }
        return status;
    }

    public boolean deployVM(int vmid, int hostid)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(vmid, oneClient);
            OneResponse val = vm.deploy(hostid);
			if(!val.isError())
			{
			    logger.debug("VM with ONE id: " + vmid + " was sent deploy command on onehost " + hostid + ": ONE response - " + val.getMessage());
			    status = true;
			}
			else
			{
			    logger.warn("Deployment of VM with ONE id: " + vmid + " FAILED on host " + hostid + ": ONE response - " + val.getErrorMessage());
			    status = false;
			}            
        }
        return status;
    }
    
    public OpenNebulaVM getVmInfo(int id)
    {
        OpenNebulaVM oneVM = new OpenNebulaVM();
        OneResponse val = VirtualMachine.info(oneClient, id);
        VirtualMachine temp = new VirtualMachine(id, oneClient);
        temp.info();
        String lcm_state = temp.lcmStateStr();
        String state = temp.stateStr();
        String input = "";
        if(!val.isError())
        {
            input = val.getMessage();
            oneVM.rawMsg = input;
        }
        else
        {
            logger.warn("OpenNebula 3 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
        }
        if(input.length() > 0)
		{
			parser = new XMLParser(input, 3, oneVM, "3.4.1");
			oneVM = (OpenNebulaVM)parser.parse();
		}
        if(oneVM != null)
        {
        	oneVM.state_value = state;
        	oneVM.lcmstate_value = lcm_state;
        }
        return oneVM;
    }
}
