/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.ow2.contrail.provider.vep.SchedulerClient.DateUtils;
import org.ow2.contrail.provider.vep.SchedulerClient.SchedulerClient;
import org.ow2.contrail.provider.vep.objects.Application.Reservation;
import org.ow2.contrail.provider.vep.objects.CEE.ConstraintMap;
import org.ow2.contrail.provider.vep.objects.CEE.DefaultMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import org.ow2.contrail.provider.vep.App;
import org.ow2.contrail.provider.vep.GAFSClient.GAFSClient;
import org.ow2.contrail.provider.vep.GAFSClient.GAFSConnector;

public class CEE extends VEPObject {

    private String parent;
    private String ceeId;
    private String status = "";
    private String permissions;
    private int reservationId;
    private ArrayList<String> vmHandlers = new ArrayList<String>();
    private ArrayList<String> netHandlers = new ArrayList<String>();
    private ArrayList<String> storageHandlers = new ArrayList<String>();
    private ArrayList<String> constraints = new ArrayList<String>();
    private ArrayList<Application> applications = new ArrayList<Application>();
    private ArrayList<ConstraintMap> mappedConstraints;
    private ArrayList<DefaultMap> mappedRessources;
    private String userid;
    private String userName;
    private ArrayList<ConstraintMap> mappedStorageConstraints;

    //for CEE Update and retrieval
    public CEE(String ceeId) {
        super("CEE", true);
        this.ceeId = ceeId;
        this.setResourceUri("VEP/CEE");
        this.setId("/cee/" + ceeId);
    }

    //For CEE Collection
    public CEE(boolean instantiate) {
        super("CEE", instantiate);
        this.setResourceUri("VEP/CEE");
    }

    //for CEE Creation
    public CEE() {
        super("CEE", true);
        this.setResourceUri("VEP/CEE");
    }

    public void retrieveCEE() throws SQLException {
        ResultSet rs = db.query("select", "*", "cee", "where id=" + ceeId);
        if (rs.next()) {
            status = rs.getString("status");
            this.setName(rs.getString("name"));
            permissions = rs.getString("permissions");
            reservationId = rs.getInt("reservationid");
            rs = db.query("select", "id", "application", "where ceeid=" + ceeId);
            while (rs.next()) {
                applications.add(new Application(ceeId, String.valueOf(rs.getInt("id"))));
            }
            rs = db.query("select", "id", "ceevmhandlers", "where ceeid=" + ceeId);
            while (rs.next()) {
                vmHandlers.add(String.valueOf(rs.getInt("id")));
            }
            rs = db.query("select", "id", "ceenethandlers", "where ceeid=" + ceeId);
            while (rs.next()) {
                netHandlers.add(String.valueOf(rs.getInt("id")));
            }
            rs = db.query("select", "id", "ceestoragehandlers", "where ceeid=" + ceeId);
            while (rs.next()) {
                storageHandlers.add(String.valueOf(rs.getInt("id")));
            }
            rs = db.query("select", "id", "ceeconstraints", "where ceeid=" + ceeId);
            while (rs.next()) {
                constraints.add(String.valueOf(rs.getInt("id")));
            }
        } else {
            setError("Could not retrieve CEE " + ceeId);
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public void setUserName(String requesterUname) {
        this.userName = requesterUname;
    }

    public void updateVMHandlers(ArrayList<String> vmhids) {
        this.vmHandlers = vmhids;
    }

    public void updateNetworkHandlers(ArrayList<String> nhids) {
        this.netHandlers = nhids;
    }

    public void updateStorageHandlers(ArrayList<String> shids) {
        this.storageHandlers = shids;
    }

    public void checkStorage() {
        //Application app=this.applications.get(0);
        //app.checkStorageConstraints();
        //GAFSConnector.setVolumePrivilegies(name,userid, "").post(gfs);
        //for now we take the first element of the "NOT IN COUNTRY3 list as it's only what is supported by GAFS
         ResultSet rs;   
         String  endpoint = null; String volumename="testVolume";
            List<String> inCountry = new ArrayList<String>();
            List<String> outCountry = new ArrayList<String>();
          try {
        rs = db.query("select", "ceestoragehandlers.id as cid, endpoint, mountendpoint", "ceestoragehandlers, storagehandler, storage", "where storage.storagetypeid="+App.GAFS_SHARE+" and storagehandler.storageid=storage.id and ceestoragehandlers.storagehandlerid=storagehandler.id");
								if(rs.next()) { endpoint=rs.getString("endpoint");}else{ return;};
            GAFSClient gfs=new GAFSClient(endpoint,true);
           
           
            int replicas;
            for (ConstraintMap cm : this.mappedStorageConstraints) {
                //if (cm.on.get(0).equals(sd.getId())) {
                    rs = db.query("select", "descp", "constraints", "where id=" + cm.id);
           
                if (rs.next()) {
                    String descp = rs.getString("descp");
                    if (descp.equalsIgnoreCase("IN COUNTRY")) {
                        if (!inCountry.contains(cm.param)) {
                            inCountry.add(cm.param);
                        }
                    } else if (descp.equalsIgnoreCase("NOT IN COUNTRY")) {
                        if (!outCountry.contains(cm.param)) {
                            outCountry.add(cm.param);
                        }
                    } else if (descp.equalsIgnoreCase("REPLICAS")) {
                        replicas = Integer.parseInt(cm.param);
                    }
                }
            
                
            }
           
            boolean authorized = true;
            if (inCountry.size() > 0) {
                authorized = GAFSConnector.setPolicies(volumename, GAFSConnector.IN_COUNTRY, inCountry.get(0)).post(gfs);
            } else if (outCountry.size() > 0) {
                authorized = GAFSConnector.setPolicies(volumename, GAFSConnector.NOT_IN_COUNTRY, outCountry.get(0)).post(gfs);
            }
            if (!authorized) {
                //GAFSConnector.deleteVolume(name, "").post(gfs);
                setError("Storage Constraint not satisfied");
                return;
                //throw new SQLException ("Storage Constraints not satisfied");
                //break;

            } else {
            }
             } catch (SQLException ex) {
                this.setError("SQL Execption");
            }

    }

    public class ConstraintMap {

        public final String id;
        public final String param;
        public final List<String> on;
        public final String descp;

        public ConstraintMap(String id, String param, List<String> on) {
            this.id = id;
            this.param = param;
            this.on = on;
            this.descp = "";
        }

        public ConstraintMap(String id, String param, List<String> on, String cnType) {
            this.id = id;
            this.param = param;
            this.on = on;
            this.descp = cnType;
        }
    }

    public class DefaultMap {

        public final String type;
        public final String ovfmapid;
        public final String handlerid;
        public Integer cpuFreq;
        public Integer memory;
        public Integer cpuCores;

        public DefaultMap(String type, String ovfmapid, String handlerid) {
            this.type = type;
            this.ovfmapid = ovfmapid;
            this.handlerid = handlerid;
        }
    }

    public void updateDefaultMap(ArrayList<DefaultMap> dmids) {
        this.mappedRessources = dmids;
    }

    public void updateApplications(ArrayList<Application> appsids) {
        this.applications = appsids;
    }

    public void updateConstraints(ArrayList<ConstraintMap> cmids) {
        this.mappedConstraints = cmids;
    }

    public void updateStorageConstraints(ArrayList<ConstraintMap> storagecmids) {
        this.mappedStorageConstraints = storagecmids;
    }

    //This method should only be called once all parameters have been checked with the corresponding method or will result in partial cees
    public HashMap<String, Object> registerToDb() throws SQLException {
        HashMap<String, Object> vepResponse = new HashMap<String, Object>();
        ResultSet rs = null;
        boolean create = this.ceeId == null ? true : false;
        boolean exist = true;
        if (create) {
            //highly dangerous outside of a non synchronized method
            rs = db.query("select", "max(id)", "cee", "");

            int nextId;
            if (rs.next()) {
                nextId = rs.getInt(1) + 1;
            } else {
                nextId = 1;
            }
            logger.debug("Creating a new CEE - " + nextId + " - for user " + userid);
            rs.close();
            if (userid == null && userName == null) {
                setError("No Userid or Username specified.");
            } else if (userid == null && userName != null) {
                //cd ..
                logger.info("USERNAME:" + userName);
                rs = db.query("select", "id", "user", "where username='" + userName + "'");
                if (rs.next()) {
                    userid = rs.getString("id");
                } else {
                    setError("No user for specified username, can not create.");
                }
            }
            if (!isError()) {

                exist = db.insert("cee(id,userid)", "(" + nextId + "," + this.userid + ")");
                this.ceeId = String.valueOf(nextId);
                this.setId("/cee/" + this.ceeId);
            }
        }
        //check existence of everything before adding
        if (exist && !isError())//cee is now in the db, or if an update a cee exists at the corresponding id
        {
            if (this.getName() != null) {
                db.update("cee", "name='" + getName() + "'", "where id=" + this.ceeId);
            }

            //VM Handlers
            if (vmHandlers != null && !isError()) {
                //VMHandlers to be updated
                String toDelete = "";
                for (String vmhId : vmHandlers) {
                    toDelete += vmhId + ",";
                    rs = db.query("select", "id", "ceevmhandlers", "where ceeid=" + this.ceeId + " and vmhandlerid=" + vmhId);
                    if (rs.next()) {
                        //vmhandler already registered
                    } else {
                        //vmhandler not already registered
                        //rs = db.query("select", "max(id)", "ceevmhandlers", "");
                        //rs.next();
                        //int nextId = rs.getInt("id");
                        db.insert("ceevmhandlers(vmhandlerid,ceeid)", "(" + vmhId + ", " + this.ceeId + ")");
                    }
                }
                //TODO:Check and improve later
                //Deletion of the unused VM handlers and the linked datas
                if (toDelete.length() > 0) {
                    toDelete = toDelete.substring(0, toDelete.length() - 1);
                    rs = db.query("select", "id", "ceevmhandlers", "where ceeid=" + this.ceeId + " and vmhandlerid not in (" + toDelete + ")");
                    while (rs.next()) {
                        int ceevmhandlerid = rs.getInt("id");
                        rs.close();
                        rs = db.query("select", "*", "vmslots", "where ceeid=" + this.ceeId + " and ceevmhandlerid=" + ceevmhandlerid);
                        while (rs.next()) {
                            String slotStatus = rs.getString("status");
                            if (!slotStatus.equals("DEP")) {
                                //VMSlot currently deployed with this handler, cannot delete
                                setError("A vm slot using the handler " + ceevmhandlerid + " is currently deployed, it cannot be deleted.");
                                break;
                            }
                        }
                        rs.close();
                        if (!isError()) {
                            rs = db.query("select", "id", "vmslots", "where ceeid=" + this.ceeId + " and ceevmhandlerid=" + ceevmhandlerid);
                            while (rs.next()) {
                                int slotid = rs.getInt("id");
                                db.delete("connectioninfo", "where vmslotid=" + slotid);
                            }
                            db.delete("vmslots", "where ceeid=" + this.ceeId + "and ceevmhandlerid=" + ceevmhandlerid);
                            db.update("ovfmap", "ceevmhandlerid=0", "where ceevmhandlerid=" + ceevmhandlerid);
                            //delete from ceevmhandlerconstraint
                            rs = db.query("select", "ceeconstraintlistid", "ceevmhandlerconstraint", "where ceevmhandlerid=" + ceevmhandlerid);
                            while (rs.next()) {
                                int listid = rs.getInt("ceeconstraintlistid");
                                db.delete("ceeconstraintslist", "where id=" + listid);
                            }
                            db.delete("ceevmhandlerconstraint", "where ceevmhandlerid=" + ceevmhandlerid);
                            db.delete("ceevmhandlers", "where ceeid=" + this.ceeId + " and vmhandlerid=" + ceevmhandlerid);
                            //now reset the query, no need to start searching inside the result as we deleted the previous result and can start from scratch
                            rs = db.query("select", "id", "ceevmhandlers", "where ceeid=" + this.ceeId + " and vmhandlerid not in (" + toDelete + ")");
                        } else {
                            break;
                        }
                    }
                }

            }
            if (isError()) {
                logger.error(getError());
            }

            //Network Handlers
            if (netHandlers.size() > 0 && !isError()) {
                String toDelete = "";
                for (String netId : netHandlers) {
                    toDelete += netId + ", ";
                    rs = db.query("select", "id", "ceenethandlers", "where ceeid=" + this.ceeId + " and vnethandlerid=" + netId);
                    if (!rs.next()) {
                        db.insert("ceenethanders(ceeid, vnethandlerid)", "(" + this.ceeId + ", " + netId + ")");
                    }
                }
                if (toDelete.length() > 0) {
                    toDelete = toDelete.substring(0, toDelete.length() - 1);
                    //delete unused vnethandlers
                    rs = db.query("select", "id", "ceenethandlers", "where ceeid=" + this.ceeId + " and vnethandlerid not in (" + toDelete + ")");
                    while (rs.next()) {
                        int ceenetid = rs.getInt("id");
                        db.delete("ceenethandlerlist", "where ceenethandlerid=" + ceenetid);
                    }
                    //one shot delete to test
                    db.delete("ceenethandlers", "where ceeid=" + this.ceeId + " and vnethandlerid not in (" + toDelete + ")");
                }
            }
            if (netHandlers.isEmpty() && !isError()) {
                //insert all VIN Network handlers in this CEE if no handlers are registered at all
                rs = db.query("select", "id", "ceenethandlers", "where ceeid=" + this.ceeId);
                logger.debug("Inserting VIN Network references");
                if (!rs.next()) {
                    logger.debug("No network registered for this cee, adding vin networks");
                    rs = db.query("select", "id", "vnethandler", "where name='vin'");
                    while (rs.next()) {
                        int vinNetId = rs.getInt("id");
                        logger.debug("writing vnets...");
                        db.insert("ceenethandlers(vnethandlerid,ceeid)", "(" + vinNetId + "," + this.ceeId + ")");
                        //TODO:Test with two registered vin networks
                    }
                }
            }

            //Storage Handlers
            if (storageHandlers.size() > 0 && !isError()) {
                String toDelete = "";
                for (String sId : storageHandlers) {
                    toDelete += sId + ",";
                    rs = db.query("select", "id", "ceestoragehandlers", "where ceeid=" + this.ceeId + " and storagehandlerid=" + sId);
                    if (!rs.next()) {
                        //storage handler not already registered
                        //Does this SH id exists?
                        rs = db.query("select", "id", "storagehandler", "where id = " + sId);
                        if (rs.next()) {
                            db.insert("ceestoragehandlers(storagehandlerid,ceeid)", "(" + sId + ", " + this.ceeId + ")");
                        } else {
                            setError("storagehandler id " + sId + " does not exist, cancelling.");
                            break;
                        }
                    }
                }
                //Deletion of now unused SHs
                if (toDelete.length() > 0) {
                    toDelete = toDelete.substring(0, toDelete.length() - 1);
                    rs = db.query("select", "id", "ceestoragehandlers", "where ceeid=" + ceeId + " and storagehandlerid not in (" + toDelete + ")");
                    while (rs.next()) {
                        int cshid = rs.getInt("id");

                        //delete the ceeregisteredstorage references
                        db.query("select", "id, usecount", "ceeregisteredstorage", "where ceestoragehandlerid=" + cshid);
                        //should check that usecount == 0 before deleting, therefore needs to update usecount when deleting an ovfmap (decrement)
                        while (rs.next()) {
                            if (rs.getInt("usecount") == 0) {
                                int crsId = rs.getInt("id");
                                db.delete("ceeregisteredstorage", "where id=" + crsId);
                            }
                        }
                        rs.close();

                        //delete the ceestoragehandlerlist references
                        rs = db.query("select", "id", "ceestoragehandlerlist", "where ceestoragehandlerid=" + cshid);
                        while (rs.next()) {
                            int cshlid = rs.getInt("id");
                            db.delete("ceestoragehandlerlist", "where id=" + cshlid);
                        }

                        //delete the ceestoragehandlers reference
                        db.delete("ceestoragehandlers", "where id=" + cshid);

                        //now reset query
                        rs = db.query("select", "id", "ceestoragehandlers", "where ceeid=" + this.ceeId + " and storagehandlerid not in (" + toDelete + ")");
                    }
                }
            }

            //Applications
            if (applications != null && !isError()) {
                for (Application app : applications) {
                    app.setUserid(userid);
                    app.setCeeId(String.valueOf(ceeId));
                    app.setStorageConstraints(mappedStorageConstraints);
                    app.setUsername(userName);
                    app.registerToDb();
                    if (app.isError()) {
                        this.setError(app.getError());
                    }
                }
            }
            if (mappedRessources != null && !isError()) {
                for (DefaultMap dm : mappedRessources) {
                    //get the ovf ids for all the the ovfs registered in this CEE
                    rs = db.query("select", "ovf.id as oid", "ovf, application", "where application.ceeid=" + this.ceeId + " and ovf.applicationid=application.id");
                    String ovfids = "";
                    while (rs.next()) {
                        ovfids += rs.getInt("oid") + ",";
                    }
                    if (ovfids != "") {
                        ovfids = ovfids.substring(0, ovfids.length() - 1);
                    } else {
                        this.setError("Couldn't find any ovfs registered in this CEE, can not map resources");
                    }

                    if (!isError()) {
                        //find the corresponding ovfmap
                        rs = db.query("select", "id", "ovfmap", "where ovfmap.ovfcontainerid in (" + ovfids + ") and ovfid='" + dm.ovfmapid + "'");
                        if (rs.next()) {
                            int id = rs.getInt("id");
                            //now add the handler and default values if present (handler must be present, but not resource values)
                            //Handler part
                            //check existence
                            rs = db.query("select", "id", "ceevmhandlers", "where vmhandlerid=" + dm.handlerid);
                            if (rs.next()) {
                                boolean statusUpdate = db.update("ovfmap", "ceevmhandlerid=" + dm.handlerid, "WHERE id=" + id);
                                if (statusUpdate) {
                                    rs = db.query("select", "*", "vmhandler", "where id=" + dm.handlerid);
                                    rs.next();
                                    //are there any values for cpu/ram/mem?
                                    int cpuFreq = rs.getInt("cpufreq_low");
                                    int cpuFreqHigh = rs.getInt("cpufreq_high");
                                    int cpuCores = rs.getInt("corecount_low");
                                    int cpuCoreHigh = rs.getInt("corecount_high");
                                    int memory = rs.getInt("ram_low");
                                    int memoryHigh = rs.getInt("ram_high");
                                    //Check values against the handler bounds
                                    if (!isError() && dm.memory != null) {
                                        if (dm.memory >= memory && dm.memory <= memoryHigh) {
                                            memory = dm.memory;
                                        } else {
                                            this.setError("RAM Value for VS " + dm.ovfmapid + " is outside the bounds of its handler");
                                        }

                                    }
                                    if (!isError() && dm.cpuFreq != null) {
                                        if (dm.cpuFreq >= cpuFreq && dm.cpuFreq <= cpuFreqHigh) {
                                            cpuFreq = dm.cpuFreq;
                                        } else {
                                            this.setError("CPUFreq Value for VS " + dm.ovfmapid + " is outside the bounds of its handler");
                                        }
                                    }
                                    if (!isError() && dm.cpuCores != null) {
                                        if (dm.cpuCores >= cpuCores && dm.cpuCores <= cpuCoreHigh) {
                                            cpuCores = dm.cpuCores;
                                        } else {
                                            this.setError("CPUCores Value for VS " + dm.ovfmapid + " is outside the bounds of its handler");
                                        }
                                    }
                                    db.update("ovfmap", "ram=" + memory + ", cpufreq=" + cpuFreq + ", corecount=" + cpuCores, "where id=" + id);
                                }
                            } else {
                                this.setError("VMHandler " + dm.handlerid + " does not exist or is not used in this CEE");
                            }

                        } else {
                            this.setError("Virtual System " + dm.ovfmapid + " does not exist in our database.");
                        }
                    }
                }
            }

            //parse apps again for reservations
            if (applications != null && !isError()) {
                for (Application app : applications) {
                    HashMap<String, VEPVirtualMachine> ovfmapitems = new HashMap<String, VEPVirtualMachine>();
                    if (!app.getReservations().isEmpty()) {
                        //gets all ovfmap items registered for this application
                        rs = db.query("select", "ovfmap.id as oid, application.id as aid, ovfid,ram,corecount,cpufreq", "ovfmap,ovf,application", "where application.id=" + app.getApplicationId() + " and ovf.applicationid=application.id and ovfmap.ovfcontainerid=ovf.id");
                        while (rs.next()) {
                            VEPVirtualMachine v = new VEPVirtualMachine(false);
                            v.setRam(rs.getInt("ram"));
                            v.setCorecount(rs.getInt("corecount"));
                            v.setCpufreq(rs.getInt("cpuFreq"));
                            v.setOvfmapname(rs.getString("ovfid"));
                            v.setOvfmapid(rs.getInt("oid"));
                            v.setApplicationId(String.valueOf(rs.getInt("aid")));
                            ovfmapitems.put(v.getOvfmapname(), v);
                        }
                    }
                    HashMap<VEPVirtualMachine, Integer> map = new HashMap<VEPVirtualMachine, Integer>();
                    for (Reservation r : app.getReservations()) {
                        VEPVirtualMachine v = ovfmapitems.get(r.ovfmapname);
                        if (v == null) {
                            setError("Trying to reserve for an undefined machine");
                        } else if (!isError()) {
                            v.countToDeploy = r.count;
                            v.endDateOfReservation = r.endDate;
                            int rId = db.insertAndGetKey("reservations(applicationid,ovfmapid)", "(" + app.getApplicationId() + "," + v.getOvfmapid() + ")");
                            v.reservationId = rId;
                            map.put(v, r.count);
                        }
                    }
                    //now use the map to make a reservation
                    if (!isError()) {
                        HashMap<VEPVirtualMachine, Integer> response = new HashMap<VEPVirtualMachine, Integer>();
                        try {
                            if (map.size() > 0) {
                                //reservationId is applicationId as it is unique per application
                                logger.debug("ASK FOR RESERVATION TO THE SCHEDULER");
                                //TODO:Ask Filippo for Date formatting, using new Date() in the meantime
                                response = SchedulerClient.makeReservation(SchedulerClient.ACTION, map, Integer.parseInt(app.getApplicationId()), SchedulerClient.toSchedulerReservationConstraints(mappedConstraints, map));
                                //response = SchedulerClient.makeReservation(SchedulerClient.ACTION, Integer.parseInt(app.getApplicationId()), map, new Date());
//								response = SchedulerClient.makeReservation(SchedulerClient.CHECK, 1, map, DateUtils.deserialize(endDate));

                                vepResponse.put("reservation", true);
                                ArrayList<VEPVirtualMachine> vmList = new ArrayList<VEPVirtualMachine>();

                                for (Map.Entry<VEPVirtualMachine, Integer> entry : response.entrySet()) {
                                    VEPVirtualMachine vm = entry.getKey();
                                    vm.countToDeploy = entry.getValue();
                                    vmList.add(vm);
                                }
                                vepResponse.put("vmList", vmList);
                            } else {
                                vepResponse.put("reservation", false);
                            }

                        } catch (Exception e) {
                            setError("Exception while making a reservation.");
                            logger.debug("Exception while making a reservation: ", e);
                        }
                        //use Scheduler Response for something?
                    }
                }
            }
            //store the cee constraints in the db
            if (mappedConstraints != null && !isError()) {
                //write all contstraints in ceeconstraints

                Set<Integer> constraints = new HashSet<Integer>();
                for (ConstraintMap cntr : mappedConstraints) {
                    constraints.add(Integer.parseInt(cntr.id));
                }

                Iterator iterator = constraints.iterator();
                while (iterator.hasNext()) {
                    if (rs != null) {
                        rs.close();
                    }
                    int constraintId = (Integer) iterator.next();
                    rs = db.query("select", "id", "constraints", "where constraints.id='" + constraintId + "'");
                    if (!rs.next()) {
                        constraintId = -1;
                    }
                    rs.close();
                    int keyConstraint;
                    if (constraintId > 0) {
                        keyConstraint = db.insertAndGetKey("ceeconstraints(constrainttype,ceeid)", "(" + constraintId + " ," + this.ceeId + ")");
                        for (ConstraintMap cntr : mappedConstraints) {
                            if (Integer.parseInt(cntr.id) == constraintId) {
                                int keyC = db.insertAndGetKey("ceeconstraintslist(ceeconstraintsid)", "(" + keyConstraint + ")");
                                for (String virtualS : cntr.on) {
                                    rs = db.query("select", "ovfmap.id as oid", "ovfmap,ovf,application", "where ovfmap.ovfid='" + virtualS + "' and ovf.applicationid=application.id and ovfmap.ovfcontainerid=ovf.id and application.ceeid=" + this.ceeId);
                                    int vsID = rs.getInt("oid");
                                    db.insertAndGetKey("ovfmapconstraint(ovfmapid,ceeconstraintslistid,param)", "(" + vsID + "," + keyC + ",'" + cntr.param + "')");
                                }
                            }

                        }

                    }
                }


                //for each contraints in the list create a ceeconstraintlist and then create a OVFMAPCONSTRAINT ROW using parameter

            }


        }
        return vepResponse;
    }

    /**
     * Get an answer to a CHECK request, either with or without reservation
     *
     * @throws SQLException
     */
    public HashMap<String, Object> checkSchedulerReservation() throws SQLException {
        HashMap<String, VEPVirtualMachine> toSchedule = new HashMap<String, VEPVirtualMachine>();
        ResultSet rs;
        for (Application app : applications) {
            for (String ovf : app.getOvfs()) {
                if (isError()) {
                    break;//Do not proceed further if one ovf fails
                }
                try {
                    OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovf, "oneadmin", "3.4");
                    String[] ids = parser.getIDs();
                    for (int i = 0; i < ids.length; i++) {
                        parser.getVSSpecs(ids[i]);
                        HashMap<String, Integer> temp = parser.getVSSpecs(ids[i]);
                        int cpuFreq = -1; //ignore this parameter for the moment
                        int diskSize = -1; //ignore this parameter for the moment
                        int ram = temp.get("ram");
                        int cpuCores = temp.get("rcores");
                        VEPVirtualMachine v = new VEPVirtualMachine(false);
                        v.setRam(ram);
                        v.setCorecount(cpuCores);
                        //Adds the VMs to the HashMap
                        toSchedule.put(ids[i], v);
                    }
                } catch (MalformedOVFException e) {
                    setError("OVF malformed, can not parse");
                    logger.debug(e);
                } catch (Exception e) {
                    setError("Unknown error while checking the CEE");
                    logger.debug(e);
                }
            }
            for (Reservation r : app.getReservations()) {
                logger.debug("HERE FINALLY");
                VEPVirtualMachine v = toSchedule.get(r.ovfmapname);
                if (v == null) {
                    setError("Error trying to reserve a VM for a VS not specified in the attached OVF");
                } else {
                    v.countToDeploy = r.count;
                    v.endDateOfReservation = r.endDate;
                }
            }
        }
        if (mappedRessources != null && !isError()) {
            for (DefaultMap dm : mappedRessources) {
                if (isError()) {
                    break;
                }
                rs = db.query("select", "*", "vmhandler", "where id=" + dm.handlerid);
                if (rs.next()) {
                    //are there any values for cpu/ram/mem?
                    int cpuFreq = rs.getInt("cpufreq_low");
                    int cpuFreqHigh = rs.getInt("cpufreq_high");
                    int cpuCores = rs.getInt("corecount_low");
                    int cpuCoreHigh = rs.getInt("corecount_high");
                    int memory = rs.getInt("ram_low");
                    int memoryHigh = rs.getInt("ram_high");
                    //Check values against the handler bounds
                    VEPVirtualMachine v = toSchedule.get(dm.ovfmapid);
                    if (v == null) {
                        setError("OVF Item " + dm.ovfmapid + " referenced in the mapping does not exist in the ovf");
                    }
                    if (!isError() && v.getRam() <= memoryHigh && v.getCorecount() <= cpuCoreHigh)//check the ovf against the bounds
                    {
                        if (!isError() && dm.memory != null) {
                            if (dm.memory >= memory && dm.memory <= memoryHigh && dm.memory >= v.getRam()) {
                                memory = dm.memory;
                            } else {
                                this.setError("RAM Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                            }

                        }
                        if (!isError() && dm.cpuFreq != null) {
                            if (dm.cpuFreq >= cpuFreq && dm.cpuFreq <= cpuFreqHigh) {
                                cpuFreq = dm.cpuFreq;
                            } else {
                                this.setError("CPUFreq Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                            }
                        }
                        if (!isError() && dm.cpuCores != null) {
                            if (dm.cpuCores >= cpuCores && dm.cpuCores <= cpuCoreHigh && dm.cpuCores >= v.getCorecount()) {
                                cpuCores = dm.cpuCores;
                            } else {
                                this.setError("CPUCores Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                            }
                        }
                        if (!isError()) {
                            v.setRam(memory);
                            v.setCorecount(cpuCores);
                            v.setCpufreq(cpuFreq);
                            //no freq supported in ovf
                        }
                    } else {
                        setError("OVF Item " + dm.ovfmapid + " mapping specified an improper handler");
                        break;
                    }
                } else {
                    setError("One of the handler specified in the default mapping does not exist");
                    break;
                }
            }
        }
        //Now check the values inside toSchedule with the Scheduler
        if (!isError()) {
            int i = 1;
            boolean reserve = false;
            String endDate = "";
            HashMap<VEPVirtualMachine, Integer> map = new HashMap<VEPVirtualMachine, Integer>();
            for (Map.Entry<String, VEPVirtualMachine> entry : toSchedule.entrySet()) {
                VEPVirtualMachine v = entry.getValue();
                v.setVmId(String.valueOf(i++));
                v.setCeeId("1");
                v.setApplicationId("1");
                v.setOvfmapname(entry.getKey());
                //TODO:Handle cpuFreq better
                //v.setCpufreq(800);
                if (v.countToDeploy != 0) {
                    reserve = true;
                    endDate = v.endDateOfReservation;
                    map.put(v, v.countToDeploy);
                } else {
                    map.put(v, 1);
                }
            }
            HashMap<VEPVirtualMachine, Integer> response = new HashMap<VEPVirtualMachine, Integer>();
            HashMap<String, Object> vepResponse = new HashMap<String, Object>();
            try {
                if (reserve) {
                    //TODO:Ask Filippo for Date formatting, using new Date() in the meantime
                    //response = SchedulerClient.makeReservation(SchedulerClient.CHECK, 0, map, new Date());
                    response = SchedulerClient.makeReservation(SchedulerClient.CHECK, 0, map, new Date(), SchedulerClient.toSchedulerReservationConstraints(this.mappedConstraints, map));
//					response = SchedulerClient.makeReservation(SchedulerClient.CHECK, 1, map, DateUtils.deserialize(endDate));
                    vepResponse.put("reservation", true);
                } else {
                    //If no reservation, still use the makeReservation call with an endDate of now 
                    response = SchedulerClient.makeReservation(SchedulerClient.CHECK, 0, map, new Date());
                    vepResponse.put("reservation", false);
                }
            } catch (Exception e) {
                setError("Exception while making a reservation.");
                logger.debug("Exception while making a reservation: ", e);
            }
            ArrayList<VEPVirtualMachine> vmList = new ArrayList<VEPVirtualMachine>();

            for (Map.Entry<VEPVirtualMachine, Integer> entry : response.entrySet()) {
                VEPVirtualMachine vm = entry.getKey();
                vm.countToDeploy = entry.getValue();
                vmList.add(vm);
            }
            vepResponse.put("vmList", vmList);
            return vepResponse;
        } else {
            return null;
        }

    }

    public boolean checkParameters() throws SQLException {
        ResultSet rs;
        boolean exist = true;
        //check existence of everything
        if (exist)//cee is now in the db, or if an update a cee exists at the corresponding id
        {
            //Check existence -> for each, if it does not exist, send a error back
            if (vmHandlers != null && !isError()) {
                //VMHandlers to be updated
                for (String vmhId : vmHandlers) {
                    rs = db.query("select", "id", "vmhandler", "where id = " + vmhId);
                    if (rs.next()) {
                    } else {
                        this.setError("vmHandler id " + vmhId + " does not exist, cancelling.");
                        rs.close();
                        break;
                    }
                }
            }
            if (netHandlers != null && !isError()) {
                for (String netId : netHandlers) {
                    rs = db.query("select", "id", "vnethandler", "where id = " + netId);
                    if (rs.next()) {
                    } else {
                        this.setError("vnetandler id " + netId + " does not exist, cancelling.");
                        break;
                    }
                }
            }
            if (storageHandlers != null && !isError()) {
                for (String sId : storageHandlers) {
                    rs = db.query("select", "id", "storagehandler", "where id = " + sId);
                    if (rs.next()) {
                    } else {
                        this.setError("storagehandler id " + sId + " does not exist, cancelling.");
                        break;
                    }
                }
            }
            if (applications != null && !isError()) {
                for (Application app : applications) {
                    app.checkParameters();
                    if (app.isError()) {
                        this.setError(app.getError());
                        break;
                    }
                }
            }
            //mapping are not checked there
        }
        return isError();
    }

//	public boolean checkDeployability
    public void setUserId(String requesterUname) {
        this.userid = requesterUname;
    }

    /**
     * Deletes a CEE
     *
     * @throws SQLException
     */
    public void delete() throws SQLException {
        retrieveCEE();
        if (!isError()) {
            ResultSet rs;
            //loop over apps to stop all running instances
//            for (Application a : applications) {
//                a.doAction("stop");
//                if (a.isError()) {
//                    setError("Error while stopping application " + a.getApplicationId());
//                }
//            }
//            if (!isError()) {
            for (Application a : applications) {
                a.doAction("delete");
                if (a.isError()) {
                    setError("Error while deleting application " + a.getApplicationId());
                }
            }
            if (!isError()) {
                //delete handlers from the cee as well as constraints
                db.delete("ceevmhandlers", "where ceeid=" + ceeId);
                db.delete("ceenethandlers", "where ceeid=" + ceeId);
                db.delete("ceestoragehandlers", "where ceeid=" + ceeId);

                //:TODO search through all types of constraints here (db call to rewrite)
//					db.delete("ceeconstraintslist, ceeconstraints, ovfmapconstraint, ceevmhandlerconstraint, vmslotconstraint", "where ceeconstraints.ceeid="+ceeId);


                db.delete("cee", "where id=" + ceeId);

            }
//            }
        }

    }

    public void checkCEErules() {
        HashMap<String, VEPVirtualMachine> toSchedule = new HashMap<String, VEPVirtualMachine>();
        ResultSet rs;
        for (Application app : applications) {
            for (String ovf : app.getOvfs()) {
                if (isError()) {
                    break;//Do not proceed further if one ovf fails
                }
                try {
                    OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovf, "oneadmin", "3.4");
                    String[] ids = parser.getIDs();
                    for (int i = 0; i < ids.length; i++) {
                        parser.getVSSpecs(ids[i]);
                        HashMap<String, Integer> temp = parser.getVSSpecs(ids[i]);
                        int cpuFreq = -1; //ignore this parameter for the moment
                        int diskSize = -1; //ignore this parameter for the moment
                        int ram = temp.get("ram");
                        int cpuCores = temp.get("rcores");
                        VEPVirtualMachine v = new VEPVirtualMachine(false);
                        v.setRam(ram);
                        v.setCorecount(cpuCores);
                        //Adds the VMs to the HashMap
                        toSchedule.put(ids[i], v);
                    }
                } catch (MalformedOVFException e) {
                    setError("OVF malformed, can not parse");
                    logger.debug(e);
                } catch (Exception e) {
                    setError("Unknown error while checking the CEE");
                    logger.debug(e);
                }
            }
            for (Reservation r : app.getReservations()) {
                logger.debug("HERE FINALLY");
                VEPVirtualMachine v = toSchedule.get(r.ovfmapname);
                if (v == null) {
                    setError("Error trying to reserve a VM for a VS not specified in the attached OVF");
                } else {
                    v.countToDeploy = r.count;
                    v.endDateOfReservation = r.endDate;
                }
            }
        }
        if (mappedRessources != null && !isError()) {
            for (DefaultMap dm : mappedRessources) {
                try {
                    if (isError()) {
                        break;
                    }
                    rs = db.query("select", "*", "vmhandler", "where id=" + dm.handlerid);
                    if (rs.next()) {
                        //are there any values for cpu/ram/mem?
                        int cpuFreq = rs.getInt("cpufreq_low");
                        int cpuFreqHigh = rs.getInt("cpufreq_high");
                        int cpuCores = rs.getInt("corecount_low");
                        int cpuCoreHigh = rs.getInt("corecount_high");
                        int memory = rs.getInt("ram_low");
                        int memoryHigh = rs.getInt("ram_high");
                        //Check values against the handler bounds
                        VEPVirtualMachine v = toSchedule.get(dm.ovfmapid);
                        if (v == null) {
                            setError("OVF Item " + dm.ovfmapid + " referenced in the mapping does not exist in the ovf");
                        }
                        if (!isError() && v.getRam() <= memoryHigh && v.getCorecount() <= cpuCoreHigh)//check the ovf against the bounds
                        {
                            if (!isError() && dm.memory != null) {
                                if (dm.memory >= memory && dm.memory <= memoryHigh && dm.memory >= v.getRam()) {
                                    memory = dm.memory;
                                } else {
                                    this.setError("RAM Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                                }

                            }
                            if (!isError() && dm.cpuFreq != null) {
                                if (dm.cpuFreq >= cpuFreq && dm.cpuFreq <= cpuFreqHigh) {
                                    cpuFreq = dm.cpuFreq;
                                } else {
                                    this.setError("CPUFreq Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                                }
                            }
                            if (!isError() && dm.cpuCores != null) {
                                if (dm.cpuCores >= cpuCores && dm.cpuCores <= cpuCoreHigh && dm.cpuCores >= v.getCorecount()) {
                                    cpuCores = dm.cpuCores;
                                } else {
                                    this.setError("CPUCores Value for VS" + dm.ovfmapid + " is outside the bounds of its handler or inferior to its ovf item");
                                }
                            }
                            if (!isError()) {
                                v.setRam(memory);
                                v.setCorecount(cpuCores);
                                v.setCpufreq(cpuFreq);
                                //no freq supported in ovf
                            }
                        } else {
                            setError("OVF Item " + dm.ovfmapid + " mapping specified an improper handler");
                            break;
                        }
                    } else {
                        setError("One of the handler specified in the default mapping does not exist");
                        break;
                    }
                } catch (SQLException ex) {
                    setError("sqlException");
                }
            }
        }
    }
}
