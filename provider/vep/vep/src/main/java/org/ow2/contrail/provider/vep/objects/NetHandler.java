/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NetHandler extends VEPObject {

	private boolean pubipsupport;
	private boolean vlantagsupport;
	private boolean dhcpsupport;
	private int cloudnetworkid;
	private String ceeId;
	private String netHandlerId;
	
	public NetHandler(boolean instantiate) {
		super("NetHandler", instantiate);
	}
	
	public NetHandler(String ceeId, String netHandlerId) {
		super("NetHandler", true);
		this.ceeId = ceeId; //not useful?
		this.netHandlerId = netHandlerId;
		this.setId("/cee/"+ceeId+"/networkHandler/"+netHandlerId);
		this.setResourceUri("VEP/NetworkHandler");
	}
	
	public NetHandler(String netHandlerId) {
		super("NetHandler", true);
		this.netHandlerId = netHandlerId;
		this.setId("/networkHandler/"+netHandlerId);
		this.setResourceUri("VEP/NetworkHandler");
	}
	
	public boolean retrieveNetHandler() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "vnethandler", "where id="+netHandlerId);
		if (rs.next())
		{
			this.setName(rs.getString("name"));
			this.setCloudnetworkid(rs.getInt("cloudnetworkid"));
			this.setVlantagsupport(rs.getBoolean("vlantagsupport"));
			this.setPubipsupport(rs.getInt("pubipsupport")!=0);
			this.setDhcpsupport(rs.getBoolean("dhcpsupport"));
			success = true;
		}
		return success;
	}



	public boolean isVlantagsupport() {
		return vlantagsupport;
	}

	public void setVlantagsupport(boolean vlantagsupport) {
		this.vlantagsupport = vlantagsupport;
	}

	public boolean isDhcpsupport() {
		return dhcpsupport;
	}

	public void setDhcpsupport(boolean dhcpsupport) {
		this.dhcpsupport = dhcpsupport;
	}

	public int getCloudnetworkid() {
		return cloudnetworkid;
	}

	public void setCloudnetworkid(int cloudnetworkid) {
		this.cloudnetworkid = cloudnetworkid;
	}

	public boolean isPubipsupport() {
		return pubipsupport;
	}

	public void setPubipsupport(boolean pubipsupport) {
		this.pubipsupport = pubipsupport;
	}

}
