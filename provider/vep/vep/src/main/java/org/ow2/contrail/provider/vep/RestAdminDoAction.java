/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.log4j.Logger;

public class RestAdminDoAction extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	
	public RestAdminDoAction()
	{
		logger = Logger.getLogger("VEP.RestAdminDoAction");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestAdminDoAction", dbType);
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
		Form form = new Form(entity);
		String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        String actionType = form.getFirstValue("action");
        
        boolean proceed = false;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
	        	{
	        		proceed = true;
	        	}
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        			proceed = true;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("Admin authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception Caught: ", ex);
        	proceed = false;
        }
        
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(proceed, username, password, actionType, form);
            else if(acceptType.contains("json"))
            {
                //response = toJson(proceed);
            }
        }
        else
        {
            //default rendering ...
            response = toHtml(proceed, username, password, actionType, form);
        }
        return response;
    }
	
	public Representation toHtml(boolean proceed, String username, String password, String actionType, Form form) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	String action = ((String) getRequest().getAttributes().get("action"));
        	
        	stringBuilder.append("<b>Welcome to VEP Service Management Tool</b><br>");
        	if(actionType.equalsIgnoreCase("changeaccount"))
        		stringBuilder.append(username).append(", you wish to manage your account parameters!<hr>");
        	else if(actionType.equalsIgnoreCase("managedatacenter"))
        		stringBuilder.append(username).append(", you wish to manage your datacanter!<hr>");
        	else if(actionType.equalsIgnoreCase("changeconfig"))
        		stringBuilder.append(username).append(", you wish to modify system configuration!<hr>");
        	else if(actionType.equalsIgnoreCase("managecloud"))
        		stringBuilder.append(username).append(", you wish to manage your cloud parameters!<hr>");
        	else if(actionType.equalsIgnoreCase("checkstatus"))
        		stringBuilder.append(username).append(", you wish to check current resource status!<hr>");
        	
        	stringBuilder.append("<table><tr><td valign='top' width='850' style='font-family:Verdana; font-size:10pt;'>");
        	if(action == null)
        	{
	        	//////////////////////////////////////// Generating appropriate forms next
        		if(actionType.equalsIgnoreCase("checkstatus"))
	        	{
        			stringBuilder.append("<form name='markprocessed' action='../admin/doaction/markprocessed' method='post' style='font-family:Verdana;font-size:9pt;'>");
        			stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
	    	        try
	    	        {
	    	        	ResultSet rs = db.query("select", "count(*)", "errormessagelist", "WHERE isprocessed=0");
	    	        	int pendingMsgCount = rs.getInt(1);
	    	        	if(pendingMsgCount == 0)
	    	        	{
	    	        		stringBuilder.append("There is no pending message to process.");
	    	        	}
	    	        	else
	    	        	{
	    	        		stringBuilder.append("Pending system messages to process - <br><br>");
	    	        		stringBuilder.append("<table border='1' style='font-family:Verdana;font-size:9pt;background:#FFFFFF;border-width:1px;border-style:solid;border-color:navy;'>");
	    	        		stringBuilder.append("<tr><th>Id<th>Concise Message<th>Detailed Message<th>VEP User<th>State<th>Table List<th>Timestamp<th>Processed?</tr>");
	    	        		rs = db.query("select", "*", "errormessagelist", "WHERE isprocessed=0");
	    	        		while(rs.next())
	    	        		{
	    	        			int id = rs.getInt("id");
	    	        			String msg = rs.getString("shortmsg");
	    	        			String longMsg = rs.getString("detailedinfo");
	    	        			String userName = rs.getString("username");
	    	        			String state = rs.getString("state");
	    	        			String tableList = rs.getString("tablelist");
	    	        			String timeStamp = rs.getString("timestamp");
	    	        			stringBuilder.append("<tr><td>").append(id).append("<td>").append(msg).append("<td>").append(longMsg).append("<td>").append(userName).append("<td>")
	    	        				.append(state).append("<td>").append(tableList).append("<td>").append(timeStamp).append("<td><input type='radio' title='check if you wish to mark this message as processed' name='messageid' value='")
	    	        				.append(id).append("'>");
	    	        		}
	    	        		stringBuilder.append("</table>");
	    	    	        stringBuilder.append("<br><input title='select the message that you wish to mark as processed and then press this button' type='submit' value='mark as processed' align='left'>");
	    	        	}
	    	        }
	    	        catch(Exception ex)
	    	        {
	    	        	logger.info("Exception caught while generating the pending admin messages.");
	    	        	//if(logger.isDebugEnabled())
	    	        	//	ex.printStackTrace(System.err);
	    	        	logger.debug("Exception Caught: ", ex);
	    	        }
	    	        stringBuilder.append("</form>");
	        	}
        		else if(actionType.equalsIgnoreCase("changeaccount"))
	        	{
	        		stringBuilder.append("<form name='changepass' action='../admin/doaction/changepass' method='post' style='font-family:Verdana;font-size:9pt;'>");
	    	        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;background:#FFFFFF;'><tr><td>");
	    	        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
	    			stringBuilder.append("Current password <td><input type='password' name='currpass' size='16'><td>");
	    			stringBuilder.append("<tr><td>New password <td><input type='password' name='newpass1' size='16'><td> at least 8 characters");
	    			stringBuilder.append("<tr><td>Retype new password<td><input type='password' name='newpass2' size='16'><td>");
	    	        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='change password'><td>");
	    	        stringBuilder.append("</table></form>");
	    	        //display linked cloud account for this admin account
	    	        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:navy;'>");
	    	        stringBuilder.append("Below are the various cloud accounts linked to your administrator's id:<br><br>");
	    	        try
	    	        {
	    	        	ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
	    	        	int vepuserId = -1;
	    	        	if(rs.next())
	    	        		vepuserId = rs.getInt("id");
	    	        	if(vepuserId == -1)
	    	        	{
	    	        		stringBuilder.append("<br><font color='red'>VEP DB is currupted. Please fix it!</font><br>");
	    	        	}
	    	        	else
	    	        	{
	    	        		stringBuilder.append("<table cellspacing='2' cellpadding='2' border='1' style='width:500px;font-family:Verdana;font-size:9pt;color:gray;border-width:1px;border-style:dashed;'><tr><th>map-id<th>cloud-type<th>iaas-user<th>iaas-uid<th>iaas-password</tr>");
	    	        		rs = db.query("select", "*", "accountmap", "where vepuser=" + vepuserId);
	    	        		int counter=0;
	    	        		while(rs.next())
	    	        		{
	    	        			int mapid = rs.getInt("id");
	    	        			String iaasUser = rs.getString("iaasuser");
	    	        			String iaasPass = rs.getString("iaaspass");
	    	        			String iaasId = rs.getString("iaasuid");
	    	        			int cloudTypeId = rs.getInt("cloudtype");
	    	        			String cloudType = "";
	    	        			counter++;
	    	        			rs = db.query("select", "name", "cloudtype", "WHERE id=" + cloudTypeId);
	            				if(rs.next())
	            				{
	            					cloudType = rs.getString("name");
	            				}
	            				rs.close();
	            				stringBuilder.append("<tr><td>").append(mapid).append("<td>").append(cloudType).append("<td>").append(iaasUser).append("<td>").append(iaasId).append("<td>").append(iaasPass).append("</tr>");
	            				rs = db.query("select", "*", "accountmap", "where vepuser=" + vepuserId);
	            				int count = 0;
	            				while(count < counter) { rs.next(); count++; }    				
	    	        		}
	    	        		stringBuilder.append("</table>");
	    	        	}
	    	        }
	    	        catch(Exception ex)
	    	        {
	    	        	logger.warn("Exception caught while requestion account map information for admin account");
	    	        	//if(logger.isDebugEnabled())
	    	        	//	ex.printStackTrace(System.err);
	    	        	logger.debug("Exception Caught: ", ex);
	    	        }
	    	        stringBuilder.append("</div><br><div style='font-family:Verdana; font-size:9pt; color:navy;'>");
	    	        stringBuilder.append("Please use the form below to update/add a new cloud account mapped to this account.<br><br>");
	        		stringBuilder.append("<form name='editcloudmap' action='../admin/doaction/editcloudmap' method='post' style='font-family:Verdana;font-size:9pt;color:black;'>");
			        stringBuilder.append("<table style='width:500px;font-family:Verdana;font-size:9pt;background:white;color:black;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr style='background:silver;'><td colspan='2'>Please check the box in case you are modifying an existing entry. If left unchecked the submit action will add a new cloud account mapping for your account.</tr>");
					stringBuilder.append("<tr><td>Check if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>Select the entry (if modifying)<td align='right'><select name='mapid'><option value='-1'>Not selected</option>");
					int vepuserId = -1;
					try
					{
						ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
	    	        	if(rs.next())
	    	        		vepuserId = rs.getInt("id");
						rs = db.query("select", "id", "accountmap", "where vepuser=" + vepuserId);
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
						
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<input type='hidden' name='vepuserid' value='").append(vepuserId).append("'>");
					stringBuilder.append("<tr><td>Destination Cloud<td align='right'><select name='cloudtypeid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id, name", "cloudtype", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("<tr><td>IaaS Username<td align='right'><input type='text' name='iaasuser' size='24'>");
					stringBuilder.append("<tr><td>IaaS Password<td align='right'><input type='password' name='iaaspass' size='24'>");
					stringBuilder.append("<tr><td>IaaS ID<td align='right'><input type='text' name='iaasuid' size='24'>");
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	    	        stringBuilder.append("</div>");
	        	}
	        	else if(actionType.equalsIgnoreCase("changeconfig"))
	        	{
	        		Properties vepProp = new Properties();
	        		try
	        		{
	        			FileInputStream in = new FileInputStream(VEPHelperMethods.getPropertyFile());
	        			vepProp.load(in);
	        			Set<Object> keys = vepProp.keySet();
	        			Collection<Object> values = vepProp.values();
	        			Iterator<Object> iter = keys.iterator();
	        			Iterator<Object> iterVal = values.iterator();
	        			
	        			stringBuilder.append("<form name='changepass' action='../admin/doaction/changesetting' method='post' style='font-family:Verdana;font-size:8pt;'>");
		    	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
		    	        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
		    	        stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		    	        stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
	        			
	        			while(iter.hasNext() && iterVal.hasNext())
	        			{
	        				String key = (String)iter.next();
	        				stringBuilder.append("<tr><td>").append(key)
	        					.append("<td><input type='text' size='48' name='prop-").append(key).append("' value='")
	        					.append(iterVal.next()).append("'>");
	        			}
	        			in.close();
	        			stringBuilder.append("<tr><td><td align='right'><input type='submit' value='change settings'>");
	        			stringBuilder.append("</table></form>");
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception while reading the properties file.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        	}
	        	else if(actionType.equalsIgnoreCase("managecloud"))
	        	{
	        		stringBuilder.append("Summary view of your cloud parameters -<br>");
	        		stringBuilder.append("<table cellspacing='2' cellpadding='2' style='font-size:9pt;font-family:Courier;background:silver;'>");
	        		stringBuilder.append("<tr><td style='background:#333333;color:white;width:140px;' align='center'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayTab(1);'>Supported clouds</a>");
	        		stringBuilder.append("<td style='background:#333333;color:white;width:140px;' align='center'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayTab(2);'>Constraints list</a>");
	        		stringBuilder.append("<td style='background:#333333;color:white;width:140px;' align='center'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayTab(3);'>VM handlers</a>");
	        		stringBuilder.append("<td style='background:#333333;color:white;width:200px;' align='center'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayTab(4);'>Virtual Network handlers</a>");
	        		stringBuilder.append("<td style='background:#333333;color:white;width:140px;' align='center'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayTab(5);'>Storage handlers</a>");
	        		stringBuilder.append("</tr></table>");
	        		
	        		stringBuilder.append("<script type=\"text/javascript\">")
		        		.append("function displayTab(val) {")
		        		.append("if(val == 1) {")
		        		.append("document.getElementById('default').style.display = \"none\";")
		        		.append("document.getElementById('clouds').style.display = \"block\";")
		        		.append("document.getElementById('constraints').style.display = \"none\";")
		        		.append("document.getElementById('vmhandlers').style.display = \"none\";")
		        		.append("document.getElementById('vnethandlers').style.display = \"none\";")
		        		.append("document.getElementById('storagehandlers').style.display = \"none\";")
		        		.append("}")
		        		.append("if(val == 2) {")
		        		.append("document.getElementById('default').style.display = \"none\";")
		        		.append("document.getElementById('clouds').style.display = \"none\";")
		        		.append("document.getElementById('constraints').style.display = \"block\";")
		        		.append("document.getElementById('vmhandlers').style.display = \"none\";")
		        		.append("document.getElementById('vnethandlers').style.display = \"none\";")
		        		.append("document.getElementById('storagehandlers').style.display = \"none\";")
		        		.append("}")
		        		.append("if(val == 3) {")
		        		.append("document.getElementById('default').style.display = \"none\";")
		        		.append("document.getElementById('clouds').style.display = \"none\";")
		        		.append("document.getElementById('constraints').style.display = \"none\";")
		        		.append("document.getElementById('vmhandlers').style.display = \"block\";")
		        		.append("document.getElementById('vnethandlers').style.display = \"none\";")
		        		.append("document.getElementById('storagehandlers').style.display = \"none\";")
		        		.append("}")
		        		.append("if(val == 4) {")
		        		.append("document.getElementById('default').style.display = \"none\";")
		        		.append("document.getElementById('clouds').style.display = \"none\";")
		        		.append("document.getElementById('constraints').style.display = \"none\";")
		        		.append("document.getElementById('vmhandlers').style.display = \"none\";")
		        		.append("document.getElementById('vnethandlers').style.display = \"block\";")
		        		.append("document.getElementById('storagehandlers').style.display = \"none\";")
		        		.append("}")
		        		.append("if(val == 5) {")
		        		.append("document.getElementById('default').style.display = \"none\";")
		        		.append("document.getElementById('clouds').style.display = \"none\";")
		        		.append("document.getElementById('constraints').style.display = \"none\";")
		        		.append("document.getElementById('vmhandlers').style.display = \"none\";")
		        		.append("document.getElementById('vnethandlers').style.display = \"none\";")
		        		.append("document.getElementById('storagehandlers').style.display = \"block\";")
		        		.append("}")
		        		.append("} </script>");
	        		
	        		
	        		stringBuilder.append("<div id='default' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:block;'>");
	        		stringBuilder.append("<br><br>Click on the link above to view summary data.");
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("<div id='clouds' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:none;'>");
	        		try
	        		{
	        			int counter = 0;
	        			ResultSet rs = db.query("select", "*", "cloudtype", "");
	        			while(rs.next())
	        			{
	        				stringBuilder.append("<div style='background:#CCCCFF;color:black;width:730px;padding:5px;'>")
	        				.append("Internal ID: ").append(rs.getInt("id"))
        					.append("<br>Name: ").append(rs.getString("name")).append("<br>Head IP: ").append(rs.getString("headip"))
        					.append("<br>Head Port: ").append(rs.getString("headport")).append("<br>Version: ")
        					.append(rs.getString("version")).append("<br>Shared Folder: ").append(rs.getString("sharedfolder"))
        					.append("<br>Cloud of Type: ");
	        				int cloudtypeid = rs.getInt("typeid");
	        				counter++;
	        				rs = db.query("select", "name", "supportedcloud", "WHERE id=" + cloudtypeid);
	        				stringBuilder.append(rs.getString("name"));
        					stringBuilder.append("</div><br>");
        					rs = db.query("select", "*", "cloudtype", "");
        					int count=0;
        					while(count < counter) { rs.next(); count++; }
	        			}
	        			rs.close();
	        			if(counter == 0)
	        			{
	        				stringBuilder.append("<div style='background:#FFCCCC;color:black;width:730px;padding:5px;'>No cloud manager found.</div>");
	        			}
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception caught while retrieving the cloud parameters.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("<div id='constraints' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:none;'>");
	        		try
	        		{
	        			int counter = 0;
	        			ResultSet rs = db.query("select", "*", "constraints", "");
	        			while(rs.next())
	        			{
	        				stringBuilder.append("<div style='background:#CCCCFF;color:black;width:730px;padding:5px;'>")
	        				.append("Internal ID: ").append(rs.getInt("id")).append("<br>Rule: ")
	        				.append(rs.getString("descp")).append("</div><br>");
	        				counter++;
	        			}
	        			rs.close();
	        			if(counter == 0)
	        			{
	        				stringBuilder.append("<div style='background:#FFCCCC;color:black;width:730px;padding:5px;'>No constraints found.</div>");
	        			}
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception caught while retrieving the cloud parameters.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("<div id='vmhandlers' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:none;'>");
	        		try
	        		{
	        			int counter = 0;
	        			ResultSet rs = db.query("select", "*", "vmhandler", "");
	        			while(rs.next())
	        			{
	        				stringBuilder.append("<div style='background:#CCCCFF;color:black;width:730px;padding:5px;'>")
	        					.append("Internal ID: ").append(rs.getInt("id"))
	        					.append("<br>Name: ").append(rs.getString("name")).append("<br>CPU Freq: ").append(rs.getInt("cpufreq_low"))
	        					.append(" - ").append(rs.getInt("cpufreq_high")).append(" MHz<br>RAM: ")
	        					.append(rs.getInt("ram_low")).append(" - ").append(rs.getInt("ram_high"))
	        					.append(" MB<br>Cores: ").append(rs.getInt("corecount_low"))
	        					.append(" - ").append(rs.getInt("corecount_high")).append("<br>Disk: ")
	        					.append(rs.getInt("disksize_low")).append(" - ").append(rs.getInt("disksize_high"))
	        					.append(" MB</div><br>");
	        				counter++;
	        			}
	        			rs.close();
	        			if(counter == 0)
	        			{
	        				stringBuilder.append("<div style='background:#FFCCCC;color:black;width:730px;padding:5px;'>No vm handlers found.</div>");
	        			}
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception caught while retrieving the cloud parameters.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("<div id='vnethandlers' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:none;'>");
	        		try
	        		{
	        			int counter = 0;
	        			ResultSet rs = db.query("select", "*", "vnethandler", "");
	        			while(rs.next())
	        			{
	        				stringBuilder.append("<div style='background:#CCCCFF;color:black;width:730px;padding:5px;'>")
	        				.append("Internal ID: ").append(rs.getInt("id"))
        					.append("<br>Name: ").append(rs.getString("name"))
        					.append("<br>Public IP available: ").append(rs.getInt("pubipsupport")).append(" (1 = YES)<br>VLan Tag Support: ")
        					.append(rs.getInt("vlantagsupport")).append("<br>DHCP Support: ").append(rs.getInt("dhcpsupport"))
        					.append("</div><br>");
	        				counter++;
	        			}
	        			rs.close();
	        			if(counter == 0)
	        			{
	        				stringBuilder.append("<div style='background:#FFCCCC;color:black;width:730px;padding:5px;'>No vnet handler found.</div>");
	        			}
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception caught while retrieving the cloud parameters.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("<div id='storagehandlers' style='width:775px;padding:5px;background:#FFFFFF;height:250px;font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;overflow:auto;display:none;'>");
	        		try
	        		{
	        			int counter = 0;
	        			ResultSet rs = db.query("select", "*", "storagehandler", "");
	        			while(rs.next())
	        			{
	        				stringBuilder.append("<div style='background:#CCCCFF;color:black;width:730px;padding:5px;'>")
	        				.append("Internal ID: ").append(rs.getInt("id"))
        					.append("<br>Name: ").append(rs.getString("name"))
        					.append("</div><br>");
	        				counter++;
	        			}
	        			rs.close();
	        			if(counter == 0)
	        			{
	        				stringBuilder.append("<div style='background:#FFCCCC;color:black;width:730px;padding:5px;'>No storage handler found.</div>");
	        			}
	        		}
	        		catch(Exception ex)
	        		{
	        			logger.warn("Exception caught while retrieving the cloud parameters.");
	        			//if(logger.isDebugEnabled())
	        			//	ex.printStackTrace(System.err);
	        			logger.debug("Exception Caught: ", ex);
	        		}
	        		stringBuilder.append("</div><br>");
	        		stringBuilder.append("Cloud settings management panel -");
	        		stringBuilder.append("<table cellspacing='0' style='width:850px;font-family:Verdana;font-size:9pt;'>");
	        		stringBuilder.append("<tr><td style='width:200px;' valign='top'>");
	        		/////////////////////cloud parameters menu goes here
	        		stringBuilder.append("<div style='background:#CCCCCC;padding:5px;border-width:1px;border-style:dotted;width:199px;font-family:Verdana;font-size:10pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(1);'>Manage Clouds</a></div>");
	        		stringBuilder.append("<div style='background:#999999;padding:5px;border-width:1px;border-style:dotted;width:199px;font-family:Verdana;font-size:10pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(2);'>Manage Constraints</a></div>");
	        		stringBuilder.append("<div style='background:#666666;padding:5px;border-width:1px;border-style:dotted;width:199px;font-family:Verdana;font-size:10pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(3);'>Manage VM handlers</a></div>");
	        		stringBuilder.append("<div style='background:#333333;padding:5px;border-width:1px;border-style:dotted;width:199px;font-family:Verdana;font-size:10pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(4);'>Manage Vnet handlers</a></div>");
	        		stringBuilder.append("<div style='background:#000000;padding:5px;border-width:1px;border-style:dotted;width:199px;font-family:Verdana;font-size:10pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(5);'>Manage Storage handlers</a></div>");
	        		stringBuilder.append("<td valign='top' style='width:840px;'>");
	        		
	        		stringBuilder.append("<script type=\"text/javascript\">")
	        		.append("function displayFormTab(val) {")
	        		.append("if(val == 1) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"block\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("if(val == 2) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"block\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("if(val == 3) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"block\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("if(val == 4) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"block\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("if(val == 5) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"block\";")
	        		.append("}")
	        		.append("} </script>");
	        		
	        		stringBuilder.append("<script type=\"text/javascript\">")
	        		.append("function displayCloudSpecificTab(val) {")
	        		.append("if(val == 1) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"block\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"none\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("if(val == 2) {")
	        		.append("document.getElementById('form-default').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-choice').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-opennebula').style.display = \"none\";")
	        		.append("document.getElementById('form-cloud-openstack').style.display = \"block\";")
	        		.append("document.getElementById('form-constraint').style.display = \"none\";")
	        		.append("document.getElementById('form-vmhandler').style.display = \"none\";")
	        		.append("document.getElementById('form-vnethandler').style.display = \"none\";")
	        		.append("document.getElementById('form-storagehandler').style.display = \"none\";")
	        		.append("}")
	        		.append("} </script>");
	        		/////////////////////cloud parameters menu action pane goes here
	        		stringBuilder.append("<div id='form-default' style='padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;'>");
	        		stringBuilder.append("<br><br>Please click on the buttons on your left to access the approariate forms.");
	        		stringBuilder.append("</div>");
	        		
	        		
	        		/// Generate OpenNebula or Openstack cloud choice ///
	        		stringBuilder.append("<div id='form-cloud-choice' style='background:#CCCCCC;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please select the OpenNebula or the OpenStack link to edit or add a cloud of a specific type");
	        		stringBuilder.append("<br />");
	        		stringBuilder.append("<a style='color:black;' href='javascript:void(0);' onclick='javascript:displayCloudSpecificTab(1);'>OpenNebula</a>");
	        		stringBuilder.append("<br />");
	        		stringBuilder.append("<a style='color:black;' href='javascript:void(0);' onclick='javascript:displayCloudSpecificTab(2);'>OpenStack</a>");
	        		stringBuilder.append("</div>");
	        		
	        		
	        		////////////////////form for generating cloud management fields for opennebula////////////////
	        		stringBuilder.append("<div id='form-cloud-opennebula' style='background:#CCCCCC;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
	        		stringBuilder.append("<form name='editcloud' action='../admin/doaction/editcloud' method='post' style='font-family:Verdana;font-size:10pt;'>");
			        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#CCCCCC;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='cloudid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id", "cloudtype", "where typeid="+App.OPENNEBULA3_HOST);
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='cloudname' size='20'>");
					stringBuilder.append("<tr><td>Head node IP<td align='right'><input type='text' name='headip' size='20'>");
					stringBuilder.append("<tr><td>Head node port<td align='right'><input type='text' name='headport' size='20'>");
					stringBuilder.append("<tr><td>Software version<td align='right'><input type='text' name='version' size='20'>");
					stringBuilder.append("<tr><td>Shared folder path (for contextualization)<td align='right'><input type='text' name='folderpath' size='20'>");
					stringBuilder.append("<tr><td>Type of Cloud<td align='right'><select name='typeid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id, name", "supportedcloud", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	        		stringBuilder.append("</div>");
	        		
	        		
	        		////////////////////form for generating cloud management fields for openstack////////////////
	        		stringBuilder.append("<div id='form-cloud-openstack' style='background:#CCCCCC;padding:5px;border-width:1px;border-style:dashed;width:620px;height:465px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
		        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
		        		stringBuilder.append("<form name='editcloud' action='../admin/doaction/editcloudopenstack' method='post' style='font-family:Verdana;font-size:10pt;'>");
				        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#CCCCCC;'>");
				        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
						stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
						stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
						stringBuilder.append("<tr><td colspan='2'><hr></tr>");
						stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
						stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='cloudid'><option value='-1'>Not selected</option>");
						try
						{
							ResultSet rs = db.query("select", "id", "cloudtype", "where typeid="+App.OPENSTACK_HOST);
							while(rs.next())
							{
								stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
							}
						}
						catch(Exception ex){}
						stringBuilder.append("</select>");
						//end select list of existing cloud
						
						stringBuilder.append("<tr><td colspan='2'><hr></tr>");
						stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='cloudname' size='20'>");
						stringBuilder.append("<tr><td>KeyStone admin IP (starting with http://) <td align='right'><input type='text' name='keystoneadminip' size='20'>");
						stringBuilder.append("<tr><td>KeyStone admin port<td align='right'><input type='text' name='keystoneadminport' size='20'>");
						stringBuilder.append("<tr><td>KeyStone user IP (starting with http://) <td align='right'><input type='text' name='keystoneuserip' size='20'>");
						stringBuilder.append("<tr><td>KeyStone user port<td align='right'><input type='text' name='keystoneuserport' size='20'>");
						stringBuilder.append("<tr><td>Region (for all VMs)<td align='right'><input type='text' name='region' size='20'>");
						stringBuilder.append("<tr><td>Tenant name (for the administrator users)<td align='right'><input type='text' name='tenantname' size='20'>");
						stringBuilder.append("<tr><td>Tenant id (for all users auto-created, may be different than the id of the admin tenant)<td align='right'><input type='text' name='tenantid' size='33'>");
						stringBuilder.append("<tr><td>Role id (for all users auto-created)<td align='right'><input type='text' name='roleid' size='33'>");
						stringBuilder.append("<tr><td>Shared folder path (for contextualization)<td align='right'><input type='text' name='folderpath' size='20'>");
				        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
				        stringBuilder.append("</table></form>");					
	        		stringBuilder.append("</div>");
	        		
	        		////////////////////////////////form for constraints management
	        		stringBuilder.append("<div id='form-constraint' style='background:#999999;color:black;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new constraint entry for your cloud platform.");
	        		stringBuilder.append("<form name='editconstraint' action='../admin/doaction/editconstraint' method='post' style='font-family:Verdana;font-size:10pt;'>");
			        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#999999;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='ruleid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id", "constraints", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Constraint rule<td align='right'>"
                                                +"<select name='rule'><option value='NOT IN COUNTRY' selected>NOT IN COUNTRY</option><option value='IN COUNTRY'>IN COUNTRY</option><option value='SAME RACK'>SAME RACK</option><option value='NOT SAME HOST'>NOT SAME HOST</option><option value='REPLICAS'>REPLICAS</option></select>");
                                                //+ "<input type='text' name='rule' size='20'>");
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	        		stringBuilder.append("</div>");
	        		////////////////////////////////form for VM handler management
	        		stringBuilder.append("<div id='form-vmhandler' style='background:#666666;color:white;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new constraint entry for your cloud platform.");
	        		stringBuilder.append("<form name='editvmhandler' action='../admin/doaction/editvmhandler' method='post' style='font-family:Verdana;font-size:10pt;'>");
			        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#666666;color:white;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='vmhandlerid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id", "vmhandler", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>VM handler name<td align='right'><input type='text' name='vmhandlername' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>CPU Frequency (lower value) in MHz<td align='right'><input type='text' name='cpufreq-low' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>CPU Frequency (upper value) in MHz<td align='right'><input type='text' name='cpufreq-high' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>Main Memory (lower value) in MB<td align='right'><input type='text' name='ram-low' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>Main Memory (upper value) in MB<td align='right'><input type='text' name='ram-high' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>CPU Cores (lower value)<td align='right'><input type='text' name='cpucore-low' size='2' style='height:22px;'>");
					stringBuilder.append("<tr><td>CPU Cores (upper value)<td align='right'><input type='text' name='cpucore-high' size='2' style='height:22px;'>");
					stringBuilder.append("<tr><td>Local Storage (lower value) in MB<td align='right'><input type='text' name='disk-low' size='10' style='height:22px;'>");
					stringBuilder.append("<tr><td>Local Storage (upper value) in MB<td align='right'><input type='text' name='disk-high' size='10' style='height:22px;'>");
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	        		stringBuilder.append("</div>");
	        		///////////////////////////////form for Vnet handler management
	        		stringBuilder.append("<div id='form-vnethandler' style='background:#333333;color:white;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new constraint entry for your cloud platform.");
	        		stringBuilder.append("<form name='editvnethandler' action='../admin/doaction/editvnethandler' method='post' style='font-family:Verdana;font-size:10pt;'>");
			        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#333333;color:white;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='vnethandlerid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id", "vnethandler", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Vnet handler name<td align='right'><input type='text' name='vnethandlername' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>Check this if managed public IP addresses available<td align='right'><input type='checkbox' name='publicipsupport' value='yes'>");
					stringBuilder.append("<tr><td>Check this if vlan tag is supported<td align='right'><input type='checkbox' name='vlansupport' value='yes'>");
					stringBuilder.append("<tr><td>Check this if dhcp is supported<td align='right'><input type='checkbox' name='dhcpsupport' value='yes'>");
					stringBuilder.append("<tr><td>Select cloud network this handler links to<td align='right'><select name='cloudnetworkid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id, name", "cloudnetwork", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append(" - ").append(rs.getString("name")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	        		stringBuilder.append("</div>");
	        		//////////////////////////////form for Storage handler management
	        		stringBuilder.append("<div id='form-storagehandler' style='background:#000000;color:white;padding:5px;border-width:1px;border-style:dashed;width:620px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new constraint entry for your cloud platform.");
	        		stringBuilder.append("<form name='editstoragehandler' action='../admin/doaction/editstoragehandler' method='post' style='font-family:Verdana;font-size:10pt;'>");
			        stringBuilder.append("<table style='width:610px;font-family:Verdana;font-size:10pt;background:#000000;color:white;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='action' value='").append(actionType).append("'>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Check this if modifying an existing entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>If modifying, select the entry to modify<td align='right'><select name='storagehandlerid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id", "storagehandler", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<tr><td>Storage handler name<td align='right'><input type='text' name='storagehandlername' size='20' style='height:22px;'>");
					stringBuilder.append("<tr><td>Select cloud storage this handler links to<td align='right'><select name='cloudstorageid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id, name", "storage", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append(" - ").append(rs.getString("name")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	        		stringBuilder.append("</div>");
	        		
	        		stringBuilder.append("</table>");
	        	}
	        	////////////////////////////////////////
	        	stringBuilder.append("<td valign='top'>");
		        stringBuilder.append("<form name='goback' action='../admin/dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='cancel and go back'>");
		        stringBuilder.append("</table></form>");
        	}
        	else
        	{
        		if(action.equalsIgnoreCase("markprocessed"))
        		{
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			int messageId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				//System.err.println(key + ", " + value);
        				if(key.equalsIgnoreCase("messageid")) messageId = Integer.parseInt(value);
        			}
        			boolean status = true;
        			String message = "";
        			if(messageId != -1)
        			{
        				status = db.update("errormessagelist", "isprocessed=1", "WHERE id=" + messageId);
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("changepass"))
        		{
        			String oldpass = form.getFirstValue("currpass");
        	        String newpass1 = form.getFirstValue("newpass1");
        	        String newpass2 = form.getFirstValue("newpass2");
        	        boolean status = true;
        	        if(password.equals(oldpass) && newpass1.equals(newpass2) && newpass1.length() >= 8)
        	        {
        	        	try
        	        	{
        	        		db.update("user", "password='" + VEPHelperMethods.makeSHA1Hash(newpass1) + "'", "WHERE username='" + username + "'");
        	        	}
        	        	catch(Exception ex)
        	        	{
        	        		status = false;
        	        		//if(logger.isDebugEnabled())
        	        		//	ex.printStackTrace(System.err);
        	        		logger.debug("Exception Caught: ", ex);
        	        	}
        	        }
        	        else
        	        {
        	        	status = false;
        	        }
        	        if(!status)
    	        	{
    	        		stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
    	        	}
    	        	else
    	        	{
    	        		stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
    	        	}
        	        if(status)
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(newpass1).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        else
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        		}
        		else if(action.equalsIgnoreCase("editcloudmap"))
        		{
        	        boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int mapId = -1;
        			int cloudtypeId = -1;
        			String iaasUser = "";
        			String iaasPass = "";
        			//String tenant = "";
        			String iaasUid = "";
        			int vepuserId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("mapid")) mapId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("cloudtypeid")) cloudtypeId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("iaasuser")) iaasUser = value;
        				if(key.equalsIgnoreCase("iaaspass")) iaasPass = value;
        				if(key.equalsIgnoreCase("iaasuid")) iaasUid = value;
        				//if(key.equalsIgnoreCase("tenant")) tenant = value;
        				if(key.equalsIgnoreCase("vepuserid"))
        				{
        					try
        					{
        						vepuserId = Integer.parseInt(value.trim());
        					}
        					catch(Exception ex)
        					{
        						vepuserId = -1;
        						logger.warn("Exception caught while parsing the vep user account id.");
        					}
        				}
        			}
        			String message = "";
        			if(isModify && mapId == -1)
        			{
        				status = false;
        				message = "You must choose the cloud account entry to modify. ";
        			}
        			if(iaasUser == null || iaasPass == null || cloudtypeId == -1)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				iaasUser = iaasUser.trim();
        				iaasPass = iaasPass.trim();
        				if(iaasUid != null)
        					iaasUid = iaasUid.trim();
        				else
        					iaasUid = "";
//        				if(tenant != null)
//        					tenant = tenant.trim();
//        				else
//        					tenant = "";
            			if(iaasUser.length() == 0 || iaasPass.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			int typeid = -1; //cloud type, OS or ONE
        			ResultSet rs = db.query("select", "typeid", "cloudtype", "where id="+cloudtypeId);
        			try {
        				if(rs.next())
	        			{
	        				typeid = rs.getInt("typeid");
//	        				if(typeid == App.OPENSTACK_HOST && tenant.length()==0)
//	        				{
//	        					status = false;
//	        					message += "You must provide a tenant name when registering an OpenStack cloud administrator.";
//	        				}
	        			}
        			} catch (Exception ex) {
        				logger.error("Couldn't retrieve cloud type id for cloud id: "+cloudtypeId);
        				logger.debug("Exception Caught: ", ex);
						status = false;
        			}
        			if(status)
        			{
        				if(isModify)
        				{
        					try
        					{
        						//if(typeid == App.OPENSTACK_HOST)
        						//	status = db.update("accountmap", "cloudtype=" + cloudtypeId + ", iaasuser='" + iaasUser + "', iaaspass='" + iaasPass + "', iaasuid='" + iaasUid + "', vepuser=" + vepuserId + ", tenant='" + tenant + "'", "where id=" + mapId);
        						//else
        							status = db.update("accountmap", "cloudtype=" + cloudtypeId + ", iaasuser='" + iaasUser + "', iaaspass='" + iaasPass + "', iaasuid='" + iaasUid + "', vepuser=" + vepuserId, "where id=" + mapId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cloud account mapping.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "accountmap", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                //if(typeid == App.OPENSTACK_HOST)
                                //	status = db.insert("accountmap", "(" + id + ", " + cloudtypeId + ", '" + iaasUser + "', '" + iaasPass + "', '" + iaasUid + "', " + vepuserId + ", '" + tenant + "')");
                                //else
                                	status = db.insert("accountmap", "(" + id + ", " + cloudtypeId + ", '" + iaasUser + "', '" + iaasPass + "', '" + iaasUid + "', " + vepuserId + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new cloud account mapping.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("changesetting"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			Properties props = new Properties();
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(!key.startsWith("prop-")) continue;
        				key = key.split("-")[1];
        				if(value == null) value = "";
        				else value = value.trim();
        				props.setProperty(key, value);
        			}
        			try
        			{
        				FileOutputStream fos = null;
        				props.store((fos = new FileOutputStream(VEPHelperMethods.getPropertyFile())), "Author: Piyush Harsh");
        				fos.close();
        				logger.info("VEP system configuration was updated.");
        			}
        			catch(Exception ex)
        			{
        				status = false;
        				//if(logger.isDebugEnabled())
        				//	ex.printStackTrace(System.err);
        				logger.debug("Exception Caught: ", ex);
        				logger.warn("Exception caught while updating VEP system configuration.");
        			}
        	        if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editcloud"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int cloudId = -1;
        			String cloudName = "";
        			String headIp = "";
        			String headPort = "";
        			String cloudVersion = "";
        			String folderPath = "";
        			int portValue = -1;
        			int typeId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("cloudid")) cloudId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("typeid")) typeId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("cloudname")) cloudName = value;
        				if(key.equalsIgnoreCase("headip")) headIp = value;
        				if(key.equalsIgnoreCase("headport")) headPort = value;
        				if(key.equalsIgnoreCase("version")) cloudVersion = value;
        				if(key.equalsIgnoreCase("folderpath")) folderPath = value;
        			}
        			String message = "";
        			if(isModify && cloudId == -1)
        			{
        				status = false;
        				message = "You must choose the cloud entry to modify. ";
        			}
        			if(cloudName == null || headIp == null || headPort == null || cloudVersion == null || folderPath == null || typeId == -1)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				cloudName = cloudName.trim();
            			headIp = headIp.trim();
            			headPort = headPort.trim();
            			cloudVersion = cloudVersion.trim();
            			folderPath = folderPath.trim();
            			if(cloudName.length() == 0 || headIp.length() == 0 || headPort.length() == 0 || cloudVersion.length() == 0 || folderPath.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				try
        				{
        					portValue = Integer.parseInt(headPort);
        				}
        				catch(Exception ex)
        				{
        					portValue = -1;
        					status = false;
        					message += "You must provide a valid head node port value. ";
        				}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("cloudtype", "name='" + cloudName + "', headip='" + headIp + "', headport=" + portValue + ", version='" + cloudVersion + "', sharedfolder='" + folderPath + "', typeid=" + typeId, "where id=" + cloudId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cloud entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "cloudtype", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("cloudtype", "(" + id + ", '" + cloudName + "', '" + headIp + "', " + portValue + ", '" + cloudVersion + "', '" + folderPath + "', " + typeId + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new cloud entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editcloudopenstack"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int cloudId = -1;
        			String cloudName = "";
        			String keystoneAdminIp = "";
        			String keystoneAdminPort = "";
        			String keystoneUserIp = "";
        			String keystoneUserPort = "";
        			String region = "";
        			String tenantName = "";
        			String tenantId = "";
        			String roleId = "";
        			String folderPath = "";
        			int keystoneAdminPortValue = -1;
        			int keystoneUserPortValue = -1;
        			int typeId = App.OPENSTACK_HOST;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("cloudid")) cloudId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("cloudname")) cloudName = value;
        				if(key.equalsIgnoreCase("keystoneadminip")) keystoneAdminIp = value;
        				if(key.equalsIgnoreCase("keystoneadminport")) keystoneAdminPort = value;
        				if(key.equalsIgnoreCase("keystoneuserip")) keystoneUserIp = value;
        				if(key.equalsIgnoreCase("keystoneuserport")) keystoneUserPort = value;
        				if(key.equalsIgnoreCase("tenantname")) tenantName = value;
        				if(key.equalsIgnoreCase("tenantid")) tenantId = value;
        				if(key.equalsIgnoreCase("roleid")) roleId = value;
        				if(key.equalsIgnoreCase("region")) region = value;
        				if(key.equalsIgnoreCase("folderpath")) folderPath = value;
        			}
        			String message = "";
        			if(isModify && cloudId == -1)
        			{
        				status = false;
        				message = "You must choose the cloud entry to modify. ";
        			}
        			if(cloudName == null || keystoneAdminIp == null || keystoneAdminPort == null || keystoneUserIp == null || keystoneUserPort == null || region == null || folderPath == null || typeId == -1 || tenantName == null || tenantId == null || roleId == null)
        			{
        				status = false;
        				logger.debug("one of the fields is null");
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				cloudName = cloudName.trim();
        				keystoneAdminIp = keystoneAdminIp.trim();
        				keystoneAdminPort = keystoneAdminPort.trim();
        				keystoneUserIp = keystoneUserIp.trim();
        				keystoneUserPort = keystoneUserPort.trim();
            			region = region.trim();
            			folderPath = folderPath.trim();
            			tenantName = tenantName.trim();
            			tenantId = tenantId.trim();
            			roleId = roleId.trim();
            			if(cloudName.length() == 0 || keystoneAdminIp.length() == 0 || keystoneAdminPort.length() == 0 || keystoneUserIp.length() == 0 || keystoneUserPort.length() == 0 || region.length() == 0 || folderPath.length() == 0 || tenantName.length() == 0 || tenantId.length() == 0 || roleId.length() == 0)
            			{
            				status = false;
            				logger.debug("one of the trimmed fields is null");
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				try
        				{
        					keystoneAdminPortValue = Integer.parseInt(keystoneAdminPort);
        					keystoneUserPortValue = Integer.parseInt(keystoneUserPort);
        				}
        				catch(Exception ex)
        				{
        					keystoneAdminPortValue = -1;
        					keystoneUserPortValue = -1;
        					status = false;
        					message += "You must provide a valid head node port value. ";
        				}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("cloudtype", "name='" + cloudName + "', sharedfolder='" + folderPath + "', typeid=" + typeId, "where id=" + cloudId);
        						if(status)
        							status = db.update("openstackcloud", "keystoneadminip='"+keystoneAdminIp+"', keystoneadminport="+keystoneAdminPortValue+", keystoneuserip='"+keystoneUserIp+"', keystoneuserport="+keystoneUserPortValue+", region='"+region+"', tenantname='"+tenantName+"', tenantid='"+tenantId+"', roleid='"+roleId+"'", "where id="+cloudId);
        					}
        					catch(Exception ex)
        					{
        						logger.error("Exception caught while updating an existing cloud entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "cloudtype", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("cloudtype(id,name,sharedfolder,typeid)", "(" + id + ", '" + cloudName + "', '" + folderPath + "', " + typeId + ")");
                                if(status)
                                	status = db.insert("openstackcloud", "(" + id + ", '" + keystoneUserIp + "', " + keystoneUserPortValue + ", '" + keystoneAdminIp + "', " + keystoneAdminPortValue + ", '" + region + "', '"+tenantName+"', '"+tenantId+"', '"+roleId+"')");
        					}
        					catch(Exception ex)
        					{
        						logger.error("Exception caught while adding a new cloud entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editconstraint"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int ruleId = -1;
        			String rule = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("ruleid")) ruleId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("rule")) rule = value;
        			}
        			String message = "";
        			if(isModify && ruleId == -1)
        			{
        				status = false;
        				message = "You must choose the constraint entry to modify. ";
        			}
        			if(rule == null)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				rule = rule.trim();
            			if(rule.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("constraints", "descp='" + rule + "'", "where id=" + ruleId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing constraint.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "constraints", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                int type = App.GENERALCONSTRAINT;
                                if(rule.equalsIgnoreCase("REPLICAS"))
                                	type = App.STORAGECONSTRAINT;
                                else if(rule.equalsIgnoreCase("NOT SAME HOST") || rule.equals("SAME RACK"))
                                	type = App.VMCONSTRAINT;
                                status = db.insert("constraints", "(" + id + ", '" + rule + "', "+type+")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new constraint rule.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editvmhandler"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int vmhandlerId = -1;
        			String vmhandlerName = "";
        			int cpufreq_low = -1;
        			int cpufreq_high = -1;
        			int ram_low = -1;
        			int ram_high = -1;
        			int corecount_low = -1;
        			int corecount_high = -1;
        			int disksize_low = -1;
        			int disksize_high = -1;
        			String message = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("vmhandlerid")) vmhandlerId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("vmhandlername")) vmhandlerName = value;
        				try
        				{
        					if(key.equalsIgnoreCase("cpufreq-low")) cpufreq_low = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("cpufreq-high")) cpufreq_high = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("ram-low")) ram_low = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("ram-high")) ram_high = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("cpucore-low")) corecount_low = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("cpucore-high")) corecount_high = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("disk-low")) disksize_low = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("disk-high")) disksize_high = Integer.parseInt(value.trim());
        				}
        				catch(Exception ex)
        				{
        					status = false;
        					logger.info("Incorrect parameter was provided.");
        					message = "Data entry error. Check for type consistency. ";
        				}
        			}
        			
        			if(isModify && vmhandlerId == -1)
        			{
        				status = false;
        				message += "You must choose the vmhandler entry to modify. ";
        			}
        			if(vmhandlerName == null || cpufreq_low == -1 || cpufreq_high == -1 || ram_low == -1 || ram_high == -1 || corecount_low == -1 || corecount_high == -1 || disksize_low == -1 || disksize_high == -1)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				vmhandlerName = vmhandlerName.trim();
            			if(vmhandlerName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
            			if(cpufreq_high < cpufreq_low || ram_low > ram_high || corecount_low > corecount_high || disksize_low > disksize_high)
            			{
            				message += "Check if the values entered are correct. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("vmhandler", "name='" + vmhandlerName + "', cpufreq_low=" + cpufreq_low + ", cpufreq_high=" + cpufreq_high + ", ram_low=" + ram_low + 
        								", ram_high=" + ram_high + ", corecount_low=" + corecount_low + ", corecount_high=" + corecount_high + ", disksize_low=" + disksize_low + ", disksize_high=" + disksize_high, 
        								"where id=" + vmhandlerId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing vmhandler.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "vmhandler", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("vmhandler", "(" + id + ", '" + vmhandlerName + "', " + cpufreq_low + ", " + cpufreq_high + ", " + ram_low + ", " + ram_high +
                                		", " + corecount_low + ", " + corecount_high + ", " + disksize_low + ", " + disksize_high + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new vmhandler entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editvnethandler"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int vnethandlerId = -1;
        			String vnethandlerName = "";
        			int publicIpSupport = 0;
        			int vlanSupport = 0;
        			int dhcpSupport = 0;
        			int cloudNetworkId = -1;
        			String message = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("vnethandlerid")) vnethandlerId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("vnethandlername")) vnethandlerName = value;
        				if(key.equalsIgnoreCase("cloudnetworkid")) cloudNetworkId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("publicipsupport")) publicIpSupport = 1;
        				if(key.equalsIgnoreCase("vlansupport")) vlanSupport = 1;
        				if(key.equalsIgnoreCase("dhcpsupport")) dhcpSupport = 1;
        			}
        			
        			if(isModify && vnethandlerId == -1)
        			{
        				status = false;
        				message += "You must choose the vnethandler entry to modify. ";
        			}
        			if(cloudNetworkId == -1)
        			{
        				status = false;
        				message += "You must choose a valid cloud network to attach this handler to. ";
        			}
        			if(status)
        			{
        				vnethandlerName = vnethandlerName.trim();
            			if(vnethandlerName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("vnethandler", "name='" + vnethandlerName + "', pubipsupport=" + publicIpSupport + ", vlantagsupport=" + vlanSupport + ", dhcpsupport=" + dhcpSupport + 
        								", cloudnetworkid=" + cloudNetworkId, 
        								"where id=" + vnethandlerId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing vnethandler.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "vnethandler", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("vnethandler", "(" + id + ", '" + vnethandlerName + "', " + publicIpSupport + ", " + vlanSupport + ", " + dhcpSupport + ", " + cloudNetworkId + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new vnethandler entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editstoragehandler"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			int storagehandlerId = -1;
        			String storagehandlerName = "";
        			int cloudStorageId = -1;
        			String message = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("storagehandlerid")) storagehandlerId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("storagehandlername")) storagehandlerName = value;
        				if(key.equalsIgnoreCase("cloudstorageid")) cloudStorageId = Integer.parseInt(value.trim());
        			}
        			
        			if(isModify && storagehandlerId == -1)
        			{
        				status = false;
        				message += "You must choose the storagehandler entry to modify. ";
        			}
        			if(cloudStorageId == -1)
        			{
        				status = false;
        				message += "You must choose a valid cloud storage to attach this handler to. ";
        			}
        			if(status)
        			{
        				storagehandlerName = storagehandlerName.trim();
            			if(storagehandlerName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("storagehandler", "name='" + storagehandlerName + "', storageid=" + cloudStorageId, "where id=" + storagehandlerId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing storagehandler.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "storagehandler", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("storagehandler", "(" + id + ", '" + storagehandlerName + "', " + cloudStorageId + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new storagehandler entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='action' value='").append(actionType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        	}
	        
	        stringBuilder.append("</table>");
        }
        else
        {
        	
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
