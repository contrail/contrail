/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;


public class ImageStatusUpdate implements Runnable
{
	private Thread t;
	private String pName;
	private boolean isRunning;
	private Logger logger;
	private DBHandler db;
	private long periodicity;
	private HashMap<Integer, Object> connectors;

	public ImageStatusUpdate(String processname, long period)
	{
		pName = processname;
		isRunning = false;
		periodicity = period;
		t = new Thread(this);
		logger = Logger.getLogger("VEP.ImageUpdate");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
		db = new DBHandler("ImageUpdate", dbType);
		connectors = new HashMap<Integer, Object>();

		//openstack cloud
		ResultSet rs = db.query("select", "*", "openstackcloud, cloudtype", "where cloudtype.id = openstackcloud.id");
		try {
			int count = 0;
			while(rs.next())
			{
				count++;
				int cloudTypeId = rs.getInt(1);
				HashMap<String, Object> openstackinfo = new HashMap<String, Object>();
				openstackinfo.put("region", rs.getString("region"));
				rs.close();
				rs = db.query("select", "user.id as uid", "user, accountmap", "WHERE role='administrator' and accountmap.cloudtype=" + cloudTypeId + " and accountmap.vepuser=user.id");
				int userid = rs.getInt("uid");
				OpenStackConnector osc = VEPHelperMethods.getOSC(userid, cloudTypeId);
				openstackinfo.put("connector", osc);
				connectors.put(cloudTypeId, openstackinfo);
				rs.close();
				rs = db.query("select", "*", "openstackcloud, cloudtype", "where cloudtype.id = openstackcloud.id");
				int i = 0;
				while(i<count)
				{
					rs.next();
					i++;
				}
			}


			//opennebula cloud
			rs = db.query("select", "*", "cloudtype", "where cloudtype.typeid ="+App.OPENNEBULA3_HOST);
                        
                        
                        if(rs.next())
			{
			String cloudIP = rs.getString("headip");
			int cloudPort = rs.getInt("headport");
			int cloudTypeId = rs.getInt("id");
                        
			//			rs.close();

			rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype=" + cloudTypeId + " and accountmap.vepuser=user.id");
			if(rs.next())
			{
				String iaasUser=rs.getString("iaasuser");
				String iaasPass=rs.getString("iaaspass");
				One3XMLRPCHandler one3handle = new One3XMLRPCHandler(cloudIP, Integer.toString(cloudPort), iaasUser, iaasPass, "image status");

				connectors.put(cloudTypeId, one3handle);
			}}
		} catch (Exception e) {
			logger.error("Exception caught", e);
		}
	}

	public String getProcessName()
	{
		return pName;
	}

	public boolean getRunningState()
	{
		return isRunning;
	}

	public void start()
	{
		try
		{
			isRunning = true;
			t.start();
		}
		catch(Exception ex)
		{
			logger.info("Exception caught inside image status updater thread, process name: " + pName);
			logger.debug("Exception caught ", ex);
		}
	}

	public void stop()
	{
		try
		{
			isRunning = false;
			t.join();
			logger.trace("VEP Image Status Updater service has stopped.");
		}
		catch (Exception ex)
		{
			logger.warn("Exception caught while stopping the Image Status Updater service. " + ex.toString());
		}
	}

	@Override
	public void run() 
	{
		logger.info("Starting the image status updater thread - process name: " + pName);
		isRunning = true;
		while(isRunning)
		{
			logger.trace("Starting another loop for updating the IaaS image status flags.");
			try
			{
				ResultSet rs = db.query("select", "osdiskmap.id as osdid, cloudtypeid, iaasid, typeid,status", "osdiskmap, cloudtype", "where osdiskmap.cloudtypeid=cloudtype.id and osdiskmap.status!='RDY'");
				int count = 0;
				while(rs.next())
				{
					count++;
					int cloudTypeId = rs.getInt("cloudtypeid");
					int osdId = rs.getInt("osdid");
					String iaasId = rs.getString("iaasid");
					int typeId = rs.getInt("typeid");
					String status = rs.getString("status");
					if(iaasId != null)
					{
						if(typeId == App.OPENNEBULA3_HOST)
						{
//							if(!status.equalsIgnoreCase("RDY"))
//							{
								One3XMLRPCHandler one3handle = (One3XMLRPCHandler) connectors.get(cloudTypeId);
								if(one3handle != null) 
								{
									String imgStatus = one3handle.imageStatus(Integer.parseInt(iaasId));
									if(imgStatus.equalsIgnoreCase("rdy") || imgStatus.equalsIgnoreCase("used"))
									{
										db.update("osdiskmap", "status='RDY'", "WHERE id=" + osdId);
									}
									else if(imgStatus.equalsIgnoreCase("err"))
									{
										db.update("osdiskmap", "status='ERR'", "WHERE id=" + osdId);
									}
									else if(imgStatus.equalsIgnoreCase("lock"))
									{
										db.update("osdiskmap", "status='LCK'", "WHERE id=" + osdId);
									}
									else if(imgStatus.equalsIgnoreCase("disa"))
									{
										db.update("osdiskmap", "status='DIS'", "WHERE id=" + osdId);
									}
									else
									{
										db.update("osdiskmap", "status='UNK'", "WHERE id=" + osdId);
									}
								} else {
									logger.warn("No handler created for opennebula image id "+osdId+", problem may be lack of admin account for this cloud.");
								}
//							} else {
//								logger.debug("Image " + osdId + " already in ready state.");
//							}
						}
						else if(typeId == App.OPENSTACK_HOST)
						{
//							if(!status.equalsIgnoreCase("RDY"))
//							{
								HashMap<String, Object> conn = (HashMap<String, Object>)connectors.get(cloudTypeId);
								if(conn != null)
								{
									String region = (String)conn.get("region");
									OpenStackConnector osc = (OpenStackConnector)conn.get("connector");
									if(region != null && osc != null)
									{
										status = osc.imageStatus(iaasId, region);
										db.update("osdiskmap", "status='"+status+"'", "WHERE id=" + osdId);
									}
								} else {
									logger.warn("No handler created for openstack image id "+osdId+", problem may be lack of admin account for this cloud.");
								}
//							} else {
//								logger.debug("Image " + osdId + " already in ready state.");
//							}
						}
						else
						{
							throw new Exception("This cloud is currently not supported.");
						}
					}
					else
					{
						logger.warn("Improper IaaS Id detected for registered diskmap image " + osdId);
					}
					//now resetting the sql query
					int counter = 0;
					rs.close();
					rs = db.query("select", "osdiskmap.id as osdid, cloudtypeid, iaasid, typeid, status", "osdiskmap, cloudtype", "where osdiskmap.cloudtypeid=cloudtype.id and osdiskmap.status!='RDY'");
					while(counter < count) 
					{ 
						rs.next(); 
						counter++;
					}
				}
				rs.close();
				//				}

			}
			catch(Exception ex)
			{
				logger.info("Exception caught during syncing image status");
				logger.debug("Image status sync exception.", ex);
			}
			try
			{
				Thread.sleep(periodicity);
			}
			catch(Exception ex)
			{

			}
		}
	}

}
