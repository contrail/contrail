/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class Scheduler {

   static public String FIRSTFIT="ff";
   static public String ROUNDROBIN="rr";
   
   
   
   
   
   
   
   
   public static Map<String, String> requestDeploy(List<Integer> vmslot,String algorithm) throws SQLException {
    if(algorithm.equals(Scheduler.FIRSTFIT)){
        return deployFF(vmslot);
    }else{
        if(algorithm.equals(Scheduler.ROUNDROBIN)){
            return deployRR(vmslot);
        }else
            return null;
        
    }
       
    }
    
     public static Map<String, String> deployRR(List<Integer> vmslot) throws SQLException {
          Logger logger = Logger.getLogger("VEP.Scheduler");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        if(vmslot.size()==0){
            Map<String,String> result=new HashMap<String, String>();
            return result;
        }
        DBHandler db = new DBHandler("Scheduler", dbType);
        if (db != null) {
            VMToSchedule[] vms = new VMToSchedule[vmslot.size()];

            for (int i = 0; i < vmslot.size(); i++) {
                try {
                    ResultSet rs = db.query("select", "*", "vmslots,vmhandler,ceevmhandlers", "where vmslots.ceevmhandlerid=ceevmhandlers.id and ceevmhandlers.vmhandlerid=vmhandler.id and vmslots.id=" + vmslot.get(i));
                    int cpuF = rs.getInt("cpufreq_low");
                    int ram = rs.getInt("ram_low");
                    int disk = rs.getInt("disksize_low");
                    int cpuN = rs.getInt("corecount_low");
                    vms[i] = new VMToSchedule(vmslot.get(i), ram, cpuF, cpuN, disk);
                } catch (SQLException ex) {
                    java.util.logging.Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            ResultSet rs;
            //select all host
            
           
            //select all vmslot running or already scheduled
            ArrayList<VMToSchedule> allVMslot = new ArrayList();
            rs = db.query("select", "vmslots.id as vmid,hostid,cpufreq_low,ram_low,disksize_low,corecount_low", "vmslots,vmhandler,ceevmhandlers", "where vmslots.ceevmhandlerid=ceevmhandlers.id and ceevmhandlers.vmhandlerid=vmhandler.id and vmslots.status='RNG' or vmslots.status='SCH'");
            while (rs.next()) {
                int id = rs.getInt("vmid");
                int hostid = rs.getInt("hostid");
                int cpuF = rs.getInt("cpufreq_low");
                int ram = rs.getInt("ram_low");
                int disk = rs.getInt("disksize_low");
                int cpuN = rs.getInt("corecount_low");
                VMToSchedule newVM = new VMToSchedule(id, ram, cpuF, cpuN, disk);
                newVM.setHost(hostid);
                allVMslot.add(newVM);
            }
            //update host
            rs.close();
            ArrayList<Host> hosts=new ArrayList();
            rs=db.query("select", "*", "host", "");
            while(rs.next()){
                int ram=rs.getInt("ram");
                int hostid=rs.getInt("id");
                int cpufreq=rs.getInt("cpufreq");
                int corecount=rs.getInt("corecount");
                int disksize=rs.getInt("disksize");
                String hostname=rs.getString("hostname");
                //rs.getString("iaasid");
                Host singleHost = new Host(hostname, hostid,cpufreq,corecount,ram,disksize);
                for(int i=0;i<allVMslot.size();i++){
                    VMToSchedule app=allVMslot.get(i);
                    int hid=app.getHostid();
                    if(hid==hostid){
                        singleHost.updateCpuCores(app.getCpuN());
                        singleHost.updateDisksize(app.getDisk());
                        singleHost.updateRam(app.ram);
                    }
                }
                hosts.add(singleHost);
            }
            
          //  System.out.println("HOST NUMBER"+hosts.size());
            
            Map<String,String> result=new HashMap<String, String>();
   
            for(int i=0;i<vms.length;i++){
               VMToSchedule vm=vms[i]; 
             
                 Host singleHost=hosts.get(i%hosts.size());
                 
                // System.out.println("HOST  : id"+singleHost.getHostname()+" cpu:"+singleHost.getCorecount()+" " + singleHost.getCpufreq()  +"Hz \n DISK"+singleHost.getDisksize()+"\n RAM"+singleHost.ram);
                //System.out.println("VMslot: cpu:"+vm.getCpuN()+" "+vm.getCpuF()+"HZ \n DISK"+vm.getDisk()+"\n RAM"+vm.getRam());
                if((((singleHost.getCorecount()>=vm.getCpuN())&&(singleHost.getCpufreq()>=vm.getCpuF()))&&(singleHost.ram>=vm.getRam()))&&(singleHost.getDisksize()>=vm.getDisk())){
                    singleHost.updateRam(vm.getRam());
                    singleHost.updateDisksize(vm.getDisk());
                    singleHost.updateCpuCores(vm.getCpuN());
                    vm.setHost(singleHost.getId());
                    result.put(String.valueOf(vm.getId()), String.valueOf(vm.getHostid()));
                   
                }else 
                    return null;
            }
            return result;
        }
         
         
         
         
         
         
         return null;
     }
    
    
    
    
    
    
    
    
    
    
    public static Map<String, String> deployFF(List<Integer> vmslot) throws SQLException {
        Logger logger = Logger.getLogger("VEP.Scheduler");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        if(vmslot.size()==0){
            Map<String,String> result=new HashMap<String, String>();
            return result;
        }
        DBHandler db = new DBHandler("Scheduler", dbType);
        if (db != null) {
            VMToSchedule[] vms = new VMToSchedule[vmslot.size()];

            for (int i = 0; i < vmslot.size(); i++) {
                try {
                    ResultSet rs = db.query("select", "*", "vmslots,vmhandler,ceevmhandlers", "where vmslots.ceevmhandlerid=ceevmhandlers.id and ceevmhandlers.vmhandlerid=vmhandler.id and vmslots.id=" + vmslot.get(i));
                    int cpuF = rs.getInt("cpufreq_low");
                    int ram = rs.getInt("ram_low");
                    int disk = rs.getInt("disksize_low");
                    int cpuN = rs.getInt("corecount_low");
                    vms[i] = new VMToSchedule(vmslot.get(i), ram, cpuF, cpuN, disk);
                } catch (SQLException ex) {
                    java.util.logging.Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            ResultSet rs;
            //select all host
            
           
            //select all vmslot running or already scheduled
            ArrayList<VMToSchedule> allVMslot = new ArrayList();
            rs = db.query("select", "vmslots.id as vmid,hostid,cpufreq_low,ram_low,disksize_low,corecount_low", "vmslots,vmhandler,ceevmhandlers", "where vmslots.ceevmhandlerid=ceevmhandlers.id and ceevmhandlers.vmhandlerid=vmhandler.id and vmslots.status='RNG' or vmslots.status='SCH'");
            while (rs.next()) {
                int id = rs.getInt("vmid");
                int hostid = rs.getInt("hostid");
                int cpuF = rs.getInt("cpufreq_low");
                int ram = rs.getInt("ram_low");
                int disk = rs.getInt("disksize_low");
                int cpuN = rs.getInt("corecount_low");
                VMToSchedule newVM = new VMToSchedule(id, ram, cpuF, cpuN, disk);
                newVM.setHost(hostid);
                allVMslot.add(newVM);
            }
            //update host
            rs.close();
            ArrayList<Host> hosts=new ArrayList();
            rs=db.query("select", "*", "host", "");
            while(rs.next()){
                int ram=rs.getInt("ram");
                int hostid=rs.getInt("id");
                int cpufreq=rs.getInt("cpufreq");
                int corecount=rs.getInt("corecount");
                int disksize=rs.getInt("disksize");
                String hostname=rs.getString("hostname");
                //rs.getString("iaasid");
                Host singleHost = new Host(hostname, hostid,cpufreq,corecount,ram,disksize);
                for(int i=0;i<allVMslot.size();i++){
                    VMToSchedule app=allVMslot.get(i);
                    int hid=app.getHostid();
                    if(hid==hostid){
                        singleHost.updateCpuCores(app.getCpuN());
                        singleHost.updateDisksize(app.getDisk());
                        singleHost.updateRam(app.ram);
                    }
                }
                hosts.add(singleHost);
            }


            //order 
            //vms = selectionSort(vms);
            //declaration array host
            
            
            Map<String,String> result=new HashMap<String, String>();
            
            
            for (int i = 0; i < vms.length; i++) {
                VMToSchedule vm=vms[i];
                boolean condition=false;
                for(int j=0;j<hosts.size()&&condition==false;j++){
               
                Host singleHost=hosts.get(j);
                //System.out.println("HOST  : cpu:"+singleHost.getCorecount()+" " + singleHost.getCpufreq()  +"Hz \n DISK"+singleHost.getDisksize()+"\n RAM"+singleHost.ram);
                //System.out.println("VMslot: cpu:"+vm.getCpuN()+" "+vm.getCpuF()+"HZ \n DISK"+vm.getDisk()+"\n RAM"+vm.getRam());
                if((((singleHost.getCorecount()>=vm.getCpuN())&&(singleHost.getCpufreq()>=vm.getCpuF()))&&(singleHost.ram>=vm.getRam()))&&(singleHost.getDisksize()>=vm.getDisk())){
                    condition=true;  
                    singleHost.updateRam(vm.getRam());
                    singleHost.updateDisksize(vm.getDisk());
                    singleHost.updateCpuCores(vm.getCpuN());
                    vm.setHost(singleHost.getId());
                    result.put(String.valueOf(vm.getId()), String.valueOf(vm.getHostid()));
                   
                }
            }
                if(condition==false)
                    return null;

        }
          return result;  
            
        }
        return null;

    }


   


    static class VMToSchedule {

        private int id,ram, cpuF, cpuN, disk;
        private int hostid;

        public int getHostid() {
            return hostid;
        }

        public int getId() {
            return id;
        }

       

        public int getRam() {
            return ram;
        }

        public int getCpuF() {
            return cpuF;
        }

        public int getCpuN() {
            return cpuN;
        }

        public int getDisk() {
            return disk;
        }

        public VMToSchedule(int id,int ram, int cpuF, int cpuN, int disk) {
           this.id=id;
            this.cpuF = cpuF;
            this.cpuN = cpuN;
            this.ram = ram;
            this.disk = disk;
        }

        private void setHost(int hostid) {
            this.hostid=hostid;
        }
    }

    
    
   
    
    
    
    
  public static VMToSchedule[] selectionSort(VMToSchedule[] data){
	  int lenD = data.length;
	  int j = 0;
          VMToSchedule tmp;
	  for(int i=0;i<lenD;i++){
	    j = i;
	    for(int k = i;k<lenD;k++){
	      if(data[j].getCpuN()>data[k].getCpuN()){
	        j = k;
	      }else{
                  if(data[j].getCpuN()==data[k].getCpuN()){
                     if(data[j].getCpuF()>data[k].getCpuF()){  
                         j=k;
                     }else{
                         if(data[j].getCpuF()==data[k].getCpuF()){
                     if(data[j].getRam()>data[k].getRam()){  
                         j=k;
                            }
                         }
                     }
                        
              }
             }
	    }
	    tmp = data[i];
	    data[i] = data[j];
	    data[j] = tmp;
	  }
	  return data;
	} 
    
    
    
    
    
    
    static private class Host{

        public int getId() {
            return id;
        }

        public int getCpufreq() {
            return cpufreq;
        }

        public int getCorecount() {
            return corecount;
        }

        public int getRam() {
            return ram;
        }

        public int getDisksize() {
            return disksize;
        }

        public String getIaasId() {
            return iaasId;
        }

        public String getHostname() {
            return hostname;
        }
        int id,cpufreq,corecount,ram,disksize;
        String iaasId,hostname;
        public Host(String hostname, int id,int cpufreq,int corecount,int ram,int disksize){
            this.hostname=hostname;
            this.id=id;
            this.ram=ram;
            this.corecount=corecount;
            this.cpufreq=cpufreq;
            this.disksize=disksize;
        }
        public  void updateRam(int ram){
            this.ram=this.ram-ram;
        }
        public void updateCpuCores(int corecount){
            this.corecount-=corecount;
        }
        public void updateDisksize(int disksize){
            this.disksize-=disksize;
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    











}