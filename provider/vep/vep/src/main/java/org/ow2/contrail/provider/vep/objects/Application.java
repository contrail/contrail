/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.common.implementation.ovf.SharedDisk;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import org.ow2.contrail.provider.vep.App;
import org.ow2.contrail.provider.vep.CEEDeploymentThread;
import org.ow2.contrail.provider.vep.DiskManagement;
import org.ow2.contrail.provider.vep.One3XMLRPCHandler;
import org.ow2.contrail.provider.vep.GAFSClient.GAFSClient;
import org.ow2.contrail.provider.vep.GAFSClient.GAFSConnector;
import org.ow2.contrail.provider.vep.SchedulerClient.ConstraintTranslator;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.ow2.contrail.provider.vep.SchedulerClient.SchedulerClient;
import org.ow2.contrail.provider.vep.objects.CEE.ConstraintMap;
import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application extends VEPObject {
	
	private String state = "";
	private String ceeId;
	private String applicationId;
	private String userid;
	private String username;
	private ArrayList<Reservation> reservations = new ArrayList<Reservation>();
	private ArrayList<Integer> ovfDBIDs = new ArrayList<Integer>();
	private ArrayList<String> ovfs = new ArrayList<String>();
	private ArrayList<VEPVirtualMachine> vms = new ArrayList<VEPVirtualMachine>();
         ArrayList<CEE.ConstraintMap> cntr=new ArrayList<ConstraintMap>();
		private ArrayList<ConstraintMap> storageConstraintsMap;
	
	public Application(boolean instantiate) {
		super("Application", instantiate);
		this.setResourceUri("VEP/Application");
	}

	public Application(String ceeId, String applicationId) {
		super("Application", true);
		this.ceeId = ceeId; //not useful?
		this.applicationId = applicationId;
		this.setId("/cee/"+ceeId+"/application/"+applicationId);
		this.setResourceUri("VEP/Application");
	}
	
	//for Application Creation
	public Application() {
		super("Application", true);
		this.setResourceUri("VEP/Application");
	}
	public boolean retrieveApplication() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "application", "where id="+applicationId);
		if (rs.next())
		{
			this.setName(rs.getString("name"));
			success = true;
		}
		rs = db.query("select", "user.id as uid, username","user,cee","where cee.id="+ceeId+" and user.id=cee.userid");
		if(rs.next())
		{
			this.setUserid(String.valueOf(rs.getInt("uid")));
			this.setUsername(rs.getString("username"));
		}
		rs = db.query("select", "id, descp", "ovf", "where applicationid="+applicationId);
		while(rs.next())
		{
			ovfs.add(rs.getString("descp"));
			ovfDBIDs.add(rs.getInt("id"));
		}
		
		rs = db.query("select", "vmslots.id as vid,vmslots.status as vstatus", "vmslots,ovfmap,ovf", "where ovf.applicationid="+applicationId+" and ovfmap.ovfcontainerid=ovf.id and vmslots.ovfmapid=ovfmap.id");
		while(rs.next())
		{
			VEPVirtualMachine v = new VEPVirtualMachine(true);
			v.setState(rs.getString("vstatus"));
			int vmId = rs.getInt("vid");
			v.setCeeId(ceeId);
			v.setApplicationId(applicationId);
			v.setVmId(String.valueOf(vmId));
			v.setId(getId()+"/vm/"+vmId);
			v.setUserid(userid);
			vms.add(v);
		}
		rs = db.query("select", "reservations.id as rid, reservations.ovfmapid as oid, ovfmap.ovfid as ovfitem", "reservations,ovfmap", "where reservations.applicationid="+applicationId+" and ovfmap.id=reservations.ovfmapid");
		while(rs.next())
		{
			Reservation r = new Reservation(rs.getString("ovfitem"), 1, null);//TODO:Fix the value for the number of reserved slots
			r.free = 1; //TODO:Fix the number of free slots atm
                        
                        
			r.id = rs.getInt("rid");
			r.ovfmapid = rs.getInt("oid");
			reservations.add(r);
		}
                
                ArrayList<String> vmslotTable=new ArrayList<String>();
                rs=db.query("select","vmslots.id as vmid,vmslots.ovfmapid as ovfidMap","vmslots","");
                while(rs.next()){
                       vmslotTable.add(rs.getString("vmid")+"#"+rs.getString("ovfidMap"));
                }
                
                
                
                rs=db.query("select", "ovfmapconstraint.param as par,ovfmapconstraint.ovfmapid as vs, ceeconstraintslist.id as cnID,constraints.descp as typeC ", "ovfmapconstraint,constraints,ceeconstraints,ceeconstraintslist,ovfmap", "where ovfmapconstraint.ceeconstraintslistid=ceeconstraintslist.id and ceeconstraintslist.ceeconstraintsid=ceeconstraints.id and ceeconstraints.constrainttype=constraints.id and ovfmapconstraint.ovfmapid=ovfmap.id and ovfmap.id in (select ovfmap.id from ovfmap,ovf where ovfmap.ovfcontainerid=ovf.id and ovf.applicationid="+this.applicationId+")");
                //ArrayList<CEE.ConstraintMap> cntr=new ArrayList<ConstraintMap>();
                ArrayList<CEE.ConstraintMap> cntrVS=new ArrayList<ConstraintMap>();
                CEE cee=new CEE();
                while(rs.next()){
                    String cntrID=rs.getString("cnID");
                    String cnType=rs.getString("typeC");
                    boolean found=false;
                    for(int i=0;i<cntr.size();i++){
                        if(cntrID.equalsIgnoreCase(cntrVS.get(i).id)){
                            
                            for(String check:vmslotTable){
                                  String[] doublevalue=check.split("#");
                                  if(doublevalue[1].equalsIgnoreCase(rs.getString("vs"))) 
                                      cntrVS.get(i).on.add(doublevalue[0]);
          
                              }
                             found=true;
                             break;
                            }
                           
                        
                    }
                    if(!found){
                              ArrayList<String> vs=new ArrayList<String>();
                              for(String check:vmslotTable){
                            	  
                                  String[] doublevalue=check.split("#");
                                  logger.debug(doublevalue[0]);
                                  if(doublevalue[1].equalsIgnoreCase(rs.getString("vs"))) 
                                      vs.add(doublevalue[0]);
                                          
                              }
                              //vs.add(rs.getString("vs"));
                              String parameter=rs.getString("par");              
                              cntrVS.add(cee.new ConstraintMap(cntrID,parameter,vs,cnType));
                    }
                }
                this.cntr=cntrVS;
                
                
                
		return success;
	}

	public void setUsername(String username) {
		this.username = username;	
	}
	
	private String getUsername() {
		return username;	
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void checkParameters() {
		//checks the ovf
		if(ovfs != null && !isError())
		{
			// Check ovf against its schema there
		}
		if(vms != null && !isError())
		{
			//check existence of VS references in the ovfmap table or one of the ovf uploaded at the same time (if not in the table AND this.ovfs==null, then error, IF this.ovfs!=null, check inside these ovfs)
		}
		
	}

	//Application contains VM objects and OVFs and a list of reservations
	public void registerToDb() throws SQLException {
		ResultSet rs;
       
		if(applicationId == null)
		{
			rs = db.query("select", "id", "application", "where ceeid="+this.ceeId+" and name='"+this.getName()+"'");
			if(rs.next())
			{
				this.applicationId = String.valueOf(rs.getInt("id"));
			}
			else
			{
				//app does not exist yet, create it
				int key = db.insertAndGetKey("application(name, ceeid)","('"+this.getName()+"',"+this.ceeId+")");
				if(key != 0)
				{
					logger.info("Created a new application inside CEE "+ceeId);
					this.applicationId = String.valueOf(key);
				} else 
					this.setError("Could not create a new application inside CEE "+ceeId);
				
			}
		} else {
			rs = db.query("select", "id", "application", "where ceeid="+this.ceeId+" and id="+this.applicationId);
			if(!rs.next()) setError("Application Id "+this.applicationId+" does not exist for cee "+this.ceeId);
		}

		if(ovfs != null && ovfs.size()>0 && !isError())
		{
			//Delete current ovfs
			//TODO: replace the static one version and admin uname by values from the properties or from the db
			String version = "3.6";
			String adminuname = "oneadmin";
			boolean status = true;
			rs = db.query("select", "*", "ovf", "WHERE applicationid=" + this.applicationId);
			//Deleting old ovfs
			ArrayList<Integer> previousovfsids = new ArrayList<Integer>();
			while(rs.next() && status)
			{
				int id = rs.getInt("id");
				db.delete("ovf", "where id="+id);
				db.update("ovfmap", "status='OR'", "WHERE ovfcontainerid=" + id); //setting all entries to OR
				previousovfsids.add(id);
			}
			for(String ovf : ovfs)
			{
				//add the ovf to the table
				//we never update an existing ovf and its corresponding ovfmap items, when an update comes we delete the ovf and register a new one
				//TODO:Change this behaviour?
				
				try {
					OpenNebulaOVFParser parser = new OpenNebulaOVFParser(ovf, version, adminuname);
					String[] ovfIds = parser.getIDs();
					//INSERT
					
					int ovfkey = db.insertAndGetKey("ovf(applicationid, descp)", "("+this.applicationId+", '"+ovf.trim()+"')");//TODO:Fail if a ' is in the ovf
										
					
					for(int i=0; i<ovfIds.length && status; i++)
					{
						//getting the OS disk details for this virtual system
						String[] diskIds = parser.getVMDisksId(ovfIds[i]);   
						int osDiskId = -1;
						for(int j=0; j<diskIds.length; j++)
						{
							String ovfDiskPath = parser.getImageDiskPath(ovfIds[i], diskIds[j]);
							String ovfDiskName = diskIds[j];
							//System.err.println("For virtual system: " + ovfIds[i] + " - disk name: " + ovfDiskName + ", path=" + ovfDiskPath);
							//now checking if this disk entry is in the osdisk table
							rs = db.query("select", "*", "osdisk", "WHERE ceeid=" + ceeId + " AND ovfimagename='" + ovfDiskName + "' AND ovfimagepath='" + ovfDiskPath + "'");
							if(rs.next())
							{
								osDiskId = rs.getInt("id");
								//if status = OR then set status as INI
								String stat = rs.getString("status");
								if(stat.equalsIgnoreCase("OR"))
									db.update("osdisk", "status='INI', usecount=0", "WHERE id=" + osDiskId);
							}
							else
							{
								//create a placeholder entry in the osdisk table
								rs = db.query("select", "max(id)", "osdisk", "");
			                    osDiskId = 1;
			                    if(rs.next()) osDiskId = rs.getInt(1) + 1;
			                    rs.close();
			                    status = db.insert("osdisk", "(" + osDiskId + ", '', '" + ovfDiskPath + "', '" + ovfDiskName + "', 'INI', 0, " + ceeId + ")"); //for the moment setting diskcount to 0
			                    logger.info("Added a new OS Disk entry for VirtualSystem: " + ovfIds[i] + ", OVFDiskPath=" + ovfDiskPath);
							}
							break; //to prevent processing of more than 1 OS disk per v-system
						}
						//the above code will break if a virtual system actually contains two OS disks, so a fix has to be designed in the parser
						
						
						//as the code is designed, there should always be no existing ovfmap item at this step, code kept for retrocompatibility
						rs = db.query("select", "*", "ovfmap", "WHERE ovfcontainerid=" + ovfkey + " AND ovfid='" + ovfIds[i] + "'");
						int ovfmapId = -1;
						if(!rs.next())
						{
							//create a new OVFmap entry
							rs = db.query("select", "max(id)", "ovfmap", "");
		                    ovfmapId = 1;
		                    if(rs.next()) ovfmapId = rs.getInt(1) + 1;
		                    rs.close();
		                    
		                    status = db.insert("ovfmap", "(" + ovfmapId + ", " + ovfkey + ", '" + ovfIds[i] + "', " + 0 + ", -1, 'RDY', '', '', NULL, NULL, NULL)"); //for the moment setting osdisk id to -1 FIX LATER

		                    logger.info("Added a new OVF Map entry for VirtualSystem: " + ovfIds[i]);	     
		                    
		                    //add context data
		                    String context = parser.getContext(ovfIds[i]);
		            		if(context.trim().length() == 0)
		            			db.updateContext(ovfmapId, "", "ovfmap");
		            		else
		            			db.updateContext(ovfmapId, context.trim(), "ovfmap");
		                    
		                    
		                    //make this new ovfmap entry point to the osdiskid
		                    rs = db.query("select", "*", "osdisk", "WHERE id=" + osDiskId);
							if(rs.next())
							{
								int usecount = rs.getInt("usecount") + 1;
								status = db.update("osdisk", "usecount=" + usecount, "WHERE id=" + osDiskId);
							}
							status = db.update("ovfmap", "osdiskid==" + osDiskId, "WHERE id=" + ovfmapId);
							logger.info("OS Disk reference for this virtual system - " + ovfIds[i] + " in CEE " + ceeId + " was updated.");	
						
							
							//Check for Shared Storage to be created
							//Cross-check with the CEE Storage Handlers to verify that user has rights to create shared storage volumes
							Collection<SharedDisk> sds = parser.getSharedDisks(ovfIds[i]);
							if(sds.size()>0)
							{
								//if there is any SharedDisk, make a check (save as a boolean) to be sure that there is a GAFS storage handler in this CEE.
								//may not be useful if there is no disk to create
								//we take the first registered GAFS handler, how to handle multiple GAFS handlers?
								int gafsHandlerId = -1;
								String endpoint = "";
								String endpointMount="";
								GAFSClient gfs = null;
								rs = db.query("select", "ceestoragehandlers.id as cid, endpoint, mountendpoint", "ceestoragehandlers, storagehandler, storage", "where storage.storagetypeid="+App.GAFS_SHARE+" and ceestoragehandlers.ceeid="+ceeId+" and storagehandler.storageid=storage.id and ceestoragehandlers.storagehandlerid=storagehandler.id");
								if(rs.next()) {gafsHandlerId=rs.getInt("cid"); endpoint = rs.getString("endpoint"); endpointMount = rs.getString("mountendpoint");}
								boolean ovfMapUsesStorageHandler = false; // to check if an entry linking ceestoragehandler to ovfmap was created
								int ceestoragehandlerlistid = -1;
								for(SharedDisk sd : sds)
								{
									if(sd.getFile() == null)
									{
										//no fileRef saved, we need to create a volume
										if(gafsHandlerId != -1)
										{
											if(!ovfMapUsesStorageHandler)
											{
												ceestoragehandlerlistid = db.insertAndGetKey("ceestoragehandlerlist(ceestoragehandlerid,ovfmapid)", "("+gafsHandlerId+","+ovfmapId+")");
												ovfMapUsesStorageHandler = true;
											}
											String name = applicationId+"_"+sd.getId();
											rs = db.query("select", "*", "ceeregisteredstorage", "where ceestoragehandlerid="+gafsHandlerId+" and name='"+name+"'");
											//if nothing is found, the volume has not been created yet, otherwise another ovfmap already uses it.
											if(rs.next())
											{
												//increment usecount if existing
												int id = rs.getInt("id");
												int usecount = rs.getInt("usecount");
												db.update("ceeregisteredstorage", "usecount="+usecount++, "where id="+id);
                                                                                                if (context.trim().length() != 0) {
															//gafsurl+name
															context+= " GAFS_VOLUME_URL= \"" +endpointMount+"/"+rs.getString("name") +"\" ," ;
															db.updateContext(ovfmapId, context.trim(), "ovfmap");
														} else {
															context= " GAFS_VOLUME_URL= \"" +endpointMount+"/"+rs.getString("name") +"\" ," ;
															db.updateContext(ovfmapId, context.trim(), "ovfmap");
														}
											} else {
												//otherwise create with usecount = 1
												//retrieve constraints for this volume
												int replicas = -1;
												List<String> inCountry = new ArrayList<String>();
												List<String> outCountry = new ArrayList<String>();
												
												for(ConstraintMap cm : storageConstraintsMap)
												{
													if(cm.on.get(0).equals(sd.getId()))
													{
														rs = db.query("select", "descp", "constraints", "where id="+cm.id);
														if(rs.next())
														{
															String descp = rs.getString("descp");
															if(descp.equalsIgnoreCase("IN COUNTRY"))
															{
																if(!inCountry.contains(cm.param))
																	inCountry.add(cm.param);
															} else if(descp.equalsIgnoreCase("NOT IN COUNTRY"))
															{
																if(!outCountry.contains(cm.param))
																	outCountry.add(cm.param);
															} else if(descp.equalsIgnoreCase("REPLICAS"))
															{
																replicas = Integer.parseInt(cm.param);
															}
														}
													}
												}
												
                                                                                                if(gfs == null) gfs=new GAFSClient(endpoint,true);
												boolean created = GAFSConnector.createVolume(name,username).post(gfs);
											
												//add context data
												//context = parser.getContext(ovfIds[i]);

												if(created){
													//GAFSConnector.setVolumePrivilegies(name,userid, "").post(gfs);
													//for now we take the first element of the "NOT IN COUNTRY3 list as it's only what is supported by GAFS
													boolean authorized = true;
													if(inCountry.size()>0)
														authorized = GAFSConnector.setPolicies(name, GAFSConnector.IN_COUNTRY, inCountry.get(0)).post(gfs);
													else if(outCountry.size()>0)
														authorized = GAFSConnector.setPolicies(name, GAFSConnector.NOT_IN_COUNTRY, outCountry.get(0)).post(gfs);
													if(!authorized)
													{
														GAFSConnector.deleteVolume(name, "").post(gfs);
                                                                                                                setError("Storage Constraint not satisfied");
                                                                                                                //throw new SQLException ("Storage Constraints not satisfied");
                                                                                                                //break;
                                                                                                                
													} else {
														db.insert("ceeregisteredstorage(ceestoragehandlerid,name,usecount)", "("+gafsHandlerId+",'"+name+"',1)");
														if (context.trim().length() != 0) {
															//gafsurl+name
															context+= " GAFS_VOLUME_URL= \"" +endpointMount+"/"+name +"\" ," ;
															db.updateContext(ovfmapId, context.trim(), "ovfmap");
														} else {
															context= " GAFS_VOLUME_URL= \"" +endpointMount+"/"+name +"\" ," ;
															db.updateContext(ovfmapId, context.trim(), "ovfmap");
														}
													}
												} else {
													setError("Error while creating volume name: "+name+" on endpoint: "+endpoint);
													break;
												}
											}
											db.insert("ovfmapstorageinfo(ceestoragehandlerlistid,ovfmapid)", "("+ceestoragehandlerlistid+","+ovfmapId+")");
										} else {
											setError("Can not create a volume: CEE has no GAFS handler");
											break;
										}
									}
								}
							}


							
							//if vin networks are registered for this cee, all of them are also registered in the ceenethandlerlist 
							//TODO:Breaks compatibility with REST GUI
							rs = db.query("select", "*", "ceenethandlers", "where ceeid="+this.ceeId);
							logger.debug("Looking for VIN Networks");
							if(rs.next())
							{
								ArrayList<Integer> hids = new ArrayList<Integer>();
								rs = db.query("select", "ceenethandlers.id as hid", "ceenethandlers, vnethandler" , "where ceenethandlers.ceeid="+this.ceeId+" and vnethandler.id=ceenethandlers.vnethandlerid and vnethandler.name='vin'");
								while(rs.next())
								{
									int hid = rs.getInt("hid");
									hids.add(hid);
									logger.debug("Found a VIN Network:"+hid);
								}
								for(Integer hid : hids)
								{
									db.insert("ceenethandlerlist(ovfmapid,ceenethandlerid)","("+ovfmapId+","+hid+")");
								}
							}
						
						} 
						
		/**
	//TODO:Network section of ovf ignored for now, add something there later
						//now find the corresponding network and storage handlers for this OVFID system and link or update those db entries
						//also update and verify the OS image for the ovfmap entry ...
						int[] vnetHandlersIdList = findBestVNetHandlerMatch(ovfIds[i], ovfDesc, ceeId, nethandlerMatrix);
						//first delete all old ceenethandlerlist entries for this ovfmap
						status = db.delete("ceenethandlerlist", "WHERE ovfmapid=" + ovfmapId);
						for(int j=0; status && vnetHandlersIdList!=null && j<vnetHandlersIdList.length; j++)
						{
							//System.err.println("received: " + vnetHandlersIdList[j] + " for VirtualSystem " + ovfIds[i]);
							if(vnetHandlersIdList[j] != -1)
							{	
								//create a new ceenethandlerlist entry for this OVFmap entry
								rs = db.query("select", "max(id)", "ceenethandlerlist", "");
			                    int netId = 1;
			                    if(rs.next()) netId = rs.getInt(1) + 1;
			                    rs.close();
			                    status = db.insert("ceenethandlerlist", "(" + netId + ", " + ovfmapId + ", " + vnetHandlersIdList[j] + ")");
			                    logger.info("Added a new CEENetHandlerList entry for VirtualSystem: " + ovfIds[i] + ", VNetHanlderId=" + vnetHandlersIdList[j]);
							}
						}
						int[] storageHandlersIdList = findBestStorageHandlerMatch(ovfIds[i], ovfDesc, ceeId);
		*/
				
					}
					
					//delete all ORphaned ovfmap from previous ovfs
					for(int previousid : previousovfsids)
					{
						//now for all the entries whose status = OR delete them, remove corresponding ceenethandlerlist, ceestoragehandlerlist, osdisk entries, and set corresponding VMSLOT entries link to -1
						rs = db.query("select", "id, osdiskid", "ovfmap", "WHERE ovfcontainerid=" + previousid + " AND status='OR'");
						int count1 = 0;
						while(rs.next())
						{
							count1++;
							int ovfmapID = rs.getInt("id");
							int diskID = rs.getInt("osdiskid");
							if(status && diskID != -1)
							{
								rs = db.query("select", "usecount", "osdisk", "WHERE id=" + diskID);
								int newCount = rs.getInt("usecount") - 1;
								if(newCount == 0) status = db.update("osdisk", "status='OR', usecount=0", "WHERE id=" + diskID); //setting this entry status to OR
								else
									status = db.update("osdisk", "usecount=" + newCount, "WHERE id=" + diskID); //reducing the usecount value by 1
							}
							
							//delete all vnethandlerlist entries linked to this entry
							if(status) status = db.delete("ceenethandlerlist", "WHERE ovfmapid=" + ovfmapID);
							//similarly clear the cee storagehandlerslist entries if any
							if(status) status = db.delete("ceestoragehandlerlist", "WHERE ovfmapid=" + ovfmapID);
							//set VMslot ovfmapid to -1 for this entry
							if(status) status = db.update("vmslots", "ovfmapid=-1", "WHERE ovfmapid=" + ovfmapID);
							
							//now resetting the sql query
							rs = db.query("select", "id, osdiskid", "ovfmap", "WHERE ovfcontainerid=" + previousid + " AND status='OR'");
							int counter=0;
							while(counter < count1) { rs.next(); counter++; }
						}
						//now simply delete all OR entries
						if(status) status = db.delete("ovfmap", "WHERE ovfcontainerid=" + previousid + " AND status='OR'");
					}

					//now call the disk management thread to manage the disk download and storage in local folder
					DiskManagement dprocess = new DiskManagement(Integer.parseInt(ceeId), this.userid + "-" + ceeId + "-diskmove", String.valueOf(this.userid));
					dprocess.start();
					VEPHelperMethods.addService(this.userid + "-" + ceeId + "-diskmove", dprocess);	//so that the thread is not killed when this process goes out of scope


				} catch (MalformedOVFException e) {
					// TODO Auto-generated catch block
					this.setError("Detected a malformed OVF");
					logger.debug(e);
				} catch (Exception e) {
					this.setError("Unknown error while parsing an ovf");
					logger.debug("unknozn:",e);
				}
			}
		}
		/*if(reservations != null && reservations.size() > 0 && !isError())
		{
			for(Reservation res : reservations)
			{
				
			}
		}*/
		if(vms != null && vms.size() > 0 && !isError())
		{
			String toDelete = ""; 
			//skip existing vms and register new ones
			for(VEPVirtualMachine v : vms)
			{
				//retrieve vm from name:TODO, vmid is not filled at that time
				v.setCeeId(ceeId);
				v.setApplicationId(applicationId);
				v.retrieveIdFromName();
				if(v.getVmId() != null)
				{
					//If there is an id, it's an update of an already existing application, do not modify this vm slot
					toDelete += v.getVmId()+",";
				} else {
					v.registerToDb();
					if(v.isError())
					{
						setError("Error while trying to register VMSlot with name: "+v.getName()+". The error is:"+v.getError());
						break;
					} else {
						toDelete+=v.getVmId()+",";
					}
				}
			} 
			//delete remaining unlisted vms
			if(!isError() && toDelete.length()>0)
			{
				ArrayList<Integer> slots = new ArrayList<Integer>();
				toDelete = toDelete.substring(0, toDelete.length()-1);
				rs = db.query("select", "vmslots.id as vid", "vmslots,ovfmap,ovf", "where ovf.applicationid="+applicationId+" and ovfmap.ovfcontainerid=ovf.id and vmslots.ovfmapid=ovfmap.id and vmslots.id not in ("+toDelete+")");
				while(rs.next())
				{
					slots.add(rs.getInt("vid"));
				}
				for(Integer vmId : slots)
				{
					VEPVirtualMachine v = new VEPVirtualMachine(true);
					v.setVmId(String.valueOf(vmId));
					v.retrieveVirtualMachine();
					v.delete();
				}
			}
		} else if(vms != null && vms.size() == 0 && !isError())
		{
			//delete all vms in the db
			ArrayList<Integer> slots = new ArrayList<Integer>();
			rs = db.query("select", "vmslots.id as vid", "vmslots,ovfmap,ovf", "where ovf.applicationid="+applicationId+" and ovfmap.ovfcontainerid=ovf.id and vmslots.ovfmapid=ovfmap.id");
			while(rs.next())
			{
				slots.add(rs.getInt("vid"));
			}
			for(Integer vmId : slots)
			{
				VEPVirtualMachine v = new VEPVirtualMachine(true);
				v.setVmId(String.valueOf(vmId));
				v.delete();
			}
		}
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public ArrayList<String> getOvfs() {
		return ovfs;
	}

	public String getCeeId() {
		return ceeId;
	}

	public void setCeeId(String ceeId) {
		this.ceeId = ceeId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	public void setStorageConstraints(
			ArrayList<ConstraintMap> mappedStorageConstraints) {
            if(mappedStorageConstraints==null){
                this.storageConstraintsMap = new ArrayList<ConstraintMap>();
            }else
		this.storageConstraintsMap = mappedStorageConstraints;
	}

    void checkStorageConstraints() {
         ResultSet rs;   
         String  endpoint = null; String volumename="testVolume";
            List<String> inCountry = new ArrayList<String>();
            List<String> outCountry = new ArrayList<String>();
          try {
        rs = db.query("select", "ceestoragehandlers.id as cid, endpoint, mountendpoint", "ceestoragehandlers, storagehandler, storage", "where storage.storagetypeid="+App.GAFS_SHARE+" and ceestoragehandlers.ceeid="+ceeId+" and storagehandler.storageid=storage.id and ceestoragehandlers.storagehandlerid=storagehandler.id");
								if(rs.next()) { endpoint=rs.getString("endpoint");}else{ return;};
            GAFSClient gfs=new GAFSClient(endpoint,true);
           
           
            int replicas;
            for (ConstraintMap cm : storageConstraintsMap) {
                //if (cm.on.get(0).equals(sd.getId())) {
                    rs = db.query("select", "descp", "constraints", "where id=" + cm.id);
           
                if (rs.next()) {
                    String descp = rs.getString("descp");
                    if (descp.equalsIgnoreCase("IN COUNTRY")) {
                        if (!inCountry.contains(cm.param)) {
                            inCountry.add(cm.param);
                        }
                    } else if (descp.equalsIgnoreCase("NOT IN COUNTRY")) {
                        if (!outCountry.contains(cm.param)) {
                            outCountry.add(cm.param);
                        }
                    } else if (descp.equalsIgnoreCase("REPLICAS")) {
                        replicas = Integer.parseInt(cm.param);
                    }
                }
            
                
            }
           
            boolean authorized = true;
            if (inCountry.size() > 0) {
                authorized = GAFSConnector.setPolicies(volumename, GAFSConnector.IN_COUNTRY, inCountry.get(0)).post(gfs);
            } else if (outCountry.size() > 0) {
                authorized = GAFSConnector.setPolicies(volumename, GAFSConnector.NOT_IN_COUNTRY, outCountry.get(0)).post(gfs);
            }
            if (!authorized) {
                //GAFSConnector.deleteVolume(name, "").post(gfs);
                setError("Storage Constraint not satisfied");
                return;
                //throw new SQLException ("Storage Constraints not satisfied");
                //break;

            } else {
            }
             } catch (SQLException ex) {
                this.setError("SQL Execption");
            }
        
    }
	
	public class Reservation{
		public final String ovfmapname;//Name of the VS
		public int count; //Max reserved slots
		public String endDate;//End date of reservation
		public int free;//Free slots at the moment 
		public int id;//id in the db if already registered
		public int ovfmapid;//id of the ovfmap entry ovfmapname refers to (if it is already registered)
		public Reservation(String ovfmapname, int count, String endDate){
			this.count = count;
			this.ovfmapname = ovfmapname;
			this.endDate = endDate;
		}
	}

	public ArrayList<Reservation> getReservations() {
		return reservations;
	}
        
        public ArrayList<Reservation> getReservationFromScheduler(){
            if(reservations!=null){ 
            HashMap<VEPVirtualMachine,Integer> reservationList=SchedulerClient.getReservationInfo(this.applicationId);
            if(reservationList==null)
                return reservations;
              for (Map.Entry<VEPVirtualMachine, Integer> entry : reservationList.entrySet()) {
                VEPVirtualMachine vm = entry.getKey();
                //find right Reservation
                for(Reservation r:reservations){
                    if(r.id==vm.reservationId){
                        r.free=entry.getValue();
                        r.count=vm.countToDeploy;
                    }
                }
                
                
            }}
             return reservations;
        }

	public void setReservations(ArrayList<Reservation> reservations) {
		this.reservations = reservations;
	}

	public ArrayList<VEPVirtualMachine> getVms() {
		return vms;
	}

	public void setVms(ArrayList<VEPVirtualMachine> vms) {
		this.vms = vms;
	}

	/**
	 * Action router of the application
	 * @param action
	 * @throws SQLException 
	 */
	public void doAction(String action) throws SQLException {
		if(action.equalsIgnoreCase("start"))
			start();
		else if(action.equalsIgnoreCase("stop"))
			stop();
		else if(action.equalsIgnoreCase("delete"))
			delete();
		else
			setError("Action: '"+action+"' does not exist or is not implemented yet.");
	}

	/**
	 * Stop the application's vms
	 * @throws SQLException 
	 */
	private void stop() throws SQLException {
		retrieveApplication();

		for(VEPVirtualMachine v: vms)
		{
			logger.debug("STOPPING VM "+v.getVmId());
			v.retrieveVirtualMachine();
			v.doAction("stop");
			if(v.isError())
			{
				setError("Error while shutting down a VM: "+v.getError());
				break;
			}
		}
		/*
		//get the list of all VM slots belonging to this CEE that are in state DEP=deployed
		rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId + " AND status='DEP'");
		int count = 0;
		while(rs.next() && status)
		{
			int vmslotId = rs.getInt("id");
			int hostId = rs.getInt("hostid");
			if(hostId == -1) 
			{
				message += "VM slot " + vmslotId + " VM is not scheduled on any host. ";
			}
			else
			{
				int iaasId = -1;
				try
				{
					iaasId = Integer.parseInt(rs.getString("iaasid"));
				}
				catch(Exception ex)
				{
					iaasId = -1; //VM not running or some thing else went wrong
				}
				if(iaasId == -1)
				{
					message += "VM slot " + vmslotId + " VM cloud ID was not found. ";
					//not running so no need to stop the associated VM
				}
				else
				{
					rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
					if(rs.next())
					{
						int cloudTypeId = rs.getInt("cloudtypeid");
						rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
						if(rs.next())
						{
							String headIP = rs.getString("headip");
							int headPort = rs.getInt("headport");
							String cloudVersion = rs.getString("version");
							int type = rs.getInt("typeid");
							rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
							if(rs.next())
							{
								String cloudType = rs.getString("name");
								rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userId + " AND cloudtype=" + cloudTypeId);
								if(rs.next())
								{
									String iaasUser = rs.getString("iaasuser");
									String iaasPass = rs.getString("iaaspass");
									if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
									{
										One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
										try
										{
											status = one3.shutdownVM(iaasId);
											if(!status)
											{
												message += "Probably the VM state does not permit SHUTDOWN operation. ";
											}
										}
										catch(Exception ex)
										{
											//warn system admin of a potential runaway VM
											try
											{
												int msgid = 1;
												rs = db.query("select", "max(id)", "errormessagelist", "");
												if(rs.next())
												{
													msgid = rs.getInt(1) + 1;
												}
												db.insert("errormessagelist", "(" + msgid + ", 'Possible Runaway VM', 'The VM IaaS Id is " + iaasId + " in cloud with id " + cloudTypeId + ". Exception message: " + ex.getMessage() + "', '" + username + "', 'WARN', 'vmslots', '" + System.currentTimeMillis() + "', 0)");
											}
											catch(Exception ex1)
											{
												
											}
											status = false;
											message += "Exception caught during VM shutdown operation. ";
											logger.info("Exception caught during VM shutdown operation for VM slot: " + vmslotId);
											//if(logger.isDebugEnabled())
											//	ex.printStackTrace(System.err);
											logger.debug("Exception caught ", ex);
										}
									}
									else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
									{
										status = false;
										message += "This cloud type is currently not supported. ";
										
										
									}
								}
								else
								{
									status = false;
									message += "Corresponding cloud account for this user is not found. ";
								}
							}
							else
							{
								
							}
						}
						else
						{
							
						}
					}
					else
					{
						
					}
				}
			}
			status = db.update("vmslots", "status='INI', hostid=-1", "WHERE id=" + vmslotId);
			//now resetting the query
			rs = db.query("select", "id, hostid, iaasid", "vmslots", "WHERE ceeid=" + ceeId + " AND status='DEP'");
		}*/
	}

	/**
	 * Start the application's vms
	 * @throws SQLException 
	 */
	private void start() throws SQLException {
		logger.debug("Starting Application");
		ResultSet rs;
		retrieveApplication();
		/*HashMap<Integer, Integer> reservationsMap = new HashMap<Integer, Integer>(); // Stores the id of the reservations in a <ovfmap id:reservations id> map, Easier to fill the VMs info that way.
		if(!reservations.isEmpty()){
			for(Reservation r : reservations)
			{
				reservationsMap.put(r.ovfmapid, r.id);
			}
		}*/
		for(VEPVirtualMachine v : vms)
		{
			v.instantiate();
			v.retrieveVirtualMachine();//fill the vm's data on cpufreq, corecount, ram
			/*if(reservationsMap.containsKey(v.getOvfmapid())) //fill the reservationid of the vm object if there is an already existing reservation for its ovfmap entry
				v.reservationId = reservationsMap.get(v.getOvfmapid());*/
		}
		if(!isError())
		{
			//remove VMs where state =RN or =DEP from the vms collection
			for(Iterator<VEPVirtualMachine> it = vms.iterator(); it.hasNext();)
			{
				String st = it.next().getState();
				if (st.equals("RN")||st.equals("DEP"))
					it.remove();
			}
			//Deploy request to the Scheduler   
			//convert constraint in vms constraints!!
			logger.debug("schedulerClient  first constraint:"+this.cntr.size());

			try {
				//TODO:uncomment next line, remove following - DONE and COMMENTED NEXT
				SchedulerClient.deploy(vms,SchedulerClient.toSchedulerConstraints(this.cntr));
				//vms.get(0).setHostid(4);
				
				//				SchedulerClient.deploy(vms);
				//now loop on the vms, if one host is null, do not deploy, otherwise go on deployment

				for(VEPVirtualMachine v : vms)
				{
					if(v.getHostid() == 0)
					{
						setError("The Scheduler could not deploy all the required VMs. First VM to fail's name is: "+v.getName());
						break;
					} //else {
					//update the vmslot with the hostid
					//db.update("vmslots", "hostid="+v.getHostid(), "where id="+v.getVmId());
					//}
				}
			} catch (Exception ex)
			{
				logger.debug("Exception:", ex);
				setError("Could not contact scheduler for scheduling request");
			}
			//this should be at the end of the function in case of deploying error
			if(isError()){
				for(VEPVirtualMachine v : vms)
				{ 
					//all the vm should be also stopped
					logger.debug("VM TO DELETE ID:-->"+v.getVmId());
					int toDelete=Integer.parseInt(v.getVmId());
					SchedulerClient.deleteVM(toDelete);
					logger.debug("VM DELETED ID:-->"+v.getVmId());
				}
			}
			//TODO: REMANGE THE ERROR
			if(!isError())
			{
				//All vms should now have an host id, try to deploy them
				for(VEPVirtualMachine v : vms)
				{
					db.update("vmslots", "hostid="+v.getHostid(), "where id="+v.getVmId());
					v.updateDiskMap();
					if(v.isError())
					{
						setError(v.getError());
						break;
					}
				}
				//now actually deploy each vm
				if(!isError())
				{
					//now start a CEEDeploymentThread with the list of vms
					CEEDeploymentThread deploy = new CEEDeploymentThread(vms);
					deploy.username = username;
					deploy.userid = Integer.parseInt(userid);
					deploy.start();
				}
			}

		}
	}

	public ArrayList<Integer> getOvfDBIDs() {
		return ovfDBIDs;
	}
	
	private void delete() throws SQLException
	{
		for(VEPVirtualMachine v: vms)
		{
//			v.retrieveVirtualMachine(); (not useful)
			v.delete();
		}
		//retrieve ovfmapids of this app then delete
		//also delete osdisk, osdiskmap
		//save results first because using the db while in a loop resets the query
		ResultSet rs = db.query("select", "ovfmap.id as oid, osdiskid", "ovfmap,ovf",  "where ovf.applicationid="+applicationId+" and ovfmap.ovfcontainerid=ovf.id");
		String ovfmapids = "(";
		String osdiskids = "(";
		boolean ovfmapidsToDel = false;
		while(rs.next())
		{
			ovfmapidsToDel = true;
			ovfmapids+=rs.getInt("oid")+",";
			osdiskids+=rs.getInt("osdiskid")+",";
		}
		boolean status = true;
		if(ovfmapidsToDel)
		{
			ovfmapids = ovfmapids.substring(0, ovfmapids.length()-1)+")";
			osdiskids = osdiskids.substring(0, osdiskids.length()-1)+")";
			//retrieve iaas info from the osdiskmap to delete on the provider
			rs = db.query("select", "osdiskmap.id as omid, cloudtypeid, iaasid", "osdiskmap", "where osdiskid in "+osdiskids);
			while(rs.next())
			{
				int omid = rs.getInt("omid");
				boolean imageState = deleteImage(omid, rs.getInt("cloudtypeid"), rs.getString("iaasid"));
				if(imageState)
				{
					//iaas image deleted, delete os* and ovfmap references for this image
					db.delete("osdiskmap", "where id="+omid);
				} else {
					logger.error("Could not delete one of the images (id: "+omid+") during an application.delete call");
					status = false;
				}
			}
			if(status) //if !status, something has gone wrong, one of the osdiskmap entry could not be deleted, so we should not get rid of osdisk and ovfmap entries yet.
			{
				rs = db.query("select", "localpath", "osdisk", "where id in "+osdiskids);
				while(rs.next())
				{
					String localPath = rs.getString("localpath");
					String defaultRepo = VEPHelperMethods.getProperty("imageRepositoryDefault.path", logger);
					if(!localPath.startsWith(defaultRepo))
					{
						//only delete local images which are not in the default image repository (used for vep defined permanent images)
						File f = new File(localPath);
						if(f.exists())
						{
							f.delete();
						}
					}
				}
				db.delete("osdisk", "where id in "+osdiskids);
				db.delete("ceenethandlerlist", "where ovfmapid in "+ovfmapids);
				db.delete("ceestoragehandlerlist", "where ovfmapid in "+ovfmapids);
				db.delete("ovfmapconstraint", "where ovfmapid in "+ovfmapids);
				db.delete("ovfmap", "where id in "+ovfmapids);
			}
		}
		
		if(status) //if everything went well until now
		{
			//delete ovfs
			if(ovfDBIDs.isEmpty())
			{
				rs = db.query("select", "id", "ovf", "where applicationid="+applicationId);
				while(rs.next()){
					ovfDBIDs.add(rs.getInt("id"));}
			}
			for(int id : ovfDBIDs)
			{
				db.delete("ovf", "where id="+id);
			}
			//delete app;
			db.delete("application","where id="+applicationId);
		}
	}

	private boolean deleteImage(int id, int cloudTypeId, String iaasId) throws SQLException {
		boolean status = false;
		if(iaasId != null && cloudTypeId != 0)
		{
			ResultSet rs = db.query("select", "headip, headport, version, typeid", "cloudtype", "WHERE id=" + cloudTypeId);
			if(rs.next())
			{
				int typeId = rs.getInt("typeid");
				if(typeId == App.OPENNEBULA3_HOST)
				{
					String headIp = rs.getString("headip");
					int headPort = rs.getInt("headport");
					String version = rs.getString("version");

					if(version.startsWith("3."))
					{
						rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype=" + cloudTypeId + " and accountmap.vepuser=user.id");
						String iaasUser, iaasPass;
						if(rs.next())
						{
							try
							{
								iaasUser = rs.getString("iaasuser");
								iaasPass = rs.getString("iaaspass");
								One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "image removal");
								status = one3handle.removeImage(Integer.parseInt(iaasId));
								if(!status)
								{
									setError("Exception caught during image deletion operation. ");
									logger.error("Could not delete image (iaasid: "+iaasId+" on cloud: "+cloudTypeId+").");
								}
							}
							catch(SQLException ex)
							{
								logger.error("Exception caught during image deletion operation (iaasid: "+iaasId+" on cloud: "+cloudTypeId+").");
								setError("Exception caught during image deletion operation.");
								logger.debug("Exception caught ", ex);
							}
						}
						else
						{
							logger.error("No valid cloud account map found for an administrator account. ");
						}
					}
					else
					{
						logger.error("OpenNebula version not supported yet (or too old to be supported anymore).");
					}
				} else if (typeId == App.OPENSTACK_HOST)
				{
					OpenStackConnector osc = null;
					try {
						osc = VEPHelperMethods.getOSC(cloudTypeId);
					} catch (Exception e)
					{
						logger.debug("Could not create OpenStackConnector", e);
						setError("Could not create OpenStackConnector");
					}
					if(osc != null)
					{
						rs.close();
						rs = db.query("select", "region", "openstackcloud", "where id="+cloudTypeId);
						if(rs.next())
						{
							String region = rs.getString("region");
							status = osc.removeImage(iaasId, region);
							if(!status)
							{
								logger.error("Openstack image state does not exist or permit SHUTDOWN operation.");
								setError("Openstack image state does not exist or permit SHUTDOWN operation.");
							}
						}
					}
				} else {
					setError("Unknown cloud type");
					logger.error("Unknown cloud type (not opennebula or openstack).");
				}
			}
		}
		else
		{
			logger.error("Tried to delete a non-existing image (id: "+id+").");
		}
		return status;
	}
        
	private List<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> toSchedulerConstraints(List<ConstraintMap> cntrMap) {
		ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> result=new ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> ();
		for(ConstraintMap cc:cntrMap){

			String descp;
			descp = cc.descp;
			org.ow2.contrail.provider.vep.SchedulerClient.Constraint app=new ConstraintTranslator().createConstraintFromVEP(descp,cc.param,cc.on);
			result.add(app);
		}
		return result;
	}

        /**
         * Checks if application belongs to CEE
         * @return
         * @throws SQLException 
         */
		public boolean checkRelation() throws SQLException {
			boolean authorized = false;
			
			ResultSet rs = db.query("select", "ceeid", "application", "where id="+applicationId);
			if(rs.next())
			{
				if(rs.getInt("ceeid") == Integer.parseInt(ceeId)) authorized = true;
			}
			return authorized;
		}


	
        

        
        
        
}


