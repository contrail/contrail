/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */

public class VEPAccessControl 
{
    private static Logger logger;
    private static String[] adminGroups = {"admin", "cloudadministrator"};
    private static String[] localAdminGroups = {"admin"};
    
    VEPAccessControl()
    {
        logger = Logger.getLogger("VEP.AccessControl");
    }
    
    public static boolean isAdmin(String username) throws SQLException
    {
        DBHandler db = new DBHandler("VEPAccesControl", VEPHelperMethods.getProperty("vepdb.choice", logger)); 
        ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
        String[] groups = null;
        if(rs.next())
        {
            int uid = rs.getInt("uid");
            rs.close();
            rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
            String groupList = "";
            while(rs.next())
            {
                groupList += rs.getString("gname") + ",";
            }
            groups = groupList.split(","); //the last index will be empty because of the trailing ,
            logger.trace("GroupsList for user: " + username + " is: " + groupList);
        }
        return (belongsToAdminGroups(groups) || belongsToLocalAdminGroups(groups));
    }
    
    public static boolean isGroupMember(String user1, String user2)
    {
        return false;
    }
    
    public static boolean isLocalAdmin(String username) throws SQLException
    {
        DBHandler db = new DBHandler("VEPAccesControl", VEPHelperMethods.getProperty("vepdb.choice", logger)); 
        ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
        String[] groups = null;
        if(rs.next())
        {
            int uid = rs.getInt("uid");
            rs.close();
            rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
            String groupList = "";
            while(rs.next())
            {
                groupList += rs.getString("gname") + ",";
            }
            groups = groupList.split(","); //the last index will be empty because of the trailing ,
            logger.trace("GroupsList for user: " + username + " is: " + groupList);
        }
        return belongsToLocalAdminGroups(groups);
    }
    
    private static boolean belongsToAdminGroups(String[] list)
    {
        for(int i=0; list!=null && (i < list.length); i++)
        {
            for(int j=0; j < adminGroups.length; j++)
                if(list[i].equalsIgnoreCase(adminGroups[j]))
                    return true;
        }
        return false;
    }
    
    private static boolean belongsToLocalAdminGroups(String[] list)
    {
        for(int i=0; list!=null && (i < list.length); i++)
        {
            for(int j=0; j < localAdminGroups.length; j++)
                if(list[i].equalsIgnoreCase(localAdminGroups[j]))
                    return true;
        }
        return false;
    }
}
