/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DatabaseMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.ow2.contrail.provider.cnr_pep_java.Attribute;
import org.ow2.contrail.provider.cnr_pep_java.PEP_callout;
import org.ow2.contrail.provider.cnr_pep_java.XACMLType;
import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;

/**
 * Provides several useful helper functions to rest of the application
 * 
 * @author piyushma
 */
public class VEPHelperMethods 
{
    private static String vepProperties;
    private static Logger logger;
    private String webHost, webPort;
    
    private static String[] tables = {"ceeregisteredstorage","user", "accountmap", "ugroup", "resetquestion", "supportedcloud",
    		"cloudtype", "datacenter", "cluster", "interconnect", "rack", "storage", "countrylist",
    		"storagetype", "host", "l2switch", "switchtype", "cloudnetwork", "monitoring", "errormessagelist",
    		"objecttype", "destinationtype", "accesslog", "vnethandler", "vmhandler",
    		"storagehandler", "constraints", "ceeconstraintslist", "vmslotconstraint",
    		"ceevmhandlerconstraint", "ceeconstraints", "ovfmapconstraint", "cee", "ceestoragehandlers",
    		"ceevmhandlers", "application", "ovf", "ovfmap", "vmslots", "ceenethandlers",
    		"ceenethandlerlist", "ceestoragehandlerlist", "connectioninfo", "osdisk", "osdiskmap", "sqlite_sequence","reservations", "openstackcloud","ovfmapstorageinfo"};
    
    private static String[] allowedKeys = {"rest", "dbsync", "hostsync", "caservice"};
    
    private static final String IPADDRESS_PATTERN = 
    		"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    private static HashMap<String, Object> services;
     
    
    
    static String checkCert(List list) {
         List<X509Certificate> certs = list;
        String username=null;
        if( (certs != null ) && (certs.size()>0))
        {
            X509Certificate Cert = certs.get(0);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
                
            }
            logger.info("REST request came from: " + username);
            
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
            return null;
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
            return null;
        }
        return username;
    }

   /**
     * initializes the VEPHelperMethods object
     * 
     * @param vepProp   the full path of the VEP properties file
     */
    public VEPHelperMethods(String vepProp)
    {
        vepProperties = vepProp;
        logger = Logger.getLogger("VEP.HelperMethods");
        services = new HashMap<String, Object>();
        webHost = VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger);
        webPort = VEPHelperMethods.getProperty("webuser-interface.port", logger);
    }
    
    /**
     * Retrieves the VEP properties file path
     * 
     * @return      properties file path
     */
    public static String getPropertyFile()
    {
        return vepProperties;
    }
    
    public static HashMap<String, Object> getServicesMap()
    {
    	return services;
    }
    
    public static boolean addService(String key, Object obj)
    {
    	if(belongsTo(allowedKeys, key))
    	{
    		services.put(key, obj);
    		return true;
    	}
    	return false;
    }
    
    public static Object getService(String key)
    {
    	return services.get(key);
    }
    
    /**
     * Performs an external authorization check for the call
     * 
     * @param uid       VEP user id
     * @param ovfsno    OVF serial number
     * @return          boolean true/false depending on whether the authorization check passed or failed
     * @throws SQLException 
     */
    public static boolean pdpCheck(int uid, String ovfsno) throws SQLException
    {
        String pdp = VEPHelperMethods.getProperty("pdp.use", logger);
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        DBHandler db = new DBHandler("VEPHelperMethods", dbType); 
        
        String activeVM = "0";
        PEP_callout pep_callout;
        
        List<Attribute> accessRequest = new ArrayList<Attribute>();
        String issuer = "CNR-Federation";
        //resource attributes
        Attribute attrResource = new Attribute(
                "urn:oasis:names:tc:xacml:1.0:resource:resource-id",
                XACMLType.STRING,
                "VMTemplate",
                issuer,
                Attribute.RESOURCE);
        accessRequest.add(attrResource);
        
        boolean usepdp = false;
        if(pdp != null)
            try
            {
                usepdp = Boolean.parseBoolean(pdp);
            }
            catch(Exception e)
            {
                usepdp = false;
            }
        else
            usepdp = false;
        
        /////////////////////////////////////temporary solution to disable PDP
        usepdp = false;
        
        if(!usepdp)
            return true;
        else
        {
            logger.debug("External access control through PDP module has been enabled.");
            if(ovfsno != null)
            {
                int ovfVM = 0;
                //Adding the number of VMs from the OVF to the total count, if there are more VMs defined in the OVF than the max number of VM allowed in PEP,
                //       VEP doesn't allow the instantiation of this OVF even if no VMs are active yet.
                ResultSet rs = db.query("select", "count(*) as no", "vmachinetemplate", "where ovfsno=" + ovfsno);
                if(rs.next())
                    ovfVM = rs.getInt("no");
                rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                    activeVM = String.valueOf(rs.getInt("no")+ovfVM-1);//-1, otherwise doesn't allow an ovf of 2 VMs to start even with no active VM
                else
                    activeVM = String.valueOf(ovfVM);
            }
            else
            {
                ResultSet rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                {
                    activeVM = String.valueOf(rs.getInt("no"));
                }
                else
                    activeVM = "0";
            }
            
            attrResource = new Attribute(
                    "urn:contrail:names:vep:resource:num-vm-running",
                    XACMLType.INT,
                    activeVM,
                    issuer,
                    Attribute.RESOURCE);
            accessRequest.add(attrResource);
            
            //action attributes
            Attribute attrAction = new Attribute(
                    "urn:contrail:vep:action:id",
                    XACMLType.STRING,
                    "deploy",
                    issuer,
                    Attribute.ACTION);
            accessRequest.add(attrAction);
            
            // Allocate PDP end point reference
            String pdp_endpoint = VEPHelperMethods.getProperty("pdp.endpoint", logger);
            if (pdp_endpoint == null)
                pdp_endpoint = "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap";
            pep_callout = new PEP_callout(pdp_endpoint);
            
            //invoke PDP for the access decision
            boolean accessDecision =  pep_callout.isPermit(accessRequest);
            // if true, the deploy action should be executed and denied otherwise 

            return accessDecision;
        }
    }
    
    /**
     * Hashes the input string into SHA1 hash
     * 
     * @param input     the input string to hash
     * @return          the hex encoded SHA1 hash of the input
     * @throws NoSuchAlgorithmException 
     */
    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException
    {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.reset();
            byte[] buffer = input.getBytes();
            md.update(buffer);
            byte[] digest = md.digest();

            String hexStr = "";
            for (int i = 0; i < digest.length; i++) 
            {
                    hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
            }
            return hexStr;
    }
    
    /**
     * adds a new key:value property into the VEP properties file
     * 
     * @param key       the name of the key
     * @param val       the value of the key
     * @param logger    the logger reference from the calling environment
     * @param fileName  the full path of the VEP properties file
     * @return 
     */
    public static boolean addProperty(String key, String val, Logger logger)
    {
        Properties props = new Properties();
        try
        {
            logger.trace("Loading system properties into memory.");
            FileInputStream fis = new FileInputStream(vepProperties);
            FileOutputStream fos = null;
            props.load(fis);
            logger.trace("System properties successfully loaded.");
            props.setProperty(key, val);
            logger.trace("Adding/modifying system property [key=" + key + ", value=" + val + "]");
            fis.close();
            props.store((fos = new FileOutputStream(vepProperties)), "Author: Piyush Harsh");
            fos.close();
            logger.trace("Stored system properties back to file successfully.");
        }
        catch(Exception ex)
        {
            logger.warn("Unable to write back system properties file.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
            return false;
        }
        return true;
    }
    
    /**
     * Retrieves the key value from the VEP properties file
     * 
     * @param key       the key name to be retrieved
     * @param logger    the logger reference from the calling environment
     * @param fileName  the full VEP properties file path
     * @return          the value for the requested key
     */
    public static String getProperty(String key, Logger logger)
    {
        String result = null;
        Properties props = new Properties();
        try
        {
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Trying to read system properties file.");
            FileInputStream fis = new FileInputStream(vepProperties);
            props.load(fis);
            result = props.getProperty(key);
            fis.close();
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Closing system properties file. [key=" + key + ", value=" + result + "].");
            else
            {
                if(logger == null)
                    System.out.println("LOGGER NOT YET INITIALIZED: Closing system properties file. [key=" + key + ", value=" + result + "].");
            }
        }
        catch(Exception ex)
        {
            result = null;
            if(logger!=null && logger.isDebugEnabled())
            {
            	logger.error("Error accessing system properties file.");
                ex.printStackTrace(System.err);
            }
            else if(logger != null)
                logger.error(ex.getMessage());
            else
                ex.printStackTrace(System.err);
        }
        return result;
    }
    
    /**
     * It formats a long sentence to wrap into a desired width
     * 
     * @param text      The text to be wrapped
     * @param len       the character width size to wrap into
     * @param wordWrap  true for enabling word-wrap
     * @return          array of String containing the wrapped text
     */
    public static String[] wrapText(String text, int len, boolean wordWrap)
    {
      // return empty array for null text
      if(text == null)
        return new String[] {};

      // return text if len is zero or less
      if(len <= 0)
        return new String[] {text};

      // return text if less than length
      if(text.length() <= len)
        return new String[] {text};

      char [] chars = text.toCharArray();
      ArrayList<String> lines = new ArrayList<String>();
      StringBuilder line = new StringBuilder();
      StringBuffer word = new StringBuffer();

      for(int i = 0; i < chars.length; i++)
      {
        word.append(chars[i]);

        if(wordWrap && chars[i] == ' ')
        {
          if((line.length() + word.length()) > len)
          {
            lines.add(line.toString());
            line.delete(0, line.length());
          }
          line.append(word);
          word.delete(0, word.length());
        }
        else if(!wordWrap)
        {
            if(word.length() == len)
            {
                lines.add(word.toString());
                word.delete(0, word.length());
            }
        }
      }

      // handle any extra chars in current word
      if (word.length() > 0)
      {
        if((line.length() + word.length()) > len)
        {
          lines.add(line.toString());
          line.delete(0, line.length());
        }
        line.append(word);
      }
      // handle extra line
      if (line.length() > 0)
      {
        lines.add(line.toString());
      }

      String[] ret = new String[lines.size()];
      for(int i =0; i<lines.size(); i++)
      {
          ret[i] = (String)lines.get(i);
      }
      return ret;
    }
    
    /**
     * This methods returns the HTML code that generates the REST web-interface header
     * 
     * @param firstPage     boolean specifying whether the page is the home page or not
     * @return          String containing the HTML code for the header
     */
    public static String getRESTwebHeader(boolean firstPage, boolean isAdmin, boolean doRedirect)
    {
        String response = "";
        response += "<html>\n" +
        "<head>\n" +
        "<title>Contrail: Virtual Execution Platform</title>\n";
        if(doRedirect)
        	response += "<META HTTP-EQUIV=\"REFRESH\" CONTENT=\"2;url=../\">";
        response += "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n" +
        "<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">\n" +
        "<style type=\"text/css\">\n" +
        "   A:link {color: #999999; text-decoration: none}\n" +
        "   A:visited {color: #333333; text-decoration: none}\n" +
        "   A:active {color: #333333; text-decoration: none}\n" +
        "   A:hover {color: #999999; font-weight:bold; color: #000000;}\n" +
        "</style>\n" +
        "<script src='http://"+webHost+":"+webPort+"/spin.js' type='text/javascript'></script>\n" +
        "<script type=\"text/javascript\">\n" +
        "<!--\n" +
        "function showstuff(boxid){\n" +
        "   document.getElementById(boxid).style.visibility=\"visible\";\n" +
        "}\n" +
        "function hidestuff(boxid){\n" +
        "   document.getElementById(boxid).style.visibility=\"hidden\";\n" +
        "}\n" +
        "function setContentHeight()\n" +
        "{\n" +
        "   var viewportwidth;\n" +
        "   var viewportheight;\n" +
        "   // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight\n" +
        "   if (typeof window.innerWidth != 'undefined')\n" +
        "   {\n" +
        "       viewportwidth = window.innerWidth,\n" +
        "       viewportheight = window.innerHeight\n" +
        "   }\n" +
        "   // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)\n" +
        "   else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)\n" +
        "   {\n" +
        "       viewportwidth = document.documentElement.clientWidth,\n" +
        "       viewportheight = document.documentElement.clientHeight\n" +
        "   }\n" +
        "   // older versions of IE\n" +
        "   else\n" +
        "   {\n" +
        "       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,\n" +
        "       viewportheight = document.getElementsByTagName('body')[0].clientHeight\n" +
        "   }\n" +
        "   if(viewportheight - 370 > 240)\n" +
        "       document.getElementById('content').style.height = (viewportheight - 370) + 'px';\n" +
        "   else\n" +
        "       document.getElementById('content').style.height = '240px';\n" +
        "}\n\n" +
        "function goBack()\n" +
        "{\n" +
        "   window.history.back();\n" +
        "}\n" +
        "//-->\n" +
        "</script>\n" +
        "</head>\n\n";
        if(!isAdmin)
        	response += "<body style=\"background:#CED1D6;\" >\n\n";
        else
        	response += "<body style=\"background:#CCCCCC;\" >\n\n";
        
        response +=
        //"<body style=\"background:#CCCCCC;\" onResize=\"javascript:setContentHeight();\" onLoad=\"javascript:setContentHeight();\">\n\n" + 
       

        "<div id=\"header\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n";
        if(firstPage)
            response += "<img src=\"http://"+webHost+":"+webPort+"/banner.png\">\n";
        else
            response += "<img src=\"http://"+webHost+":"+webPort+"/banner.png\" onClick=\"javascript:goBack();\">\n";
        
        if(!isAdmin)
        	response += "</div>\n\n" +
        			"<div id=\"content\" style=\"width:1024px;margin:0 auto;background:white;color:#000000;font-family:Arial;font-size:10pt;padding:10px;overflow:auto;\">\n";
        else
        	response += "</div>\n\n" +
        	        "<div id=\"content\" style=\"width:1024px;margin:0 auto;background:white;color:#000000;font-family:Arial;font-size:10pt;padding:10px;overflow:auto;\">\n";

        if(!isAdmin)
        	response +=
        "<b>Welcome to the Virtual Execution Platform REST Web-Interface</b><br><br>\n" +
        "<i>Click on the links displayed below to retrieve information on VEP resources. Certain resources will allow you to perform actions on them, for other resources " +
        "only a GET operation is allowed from this web interface. For more complete feature access you are advised to use a full-featured REST client.</i><br><br>";   
        return response;
    }
    
    /**
     * generates the REST web interface footer code
     * 
     * @return      string containing the HTML code for the footer
     */
    public static String getRESTwebFooter()
    {
        String response = "";
        response += "</div>\n\n" +

        "<div id=\"footer\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n" +
        "<table style=\"border:0px;padding:0px;color:#FFFFFF;font-family:Arial;font-size:9pt;\">\n" +
        "<tr>\n" +
        "<td><img src=\"http://"+webHost+":"+webPort+"/logo-footer.png\"></td>\n" + 
        "<td style=\"width:440px;text-align:justify;border-right:1px;border-left:0px;border-top:0px;border-bottom:0px;border-color:#FFFFFF;\" valign=\"top\">\n" +
        "This software is released under BSD license and is free to use. Contrail project is funded by European Commission under FP7 257438 directive.\n" +
        "The source code for Contrail VEP software can be downloaded from \n" +
        "<a href=\"http://websvn.ow2.org/listing.php?repname=contrail&path=%2Ftrunk%2Fprovider%2Fsrc%2Fvep%2F\" style=\"text-decoration:none;color:#CCCCFF;\" target=\"_blank\">OW2 repository</a>.\n" +
        "<br><br><font style=\"font-size:8pt;color:#CCCCCC;\">VEP REST Web-Interface and the software has been designed by Piyush Harsh with inputs from Florian Dudouet, Ales Cernivec, and Yvon Jegou.</font>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;border-left:1px;border-right:1px;border-top:0px;border-bottom:0px;border-style:dashed;border-color:#999999;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\"><a href='http://contrail-project.eu/' target='_blank' style='color:gray;'>Contrail consortium</a></td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contact us</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contribute to VEP</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Features release timeline</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Documentation</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\"><a href='http://vep.gforge.inria.fr/' target='_blank' style='color:gray;'>VEP Wiki</a></td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\"><a href='https://gforge.inria.fr/frs/?group_id=4111' target='_blank' style='color:gray;'>Download VEP</a></td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Key developers</td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">WP5 Deliverables</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\"><a href='http://"+webHost+":"+webPort+"/VEPnewdbSchema.png' target='_blank' style='color:gray;'>VEP DB Schema</a></td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "</table>\n" +
        "</div>\n\n" +

        "</body>\n" +
        "</html>";
        return response;
    }
    
    public static boolean testDBconsistency()
    {
    	boolean value = true;
        if(value==true)
            return value;
    	try
        {
            logger.trace("Running VEP database sanity checks (primitive checks) now.");
            if(DBHandler.dbHandle != null)
            {
                DatabaseMetaData dbm = DBHandler.dbHandle.getMetaData();
                String[] types = {"TABLE"};
                ResultSet rs = dbm.getTables(null,null,"%",types);
                int count=0;
                while (rs.next())
                {
                    count++;
                    String tableName = rs.getString("TABLE_NAME");
                    value = (value && belongsTo(tables, tableName));
                }
                logger.trace("VEP database has " + count + " tables.");
//                if(count < tables.length)
                if(!value)
                { 
                    logger.error("VEP database file failed sanity checks, some tables should not exist. It may mean that you miss some tables or, depending on the version of the DB you are using, have supplementary tables.");
//                    value = false;
                }
                else
                {
                    logger.trace("VEP database sanity checks (primitive checks) were completed successfully.");
                }
            }
            else
            {
                logger.warn("VEP database handler object is not valid. Terminating database sanity checks. Check system properties and try again.");
                value = false;
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught during database sanity checks.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
            value = false;
        }
    	return value;
//    	return true;
    }
    
    public static boolean initializeDB()
    {
    	boolean status = true;
    	if(DBHandler.dbHandle != null)
    	{
    		try
    		{
    			/*
    			 * 
    			 * New DB entries for cee, ovf, application, ovfmap. To change below
    			 * 
    			 * 
create table cee (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(128), userid integer references user(id), permissions varchar(3),reservationid integer, status varchar(3));
create table application (id integer primary key autoincrement, name varchar(128), ceeid integer references cee(id));
create table ovf (id integer primary key autoincrement, applicationid integer references application(id), descp varchar(102400));
create table ovfmap (id integer primary key autoincrement, ovfcontainerid integer references ovf(id), ovfid varchar(128), ceevmhandlerid integer references ceevmhandlers(id), osdiskid integer references osdisk(id), status varchar(3), context  text, staticip varchar(128), ram integer, corecount integer, cpufreq integer);
create table ceevmhandlers(id integer primary key autoincrement, vmhandlerid integer references vmhandler(id), ceeid integer references cee(id));
create table reservations (id integer primary key autoincrement, applicationid integer references application(id), ovfmapid integer references ovfmap(id));
create table vmslots (id integer primary key autoincrement, ceeid integer references cee(id), name varchar(256), ceevmhandlerid integer references ceevmhandlers(id), hostid integer, ovfmapid integer references ovfmap(id), status varchar(3), template varchar(10240), iaasid varchar(256), osdiskmapid integer references osdiskmap(id), starttime integer, uptime integer, endtime integer, corecount integer, ram integer, cpufreq integer, context varchar(5120))    			 
create table ceenethandlers(id integer primary key autoincrement, vnethandlerid integer references vnethandler(id), ceeid references cee(id))
create table ceenethandlerlist(id integer primary key autoincrement, ovfmapid integer references ovfmap(id), ceenethandlerid references ceenethandlers(id))
create table ceeconstraints(id integer primary key autoincrement, constrainttype integer references constraints(id), ceeid integer references cee(id))
create table ceeconstraintslist(id integer primary key autoincrement, ceeconstraintsid integer references ceeconstraints(id))
create table ovfmapconstraint(id integer primary key autoincrement, ovfmapid integer references ovfmap(id), ceeconstraintslistid integer references ceeconstraintslist(id), param varchar(512))
create table ceeregisteredstorage(id integer primary key autoincrement, ceestoragehandlerid integer references ceestoragehandlers(id), name varchar(256), usecount integer)    			 
    			 */
    			
    			logger.info("Starting the DB initialization process.");
    			Statement stat = DBHandler.dbHandle.createStatement();
    			for(int i=0; i<tables.length; i++)
    			{
                            if(!tables[i].equalsIgnoreCase("sqlite_sequence")){
    				String stmt = "DROP TABLE IF EXISTS " + tables[i] + ";";
    				logger.info("Trying to execute statement: " + stmt);
    				stat.executeUpdate(stmt);
                            }
    			}
    			logger.info("Dropped all/any existing table(s) from the DB.");
    			
    			String createTable = "CREATE TABLE user "
                        + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " 
    					+ "username VARCHAR(256) NOT NULL, "
                        + "password VARCHAR(256) NOT NULL, "
    					+ "email VARCHAR(128), "
                        + "passresetid INT, "
    					+ "certificate BLOB, "
                        + "role VARCHAR(64), "
                        + "unique(username))";
    			stat.executeUpdate(createTable);
                logger.info("VEP table user created successfully.");
                //now inserting an initial system administrator account as a default
                String query = "INSERT INTO user VALUES (1, 'admin', '789b49606c321c8cf228d17942608eff0ccc4171', 'admin@cloud-provider.net', 1, '', 'administrator')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted a default admin account into table user.");
                
                createTable = "CREATE TABLE ugroup "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "userid INT NOT NULL)";
                stat.executeUpdate(createTable);
                logger.info("VEP table group created successfully.");
                
                createTable = "CREATE TABLE resetquestion "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "question VARCHAR(128) NOT NULL)";
                stat.executeUpdate(createTable);
                logger.info("VEP table resetquestion created successfully.");
                //now inserting initial sets of reset questions
                query = "INSERT INTO resetquestion VALUES (1, 'Where were you born?')";
                stat.executeUpdate(query);
                query = "INSERT INTO resetquestion VALUES (2, 'What was the name of your high school?')";
                stat.executeUpdate(query);
                query = "INSERT INTO resetquestion VALUES (3, 'What is your favorite color?')";
                stat.executeUpdate(query);
                query = "INSERT INTO resetquestion VALUES (4, 'What was the make of your first car?')";
                stat.executeUpdate(query);
                query = "INSERT INTO resetquestion VALUES (5, 'What is your official card number?')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted reset questions in table resetquestion.");
                
                createTable = "CREATE TABLE accountmap "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "cloudtype INT, "
                		+ "iaasuser VARCHAR(256), "
                		+ "iaaspass VARCHAR(256), "
                		+ "iaasuid VARCHAR(256), "
                		+ "vepuser INT NOT NULL "
                	
                		+ ")";
                stat.executeUpdate(createTable);
                logger.info("VEP table accountmap created successfully.");
                
                createTable = "CREATE TABLE monitoring "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "objecttypeid INT, "
                		+ "objectid INT, "
                		+ "periodicity INT, "
                		+ "destinationtypeid INT, "
                		+ "param VARCHAR(512))";
                stat.executeUpdate(createTable);
                logger.info("VEP table monitoring created successfully.");
                
                createTable = "CREATE TABLE objecttype "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(64) NOT NULL)";
                stat.executeUpdate(createTable);
                logger.info("VEP table objecttype created successfully.");
                //now inserting initial sets of object types
                query = "INSERT INTO objecttype VALUES (1, 'vmslots')";
                stat.executeUpdate(query);
                query = "INSERT INTO objecttype VALUES (2, 'cee')";
                stat.executeUpdate(query);
                query = "INSERT INTO objecttype VALUES (3, 'ceestoragehandlers')";
                stat.executeUpdate(query);
                query = "INSERT INTO objecttype VALUES (4, 'ceenethandlers')";
                stat.executeUpdate(query);
                query = "INSERT INTO objecttype VALUES (5, 'host')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted object types in table objecttype.");
                
                createTable = "CREATE TABLE destinationtype "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128) NOT NULL)";
                stat.executeUpdate(createTable);
                logger.info("VEP table destinationtype created successfully.");
                //now inserting initial sets of destination types
                query = "INSERT INTO destinationtype VALUES (1, 'file')";
                stat.executeUpdate(query);
                query = "INSERT INTO destinationtype VALUES (2, 'rabbitmq')";
                stat.executeUpdate(query);
                query = "INSERT INTO destinationtype VALUES (3, 'activemq')";
                stat.executeUpdate(query);
                query = "INSERT INTO destinationtype VALUES (4, 'logserver')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted destination types in table destinationtype.");
                
                createTable = "CREATE TABLE accesslog "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "querytype VARCHAR(32), "
                		+ "userid INT, "
                		+ "querytable VARCHAR(32), "
                		+ "args VARCHAR(128), "
                		+ "timestamp INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table accesslog created successfully.");
                
                createTable = "CREATE TABLE errormessagelist "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "shortmsg VARCHAR(128), "
                		+ "detailedinfo VARCHAR(1024), "
                		+ "username VARCHAR(256), "
                		+ "state VARCHAR(32), "
                		+ "tablelist VARCHAR(256), "
                		+ "timestamp VARCHAR(128), "
                		+ "isprocessed INT"
                		+ ")";
                stat.executeUpdate(createTable);
                logger.info("VEP table errormessagelist created successfully.");
                
                createTable = "CREATE TABLE constraints "
                		+ "(id integer primary key autoincrement, "
                		+ "descp VARCHAR(256), "
                		+ "type INTEGER)";
                stat.executeUpdate(createTable);
                logger.info("VEP table constraints created successfully.");
                
                createTable = "CREATE TABLE storagehandler "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "storageid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table storagehandler created successfully.");
                
                createTable = "CREATE TABLE cloudtype "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "headip VARCHAR(128), "
                		+ "headport INT, "
                		+ "version VARCHAR(32), "
                		+ "sharedfolder VARCHAR(256), "
                		+ "typeid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table cloudtype created successfully.");
                
                createTable = "CREATE TABLE supportedcloud "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table supportedcloud created successfully.");
                //now inserting initial sets of interconnect types
                query = "INSERT INTO supportedcloud VALUES (1, 'OpenNebula')";
                stat.executeUpdate(query);
                query = "INSERT INTO supportedcloud VALUES (2, 'OpenStack')";
                stat.executeUpdate(query);
                query = "INSERT INTO supportedcloud VALUES (3, 'Nimbus')";
                stat.executeUpdate(query);
                query = "INSERT INTO supportedcloud VALUES (4, 'Eucalyptus')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted supported cloud types in table supportedcloud.");
                
                createTable = "CREATE TABLE vmhandler "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "cpufreq_low INT, "
                		+ "cpufreq_high INT, "
                		+ "ram_low INT, "
                		+ "ram_high INT, "
                		+ "corecount_low INT, "
                		+ "corecount_high INT, "
                		+ "disksize_low INT, "
                		+ "disksize_high INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table vmhandler created successfully.");
                
                createTable = "CREATE TABLE vnethandler "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "pubipsupport INT, "
                		+ "vlantagsupport INT, "
                		+ "dhcpsupport INT, "
                		+ "cloudnetworkid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table vnethandler created successfully.");
                
                createTable = "CREATE TABLE countrylist "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "code VARCHAR(2))";
                stat.executeUpdate(createTable);
                logger.info("VEP table countrylist created successfully.");
                //now populating the list of countries
                query = "INSERT INTO countrylist VALUES (1, 'Afghanistan', 'AF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (2, 'Åland Islands', 'AX')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (3, 'Albania', 'AL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (4, 'Algeria', 'DZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (5, 'American Samoa', 'AS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (6, 'Andorra', 'AD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (7, 'Angola', 'AO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (8, 'Anguilla', 'AI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (9, 'Antarctica', 'AQ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (10, 'Antigua and Barbuda', 'AG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (11, 'Argentina', 'AR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (12, 'Armenia', 'AM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (13, 'Aruba', 'AW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (14, 'Australia', 'AU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (15, 'Austria', 'AT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (16, 'Azerbaijan', 'AZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (17, 'Bahamas', 'BS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (18, 'Bahrain', 'BH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (19, 'Bangladesh', 'BD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (20, 'Barbados', 'BB')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (21, 'Belarus', 'BY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (22, 'Belgium', 'BE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (23, 'Belize', 'BZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (24, 'Benin', 'BJ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (25, 'Bermuda', 'BM')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 10% done.");
                query = "INSERT INTO countrylist VALUES (26, 'Bhutan', 'BT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (27, 'Bolivia, Plurinational State of', 'BO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (28, 'Bonaire, Sint Eustatius and Saba', 'BQ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (29, 'Bosnia and Herzegovina', 'BA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (30, 'Botswana', 'BW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (31, 'Bouvet Island', 'BV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (32, 'Brazil', 'BR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (33, 'British Indian Ocean Territory', 'IO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (34, 'Brunei Darussalam', 'BN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (35, 'Bulgaria', 'BG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (36, 'Burkina Faso', 'BF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (37, 'Burundi', 'BI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (38, 'Cambodia', 'KH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (39, 'Cameroon', 'CM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (40, 'Canada', 'CA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (41, 'Cape Verde', 'CV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (42, 'Cayman Islands', 'KY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (43, 'Central African Republic', 'CF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (44, 'Chad', 'TD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (45, 'Chile', 'CL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (46, 'China', 'CN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (47, 'Christmas Island', 'CX')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (48, 'Cocos (Keeling) Islands', 'CC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (49, 'Colombia', 'CO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (50, 'Comoros', 'KM')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 20% done.");
                query = "INSERT INTO countrylist VALUES (51, 'Congo', 'CG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (52, 'Congo, the Democratic Republic of the', 'CD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (53, 'Cook Islands', 'CK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (54, 'Costa Rica', 'CR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (55, 'Côte d-Ivoire', 'CI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (56, 'Croatia', 'HR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (57, 'Cuba', 'CU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (58, 'Curaçao', 'CW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (59, 'Cyprus', 'CY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (60, 'Czech Republic', 'CZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (61, 'Denmark', 'DK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (62, 'Djibouti', 'DJ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (63, 'Dominica', 'DM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (64, 'Dominican Republic', 'DO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (65, 'Ecuador', 'EC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (66, 'Egypt', 'EG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (67, 'El Salvador', 'SV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (68, 'Equitorial Guinea', 'GQ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (69, 'Eritrea', 'ER')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (70, 'Estonia', 'EE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (71, 'Ethiopia', 'ET')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (72, 'Falkland Islands (Malvinas)', 'FK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (73, 'Faroe Islands', 'FO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (74, 'Fiji', 'FJ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (75, 'Finland', 'FI')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 30% done.");
                query = "INSERT INTO countrylist VALUES (76, 'France', 'FR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (77, 'French Guiana', 'GF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (78, 'French Polynesia', 'PF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (79, 'French Southern Territories', 'TF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (80, 'Gabon', 'GA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (81, 'Gambia', 'GM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (82, 'Georgia', 'GE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (83, 'Germany', 'DE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (84, 'Ghana', 'GH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (85, 'Gibraltar', 'GI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (86, 'Greece', 'GR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (87, 'Greenland', 'GL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (88, 'Grenada', 'GD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (89, 'Guadeloupe', 'GP')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (90, 'Guam', 'GU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (91, 'Guatemala', 'GT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (92, 'Guernsey', 'GG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (93, 'Guinea', 'GN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (94, 'Guinea-Bissau', 'GW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (95, 'Guyana', 'GY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (96, 'Haiti', 'HT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (97, 'Heard Island and McDonald Islands', 'HM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (98, 'Holy See (Vatican City State)', 'VA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (99, 'Honduras', 'HN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (100, 'Hong Kong', 'HK')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 40% done.");
                query = "INSERT INTO countrylist VALUES (101, 'Hungary', 'HU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (102, 'Iceland', 'IS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (103, 'India', 'IN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (104, 'Indonesia', 'ID')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (105, 'Iran, Islamic Republic of', 'IR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (106, 'Iraq', 'IQ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (107, 'Ireland', 'IE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (108, 'Isle of Man', 'IM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (109, 'Israel', 'IL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (110, 'Italy', 'IT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (111, 'Jamaica', 'JM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (112, 'Japan', 'JP')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (113, 'Jersey', 'JE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (114, 'Jordan', 'JO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (115, 'Kazakhstan', 'KZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (116, 'Kenya', 'KE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (117, 'Kiribati', 'KI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (118, 'Korea, Democratic People-s Republic of', 'KP')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (119, 'Korea, Republic of', 'KR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (120, 'Kuwait', 'KW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (121, 'Kyrgyzstan', 'KG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (122, 'Lao People-s Democratic Republic', 'LA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (123, 'Latvia', 'LV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (124, 'Lebanon', 'LB')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (125, 'Lesotho', 'LS')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 50% done.");
                query = "INSERT INTO countrylist VALUES (126, 'Liberia', 'LR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (127, 'Libya', 'LY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (128, 'Liechtenstein', 'LI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (129, 'Lithuania', 'LT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (130, 'Luxembourg', 'LU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (131, 'Macao', 'MO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (132, 'Macedonia, the former Yugoslav Republic of', 'MK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (133, 'Madagascar', 'MG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (134, 'Malawi', 'MW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (135, 'Malaysia', 'MY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (136, 'Maldives', 'MV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (137, 'Mali', 'ML')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (138, 'Malta', 'MT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (139, 'Marshall Islands', 'MH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (140, 'Martinique', 'MQ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (141, 'Mauritania', 'MR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (142, 'Mauritius', 'MU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (143, 'Mayotte', 'YT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (144, 'Mexico', 'MX')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (145, 'Micronesia, Federated States of', 'FM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (146, 'Moldova, Republic of', 'MD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (147, 'Monaco', 'MC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (148, 'Mongolia', 'MN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (149, 'Montenegro', 'ME')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (150, 'Montserrat', 'MS')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 60% done.");
                query = "INSERT INTO countrylist VALUES (151, 'Morocco', 'MA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (152, 'Mozambique', 'MZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (153, 'Myanmar', 'MM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (154, 'Namibia', 'NA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (155, 'Nauru', 'NR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (156, 'Nepal', 'NP')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (157, 'Netherlands', 'NL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (158, 'New Caledonia', 'NC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (159, 'New Zealand', 'NZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (160, 'Nicaragua', 'NI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (161, 'Niger', 'NE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (162, 'Nigeria', 'NG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (163, 'Niue', 'NU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (164, 'Norfolk Island', 'NF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (165, 'Northern Mariana Islands', 'MP')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (166, 'Norway', 'NO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (167, 'Oman', 'OM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (168, 'Pakistan', 'PK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (169, 'Palau', 'PW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (170, 'Palestinian Territory, Occupied', 'PS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (171, 'Panama', 'PA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (172, 'Papua New Guinea', 'PG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (173, 'Paraguay', 'PY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (174, 'Peru', 'PE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (175, 'Philippines', 'PH')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 70% done.");
                query = "INSERT INTO countrylist VALUES (176, 'Pitcairn', 'PN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (177, 'Poland', 'PL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (178, 'Portugal', 'PT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (179, 'Puerto Rico', 'PR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (180, 'Qatar', 'QA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (181, 'Réunion', 'RE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (182, 'Romania', 'RO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (183, 'Russian Federation', 'RU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (184, 'Rwanda', 'RW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (185, 'Saint Barthélemy', 'BL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (186, 'Saint Helena, Ascension and Tristan da Cunha', 'SH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (187, 'Saint Kitts and Nevis', 'KN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (188, 'Saint Lucia', 'LC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (189, 'Saint Martin (French part)', 'MF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (190, 'Saint Pierre and Miquelon', 'PM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (191, 'Saint Vincent and the Grenadines', 'VC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (192, 'Samoa', 'WS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (193, 'San Marino', 'SM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (194, 'Sao Tome and Principe', 'ST')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (195, 'Saudi Arabia', 'SA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (196, 'Senegal', 'SN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (197, 'Serbia', 'RS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (198, 'Seychelles', 'SC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (199, 'Sierra Leone', 'SL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (200, 'Singapore', 'SG')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 80% done.");
                query = "INSERT INTO countrylist VALUES (201, 'Sint Maarten (Dutch part)', 'SX')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (202, 'Slovakia', 'SK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (203, 'Slovenia', 'SI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (204, 'Solomon Islands', 'SB')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (205, 'Somalia', 'SO')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (206, 'South Africa', 'ZA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (207, 'South Georgia and the South Sandwich Islands', 'GS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (208, 'South Sudan', 'SS')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (209, 'Spain', 'ES')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (210, 'Sri Lanka', 'LK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (211, 'Sudan', 'SD')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (212, 'Suriname', 'SR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (213, 'Svalbard and Jan Mayen', 'SJ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (214, 'Swaziland', 'SZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (215, 'Sweden', 'SE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (216, 'Switzerland', 'CH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (217, 'Syrian Arab Republic', 'SY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (218, 'Taiwan, Province of China', 'TW')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (219, 'Tajikistan', 'TJ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (220, 'Tanzania, United Republic of', 'TZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (221, 'Thailand', 'TH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (222, 'Timor-Leste', 'TL')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (223, 'Togo', 'TG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (224, 'Tokelau', 'TK')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (225, 'Tonga', 'TO')";
                stat.executeUpdate(query);
                logger.info("Inserting country names into country list: 90% done.");
                query = "INSERT INTO countrylist VALUES (226, 'Trinidad and Tobago', 'TT')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (227, 'Tunisia', 'TN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (228, 'Turkey', 'TR')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (229, 'Turkmenistan', 'TM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (230, 'Turks and Caicos Islands', 'TC')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (231, 'Tuvalu', 'TV')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (232, 'Uganda', 'UG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (233, 'Ukraine', 'UA')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (234, 'United Arab Emirates', 'AE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (235, 'United Kingdom', 'GB')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (236, 'United States', 'US')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (237, 'United States Minor Outlying Islands', 'UM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (238, 'Uruguay', 'UY')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (239, 'Uzbekistan', 'UZ')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (240, 'Vanuatu', 'VU')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (241, 'Venezuela, Bolivarian Republic of', 'VE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (242, 'Viet Nam', 'VN')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (243, 'Virgin Islands, British', 'VG')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (244, 'Virgin Islands, U.S.', 'VI')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (245, 'Wallis and Futuna', 'WF')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (246, 'Western Sahara', 'EH')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (247, 'Yemen', 'YE')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (248, 'Zambia', 'ZM')";
                stat.executeUpdate(query);
                query = "INSERT INTO countrylist VALUES (249, 'Zimbabwe', 'ZW')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted country names in table countrylist.");
                
                createTable = "CREATE TABLE datacenter "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "countryid INT, "
                		+ "descp VARCHAR(1024))";
                stat.executeUpdate(createTable);
                logger.info("VEP table datacenter created successfully.");
                
                createTable = "CREATE TABLE cluster "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "interconnectid INT, "
                		+ "descp VARCHAR(1024), "
                		+ "datacenterid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table cluster created successfully.");
                
                createTable = "CREATE TABLE interconnect "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table interconnect created successfully.");
                //now inserting initial sets of interconnect types
                query = "INSERT INTO interconnect VALUES (1, 'Gigabit Ethernet')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (2, 'Gigabit Ethernet (Level 5 NICs)')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (3, '10 Gigabit Ethernet')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (4, 'Infiniband')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (5, 'Infinipath')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (6, 'Myrinet')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (7, 'QsNet (Quadrics)')";
                stat.executeUpdate(query);
                query = "INSERT INTO interconnect VALUES (8, 'SCI (Dolphin)')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted interconnect types in table interconnect.");
                
                createTable = "CREATE TABLE rack "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "descp VARCHAR(1024), "
                		+ "clusterid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table rack created successfully.");
                
                createTable = "CREATE TABLE storage "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "storagetypeid INT, "
                		+ "endpoint VARCHAR(256), "
                		+ "rackid INT,"
                		+ "mountendpoint VARCHAR(256))";
                stat.executeUpdate(createTable);
                logger.info("VEP table storage created successfully.");

                createTable = "CREATE TABLE storagetype "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table storagetype created successfully.");
                query = "INSERT INTO storagetype VALUES (1, 'Local Disk')";
                stat.executeUpdate(query);
                query = "INSERT INTO storagetype VALUES (2, 'NFS Share')";
                stat.executeUpdate(query);
                query = "INSERT INTO storagetype VALUES (3, 'XTreemFs GAFS Share')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted storage types in table storagetype.");
                
                createTable = "CREATE TABLE cloudnetwork "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "cloudtypeid INT, "
                		+ "type VARCHAR(128), "
                		+ "iaasid VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table cloudnetwork created successfully.");
                
                createTable = "CREATE TABLE switchtype "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table switchtype created successfully.");
                query = "INSERT INTO switchtype VALUES (1, 'Store and Forward')";
                stat.executeUpdate(query);
                query = "INSERT INTO switchtype VALUES (2, 'Cut Through Fast Forward')";
                stat.executeUpdate(query);
                query = "INSERT INTO switchtype VALUES (3, 'Cut Through Fragment Free')";
                stat.executeUpdate(query);
                logger.info("Successfully inserted switch types in table switchtype.");
                
                createTable = "CREATE TABLE l2switch "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "descp VARCHAR(1024), "
                		+ "switchtypeid INT, "
                		+ "mbps INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table l2switch created successfully.");
                
                createTable = "CREATE TABLE host "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "cpufreq INT, "
                		+ "corecount INT, "
                		+ "ram INT, "
                		+ "disksize INT, "
                		+ "hostname VARCHAR(128), "
                		+ "cpuarch VARCHAR(128), "
                		+ "virtualization VARCHAR(128), "
                		+ "l2switchid INT, "
                		+ "rackid INT, "
                		+ "cloudtypeid INT, "
                		+ "iaasid VARCHAR(256))";
                stat.executeUpdate(createTable);
                logger.info("VEP table host created successfully.");
                
                
                //create table ceeconstraints(id integer primary key autoincrement, constrainttype integer references constraints(id), ceeid integer references cee(id))
                
                createTable = "CREATE TABLE ceeconstraints "
                		+ "(id integer primary key autoincrement, "
                		+ "constrainttype integer references constraints(id), "
                		+ "ceeid integer references cee(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceeconstraints created successfully.");
                
                
                //create table ceeconstraintslist(id integer primary key autoincrement, ceeconstraintsid integer references ceeconstraints(id))
                
                createTable = "CREATE TABLE ceeconstraintslist "
                		+ "(id integer primary key autoincrement, "
                		+ "ceeconstraintsid integer references ceeconstraints(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceeconstraintslist created successfully.");
                
                createTable = "CREATE TABLE ceevmhandlerconstraint "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "ceevmhandlerid INT, "
                		+ "ceeconstraintlistid INT, "
                		+ "param VARCHAR(512))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceevmhandlerconstraint created successfully.");
                
                createTable = "CREATE TABLE vmslotconstraint "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "vmslotid INT, "
                		+ "ceeconstraintlistid INT, "
                		+ "param VARCHAR(512))";
                stat.executeUpdate(createTable);
                logger.info("VEP table vmslotconstraint created successfully.");
                
                
                //create table ovfmapconstraint(id integer primary key autoincrement, ovfmapid integer references ovfmap(id), ceeconstraintslistid integer references ceeconstraintslist(id), param varchar(512))
                
                createTable = "CREATE TABLE ovfmapconstraint "
                		+ "(id integer primary key autoincrement, "
                		+ "ovfmapid integer references ovfmap(id), "
                		+ "ceeconstraintslistid integer references ceeconstraintslist(id), "
                		+ "param VARCHAR(512))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ovfmapconstraint created successfully.");
                
                createTable = "CREATE TABLE ceestoragehandlers "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "storagehandlerid INT, "
                		+ "ceeid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceestoragehandlers created successfully.");
                
                
                //create table cee (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(128), userid integer references user(id), permissions varchar(3),reservationid integer, status varchar(3));
                
                createTable = "CREATE TABLE cee "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "name VARCHAR(128), "
                		+ "userid INTEGER references user(id), "
                		+ "permissions VARCHAR(3), "
                		+ "reservationid INTEGER, "
                		+ "status VARCHAR(3))";
                stat.executeUpdate(createTable);
                logger.info("VEP table cee created successfully.");
                
                createTable = "CREATE TABLE osdisk "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "localpath VARCHAR(256), "
                		+ "ovfimagepath VARCHAR(256), "
                		+ "ovfimagename VARCHAR(128), "
                		+ "status VARCHAR(3), "
                		+ "usecount INT, "
                		+ "ceeid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table osdisk created successfully.");
                
                //id should be exactly the same as cloudtype.id, 1-1 relation, no autoincrement. Reason for this table is to not extend the previous one more dedicated to OpenNebula
                createTable = "CREATE table openstackcloud "
                		+ "(id INTEGER PRIMARY KEY, "
                		+ "keystoneuserip VARCHAR(512),"
                		+ "keystoneuserport INTEGER,"
                		+ "keystoneadminip VARCHAR(512),"
                		+ "keystoneadminport INTEGER,"
                          	+ "region VARCHAR(512)," 
                		+ "tenantname VARCHAR(512),"
                		+ "tenantid VARCHAR(512),"
                		+ "roleid VARCHAR(512))";
                
                 stat.executeUpdate(createTable);

                	

                createTable = "CREATE TABLE osdiskmap "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "cloudtypeid INT, "
                		+ "iaasname VARCHAR(128), "
                		+ "iaastemplate VARCHAR(1024), "
                		+ "osdiskid INT, "
                		+ "iaasid VARCHAR(128), "
                		+ "status VARCHAR(3))";
                stat.executeUpdate(createTable);
                logger.info("VEP table osdiskmap created successfully.");
                
                //create table application (id integer primary key autoincrement, name varchar(128), ceeid integer references cee(id));
                
                createTable = "CREATE TABLE application "
                		+ "(id integer primary key autoincrement, "
                		+ "name VARCHAR(128), "
                		+ "ceeid integer references cee(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table application created successfully.");
                
                
                //create table ovf (id integer primary key autoincrement, applicationid integer references application(id), descp varchar(102400));
                
                createTable = "CREATE TABLE ovf "
                		+ "(id integer primary key autoincrement, "
                		+ "applicationid integer references application(id), "
                		+ "descp VARCHAR(102400))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ovf created successfully.");
                
                //create table vmslots (id integer primary key autoincrement, ceeid integer references cee(id), name varchar(256), 
                //ceevmhandlerid integer references ceevmhandlers(id), hostid integer, ovfmapid integer references ovfmap(id), status varchar(3), 
                //template varchar(10240), iaasid varchar(256), osdiskmapid integer references osdiskmap(id), starttime integer, uptime integer, 
                //endtime integer, corecount integer, ram integer, cpufreq integer, context varchar(5120))
                
                
                createTable = "CREATE TABLE vmslots "
                		+ "(id integer primary key autoincrement, "
                		+ "ceeid integer references cee(id), "
                		+ "name varchar(256), "
                		+ "ceevmhandlerid integer references ceevmhandlers(id), "
                		+ "hostid INT, "
                		+ "ovfmapid integer references ovfmap(id), "
                		+ "status VARCHAR(3), "
                		+ "template VARCHAR(10240), "
                		+ "iaasid VARCHAR(256), "
                		+ "osdiskmapid integer references osdiskmap(id), "
                		+ "starttime INT, "
                		+ "uptime INT, "
                		+ "endtime INT, "
                		+ "corecount integer, "
                		+ "ram integer, "
                		+ "cpufreq integer, "
                		+ "context varchar(5120))";
                stat.executeUpdate(createTable);
                logger.info("VEP table vmslots created successfully.");
                
                createTable = "CREATE TABLE connectioninfo "
                		+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                		+ "vmslotid INT, "
                		+ "ip VARCHAR(128), "
                		+ "hostname VARCHAR(256), "
                		+ "vncport INT, "
                		+ "vncwhitelist VARCHAR(128))";
                stat.executeUpdate(createTable);
                logger.info("VEP table connectioninfo created successfully.");
                
                
                //create table ceevmhandlers(id integer primary key autoincrement, vmhandlerid integer references vmhandler(id), ceeid integer references cee(id));
                
                createTable = "CREATE TABLE ceevmhandlers "
                		+ "(id integer primary key autoincrement, "
                		+ "vmhandlerid integer references vmhandler(id), "
                		+ "ceeid integer references cee(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceevmhandlers created successfully.");
                
                
                //create table ovfmap (id integer primary key autoincrement, ovfcontainerid integer references ovf(id), ovfid varchar(128), ceevmhandlerid integer references ceevmhandlers(id), 
                //osdiskid integer references osdisk(id), status varchar(3), context  text, staticip varchar(128), ram integer, corecount integer, cpufreq integer);
                
                createTable = "CREATE TABLE ovfmap "
                		+ "(id integer primary key autoincrement, "
                		+ "ovfcontainerid integer references ovf(id), "
                		+ "ovfid VARCHAR(128), "
                		+ "ceevmhandlerid integer references ceevmhandlers(id), "
                		+ "osdiskid integer references osdisk(id), "
                		+ "status VARCHAR(3), "
                		+ "context TEXT, "
                		+ "staticip VARCHAR(128), "
                		+ "ram integer, "
                		+ "corecount integer, "
                		+ "cpufreq integer)";
                stat.executeUpdate(createTable);
                logger.info("VEP table ovfmap created successfully.");
                
                //create table ceenethandlers(id integer primary key autoincrement, vnethandlerid integer references vnethandler(id), ceeid references cee(id))
                
                createTable = "CREATE TABLE ceenethandlers "
                		+ "(id integer primary key autoincrement, "
                		+ "vnethandlerid integer references vnethandler(id), "
                		+ "ceeid references cee(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceenethandlers created successfully.");
                
                //create table ceenethandlerlist(id integer primary key autoincrement, ovfmapid integer references ovfmap(id), ceenethandlerid references ceenethandlers(id))
                
                createTable = "CREATE TABLE ceenethandlerlist "
                		+ "(id integer primary key autoincrement, "
                		+ "ovfmapid integer references ovfmap(id), "
                		+ "ceenethandlerid references ceenethandlers(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceenethandlerlist created successfully.");
                
                createTable = "CREATE TABLE ceestoragehandlerlist "
                		+ "(id integer primary key autoincrement, "
                		+ "ovfmapid INT, "
                		+ "ceestoragehandlerid INT)";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceestoragehandlerlist created successfully.");
                
                //create table reservations (id integer primary key autoincrement, applicationid integer references application(id), ovfmapid integer references ovfmap(id));
                createTable = "CREATE TABLE reservations "
                		+ "(id integer primary key autoincrement, "
                		+ "applicationid integer references application(id), "
                		+ "ovfmapid integer references ovfmap(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table reservations created successfully.");
                
                //create table ceeregisteredstorage(id integer primary key autoincrement, ceestoragehandlerid integer references ceestoragehandlers(id), 
                //name varchar(256), usecount integer)
                createTable = "CREATE TABLE ceeregisteredstorage "
                		+ "(id integer primary key autoincrement, "
                		+ "ceestoragehandlerid integer references ceestoragehandlers(id), "
                		+ "name varchar(256), "
                		+ "usecount integer)";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceeregisteredstorage created successfully.");
    		
                createTable = "CREATE TABLE ovfmapstorageinfo "
                		+ "(id integer primary key autoincrement, "
                		+ "ceestoragehandlerlistid integer references ceestoragehandlerlist(id), "
                 		+ "ovfmapid integer references ovfmap(id))";
                stat.executeUpdate(createTable);
                logger.info("VEP table ceeregisteredstorage created successfully.");
    		
    		}
    		catch(Exception ex)
    		{
    			logger.error("Exception caught while DB initialization process.");
    			//if(logger.isDebugEnabled())
    			//	ex.printStackTrace(System.err);
    			logger.debug("Exception Caught: ", ex);
    			status = false;
    		}
    	}
    	else
    	{
    		logger.error("Database connection object is not properly initialized.");
    		status = false;
    	}
    	return status;
    }
    
    public static boolean belongsTo(String[] list, String element)
    {
    	for(int i=0; i<list.length; i++)
    	{
    		if(list[i].equalsIgnoreCase(element)) return true;
    	}
    	return false;
    }
    
    public static boolean isIpAddress(String ip)
    {
    	Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
    	Matcher matcher = pattern.matcher(ip);
    	return matcher.matches();
    }
    
    /**
     * Retrieves an admin OpenStackConnector
     * @param cloudtypeid id of the OpenStack cloud
     * @return OpenStackConnector initialized
     * @throws Exception
     */
    public static OpenStackConnector getOSC(int cloudtypeid) throws Exception{
    	return getOSC(0,cloudtypeid);
    }
    
    /**
     * Returns a OpenStackConnector to use for various tasks
     * @param userid Client userid (if 0, will use an admin account for both user and admin endpoint)
     * @param cloudtypeid id of the OpenStack cloud
     * @return OpenStackConnector initialized
     * @throws Exception
     */
    public static OpenStackConnector getOSC(int userid, int cloudtypeid) throws Exception{
		ResultSet rs;
		DBHandler db = new DBHandler("OpenStackConnection", VEPHelperMethods.getProperty("vepdb.choice", logger));
//		rs = db.query("select", "*", "accountmap,openstackcloud,cloudtype,user", "where ((vepuser="+userid+" and accountmap.cloudtype="+cloudtypeid +" and cloudtype.typeid="+App.OPENSTACK_HOST+") or ( user.role='administrator' and accountmap.vepuser=user.id and accountmap.cloudtype=cloudtype.id and cloudtype.typeid="+App.OPENSTACK_HOST+")) and openstackcloud.id=cloudtype.id and cloudtype.id="+cloudtypeid );
		if(userid == 0)
		{
			rs = db.query("select", "*", "accountmap,  openstackcloud, cloudtype, user", "where user.role='administrator' and accountmap.vepuser=user.id and accountmap.cloudtype="+cloudtypeid+" and cloudtype.typeid="+App.OPENSTACK_HOST+" and openstackcloud.id=cloudtype.id and accountmap.cloudtype=cloudtype.id");
		} else {
			rs = db.query("select", "*", "accountmap,openstackcloud,cloudtype,user", "where vepuser="+userid+" and accountmap.cloudtype="+cloudtypeid +" and cloudtype.typeid="+App.OPENSTACK_HOST+" and openstackcloud.id=cloudtype.id and cloudtype.id="+cloudtypeid );
		}
		String userU, userP, tenant, endpointU, endpointA, adminP, adminU;
		userU = userP = tenant = endpointU = endpointA = adminU = adminP = null;
		if(rs.next())
		{
			if(rs.getString("role").equals("administrator"))
			{
//				if(adminU == null)
//				{
					//fill adminU and adminP with the first found administrator for this cloud, otherwise continue the loop
					adminU = rs.getString("iaasuser");
					adminP = rs.getString("iaaspass");
//				}
			} 
			userU = rs.getString("iaasuser");
			userP = rs.getString("iaaspass");
			tenant = rs.getString("tenantname");
			endpointU = rs.getString("keystoneuserip")+":"+rs.getString("keystoneuserport")+"/v2.0/";
			endpointA = rs.getString("keystoneadminip")+":"+rs.getString("keystoneadminport")+"/v2.0/";
		
			if(adminU == null)
			{
				rs = db.query("select", "iaasuser, iaaspass", "user, accountmap", "where user.role='administrator' and accountmap.vepuser=user.id and accountmap.cloudtypeid="+cloudtypeid);
				if(rs.next())
				{
					adminU = rs.getString("iaasuser");
					adminP = rs.getString("iaaspass");
				}
			}
		}
		
		if(userU != null && userP != null && adminU != null && adminP != null && tenant != null && endpointA != null && endpointU != null)
		{
			OpenStackConnector osc = new OpenStackConnector(userU, tenant, userP, endpointU, adminU, tenant, adminP, endpointA, "OpenStackConnector creation");
			return osc;
		} else {
			throw new Exception("Could not find either an accountmap entry for the specified userid, or an administrator account for this cloud");
		}
    }
}
