/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.SchedulerClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author fgaudenz
 */
public class Constraint {
     private String type;
     private String value;
     private String operator;
     
     private List<String> vms;
     
     public Constraint(String type,
        String value,
        String operator,
        List<String> vms){
        this.type=type;
        this.operator=operator;
        this.value=value;
        this.vms=vms;
     }
     
     public Constraint(String type,
        String operator,
        List<String> vms){
        this.type=type;
        this.operator=operator;
        this.value=null;
        this.vms=vms;
     }
     
     public Constraint(String type,String operator){
         this.type=type;
         this.operator=operator;  
     }
     public JSONObject toJSON(){
         JSONObject obj=new JSONObject();
         obj.put("id", type);
         obj.put("operator", operator);
         if(value!=null){
             obj.put("value",value);
         }
         List vmsArray=new LinkedList();
         for(String vm:vms){
            // System.out.prinln()
         Map m2 = new HashMap();
         m2.put("id", vm);
         vmsArray.add(m2);
         }
         obj.put("VMs",vmsArray);
         return obj;        
     }

    public String getType() {
        return type;
    }

    public String getOperator() {
        return operator;
    }
     
     
}
