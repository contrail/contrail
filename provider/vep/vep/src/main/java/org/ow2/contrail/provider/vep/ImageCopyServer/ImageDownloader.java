/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.ImageCopyServer;

import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class ImageDownloader {
    
    private Logger logger;
    public ImageDownloader(){
        logger = Logger.getLogger("VEP.DiskManagement");
    }
     
    public String manageImage(String source,String destination){
        String[] ctrl = source.split("http://");
        //String destination;
        if (ctrl.length > 1) {
            try {
                destination=download(source,destination);
                return destination;
            } catch (Exception ex) {
                logger.error("Impossible to download the image");
                return null;
            }
        }else{
            if(checkImage(source))
            return source;
            else return null;
        }
    }
    
      private String download(String source, String destination) throws MalformedURLException, FileNotFoundException, IOException {
       
              String[] ctrl = source.split("http://");
        //ftp??
        
        if (ctrl.length > 1) {
            logger.debug("Image from http ->"+source);
            URL u;
            InputStream is = null;
            BufferedInputStream bis;
            String s;
            u = new URL(source);

            //----------------------------------------------//
            // Step 3:  Open an input stream from the url.  //
            //----------------------------------------------//

            is = u.openStream();         // throws an IOException

            //-------------------------------------------------------------//
            // Step 4:                                                     //
            //-------------------------------------------------------------//
            // Convert the InputStream to a buffered DataInputStream.      //
            // Buffering the stream makes the reading faster; the          //
            // readLine() method of the DataInputStream makes the reading  //
            // easier.                                                     //
            //-------------------------------------------------------------//

            bis = new BufferedInputStream(is);

            //------------------------------------------------------------//
            // Step 5:                                                    //
            //-----------------------------------------------------//
            // Now just read each record of the input stream, and print   //
            // it out.  Note that it's assumed that this problem is run   //
            // from a command-line, not from an application or applet.    //
            //------------------------------------------------------------//

            FileOutputStream fout = new FileOutputStream(destination);

            byte data[] = new byte[1024];
            int count;
            while ((count = bis.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
            bis.close();
            fout.close();


            logger.info("Image Downloading done,  source:"+source+" destination:"+destination);
            return destination;
        }
         return null;
           
            

    }

    private boolean checkImage(String source) {
        String folderP=VEPHelperMethods.getProperty("imageRepositoryDefault.path", logger);
        ArrayList<String> checker=new ArrayList<String>();
                
                
   File folder=new File(folderP);
   for (File fileEntry : folder.listFiles()) {
        if (!fileEntry.isDirectory());  
            if(source.equals(fileEntry.getAbsolutePath()))
                return true;
        }
    return false;
    }
    
    
    
}
