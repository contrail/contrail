/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.restlet.data.Disposition;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.restlet.data.Status;

public class RestUserDoLogin extends ServerResource {

    private Logger logger;
    private DBHandler db;

    public RestUserDoLogin() {
        logger = Logger.getLogger("VEP.RestUserDoLogin");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestUserDoLogin", dbType);
    }

    @Post("text")
    public Representation getResult(Representation entity) throws ResourceException {
        logger.info("HTML");
        Form form = new Form(entity);
        String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        String action=null;
       
            action=form.getFirstValue("action");
            
        String userid = null;
        //logger.info(password);
        //String actionType = ((String) getRequest().getAttributes().get("action"));
        boolean proceed = false;
        try {
            ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
            if (rs.next()) {
                //logger.info(VEPHelperMethods.makeSHA1Hash(password) + "VS" + rs.getString("password"));
                if (VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password"))) {
                    userid = rs.getString("id");
                    proceed = true;
                } else {
                    proceed = false;
                }
            } else {
                proceed = false;
            }
        } catch (Exception ex) {
            logger.warn("User authentication resulted in exception.");
            //if(logger.isDebugEnabled())
            //	ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
            proceed = false;
        }

        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");

        Representation response = null;

        if (acceptType != null) {
            if (acceptType.contains("html")) {
                response = toHtml(proceed, userid, username, password, action);
            } else if (acceptType.contains("json")) {
                //response = toJson(proceed);
            }
        } else {
            //default rendering ...
            response = toHtml(proceed, userid, username, password, "listCEE");
        }
        return response;
    }

    public Representation toHtml(boolean proceed, String userid, String username, String password, String action) throws ResourceException {
        String result = "";
        //stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        int userId = -1;
        if (proceed) {
            byte[] temp = null;
            String str = "";
            if (action.equalsIgnoreCase("createCEE")) {
                try {
                   // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/create_cee.html"));
                   InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("create_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                } catch (Exception ex) {
                    logger.error("create CEE page missing");
                }
                result = str;
            } else {
                if (action.equalsIgnoreCase("listCEE")) {
                    try {
                        logger.info("SHOW list-cee.html");
                        //str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/list_cee.html"));
                        InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("list_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                    } catch (Exception ex) {
                        logger.error("list CEE page missing");
                    }
                }else{
                    if(action.equalsIgnoreCase("editCEE")){
                      try {
//                        str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/edit_cee.html"));
                  
                          InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("edit_CEE.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
                    } catch (Exception ex) {
                        logger.error("create CEE page missing");
                    }  
                    }
                }
            }
            str = str.replace("%USERNAME%", username);
            str = str.replace("%PASSWORD%", password);
            str = str.replace("%USERID%", userid);
            result=str;
            //FILIPPO:AUTHORIZED
        } else {
            String str = "";
            try {
               // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/wrongLogin.html"));
                InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("wrongLogin.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
            } catch (IOException ex) {
            }
            logger.info(str);
            
            result = str;
        }

        result=result.replaceAll("%WEBUSERHOST%", VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        result=result.replaceAll("%WEBUSERPORT%", VEPHelperMethods.getProperty("webuser-interface.port", logger));
        StringRepresentation value = new StringRepresentation(result, MediaType.TEXT_HTML);
        return value;
    }

    public boolean doMapping(int vepUserId, int cloudId, String cloudName, String version, String headIp, int headPort, String iaasUser, String iaasPass, String[] message) {
        //iaasUser and iaasPass are administrator accounts using which a new user mapping is to be created and stored
        boolean status = false;
        message[0] = "";
        //make sure that a mapping does not already exist
        try {
            ResultSet rs = db.query("select", "count(*)", "accountmap", "WHERE vepuser=" + vepUserId + " AND cloudtype=" + cloudId);
            int count = 0;
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            if (count > 0) {
                status = false;
                message[0] += "Account mapping for the selected cloud type already exist. ";
            } else {
                //generating a random IaaS user ID
                int uname_size = 8;
                String tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
                //now test to see if this is already in use
                boolean genNext = true;
                while (genNext) {
                    logger.info("Testing if iaas mapping by id " + tempUser + " already exists.");
                    rs = db.query("select", "*", "accountmap", "where iaasuser='" + tempUser + "' AND cloudtype=" + cloudId);
                    if (rs.next()) {
                        genNext = true;
                        logger.info("IaaS mapping by id " + tempUser + " already exists. Will try to create a new id.");
                    } else {
                        genNext = false;
                        logger.info("IaaS mapping by id " + tempUser + " does not exists. Will try to register with IaaS daemon.");
                    }
                    rs.close();
                    if (genNext) {
                        tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
                    }
                }
                //generating a random password for this account
                String tempPass = RandomStringUtils.randomAlphanumeric(32);
                int iaasId = -1;
                if (cloudName.equalsIgnoreCase("opennebula") && version.startsWith("2.2")) {
                    try {
                        One2XMLRPCHandler one2handle = new One2XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
                        iaasId = one2handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
                    } catch (Exception ex) {
                        iaasId = -1;
                        message[0] += "Error connecting to the IaaS controller. ";
                    }
                } else if (cloudName.equalsIgnoreCase("opennebula") && (version.startsWith("3.4") || version.startsWith("3.6"))) {
                    try {
                        One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
                        iaasId = one3handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
                    } catch (Exception ex) {
                        message[0] += "Error connecting to the IaaS controller. ";
                        iaasId = -1;
                    }
                }
                if (iaasId != -1) {
                    rs = db.query("select", "max(id)", "accountmap", "");
                    int id = 1;
                    if (rs.next()) {
                        id = rs.getInt(1) + 1;
                    }
                    rs.close();
                    status = db.insert("accountmap", "(" + id + ", " + cloudId + ", '" + tempUser + "', '" + tempPass + "', '" + iaasId + "', " + vepUserId + ")");
                } else {
                    status = false;
                }
            }
        } catch (Exception ex) {
            logger.warn("Exception caught inside doMapping function.");
            //if(logger.isDebugEnabled())
            //	ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
        }
        return status;
    }

    @Post("json")
    public Representation getResult(String value) throws ResourceException {
        logger.info("JSON");
        JSONObject json = (JSONObject) JSONValue.parse(value);
        String username = (String) json.get("username");
        String password = (String) json.get("password");
        String action = (String) json.get("action");
        String userid = null;
        //logger.info(password);
        //String actionType = ((String) getRequest().getAttributes().get("action"));
        boolean proceed = false;
        try {
            ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
            if (rs.next()) {
                //logger.info(VEPHelperMethods.makeSHA1Hash(password) + "VS" + rs.getString("password"));
                if (VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password"))) {
                    userid = rs.getString("id");
                    proceed = true;
                } else {
                    proceed = false;
                }
            } else {
                proceed = false;
            }
        } catch (Exception ex) {
            logger.warn("User authentication resulted in exception.");
            //if(logger.isDebugEnabled())
            //	ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
            proceed = false;
        }


        String acceptType = null;

        Representation response = null;

        if (acceptType != null) {
            if (acceptType.contains("html")) {
                response = toHtml(proceed, userid, username, password, action);
            } else if (acceptType.contains("json")) {
                //response = toJson(proceed);
            }
        } else {
            //default rendering ...
            response = toHtml(proceed, userid, username, password, action);
        }
        return response;


    }
}
