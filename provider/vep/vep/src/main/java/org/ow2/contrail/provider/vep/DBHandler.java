/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * Database handler class that provides methods for manipulating and querying 
 * either a sqlite or a mysql database.
 * 
 * @author piyush
 */
public class DBHandler 
{
    public static Connection dbHandle;
    private Statement statement;
    private ResultSet rs;
    private Logger logger;
    private boolean systemOut;
    private String dbType;
    
    /**
     * Creates the database handler object
     * 
     * @param entryPoint    the local class name where the object is being created
     * @param dbtype        specify the database type, sqlite or mysql
     */
    public DBHandler(String entryPoint, String dbtype)
    {
        try
        {
            systemOut = false;
            dbType = dbtype;
            logger = Logger.getLogger("VEP.dbHandler");
            if(dbHandle != null)
            {
                statement = dbHandle.createStatement();
                statement.setQueryTimeout(30);
                if(systemOut)
                System.out.println("Successfully initialized dbHandler object from " + entryPoint + " with type set to " + dbType + ".");
                logger.trace("dbHandler object initialized. EntryPoint: " + entryPoint);
            }
            else
            {
                logger.warn("dbHandler object initialization failed, null value received.");
                statement = null;
                if(systemOut)
                System.out.println("dbHandler object initialization from " + entryPoint + " failed! NULL value found.");
            }
        }
        catch(SQLException ex)
        {
            statement = null;
            logger.error("SQLException caught while initializating dbHandler.");
            if(systemOut)
            System.out.println("Caught exception in dbHandler construction.");
        }
    }
    
    /**
     * Queries the database and returns the results
     * 
     * @param queryType could be <b>select</b>
     * @param filter    record fields to return, use <b>*</b> for all fields
     * @param table     table name
     * @param conditions    query conditions
     * @return          the query set as ResultSet object
     */
    public ResultSet query(String queryType, String filter, String table, String conditions)
    {
        try
        {
            if(systemOut)
            System.out.println("Executing query: " + queryType + " " + filter + " from " + table + " " + conditions);
            logger.debug("Executing query: " + queryType + " " + filter + " from " + table + " " + conditions);
            rs = statement.executeQuery(queryType + " " + filter + " from " + table + " " + conditions);
            return rs;
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL query.");
            ex.printStackTrace(System.out);
            return null;
        }
    }
    
    /**
     * Inserts a template into the database for a specific id, needs a special request to avoid characters escaping (and sql injection).
     * Uses a PreparedStatement
     * @param id	the id of the vmslot to update
     * @param template	the vmslot template as a string
     * @return          boolean true/false depending if the operation succeeded or not
     */
    public boolean updateTemplate(int vmslotid, String template)
    {
    	boolean result = false;
    	java.sql.PreparedStatement stmt = null;
    	try{
    		stmt = dbHandle.prepareStatement("UPDATE vmslots SET template = ? WHERE id="+vmslotid);
    		stmt.setString(1, template);
    		logger.debug("Executing query: UPDATE vmslots SET template = " + template + " WHERE id=" + vmslotid);
            if(systemOut)
            System.out.println("Executing query: UPDATE vmslots SET template = " + template + " WHERE id=" + vmslotid);
            stmt.executeUpdate();
            result = true;
    	}

		catch(SQLException ex)
		{
			logger.error("SQLException caught while performing SQL update.");
            ex.printStackTrace(System.out);
		}
    	finally {
    		if(stmt != null)
    		{
    			try {
					stmt.close();
				} catch (SQLException ex) {
					logger.error("SQLException caught while performing SQL update.");
		            ex.printStackTrace(System.out);
				}
    		}
    	}
    	
    	return result;
    }
    
    /**
     * Updates the context text field of a specific ovfmap, needs a special request to avoid characters escaping (and sql injection).
     * Uses a PreparedStatement
     * @param id	the id of the ovfmap or the vmslot to update
     * @param context	the context as a string
     * @return          boolean true/false depending if the operation succeeded or not
     */
    public boolean updateContext(int id, String context, String table)
    {
    	boolean result = false;
    	java.sql.PreparedStatement stmt = null;
    	try{
    		if(table.equals("ovfmap"))
    		{
        		stmt = dbHandle.prepareStatement("UPDATE ovfmap SET context = ? WHERE id="+id);
        		stmt.setString(1, context);
        		logger.debug("Executing query: UPDATE ovfmap SET context = " + context + " WHERE id=" + id);
                if(systemOut)
                System.out.println("Executing query: UPDATE ovfmap SET context = " + context + " WHERE id=" + id);
    		} else if(table.equals("vmslots"))
    		{
        		stmt = dbHandle.prepareStatement("UPDATE vmslots SET context = ? WHERE id="+id);
        		stmt.setString(1, context);
        		logger.debug("Executing query: UPDATE vmslots SET context = " + context + " WHERE id=" + id);
                if(systemOut)
                System.out.println("Executing query: UPDATE vmslots SET context = " + context + " WHERE id=" + id);
    		}
            stmt.executeUpdate();
            result = true;
    	}

		catch(SQLException ex)
		{
			logger.error("SQLException caught while performing SQL update.");
            ex.printStackTrace(System.out);
		}
    	finally {
    		if(stmt != null)
    		{
    			try {
					stmt.close();
				} catch (SQLException ex) {
					logger.error("SQLException caught while performing SQL update.");
		            ex.printStackTrace(System.out);
				}
    		}
    	}
    	
    	return result;
    }
    
    /**
     * inserts a row into the desired table of the database
     * 
     * @param table     table name to insert the record into
     * @param values    list of comma separated values inclosed in ( ... )
     * @return          boolean true/false depending if the operation succeeded or not
     */
    public boolean insert(String table, String values)
    {
        try
        {
            if(systemOut)
            System.out.println("Executing query: INSERT INTO " + table + " VALUES " + values);
            if(table.startsWith("ovf("))
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + " truncated for sanitization reasons.");
            else
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + values);
            statement.executeUpdate("INSERT INTO " + table + " VALUES " + values);
            ResultSet rsG = statement.getGeneratedKeys();
            if(rsG.next())
            {
            	System.err.println("INSERTED KEY:"+rsG.getInt(1));
            }
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL insert.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    /**
     * inserts a row into the desired table of the database
     * 
     * @param table     table name to insert the record into
     * @param values    list of comma separated values inclosed in ( ... )
     * @return          boolean the newly inserted key or 0
     */
    public int insertAndGetKey(String table, String values)
    {
    	int key = 0;
        try
        {
            if(systemOut)
            System.out.println("Executing query: INSERT INTO " + table + " VALUES " + values);
            if(table.startsWith("ovf("))
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + " truncated for sanitization reasons.");
            else
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + values);
            statement.executeUpdate("INSERT INTO " + table + " VALUES " + values);
            ResultSet rsG = statement.getGeneratedKeys();
            if(rsG.next())
            {
            	key = rsG.getInt(1);
            }
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL insert.");
            ex.printStackTrace(System.out);
            return 0;
        }
        return key;
    }
    
    
    /**
     * Updates a record in the table with new values
     * 
     * @param table     table name in which the record belongs to
     * @param values    comma separated list of new key=value pairs
     * @param conditions    rule for target record selection beginning with <b>WHERE</b>
     * @return          boolean true/false depending if the operation succeeded or not
     */
    public boolean update(String table, String values, String conditions)
    {
        try
        {
            logger.debug("Executing query: UPDATE " + table + " SET " + values + " " + conditions);
            if(systemOut)
            System.out.println("Executing query: UPDATE " + table + " SET " + values + " " + conditions);
            statement.executeUpdate("UPDATE " + table + " SET " + values + " " + conditions);
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL update.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    /**
     * Deletes a record from the table in the database
     * 
     * @param table     the table name
     * @param conditions    conditions for the selection of the record to be deleted
     * @return          boolean true/false depending if the operation succeeded or not
     */
    public boolean delete(String table, String conditions)
    {
        try
        {
            logger.debug("Executing query: DELETE FROM " + table + " " + conditions);
            if(systemOut)
            System.out.println("Executing query: DELETE FROM " + table + " " + conditions);
            statement.executeUpdate("DELETE FROM " + table + " " + conditions);
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL delete.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    /**
     * Marks the beginning of the transactional record entry sequence
     * 
     * @return  boolean true/false depending if the operation succeeded or not
     */
    public boolean beginTransaction()
    {
        try
        {
            logger.debug("Executing query: BEGIN TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: BEGIN TRANSACTION");
            if(dbType.contains("mysql"))
                statement.execute("START TRANSACTION"); //changed from begin transaction
            else
                statement.execute("BEGIN TRANSACTION"); //changed from begin transaction
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL begin transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    /**
     * Commits the transaction sequence
     * 
     * @return      boolean true/false depending if the operation succeeded or not
     */
    public boolean commitTransaction()
    {
        try
        {
            logger.debug("Executing query: COMMIT TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: COMMIT TRANSACTION");
            if(dbType.contains("mysql"))
                statement.execute("COMMIT"); //changed from COMMIT TRANSACTION
            else
                statement.execute("COMMIT TRANSACTION"); //changed from COMMIT TRANSACTION
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL commit transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    /**
     * Marks the end of the transaction section
     * 
     * @return      boolean true/false depending if the operation succeeded or not
     */
    public boolean endTransaction()
    {
        try
        {
            logger.debug("Executing query: END TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: END TRANSACTION");
            statement.execute("END TRANSACTION");
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL end transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    /**
     * This function can validate if the input word is database safe or not
     * 
     * @param str       the single word to be validated
     * @return      boolean true/false depending if the word is database safe or not
     */
    public static boolean validateSingleWord(String str)
    {
        //if str contains ' or " then invalid
        if(str.contains("'") || str.contains("\"") || str.contains(" ")) return false;
        else return true;
    }
    
    /**
     * This function can be used to check a full sentence if it is database safe or not
     * 
     * @param str       the sentence to be validated
     * @return          boolean true/false depending if the sentence is database safe or not
     */
    public static boolean validateSentence(String str)
    {
        //if str contains ' or " then invalid
        if(str.contains("'")) return false;
        else return true;
    }
    
    /**
     * makes a audit log entry into the db history table, entry timestamp is auto generated
     *
     * @param uname         username on whose behalf the action was performed
     * @param sqlString     the SQL statement that was executed
     * @param description   description string
     * @param code          three character code value to enter
     * @return              void, returns nothing
     */
    public void auditLog(String uname, String sqlString, String description, String code)
    {
        //this function only makes inserts into the history table.
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try
        {
            insert("history", "('" + dateFormat.format(date) + "', '" + uname + "', '" + code + "', '" + sqlString + "', '" + description + "')");
        }
        catch(Exception ex)
        {
            insert("history", "('" + dateFormat.format(date) + "', '" + uname + "', '" + "EXCP" + "', '" + sqlString + "', '" + ex.getMessage() + "')");
        }
    }
}
