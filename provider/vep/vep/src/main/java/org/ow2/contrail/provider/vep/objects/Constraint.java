/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Constraint extends VEPObject {

	private String constraintType;
	private String parameter;
	private int vmSlotId;
	private int ceevmhandlerid;
	private int ovfmapid;
	private String ceeId;
	private String constraintListId;
	private int type;

	static final int VMHANDLER_CONST = 1;
	static final int OVFMAP_CONST = 2;
	static final int VMSLOT_CONST = 3;
	
	public Constraint(boolean instantiate) {
		super("Constraint", instantiate);
	}

	public Constraint(String ceeId, String constraintListId) {
		super("Constraint", true);
		this.ceeId = ceeId; //not useful?
		this.constraintListId = constraintListId;
		this.setId("/cee/"+ceeId+"/constraint/"+constraintListId);
		this.setResourceUri("VEP/CEEConstraint");
	}

//	logger.debug("Retrieving Constraints for cee " + ceeId);
//	HashMap<Integer, Integer> constraintsType = new HashMap<Integer, Integer>();
//	rs = db.query("select", "id, constrainttype", "ceeconstraints", "where ceeid = " + ceeId);
//	while (rs.next()){
//		constraintsType.put(rs.getInt("id"), rs.getInt("constrainttype"));
//	}
//	//finish the rs, otherwise it needs to be reset
//	for(Entry<Integer, Integer> entry : constraintsType.entrySet())
//	{
//		
//	}
	
	public boolean retrieveConstraint() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "ceeconstraintsid", "ceeconstraintslist", "where id="+constraintListId);
		if (rs.next())
		{
			this.setName("N/A");
			int ceeconstraintsid = rs.getInt("ceeconstraintsid");
			rs = db.query("select", "constraints.descp", "constraints, ceeconstraintslist, ceeconstraints", 
					"where ceeconstraints.id=" + ceeconstraintsid + " and constraints.id=ceeconstraints.constrainttype group by constraints.id");
			constraintType = rs.getString("descp");
			//now find out the type
			rs = db.query("select", "*", "ceevmhandlerconstraint", "where ceeconstraintlistid="+constraintListId);
			if(rs.next())
			{
				//it's a vmhandler const
				ceevmhandlerid = rs.getInt("ceevmhandlerid");
				parameter = rs.getString("param");
				this.type = VMHANDLER_CONST;
			} else {
				rs = db.query("select", "*", "ovfmapconstraint", "where ceeconstraintlistid="+constraintListId);
				if(rs.next())
				{
					//it's a ovfmap const
					ovfmapid = rs.getInt("ovfmapid");
					parameter = rs.getString("param");
					this.type = OVFMAP_CONST;
				} else {
					rs = db.query("select", "*", "vmslotconstraint", "where ceeconstraintlistid="+constraintListId);
					if(rs.next())
					{
						//it's a vmslot const
						vmSlotId = rs.getInt("vmslotid");
						parameter = rs.getString("param");
						this.type = VMSLOT_CONST;
					} else {
						//Oops, something happened to the db
					}
				}
			}
			success = true;
		}
		return success;
	}

	public String getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public int getVmSlotId() {
		return vmSlotId;
	}

	public void setVmSlotId(int vmSlotId) {
		this.vmSlotId = vmSlotId;
	}

	public int getCeevmhandlerid() {
		return ceevmhandlerid;
	}

	public void setCeevmhandlerid(int ceevmhandlerid) {
		this.ceevmhandlerid = ceevmhandlerid;
	}

	public int getOvfmapid() {
		return ovfmapid;
	}

	public void setOvfmapid(int ovfmapid) {
		this.ovfmapid = ovfmapid;
	}

	public String getCeeId() {
		return ceeId;
	}

	public void setCeeId(String ceeId) {
		this.ceeId = ceeId;
	}

	public String getConstraintListId() {
		return constraintListId;
	}

	public void setConstraintListId(String constraintListId) {
		this.constraintListId = constraintListId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
