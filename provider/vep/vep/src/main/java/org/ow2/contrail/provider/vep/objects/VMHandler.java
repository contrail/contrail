/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VMHandler extends VEPObject {
	
	private int cpu_l;
	private int cpu_h;
	private int ram_l;
	private int ram_h;
	private int corecount_l;
	private int corecount_h;
	private int disksize_l;
	private int disksize_h;
	
	private String vmHandlerId;
	private String ceeId;

	public VMHandler(boolean instantiate) {
		super("VMHandler", instantiate);
	}
	
	public VMHandler(String vmHandlerId) {
		super("VMHandler", true);
		this.vmHandlerId = vmHandlerId;
		this.setId("/vmhandler/"+vmHandlerId);
		this.setResourceUri("VEP/VMHandler");
	}
	
	public VMHandler(String ceeId, String vmHandlerId) {
		super("VMHandler", true);
		this.ceeId = ceeId; //not useful?
		this.vmHandlerId = vmHandlerId;
		this.setId("/cee/"+ceeId+"/vmhandler/"+vmHandlerId);
		this.setResourceUri("VEP/VMHandler");
	}
	
	public boolean retrieveVMHandler() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "vmhandler", "where id="+vmHandlerId);
		if (rs.next())
		{
			this.setName(rs.getString("name"));
			this.setCorecount_l(rs.getInt("corecount_low"));
			this.setCorecount_h(rs.getInt("corecount_high"));
			this.setDisksize_l(rs.getInt("disksize_low"));
			this.setDisksize_h(rs.getInt("corecount_high"));
			this.setCpu_l(rs.getInt("cpufreq_low"));
			this.setCpu_h(rs.getInt("cpufreq_high"));
			this.setRam_l(rs.getInt("ram_low"));
			this.setRam_h(rs.getInt("ram_high"));
			success = true;
		}
		return success;
	}
	
	public int getCpu_l() {
		return cpu_l;
	}

	public void setCpu_l(int cpu_l) {
		this.cpu_l = cpu_l;
	}

	public int getCpu_h() {
		return cpu_h;
	}

	public void setCpu_h(int cpu_h) {
		this.cpu_h = cpu_h;
	}

	public int getRam_l() {
		return ram_l;
	}

	public void setRam_l(int ram_l) {
		this.ram_l = ram_l;
	}

	public int getRam_h() {
		return ram_h;
	}

	public void setRam_h(int ram_h) {
		this.ram_h = ram_h;
	}

	public int getCorecount_l() {
		return corecount_l;
	}

	public void setCorecount_l(int corecount_l) {
		this.corecount_l = corecount_l;
	}

	public int getCorecount_h() {
		return corecount_h;
	}

	public void setCorecount_h(int corecount_h) {
		this.corecount_h = corecount_h;
	}

	public int getDisksize_l() {
		return disksize_l;
	}

	public void setDisksize_l(int disksize_l) {
		this.disksize_l = disksize_l;
	}

	public int getDisksize_h() {
		return disksize_h;
	}

	public void setDisksize_h(int disksize_h) {
		this.disksize_h = disksize_h;
	}

}
