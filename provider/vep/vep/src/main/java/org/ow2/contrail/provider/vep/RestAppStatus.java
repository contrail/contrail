/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestAppStatus extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	private SSLCertHandler certHandler;
	
	public RestAppStatus()
	{
		logger = Logger.getLogger("VEP.RestAppStatus");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestAppStatus", dbType);
        certHandler = new SSLCertHandler(true); //bypasses the creation of CA service objects
	}
	
	@Post
	public Representation showAppStatus(Representation entity) throws ResourceException
	{
		Form form = new Form(entity);
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String username = "";
		String password = "";
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println(key + ", " + value);
			if(key.equalsIgnoreCase("username")) username = value;
			if(key.equalsIgnoreCase("password")) password = value;
		}
		
		//first verify whether the username and password are correct, then proceed for certificate verification
		boolean proceed = false;
		int userId = -1;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	proceed = false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        		{
	        			userId = rs.getInt("id");
	        			proceed = true;
	        		}
	        		else
	        			proceed = false;
	        	}
	        	else
	        	{
	        		proceed = false;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception caught ", ex);
        	proceed = false;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	//now verify the certificate information
        	String certUser = "";
        	@SuppressWarnings("unchecked")
			List<X509Certificate> certs = (List<X509Certificate>)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
            for(int i=0; certs != null && i < certs.size() && proceed; i++)
            {
                X509Certificate Cert = certs.get(i);
                proceed = SSLCertHandler.isCertValid(Cert);
                certUser = SSLCertHandler.getCertDetails(Cert, "CN");
                logger.info("Certificate User name: " + certUser + " Validity [" + proceed + "]");
            }
            
            if(certUser.equalsIgnoreCase(username))
            {
            	String action = ((String) getRequest().getAttributes().get("action"));
            	if(action == null)
            		displayStatus(stringBuilder, username, password, userId, form);
            	else
            	{
            		doAction(stringBuilder, username, password, userId, form, action);
            	}
            }
            else
            {
            	stringBuilder.append("<b>Welcome to VEP Virtual Machine Management Console</b><br>").append(username).append(", access to this page <b>has been blocked</b> due to certificate error!<hr>");
    	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
    	        
    	        stringBuilder.append("<font color='red'>Invalid Certificate! Please re-install your certificate and try again. You will be forced to logout. Please re-login to download your user certificate.</font><br><br>");
    	        
    	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
    	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
    	        stringBuilder.append("<tr><td align='left'><input type='submit' value='logout'>");
    	        stringBuilder.append("</table></form>");
    	        stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;background:#FFD1B7;;padding:5px;'>It is advised to close your browser after you logout in order to clear the old certificate selection from the browser cache.</p>");
    	        stringBuilder.append("</table>");
            }
        }
        else
        {
        	stringBuilder.append("<b>Welcome to VEP Virtual Machine Management Console</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
		return value;
	}
	
	private void displayStatus(StringBuilder stringBuilder, String username, String password, int userId, Form form)
	{
		stringBuilder.append("Welcome <i><font color='green'>").append(username).append("</font></i> to VEP <b>Application</b> Status Page. ")
			.append("You can check the status of your deployed applications from this page.<br><hr>");
		stringBuilder.append("<table style='width:1000px;font-family:Verdana;font-size:10pt;'>")
			.append("<tr><td style='width:800px;font-family:Verdana;font-size:10pt;'>");
		///////////////////////// main VM management elements go next

		try
		{
			stringBuilder.append("<table cellspacing='2' cellpadding='2' style='width:799px;font-family:Verdana;font-size:9pt;border:1px;border-style:solid;background:#FBF2EF;'>");
			ResultSet rs = db.query("select", "id, name, status", "cee", "WHERE userid=" + userId);
			int count = 0;
			while(rs.next())
			{
				count++;
				int ceeId = rs.getInt("id");
				stringBuilder.append("<tr><td valign='top' style='background:#EBFDCF;border:1px;border-style:solid;border-color:#A5DF00;'>CEE Name: ").append(rs.getString("name")).append("<br>Status: ").append(rs.getString("status"));
				//getting the list of ovf for this cee
				//first getting the default application id
				rs = db.query("select", "id", "application", "WHERE ceeid=" + ceeId + " AND name='default'");
				int appId = -1;
				if(rs.next()) appId = rs.getInt("id");
				rs.close();
				
				rs = db.query("select", "id", "ovf", "WHERE applicationid=" + appId);
				
				int count1 = 0;
				while(rs.next())
				{
					count1++;
					int ovfId = rs.getInt("id");
					//now getting the list of ovfmaps for this OVF 
					rs = db.query("select", "id, ovfid, status", "ovfmap", "WHERE ovfcontainerid=" + ovfId);
					
					stringBuilder.append("<td valign='top'>");
					stringBuilder.append("<table cellspacing='2' cellpadding='2' style='font-family:Verdana;font-size:9pt;border:1px;border-style:dashed;'>");
					int count2 = 0;
					while(rs.next())
					{
						count2++;
						int ovfmapId = rs.getInt("id");
						
						stringBuilder.append("<tr><td valign='top' style='background:#CFE1FD;'>OVF Virtual System Name: ").append(rs.getString("ovfid")).append("<br>Status: ").append(rs.getString("status"));
						//now getting the list of VMslots for this ovfmap entry
						rs = db.query("select", "id, status", "vmslots", "WHERE ovfmapid=" + ovfmapId);
						
						stringBuilder.append("<td valign='top'>");
						stringBuilder.append("<table cellspacing='2' cellpadding='2' style='font-family:Verdana;font-size:9pt;border:1px;border-style:dotted;'>");
						int count3 = 0;
						while(rs.next())
						{
							count3++;
							stringBuilder.append("<tr><td valign='top' style='background:#CFEEFD;'>Virtual Machine Slot: ").append(rs.getInt("id")).append("<br>Status: ").append(rs.getString("status"));
						}
						stringBuilder.append("</table>");
						
						//resetting the sql query
						int counter = 0;
						rs = db.query("select", "id, ovfid, status", "ovfmap", "WHERE ovfcontainerid=" + ovfId);
						while(counter < count2) {rs.next(); counter++; }
					}
					stringBuilder.append("</table>");
					
					//resetting the sql query
					int counter = 0;
					rs = db.query("select", "id", "ovf", "WHERE applicationid=" + appId);
					while(counter < count1) {rs.next(); counter++; }
				}
				
				//now resetting the SQL query
				int counter = 0;
				rs = db.query("select", "id, name, status", "cee", "WHERE userid=" + userId);
				while(counter < count) {rs.next(); counter++; }
			}
			stringBuilder.append("</table>");
		}
		catch(Exception ex)
		{
			stringBuilder.append("</table>");
		}
		stringBuilder.append("<td valign='top' style='font-family:Verdana;font-size:10pt;'>");
		///////////////////////// navigation buttons go here
		stringBuilder.append("<form name='goback' action='../dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
		stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		//stringBuilder.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
		//	.append("<input type='hidden' name='requesttype' value='loadcee'>");
        stringBuilder.append("<input type='submit' value='cancel and go back'></form>");
        
		stringBuilder.append("</table>");
	}
	
	private void doAction(StringBuilder stringBuilder, String username, String password, int userId, Form form, String action)
	{
		
	}
}
