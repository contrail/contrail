/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;


import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.log4j.Logger;


public class RestAdminManageDatacenter extends ServerResource
{
	private Logger logger;
	private DBHandler db;
    private String webHost, webPort;
	
	public RestAdminManageDatacenter()
	{
		logger = Logger.getLogger("VEP.RestAdminManageDatacenter");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        webHost = VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger);
        webPort = VEPHelperMethods.getProperty("webuser-interface.port", logger);
        db = new DBHandler("RestAdminManageDatacenter", dbType);
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
		Form form = new Form(entity);
		String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        
        boolean proceed = false;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	if(username.equalsIgnoreCase("admin") && VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase("789b49606c321c8cf228d17942608eff0ccc4171"))
	        	{
	        		proceed = true;
	        	}
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        			proceed = true;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("Admin authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception Caught: ", ex);
        	proceed = false;
        }
        
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(proceed, username, password, form);
            else if(acceptType.contains("json"))
            {
                //response = toJson(proceed);
            }
        }
        else
        {
            //default rendering ...
            response = toHtml(proceed, username, password, form);
        }
        return response;
    }
	
	public Representation toHtml(boolean proceed, String username, String password, Form form) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	String action = ((String) getRequest().getAttributes().get("action"));
        	stringBuilder.append("<b>Welcome to VEP Service Management Tool</b><br>");
        	stringBuilder.append(username).append(", you wish to manage your datacenter!<hr>");
        	stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
        	///////////////////////////////displaying the summary of the datacenter now/////////////////////
        	if(action == null)
        	{
        		stringBuilder.append("Summary view of your datacenter resources -<br>");
        		stringBuilder.append("<table cellspacing='0' cellpadding='0' style='font-size:9pt;font-family:Courier;background:white;width:790px;'>");
        		stringBuilder.append("<tr><td valign='top' align='left' style='width:180px;font-family:Verdana;font-size:9pt;color:black;'>")	
        				.append("<div style='background:#AAB3C4;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/datacenter.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(1);'>datacenter</a></table></div>")
        				.append("<div style='background:#8A909D;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/cluster.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(2);'>cluster</a></table></div>")
        				.append("<div style='background:#AAB3C4;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/rack.jpg' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(3);'>rack</a></table></div>")
        				.append("<div style='background:#8A909D;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/l2switch.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(4);'>l2switch</a></table></div>")
        				.append("<div style='background:#AAB3C4;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/storage.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(5);'>storage</a></table></div>")
        				.append("<div style='background:#8A909D;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/host.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(6);'>host</a></table></div>")
        				.append("<div style='background:#AAB3C4;'><table><tr><td><img src='http://"+webHost+":"+webPort+"/cloudnetwork.png' width='24px'><td><a style='color:black;' href='javascript:void(0);' onClick='javascript:displayTab(7);'>cloud network</a></table></div>");
        		stringBuilder.append("<td valign='top' style='width:575px;font-family:Verdana;font-size:10pt;color:black;background:white;'>");
        		
        		stringBuilder.append("<script type=\"text/javascript\">")
        		.append("function displayTab(val) {")
        		.append("if(val == 1) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"block\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 2) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"block\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 3) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"block\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 4) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"block\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 5) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"block\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 6) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"block\";")
        		.append("document.getElementById('network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 7) {")
        		.append("document.getElementById('default').style.display = \"none\";")
        		.append("document.getElementById('datacenter').style.display = \"none\";")
        		.append("document.getElementById('cluster').style.display = \"none\";")
        		.append("document.getElementById('rack').style.display = \"none\";")
        		.append("document.getElementById('l2switch').style.display = \"none\";")
        		.append("document.getElementById('storage').style.display = \"none\";")
        		.append("document.getElementById('host').style.display = \"none\";")
        		.append("document.getElementById('network').style.display = \"block\";")
        		.append("}")
        		.append("} </script>");
        		
        		stringBuilder.append("<div id='default' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:block;'>");
        		stringBuilder.append("<br>Please click on the appropriate link ...");
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='datacenter' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "datacenter", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/datacenter-big.png'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Name: ").append(rs.getString("name")).append("<br>Description: ").append(rs.getString("descp"))
    					.append("<br>Country: ");
        				counter++;
        				int countryId = rs.getInt("countryid");
        				rs = db.query("select", "*", "countrylist", "WHERE id=" + countryId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "datacenter", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No datacenter found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the datacenter details.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='cluster' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "cluster", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/cluster-big.jpg'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Name: ").append(rs.getString("name")).append("<br>Description: ").append(rs.getString("descp"))
    					.append("<br>Belongs to datacenter: ");
        				counter++;
        				int datacenterId = rs.getInt("datacenterid");
        				int interconnectId = rs.getInt("interconnectid");
        				rs = db.query("select", "*", "datacenter", "WHERE id=" + datacenterId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("<br>Interconnect: ");
        				}
        				rs.close();
        				rs = db.query("select", "*", "interconnect", "WHERE id=" + interconnectId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "cluster", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No cluster found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the cluster details.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='rack' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "rack", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/rack-big.png'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Name: ").append(rs.getString("name")).append("<br>Description: ").append(rs.getString("descp"))
    					.append("<br>Belongs to cluster: ");
        				counter++;
        				int clusterId = rs.getInt("clusterid");
        				rs = db.query("select", "*", "cluster", "WHERE id=" + clusterId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "rack", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No rack found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the rack data.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='l2switch' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "l2switch", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/switch-big.jpg'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Description: ").append(rs.getString("descp")).append("<br>Switch Type: ");
        				counter++;
        				int switchtypeId = rs.getInt("switchtypeid");
        				int mbps = rs.getInt("mbps");
        				rs = db.query("select", "*", "switchtype", "WHERE id=" + switchtypeId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("<br>Switching speed (in mbps): ");
        				}
        				rs.close();
        				stringBuilder.append(mbps).append("</div>");
        				rs = db.query("select", "*", "l2switch", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No l2switch found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the l2switch data.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='storage' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "storage", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/storage-big.png'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Name: ").append(rs.getString("name")).append("<br>Mount Endpoint: ").append(rs.getString("endpoint")).append("<br>Storage Type: ");
    					int storagetypeId = rs.getInt("storagetypeid");
    					int rackId = rs.getInt("rackid");
        				counter++;
        				rs = db.query("select", "*", "storagetype", "WHERE id=" + storagetypeId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("<br>Belongs in rack: ");
        				}
        				rs.close();
        				rs = db.query("select", "*", "rack", "WHERE id=" + rackId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "storage", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No storage found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the storage data.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='host' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:228px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "host", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/host-big.jpg'><td><div style='width:350px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Host Name: ").append(rs.getString("hostname")).append("<br>CPU Architecture: ").append(rs.getString("cpuarch")).append("<br>Virtualization: ").append(rs.getString("virtualization"))
    					.append("<br>IaaS ID: ").append(rs.getString("iaasid")).append("<br>Number of CPU Cores: ").append(rs.getInt("corecount")).append("<br>CPU Frequency (in MHz): ")
    					.append(rs.getInt("cpufreq")).append("<br> Main Memory (in KB): ").append(rs.getInt("ram")).append("<br> Disk Capacity (in MB): ").append(rs.getInt("disksize")).append("<br>L2 Switch ID: ");
    					int l2switchId = rs.getInt("l2switchid");
    					int rackId = rs.getInt("rackid");
    					int cloudId = rs.getInt("cloudtypeid");
        				counter++;
        				rs = db.query("select", "id, switchtypeid", "l2switch", "WHERE id=" + l2switchId);
        				if(rs.next())
        				{
        					int id = rs.getInt("id");
        					int switchTypeId = rs.getInt("switchtypeid");
        					rs = db.query("select", "name", "switchtype", "WHERE id=" + switchTypeId);
        					if(rs.next())
        					{
        						stringBuilder.append(id).append(" - ").append(rs.getString("name")).append("<br>Belongs in Rack: ");
        					}
        				}
        				rs.close();
        				rs = db.query("select", "*", "rack", "WHERE id=" + rackId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("<br>Belongs to Cloud: ");
        				}
        				rs.close();
        				rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "host", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No host found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the host data.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("<div id='network' style='padding:5px;font-family:Courier;font-size:10pt;color:black;overflow:auto;width:580px;height:198px;border-width:1px;border-color:silver;border-style:solid;display:none;'>");
        		try
        		{
        			int counter = 0;
        			ResultSet rs = db.query("select", "*", "cloudnetwork", "");
        			stringBuilder.append("<table cellpadding='2' cellspacing='2' style='font-family:Courier;font-size:10pt;color:navy;'><tr>");
        			while(rs.next())
        			{
        				stringBuilder.append("<td><img src='http://"+webHost+":"+webPort+"/network-big.png'><td><div style='width:250px;background:#C6D9F1;color:#0D4992;font-family:Courier;font-size:9pt;padding:5px;'>")
        				.append("Internal ID: ").append(rs.getInt("id"))
    					.append("<br>Name: ").append(rs.getString("name")).append("<br>Network Type: ").append(rs.getString("type")).append("<br>IaaS ID: ").append(rs.getString("iaasid")).append("<br>Cloud: ");
    					int cloudId = rs.getInt("cloudtypeid");
        				counter++;
        				rs = db.query("select", "name", "cloudtype", "WHERE id=" + cloudId);
        				if(rs.next())
        				{
        					stringBuilder.append(rs.getString("name")).append("</div>");
        				}
        				rs.close();
        				rs = db.query("select", "*", "cloudnetwork", "");
        				int count = 0;
        				while(count < counter) { rs.next(); count++; }
        			}
        			rs.close();
        			if(counter == 0)
        			{
        				stringBuilder.append("<td><div style='background:#FFCCCC;color:black;padding:5px;'>No cloud network found.</div>");
        			}
        			stringBuilder.append("</table>");
        		}
        		catch(Exception ex)
        		{
        			logger.warn("Exception caught while retrieving the cloud network data.");
        			//if(logger.isDebugEnabled())
        			//	ex.printStackTrace(System.err);
        			logger.debug("Exception Caught: ", ex);
        		}
        		stringBuilder.append("</div>");
        		
        		stringBuilder.append("</table><br>");
        		///////////////////////////now showing the form for modifying various elements
        		stringBuilder.append("Datacenter settings management panel -");
        		stringBuilder.append("<table cellspacing='0' style='width:790px;font-family:Verdana;font-size:9pt;'>");
        		stringBuilder.append("<tr><td style='width:200px;' valign='top'>");
        		/////////////////////datacenter element menu goes here
        		stringBuilder.append("<div style='background:#666666;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(1);'>Manage Datacenter</a></div>");
        		stringBuilder.append("<div style='background:#333333;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(2);'>Manage Cluster</a></div>");
        		stringBuilder.append("<div style='background:#666666;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(3);'>Manage Rack</a></div>");
        		stringBuilder.append("<div style='background:#333333;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(4);'>Manage L2switch</a></div>");
        		stringBuilder.append("<div style='background:#666666;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(5);'>Manage Storage</a></div>");
        		stringBuilder.append("<div style='background:#333333;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(6);'>Manage Host</a></div>");
        		stringBuilder.append("<div style='background:#666666;padding:5px;border-width:1px;border-style:dotted;width:175px;font-family:Verdana;font-size:9pt;'><a style='color:white;' href='javascript:void(0);' onclick='javascript:displayFormTab(7);'>Manage Cloud Network</a></div>");
        		stringBuilder.append("<td valign='top' style='width:780px;'>");
        		
        		stringBuilder.append("<script type=\"text/javascript\">")
        		.append("function displayFormTab(val) {")
        		.append("if(val == 1) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"block\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 2) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"block\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 3) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"block\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 4) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"block\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 5) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"block\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 6) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"block\";")
        		.append("document.getElementById('form-network').style.display = \"none\";")
        		.append("}")
        		.append("if(val == 7) {")
        		.append("document.getElementById('form-default').style.display = \"none\";")
        		.append("document.getElementById('form-datacenter').style.display = \"none\";")
        		.append("document.getElementById('form-cluster').style.display = \"none\";")
        		.append("document.getElementById('form-rack').style.display = \"none\";")
        		.append("document.getElementById('form-l2switch').style.display = \"none\";")
        		.append("document.getElementById('form-storage').style.display = \"none\";")
        		.append("document.getElementById('form-host').style.display = \"none\";")
        		.append("document.getElementById('form-network').style.display = \"block\";")
        		.append("}")
        		.append("} </script>");
        		
        		/////////////////////datacenter menu action pane goes here
        		stringBuilder.append("<div id='form-default' style='padding:5px;border-width:1px;border-style:dashed;width:575px;height:400px;font-family:Verdana;font-size:10pt;'>");
        		stringBuilder.append("<br><br>Please click on the buttons on your left to access the approariate forms.");
        		stringBuilder.append("</div>");
        		////////////////////form for generating datacenter fields////////////////
        		stringBuilder.append("<div id='form-datacenter' style='background:#666666;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
        		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
        		stringBuilder.append("<form name='editdatacenter' action='../admin/managedatacenter/editdatacenter' method='post' style='font-family:Verdana;font-size:10pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:10pt;background:#666666;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='datacenterid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "datacenter", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='datacentername' size='30'>");
				stringBuilder.append("<tr><td>Datacenter physical location<td align='right'><select name='countryid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "countrylist", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("<tr><td>Brief description<td align='right'><textarea name='datacenterdesc' cols='30' rows='4'> </textarea>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
        		stringBuilder.append("</div>");
        		////////////////////form for generating cluster fields////////////////
	    		stringBuilder.append("<div id='form-cluster' style='background:#333333;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
	    		stringBuilder.append("<form name='editcluster' action='../admin/managedatacenter/editcluster' method='post' style='font-family:Verdana;font-size:10pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:10pt;background:#333333;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='clusterid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "cluster", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='clustername' size='30'>");
				stringBuilder.append("<tr><td>Select interconnect technology<td align='right'><select name='interconnectid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "interconnect", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("<tr><td>Brief description<td align='right'><textarea name='clusterdesc' cols='30' rows='4'> </textarea>");
				stringBuilder.append("<tr><td>Belongs to datacenter: <td align='right'><select name='datacenterid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "datacenter", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
	    		////////////////////form for generating rack fields////////////////
	    		stringBuilder.append("<div id='form-rack' style='background:#666666;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
	    		stringBuilder.append("<form name='editrack' action='../admin/managedatacenter/editrack' method='post' style='font-family:Verdana;font-size:10pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:10pt;background:#666666;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='rackid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "rack", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='rackname' size='30'>");
				stringBuilder.append("<tr><td>Brief description<td align='right'><textarea name='rackdesc' cols='30' rows='4'> </textarea>");
				stringBuilder.append("<tr><td>Belongs to cluster: <td align='right'><select name='clusterid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "cluster", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
	    		////////////////////form for generating l2switch fields////////////////
	    		stringBuilder.append("<div id='form-l2switch' style='background:#333333;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
	    		stringBuilder.append("<form name='editl2switch' action='../admin/managedatacenter/editl2switch' method='post' style='font-family:Verdana;font-size:10pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:10pt;background:#333333;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='l2switchid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "l2switch", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Brief description<td align='right'><textarea name='l2switchdesc' cols='30' rows='4'> </textarea>");
				stringBuilder.append("<tr><td>Select switch type<td align='right'><select name='switchtypeid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "switchtype", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("<tr><td>Throughput (in mbps)<td align='right'><input type='text' name='mbps' size='10'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
	    		////////////////////form for generating storage fields////////////////
	    		stringBuilder.append("<div id='form-storage' style='background:#666666;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:10pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud entry for your datacenter.");
	    		stringBuilder.append("<form name='editstorage' action='../admin/managedatacenter/editstorage' method='post' style='font-family:Verdana;font-size:10pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:10pt;background:#666666;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='storageid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "storage", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Name<td align='right'><input type='text' name='storagename' size='30'>");
				stringBuilder.append("<tr><td>Select storage type<td align='right'><select name='storagetypeid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "storagetype", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("<tr><td>Endpoint Storage Manager Interface<td align='right'><input type='text' name='storageendpoint' size='30'>");
                                stringBuilder.append("<tr><td>Base Endpoint to Mount the Image<td align='right'><input type='text' name='storageendpointmount' size='30'>");
				stringBuilder.append("<tr><td>Belongs to rack: <td align='right'><select name='rackid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "rack", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
	    		////////////////////form for generating host fields////////////////
	    		stringBuilder.append("<div id='form-host' style='background:#333333;color:white;padding:5px;width:575px;height:500px;font-family:Verdana;font-size:9pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new host entry for your datacenter.");
	    		stringBuilder.append("<form name='edithost' action='../admin/managedatacenter/edithost' method='post' style='font-family:Verdana;font-size:9pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:9pt;background:#333333;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='hostid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "host", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				//get the cloud accounts and query for host list
				LinkedList<String> hostList = new LinkedList<String>();
				try
				{
					ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
    	        	int vepuserId = -1;
    	        	if(rs.next())
    	        		vepuserId = rs.getInt("id");
    	        	if(vepuserId == -1)
    	        	{
    	        		stringBuilder.append("<br><font color='red'>VEP DB is currupted. Please fix it!</font><br>");
    	        	}
    	        	else
    	        	{
    	        		rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + vepuserId);
    	        		int counter = 0;
    	        		while(rs.next())
    	        		{
    	        			int cloudid = rs.getInt("cloudtype");
    	        			String iaasUser = rs.getString("iaasuser");
    	        			String iaasPass = rs.getString("iaaspass");
    	        			String cloudName = "";
    	        			String headIP = "";
    	        			int headPort = -1;
    	        			int cloudTypeId = -1;
    	        			String cloudVersion = "";
    	        			String cloudTypeName = "";
    	        			counter++;
    	        			rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudid);
    	        			if(rs.next())
    	        			{
    	        				cloudName = rs.getString("name");
    	        				headIP = rs.getString("headip");
    	        				headPort = rs.getInt("headport");
    	        				cloudVersion = rs.getString("version");
    	        				cloudTypeId = rs.getInt("typeid");
    	        			}
    	        			if(cloudTypeId != -1)
    	        			{
    	        				rs = db.query("select", "name", "supportedcloud", "WHERE id=" + cloudTypeId);
    	        				if(rs.next())
    	        					cloudTypeName = rs.getString("name");
    	        			}
    	        			if(cloudTypeId == App.OPENNEBULA3_HOST)
    	        				getHostList(hostList, iaasUser, iaasPass, cloudName, headIP, headPort, cloudTypeName, cloudVersion);
    	        			else if(cloudTypeId == App.OPENSTACK_HOST)
    	        				getOSHostList(hostList, cloudid, iaasUser, iaasPass, cloudName);
    	        			rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + vepuserId);
    	        			int count = 0;
            				while(count < counter) { rs.next(); count++; }    
    	        		}
    	        	}
				}
				catch(Exception ex)
				{
					logger.error("Exception caught",ex);
				}
				stringBuilder.append("<tr><td>");
				stringBuilder.append("Select a host to autofill the form");
				stringBuilder.append("<td align='right'><select id='hostlist' name='hoststring'><option value='-1'>Not selected</option>");
				for(int i=0; i<hostList.size(); i++)
				{
					String[] desc = hostList.get(i).split(" ");
					stringBuilder.append("<option value='").append(hostList.get(i)).append("'>").append("Cloud [").append(desc[0]).append("] - Host [").append(desc[1]).append("]").append("</option>");
				}
				stringBuilder.append("</select><br>");
				
				stringBuilder.append("\n<script type=\"text/javascript\">\n")
					.append("var selectmenu=document.getElementById(\"hostlist\");\n")
					.append("selectmenu.onchange=function(){ //run some code when \"onchange\" event fires\n")
					.append("var chosenoption=this.options[this.selectedIndex]; //this refers to \"selectmenu\"\n")
					.append("if (chosenoption.value!=\"-1\"){\n")
					.append("var elements = chosenoption.value;\n")
					.append("var parts = elements.split(\" \");\n")
					.append("document.getElementById(\"cpufreq\").value = parts[2];\n")
					.append("document.getElementById(\"cpucore\").value = parts[3];\n")
					.append("document.getElementById(\"ram\").value = parts[4];\n")
					.append("document.getElementById(\"hostname\").value = parts[1];\n")
					.append("document.getElementById(\"cpuarch\").value = parts[5];\n")
					.append("document.getElementById(\"virttype\").value = parts[6];\n")
					.append("document.getElementById(\"iaasid\").value = parts[7];\n")
				 	.append("}\n")
					.append("}\n")
					.append("</script>\n");
				stringBuilder.append("<tr><td>CPU Frequency (in MHz)<td align='right'><input id='cpufreq' type='text' name='cpufreq' size='25'>");
				stringBuilder.append("<tr><td>Number of Cores<td align='right'><input id='cpucore' type='text' name='cpucore' size='25'>");
				stringBuilder.append("<tr><td>RAM (in KBytes)<td align='right'><input id='ram' type='text' name='ram' size='25'>");
				stringBuilder.append("<tr><td>Disc Capacity (in MB)<td align='right'><input id='disksize' type='text' name='disksize' size='25'>");
				stringBuilder.append("<tr><td>Hostname<td align='right'><input id='hostname' type='text' name='hostname' size='25'>");
				stringBuilder.append("<tr><td>CPU Architecture<td align='right'><input id='cpuarch' type='text' name='cpuarch' size='25'>");
				stringBuilder.append("<tr><td>Virtualization<td align='right'><input id='virttype' type='text' name='virttype' size='25'>");
				stringBuilder.append("<tr><td>Connected to L2 switch: <td align='right'><select name='l2switchid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, switchtypeid", "l2switch", "");
					int counter=0;
					while(rs.next())
					{
						int typeid = rs.getInt("switchtypeid");
						int id = rs.getInt("id");
						counter++;
						rs = db.query("select", "name", "switchtype", "WHERE id=" + typeid);
						if(rs.next())
						{
							stringBuilder.append("<option value='").append(id).append("'>").append(id).append(" - ").append(rs.getString("name")).append("</option>");
						}
						rs = db.query("select", "id, switchtypeid", "l2switch", "");
    					int count=0;
    					while(count < counter) { rs.next(); count++; }
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td>Belongs to rack: <td align='right'><select name='rackid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "rack", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td>Belongs to cloud: <td align='right'><select name='cloudtypeid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "cloudtype", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td>IaaS Host ID<td align='right'><input id='iaasid' type='text' name='iaasid' size='25'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
	    		////////////////////form for generating network fields////////////////
	    		stringBuilder.append("<div id='form-network' style='background:#666666;color:white;padding:5px;width:575px;height:400px;font-family:Verdana;font-size:9pt;overflow:auto;display:none;'>");
	    		stringBuilder.append("Please check the box in case you are modifying an existing entry. If left unchecked the submit action will create a new cloud network entry for your datacenter.");
	    		stringBuilder.append("<form name='editnetwork' action='../admin/managedatacenter/editnetwork' method='post' style='font-family:Verdana;font-size:9pt;color:white;'>");
		        stringBuilder.append("<table style='width:555px;font-family:Verdana;font-size:9pt;background:#666666;color:white;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				stringBuilder.append("<tr><td>Check this if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
				stringBuilder.append("<tr><td>Check this if deleting an entry<td align='right'><input type='checkbox' name='deletebox' value='yes'>");
				stringBuilder.append("<tr><td>Select the entry (if modifying or deleting)<td align='right'><select name='networkid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id", "cloudnetwork", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
				stringBuilder.append("<tr><td colspan='2'><hr></tr>");
				//get the cloud accounts and query for network list
				LinkedList<String> networkList = new LinkedList<String>();
				try
				{
					ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
		        	int vepuserId = -1;
		        	if(rs.next())
		        		vepuserId = rs.getInt("id");
		        	if(vepuserId == -1)
		        	{
		        		stringBuilder.append("<br><font color='red'>VEP DB is currupted. Please fix it!</font><br>");
		        	}
		        	else
		        	{
		        		rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + vepuserId);
		        		int counter = 0;
		        		while(rs.next())
		        		{
		        			int cloudid = rs.getInt("cloudtype");
		        			String iaasUser = rs.getString("iaasuser");
		        			String iaasPass = rs.getString("iaaspass");
		        			String cloudName = "";
		        			String headIP = "";
		        			int headPort = -1;
		        			int cloudTypeId = -1;
		        			String cloudVersion = "";
		        			String cloudTypeName = "";
		        			counter++;
		        			rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudid);
		        			if(rs.next())
		        			{
		        				cloudName = rs.getString("name");
		        				headIP = rs.getString("headip");
		        				headPort = rs.getInt("headport");
		        				cloudVersion = rs.getString("version");
		        				cloudTypeId = rs.getInt("typeid");
		        			}
		        			if(cloudTypeId != -1)
		        			{
		        				rs = db.query("select", "name", "supportedcloud", "WHERE id=" + cloudTypeId);
		        				if(rs.next())
		        					cloudTypeName = rs.getString("name");
		        			}
		        			getNetworkList(networkList, iaasUser, iaasPass, cloudName, headIP, headPort, cloudTypeName, cloudVersion);
		        			rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + vepuserId);
		        			int count = 0;
	        				while(count < counter) { rs.next(); count++; }    
		        		}
		        	}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("<tr><td>");
				stringBuilder.append("Select to autofill the form");
				stringBuilder.append("<td align='right'><select id='vnetworklist' name='networkstring'><option value='-1'>Not selected</option>");
				for(int i=0; i<networkList.size(); i++)
				{
					String[] desc = networkList.get(i).split(" ");
					stringBuilder.append("<option value='").append(networkList.get(i)).append("'>").append("Cloud [").append(desc[0]).append("] - Network [").append(desc[1]).append("]").append("</option>");
				}
				stringBuilder.append("</select><br>");
				stringBuilder.append("\n<script type=\"text/javascript\">\n")
					.append("var selectmenu=document.getElementById(\"vnetworklist\");\n")
					.append("selectmenu.onchange=function(){ //run some code when \"onchange\" event fires\n")
					.append("var chosenoption=this.options[this.selectedIndex]; //this refers to \"selectmenu\"\n")
					.append("if (chosenoption.value!=\"-1\"){\n")
					.append("var elements = chosenoption.value;\n")
					.append("var parts = elements.split(\" \");\n")
					.append("document.getElementById(\"vnetname\").value = parts[1];\n")
					.append("document.getElementById(\"vnetiaasid\").value = parts[2];\n")
				 	.append("}\n")
					.append("}\n")
					.append("</script>\n");
				stringBuilder.append("<tr><td>Name<td align='right'><input id='vnetname' type='text' name='vnetname' size='25'>");
				stringBuilder.append("<tr><td>Vnet IaaS ID<td align='right'><input id='vnetiaasid' type='text' name='vnetiaasid' size='25'>");
				stringBuilder.append("<tr><td>Network Type<td align='right'><input id='vnettype' type='text' name='vnettype' size='25'>");
				stringBuilder.append("<tr><td>Belongs to Cloud: <td align='right'><select name='cloudtypeid'><option value='-1'>Not selected</option>");
				try
				{
					ResultSet rs = db.query("select", "id, name", "cloudtype", "");
					while(rs.next())
					{
						stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
					}
				}
				catch(Exception ex)
				{
					
				}
				stringBuilder.append("</select>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
		        stringBuilder.append("</table></form>");
	    		stringBuilder.append("</div>");
        		
        		stringBuilder.append("</table>");
        		////////////////////////////////////////////////////
        		stringBuilder.append("<td valign='top'>");
		        stringBuilder.append("<form name='goback' action='../admin/dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
		        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
				stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='cancel and go back'>");
		        stringBuilder.append("</table></form>");
        	}
        	else
        	{
        		if(action.equalsIgnoreCase("editdatacenter"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int datacenterId = -1;
        			String datacenterName = "";
        			int datacenterLocation = -1;
        			String datacenterDesc = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("datacenterid")) datacenterId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("datacentername")) datacenterName = value;
        				if(key.equalsIgnoreCase("countryid")) datacenterLocation = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("datacenterdesc")) datacenterDesc = value;
        			}
        			String message = "";
        			if((isDelete || isModify) && datacenterId == -1)
        			{
        				status = false;
        				message = "You must choose the datacenter entry to modify or delete. ";
        			}
        			if((datacenterName == null || datacenterDesc == null || datacenterLocation == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				datacenterName = datacenterName.trim();
        				datacenterDesc = datacenterDesc.trim();
            			if(datacenterName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("datacenter", "name='" + datacenterName + "', countryid=" + datacenterLocation + ", descp='" + datacenterDesc + "'", "where id=" + datacenterId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing datacenter.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if(isDelete) {
        					try
        					{
        						status = db.delete("datacenter", "where id=" + datacenterId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing datacenter.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "datacenter", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("datacenter", "(" + id + ", '" + datacenterName + "', " + datacenterLocation + ", '" + datacenterDesc + "')");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new datacenter entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editcluster"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int clusterId = -1;
        			String clusterName = "";
        			int interconnectId = -1;
        			String clusterDesc = "";
        			int datacenterId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("clusterid")) clusterId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("clustername")) clusterName = value;
        				if(key.equalsIgnoreCase("interconnectid")) interconnectId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("clusterdesc")) clusterDesc = value;
        				if(key.equalsIgnoreCase("datacenterid")) datacenterId = Integer.parseInt(value.trim());
        			}
        			String message = "";
        			if((isDelete || isModify) && clusterId == -1)
        			{
        				status = false;
        				message = "You must choose the cluster entry to modify or delete. ";
        			}
        			if((clusterName == null || clusterDesc == null || interconnectId == -1 || datacenterId == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				clusterName = clusterName.trim();
        				clusterDesc = clusterDesc.trim();
            			if(clusterName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("cluster", "name='" + clusterName + "', interconnectid=" + interconnectId + ", descp='" + clusterDesc + "', datacenterid=" + datacenterId, "where id=" + clusterId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cluster.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if (isDelete) {
        					try
        					{
        						status = db.delete("cluster", "where id=" + clusterId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing cluster.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "cluster", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("cluster", "(" + id + ", '" + clusterName + "', " + interconnectId + ", '" + clusterDesc + "', " + datacenterId +  ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new cluster entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editrack"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int rackId = -1;
        			String rackName = "";
        			String rackDesc = "";
        			int clusterId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("rackid")) rackId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("rackname")) rackName = value;
        				if(key.equalsIgnoreCase("rackdesc")) rackDesc = value;
        				if(key.equalsIgnoreCase("clusterid")) clusterId = Integer.parseInt(value.trim());
        			}
        			String message = "";
        			if((isDelete || isModify) && rackId == -1)
        			{
        				status = false;
        				message = "You must choose the rack entry to modify or delete. ";
        			}
        			if((rackName == null || rackDesc == null || clusterId == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				rackName = rackName.trim();
        				rackDesc = rackDesc.trim();
            			if(rackName.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("rack", "name='" + rackName + "', descp='" + rackDesc + "', clusterid=" + clusterId, "where id=" + rackId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing rack.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else if(isDelete)
        				{
        					try
        					{
        						status = db.delete("rack", "where id=" + rackId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing rack.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} 
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "rack", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("rack", "(" + id + ", '" + rackName + "', '" + rackDesc + "', " + clusterId +  ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new rack entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editl2switch"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int l2switchId = -1;
        			String l2switchDesc = "";
        			int switchtypeId = -1;
        			int mbps = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("l2switchid")) l2switchId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("mbps"))
        				{
        					try
        					{
        						mbps = Integer.parseInt(value.trim());
        					}
        					catch(Exception ex)
        					{
        						mbps = -1;
        						logger.warn("Exception caught while parsing the switch mbps speed value.");
        					}
        				}
        				if(key.equalsIgnoreCase("l2switchdesc")) l2switchDesc = value;
        				if(key.equalsIgnoreCase("switchtypeid")) switchtypeId = Integer.parseInt(value.trim());
        			}
        			String message = "";
        			if((isDelete || isModify) && l2switchId == -1)
        			{
        				status = false;
        				message = "You must choose the l2 switch entry to modify or delete. ";
        			}
        			if((l2switchDesc == null || switchtypeId == -1 || mbps == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				l2switchDesc = l2switchDesc.trim();
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("l2switch", "descp='" + l2switchDesc + "', switchtypeid=" + switchtypeId + ", mbps=" + mbps, "where id=" + l2switchId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing l2 switch.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if(isDelete)
        				{
        					try
        					{
        						status = db.delete("l2switch", "where id=" + l2switchId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing l2 switch.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "l2switch", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("l2switch", "(" + id + ", '" + l2switchDesc + "', " + switchtypeId + ", " + mbps +  ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new l2 switch entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editstorage"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int storageId = -1;
        			String storageName = "";
        			int storagetypeId = -1;
        			String storageEndpoint = "";
                                String storageMountEndpoint="";
        			int rackId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("storageid")) storageId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("storagename")) storageName = value;
        				if(key.equalsIgnoreCase("storagetypeid")) storagetypeId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("storageendpoint")) storageEndpoint = value;
                                        if(key.equalsIgnoreCase("storageendpointmount")) storageMountEndpoint= value;
        				if(key.equalsIgnoreCase("rackid")) rackId = Integer.parseInt(value.trim());
        			}
        			String message = "";
        			if((isDelete || isModify) && storageId == -1)
        			{
        				status = false;
        				message = "You must choose the storage entry to modify or delete. ";
        			}
        			if((storageName == null || storagetypeId == -1 || rackId == -1 || storageEndpoint == null) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				storageName = storageName.trim();
        				storageEndpoint = storageEndpoint.trim();
        				if(storageName.length() == 0 || storageEndpoint.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("storage", "name='" + storageName + "', storagetypeid=" + storagetypeId + ", endpoint='" + storageEndpoint + "', rackid=" + rackId+" , mountendpoint='"+storageMountEndpoint+"'", "where id=" + storageId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing storage entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if(isDelete)
        				{
        					try
        					{
        						status = db.delete("storage", "where id=" + storageId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing storage entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "storage", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("storage", "(" + id + ", '" + storageName + "', " + storagetypeId + ", '" + storageEndpoint + "', " + rackId +  " , '"+storageMountEndpoint+"')");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new storage entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("edithost"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int hostId = -1;
        			int cpuFreq = -1;
        			int coreCount = -1;
        			int ram = -1;
        			int diskSize = -1;
        			String hostName = "";
        			String cpuArch = "";
        			String virtualization = "";
        			int l2switchId = -1;
        			int rackId = -1;
        			int cloudId = -1;
        			String iaasId = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("hostid")) hostId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("l2switchid")) l2switchId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("rackid")) rackId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("cloudtypeid")) cloudId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("hostname")) hostName = value;
        				if(key.equalsIgnoreCase("cpuarch")) cpuArch = value;
        				if(key.equalsIgnoreCase("virttype")) virtualization = value;
        				if(key.equalsIgnoreCase("iaasid")) iaasId = value;
        				try
        				{
        					if(key.equalsIgnoreCase("cpufreq")) cpuFreq = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("cpucore")) coreCount = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("ram")) ram = Integer.parseInt(value.trim());
        					if(key.equalsIgnoreCase("disksize")) diskSize = Integer.parseInt(value.trim());
        				}
        				catch(Exception ex)
        				{
        					cpuFreq = coreCount = ram = diskSize = -1;
        				}
        			}
        			String message = "";
        			if((isDelete || isModify) && hostId == -1)
        			{
        				status = false;
        				message = "You must choose the host entry to modify or delete. ";
        			}
        			if((hostName == null || cpuArch == null || virtualization == null || iaasId == null || l2switchId == -1 || rackId == -1 || cloudId == -1 || coreCount == -1 || ram == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				hostName = hostName.trim();
        				cpuArch = cpuArch.trim();
        				virtualization = virtualization.trim();
        				iaasId = iaasId.trim();
        				if(hostName.length() == 0 || cpuArch.length() == 0 || virtualization.length() == 0 || iaasId.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty or with spaces. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("host", "cpufreq=" + cpuFreq + ", corecount=" + coreCount + ", ram=" + ram + ", disksize=" + diskSize + ", hostname='" + hostName + "', cpuarch='"
        							+ cpuArch + "', virtualization='" + virtualization + "', l2switchid=" + l2switchId + ", rackid=" + rackId + ", cloudtypeid=" + cloudId + ", iaasid='" + iaasId + "'",
        							"where id=" + hostId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing host entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if(isDelete)
        				{
        					try
        					{
        						status = db.delete("host", "where id=" + hostId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while deleting an existing host entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					boolean hostExist = false;
        					try
        					{
        						rs = db.query("select", "count(*)", "host", "WHERE hostname='" + hostName + "' and cloudtypeid="+cloudId);
        						if(rs.next())
        						{
        							if(rs.getInt(1) > 0)
        								hostExist = true;
        						}
        						rs.close();
        					}
        					catch(Exception ex)
        					{
        						logger.warn("SQL exception caught while finding duplicate host entry");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        					}
        					try
        					{
        						if(!hostExist)
        						{
        							rs = db.query("select", "max(id)", "host", "");
        							int id = 1;
        							if(rs.next()) id = rs.getInt(1) + 1;
        							rs.close();
        							status = db.insert("host", "(" + id + ", " + cpuFreq + ", " + coreCount + ", " + ram + ", " + diskSize + ", '" + hostName + "', '" + cpuArch + "', '" + virtualization
                                		+ "', " + l2switchId + ", " + rackId + ", " + cloudId + ", '" + iaasId +  "')");
        						}
        						else
        						{
        							status = false;
        							message += "Duplicate host entry detected. ";
        						}
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new host entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        		else if(action.equalsIgnoreCase("editnetwork"))
        		{
        			boolean status = true;
        			Map<String, String> values = form.getValuesMap();
        			Set<String> keys = values.keySet();
        			Collection<String> keyval = values.values();
        			Iterator<String> keyIter = keys.iterator();
        			Iterator<String> valIter = keyval.iterator();
        			boolean isModify = false;
        			boolean isDelete = false;
        			int networkId = -1;
        			String name = "";
        			int cloudId = -1;
        			String networkType = "";
        			String iaasId = "";
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("deletebox")) isDelete = true;
        				if(key.equalsIgnoreCase("networkid")) networkId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("vnetname")) name = value;
        				if(key.equalsIgnoreCase("vnettype")) networkType = value;
        				if(key.equalsIgnoreCase("vnetiaasid")) iaasId = value;
        				if(key.equalsIgnoreCase("cloudtypeid")) cloudId = Integer.parseInt(value.trim());
        			}
        			String message = "";
        			if((isDelete || isModify) && networkId == -1)
        			{
        				status = false;
        				message = "You must choose the host entry to modify or delete. ";
        			}
        			if((name == null || networkType == null || iaasId == null || cloudId == -1) && !isDelete)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status && !isDelete)
        			{
        				name = name.trim();
        				networkType = networkType.trim();
        				iaasId = iaasId.trim();
        				if(name.length() == 0 || networkType.length() == 0 || iaasId.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("cloudnetwork", "name='" + name + "', cloudtypeid=" + cloudId + ", type='" + networkType + "', iaasid='" + iaasId + "'", "where id=" + networkId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cloud network entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				} else if(isModify)
        				{
        					try
        					{
        						status = db.delete("cloudnetwork", "where id=" + networkId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cloud network entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "cloudnetwork", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("cloudnetwork", "(" + id + ", '" + name + "', " + cloudId + ", '" + networkType + "', '" + iaasId + "')");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new cloud network entry.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../managedatacenter' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
        	}
        	stringBuilder.append("</table>");
        }
        else
        {
        	
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
	
	//retrieve host list from a openstack host
	private void getOSHostList(LinkedList<String> hostList, int cloudid,
			String iaasUser, String iaasPass, String cloudName) throws SQLException {
		ResultSet rs = db.query("select", "*", "openstackcloud", "where id="+cloudid);
		if(rs.next())
		{
			String keystoneAdminIp = rs.getString("keystoneadminip");
			String keystoneAdminPort = rs.getString("keystoneadminport");
			String tenantName = rs.getString("tenantname");
			String tenantId = rs.getString("tenantid");
			String region = rs.getString("region");
			String keyStoneAdminAPI = keystoneAdminIp+":"+keystoneAdminPort+"/v2.0/"; 
			
			OpenStackConnector osc = new OpenStackConnector(iaasUser, tenantName, iaasPass,
					keyStoneAdminAPI, iaasUser, tenantName, iaasPass,
					keyStoneAdminAPI, "Host listing"); //use same endpoint, anyway we only use the second one there
			
			LinkedList<OpenStackConnector.Hypervisor> list = osc.getHypervisorList(region);
			if(list != null)
			{
				for(OpenStackConnector.Hypervisor h : list)
				{
					h = osc.getHypervisorInfo(region, h.GetId());
					hostList.add(cloudName + " " + h.GetHostname() + " " + "Unknown" + " " + h.GetVcpus() + " " + h.GetMemoryMb() + " " + "Unknown" + " " + h.GetHypervisorType() + " " + h.GetId());
				}
			}
		}
	}

	//too much OpenNebula specific
	public void getHostList(LinkedList<String> hostList, String iaasUser, String iaasPass, String cloudName, String headIP, int headPort, String cloudTypeName, String cloudVersion)
	{
		if(cloudTypeName.contains("OpenNebula"))
		{
			if(cloudVersion.startsWith("2.2"))
			{
				One2XMLRPCHandler one2handle = new One2XMLRPCHandler(headIP, Integer.toString(headPort), iaasUser, iaasPass, "manage datacenter");
				LinkedList<OpenNebulaHost> one2hostlist = one2handle.getHostList();
				if(one2hostlist != null)
				{
					for(int i=0; i< one2hostlist.size(); i++)
					{
						OpenNebulaHost host = one2hostlist.get(i);
						hostList.add(cloudName + " " + host.hostName + " " + host.cpuspeed + " " + host.number_cores + " " + host.max_mem + " " + host.arch + " " + host.hypervisor + " " + host.id);
					}
				}
			}
			if(cloudVersion.startsWith("3.4") || cloudVersion.startsWith("3.6"))
			{
				One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIP, Integer.toString(headPort), iaasUser, iaasPass, "manage datacenter");
				LinkedList<OpenNebulaHost> one3hostlist = one3handle.getHostList();
				if(one3hostlist != null)
				{
					for(int i=0; i< one3hostlist.size(); i++)
					{
						OpenNebulaHost host = one3hostlist.get(i);
						hostList.add(cloudName + " " + host.hostName + " " + host.cpuspeed + " " + host.number_cores + " " + host.max_mem + " " + host.arch + " " + host.hypervisor + " " + host.id);
					}
				}
			}
		} 
	}
	
	public void getNetworkList(LinkedList<String> networkList, String iaasUser, String iaasPass, String cloudName, String headIP, int headPort, String cloudTypeName, String cloudVersion)
	{
		if(cloudTypeName.contains("OpenNebula"))
		{
			if(cloudVersion.startsWith("2.2"))
			{
				One2XMLRPCHandler one2handle = new One2XMLRPCHandler(headIP, Integer.toString(headPort), iaasUser, iaasPass, "manage datacenter");
				LinkedList<OpenNebulaNetwork> one2networklist = one2handle.getVirtualNetworkList();
				if(one2networklist != null)
				{
					for(int i=0; i< one2networklist.size(); i++)
					{
						OpenNebulaNetwork network = one2networklist.get(i);
						networkList.add(cloudName + " " + network.name + " " + network.id);
					}
				}
			}
			if(cloudVersion.startsWith("3.4") || cloudVersion.startsWith("3.6"))
			{
				One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIP, Integer.toString(headPort), iaasUser, iaasPass, "manage datacenter");
				LinkedList<OpenNebulaNetwork> one3networklist = one3handle.getVirtualNetworkList();
				if(one3networklist != null)
				{
					for(int i=0; i< one3networklist.size(); i++)
					{
						OpenNebulaNetwork network = one3networklist.get(i);
						networkList.add(cloudName + " " + network.name + " " + network.id);
					}
				}
			}
		}
	}
}
