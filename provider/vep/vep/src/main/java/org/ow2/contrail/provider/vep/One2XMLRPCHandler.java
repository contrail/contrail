/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.ow2.contrail.provider.vep.fixImage2_2.ONEImage;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.image.ImagePool;
import org.opennebula.client.user.User;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class One2XMLRPCHandler extends DefaultHandler 
{
	private String oneIP;
    private String onePort;
    private String oneUser;
    private String onePass;
    private Client oneClient;
    private Logger logger;
    private XMLParser parser;
    
	public One2XMLRPCHandler(String IP, String port, String user, String pass, String entryPoint) 
    {
		oneIP = IP;
        onePort = port;
        oneUser = user;
        onePass = pass;
        logger = Logger.getLogger("VEP.One2XMLRPC");
        try
        {
            logger.info("Creating one2Client: new Client(" + oneUser + ":" + onePass + ", \"http://" + oneIP + ":" + onePort + "/RPC2\") - Entrypoint: " + entryPoint);
            oneClient  = new Client(oneUser + ":" + onePass, "http://" + oneIP + ":" + onePort + "/RPC2");
        }
        catch(Exception ex)
        {
            logger.error("OpenNebula2  XML RPC connection could not be established. Check system settings and restart the application.");
            oneClient = null;
            //if(logger.isDebugEnabled())
            //    ex.printStackTrace(System.err);
            logger.debug("Exception Caught: ", ex);
        }
    }
	
	@SuppressWarnings("unchecked")
	public LinkedList<OpenNebulaHost> getHostList()
	{
		OneResponse val = org.opennebula.client.host.HostPool.info(oneClient);
		String input = "";
		LinkedList<OpenNebulaHost> hostPool = new LinkedList<OpenNebulaHost>();
		if(!val.isError())
        {
            input = val.getMessage();
        }
        else
        {
            logger.warn("OpenNebula 2 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
        }
		if(input.length() > 0)
		{
			parser = new XMLParser(input, 1, hostPool, "2.2");
			hostPool = (LinkedList<OpenNebulaHost>)parser.parse();
		}
		return hostPool;
	}
	
	public int addUser(String name, String password)
    {
        int value = -1;
        if(oneClient != null)
        {
            OneResponse val = User.allocate(oneClient, name, password);
            logger.info("Adding OpenNebula 2.2 user " + name + ": Got response: " + val.getMessage());
            if(val.getMessage() != null)
                value = Integer.parseInt(val.getMessage());
        }
        return value;
    }
	
	@SuppressWarnings("unchecked")
	public LinkedList<OpenNebulaNetwork> getVirtualNetworkList()
	{
		OneResponse val = org.opennebula.client.vnet.VirtualNetworkPool.info(oneClient, -2);
		String input = "";
		LinkedList<OpenNebulaNetwork> virtualNetworkPool = new LinkedList<OpenNebulaNetwork>();
		if(!val.isError())
        {
            input = val.getMessage();
        }
        else
        {
            logger.warn("OpenNebula 2 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
        }
		if(input.length() > 0)
		{
			parser = new XMLParser(input, 2, virtualNetworkPool, "2.2");
			virtualNetworkPool = (LinkedList<OpenNebulaNetwork>)parser.parse();
		}
		return virtualNetworkPool;
	}
        
        
    public LinkedList<ONEImage> getImageList()
    {
        LinkedList<ONEImage> imageList = null;
        if(oneClient != null)
        {
            imageList = new LinkedList<ONEImage>();
            String response = "";
            OneResponse val = ImagePool.info(oneClient, -2); //-2 returns all VMs
            if(!val.isError())
            {
                logger.debug("OpenNebula Image Pool info retrieved.");
                response = val.getMessage();
                //System.out.println("Imagepool Response: " + response);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                logger.error("OpenNebula Image Pool info retrieval failed: " + val.getErrorMessage());
            }
        }
        return imageList;
    }
}
