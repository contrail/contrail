/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import org.ow2.contrail.provider.vep.objects.Application;

public class RestCIMIApplicationAction extends ServerResource{

	private Logger logger;
	private DBHandler db;
	
	public RestCIMIApplicationAction() {
		logger = Logger.getLogger("VEP.ApplicationAction");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestApplicationAction", dbType);
	}
	
	@Post
	public Representation doAction()
	{
		Representation response = null;
		JSONObject json = new JSONObject();
//		String userName = "ghostwheel";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String appid = ((String) getRequest().getAttributes().get("applicationid"));
		String action = ((String) getRequest().getAttributes().get("action"));
		try
		{
			if(ceeid != null && appid != null && action != null)
			{
				Application a = CIMIParser.appCreate(userName, null, ceeid, appid);
				if (a!=null)
				{
					a.doAction(action);
					if(!a.isError())
					{
						setStatus(Status.SUCCESS_OK);
					} else {
						json.put("error", a.getError());
						setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
					}
				} else {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				}
			} else {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		} catch (Exception e)
		{
			logger.debug("SQLException: ",e);
			setStatus(Status.SERVER_ERROR_INTERNAL);
		}
		
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}
	
	
}
