/*Copyright (c) 2013, Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR).
All rights reserved.

This file is part of the Contrail system see <http://contrail-project.eu/>
for more details. The Contrail project has been developed with the
financial support of the European Commission's ICT FP7 program
under Grant Agreement # 257438.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  -         Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

  -        Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

  -        Neither the name of Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR),
        nor the names of its contributors nor the name of the Contrail project may be used
        to endorse or promote products derived from this software without specific prior written
        permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR),
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,  PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;  OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,  WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

package org.ow2.contrail.provider.vep.openstack;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.commons.codec.binary.Base64;
import org.eclipse.jetty.util.ajax.JSON;

import org.jclouds.ContextBuilder;
import org.jclouds.collect.PagedIterable;
import org.jclouds.io.Payload;
import org.jclouds.io.payloads.FilePayload;
import org.jclouds.openstack.glance.v1_0.GlanceApi;
import org.jclouds.openstack.glance.v1_0.GlanceApiMetadata;
import org.jclouds.openstack.glance.v1_0.domain.ContainerFormat;
import org.jclouds.openstack.glance.v1_0.domain.DiskFormat;
import org.jclouds.openstack.glance.v1_0.domain.Image;
import org.jclouds.openstack.glance.v1_0.domain.ImageDetails;
import org.jclouds.openstack.glance.v1_0.features.ImageApi;
import org.jclouds.openstack.glance.v1_0.options.CreateImageOptions;
import org.jclouds.openstack.keystone.v2_0.KeystoneApi;
import org.jclouds.openstack.keystone.v2_0.KeystoneApiMetadata;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.NovaApiMetadata;
import org.jclouds.openstack.nova.v2_0.domain.Flavor;
import org.jclouds.openstack.nova.v2_0.domain.RebootType;
import org.jclouds.openstack.nova.v2_0.domain.Server;
import org.jclouds.openstack.nova.v2_0.features.FlavorApi;

import com.woorea.openstack.base.client.OpenStackSimpleTokenProvider;
import com.woorea.openstack.keystone.Keystone;
import com.woorea.openstack.keystone.api.UsersResource.List;
import com.woorea.openstack.keystone.model.authentication.UsernamePassword;
import com.woorea.openstack.keystone.model.Role;
import com.woorea.openstack.keystone.model.Roles;
import com.woorea.openstack.keystone.model.User;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Basic interface to connect to OpenStack 
 */
public class OpenStackConnector {

    private String userU;
    private String tenantU;
    private String passU;
    private String endpointU;
    private String identityU;
    private String userA;
    private String tenantA;
    private String passA;
    private String endpointA;
    private String novaEndpointU;
    private String identityA;
    private static String NOVA="nova";
    private static String GLANCE="glance";
    /**
     * Used to store information about hypervisors
     */
    public class Hypervisor
    {
        private String id;
        private String hostname;
              
        private Long vcpus_used;
        private Long local_gb_used;
        private Long memory_mb_used;
        private Long memory_mb;
        private Long current_workload;
        private Long vcpus;
        private Long running_vms;
        private Long free_disk_gb;
        private Long hypervisor_version;
        private Long disk_available_least;
        private Long local_gb;
        private Long free_ram_mb;

        private String hypervisor_type;
        private String cpu_info;

        /**
         * Construct basic hypervisor description. Only id and hostname. Other fields are null
         * @param id Hypervisor ID
         * @param hostname Hostname
         */
        public Hypervisor(String id, String hostname)
        {
            this.id = id;
            this.hostname = hostname;
            vcpus_used = null;
            local_gb_used = null;
            memory_mb_used = null;
            memory_mb = null;
            current_workload = null;
            vcpus = null;
            running_vms = null;
            free_disk_gb = null;
            hypervisor_version = null;
            disk_available_least = null;
            local_gb = null;
            free_ram_mb = null;

            hypervisor_type = null;
            cpu_info = null;
        }
        
        /**
         * Constructs full described hypervisor
         * @param id ID
         * @param hostname Hostname
         * @param vcpus_used Number of used vcpus
         * @param local_gb_used Amount of used local storage in GB
         * @param memory_mb_used Amount of RAM used in MB
         * @param memory_mb Total amount of RAM in MB
         * @param current_workload NOT DESCRIBED YET
         * @param vcpus Number of cores
         * @param running_vms Number of running instances
         * @param free_disk_gb Amount of free disk space
         * @param hypervisor_version Version of hypervisor
         * @param disk_available_least NOT DESCRIBED YET
         * @param local_gb Total size of local storage in GB
         * @param free_ram_mb Amount of free ram
         * @param hypervisor_type Type of hypervisor
         * @param cpu_info Some information about CPU (architecture, supported command sets etc)
         */
        public Hypervisor(String id,
                String hostname,
                Long vcpus_used,
                Long local_gb_used,
                Long memory_mb_used,
                Long memory_mb,
                Long current_workload,
                Long vcpus,
                Long running_vms,
                Long free_disk_gb,
                Long hypervisor_version,
                Long disk_available_least,
                Long local_gb,
                Long free_ram_mb,
                String hypervisor_type,
                String cpu_info)
        {
            this.id = id;
            this.hostname = hostname;
            this.vcpus_used = vcpus_used;
            this.local_gb_used = local_gb_used;
            this.memory_mb_used = memory_mb_used;
            this.memory_mb = memory_mb;
            this.current_workload = current_workload;
            this.vcpus = vcpus;
            this.running_vms = running_vms;
            this.free_disk_gb = free_disk_gb;
            this.hypervisor_version = hypervisor_version;
            this.disk_available_least = disk_available_least;
            this.local_gb = local_gb;
            this.free_ram_mb = free_ram_mb;

            this.hypervisor_type = hypervisor_type;
            this.cpu_info = cpu_info;
        }
        
        public String GetId()
        {
            return id;
        }
        
        public String GetHostname()
        {
            return hostname;
        }
        
        public Long GetVcpusUsed()
        {
            return vcpus_used;
        }
        
        public Long GetLocalGbUsed()
        {
            return local_gb_used;
        }       
        
        public Long GetMemoryMbUsed()
        {
            return memory_mb_used;
        }
        
        public Long GetMemoryMb()
        {
            return memory_mb;
        }
        
        public Long GetCurrentWorkload()
        {
            return current_workload;
        }
        
        public Long GetVcpus()
        {
            return vcpus;
        }
        
        public Long GetRunningVMs()
        {
            return running_vms;
        }
        
        public Long GetFreeDiskGb()
        {
            return free_disk_gb;
        }
        
        public Long GetHypervisorVersion()
        {
            return hypervisor_version;
        }
        
        public Long GetDiskAvailableLeast()
        {
            return disk_available_least;
        }
        
        public Long GetLocalGb()
        {
            return local_gb;
        }
        
        public Long GetFreeRamMb()
        {
            return free_ram_mb;
        }

        public String GetHypervisorType()
        {
            return hypervisor_type;
        }
        
        public String GetCPUInfo()
        {
            return cpu_info;
        }
    }
    
    /**
     * Class used just to return token and endpoint from function
     */
    public class TokenEndpoint
    {
         private String token;
         private String endpoint;
         
         /**
          * Constructor
          * @param token Token (UUID)
          * @param endpoint Endpoint (URI)
          */
         public TokenEndpoint(String token, String endpoint)
         {
             this.token = token;
             this.endpoint = endpoint;
         }
         
         /**
          * @return Returns the token
          */
         public String GetToken()
         {
             return token;
         }
         
         
         /**
          * @return Returns the endpoint
          */
         public String GetEndpoint()
         {
             return endpoint;
         }
         
         
    }
    
    /**
     * Constructor of the class. Parameters for simple user used to produce
     * common actions. Administrator credentials and endpoint are needed to
     * manipulate users and possibly some other kind of administrative actions
     * @param userU Username (simple user)
     * @param tenantU Tenant name (simple user) 
     * @param passU Password (simple user)
     * @param endpointU Endpoint (keystone user)
     * @param userA Username (admin)
     * @param tenantA Tenant name (admin) 
     * @param passA Password (admin)
     * @param endpointA Endpoint (keystone  admin)
     * @param entryPoint Kept for compatibility, initial purpose: logging
     */
    public OpenStackConnector(
            String userU, 
            String tenantU, 
            String passU, 
            String endpointU, 
            String userA, 
            String tenantA, 
            String passA, 
            String endpointA,
            String entryPoint)
    {
	this.userU = userU;
        this.tenantU = tenantU;
        this.passU = passU;
        this.endpointU = endpointU;
        identityU = tenantU + ":" + userU;
        this.userA = userA;
        this.tenantA = tenantA;
        this.passA = passA;
        this.endpointA = endpointA;
        identityA = tenantA + ":" + userA;
    }
    
    /**
     * Service method returning well-constructed KeystoneApi object
     * @return NovaApi object
     */
    private KeystoneApi GetKeystoneApi()
    {
        return ContextBuilder.
                newBuilder(new KeystoneApiMetadata()).
                endpoint(endpointU).
                credentials(identityU,passU).
                buildApi(KeystoneApi.class);
    }
    
    
    /**
     * Service method returning well-constructed NovaApi object
     * @return NovaApi object
     */
    private NovaApi GetNovaApi()
    {
        return ContextBuilder.
                newBuilder(new NovaApiMetadata()).
                endpoint(endpointU).
                credentials(identityU,passU).
                buildApi(NovaApi.class);
    }
    
        
    /**
     * Service method returning well-constructed GlanceApi object
     * @return GlanceApi object
     */
    private GlanceApi GetGlanceApi()
    {
        return 
                ContextBuilder.
                newBuilder(new GlanceApiMetadata()).
                endpoint(endpointU).
                credentials(identityU, passU).
                buildApi(GlanceApi.class);
    }
    
    /**
     * Use it to retrieve list of zones
     * @return List of configured zones
     */
    public LinkedList<String> getZonesList()
    {
       return new LinkedList(GetNovaApi().getConfiguredZones());        
    }
    
    
    /**
     * Gets hypervisor with full information about it
     * @param zone Zone name
     * @param hypervisorId Hypervisor ID
     * @return 
     */
    public Hypervisor getHypervisorInfo(String zone, String hypervisorId)
    {
        try
        {
            OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zone,OpenStackConnector.NOVA);
            
            String computeEndPoint = tokenEndpoint.GetEndpoint();
            String tokenID = tokenEndpoint.GetToken();
            
            computeEndPoint += "/os-hypervisors/" + hypervisorId;
            HttpGet hur = new HttpGet(computeEndPoint);
            hur.addHeader("x-auth-project", tenantU);
            hur.addHeader("x-auth-token", tokenID);
            hur.addHeader("accept", "application/json");
            
            HttpClient httpClient = new DefaultHttpClient();
            JSONParser parser = new JSONParser();
                     
            
            HttpResponse resp = httpClient.execute(hur);
            HttpEntity respEntity = resp.getEntity();
            String respString = EntityUtils.toString(respEntity);
            
            JSONObject result = (JSONObject)parser.parse(respString);
            JSONObject hyp = (JSONObject)result.get("hypervisor");
            
            String id = ((Long)hyp.get("id")).toString();
            //String hostname = (String)hyp.get("hypervisor_hostname");
            String hostname = (String)((JSONObject)hyp.get("service")).get("host");
            
            Long vcpus_used = (Long)hyp.get("vcpus_used");
            Long local_gb_used = (Long)hyp.get("local_gb_used");
            Long memory_mb_used = (Long)hyp.get("memory_mb_used");
            Long memory_mb = (Long)hyp.get("memory_mb");
            Long current_workload = (Long)hyp.get("current_workload");
            Long vcpus = (Long)hyp.get("vcpus");
            Long running_vms = (Long)hyp.get("running_vms");
            Long free_disk_gb = (Long)hyp.get("free_disk_gb");
            Long hypervisor_version = (Long)hyp.get("hypervisor_version");
            Long disk_available_least = (Long)hyp.get("disk_available_least");
            Long local_gb = (Long)hyp.get("local_gb");
            Long free_ram_mb = (Long)hyp.get("free_ram_mb");
            
            String hypervisor_type = (String)hyp.get("hypervisor_type");
            String cpu_info = (String)hyp.get("cpu_info");
       
            return new Hypervisor(id, 
                    hostname, 
                    vcpus_used, 
                    local_gb_used,
                    memory_mb_used,
                    memory_mb,
                    current_workload,
                    vcpus,
                    running_vms,
                    free_disk_gb,
                    hypervisor_version,
                    disk_available_least,
                    local_gb,
                    free_ram_mb,
                    hypervisor_type,
                    cpu_info);
                        
        }
        catch(Exception e)
        {
              return null;          
        }
    }
    
    /**
     * Returns list of hypervisors. Only id and hostname parameters will be filled
     * To get more precise information use getHypervisorInfo
     * @param zone Name of the zone to get hypervizors for
     * @return 
     */	
    public LinkedList<Hypervisor> getHypervisorList(String zone)
    {
        try
        {
            OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zone,OpenStackConnector.NOVA);
            
            String computeEndPoint = tokenEndpoint.GetEndpoint();
            String tokenID = tokenEndpoint.GetToken();
            
            computeEndPoint += "/os-hypervisors";
            HttpGet hur = new HttpGet(computeEndPoint);
            hur.addHeader("x-auth-project", tenantU);
            hur.addHeader("x-auth-token", tokenID);
            hur.addHeader("accept", "application/json");
            
            HttpClient httpClient = new DefaultHttpClient();
            JSONParser parser = new JSONParser();
                     
            
            HttpResponse resp = httpClient.execute(hur);
            HttpEntity respEntity = resp.getEntity();
            String respString = EntityUtils.toString(respEntity);
            
            JSONObject result = (JSONObject)parser.parse(respString);
            JSONArray hypervisors = (JSONArray)result.get("hypervisors");
            
            LinkedList<Hypervisor> hypervisorList = new LinkedList<Hypervisor>();
            
            for(Object hypervisor : hypervisors)
            {
                JSONObject hyp = (JSONObject)hypervisor;
                String id = ((Long)hyp.get("id")).toString();
                String hostname = (String)hyp.get("hypervisor_hostname");
                hypervisorList.add(new Hypervisor(id,hostname));
            }
            return hypervisorList;
            
                        
        }
        catch(Exception e)
        {
              return null;          
        }
        
    }
    
    /**
     * TODO: Add the role for the user in the tenant
     * @param username New user name
     * @param password New user password
     * @param name New user real name, e.g. Vladimir Vladimirovitch Poutine
     * @param email New user e-mail
     * @return UUID-string of new user
     */
    public String addUser(String username, String password, String name, String email, String tenantID, String roleID) 
    {
    	
        User user = new User();
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(password);
        user.setName(username);
        user.setEnabled(Boolean.TRUE);
        
        
        
        Keystone keystone = new Keystone(endpointA);
        com.woorea.openstack.keystone.model.Access access = keystone.tokens()
                .authenticate(new UsernamePassword(userA,passA))
                .withTenantName(tenantA)
                .execute();
        
        
        //keystone = new Keystone(endpointA); //won't work without admin endpoint
        keystone.setTokenProvider(new OpenStackSimpleTokenProvider(access.getToken().getId()));
	user = keystone.users().create(user).execute();
	keystone.users().update(user.getId(), user);
	keystone.tenants().addUser(tenantID, user.getId(), roleID).execute();
	return user.getId();
    	
    	
    }
    
    /**
     * Returns token for authorization with user credentials,
     * ending slashes are trimmed
     * @param zone Zone 
     * @return Token and Endpoint for Nova service
     * @throws IOException 
     */
    private OpenStackConnector.TokenEndpoint GetTokenEndpoint(String zone,String serviceToCheck) throws IOException
    {
        
      
        JSONObject passwordCredentials = new JSONObject();
        passwordCredentials.put("username", userU);
        passwordCredentials.put("password", passU);
        
        JSONObject auth  = new JSONObject();
        auth.put("tenantName", tenantU);
        auth.put("passwordCredentials", passwordCredentials);
        
        JSONObject json_result = new JSONObject();
        json_result.put("auth", auth);
        
        
        HttpPost hur = new HttpPost(endpointU+"tokens");
        hur.addHeader("content-type", "application/json");
        hur.addHeader("accept","application/json");
        hur.setEntity(new StringEntity(json_result.toString()));
        
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse resp0 = httpClient.execute(hur);
        HttpEntity respEntity0 = resp0.getEntity();
        
        String respString0 = EntityUtils.toString(respEntity0);
        JSONParser parser = new JSONParser();
        
        String computeEndPoint = null;
        String tokenID = null;
        
        try
        {
            
            
            
            JSONObject result = (JSONObject)parser.parse(respString0);
            JSONObject access = (JSONObject)result.get("access");
            JSONObject token = (JSONObject)access.get("token");
            JSONObject tenant = (JSONObject)token.get("tenant");
            String tenantID = (String)tenant.get("id");
            tokenID  = (String)token.get("id");
            JSONArray serviceCatalog = (JSONArray)access.get("serviceCatalog");
            if(serviceToCheck.equals(OpenStackConnector.NOVA)){
            for(Object i : serviceCatalog)
            {
                JSONObject service = (JSONObject)i;
                String type = (String)service.get("type");
               
                if("compute".equals(type))
                {
                    //we can have more than one point here but is's supposed to have only one
                    JSONArray endpoints = (JSONArray)service.get("endpoints");               
                    for(Object j : endpoints)
                    {
                        String region = (String)((JSONObject)j).get("region");
                        if(region.equals(zone))
                        {
                            computeEndPoint = (String)((JSONObject)j).get("publicURL");
                            break;
                        }
                    }
                                 
                    if(null!=computeEndPoint)break;
                }
                
                
            }
            }else if(serviceToCheck.equals(OpenStackConnector.GLANCE)){
            for(Object i : serviceCatalog)
            {
                JSONObject service = (JSONObject)i;
                String type = (String)service.get("type");
               
                if("image".equals(type))
                {
                    //we can have more than one point here but is's supposed to have only one
                    JSONArray endpoints = (JSONArray)service.get("endpoints");               
                    for(Object j : endpoints)
                    {
                        String region = (String)((JSONObject)j).get("region");
                        if(region.equals(zone))
                        {
                            computeEndPoint = (String)((JSONObject)j).get("publicURL");
                            break;
                        }
                    }
                                 
                    if(null!=computeEndPoint)break;
                }
                
                
            }
        }
            
            
            
            while(computeEndPoint.endsWith("/")) computeEndPoint = computeEndPoint.substring(0, computeEndPoint.length()-1);
        }
            
        catch (Exception e)
        {
        }
        return new OpenStackConnector.TokenEndpoint(tokenID,computeEndPoint);
    
            
    }
    
    /**
     * Returns JSON object describing network.
     * Cannot be converted to a specialized object because of lack of information about data types in response
     * @param zone Zone name
     * @param id ID of network
     * @return JSONObject. Look at the OpenStack documentation. Request nova-api /os-networks/{network-id}
     */
    public JSONObject getVirtualNetwork(String zone, String id)
    {
        try
        {
            OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zone,OpenStackConnector.NOVA);
            
            String computeEndPoint = tokenEndpoint.GetEndpoint();
            String tokenID = tokenEndpoint.GetToken();
            
            computeEndPoint += "/os-networks/" + id;
            HttpGet hur = new HttpGet(computeEndPoint);
            hur.addHeader("x-auth-project", tenantU);
            hur.addHeader("x-auth-token", tokenID);
            hur.addHeader("accept", "application/json");
            
            HttpClient httpClient = new DefaultHttpClient();
            JSONParser parser = new JSONParser();
                     
            
            HttpResponse resp = httpClient.execute(hur);
            HttpEntity respEntity = resp.getEntity();
            String respString = EntityUtils.toString(respEntity);
            
                        
            JSONObject result = (JSONObject)parser.parse(respString);
            JSONObject network = (JSONObject)result.get("network");
            
            
            return network;
                        
        }
        catch(Exception e)
        {
              return null;          
        }
    }
        
    /**
     * Returns list of virtual networks
     * @param zone 
     * @return
     * @throws IOException 
     */
    public LinkedList<String> getVirtualNetworkList(String zone) 
    {
        try
        {
            OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zone,OpenStackConnector.NOVA);
            
            String computeEndPoint = tokenEndpoint.GetEndpoint();
            String tokenID = tokenEndpoint.GetToken();
            
            computeEndPoint += "/os-networks";
            HttpGet hur = new HttpGet(computeEndPoint);
            hur.addHeader("x-auth-project", tenantU);
            hur.addHeader("x-auth-token", tokenID);
            hur.addHeader("accept", "application/json");
            
            HttpClient httpClient = new DefaultHttpClient();
            JSONParser parser = new JSONParser();
                     
            
            HttpResponse resp = httpClient.execute(hur);
            HttpEntity respEntity = resp.getEntity();
            String respString = EntityUtils.toString(respEntity);
            
            //Strange workaround because of strange OpenStack answer with timestamp(float)'s
            //We don't need timestamps so we can just replace timestamp(float) by float's
            respString = respString.replace("timestamp","").replace("(", "").replace(")", "");
            
            JSONObject result = (JSONObject)parser.parse(respString);
            JSONArray networks = (JSONArray)result.get("networks");
            
            LinkedList<String> networkList = new LinkedList<String>();
            
            for(Object network : networks)
            {
                networkList.add((String)((JSONObject)network).get("id"));
            }
            return networkList;
                        
        }
        catch(Exception e)
        {
              return null;          
        }
        
    }
    
    /**
     * Create new disk image
     * @param name Name of the image (can not be unique)
     * @param filename Name of the file (local)
     * @param zoneId Id of OpenStack zone
     * @param diskFormat @see org.jclouds.openstack.glance.v1_0.domain.DiskFormat
     * @param containerFormat @see org.jclouds.openstack.glance.v1_0.domain.ContainerFormat
     * @return Added image id
     */
    private String addImageSmall(
            String name, 
            String filename, 
            String zoneId, 
            DiskFormat diskFormat, 
            ContainerFormat containerFormat)
    {
        
        CreateImageOptions cio = new CreateImageOptions();
        cio.diskFormat(diskFormat);
        cio.containerFormat(containerFormat);
        cio.isPublic(true);
        
        
        Payload payload = new FilePayload(new File(filename));
        
        GlanceApi api = GetGlanceApi();
//        String imapi = api.getImageApiForZone(zoneId)
//        .create(name, payload, cio).getId();
        ImageApi ap = api.getImageApiForZone(zoneId);
        ImageDetails id = ap.create(name, payload, cio);
        return id.getId();
//        ImageDetails im = GetGlanceApi().getImageApiForZone(zoneId).create(name, payload, cio);
//        if(im.getStatus() == Image.Status.KILLED || im.getStatus() == Image.Status.UNRECOGNIZED)
//        	return null;
//        else 
//        	return im.getId();        
        //String id = GetGlanceApi().getImageApiForZone(zoneId).reserve(name, cio).getId();
        //GetGlanceApi().getImageApiForZone(zoneId).upload(id, payload, uio);
        //return id;
        
    }
    
    /**
     * Get image status. Mapping of statuses:
     * 
     * @param imageId UUID of image
     * @param zoneId Zone ID
     * @return 
     * <ul>
     *  <li>ACTIVE          : "rdy"</li>
     *  <li>DELETED         : "err"</li>
     *  <li>KILLED          : "err"</li>
     *  <li>PENDING_DELETE  : "lock"</li>
     *  <li>QUEUED          : "lock"</li>
     *  <li>UNRECOGNIZED    : "UNK"</li>
     *  <li>Get image error  : "UNK"</li>
     * </ul>
     */
    public String imageStatus(String imageId, String zoneId)
    {
        ImageDetails imageDetails = GetGlanceApi().getImageApiForZone(zoneId).get(imageId);
        if(imageDetails!=null)
        {
            Image.Status status = imageDetails.getStatus();
            switch(status)
            {
                case ACTIVE:
                    return "RDY";
                case DELETED:
                    return "ERR";
                case KILLED:
                    return "ERR";
                case PENDING_DELETE:
                    return "LCK";
                case QUEUED:
                    return "LCK";
                case UNRECOGNIZED:
                    return "UNK";
                    //used rdy err lock disa UNK
            }
            
        }
        return "UNK";
           
    }
    
    /**
     * Delete disk image
     * @param imageId Glance image id
     * @param zone Zone name
     * @return true if success, false if fail
     */
    public boolean removeImage(String imageId, String zone)
    {
        return GetGlanceApi().getImageApiForZone(zone).delete(imageId);
    }

    
    private long createNewFlavour(String zone, String flavourName, int diskSize, int ephemeralSize, int ramSize, int cores)  
    {
        NovaApi novaApi = GetNovaApi();
        FlavorApi flavorApi = novaApi.getFlavorApiForZone(zone);
        PagedIterable<? extends Flavor> flavours = flavorApi.listInDetail();
        long max=0l;
        for(Iterable <? extends Flavor> i : flavours)
        {
            for(Flavor j : i)
            {
                long t = Long.parseLong(j.getId());
                max = (t>max)?t:max;                
            }
            
        }
        ++max;
        
        Flavor.Builder fb = Flavor.builder();
        fb.disk(diskSize);
        fb.ephemeral(ephemeralSize);
        fb.id(Long.toString(max));
        fb.ram(ramSize);
        fb.name(flavourName);
        fb.vcpus(cores);
        
        Flavor fv = fb.build();
        flavorApi.create(fv);
        return max;
    }
    
    private int injectMaxFileSize = 10*(1<<10); //10 KB
    private int injectMaxNumberOfFiles = 5;
    
    /**
     * Opens a file and stores it to byte array 
     * Won't read file larger than injectMaxFileSize bytes
     * @param path path to the file
     * @return byte array containing file
     * @throws IOException 
     */
    private byte[] FileToByteArray(String path) throws Exception, IOException
    {
        File file = new File(path);
        InputStream is = new FileInputStream(file);
            
        long length = file.length();
        
        if (length > injectMaxFileSize)
        {
            throw new Exception("File \""+path+"\" is too large (max: " + Integer.toString(injectMaxFileSize) +")");
        }
        
        byte[] bytes = new byte[(int)length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0)
        {
            offset += numRead;
        }
        
        if (offset < bytes.length) 
        {
            throw new IOException("Could not completely read file "+ path);
        }

        is.close();
        
        return bytes;
    }
    
    /**
     * Builds JSONArray for injecting files in VM.
     * Service internal purpose only
     * @param files Map. Key - path to the file on VM, value - path on local machine
     * @return 
     */
    private JSONArray buildFileInjections(Map<String,String> files) throws Exception, IOException
    {
        if(files.size()>injectMaxNumberOfFiles) 
        {
            throw new Exception("Too much files (max: " + Integer.toString(injectMaxNumberOfFiles) +")");
        }
        
        JSONArray result = new JSONArray();
        Set<Map.Entry<String,String>> entries = files.entrySet();
        for (Map.Entry<String,String> i : entries)
        {
            
            JSONObject jObj = new JSONObject();
            
            byte[] binaryData = FileToByteArray(i.getValue());
            String base64BinaryData = Base64.encodeBase64String(binaryData);
            jObj.put("path", i.getKey());
            jObj.put("contents", base64BinaryData);
            base64BinaryData=null; //to free memory
            result.add(jObj);
        }
        return result;
    }
    
    
    /**
     * Creates new virtual machine and flavour. 
     * Limitations on files to inject. At most 5 files not bigger than 10 KB. 
     * Only text files.  If you need to pass binary data encode it as Base64(for example) first - For now it works with binary data.
     * Automatically starts the VM (no need to start it)
     * @param VMName Name of the virtual machine may not be unique
     * @param imageId Image id to boot from
     * @param zone Zone id to create VM in
     * @param diskSize Size of persistent disk in MB
     * @param ephemeralSize Size of ephemeral disk (kill after delete, not reboot VM) in MB 
     * @param ramSize Size of RAM in MB
     * @param cores Number of cores
     * @param hostname Host name of the server to boot the instance on
     * @param networks LinkedList of network uuid's
     * @param filesToInject Map. Key contains filename on VM. Value contains name of the file on local machine.
     * @param sshKeyName Provide an ssh-key to access to machine. Provide null to do not add the key
     * @return VM id 
     * @throws Exception, IOException, ParseException
     */
    public String addVM(String VMName, String imageId, String zone, int diskSize, int ephemeralSize, int ramSize, int cores, String hostname, LinkedList<String> networks, Map<String, String> filesToInject, String sshKeyName, String sshKeyValue, String userData, JSONObject metadata) throws IOException, ParseException, Exception
    {
        
        OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zone,OpenStackConnector.NOVA);
        long flavourId = createNewFlavour(zone, VMName+"-Flavour-" + UUID.randomUUID().toString() , diskSize, ephemeralSize, ramSize, cores);
//        flavourId=9; 
        JSONArray networksJSON = new JSONArray();
        for(String i : networks)
        {
            JSONObject t=new JSONObject();
            t.put("uuid", i);
            networksJSON.add(t);
        }
                
        JSONObject server = new JSONObject();
        server.put("name", VMName);
        server.put("imageRef", imageId);
        server.put("availability_zone", zone+":"+hostname);
        server.put("flavorRef", Long.toString(flavourId));
        server.put("networks", networksJSON);
        server.put("personality", buildFileInjections(filesToInject));
        
        if(sshKeyName!=null)
        {
            server.put("key_name", sshKeyName); 
            HttpGet hur = new HttpGet(tokenEndpoint.endpoint+"/os-keypairs");
            hur.addHeader("x-auth-project-id", tenantU);
            hur.addHeader("x-auth-token", tokenEndpoint.token);
            hur.addHeader("accept", "application/json");
            hur.addHeader("content-type", "application/json");
                
            HttpClient httpClient = new DefaultHttpClient();
            JSONParser parser = new JSONParser();
               
            try
            {
                HttpResponse resp = httpClient.execute(hur);
                HttpEntity respEntity = resp.getEntity();
                String respString = EntityUtils.toString(respEntity);

                JSONObject respJSON = (JSONObject)parser.parse(respString);
                JSONArray kps =  (JSONArray)respJSON.get("keypairs");
                boolean addKey=true;
                for(int i=0; i<kps.size(); i++)
                {
                	JSONObject kp = (JSONObject) kps.get(i);
                	if(((JSONObject)kp.get("keypair")).get("name").equals(sshKeyName)){
                		addKey=false;
                		break;
                	}
                }
                if(addKey)
                {
                	 HttpPost ps = new HttpPost(tokenEndpoint.endpoint+"/os-keypairs");
                	 ps.addHeader("x-auth-project-id", tenantU);
                	 ps.addHeader("x-auth-token", tokenEndpoint.token);
                	 ps.addHeader("accept", "application/json");
                	 ps.addHeader("content-type", "application/json");
                     JSONObject keypairdata = new JSONObject();
                     keypairdata.put("name", sshKeyName);
                     keypairdata.put("public_key", sshKeyValue);
                     JSONObject kp = new JSONObject();
                     kp.put("keypair", keypairdata);
                     String requestString = kp.toJSONString();
                     ps.setEntity(new StringEntity(requestString));
                     
                     httpClient = new DefaultHttpClient();
                     parser = new JSONParser();
                        
                     try
                     {
                         resp = httpClient.execute(ps);
                         respEntity = resp.getEntity();
                         respString = EntityUtils.toString(respEntity);

                         respJSON = (JSONObject)parser.parse(respString);
                     } catch(Exception e){throw e;}
                }
            } catch(Exception e){throw e;}
        }
        
        if(userData != null)
        {
        	userData =  Base64.encodeBase64String(userData.getBytes());
        	server.put("user_data", userData);
        }
        
        if(!metadata.isEmpty())
        {
        	server.put("metadata", metadata);
        }
        
        JSONObject request = new JSONObject();
        request.put("server",server);
        
        HttpPost hur = new HttpPost(tokenEndpoint.endpoint+"/servers");
        hur.addHeader("x-auth-project-id", tenantU);
        hur.addHeader("x-auth-token", tokenEndpoint.token);
        hur.addHeader("accept", "application/json");
        hur.addHeader("content-type", "application/json");
        String requestString = request.toJSONString();
        //System.out.println(requestString);
        hur.setEntity(new StringEntity(requestString));
            
        HttpClient httpClient = new DefaultHttpClient();
        JSONParser parser = new JSONParser();
           
        try
        {
            HttpResponse resp = httpClient.execute(hur);
            HttpEntity respEntity = resp.getEntity();
            String respString = EntityUtils.toString(respEntity);

            JSONObject respJSON = (JSONObject)parser.parse(respString);
            server =  (JSONObject)respJSON.get("server");
            String serverId = (String)server.get("id");
            return serverId;
        }
        catch(Exception e)
        {
            Thread.sleep(5000);
            //return addVM(VMName, imageId, zone, diskSize, ephemeralSize, ramSize, cores, hostname, networks, filesToInject, sshKeyName, sshKeyValue,userData,metadata);
            e.printStackTrace();
            throw e;
        
        }
    }
    
    /**
     * Shutdown the machine
     * @param VMId Virtual machine Nova ID
     * @param zone Zone name
     * @return true if success, else returns false
     */
    public boolean shutdownVM(String VMId, String zone)
    {
        try
        {
            GetNovaApi().getServerApiForZone(zone).stop(VMId);
            return true;
        }
        catch(Throwable t)
        {
            return false;
        }
        
    }
    
    /**
     * Reboot specified VM
     * @param VMId Virtual machine Nova id
     * @param zone Zone name
     * @param rebootType Reboot type: Hard or Soft
     * @return true if success, else returns false
     */
    public boolean restartVM(String VMId, String zone, RebootType rebootType)
    {
        try
        {
            GetNovaApi().getServerApiForZone(zone).reboot(VMId, rebootType);
            return true;
        }
        catch(Throwable t)
        {
            return false;
        }
    }
    
    /**
     * Start specified VM
     * @param VMId Virtual machine Nova id
     * @param zone Zone name
     * @return true if success, else returns false
     */
    public boolean startVM(String VMId, String zone)
    {
        try
        {
            GetNovaApi().getServerApiForZone(zone).start(VMId);
            return true;
        }
        catch(Throwable t)
        {
            return false;
        }
    }

    /* Delete specified VM
    * @param VMId Virtual machine Nova id
    * @param zone Zone name
    * @param deleteFlavor delete flavor after stopping the machine
    * @return true if success, else returns false
    */
    public boolean deleteVM(String VMId, String zone, Boolean deleteFlavor)
    {
        
        try
        {
            String flavorId="";
            if(deleteFlavor)
            {
                flavorId = getVmInfo(VMId, zone).getFlavor().getId();
            }
            GetNovaApi().getServerApiForZone(zone).delete(VMId);
            if(deleteFlavor)
            {
                GetNovaApi().getFlavorApiForZone(zone).delete(flavorId);
            }
            return true;
        }
        catch(Throwable t)
        {
            return false;
        }
    }
    
    /**
     * The same as addVM 
     * @see #addVM
     */
    public String deployVM(String VMName, String imageId, String zone, int diskSize, int ephemeralSize, int ramSize, int cores, String hostname, LinkedList<String> networks, Map<String, String> filesToInject, String sshKeyName, String sshKeyValue, String userData, JSONObject metaData) throws Exception, IOException, ParseException
    {
        return addVM(VMName, imageId, zone, diskSize, ephemeralSize, ramSize, cores, hostname, networks, filesToInject,sshKeyName, sshKeyValue, userData, metaData);
    }
    
    /**
     * Returns object containing full data about VM
     * @param VMId Virtual machine Nova id
     * @param zone Zone name
     * @return VM object
     */
    public Server getVmInfo(String VMId, String zone)
    {
        return GetNovaApi().getServerApiForZone(zone).get(VMId);
    }
    
    
    /**
     * Create new disk image
     * @param name Name of the image (can not be unique)
     * @param filename Name of the file (local)
     * @param zoneId Id of OpenStack zone
     * @param diskFormat @see org.jclouds.openstack.glance.v1_0.domain.DiskFormat
     * @param containerFormat @see org.jclouds.openstack.glance.v1_0.domain.ContainerFormat
     * @return Added image id
     */
        public String addImage(
            String name, 
            String filename, 
            String zoneId, 
            DiskFormat diskFormat, 
            ContainerFormat containerFormat)
    throws IOException, ParseException, Exception
    {
        File file = new File(filename);
        if( file.length() < Integer.MAX_VALUE){
            return this.addImageSmall(name, filename, zoneId, diskFormat, containerFormat);
        }
       String result=null;
        OpenStackConnector.TokenEndpoint tokenEndpoint = GetTokenEndpoint(zoneId,OpenStackConnector.GLANCE);

        org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
        Logger.getLogger("httpclient").setLevel(Level.OFF);
        PostMethod httppost = new PostMethod(
                tokenEndpoint.endpoint+"/images");
           httppost.addRequestHeader("x-auth-project-id", tenantU);
        httppost.addRequestHeader("x-auth-token",tokenEndpoint.token );
        httppost.addRequestHeader("accept", "application/json");
         httppost.addRequestHeader("x-image-meta-disk_format",diskFormat.value());
         httppost.addRequestHeader("x-image-meta-container_format",containerFormat.value());
        
        httppost.addRequestHeader("x-image-meta-name",name);
        httppost.addRequestHeader("content-type", "application/octet-stream");
      
       
        
        httppost.setRequestEntity(
                new InputStreamRequestEntity(new FileInputStream(file)));
        
        httppost.setContentChunked(true);
        
        try {
            
            client.executeMethod(httppost);
            
            if (httppost.getStatusCode() == HttpStatus.SC_CREATED) {
                
                 Object obj=JSONValue.parse(httppost.getResponseBodyAsString());
                 JSONObject  jobj=(JSONObject)obj;
                 jobj=(JSONObject)jobj.get("image");
                 result = (String)jobj.get("id");
            } else {
                
                System.out.println("Unexpected failure: " + 
                httppost.getStatusLine().toString()+"\n"+ httppost.getResponseBodyAsString());
                
                
            }
            
        } finally {
            httppost.releaseConnection();
        }
            return result;
       
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

