/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import eu.contrail.security.SecurityUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author florian
 */
public class TemplateGenerator {
	private int typeId;
	private String version;
	private String adminIaaSUname;
	private final static String COMMON_TEMPLATE =  "NAME = \"vmNAME\" \nCPU = vmCPUpct \nVCPU = vmVCPU \nMEMORY = vmMEM\n"
			+ "OS = [ boot=\"hd\", arch=\"x86_64\" ]\n" + "vmNetworks" + "vmDisks" + "GRAPHICS = [\n" + "type=\"vnc\",\n" + "listen=\"localhost\"\n" + "]";


	public TemplateGenerator(int typeId, String version){
		this.typeId = typeId;
		this.version = version;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @param params String vcpu, String ram, String username, ArrayList<HashMap<String, String>> networks, 
	 * HashMap<String, String> osDisk, String ovfId, String ovfVSId, String name, String context, String adminIaasUname, 
	 * String cert, String uuid, String vmSlotId
	 * @return
	 */
	public String getTemplate(HashMap<String, Object> params){
		String template = "";
		switch(this.typeId) {
		case App.OPENNEBULA3_HOST:
			//OpenNebula case
			String vcpu = params.get("vcpu").toString();
			vcpu = "1";//temp hack TODO:Make it better, how to retrieve this value from tables?
			String ram = params.get("ram").toString();
                        //change Filippo
			//String username = params.get("username").toString();
			ArrayList<HashMap<String, String>> networks = (ArrayList<HashMap<String, String>>)params.get("networks");
			HashMap<String, String> osDisk = (HashMap<String, String>) params.get("osDisk");
			String ovfId = params.get("ovfId").toString();
			String ovfVSId = params.get("ovfVSId").toString();
			String name = params.get("name").toString();
			String context = "";
			if(params.get("context")!= null)
			{
				context = params.get("context").toString();
			} 
			adminIaaSUname = params.get("adminIaaSUname").toString();

			String destination=null;
			if(params.get("cert")!=null)
				destination=this.sendCertificate((String)params.get("uuid"),(String)params.get("vmslotId") , (X509Certificate)params.get("cert"),(PrivateKey)params.get("privateKey"));
			//transferred       
			template = getONETemplate(name, vcpu, ram, osDisk, networks, context,destination);
			break;
		default:
			break;
		}
		return template;
	}



	private String getONETemplate(String name, String vcpu, String ram, HashMap<String, String> osDisk, ArrayList<HashMap<String, String>> networks, String context,String pathCert){
		String template  = "";
		String vmName = name;
		String vmCPUpct = "1";
		String vmVCPU = vcpu;
		String vmMEM = ram; 
		String vmDisks = "";
                String contextNetwork="";
		//        
		if(version.startsWith("3")){
			//check image format to do
                         vmDisks += "DISK = [IMAGE=\"" + osDisk.get("name") + "\",\nIMAGE_UNAME=\"" + adminIaaSUname + "\", \ndriver = \"qcow2\" ,\ndisk_prefix=\"vd\",  \nreadonly=\"no\" ]\n";
                        for (HashMap<String, String> n : networks){
                         contextNetwork="hostname    = \""+vmName+"\",\n"+
                                "ip_public   = \"$NIC[IP, NETWORK=\\\""+n.get("name")+"\\\"]\",\n"+
    "dns        = \"$NETWORK[DNS, NETWORK=\\\""+n.get("name")+"\\\"]\",\n"+
    "gateway    = \"$NETWORK[GATEWAY, NETWORK=\\\""+n.get("name")+"\\\"]\",\n"+
    "root_pubkey = id_rsa.pub,";
                        }
                }else if(version.startsWith("2"))
			vmDisks += "DISK = [IMAGE=\"" + osDisk.get("name") + "\"]\n";
		String vmNetworks = "";
               
		for (HashMap<String, String> n : networks){
			if(version.startsWith("3")){
				vmNetworks += "NIC = [NETWORK=\"" + n.get("name") + "\",\nNETWORK_UNAME=\"" + adminIaaSUname + "\"]\n";
                                
                        }
			else if(version.startsWith("2"))
				vmNetworks += "NIC = [NETWORK=\"" + n.get("name") + "\"]\n";
		}

		template = COMMON_TEMPLATE.replaceAll("vmNAME", vmName).replaceAll("vmCPUpct", vmCPUpct).replaceAll("vmVCPU", vmVCPU)
				.replaceAll("vmMEM", vmMEM).replaceAll("vmNetworks", vmNetworks).replaceAll("vmDisks", vmDisks);
		if(!(context.trim().length() == 0))
		{        
			String contextTemplate = "CONTEXT=[\n";
			contextTemplate+="files=\""+VEPHelperMethods.getProperty("vep.share", null)+"/init.sh";
			if(pathCert!=null) contextTemplate+=" "+pathCert;
                        
                        ///need to change networkid FILIPPO
			contextTemplate+="\",\n";
                        contextTemplate+=contextNetwork;
			contextTemplate += context;
			contextTemplate = contextTemplate.substring(0, contextTemplate.length() - 2);
			contextTemplate = contextTemplate + "\n]";
			template += "\n" + contextTemplate;
		}else{


		}

		return template;
	}

	public String sendCertificate(String username, String vmslotid, X509Certificate cert,PrivateKey privatek){
		String remoteDestination=null;
		String certDestination="cert";
		try{
			remoteDestination=VEPHelperMethods.getProperty("vep.share", null);
			checkDirectory(remoteDestination,false);
			remoteDestination+="/"+username;
			checkDirectory(remoteDestination,false);

			remoteDestination+="/"+vmslotid;
			checkDirectory(remoteDestination,true);

			remoteDestination+="/"+certDestination;
			SecurityUtils.writeCertificate(cert,remoteDestination+".pem");
			FileOutputStream fop = new FileOutputStream(new File(remoteDestination+".key"));
			SecurityUtils.writeKey(fop, privatek);
			//  SecurityUtils.writeKey(, privatek, null);
			//  SecurityUtils.writeCertificate(privatek,remoteDestination);
			return remoteDestination+".pem "+remoteDestination+".key";
			/*	String copyServerIp = VEPHelperMethods.getProperty("copyserver.ip", null);
			int copyServerPort = Integer.parseInt(VEPHelperMethods.getProperty("copyserver.port", null));
			Socket sock = new Socket(copyServerIp, copyServerPort);
			PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out.println("hello");
			String response = in.readLine();
			if(response.equalsIgnoreCase("hello"))
			{
				out.println(VEPHelperMethods.getProperty("copyserver.admin",null));
				response = in.readLine();
				if(response.equalsIgnoreCase("ok"))
				{
					out.println(VEPHelperMethods.getProperty("copyserver.password", null));
					response = in.readLine();
					if(response.equalsIgnoreCase("ok"))
					{
						out.println("SRC=" + "stream://" + " USR=" + vmslotid);
						Socket sockStream = new Socket(copyServerIp, copyServerPort+1);                                            
						File file = new File(VEPHelperMethods.getProperty("vep.scratch", null)+certDestination);
						// Get the size of the file
						long length = file.length();
						if (length > Integer.MAX_VALUE) {
						    System.out.println("File is too large.");
						}
					    byte[] bytes = new byte[(int) length];
					    FileInputStream fis = new FileInputStream(file);
					    BufferedInputStream bis = new BufferedInputStream(fis);
					    BufferedOutputStream out1 = new BufferedOutputStream(sockStream.getOutputStream());

					    int count;
					    while ((count = bis.read(bytes)) > 0) {
					        out1.write(bytes, 0, count);
					    }
					    fis.close();
					    bis.close();
					    out1.close();
					    sockStream.close();
						response = in.readLine();
						if(response.startsWith("ok "))
						{                                                                    
							out.println("DST=" + "cert://"+vmslotid);
							response = in.readLine();
							if(response.startsWith("ok "))
							{
								remoteDestination=response.replaceAll("ok PATH=", "");
								//db.update("osdisk", "status='RDY'", "WHERE id=" + osDiskId);
								response = in.readLine();
								if(response.equalsIgnoreCase("quit"))
								{
									try
									{
										sock.close();
									}
									catch(Exception ex)
									{
										return null;
									}
								}
							}
						}
					}
				}
			}
			out.flush();
			out.close();

		    sock.close();  
		    return remoteDestination;
			 */								
		}
		catch(Exception ex)
		{
			return null;
		} 
	}



	private static void checkDirectory(String directory,boolean erase) throws IOException{
		File dir=new File(directory);  
		if ((dir != null) && dir.exists() && dir.isDirectory()) {
			System.out.println("EXIST DIRECTORY");
			if(erase){
				deleteInside(dir);
				dir.mkdir();
			}
		}else{
			System.out.println("CREATE DIRECTORY");
			dir.mkdir();
		}
	}

	private static void deleteInside(File file) throws IOException {
		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				return;
				//  System.out.println("Directory is deleted : " 
				//                              + file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					deleteInside(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					//System.out.println("Directory is deleted : " 
					//                           + file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			//System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
}