/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.OAuthClient;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.CCFlowClient;
import org.ow2.contrail.common.oauth.client.CertRetriever;
import org.ow2.contrail.common.oauth.client.KeyAndCertificate;

import eu.contrail.security.SecurityCommons;

/**
 *
 * @author fgaudenz
 */
public class OAuthClientFactory {
   public static VEPHelperMethods helperMethods;
      public static void main(String[] args) throws URISyntaxException, URISyntaxException, Exception 
    {
    	  String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator") + 
      			".vep" + System.getProperty("file.separator")+"/vep.properties";
    	  helperMethods = new VEPHelperMethods(defaultPath);
       OAuthClientFactory app=new OAuthClientFactory("76938185-cc9a-3b03-a68e-b7a5c3bf5d7c");
//       System.out.println("CERTIFICATE :"+app.getDelegateCertificate());
       KeyAndCertificate certs=app.getDelegateCertificate();
       X509Certificate cert=certs.getCertificate();
       PrivateKey pk=certs.getPrivateKey();
       SecurityCommons sc = new SecurityCommons();
       
//       String publicKey = ((X509Certificate)cert).toString();
       ByteArrayOutputStream c = new ByteArrayOutputStream();
       sc.writeCertificate(c, cert);
       String publicKey = c.toString();
//		params.put("publicKey", publicKey);
		
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		
		
//		PrivateKey pk = (PrivateKey)params.get("privateKey");
		sc.writeKey(b, pk);
		String privateKey = b.toString();
//		params.put("privateKey", privateKey);
		System.out.println("public: "+publicKey);
		System.out.println("private: "+privateKey);
    }
    
          /* String tokenEndpoint="https://contrail.xlab.si:8443/oauth-as/r/access_token/request";
           String keystoreFile="/home/fgaudenz/vepTester/oauth-java-vepclient1.jks";
           String keystorePass="contrail";
           String truststoreFile="/home/fgaudenz/vepTester/cacerts.jks";
           String truststorePass="contrail";
           String clientId="oauth-java-vepclient1";
           String clientSecret="somesecret";
           String resourceOwner="76938185-cc9a-3b03-a68e-b7a5c3bf5d7";
                   //"5a947f8c-83d3-4da0-a52c-d9436ae77bb5";
           String scope="";
           String userCertEndpoint="https://contrail.xlab.si:8443/ca/o/delegateduser";
           boolean active=true; */
          
           
           
             String tokenEndpoint;
           String keystoreFile;
           String keystorePass;
           String truststoreFile;
           String truststorePass;
           String clientId;
           String clientSecret;
           String resourceOwner;
                   //"5a947f8c-83d3-4da0-a52c-d9436ae77bb5";
           String scope;
           String userCertEndpoint;
           boolean active=true; 
          
    public OAuthClientFactory(String userUUID) throws URISyntaxException, Exception {
//          System.setProperty("https.proxyHost", "131.254.14.157");
//           System.setProperty("https.proxyPort", "3128");
//              System.setProperty("http.proxyHost", "131.254.14.157");
//           System.setProperty("http.proxyPort", "3128");
          this.resourceOwner=userUUID;
        
        try{   
          this.tokenEndpoint = VEPHelperMethods.getProperty("authzserver.authorizationEndpointUri", null);
          this.keystoreFile= VEPHelperMethods.getProperty("authzserver.keystoreFile", null);
          this.keystorePass=VEPHelperMethods.getProperty("authzserver.keystorePass", null);
          this.truststoreFile=VEPHelperMethods.getProperty("authzserver.truststoreFile", null);
          this.truststorePass=VEPHelperMethods.getProperty("authzserver.truststorePass", null);
          this.clientId=VEPHelperMethods.getProperty("authzserver.cliendId", null);
          this.clientSecret=VEPHelperMethods.getProperty("authzserver.clientSecret", null);
          this.userCertEndpoint=VEPHelperMethods.getProperty("authzserver.userCertEndpoint", null);
          if(this.tokenEndpoint==null) active=false;
          if(this.clientId==null) active=false;
          if(this.keystoreFile==null) active=false;
          if(this.keystorePass==null) active=false;
          if(this.truststoreFile==null) active=false;
          if(this.truststorePass==null) active=false;
          if(this.clientSecret==null) active=false;
          if(this.userCertEndpoint==null)    active=false;
        }catch(Exception ex){
            active=false;
        }
    }
    
   private AccessToken getToken() throws URISyntaxException, URISyntaxException, Exception {
   
   if(active==true){    
   CCFlowClient ccFlowClient;
   ccFlowClient = new CCFlowClient(new URI(tokenEndpoint), keystoreFile, keystorePass,truststoreFile, truststorePass);
   ccFlowClient.setClientId(clientId);
   ccFlowClient.setClientSecret(clientSecret);
   AccessToken accessToken = ccFlowClient.requestAccessToken(resourceOwner,null);
   System.out.println("Received access token: " + accessToken.getValue());
   return accessToken;
   }else
       return null;
    }
   
    
    
    public KeyAndCertificate getDelegateCertificate() throws Exception{
        if(active==true){
        CertRetriever certRetriever = new CertRetriever(new URI(userCertEndpoint), keystoreFile, keystorePass,truststoreFile, truststorePass);
        KeyAndCertificate certs = certRetriever.retrieveCert(this.getToken().getValue());
        return certs;
        }else
            return null;
       
        
    }
    
    
/*    public static AccessToken getTokenAccess(String resourceOwner, String scope) throws Exception {
        URI tokenEndpointUri = new URI("https://contrail.xlab.si:8443/oauth-as/r/access_token/request");
        CCFlowClient ccFlowClient = new CCFlowClient(tokenEndpointUri,
                "/home/fgaudenz/Code/trunk/common/oauth/oauth-java-client-demo/src/main/resources/oauth-java-client-demo.jks",
               "contrail",
                "/home/fgaudenz/Code/trunk/common/oauth/oauth-java-client-demo/src/main/resources/cacerts.jks",
               "contrail");
        ccFlowClient.setClientId(Conf.getInstance().getClientId());
        ccFlowClient.setClientSecret(Conf.getInstance().getClientSecret());

        return ccFlowClient.requestAccessToken(resourceOwner, scope);
    }*/

    public boolean isActive() {
        return active;
    }
}
