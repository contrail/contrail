/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VMCollection extends VEPCollection {

	private String ceeId;
	private String applicationId;
	public ArrayList<VEPVirtualMachine> vms = new ArrayList<VEPVirtualMachine>();
	
	public VMCollection(String ceeId, String applicationId) {
		super("VMCollection", true);
		this.ceeId = ceeId;
		this.applicationId = applicationId;
		this.setResourceUri("VEP/VMCollection");
		this.setId(ceeId+"/vms");
	}

	/**
	 * Retrieve from VMSlots
	 * Selects all ovf from applicationid, select all vmslots from vmslots where ovf = one of the previous + ceeid = the requested ceeid
	 * @throws SQLException
	 */
	public void retrieveVMCollection() throws SQLException
	{
		ResultSet rs;
		logger.debug("Retrieving VirtualMachines collection for application " + applicationId);

		rs = db.query("select", "v.id", "vmslots as v, ovf as o, ovfmap as om", "where o.applicationid="  + applicationId + " and om.ovfcontainerid=o.id and v.ovfmapid=om.id");
		while (rs.next()){
			VEPVirtualMachine v = new VEPVirtualMachine(false);
			v.setId("/cee/" + ceeId + "/application/" + applicationId + "/vm/" + rs.getInt("id"));
			vms.add(v);
			this.incrementCount();
		}
	}
}
