/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.ow2.contrail.provider.vep.App;

public class StorageHandler extends VEPObject {

	private String ceeId;
	private String storageHandlerId;
	private String endpoint;
	private String storagetype;
	private int rackid;
	
	
	public StorageHandler(boolean instantiate) {
		super("StorageHandler", instantiate);
	}
	
	public StorageHandler(String ceeId, String storageHandlerId) {
		super("StorageHandler", true);
		this.ceeId = ceeId; //not useful?
		this.storageHandlerId = storageHandlerId;
		this.setId("/cee/"+ceeId+"/storageHandler/"+storageHandlerId);
		this.setResourceUri("VEP/StorageHandler");
	}
	
	public StorageHandler(String storageHandlerId) {
		super("NetHandler", true);
		this.storageHandlerId = storageHandlerId;
		this.setId("/storageHandler/"+storageHandlerId);
		this.setResourceUri("VEP/StorageHandler");
	}

	public void retrieveStorageHandler() throws SQLException
	{
		ResultSet rs = db.query("select", "storagehandler.name as shname, storagetypeid, endpoint, rackid", "storagehandler,storage", "where storagehandler.id="+storageHandlerId+" and storage.id=storagehandler.storageid");
		if (rs.next())
		{
			this.setName(rs.getString("shname"));
			switch (rs.getInt("storagetypeid")) {
			case App.GAFS_SHARE:
				storagetype = "GAFS Share";
				break;
			case App.LOCAL_DISK_STORAGE:
				storagetype = "Local Disk Storage";
				break;
			case App.NFS_SHARE:
				storagetype = "NFS Share";
				break;
			default:
				setError("Unknown Storage Type");
			}
			endpoint = rs.getString("endpoint"); // may be null
			rackid = rs.getInt("rackid");
		} else {
			setError("Unknown Storage Handler id");
		}
	}

	public String getEndPoint() {
		return endpoint;
	}

	public String getType() {
		return storagetype;
	}
}
