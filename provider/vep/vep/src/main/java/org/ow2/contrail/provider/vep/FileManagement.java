/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;



import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class FileManagement implements Runnable
{
	private Thread t;
        private String fileDest,fileSource;
	private String pName;
	private boolean isRunning;
	private Logger logger;
	private String username;
	
	public FileManagement(String fileDest,String fileSource, String processname)
	{
                fileDest=fileDest;
                fileSource=fileSource;
		pName = processname;
		isRunning = false;
		t = new Thread(this);
		logger = Logger.getLogger("VEP.FileManagement");
                username="contextualization";
	}
	
	public String getProcessName()
	{
		return pName;
	}
	
	public boolean getRunningState()
	{
		return isRunning;
	}
	
	public void start()
	{
		try
		{
			t.start();
		}
		catch(Exception ex)
		{
			logger.info("Exception caught inside file-management thread, process name: " + pName);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
	}

	public void run() 
	{
		logger.info("Starting the file management thread - process name: " + pName);
		isRunning = true;
		
			
	
				String source=fileSource;
				String dest = fileDest;
				//start the copy part
				try
				{
					String copyServerIp = VEPHelperMethods.getProperty("copyserver.ip", logger);
					int copyServerPort = Integer.parseInt(VEPHelperMethods.getProperty("copyserver.port", logger));
					Socket sock = new Socket(copyServerIp, copyServerPort);
					PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
					out.println("hello");
					String response = in.readLine();
					if(response.equalsIgnoreCase("hello"))
					{
						out.println(VEPHelperMethods.getProperty("copyserver.admin", logger));
						response = in.readLine();
						if(response.equalsIgnoreCase("ok"))
						{
							out.println(VEPHelperMethods.getProperty("copyserver.password", logger));
							response = in.readLine();
							if(response.equalsIgnoreCase("ok"))
							{
								out.println("SRC=" + source + " USR=" + username);
								response = in.readLine();
								if(response.startsWith("ok "))
								{
									out.println("DST=" + dest);
									response = in.readLine();
									if(response.equalsIgnoreCase("ok"))
									{
										response = in.readLine();
										if(response.equalsIgnoreCase("quit"))
										{
											try
											{
												sock.close();
											}
											catch(Exception ex)
											{
												
											}
										}
									}
									else
									{
										logger.info("Error during contexualization");
									}
								}
								else
								{
									logger.info("Error during contexualization");
								}
							}
							else
							{
								logger.info("Error during contexualization");
							}
						}
						else
						{
							logger.info("Error during contexualization");
						}
					}
					else
					{
						logger.info("Error during contexualization");
					}
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while trying to copy remote file from source=" + source + " to destination=" + dest);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
				}
				logger.info("Terminating the file management thread - process name: " + pName);
                                isRunning = false;
	}
		

        public static int checkCreateDirectory(String dir){
    	String dir1=VEPHelperMethods.getProperty("vep.scratch", null)+dir;
        File directory= new File(dir1);
    	if(!directory.exists())
    	{
    		boolean result = directory.mkdir();  
    	    if(result)
    	    {    
    	      return 0;   
    	    }
    	    else
    	    {
    	    	return -1; 
    	    }
    	}
    	else
    	{
    		return 1;
    	}
    }
        
        
        
     public static boolean delete( File fileOrDirectory ) {
      if (fileOrDirectory == null) return false;
      if (!fileOrDirectory.exists()) return false;

      // The file/directory exists, so if a directory delete all of the contents ...
      if (fileOrDirectory.isDirectory()) {
          for (File childFile : fileOrDirectory.listFiles()) {
              delete(childFile); // recursive call (good enough for now until we need something better)
          }
          // Now an empty directory ...
      }
      // Whether this is a file or empty directory, just delete it ...
      return fileOrDirectory.delete();
  }  
        
        
}
