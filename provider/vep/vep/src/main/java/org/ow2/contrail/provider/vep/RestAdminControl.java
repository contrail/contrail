/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;

public class RestAdminControl extends ServerResource
{
	private Logger logger;
    private String webHost, webPort;

	public RestAdminControl()
	{
		logger = Logger.getLogger("VEP.RestAdminControl");
        webHost = VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger);
        webPort = VEPHelperMethods.getProperty("webuser-interface.port", logger);
	}
	
	@Get
	public Representation getValue() throws ResourceException
	{
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml();
            else if(acceptType.contains("json"))
                response = toJson();
        }
        else
        {
            //default rendering ...
            response = toHtml();
        }
        return response;
	}
	
	public Representation toHtml() throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        stringBuilder.append("<b>Welcome to VEP Service Management Tool</b><br>Here you can check the status of various ")
        			.append("running services and manage some of the configuration parameters.<hr>");
        stringBuilder.append("<table><tr><td valign='top' width='790'>");
        boolean dbCheck = VEPHelperMethods.testDBconsistency();
        if(dbCheck)
        {
        	stringBuilder.append("<table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/database.png' width='48'><td>");
        	stringBuilder.append("<td valign='center' align='left'><i>The system database is consistent.</i>.</td></tr></table>");
        }
        else
        {
        	stringBuilder.append("<table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/database.png' width='48'><td>");
        	stringBuilder.append("<td valign='center' align='left'>The system database is inconsistent. Please login to reinitialize!<br><i>Beware! All old data values will be lost after re-initialization.</i>.</td></tr></table>");
        }
        RestServer temp = (RestServer) VEPHelperMethods.getService("rest");
        if(temp != null && temp.getState())
        {
        	stringBuilder.append("<br><table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/rest.png' width='48'><td>");
        	stringBuilder.append("<td valign='center' align='left'><i>The REST server is operational.</i>.</td></tr></table>");
        }
        else
        {
        	stringBuilder.append("<br><table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/rest.png' width='48'><td>");
        	stringBuilder.append("<td valign='center' align='left'><i>The REST server is not operational.</i>.</td></tr></table>");
        }
        try
        {
        	String copyServerIp = VEPHelperMethods.getProperty("copyserver.ip", logger);
        	int copyServerPort = Integer.parseInt(VEPHelperMethods.getProperty("copyserver.port", logger));
        	
        	Socket sock = new Socket(copyServerIp, copyServerPort);
        	PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
        	BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        	out.println("hello");
        	String response = in.readLine();
        	if(response.equalsIgnoreCase("hello"))
        	{
        		stringBuilder.append("<br><table style='border:0px;background:#B0C4DE;font-family:Verdana;font-size:10pt;width:789px;'>");
            	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/copy.png' width='48'><td>");
            	stringBuilder.append("<td valign='center' align='left'><i>The COPY service is operational.</i>.</td></tr></table>");
        	}
        	try
        	{
        		sock.close();
        	}
        	catch(Exception ex)
        	{
        		
        	}
        }
        catch(Exception ex)
        {
        	logger.warn("Copy service is not running probably.");
        	stringBuilder.append("<br><table style='border:0px;background:#FFDAB9;font-family:Verdana;font-size:10pt;width:789px;'>");
        	stringBuilder.append("<tr><td><img src='http://"+webHost+":"+webPort+"/copy.png' width='48'><td>");
        	stringBuilder.append("<td valign='center' align='left'><i>The COPY service status can not be determined.</i>.</td></tr></table>");
        }
    	
    	stringBuilder.append("<div style='font-family:Courier;font-size:10pt;'>");
    	stringBuilder.append("<br><b>VEP Configuration Parameters</b><br><br>");
    	try
    	{
    		FileReader fr = new FileReader(VEPHelperMethods.getPropertyFile());
    		BufferedReader br = new BufferedReader(fr);
    		String s;
    		while((s = br.readLine()) != null) 
    		{
    			if(s.length()>100)s=s.substring(0,80)+"[cut for readability]";
    			stringBuilder.append(s).append("<br>");
    		}
    		fr.close(); 
    	}
    	catch(Exception ex)
    	{
    		stringBuilder.append("<p style='color:red;'>Error trying to read the system configuration file.</p>");
    	}
    	stringBuilder.append("</div>");
    	
        stringBuilder.append("<td valign='top'>");
        stringBuilder.append("<form name='adminlogin' action='dologin' method='post' style='font-family:Verdana;font-size:8pt;'>");
        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
        stringBuilder.append("<tr><td>Username<td><input type='text' name='username' size='10'><br>");
        stringBuilder.append("<tr><td>Password<td><input type='password' name='password' size='10'><br>");
        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='login'>");
        stringBuilder.append("</table></form>");
        stringBuilder.append("</table>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
	
	@SuppressWarnings("unchecked")
	public Representation toJson() throws ResourceException
    {
        JSONObject obj = new JSONObject();
        obj.put("title", "Contrail VJSONObjectEP REST Interface");
        JSONArray arr = new JSONArray();
        arr.add("Hosts");
        arr.add("Registered OVFs");
        arr.add("VM Templates");
        arr.add("Virtual Machines");
        arr.add("Users");
        obj.put("target names", arr);
        JSONArray link = new JSONArray();
        link.add("/host/");
        link.add("/ovf/");
        link.add("/template/");
        link.add("/vm/");
        link.add("/user/");
        obj.put("links", link);
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
}
