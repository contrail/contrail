/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.GAFSClient;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Protocol;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

/**
 *
 * @author fgaudenz
 */
public class GAFSConnector {
    private JSONObject obj;
    
    public static final String NOT_IN_COUNTRY="1000.not.country";
    public static final String IN_COUNTRY="1000.country";
    private static final String CREATE="creation";
    private static final String POLICY="setPolicy";
    private static final String DELETE="delete";
    private String action;
    public GAFSConnector(JSONObject obj,String action){
        this.action=action;
        this.obj=obj;
    }
    
    private static JSONObject initJSON(String method){
        JSONObject json=new JSONObject();
         json.put("method", method);
         json.put("jsonrpc", "2.0");
         json.put("id","1");
         return json;
    }
     public static GAFSConnector createVolume(String name,String uuid){
         JSONObject json=initJSON("createVolume");
         
         
         JSONObject params=new JSONObject();
         params.put("volume_name", name);
         params.put("owner",uuid);
         params.put("owner_groupname",uuid);
         params.put("mode", "0777");
         json.put("params", params);
         
         
         
         return new GAFSConnector(json,GAFSConnector.CREATE);
     }
     
     
     public static GAFSConnector deleteVolume(String name,String policy){
         JSONObject json=initJSON("deleteVolume");
         
         
         JSONObject params=new JSONObject();
         params.put("volume_name", name);
         json.put("params", params);
         
         
         
         return new GAFSConnector(json,GAFSConnector.DELETE); 
     }
     
     
     
     
     public static GAFSConnector setVolumePrivilegies(String name,String user,String accessRight){
//         { "method":"setACL", "params":{"volume_name":"policy_test_volume","user_name":"patrick","user_accessrights":"xrw"}, "jsonrpc":"2.0", "id":"1" }
          JSONObject json=initJSON("setACL");  
          JSONObject params=new JSONObject();
         params.put("volume_name", name);
         params.put("user_name", user);
         params.put("user_accessrights", accessRight);
         json.put("params", params);
         return new GAFSConnector(json,""); 
     }
     
     public static GAFSConnector setPolicies(String volumeName,String constraint,String parameter){
         if(((volumeName==null)||(constraint==null))||(parameter==null)){
             return null;
         }else{
             if(constraint.equalsIgnoreCase(GAFSConnector.NOT_IN_COUNTRY)){
                 JSONObject json=initJSON("setPolicyAttribute");
                 JSONObject params=new JSONObject();
                 params.put("volume_name", volumeName);
                 params.put("attribute_name","1000.not.country");
                 params.put("attribute_value", parameter);
                 json.put("params", params);
                 return new GAFSConnector(json,GAFSConnector.POLICY);
             }else{
                 if(constraint.equalsIgnoreCase(GAFSConnector.IN_COUNTRY)){
                    JSONObject json=initJSON("setPolicyAttribute");
                 JSONObject params=new JSONObject();
                 params.put("volume_name", volumeName);
                 params.put("attribute_name","1000.country");
                 params.put("attribute_value", parameter);
                 json.put("params", params);
                 return new GAFSConnector(json,GAFSConnector.POLICY);
                 }else{
                     return null;
                 }
                     
             }
                    
            
                 
         }
         
     }
     
     
     
     
     
    
    public boolean post(GAFSClient gfs){
        Client client = new Client(new Context(), Protocol.HTTP);
        ClientResource itemsResource = new ClientResource(
               gfs.getEndpoint());
        itemsResource.setNext(client);
        ClientResource itemResource = null;
        // Create a new item
        Logger logger;
        logger = Logger.getLogger("VEP.GAFSClient");
        logger.debug(obj.toJSONString());
        StringRepresentation stringRep = new StringRepresentation(obj.toJSONString());
        stringRep.setMediaType(MediaType.APPLICATION_JSON);
        Representation r = itemsResource.post(stringRep);
        //Representation r = itemsResource.get();
        if (itemsResource.getStatus().isSuccess()) {
            try {
                String result = r.getText();
                System.out.println(result);
                return check(result);
                //itemResource = new ClientResource(r.getIdentifier());
            } catch (IOException ex) {
                System.out.println(ex);
                return false;
            }
            
    }
        return false;
    }

    private boolean check(String response) {
       JSONObject jObj = (JSONObject) JSONValue.parse(response);
         if(jObj.containsKey("error")){
        return false;
         }else{
             if((this.action.equalsIgnoreCase(GAFSConnector.CREATE))||(this.action.equalsIgnoreCase(GAFSConnector.DELETE))){
                 return true;
             }else{
                jObj=(JSONObject) jObj.get("result"); 
                int osd=((Long)jObj.get("usable_osds")).intValue();
                 if(osd>0)
                     return true;
                 else
                     return false;
             }
             
         }
    }
}
