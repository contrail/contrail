/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import org.ow2.contrail.provider.vep.html.HttpVepServer;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.bridge.SLF4JBridgeHandler;

public class Start 
{
	private static String vepProperties;
    private static String logPropFile;
    private static int logLevel;
    private static RestServer rServer;
    private static ImageStatusUpdate imageUpdater;
//    private static VmStatusUpdate vmUpdater;
    private static int restPortValue;
    private static int restHTTPSPortValue;
    private static Connection dbHandle;
    private static DBHandler dbHandler;
    private static Logger rootlogger;
    private static Logger logger;
    private static String dbType;
    private static String dbName;
    private static VEPHelperMethods helperMethods;
    private static VEPAccessControl vepAccessControl;
    private static SSLCertHandler certHandler;
    private static VEPShutdownHook vepCleaner;
    private static boolean noTermOut;
    
	public static void startVEP(String propfile, String logpropfile, int loglevel, boolean noTerm, boolean initdb)
	{
		//////////////initializing the system properties////////////////////////
		vepProperties = propfile;
        logPropFile = logpropfile;
        logLevel = loglevel;
        noTermOut = noTerm;
        helperMethods = new VEPHelperMethods(vepProperties);
        vepCleaner = new VEPShutdownHook();
    	vepCleaner.attachShutdownHook();
        /////////////////////////Initializing the logger////////////////////////
        java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        rootLogger.removeHandler(handlers[0]);
        SLF4JBridgeHandler.install();
        ////////////////////////////////////////////////////////////////////////
        Properties log4j = new Properties();
       
        
        try
        {
            FileOutputStream fos = null;
            String logFile = "";
            String logSize = "";
            try
            {
                logFile = VEPHelperMethods.getProperty("veplog.file", logger);
                logSize = VEPHelperMethods.getProperty("veplog.size", logger);
            }
            catch(Exception ex)
            {
                //unable to read the log file values from system properties file
                //using default
                System.err.println("Unable to read log file settings from system properties file. using default values: vep.log and 1024 KB");
                System.err.println(ex.getMessage());
                logFile = "vep.log";
                logSize = "1024";
            }
            int LogSize = 0;
            boolean logFilePresent = false;
            if(logFile!=null && logFile.trim().length() > 0)
            {
                logFile = logFile.trim();
                if(logSize != null && logSize.trim().length() > 0)
                {
                    try
                    {
                        LogSize = Integer.parseInt(logSize);
                    }
                    catch(NumberFormatException ex)
                    {
                        System.err.println(ex.getMessage());
                        LogSize = 1024;
                    }
                }
                //LogSize is in KB
                logFilePresent = true;
            }
            
            String logPart = "";
            switch(logLevel)
            {
                case 0:
                    logPart = "OFF";
                    break;
                case 1:
                    logPart = "FATAL";
                    break;
                case 2:
                    logPart = "ERROR";
                    break;
                case 3:
                    logPart = "WARN";
                    break;
                case 4:
                    logPart = "INFO";
                    break;
                case 5:
                    logPart = "DEBUG";
                    break;
                default:
                    logPart = "TRACE";
                    break;
            }
            
            if(logFilePresent)
            {
                if(!noTermOut)
                    log4j.setProperty("log4j.rootLogger", logPart + ", stdout, R");
                else
                    log4j.setProperty("log4j.rootLogger", logPart + ", R");
            }
            else
            {
                log4j.setProperty("log4j.rootLogger", logPart + ", stdout");
            }
            log4j.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
            log4j.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
            log4j.setProperty("log4j.appender.stdout.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            if(logFilePresent)
            {
                log4j.setProperty("log4j.appender.R", "org.apache.log4j.RollingFileAppender");
                log4j.setProperty("log4j.appender.R.File", logFile);
                log4j.setProperty("log4j.appender.R.MaxFileSize", Integer.toString(LogSize) + "KB");
                // Keep up to 10 backup file
                log4j.setProperty("log4j.appender.R.MaxBackupIndex", "10");
                log4j.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
                log4j.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            }
            log4j.store((fos = new FileOutputStream(logPropFile)), "Author: Piyush Harsh");
            fos.close();
        }
        catch(Exception ex)
        {
            System.out.println("Unable to save to log4j properties file: " + ex);
        }
        rootlogger = Logger.getLogger("VEP");
        switch(logLevel)
        {
            case 0:
                rootlogger.setLevel(Level.OFF);
                break;
            case 1:
                rootlogger.setLevel(Level.FATAL);
                break;
            case 2:
                rootlogger.setLevel(Level.ERROR);
                break;
            case 3:
                rootlogger.setLevel(Level.WARN);
                break;
            case 4:
                rootlogger.setLevel(Level.INFO);
                break;
            case 5:
                rootlogger.setLevel(Level.DEBUG);
                break;
            default:
                rootlogger.setLevel(Level.TRACE);
                break;
        }
        //BasicConfigurator.configure();
        PropertyConfigurator.configure(logPropFile);
        SLF4JBridgeHandler.install();
        logger = Logger.getLogger("VEP.start");
        /////////////////////Logger initialization done/////////////////////////
        vepAccessControl = new VEPAccessControl(); //just to execute the constructor
        //certHandler = new SSLCertHandler();
        initializeDBHandler();
        logger.debug("test init db");
        if(initdb) VEPHelperMethods.initializeDB();
        startRestServer();
        startImageStatusUpdater();
        //startVmStatusUpdater();
        //System.exit(0);
         try {
           
            HttpVepServer httpd=new HttpVepServer(VEPHelperMethods.getProperty("webuser-interface.port", logger),VEPHelperMethods.getProperty("webuser-interface.path", logger),initdb);
            if(initdb){ logger.info("VEP initialized - Please  Restart it");
            System.exit(0);
            }
            
            logger.info("WEBSERVER STARTED:"+VEPHelperMethods.getProperty("webuser-interface.port", logger) + VEPHelperMethods.getProperty("webuser-interface.path", logger));
                    } catch (IOException ex) {
           
        }        
	}
	
	private static void initializeDBHandler()
	{
		try 
        {
            logger.trace("Trying to determine which database driver to use.");
            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
            logger.trace("Found database choice: " + dbType);
            if(dbType.contains("sqlite"))
                Class.forName("org.sqlite.JDBC");
            else
            {
                Class.forName("org.gjt.mm.mysql.Driver");
                dbName = VEPHelperMethods.getProperty("mysql.dbname", logger);
            }
            if(dbType.contains("sqlite"))
                logger.trace("jdbc:sqlite drivers loaded successfully.");
            else
                logger.trace("jdbc:mysql drivers loaded successfully.");
            
            if(dbType.contains("sqlite"))
            {
                logger.trace("Trying to read database file name.");
                String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger);
                if(dbFile != null && dbFile.length() > 3)
                {
                    try
                    {
                        dbHandle = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
                        logger.trace("VEP jdbc:sqlite connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        //testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("DB connection could not be established for the specified DB file.");
                        //if(logger.isDebugEnabled())
                        //	sqlEx.printStackTrace(System.err);
                        logger.debug("Exception Caught: ", sqlEx);
                    }
                }
                else
                {
                    logger.warn("Database file is not specified. Check system settings and try again.");
                }
            }
            else
            {
                logger.trace("Trying to open mysql connection.");
                String mysqlUser = VEPHelperMethods.getProperty("mysql.user", logger);
                String mysqlPass = VEPHelperMethods.getProperty("mysql.pass", logger);
                String mysqlIP = VEPHelperMethods.getProperty("mysql.ip", logger);
                String mysqlPort = VEPHelperMethods.getProperty("mysql.port", logger);
                if(mysqlUser != null && mysqlPass != null)
                {
                    try
                    {
                        dbHandle = DriverManager.getConnection("jdbc:mysql://" + mysqlIP + ":" + mysqlPort + "/" + dbName + "?" + "user=" + mysqlUser + "&password=" + mysqlPass);
                        logger.trace("VEP jdbc:mysql connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        //testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("Mysql DB connection could not be established for the specified mysql parameters.");
                        //if(logger.isDebugEnabled())
                        //	sqlEx.printStackTrace(System.err);
                        logger.debug("Exception Caught: ", sqlEx);
                    }
                }
                else
                {
                    logger.warn("Mysql connection parameters not specified. Check system settings and try again.");
                }
            }
        } 
        catch (ClassNotFoundException ex) 
        {
            logger.fatal("Missing mysql or sqlite library ... could not load necessary drivers.");
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        if(dbHandle != null)
        {
            dbHandler.dbHandle = dbHandle;
            dbHandler = new DBHandler("VEP Start", dbType);
            logger.trace("DB handler created successfully using jdbc:sqlite connection object.");
        }
        else
        {
            logger.warn("VEP DB file has not been specified. Check system settings and try again.");
        }
        ///////////////////VEP database handle initialization done//////////////
	}
	
	public static void startImageStatusUpdater()
	{
		if(imageUpdater == null)
		{
			imageUpdater = new ImageStatusUpdate("IaaSOSImageStatusUpdate", 30000);
			imageUpdater.start();
			if(imageUpdater != null && imageUpdater.getRunningState())
				  VEPHelperMethods.addService("imagecheck", imageUpdater);
		}
	}
        
        
        
        
//        public static void startVmStatusUpdater()
//	{
//		if(vmUpdater == null)
//		{
//			vmUpdater = new VmStatusUpdate("IaaSOSImageStatusUpdate", 30000);
//			vmUpdater.start();
//			if(vmUpdater != null && vmUpdater.getRunningState())
//				  VEPHelperMethods.addService("vmcheck", vmUpdater);
//		}
//	}
	
	public static void startRestServer()
	{
		///////////////////VEP REST Service Start///////////////////////////////
		if(rServer == null)
		{
		   //Get the properties from the properties file
		
		  String restPortString = VEPHelperMethods.getProperty("rest.restHTTPPort", logger);
		  boolean http = true;
		  boolean https = true;
		  if(restPortString == null || restPortString.equals(""))
		  {
		      logger.warn("Empty REST HTTP port value specified, using default port 10500.");
		      restPortValue = 10500; //using default
		  } 
		  else if(restPortString.equals("-1"))
		  {
		      logger.warn("-1 provided for HTTP Port, disabling HTTP Rest.");
		      restPortValue = -1;
		  }
		  else 
		  {
		      try 
		      {
		          restPortValue = Integer.parseInt(restPortString);
		      }
		      catch(Exception e)
		      {
		          logger.error("Property rest.restHTTPPort could not be read, disabling rest Server. Please correct this value or provide none to use the default value");
		          http = false;
		      }
		  }
		
		  String restHTTPSPortString = VEPHelperMethods.getProperty("rest.restHTTPSPort", logger);
		  if(restHTTPSPortString == null || restHTTPSPortString.equals("") || restHTTPSPortString.equals("-1"))
		  {
		      logger.error("Empty or -1 provided for HTTPSPort, disabling https");
		      restHTTPSPortValue = -1;
		  }
		  else
		  {
		      try
		      {
		          restHTTPSPortValue = Integer.parseInt(restHTTPSPortString);
		      }
		      catch(Exception e)
		      {
		          logger.error("Property rest.restHTTPSPort could not be read, disabling rest Server. Please correct this value or provide none to disable HTTPS");
		          https = false;
		      }
		  }
		
		  String keyStorePath = VEPHelperMethods.getProperty("rest.keystore", logger);
		  String keyStorePass = VEPHelperMethods.getProperty("rest.keystorepass", logger);
		  String keyPass = VEPHelperMethods.getProperty("rest.keypass", logger);
		
		  if(http && https)
		  {
		      boolean clientAuthSelected = Boolean.parseBoolean(VEPHelperMethods.getProperty("rest.clientAuthSelected", logger));
		
		      if(clientAuthSelected)
		          rServer = new RestServer(restPortValue, restHTTPSPortValue, true, keyStorePath, keyStorePass, keyPass);
		      else
		          rServer = new RestServer(restPortValue, restHTTPSPortValue, false, keyStorePath, keyStorePass, keyPass);
		      logger.trace("Starting REST server.");
		      rServer.start();
		      if(restPortValue != -1)
		      {
		          logger.info("Started REST server at HTTP port " + restPortValue + " HTTPS port " + restHTTPSPortValue + ".");
		      }
		      else
		      {
		          logger.info("Started REST server at HTTPS port " + restHTTPSPortValue + "."); 
		      }
		  }
		  else
		  {
		      logger.warn("REST Server could not be started because of incorrect values for HTTP or HTTPS port");
		  }
		  //introducing a delay of 500 msec
		  try
		  {
		      Thread.sleep(500);
		  }
		  catch(Exception ex)
		  {
		      //ignore this exception
		  }
		  if(rServer != null && rServer.getState())
			  VEPHelperMethods.addService("rest", rServer);
		}
		else
		{
			logger.info("Stopping REST services now.");
			try
			{
				rServer.stop();
			}
			catch(Exception ex)
			{
				//if(logger.isDebugEnabled())
				//	ex.printStackTrace(System.err);
				logger.debug("Exception Caught: ", ex);
			}
			rServer = null;
			startRestServer();
		}
		/////////////////VEP REST Service started//////////////////////////
	}
}
