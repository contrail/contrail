/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VMHandlerCollection extends VEPCollection {
	
	private String ceeId;
	private ArrayList<VMHandler> vmhs = new ArrayList<VMHandler>();
	
	public VMHandlerCollection(String ceeId)
	{
		super("VMHandlerCollection", true);
		this.setId("/cee/"+ceeId+"/VMHandlers");
		this.setResourceUri("VEP/VMHandlerCollection");
		this.ceeId = ceeId;
	}
	
	public VMHandlerCollection() {
		super("VMHandlerCollection", true);
		this.setId("/VMHandlers");
		this.setResourceUri("VEP/VMHandlerCollection");
	}
	
	public void retrieveVMHandlerCollection() throws SQLException
	{
		ResultSet rs;
		if(ceeId != null)
		{
			logger.debug("Retrieving VM Handlers for cee " + ceeId);
			rs = db.query("select", "vmhandler.id as vid, vmhandler.name as vname", "ceevmhandlers,vmhandler", "where ceevmhandlers.ceeid = " + ceeId + " and vmhandler.id=ceevmhandlers.vmhandlerid");
			while (rs.next()){
				VMHandler v = new VMHandler(false);
				v.setName(rs.getString("vname"));
				v.setId("/VMHandler/" + rs.getInt("vid"));
				vmhs.add(v);
				this.incrementCount();
			}
		} else {
			logger.debug("Retrieving VM Handlers");
			rs = db.query("select", "id, name", "vmhandler", "");
			while (rs.next()){
				VMHandler v = new VMHandler(false);
				v.setName(rs.getString("name"));
				v.setId("/VMHandler/" + rs.getInt("id"));
				vmhs.add(v);
				this.incrementCount();
			}
		}
		int k;
	}
	


	public ArrayList<VMHandler> getVmhs() {
		return vmhs;
	}
}
