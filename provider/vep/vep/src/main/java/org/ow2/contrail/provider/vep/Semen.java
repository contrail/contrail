/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;

public class Semen extends ServerResource
{
	@Get
	public Representation getValue() throws ResourceException
	{
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml();
            else if(acceptType.contains("json"))
            {
                //response = toJson();
            }
        }
        else
        {
            //default rendering ...
            response = toHtml();
        }
        return response;
	}
	
	public Representation toHtml() throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
       /* stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>");
        stringBuilder.append("Welcome to Contrail Virtual Execution Platform access point. Before you can proceed, you must login.<br>");
        stringBuilder.append("<form name='login-form' method='post' action='./dologin' style='font-family:Verdana;font-size:9pt;'>");
        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>").append("<tr><td>Username<td><input name='username' type='text' size='20'>")
        	.append("<tr><td>Password<td><input name='password' type='password' size='20'><tr><td><td align='right'><input type='submit' value='login'></form>")
        	.append("</table>If you do not have an account, you can create one <a href='./newaccount'>here</a>.");
        stringBuilder.append("</div>");*/
        String str="";
        try {
            str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/index.html"));
        } catch (IOException ex) {
            
        }
        
        
        //tringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(str, MediaType.TEXT_HTML);
        return value;
    }
}
