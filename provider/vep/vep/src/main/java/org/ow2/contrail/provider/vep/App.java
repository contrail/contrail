/*Copyright (c) 2014, Inria. All rights reserved.
 
 This file is part of the Contrail system see <http://contrail-project.eu/>
 for more details. The Contrail project has been developed with the
 financial support of the European Commission's ICT FP7 program
 under Grant Agreement # 257438.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 -         Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 -        Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 -        Neither the name of Inria, nor the names of its contributors nor
 the name of the Contrail project may be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Inria, BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY,  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.ow2.contrail.provider.vep;

/**
 * This is the main execution point for vep, it starts the management services
 *
 */

import org.ow2.contrail.provider.vep.SchedulerClient.SchedulerSync;
import org.ow2.contrail.provider.vep.html.HttpVepServer;
import org.apache.commons.cli.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App 
{
	private static BasicParser parser;
    private static CommandLine cli;
    private static HelpFormatter helpFormatter;
    
    private static String propFile;
    private static String logPropFile;
    private static boolean helpShown;
    private static boolean vepPropProvided;
    private static boolean loggerPropProvided;
    private static int logLevel;
    private static boolean noTermOut;
    
    public static final int OPENNEBULA3_HOST = 1;
    public static final int OPENSTACK_HOST = 2;
    
    public static final int LOCAL_DISK_STORAGE = 1;
    public static final int NFS_SHARE = 2;
    public static final int GAFS_SHARE = 3;
    
	public static final int VMCONSTRAINT = 1;
	public static final int STORAGECONSTRAINT = 2;
	public static final int GENERALCONSTRAINT = 3;
    
    
    private static boolean synscheduler = false;
    private static boolean initdb = false;
    
    public static void main(String[] args)
    {
        initdb=false;
    	String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator") + 
    			".vep" + System.getProperty("file.separator");
        //String VEPrunningDir= System.getProperty("user.dir");
        
    	File dir = new File(defaultPath);
    	System.out.println("Trying to locate the VEP system folder ...");
    	if(!dir.exists())
    	{
    		System.out.print(" ... VEP configuration directory not found. Creating one now: ");
    		boolean result = dir.mkdir();  
    	    if(result)
    	    {    
    	       System.out.println("done.");  
    	    }
    	    else
    	    {
    	    	System.out.println("error! You may experience problem with VEP module.");  
    	    }
    	}
    	else
    	{
    		System.out.println(" ... VEP system folder located!");
    	}
    	
    	Options options = new Options();
        options.addOption("p", "vep-properties", true, "path to the VEP properties file");
        options.addOption("l", "log-properties", true, "path to the VEP logger properties file");
        options.addOption("h", "help", false, "usage help");
        options.addOption("d", "default", false, "start with a default VEP properties file, you must change the " +
        		"defaults to the right values manually");
        options.addOption("v", "log-level", true, "log verbosity level, (0 = off, 1 = fatal, 2 = error, 3 = warn, " +
        		"4 = info, 5 = debug, 6 = everything)");
        options.addOption("s", "supress-term-log", false, "supress logger output to terminal");
        options.addOption("u", "sync-scheduler", false, "synchronize scheduler");
        //options.addOption("db"," sqlite-db-path",false,"run vep using the sqlite db provided");
    	if(args.length > 0)
    	{
            parser = new BasicParser();
            try
            {
                cli = parser.parse(options, args);
                if(cli.hasOption('u')){
                	synscheduler = true;
                }
                if(cli.hasOption('p'))
                {
                	propFile = cli.getOptionValue('p');
                    vepPropProvided = true;
                }
                
                if(cli.hasOption('s'))
                {
                    noTermOut = true;
                }
                
                if(cli.hasOption('l'))
                {
                    logPropFile = cli.getOptionValue('l');
                    loggerPropProvided = true;
                }
                
                if(cli.hasOption('h'))
                {
                    helpFormatter = new HelpFormatter();
                    helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar vep-0.0.1-SNAPSHOT.jar", 
                    		"Contrail Virtual Execution Platform Module", options, 3, 5, "", true);
                    helpShown = true;
                }
                
                if(cli.hasOption('d'))
                {
                     initdb= true;
                    Properties props = new Properties();
                    System.out.println("Creating the default vep.properties file at " + defaultPath + "vep.properties");
                    FileOutputStream fos = null;
                    String dbFile = defaultPath+"vep.db";
                    String logFile = "vep.log";
                    String size = "1024";
                    String restkeyFile = defaultPath+"VEPRestKeyStore.jks";
                    String restStorePass = "pass1234";
                    String restKeyPass = "pass1234";
                    String dbType = "sqlite";
                    String mySqlIP = "127.0.0.1";
                    String mySqlPort = "3306";
                    String mySqlUser = "vepuser";
                    String mySqlPass = "contrail";
                    String restHTTPPort = "10500";
                    String restHTTPSPort = "8183";
                    String scratchFolder = "/tmp/";
               /*    String copyServerIp = "127.0.0.1";
                    String copyServerPort = "10556";
                    String copyServerAdmin = "admin";
                    String copyServerPassword = "pass1234";*/
                    props.setProperty("vepdb.choice", dbType);
                    props.setProperty("sqlite.db", dbFile);
                    props.setProperty("mysql.ip", mySqlIP);
                    props.setProperty("mysql.port", mySqlPort);
                    props.setProperty("mysql.user", mySqlUser);
                    props.setProperty("mysql.pass", mySqlPass);
                    props.setProperty("mysql.dbname", "vepdb2");
                    props.setProperty("veplog.file", logFile);
                    props.setProperty("veplog.size", size);
                    props.setProperty("rest.restHTTPPort", restHTTPPort);
                    props.setProperty("rest.restHTTPSPort", restHTTPSPort);
                    props.setProperty("rest.keystore", restkeyFile);
                    props.setProperty("rest.keystorepass", restStorePass);
                    props.setProperty("rest.keypass", restKeyPass);
                    props.setProperty("cli.port", "10555");
                    props.setProperty("pdp.use", "false");
                 //   props.setProperty("pdp.endpoint", "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap");
                    props.setProperty("caservice.uri", "https://one-test.contrail.rl.ac.uk:8443/ca/delegateduser");
                    props.setProperty("caservice.storepass", "changeme");
                    props.setProperty("caservice.keystore", "");
                    props.setProperty("vep.scratch", scratchFolder);
                    props.setProperty("user.timeout", "600");	//default inactive period = 10 minutes
                  //  props.setProperty("copyserver.ip", copyServerIp);
                  //  props.setProperty("copyserver.port", copyServerPort);
                  //  props.setProperty("copyserver.admin", copyServerAdmin);
                  //  props.setProperty("copyserver.password", copyServerPassword);
                    props.setProperty("vep.share","/tmp/");
                    props.setProperty("scheduler.url", "http://127.0.0.1");
                    props.setProperty("scheduler.port","8080");
                    props.setProperty("webuser-interface.port","8000");
                    props.setProperty("webuser-interface.path", defaultPath+"webuserInterface");
                    props.setProperty("webuser-interface.defaultHost", "127.0.0.1");
                    props.setProperty("imageRepositoryDefault.path","/opt/images/");
                    props.setProperty("\\#authzserver.authorizationEndpointUri","https://contrail.xlab.si:8443/oauth-as/r/access_token/request");
                    props.setProperty("\\#authzserver.keystoreFile","oauth-java-vepclient1.jks");
                    props.setProperty("\\#authzserver.keystorePass","contrail");
                    props.setProperty("\\#authzserver.truststoreFile","cacerts.jks");
                    props.setProperty("\\#authzserver.truststorePass","contrail");
                    props.setProperty("\\#authzserver.cliendId","oauth-java-vepclient1");
                    props.setProperty("\\#authzserver.clientSecret","somesecret");
                    props.setProperty("\\#authzserver.userCertEndpoint","https://contrail.xlab.si:8443/ca/o/delegateduser");
                    props.setProperty("\\#vm.insertadminkey", "false");
                    props.setProperty("\\#vm.adminkeyname", "");
                    props.setProperty("\\#vm.adminkeyvalue", "");

                    try
                    {
                        props.store((fos = new FileOutputStream(defaultPath + "vep.properties")), "Author: VEP Team");
                        fos.close();
                        System.out.println("Created the vep.properties file.");
                       
                        logLevel = 6;
                        
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                        System.out.println("Exception caught while creating the vep properties file at " + defaultPath + 
                        		"vep.properties");
                        System.out.println("Exception: " + ex.getMessage());
                    }
                }
                
                if(cli.hasOption('v'))
                {
                    String loglevel = cli.getOptionValue('v');
                    try
                    {
                        logLevel = Integer.parseInt(loglevel);
                    }
                    catch(Exception ex)
                    {
                        logLevel = 6;
                    }
                }
                
                
                if(vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    logPropFile = defaultPath + "log4j.properties";
                    System.out.println("VEP logger properties file path was not specified, " +
                    		"using the default values for vep logger properties file: " + logPropFile);
                }
                if(!vepPropProvided && loggerPropProvided && !helpShown)
                {
                    propFile = defaultPath + "vep.properties";
                    System.out.println("VEP system properties file path was not specified, using the default " +
                    		"values for vep properties file: " + propFile);
                }
                if(!vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    logPropFile = defaultPath + "log4j.properties";
                    propFile = defaultPath + "vep.properties";
                    System.out.println("VEP system properties file path was not specified, using the default " +
                    		"values for vep properties (" + propFile + 
                    ") and vep logger properties files (" + logPropFile + ").");
                }
            }
            catch(ParseException ex)
            {
                helpFormatter = new HelpFormatter();
                helpFormatter.printUsage(new PrintWriter(System.out, true), 80, "Contrail Virtual Execution Platform " +
                		"Module", options);
            }
    	}
    	else
    	{
    		helpFormatter = new HelpFormatter();
    		System.out.println("No runtime option was found, the runtime options are shown below.");
            helpFormatter.printUsage(new PrintWriter(System.out, true), 80, "java -jar vep-0.0.1-SNAPSHOT.jar", options);
            //assuming the default files
            logPropFile = defaultPath + "log4j.properties";
            propFile = defaultPath + "vep.properties";
            System.out.println("VEP system properties file path was not specified, using the default values for " +
            		"vep properties (" + propFile + ") and vep logger properties files (" + logPropFile + ").");
            logLevel = 6; //using a default log level set to error mode
    	}
        if(!helpShown)
    	launchApp();
    }
    
    private static void launchApp()
    {
    	Start.startVEP(propFile, logPropFile, logLevel, noTermOut,initdb);
    	if(synscheduler)
    	{
    		SchedulerSync.syncScheduler();
                System.exit(0);
    	}
    }
}
