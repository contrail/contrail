#!/bin/bash
installation_dir=$(pwd)
private_keyInst="${installation_dir}/veplocal.key"
public_keyInst="${installation_dir}/veplocal.crt"
running_dirInst="${installation_dir}/vep/"
capath="${installation_dir}/ca.crt"
capassword="changeit"
whichjava=$(which java)
javahome=$(echo $(readlink -f /usr/bin/java | sed "s:bin/java::"))

scheduler_dump="${installation_dir}/scheduler.txt"





echo -e "------------------------------------------------------------------------------\n \n Please enter the mysql root password:"
read mysqlpassword
mysql -u root -p$mysqlpassword -e "create database schedulerEmpty"
mysql -u root -p$mysqlpassword -e "CREATE USER 'scheduler'@'localhost' IDENTIFIED BY 'password'";
mysql -u root -p$mysqlpassword -e "grant all privileges on schedulerEmpty.* to 'scheduler'@'localhost' identified by  'password'"
mysql -u root -p$mysqlpassword schedulerEmpty < $scheduler_dump







java -jar target/vep-2.2-SNAPSHOT-jar-with-dependencies.jar -d









while [ $# -gt 0 ]
do
case "$1" in 
        -d | --database )      
                                db="$2"
                                shift 2
                                ;;

        -s | --private_key )  
                                private_key+="$2"
                                shift 2
                                ;;
        -p  | --public_key )
                                public_key="$2"
                                shift 2
                                ;;
        -c  | --cacert )
                                public_ca="$2"
                                shift 2
                                ;;
        -k | --ca_key)          private_ca="$2"
                                shift 2
	                        ;;

        *) echo "error"
           exit -1
           ;;
    esac
done



if [ -n "$private_key" ]; then
private_keyInst=$private_key
fi
if [ -n "$public_key" ]; then
public_keyInst=$public_key
fi
if [ -n "$public_ca" ]; then
capath=$public_ca
fi
if [ -n "$db" ]; then
cp $(db) ./vep/
fi


sudo keytool -import -alias vepCA -file $capath -keystore $javahome/lib/security/cacerts -storepass $capassword -noprompt
sudo openssl pkcs12 -export -inkey $private_keyInst -in $public_keyInst -out vep.pfx -name default -password pass:pass1234
sudo keytool -importkeystore -srckeystore vep.pfx -srcstoretype pkcs12 -destkeystore VEPRestKeyStore.jks -srcalias default -destalias vep_server -destalias vep_server -destkeypass pass1234 -srckeypass pass1234 -deststorepass pass1234 -srcstorepass pass1234 -noprompt

mv VEPRestKeyStore.jks /root/.vep/
rm vep.pfx

