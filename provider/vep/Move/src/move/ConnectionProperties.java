/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package move;

import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class ConnectionProperties {

   
    private int port;
    private String[] user;
    private String[] psw;
    private String tempMem;
    private String proxyURL,proxyPort,proxyUsr,proxyPass;
    private Logger logger;
    public String getTempMem() {
        return tempMem;
    }
    
    private Properties pr;
    public ConnectionProperties(String name){
        logger = Logger.getLogger("MoveFileServerVEP.properties");
        
        
        this.pr=PropertyLoader.loadProperties(name);
        this.port=Integer.parseInt(pr.getProperty("port"));
        String users=pr.getProperty("user");
        this.user=users.split(" ");
        String psws=pr.getProperty("password");
        this.psw=psws.split(" ");
        this.tempMem=pr.getProperty("temporary_memory");
        this.proxyPort=pr.getProperty("proxy_port");
        this.proxyURL=pr.getProperty("proxy_url");
        this.proxyUsr=pr.getProperty("proxy_user");
        this.proxyPass=pr.getProperty("proxy_pass");
        
      if((this.proxyURL!=null)&&(this.proxyPort!=null)){
         logger.info("Using proxy="+this.proxyURL+":"+this.proxyPort);    
         System.getProperties().put("http.proxyHost", this.proxyURL);
         System.getProperties().put("http.proxyPort", this.proxyPort);
         
         if((this.proxyUsr!=null)&&(this.proxyPass!=null)){
            System.getProperties().put("http.proxyUser", this.proxyUsr);
            System.getProperties().put("http.proxyPassword", this.proxyPass); 
         }
    }
    }
    
     public int getPort() {
        return port;
    }
     
    public boolean checkPassword(String userRequest,String pwdRequest) {
        for(int i=0;i<user.length;i++){
            if(userRequest.compareTo(user[i])==0)
                if(pwdRequest.compareTo(this.psw[i])==0)
                    return true;
                else
                    return false;
        }
        return false;
    }

    public boolean checkUser(String userRequest) {
        for(int i=0;i<user.length;i++){
            if(userRequest.compareTo(user[i])==0)
                return true;
        }
        return false;
    }

    String getCertPath() {
        return pr.getProperty("destination.certificates");
    }
    
}
