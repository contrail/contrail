/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package move;

import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;

 /*
 * @author fgaudenz
 */
public class ONEImageServer implements Runnable {
    private String TEMPORARY_MEMORY;
    private int port;
    private boolean continueFlag;
    private Thread t;
    private ServerSocket server;
    private Socket client;
    private PrintWriter out;
    private BufferedReader in;
    private String NFS;
    private String ONEIMAGEPATH;
    private ConnectionProperties connPP;
    private  Logger logger;
    private ServerSocket stream;
    public ONEImageServer(String fileProperties,Socket sock,ServerSocket sockStream) 
    {
        
        logger = Logger.getLogger("MoveFileServerVEP.ONEImageServer-"+this);
        
        this.stream=sockStream;
        connPP=new ConnectionProperties(fileProperties);
        //set port
        port = connPP.getPort();
        TEMPORARY_MEMORY=connPP.getTempMem();
        continueFlag = true;
       
       
            client=sock;
            t = null;
           
    }

    public static void startCLIserver(String fileProperties) {
        Logger logger=Logger.getLogger("MoveFileServerVEP.StartServer");
        try {
            ConnectionProperties connPP=new ConnectionProperties(fileProperties);
            //set port
            int port = connPP.getPort();
            
           
            ServerSocket ssock = new ServerSocket(port);
            ServerSocket streamssock=new ServerSocket(port+1);
            logger.info("Listening");
            while (true) {
                Socket sock = null;
                Socket streamSock=null;
                try {
                    sock = ssock.accept();
                } catch (IOException ex) {
                    logger.error("Address already in use");
                }
                logger.info("Connected");
                new Thread(new ONEImageServer(fileProperties,sock,streamssock)).start();
                }
        } catch (IOException ex) {
           logger.error("ERROR STARTING THE SERVER");
        }
    }

    public void stopCLIserver() {
        continueFlag = false;
        //just send a dummy connect if server is blocking on client incoming request
        try {
            if (client != null && client.isConnected()) {
                client.close();
            }
            
            //stop socket
            Socket sock = new Socket("127.0.0.1", port);
            sock.close();
            server.close();
            t.join(1000);
        } catch (Exception ex) {
            
        }
    }

    public void run() {
        logger.info("CLI Server has started ...");
        logger.info("client connection started");
        boolean forceQuit = false;
        while (!forceQuit) {
            try {
                String user = "";
                String passw = "";
                //boolean variable to check status of connection
                boolean usr = true;
                boolean pass = false;
                boolean auth = false;
               
                //set timeout conenction
                client.setSoTimeout(1000 * 60 * 20); //20 minutes idle time
                //stream to answer to the client
                out = new PrintWriter(client.getOutputStream(), true);
                //stream to read messages from client
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                //get first message
                String userInput = in.readLine();
                //"hello" is the first word
                if (userInput.compareTo("hello") != 0) {
                    forceQuit = true;

                } else {
                    //logger.info("hello");
                    //first answer
                    out.println("hello");
                }
                //get user message
                userInput = in.readLine();
                //connection loop
                while (client.isConnected() && !forceQuit && !client.isClosed()) {
                    if (usr) {
                        //username control
                        out.println("ok");
                        user = userInput;
                        usr = false;
                        pass = true;
                    } else if (pass) {
                        //password control
                        passw = userInput;
                        if (connPP.checkPassword(user, passw)) {
                            out.println("ok");
                            pass = false;
                            auth = true;
                        } else {
                            out.println("fail");
                            usr = true;
                            pass = false;
                        }

                    } else if (auth) {
                            
                        //authentication ok
                        //downloading step
                           
                        //copy to NFS
                        NFS = this.TEMPORARY_MEMORY;
                        String[] message = userInput.split(" ");
                        String path = message[0];
                        logger.info(message[0]);
                        logger.info(message[1]);
                        String[] fileName = path.split("=");
                        logger.info(userInput);
                        //get source
                        if (fileName[0].compareTo("SRC") == 0) {
                            path = fileName[1];
                        }
                        fileName = path.split("/");
                        ONEImageCopy oneimage = new ONEImageCopy(this.connPP);
                        String fileNFS = NFS + "/" + message[1].split("=")[1] + "_" + fileName[fileName.length - 1];
                        try {
                            
                            fileNFS=oneimage.move(path, fileNFS, this.stream);
                            //oneimage.move(path, fileNFS);
                            //return ok+path
                            //what's happen if the file is already in the NFS but it is different??
                            //duplicate
                            
                            out.println("ok PATH=" + fileNFS);
                            System.out.println("ok PATH=" + fileNFS);

                        } catch (MalformedURLException badsock) {
                            logger.error(badsock);
                            System.out.println("error");
                            forceQuit = true;
                            out.println("fail");
                           
                            client.close();
                        } catch (FileNotFoundException badFile) {
                            logger.error(badFile);
                             System.out.println("error");
                            forceQuit = true;
                            out.println("fail");
                           
                            client.close();
                        } catch (IOException notfound) {
                            logger.error(notfound);
                            forceQuit = true;
                            System.out.println("error");
                            out.println("fail");
                            client.close();
                        }

                        
                        //copy function
                        //
                        userInput = in.readLine();
                        path = userInput;
                        logger.info(userInput);
                        fileName = path.split("=");
                        if (fileName[0].compareTo("DST") == 0) {
                            path = fileName[1];
                        }
                        try {
                            if (path.compareTo("") != 0) {
                                fileNFS=oneimage.move(fileNFS, path,this.stream);
                            }
                            logger.info("FIle copied Information -> user:" + user + " tempFile:" + fileNFS + " destinationFile:" + path);
                            out.println("ok PATH="+fileNFS);
                            //logger.info("quit");
                            out.println("quit");
                        } catch (MalformedURLException badsock) {
                            logger.error(badsock);
                            forceQuit = true;
                            out.println("fail");
                            client.close();
                        } catch (FileNotFoundException badFile) {
                            logger.error(badFile);
                            forceQuit = true;
                            out.println("fail");
                            client.close();
                        }





                        //quit the connection
                        forceQuit = true;
                    }


                    if (!forceQuit) {
                        userInput = in.readLine();
                    }
                }
                logger.info("Connection with the client close");
                client.close();

            }catch(NullPointerException exNull){
          logger.error("connection close for an exception");      
  forceQuit=true;
      if (client != null && client.isConnected()) {
          
                    out.println("quit");
                    try {
                        client.close();
                    } catch (IOException ex) {
                       logger.error("connection close for an exception");      
                    }
 }
            } catch (ArrayIndexOutOfBoundsException exArray) {

              
                
                forceQuit=true;
                
                if (client != null && client.isConnected()) {
                    out.println("quit");
                    try {
                        client.close();
                    } catch (IOException ex) {
                       // Logger.getLogger(ONEImageServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            } catch (SocketException sockex) {
                logger.warn("CLI client read error, closing connection: " + sockex.getMessage());
                
                forceQuit=true;
                try {
                    if (client != null && client.isConnected()) {
                        out.println("quit");
                        client.close();
                    }
                } catch (Exception ex) {
                    //ignore this exception
                }
            } catch (SocketTimeoutException timeout) {
                logger.warn("connection timeout, closing connection: " + timeout.getMessage());
                
                forceQuit=true;
                
                try {
                    if (client != null && client.isConnected()) {
                        out.println("You have been inactive for more than 20 minutes. For security reasons this connection will be closed now.");
                        client.close();
                    }
                } catch (Exception ex) {
                    //ignore this exception
                }
            } catch (Exception ex) {
                
                forceQuit=true;
                
                logger.info("server encountered an exception: " + ex.getMessage());
                ex.printStackTrace(System.err);
                
                    if (client != null && client.isConnected()) {
                        out.println("quit");
                    try {
                        client.close();
                    } catch (IOException ex1) {
                       // Logger.getLogger(ONEImageServer.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    }
                
            }

        }
        try {
            client.close();
        } catch (IOException ex) {
            //Logger.getLogger(ONEImageServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        logger.info("connection closed");
    }
}
