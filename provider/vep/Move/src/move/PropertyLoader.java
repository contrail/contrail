package move;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class PropertyLoader
{
    /**
     * Looks up a resource named 'name' in the classpath. The resource must map
     * to a file with .properties extention. The name is assumed to be absolute
     * and can use either "/" or "." for package segment separation with an
     * optional leading "/" and optional ".properties" suffix. Thus, the
     * following names refer to the same resource:
     * <pre>
     * some.pkg.Resource
     * some.pkg.Resource.properties
     * some/pkg/Resource
     * some/pkg/Resource.properties
     * /some/pkg/Resource
     * /some/pkg/Resource.properties
     * </pre>
     * 
     * @param name classpath resource name [may not be null]
     * 
     * @return resource converted to java.util.Properties [may be null if the
     * resource was not found and THROW_ON_LOAD_FAILURE is false]
     * @throws IllegalArgumentException if the resource was not found and
     * THROW_ON_LOAD_FAILURE is true
     */
    public static Properties loadProperties (String name)
    {
        if (name == null)
            throw new IllegalArgumentException ("null input: name");
        
        
            
       /* if (name.endsWith (SUFFIX))
            throw new IllegalArgumentException (name+" input: name");
        */
        Properties result = new Properties();
 
    	try {
               //load a properties file
    		result.load(new FileInputStream(name));//check name
 
               //get the property value and print it out
 
        }catch (FileNotFoundException ef){
            try {
                File f=new File(name);
                f.createNewFile();
            } catch (IOException ex) {
            }
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
      
        
        return result;
    }
    
   
    private static final String SUFFIX = ".properties";
} // End of class