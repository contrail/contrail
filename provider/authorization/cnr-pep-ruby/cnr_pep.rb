#!/usr/bin/env ruby

# -------------------------------------------------------------------------- #
# CNR-PEP for OpenNebula. Aliaksandr Lazouski, Paolo Mori
#--------------------------------------------------------------------------- #

ONE_LOCATION=ENV["ONE_LOCATION"]

if !ONE_LOCATION
    RUBY_LIB_LOCATION="/usr/lib/one/ruby"
    ETC_LOCATION="/etc/one/"
else
    RUBY_LIB_LOCATION=ONE_LOCATION+"/lib/ruby"
    ETC_LOCATION=ONE_LOCATION+"/etc/"
end

$: << RUBY_LIB_LOCATION

require 'pp'

require 'rubygems'
require 'OpenNebulaDriver'
require 'simple_auth'
require 'simple_permissions'
require 'yaml'
require 'sequel'
require 'ssh_auth'

require 'OpenNebula'
require 'client_utilities'
require 'command_parse'
require 'soap/wsdlDriver'

class AuthorizationManager < OpenNebulaDriver
    def initialize
        super(15, true)
        
        config_data=File.read(ETC_LOCATION+'/auth/auth.conf')
        STDERR.puts(config_data)
        @config=YAML::load(config_data)
        
        STDERR.puts @config.inspect
        
        database_url=@config[:database]
        @db=Sequel.connect(database_url)
        
        # Get authentication driver
        begin
            driver_prefix=@config[:authentication].capitalize
            driver_name="#{driver_prefix}Auth"
            driver=Kernel.const_get(driver_name.to_sym)
            @authenticate=driver.new
            STDERR.puts "Using '#{driver_prefix}' driver for authentication"
        rescue
            STDERR.puts "Driver '#{driver_prefix}' not found, "<<
                "using SimpleAuth instead"
            @authenticate=SimpleAuth.new
        end

        # Get authorization driver        
        @authorize_xacml = SOAP::WSDLDriverFactory
        @is_xacml_pdp_available = false;
        
        @authorize_original = SimplePermissions.new(@db, OpenNebula::Client.new, @config)
      
        register_action(:AUTHENTICATE, method('action_authenticate'))
        register_action(:AUTHORIZE, method('action_authorize'))
    end
    
    def action_authenticate(request_id, user_id, user, password, token)
        auth=@authenticate.auth(user_id, user, password, token)
        if auth==true
            send_message('AUTHENTICATE', RESULT[:success],
                request_id, 'Successfully authenticated')
        else
            send_message('AUTHENTICATE', RESULT[:failure],
                request_id, auth)
        end
    end


    # check the xacml_pdp availability upon the first request
    # this is not done in constructor of the class due to One time contrains on the cunstructor creation
    def init_xacml_pdp
        # get authorization driver, set SOAP XACML PDP location. if not available, native One will be used
        begin
          @authorize_xacml = SOAP::WSDLDriverFactory.new(@config[:pdp].to_s + '?wsdl').create_rpc_driver
          @is_xacml_pdp_available = true
        rescue
          open('contrail-pep.log', 'a') { |f|
            f.puts "xacml-pdp is unavailable, opennebula pdp will be used"}
        end
    end

    # CNR-PEP
    def action_authorize(request_id, user_id, *tokens)
      response = false      
      if !@is_xacml_pdp_available
        init_xacml_pdp
      end

      if @is_xacml_pdp_available
        request = build_xacml_request(request_id, user_id, tokens.flatten)
        begin
            xacml_resp = @authorize_xacml.evaluateAuthZreq(:authZreq => request)
            xacml_resp = xacml_resp.inspect
            response = xacml_resp.include? '>Permit<'
        rescue
          open('contrail-pep.log', 'a') { |f| f.puts "error calling xacml pdp"}
          @is_xacml_pdp_available = false
        end
      else
        begin
          response = @authorize_original.auth(user_id, tokens.flatten)
        rescue
          response = false
          open('contrail-pep.log', 'a') { |f| f.puts "error calling local pdp"}
        end
      end
            
      enforce_decision(request_id, response)
      open('contrail-pep.log', 'a') { |f| 
        f.puts "the request #{request_id} from #{user_id} has been processed. access decision is " + response.to_s}
    end

   # this should be depricated when contrail authentication manager will be available
   def get_user_dn(id)
     #if no user with such id, returns nill
     user=OpenNebula::User.new_with_id(id, get_one_client)
     user.info
     user_name = user.name.to_s           
     return user_name + "-" + id
   end

      #this should be depricated when contrail attribute manager will be available
   def build_xacml_request(rid, uid, tokens)
     result = ''
     tokens.each do |token|        
        object, id, action, owner, pub=token.split(':')
        #if the object is not created yet, set its id to "new" in the request
        o_id = id.to_i;
        if o_id.to_s != id
          id = 'new'
        end
        # log the request
        open('contrail-pep.log', 'a') { |f|
          f.puts "request_id:#{rid},user_id:#{uid};obejct:#{object},id:#{id},action:#{action},owner:#{owner},pub:#{pub} \n"
        }
        #make the request
        result = '<?xml version="1.0" encoding="UTF-8"?>
          <Request
            xmlns="urn:oasis:names:tc:xacml:2.0:context:schema:os"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:oasis:names:tc:xacml:2.0:context:schema:os
            access_control-xacml-2.0-context-schema-os.xsd">
            <Subject>
              <Attribute
                AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id"
                DataType="http://www.w3.org/2001/XMLSchema#string">
                <AttributeValue>' + get_user_dn(uid) + '</AttributeValue>
              </Attribute>
            </Subject>
            <Resource>
               <Attribute
                 AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id"
                 DataType="http://www.w3.org/2001/XMLSchema#string">
                 <AttributeValue>'+ object + '-' + id + '</AttributeValue>
               </Attribute>
               <Attribute
                 AttributeId="urn:oasis:names:tc:xacml:2.0:resource:owner"
                 DataType="http://www.w3.org/2001/XMLSchema#string">
                 <AttributeValue>'+ get_user_dn(owner) + '</AttributeValue>
               </Attribute>
               <Attribute
                 AttributeId="urn:oasis:names:tc:xacml:2.0:resource:public"
                 DataType="http://www.w3.org/2001/XMLSchema#string">
                 <AttributeValue>' + pub + '</AttributeValue>
               </Attribute>
            </Resource>
            <Action>
              <Attribute
                AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id"
                DataType="http://www.w3.org/2001/XMLSchema#string">
                <AttributeValue>' + action + '</AttributeValue>
              </Attribute>
            </Action>
            <Environment/>
        </Request>'
        break result if result!=true
     end
     return result
   end

   def enforce_decision(request_id, response)    
    if response
      send_message('AUTHORIZE', RESULT[:success],
                request_id, 'success')
    else
      send_message('AUTHORIZE', RESULT[:failure],
                request_id, 'non applicable')
     end
   end
   
end

begin
    am=AuthorizationManager.new
rescue Exception => e
    puts "Error: #{e}"
    exit(-1)
end

am.start_driver
