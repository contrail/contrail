
package org.ow2.contrail.provider.provisioningmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Random;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.Ignore;
import org.ow2.contrail.provider.provisioningmanager.Main;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.*;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.multipart.MultiPart;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import junit.framework.TestCase;


public class TestAllProvisioningManager extends TestCase {

    private HttpServer httpServer;
    
    private WebResource r;

    public TestAllProvisioningManager(String testName) {
        super(testName);
    }
    
     
    protected void xsetUp() throws Exception {
        
    	super.setUp(); 
        //start the Grizzly2 web container 
        httpServer = Main.startUnsecureServer();
        
        //create the client
        Client c = Client.create();
        r = c.resource(ProvisioningManagerProperties.getBaseURI());
    }

    
    protected void xtearDown() throws Exception {
       super.tearDown();
       httpServer.stop();
    }

    public void testBlank(){
    	assertTrue(true);
    }
    
    public void xtestSubmit() {
        
    	MultiPart mp = new MultiPart();
    	StringBuffer sb = null;
    	Random rnd = new Random();
    	long id = rnd.nextLong();
    	
    	String appName  = "ContrailTestApp_"+id;
    	String slaID	= "SLASOI.xml";
    	String username = "ContrailVEPUser_"+id;
    	
    	String xuser = "gregorb";
    	
    	try{
    	
	    	File file = new File("src/main/resources/dsl-test-xlab.xml");
			
			if(!file.exists()){
				file = new File("dsl-test-xlab.xml");
			}
			
			if(!file.exists()){
				
				ProvisioningManagerProperties p = new ProvisioningManagerProperties();
				file = new File(p.getClass().getResource("dsl-test-xlab.xml").toURI());
			}
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
    	
    	}catch (Exception e) {
			e.printStackTrace();
		}
		
		String ovfXML = sb.toString();
		
		// creating the JSON object
		JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields
		try
		{
			jobject.put(USERNAME, username);

		 	// currently fixed to -1 as VEP requested
			jobject.put(USERVID, "-1");

			// currently fixed to user
			jobject.put(USERROLE, "user");

			// for now: fixed to gregorb
			jobject.put(XUSERNAME, xuser);

			JSONArray jsonArray = new JSONArray();
			jsonArray.put("user");
		 	jsonArray.put(username);
		 
		 	jobject.put(USERGROUPS, jsonArray);

		 	JSONObject answer = r.path("/user-mgmt/propagate")
				 				 .type("application/json")
				 				 .put(JSONObject.class, jobject);

		 	String jsonResult = answer.getString(RETURN_STATUS);
		 	
		 	System.out.println("User Registration for user " + username + " completed with code: "+jsonResult);
		
		} catch(Exception e){
			e.printStackTrace();
		}
		
		
    	mp.bodyPart	(appName, 	MediaType.TEXT_PLAIN_TYPE)
    	  .bodyPart	(slaID,		MediaType.TEXT_PLAIN_TYPE)
    	  .bodyPart	(ovfXML, 	MediaType.TEXT_XML_TYPE)
    	  .bodyPart	(username, 	MediaType.TEXT_PLAIN_TYPE);
   
    	JSONObject answer =  r.path("application-mgmt/submit")
    						  .type("multipart/mixed")
    						  .put(JSONObject.class, mp);
        
    	try {
    		
    		String appID = answer.getString(APP_ID);
			System.out.println("ProvisioningManager-ID of the submitted app: "+appID);
			
			JSONObject requestEntity = new JSONObject();
			requestEntity.put(USERNAME, username);
			answer = r.path("application-mgmt/start/"+appID)
					  .type(MediaType.APPLICATION_JSON)
					  .post(JSONObject.class, requestEntity);
			
			JSONArray jArray = answer.getJSONArray(VM_IDS);
    		assertNotNull(jArray);
    		
    		for(int i=0;i<jArray.length();i++){
    			System.out.println("VM_"+i+" - "+jArray.get(i));
    		}
    		
    		answer = r.path("application-mgmt/stop/"+appID)
					  .type(MediaType.APPLICATION_JSON)
					  .post(JSONObject.class, requestEntity);
    		
    		System.out.println("Application Stopped with code: "+answer.getString(RETURN_STATUS));
    		
    		assertNotNull(answer);
    		
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }

}
