#!/bin/bash
source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

username=ContrailVEPUser_$RANDOM
xuser=gregorb
appName=ContrailTestApp_$RANDOM
OVF_FILE=/srv/contegrator-test/vep-cli/dsl-test-xlab.ovf


function propagate {
# param
# 	path, the ProvisioningManager path

curl -i -v -X PUT \
        --header "X-Username: gregorb" \
        --header "Accept: application/json" \
        --header "Content-type: application/json" \
        --data '{"name":'$username', "vid":"-1", "role":"user", "xUser":'$xuser', "groups":["user", '$username']}' \
        $path/user-mgmt/propagate
}

function submit {
# param
# 	path, the ProvisioningManager path

slaID=SLASOI.xml

OVF="`cat $OVF_FILE`"

curl -i -v -X PUT \
		--header "X-Username: gregorb" \
        --header "Accept: application/json" \
        --header "Content-type: multipart/mixed" \
        --data "$appName $slaID $OVF $username" \
		$path/application-mgmt/submit
}

function start {
# param
# 	path, the ProvisioningManager path
#   APP_ID, an application ID

appName=ContrailTestApp_$APP_ID

curl -i -v -X POST \
        --header "X-Username: gregorb" \
        --header "Accept: application/json" \
        --header "Content-type: application/json" \
        --data '{"name":$username}' \
        $path/application-mgmt/start/$appName 
}

function stop {
# param
# 	path, the ProvisioningManager path
#   APP_ID, an application ID

appName=ContrailTestApp_$APP_ID

curl -i -v -X POST \
        --header "X-Username: gregorb" \
        --header "Accept: application/json" \
        --header "Content-type: application/json" \
		$path/application-mgmt/stop/$appName
}

# Main
path=$1
#APP_ID=$2

propagate
#submit
#start
#stop
