package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the OVFS database table.
 * 
 */
@Entity
@Table(name="OVFS")
public class Ovf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false)
	private long id;

	@Column(name="OVF_VEP_ID")
	private int ovfVepId;

    @Lob()
	@Column(name="OVF_XML_DESCR")
	private String ovfXmlDescr;

	@Column(length=30)
	private String status;

	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="ovf", cascade={CascadeType.PERSIST})
	private List<Application> applications;

	//bi-directional many-to-one association to VepTemplate
	@OneToMany(mappedBy="ovf",cascade={CascadeType.PERSIST})
	private List<VepTemplate> vepTemplates;

    public Ovf() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getOvfVepId() {
		return this.ovfVepId;
	}

	public void setOvfVepId(int ovfVepId) {
		this.ovfVepId = ovfVepId;
	}

	public String getOvfXmlDescr() {
		return this.ovfXmlDescr;
	}

	public void setOvfXmlDescr(String ovfXmlDescr) {
		this.ovfXmlDescr = ovfXmlDescr;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	
	public List<VepTemplate> getVepTemplates() {
		return this.vepTemplates;
	}

	public void setVepTemplates(List<VepTemplate> vepTemplates) {
		this.vepTemplates = vepTemplates;
	}
	
}