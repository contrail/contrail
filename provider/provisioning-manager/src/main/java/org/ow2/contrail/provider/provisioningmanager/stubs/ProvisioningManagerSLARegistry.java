package org.ow2.contrail.provider.provisioningmanager.stubs;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;
// import org.slasoi.gslam.core.negotiation.SLARegistry;
// import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

// import eu.slaatsoi.slamodel.SLADocument;

public class ProvisioningManagerSLARegistry{
	// dummy declaration
}
/*
@Deprecated
public class ProvisioningManagerSLARegistry implements SLARegistry, 
													SLARegistry.IAdminUtils, 
													SLARegistry.IQuery, 
													SLARegistry.IRegister{

	
	private static String SLA_PATH_STD = "/srv/contrail/SLAs";
	private static String SLA_PATH = "SLA_PATH";
	
	private static ProvisioningManagerSLARegistry _instance;
	public static synchronized ProvisioningManagerSLARegistry getInstance() { 
		if (_instance == null) { 
			_instance = new ProvisioningManagerSLARegistry(); 
		} 
		return _instance; 
	}
	
	private ProvisioningManagerSLARegistry(){ }
	
	
	@Override
	public IAdminUtils getIAdmin() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public IQuery getIQuery() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public IRegister getIRegister() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public UUID register(SLA arg0, UUID[] arg1, SLAState arg2)
			throws RegistrationFailureException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID sign(UUID arg0) throws UpdateFailureException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID update(UUID arg0, SLA arg1, UUID[] arg2, SLAState arg3)
			throws UpdateFailureException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID[] getDependencies(UUID arg0) throws InvalidUUIDException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] arg0)
			throws InvalidUUIDException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SLA[] getSLA(UUID[] arg0) throws InvalidUUIDException {
		
		SLASOIParser slasoiParser = new SLASOIParser();
        SLA sla = null;
		SLADocument slaXml = null;
		
		try {
		
			String slaPath = ProvisioningManagerProperties.getProperty(SLA_PATH);
			if( slaPath == null || slaPath.trim().isEmpty() ){
				slaPath = SLA_PATH_STD;
			}
			
			File file = new File(slaPath+"/"+arg0[0].getValue());
			
			//if(!file.exists()){
				
			//	file = new File(arg0[0].getValue());
			//}
		
			System.out.println("loading SLA from: "+file.getAbsolutePath());
			
			InputStream is = new FileInputStream( file );
			slaXml = SLADocument.Factory.parse(is);
			
			sla = slasoiParser.parseSLA(slaXml.xmlText());
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		
			
		return new SLA[]{sla};
	}

	@Override
	public SLA[] getSLAsByParty(UUID arg0) throws InvalidUUIDException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID[] getSLAsByState(SLAState[] arg0, boolean arg1)
			throws InvalidStateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SLA[] getSLAsByTemplateId(UUID arg0) throws InvalidUUIDException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SLAStateInfo[] getStateHistory(UUID arg0, boolean arg1)
			throws InvalidUUIDException, InvalidStateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID[] getUpwardDependencies(UUID arg0) throws InvalidUUIDException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear(UUID[] arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAll() {
		// TODO Auto-generated method stub
		
	}

}
*/