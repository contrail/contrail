package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

public interface ExternalizedReturnJSONstrings {
	
	public static final String RETURN_STATUS		=		"STATUS";
	public static final String APP_ID				=		"appId";
	public static final String VM_IDS				=		"vm_id";
	

}
