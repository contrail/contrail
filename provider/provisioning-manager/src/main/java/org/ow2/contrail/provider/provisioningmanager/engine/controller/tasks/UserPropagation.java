package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.ow2.contrail.provider.provisioningmanager.RESTitf.ApplicationManagement;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.PmUser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam.SLAMconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;


public class UserPropagation implements Callable<Boolean> {
	
	private String username;
	private String role;
	private Integer slaID;
	private String xUserName;
	private JSONArray groups;
	private String userUuid;
	private static Logger logger = Logger.getLogger(UserPropagation.class);
	
	public UserPropagation(String Username, String Role, String uuid, Integer SlaID, String XUserName, JSONArray Groups) {
		
		this.username = Username;
		this.role = Role;
		this.slaID = SlaID;
		this.xUserName = XUserName;
		this.groups = Groups;
		this.userUuid = uuid;
	}
	
	public UserPropagation(String Username,  String uuid) {
		
		this.username = Username;
		this.userUuid = uuid;
	}

	@Override
	public Boolean call() throws Exception {
		

		
		//SLAMconnector.getInstance().registerSLAUser(slaID);
		
		/* Store User in PM database */
		PmUser user = new PmUser();
		user.setUsername(this.username);
		user.setUserUuid(this.userUuid);
		//user.setSLAuserID(slaID);
		
		if(DBconnector.getInstance().retrieveObjectByClass(PmUser.class, username) == null){
			logger.info("Create new User: "+ username +": "+userUuid);
			DBconnector.getInstance().storeObject(user);
		}
		
		return new Boolean(true);
	}
}