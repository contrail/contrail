package org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers;

import java.util.HashMap;

@Deprecated
public class ApplicationsData {

	public static int appID = 0;
	public static HashMap<Integer, HashMap<String, String>> applications = new HashMap<Integer, HashMap<String, String>>();
	
	private static ApplicationsData _instance = null;
	
	public static synchronized ApplicationsData getInstance() { 
		if (_instance == null) { 
			_instance = new ApplicationsData(); 
		} 
		return _instance; 
	}
	
	private ApplicationsData(){	}

	public Integer storeApp(HashMap<String, String> app) {
		// TODO Current code has to be substituted with effective one
		
		appID++;
		applications.put(appID, app);
		
		return appID;
	}

	public HashMap<String, String> retrieve(Integer AppID) {
		// TODO Current code has to be substituted with effective one
		return applications.get(AppID);
	}

	public void logModification(String AppId, String operation) {
		// TODO Auto-generated method stub
		
	}
}
