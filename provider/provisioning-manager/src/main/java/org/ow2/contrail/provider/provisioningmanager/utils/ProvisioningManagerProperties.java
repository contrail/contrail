package org.ow2.contrail.provider.provisioningmanager.utils;

import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.PROVISIONING_MANAGER_IP;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.PROVISIONING_MANAGER_PORT;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.PROVISIONING_MANAGER_SECURE_PORT;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.core.UriBuilder;

/*
 * CHANGE: Added support for server-side https URI @author: Marco
 * getBaseURI(): for https uri
 * getUnsecureBaseURI(): for http uri
 */

public class ProvisioningManagerProperties 
{

	private static Properties props = null; 
	private static String configurationPath = "";
	
	private static final String stdHostName 	= "localhost";
	private static final String stdPortName 	= "4242";
	private static final String stdSecurePortName 	= "8282";
	private static final String REST_PROTOCOL 	= "https://";	
	private static final String UNSECURE_REST_PROTOCOL 	= "http://";	
	
	public static URI getBaseURI() 
	{
		String HOSTNAME = ProvisioningManagerProperties.getProperty(PROVISIONING_MANAGER_IP);
		if(HOSTNAME.trim().isEmpty()){
			try {
				HOSTNAME = InetAddress.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				HOSTNAME = stdHostName;
			}
		}
		
		String HOSTPORT = ProvisioningManagerProperties.getProperty(PROVISIONING_MANAGER_SECURE_PORT);
		if(HOSTPORT.trim().isEmpty()){
			HOSTPORT = stdSecurePortName;
		}
		
		return UriBuilder.fromUri(REST_PROTOCOL+HOSTNAME+"/").port(Integer.parseInt(HOSTPORT)).build();
	}
	
	public static URI getUnsecureBaseURI() 
	{
		String HOSTNAME = ProvisioningManagerProperties.getProperty(PROVISIONING_MANAGER_IP);
		if(HOSTNAME.trim().isEmpty()){
			try {
				HOSTNAME = InetAddress.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				HOSTNAME = stdHostName;
			}
		}
		
		String HOSTPORT = ProvisioningManagerProperties.getProperty(PROVISIONING_MANAGER_PORT);
		if(HOSTPORT.trim().isEmpty()){
			HOSTPORT = stdPortName;
		}
		
		return UriBuilder.fromUri(UNSECURE_REST_PROTOCOL+HOSTNAME+"/").port(Integer.parseInt(HOSTPORT)).build();
	}
	
	public static void setConfigurationFilePath(String conf)
	{
		configurationPath = conf;
	}
	
	public static String getProperty(String propertyName){
		
		if(props == null){
			loadPropertyFile(configurationPath);
		}
		
		return props.getProperty(propertyName);
	}  
	
	public static Set<Entry<Object, Object>> getAllProperties(){
		
		if(props == null){
			loadPropertyFile(configurationPath);
		}
		
		return props.entrySet();
	}
	
	private static void loadPropertyFile(String configurationPath)
	{
		
		props = new Properties();
		try {
			
			File file = new File(configurationPath);
			
			if(!file.exists()){
				
				file = new File("ProvisioningManager.settings");
			}
			
			if(!file.exists()){
				
				file = new File(System.getProperty("user.dir") + "/" + "src/main/resources/ProvisioningManager.settings");
			}
			
			if(!file.exists()){
				
				ProvisioningManagerProperties p = new ProvisioningManagerProperties();
				file = new File(p.getClass().getResource("ProvisioningManager.settings").toURI());
			}
			
			props.load(new FileReader(file));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String args[]){
		
		for(Entry<Object, Object> entry : getAllProperties()){
			
			System.out.println(entry.getKey() + " - " + entry.getValue());
		}
		
	}
}
