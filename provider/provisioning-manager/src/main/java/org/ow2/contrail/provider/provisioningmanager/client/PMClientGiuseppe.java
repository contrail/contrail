package org.ow2.contrail.provider.provisioningmanager.client;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERGROUPS;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERNAME;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERROLE;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERVID;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.XUSERNAME;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.APP_ID;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.MultiPart;

/**
 * 
 * @author ales.cernivec@xlab.si
 *
 */
public class PMClientGiuseppe {

	private static WebResource r;
	private static String URI;
	
	/**
	 * Helper method for printing stack trace into a string.
	 * 
	 * @param aThrowable error to be traced
	 * @return string presenting stack trace
	 */
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	public static void propagateUser(HashMap<String, String> arguments)throws Exception{
		// creating the JSON object
		JSONObject jobject = new JSONObject();
		// fill the JSON with the proper fields
		try
		{
			jobject.put(USERNAME, arguments.get("username"));
			// currently fixed to -1 as VEP requested
			jobject.put(USERVID, arguments.get("vid"));
			jobject.put(USERROLE, arguments.get("role"));
			jobject.put(XUSERNAME, arguments.get("xuser"));
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(arguments.get("role"));
			jobject.put(USERGROUPS, jsonArray);
			JSONObject answer = r.path("/user-mgmt/propagate")
			.type("application/json")
			.put(JSONObject.class, jobject);
			String jsonResult = answer.getString(RETURN_STATUS);
			System.out.println("User Registration for user " + arguments.get("username") + " completed with code: "+jsonResult);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	public static HashMap<String, String> submitApp(HashMap<String, String> arguments) throws Exception{
		MultiPart mp = new MultiPart();
		HashMap<String, String> ret = new HashMap<String, String>();
		String appName  = arguments.get("appName");
		String slaID  = arguments.get("slaID");
		String username  = arguments.get("username");
		StringBuffer sb = new StringBuffer();
		String ovfFileName = arguments.get("ovfPath");
		try{
			File file = new File(ovfFileName);
			if(!file.exists()){
				throw new Exception("OVF does not exist.");
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		String ovfXML = sb.toString();	
		try {
			mp.bodyPart	(appName, 	MediaType.TEXT_PLAIN_TYPE)
			.bodyPart	(ovfXML, 	MediaType.TEXT_XML_TYPE)
			.bodyPart	(username, 	MediaType.TEXT_PLAIN_TYPE)
			.bodyPart   (slaID, MediaType.TEXT_PLAIN_TYPE);
			JSONObject answer =  r.path("application-mgmt/submit")
			.type("multipart/mixed")
			.put(JSONObject.class, mp);
			ret.put("status" , answer.getString(RETURN_STATUS));
			ret.put("idApp", answer.getString(APP_ID));
		}catch(Exception err){
			err.printStackTrace();
		}
		
		return ret;
	}
	
	public static void startApp(HashMap<String, String> arguments, ArrayList<String> vs_name){
		
		String idApp = arguments.get("idApp");
		String uName = arguments.get("username");		
		
		JSONObject mp = new JSONObject();
		JSONObject jsonVinID = new JSONObject();
		
		
		try{
			mp.put("name", uName);
			for(int i=0;i<vs_name.size(); i++)
				jsonVinID.put("a"+i, vs_name.get(i) );
			mp.put("vin_ids", jsonVinID);
			
			JSONObject answer = r.path("application-mgmt/start/" + idApp).type("application/json")
			.post(JSONObject.class, mp);
			System.out.println("Status: " + answer.getString(RETURN_STATUS)+ "\n \"VM_ID\": " +answer.getJSONArray(VM_IDS));
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void stopApp(HashMap<String, String> arguments){
		String appID = arguments.get("idApp");
		String name = arguments.get("username");
		JSONObject mp = new JSONObject();
		try{
			mp.put("name",name);
			JSONObject answare = r.path("application-mgmt/stop/"+appID).type("application/json").post(JSONObject.class, mp);
			System.out.println("Status: " + answare.get(RETURN_STATUS));
			if(answare.getString(RETURN_STATUS).equalsIgnoreCase("ok"))
				System.out.print("vm id stopped: " + answare.getJSONArray(VM_IDS));
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
		
	/**
	 * @param argsjson
	 */
	public static void main(String[] args){
		Random rand = new Random(System.currentTimeMillis());
		
		HashMap<String, String> argsMap = new HashMap<String, String>();
		ArrayList<String> vs_names = new ArrayList<String>();
		
		Client c = Client.create();
		r = c.resource("http://greco.isti.cnr.it:4242/");
		
		argsMap.put("url","http://greco.isti.cnr.it:4242");
		
		argsMap.put("username","alice");
		argsMap.put("vid", "-1");
		argsMap.put("role","user");
		argsMap.put("xuser","admin");
		
		//argsMap.put("ovfPath", "/home/tirocinante/workspace/trunk/provider/provisioning-manager/src/main/resources/ovf-cnr-ubntServer.xml");
		argsMap.put("ovfPath", "/home/tirocinante/workspace/trunk/provider/provisioning-manager/src/main/resources/ovf-MultipleVM-cnr-ubntServer.xml");
		argsMap.put("appName", "UbuntuTest");
		argsMap.put("slaID", "10");
		
		URI = argsMap.get("url");
		
		vs_names.add("VirtualSystem1");
		vs_names.add("VirtualSystem2");
		HashMap<String , String> ret;
		String idApp = null;
		try {
			propagateUser(argsMap);
			ret = submitApp(argsMap);
			System.out.println("Application ID submitted: " + ret.get("idApp"));
			if(Integer.parseInt(ret.get("idApp")) != -1){
				argsMap.put("idApp", ret.get("idApp"));
				startApp(argsMap, vs_names);
	//			stopApp(argsMap);
				System.out.println("status: " + ret.get("status"));
				System.out.println("other:" + ret.get("idApp"));
			}else
				System.out.println("Inconsistent DB");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	
}
