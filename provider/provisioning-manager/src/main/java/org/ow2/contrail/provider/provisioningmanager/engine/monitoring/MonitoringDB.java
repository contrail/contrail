package org.ow2.contrail.provider.provisioningmanager.engine.monitoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MonitoringDB 
{
	int MAX_SIZE = 1000;
	int CROP_SIZE = 500;
	ArrayList<String> data;
	
	public MonitoringDB()
	{
		data = new ArrayList<String>(); 
	}
	
	public void registerEntry(String entry)
	{
		data.add(entry);
		
		if (data.size() > MAX_SIZE)
		{
			String[] original = new String[data.size()]; 
			original = data.toArray(original);
			
			String[] target = Arrays.copyOfRange(original, CROP_SIZE, original.length);
			data.clear();
			data.addAll(Arrays.asList(target));
		}
	}
	
	public List<String> getData()
	{
		return data;
	}

	
}
