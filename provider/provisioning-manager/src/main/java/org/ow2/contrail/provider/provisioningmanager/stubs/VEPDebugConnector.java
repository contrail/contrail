package org.ow2.contrail.provider.provisioningmanager.stubs;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.*; 

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;


public class VEPDebugConnector extends VEPconnector{ 

	//HashMap<>
    /**
     * static Singleton instance
     */
    private static VEPDebugConnector instance = null;
    private static String VEP_URI = null;
    private static Logger logger = Logger.getLogger(VEPDebugConnector.class);
    private static String loggerTag = "[VEPDebugConnector] ";
    private int sno = 0; // ovfId
    private int template = 1;
    private HashMap<Integer,String> ovfIds = new HashMap<Integer, String>();
    
    /**
     * Private constructor for singleton
     * @param ceeId 
     */
    public VEPDebugConnector() {

        String HOSTNAME = ProvisioningManagerProperties.getProperty(VEP_IP);
        if (HOSTNAME.trim().isEmpty()) {
            try {
                HOSTNAME = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                HOSTNAME = stdVepHostName;
            }
        }

        String HOSTPORT = ProvisioningManagerProperties.getProperty(VEP_PORT);
        if (HOSTPORT.trim().isEmpty()) {
            HOSTPORT = stdVepPortName;
        }
        
        VEP_URI = VEP_PROTOCOL_PREFIX + HOSTNAME + ":" + HOSTPORT + "/api/cimi/cee/";

    }

    public String getVEP_URI() {
        return "[DEBUG] " + VEP_URI;
    }

    
    @Override
    public JSONObject getApplications(String ceeIdentifier, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "getApplications: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	int randomApp = (int) (Math.random()*1000);
    	logger.info(randomApp);
        JSONObject ret = new JSONObject("{\"id\":\"/api/cimi"+ceeIdentifier+"/applications\",\"count\":1," +
        		"\"resourceURI\":\"VEP/ApplicationCollection\"," +
        		"\"applications\":[{\"name\":\"ApplicationSLA\",\"href\":\"/api/cimi/cee/"+ceeIdentifier+"/application/"+randomApp+"\"}]}"); 
        
        return ret;
    }
    
    @Override
    public JSONObject getApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "getApplication: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	int randomVm = (int) (Math.random()*1000);
    	logger.info(loggerMethodTag + "GET TO "+VEP_URI+ceeIdentifier+"/application/"+appId);
        JSONObject ret = new JSONObject("{\"id\":\"/api/cimi/cee/"+ceeIdentifier+"/application/"+appId+"\"," +
        		"\"resourceURI\":\"VEP/Application\"," +
        		"\"VMs\":[{\"state\":\"DEP\",\"href\":\"/api/cimi/api/cimi/cee/"+ceeIdentifier+"/application/"+ appId+"/VMs/"+randomVm+"\"}," +
        				"{\"state\":\"DEP\",\"href\":\"/api/cimi/api/cimi/cee/"+ceeIdentifier+"/application/"+ appId+"/VMs/"+(randomVm+1)+"\"}]," +
        		"\"operations\":[{\"rel\":\"edit\",\"href\":\"/cee/"+ceeIdentifier+"/application/"+appId+"\"},{\"rel\":\"delete\",\"href\":\"/cee/"+ceeIdentifier+"/application/"+appId+"\"},{\"rel\":\"/VEP/actions/start\",\"href\":\"/cee/"+ceeIdentifier+"/application/"+appId+"/action/start\"},{\"rel\":\"/VEP/actions/stop\",\"href\":\"/cee/"+ceeIdentifier+"/application/"+appId+"/action/stop\"}]," +
   				"\"OVFs\":[{\"href\":\"/api/cimi/cee/"+ceeIdentifier+"/application/"+appId+"/OVF/1\"}]}");
        return ret;
    }
    
    @Override
    public JSONObject deployApplication(String ceeIdentifier, String appId, JSONObject deploymentDocument, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "deployApplication: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "ceeId: " + appId);
        JSONObject ret = new JSONObject();
        logger.info("POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+ " with:");
        logger.info("DeploymentDocument: "+deploymentDocument+"\n");
        ret.put(RETURN_STATUS, "200");
        logger.info("JSON BODY POST:\n" + deploymentDocument.toString()+"\n");
        return ret; 
    }
    
    @Override
    public JSONObject startApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "startApplication: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	JSONObject ret = new JSONObject(); 
    	logger.info(loggerMethodTag + "POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+ "/action/start");
       	ret.put(RETURN_STATUS, "200");
        return ret; 
    }

    @Override
    public JSONObject stopApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "stopApplication: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	JSONObject ret = new JSONObject(); 
    	logger.info(loggerMethodTag + "POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+ "/action/stop");
	    ret.put(RETURN_STATUS, "200");
	    return ret; 
    }
    
    @Override
    public JSONObject deleteApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "deleteApplication: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	JSONObject ret = new JSONObject(); 
    	logger.info(loggerMethodTag + "POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+ "/action/delete");
	    ret.put(RETURN_STATUS, "200");
	    return ret; 
    }
 
    @Override
    public JSONObject getVm(String ceeIdentifier, String appId, String vmId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "getVm: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	logger.info(loggerMethodTag + "vmId: " + vmId);
    	logger.info(loggerMethodTag + "GET TO "+VEP_URI+ceeIdentifier+"/application/"+appId+"/vm/"+vmId);
        JSONObject ret = new JSONObject("{\"id\":\"/api/cimi/cee/"+ceeIdentifier+"/application/"+appId+"/vm/"+vmId+"\"" +
        		",\"resourceURI\":\"VEP/VirtualMachine\",\"name\":\"vm-1\",\"state\":\"DEP\",\"virtualSystem\":{\"href\":\"#ubu1\"},\"ip\":\"192.168.122.55\",\"vmhandler\":{\"href\":\"/vmhandler/2\"}}");
        return ret;
    }
    
    @Override
    public JSONObject stopVm(String ceeIdentifier, String appId, String vmId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "stopVm: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	logger.info(loggerMethodTag + "vmId: " + vmId);
    	logger.info(loggerMethodTag + "POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+"/vm/"+vmId+"/action/stop");
        JSONObject ret = new JSONObject();
        return ret;
    }
    
    @Override
    public JSONObject deleteVm(String ceeIdentifier, String appId, String vmId, String userUuid) throws JSONException {
    	String loggerMethodTag = loggerTag + "deleteVm: ";
    	logger.info(loggerMethodTag + "uuid: " + userUuid);
    	logger.info(loggerMethodTag + "ceeId: " + ceeIdentifier);
    	logger.info(loggerMethodTag + "appId: " + appId);
    	logger.info(loggerMethodTag + "vmId: " + vmId);
    	logger.info(loggerMethodTag + "DELETE TO "+VEP_URI+ceeIdentifier+"/application/"+appId+"/vm/"+vmId);
        JSONObject ret = new JSONObject();
        ret.put(RETURN_STATUS, "200");
        return ret;
    }
    public String getOVFApplication(String ceeId, String appId) throws JSONException, UniformInterfaceException {
    	  //GET /ceeId/application/ "OVFs":[{"href":"\/api\/cimi\/cee\/94\/application\/141\/OVF\/119"}]
          return null;
      }
    
    public JSONObject submitOVF(String appName, String ovfXmlDescr, String user) {

        JSONObject ret = null;

        ret = new JSONObject();
        try {
			ret.put("message", "SUCCESS_ACCEPTED");
			ret.put("title", "pippo");
			ovfIds.put(sno, ovfXmlDescr);
	        ret.put("sno", sno++);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
        return ret;

    }

    public JSONObject initializeOVF(String ovfID, String user) {

        JSONObject ret = null;
        ret = new JSONObject();
        String ovf = ovfIds.get(sno);
        try {
			ApplicationDescriptor ap = OVFParser.ParseOVF(ovf);
			String appName = ap.getName();
			Collection<ApplianceDescriptor> d = ap.getAllAppliances();
			Vector<ApplianceDescriptor> v = new Vector<ApplianceDescriptor>(d);
			
			if (Integer.parseInt(ovfID) < sno){
				JSONArray obj = new JSONArray();
				JSONArray ovflist = new JSONArray();
				for (int i = 1; i <= d.size(); i++){
					obj.put(template++);
					ovflist.put(v.get(i).getID());
				}
	        	ret.put("template_ids", obj);
	        	ret.put("title", "pippo");
	        	ret.put("application_name", appName);
	        	ret.put("ovfid_list", ovflist);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		} 
        

        return ret;
    }
    
    public JSONObject getVMinfo(int VMid, String user){
    
        JSONObject ret = new JSONObject();  
        try {
        	ret.put("vnc_ip", "localhost");
        	ret.put("id", VMid);
        	ret.put("title", "Virtual Machine Detail");
        	ret.put("host_id", 1);
        	ret.put("name", "Ubuntu HTTP Server");
        	ret.put("state", "RN");
        	ret.put("iaas_id", 88);
        	ret.put("template_id", 1);
        	ret.put("user_id", 2);
        	ret.put("controller", "OpenNebula");
        	ret.put("vnc_port", "5988");
        	ret.put("ip", "192.168.0.100");
        }
        catch (Exception e){
        	e.printStackTrace();
        }
        
         return ret;
    } 

    public JSONObject startVirtualSystem(String templateID, String user, String vinID) {
    	JSONObject ret = null;
    	
    	JSONObject deploy = new JSONObject();
    	try {
    		deploy.put("title", "VM template deploy action result");
    		deploy.put(	"iaas_id", 90);
    		deploy.put(	"vm_state", "DP");
    		deploy.put(	"vm_id", 3);
    		deploy.put(	"controller", "OpenNebula");
    		deploy.put(	"app_name", "Contrail_Test_Application");

    		JSONObject vm_info = getVMinfo(3, " ");
    		ret.putOpt("deploy", deploy);
    		ret.putOpt("vm-info", vm_info);
    	}
    	catch (Exception e){
    		e.printStackTrace();
    	}
    	return ret;
    }



    public JSONObject getUserOVF(String user) {

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        WebResource service = client.resource(VEP_URI + "/" + OVF_PATH + "/");	//+ID_PATH+"/"+ovfID+"/"+ACTION_PATH+"/"+INITIALIZE_OP);

        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .header(USERNAME_HEADER, user);

        return buildConn.get(JSONObject.class);

    }
}
