package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db;

public interface ExternalizedDBstrings {

	// PERSISTENCE DETAILS
	public static final String PERSISTENCE_UNIT_NAME	= "derbyAppDB";
	
}
