package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
// import org.apache.tools.ant.filters.StringInputStream;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.RESTitf.ApplicationManagement;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Ovf;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.PmUser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Sla;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam.SLAMconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;
import org.slasoi.slamodel.sla.SLA;

import com.sun.jersey.api.client.UniformInterfaceException;


public class ApplicationSubmission implements Callable<JSONObject> {
 
	private String appName = null;
	// private String slaRef = null;
	private String ovfXmlDescr = null;
	private String username = null;
	private Integer slaUserID = null;
	private Integer ceeId = null;
	private String userUuid;
	
	private static Logger logger = Logger.getLogger(ApplicationSubmission.class);

	public ApplicationSubmission(String AppName, String OVFXmlDescr, String userName, Integer SlaUserID, Integer ceeIdentifier, String uuid) {
		this.appName = AppName;
		this.ovfXmlDescr = OVFXmlDescr;
		this.username = userName;
		this.slaUserID = SlaUserID;
		this.ceeId = ceeIdentifier;
		this.userUuid = uuid;
	}
	

	@Override
	public JSONObject call()  {
		String loggerTag = "[ApplicationSubmission] ";
		/*
		 * TODO: Retrieve CeeId from SLAM-P
		 */
		JSONObject returnObject = new JSONObject();
		String ceeIdentifier= Integer.toString(this.ceeId);
		String appId = null;
		Application submittedApp = new Application();
		try{
			logger.info(this.userUuid + " " + this.username);
			PmUser PMUser = null;
			try {
				if(this.userUuid != "")
					PMUser = DBconnector.getInstance().retriveUserByUuid(this.userUuid);
				else 
					PMUser = DBconnector.getInstance().retrieveObjectByClass(PmUser.class, this.username);
			}catch (Exception e) {
				logger.info(loggerTag + "User Not already registered. regitering...");
			}
			// If no user is already in the DB, a new one is created
			if (PMUser == null) {
				
				PMUser = new PmUser();
				PMUser.setUsername(this.username);
				PMUser.setUserUuid(this.userUuid);
				//PMUser.setSlaUserId(slaUserID);
				/*
				ArrayList<Sla> alist = new ArrayList<Sla>();
				if (sla_support_activated) {
					for (SLA sla : allSLAs()) {
						Sla slaEntity = new Sla();
						slaEntity.setId(sla.getUuid().toString());
						alist.add(slaEntity);
						//System.out.println("create new SLAUser: " + slaEntity.getSlaXmlDescr() + " .....");
					}
					PMUser.setSlas(alist);
				}
				*/
				DBconnector.getInstance().storeObject(PMUser);
				logger.info(loggerTag +"created new User: " + PMUser);
			} else logger.info(loggerTag +"User already registered: " + PMUser);
			logger.info("User " + PMUser);
			//returnObject.put("ceeId", ceeIdentifier);
			//Retrieve applicationId
			JSONObject getResponse = VEPconnector.getInstance().getApplications(ceeIdentifier, this.userUuid);
			logger.info(loggerTag + "responsegetAPPS: " + getResponse);
			String[] appHrefString = getResponse.getJSONArray("applications").getJSONObject(0).getString("href").split("/");
			appId = appHrefString[appHrefString.length - 1];
			logger.info(loggerTag + "APPID retrieved: " + appId);
			//Retrieve application information
			JSONArray appVMs = new JSONArray();
			JSONObject getAppResponse = VEPconnector.getInstance().getApplication(ceeIdentifier,appId, this.userUuid);
			logger.info(loggerTag + "response getAPP: " + getResponse);
			//appVMs = getAppResponse.getJSONArray("VMs");
			
			//System.out.println(getResponse);
			//System.out.println("retrives VMs from application-"+appId+"\n"+appVMs);
			 
			//Verify if sla support is activated
			/*
			 * TODO
			 */
			//boolean sla_support_activated = ProvisioningManagerProperties
			//		.getProperty("SLA_SUPPORT_ENABLED").equalsIgnoreCase("true");
			// Retrieve the user description at Provisioning Manager level
			// Create an object to store in the DB
			submittedApp = new Application();
			// Store Ovf and App
			submittedApp.setPmUser(PMUser);
			submittedApp.setCeeId(ceeIdentifier);
			submittedApp.setVepAppId(appId);
			Ovf ovfApp = new Ovf();
			ovfApp.setOvfXmlDescr(this.ovfXmlDescr);
			DBconnector.getInstance().storeObject(ovfApp);
			submittedApp.setOvf(ovfApp);
			
			// VERIFY DB STATUS
			/*
			 * TODO verify if appId have to be unique in the PM database
			 */
			DBconnector.getInstance().storeObject(submittedApp);
			Application application = DBconnector.getInstance().retrieveObject(submittedApp);
			logger.info(loggerTag +"Application "+appId+" stored in the DB");
		} catch (javax.persistence.RollbackException e){
			Application appl = new Application();
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId);
			logger.info(loggerTag +"Application "+appId+" already registered.");
		} catch (UniformInterfaceException e) {
			logger.error(loggerTag + "UniformInterfaceException"+ e.getMessage());
			e.printStackTrace();
		} catch (JSONException ex) {
			logger.info(loggerTag +"JSONError catched");
			ex.printStackTrace();
			try {
				returnObject.put(RETURN_STATUS, "500");
				returnObject.put(APP_ID, "-1");
			} catch (JSONException e) {e.printStackTrace();}
			ex.printStackTrace();
			return returnObject;
			
		} 
		try {
			logger.info(loggerTag + " Application Submitting: " + appId);
			returnObject.put(RETURN_STATUS, "200");
			returnObject.put(APP_ID, appId);
		} catch (JSONException e) {e.printStackTrace();}
		
		
		Application applDb = DBconnector.getInstance().getApplicationEntityByVepId(appId);
		logger.info("Application Stores in the DB:\n");
		logger.info("\tAppId: "+ applDb.getId()+"\n\tAppVepId: "+applDb.getVepAppId()+"\n\tCeeId: "+ applDb.getCeeId()+"\n\tAppUser: "+applDb.getPmUser().getUsername());
		return returnObject;
	}

	private ArrayList<SLA> allSLAs() throws Exception {
		String[] SLAs = SLAMconnector.getInstance().getUserSLAs(slaUserID);
		SLA sla = null;
		ArrayList<SLA> SLAarray = new ArrayList<SLA>();

		/*
		 * TODO: to be refactored according to the new interaction pattern.
				 these classes are not available in the new package
			
		for (String s : SLAs) {
			InputStream is = new StringInputStream(s);
			slaXml = SLADocument.Factory.parse(is);
			sla = slasoiParser.parseSLA(slaXml.xmlText());
			SLAarray.add(sla);
			
		}
		*/
		return SLAarray;
	}
	/*

	private ArrayList<SLA> selectSLA(ApplicationDescriptor ovf)
			throws Exception {

		ArrayList<SLA> all = allSLAs();
		ArrayList<SLA> outputSLAarray = new ArrayList<SLA>();

		for (SLA s : all) {

			if (ConsistencyCheck.getInstance().checkOVFSLAConsistency(ovf, s)) {
				outputSLAarray.add(s);
			}
		}

		return outputSLAarray;
	}
*/
}
