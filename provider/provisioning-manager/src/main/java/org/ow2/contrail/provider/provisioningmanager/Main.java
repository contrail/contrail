package org.ow2.contrail.provider.provisioningmanager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import org.apache.log4j.Logger;


import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.ProvisioningManagerController;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.container.ContainerFactory;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.container.grizzly2.GrizzlyWebContainerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * CHANGE: Added support for https connection (server side) @author: Marco
 */

public class Main 
{

	private static final String REST_ITF_PACKAGE = "org.ow2.contrail.provider.provisioningmanager.RESTitf";
	public static boolean running = true;
	private static HttpServer secureWebServer;
	private static HttpServer unsecureWebServer;
	private static Logger logger = Logger.getLogger(Main.class);
	private static String loggerTag="[PROVISIONING MANAGER][MAIN CLASS] ";
	
	
	public static HttpServer startSecureServer() throws IOException {
	
		//Setting Contex e Servlet Container for REST Services
		WebappContext secureContext = new WebappContext("secure_provisioningmanager");
        ServletRegistration secure_registration = secureContext.addServlet("ServletContainer", ServletContainer.class);
        secure_registration.setInitParameter("com.sun.jersey.config.property.packages", REST_ITF_PACKAGE);
        // set up security context
        SSLContextConfigurator sslContext = new SSLContextConfigurator();
		sslContext.setKeyStoreFile("src/main/resources/keystore_server"); // contains server key
		sslContext.setKeyStorePass("contrail");
		sslContext.setTrustStoreFile("src/main/resources/truststore_server"); // contains client certificate
		sslContext.setTrustStorePass("contrail");
		
		//Add resources to Container
		HttpHandler handler = ContainerFactory.createContainer(HttpHandler.class, new PackagesResourceConfig(REST_ITF_PACKAGE));
		try {
			//Create Grizzly Web Server with SSL Context
			secureWebServer = GrizzlyServerFactory.createHttpServer(ProvisioningManagerProperties.getBaseURI(), 
							handler,
							true, 
							new SSLEngineConfigurator(sslContext).setClientMode(false).setNeedClientAuth(true));
			
			// deploy Grizzly embedded server //
			secureContext.deploy(secureWebServer);
			logger.info(loggerTag+secureContext.getServerInfo());
	    } catch (Exception ex) {
	        	logger.error(loggerTag+ex);
			}
		// start Grizzly server //
		secureWebServer.start();
		return secureWebServer;
	}
public static HttpServer startUnsecureServer() throws IOException {
		
		//Setting Contex e Servlet Container for REST Services
		WebappContext unsecureContext = new WebappContext("unsecure_provisioningmanager");
        ServletRegistration unsecure_registration = unsecureContext.addServlet("ServletContainer", ServletContainer.class);
        unsecure_registration.setInitParameter("com.sun.jersey.config.property.packages", REST_ITF_PACKAGE);
		//Add resources to Container
		HttpHandler handler = ContainerFactory.createContainer(HttpHandler.class, new PackagesResourceConfig(REST_ITF_PACKAGE));
		try {
			
			//Create Grizzly Web Server without SSL Context
			unsecureWebServer = GrizzlyServerFactory.createHttpServer(ProvisioningManagerProperties.getUnsecureBaseURI(), handler);

			// deploy Grizzly embedded server //
			unsecureContext.deploy(unsecureWebServer);
	        } catch (Exception ex) {
			logger.error(loggerTag+ex);
		}
		// start Grizzly server //
		unsecureWebServer.start();
		return unsecureWebServer;
	}
	

	public static HttpServer startServer() throws IOException {
		
		final Map<String, String> initParams = new HashMap<String, String>();
		initParams.put("com.sun.jersey.config.property.packages", REST_ITF_PACKAGE);
	
		return GrizzlyWebContainerFactory.create(ProvisioningManagerProperties.getBaseURI(), initParams);
	}

	public static void main(String[] args) throws IOException 
	{	/* NEW MAIN */
		//enable this for ssl debug
		//System.setProperty("javax.net.debug", "ssl"); 
		//Handler fh = new FileHandler("jersey_test.log");
        //Logger.getLogger("").addHandler(fh);
        
        //Logger.getLogger("com.sun.jersey").setLevel(Level.FINEST);
		if (args.length < 1) 
		{
			logger.warn(loggerTag+"No configuration file specified. Using defaults.");
		}
		else
			ProvisioningManagerProperties.setConfigurationFilePath(args[0]);
		
		ProvisioningManagerController.getInstance();
		
		HttpServer httpsServer = startSecureServer();
		
		HttpServer httpServer = startUnsecureServer();
		
		if(ProvisioningManagerProperties.getProperty("VEP_VERSION").equals("0"))
			logger.info(loggerTag+String.format("Secure Provisioning manager started at %s", ProvisioningManagerProperties.getBaseURI()) +
									  ", bounded with VEP Debug ");
		else
			logger.info(loggerTag+String.format("Secure Provisioning manager started at %s", ProvisioningManagerProperties.getBaseURI()) +
						String.format(", bounded with VEP at %s:%s", 
				ProvisioningManagerProperties.getProperty("VEP_IP"), ProvisioningManagerProperties.getProperty("VEP_PORT")));
		

		if(ProvisioningManagerProperties.getProperty("VEP_VERSION").equals("0"))
			logger.info(loggerTag+String.format("Unsecure Provisioning manager started at %s", ProvisioningManagerProperties.getUnsecureBaseURI()) +
									  ", bounded with VEP Debug ");
		else
			logger.info(loggerTag+String.format("Unsecure Provisioning manager started at %s", ProvisioningManagerProperties.getUnsecureBaseURI()) +
				         String.format(", bounded with VEP at %s:%s", 
				ProvisioningManagerProperties.getProperty("VEP_IP"), ProvisioningManagerProperties.getProperty("VEP_PORT")));
		
		while(running){
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		httpsServer.stop();
		httpServer.stop();
		System.exit(0);
	}
}
