package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import java.util.HashMap;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers.ApplicationsData;

public class ApplicationInformation implements Callable<JSONObject> {

	private String ceeIdentifier;
	private String applicationId;
	private String userUuid;
	
	private static Logger logger = Logger.getLogger(ApplicationInformation.class);
	private static String loggerTag = "[ApplicationInformationTask] ";
	public ApplicationInformation(String ceeId, String appId, String uuid){
		this.ceeIdentifier = ceeId;
		this.applicationId = appId;
		this.userUuid = uuid;
		
	}
	
	
	@Override
	public JSONObject call() throws Exception {
		logger.info(loggerTag +"contact VEP for GET application/"+this.applicationId+" for info");
		JSONObject retlist = new JSONObject();
		retlist = VEPconnector.getInstance().getApplication(this.ceeIdentifier, this.applicationId, this.userUuid);
		return retlist;
		
		/*OLD METHOD
		return ApplicationsData.getInstance().retrieve(appID);
		*/
	}

}
