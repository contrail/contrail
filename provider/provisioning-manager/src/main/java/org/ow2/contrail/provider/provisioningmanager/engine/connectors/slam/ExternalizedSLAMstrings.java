package org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam;

public interface ExternalizedSLAMstrings {

	// STANDARD VEP ADDRESSES
	public static final String stdSlamHostName 		= "localhost";
	public static final String stdSlamPortName 		= "8080";
	public static final String SLAM_PROTOCOL_PREFIX	= "http://";
		
	// PATH ELEMENTS
	public static final String GLOBAL_SLAM_SERVICE_PATH	= "WebServiceProject/services/";
	public static final String PARTY_SERVICE_PATH		= "PartyWSHttpSoap11Endpoint";
	public static final String REPORTING_SERVICE_PATH	= "ReportingHttpSoap11Endpoint";
	
	
	// RETURN VAULES
	public static final String SLA_DELIMITER = "<slam\\:SLA.+?</slam\\:SLA>";
	
}
