package org.ow2.contrail.provider.provisioningmanager.engine.monitoring;

public class LocalProviderWatcher 
{

	MonitoringDB mondb;
	
	public boolean init() throws Exception
	{
		mondb = new MonitoringDB();
		
		RabbitReceiver rr = new RabbitReceiver(mondb);
		try {
			rr.consume(null);
		} catch (Exception e) {
			throw new Exception("| RabbitMQ misconfigured or unavailable |");
		}
		
		return true;
	}
	
}
