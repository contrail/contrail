package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.provider.provisioningmanager.client.HttpsClient;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.PmUser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers.ApplicationsData;


public class ApplicationChanges implements Callable<JSONObject> {

	private String appId;
	private JSONObject operation;
	private String userUuid;

	private static Logger logger = Logger.getLogger(ApplicationChanges.class);
	private static String loggerTag = "[ApplicationChangesTask] ";
	
	public ApplicationChanges(String AppId, JSONObject inputBody, String uuid) {
		
		this.appId = AppId;
		this.operation = inputBody;	
		this.userUuid = uuid;
	}

	@Override
	public JSONObject call() throws Exception {
		JSONObject ret = new JSONObject();
		// TODO contact VEP for doing modifications
		String ceeIdentifier = null;
		PmUser userApp = null;
		Application appl = null;
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(this.appId); 
			userApp = appl.getPmUser();
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.error(loggerTag + "Application with " + this.appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		
		
		if(this.appId != null && this.operation != null) {
			String action = operation.getString("action");
			if (action.equals("DEL")){
				logger.info(loggerTag + "Executing delete virtual machine");
				String vmId = operation.getString("VM_ID");
				JSONObject returnStatus = VEPconnector.getInstance().deleteVm(ceeIdentifier, this.appId, vmId, this.userUuid);
				List<Vm> listVm = DBconnector.getInstance().getVmsOfApplication(this.appId);
				for (Vm vm : listVm){
					if(vm.getVmVepId() == Long.parseLong(vmId)){
						DBconnector.getInstance().deleteVm(vm);
						logger.info(loggerTag + "delete vm " + vm.getVmVepId() + " from DB");
					}
				}
				if (returnStatus.get(RETURN_STATUS).equals("200"))  ret.put(RETURN_STATUS,"200");
				else ret.put(RETURN_STATUS,"500");
				logger.info(loggerTag + "delete virtual machine executed");
			} else {
				if (action.equals("ADD")){
					logger.info(loggerTag + "Executing add virtual machine");
					String vsId = operation.getString("VS_ID");
					List<Vm> listVm = DBconnector.getInstance().getVmsOfApplication(this.appId);
					int vmNum = operation.getInt("number");
					if (vmNum == listVm.size()){
						logger.info(loggerTag + "Nothing To Do: All Vms are already deploied");
						ret.put(RETURN_STATUS,"200");
						return ret;
					} else {
						if (vmNum <  listVm.size()) {
							logger.error(loggerTag + "Wrong Vms number in request");
							ret.put(RETURN_STATUS,"500");
						} else{
							logger.info(loggerTag + "recreating Deployment Document for Application: " + this.appId);
							
							JSONObject deploymentDocument = new JSONObject();
							JSONArray vms = new JSONArray();
							int vmIterationCount = 1;
							for(int i=0; i<listVm.size();i++){
								JSONObject vm = new JSONObject();
								vm.put("name", listVm.get(i).getVirtualSystem() + "-" + vmIterationCount);
								vm.put("virtualSystem", new JSONObject().put("href", listVm.get(i).getVirtualSystem()));
								JSONObject context = new JSONObject();
								vm.put("contextualization", context);
								vms.put(vm);
								vmIterationCount++;
							}
							for(int i=0; i< vmNum - listVm.size(); i++){
								JSONObject vm = new JSONObject();
								vm.put("name", listVm.get(i).getVirtualSystem() + "-" + vmIterationCount);
								vm.put("virtualSystem", new JSONObject().put("href", listVm.get(i).getVirtualSystem()));
								JSONObject context = new JSONObject();
								vm.put("contextualization", context);
								vms.put(vm);
								vmIterationCount++;
							}
							deploymentDocument.put("VMs", vms);
							logger.info(deploymentDocument);
							logger.info(loggerTag + "add virtual machine executed");
							JSONObject returnDepStatus = VEPconnector.getInstance().deployApplication(ceeIdentifier, this.appId, deploymentDocument, userApp.getUserUuid());
							if (returnDepStatus.get(RETURN_STATUS).equals("200")){
								
								ret.put(RETURN_STATUS,"200");
							}
							else ret.put(RETURN_STATUS,"500");
						}
					}
				}
			}
		} else ret.put(RETURN_STATUS,"500");
		
		return ret;
	}

}
