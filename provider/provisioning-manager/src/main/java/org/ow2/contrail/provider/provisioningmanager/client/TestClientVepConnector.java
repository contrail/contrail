package org.ow2.contrail.provider.provisioningmanager.client;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEP2connector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.ProvisioningManagerController;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationReservationCeck;
import org.ow2.contrail.provider.provisioningmanager.stubs.VEPDebugConnector;

import com.sun.jersey.api.client.UniformInterfaceException;

public class TestClientVepConnector {

	/**
	 * @param args
	 * @throws JSONException 
	 * @throws UniformInterfaceException 
	 */
	public static void main(String[] args) throws UniformInterfaceException, JSONException {
		/*
		JSONObject getResponse = VEPDebugConnector.getInstance().getApplication("50", "49");
		System.out.println(getResponse);
		JSONArray vms_info=getResponse.optJSONArray("VMs");
		JSONArray vms_id = new JSONArray();
		for (int i=0;i<vms_info.length();i++){
			String[] VmId = ((JSONObject)vms_info.get(i)).getString("href").split("/");
			vms_id.put(VmId[VmId.length-1]);
		}
		
		JSONObject returnStatus = new JSONObject();
		returnStatus.put(VM_IDS, vms_id);
		for (int i=0;i<vms_id.length();i++){
			System.out.println(vms_id.get(i));
			JSONObject getResponse2 = VEPDebugConnector.getInstance().getVm("50","49", (String) vms_id.get(i),"fgaudenzi");
			System.out.println(getResponse2);
		}
		
		JSONObject returnStatus = new JSONObject();
		JSONObject getApp = VEPconnector.getInstance().getApplication("50", "49");
		JSONArray vms_info=getApp.optJSONArray("VMs");
		JSONArray vms_ids = new JSONArray();
		for (int i=0;i<vms_info.length();i++){
			String[] VmId = ((JSONObject)vms_info.get(i)).getString("href").split("/");
			String vmId =VmId[VmId.length-1];
			JSONObject getVm = VEPconnector.getInstance().getVm("50", "49", vmId, "user");
			String vSystem = ((JSONObject) getVm.get("virtualSystem")).getString("href");
			JSONArray vm_vs = new JSONArray();
			vm_vs.put(vmId);
			vm_vs.put(vSystem);
			vms_ids.put(vm_vs);
			
			//Store Vm in the DB
			Vm vmToStore = new Vm();
			vmToStore.setVmVepId(Long.parseLong(vmId));
			//vmToStore.setApplication(DBconnector.getInstance().getApplicationEntitybyID("140"));
			vmToStore.setVirtualSystem(vSystem);
			vmToStore.setStatus(((JSONObject)vms_info.get(i)).getString("state"));
			DBconnector.getInstance().storeObject(vmToStore);
			System.out.println("VM Store in DB:\n" +vmToStore);
		}
		returnStatus.put(VM_IDS, vms_ids);
		System.out.println(returnStatus);
		*/
		JSONObject appReservationReturn= appReservationCheck("9","140");
		System.out.println(appReservationReturn);
		
		
	}
	
	public static JSONObject appReservationCheck(String ceeId, String appId){
		JSONObject ret = new JSONObject();
		ApplicationReservationCeck reservationCeck= new ApplicationReservationCeck(appId, ceeId);
		Future<JSONArray> retValue = ProvisioningManagerController.getInstance().exec(reservationCeck);
		try {
			try {
				ret = ret.put("reservation", retValue.get());
			} catch (JSONException e) {	e.printStackTrace(); }
		} catch (InterruptedException ex) {
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e) { e.printStackTrace();}
		} catch (ExecutionException ex) {
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e) { e.printStackTrace();}
		}
		try { ret.put(RETURN_STATUS, "200");
		} catch (JSONException e) {	e.printStackTrace(); }
		
		return ret;
	
	}

}
