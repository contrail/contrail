package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

public class ApplicationReservationCeck implements Callable<JSONArray> {

	private String ceeIdentifier;
	private String applicationId;
	private String userUuid;
	private static Logger logger = Logger.getLogger(ApplicationReservationCeck.class);
	private static String loggerTag = "[ApplicationReservationCeckTask] ";
	public ApplicationReservationCeck(String appId, String uuid){
		this.userUuid = uuid;
		this.applicationId=appId;
		Application application = DBconnector.getInstance().getApplicationEntitybyID(applicationId);
		this.ceeIdentifier=application.getCeeId();
	}
	
	public ApplicationReservationCeck(String appId, String uuid, String ceeId){
		this.userUuid = uuid;
		this.applicationId=appId;
		this.ceeIdentifier=ceeId;
	}
	
	@Override
	public JSONArray call() throws Exception {
		logger.info(loggerTag + "Entering Application Reservation Ceck");
		logger.info(loggerTag + "GET Application: " + this.applicationId);
		JSONObject getAppResponse = VEPconnector.getInstance().getApplication(this.ceeIdentifier,this.applicationId,this.userUuid);
		logger.info(loggerTag + getAppResponse);
		JSONArray reservations = getAppResponse.getJSONObject("reservation").getJSONArray("reservations");
		logger.info(loggerTag + "reservation for App: "+ this.applicationId +"\n"+reservations);
		return reservations;
	}

}
