package org.ow2.contrail.provider.provisioningmanager.RESTitf;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.ow2.contrail.provider.provisioningmanager.Main;



import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.*;

@Path("/"+PROVISIONING_MGMT_PATH)
public class ProvisioningManagement {
	
	@PUT
	@Path(STOP_PROVISIONING_PATH)
	@Consumes("text/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized void submitApp(String inputBody){
	
		if(inputBody.equalsIgnoreCase("prov-stop")){
			Main.running = false;
		}
		
	}
		
	

}
