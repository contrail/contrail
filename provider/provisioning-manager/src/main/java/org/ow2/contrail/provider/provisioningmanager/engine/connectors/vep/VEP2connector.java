package org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.USERGROUPS_VALUE;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.USERROLE_VALUE;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.USERVID_VALUE;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.VEP_PROTOCOL_PREFIX;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.VEP_PROTOCOL_PREFIX_SECURE;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.stdVepHostName;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.stdVepPortName;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.VEP_IP;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.VEP_PORT;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.crypto.dsig.XMLObject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualSystem;
import org.ow2.contrail.provider.provisioningmanager.stubs.VEPDebugConnector;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

/**
 * NEW CLASS: Class to use interface of the VEP2.0 through (Https) secure connection.
 * @author Marco Distefano
 *
 */

public class VEP2connector extends VEPconnector {
	/**
     * static Singleton instance
     */
    private static String VEP_URI = null;

  	//path and password of the keystore of the federation-core component
  	private static final String keystore_path = "src/main/resources/pm1.pfx";
  	private static final String keystore_password = "contrail";
  	//SSLContext for https connection to provisioning manager
  	private static SSLContext context = null;
  	private static HTTPSProperties prop = null;
  	private static Client client = null;
  	
  	private static Logger logger = Logger.getLogger(VEP2connector.class);
    private static String loggerTag = "[VEP2Connector] ";
    /**
     * Private constructor for singleton
     */
    public VEP2connector() {
    	String loggerMethodTag = loggerTag + "Inizialize Driver: ";
    	try {
    		//setting client truststore and keystore 
    		TrustManager mytm[] = null;
            KeyManager mykm[] = null;
            try {
            	//Create a trust manager that does not validate certificate chains:
            	 mytm = new TrustManager[]{new MyX509TrustManager()};
            	 mykm = new KeyManager[]{new MyX509KeyManager(keystore_path, keystore_password.toCharArray())};
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
         // Create all-trusting host name verifier
         HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					System.out.println("Warning: URL Host: " + urlHostName + " vs. "
		                    + session.getPeerHost());
					return true;
				}
            };
          logger.info(loggerMethodTag + "Set up truststore and keystore of the provisioning-manager (client-code) component");
          
          //setting SSL context and client
            try {
                context = SSLContext.getInstance("SSL");
                //context.init(null, mytm, null);
                context.init(mykm, mytm, null);
            } catch (NoSuchAlgorithmException nae) {
                nae.printStackTrace();
            } catch (KeyManagementException kme) {
                kme.printStackTrace();
            }
            prop = new HTTPSProperties(allHostsValid, context);
            client = Client.create();
            logger.info(loggerMethodTag + "Set up client with SSL context for the communication with VEP");
            
		} catch (Exception e) {
	            e.printStackTrace();
	        }    
            
        String HOSTNAME = ProvisioningManagerProperties.getProperty(VEP_IP);
        if (HOSTNAME.trim().isEmpty()) {
            try {
                HOSTNAME = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                HOSTNAME = stdVepHostName;
            }
        }

        String HOSTPORT = ProvisioningManagerProperties.getProperty(VEP_PORT);
        if (HOSTPORT.trim().isEmpty()) { 
            HOSTPORT = stdVepPortName;
        }
        
		//VEP uri builded
        VEP_URI = VEP_PROTOCOL_PREFIX_SECURE + HOSTNAME + ":" + HOSTPORT + "/api/cimi/cee/";
        
    }

    public String getVEP_URI() {
        return VEP_URI;
    }
    
     
    @Override
    public JSONObject getApplications(String ceeIdentifier, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "getApplications: ";
        // Submit to VEP
        JSONObject ret = new JSONObject(); 
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/applications");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.header("X-Certificate", "");
        
        try {
			logger.info(loggerMethodTag + "executing GET : "+VEP_URI+ceeIdentifier+"/applications");
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "GET");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
			
	        //execute the request
			ClientResponse answer = client.handle(clientRequest);
	        
			//get answer body
			ret = answer.getEntity(JSONObject.class);
			//ret.put("status", answer.getStatus());
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET /applications "+ e1);
		}
        return ret;
    }
    
    @Override
    public JSONObject getApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "getApplication: ";
        // Submit to VEP
        JSONObject ret = null;
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appId); 
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.header("X-Certificate", "");
        
        try {
        	logger.info(loggerMethodTag +"executing GET "+VEP_URI+ceeIdentifier+"/application/"+appId);
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "GET");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			
	        //execute the request
			ClientResponse answer = client.handle(clientRequest);
	        
			//get answer body
			ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET ../application/"+appId+ "\nError: "+ e1);
		}
        return ret;
    }

    @Override
    public JSONObject deployApplication(String ceeIdentifier, String appId, JSONObject deploymentDocument, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "deployApplication: ";
        // Submit to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appId);
        logger.info(loggerMethodTag +"POST TO "+VEP_URI+ceeIdentifier+"/application/"+appId+ " with:\n");
        logger.info(loggerMethodTag +"DeploymentDocument: "+deploymentDocument);
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.header("X-Certificate", "");
        requestBuilder.type("application/json");
        requestBuilder.entity(deploymentDocument);
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        
			//get answer body
			logger.info(loggerMethodTag + "return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put(RETURN_STATUS, answer.getStatus()+"");
			return ret; 
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on POST ../application/"+appId+ "\nError: "+ e1);
		}
        return ret; 
    }
    @Override
    public JSONObject startApplication(String ceeIdentifier, String appID, String userUuid) throws JSONException, UniformInterfaceException {
    	String loggerMethodTag = loggerTag + "startApplication: ";
        // Start to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appID+"/action/start");
        logger.debug(loggerMethodTag +"POST TO "+VEP_URI+ceeIdentifier+"/application/"+appID+ "/action/start  with:\n");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.entity("");
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	       
	        //execute the request
			answer = client.handle(clientRequest);
	        //put the return status in the JSON Response
			logger.info(loggerMethodTag + "return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put(RETURN_STATUS, answer.getStatus()+"");
			//get answer body
			//ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET /applications "+ e1);
		}
        return ret;
    }
    
    public JSONObject stopApplication(String ceeIdentifier, String appID, String userUuid) {
    	String loggerMethodTag = loggerTag + "stopApplication: ";
        // Stop to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appID+"/action/stop");
        logger.debug(loggerMethodTag +"POST TO "+VEP_URI+ceeIdentifier+"/application/"+appID+ "/action/stop  with:\n");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.entity("");
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        //put the return status in the JSON Response
			logger.info(loggerMethodTag +"return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put("status", answer.getStatus()+"");
			//get answer body
			//ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET /applications "+ e1);
		}
        return ret;
    }
    
    @Override
    public JSONObject deleteApplication(String ceeIdentifier, String appID, String userUuid) {
    	String loggerMethodTag = loggerTag + "deleteApplication: ";
        // Delete APP to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appID+"/action/delete");
        logger.debug(loggerMethodTag +"POST TO "+VEP_URI+ceeIdentifier+"/application/"+appID+ "/action/delete  with:\n");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.entity("");
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        //put the return status in the JSON Response
			logger.info(loggerMethodTag +"return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put("status", answer.getStatus()+"");
			//get answer body
			//ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET /applications "+ e1);
		}
        return ret;
    }
    
    @Override
    public JSONObject getVm(String ceeIdentifier, String appId, String vmId, String userUuid) throws JSONException{
    	String loggerMethodTag = loggerTag + "getVm: ";
        // GetVm to VEP
        JSONObject ret = null;
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appId+"/vm/"+vmId); 
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.header("X-Certificate", "");
        
        try {
        	logger.debug(loggerMethodTag+"executing GET "+VEP_URI+ceeIdentifier+"/application/"+appId+"/vm/"+vmId);
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "GET");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
			
	        //execute the request
			ClientResponse answer = client.handle(clientRequest);
	        
			//get answer body
			ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET ../application/"+appId+ "\nError: "+ e1);
		}
        return ret;
    }
    
    @Override
    public JSONObject stopVm(String ceeIdentifier, String appID, String vmId, String userUuid) {
    	String loggerMethodTag = loggerTag + "stopVm: ";
        // StopVm to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appID+"/vm/"+vmId+"/action/stop");
        logger.info(loggerMethodTag+"POST TO "+VEP_URI+ceeIdentifier+"/application/"+appID+ "/action/stop  with:\n");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.entity("");
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        //put the return status in the JSON Response
			logger.info(loggerMethodTag+"return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put("status", answer.getStatus()+"");
			//get answer body
			//ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag + "generic error on GET /applications "+ e1);
		}
        return ret;
    }
    
    @Override
    public JSONObject deleteVm(String ceeIdentifier, String appID, String vmId, String userUuid) {
    	String loggerMethodTag = loggerTag + "deleteVm: ";
        // Submit to VEP
        JSONObject ret = new JSONObject();
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appID+"/vm/"+vmId);
        logger.debug(loggerMethodTag+"DELETE TO "+VEP_URI+ceeIdentifier+"/application/"+appID+ "/vm/"+vmId+"  with:\n");
        requestBuilder.header("X-Username", userUuid);
        requestBuilder.entity("");
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "DELETE");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        //put the return status in the JSON Response
			logger.info(loggerMethodTag +"return status: "+RETURN_STATUS+": "+answer.getStatus());
			ret.put("status", answer.getStatus()+"");
			//get answer body
			//ret = answer.getEntity(JSONObject.class);
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			logger.error(loggerMethodTag + error + "\n Error return: " + e1);
		} catch (Exception e1){
			logger.error(loggerMethodTag +"generic error on GET /applications "+ e1);
		}
        return ret;
    }
    /*
    @Override
    public JSONObject submitApplication(String ceeIdentifier, String appId, JSONObject deploymentDocument) throws JSONException, UniformInterfaceException {

        // Submit to VEP
        JSONObject ret = null;
        ClientRequest.Builder requestBuilder = ClientRequest.create();
        UriBuilder requestUri = UriBuilder.fromUri(VEP_URI+ceeIdentifier+"/application/"+appId);
        requestBuilder.header("X-Username", "fgaudenzi");
        requestBuilder.header("X-Certificate", "");
        requestBuilder.type("application/json");
       
        System.out.println("JSON BODY POST:\n" + deploymentDocument.toString());
        requestBuilder.entity(deploymentDocument);
        ClientResponse answer=null;
        try {
	        //create request
	        ClientRequest clientRequest = requestBuilder.build(requestUri.build(), "POST");
	        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
	        
	        //execute the request
			answer = client.handle(clientRequest);
	        
			//get answer body
			Status status = answer.getClientResponseStatus();
			System.out.println("status: "+status);
			ret.put("responseStatus", status.toString());
			
        } catch (ClientHandlerException e1){
			String error = "Unable to contact VEP2\n";
			System.out.println("[VEP2 CONNECTOR] "+ error + "\n Error return: " + e1);
		} catch (Exception e1){
				System.out.println("[VEP2 CONNECTOR]: generic error on POST ../application/"+appId+ "\nError: "+ e1);
		}
        return ret; 
    }
    */
    
    public String getOVFApplication(String ceeIdentifier, String ceeId, String appId) throws JSONException, UniformInterfaceException {
    	//GET /ceeId/application/ "OVFs":[{"href":"\/api\/cimi\/cee\/94\/application\/141\/OVF\/119"}]
        return null;
    	
    }
    
    
    
    static class MyX509TrustManager implements TrustManager,  X509TrustManager {

        /*
         * The default PKIX X509TrustManager9.  We'll delegate
         * decisions to it, and fall back to the logic in this class if the
         * default X509TrustManager doesn't trust it.
         */
    	X509TrustManager pkixTrustManager;
    	
        public  java.security.cert.X509Certificate[] getAcceptedIssuers()
        {
            return null;
        }
 
        public boolean isServerTrusted(
                java.security.cert.X509Certificate[] certs)
        {
            return true;
        }
 
        public boolean isClientTrusted(
                java.security.cert.X509Certificate[] certs)
        {
            return true;
        }
 
        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException
        {
            return;
        }
 
        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException
        {
            return;
        }
        /*
         * This costructor have to be enable to configure properly the trustmanager 
         */
        /*
        MyX509TrustManager(String trustStore, char[] password) throws Exception {
            this(new File(trustStore), password);
        }

        MyX509TrustManager(File trustStore, char[] password) throws Exception {
            // create a "default" JSSE X509TrustManager.

            KeyStore ks = KeyStore.getInstance("PKCS12");

            ks.load(new FileInputStream(trustStore), password);

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKCS12");
            tmf.init(ks);

            TrustManager tms [] = tmf.getTrustManagers();

            /*
             * Iterate over the returned trustmanagers, look
             * for an instance of X509TrustManager.  If found,
             * use that as our "default" trust manager.
            
            for (int i = 0; i < tms.length; i++) {
                if (tms[i] instanceof X509TrustManager) {
                    pkixTrustManager = (X509TrustManager) tms[i];
                    return;
                }
            }
			
            /*
             * Find some other way to initialize, or else we have to fail the
             * constructor.
             
            throw new Exception("Couldn't initialize");
        } */
    }

    /**
    * Inspired from http://java.sun.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html
    *
    */
    static class MyX509KeyManager implements X509KeyManager {

        /*
         * The default PKIX X509KeyManager.  We'll delegate
         * decisions to it, and fall back to the logic in this class if the
         * default X509KeyManager doesn't trust it.
         */
        X509KeyManager pkixKeyManager;

        MyX509KeyManager(String keyStore, char[] password) throws Exception {
            this(new File(keyStore), password);
        }

        MyX509KeyManager(File keyStore, char[] password) throws Exception {
            // create a "default" JSSE X509KeyManager.

            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(new FileInputStream(keyStore), password);

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509", "SunJSSE");
            kmf.init(ks, password);

            KeyManager kms[] = kmf.getKeyManagers();

            /*
             * Iterate over the returned keymanagers, look
             * for an instance of X509KeyManager.  If found,
             * use that as our "default" key manager.
             */
            for (int i = 0; i < kms.length; i++) {
                if (kms[i] instanceof X509KeyManager) {
                    pkixKeyManager = (X509KeyManager) kms[i];
                    return;
                }
            }
            /*
             * Find some other way to initialize, or else we have to fail the
             * constructor.
             */
            throw new Exception("Couldn't initialize KeyManager");
        }

       public PrivateKey getPrivateKey(String arg0) {
           return pkixKeyManager.getPrivateKey(arg0);
       }

       public X509Certificate[] getCertificateChain(String arg0) {
           return pkixKeyManager.getCertificateChain(arg0);
       }

       public String[] getClientAliases(String arg0, Principal[] arg1) {
           return pkixKeyManager.getClientAliases(arg0, arg1);
       }

       public String chooseClientAlias(String[] arg0, Principal[] arg1, Socket arg2) {
           return pkixKeyManager.chooseClientAlias(arg0, arg1, arg2);
       }

       public String[] getServerAliases(String arg0, Principal[] arg1) {
           return pkixKeyManager.getServerAliases(arg0, arg1);
       }

       public String chooseServerAlias(String arg0, Principal[] arg1, Socket arg2) {
           return pkixKeyManager.chooseServerAlias(arg0, arg1, arg2);
       }
    }
    
    
}
