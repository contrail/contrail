package org.ow2.contrail.provider.provisioningmanager.engine.monitoring;

import java.io.IOException;

import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;
import org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;


public class RabbitReceiver {
	public static final String EXCHANGE_NAME = "input";
	public static final String ROUTING_KEY_PREFIX = "input";
	public static final boolean DURABLE_FLAG = false;
	public static final boolean AUTO_DELETE_FLAG = true;
	public static final boolean INTERNAL_FLAG = false;
	public static final String TOPIC = "topic";

	
	MonitoringDB mondb;
	
	public RabbitReceiver(MonitoringDB mondb)
	{
		this.mondb = mondb;
	}
	
	public void consume(String[] argv) throws Exception {

		String HOSTNAME = ProvisioningManagerProperties.getProperty(PropertiesName.RABBITMQ_IP);
		String stdRabbitMQhostname = "localhost";
		if(HOSTNAME.trim().isEmpty()){
			HOSTNAME = stdRabbitMQhostname;
		}
		
		// creating connection to the rabbitmq provider
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOSTNAME);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, TOPIC, DURABLE_FLAG, AUTO_DELETE_FLAG, INTERNAL_FLAG, null);
		
		String queueName = channel.queueDeclare().getQueue();
		String key = "input.#";
		channel.queueBind(queueName, EXCHANGE_NAME, key);
	
		boolean autoAck = false;
		ContrailConsumer cc = this.new ContrailConsumer(channel);
		
		channel.basicConsume(queueName, autoAck, cc);
		
	}

	private class ContrailConsumer implements Consumer {

		private Channel channel;
		
		public ContrailConsumer(Channel channel){
			this.channel=channel;
		}
		@Override
		public void handleCancel(String arg0) throws IOException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void handleCancelOk(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void handleConsumeOk(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void handleDelivery(String arg0, Envelope envelope,
				BasicProperties properties, byte[] arg3) throws IOException 
		{
			//String routingKey = envelope.getRoutingKey();
			//String contentType = properties.getContentType();
			long deliveryTag = envelope.getDeliveryTag();
			
			String message = new String(arg3);
			
			// saving the message...
			//System.out.println(message);
			mondb.registerEntry(message);
			
			// (process the message components here ...)
			channel.basicAck(deliveryTag, false);
			
		}

		@Override
		public void handleRecoverOk(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void handleShutdownSignal(String arg0,
				ShutdownSignalException arg1) {
			// TODO Auto-generated method stub
			
		}

	}
}
