package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db;

import java.util.List;

import javax.persistence.*;

import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Ovf;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.PmUser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.VepTemplate;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.ExternalizedDBstrings.*;

public class DBconnector {

	/**
	 * static Singleton instance 
	 */
	private static DBconnector instance;
	private EntityManager em;

	/**
	 * Private constructor for singleton
	 */
	private DBconnector() {
		
			em = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
			em.setFlushMode(FlushModeType.COMMIT);
	}

	/**
	 * Static getter method for retrieving the singleton instance
	 */
	public static DBconnector getInstance() {
		if (instance == null) {
			instance = new DBconnector();
		}

		return instance;
	}
	
	public Object storeObject(Object item){
		
		EntityTransaction transaction = em.getTransaction();
	    transaction.begin();
	    em.persist(item);
        transaction.commit();
     
		return item;
	}
	
	public boolean updateObject(Object item){
		
		em.getTransaction().begin();
	    em.persist(item);
	    em.getTransaction().commit();
     
		return true;
	}
	
	public Application retrieveObject(Application item){
	
		return em.find(Application.class, item.getId());
	}
	
	public<T> T retrieveObjectByClass(Class<T> item, Object key){
		
		return em.find(item, key);
	}

	public PmUser retriveUserByUuid(String uuid){
		TypedQuery<PmUser> q = em.createQuery("SELECT u FROM PmUser u WHERE u.userUuid='"+uuid+"'", PmUser.class);
		return q.getSingleResult();
		
	}
	public List<Long> getAllApplicationEntityIDs(){

		TypedQuery<Long> q = em.createQuery("SELECT ae.id FROM Application ae", Long.class);
		return q.getResultList();
		
	}
	
//	public List<Ovf> getUserOVFlist(String uname){
//
//		TypedQuery<Long> q = em.createQuery("SELECT ae.id FROM Application ae Ovf o " +
//				"where ae.pmUser = '" + , Long.class);
//		return q.getResultList();
//		
//	}
	
	public Application getApplicationEntityIDbyOvfID(String uname, String ovfid){
		TypedQuery<Application> q;
		try{
			q = em.createQuery("SELECT ae FROM Application ae, Ovf o, PmUser p where " +
					"p.username =  '" + uname + "' AND p.applications = ae AND o.applications = ae AND o.ovfVepId =  " + Integer.parseInt(ovfid), Application.class);
			return q.getSingleResult();
		}catch (NoResultException e) {
			// TODO: handle exception
			return null;
		}
		
	}
	
	public Application getApplicationEntitybyID(String appID){

		TypedQuery<Application> q = em.createQuery("SELECT ae FROM Application ae where ae.id = " + appID, Application.class);
		
		return q.getSingleResult();
		
	}
	
	public Application getApplicationEntityByVepId(String vepAppID){

		TypedQuery<Application> q = em.createQuery("SELECT ae FROM Application ae where ae.vepAppId = " + vepAppID, Application.class);
		return q.getSingleResult();
		
	}
	
	public List<VepTemplate> retrieveApplicationTemplates(String appID) {
		
		Application app = em.find(Application.class, Long.parseLong(appID));
		
		return app.getOvf().getVepTemplates();
		
	}
	
	public List<Vm>  getVmsOfApplication(String vepAppID){
		TypedQuery<Vm> q = em.createQuery("SELECT vm FROM Vm vm where vm.Application.vepAppId = " + vepAppID, Vm.class);
		return q.getResultList();
	}
	
	/*public void deleteVm(String vmId){
		
		Vm vm = em.find(Vm.class, Long.parseLong(vmId));
		em.remove(vm);
	}*/
	
	public void deleteVm(Vm vm){
		
		em.remove(vm);
	}
}
