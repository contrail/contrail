package org.ow2.contrail.provider.provisioningmanager.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;

public class AuthzTest {

	private static String filename="./src/main/resources/cert.cer";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String orig = "original String before base64 encoding in Java";

        
		//encoding  byte array into base 64
        //byte[] encoded = Base64.encodeBase64(orig.getBytes());     
      
        //System.out.println("Original String: " + orig );
        //System.out.println("Base64 Encoded String : " + new String(encoded));
      
        //decoding byte array into base64
        //byte[] decoded = Base64.decodeBase64(encoded);      
        //System.out.println("Base 64 Decoded  String : " + new String(decoded));

		X509Certificate certificate = getCertificateFromFile(filename);
		System.out.println(certificate.toString());
		System.out.println(certificate.getSerialNumber());
		try {
			String encodedCert = serializeCert(certificate);
			System.out.println(encodedCert);
			
			byte[] decoded = Base64.decodeBase64(encodedCert);
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			InputStream in = new ByteArrayInputStream(decoded);
			
			X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);
			String[] splittedSubject = ((String) cert.getSubjectAlternativeNames().iterator().next().get(1)).split(":");
			if(splittedSubject[1].equals("uuid")) System.out.println(splittedSubject[2]);
			else System.out.println("no uuid found");
		} catch (CertificateEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	protected static X509Certificate getCertificateFromFile(String fileName){
		X509Certificate cert = null;
		try {
			InputStream inStream = new FileInputStream(fileName);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			cert = (X509Certificate)cf.generateCertificate(inStream);
			inStream.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return cert;
	}
	
	protected static String serializeCert(X509Certificate cert) throws CertificateEncodingException{
		//return new String(cert.getEncoded());
		//return Base64.encodeBase64(cert.getEncoded());
		return new String(Base64.encodeBase64(cert.getEncoded()));
	}
	

}
