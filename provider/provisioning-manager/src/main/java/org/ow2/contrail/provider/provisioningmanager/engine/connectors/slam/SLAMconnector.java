package org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam.ExternalizedSLAMstrings.stdSlamHostName;
import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.slam.ExternalizedSLAMstrings.stdSlamPortName;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SLAMconnector {

	private static final String STD = "I";

	/**
	 * static Singleton instance 
	 */
	private static SLAMconnector instance;
	
	// private PartyWSSoap11BindingStub partySOAPcall;
	// private ReportingSoap11BindingStub reportingSOAPcall;

	/**
	 * Private constructor for singleton
	 */
	private SLAMconnector() {

		// String SLAM_HOSTNAME = ProvisioningManagerProperties.getProperty(SLAM_IP);
		String SLAM_HOSTNAME = "";
		if(SLAM_HOSTNAME.trim().isEmpty()){
			try {
				SLAM_HOSTNAME = InetAddress.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				SLAM_HOSTNAME = stdSlamHostName;
			}
		}
		
		// String SLAMPORT = ProvisioningManagerProperties.getProperty(SLAM_PORT);
		String SLAMPORT = "";
		if(SLAMPORT.trim().isEmpty()){
			SLAMPORT = stdSlamPortName;
		}
		
		/*
		try {
			partySOAPcall = new PartyWSSoap11BindingStub();
			partySOAPcall._setProperty(PartyWSSoap11BindingStub.ENDPOINT_ADDRESS_PROPERTY, SLAM_PROTOCOL_PREFIX+SLAM_HOSTNAME+":"+SLAMPORT+"/"+GLOBAL_SLAM_SERVICE_PATH+PARTY_SERVICE_PATH);
			
			reportingSOAPcall = new ReportingSoap11BindingStub();
			reportingSOAPcall._setProperty(PartyWSSoap11BindingStub.ENDPOINT_ADDRESS_PROPERTY, SLAM_PROTOCOL_PREFIX+SLAM_HOSTNAME+":"+SLAMPORT+"/"+GLOBAL_SLAM_SERVICE_PATH+REPORTING_SERVICE_PATH);
			
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
	}

	/**
	 * Static getter method for retrieving the singleton instance
	 */
	public static SLAMconnector getInstance() {
		if (instance == null) {
			instance = new SLAMconnector();
		}

		return instance;
	}

	/*
	public CreatePartyResponseType registerSLAUser(Integer slaID) {
		
		CreatePartyResponseType ret = null;
		
		PartyType party = new PartyType();
		party.setPartyId(slaID);
		
		try {
			ret = partySOAPcall.createParty(STD, party);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	*/
	
	public String[] getUserSLAs(Integer slaID){
		
		/*
		try {
			
			String ret = reportingSOAPcall.getSLAbyUserID(Integer.toString(slaID));
		
			Pattern slaPattern = Pattern.compile(SLA_DELIMITER);
			Matcher slaMatcher = slaPattern.matcher(ret);
		
			ArrayList<String> slaStrs = new ArrayList<String>();
			while (slaMatcher.find()) {
				slaStrs.add(slaMatcher.group());
			} 

			return slaStrs.toArray(new String[slaStrs.size()]);
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		
		return null;
		
	}
}
