package org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers.ExternalizedManagedDataStrings.OVF_ID;
import static org.ow2.contrail.provider.provisioningmanager.utils.PropertiesName.*;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.VepTemplate;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers.ApplicationsData;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationSubmission;
import org.ow2.contrail.provider.provisioningmanager.stubs.VEPDebugConnector;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;

/**
 * CHANGES: modified getInstance() method to instantiate proper VEPConnector
 * 			added getApplications() method
 * @author: Marco
 */
public class VEPconnector {

    /**
     * static Singleton instance
     */
    private static VEPconnector instance = null;
    private static String VEP_URI = null;
    private static Logger logger = Logger.getLogger(VEPconnector.class);
    /**
     * Private constructor for singleton
     */
    protected VEPconnector() {

        String HOSTNAME = ProvisioningManagerProperties.getProperty(VEP_IP);
        if (HOSTNAME.trim().isEmpty()) {
            try {
                HOSTNAME = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                HOSTNAME = stdVepHostName;
            }
        }

        String HOSTPORT = ProvisioningManagerProperties.getProperty(VEP_PORT);
        if (HOSTPORT.trim().isEmpty()) {
            HOSTPORT = stdVepPortName;
        }

        VEP_URI = VEP_PROTOCOL_PREFIX + HOSTNAME + ":" + HOSTPORT;

    }

    public String getVEP_URI() {
        return VEP_URI;
    }

    /**
     * Static getter method for retrieving the singleton instance
     */
    public static VEPconnector getInstance() {
    	int VEP_VERS = Integer.parseInt(ProvisioningManagerProperties.getProperty(VEP_VERSION));
    	if (instance == null) {
    		switch (VEP_VERS)
    		{
        		case 0:
        		{
        			instance = new VEPDebugConnector();
        			break;
        		}
        		case 1:
        		{
        			instance = new VEPconnector();
        			break;
        		}
        		case 2:
        		{
        			instance = new VEP2connector();
        			break;
        		}
    		}
	 }
    return instance;
    }

    
    public void registerUser(String username, String uuid) {
    	
    	
        try { 

            JSONObject jobject = new JSONObject();
            jobject.put(USERVID_VALUE, uuid);

            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource service = client.resource(VEP_URI + "/" + USER_PATH + "/" + username);

            service.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header(USERNAME_HEADER, uuid)
                    .put(jobject);

        } catch (JSONException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    //add @author: Marco
    public JSONObject getApplications(String ceeIdentifier, String userUuid) throws JSONException, UniformInterfaceException {
		
    	logger.warn("Wrong VEP Connector");
    	return null;
    }
    
    //add @author: Marco
    public JSONObject getApplication(String ceeIdentifier, String appId, String userUuid) throws JSONException, UniformInterfaceException {
		
    	logger.warn("Wrong VEP Connector");
    	return null;
    	
    }

    public JSONObject startApplication(String ceeIdentifier, String appID, String userUuid) throws JSONException, UniformInterfaceException { 

        // Submit to VEP
        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        HashMap<String, String> appData = ApplicationsData.getInstance().retrieve(Integer.parseInt(appID));
        String ovfID = appData.get(OVF_ID);

        WebResource service =
                client.resource(VEP_URI + "/" + OVF_PATH + "/" + ID_PATH + "/" + ovfID + "/" + ACTION_PATH + "/" + DEPLOY_OP);

        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .header(USERNAME_HEADER, userUuid);

        ret = buildConn.put(JSONObject.class);

        return ret;
    }
    
    public JSONObject submitApplication(String ceeIdentifier, String appId, JSONObject deploymentDocument, String userUuid) throws JSONException, UniformInterfaceException {
    	logger.warn("Wrong VEP Connector");
    	return null;
    }
    public JSONObject deployApplication(String ceeIdentifier, String appId, JSONObject deploymentDocument, String userUuid) throws JSONException, UniformInterfaceException {
    	logger.warn("Wrong VEP Connector");
    	return null;
    }
    
    public JSONObject deleteApplication(String ceeIdentifier, String appID, String userUuid) throws JSONException, UniformInterfaceException {
    	logger.warn("Wrong VEP Connector");
    	return null;
    }
    public JSONObject submitOVF(String appName, String ovfXmlDescr, String user) {

        // Submit OVF to VEP
        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(VEP_URI + "/" + OVF_PATH + "/" + appName);

        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_XML)
                .header(USERNAME_HEADER, user);

        ret = buildConn.put(JSONObject.class, ovfXmlDescr);

        return ret;

    }

    public JSONObject initializeOVF(String ovfID, String user) {

        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        WebResource service = client.resource(VEP_URI + "/" + OVF_PATH + "/" + ID_PATH + "/" + ovfID + "/" + ACTION_PATH + "/" + INITIALIZE_OP);
        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .header(USERNAME_HEADER, user);

        ret = buildConn.put(JSONObject.class);

        return ret;
    }
    
    public JSONObject getVMinfo(int VMid, String user){
    
        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
    
         WebResource service = client.resource(VEP_URI + "/" + VM_PATH + "/" + VMid);
         
         Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header(USERNAME_HEADER, user);
         
         ret = buildConn.get(JSONObject.class);
         
         return ret;
    } 

    public JSONObject startVirtualSystem(String templateID, String user, String vinID) {

        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        WebResource service = client.resource(VEP_URI + "/" + TEMPLATE_PATH + "/" + templateID + "/" + ACTION_PATH + "/" + DEPLOY_OP);

        if (vinID != null) {

            Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header(USERNAME_HEADER, user);

            JSONObject VIN_ID = null;
            try {
                VIN_ID = new JSONObject();
                VIN_ID.put("vinID", vinID);
                JSONObject partialRet = buildConn.put(JSONObject.class, VIN_ID);
                String vm_id = partialRet.getString("vm_id");
                JSONObject VMinfo =  getVMinfo(Integer.parseInt(vm_id.split(":")[0]), user);
                
                ret = new JSONObject();
                ret.putOpt("deploy", partialRet);
                ret.putOpt("vm-info", VMinfo);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else { 
            Builder buildConn = service.accept(MediaType.APPLICATION_JSON).header(USERNAME_HEADER, user);
            ret = buildConn.put(JSONObject.class);

        }
        return ret;
    }

    public JSONObject stopApplication(String ceeIdentifier, String appID, String user) throws JSONException {
    	
        // Submit to VEP
        JSONObject ret = null;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
 
        List<VepTemplate> templateIDs = DBconnector.getInstance().retrieveApplicationTemplates(appID);
            JSONArray retList = new JSONArray();
            for (int i = 0; i < templateIDs.size(); i++) {
                List<Vm> vmList = templateIDs.get(i).getVms();
                
                /*
                for (Vm v: vmList){
                	System.out.println("*" + v.getVmVepId());
                }
                */
                
                for (int j = 0; j < vmList.size(); j++) {
                	Vm vm = vmList.get(j);
         
                    if (vm.getStatus().equalsIgnoreCase("RN") || vm.getStatus().equalsIgnoreCase("DP")) {
                        long vm_id = vmList.get(j).getVmVepId();
                        WebResource service = client.resource(VEP_URI + "/" + VM_PATH + "/" + vm_id + "/" + ACTION_PATH + "/" + STOP_OP);
                        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                                .type(MediaType.APPLICATION_JSON)
                                .header(USERNAME_HEADER, user);
                        ret = buildConn.put(JSONObject.class);
                        retList.put(vmList.get(j).getVmVepId());
                        vmList.get(j).setStatus("ND");
                        vmList.get(j).setVmVepId(-1);
                        DBconnector.getInstance().updateObject(vmList.get(j));
                    }
                    try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
            if (ret == null) {
                ret = new JSONObject();
                ret.put(RETURN_STATUS, "no vm stopped");
            } else {
                ret.put(RETURN_STATUS, "ok");
                ret.put("vm_id", retList);
            }

        return ret;

    }

    public JSONObject getUserOVF(String user) {

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        WebResource service = client.resource(VEP_URI + "/" + OVF_PATH + "/");	//+ID_PATH+"/"+ovfID+"/"+ACTION_PATH+"/"+INITIALIZE_OP);

        Builder buildConn = service.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .header(USERNAME_HEADER, user);

        return buildConn.get(JSONObject.class);

    }

	public JSONObject getVm(String ceeIdentifier, String appId, String vmId, String user) throws JSONException {
		System.out.println("Wrong VEP Connector");
    	return null;
	}

	public JSONObject stopVm(String ceeIdentifier, String appID, String vmId, String user) throws JSONException {
		System.out.println("Wrong VEP Connector");
    	return null;
	}
	
	public JSONObject deleteVm(String ceeIdentifier, String appId, String vmId, String user) throws JSONException {
		System.out.println("Wrong VEP Connector");
    	return null;
	}


}
