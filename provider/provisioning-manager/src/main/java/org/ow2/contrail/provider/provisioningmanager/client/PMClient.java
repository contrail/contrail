package org.ow2.contrail.provider.provisioningmanager.client;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERGROUPS;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERNAME;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERROLE;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERVID;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.XUSERNAME;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.APP_ID;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Random;

import javax.jws.WebResult;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEP2connector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;
import org.ow2.contrail.provider.provisioningmanager.stubs.VEPDebugConnector;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;

import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.MultiPart;

/**
 * 
 * @author ales.cernivec@xlab.si
 *
 */
public class PMClient {
	
	private static WebResource r;
	private static String URI;
	
	/**
	 * Helper method for printing stack trace into a string.
	 * 
	 * @param aThrowable error to be traced
	 * @return string presenting stack trace
	 */
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	public static void propagateUser(HashMap<String, String> arguments)throws Exception{
		// creating the JSON object
		JSONObject jobject = new JSONObject();
		// fill the JSON with the proper fields
		try
		{
			jobject.put(USERNAME, arguments.get("username"));
			// currently fixed to -1 as VEP requested
			jobject.put(USERVID, arguments.get("vid"));
			// currently fixed to user
			jobject.put(USERROLE, arguments.get("role"));
			// for now: fixed to gregorb
			jobject.put(XUSERNAME, arguments.get("xuser"));
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(arguments.get("role"));
			jsonArray.put(arguments.get("xuser"));
			jobject.put(USERGROUPS, jsonArray);
			JSONObject answer = r.path(URI+"/user-mgmt/propagate")
			.type("application/json")
			.put(JSONObject.class, jobject);
			String jsonResult = answer.getString(RETURN_STATUS);
			System.out.println("User Registration for user " + arguments.get("xuser") + " completed with code: "+jsonResult);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void submitApp(HashMap<String, String> arguments) throws Exception{
		MultiPart mp = new MultiPart();
		//		String appName  = "ContrailTestApp_"+id;
		//		String slaID	= "SLASOI.xml";
		//		String username = "ContrailVEPUser_"+id;
		//
		//		String xuser = "gregorb";
		String appName  = arguments.get("appName");
		String slaID  = arguments.get("slaID");
		String username  = arguments.get("username");
		StringBuffer sb = new StringBuffer();
		String ovfFileName = arguments.get("ovfPath");
		try{
			File file = new File(ovfFileName);
			if(!file.exists()){
				throw new Exception("OVF does not exist.");
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		String ovfXML = sb.toString();		
		try {
			mp.bodyPart	(appName, 	MediaType.TEXT_PLAIN_TYPE)
			.bodyPart	(slaID,		MediaType.TEXT_PLAIN_TYPE)
			.bodyPart	(ovfXML, 	MediaType.TEXT_XML_TYPE)
			.bodyPart	(username, 	MediaType.TEXT_PLAIN_TYPE);
			JSONObject answer =  r.path(URI+"application-mgmt/submit")
			.type("multipart/mixed")
			.put(JSONObject.class, mp);
			String appID = answer.getString(APP_ID);
			System.out.println("ProvisioningManager-ID of the submitted app: "+appID);			
		}catch(Exception err){
			err.printStackTrace();
		}
	}
	public static void startApp(HashMap<String, String> arguments){

	}

	public static void stopApp(HashMap<String, String> arguments){

	}

	public static void printUsage(){
		System.out.println("Usage:\n" +
                "java -jar provisioningmanager.jar command [command_arguments] [-url <provisioning manager URL>] [-username <user name>]" +
                "[-vid <vid>] [-role <role>] [-xuser <xuser>] [-appName <appName>] [-slaID <slaID>] [-ovfPath <ovfPath>] \n" +
                "command is one of: prop_user, submit_app, start_app, stop_app");
	}

	public static void main(String[] args) throws UniformInterfaceException, JSONException{
		int len = args.length;
		if(len == 0){
			printUsage();
			System.exit(1);
		}
		try{
			String command = args[0];
			HashMap<String, String> argsMap = new HashMap<String, String>();
			for(int i = 1;i<len;i=i+2){
				if(!args[i].startsWith("-")){
					throw new Exception("Error parsing argumens");
				}
				argsMap.put(args[i].substring(1), args[i+1]);
			}
			System.out.println(String.format("Command %s", command));
			URI = argsMap.get("url");
			System.out.println(String.format("arguments %s", argsMap.toString()));
			System.out.println(String.format("URI %s", URI));

			if(command.toLowerCase().equals("prop_user")){
				propagateUser(argsMap);
			}
			if(command.toLowerCase().equals("start_app")){
				startApp(argsMap);
			}
			if(command.toLowerCase().equals("submit_app")){
				submitApp(argsMap);
			}
			if(command.toLowerCase().equals("stop_app")){
				stopApp(argsMap);
			}

		}catch(Exception err){
			System.err.println(getStackTrace(err));
		}
	}

	public void xtestSubmit() {

		MultiPart mp = new MultiPart();
		StringBuffer sb = null;
		Random rnd = new Random();
		long id = rnd.nextLong();

		String appName  = "ContrailTestApp_"+id;
		String slaID	= "SLASOI.xml";
		String username = "ContrailVEPUser_"+id;

		String xuser = "gregorb";

		try{

			File file = new File("src/main/resources/dsl-test-xlab.xml");

			if(!file.exists()){
				file = new File("dsl-test-xlab.xml");
			}

			if(!file.exists()){

				ProvisioningManagerProperties p = new ProvisioningManagerProperties();
				file = new File(p.getClass().getResource("dsl-test-xlab.xml").toURI());
			}

			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		String ovfXML = sb.toString();

		// creating the JSON object
		JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields
		try
		{
			jobject.put(USERNAME, username);

			// currently fixed to -1 as VEP requested
			jobject.put(USERVID, "-1");

			// currently fixed to user
			jobject.put(USERROLE, "user");

			// for now: fixed to gregorb
			jobject.put(XUSERNAME, xuser);

			JSONArray jsonArray = new JSONArray();
			jsonArray.put("user");
			jsonArray.put(username);

			jobject.put(USERGROUPS, jsonArray);

			JSONObject answer = r.path("/user-mgmt/propagate")
			.type("application/json")
			.put(JSONObject.class, jobject);

			String jsonResult = answer.getString(RETURN_STATUS);

			System.out.println("User Registration for user " + username + " completed with code: "+jsonResult);

		} catch(Exception e){
			e.printStackTrace();
		}


		mp.bodyPart	(appName, 	MediaType.TEXT_PLAIN_TYPE)
		.bodyPart	(slaID,		MediaType.TEXT_PLAIN_TYPE)
		.bodyPart	(ovfXML, 	MediaType.TEXT_XML_TYPE)
		.bodyPart	(username, 	MediaType.TEXT_PLAIN_TYPE);

		JSONObject answer =  r.path("application-mgmt/submit")
		.type("multipart/mixed")
		.put(JSONObject.class, mp);

		try {

			String appID = answer.getString(APP_ID);
			System.out.println("ProvisioningManager-ID of the submitted app: "+appID);

			JSONObject requestEntity = new JSONObject();
			requestEntity.put(USERNAME, username);
			answer = r.path("application-mgmt/start/"+appID)
			.type(MediaType.APPLICATION_JSON)
			.post(JSONObject.class, requestEntity);

			JSONArray jArray = answer.getJSONArray(VM_IDS);

			for(int i=0;i<jArray.length();i++){
				System.out.println("VM_"+i+" - "+jArray.get(i));
			}

			answer = r.path("application-mgmt/stop/"+appID)
			.type(MediaType.APPLICATION_JSON)
			.post(JSONObject.class, requestEntity);

			System.out.println("Application Stopped with code: "+answer.getString(RETURN_STATUS));

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
