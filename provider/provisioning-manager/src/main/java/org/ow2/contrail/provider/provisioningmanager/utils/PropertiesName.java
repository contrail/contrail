package org.ow2.contrail.provider.provisioningmanager.utils;
/*
 * CHANGE: Added VEP_VERSION and PROVISIONING_MANAGER_SECURE_PORT properties. @author: Marco
 * 
 */
public interface PropertiesName {

	public static final String VEP_PORT = "VEP_PORT";
	public static final String VEP_IP = "VEP_IP";
	public static final String VEP_VERSION = "VEP_VERSION";	
	
	public static final String SLAM_PORT = "SLAM_PORT";
	public static final String SLAM_IP = "SLAM_IP";
	public static final String SLA_SUPPORT_ENABLED = "SLA_SUPPORT_ENABLED";
	
	public static final String PROVISIONING_MANAGER_IP = "PROVISIONING_MANAGER_IP";
	public static final String PROVISIONING_MANAGER_PORT = "PROVISIONING_MANAGER_PORT";
	public static final String PROVISIONING_MANAGER_SECURE_PORT = "PROVISIONING_MANAGER_SECURE_PORT";
	public static final String RABBITMQ_IP = "RABBITMQ_IP";
	
}
