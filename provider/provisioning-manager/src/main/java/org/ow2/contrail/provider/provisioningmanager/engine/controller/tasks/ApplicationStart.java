package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import static org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.ExternalizedVEPstrings.VM_STATE_LIST;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.VepTemplate;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

import com.sun.jersey.api.client.UniformInterfaceException;


public class ApplicationStart implements Callable<JSONObject> {

	private String appID = null; 
	private String userUuid = null;
	private String ceeIdentifier= null;
	
	private static Logger logger = Logger.getLogger(ApplicationStart.class);
	private static String loggerTag = "[ApplicationStartTask] ";
	
	public ApplicationStart(String uuid, String ceeId, String AppID){
		this.appID = AppID; 
		this.userUuid = uuid;
		this.ceeIdentifier = ceeId;
	}
	
	@Override
	public JSONObject call() throws Exception {

		JSONObject returnStatus = new JSONObject();
		
		try{
			JSONObject ret = null;
			ret = VEPconnector.getInstance().startApplication(this.ceeIdentifier, this.appID, this.userUuid);
			
			if(ret.get(RETURN_STATUS).equals("200")){
				returnStatus.put(RETURN_STATUS, ret.get(RETURN_STATUS) );
				JSONObject getApp = VEPconnector.getInstance().getApplication(this.ceeIdentifier, this.appID, this.userUuid);
				JSONArray vms_info=getApp.optJSONArray("VMs");
				JSONArray vms_id = new JSONArray();
				for (int i=0;i<vms_info.length();i++){
					String[] VmId = ((JSONObject)vms_info.get(i)).getString("href").split("/");
					String vmId =VmId[VmId.length-1];
					JSONObject getVm = VEPconnector.getInstance().getVm(this.ceeIdentifier, this.appID, vmId, this.userUuid);
					String vSystem = ((JSONObject) getVm.get("virtualSystem")).getString("href");
					JSONArray vm_vs = new JSONArray();
					vm_vs.put(vmId);
					vm_vs.put(vSystem);
					vms_id.put(vm_vs);
					List<Vm> listVm = DBconnector.getInstance().getVmsOfApplication(this.appID);
					
					//check if vm is already registered
					boolean vmExists = false;
					for (Vm vm : listVm){
						if(vm.getVmVepId() == Long.parseLong(vmId)){
							vmExists=true;
							logger.info(loggerTag + "VM " +vmId+ " already registered");
						}
					}
					if(!vmExists){
						//Store Vm in the DB
						Vm vmToStore = new Vm();
						vmToStore.setVmVepId(Long.parseLong(vmId));
						vmToStore.setApplication(DBconnector.getInstance().getApplicationEntityByVepId(this.appID));
						vmToStore.setVirtualSystem(vSystem);
						vmToStore.setStatus(((JSONObject)vms_info.get(i)).getString("state"));
						DBconnector.getInstance().storeObject(vmToStore);
						logger.debug(loggerTag + "VM Store in DB:\n" +vmToStore);
					}
					
				}
				returnStatus.put(VM_IDS, vms_id);
			}
			else
				returnStatus.put(RETURN_STATUS,  ret.get(RETURN_STATUS));
			

		} catch(UniformInterfaceException e){
		
			returnStatus.put(RETURN_STATUS, "UniformInterfaceException"+ e.getResponse().getClientResponseStatus().getStatusCode());
			e.printStackTrace();
			return returnStatus;
	
		} catch (JSONException e) {
			returnStatus.put(RETURN_STATUS, "500" );
			e.printStackTrace();
		} catch (Exception e) {
			returnStatus.put(RETURN_STATUS, "500" );
			e.printStackTrace();
		}
	
		return returnStatus;
		
	}
	
	private Vm makeVm(JSONObject ret) throws JSONException {
		Vm vMachine = new Vm();
		vMachine.setStatus(ret.getString("vm_state"));
		vMachine.setVmVepId(ret.getLong("vm_id"));
		return vMachine;
	}

	private VepTemplate[] getTemplateIDs(String AppID) {
		
		List<VepTemplate> templates = DBconnector.getInstance().retrieveApplicationTemplates(AppID);
		return templates.toArray(new VepTemplate[templates.size()]);
		
	}
	
}
