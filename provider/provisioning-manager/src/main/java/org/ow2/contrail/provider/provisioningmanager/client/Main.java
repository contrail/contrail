package org.ow2.contrail.provider.provisioningmanager.client;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;



public class Main {

	

	public static void main(String[] args) throws JSONException {
		
		JSONObject jsonRootObj =  new JSONObject();
		JSONObject jsonChildObj =  new JSONObject();
		
		jsonRootObj.put("primoInRoot", "ciao");
		
		jsonChildObj = new JSONObject();
		jsonChildObj.put("primoInChild", "mini-ciao");
		jsonChildObj.put("secondoInChild", "mini-bonjour");
		
		jsonRootObj.put("secondoInRoot", jsonChildObj);
		
		jsonRootObj.put("terzoInRoot", "adieu");
		
		System.out.println(jsonRootObj);
	}
}

   