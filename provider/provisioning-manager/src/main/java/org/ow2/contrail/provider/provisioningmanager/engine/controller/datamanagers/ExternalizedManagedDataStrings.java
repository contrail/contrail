package org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers;

public interface ExternalizedManagedDataStrings {

	public static final String APP_NAME			=		"APP-NAME";
	public static final String USER_NAME		=		"USER-NAME";
	public static final String OVF_XML_DESCR	=		"OVF-DESCR";
	public static final String OVF_ID			=		"OVF-ID";
	public static final String SLA_ID			=		"SLA-ID";
	
	public static final String APP_STATUS		=		"APP-STATUS";
	public static final String OVF_REGISTERED	=		"OVF_REGISTERED";
	public static final String OVF_INITIALIZED	=		"OVF_INITIALIZED";
}
