--------------------------------------------------------------------------------
            		   Provisioning Manager
--------------------------------------------------------------------------------

Support:  For  any   question  about   the   integration  send   an  email   to:
patrizio.dazzi@isti.cnr.it

Provisioning Manager:  The provisioning  manager  is  the  software entity  that
receives requests from  the federation and dialogs with VEP,  OVF parser and SLA
support for resource provisioning.

In  the  actual  version  0.0.1-SNAPSHOT   it  only  offers  a  limited  set  of
functionality  w.r.t. the  ones envisioned  for  the final  version of  Contrail
project. In  its current version, the  Provisioning Manager (aka PM)  is able to
propagate user registration  to the associated VEP but is  unable to modify that
user.  PM  is able to  register OVFs to  the VEP and to  deploy them but  not to
modify them.  It is able  to start and  to stop the OVF  running in the  VEP. To
contact the  VEP the PM  reads from its  configuration file the VEP  address and
port indicated by VEP_IP and VEP_PORT variables, respectively.

When  the  PM  starts, it  creates  a  RESTful  service  in its  actual  running
machine. The IP and the port  are retrieved from the configuration file by using
PROVISIONING_MANAGER_IP  and PROVISIONING_MANAGER_PORT  variables. If  no  IP is
specified, the PM  will setup the service to run at  the address associated with
the  first network  interface  on the  running  machine. Once  activated the  PM
connects with RabbitMQ to  retrieve provider related monitoring information. The
IP of the RabbitMQ is specified  in the configuration file by using the variable
RABBITMQ_IP.
 
When PM receives  a request to provide resources it parses  the OVF for checking
its  validity and  for  extracting the  application  structure. To  this end  it
exploits the Contrail OVF toolkit. Along  this check it also exploits the SLA-ID
received to connect  to SLA repository to retrieve the  corresponding SLA and to
check its  compliance with the associated  OVF.  In the current  version the SLA
repository has been  substituted with a fake one  keeping the standard interface
but just searching  for SLAs in a disk folder specified  by SLA_PATH variable in
the configuration file.

In order to specify  the configuration file to use a parameter  can be passed to
PM as  first parameter  when it  is started. Such  parameter corresponds  to the
absolute path to the configuration file.



RESTful Calls  
------------- 

Provisioning Manager  is bundled with a java  test class that is  used to verify
the installation of  the PM and its integration with  VEP, RabbitMQ, OVF toolkit
and SLA  repository. To  this end  this class creates  a fake  user in  the VEP,
submit an OVF  file with a given  SLA-ID that correspond to a  fake SLA provided
with PM. The OVF is started and  then stopped. All these calls are performed via
RESTful interface. In the current version the PM support X types of calls:

1) User Propagation;
2) Application Submission;
3) Application Start;
4) Application Stop;


The User Propagation action can be  activated by issuing  a PUT operation to the
URL http://<PM_IP>:<PM_PORT>/user-mgmt/propagate In the body of the request must
be provided a JSON object containing the username of the  user to propagate, its
role,  its  vid, its  groups  and the  username  of  the federation  coordinator
issuing   the   request.   These  values   must  be  provided   associated  with
corresponding  JSON identifiers.   Namely, "name",  "role", "vid",  "groups" and
"xUser". If everything works the PM returns  0 as a code, if something goes in a
wrong way, it returns -1.

Application Submission can  be achieved by issuing a  POST  operation to the URL
http://<PM_IP>:<PM_PORT>/application-mgmt/submit In  the body of  the request is
required to  provide a content  organized according to the  MIME multipart/mixed
data type. This contains the AppName, the SLA_ID, the OVF xml descriptor and the
username of the user submitting the application. AppName is a name that the user
has  to associate  to the  application she  is submitting.   This  RESTful calls
returns  a  JSON  object  containing  the  code  returned  by  the  VEP  (STATUS
identifier) and the  Provisioning Manager ID of the  application submitted. This
value  has to  be provided  to PM  for starting  and stopping  the corresponding
application.

Application   Start    can   be   triggered    by   accessing   to    the   URL:
http://<PM_IP>:<PM_PORT>/application-mgmt/start/{ID}  where  {ID}  needs  to  be
substituted with  the ID obtained  by the submit  operation. This call  takes in
input  a JSON  object containing  the username  of the  user issuing   the start
command and returns  a JSON object containing a JSON  object describing the call
STATUS  and a  JSON array  identified by  the "VM-IDS"  string. This  last array
contains the details about the VMs created by the VEP.

Application Stop  can be  invoked by accessing  the Provisioning Manager  at the
URL:  http://<PM_IP>:<PM_PORT>/application-mgmt/stop/{ID} where  {ID} is  the ID
returned by  submit operation.  In the content  body has  to be provided  a JSON
object indicating the username of the user issuing the request.
