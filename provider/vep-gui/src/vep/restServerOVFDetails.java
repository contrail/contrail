/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
//import org.ow2.contrail.federation.opennebula.OpenNebulaOVFParser;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser;


/**
 *
 * @author piyush
 */
public class restServerOVFDetails extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String oneIp;
    private String onePort;
    private OpenNebulaOVFParser ovfParser;
    private String vepProperties;
    private String oneVersion;
    private String oneAdmin;
    
    public restServerOVFDetails()
    {
        vepProperties = VEPHelperMethods.getPropertyFile();
        logger = Logger.getLogger("VEP.restOVFdetails");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        oneIp = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        oneAdmin = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
        db = new dbHandler("restServerOVFdetails", dbType);   
    }
    
    @Delete("json")
    public Representation deleteOVF()
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String ovfsno = ((String) getRequest().getAttributes().get("sno"));
        String username = requestHeaders.getFirstValue("X-Username");
        String ovfaction = ((String) getRequest().getAttributes().get("action"));
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        JSONObject response = new JSONObject();
        ResultSet rs = null;
        String[] groups;
        boolean isAdmin = false;
        int requester_uid = -1;
        String oneuser = "";
        String onepass = "";
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            response.put("error", "CLIENT_ERROR_BAD_REQUEST");
        }
        else
        {
            if(ovfaction != null)
            {
                this.setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
                response.put("error", "CLIENT_ERROR_METHOD_NOT_ALLOWED");
            }
            else
            {
                //check to see if user exists and has permissions to delete
                try
                {
                    rs = db.query("select", "*", "user", "where username='" + username + "'");
                    if(rs.next())
                    {
                        requester_uid = rs.getInt("uid");
                        oneuser = rs.getString("oneuser");
                        onepass = rs.getString("onepass");
                        rs.close();
                        rs = db.query("select", "*", "ugroup", "where uid=" + requester_uid + "");
                        String groupList = "";
                        while(rs.next())
                        {
                            groupList += rs.getString("gname") + ",";
                        }
                        groups = groupList.split(","); //the last index will be empty because of the trailing ,
                        logger.trace("GroupsList for user: " + username + " is: " + groupList);
                        for(int i=0; i<groups.length; i++)
                        {
                            if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                            {
                                isAdmin = true;
                                logger.trace("Setting isAdmin to true.");
                                break;
                            }
                        }
                        rs.close();
                        //now if admin or same user then proceed with delete
                        rs = db.query("select", "*", "ovf", "where sno=" + ovfsno);
                        boolean status = false;
                        if(rs.next())
                        {
                            int userId = rs.getInt("uid");
                            rs.close();
                            //then delete after checking the permission bits
                            if(isAdmin || userId == requester_uid)
                            {
                                //first do housekeeping
                                //sending shutdown command to all VMs
                                String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
                                ONExmlrpcHandler onehandle = null;
                                ONE3xmlrpcHandler one3handle = null;
                                if(oneVersion.startsWith("2.2"))
                                {
                                    logger.debug("Creating corresponding ONE account for user " + username + " [" + oneuser + ", " + onepass + "].");
                                    onehandle = new ONExmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerOVFDetails:deleteOVF");
                                }
                                else if(oneVersion.startsWith("3.4"))
                                {
                                    logger.debug("Creating corresponding ONE3 account for user " + username + " [" + oneuser + ", " + onepass + "].");
                                    one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerOVFDetails:deleteOVF");
                                }
                                rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                int counter = 0;
                                while(rs.next())
                                {
                                    counter++;
                                    int vmid = rs.getInt("vmid");
                                    ResultSet rs1 = db.query("select", "id, onevmid, state", "vmachine", "where vmid=" + vmid);
                                    while(rs1.next())
                                    {
                                        int id = rs1.getInt("id");
                                        String state = rs1.getString("state");
                                        if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                        {
                                            if(onehandle != null)
                                                onehandle.shutdownVM(rs1.getInt("onevmid"));
                                            else if(one3handle != null)
                                                one3handle.shutdownVM(rs1.getInt("onevmid"));
                                            //wait for 5 seconds before sending next command
                                            Thread.sleep(5000);
                                        }
                                    }
                                    rs1.close();
                                    //delete diskimage entry corresponding to this vmid
                                    db.delete("diskimage", "where vmid=" + vmid);
                                    //now also remove all vm entries corresponding to this vmid from vmachine table
                                    //db.delete("vmachine", "where vmid=" + vmid + " AND state='FN'");
                                    //while(db.query("select", "*", "vmachine", "vmid=" + vmid + " AND (state='RN' OR state='SH')").next())
                                    //{
                                    //    Thread.sleep(2000);
                                    //}
                                    db.delete("vmachine", "where vmid=" + vmid);
                                    
                                    rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                    for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                                }
                                rs.close();
                                //now remove all template entries that correspond to vmids corresponding to ovfsno
                                //also remove all vmachine entries that corresponds to the removed vmid
                                db.delete("vmachinetemplate", "where ovfsno=" + ovfsno);
                                
                                //now delete the OVF entry
                                status = db.delete("ovf","where sno=" + ovfsno);
                                if(status)
                                {
                                    this.setStatus(Status.SUCCESS_OK);
                                    response.put("message", "SUCCESS_OK");
                                }
                                else
                                {
                                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                    response.put("error", "Unable to delete OVF");
                                }
                            }
                            else
                            {
                                this.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                response.put("error", "CLIENT_ERROR_FORBIDDEN");
                            }
                            rs.close();
                        }
                        else
                        {
                            //no OVF found
                            this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                            response.put("error", "CLIENT_ERROR_NOT_FOUND");
                        }
                    }
                    else
                    {
                        this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                        response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                    }
                }
                catch(Exception ex)
                {
                    ex.printStackTrace(System.err);
                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                    response.put("error", "Exception caught: " + ex.getMessage());
                    logger.debug("Exception caught while trying to delete the OVF " + ovfsno);
                }
                
            }
        }
        StringRepresentation out = new StringRepresentation(response.toJSONString(), MediaType.APPLICATION_JSON);
        return out;
    }
    
    @Get("json")
    public Representation toValue()
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        String ovfsno = ((String) getRequest().getAttributes().get("sno"));
        String username = requestHeaders.getFirstValue("X-Username");
        
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(ovfsno, username);
            else if(acceptType.contains("xml"))
                response = toXml(ovfsno, username);
        }
        else
        {
            //default rendering ...
            response = toHtml(ovfsno, username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toXml(String ovfsno, String username)
    {
        StringBuilder stringBuilder = new StringBuilder();
        String ovfGroup;
        String ovfPerm;
        boolean isAdmin = false;
        boolean isMember = false;
        int user_uid = -1;
        String[] groups = null;
        try
        {
            ResultSet rs;
            if(username != null)
            {
                rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    user_uid = uid;
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                }
                rs.close();
            }
            rs = db.query("select", "*", "ovf", "where sno=" + ovfsno);
            if(rs.next())
            {
                //now check for proper permissions
                ovfGroup = rs.getString("gname");
                ovfPerm = rs.getString("perm");
                int uid = rs.getInt("uid");
                int gPerm = 0;
                int oPerm = 0;
                try
                {
                    gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                    oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                }
                catch(Exception ex)
                {
                    logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                    gPerm = oPerm = 0;
                }
                for(int i=0; groups!=null && i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase(ovfGroup))
                    {
                        isMember = true;
                        logger.trace("User " + username + " is member of OVF group.");
                        break;
                    }
                }
                if(uid == user_uid || isAdmin)
                {
                    String content = rs.getString("descp");
                    stringBuilder.append(content);
                    stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
                    stringBuilder.append("<VEPDetails>\n");
                    stringBuilder.append("<ceeid>").append(rs.getInt("ceeid")).append("</ceeid>\n");
                    stringBuilder.append("<state>").append(rs.getString("state")).append("</state>\n");
                    stringBuilder.append("<group>").append(rs.getString("gname")).append("</group>\n");
                    stringBuilder.append("<permissions>").append(rs.getString("perm")).append("</permissions>\n");
                    stringBuilder.append("<ovfname>").append(rs.getString("ovfname")).append("</ovfname>\n");
                    stringBuilder.append("</VEPDetails>\n");
                    //content = StringEscapeUtils.escapeXml(content);
                }
                else if(isMember)
                {
                    if(gPerm >= 4)
                    {
                        String content = rs.getString("descp");
                        stringBuilder.append(content);
                        stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
                        stringBuilder.append("<VEPDetails>\n");
                        stringBuilder.append("<ceeid>").append(rs.getInt("ceeid")).append("</ceeid>\n");
                        stringBuilder.append("<state>").append(rs.getString("state")).append("</state>\n");
                        stringBuilder.append("<group>").append(rs.getString("gname")).append("</group>\n");
                        stringBuilder.append("<permissions>").append(rs.getString("perm")).append("</permissions>\n");
                        stringBuilder.append("<ovfname>").append(rs.getString("ovfname")).append("</ovfname>\n");
                        stringBuilder.append("</VEPDetails>\n");
                    }
                    else
                    {
                        stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
                        stringBuilder.append("<ErrorDetails>\n");
                        stringBuilder.append("<error>").append("You do not seem to have proper rights to access the data.").append("</error>\n");
                        stringBuilder.append("</ErrorDetails>\n");
                        this.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                    }
                }
                else
                {
                    if(oPerm >= 4)
                    {
                        String content = rs.getString("descp");
                        stringBuilder.append(content);
                        stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
                        stringBuilder.append("<VEPDetails>\n");
                        stringBuilder.append("<ceeid>").append(rs.getInt("ceeid")).append("</ceeid>\n");
                        stringBuilder.append("<state>").append(rs.getString("state")).append("</state>\n");
                        stringBuilder.append("<group>").append(rs.getString("gname")).append("</group>\n");
                        stringBuilder.append("<permissions>").append(rs.getString("perm")).append("</permissions>\n");
                        stringBuilder.append("<ovfname>").append(rs.getString("ovfname")).append("</ovfname>\n");
                        stringBuilder.append("</VEPDetails>\n");
                    }
                    else
                    { 
                        stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
                        stringBuilder.append("<ErrorDetails>\n");
                        stringBuilder.append("<error>").append("You do not seem to have proper rights to access the data.").append("</error>\n");
                        stringBuilder.append("</ErrorDetails>\n");
                        this.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                    }
                }
                rs.close();
            }
        }
        catch(SQLException ex)
        {
            ex.printStackTrace(System.err);
            logger.warn("Caught exception while displaying the OVF details. Exception: " + ex.getMessage());
            stringBuilder.append("\n<!-- OVF Application VEP specific details follows next -->\n");
            stringBuilder.append("<ErrorDetails>\n");
            stringBuilder.append("<error>").append("Exception Caught: ").append(ex.getMessage()).append("</error>\n");
            stringBuilder.append("</ErrorDetails>\n");
            this.setStatus(Status.SERVER_ERROR_INTERNAL);
        }
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.APPLICATION_XML);
        return value;
    }
    
    public Representation toHtml(String ovfsno, String username)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        stringBuilder.append("Details on OVF with serial <i>").append(ovfsno).append("</i> is shown below:<br><br>");
        stringBuilder.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
        stringBuilder.append("<tr>");
        stringBuilder.append("<td valign='top' style='width:128px;background:white;'><img style='width:128px;' src='https://www.cise.ufl.edu/~pharsh/public/ovf.jpg'>");
        stringBuilder.append("<td valign='top' align='left' bgcolor='white' width='*'>");
        
        String ovfGroup;
        String ovfPerm;
        boolean isAdmin = false;
        boolean isMember = false;
        int user_uid = -1;
        String[] groups = null;
        try
        {
            ResultSet rs;
            if(username != null)
            {
                rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    user_uid = uid;
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                }
                rs.close();
            }
            rs = db.query("select", "*", "ovf", "where sno=" + ovfsno);
            if(rs.next())
            {
                //now check for proper permissions
                ovfGroup = rs.getString("gname");
                ovfPerm = rs.getString("perm");
                int uid = rs.getInt("uid");
                int gPerm = 0;
                int oPerm = 0;
                try
                {
                    gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                    oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                }
                catch(Exception ex)
                {
                    logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                    gPerm = oPerm = 0;
                }
                for(int i=0; groups!=null && i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase(ovfGroup))
                    {
                        isMember = true;
                        logger.trace("User " + username + " is member of OVF group.");
                        break;
                    }
                }
                if(uid == user_uid || isAdmin)
                {
                    //sjow ovf contents
                    stringBuilder.append("<div style='background:#CED1D6;font-weight:bold;'>");
                    stringBuilder.append("Constrained Execution Environment ID: ").append(rs.getInt("ceeid")).append("<br>");
                    stringBuilder.append("State: ").append(rs.getString("state")).append("<br>");
                    stringBuilder.append("Group: ").append(rs.getString("gname")).append("<br>");
                    stringBuilder.append("Permission Bits: ").append(rs.getString("perm")).append("<br>");
                    stringBuilder.append("OVF Name: ").append(rs.getString("ovfname")).append("<br>");
                    stringBuilder.append("</div><br>");
                    //now printing the actual OVF descriptor
                    //stringBuilder.append("<div style='white-space:normal;word-wrap:normal;background:#CCCCFF;overflow:auto;height:360px;width:880px;font-family:Console;font-size:8pt;'>");
                    stringBuilder.append("<div style='white-space:normal;word-wrap:normal;background:#ECFCF3;overflow:auto;font-weight:bold;word-wrap:break-word;width:880px;font-family:Console;font-size:8pt;'>");
                    String content = rs.getString("descp");
                    content = StringEscapeUtils.escapeXml(content);
                    stringBuilder.append("<pre>").append(content).append("</pre>");
                    stringBuilder.append("</div>");
                }
                else if(isMember)
                {
                    if(gPerm >= 4)
                    {
                        //show ovf contents
                        stringBuilder.append("<div style='background:#CED1D6;'>");
                        stringBuilder.append("Constrained Execution Environment ID: ").append(rs.getInt("ceeid")).append("<br>");
                        stringBuilder.append("State: ").append(rs.getString("state")).append("<br>");
                        stringBuilder.append("Group: ").append(rs.getString("gname")).append("<br>");
                        stringBuilder.append("Permission Bits: ").append(rs.getString("perm")).append("<br>");
                        stringBuilder.append("OVF Name: ").append(rs.getString("ovfname")).append("<br>");
                        stringBuilder.append("</div><br>");
                        //now printing the actual OVF descriptor
                        stringBuilder.append("<div style='word-wrap:pre-wrap;background:#ECFCF3;overflow:auto;font-weight:bold;word-wrap:break-word;width:880px;font-family:Console;font-size:8pt;'>");
                        String content = rs.getString("descp");
                        content = StringEscapeUtils.escapeXml(content);
                        stringBuilder.append("<pre>").append(content).append("</pre>");
                        stringBuilder.append("</div>");
                    }
                    else
                    {
                        stringBuilder.append("<div style='background:#CCCCFF;'>");
                        stringBuilder.append("You do not seem to have proper rights to access the data.");
                        stringBuilder.append("</div>");
                    }
                }
                else
                {
                    if(oPerm >= 4)
                    {
                        //show ovf contents
                        stringBuilder.append("<div style='background:#CED1D6;'>");
                        stringBuilder.append("Constrained Execution Environment ID: ").append(rs.getInt("ceeid")).append("<br>");
                        stringBuilder.append("State: ").append(rs.getString("state")).append("<br>");
                        stringBuilder.append("Group: ").append(rs.getString("gname")).append("<br>");
                        stringBuilder.append("Permission Bits: ").append(rs.getString("perm")).append("<br>");
                        stringBuilder.append("OVF Name: ").append(rs.getString("ovfname")).append("<br>");
                        stringBuilder.append("</div><br>");
                        //now printing the actual OVF descriptor
                        stringBuilder.append("<div style='word-wrap:pre-wrap;background:#ECFCF3;overflow:auto;font-weight:bold;word-wrap:break-word;width:880px;font-family:Console;font-size:8pt;'>");
                        String content = rs.getString("descp");
                        content = StringEscapeUtils.escapeXml(content);
                        stringBuilder.append("<pre>").append(content).append("</pre>");
                        stringBuilder.append("</div>");
                    }
                    else
                    {
                        stringBuilder.append("<div style='background:#CCCCFF;'>");
                        stringBuilder.append("You do not seem to have proper rights to access the data.");
                        stringBuilder.append("</div>");
                    }
                }
                rs.close();
            }
            //stringBuilder.append("</ul><br><br>");
        }
        catch(SQLException ex)
        {
            ex.printStackTrace(System.err);
            logger.warn("Caught exception while displaying the OVF details. Exception: " + ex.getMessage());
            stringBuilder.append("<div style='background:red;'>");
            stringBuilder.append("Exception caught: ").append(ex.getMessage());
            stringBuilder.append("</div>");
        }
        stringBuilder.append("</table><br>");
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
    
    @Put("json|xml") //accept-list only json supported
    public Representation updateExecuteOvf(String ovfDesc)
    {
        //only updates are allows through this URL entry point
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        JSONObject response = new JSONObject();
        ResultSet rs = null;
        String ovfsno = ((String) getRequest().getAttributes().get("sno"));
        String username = requestHeaders.getFirstValue("X-Username");
        String ovfaction = ((String) getRequest().getAttributes().get("action"));
        String ovfgroup = requestHeaders.getFirstValue("X-Ovfgroup");
        String ovfperm = requestHeaders.getFirstValue("X-Ovfperm");
        //username contained in the certificate overrides username in header
        X509Certificate certUser = null;
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            certUser = Cert;
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        //if body contains no OVF then also it should be fine
        if(username == null || ovfsno == null)
        {
            response.put("error", "CLIENT_ERROR_BAD_REQUEST");
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
        else
        {
            if(ovfaction == null) //update request only ...
            {
                response.put("title", "OVF Update Request Handler");
                //if any value is missing then no change in the stored value
                String updateVal = "";
                if(ovfDesc != null && ovfDesc.trim().length() > 0)
                {
                    updateVal = "descp='" + ovfDesc + "'";
                }
                if(ovfgroup != null)
                {
                    if(updateVal.length() > 0)
                        updateVal += ", gname='" + ovfgroup + "'";
                    else
                        updateVal = "gname='" + ovfgroup + "'";
                }
                if(ovfperm != null)
                {
                    if(updateVal.length() > 0)
                        updateVal += ", perm='" + ovfperm + "'";
                    else
                        updateVal = "perm='" + ovfperm + "'";
                }
                //now determining if this user has rights to modify the OVF
                int uid = -1;
                //now get the uid for the username
                try
                {
                    rs = db.query("select", "*", "user", "where username='" + username + "'");
                    if(rs.next())
                        uid = rs.getInt("uid");
                    else
                        uid = -2;
                    rs.close();
                }
                catch(Exception ex)
                {
                    logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
                }
                if(uid != -1 && uid != -2)
                {
                    try
                    {
                        rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                        String groupList = "";
                        while(rs.next())
                        {
                            groupList += rs.getString("gname") + ",";
                        }
                        String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                        logger.trace("GroupsList: " + groupList);
                        boolean isAdmin = false;
                        for(int i=0; i<groups.length; i++)
                        {
                            if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                            {
                                isAdmin = true;
                                logger.trace("Setting isAdmin to true.");
                                break;
                            }
                        }
                        rs.close();   
                        rs = db.query("select", "*", "ovf", "where sno=" + ovfsno);
                        
                        boolean status = false;
                        if(rs.next())
                        {
                            //then update after checking the permission bits
                            if(isAdmin || rs.getInt("uid") == uid)
                            {
                                if(updateVal.length() > 1)
                                    status = db.update("ovf", updateVal, "where sno=" + ovfsno);
                                else
                                {
                                    //nothing to update
                                    response.put("error", "CLIENT_ERROR_NOT_ACCEPTABLE");
                                    this.setStatus(Status.CLIENT_ERROR_NOT_ACCEPTABLE);
                                }
                            }
                            else
                            {
                                String ovfGroup = rs.getString("gname");
                                String ovfPerm = rs.getString("perm");
                                int gPerm = 0;
                                int oPerm = 0;
                                try
                                {
                                    gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                                    oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                                }
                                catch(Exception ex)
                                {
                                    logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                                    gPerm = oPerm = 0;
                                }
                                boolean isMember = false;
                                for(int i=0; i<groups.length; i++)
                                {
                                    if(groups[i].equalsIgnoreCase(ovfGroup))
                                    {
                                        isMember = true;
                                        logger.trace("User " + username + " is member of OVF group.");
                                        break;
                                    }
                                }
                                if(isMember)
                                {
                                    if(gPerm == 2 || gPerm == 3 || gPerm == 6 || gPerm == 7)
                                    {
                                        if(updateVal.length() > 1)
                                            status = db.update("ovf", updateVal, "where sno=" + ovfsno);
                                        else
                                        {
                                            //nothing to update
                                            response.put("error", "CLIENT_ERROR_NOT_ACCEPTABLE");
                                            this.setStatus(Status.CLIENT_ERROR_NOT_ACCEPTABLE);
                                        }
                                    }
                                    else
                                    {
                                        response.put("error", "CLIENT_ERROR_FORBIDDEN");
                                        this.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                    }
                                }
                                else
                                {
                                    if(oPerm == 2 || oPerm == 3 || oPerm == 6 || oPerm == 7)
                                    {
                                        if(updateVal.length() > 1)
                                            status = db.update("ovf", updateVal, "where sno=" + ovfsno);
                                        else
                                        {
                                            //nothing to update
                                            response.put("error", "CLIENT_ERROR_NOT_ACCEPTABLE");
                                            this.setStatus(Status.CLIENT_ERROR_NOT_ACCEPTABLE);
                                        }
                                    }
                                    else
                                    {
                                        response.put("error", "CLIENT_ERROR_FORBIDDEN");
                                        this.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                    }
                                }
                            }
                            if(status)
                            {
                                response.put("message", "SUCCESS_ACCEPTED");
                                this.setStatus(Status.SUCCESS_ACCEPTED);
                            }
                            else
                            {
                                if(updateVal.length() > 1)
                                {
                                    response.put("error", "SERVER_ERROR_INTERNAL");
                                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                }
                            }
                        }
                        else
                        {
                            //no such ovf application exists, updated fails
                            this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                            response.put("error", "CLIENT_ERROR_NOT_FOUND");
                            logger.debug("OVF update failed, application with serial: " + ovfsno + " was not found.");
                        }
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while updating OVF entry for " + username + ", serial= " + ovfsno + ": " + ex.getMessage());
                        response.put("error", "SERVER_ERROR_INTERNAL");
                        this.setStatus(Status.SERVER_ERROR_INTERNAL);
                    }
                }
                else
                {
                    if(uid == -1)
                    {
                        response.put("error", "SERVER_ERROR_INTERNAL");
                        this.setStatus(Status.SERVER_ERROR_INTERNAL);
                    }
                    else
                    {
                        response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED"); //user must exist at VEP before hand
                        this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                        logger.debug("User " + username + " does not exist. OVF modification attempt failed.");
                    }
                }
            }
            else
            {
                response.put("title", "OVF Actions Handler");
                //ovf action was specified
                //now determining if this user has rights to execute the OVF
                int uid = -1;
                //now get the uid for the username
                try
                {
                    rs = db.query("select", "*", "user", "where username='" + username + "'");
                    if(rs.next())
                        uid = rs.getInt("uid");
                    else
                        uid = -2;
                    rs.close();
                }
                catch(Exception ex)
                {
                    response.put("error", "SERVER_ERROR_INTERNAL");
                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                    logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
                }
                if(uid != -1 && uid != -2)
                {
                    //owners have default full rights regardless of owner rights
                    //strategy: find if ovf->uid is same as user->uid then isOwner = true
                    //else if ovf->group equals one of the groups to which this user belongs then isGroup = true
                    try
                    {
                        rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                        String groupList = "";
                        while(rs.next())
                        {
                            groupList += rs.getString("gname") + ",";
                        }
                        rs.close();
                        rs = db.query("select", "*", "ovf", "where sno=" + ovfsno);
                        String ovfGroup = "";
                        int ovfUid = -1;
                        int gPerm = 0;
                        int oPerm = 0;
                        String ovfValue = null;
                        String ovfPerm = null;
                        if(rs.next())
                        {
                            ovfUid = rs.getInt("uid");
                            ovfGroup = rs.getString("gname");
                            ovfValue = rs.getString("descp");
                            ovfPerm = rs.getString("perm");
                            rs.close();
                            try
                            {
                                gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                                oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                            }
                            catch(Exception ex)
                            {
                                logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                                gPerm = oPerm = 0;
                            }
                        }
                        boolean isOwner = false;
                        boolean isGrpMem = false;
                        if(uid == ovfUid) isOwner = true;
                        String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                        logger.trace("GroupsList: " + groupList);
                        boolean isAdmin;
                        for(int i=0; i<groups.length; i++)
                        {
                            if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                            {
                                isAdmin = true;
                                logger.trace("Setting isAdmin to true.");
                            }
                            if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                        }
                        
                        //now proceed with the actions implementation
                        if(isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                        {
                            if(ovfValue != null && ovfaction.equalsIgnoreCase("initialize"))
                            {
                                try
                                {   
                                    ovfParser = new OpenNebulaOVFParser(ovfValue, oneVersion, oneAdmin);
                                    
                                    String appName = ovfParser.getApplicationName();
                                    int vmCount = ovfParser.getCount();
                                    String[] ovfIds = ovfParser.getIDs();
                                    //first get a list of ovfids corresponding to this ovf which may be already present in the database
                                    rs = db.query("select", "vmid, ovfid", "vmachinetemplate", "where ovfsno=" + ovfsno + "");
                                    int count = 0;
                                    while(rs.next())
                                    {
                                        //check if this ovfid is in the list of ovfids , if not mark as orphan for garbage collector to clean up periodically
                                        count++;
                                        boolean isFound = false;
                                        for(int i=0; i<ovfIds.length; i++)
                                        {
                                            if(ovfIds[i].equalsIgnoreCase(rs.getString("ovfid"))) isFound = true;
                                        }
                                        if(!isFound)
                                        {
                                            //mark this entry as orphaned. 2 letter code: OR
                                            db.update("vmachinetemplate", "state='OR'", "where vmid=" + rs.getInt("vmid") + " AND ovfid='" + rs.getString("ovfid") + "'");
                                        }
                                        rs = db.query("select", "vmid, ovfid", "vmachinetemplate", "where ovfsno=" + ovfsno + "");
                                        for(int i=0; i<count; i++) rs.next();
                                    }
                                    rs.close();
                                    
                                    //now update an existing entry or add a new one if needed.
                                    for(int i=0; i<ovfIds.length; i++)
                                    {
                                        //first get the VM template
                                        String vmTemplate = ovfParser.getVMTemplate(ovfIds[i]);
                                        rs = db.query("select", "vmid, state", "vmachinetemplate", "where ovfsno=" + ovfsno + " AND ovfid='" + ovfIds[i] + "'");
                                        boolean status = false;
                                        int vmid = 0;
                                        if(rs.next())
                                        {
                                            //now update the entry ...
                                            vmid = rs.getInt("vmid");
                                            String state = rs.getString("state");
                                            //if state was ER then change to IN
                                            if(state.equalsIgnoreCase("er"))
                                                status = db.update("vmachinetemplate", "appname='" + appName + "', descp='" + vmTemplate + "', state='IN'", "where vmid=" + vmid);
                                            else //do not change the state
                                                status = db.update("vmachinetemplate", "appname='" + appName + "', descp='" + vmTemplate + "'", "where vmid=" + vmid);
                                            rs.close();
                                            if(status) logger.debug("Vmachine Template was updated successfully for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i]);
                                            else logger.warn("Vmachine Template could not be updated for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i]);
                                        }
                                        else
                                        {
                                            //create a new entry
                                            rs = db.query("select", "max(vmid)", "vmachinetemplate", "");
                                            if(rs.next()) vmid = rs.getInt(1) + 1;
                                            status = db.insert("vmachinetemplate", "(" + vmid + ", " + ovfUid + ", '" + ovfGroup + "', '" + ovfPerm + "', '" +
                                                    appName + "', '-1', '-1', 'IN', " + ovfsno + ", '" + ovfIds[i] + "', -1, '" + vmTemplate + "')");
                                            if(status) logger.debug("Vmachine Template was added successfully for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i]);
                                            else logger.warn("Vmachine Template could not be added for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i]);
                                            rs.close();
                                        }
                                        //now do the same for image templates and at the end return the list of template ids back to the federation.
                                        
                                        String[] disks = ovfParser.getVMDisksId(ovfIds[i]);
                                        rs = db.query("select", "*", "diskimage", "where vmid=" + vmid);
                                        while(rs.next())
                                        {
                                            int imageid = rs.getInt("id");
                                            String imageName = rs.getString("name");
                                            logger.debug("Found disk: ImageID" + imageid + ", imageName: " + imageName);
                                            boolean found = false;
                                            for(int k=0; k<disks.length; k++)
                                                if(disks[k].equalsIgnoreCase(imageName)) found = true;
                                            if(!found) db.update("diskimage", "state='OR'", "where id=" + imageid);
                                        }
                                        rs.close();
                                        
                                        for(int j=0; j<disks.length; j++)
                                        {
                                            String diskTemplate = ovfParser.getImageTemplate(ovfIds[i], disks[j]);
                                            String diskPath = ovfParser.getImageDiskPath(ovfIds[i], disks[j]);
                                            //if image template has changed then set the image state to IN initialization
                                            //IN provides hint that the image template has not been submitted to the ONE
                                            rs = db.query("select", "*", "diskimage", "where vmid=" + vmid + " AND name='" + disks[j] + "'");
                                            if(rs.next())
                                            {
                                                int id = rs.getInt("id");
                                                if(!rs.getString("oneimgdescp").equalsIgnoreCase(diskTemplate))
                                                {
                                                    db.update("diskimage", "state='IN', oneimgdescp='" + diskTemplate + "', localpath='" + diskPath + "'", "where id=" + id);
                                                }
                                                rs.close();
                                            }
                                            else
                                            {
                                                //create a new entry
                                                int imgid = 0;
                                                rs = db.query("select", "max(id)", "diskimage", "");
                                                if(rs.next()) imgid = rs.getInt(1) + 1;
                                                status = db.insert("diskimage", "(" + imgid + ", '-1', '" + disks[j] + "', '-1', -1, " + vmid + ", 'IN', '" 
                                                        + diskTemplate + "', '" + diskPath + "')");
                                                if(status) logger.debug("Disk Template was added successfully for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i] + " for diskId: " + disks[j]);
                                                else logger.warn("Disk Template could not be added for OVF Sno: " + ovfsno + ", ovfid: " + ovfIds[i] + " for diskId: " + disks[j]);
                                                rs.close();
                                            }
                                        }
                                    }
                                    //user uid must be translated to opennebula username before issueing any commands to ONE front end       
                                    //now generating the list of vmachinetemplates and list of disks to return back to REST client
                                    rs = db.query("select", "*", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                    JSONArray vmTemplatesIds = new JSONArray();
                                    JSONArray templateStatus = new JSONArray();
                                    JSONArray ovfIdentifiers = new JSONArray();
                                    String applicationName = "";
                                    while(rs.next())
                                    {
                                        applicationName = rs.getString("appname");
                                        vmTemplatesIds.add(rs.getInt("vmid"));
                                        templateStatus.add(rs.getString("state"));
                                        ovfIdentifiers.add(rs.getString("ovfid"));
                                    }
                                    rs.close();
                                    response.put("application_name", applicationName);
                                    response.put("ovfid_list", ovfIdentifiers);
                                    response.put("template_ids", vmTemplatesIds);
                                }
                                catch(Exception ex)
                                {
                                    response.put("error", "SERVER_ERROR_INTERNAL");
                                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                    ex.printStackTrace(System.err);
                                }
                            }
                            else if(ovfaction.equalsIgnoreCase("deploy"))
                            {
                                //here for the ovfsno to be deployed, get all disks
                                //and decide cross-ref with ONE imagelist to see
                                //if the image template is already regsitered in ONE system
                                //if not then register the image in the ONE system
                                //also get the ONE user mapped to the requesting user
                                
                                JSONArray imgStateList = new JSONArray();
                                JSONArray vmStateList = new JSONArray();
                                
                                rs = db.query("select", "oneuser, onepass, oneid", "user", "where username='" + username + "'");
                                String oneUser = "";
                                String onePass = "";
                                int oneId = -1;
                                
                                if(rs.next())
                                {
                                    oneUser = rs.getString("oneuser");
                                    onePass = rs.getString("onepass");
                                    oneId = rs.getInt("oneid");
                                }
                                rs.close();
                                if(oneId != -1)
                                {
                                    logger.debug("Retrieved corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + ", one-id: " + oneId + "].");
                                    ONExmlrpcHandler onehandle = null;
                                    ONE3xmlrpcHandler one3handle = null;
                                    if(oneVersion.startsWith("2.2"))
                                    {
                                        onehandle = new ONExmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerOVFDetails:OVFaction-deploy");
                                    }
                                    else if(oneVersion.startsWith("3.4"))
                                    {
                                        one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerOVFDetails:OVFaction-deploy");
                                    }
                                    
                                    //now retrieve the diskimages corresponding to the ovfsno and see if
                                    //some are already in the ONE system, if they are update the db entries filling in necessary details
                                    //for the remaining entries, issue image templates for registration to the ONE front end.
                                    rs = db.query("select", "vmid, state", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                    String vmtemplateState = "ND";
                                    String condition = "WHERE ";
                                    if(rs.next())
                                    {
                                        vmtemplateState = rs.getString("state");
                                        condition += "vmid=" + rs.getInt("vmid");
                                    }
                                    if(vmtemplateState.equalsIgnoreCase("IN") || vmtemplateState.equalsIgnoreCase("DP"))
                                    {
                                        while(rs.next())
                                        {
                                            condition += " OR vmid=" + rs.getInt("vmid");
                                        }
                                        rs.close();
                                        String uuid ="";
                                        //getcertificate
                                        try{
                                            if(certUser!=null)
                                                uuid=SSLCertHandler.getCertDetails(certUser, "uuid");
                                        }catch(Exception e)
                                        {
                                            uuid="1";
                                            logger.warn("defaulted to user 1 for pep check");
                                        }
                                        if(VEPHelperMethods.pdpCheckVMDeployment(uid, ovfsno, uuid))
                                        {
                                            rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", condition);
                                            int counter = 0;
                                            while(rs.next())
                                            {
                                                counter++;
                                                String imgName = rs.getString("name");
                                                String imgPath = rs.getString("localpath");
                                                String imgState =rs.getString("state");
                                                String imgTemplate = rs.getString("oneimgdescp");
                                                int imgId = rs.getInt("id");
                                                if(!imgState.equalsIgnoreCase("OR")) //if state is ORPHANed then do not do anything, it will be garbage collected soon
                                                {
                                                    //System.err.println("DEBUG: imgName: " + imgName + " imgPath: " + imgPath + " imgState: " + imgState);
                                                    //getting the already registered ONE image list
                                                    LinkedList<ONEImage> imageList = null;
                                                    if(onehandle != null)
                                                        imageList = onehandle.getImageList();
                                                    else if(one3handle != null)
                                                        imageList = one3handle.getImageList();
                                                    
                                                    boolean found = false;
                                                    for(int i=0; i< imageList.size(); i++)
                                                    {
                                                        ONEImage img = imageList.get(i);
                                                        logger.debug("Got ONE Image Details: " + img.imageName + ", " + img.localPath + ", " + img.state);
                                                        if(img.imageName.equalsIgnoreCase(imgName) && img.localPath.equalsIgnoreCase(imgPath))
                                                        {
                                                            //System.err.println("Comparision Matches, inside if.");
                                                            //update the diskimage entry with missing details
                                                            boolean status = db.update("diskimage", "oneimgname='" + img.imageName + "', oneimgid=" + img.id + ", state='RG'", "where id=" + imgId);
                                                            if(status)
                                                            {
                                                                logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB updated.");
                                                                imgStateList.add(imgName + ":RG");
                                                            }
                                                            else
                                                            {
                                                                logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB update failed.");
                                                                imgStateList.add(imgName + ":" + imgState);
                                                            }
                                                            //System.err.println("Setting found=true and beaking.");
                                                            found = true;
                                                            break;
                                                        }
                                                    }

                                                    if(!found)
                                                    {
                                                        //issue ONE image register command and store corresponding data
                                                        //for now we assume that the VM image resides locally.
                                                        int oneImgId = -1;
                                                        if(onehandle != null)
                                                            oneImgId = onehandle.addImage(imgTemplate);
                                                        else if(one3handle != null)
                                                            oneImgId = one3handle.addImage(imgTemplate);
                                                        
                                                        if(oneImgId != -1)
                                                        {
                                                            ONEImage temp = null;
                                                            if(onehandle != null)
                                                                temp = onehandle.getImageInfo(oneImgId);
                                                            else if(one3handle != null)
                                                                temp = one3handle.getImageInfo(oneImgId);
                                                            
                                                            boolean status = db.update("diskimage", "oneimgname='" + temp.imageName + "', oneimgid=" + temp.id + ", state='RG'", "where id=" + imgId);
                                                            if(status)
                                                            {
                                                                logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB updated.");
                                                                imgStateList.add(imgName + ":RG");
                                                            }
                                                            else
                                                            {
                                                                logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB update failed.");
                                                                imgStateList.add(imgName + ":" + imgState);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logger.warn("Some error while registering image " + imgName + " disk-id: " + imgId + " with ONE.");
                                                            imgStateList.add(imgName + ":" + imgState);
                                                            response.put("error", "SERVER_ERROR_INTERNAL");
                                                            this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                                        }
                                                    }
                                                }
                                                rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", condition);
                                                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                                            }
                                            rs.close();

                                            response.put("image_state_list", imgStateList);

                                            //now issue start for each Vmachine template entry for this ovfsno
                                            rs = db.query("select", "vmid, state, ovfid, appname, descp", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                            counter = 0;
                                            while(rs.next())
                                            {
                                                counter++;
                                                String state = rs.getString("state");
                                                String ovfid = rs.getString("ovfid");
                                                String appname = rs.getString("appname");
                                                String template = rs.getString("descp");
                                                int vmid = rs.getInt("vmid");
                                                if(!state.equalsIgnoreCase("OR")) //ignore OR state as it will be garbage collected soon
                                                {
                                                    //depending on the SLA placement restrictions, add placement section in the template part before deploying
                                                    //ONEVm temp = onehandle.getVmInfo(44);
                                                    int onevmid = -1;
                                                    if(onehandle != null)
                                                        onevmid = onehandle.addVM(template + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                                                    else if(one3handle != null)
                                                        onevmid = one3handle.addVM(template + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                                                    
                                                    Thread.sleep(2000); //two seconds gap between VM submission as a precaution
                                                    if(onevmid != -1)
                                                    {
                                                        ONEVm temp = null;
                                                        if(onehandle != null)
                                                            temp = onehandle.getVmInfo(onevmid);
                                                        else if(one3handle != null)
                                                            temp = one3handle.getVmInfo(onevmid);

                                                        //now store in the vmachine table, also change the state of the ovf entry to DP from ND
                                                        ResultSet rs1 = db.query("select", "max(id)", "vmachine", "");
                                                        int vid = 0;
                                                        if(rs1.next()) vid = rs1.getInt(1) + 1;
                                                        rs1.close();
                                                        boolean status = db.insert("vmachine", "(" + vid + ", " + uid + ", 'OpenNebula', " + temp.id + ", " + vmid + ", '" + temp.name + "', 'DP', -1, '" +
                                                                temp.ip + "', '" + temp.graphics_port + "', '" + temp.graphics_ip + "', '')");
                                                        if(status)
                                                        {
                                                            if(one3handle != null)
                                                            {
                                                                //extra step to deploy the VM
                                                                int hid = VEPHostPool.getNextHost();
                                                                if(hid != -1)
                                                                {
                                                                    status = one3handle.deployVM(onevmid, hid);
                                                                    if(status)
                                                                    {
                                                                        logger.debug("VM with ONE3ID:" + onevmid + " was successfully allocated and deployed on machine with ONEhostID: " + hid + ".");
                                                                    }
                                                                    else
                                                                    {
                                                                        logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment on machine with ONEhostID: " + hid + " FAILED.");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment FAILED because of unavailability of host to deploy.");
                                                                }
                                                            }
                                                            vmStateList.add(vid + ":" + appname + ":" + ovfid + ":" + temp.name + ":DP:" + temp.id);
                                                            logger.debug("Successfully deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                                            db.update("vmachinetemplate", "state='DP'", "where vmid=" + vmid);
                                                        }
                                                        else
                                                        {
                                                            //vid : ApplicationName : ovfid : ONE Name : STATE : ONE Id
                                                            vmStateList.add(vid + ":" + appname + ":" + ovfid + ":" + temp.name + ":UN:" + temp.id);
                                                            logger.warn("Failed to store deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                                            db.update("vmachinetemplate", "state='UN'", "where vmid=" + vmid);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logger.warn("Some error while deploying VM " + vmid + ":" + ovfid + " ovf-id: " + ovfsno + " with ONE.");
                                                        db.update("vmachinetemplate", "state='ER'", "where vmid=" + vmid);
                                                        vmStateList.add("-1:" + appname + ":" + ovfid + ":-na-:ER:-1");
                                                        response.put("error", "SERVER_ERROR_INTERNAL");
                                                        this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                                    }
                                                }
                                                rs = db.query("select", "vmid, state, ovfid, appname, descp", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                                            }
                                            rs.close();
                                            response.put("vm_state_list", vmStateList);
                                            db.update("ovf", "state='DP'", "where sno=" + ovfsno);
                                        }
                                        else
                                        {
                                            this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                                            response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                                            response.put("origin", "PDP Check denied authorization");
                                        }
                                    }
                                    else
                                    {
                                        rs.close();
                                        //first issue a initialize command
                                        logger.warn("Problem deploying the OVF as it was not initialized.");
                                        response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                                        this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                                    }
                                }
                                else
                                {
                                    logger.warn("Problem resolving one username and password for the user " + username);
                                    response.put("error", "CLIENT_ERROR_FAILED_DEPENDENCY");
                                    this.setStatus(Status.CLIENT_ERROR_FAILED_DEPENDENCY);
                                }
                            }
                            else if(ovfaction.equalsIgnoreCase("start"))
                            {
                                
                            }
                            else if(ovfaction.equalsIgnoreCase("suspend"))
                            {
                                
                            }
                            else if(ovfaction.equalsIgnoreCase("resume"))
                            {
                                
                            }
                            else if(ovfaction.equalsIgnoreCase("stop"))
                            {
                                rs = db.query("select", "oneuser, onepass, oneid", "user", "where username='" + username + "'");
                                String oneUser = "";
                                String onePass = "";
                                int oneId = -1;
                                
                                if(rs.next())
                                {
                                    oneUser = rs.getString("oneuser");
                                    onePass = rs.getString("onepass");
                                    oneId = rs.getInt("oneid");
                                }
                                rs.close();
                                if(oneId != -1)
                                {
                                    logger.debug("Retrieved corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + ", one-id: " + oneId + "].");
                                    ONExmlrpcHandler onehandle = null;
                                    ONE3xmlrpcHandler one3handle = null;
                                    if(oneVersion.startsWith("2.2"))
                                    {
                                        onehandle = new ONExmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerOVFDetails:OVFaction-stop");
                                    }
                                    else if(oneVersion.startsWith("3.4"))
                                    {
                                        one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerOVFDetails:OVFaction-stop");
                                    }
                                    
                                    //now retrieve the diskimages corresponding to the ovfsno and see if
                                    //some are already in the ONE system, if they are update the db entries filling in necessary details
                                    //for the remaining entries, issue image templates for registration to the ONE front end.
                                    rs = db.query("select", "state", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                    String vmtemplateState = "ND";
                                    if(rs.next())
                                    {
                                        vmtemplateState = rs.getString("state");
                                    }
                                    rs.close();
                                    if(vmtemplateState.equalsIgnoreCase("DP") || vmtemplateState.equalsIgnoreCase("IN"))
                                    {
                                        rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                        int counter = 0;
                                        while(rs.next())
                                        {
                                            counter++;
                                            int vmid = rs.getInt("vmid");
                                            //now reset this vmachine template to IN
                                            db.update("vmachinetemplate", "state='IN'", "where vmid=" + vmid);
                                            //get list of all VMs corresponding to this vmid
                                            ResultSet rs1 = db.query("select", "id, onevmid, state", "vmachine", "where vmid=" + vmid);
                                            while(rs1.next())
                                            {
                                                int oneid = rs1.getInt("onevmid");
                                                String state = rs1.getString("state");
                                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                                {
                                                    if(onehandle != null)
                                                        onehandle.shutdownVM(oneid);
                                                    else if(one3handle != null)
                                                        one3handle.shutdownVM(oneid);
                                                    //wait for 5 seconds before sending the next shutdown command
                                                    Thread.sleep(5000);
                                                }
                                            }
                                            rs1.close();
                                            rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                                            for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                                        }
                                        rs.close();
                                        response.put("message", "OVF stop action was completed.");
                                    }
                                    else
                                    {
                                        //first issue a initialize and deploy command
                                        logger.warn("Problem deploying the OVF as it was not initialized.");
                                        response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                                        this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                                    }
                                }
                                else
                                {
                                    logger.warn("Problem resolving one username and password for the user " + username);
                                    response.put("error", "CLIENT_ERROR_FAILED_DEPENDENCY");
                                    this.setStatus(Status.CLIENT_ERROR_FAILED_DEPENDENCY);
                                }
                            }
                            else
                            {
                                //unknown action
                                response.put("error", "SERVER_ERROR_NOT_IMPLEMENTED"); //user must exist at VEP before hand
                                this.setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
                            }
                        }
                        else
                        {
                            //no permission to execute
                            logger.warn("The user has no permission to execute" + ovfaction + " command.");
                            response.put("error", "No permission to execute the command: " + ovfaction + ".");
                            this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                        }
                    }
                    catch(Exception ex)
                    {
                        response.put("error", "SERVER_ERROR_INTERNAL");
                        this.setStatus(Status.SERVER_ERROR_INTERNAL);
                        ex.printStackTrace(System.err);
                    }
                }
                else
                {
                    //return error codes
                }
                //response.put("error", "SERVER_ERROR_NOT_IMPLEMENTED"); //user must exist at VEP before hand
                //this.setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
                
            }
        }
        StringRepresentation out = new StringRepresentation(response.toJSONString(), MediaType.APPLICATION_JSON);
        return out;
    }
}
