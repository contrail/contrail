/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import org.apache.log4j.Logger;
/**
 *
 * @author piyush
 */
public class PeriodicThread implements Runnable
{
    private Thread t;
    private long periodicity;
    private String objectType;
    private String actionType;
    private JTextArea logArea;
    private boolean killSignal;
    private JPanel panelStatus;
    private String[] parameters;
    private boolean isAlive;
    
    
    private JLabel output;
    private JComboBox list;
    private Logger logger;
    private VEPDBSyncCleanup dbsync;
    private String vepProperties;
    private ClusterStats runningStat;
    private DirtyFlag dirtyFlag;
    private ONEWorker oneWorker;
    private ONE3Worker one3Worker;
    private String oneVersion;
    private VEPHostPool hostPool;
    
    public PeriodicThread(long period, String obj, String action, JTextArea log, JLabel out, JComboBox ls, JPanel stat, String param, String vepProp, ClusterStats cstat, DirtyFlag dflag, VEPHostPool hpool)
    {
        periodicity = period;
        objectType = obj;
        actionType = action;
        logArea = log;
        output = out;
        list = ls;
        panelStatus = stat;
        logger = Logger.getLogger("VEP.PeriodicThread");
        vepProperties = vepProp;
        runningStat = cstat;
        killSignal = false;
        parameters = param.split(":"); //param to be provided has to be : separated
        dirtyFlag = dflag;
        oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        hostPool = hpool;
        t = new Thread(this);
    }
    
    public void start()
    {
        logger.trace("Starting thread " + objectType + " with [parameters[0]: " + parameters[0] + ", periodicity: " + periodicity + "].");
        t.start();
        isAlive = true;
    }
    
    public void terminate()
    {
        killSignal = true;
        //now try to awake the thread if asleep
        try
        {
            t.interrupt();
            logger.trace("Stopping thread " + objectType + " [parameters[0]: " + parameters[0] + ", periodicity: " + periodicity + "].");
        }
        catch(Exception ex)
        {
            
        }
        if(panelStatus != null)
            panelStatus.setBackground(Color.red);
    }

    @SuppressWarnings("SleepWhileInLoop")
    public void run() 
    {
        logger.trace("Thread " + objectType + " with action=" + actionType + " started.");
        if(objectType.equalsIgnoreCase("one-xmlrpc") && actionType.equalsIgnoreCase("hostmonitor"))
        {
            if(parameters.length != 4)
            {
                JDialog errorMessage;
                JOptionPane errMess = new JOptionPane("ONE XMLRPC parameters are not set correctly.<br>Go to <b>Edit -> Settings</b> and set parameters and try again.", JOptionPane.ERROR_MESSAGE);
                errorMessage = errMess.createDialog(output.getRootPane(), "Action can not be completed");
                logger.error("PeriodicThread: " + objectType + ":" + actionType + " :: incorrect parameters passed!");
                errorMessage.setVisible(true);
                panelStatus.setBackground(Color.ORANGE);
            }
        }
        else if(objectType.equalsIgnoreCase("vepdb-sync") && actionType.equalsIgnoreCase("dosync"))
        {
            dbsync = new VEPDBSyncCleanup(vepProperties);
        }
        while(!killSignal)
        {
            try
            {
                if(objectType.equalsIgnoreCase("one-xmlrpc") && actionType.equalsIgnoreCase("hostmonitor") && (parameters.length == 4))
                {
                    if(oneVersion.startsWith("2.2"))
                    {
                        oneWorker = new ONEWorker(list, output, vepProperties, parameters, runningStat, panelStatus);
                        oneWorker.updateParams();
                    }
                    else if(oneVersion.startsWith("3.4"))
                    {
                        one3Worker = new ONE3Worker(list, output, vepProperties, parameters, runningStat, panelStatus);
                        one3Worker.updateParams();
                    }
                }
                else if(objectType.equalsIgnoreCase("vepdb-sync") && actionType.equalsIgnoreCase("dosync"))
                {
                    if(dbsync != null)
                    {
                        if(dirtyFlag != null && dirtyFlag.dirtyFlag)
                        {
                            dbsync.updateParams();
                            dirtyFlag.dirtyFlag = false;
                        }
                        dbsync.doSyncCleanup();
                    }
                    if(hostPool != null)
                        hostPool.sync();
                }
                Thread.sleep(periodicity);
            }
            catch(Exception ex)
            {
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
            }
        }
        logger.trace("Thread " + objectType + " with action=" + actionType + " stopped.");
        isAlive = false;
    }
    
    public boolean isRunning()
    {
        return isAlive;
    }
}
