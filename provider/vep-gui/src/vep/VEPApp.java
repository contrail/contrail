/*
 * TestApp.java
 */

package vep;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Properties;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.apache.commons.cli.*;

/**
 * The main class of the application.
 */
public class VEPApp extends SingleFrameApplication 
{
    private static BasicParser parser;
    private static CommandLine cli;
    private static HelpFormatter helpFormatter;
    private static String propFile;
    private static String logPropFile;
    private static boolean helpShown;
    private static boolean vepPropProvided;
    private static boolean loggerPropProvided;
    private static boolean noGuiMode;
    private static int logLevel;
    private static String[] arguments;
    private static VEPNoGui vepNoGui;
    private static boolean noTermOut;
    /**
     * At startup create and show the main frame of the application.
     */
    
    @Override protected void startup() 
    {
        if(!helpShown && !noGuiMode)
        {
            show(new VEPView(this, propFile, logPropFile, logLevel, noTermOut));
        }
        else if(!helpShown && noGuiMode)
        {
            vepNoGui = new VEPNoGui(propFile, logPropFile, logLevel, noTermOut);
        }
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) 
    {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of TestApp
     */
    public static VEPApp getApplication() 
    {
        return Application.getInstance(VEPApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) 
    {
        helpShown = false;
        vepPropProvided = false;
        loggerPropProvided = false;
        logLevel = 6; //default set to trace
        noGuiMode = false;
        noTermOut = false;
        arguments = args;
        String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator") + ".vep-gui" + System.getProperty("file.separator");
        if(args.length > 0)
        {
            Options options = new Options();
            options.addOption("p", "vep-properties", true, "path to the VEP properties file");
            options.addOption("l", "log-properties", true, "path to the VEP logger properties file");
            options.addOption("h", "help", false, "usage help");
            //options.addOption("a", "use-pep", false, "use federation policy enforcement (federation access control) in addition to internal resource access control");
            options.addOption("d", "default", false, "start with a default VEP properties file, you must change the defaults to the right values manually");
            options.addOption("v", "log-level", true, "log verbosity level, (0 = off, 1 = fatal, 2 = error, 3 = warn, 4 = info, 5 = debug, 6 = everything)");
            options.addOption("t", "no-gui", false, "terminal only (no GUI) mode");
            options.addOption("s", "supress-term-log", false, "supress logger output to terminal");
            
            parser = new BasicParser();
            try
            {
                cli = parser.parse(options, args);
                
                if(cli.hasOption('p'))
                {
                    propFile = cli.getOptionValue('p');
                    vepPropProvided = true;
                }
                
                if(cli.hasOption('t'))
                {
                    noGuiMode = true;
                }
                
                if(cli.hasOption('s'))
                {
                    noTermOut = true;
                }
    
                if(cli.hasOption('l'))
                {
                    logPropFile = cli.getOptionValue('l');
                    loggerPropProvided = true;
                }
                
                if(cli.hasOption('h'))
                {
                    helpFormatter = new HelpFormatter();
                    helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar VEPController.jar", "Contrail Virtual Execution Platform Controller Module", 
                                    options, 3, 5, "", true);
                    helpShown = true;
                }
                
                if(cli.hasOption('d'))
                {
                    try
                    {
                        //check to see if the default path exists
                        System.out.println("Checking to see if the system default path exists or not.");
                        boolean success = (new File(defaultPath)).mkdir();
                        if(success)
                            System.out.println("Default path " + defaultPath + " was created successfully.");
                        else
                            System.out.println("Default path " + defaultPath + " already exists.");
                    }
                    catch(Exception ex)
                    {
                        System.out.println("Could not create the default directory to store the vep properties, exception caught: " + ex.getMessage());
                    }
                    Properties props = new Properties();
                    System.out.println("Creating the default vep.properties file at " + defaultPath + "vep.properties");
                    FileOutputStream fos = null;
                    String IPval = "127.0.0.1";
                    String port = "2633";
                    String userName = "oneadmin";
                    String userPass = "changeme";
                    String dbFile = "vep.db";
                    String logFile = "vep.log";
                    String size = "1024";
                    String clusterId = "";
                    String restkeyFile = "";
                    String restStorePass = "changeme";
                    String restKeyPass = "changeme";
                    String dbType = "sqlite";
                    String mySqlIP = "127.0.0.1";
                    String mySqlPort = "3306";
                    String mySqlUser = "vepuser";
                    String mySqlPass = "contrail";
                    String restHTTPPort = "10500";
                    String restHTTPSPort = "-1";
                    props.setProperty("vepdb.choice", dbType);
                    props.setProperty("mysql.ip", mySqlIP);
                    props.setProperty("mysql.port", mySqlPort);
                    props.setProperty("mysql.user", mySqlUser);
                    props.setProperty("mysql.pass", mySqlPass);
                    props.setProperty("one.ip", IPval);
                    props.setProperty("one.port", port);
                    props.setProperty("one.user", userName);
                    props.setProperty("one.pass", userPass);
                    props.setProperty("sqlite.db", dbFile);
                    props.setProperty("veplog.file", logFile);
                    props.setProperty("veplog.size", size);
                    props.setProperty("rest.restHTTPPort", restHTTPPort);
                    props.setProperty("rest.restHTTPSPort", restHTTPSPort);
                    props.setProperty("rest.keystore", restkeyFile);
                    props.setProperty("rest.keystorepass", restStorePass);
                    props.setProperty("rest.keypass", restKeyPass);
                    props.setProperty("contrail.cluster", clusterId);
                    props.setProperty("cli.port", "10555");
                    props.setProperty("pdp.use", "false");
                    props.setProperty("pdp.endpoint", "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap");
                    props.setProperty("one.version", "2.2.1");
                    
                    try
                    {
                        props.store((fos = new FileOutputStream(defaultPath + "vep.properties")), "Author: Piyush Harsh");
                        fos.close();
                        System.out.println("Created the vep.properties file.");
                    }
                    catch(Exception ex)
                    {
                        System.out.println("Exception caught while creating the vep properties file at " + defaultPath + "vep.properties");
                        System.out.println("Exception: " + ex.getMessage());
                    }
                }
                
                if(cli.hasOption('v'))
                {
                    String loglevel = cli.getOptionValue('v');
                    try
                    {
                        logLevel = Integer.parseInt(loglevel);
                    }
                    catch(Exception ex)
                    {
                        logLevel = 6;
                    }
                }
                
                if(vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    
                    logPropFile = defaultPath + "log4j.properties";
                    System.out.println("VEP logger properties file path was not specified, using the default values for vep logger properties file: " + logPropFile);
                }
                if(!vepPropProvided && loggerPropProvided && !helpShown)
                {
                    propFile = defaultPath + "vep.properties";
                    System.out.println("VEP system properties file path was not specified, using the default values for vep properties file: " + propFile);
                }
                if(!vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    logPropFile = defaultPath + "log4j.properties";
                    propFile = defaultPath + "vep.properties";
                    System.out.println("VEP system properties file path was not specified, using the default values for vep properties (" + propFile + 
                    ") and vep logger properties files (" + logPropFile + ").");
                }
            }
            catch(ParseException ex)
            {
                helpFormatter = new HelpFormatter();
                helpFormatter.printUsage(new PrintWriter(System.out, true), 80, "Contrail Virtual Execution Platform Controller Module", options);
                helpShown = true;
            }
        }
        else
        {
            logPropFile = defaultPath + "log4j.properties";
            propFile = defaultPath + "vep.properties";
            System.out.println("VEP system properties file path was not specified, using the default values for vep properties (" + propFile + 
                    ") and vep logger properties files (" + logPropFile + ").");
        }
        launch(VEPApp.class, args);
    }
}
