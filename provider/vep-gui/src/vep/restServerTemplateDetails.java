/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
/**
 *
 * @author piyush
 */
public class restServerTemplateDetails extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String vepProperties;
    
    public restServerTemplateDetails()
    {
        vepProperties = VEPHelperMethods.getPropertyFile();
        logger = Logger.getLogger("VEP.restTemplateDetails");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("restServerTemplateDetails", dbType);   
    }
    
    @Get("json")
    public Representation toValue()
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        String templateID = ((String) getRequest().getAttributes().get("id"));
        String username = requestHeaders.getFirstValue("X-Username");
        
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(templateID, username);
        }
        else
        {
            //default rendering ...
            response = toHtml(templateID, username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toHtml(String templateID, String username) throws ResourceException 
    { 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        stringBuilder.append("Template <i>").append(templateID).append("</i> description is shown below<br><br>");
        stringBuilder.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
        stringBuilder.append("<tr>");
        stringBuilder.append("<td valign='top' style='width:128px;background:white;'><img src='https://www.cise.ufl.edu/~pharsh/public/template.png'>");
        stringBuilder.append("<td valign='top' align='left' bgcolor='white' width='*'>");
        
        String[] groups;
        boolean isAdmin = false;
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            stringBuilder.append("<b>User credentials missing ... can not display template details.</b><br><br>");
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs.close();
                    if(isAdmin)
                    {
                        rs = db.query("select", "*", "vmachinetemplate", "where vmid=" + templateID);
                        if(rs.next())
                        {
                            // read the result set
                            stringBuilder.append("<div style='background:#CED1D6;font-weight:bold;'>");
                            stringBuilder.append("Group: ").append(rs.getString("gname")).append("<br>");
                            stringBuilder.append("Permission Bits: ").append(rs.getString("perm")).append("<br>");
                            stringBuilder.append("Application Name: ").append(rs.getString("appname")).append("<br>");
                            stringBuilder.append("State: ").append(rs.getString("state")).append("<br>");
                            stringBuilder.append("OVF Serial: ").append(rs.getInt("ovfsno")).append("<br>");
                            stringBuilder.append("OVF Element ID: ").append(rs.getString("ovfid")).append("<br>");
                            stringBuilder.append("</div><br>");
                            
                            stringBuilder.append("<div style='white-space:normal;word-wrap:normal;background:#ECFCF3;overflow:auto;font-weight:bold;word-wrap:break-word;width:880px;font-family:Console;font-size:8pt;'>");
                            String content = rs.getString("descp");
                            content = StringEscapeUtils.escapeXml(content);
                            stringBuilder.append("<pre>").append(content).append("</pre>");
                            stringBuilder.append("</div>");
                        }
                        else
                        {
                            stringBuilder.append("<div style='background:#99CCCC;'>");
                            stringBuilder.append("No details found ...");   
                            stringBuilder.append("</div>");   
                        }
                        rs.close();
                    }
                    else
                    {
                        rs = db.query("select", "*", "vmachinetemplate", "where vmid=" + templateID);
                        if(rs.next())
                        {
                            // read the result set
                            if(uid == rs.getInt("uid"))
                            {
                                stringBuilder.append("<div style='background:#CED1D6;font-weight:bold;'>");
                                stringBuilder.append("Group: ").append(rs.getString("gname")).append("<br>");
                                stringBuilder.append("Permission Bits: ").append(rs.getString("perm")).append("<br>");
                                stringBuilder.append("Application Name: ").append(rs.getString("appname")).append("<br>");
                                stringBuilder.append("State: ").append(rs.getString("state")).append("<br>");
                                stringBuilder.append("OVF Serial: ").append(rs.getInt("ovfsno")).append("<br>");
                                stringBuilder.append("OVF Element ID: ").append(rs.getString("ovfid")).append("<br>");
                                stringBuilder.append("</div><br>");
                                
                                stringBuilder.append("<div style='white-space:normal;word-wrap:normal;background:#ECFCF3;overflow:auto;font-weight:bold;word-wrap:break-word;width:880px;font-family:Console;font-size:8pt;'>");
                                String content = rs.getString("descp");
                                content = StringEscapeUtils.escapeXml(content);
                                stringBuilder.append("<pre>").append(content).append("</pre>");
                                stringBuilder.append("</div>");
                            }
                            else
                            {
                                stringBuilder.append("<div style='background:#99CCCC;'>");
                                stringBuilder.append("You are not the owner of the resource, access blocked ...");   
                                stringBuilder.append("</div>"); 
                            }
                        }
                        else
                        {
                            stringBuilder.append("<div style='background:#99CCCC;'>");
                            stringBuilder.append("No details found ...");   
                            stringBuilder.append("</div>");   
                        }
                        rs.close();
                    }
                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    stringBuilder.append("<b>User not found ... can not display the template details.</b><br><br>");
                }
            }
            catch(Exception ex)
            {
                stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                stringBuilder.append(ex.getMessage());
                logger.debug("Exception caught: " + ex.getMessage());
                stringBuilder.append("</div>");
            }
        }
        stringBuilder.append("</table><br>");
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
