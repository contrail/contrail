/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
/**
 *
 * @author piyush
 */
public class restServerHostResource extends ServerResource {
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String vepProperties;
    
    public restServerHostResource()
    {
        vepProperties = VEPHelperMethods.getPropertyFile();
        logger = Logger.getLogger("VEP.restHostResource");
//        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, "vep.properties");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("restServerHostResource", dbType);   
    }
    
    @Get("json")
    public Representation getValue() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;

        String username = requestHeaders.getFirstValue("X-Username");
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(username);
            else if(acceptType.contains("json"))
                response = toJson(username);
        }
        else
        {
            //default rendering ...
            response = toHtml(username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toJson(String username) throws ResourceException 
    {
        JSONObject obj = new JSONObject();
        obj.put("title", "List of available hosts");
        JSONArray arr = new JSONArray();
        JSONArray link = new JSONArray();
        int count = 0;
        String[] groups;
        boolean isAdmin = false;
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            obj.put("error", "CLIENT_ERROR_BAD_REQUEST");
        }
        else
        {
            try
            {
                isAdmin = VEPAccessControl.isAdmin(username);
                if(isAdmin)
                {
                    ResultSet rs = db.query("select", "cid", "computenode", "");
                    while(rs.next())
                    {
                        // read the result set
                        int nodeId = rs.getInt("cid");
                        arr.add("Computenode " + nodeId);
                        link.add("/host/" + nodeId);
                        count++;
                    }
                    rs.close();
                    obj.put("hosts", arr);
                    obj.put("links", link);
                    obj.put("count", count);
                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                    obj.put("error", "CLIENT_ERROR_UNAUTHORIZED");
                }
            }
            catch(Exception ex)
            {
                obj.put("error", "SQL error occured.");
                logger.debug("Exception caught: " + ex.getMessage());
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
            }
        }
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
    
    public Representation toHtml(String username) throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        stringBuilder.append("List of available Hosts<br>");
        stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
        int count = 0;
        String[] groups;
        boolean isAdmin = false;
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            stringBuilder.append("</ul><b>User credentials missing ... can not display host list.</b><br><br>");
        }
        else
        {
            try
            {
                isAdmin = VEPAccessControl.isAdmin(username);
                if(isAdmin)
                {
                    ResultSet rs = db.query("select", "cid", "computenode", "");
                    while(rs.next())
                    {
                        // read the result set
                        int nodeId = rs.getInt("cid");
                        stringBuilder.append("<li>Computenode ").append(nodeId).append(" <a href='").append(nodeId).append("' title='details on computenode ").append(nodeId).append("'>/host/").append(rs.getInt("cid")).append("</a>");
                        count++;
                    }
                    rs.close();
                    stringBuilder.append("</ul>");
                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                    stringBuilder.append("</ul><b>Unauthorized user ... can not display host list.</b><br><br>");
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace(System.out);
                stringBuilder.append("</ul>");
                stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                stringBuilder.append(ex.getMessage());
                logger.debug("Exception caught: " + ex.getMessage());
                stringBuilder.append("</div>");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
            }
        }
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
