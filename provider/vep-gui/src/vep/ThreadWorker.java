/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

/**
 *
 * @author piyush
 */
public class ThreadWorker implements Runnable
{
    private Object obj;
    private String objectType;
    private String actionType;
    private Thread t;
    private int statusCode;
    private boolean finished;
    
    public ThreadWorker(String name, String act, Object o)
    {
        objectType = name;
        actionType = act;
        obj = o;
        statusCode = -2; //-2 means that the thread is still processing
        finished = false;
        t = new Thread(this);
    }
    
    public void start()
    {
        t.start();
    }
    
    public int getReturnCode()
    {
        return statusCode;
    }
    
    public boolean hasFinished()
    {
        return finished;
    }
    
    public void run() 
    {
        if(objectType.equalsIgnoreCase("xmlhandler"))
        {
            ovfValidator temp = (ovfValidator)obj;
            if(actionType.equalsIgnoreCase("validate"))
                statusCode = temp.validate();
        }
        finished = true;
    }
}
