/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerVMAction extends ServerResource {
    private dbHandler db;
    private Logger logger;
    private ONExmlrpcHandler oneHandle;
    private ONE3xmlrpcHandler one3Handle;
    private String oneIP;
    private String oneXmlPort;
    private String oneuser;
    private String onepass;
    private String dbType;
    private String vepProperties;
    private String oneVersion;
    
    public restServerVMAction()
    {
        logger = Logger.getLogger("VEP.restVMAction");
        vepProperties = VEPHelperMethods.getPropertyFile();
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        db = new dbHandler("restServerVMAction", dbType);  
    }
    
    @Put("json")
    public Representation doVMaction()
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        JSONObject response = new JSONObject();
        response.put("title", "VM Action Result");
        String id = ((String) getRequest().getAttributes().get("id")); //vm id
        String action = ((String) getRequest().getAttributes().get("action"));
        String username = requestHeaders.getFirstValue("X-Username");
        
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(username == null)
        {
            response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
            this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    oneuser = rs.getString("oneuser");
                    onepass = rs.getString("onepass");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    rs.close();
                    String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList: " + groupList);
                    boolean isAdmin = false;
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    //only allows action on VMs by admins or VM owners and no one else
                    rs = db.query("select", "controller, onevmid, state, uid", "vmachine", "where id=" + id);
                    if(rs.next())
                    {
                        if(uid == rs.getInt("uid") || isAdmin)
                        {
                            int oneid = rs.getInt("onevmid");
                            String controller = rs.getString("controller");
                            String state = rs.getString("state");
                            if(controller.equalsIgnoreCase("opennebula"))
                            {
                                oneHandle = null;
                                one3Handle = null;
                                if(oneVersion.startsWith("2.2"))
                                    oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneuser, onepass, "restServerVMAction");
                                else if(oneVersion.startsWith("3.4"))
                                    one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneuser, onepass, "restServerVMAction");
                            }
                            if((oneHandle == null) && (one3Handle == null))
                            {
                                response.put("error", "SERVER_ERROR_SERVICE_UNAVAILABLE");
                                this.setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE);
                            }
                            else
                            {
                                if(action.equalsIgnoreCase("stop"))
                                {
                                    boolean status = false;
                                    if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                    {
                                        if(oneHandle != null)
                                            status = oneHandle.shutdownVM(oneid);
                                        else if(one3Handle != null)
                                            status = one3Handle.shutdownVM(oneid);
                                    }
                                    if(!status)
                                    {
                                        response.put("error", "Action could not be performed.");
                                        this.setStatus(Status.CLIENT_ERROR_CONFLICT);
                                    }
                                    else
                                    {
                                        response.put("message", "operation sent to ONE.");
                                    }
                                }
                                else if(action.equalsIgnoreCase("suspend"))
                                {
                                    boolean status = false;
                                    if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT") || state.equalsIgnoreCase("DP"))
                                    {
                                        if(oneHandle != null)
                                            status = oneHandle.suspendVM(oneid);
                                        else if(one3Handle != null)
                                            status = one3Handle.suspendVM(oneid);
                                    }
                                    if(!status)
                                    {
                                        response.put("error", "Action could not be performed.");
                                        this.setStatus(Status.CLIENT_ERROR_CONFLICT);
                                    }
                                    else
                                    {
                                        response.put("message", "operation sent to ONE.");
                                    }
                                }
                                else if(action.equalsIgnoreCase("resume"))
                                {
                                    boolean status = false;
                                    if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("SP") || state.equalsIgnoreCase("DP"))
                                    {
                                        if(oneHandle != null)
                                            status = oneHandle.resumeVM(oneid);
                                        else if(one3Handle != null)
                                            status = one3Handle.resumeVM(oneid);
                                    }
                                    if(!status)
                                    {
                                        response.put("error", "Action could not be performed.");
                                        this.setStatus(Status.CLIENT_ERROR_CONFLICT);
                                    }
                                    else
                                    {
                                        response.put("message", "operation sent to ONE.");
                                    }
                                }
                                else if(action.equalsIgnoreCase("restart"))
                                {
                                    boolean status = false;
                                    if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("BT"))
                                    {
                                        if(oneHandle != null)
                                            status = oneHandle.restartVM(oneid);
                                        else if(one3Handle != null)
                                            status = one3Handle.restartVM(oneid);
                                    }
                                    if(!status)
                                    {
                                        response.put("error", "Action could not be performed.");
                                        this.setStatus(Status.CLIENT_ERROR_CONFLICT);
                                    }
                                    else
                                    {
                                        response.put("message", "operation sent to ONE.");
                                    }
                                }
                                else
                                {
                                    //not supported
                                    response.put("error", "SERVER_ERROR_NOT_IMPLEMENTED");
                                    this.setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
                                    logger.debug("VM action received: " + action + ", not supported.");
                                }
                            }
                        }
                        else
                        {
                            //forbidden to perform action
                            response.put("error", "CLIENT_ERROR_UNAUTHORIZED");
                            this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                        }
                        rs.close();
                    }
                    else
                    {
                        //no vm exists for this id
                        response.put("error", "CLIENT_ERROR_NOT_FOUND");
                        this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                        logger.debug("The VM with id " + id + " was not found. Can not perform the action: " + action);
                    }
                }
                else
                {
                    //no user exists at VEP, error
                    response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    logger.debug("Error: Username not found at VEP.");
                }
            }
            catch(Exception ex)
            {
                response.put("error", "SERVER_ERROR_INTERNAL");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                logger.warn("Exception caught inside restVMAction: " + ex.getMessage());
            }
        }
        
        StringRepresentation value = new StringRepresentation(response.toJSONString(), MediaType.APPLICATION_JSON);
        return value; 
    }
}
