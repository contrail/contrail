/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

/**
 *
 * @author piyush
 */

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import javax.swing.JLabel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.opennebula3.client.Client;
import org.opennebula3.client.OneResponse;
import org.opennebula3.client.Pool;
import org.opennebula3.client.cluster.Cluster;
import org.opennebula3.client.cluster.ClusterPool;
import org.opennebula3.client.host.Host;
import org.opennebula3.client.image.Image;
import org.opennebula3.client.image.ImagePool;
import org.opennebula3.client.user.User;
import org.opennebula3.client.vm.VirtualMachine;

public class ONE3xmlrpcHandler extends DefaultHandler
{
    private String oneIP;
    private String onePort;
    private String oneUser;
    private String onePass;
    private Client oneClient;
    private String tempVal;
    private ONEHost node;
    private String hostId;
    private JLabel output;
    private int actionType;
    private VEPComputeNode vepCNode;
    private Logger logger;
    private boolean imageParse;
    private boolean vmParse;
    private boolean graphicsData;
    private boolean clusterParse;
    private boolean singleHostInfo;
    private ONEImage image;
    private ONECluster cluster;
    private LinkedList<ONEImage> imageList;
    private LinkedList<ONECluster> clusterList;
    private ONEVm vmachine;
    
    public ONE3xmlrpcHandler(String IP, String port, String user, String pass, String entryPoint)
    {
        oneIP = IP;
        onePort = port;
        oneUser = user;
        onePass = pass;
        tempVal = "";
        hostId = "";
        actionType = -1;
        output = null;
        imageList = null;
        vmachine = null;
        imageParse = false;
        vmParse = false;
        graphicsData = false;
        clusterParse = false;
        logger = Logger.getLogger("VEP.ONE3xmlrpcHandler");
        try
        {
            logger.debug("Creating one3Client: new Client(" + oneUser + ":" + onePass + ", \"http://" + oneIP + ":" + onePort + "/RPC2\")");
            oneClient  = new Client(oneUser + ":" + onePass, "http://" + oneIP + ":" + onePort + "/RPC2");
            logger.debug("ONE3 xml-rpc handler created successfully. Entrypoint: " + entryPoint);
        }
        catch(Exception ex)
        {
            logger.error("OpenNebula3 XML RPC connection could not be established. Check system settings and restart the application.");
            oneClient = null;
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
    public int addUser(String name, String password) //in ONE 3 provide the normal password
    {
        int value = -1;
        if(oneClient != null)
        {
            OneResponse val = User.allocate(oneClient, name, password);
            logger.debug("Adding OpenNebula3 user " + name + ": Got response: " + val.getMessage());
            //System.out.println(val.getMessage());
            if(val.getMessage() != null)
                value = Integer.parseInt(val.getMessage());
        }
        return value;
    }
    
    public boolean removeUser(int uid)
    {
        boolean value = false;
        if(oneClient != null)
        {
            OneResponse val = User.delete(oneClient, uid);
            if(!val.isError()) value = true;
        }
        return value;
    }
    
    public boolean addHost(String name, String im, String vmm, String vnm)
    {
        if(oneClient != null)
        {
            OneResponse val = Host.allocate(oneClient, name, im, vmm, vnm);
            if(val.isError()) 
            {
                logger.error("ONE3 Host addition failed: " + val.getErrorMessage());
                return false;
            }
            else
            {
                logger.debug("ONE3 Host addition [name=" + name + "] succeeded. " + val.getMessage());
                return true;
            }
        }
        return false; //oneClient is false
    }
    
    public int addCluster(String name)
    {
        logger.debug("Received a request to create a new cluster in ONE3.");
        if(oneClient != null)
        {
            OneResponse val = Cluster.allocate(oneClient, name);
            if(val.isError()) 
            {
                logger.error("Cluster addition failed: " + val.getErrorMessage());
                return -1;
            }
            else
            {
                logger.debug("Cluster addition [name=" + name + "] succeeded. " + val.getMessage());
                return Integer.parseInt(val.getMessage());
            }
        }
        else
        {
            logger.error("ONE3 oneClient object is not properly set.");
        }
        return -1; //oneClient is false
    }
    
    public LinkedList<ONECluster> getClusterList()
    {
        clusterList = null;
        if(oneClient != null)
        {
            clusterList = new LinkedList<ONECluster>();
            String response = "";
            OneResponse val = ClusterPool.info(oneClient);
            if(!val.isError())
            {
                logger.debug("OpenNebula Cluster Pool info retrieved.");
                response = val.getMessage();
                System.out.println("ClusterPool Response: " + response);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                logger.error("OpenNebula Cluster Pool info retrieval failed: " + val.getErrorMessage());
            }
        }
        return clusterList;
    }
    
    public String getClusterInfo(int id)
    {
        if(oneClient != null)
        {
            OneResponse val = Cluster.info(oneClient, id);
            if(val.isError()) 
            {
                logger.error("Error retrieving cluster " + id + " details: " + val.getErrorMessage());
                return null;
            }
            else
            {
                logger.debug("Cluster " + id + " details: " + val.getMessage());
                return val.getMessage();
            }
        }
        return null; //oneClient is false
    }
    
    public boolean removeFromCluster(int hostID, int clusterID)
    {
        if(oneClient != null)
        {
            OneResponse val = Cluster.delHost(oneClient, clusterID, hostID);
            if(val.isError()) 
            {
                logger.error("Host removal from cluster failed: " + val.getErrorMessage());
                return false;
            }
            else
            {
                logger.debug("Host removed [id=" + hostID + "] from cluster succeeded. " + val.getMessage());
                return true;
            }
        }
        return false; //oneClient is false
    }
    
    public int addHostToCluster(int id, int hid)
    {
        logger.debug("ONE3 Received host add request for host: " + hid + " for cluster " + id);
        
        if(oneClient != null)
        {
            OneResponse val = Cluster.addHost(oneClient, id, hid);
            if(val.isError()) 
            {
                logger.error("Host " + hid + " addition to cluster " + id + " failed: " + val.getErrorMessage());
                return -1;
            }
            else
            {
                logger.debug("Host " + hid + " addition to cluster " + id + " succeed: " + val.getMessage());
                return 1;
            }
        }
        return -1;
    }
    
    public void getHostInfo(int hostID, JLabel label, VEPComputeNode vepNode)
    {
        vepCNode = vepNode;
        singleHostInfo = true;
        actionType = 1; //1 == getHostInfo
        hostId = Integer.toString(hostID);
        output = label;
        if(oneClient != null)
        {
            String response = "";
            OneResponse val = Host.info(oneClient, hostID);
            if(!val.isError())
            {
                logger.debug("Host " + hostID + " information retreived successfully.");
                response = val.getMessage();
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                logger.error("Host " + hostID + " information retrieval failed: " + val.getErrorMessage());
            }
        }
        singleHostInfo = false;
    }
    
    public ONEVm getVmInfo(int id)
    {
        vmachine = null;
        if(oneClient != null)
        {
            vmachine = new ONEVm();
            OneResponse val = VirtualMachine.info(oneClient, id);
            String response = "";
            if(!val.isError())
            {
                response = val.getMessage();
                //System.err.println(response);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                vmachine = null;
                logger.error("OpenNebula VM info retrieval failed: " + val.getErrorMessage());
            }
        }
        else
        {
            logger.error("VM " + id + " information retrieval failed: ONE3 connection problem.");
        }
        return vmachine;
    }
    
    public String getVmInfoString(int id)
    {
        String value = "";
        if(oneClient != null)
        {
            OneResponse val = VirtualMachine.info(oneClient, id);
            if(!val.isError())
            {
                value = val.getMessage();
            }
            else
            {
                logger.error("OpenNebula VM info retrieval failed: " + val.getErrorMessage());
            }
        }
        return value;
    }
    
    public boolean restartVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.restart();
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + id + " was sent restart command: ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("VM with ONE id: " + id + " was sent restart command: ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + id + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
    public boolean suspendVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.suspend();
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + id + " was sent suspend command: ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("VM with ONE id: " + id + " was sent suspend command: ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + id + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
    public boolean resumeVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.resume();
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + id + " was sent resume command: ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("VM with ONE id: " + id + " was sent resume command: ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + id + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
    public boolean shutdownVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.shutdown();
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + id + " was sent shutdown command: ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("VM with ONE id: " + id + " was sent shutdown command: ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + id + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
        public boolean deleteVM(int id)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(id, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.finalizeVM();
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + id + " was sent finalize/delete command: ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("VM with ONE id: " + id + " was sent finalize/delete command: ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + id + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
    public int addVM(String template)
    {
        int value = -1;
        if(oneClient != null)
        {
            OneResponse val = VirtualMachine.allocate(oneClient, template);
            logger.debug("Allocating VM in ONE - got response: " + val.getMessage());
            if(!val.isError())
                value = Integer.parseInt(val.getMessage());
            else
            {
                logger.warn("Error submitting VM to ONE: " + val.getErrorMessage());
            }
        }
        return value;
    }
    
    public boolean deployVM(int vmid, int hostid)
    {
        boolean status = false;
        if(oneClient != null)
        {
            VirtualMachine vm = new VirtualMachine(vmid, oneClient);
            if(vm != null)
            {
                OneResponse val = vm.deploy(hostid);
                if(!val.isError())
                {
                    logger.debug("VM with ONE id: " + vmid + " was sent deploy command on onehost " + hostid + ": ONE response - " + val.getMessage());
                    status = true;
                }
                else
                {
                    logger.warn("Deployment of VM with ONE id: " + vmid + " FAILED on host " + hostid + ": ONE response - " + val.getErrorMessage());
                    status = false;
                }
            }
            else
            {
                //vm does not exist
                status = false;
                logger.warn("VM with ONE id: " + vmid + " may not exist. Error creating ONE VM object.");
            }            
        }
        return status;
    }
    
    public int addImage(String template)
    {
        int value = -1;
        if(oneClient != null)
        {
            logger.debug("Received this image template:\n" + template);
            OneResponse val = Image.allocate(oneClient, template, 1); //default datastore id is 1 assumed
            logger.debug("Registering image with ONE - got response: " + val.getMessage());
            if(!val.isError())
                value = Integer.parseInt(val.getMessage());
            else
            {
                logger.warn("Error submitting Image to ONE: " + val.getErrorMessage());
            }
        }
        return value;
    }
    
    public LinkedList<ONEImage> getImageList()
    {
        imageList = null;
        if(oneClient != null)
        {
            imageList = new LinkedList<ONEImage>();
            String response = "";
            OneResponse val = ImagePool.info(oneClient, Pool.ALL); //-2 returns all VMs
            if(!val.isError())
            {
                logger.debug("OpenNebula Image Pool info retrieved.");
                response = val.getMessage();
                //System.out.println("Imagepool Response: " + response);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                logger.error("OpenNebula Image Pool info retrieval failed: " + val.getErrorMessage());
            }
        }
        return imageList;
    }
    
    public ONEImage getImageInfo(int id)
    {
        imageList = null;
        if(oneClient != null)
        {
            imageList = new LinkedList<ONEImage>();
            OneResponse val = Image.info(oneClient, id);
            String response = "";
            if(!val.isError())
            {
                response = val.getMessage();
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            else
            {
                logger.error("OpenNebula Image info retrieval failed: " + val.getErrorMessage());
            }
        }
        return imageList.getFirst();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
    {
        tempVal = "";
        if(qName.equalsIgnoreCase("HOST"))
        {
            node = new ONEHost(); //instatiate a new host object
	}
        if(qName.equalsIgnoreCase("VM"))
        {
            vmParse = true;
	}
        if(qName.equalsIgnoreCase("IMAGE_POOL"))
        {
            imageParse = true;
	}
        if(qName.equalsIgnoreCase("GRAPHICS"))
        {
            graphicsData = true;
	}
        if(qName.equalsIgnoreCase("CLUSTER_POOL"))
        {
            clusterParse = true;
	}
        if(qName.equalsIgnoreCase("CLUSTER"))
        {
            if(clusterParse)
                cluster = new ONECluster();
	}
        if(qName.equalsIgnoreCase("IMAGE"))
        {
            if(!vmParse)
                imageParse = true;
            image = new ONEImage();
	}
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
	tempVal = new String(ch,start,length);
        if(tempVal.contains("CDATA"))
        {
            
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equalsIgnoreCase("ID"))
        {
            if(imageParse)
            {
                image.id = Integer.parseInt(tempVal);
            }
            else if(vmParse)
            {
                vmachine.id = Integer.parseInt(tempVal);
            }
            else if(clusterParse)
            {
                cluster.id = Integer.parseInt(tempVal);
            }
            else
            {
                node.id = tempVal;
                vepCNode.hostID = Integer.parseInt(tempVal);
            }
        }
        else if(qName.equalsIgnoreCase("UID"))
        {
            if(imageParse)
                image.oneuid = Integer.parseInt(tempVal);
            else if(vmParse)
                vmachine.uid = Integer.parseInt(tempVal);
        }
        else if(qName.equalsIgnoreCase("USERNAME"))
        {
            if(imageParse)
                image.oneusername = tempVal;
        }
        else if(qName.equalsIgnoreCase("UNAME"))
        {
            if(imageParse)
                image.oneusername = tempVal;
        }
        else if(qName.equalsIgnoreCase("NAME"))
        {
            if(imageParse)
            {
                image.imageName = tempVal;
            }
            else if(clusterParse)
            {
                cluster.name = tempVal;
            }
            else if(vmParse)
            {
                vmachine.name = tempVal;
            }
            else
            {
                node.name = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("LAST_POLL"))
        {
            if(vmParse)
                vmachine.last_poll = tempVal;
        }
        else if(qName.equalsIgnoreCase("TYPE"))
        {
            if(imageParse)
                image.imageType = tempVal;
            else if(vmParse && graphicsData)
                vmachine.graphics_type = tempVal;
            //System.out.println("Image Type: " + tempVal);
        }
        else if(qName.equalsIgnoreCase("PUBLIC"))
        {
            if(imageParse)
                image.isPublic = tempVal;
        }
        else if(qName.equalsIgnoreCase("PERSISTENT"))
        {
            if(imageParse)
                image.isPersistent = Integer.parseInt(tempVal);
        }
        else if(qName.equalsIgnoreCase("REGTIME"))
        {
            if(imageParse)
                image.regTime = Long.parseLong(tempVal);
        }
        else if(qName.equalsIgnoreCase("LCM_STATE"))
        {
            if(vmParse)
                vmachine.lcm_state = tempVal;
        }
        else if(qName.equalsIgnoreCase("STIME"))
        {
            if(vmParse)
                vmachine.start_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("ETIME"))
        {
            if(vmParse)
                vmachine.end_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("DEPLOY_ID"))
        {
            if(vmParse)
                vmachine.deploy_id = tempVal;
        }
        else if(qName.equalsIgnoreCase("MEMORY"))
        {
            if(vmParse)
                if(vmachine.memory < 0)
                    vmachine.memory = Long.parseLong(tempVal);
        }
        else if(qName.equalsIgnoreCase("CPU"))
        {
            if(vmParse)
                if(vmachine.cpu < 0)
                    vmachine.cpu = Integer.parseInt(tempVal);
        }
        else if(qName.equalsIgnoreCase("CPUSPEED"))
        {
            if(singleHostInfo)
                vepCNode.cpu_speed = Long.parseLong(tempVal);
        }
        else if(qName.equalsIgnoreCase("LISTEN"))
        {
            if(vmParse && graphicsData)
                vmachine.graphics_ip = tempVal;
        }
        else if(qName.equalsIgnoreCase("PORT"))
        {
            if(vmParse && graphicsData)
                vmachine.graphics_port = Integer.parseInt(tempVal);
        }
        else if(qName.equalsIgnoreCase("BRIDGE"))
        {
            if(vmParse)
                vmachine.bridge = tempVal;
        }
        else if(qName.equalsIgnoreCase("IP"))
        {
            if(vmParse)
            {
                if(vmachine.ip.length() == 0)
                    vmachine.ip = tempVal;
                else
                    vmachine.ip += "," + tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("MAC"))
        {
            if(vmParse)
            {
                //vmachine.mac = tempVal;
                if(vmachine.mac.length() == 0)
                    vmachine.mac = tempVal;
                else
                    vmachine.mac += "," + tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("NETWORK"))
        {
            if(vmParse)
            {
                //vmachine.network = tempVal;
                if(vmachine.network.length() == 0)
                    vmachine.network = tempVal;
                else
                    vmachine.network += "," + tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("NETWORK_ID"))
        {
            if(vmParse)
            {
                //vmachine.network_id = Integer.parseInt(tempVal);
                if(vmachine.network_id.length() == 0)
                    vmachine.network_id = tempVal;
                else
                    vmachine.network_id += "," + tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("STATE"))
        {
            if(imageParse)
            {
                image.state = Integer.parseInt(tempVal);
            }
            else if(vmParse)
            {
                vmachine.state = tempVal;
            }
            else
            {
                node.state = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("RUNNING_VMS"))
        {
            if(imageParse) //this value could be in hosts too
                image.runnVmCount = Integer.parseInt(tempVal);
        }
        else if(qName.equalsIgnoreCase("DESCRIPTION"))
        {
            if(imageParse)
                image.genDesc = tempVal;
        }
        else if(qName.equalsIgnoreCase("PATH"))
        {
            if(imageParse)
                image.localPath = tempVal;
        }
        else if(qName.equalsIgnoreCase("IM_MAD"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.im_mad = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("VM_MAD"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.vm_mad = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("VN_MAD"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.vn_mad = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("LAST_MON_TIME"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.mon_time = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("CLUSTER"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else if(clusterParse)
            {
                clusterList.add(cluster);
            }
            else
            {
                node.cluster = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("HID"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                vmachine.host_id = Integer.parseInt(tempVal);
            }
            else
            {
                node.hid = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("DISK_USAGE"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.disk_usage = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("MEM_USAGE"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.mem_usage = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("CPU_USAGE"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.cpu_usage = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("MAX_DISK"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.max_disk = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("MAX_MEM"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.max_mem = tempVal;
                vepCNode.mem = Integer.parseInt(tempVal);
            }
        }
        else if(qName.equalsIgnoreCase("MAX_CPU")) //refers to the number of cores
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.max_cpu = tempVal;
                vepCNode.no_cpu_cores = Integer.parseInt(tempVal);
            }
        }
        else if(qName.equalsIgnoreCase("FREE_DISK"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.free_disk = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("FREE_MEM"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.free_mem = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("FREE_CPU"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.free_cpu = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("USED_DISK"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.used_disk = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("USED_MEM"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.used_mem = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("USED_CPU"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.used_cpu = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("RUNNING_VMS"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.running_vms = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("HOSTNAME"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.hostName = tempVal;
                vepCNode.hostname = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("HYPERVISOR"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.hypervisor = tempVal;
                vepCNode.virtualization = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("MODELNAME"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.modelname = tempVal;
                vepCNode.description = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("ARCH"))
        {
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                node.arch = tempVal;
                vepCNode.cpuArch = tempVal;
            }
        }
        else if(qName.equalsIgnoreCase("HOST") && actionType == 1)
        {
            //print the data to the JLabel object
            if(imageParse)
            {
                
            }
            else if(vmParse)
            {
                
            }
            else
            {
                if(output!=null)
                {
                    String toPrint = "";
                    toPrint += "<html><div style='color:black;background:white;width:370px;font-family:Verdana;font-size:10pt'><b>Node information for ID=" + hostId + " is displayed below.</b><br>";
                    toPrint += "<table style='background:white;font-family:Verdana;font-size:10pt;border:0px;' cellpadding='2' cellspacing='1'>";
                    toPrint += node.getHTML3FormattedOutput();
                    toPrint += "</table></div></html>";
                    output.setText(toPrint);
                }
            }
        }
        else if(qName.equalsIgnoreCase("CLUSTER_POOL"))
        {
            if(clusterParse)
                clusterParse = false;
        }
        else if(qName.equalsIgnoreCase("IMAGE_POOL"))
        {
            if(imageParse)
                imageParse = false;
        }
        else if(qName.equalsIgnoreCase("GRAPHICS"))
        {
            graphicsData = false;
        }
        else if(qName.equalsIgnoreCase("VM"))
        {
            if(vmParse)
                vmParse = false;
        }
        else if(qName.equalsIgnoreCase("IMAGE"))
        {
            if(imageParse)
            {
                imageList.add(image);
                imageParse = false;
            }
        }
    }
}
