/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

/**
 *
 * @author piyush
 */
public class ClusterStats 
{
    public double freeMemory;
    public double usedMemory;
    public double freeCPU;
    public double usedCPU;
    public double freeDisk;
    public double freeNetwork;
    public double usedDisk;
    public double usedNetwork;
    
    public ClusterStats()
    {
        freeMemory = 0.0;
        usedMemory = 0.0;
        freeCPU = 0.0;
        usedCPU = 0.0;
        freeDisk = 0.0;
        usedDisk = 0.0;
    }
    
    
    public void reset()
    {
        freeMemory = usedMemory = freeCPU = usedCPU = freeDisk = usedDisk = 0.0;
    }
}
