/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.ChallengeScheme;
import org.restlet.routing.Router;
import org.restlet.security.ChallengeAuthenticator;
import org.restlet.security.MapVerifier;
/**
 *
 * @author piyush
 */
public class restServerRouter extends Application
{
    @Override
    public synchronized Restlet createInboundRoot() 
    //public synchronized Restlet createRoot() 
    {
        // Create a router Restlet that routes each call to a new instance of HelloWorldResource.
        Router router = new Router(getContext());
        
        // Defines only one route
        MapVerifier verifier = new MapVerifier();
        verifier.getLocalSecrets().put("admin", "pass1234".toCharArray());
        ChallengeAuthenticator guard = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Interface Access");
        guard.setVerifier(verifier);
        
        router.attach("/", restServerRootResource.class);
        
        //router.attach("/host/", guard);
        //guard.setNext(restServerHostResource.class);
        
        
        router.attach("/host/", restServerHostResource.class);
        
        router.attach("/host/{id}", restServerHostDetails.class);
        
        router.attach("/user/", restServerUserResource.class);
        router.attach("/user/{name}", restServerUserResource.class);
        
        router.attach("/ovf/", restServerOVFResource.class);
        router.attach("/ovf/{name}", restServerOVFResource.class);
        //router.attach("/ovf/{name}/action/{action}", restServerOVFResource.class);
        router.attach("/ovf/id/{sno}", restServerOVFDetails.class);
        router.attach("/ovf/id/{sno}/action/{action}", restServerOVFDetails.class);
        
        router.attach("/vm/", restServerVMResource.class);
        router.attach("/vm/{id}", restServerVMResource.class);
        router.attach("/vm/{id}/action/{action}", restServerVMAction.class);
        
        router.attach("/template/", restServerTemplateResource.class);
        router.attach("/template/{id}", restServerTemplateDetails.class);
        router.attach("/template/{id}/action/deploy", restServerTemplateAction.class);
        
        router.attach("/appcontrol/",restServerAppControl.class);
        router.attach("/appcontrol/doaction/{type}",restServerAppControl.class);
        
        ChallengeAuthenticator guard1 = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_BASIC, "VEP REST Admin Interface Access");
        MapVerifier verifier1 = new MapVerifier();
        verifier1.getLocalSecrets().put("admin-vep", "pass1234".toCharArray());
        guard1.setVerifier(verifier1);
        router.attach("/admin/", guard1);
        guard1.setNext(restServerAdminControl.class);
        return router;
    }
}
