/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.util.Series;
import org.apache.log4j.Logger;
/**
 *
 * @author piyush
 */
public class restServer implements Runnable
{
    private Component component;
    private int restPort;
    private int restHTTPSPort;
    //ChallengeAuthenticator guard;
    //MapVerifier mapVerifier;
    private Thread t;
    private Logger logger;
    private boolean state;
    private boolean clientAuthEnabled;
    private String keyStoreFile;
    private String keyStorePass; //key for the JKS
    private String keyPass; //key for the server certificate
    private Server server;
    
    public restServer(int port, int secureport, boolean clientAuth, String keyPath, String storePass, String key)
    {
        state = true;
        restPort = port;
        restHTTPSPort = secureport;
        clientAuthEnabled = clientAuth;
        keyStoreFile = keyPath;
        keyStorePass = storePass;
        keyPass = key;
        logger = Logger.getLogger("VEP.restServer");
        component = new Component();
        component.getDefaultHost().attach(new restServerRouter());
        logger = org.apache.log4j.Logger.getLogger("VEP.restServer");
        
        server = null;
        try
        {
            if(restHTTPSPort != -1)
            {
                server = component.getServers().add(Protocol.HTTPS, restHTTPSPort);

                Series<Parameter> parameters = server.getContext().getParameters();

                parameters.add("sslContextFactory", "org.restlet.ext.ssl.PkixSslContextFactory");

                if(keyStoreFile != null)
                    parameters.add("keystorePath", keyStoreFile);
                else
                {
                    parameters.add("keystorePath", "restServer.jks"); //using default keystore value
                    logger.warn("REST server keystore file has not been specified. Trying default value.");
                }

                if(keyStorePass != null)
                    parameters.add("keystorePassword", keyStorePass);
                else
                {
                    parameters.add("keystorePassword", "pass1234");
                    logger.warn("REST server keystore password has not been specified. Trying default value.");
                }

                if(keyPass != null)
                    parameters.add("keyPassword", keyPass);
                else
                {
                    parameters.add("keyPassword", "pass1234");
                    logger.warn("REST server certificate password has not been specified. Trying default value.");
                }

                parameters.add("keystoreType", "JKS");
                if(clientAuthEnabled)
                {
                    parameters.add("needClientAuthentication", "true");
                    parameters.add("wantClientAuthentication", "false");
                }
                else
                {
                    parameters.add("needClientAuthentication", "false");
                    parameters.add("wantClientAuthentication", "true");
                }
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while trying to start the HTTPS REST server.");
            server = null;
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.fatal(ex.getMessage());
        }

        if(restPort != -1)
        {
            if(server != null)
                component.getServers().add(Protocol.HTTP, restPort);
            else
                server = component.getServers().add(Protocol.HTTP, restPort);
        }
        
        //guard = new ChallengeAuthenticator(null, ChallengeScheme.HTTP_BASIC, "testRealm");
        //mapVerifier = new MapVerifier();
        //mapVerifier.getLocalSecrets().put("admin", "pass1234".toCharArray());
        //guard.setVerifier(mapVerifier);
        //guard.setNext(restServerRootResource.class);
        //component.getDefaultHost().attachDefault(guard);
        t = new Thread(this);
    }
    
    public void run() 
    {
        try
        {
            component.start();
            if(server != null)
                state = true;
            else
                state = false;
        }
        catch (Exception ex)
        {
            state = false;
            //Logger.getLogger(restServer.class.getName()).log(Level.SEVERE, null, ex);
            logger.error("REST server could not be started. Exception caught: " + ex.getMessage() + ".");
            logger.error("REST server could not be started. Try setting correct parameters under Edit -> Settings and try again.");
        }
    }
    
    public boolean getState()
    {
        return state;
    }
    
    public void start()
    {
        t.start();
        if(server != null)
            logger.trace("VEP REST server started.");
        else
            logger.trace("Problem starting VEP REST server.");
    }
    
    public void stop()
    {
        try
        {
            component.stop();
            logger.trace("VEP REST server stopped.");
        }
        catch (Exception ex)
        {
            logger.warn("Exception caught while stopping the Rest server. " + ex.toString());
            //Logger.getLogger(restServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
