/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * InitAdmins.java
 *
 * Created on Feb 10, 2012, 11:49:03 AM
 */
package vep;

import java.sql.ResultSet;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.jdesktop.application.Action;

/**
 *
 * @author piyush
 */
public class InitAdmins extends javax.swing.JDialog {

    /** Creates new form InitAdmins */
    private dbHandler dbhandler;
    private Logger logger;
    public InitAdmins(java.awt.Frame parent, dbHandler handler) 
    {
        super(parent, true); //true blocks the parent input
        initComponents();
        getRootPane().setDefaultButton(createAdminButton);
        logger = Logger.getLogger("VEP.InitAdminsForm");
        logger.trace("Created InitAdmins form.");
        dbhandler = handler;
    }

    @Action public void closeinitAdmins()
    {
        try
        {
            String localAdminPassHash = VEPHelperMethods.makeSHA1Hash(String.valueOf(localAdminPass.getPassword()));
            String localUsername = localAdminUsername.getText().trim();
            String fedUsername = fedAdminUsername.getText().trim();
            boolean status = false;
            if(localUsername.length() > 0)
            {
                status = dbhandler.insert("vepadmin", "('" + localUsername + "', '" + localAdminPassHash + "')");
            }
            if(status)
            {
                if(fedUsername.length() > 0)
                {
                    ResultSet rs = dbhandler.query("select", "*", "user", "where username='" + fedUsername + "'");
                    if(rs.next())
                    {
                        JDialog errorMessage;
                        JOptionPane errMess = new JOptionPane("Local VEP admin account creation was successfull but a\nVEP user account with this fed-admin name already exists!\nFed-user account creation suspended! If you wish you\ncan modify the VEP db manually.", JOptionPane.WARNING_MESSAGE);
                        errorMessage = errMess.createDialog(this.getRootPane(), "Warning");
                        errorMessage.setVisible(true);
                        rs.close();
                        logger.warn("Could not initialize federation admin account. Same name user account exists. Check VEP DB and try creating one by hand [user, ugroup] ...");
                    }
                    else
                    {
                        int uid = 1;
                        rs = dbhandler.query("select", "max(uid)", "user", "");
                        if(rs.next())
                        {
                            uid = rs.getInt(1) + 1;
                            logger.debug("User id to be assigned to the fed-admin account: " + uid + ".");
                        }
                        rs.close();
                        status = dbhandler.insert("user", "('" + fedUsername + "', " + uid + ", '-1', 'NIL', 'NIL', " + -1 + ",  'admin')");
                        if(status)
                        {
                            status = dbhandler.insert("ugroup", "('admin'," + uid + ")");
                            if(!status)
                            {
                                logger.warn("Could not assign group `admin' to the federation admin account. Try assigning one by hand [ugroup] ...");
                            }
                            else
                            {
                                logger.debug("VEP Local Admin and Fed Admin accounts setup complete.");
                            }
                        }
                        else
                        {
                            logger.warn("Could not initialize federation admin account. Try creating one by hand [user, ugroup] ...");
                        }
                    }
                }
                else
                {
                    JDialog errorMessage;
                    JOptionPane errMess = new JOptionPane("Local VEP admin account creation was successfull but Fed-admin\naccount name was empty! Fed-admin account creation suspended!\nTry creating one by hand if needed [user, ugroup] ...", JOptionPane.WARNING_MESSAGE);
                    errorMessage = errMess.createDialog(this.getRootPane(), "Warning");
                    errorMessage.setVisible(true);
                    logger.warn("Fed-admin account name was empty, account creation suspended! Try creating one by hand if needed [user, ugroup] ...");
                }
            }
            else
            {
                logger.warn("Could not initialize local VEP admin account. Try creating one by hand ...");
            }
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught during admin initialization.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        logger.trace("InitAdmins form disposed.");
        this.dispose();
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        adminMsgLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        localAdminUsername = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        localAdminPass = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        fedAdminUsername = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        createAdminButton = new javax.swing.JButton();

        setName("Form"); // NOI18N

        jPanel1.setMaximumSize(new java.awt.Dimension(400, 250));
        jPanel1.setMinimumSize(new java.awt.Dimension(390, 240));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(InitAdmins.class);
        adminMsgLabel.setFont(resourceMap.getFont("adminMsgLabel.font")); // NOI18N
        adminMsgLabel.setText(resourceMap.getString("adminMsgLabel.text")); // NOI18N
        adminMsgLabel.setName("adminMsgLabel"); // NOI18N
        jPanel1.add(adminMsgLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, -1, -1));

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 81, 175, 23));

        localAdminUsername.setFont(resourceMap.getFont("localAdminUsername.font")); // NOI18N
        localAdminUsername.setText(resourceMap.getString("localAdminUsername.text")); // NOI18N
        localAdminUsername.setName("localAdminUsername"); // NOI18N
        jPanel1.add(localAdminUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(199, 81, 177, -1));

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 116, 175, 23));

        localAdminPass.setFont(resourceMap.getFont("localAdminPass.font")); // NOI18N
        localAdminPass.setName("localAdminPass"); // NOI18N
        jPanel1.add(localAdminPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(199, 116, 177, -1));

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 151, 175, 23));

        fedAdminUsername.setFont(resourceMap.getFont("fedAdminUsername.font")); // NOI18N
        fedAdminUsername.setText(resourceMap.getString("fedAdminUsername.text")); // NOI18N
        fedAdminUsername.setName("fedAdminUsername"); // NOI18N
        jPanel1.add(fedAdminUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(199, 151, 177, -1));

        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getActionMap(InitAdmins.class, this);
        createAdminButton.setAction(actionMap.get("closeinitAdmins")); // NOI18N
        createAdminButton.setFont(resourceMap.getFont("createAdminButton.font")); // NOI18N
        createAdminButton.setText(resourceMap.getString("createAdminButton.text")); // NOI18N
        createAdminButton.setName("createAdminButton"); // NOI18N
        jPanel2.add(createAdminButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(306, 12, 72, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 390, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel adminMsgLabel;
    private javax.swing.JButton createAdminButton;
    private javax.swing.JTextField fedAdminUsername;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField localAdminPass;
    private javax.swing.JTextField localAdminUsername;
    // End of variables declaration//GEN-END:variables
}
