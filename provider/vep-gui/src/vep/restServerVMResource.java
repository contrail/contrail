/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerVMResource extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String vepProperties;
    
    public restServerVMResource()
    {
        logger = Logger.getLogger("VEP.restVMResource");
        vepProperties = VEPHelperMethods.getPropertyFile();
//        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, "vep.properties");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("restServerVMResource", dbType);  
    }
    
    @Get("json")
    public Representation toValue()
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        String id = ((String) getRequest().getAttributes().get("id"));
        String username = requestHeaders.getFirstValue("X-Username");
        
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(id, username);
            else if(acceptType.contains("json"))
                response = toJson(id, username);
        }
        else
        {
            //default rendering ...
            response = toHtml(id, username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    private Representation toJson(String id, String username)
    {
        JSONObject response = new JSONObject();
        if(username == null)
        {
            response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
            this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    rs.close();
                    String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList: " + groupList);
                    boolean isAdmin = false;
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    if(id == null)
                    {
                        //present the list of VMs filetered according to this user credentials
                        response.put("title", "List of Virtual Machines");
                        JSONArray vm_name = new JSONArray();
                        JSONArray vm_id = new JSONArray();
                        if(isAdmin)
                        {
                            rs = db.query("select", "*", "vmachine", "");
                            while(rs.next())
                            {
                                vm_name.add(rs.getString("name"));
                                vm_id.add(rs.getInt("id"));
                            }
                            rs.close();
                        }
                        else
                        {
                            rs = db.query("select", "*", "vmachine", "where uid=" + uid);
                            while(rs.next())
                            {
                                vm_name.add(rs.getString("name"));
                                vm_id.add(rs.getInt("id"));
                            }
                            rs.close();
                        }
                        response.put("vm_name", vm_name);
                        response.put("vm_id", vm_id);
                    }
                    else
                    {
                        //show details on this particular VM if the user is allowed to access it
                        response.put("title", "Virtual Machine Detail");
                        boolean isError = false;
                        String errorMsg = "";
                        boolean wasFound = false;
                        int vm_id = -1;
                        String vm_controller = "";
                        int vm_iaas_id = -1;
                        int vm_template_id = -1;
                        String vm_state = "";
                        int vm_host_id = -1;
                        String vm_ipaddress = "";
                        String vm_vnc_ip = "";
                        String vm_vnc_port = "";
                        String vm_name = "";
                        int vm_uid = -1;
                        
                        try
                        {
                            rs = db.query("select", "*", "vmachine", "where id=" + id);
                            if(rs.next())
                            {
                                wasFound = true;
                                vm_id = rs.getInt("id");
                                vm_controller = rs.getString("controller");
                                vm_iaas_id = rs.getInt("onevmid");
                                vm_template_id = rs.getInt("vmid");
                                vm_state = rs.getString("state");
                                vm_host_id = rs.getInt("cid");
                                vm_ipaddress = rs.getString("ipaddress");
                                vm_vnc_ip = rs.getString("vncip");
                                vm_vnc_port = rs.getString("vncport");
                                vm_name = rs.getString("name");
                                vm_uid = rs.getInt("uid");
                            }
                            else
                            {
                                wasFound = false;
                            }
                            rs.close();
                        }
                        catch(Exception ex)
                        {
                            isError = true;
                            errorMsg = ex.getMessage();
                        }
                        
                        if(isAdmin) //display the VM details
                        {
                            if(isError)
                            {
                                response.put("error", "SERVER_ERROR_INTERNAL");
                                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                logger.debug("Error: SQL Exception caught while getting VM details: " + errorMsg);
                            }
                            else
                            {
                                if(wasFound)
                                {
                                    response.put("id",vm_id);
                                    response.put("controller", vm_controller);
                                    response.put("iaas_id", vm_iaas_id);
                                    response.put("template_id", vm_template_id);
                                    response.put("state", vm_state);
                                    response.put("host_id", vm_host_id);
                                    response.put("ip", vm_ipaddress);
                                    response.put("vnc_ip", vm_vnc_ip);
                                    response.put("vnc_port", vm_vnc_port);
                                    response.put("name", vm_name);
                                    response.put("user_id", vm_uid);
                                }
                                else
                                {
                                    response.put("error", "CLIENT_ERROR_NOT_FOUND");
                                    this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                                }
                            }
                        }
                        else
                        {
                            //only display the VM details if vm's uid == uid of the requester
                            if(uid == vm_uid)
                            {
                                if(isError)
                                {
                                    response.put("error", "SERVER_ERROR_INTERNAL");
                                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                    logger.debug("Error: SQL Exception caught while getting VM details: " + errorMsg);
                                }
                                else
                                {
                                    if(wasFound)
                                    {
                                        response.put("id",vm_id);
                                        response.put("controller", vm_controller);
                                        response.put("iaas_id", vm_iaas_id);
                                        response.put("template_id", vm_template_id);
                                        response.put("state", vm_state);
                                        response.put("host_id", vm_host_id);
                                        response.put("ip", vm_ipaddress);
                                        response.put("vnc_ip", vm_vnc_ip);
                                        response.put("vnc_port", vm_vnc_port);
                                        response.put("name", vm_name);
                                        response.put("user_id", vm_uid);
                                    }
                                    else
                                    {
                                        response.put("error", "CLIENT_ERROR_NOT_FOUND");
                                        this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                                    }
                                }
                            }
                            else
                            {
                                response.put("error", "CLIENT_ERROR_UNAUTHORIZED");
                                this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                                logger.warn("Error: Username unauthorized to access VM details.");
                            }
                        }
                    }
                }
                else
                {
                    //no user exists at VEP, error
                    response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    logger.debug("Error: Username not found at VEP.");
                }
            }
            catch(Exception ex)
            {
                response.put("error", "SERVER_ERROR_INTERNAL");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                logger.warn("Caught exception while trying to display the list of VMs. Exception: " + ex.getMessage());
            }
        }
        StringRepresentation value = new StringRepresentation(response.toJSONString(), MediaType.APPLICATION_JSON);
        return value; 
    }
    
    private Representation toHtml(String id, String username)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        if(username == null)
        {
            stringBuilder.append("Unable to determine client's identity ... <i>Access denied</i><br><br>");
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    rs.close();
                    String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList: " + groupList);
                    boolean isAdmin = false;
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    if(id == null)
                    {
                        //present the list of VMs filetered according to this user credentials
                        if(isAdmin)
                        {
                            stringBuilder.append("List of Virtual Machines -<br>");
                            rs = db.query("select", "*", "vmachine", "");
                            stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
                            while(rs.next())
                            {
                                stringBuilder.append("<li>").append(rs.getString("name")).append(" <a href='").
                                    append("/vm/").append(rs.getInt("id")).append("'>").append("/vm/").append(rs.getInt("id")).append("</a>");
                            }
                            rs.close();
                            stringBuilder.append("</ul><br>");
                        }
                        else
                        {
                            stringBuilder.append("List of Virtual Machines accessible to user <i>").append(username).append("</i> -");
                            rs = db.query("select", "*", "vmachine", "where uid=" + uid);
                            stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
                            while(rs.next())
                            {
                                stringBuilder.append("<li>").append(rs.getString("name")).append(" <a href='").
                                    append("/vm/").append(rs.getInt("id")).append("'>").append("/vm/").append(rs.getInt("id")).append("</a>");
                            }
                            rs.close();
                            stringBuilder.append("</ul><br>");
                        }
                    }
                    else
                    {
                        //show details on this particular VM if the user is allowed to access it
                        stringBuilder.append("Details of virtual machine with serial <i>").append(id).append("</i> is shown below:<br><br>");
                        stringBuilder.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
                        stringBuilder.append("<tr>");
                        stringBuilder.append("<td valign='top' style='width:128px;background:white;'><img style='width:128px;' src='https://www.cise.ufl.edu/~pharsh/public/vm.png'>");
                        stringBuilder.append("<td valign='top' align='left' bgcolor='white' width='*'>");
                        boolean isError = false;
                        String errorMsg = "";
                        boolean wasFound = false;
                        int vm_id = -1;
                        String vm_controller = "";
                        int vm_iaas_id = -1;
                        int vm_template_id = -1;
                        String vm_state = "";
                        int vm_host_id = -1;
                        String vm_ipaddress = "";
                        String vm_vnc_ip = "";
                        String vm_vnc_port = "";
                        String vm_name = "";
                        int vm_uid = -1;
                        
                        try
                        {
                            rs = db.query("select", "*", "vmachine", "where id=" + id);
                            if(rs.next())
                            {
                                wasFound = true;
                                vm_id = rs.getInt("id");
                                vm_controller = rs.getString("controller");
                                vm_iaas_id = rs.getInt("onevmid");
                                vm_template_id = rs.getInt("vmid");
                                vm_state = rs.getString("state");
                                vm_host_id = rs.getInt("cid");
                                vm_ipaddress = rs.getString("ipaddress");
                                vm_vnc_ip = rs.getString("vncip");
                                vm_vnc_port = rs.getString("vncport");
                                vm_name = rs.getString("name");
                                vm_uid = rs.getInt("uid");
                            }
                            else
                            {
                                wasFound = false;
                            }
                            rs.close();
                        }
                        catch(Exception ex)
                        {
                            isError = true;
                            errorMsg = ex.getMessage();
                        }
                        
                        if(isAdmin) //display the VM details
                        {
                            if(isError)
                            {
                                stringBuilder.append("<B>Exception!! Details of exception follows ...</B>");
                                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                                stringBuilder.append("SQL Exception Caught: ").append(errorMsg);
                                logger.debug("Error: SQL Exception caught while getting VM details: " + errorMsg);
                                stringBuilder.append("</div>");
                            }
                            else
                            {
                                stringBuilder.append("<div style='background:#CED1D6;font-weight:bold;'>");
                                if(wasFound)
                                {
                                    stringBuilder.append("Virtual machine ID: ").append(vm_id).append("<br>");
                                    stringBuilder.append("IaaS controller: ").append(vm_controller).append("<br>");
                                    stringBuilder.append("IaaS ID: ").append(vm_iaas_id).append("<br>");
                                    stringBuilder.append("Virtual machine template ID: ").append(vm_template_id).append("<br>");
                                    stringBuilder.append("Virtual machine state: ").append(vm_state).append("<br>");
                                    stringBuilder.append("Is deployed on host (-1 means not deployed on any host): ").append(vm_host_id).append("<br>");
                                    stringBuilder.append("Virtual machine IP address: ").append(vm_ipaddress).append("<br>");
                                    stringBuilder.append("Virtual machine graphics IP (vnc): ").append(vm_vnc_ip).append("<br>");
                                    stringBuilder.append("Virtual machine graphics port (vnc): ").append(vm_vnc_port).append("<br>");
                                    stringBuilder.append("Virtual machine name: ").append(vm_name).append("<br>");
                                    stringBuilder.append("Virtual machine owner's ID: ").append(vm_uid);
                                }
                                else
                                {
                                    stringBuilder.append("<i>The requested virtual machine details was not found at this provider.</i>");   
                                }
                                stringBuilder.append("</div>");
                            }
                        }
                        else
                        {
                            //only display the VM details if vm's uid == uid of the requester
                            if(uid == vm_uid)
                            {
                                if(isError)
                                {
                                    stringBuilder.append("<B>Exception!! Details of exception follows ...</B>");
                                    stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                                    stringBuilder.append("SQL Exception Caught: ").append(errorMsg);
                                    logger.debug("Error: SQL Exception caught while getting VM details: " + errorMsg);
                                    stringBuilder.append("</div>");
                                }
                                else
                                {
                                    stringBuilder.append("<div style='background:#CED1D6;font-weight:bold;'>");
                                    if(wasFound)
                                    {
                                        stringBuilder.append("Virtual machine ID: ").append(vm_id).append("<br>");
                                        stringBuilder.append("IaaS controller: ").append(vm_controller).append("<br>");
                                        stringBuilder.append("IaaS ID: ").append(vm_iaas_id).append("<br>");
                                        stringBuilder.append("Virtual machine template ID: ").append(vm_template_id).append("<br>");
                                        stringBuilder.append("Virtual machine state: ").append(vm_state).append("<br>");
                                        stringBuilder.append("Is deployed on host (-1 means not deployed on any host): ").append(vm_host_id).append("<br>");
                                        stringBuilder.append("Virtual machine IP address: ").append(vm_ipaddress).append("<br>");
                                        stringBuilder.append("Virtual machine graphics IP (vnc): ").append(vm_vnc_ip).append("<br>");
                                        stringBuilder.append("Virtual machine graphics port (vnc): ").append(vm_vnc_port).append("<br>");
                                        stringBuilder.append("Virtual machine name: ").append(vm_name).append("<br>");
                                        stringBuilder.append("Virtual machine owner's ID: ").append(vm_uid);
                                    }
                                    else
                                    {
                                        stringBuilder.append("<i>The requested virtual machine details was not found at this provider.</i>");   
                                    }
                                    stringBuilder.append("</div>");
                                }
                            }
                            else
                            {
                                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                                stringBuilder.append("User ").append(username).append(" is not authorized to access this detail.");
                                logger.warn("Error: Username unauthorized to access VM details.");
                                stringBuilder.append("</div>");
                            }
                        }
                        stringBuilder.append("</table><br>");
                    }
                }
                else
                {
                    //no user exists at VEP, error
                    stringBuilder.append("<B>Exception!! Details of exception follows ...</B>");
                    stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                    stringBuilder.append("User ").append(username).append(" not registered at this provider.");
                    logger.debug("Error: Username not found at VEP.");
                    stringBuilder.append("</div>");
                }
            }
            catch(Exception ex)
            {
                stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                stringBuilder.append(ex.getMessage());
                stringBuilder.append("</div>");
                logger.warn("Caught exception while trying to display the list of VMs. Exception: " + ex.getMessage());
            }
        }
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
