/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import java.awt.Color;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 *
 * @author piyush
 */
public class ONE3Worker extends DefaultHandler 
{
    private JComboBox list;
    private JLabel output;
    LinkedList<ONEHost> rowList;
    private ONEHost tempHost;
    private Logger logger;
    private String vepProperties;
    private String[] parameters;
    private ClusterStats runningStat;
    private String tempVal;
    private JPanel panelStatus;
    
    public ONE3Worker(JComboBox ls, JLabel out, String vepprop, String[] params, ClusterStats runningstat, JPanel panelstatus)
    {
        list = ls;
        output = out;
        logger = Logger.getLogger("VEP.ONE3Worker");
        vepProperties = vepprop;
        parameters = params;
        runningStat = runningstat;
        panelStatus = panelstatus;
    }
    
    public void updateParams() //this function will update the host list as well as the metrics of the cluster
    {
        org.opennebula3.client.Client oneClient;
        try
        {
            oneClient  = new org.opennebula3.client.Client(parameters[2] + ":" + parameters[3], "http://" + parameters[0] + ":" + parameters[1] + "/RPC2"); ////////
            panelStatus.setBackground(Color.GREEN);
        }
        catch(Exception ex)
        {
            logger.error("Unable to connect to the XML-RPC endpoint. Check system properties and try again.");
            oneClient = null;
            panelStatus.setBackground(Color.ORANGE);
            panelStatus.setToolTipText("Connection error.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.fatal(ex.getMessage());
        }
        if(oneClient != null)
        {
            org.opennebula3.client.OneResponse val = org.opennebula3.client.host.HostPool.info(oneClient); /////////////
            String response = "";
            if(!val.isError())
            {
                //System.out.println("HostPool Response:\n " + val.getMessage());
                response = val.getMessage();
                panelStatus.setToolTipText("ONE XML-RPC connection is open.");
            }
            else
            {
                logger.warn("OpenNebula 3 XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
                panelStatus.setBackground(Color.ORANGE);
                panelStatus.setToolTipText("HostPool query resulted in error.");
            }
            //parse the XML response and populate the table next
            
            if(response.length() > 1)
            {
                //System.out.println("\n" + response + "\n");
                rowList = new LinkedList<ONEHost>();
                SAXParserFactory spf = SAXParserFactory.newInstance();
                try
                {
                    SAXParser sp = spf.newSAXParser();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(response));
                    sp.parse(is, this);
                }
                catch(SAXException se)
                {
                    if(logger.isDebugEnabled())
                        se.printStackTrace(System.err);
                    else
                        logger.warn(se.getMessage());
                }
                catch(ParserConfigurationException pce)
                {
                    if(logger.isDebugEnabled())
                        pce.printStackTrace(System.err);
                    else
                        logger.warn(pce.getMessage());
                }
                catch(IOException ie)
                {
                    if(logger.isDebugEnabled())
                        ie.printStackTrace(System.err);
                    else
                        logger.warn(ie.getMessage());
                }
            }
            
        }
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
    {
        tempVal = "";
        if(qName.equalsIgnoreCase("HOST"))
        {
            tempHost = new ONEHost(); //blank it for new row
	}
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
	tempVal = new String(ch,start,length);
    }
    
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equalsIgnoreCase("HOST"))
        {
            rowList.add(tempHost);
        }
        else if(qName.equalsIgnoreCase("ID"))
        {
            tempHost.id = tempVal;
        }
        else if(qName.equalsIgnoreCase("NAME"))
        {
            tempHost.name = tempVal;
        }
        else if(qName.equalsIgnoreCase("STATE"))
        {
            tempHost.state = tempVal;
        }
        else if(qName.equalsIgnoreCase("IM_MAD"))
        {
            tempHost.im_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VM_MAD"))
        {
            tempHost.vm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("TM_MAD"))
        {
            tempHost.tm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VN_MAD"))
        {
            tempHost.vn_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("LAST_MON_TIME"))
        {
            tempHost.mon_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("CLUSTER"))
        {
            tempHost.cluster = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_MEM"))
        {
            tempHost.free_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_MEM"))
        {
            tempHost.used_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_CPU"))
        {
            tempHost.free_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_CPU"))
        {
            tempHost.used_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("CPUSPEED"))
        {
            tempHost.cpuspeed = tempVal;
        }
        else if(qName.equalsIgnoreCase("HOST_POOL"))
        {
            list.setEnabled(false);
            String outHTML = "";
            outHTML += "<html><div style='padding:5px;background:white;font-family:Verdana;font-size:10pt;width:370px;'>";
            outHTML += "<table style='background:white;font-family:Verdana;font-size:10pt;border:0px;' cellpadding='2' cellspacing='1'>";
            outHTML += "<tr><th>Id</th><th>Name</th><th>State</th><th>Im_mad</th><th>Vm_mad</th><th>Vn_mad</th><th>Cluster</th><th>Last Mon Time</th></tr>";
            list.removeAllItems();
            list.addItem("not selected");
            //output.setText("ID\tNAME\tSTATE\tIM MAD\tVM MAD\tTM MAD\tCLUSTER\tLAST MON TIME\n");
            //output.append("--\t----\t-----\t------\t------\t------\t-------\t-------------\n");
            
            //resetting the running stat
            runningStat.reset();
            
            while(rowList.size() != 0)
            {
                ONEHost temp = rowList.pop();
                //here also check if in contrail cluster then update the running stats
                if(temp.cluster.equalsIgnoreCase("contrail"))
                {
                    try
                    {
                        runningStat.freeMemory += Double.parseDouble(temp.free_mem);
                        runningStat.usedMemory += Double.parseDouble(temp.used_mem);
                        double cpSpeed = Double.parseDouble(temp.cpuspeed);
                        double maxcpu = Double.parseDouble(temp.free_cpu) + Double.parseDouble(temp.used_cpu);
                        runningStat.freeCPU = (cpSpeed * (maxcpu/100.0)) * (Double.parseDouble(temp.free_cpu)/maxcpu);
                        runningStat.usedCPU = (cpSpeed * (maxcpu/100.0)) * (Double.parseDouble(temp.used_cpu)/maxcpu);
//                        System.out.println("\n" + runningStat.freeMemory + "-" + runningStat.usedMemory + "|" + runningStat.freeCPU + "-" + runningStat.usedCPU + "\n");
                    }
                    catch(Exception ex)
                    {
                        //do nothing, ignore
                    }
                }
                list.addItem("HostID " + temp.id);
                outHTML += "<tr><td style='background:gray;'>" + temp.id + "</td><td style='background:gray;'>" + temp.name + "</td><td style='background:gray;'>" + 
                        temp.state + "</td><td style='background:gray;'>" + temp.im_mad + "</td><td style='background:gray;'>" + temp.vm_mad + 
                        "</td><td style='background:gray;'>" + temp.vn_mad + "</td><td style='background:gray;'>" + temp.cluster + 
                        "</td><td style='background:gray;'>" + temp.mon_time + "</td></tr>";
                //output.append(temp.id + "\t" + temp.name + "\t" + temp.state + "\t" + temp.im_mad + "\t" + temp.vm_mad + "\t" + temp.tm_mad + "\t" + temp.cluster + "\t" + temp.mon_time + "\n");
                logger.trace("Found host: " + temp.id + " " + temp.name + " " + temp.state + " " + temp.im_mad + " " + temp.vm_mad + " " + temp.vn_mad + " " + temp.cluster + " " + temp.mon_time);
            }
            outHTML += "</table></div></html>";
            output.setText(outHTML);
            list.setEnabled(true);
        }
    }
}
