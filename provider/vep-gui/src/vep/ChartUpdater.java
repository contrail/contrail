/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
/**
 *
 * @author piyush
 */
public class ChartUpdater implements Runnable
{
    ClusterStats runningStat;
    int chartType;          //1 = total cluster CPU usage
                            //2 = total cluster memory usage
                            //3 = total disk usage
                            //4 = total network usage chart
    JInternalFrame chartFrame;
    Thread t;
    long refreshPeriodicity; //defaults to 60000 or 1 minute
    boolean runnable;
    Logger logger;
    
    public ChartUpdater(ClusterStats runStat, int type, JInternalFrame cFrame, long time)
    {
        runningStat = runStat;
        chartType = type;
        chartFrame = cFrame;
        refreshPeriodicity = time;
        logger = Logger.getLogger("VEP.ChartUpdater");
    }
    
    public void start()
    {
        t = new Thread(this);
        runnable = true;
        t.start();
    }
    
    public void stop()
    {
        try
        {
            runnable = false;
            t.join(10);
            logger.debug("Chart updater thread has stopped.");
            t.stop();
        }
        catch(Exception ex)
        {
            
        }
    }
    
    public void run() 
    {
        String chartName;
        switch(chartType)
        {
            case 1: chartName = "CPU"; break;
            case 2: chartName = "memory"; break;
            case 3: chartName = "disk"; break;
            case 4: chartName = "network"; break;
            default: chartName = "unknown"; break;
        }
        
        logger.debug("Chart updater for " + chartName + " usage has started.");
        
        try
        {
            while(runnable)
            {
                logger.debug("Updating the " + chartName + " cluster plot values.");
                switch(chartType)
                {
                    case 1:
                        DefaultCategoryDataset result = new DefaultCategoryDataset();
                        result.addValue(runningStat.usedCPU, "In Use", "Contrail");
                        result.addValue(runningStat.freeCPU, "Free", "Contrail");
                        JFreeChart chart = ChartFactory.createStackedBarChart("Cluster CPU Usage", "Total CPU", "MHz", 
                                        result, PlotOrientation.HORIZONTAL, false, true, true);
                        ChartPanel chartPanel = new ChartPanel(chart);
                        chartPanel.setPreferredSize(new java.awt.Dimension(239, 165));
                        chartFrame.setContentPane(chartPanel);
                        break;
                    case 2:
                        DefaultCategoryDataset result1 = new DefaultCategoryDataset();
                        result1.addValue(runningStat.usedMemory, "In Use", "Contrail");
                        result1.addValue(runningStat.freeMemory, "Free", "Contrail");
                        JFreeChart chart1 = ChartFactory.createStackedBarChart("Cluster Memory Usage", "Total Memory", "KiloBytes", 
                                        result1, PlotOrientation.HORIZONTAL, false, true, true);
                        ChartPanel chartPanel1 = new ChartPanel(chart1);
                        chartPanel1.setPreferredSize(new java.awt.Dimension(239, 165));
                        chartFrame.setContentPane(chartPanel1);
                        break;
                    case 3:
                        DefaultCategoryDataset result2 = new DefaultCategoryDataset();
                        result2.addValue(0, "In Use", "Contrail");
                        result2.addValue(0, "Free", "Contrail");
                        JFreeChart chart2 = ChartFactory.createStackedBarChart("Cluster Disk Usage", "Total Size", "KBytes", 
                                        result2, PlotOrientation.HORIZONTAL, false, true, true);
                        ChartPanel chartPanel2 = new ChartPanel(chart2);
                        chartPanel2.setPreferredSize(new java.awt.Dimension(239, 165));
                        chartFrame.setContentPane(chartPanel2);
                        break;
                    case 4:
                        DefaultCategoryDataset result3 = new DefaultCategoryDataset();
                        result3.addValue(0, "Tx", "Contrail");
                        result3.addValue(0, "Rx", "Contrail");
                        JFreeChart chart3 = ChartFactory.createStackedBarChart("Cluster Network Usage", "Total Traffic", "KBytes", 
                                        result3, PlotOrientation.HORIZONTAL, false, true, true);
                        ChartPanel chartPanel3 = new ChartPanel(chart3);
                        chartPanel3.setPreferredSize(new java.awt.Dimension(239, 165));
                        chartFrame.setContentPane(chartPanel3);
                        break;
                    default:
                        JPanel temp = new JPanel();
                        temp.setPreferredSize(new java.awt.Dimension(239,165));
                        chartFrame.setContentPane(temp);
                        break;
                }
                Thread.sleep(refreshPeriodicity);
            }
        }
        catch(Exception ex)
        {
            logger.warn("Error while monitoring chart updates.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        
        logger.debug("Chart updater for " + chartName + " usage has terminated.");
    }
    
}
