/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.bridge.SLF4JBridgeHandler;

/**
 *
 * @author piyush
 */
public class VEPNoGui 
{
    private String vepProperties;
    private String logPropFile;
    private int logLevel;
    private boolean adminAuthenticated;
    private restServer rServer;
    private int restPortValue;
    private int restHTTPSPortValue;
    private settingsForm settings;
    private String oneIP;
    private String oneXmlPort;
    private String oneUser;
    private String onePass;
    private ovfValidator xmlHandle;
    private LinkedList<PeriodicThread> pThreads;
    private ONExmlrpcHandler oneHandle;
    private ONE3xmlrpcHandler one3Handle;
    private VEPHost vepHost;
    private Connection dbHandle;
    private dbHandler dbhandler;
    private VEPComputeNode vepCNode;
    private Logger rootlogger;
    private Logger logger;
    private String dbType;
    private boolean wasDbInitialized;
    private CLIServer cliserver;
    private VEPHelperMethods helperMethods;
    private SSLCertHandler certHandler;
    private ClusterStats runningStat;
    private ChartUpdater chartUpdater;
    private VEPAccessControl vepAccessControl;
    private VEPHostPool hostPool;
    private DirtyFlag dirtyFlag; //used to track configuration file changes
    private boolean noTermOut;
    
    public VEPNoGui(String propfile, String logpropfile, int loglevel, boolean noTerm)
    {
        //////////////initializing the system properties////////////////////////
        vepProperties = propfile;
        logPropFile = logpropfile;
        logLevel = loglevel;
        noTermOut = noTerm;
        helperMethods = new VEPHelperMethods(vepProperties);
        vepAccessControl = new VEPAccessControl(); //just to execute the constructor
        certHandler = new SSLCertHandler();
        
        /////////////////////////Initializing the logger////////////////////////
        java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        rootLogger.removeHandler(handlers[0]);
        SLF4JBridgeHandler.install();
        ////////////////////////////////////////////////////////////////////////
        Properties log4j = new Properties();
        try
        {
            FileOutputStream fos = null;
            String logFile = "";
            String logSize = "";
            try
            {
                logFile = VEPHelperMethods.getProperty("veplog.file", logger, vepProperties);
                logSize = VEPHelperMethods.getProperty("veplog.size", logger, vepProperties);
            }
            catch(Exception ex)
            {
                //unable to read the log file values from system properties file
                //using default
                System.err.println("Unable to read log file settings from system properties file. using default values: vep.log and 1024 KB");
                System.err.println(ex.getMessage());
                logFile = "vep.log";
                logSize = "1024";
            }
            int LogSize = 0;
            boolean logFilePresent = false;
            if(logFile!=null && logFile.trim().length() > 0)
            {
                logFile = logFile.trim();
                if(logSize != null && logSize.trim().length() > 0)
                {
                    try
                    {
                        LogSize = Integer.parseInt(logSize);
                    }
                    catch(NumberFormatException ex)
                    {
                        System.err.println(ex.getMessage());
                        LogSize = 1024;
                    }
                }
                //LogSize is in KB
                logFilePresent = true;
            }
            
            String logPart = "";
            switch(logLevel)
            {
                case 0:
                    logPart = "OFF";
                    break;
                case 1:
                    logPart = "FATAL";
                    break;
                case 2:
                    logPart = "ERROR";
                    break;
                case 3:
                    logPart = "WARN";
                    break;
                case 4:
                    logPart = "INFO";
                    break;
                case 5:
                    logPart = "DEBUG";
                    break;
                default:
                    logPart = "TRACE";
                    break;
            }
            
            if(logFilePresent)
            {
                //log4j.setProperty("log4j.rootLogger", "TRACE, stdout, R");
                if(!noTermOut)
                    log4j.setProperty("log4j.rootLogger", logPart + ", stdout, R");
                else
                    log4j.setProperty("log4j.rootLogger", logPart + ", R");
            }
            else
            {
                //log4j.setProperty("log4j.rootLogger", "TRACE, stdout");
                log4j.setProperty("log4j.rootLogger", logPart + ", stdout");
            }
            log4j.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
            log4j.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
            log4j.setProperty("log4j.appender.stdout.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            if(logFilePresent)
            {
                log4j.setProperty("log4j.appender.R", "org.apache.log4j.RollingFileAppender");
                log4j.setProperty("log4j.appender.R.File", logFile);
                log4j.setProperty("log4j.appender.R.MaxFileSize", Integer.toString(LogSize) + "KB");
                // Keep upto 10 backup file
                log4j.setProperty("log4j.appender.R.MaxBackupIndex", "10");
                log4j.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
                log4j.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            }
            log4j.store((fos = new FileOutputStream("log4j.properties")), "Author: Piyush Harsh");
            fos.close();
        }
        catch(Exception ex)
        {
            System.out.println("Unable to save to log4j properties file: " + ex);
        }
        rootlogger = Logger.getLogger("VEP");
        switch(logLevel)
        {
            case 0:
                rootlogger.setLevel(Level.OFF);
                break;
            case 1:
                rootlogger.setLevel(Level.FATAL);
                break;
            case 2:
                rootlogger.setLevel(Level.ERROR);
                break;
            case 3:
                rootlogger.setLevel(Level.WARN);
                break;
            case 4:
                rootlogger.setLevel(Level.INFO);
                break;
            case 5:
                rootlogger.setLevel(Level.DEBUG);
                break;
            default:
                rootlogger.setLevel(Level.TRACE);
                break;
        }
        //BasicConfigurator.configure();
        PropertyConfigurator.configure("log4j.properties");
        //SLF4JBridgeHandler.install();
        logger = Logger.getLogger("VEP.NoGUI");
        /////////////////////Logger initialization done/////////////////////////
        
        ///////////////////// Thread init///////////////////////////////////////
        pThreads = new LinkedList<PeriodicThread>();
        
        ////////////////initializing the VEP database handle////////////////////
        try 
        {
            logger.trace("Trying to determine which database driver to use.");
            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
            logger.trace("Found database choice: " + dbType);
            if(dbType.contains("sqlite"))
                Class.forName("org.sqlite.JDBC");
            else
                Class.forName("org.gjt.mm.mysql.Driver");
            if(dbType.contains("sqlite"))
                logger.trace("jdbc:sqlite drivers loaded successfully.");
            else
                logger.trace("jdbc:mysql drivers loaded successfully.");
            if(dbType.contains("sqlite"))
            {
                logger.trace("Trying to read database file name.");
                String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger, vepProperties);
                if(dbFile != null && dbFile.length() > 3)
                {
                    try
                    {
                        dbHandle = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        logger.trace("VEP jdbc:sqlite connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("DB connection could not be established for the specified DB file.");
                        sqlEx.printStackTrace(System.out);
                    }
                }
                else
                {
                    logger.warn("Database file is not specified. Check system settings and try again.");
                }
            }
            else
            {
                logger.trace("Trying to open mysql connection.");
                String mysqlUser = VEPHelperMethods.getProperty("mysql.user", logger, vepProperties);
                String mysqlPass = VEPHelperMethods.getProperty("mysql.pass", logger, vepProperties);
                String mysqlIP = VEPHelperMethods.getProperty("mysql.ip", logger, vepProperties);
                String mysqlPort = VEPHelperMethods.getProperty("mysql.port", logger, vepProperties);
                if(mysqlUser != null && mysqlPass != null)
                {
                    try
                    {
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        dbHandle = DriverManager.getConnection("jdbc:mysql://" + mysqlIP + ":" + mysqlPort + "/vepdb?" + "user=" + mysqlUser + "&password=" + mysqlPass);
                        logger.trace("VEP jdbc:mysql connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("Mysql DB connection could not be established for the specified mysql parameters.");
                        sqlEx.printStackTrace(System.out);
                    }
                }
                else
                {
                    logger.warn("Mysql connection parameters not specified. Check system settings and try again.");
                }
            }
        } 
        catch (ClassNotFoundException ex) 
        {
            //Logger.getLogger(VEPView.class.getName()).log(Level.SEVERE, null, ex);
            logger.fatal("Missing mysql or sqlite library ... could not load necessary drivers.");
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        if(dbHandle != null)
        {
            dbHandler.dbHandle = dbHandle;
            dbhandler = new dbHandler("VEP Main", dbType);
            logger.trace("DB handler created successfully using jdbc:sqlite connection object.");
        }
        else
        {
            logger.warn("VEP DB file has not be specified. Check system settings and try again.");
        }
        ///////////////////VEP database handle initialization done//////////////
        
        /////////////////VEP DB Sync Thread Start///////////////////////////////
        if(dbHandle != null)
        {
            hostPool = new VEPHostPool();
            PeriodicThread tempThread = new PeriodicThread(60000, "vepdb-sync", "dosync", null, null, null, null, "", vepProperties, runningStat, dirtyFlag, hostPool);
            tempThread.start();
            logger.trace("VEPdbSync thread (periodic, periodicity 60 seconds) started.");
            pThreads.push(tempThread);
        }
        else
        {
            logger.warn("VEP database handle was not created properly, periodic sync thread will not be started.");
        }
        //////////////////VEP DB Sync Thread done///////////////////////////////
        
        //////////////////VEP CLI Server Start//////////////////////////////////
        int port = 10555;
        String cliport = VEPHelperMethods.getProperty("cli.port", logger, vepProperties);
        try
        {
            port = Integer.parseInt(cliport);
        }
        catch(NumberFormatException nfex)
        {
            logger.warn("Garbled/missing CLI Server port value found ... trying to use default port 10555.");
        }
        if(dbHandle != null)
        {
            cliserver = new CLIServer(port, dbhandler, vepProperties); //change this port with parameter from vep.configuration
            cliserver.startCLIserver();
            logger.trace("Contrail CLI Server has started on port "+ port + ".");            
        }
        else
        {
            logger.warn("VEP database handle was not created properly, CLI server thread will not be started.");
        }
        //////////////////VEP CLI Server done///////////////////////////////////
        
        //////////////////VEP REST Service Start//////////////////////////
        if(rServer == null)
        {
             //Get the properties from the properties file

            String restPortString = VEPHelperMethods.getProperty("rest.restHTTPPort", logger, vepProperties);
            boolean http = true;
            boolean https = true;
            if(restPortString == null || restPortString.equals(""))
            {
                logger.warn("Empty REST HTTP port value specified, using default port 10500.");
                restPortValue = 10500; //using default
            } 
            else if(restPortString.equals("-1"))
            {
                logger.warn("-1 provided for HTTP Port, disabling HTTP Rest.");
                restPortValue = -1;
            }
            else 
            {
                try 
                {
                    restPortValue = Integer.parseInt(restPortString);
                }
                catch(Exception e)
                {
                    logger.error("Property rest.restHTTPPort could not be read, disabling rest Server. Please correct this value or provide none to use the default value");
                    http = false;
                }
            }

            String restHTTPSPortString = VEPHelperMethods.getProperty("rest.restHTTPSPort", logger, vepProperties);
            if(restHTTPSPortString == null || restHTTPSPortString.equals("") || restHTTPSPortString.equals("-1"))
            {
                logger.error("Empty or -1 provided for HTTPSPort, disabling https");
                restHTTPSPortValue = -1;
            }
            else
            {
                try
                {
                    restHTTPSPortValue = Integer.parseInt(restHTTPSPortString);
                }
                catch(Exception e)
                {
                    logger.error("Property rest.restHTTPSPort could not be read, disabling rest Server. Please correct this value or provide none to disable HTTPS");
                    https = false;
                }
            }

            String keyStorePath = VEPHelperMethods.getProperty("rest.keystore", logger, vepProperties);
            String keyStorePass = VEPHelperMethods.getProperty("rest.keystorepass", logger, vepProperties);
            String keyPass = VEPHelperMethods.getProperty("rest.keypass", logger, vepProperties);

            if(http && https)
            {
                boolean clientAuthSelected = Boolean.parseBoolean(VEPHelperMethods.getProperty("rest.clientAuthSelected", logger, vepProperties));

                if(clientAuthSelected)
                    rServer = new restServer(restPortValue, restHTTPSPortValue, true, keyStorePath, keyStorePass, keyPass);
                else
                    rServer = new restServer(restPortValue, restHTTPSPortValue, false, keyStorePath, keyStorePass, keyPass);
                logger.trace("Starting REST server.");
                rServer.start();
                if(restPortValue != -1)
                {
                    logger.trace("Started REST server at HTTP port " + restPortValue + " HTTPS port " + restHTTPSPortValue + ".");
                }
                else
                {
                    logger.trace("Started REST server at HTTPS port " + restHTTPSPortValue + "."); 
                }
            }
            else
            {
                logger.warn("REST Server could not be started because of incorrect values for HTTP or HTTPS port");
            }
            //introducing a delay of 500 msec
            try
            {
                Thread.sleep(500);
            }
            catch(Exception ex)
            {
                //ignore this exception
            }

        }
        /////////////////VEP REST Service started//////////////////////////
        logger.trace("Contrail VEP application has started in terminal mode.");
    }
    
    private void testDb()
    {
        try
        {   
            logger.trace("Running VEP database sanity checks (primitive checks) now.");
            if(dbHandle != null)
            {
                DatabaseMetaData dbm = dbHandle.getMetaData();
                String[] types = {"TABLE"};
                ResultSet rs = dbm.getTables(null,null,"%",types);
                int count=0;
                while (rs.next())
                {
                    count++;
                    String table = rs.getString("TABLE_NAME");
                }
                if(count < 20)
                {
                    logger.warn("VEP database file failed sanity checks. Asking user for reinitialization permission.");
                    System.out.print("Do you want to reinitialize the database? (yes/no) : ");
                    byte[] response = new byte[10];
                    System.in.read(response);
                    String answer = new String(response);
                    if(answer.contains("yes"))
                    {
                        logger.trace("Initiating database reinitialization process.");
                        //call initialize database function here
                        initializeDb();
                    }
                    else
                    {
                        //if(dbHandle != null) dbHandle.close();
                        //dbHandle = null;
                        logger.warn("VEP database reinitialization permit denied. VEP database actions will be disabled.");
                    }
                }
                else
                {
                    logger.debug("VEP database sanity checks (primitive checks) were completed successfully.");
                }
            }
            else
            {
                logger.warn("VEP database handler object is not valid. Terminating database sanity checks. Check system properties and try again.");
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught during database sanity checks.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
    private void initializeDb()
    {
        if(dbHandle != null)
        {
            try
            {
                logger.trace("Initiating database (re)initialization process, deleteing any existing schema.");
                Statement stat = dbHandle.createStatement();
                stat.executeUpdate("drop table if exists user;");
                stat.executeUpdate("drop table if exists ugroup;"); 
                stat.executeUpdate("drop table if exists vorg;");
                stat.executeUpdate("drop table if exists datacenter;");
                stat.executeUpdate("drop table if exists cluster;");
                stat.executeUpdate("drop table if exists rack;");
                stat.executeUpdate("drop table if exists computenode;");
                stat.executeUpdate("drop table if exists vmachinetemplate;");
                stat.executeUpdate("drop table if exists vmachine;");
                stat.executeUpdate("drop table if exists vnet;");
                stat.executeUpdate("drop table if exists storage;");
                stat.executeUpdate("drop table if exists clmanager;");
                stat.executeUpdate("drop table if exists ipaddress;");
                stat.executeUpdate("drop table if exists history;");
                stat.executeUpdate("drop table if exists ovf;");
                stat.executeUpdate("drop table if exists ceeobj;");
                stat.executeUpdate("drop table if exists diskimage;");
                stat.executeUpdate("drop table if exists deployment;");
                stat.executeUpdate("drop table if exists ovfvmmap;");
                stat.executeUpdate("drop table if exists vepadmin;");
                
                logger.trace("Creating VEP DB schema again.");
                
                String createTable = "create table vepadmin "
                        + "(username varchar(40) NOT NULL, " 
                        + "password varchar(45) NOT NULL, "
                        + "PRIMARY KEY (username))";
                stat.executeUpdate(createTable);
                logger.trace("VEP table vepadmin created successfully.");
                
                createTable = "create table user "
                        + "(username varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "vid varchar(20) NOT NULL, "
                        + "oneuser varchar(45) NOT NULL, "
                        + "onepass varchar(45) NOT NULL, "
                        + "oneid integer NOT NULL, "
                        + "role varchar(20), PRIMARY KEY (username, vid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table user created successfully.");
                
                createTable = "create table ugroup "
                        + "(gname varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ugroup created successfully.");
                
                createTable = "create table vorg "
                        + "(vid varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "name varchar(40), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (vid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vorg created successfully.");
                
                //for datacenter entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table datacenter "
                        + "(did integer NOT NULL, " 
                        + "loc varchar(2) NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (did))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table datacenter created successfully.");
                
                //for cluster entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table cluster "
                        + "(clid integer NOT NULL, " 
                        + "nicid varchar(3), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "name varchar(40), "
                        + "PRIMARY KEY (clid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table cluster created successfully.");
                
                //for rack entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table rack "
                        + "(rid integer NOT NULL, " 
                        + "clid integer NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (rid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table rack created successfully.");
                
                //for computenode entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table computenode "
                        + "(cid integer NOT NULL, " 
                        + "mem integer(10) NOT NULL, "
                        + "cpu integer(5) NOT NULL, "
                        + "cpufreq integer(10), "
                        + "cpuarch varchar(40), "
                        + "rid integer, "
                        + "virt varchar(20) NOT NULL, "
                        + "hostname varchar(256) NOT NULL, "
                        + "clmgrtype varchar(256) NOT NULL, "
                        + "vid varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table computenode created successfully.");
                
                createTable = "create table vmachinetemplate "
                        + "(vmid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "appname varchar(128), "
                        + "nid varchar(40), "
                        + "ip varchar(15), "
                        + "state varchar(2), "
                        + "ovfsno integer, "
                        + "ovfid varchar(40), "
                        + "ceeid integer, "
                        + "descp varchar(2048), " //changed desc to descp
                        + "PRIMARY KEY (vmid, nid, ip, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachinetemplate created successfully.");
                
                createTable = "create table vmachine "
                        + "(id integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "controller varchar(45), "
                        + "onevmid integer, "
                        + "vmid integer, "
                        + "name varchar(128), "
                        + "state varchar(2), "
                        + "cid integer NOT NULL, "
                        + "ipaddress varchar(128) NOT NULL, "
                        + "vncport varchar(10), "
                        + "vncip varchar(15), "
                        + "fqdn varchar(128), "
                        + "PRIMARY KEY (id))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachine created successfully.");
                
                //for vnet entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table vnet "
                        + "(nid varchar(40) NOT NULL, " 
                        + "iprange varchar(256) NOT NULL, " //changed range to iprange
                        + "ispublic varchar(1) NOT NULL, "
                        + "vid varchar(40), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (nid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vnet created successfully.");
                
                //for storage entry, assume default access rights, admins can create, modify, others can only access
                createTable = "create table storage "
                        + "(sid varchar(40) NOT NULL, " 
                        + "type varchar(3) NOT NULL, "
                        + "vid varchar(40), "
                        + "size integer(10), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (sid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table storage created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table clmanager "
                        + "(cid integer NOT NULL, " 
                        + "hostid varchar(40) NOT NULL, "
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table clmanager created successfully.");
                
                createTable = "create table ipaddress "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "nid varchar(40) NOT NULL, "
                        + "address varchar(15) NOT NULL, "
                        + "isassigned varchar(1) NOT NULL, "
                        + "vmid integer)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ipaddress created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table history "
                        + "(time varchar(40) NOT NULL, "
                        + "username varchar(40), "
                        + "code varchar(4), "
                        + "sqlstring varchar(1024), "
                        + "descp varchar(1024))"; //changed desc to descp
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table history created successfully.");
                
                createTable = "create table ovf "
                        + "(ovfname varchar(40) NOT NULL, " 
                        + "sno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "state varchar(2), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "descp text(50240), " //changed desc to descp
                        + "PRIMARY KEY (ovfname, uid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovf created successfully.");
                
                createTable = "create table ceeobj "
                        + "(ceeid integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "slaobj BLOB, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (ceeid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ceeobj created successfully.");
                
                createTable = "create table diskimage "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "gafslink varchar(1024), "
                        + "name varchar(1024), "
                        + "oneimgname varchar(128), "
                        + "oneimgid integer, "
                        + "vmid integer, "
                        + "state varchar(2), "
                        + "oneimgdescp varchar(1024), "
                        + "localpath varchar(1024))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table diskimage created successfully.");
                
                createTable = "create table deployment "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "ovfsno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "content varchar(10240))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table deployment created successfully.");
                
                createTable = "create table ovfvmmap "
                        + "(ovfsno integer NOT NULL, " 
                        + "ovfid varchar(40) NOT NULL, "
                        + "vmid integer, PRIMARY KEY (ovfsno, ovfid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovfvmmap created successfully.");
                
                logger.trace("Database (re)initialization process successful ...");
                wasDbInitialized = true;
            }
            catch(SQLException ex)
            {
                logger.error("Database (re)initialization process failed.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        else
        {
            logger.trace("Database handler is not valid, initialization process was stopped. Check system parameters and restart the software.");
        }
    }
}
