/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * rackManagement.java
 *
 * Created on Sep 2, 2011, 11:05:53 AM
 */
package vep;

import java.awt.Dialog;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class rackManagement extends javax.swing.JPanel {
    private dbHandler handle;
    private JTextArea logArea;
    private boolean systemOut;
    private Logger logger;

    /** Creates new form rackManagement */
    public rackManagement() {
        initComponents();
        systemOut = false;
        logger = Logger.getLogger("VEP.rackMgmt");
    }
    
    public rackManagement(dbHandler handler, JTextArea log) {
        initComponents();
        systemOut = false;
        logger = Logger.getLogger("VEP.rackMgmt");
        if(systemOut)
        System.out.println("Inside customized constructor: rackManagement");
        logger.trace("Constructing DB interface panel for rack management.");
        handle = handler;
        logArea = log;
        try
        {
            //populate the choice list with list of clusters
            ResultSet rs = handle.query("SELECT", "rid, name", "rack", "");
            rackList.removeAllItems();
            int count = 0;
            while(rs.next())
            {
                if(count == 0)
                    rackList.addItem("No rack selected");
                String rid = Integer.toString(rs.getInt("rid"));
                String name = rs.getString("name");
                if(systemOut)
                System.out.println("Found rack: " + rid + ", " + name);
                logger.trace("Composing racks list: found rack " + rid + ", " + name);
                rackList.addItem(rid + ", " + name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No rack was found.");
                logger.trace("Composing racks list: no racks found.");
                rackList.addItem("No rack found");
            }
            //now populating the list of clusters
            rs = handle.query("SELECT", "clid, name", "cluster", "");
            clusterSelector.removeAllItems();
            count = 0;
            while(rs.next())
            {
                if(count == 0)
                    clusterSelector.addItem("No cluster selected");
                String clid = Integer.toString(rs.getInt("clid"));
                String name = rs.getString("name");
                if(systemOut)
                System.out.println("Found cluster: " + clid + ", " + name);
                logger.trace("Composing clusters list: found cluster " + clid + ", " + name);
                clusterSelector.addItem(clid + ", " + name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No cluster was found.");
                logger.trace("Composing clusters list: no cluster was found.");
                clusterSelector.addItem("No cluster found");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        rackList = new javax.swing.JComboBox();
        refreshButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        rackName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        clusterSelector = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        rackDesc = new javax.swing.JTextArea();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(rackManagement.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setName("Form"); // NOI18N
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        rackList.setFont(resourceMap.getFont("rackList.font")); // NOI18N
        rackList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No rack selected" }));
        rackList.setName("rackList"); // NOI18N
        rackList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rackListActionPerformed(evt);
            }
        });
        add(rackList, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 140, 20));

        refreshButton.setFont(resourceMap.getFont("refreshButton.font")); // NOI18N
        refreshButton.setText(resourceMap.getString("refreshButton.text")); // NOI18N
        refreshButton.setName("refreshButton"); // NOI18N
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        add(refreshButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 90, -1));

        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 70, 30));

        rackName.setFont(resourceMap.getFont("rackName.font")); // NOI18N
        rackName.setText(resourceMap.getString("rackName.text")); // NOI18N
        rackName.setName("rackName"); // NOI18N
        jPanel1.add(rackName, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 20, 200, -1));

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 55, 80, 30));

        clusterSelector.setFont(resourceMap.getFont("clusterSelector.font")); // NOI18N
        clusterSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No cluster selected" }));
        clusterSelector.setName("clusterSelector"); // NOI18N
        jPanel1.add(clusterSelector, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 200, 20));

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 110, -1));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        rackDesc.setColumns(20);
        rackDesc.setFont(resourceMap.getFont("rackDesc.font")); // NOI18N
        rackDesc.setLineWrap(true);
        rackDesc.setRows(5);
        rackDesc.setWrapStyleWord(true);
        rackDesc.setName("rackDesc"); // NOI18N
        jScrollPane1.setViewportView(rackDesc);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 300, 100));

        updateButton.setFont(resourceMap.getFont("updateButton.font")); // NOI18N
        updateButton.setText(resourceMap.getString("updateButton.text")); // NOI18N
        updateButton.setName("updateButton"); // NOI18N
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        jPanel1.add(updateButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 90, -1));

        deleteButton.setFont(resourceMap.getFont("deleteButton.font")); // NOI18N
        deleteButton.setText(resourceMap.getString("deleteButton.text")); // NOI18N
        deleteButton.setName("deleteButton"); // NOI18N
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel1.add(deleteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 220, 120, -1));

        addButton.setFont(resourceMap.getFont("addButton.font")); // NOI18N
        addButton.setText(resourceMap.getString("addButton.text")); // NOI18N
        addButton.setName("addButton"); // NOI18N
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jPanel1.add(addButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 220, 70, -1));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 340, 260));
    }// </editor-fold>//GEN-END:initComponents

private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
// TODO add your handling code here:
    try
    {
        //populate the choice list with list of clusters
        ResultSet rs = handle.query("SELECT", "rid, name", "rack", "");
        rackList.removeAllItems();
        int count = 0;
        while(rs.next())
        {
            if(count == 0)
                rackList.addItem("No rack selected");
            String rid = Integer.toString(rs.getInt("rid"));
            String name = rs.getString("name");
            if(systemOut)
            System.out.println("Found rack: " + rid + ", " + name);
            logger.trace("Refreshing racks list: found rack " + rid + ", " + name);
            rackList.addItem(rid + ", " + name);
            count++;
        }
        if(count == 0)
        {
            if(systemOut)
            System.out.println("No rack was found.");
            logger.trace("Refreshing racks list: no rack was found.");
            rackList.addItem("No rack found");
        }
        //now populating the list of clusters
        rs = handle.query("SELECT", "clid, name", "cluster", "");
        clusterSelector.removeAllItems();
        count = 0;
        while(rs.next())
        {
            if(count == 0)
                clusterSelector.addItem("No cluster selected");
            String clid = Integer.toString(rs.getInt("clid"));
            String name = rs.getString("name");
            if(systemOut)
            System.out.println("Found cluster: " + clid + ", " + name);
            logger.trace("Refreshing clusters list: found cluster " + clid + ", " + name);
            clusterSelector.addItem(clid + ", " + name);
            count++;
        }
        if(count == 0)
        {
            if(systemOut)
            System.out.println("No cluster was found.");
            logger.trace("Refreshing clusters list: no cluster was found.");
            clusterSelector.addItem("No cluster found");
        }
    }
    catch(Exception ex)
    {
        ex.printStackTrace(System.out);
    }
}//GEN-LAST:event_refreshButtonActionPerformed

private void rackListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rackListActionPerformed
// TODO add your handling code here:
    Object selectedItem = rackList.getSelectedItem();
    String rid = null;
    if(selectedItem != null)
        rid = selectedItem.toString().split(",")[0];
    if(rid != null && rid.contains("No rack"))
    {
        //do nothing
        rackDesc.setText("");
        rackName.setText("");
        clusterSelector.setSelectedIndex(0);
        deleteButton.setEnabled(false);
        updateButton.setEnabled(false);
        addButton.setEnabled(true);
    }
    else if(rid != null && !rid.contains("No rack"))
    {
        //populate the fields with selected user data
        try
        {
            ResultSet rs = handle.query("select", "*", "rack", "where rid=" + rid);
            if(rs.next())
            {
                rackDesc.setText(rs.getString("descp"));
                rackName.setText(rs.getString("name"));
                
                String clid = Integer.toString(rs.getInt("clid"));
                int location = 0;
                Object obj;
                while((obj = clusterSelector.getItemAt(location))!=null)
                {
                    String temp = obj.toString();
                    String clusterId = temp.split(",")[0];
                    if(clusterId.trim().equalsIgnoreCase(clid)) break;
                    location++;
                }
                if(obj != null)
                {
                    clusterSelector.setSelectedIndex(location);
                }
                else
                {
                    //no cluster assigned, clid was -1
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
        deleteButton.setEnabled(true);
        updateButton.setEnabled(true);
        addButton.setEnabled(false);
    }
}//GEN-LAST:event_rackListActionPerformed

private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
// TODO add your handling code here:
    String name = rackName.getText().trim();
    Object obj = clusterSelector.getSelectedItem();
    String clid = "-1";
    String temp = "";
    if(obj != null)
        temp = obj.toString();
    if(temp.contains("No cluster")) clid = "-1";
    else
        clid = temp.split(",")[0].trim();
    String desc = rackDesc.getText().trim();
    boolean proceed = true;
    if(clid.equalsIgnoreCase("-1")) proceed = false;
    if(proceed)
        proceed = dbHandler.validateSingleWord(name);
    if(proceed)
        proceed = dbHandler.validateSentence(desc);
    if(!proceed)
    {
        Dialog errorMessage;
        JOptionPane errMess = new JOptionPane("Some of the data you entered is not valid!\nCheck for presence of ' in Description field.\nCheck for ', \", SPACE in name.\nIf no cluster was selected, you must select one,\nif no cluster exists, you must create one.\nFix errors and try again.", JOptionPane.ERROR_MESSAGE);
        errorMessage = errMess.createDialog(this.getRootPane(), "Invalid input data");
        logger.warn("Invalid character used during add rack request. Correct and try again.");
        errorMessage.setVisible(true);
        return;
    }
    else
    {
        ResultSet rs = handle.query("select", "max(rid)", "rack", "");
        int rid = 0;
        try
        {
            if(rs.next())
                rid = rs.getInt(1) + 1;
            if(systemOut)
            System.out.println("rack ID to be assigned: " + rid);
            logger.debug("New rack id to be assigned: " + rid);
            boolean status = handle.insert("rack", "(" + rid + ", " + clid + ", '" + name + "', '" + desc + "')");
            refreshButtonActionPerformed(null);
            if(status)
            {
                logArea.append("Rack was added successfully with rack ID: " + rid + "\n");
                logger.debug("Rack was added successfully with rack ID: " + rid);
            }
            else
            {
                logger.error("Rack " + rid + " addition failed. Error during insert operation into racks table.");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
    }
}//GEN-LAST:event_addButtonActionPerformed

private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
// TODO add your handling code here:
    Object selectedItem = rackList.getSelectedItem();
    String rid = null;
    if(selectedItem != null)
        rid = selectedItem.toString().split(",")[0];
    
    String name = rackName.getText().trim();
    Object obj = clusterSelector.getSelectedItem();
    String clid = "-1";
    String temp = "";
    if(obj != null)
        temp = obj.toString();
    if(temp.contains("No cluster")) clid = "-1";
    else
        clid = temp.split(",")[0].trim();
    String desc = rackDesc.getText().trim();
    boolean proceed = true;
    if(clid.equalsIgnoreCase("-1")) proceed = false;
    if(proceed)
        proceed = dbHandler.validateSingleWord(name);
    if(proceed)
        proceed = dbHandler.validateSentence(desc);
    if(!proceed)
    {
        Dialog errorMessage;
        JOptionPane errMess = new JOptionPane("Some of the data you entered is not valid!\nCheck for presence of ' in Description field.\nCheck for ', \", SPACE in name.\nIf no cluster was selected, you must select one,\nif no cluster exists, you must create one.\nFix errors and try again.", JOptionPane.ERROR_MESSAGE);
        errorMessage = errMess.createDialog(this.getRootPane(), "Invalid input data");
        logger.warn("Invalid character used during update rack request. Correct and try again.");
        errorMessage.setVisible(true);
        return;
    }
    else
    {
        String updateList = "";
        updateList = "name='" + name + "'";
        if(!desc.isEmpty()) updateList += ",descp='" + desc + "'";
        updateList += ",clid=" + clid;
        if(rid != null)
        {
            boolean status = handle.update("rack", updateList, "where rid=" + rid);
            if(status)
            {
                logArea.append("Rack " + rid + " was updated successfully.\n");
                logger.debug("Rack " + rid + " was updated successfully.");
            }
            else
            {
                logger.error("Rack " + rid + " update operation failed. Error during update operation into racks table.");
            }
        }
    }
}//GEN-LAST:event_updateButtonActionPerformed

private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
// TODO add your handling code here:
    Object selectedItem = rackList.getSelectedItem();
    String rid = null;
    if(selectedItem != null)
        rid = selectedItem.toString().split(",")[0]; 
    if(!rid.contains("No cluster"))
    {
        Object[] options = {"Delete", "Don't Delete"};
        int n = JOptionPane.showOptionDialog(this.getRootPane(), "Do you wish to delete rack " + rid + "'s records?", "Rack Delete Operation",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[1]); //default button title
        if(n == 0)
        {
            //here delete the datacenter
            if(rid != null)
            {
                boolean status = handle.delete("rack", "where rid=" + rid);
                refreshButtonActionPerformed(null);
                if(status)
                {
                    logArea.append("Rack " + rid + " was deleted successfully.\n");
                    //now update the compute node table , set rid to -1 for matching rid
                    status = handle.update("computenode", "rid=-1", "where rid=" + rid);
                    if(status)
                    {
                        logArea.append("Computenode table was updated successfully.\n");
                        logger.debug("Rack " + rid + " was deleted. Computenodes updated to reflect changes.");
                    }
                    else
                    {
                        logger.error("Rack " + rid + " was deleted partially. Error while updating the computenode table to reflect changes.");
                    }
                }
                else
                {
                    logger.error("Rack " + rid + " could not be deleted. Error while updating the racks table.");
                }
            }
        }
    }
}//GEN-LAST:event_deleteButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JComboBox clusterSelector;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea rackDesc;
    private javax.swing.JComboBox rackList;
    private javax.swing.JTextField rackName;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
