/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.sql.ResultSet;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class VEPDBSyncCleanup
{
    Thread t;
    Logger logger;
    dbHandler db;
    ResultSet rs;
    ResultSet rs1;
    ResultSet rs2;
    String oneIP;
    String oneXmlPort;
    String oneUser;
    String onePass;
    ONExmlrpcHandler oneHandle;
    ONE3xmlrpcHandler one3Handle;
    private String vepProperties;
    
    public VEPDBSyncCleanup(String vepProp)
    {
        logger = Logger.getLogger("VEP.dbCleanup");
        vepProperties = vepProp;
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("VEPDBSyncObject", dbType);
        oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
        onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
        String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        oneHandle = null;
        one3Handle = null;
        if(oneVersion.startsWith("2.2"))
            oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPDBSyncObject");
        else if(oneVersion.startsWith("3.4"))
            one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPDBSyncObject");
    }
    
    public void updateParams()
    {
        logger.debug("Realtime parameters update initiated.");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("VEPDBSyncObject", dbType);
        oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
        onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
        String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        oneHandle = null;
        one3Handle = null;
        if(oneVersion.startsWith("2.2"))
            oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPDBSyncObject");
        else if(oneVersion.startsWith("3.4"))
            one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPDBSyncObject");
        logger.debug("Realtime parameters update finished.");
    }

    public void doSyncCleanup() 
    {
        logger.debug("Performing VEP-DB Sync and Cleanup operation now ...");
        try
        {
            rs = db.query("select", "id, onevmid, controller, state, vmid", "vmachine", "");
            int counter = 0;
            while(rs.next())
            {
                counter++;

                int oneid = rs.getInt("onevmid");
                int id = rs.getInt("id");
                String controller = rs.getString("controller");
                String currentState = rs.getString("state");
                int vmid = rs.getInt("vmid");
                ONEVm temp = null;

                String vmState = "";
                int hostId = -1;
                String ipAddress = "";
                int cid = -1;

                if(oneHandle != null)
                    temp = oneHandle.getVmInfo(oneid);
                else if(one3Handle != null)
                    temp = one3Handle.getVmInfo(oneid);

                if(temp != null)
                {
                    if(temp.state.contains("6"))
                    {
                        vmState = "FN";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;
                        db.update("vmachine", "state='FN', cid=-1", "where id=" + id);
                    }
                    else if(oneHandle != null && temp.state.contains("5") && temp.lcm_state.equalsIgnoreCase("0"))
                    {
                        vmState = "SP";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='SP', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                    else if(one3Handle != null && temp.state.contains("3") && temp.lcm_state.equalsIgnoreCase("6"))
                    {
                        vmState = "SP";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='SP', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                    else if(temp.state.contains("3") && temp.lcm_state.equalsIgnoreCase("3"))
                    {
                        vmState = "RN";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='RN', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                        //As a VM changes to running state, issue a startaccess to the pdp (if in use)
                        /*if(VEPHelperMethods.usePDP())
                        {
                             //Check to verify it's a status change and previous state was not RN
                            if(currentState != null)
                            {
                                if(!currentState.equalsIgnoreCase("RN"))
                                {
                                    //retrieve ovfsno for this VM
                                    ResultSet rsPDP = db.query("select", "ovfsno", "vmachinetemplate", "WHERE vmid=" + vmid);
                                    
                                    if(rsPDP.next());
                                    {
                                        int ovfsno = rsPDP.getInt("ovfsno");
                                        VEPHelperMethods.startaccessPDP(ovfsno);
                                    }
                                }
                            }
                        }*/
                    }
                    else if(temp.state.contains("7") && temp.lcm_state.equalsIgnoreCase("0"))
                    {
                        vmState = "FL"; //failed
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='FL', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                    else if(temp.state.contains("3") && temp.lcm_state.equalsIgnoreCase("1"))
                    {
                        vmState = "PR";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='PR', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                    else if(temp.state.contains("3") && temp.lcm_state.equalsIgnoreCase("2"))
                    {
                        vmState = "BT";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='BT', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                    else if(temp.state.contains("3") && temp.lcm_state.equalsIgnoreCase("12"))
                    {
                        vmState = "SH";
                        hostId = temp.host_id;
                        ipAddress = temp.ip;

                        rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);

                        int counter1=0;

                        while(rs1.next())
                        {
                            counter1++;
                            boolean found = false;
                            int cId = rs1.getInt("cid");
                            //now cross-ref with computenode table
                            rs2 = db.query("select", "clmgrtype", "computenode", "where cid=" + cId);
                            if(rs2.next())
                            {
                                if(rs2.getString("clmgrtype").equalsIgnoreCase(controller)) found = true;
                            }
                            rs2.close();

                            if(found)
                            {
                                cid = cId;
                                break;
                            }
                            else
                            {
                                rs1 = db.query("select", "cid", "clmanager", "WHERE hostid=" + hostId);
                                for(int i=0;i<counter1; i++) rs1.next(); //hack to overcome rs1 being closed after updates.
                            }
                        }
                        rs1.close();
                        db.update("vmachine", "state='SH', cid=" + cid + ", ipaddress='" + ipAddress + "'", "where id=" + id);
                    }
                }
                else
                {
                    vmState = "UN";
                    db.update("vmachine", "state='UN'", "where id=" + id);
                }
                rs = db.query("select", "id, onevmid, controller, state, vmid", "vmachine", "");
                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
            }
            rs.close();
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught in VEPdbcleanup method.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
}
