/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * preferenceForm.java
 *
 * Created on Jul 26, 2011, 3:18:29 PM
 */
package vep;

import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class preferenceForm extends javax.swing.JFrame {

    /** Creates new form preferenceForm */
    public preferenceForm() {
        initComponents();
        logger = Logger.getLogger("VEP.PreferenceForm");
        logger.trace("Generated Contrail Preferences form.");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        icon = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        contrailUsername = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        contrailKey = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        emailChoice = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        savePref = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(preferenceForm.class);
        setTitle(resourceMap.getString("Form.title")); // NOI18N
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setName("Form"); // NOI18N
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        icon.setIcon(resourceMap.getIcon("icon.icon")); // NOI18N
        icon.setText(resourceMap.getString("icon.text")); // NOI18N
        icon.setName("icon"); // NOI18N
        getContentPane().add(icon, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 280, -1, -1));

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 17, -1, -1));

        contrailUsername.setFont(resourceMap.getFont("contrailUsername.font")); // NOI18N
        contrailUsername.setText(resourceMap.getString("contrailUsername.text")); // NOI18N
        contrailUsername.setName("contrailUsername"); // NOI18N
        getContentPane().add(contrailUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, 210, -1));

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        contrailKey.setColumns(20);
        contrailKey.setFont(resourceMap.getFont("contrailKey.font")); // NOI18N
        contrailKey.setRows(5);
        contrailKey.setName("contrailKey"); // NOI18N
        jScrollPane1.setViewportView(contrailKey);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 420, 140));

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, -1));

        emailChoice.setFont(resourceMap.getFont("emailChoice.font")); // NOI18N
        emailChoice.setText(resourceMap.getString("emailChoice.text")); // NOI18N
        emailChoice.setName("emailChoice"); // NOI18N
        emailChoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailChoiceActionPerformed(evt);
            }
        });
        getContentPane().add(emailChoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 212, -1, 30));

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, -1, 30));

        email.setFont(resourceMap.getFont("email.font")); // NOI18N
        email.setText(resourceMap.getString("email.text")); // NOI18N
        email.setEnabled(false);
        email.setName("email"); // NOI18N
        getContentPane().add(email, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 250, 280, -1));

        savePref.setFont(resourceMap.getFont("savePref.font")); // NOI18N
        savePref.setText(resourceMap.getString("savePref.text")); // NOI18N
        savePref.setName("savePref"); // NOI18N
        savePref.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePrefActionPerformed(evt);
            }
        });
        getContentPane().add(savePref, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void emailChoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailChoiceActionPerformed
        // TODO add your handling code here:
        if(emailChoice.isSelected()) email.setEnabled(true);
        else
            email.setEnabled(false);
    }//GEN-LAST:event_emailChoiceActionPerformed

    private void savePrefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savePrefActionPerformed
        // TODO add your handling code here:
        this.dispose();
        logger.trace("Disposed Contrail Preferences form.");
    }//GEN-LAST:event_savePrefActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new preferenceForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea contrailKey;
    private javax.swing.JTextField contrailUsername;
    private javax.swing.JTextField email;
    private javax.swing.JCheckBox emailChoice;
    private javax.swing.JLabel icon;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton savePref;
    // End of variables declaration//GEN-END:variables
    Logger logger;
}
