/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VEPComputeNodeForm.java
 *
 * Created on Sep 2, 2011, 5:36:11 PM
 */
package vep;

import java.awt.Dialog;
import java.sql.ResultSet;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import org.apache.log4j.Logger;
import org.jdesktop.application.Action;

/**
 *
 * @author piyush
 */
public class VEPComputeNodeForm extends javax.swing.JDialog {
    private VEPComputeNode vepCNode;
    private dbHandler handle;
    private JTextArea logArea;
    private boolean systemOut;
    /** Creates new form VEPComputeNodeForm */
    public VEPComputeNodeForm(java.awt.Frame parent) {
        super(parent, true);
        initComponents();
        getRootPane().setDefaultButton(submitButton);
        logger = Logger.getLogger("VEP.ComputeNodeForm");
        systemOut = false;
    }
    
    public VEPComputeNodeForm(java.awt.Frame parent, VEPComputeNode vnode, dbHandler handler, JTextArea log) {
        super(parent, true);
        vepCNode = vnode;
        vepCNode.operationStatus = false;
        handle = handler;
        logArea = log;
        initComponents();
        logger = Logger.getLogger("VEP.ComputeNodeForm");
        systemOut = false;
        nodeDesc.setText(vepCNode.description);
        try
        {
            ResultSet rs = handle.query("SELECT", "rid, name", "rack", "");
            rackList.removeAllItems();
            int count = 0;
            while(rs.next())
            {
                if(count == 0)
                    rackList.addItem("No rack selected");
                String rid = Integer.toString(rs.getInt("rid"));
                String name = rs.getString("name");
                if(systemOut)
                System.out.println("Found rack: " + rid + ", " + name);
                logger.trace("Composing racklist: found rack " + rid + "," + name);
                rackList.addItem(rid + ", " + name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No rack was found.");
                logger.trace("Composing racklist: no rack was found.");
                rackList.addItem("No rack found");
            }
            //now populating the list of virtual organizations
            rs = handle.query("select DISTINCT", "vid, name", "vorg", "");
            vorgList.removeAllItems();
            count = 0;
            while(rs.next())
            {
                if(count == 0)
                    vorgList.addItem("No vorg selected");
                String vid = rs.getString("vid");
                String name = rs.getString("name");
                if(systemOut)
                System.out.println("Found vorg: " + vid + " Name: " + name);
                logger.trace("Composing virtual org list: found vorg " + vid + "," + name);
                vorgList.addItem(vid + ", " + name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No vorg was found.");
                logger.trace("Composing virtual org list: no vorg found.");
                vorgList.addItem("No vorg selected");
            }
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
    @Action public void closeForm()
    {
        String rack = rackList.getSelectedItem().toString();
        String clmanager = cmanagerList.getSelectedItem().toString();
        String vorg = vorgList.getSelectedItem().toString();
        if(vorg.contains("No vorg")) vorg = "-1";
        else
            vorg = vorg.split(",")[0];
        if(systemOut)
        System.out.println("Rack Selected: " + rack + "; Cluster Manager: " + clmanager + "; Virtual Organization: " + vorg);
        logger.debug("Rack Selected: " + rack + "; Cluster Manager: " + clmanager + "; Virtual Organization: " + vorg);
        if(rack.contains("No rack") || clmanager.contains("No manager"))
        {
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("You must select a proper rack and/or cluster manager.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(this.getRootPane(), "Error");
            logger.trace("Rack/Cluster manager selection left empty. Error condition.");
            errorMessage.setVisible(true);
        }
        else
        {
            rack = rack.split(",")[0];
            vepCNode.rackId = rack;
            vepCNode.virtualOrgId = vorg;
            vepCNode.clusterManagerType = clmanager;
            vepCNode.description = nodeDesc.getText().trim();
            if(systemOut)
            vepCNode.printInfo();
            //check if this compute node has already been added for federation use
            try
            {
                if(vepCNode.checkAction(handle))
                {
                                        //the host is already under federation control, I should update the details
                    Object[] options = {"Update", "Cancel"};
                    int n = JOptionPane.showOptionDialog(this.getRootPane(), "This node is already under federation use!\nDo you wish to update its details?", "Computenode Update Operation",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                            null,     //do not use a custom Icon
                            options,  //the titles of buttons
                            options[1]); //default button title
                    if(n == 0)
                    {
                        logger.trace("Host " + vepCNode.hostname + " already under federation control. Initiating update.");
                        //update the details
                        boolean proceed = dbHandler.validateSentence(vepCNode.description);
                        if(!proceed)
                        {
                            Dialog errorMessage;
                            JOptionPane errMess = new JOptionPane("Some of the data you provided is not valid!\nCheck for presence of ' in Description field.", JOptionPane.ERROR_MESSAGE);
                            errorMessage = errMess.createDialog(this.getRootPane(), "Invalid format");
                            logger.warn("Host description contains illegal characters. Fix errors and try again.");
                            errorMessage.setVisible(true);
                            return;
                        }
                        else
                        {
                            vepCNode.update(handle, logger, logArea);
                        }
                    }
                    else
                    {
                        logger.trace("Host " + vepCNode.hostname + " already under federation control. Canceling the changes.");
                    }
                }
                else
                {
                    //no entry was found, add this new compute node to the computenode and clmanager table in the database
                    //need to compute the cid as well
                    if(systemOut)
                    System.out.println("No existing entry was found, will try to add this one now.");
                    //check for validity of data.
                    boolean proceed = dbHandler.validateSentence(vepCNode.description);
                    if(!proceed)
                    {
                        Dialog errorMessage;
                        JOptionPane errMess = new JOptionPane("Some of the data you provided is not valid!\nCheck for presence of ' in Description field.", JOptionPane.ERROR_MESSAGE);
                        errorMessage = errMess.createDialog(this.getRootPane(), "Invalid format");
                        logger.warn("Host description contains illegal characters. Fix errors and try again.");
                        errorMessage.setVisible(true);
                        return;
                    }
                    else
                    {
                        vepCNode.insert(handle, logger, logArea);
                    }
                }
            }
            catch(Exception ex)
            {
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        this.dispose();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rackList = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cmanagerList = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        vorgList = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        nodeDesc = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        submitButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(VEPComputeNodeForm.class);
        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        rackList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No rack selected" }));
        rackList.setName("rackList"); // NOI18N

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        cmanagerList.setFont(resourceMap.getFont("cmanagerList.font")); // NOI18N
        cmanagerList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No manager selected", "OpenNebula", "OpenStack", "Nimbus", "Eucalyptus" }));
        cmanagerList.setName("cmanagerList"); // NOI18N

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        vorgList.setFont(resourceMap.getFont("vorgList.font")); // NOI18N
        vorgList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No vorg selected" }));
        vorgList.setName("vorgList"); // NOI18N

        jLabel5.setFont(resourceMap.getFont("jLabel5.font")); // NOI18N
        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        nodeDesc.setColumns(20);
        nodeDesc.setFont(resourceMap.getFont("nodeDesc.font")); // NOI18N
        nodeDesc.setLineWrap(true);
        nodeDesc.setRows(5);
        nodeDesc.setWrapStyleWord(true);
        nodeDesc.setName("nodeDesc"); // NOI18N
        jScrollPane1.setViewportView(nodeDesc);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(46, 46, 46)
                        .addComponent(rackList, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(42, 42, 42)
                        .addComponent(cmanagerList, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(9, 9, 9)
                        .addComponent(vorgList, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel5)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rackList, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmanagerList, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vorgList, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(jLabel5)
                .addGap(5, 5, 5)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 250));

        jPanel2.setBackground(resourceMap.getColor("jPanel2.background")); // NOI18N
        jPanel2.setName("jPanel2"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getActionMap(VEPComputeNodeForm.class, this);
        submitButton.setAction(actionMap.get("closeForm")); // NOI18N
        submitButton.setFont(resourceMap.getFont("submitButton.font")); // NOI18N
        submitButton.setText(resourceMap.getString("submitButton.text")); // NOI18N
        submitButton.setName("submitButton"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(submitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(266, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(submitButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 250, 360, 50));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmanagerList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea nodeDesc;
    private javax.swing.JComboBox rackList;
    private javax.swing.JButton submitButton;
    private javax.swing.JComboBox vorgList;
    // End of variables declaration//GEN-END:variables
    private Logger logger;
}
