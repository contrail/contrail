/*
 * TestView.java
 */

package vep;

import java.awt.BorderLayout;
import java.awt.Color;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import javax.swing.filechooser.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Properties;
import javax.swing.ButtonGroup;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import javax.swing.JTable;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.bridge.SLF4JBridgeHandler;

/**
 * The application's main frame.
 */
public class VEPView extends FrameView 
{
    private String vepProperties;
    private String logPropFile;
    private int logLevel;
    private boolean noTermOut;
    
    public VEPView(SingleFrameApplication app, String propfile, String logpropfile, int loglevel, boolean noTerm) 
    {
        super(app);

        initComponents();
        vepProperties = propfile;
        logPropFile = logpropfile;
        logLevel = loglevel;
        noTermOut = noTerm;
        
        helperMethods = new VEPHelperMethods(vepProperties);
        certHandler = new SSLCertHandler();
        vepAccessControl = new VEPAccessControl(); //just to execute the constructor
        
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();

        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
        this.mainPanel.setPreferredSize(new java.awt.Dimension(850,625));
        this.getFrame().setResizable(false);
        
        /////////////////////////Initializing the logger////////////////////////
        java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        rootLogger.removeHandler(handlers[0]);
        SLF4JBridgeHandler.install();
        ////////////////////////////////////////////////////////////////////////
        Properties log4j = new Properties();
        try
        {
            FileOutputStream fos = null;
            String logFile = "";
            String logSize = "";
            try
            {
//                logFile = VEPHelperMethods.getProperty("veplog.file", logger, "vep.properties");
//                logSize = VEPHelperMethods.getProperty("veplog.size", logger, "vep.properties");
                logFile = VEPHelperMethods.getProperty("veplog.file", logger, vepProperties);
                logSize = VEPHelperMethods.getProperty("veplog.size", logger, vepProperties);
            }
            catch(Exception ex)
            {
                //unable to read the log file values from system properties file
                //using default
                System.out.println("Unable to read log file settings from system properties file. using default values: vep.log and 1024 KB");
                System.err.println(ex.getMessage());
                    
                logFile = "vep.log";
                logSize = "1024";
            }
            int LogSize = 0;
            boolean logFilePresent = false;
            if(logFile!=null && logFile.trim().length() > 0)
            {
                logFile = logFile.trim();
                if(logSize != null && logSize.trim().length() > 0)
                {
                    try
                    {
                        LogSize = Integer.parseInt(logSize);
                    }
                    catch(NumberFormatException ex)
                    {
                        System.err.println(ex.getMessage());
                        LogSize = 0;
                    }
                }
                //LogSize is in KB
                logFilePresent = true;
            }
            String logPart = "";
            switch(logLevel)
            {
                case 0:
                    logPart = "OFF";
                    break;
                case 1:
                    logPart = "FATAL";
                    break;
                case 2:
                    logPart = "ERROR";
                    break;
                case 3:
                    logPart = "WARN";
                    break;
                case 4:
                    logPart = "INFO";
                    break;
                case 5:
                    logPart = "DEBUG";
                    break;
                default:
                    logPart = "TRACE";
                    break;
            }
            if(logFilePresent)
            {
//                log4j.setProperty("log4j.rootLogger", "TRACE, stdout, R");
                if(!noTermOut)
                    log4j.setProperty("log4j.rootLogger", logPart + ", stdout, R");
                else
                    log4j.setProperty("log4j.rootLogger", logPart + ", R");
            }
            else
            {
//                log4j.setProperty("log4j.rootLogger", "TRACE, stdout");
                log4j.setProperty("log4j.rootLogger", logPart + ", stdout");
            }
            log4j.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
            log4j.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
            log4j.setProperty("log4j.appender.stdout.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            if(logFilePresent)
            {
                log4j.setProperty("log4j.appender.R", "org.apache.log4j.RollingFileAppender");
                log4j.setProperty("log4j.appender.R.File", logFile);
                log4j.setProperty("log4j.appender.R.MaxFileSize", Integer.toString(LogSize) + "KB");
                // Keep upto 10 backup file
                log4j.setProperty("log4j.appender.R.MaxBackupIndex", "10");
                log4j.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
                log4j.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            }
//            log4j.store((fos = new FileOutputStream("log4j.properties")), "Author: Piyush Harsh");
            log4j.store((fos = new FileOutputStream(logPropFile)), "Author: Piyush Harsh");
            fos.close();
        }
        catch(Exception ex)
        {
            //ex.printStackTrace(System.err);
            System.out.println("Unable to save to log4j properties file: " + ex);
        }
        rootlogger = Logger.getLogger("VEP");
        switch(logLevel)
        {
            case 0:
                rootlogger.setLevel(Level.OFF);
                break;
            case 1:
                rootlogger.setLevel(Level.FATAL);
                break;
            case 2:
                rootlogger.setLevel(Level.ERROR);
                break;
            case 3:
                rootlogger.setLevel(Level.WARN);
                break;
            case 4:
                rootlogger.setLevel(Level.INFO);
                break;
            case 5:
                rootlogger.setLevel(Level.DEBUG);
                break;
            default:
                rootlogger.setLevel(Level.TRACE);
                break;
        }
//        rootlogger.setLevel(Level.TRACE); //default
        //BasicConfigurator.configure();
//        PropertyConfigurator.configure("log4j.properties");
        PropertyConfigurator.configure(logPropFile);
        logger = Logger.getLogger("VEP.main");
        /////////////////////Logger initialization done/////////////////////////
        
        /////////////Adding custom close event handler//////////////////////////
        this.getFrame().addWindowListener(new WindowAdapter(){
                    @Override
                    public void windowClosing(WindowEvent we)
                    {
                        JDialog message;
                        JOptionPane infoMessage = new JOptionPane("<html>I will perform some housekeeping before I quit!<br>Please be patient, it may take few seconds ...</html>", 
                                JOptionPane.INFORMATION_MESSAGE); 
                        message = infoMessage.createDialog(mainPanel, "Information");
                        logger.debug("EXIT button clicked, initiating state cleanup algorithm next.");
                        message.setVisible(true);
                        cleanUp();
                        System.exit(0);
                    }
        });
        /////////////Custom close event handler code finish/////////////////////
        
        
        oneStatus.setBackground(Color.orange);
        restStatus.setBackground(Color.orange);
        reportingServiceStatus.setBackground(Color.orange);
        adminAuthenticated = false;
        rServer = null;
        dirtyFlag = new DirtyFlag();
        settings = new settingsForm(vepProperties, dirtyFlag);
        DefaultCaret caret = (DefaultCaret)logArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        pThreads = new LinkedList<PeriodicThread>();
        logViewActions = new ButtonGroup();
        logViewActions.add(viewAppLog);
        logViewActions.add(viewDbLog);
        nodeActions = new ButtonGroup();
        nodeActions.add(addNode);
        nodeActions.add(editNode);
        nodeActions.add(deleteNode);
        nodeActions.add(federationAdd);
        nodeInfoPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        nodeListPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        nodeListPane.setBackground(new Color(140,140,140));
        vepHost = new VEPHost();
        vepCNode = new VEPComputeNode();
        dbHandle = null;
        try
        {
            String icon = "Lock_open.png";
            URL url = getClass().getClassLoader().getResource(icon);            
            ImageIcon imageIcon = new ImageIcon(url);
            authState1.setIcon(imageIcon);
            authState1.setToolTipText("not authenticated");
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        
        ////////////////initializing the VEP database handle////////////////////
        try 
        {
            logger.trace("Trying to determine which database driver to use.");
//            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, "vep.properties");
            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
            logger.trace("Found database choice: " + dbType);
            if(dbType.contains("sqlite"))
                Class.forName("org.sqlite.JDBC");
            else
                Class.forName("org.gjt.mm.mysql.Driver");
            dbStatus.setBackground(Color.orange);
            dbStatus.setToolTipText("Database drivers loaded.");
            if(dbType.contains("sqlite"))
                logger.trace("jdbc:sqlite drivers loaded successfully.");
            else
                logger.trace("jdbc:mysql drivers loaded successfully.");
            if(dbType.contains("sqlite"))
            {
                logger.trace("Trying to read database file name.");
//                String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger, "vep.properties");
                String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger, vepProperties);
                if(dbFile != null && dbFile.length() > 3)
                {
                    try
                    {
                        dbHandle = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        dbStatus.setBackground(Color.green);
                        logger.trace("VEP jdbc:sqlite connection was established successfully.");
                        dbStatus.setToolTipText("VEP SQLite DB connection is open.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        dbStatus.setBackground(Color.red);
                        logger.warn("DB connection could not be established for the specified DB file.");
                        dbStatus.setToolTipText("Unable to load VEP DB file.");
                        if(logger.isDebugEnabled())
                            sqlEx.printStackTrace(System.err);
                        else
                            logger.fatal(sqlEx.getMessage());
                    }
                }
                else
                {
                    logger.warn("Database file is not specified. Check system settings and try again.");
                    dbStatus.setBackground(Color.orange);
                    dbStatus.setToolTipText("<html>Database file not specified.<br>Please specify the db file in the system settings and click here.</html>");
                }
            }
            else
            {
                logger.trace("Trying to open mysql connection.");
                String mysqlUser = VEPHelperMethods.getProperty("mysql.user", logger, vepProperties);
                String mysqlPass = VEPHelperMethods.getProperty("mysql.pass", logger, vepProperties);
                String mysqlIP = VEPHelperMethods.getProperty("mysql.ip", logger, vepProperties);
                String mysqlPort = VEPHelperMethods.getProperty("mysql.port", logger, vepProperties);
                if(mysqlUser != null && mysqlPass != null)
                {
                    try
                    {
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        dbHandle = DriverManager.getConnection("jdbc:mysql://" + mysqlIP + ":" + mysqlPort + "/vepdb?" + "user=" + mysqlUser + "&password=" + mysqlPass);
                        dbStatus.setBackground(Color.green);
                        logger.trace("VEP jdbc:mysql connection was established successfully.");
                        dbStatus.setToolTipText("VEP mysql DB connection is open.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        dbStatus.setBackground(Color.red);
                        logger.warn("Mysql DB connection could not be established for the specified mysql parameters.");
                        dbStatus.setToolTipText("Unable to initialize mysql connection.");
                        if(logger.isDebugEnabled())
                            sqlEx.printStackTrace(System.err);
                        else
                            logger.fatal(sqlEx.getMessage());
                    }
                }
                else
                {
                    logger.warn("Mysql connection parameters not specified. Check system settings and try again.");
                    dbStatus.setBackground(Color.orange);
                    dbStatus.setToolTipText("<html>Mysql connection parameters not specified.<br>Please specify the db file in the system settings and click here.</html>");
                }
            }
        } 
        catch (ClassNotFoundException ex) 
        {
            //Logger.getLogger(VEPView.class.getName()).log(Level.SEVERE, null, ex);
            dbStatus.setBackground(Color.red);
            logger.fatal("Missing mysql or sqlite library ... could not load necessary drivers.");
            dbStatus.setToolTipText("Missing mysql or sqlite library ... Could not load database drivers");
        }
        catch(Exception ex)
        {
            //ex.printStackTrace(System.out);
        }
        if(dbHandle != null)
        {
            dbHandler.dbHandle = dbHandle;
            dbhandler = new dbHandler("VEP Main", dbType);
            logger.trace("DB handler created successfully using jdbc:sqlite connection object.");
        }
        else
        {
            logger.warn("VEP DB file has not be specified. Check system settings and try again.");
            dbStatus.setBackground(Color.orange);
            dbStatus.setToolTipText("<html>Database file not specified.<br>Please specify the db file in the system settings and click here.</html>");
        }
        ///////////////////VEP database handle initialization done//////////////
        
        logger.trace("Now initializing the database panel.");
        emptyPane pane = new emptyPane();
        dbContentPane.add(pane);
        if(dbStatus.getBackground() == Color.green)
        {
            hostPool = new VEPHostPool();
            PeriodicThread tempThread = new PeriodicThread(60000, "vepdb-sync", "dosync", logArea, null, null, null, "", vepProperties, runningStat, dirtyFlag, hostPool);
            tempThread.start();
            logger.trace("VEPdbSync thread (periodic, periodicity 60 seconds) started.");
            pThreads.push(tempThread);
        }
        String cliport = VEPHelperMethods.getProperty("cli.port", logger, vepProperties);
        int cliPort = 10555; //default initially
        try
        {
            cliPort = Integer.parseInt(cliport);
        }
        catch(NumberFormatException nfex)
        {
            logger.warn("Garbled/missing CLI Server port value found ... trying to use default port 10555.");
            cliPort = 10555;
        }
        cliserver = new CLIServer(cliPort, dbhandler, vepProperties); //change this port with parameter from vep.configuration
        cliserver.startCLIserver();
        runningStat = new ClusterStats();
        logger.trace("Contrail VEP application has started.");
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = VEPApp.getApplication().getMainFrame();
            aboutBox = new VEPAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        VEPApp.getApplication().show(aboutBox);
        logger.trace("About Box has been shown.");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        controlPanel = new javax.swing.JTabbedPane();
        VMPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        infoLabel = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        ovfFileName = new javax.swing.JTextField();
        OVFselector = new javax.swing.JButton();
        ovfDeploy = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        slaObjectName = new javax.swing.JTextField();
        slaChooser = new javax.swing.JButton();
        serverPanel = new javax.swing.JPanel();
        adminMessage = new javax.swing.JLabel();
        RESTPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        restHTTPPort = new javax.swing.JTextField();
        restSystemServiceCheck = new javax.swing.JCheckBox();
        restSwitch = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        restHTTPSPort = new javax.swing.JTextField();
        clientAuthSelected = new javax.swing.JCheckBox();
        authState = new javax.swing.JLabel();
        ONEPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        xmlrpcSwitch = new javax.swing.JToggleButton();
        reportingPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        reportPeriod = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        reportingSwitch = new javax.swing.JToggleButton();
        loggingPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        logLevelChoice = new javax.swing.JComboBox();
        changeLogLevel = new javax.swing.JButton();
        adminUserPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        adminUser = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        adminPass = new javax.swing.JPasswordField();
        setAdminAccount = new javax.swing.JButton();
        diagnosticPanel = new javax.swing.JPanel();
        viewAppLog = new javax.swing.JRadioButton();
        viewDbLog = new javax.swing.JRadioButton();
        numberEntriesSelector = new javax.swing.JComboBox();
        viewLog = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        logViewPane = new javax.swing.JTextArea();
        logScrollPane = new javax.swing.JScrollPane();
        logTablePane = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        idVal = new javax.swing.JTextField();
        oneItemList = new javax.swing.JComboBox();
        testONEitem = new javax.swing.JButton();
        clusterPanel = new javax.swing.JPanel();
        clusterMgmtPane = new javax.swing.JPanel();
        addNode = new javax.swing.JRadioButton();
        deleteNode = new javax.swing.JRadioButton();
        editNode = new javax.swing.JRadioButton();
        nodeList = new javax.swing.JComboBox();
        nodeInfoPane = new javax.swing.JScrollPane();
        hostInfo = new javax.swing.JLabel();
        nodeAction = new javax.swing.JButton();
        authState1 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        federationAdd = new javax.swing.JRadioButton();
        nodeListPane = new javax.swing.JScrollPane();
        nodeListLabel = new javax.swing.JLabel();
        dbPanel = new javax.swing.JPanel();
        authWarning = new javax.swing.JLabel();
        dbAuthState = new javax.swing.JLabel();
        dbPanelLabel1 = new javax.swing.JLabel();
        dbActionSelector = new javax.swing.JComboBox();
        dbScrollPane = new javax.swing.JScrollPane();
        dbContentPane = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        chartFrame = new javax.swing.JInternalFrame();
        chartSelector = new javax.swing.JComboBox();
        plotTypeLabel = new javax.swing.JLabel();
        statusPane = new javax.swing.JPanel();
        ONELabel = new javax.swing.JLabel();
        oneStatus = new javax.swing.JPanel();
        RESTLabel = new javax.swing.JLabel();
        restStatus = new javax.swing.JPanel();
        REPORTINGLabel = new javax.swing.JLabel();
        reportingServiceStatus = new javax.swing.JPanel();
        dbConnectionLabel = new javax.swing.JLabel();
        dbStatus = new javax.swing.JPanel();
        contrailLogoLabel = new javax.swing.JLabel();
        logPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        logArea = new javax.swing.JTextArea();
        eventBar = new javax.swing.JProgressBar();
        progressbarLabel = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        preferenceMenu = new javax.swing.JMenu();
        setPreferences = new javax.swing.JMenuItem();
        settingsMenu = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(VEPView.class);
        mainPanel.setBackground(resourceMap.getColor("mainPanel.background")); // NOI18N
        mainPanel.setMaximumSize(new java.awt.Dimension(800, 800));
        mainPanel.setMinimumSize(new java.awt.Dimension(790, 525));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(791, 520));
        mainPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        controlPanel.setBackground(resourceMap.getColor("controlPanel.background")); // NOI18N
        controlPanel.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        controlPanel.setFont(resourceMap.getFont("controlPanel.font")); // NOI18N
        controlPanel.setName("controlPanel"); // NOI18N

        VMPanel.setBackground(resourceMap.getColor("VMPanel.background")); // NOI18N
        VMPanel.setMaximumSize(new java.awt.Dimension(498, 384));
        VMPanel.setName("VMPanel"); // NOI18N

        jPanel5.setBackground(resourceMap.getColor("jPanel5.background")); // NOI18N
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("jPanel5.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("jPanel5.border.titleFont"))); // NOI18N
        jPanel5.setName("jPanel5"); // NOI18N

        infoLabel.setFont(resourceMap.getFont("infoLabel.font")); // NOI18N
        infoLabel.setText(resourceMap.getString("infoLabel.text")); // NOI18N
        infoLabel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        infoLabel.setName("infoLabel"); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(134, 134, 134)
                .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(177, Short.MAX_VALUE))
        );

        jPanel6.setBackground(resourceMap.getColor("jPanel6.background")); // NOI18N
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("jPanel6.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("jPanel6.border.titleFont"))); // NOI18N
        jPanel6.setName("jPanel6"); // NOI18N
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setBackground(resourceMap.getColor("jLabel10.background")); // NOI18N
        jLabel10.setFont(resourceMap.getFont("jLabel10.font")); // NOI18N
        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N
        jPanel6.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, 30));

        ovfFileName.setFont(resourceMap.getFont("ovfFileName.font")); // NOI18N
        ovfFileName.setText(resourceMap.getString("ovfFileName.text")); // NOI18N
        ovfFileName.setName("ovfFileName"); // NOI18N
        jPanel6.add(ovfFileName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 220, -1));

        OVFselector.setBackground(resourceMap.getColor("OVFselector.background")); // NOI18N
        OVFselector.setFont(resourceMap.getFont("OVFselector.font")); // NOI18N
        OVFselector.setText(resourceMap.getString("OVFselector.text")); // NOI18N
        OVFselector.setName("OVFselector"); // NOI18N
        OVFselector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OVFselectorActionPerformed(evt);
            }
        });
        jPanel6.add(OVFselector, new org.netbeans.lib.awtextra.AbsoluteConstraints(318, 20, 90, -1));

        ovfDeploy.setBackground(resourceMap.getColor("ovfDeploy.background")); // NOI18N
        ovfDeploy.setFont(resourceMap.getFont("ovfDeploy.font")); // NOI18N
        ovfDeploy.setText(resourceMap.getString("ovfDeploy.text")); // NOI18N
        ovfDeploy.setEnabled(false);
        ovfDeploy.setName("ovfDeploy"); // NOI18N
        ovfDeploy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ovfDeployActionPerformed(evt);
            }
        });
        jPanel6.add(ovfDeploy, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 20, 90, -1));

        jLabel11.setBackground(resourceMap.getColor("jLabel11.background")); // NOI18N
        jLabel11.setFont(resourceMap.getFont("jLabel11.font")); // NOI18N
        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, 20));

        slaObjectName.setFont(resourceMap.getFont("slaObjectName.font")); // NOI18N
        slaObjectName.setText(resourceMap.getString("slaObjectName.text")); // NOI18N
        slaObjectName.setName("slaObjectName"); // NOI18N
        jPanel6.add(slaObjectName, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 60, 170, -1));

        slaChooser.setBackground(resourceMap.getColor("slaChooser.background")); // NOI18N
        slaChooser.setFont(resourceMap.getFont("slaChooser.font")); // NOI18N
        slaChooser.setText(resourceMap.getString("slaChooser.text")); // NOI18N
        slaChooser.setName("slaChooser"); // NOI18N
        slaChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                slaChooserActionPerformed(evt);
            }
        });
        jPanel6.add(slaChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 60, 90, -1));

        javax.swing.GroupLayout VMPanelLayout = new javax.swing.GroupLayout(VMPanel);
        VMPanel.setLayout(VMPanelLayout);
        VMPanelLayout.setHorizontalGroup(
            VMPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, VMPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(VMPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE))
                .addContainerGap())
        );
        VMPanelLayout.setVerticalGroup(
            VMPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, VMPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        controlPanel.addTab(resourceMap.getString("VMPanel.TabConstraints.tabTitle"), VMPanel); // NOI18N

        serverPanel.setBackground(resourceMap.getColor("serverPanel.background")); // NOI18N
        serverPanel.setMaximumSize(new java.awt.Dimension(498, 384));
        serverPanel.setName("serverPanel"); // NOI18N

        adminMessage.setFont(resourceMap.getFont("adminMessage.font")); // NOI18N
        adminMessage.setText(resourceMap.getString("adminMessage.text")); // NOI18N
        adminMessage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        adminMessage.setName("adminMessage"); // NOI18N

        RESTPanel.setBackground(resourceMap.getColor("RESTPanel.background")); // NOI18N
        RESTPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("RESTPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("RESTPanel.border.titleFont"))); // NOI18N
        RESTPanel.setName("RESTPanel"); // NOI18N
        RESTPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setBackground(resourceMap.getColor("jLabel4.background")); // NOI18N
        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        RESTPanel.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, 20));

        restHTTPPort.setBackground(resourceMap.getColor("restHTTPPort.background")); // NOI18N
        restHTTPPort.setFont(resourceMap.getFont("restHTTPPort.font")); // NOI18N
        restHTTPPort.setText(resourceMap.getString("restHTTPPort.text")); // NOI18N
        restHTTPPort.setName("restHTTPPort"); // NOI18N
        RESTPanel.add(restHTTPPort, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 60, 20));

        restSystemServiceCheck.setBackground(resourceMap.getColor("restSystemServiceCheck.background")); // NOI18N
        restSystemServiceCheck.setFont(resourceMap.getFont("restSystemServiceCheck.font")); // NOI18N
        restSystemServiceCheck.setText(resourceMap.getString("restSystemServiceCheck.text")); // NOI18N
        restSystemServiceCheck.setName("restSystemServiceCheck"); // NOI18N
        RESTPanel.add(restSystemServiceCheck, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, -1, 20));

        restSwitch.setBackground(resourceMap.getColor("restSwitch.background")); // NOI18N
        restSwitch.setFont(resourceMap.getFont("restSwitch.font")); // NOI18N
        restSwitch.setText(resourceMap.getString("restSwitch.text")); // NOI18N
        restSwitch.setName("restSwitch"); // NOI18N
        restSwitch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restSwitchActionPerformed(evt);
            }
        });
        RESTPanel.add(restSwitch, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 80, 20));

        jLabel1.setBackground(resourceMap.getColor("jLabel1.background")); // NOI18N
        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        RESTPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, 20));

        restHTTPSPort.setFont(resourceMap.getFont("restHTTPSPort.font")); // NOI18N
        restHTTPSPort.setText(resourceMap.getString("restHTTPSPort.text")); // NOI18N
        restHTTPSPort.setName("restHTTPSPort"); // NOI18N
        RESTPanel.add(restHTTPSPort, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 20, 60, 20));

        clientAuthSelected.setBackground(resourceMap.getColor("clientAuthSelected.background")); // NOI18N
        clientAuthSelected.setFont(resourceMap.getFont("clientAuthSelected.font")); // NOI18N
        clientAuthSelected.setText(resourceMap.getString("clientAuthSelected.text")); // NOI18N
        clientAuthSelected.setName("clientAuthSelected"); // NOI18N
        RESTPanel.add(clientAuthSelected, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, -1, 30));

        authState.setIcon(resourceMap.getIcon("authState.icon")); // NOI18N
        authState.setText(resourceMap.getString("authState.text")); // NOI18N
        authState.setToolTipText(resourceMap.getString("authState.toolTipText")); // NOI18N
        authState.setName("authState"); // NOI18N
        authState.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                authStateMouseClicked(evt);
            }
        });

        ONEPanel.setBackground(resourceMap.getColor("ONEPanel.background")); // NOI18N
        ONEPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("ONEPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("ONEPanel.border.titleFont"))); // NOI18N
        ONEPanel.setName("ONEPanel"); // NOI18N
        ONEPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setBackground(resourceMap.getColor("jLabel6.background")); // NOI18N
        jLabel6.setFont(resourceMap.getFont("jLabel6.font")); // NOI18N
        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N
        ONEPanel.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 15, -1, 30));

        xmlrpcSwitch.setBackground(resourceMap.getColor("xmlrpcSwitch.background")); // NOI18N
        xmlrpcSwitch.setFont(resourceMap.getFont("xmlrpcSwitch.font")); // NOI18N
        xmlrpcSwitch.setText(resourceMap.getString("xmlrpcSwitch.text")); // NOI18N
        xmlrpcSwitch.setName("xmlrpcSwitch"); // NOI18N
        xmlrpcSwitch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xmlrpcSwitchActionPerformed(evt);
            }
        });
        ONEPanel.add(xmlrpcSwitch, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 80, 20));

        reportingPanel.setBackground(resourceMap.getColor("reportingPanel.background")); // NOI18N
        reportingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("reportingPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("reportingPanel.border.titleFont"))); // NOI18N
        reportingPanel.setName("reportingPanel"); // NOI18N
        reportingPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setBackground(resourceMap.getColor("jLabel7.background")); // NOI18N
        jLabel7.setFont(resourceMap.getFont("jLabel7.font")); // NOI18N
        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N
        reportingPanel.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 15, -1, 30));

        reportPeriod.setFont(resourceMap.getFont("reportPeriod.font")); // NOI18N
        reportPeriod.setText(resourceMap.getString("reportPeriod.text")); // NOI18N
        reportPeriod.setName("reportPeriod"); // NOI18N
        reportingPanel.add(reportPeriod, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 60, 20));

        jLabel8.setBackground(resourceMap.getColor("jLabel8.background")); // NOI18N
        jLabel8.setFont(resourceMap.getFont("jLabel8.font")); // NOI18N
        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N
        reportingPanel.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 20, -1, 20));

        reportingSwitch.setBackground(resourceMap.getColor("reportingSwitch.background")); // NOI18N
        reportingSwitch.setFont(resourceMap.getFont("reportingSwitch.font")); // NOI18N
        reportingSwitch.setText(resourceMap.getString("reportingSwitch.text")); // NOI18N
        reportingSwitch.setName("reportingSwitch"); // NOI18N
        reportingSwitch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportingSwitchActionPerformed(evt);
            }
        });
        reportingPanel.add(reportingSwitch, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 80, 20));

        loggingPanel.setBackground(resourceMap.getColor("loggingPanel.background")); // NOI18N
        loggingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("loggingPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("loggingPanel.border.titleFont"))); // NOI18N
        loggingPanel.setName("loggingPanel"); // NOI18N
        loggingPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setBackground(resourceMap.getColor("jLabel9.background")); // NOI18N
        jLabel9.setFont(resourceMap.getFont("jLabel9.font")); // NOI18N
        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N
        loggingPanel.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, 20));

        logLevelChoice.setFont(resourceMap.getFont("logLevelChoice.font")); // NOI18N
        logLevelChoice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "OFF", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" }));
        logLevelChoice.setName("logLevelChoice"); // NOI18N
        loggingPanel.add(logLevelChoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 110, 20));

        changeLogLevel.setBackground(resourceMap.getColor("changeLogLevel.background")); // NOI18N
        changeLogLevel.setFont(resourceMap.getFont("changeLogLevel.font")); // NOI18N
        changeLogLevel.setText(resourceMap.getString("changeLogLevel.text")); // NOI18N
        changeLogLevel.setName("changeLogLevel"); // NOI18N
        changeLogLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeLogLevelActionPerformed(evt);
            }
        });
        loggingPanel.add(changeLogLevel, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 80, 20));

        adminUserPanel.setBackground(resourceMap.getColor("adminUserPanel.background")); // NOI18N
        adminUserPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("adminUserPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("adminUserPanel.border.titleFont"))); // NOI18N
        adminUserPanel.setName("adminUserPanel"); // NOI18N
        adminUserPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(resourceMap.getFont("jLabel5.font")); // NOI18N
        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N
        adminUserPanel.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 80, 20));

        adminUser.setFont(resourceMap.getFont("adminUser.font")); // NOI18N
        adminUser.setText(resourceMap.getString("adminUser.text")); // NOI18N
        adminUser.setToolTipText(resourceMap.getString("adminUser.toolTipText")); // NOI18N
        adminUser.setName("adminUser"); // NOI18N
        adminUserPanel.add(adminUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 110, 20));

        jLabel12.setFont(resourceMap.getFont("jLabel12.font")); // NOI18N
        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N
        adminUserPanel.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, 80, 20));

        adminPass.setFont(resourceMap.getFont("adminPass.font")); // NOI18N
        adminPass.setText(resourceMap.getString("adminPass.text")); // NOI18N
        adminPass.setToolTipText(resourceMap.getString("adminPass.toolTipText")); // NOI18N
        adminPass.setName("adminPass"); // NOI18N
        adminUserPanel.add(adminPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 90, 20));

        setAdminAccount.setFont(resourceMap.getFont("setAdminAccount.font")); // NOI18N
        setAdminAccount.setText(resourceMap.getString("setAdminAccount.text")); // NOI18N
        setAdminAccount.setName("setAdminAccount"); // NOI18N
        setAdminAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setAdminAccountActionPerformed(evt);
            }
        });
        adminUserPanel.add(setAdminAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 80, 20));

        javax.swing.GroupLayout serverPanelLayout = new javax.swing.GroupLayout(serverPanel);
        serverPanel.setLayout(serverPanelLayout);
        serverPanelLayout.setHorizontalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(serverPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(RESTPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addGroup(serverPanelLayout.createSequentialGroup()
                        .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(adminMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(authState))
                        .addContainerGap())
                    .addGroup(serverPanelLayout.createSequentialGroup()
                        .addComponent(reportingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(serverPanelLayout.createSequentialGroup()
                        .addComponent(ONEPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, serverPanelLayout.createSequentialGroup()
                        .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(adminUserPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
                            .addComponent(loggingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE))
                        .addGap(9, 9, 9))))
        );
        serverPanelLayout.setVerticalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(serverPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(adminMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(RESTPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ONEPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reportingPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loggingPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adminUserPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(authState)
                .addContainerGap())
        );

        controlPanel.addTab(resourceMap.getString("serverPanel.TabConstraints.tabTitle"), serverPanel); // NOI18N

        diagnosticPanel.setBackground(resourceMap.getColor("diagnosticPanel.background")); // NOI18N
        diagnosticPanel.setMaximumSize(new java.awt.Dimension(498, 384));
        diagnosticPanel.setName("diagnosticPanel"); // NOI18N
        diagnosticPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        viewAppLog.setBackground(resourceMap.getColor("viewAppLog.background")); // NOI18N
        viewAppLog.setFont(resourceMap.getFont("viewAppLog.font")); // NOI18N
        viewAppLog.setText(resourceMap.getString("viewAppLog.text")); // NOI18N
        viewAppLog.setToolTipText(resourceMap.getString("viewAppLog.toolTipText")); // NOI18N
        viewAppLog.setName("viewAppLog"); // NOI18N
        viewAppLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewAppLogMouseClicked(evt);
            }
        });
        diagnosticPanel.add(viewAppLog, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 150, -1));

        viewDbLog.setBackground(resourceMap.getColor("viewDbLog.background")); // NOI18N
        viewDbLog.setFont(resourceMap.getFont("viewDbLog.font")); // NOI18N
        viewDbLog.setText(resourceMap.getString("viewDbLog.text")); // NOI18N
        viewDbLog.setToolTipText(resourceMap.getString("viewDbLog.toolTipText")); // NOI18N
        viewDbLog.setName("viewDbLog"); // NOI18N
        viewDbLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewDbLogMouseClicked(evt);
            }
        });
        diagnosticPanel.add(viewDbLog, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, 170, -1));

        numberEntriesSelector.setBackground(resourceMap.getColor("numberEntriesSelector.background")); // NOI18N
        numberEntriesSelector.setFont(resourceMap.getFont("numberEntriesSelector.font")); // NOI18N
        numberEntriesSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "last 10 entries", "last 25 entries", "last 50 entries", "last 100 entries", "show all" }));
        numberEntriesSelector.setEnabled(false);
        numberEntriesSelector.setName("numberEntriesSelector"); // NOI18N
        diagnosticPanel.add(numberEntriesSelector, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 10, 120, 20));

        viewLog.setBackground(resourceMap.getColor("viewLog.background")); // NOI18N
        viewLog.setFont(resourceMap.getFont("viewLog.font")); // NOI18N
        viewLog.setText(resourceMap.getString("viewLog.text")); // NOI18N
        viewLog.setName("viewLog"); // NOI18N
        viewLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewLogActionPerformed(evt);
            }
        });
        diagnosticPanel.add(viewLog, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, 80, 20));

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        logViewPane.setColumns(20);
        logViewPane.setEditable(false);
        logViewPane.setFont(resourceMap.getFont("logViewPane.font")); // NOI18N
        logViewPane.setRows(5);
        logViewPane.setName("logViewPane"); // NOI18N
        jScrollPane2.setViewportView(logViewPane);

        diagnosticPanel.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 540, 170));

        logScrollPane.setName("logScrollPane"); // NOI18N

        logTablePane.setBackground(resourceMap.getColor("logTablePane.background")); // NOI18N
        logTablePane.setName("logTablePane"); // NOI18N

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        javax.swing.GroupLayout logTablePaneLayout = new javax.swing.GroupLayout(logTablePane);
        logTablePane.setLayout(logTablePaneLayout);
        logTablePaneLayout.setHorizontalGroup(
            logTablePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logTablePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );
        logTablePaneLayout.setVerticalGroup(
            logTablePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logTablePaneLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(172, Short.MAX_VALUE))
        );

        logScrollPane.setViewportView(logTablePane);

        diagnosticPanel.add(logScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 540, 260));

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        diagnosticPanel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 40, 20));

        idVal.setBackground(resourceMap.getColor("idVal.background")); // NOI18N
        idVal.setFont(resourceMap.getFont("idVal.font")); // NOI18N
        idVal.setText(resourceMap.getString("idVal.text")); // NOI18N
        idVal.setToolTipText(resourceMap.getString("idVal.toolTipText")); // NOI18N
        idVal.setName("idVal"); // NOI18N
        diagnosticPanel.add(idVal, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 490, 70, 20));

        oneItemList.setBackground(resourceMap.getColor("oneItemList.background")); // NOI18N
        oneItemList.setFont(resourceMap.getFont("oneItemList.font")); // NOI18N
        oneItemList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cluster", "Image", "VM", "Host" }));
        oneItemList.setName("oneItemList"); // NOI18N
        diagnosticPanel.add(oneItemList, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 490, 140, 20));

        testONEitem.setBackground(resourceMap.getColor("testONEitem.background")); // NOI18N
        testONEitem.setFont(resourceMap.getFont("testONEitem.font")); // NOI18N
        testONEitem.setText(resourceMap.getString("testONEitem.text")); // NOI18N
        testONEitem.setName("testONEitem"); // NOI18N
        testONEitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testONEitemActionPerformed(evt);
            }
        });
        diagnosticPanel.add(testONEitem, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 490, 120, 20));

        controlPanel.addTab(resourceMap.getString("diagnosticPanel.TabConstraints.tabTitle"), diagnosticPanel); // NOI18N

        clusterPanel.setBackground(resourceMap.getColor("clusterPanel.background")); // NOI18N
        clusterPanel.setAutoscrolls(true);
        clusterPanel.setMaximumSize(new java.awt.Dimension(498, 384));
        clusterPanel.setName("clusterPanel"); // NOI18N
        clusterPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        clusterMgmtPane.setBackground(resourceMap.getColor("clusterMgmtPane.background")); // NOI18N
        clusterMgmtPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("clusterMgmtPane.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("clusterMgmtPane.border.titleFont"))); // NOI18N
        clusterMgmtPane.setName("clusterMgmtPane"); // NOI18N
        clusterMgmtPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addNode.setBackground(resourceMap.getColor("addNode.background")); // NOI18N
        addNode.setFont(resourceMap.getFont("addNode.font")); // NOI18N
        addNode.setText(resourceMap.getString("addNode.text")); // NOI18N
        addNode.setName("addNode"); // NOI18N
        clusterMgmtPane.add(addNode, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, 20));

        deleteNode.setBackground(resourceMap.getColor("deleteNode.background")); // NOI18N
        deleteNode.setFont(resourceMap.getFont("deleteNode.font")); // NOI18N
        deleteNode.setText(resourceMap.getString("deleteNode.text")); // NOI18N
        deleteNode.setToolTipText(resourceMap.getString("deleteNode.toolTipText")); // NOI18N
        deleteNode.setName("deleteNode"); // NOI18N
        clusterMgmtPane.add(deleteNode, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, -1, 20));

        editNode.setBackground(resourceMap.getColor("editNode.background")); // NOI18N
        editNode.setFont(resourceMap.getFont("editNode.font")); // NOI18N
        editNode.setText(resourceMap.getString("editNode.text")); // NOI18N
        editNode.setName("editNode"); // NOI18N
        clusterMgmtPane.add(editNode, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, -1, 20));

        nodeList.setBackground(resourceMap.getColor("nodeList.background")); // NOI18N
        nodeList.setFont(resourceMap.getFont("nodeList.font")); // NOI18N
        nodeList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "not selected" }));
        nodeList.setToolTipText(resourceMap.getString("nodeList.toolTipText")); // NOI18N
        nodeList.setBorder(null);
        nodeList.setEnabled(false);
        nodeList.setName("nodeList"); // NOI18N
        nodeList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nodeListActionPerformed(evt);
            }
        });
        clusterMgmtPane.add(nodeList, new org.netbeans.lib.awtextra.AbsoluteConstraints(329, 20, -1, -1));

        nodeInfoPane.setBackground(resourceMap.getColor("nodeInfoPane.background")); // NOI18N
        nodeInfoPane.setName("nodeInfoPane"); // NOI18N

        hostInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hostInfo.setText(resourceMap.getString("hostInfo.text")); // NOI18N
        hostInfo.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        hostInfo.setName("hostInfo"); // NOI18N
        nodeInfoPane.setViewportView(hostInfo);

        clusterMgmtPane.add(nodeInfoPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 72, 510, 230));

        nodeAction.setBackground(resourceMap.getColor("nodeAction.background")); // NOI18N
        nodeAction.setFont(resourceMap.getFont("nodeAction.font")); // NOI18N
        nodeAction.setText(resourceMap.getString("nodeAction.text")); // NOI18N
        nodeAction.setName("nodeAction"); // NOI18N
        nodeAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nodeActionActionPerformed(evt);
            }
        });
        clusterMgmtPane.add(nodeAction, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, 50, 20));

        authState1.setToolTipText(resourceMap.getString("authState1.toolTipText")); // NOI18N
        authState1.setName("authState1"); // NOI18N
        authState1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                authState1MouseClicked(evt);
            }
        });
        clusterMgmtPane.add(authState1, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 20, -1, -1));

        jLabel13.setBackground(resourceMap.getColor("jLabel13.background")); // NOI18N
        jLabel13.setFont(resourceMap.getFont("jLabel13.font")); // NOI18N
        jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
        jLabel13.setName("jLabel13"); // NOI18N
        clusterMgmtPane.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        federationAdd.setBackground(resourceMap.getColor("federationAdd.background")); // NOI18N
        federationAdd.setFont(resourceMap.getFont("federationAdd.font")); // NOI18N
        federationAdd.setText(resourceMap.getString("federationAdd.text")); // NOI18N
        federationAdd.setToolTipText(resourceMap.getString("federationAdd.toolTipText")); // NOI18N
        federationAdd.setName("federationAdd"); // NOI18N
        clusterMgmtPane.add(federationAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 20, 140, 20));

        clusterPanel.add(clusterMgmtPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 540, 320));

        nodeListPane.setName("nodeListPane"); // NOI18N

        nodeListLabel.setBackground(resourceMap.getColor("nodeListLabel.background")); // NOI18N
        nodeListLabel.setText(resourceMap.getString("nodeListLabel.text")); // NOI18N
        nodeListLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        nodeListLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("nodeListLabel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("nodeListLabel.border.titleFont"))); // NOI18N
        nodeListLabel.setName("nodeListLabel"); // NOI18N
        nodeListPane.setViewportView(nodeListLabel);

        clusterPanel.add(nodeListPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 540, 170));

        controlPanel.addTab(resourceMap.getString("clusterPanel.TabConstraints.tabTitle"), clusterPanel); // NOI18N

        dbPanel.setBackground(resourceMap.getColor("dbPanel.background")); // NOI18N
        dbPanel.setName("dbPanel"); // NOI18N
        dbPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        authWarning.setFont(resourceMap.getFont("authWarning.font")); // NOI18N
        authWarning.setText(resourceMap.getString("authWarning.text")); // NOI18N
        authWarning.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        authWarning.setName("authWarning"); // NOI18N
        dbPanel.add(authWarning, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, -1, -1));

        dbAuthState.setFont(resourceMap.getFont("dbAuthState.font")); // NOI18N
        dbAuthState.setIcon(resourceMap.getIcon("dbAuthState.icon")); // NOI18N
        dbAuthState.setText(resourceMap.getString("dbAuthState.text")); // NOI18N
        dbAuthState.setToolTipText(resourceMap.getString("dbAuthState.toolTipText")); // NOI18N
        dbAuthState.setName("dbAuthState"); // NOI18N
        dbAuthState.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dbAuthStateMouseClicked(evt);
            }
        });
        dbPanel.add(dbAuthState, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, -1, -1));

        dbPanelLabel1.setBackground(resourceMap.getColor("dbPanelLabel1.background")); // NOI18N
        dbPanelLabel1.setFont(resourceMap.getFont("dbPanelLabel1.font")); // NOI18N
        dbPanelLabel1.setText(resourceMap.getString("dbPanelLabel1.text")); // NOI18N
        dbPanelLabel1.setName("dbPanelLabel1"); // NOI18N
        dbPanel.add(dbPanelLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 100, 20));

        dbActionSelector.setBackground(resourceMap.getColor("dbActionSelector.background")); // NOI18N
        dbActionSelector.setFont(resourceMap.getFont("dbActionSelector.font")); // NOI18N
        dbActionSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "select an action", "user management", "datacenter management", "cluster management", "rack management", "virtual organization management", "virtual network management" }));
        dbActionSelector.setEnabled(false);
        dbActionSelector.setName("dbActionSelector"); // NOI18N
        dbActionSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbActionSelectorActionPerformed(evt);
            }
        });
        dbPanel.add(dbActionSelector, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 250, 20));

        dbScrollPane.setBackground(resourceMap.getColor("dbScrollPane.background")); // NOI18N
        dbScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        dbScrollPane.setName("dbScrollPane"); // NOI18N

        dbContentPane.setBackground(resourceMap.getColor("dbContentPane.background")); // NOI18N
        dbContentPane.setName("dbContentPane"); // NOI18N
        dbContentPane.setLayout(new javax.swing.BoxLayout(dbContentPane, javax.swing.BoxLayout.LINE_AXIS));
        dbScrollPane.setViewportView(dbContentPane);

        dbPanel.add(dbScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 540, 350));

        controlPanel.addTab(resourceMap.getString("dbPanel.TabConstraints.tabTitle"), dbPanel); // NOI18N

        mainPanel.add(controlPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 2, 570, 560));

        jSeparator1.setBackground(resourceMap.getColor("jSeparator1.background")); // NOI18N
        jSeparator1.setName("jSeparator1"); // NOI18N
        mainPanel.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(528, 1600, -1, 10));

        chartFrame.setBackground(resourceMap.getColor("chartFrame.background")); // NOI18N
        chartFrame.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("chartFrame.border.title"), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("chartFrame.border.titleFont"))); // NOI18N
        chartFrame.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        chartFrame.setFont(resourceMap.getFont("chartFrame.font")); // NOI18N
        chartFrame.setName("chartFrame"); // NOI18N
        chartFrame.setVisible(true);

        javax.swing.GroupLayout chartFrameLayout = new javax.swing.GroupLayout(chartFrame.getContentPane());
        chartFrame.getContentPane().setLayout(chartFrameLayout);
        chartFrameLayout.setHorizontalGroup(
            chartFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 239, Short.MAX_VALUE)
        );
        chartFrameLayout.setVerticalGroup(
            chartFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 141, Short.MAX_VALUE)
        );

        mainPanel.add(chartFrame, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 10, -1, 170));

        chartSelector.setBackground(resourceMap.getColor("chartSelector.background")); // NOI18N
        chartSelector.setFont(resourceMap.getFont("chartSelector.font")); // NOI18N
        chartSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Item Selected", "CPU Usage", "Memory", "Disk Space", "Network Usage" }));
        chartSelector.setName("chartSelector"); // NOI18N
        chartSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chartSelectorActionPerformed(evt);
            }
        });
        mainPanel.add(chartSelector, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 190, 150, 20));

        plotTypeLabel.setBackground(resourceMap.getColor("plotTypeLabel.background")); // NOI18N
        plotTypeLabel.setFont(resourceMap.getFont("plotTypeLabel.font")); // NOI18N
        plotTypeLabel.setText(resourceMap.getString("plotTypeLabel.text")); // NOI18N
        plotTypeLabel.setName("plotTypeLabel"); // NOI18N
        mainPanel.add(plotTypeLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 190, 80, 20));

        statusPane.setBackground(resourceMap.getColor("statusPane.background")); // NOI18N
        statusPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("statusPane.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("statusPane.border.titleFont"))); // NOI18N
        statusPane.setToolTipText(resourceMap.getString("statusPane.toolTipText")); // NOI18N
        statusPane.setName("statusPane"); // NOI18N
        statusPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ONELabel.setFont(resourceMap.getFont("ONELabel.font")); // NOI18N
        ONELabel.setText(resourceMap.getString("ONELabel.text")); // NOI18N
        ONELabel.setName("ONELabel"); // NOI18N
        statusPane.add(ONELabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, -1));

        oneStatus.setBackground(resourceMap.getColor("oneStatus.background")); // NOI18N
        oneStatus.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        oneStatus.setName("oneStatus"); // NOI18N

        javax.swing.GroupLayout oneStatusLayout = new javax.swing.GroupLayout(oneStatus);
        oneStatus.setLayout(oneStatusLayout);
        oneStatusLayout.setHorizontalGroup(
            oneStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 16, Short.MAX_VALUE)
        );
        oneStatusLayout.setVerticalGroup(
            oneStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 11, Short.MAX_VALUE)
        );

        statusPane.add(oneStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(53, 21, 20, 15));

        RESTLabel.setBackground(resourceMap.getColor("RESTLabel.background")); // NOI18N
        RESTLabel.setFont(resourceMap.getFont("RESTLabel.font")); // NOI18N
        RESTLabel.setText(resourceMap.getString("RESTLabel.text")); // NOI18N
        RESTLabel.setName("RESTLabel"); // NOI18N
        statusPane.add(RESTLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 21, -1, -1));

        restStatus.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        restStatus.setName("restStatus"); // NOI18N

        javax.swing.GroupLayout restStatusLayout = new javax.swing.GroupLayout(restStatus);
        restStatus.setLayout(restStatusLayout);
        restStatusLayout.setHorizontalGroup(
            restStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 16, Short.MAX_VALUE)
        );
        restStatusLayout.setVerticalGroup(
            restStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 11, Short.MAX_VALUE)
        );

        statusPane.add(restStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(132, 21, 20, 15));

        REPORTINGLabel.setBackground(resourceMap.getColor("REPORTINGLabel.background")); // NOI18N
        REPORTINGLabel.setFont(resourceMap.getFont("REPORTINGLabel.font")); // NOI18N
        REPORTINGLabel.setText(resourceMap.getString("REPORTINGLabel.text")); // NOI18N
        REPORTINGLabel.setName("REPORTINGLabel"); // NOI18N
        statusPane.add(REPORTINGLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(172, 21, -1, -1));

        reportingServiceStatus.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        reportingServiceStatus.setName("reportingServiceStatus"); // NOI18N

        javax.swing.GroupLayout reportingServiceStatusLayout = new javax.swing.GroupLayout(reportingServiceStatus);
        reportingServiceStatus.setLayout(reportingServiceStatusLayout);
        reportingServiceStatusLayout.setHorizontalGroup(
            reportingServiceStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 16, Short.MAX_VALUE)
        );
        reportingServiceStatusLayout.setVerticalGroup(
            reportingServiceStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 11, Short.MAX_VALUE)
        );

        statusPane.add(reportingServiceStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(254, 21, 20, 15));

        dbConnectionLabel.setBackground(resourceMap.getColor("dbConnectionLabel.background")); // NOI18N
        dbConnectionLabel.setFont(resourceMap.getFont("dbConnectionLabel.font")); // NOI18N
        dbConnectionLabel.setText(resourceMap.getString("dbConnectionLabel.text")); // NOI18N
        dbConnectionLabel.setName("dbConnectionLabel"); // NOI18N
        statusPane.add(dbConnectionLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, 140, -1));

        dbStatus.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dbStatus.setName("dbStatus"); // NOI18N
        dbStatus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dbStatusMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout dbStatusLayout = new javax.swing.GroupLayout(dbStatus);
        dbStatus.setLayout(dbStatusLayout);
        dbStatusLayout.setHorizontalGroup(
            dbStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 16, Short.MAX_VALUE)
        );
        dbStatusLayout.setVerticalGroup(
            dbStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 12, Short.MAX_VALUE)
        );

        statusPane.add(dbStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 20, -1));

        mainPanel.add(statusPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 560, 560, 50));

        contrailLogoLabel.setBackground(resourceMap.getColor("contrailLogoLabel.background")); // NOI18N
        contrailLogoLabel.setIcon(resourceMap.getIcon("contrailLogoLabel.icon")); // NOI18N
        contrailLogoLabel.setText(resourceMap.getString("contrailLogoLabel.text")); // NOI18N
        contrailLogoLabel.setName("contrailLogoLabel"); // NOI18N
        mainPanel.add(contrailLogoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 510, -1, -1));

        logPanel.setBackground(resourceMap.getColor("logPanel.background")); // NOI18N
        logPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, resourceMap.getString("logPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, resourceMap.getFont("logPanel.border.titleFont"))); // NOI18N
        logPanel.setName("logPanel"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        logArea.setBackground(resourceMap.getColor("logArea.background")); // NOI18N
        logArea.setColumns(20);
        logArea.setEditable(false);
        logArea.setFont(resourceMap.getFont("logArea.font")); // NOI18N
        logArea.setLineWrap(true);
        logArea.setRows(5);
        logArea.setWrapStyleWord(true);
        logArea.setAutoscrolls(true);
        logArea.setBorder(null);
        logArea.setName("logArea"); // NOI18N
        jScrollPane1.setViewportView(logArea);

        javax.swing.GroupLayout logPanelLayout = new javax.swing.GroupLayout(logPanel);
        logPanel.setLayout(logPanelLayout);
        logPanelLayout.setHorizontalGroup(
            logPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
        );
        logPanelLayout.setVerticalGroup(
            logPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
        );

        mainPanel.add(logPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 220, 250, 260));

        eventBar.setBackground(resourceMap.getColor("eventBar.background")); // NOI18N
        eventBar.setFont(resourceMap.getFont("eventBar.font")); // NOI18N
        eventBar.setForeground(resourceMap.getColor("eventBar.foreground")); // NOI18N
        eventBar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        eventBar.setName("eventBar"); // NOI18N
        mainPanel.add(eventBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 490, 160, 10));

        progressbarLabel.setBackground(resourceMap.getColor("progressbarLabel.background")); // NOI18N
        progressbarLabel.setFont(resourceMap.getFont("progressbarLabel.font")); // NOI18N
        progressbarLabel.setText(resourceMap.getString("progressbarLabel.text")); // NOI18N
        progressbarLabel.setName("progressbarLabel"); // NOI18N
        mainPanel.add(progressbarLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 480, 80, 30));

        menuBar.setBackground(resourceMap.getColor("menuBar.background")); // NOI18N
        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getActionMap(VEPView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        preferenceMenu.setText(resourceMap.getString("preferenceMenu.text")); // NOI18N
        preferenceMenu.setName("preferenceMenu"); // NOI18N

        setPreferences.setText(resourceMap.getString("setPreferences.text")); // NOI18N
        setPreferences.setName("setPreferences"); // NOI18N
        setPreferences.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setPreferencesActionPerformed(evt);
            }
        });
        preferenceMenu.add(setPreferences);

        settingsMenu.setText(resourceMap.getString("settingsMenu.text")); // NOI18N
        settingsMenu.setName("settingsMenu"); // NOI18N
        settingsMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingsMenuActionPerformed(evt);
            }
        });
        preferenceMenu.add(settingsMenu);

        menuBar.add(preferenceMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setBackground(resourceMap.getColor("statusPanel.background")); // NOI18N
        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setBackground(resourceMap.getColor("progressBar.background")); // NOI18N
        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 823, Short.MAX_VALUE)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPanelLayout.createSequentialGroup()
                .addContainerGap(687, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(statusMessageLabel)
                        .addComponent(statusAnimationLabel))
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void chartSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chartSelectorActionPerformed
        
        int selectedType = chartSelector.getSelectedIndex();
        if(selectedType != 1 && selectedType != 2) {
            JPanel temp = new JPanel();
            temp.setPreferredSize(new java.awt.Dimension(239,165));
            chartFrame.setContentPane(temp);
        }
        if(selectedType == 1) //total cluster CPU usage chart
        {
            logger.trace("Cluster CPU usage chart selected.");
            try 
            {
//                DefaultPieDataset data = new DefaultPieDataset();
//                data.setValue("In Use", 12312);
//                data.setValue("Free", 6512);
//                JFreeChart chart = ChartFactory.createPieChart("Cluster CPU Usage", data, false, true, true);
                DefaultCategoryDataset result = new DefaultCategoryDataset();
                result.addValue(runningStat.usedCPU, "In Use", "Contrail");
                result.addValue(runningStat.freeCPU, "Free", "Contrail");
                
                JFreeChart chart = ChartFactory.createStackedBarChart("Cluster CPU Usage", "Total CPU", "MHz", 
                                result, PlotOrientation.HORIZONTAL, false, true, true);
                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.setPreferredSize(new java.awt.Dimension(239, 165));
                chartFrame.setContentPane(chartPanel);
                if(chartUpdater != null) chartUpdater.stop();
                chartUpdater = new ChartUpdater(runningStat, selectedType, chartFrame, 60000);
                chartUpdater.start();
                logger.trace("Cluster CPU usage chart shown.");
            } 
            catch(Exception ex) 
            {
                logger.warn("Error during creating CPU usage pie chart.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        if(selectedType == 2) //total cluster memory usage chart
        {
            logger.trace("Cluster Memory usage chart selected.");
            try 
            {
                DefaultCategoryDataset result = new DefaultCategoryDataset();
//                result.addValue(10243, "In Use", "Contrail");
//                result.addValue(5500, "Free", "Contrail");
                result.addValue(runningStat.usedMemory, "In Use", "Contrail");
                result.addValue(runningStat.freeMemory, "Free", "Contrail");
                
                JFreeChart chart = ChartFactory.createStackedBarChart("Cluster Memory Usage", "Total Memory", "KiloBytes", 
                                result, PlotOrientation.HORIZONTAL, false, true, true);
                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.setPreferredSize(new java.awt.Dimension(239, 165));
                chartFrame.setContentPane(chartPanel);
                if(chartUpdater != null) chartUpdater.stop();
                chartUpdater = new ChartUpdater(runningStat, selectedType, chartFrame, 60000);
                chartUpdater.start();
                logger.trace("Cluster Memory usage chart shown.");
            } 
            catch(Exception ex) 
            {
                logger.warn("Error during creating memory usage bar chart.");
            }
        }
        if(selectedType == 3) //total cluster Disk usage chart
        {
            logger.trace("Cluster Disk usage chart selected.");
            try 
            {
                DefaultCategoryDataset result = new DefaultCategoryDataset();
                result.addValue(0, "In Use", "Contrail");
                result.addValue(0, "Free", "Contrail");
                
                JFreeChart chart = ChartFactory.createStackedBarChart("Cluster Disk Usage", "Total Size", "KBytes", 
                                result, PlotOrientation.HORIZONTAL, false, true, true);
                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.setPreferredSize(new java.awt.Dimension(239, 165));
                chartFrame.setContentPane(chartPanel);
                if(chartUpdater != null) chartUpdater.stop();
                chartUpdater = new ChartUpdater(runningStat, selectedType, chartFrame, 60000);
                chartUpdater.start();
                logger.trace("Cluster Disk usage chart shown.");
            } 
            catch(Exception ex) 
            {
                logger.warn("Error during creating Disk usage pie chart.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        if(selectedType == 4) //total cluster NET usage chart
        {
            logger.trace("Cluster NET usage chart selected.");
            try 
            {
                DefaultCategoryDataset result = new DefaultCategoryDataset();
                result.addValue(0, "Tx", "Contrail");
                result.addValue(0, "Rx", "Contrail");
                
                JFreeChart chart = ChartFactory.createStackedBarChart("Cluster Network Usage", "Total Traffic", "KBytes", 
                                result, PlotOrientation.HORIZONTAL, false, true, true);
                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.setPreferredSize(new java.awt.Dimension(239, 165));
                chartFrame.setContentPane(chartPanel);
                if(chartUpdater != null) chartUpdater.stop();
                chartUpdater = new ChartUpdater(runningStat, selectedType, chartFrame, 60000);
                chartUpdater.start();
                logger.trace("Cluster NET usage chart shown.");
            } 
            catch(Exception ex) 
            {
                logger.warn("Error during creating Net usage pie chart.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
}//GEN-LAST:event_chartSelectorActionPerformed
    /**
     * This function is called whenever there is a need to do admin authentication. It sets
     * the internal variable `adminAuthenticated' to true upon successful authentication.
     * 
     * @param null
     * 
     */
    private void performAdminAuthentication()
    {
        //first check if any local admins exist? if not then prompt user to add one
        try
        {
            ResultSet rs = dbhandler.query("select", "count(*)", "vepadmin", "");
            if(rs.next())
            {
                int count = rs.getInt(1);
                if(count < 1)
                {
                    //if(wasDbInitialized)
                    {
                        //invoke the window to enable setup of initial local and federation admin accounts
                        InitAdmins adminForm = new InitAdmins(this.getFrame(), dbhandler);
                        adminForm.setLocationRelativeTo(mainPanel);
                        adminForm.setVisible(true);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        logger.trace("Admin Authentication process initiated.");
        SecurePrompt getAuthData = new SecurePrompt(this.getFrame(), "VEP Access Control");
        getAuthData.setLocationRelativeTo(mainPanel);
        getAuthData.setVisible(true);
        //System.out.println("Hello " + name);
        String gotUser = getAuthData.getUserName();
        String gotPass = getAuthData.getPassword();
        try
        {
            ResultSet rs = dbhandler.query("select", "*", "vepadmin", "where username='" + gotUser + "'");
            if(rs.next())
            {
                if(rs.getString("password").equalsIgnoreCase(gotPass))
                {
                    adminAuthenticated = true;
                    logger.trace("Admin Authentication successful.");
                    try
                    {
                        String icon = "Lock.png";
                        URL url = getClass().getClassLoader().getResource(icon);            
                        ImageIcon imageIcon = new ImageIcon(url);
                        authState.setIcon(imageIcon);
                        authState1.setIcon(imageIcon);
                        dbAuthState.setIcon(imageIcon);
                        authState.setToolTipText("Authenticated! Click on the lock icon to forget authorization ...");
                        authState1.setToolTipText("Authenticated! Click on the lock icon to forget authorization ...");
                        dbAuthState.setToolTipText("Authenticated! Click on the lock icon to forget authorization ...");
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception in setting admin-auth-icon state after successful login.");
                        if(logger.isDebugEnabled())
                            ex.printStackTrace(System.err);
                        else
                            logger.warn(ex.getMessage());
                    }
                }
                else
                {
                    logger.trace("Admin Authentication failed.");
                }
            }
            rs.close();
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught while querying for admin data");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        logger.trace("Admin Authentication process terminated.");
    }
    
    private void restSwitchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restSwitchActionPerformed
        
        if(!adminAuthenticated)
        {
            performAdminAuthentication();
            //System.out.println(getAuthData.getPassword());
        }
        if(adminAuthenticated)
        {
            //do necessary actions depending on the state of the toggle switch
            if(restSwitch.isSelected())
            {
                if(rServer == null)
                {
                    try
                    {
                        restPortValue = Integer.parseInt(restHTTPPort.getText().trim());
                    }
                    catch(Exception ex)
                    {
                        logger.error("Incorrect or empty REST HTTP port value specified, disabling HTTP mode.");
                        if(logger.isDebugEnabled())
                            ex.printStackTrace(System.err);
                        else
                            logger.warn(ex.getMessage());
                        restPortValue = -1; //using default
                    }
                    try
                    {
                        restHTTPSPortValue = Integer.parseInt(restHTTPSPort.getText().trim());
                    }
                    catch(Exception ex)
                    {
//                        logger.error("Incorrect REST HTTPS port value specified, using default value 8183.");
                        logger.error("Incorrect REST HTTPS port value specified, disabling HTTPS mode.");
                        if(logger.isDebugEnabled())
                            ex.printStackTrace(System.err);
                        else
                            logger.warn(ex.getMessage());
//                        restHTTPSPortValue = 8183; //using default
                        restHTTPSPortValue = -1; //using default
                    }
                    
//                    String keyStorePath = VEPHelperMethods.getProperty("rest.keystore", logger, "vep.properties");
//                    String keyStorePass = VEPHelperMethods.getProperty("rest.keystorepass", logger, "vep.properties");
//                    String keyPass = VEPHelperMethods.getProperty("rest.keypass", logger, "vep.properties");
                    String keyStorePath = VEPHelperMethods.getProperty("rest.keystore", logger, vepProperties);
                    String keyStorePass = VEPHelperMethods.getProperty("rest.keystorepass", logger, vepProperties);
                    String keyPass = VEPHelperMethods.getProperty("rest.keypass", logger, vepProperties);
                    
                    if(clientAuthSelected.isSelected())
                        rServer = new restServer(restPortValue, restHTTPSPortValue, true, keyStorePath, keyStorePass, keyPass);
                    else
                        rServer = new restServer(restPortValue, restHTTPSPortValue, false, keyStorePath, keyStorePass, keyPass);
                    logger.trace("Starting REST server.");
                    rServer.start();
                    if(restPortValue != -1 && restHTTPSPortValue != -1)
                    {
                        logger.trace("Started REST server at HTTP port " + restPortValue + " HTTPS port " + restHTTPSPortValue + ".");
                        restStatus.setToolTipText("<html>REST HTTP server is running on port " + restPortValue + 
                            "<br>You can access it at http://localhost:" + restPortValue + "<br>" +
                                "REST HTTPS server is running on port " + restHTTPSPortValue + 
                            "<br>You can access it at https://localhost:" + restHTTPSPortValue +
                                "</html>");
                    }
                    else
                    {
                       if(restPortValue != -1 && restHTTPSPortValue == -1)
                       {
                           logger.trace("Started REST server at HTTP port " + restPortValue + "."); 
                           restStatus.setToolTipText("<html>REST HTTP server is running on port " + restPortValue + 
                                "<br>You can access it at http://localhost:" + restPortValue + "</html>");
                       }
                       else if(restPortValue == -1 && restHTTPSPortValue != -1)
                       {
                           logger.trace("Started REST server at HTTPS port " + restHTTPSPortValue + "."); 
                           restStatus.setToolTipText("<html>REST HTTPS server is running on port " + restHTTPSPortValue + 
                                "<br>You can access it at https://localhost:" + restHTTPSPortValue + "</html>");
                       }
                       else
                       {
                           
                       }
                    }
                    //introducing a delay of 500 msec
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch(Exception ex)
                    {
                        //ignore this exception
                    }
                    if(rServer.getState())
                        restStatus.setBackground(Color.green);
                    else
                    {
                        restStatus.setBackground(Color.orange); //some error occured
                        restStatus.setToolTipText("<html>REST server could not be started.<br>Check if server certificate store is setup properly<br>or if the ports are specified correctly and try again.</html>");
                        restSwitch.setText("start");
                        restSwitch.setSelected(false);
                        return;
                    }
                }
                restSwitch.setText("stop");
            }
            else
            {
                if(rServer != null)
                {
                    rServer.stop();
                    rServer = null;
                    restStatus.setBackground(Color.red);
                    restStatus.setToolTipText("REST server is not running.");
                    logger.trace("REST server has been stopped.");
                }
                restSwitch.setText("start");
            }
        }
        else
        {
            logger.debug("REST server control action denied. Incorrect authorization.");
            //revert the state of the toggle switch
            restSwitch.setSelected(!restSwitch.isSelected());
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("You do not have proper authorization.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(mainPanel, "Access Denied");
            errorMessage.setVisible(true);
        }
    }//GEN-LAST:event_restSwitchActionPerformed

    private void authStateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_authStateMouseClicked
        
        if(authState.getToolTipText().startsWith("Authenticated"))
        {
            adminAuthenticated = false;
            logger.debug("Transitioning from elevated permissions state to normal unauthenticated mode.");
            authState.setToolTipText("not authenticated");
            authState1.setToolTipText("not authenticated");
            dbAuthState.setToolTipText("not authenticated");
            try
            {
                String icon = "Lock_open.png";
                URL url = getClass().getClassLoader().getResource(icon);            
                ImageIcon imageIcon = new ImageIcon(url);
                authState.setIcon(imageIcon);
                authState1.setIcon(imageIcon);
                dbAuthState.setIcon(imageIcon);
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught while resetting the auth-icon.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
    }//GEN-LAST:event_authStateMouseClicked

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        logger.debug("EXIT button clicked, initiating state cleanup algorithm next.");
        JDialog message;
        JOptionPane infoMessage = new JOptionPane("<html>I will perform some housekeeping before I quit!<br>Please be patient, it may take few seconds ...</html>", 
                JOptionPane.INFORMATION_MESSAGE); 
        message = infoMessage.createDialog(mainPanel, "Information");
        message.setVisible(true);
        cleanUp();
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void setPreferencesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setPreferencesActionPerformed
        // TODO add your handling code here:
        preferenceForm pForm = new preferenceForm();
        pForm.setLocationRelativeTo(mainPanel);
        logger.trace("Preference menu form displayed.");
        pForm.setVisible(true);
    }//GEN-LAST:event_setPreferencesActionPerformed

    private void settingsMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingsMenuActionPerformed
        // TODO add your handling code here:
        settings.setLocationRelativeTo(mainPanel);
        logger.trace("Settings menu form displayed.");
        settings.setVisible(true);
    }//GEN-LAST:event_settingsMenuActionPerformed

    private void xmlrpcSwitchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xmlrpcSwitchActionPerformed
        // TODO add your handling code here:
        if(!adminAuthenticated)
        {
            performAdminAuthentication();
            //System.out.println(getAuthData.getPassword());
        }
        if(adminAuthenticated)
        {
            try
            {
//                oneIP = VEPHelperMethods.getProperty("one.ip", logger, "vep.properties");
//                oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, "vep.properties");
//                oneUser = VEPHelperMethods.getProperty("one.user", logger, "vep.properties");
//                onePass = VEPHelperMethods.getProperty("one.pass", logger, "vep.properties");
                oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
                oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
                oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
                onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
            }
            catch(Exception ex)
            {
                logger.error("vep.properties file not found. OpenNebula XML-RPC connection could not be completed.");
                oneIP = null;
                oneXmlPort = null;
                oneUser = null;
                onePass = null;
                JDialog errorMessage;
                JOptionPane errMess = new JOptionPane("<html>ONE XMLRPC parameters are not set correctly.<br>Go to <b>Edit -> Settings</b> and set parameters and try again.</html>", JOptionPane.ERROR_MESSAGE);
                errorMessage = errMess.createDialog(mainPanel, "Action can not be completed");
                errorMessage.setVisible(true);
                return;
            }
            if(xmlrpcSwitch.isSelected())
            {
                xmlrpcSwitch.setText("disable");
                String param = oneIP + ":" + oneXmlPort + ":" + oneUser + ":" + onePass;
                PeriodicThread tempThread = new PeriodicThread(60000, "one-xmlrpc", "hostmonitor", logArea, nodeListLabel, nodeList, oneStatus, param, vepProperties, runningStat, dirtyFlag, null);
                tempThread.start();
                logger.trace("XML-RPC connection thread (periodic, periodicity 60 seconds) started.");
                pThreads.push(tempThread);
            }
            else
            {
                xmlrpcSwitch.setText("enable");
                try
                {
                    nodeList.setEnabled(false);
                    PeriodicThread temp = pThreads.pop();
                    temp.terminate();
                    logger.trace("XML-RPC periodic thread stopped.");
                    nodeListLabel.setText("<html><div style='padding:5px;background:white;font-family:Verdana;font-size:10pt;width:370px;'></div></html>");
                    nodeList.removeAllItems();
                    nodeList.addItem("not selected");
                }
                catch(NoSuchElementException ex)
                {
                    logger.warn("Empty thread stack!");
                }
            }
        }
        else
        {
            //revert the state of the toggle switch
            xmlrpcSwitch.setSelected(!xmlrpcSwitch.isSelected());
            logger.debug("XML-RPC connection not initiated because of improper authorization.");
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("You do not have proper authorization.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(mainPanel, "Access Denied");
            errorMessage.setVisible(true);
        }
    }//GEN-LAST:event_xmlrpcSwitchActionPerformed

    private void reportingSwitchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportingSwitchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_reportingSwitchActionPerformed

    private void OVFselectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OVFselectorActionPerformed
        // TODO add your handling code here:
        ovfDeploy.setText("test");
        ovfDeploy.setEnabled(false);
        
        JFileChooser fc = new JFileChooser(".");
        
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".ovf")
                || f.getName().toLowerCase().endsWith(".ova")
                || f.isDirectory();
            }

            public String getDescription() {
                return "OVF File";
            }
        });
        
        int returnVal = fc.showOpenDialog(this.getComponent());
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = fc.getSelectedFile();
            //This is where a real application would open the file.
            ovfFileName.setText(file.getAbsolutePath());
            ovfDeploy.setEnabled(true);
        }
        else
        {
            ovfFileName.setText("");
            ovfDeploy.setEnabled(false);
        }
    }//GEN-LAST:event_OVFselectorActionPerformed

    private void ovfDeployActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ovfDeployActionPerformed
        // TODO add your handling code here:
        if(ovfDeploy.getText().equalsIgnoreCase("test"))
        {
            ovfDeploy.setEnabled(false);
            logger.trace("OVF validation process initiated.");
            JDialog message;
            JOptionPane infoMess = new JOptionPane("I am going to download the OVF schema files from DMTF website.\nThe download process and XML validation may take some time ...\nPlease be patient ...", JOptionPane.INFORMATION_MESSAGE);
            message = infoMess.createDialog(mainPanel, "Be patient");
            message.setVisible(true);

            xmlHandle = new ovfValidator(ovfFileName.getText().trim(), logArea);
            ThreadWorker xmlValidator = new ThreadWorker("xmlHandler", "validate", xmlHandle);
            xmlValidator.start();
            logger.trace("OVF validation process started.");
            ThreadMonitor mon = new ThreadMonitor("xmlHandler", "validate", xmlValidator, eventBar, logArea, mainPanel, ovfDeploy);
            mon.start();
            logger.trace("OVF validation process monitoring thread started.");
        }
        if(ovfDeploy.getText().equalsIgnoreCase("deploy"))
        {
            JDialog message;
            JOptionPane infoMess = new JOptionPane("This feature is currently disabled, try deploying\nyour application through the REST interface!", JOptionPane.INFORMATION_MESSAGE);
            message = infoMess.createDialog(mainPanel, "Not allowed");
            message.setVisible(true);
        }
    }//GEN-LAST:event_ovfDeployActionPerformed

    private void slaChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_slaChooserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_slaChooserActionPerformed

    private void authState1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_authState1MouseClicked
        // TODO add your handling code here:
        if(authState1.getToolTipText().startsWith("Authenticated"))
        {
            adminAuthenticated = false;
            logger.debug("Transitioning from elevated permissions state to normal unauthenticated mode.");
            authState1.setToolTipText("not authenticated");
            authState.setToolTipText("not authenticated");
            dbAuthState.setToolTipText("not authenticated");
            try
            {
                String icon = "Lock_open.png";
                URL url = getClass().getClassLoader().getResource(icon);            
                ImageIcon imageIcon = new ImageIcon(url);
                authState.setIcon(imageIcon);
                authState1.setIcon(imageIcon);
                dbAuthState.setIcon(imageIcon);
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught in resetting the auth-icon.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
    }//GEN-LAST:event_authState1MouseClicked

    private void nodeListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nodeListActionPerformed

        if(nodeList.isEnabled())
        {
            String selectedIndex = (String)nodeList.getSelectedItem();
            String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
            if(oneVersion.startsWith("2.2"))
            {
                if(oneHandle == null)
                    oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:nodeListActionPerformed");
                if(!selectedIndex.startsWith("not "))
                {
                    int hostId = Integer.parseInt(selectedIndex.split(" ")[1]);
                    logger.trace("OpenNebula host " + hostId + " selected.");
                    oneHandle.getHostInfo(hostId, hostInfo, vepCNode);
                }
                else
                {
                    hostInfo.setText("<html><div style='color:black;background:white;width:370px;font-family:Verdana;font-size:10pt'>Selected node information is displayed here.</div></html>");
                }
            }
            else if(oneVersion.startsWith("3.4"))
            {
                if(one3Handle == null)
                    one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:nodeListActionPerformed");
                if(!selectedIndex.startsWith("not "))
                {
                    int hostId = Integer.parseInt(selectedIndex.split(" ")[1]);
                    logger.trace("OpenNebula3 host " + hostId + " selected.");
                    one3Handle.getHostInfo(hostId, hostInfo, vepCNode);
                }
                else
                {
                    hostInfo.setText("<html><div style='color:black;background:white;width:370px;font-family:Verdana;font-size:10pt'>Selected node information is displayed here.</div></html>");
                }
            }
        }
        
    }//GEN-LAST:event_nodeListActionPerformed

    private void nodeActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nodeActionActionPerformed
        // TODO add your handling code here:
        //System.out.println(nodeActions.getSelection());
        String action = null;
        if(deleteNode.isSelected()) action = "delete";
        else if(addNode.isSelected()) action = "add";
        else if(editNode.isSelected()) action = "edit";
        else if(federationAdd.isSelected()) action = "addfed";
        
        int host = -1;
        if(action != null && (action.startsWith("delete") || action.startsWith("edit") || action.startsWith("addfed")))
        {
            if(nodeList.isEnabled() && !((String)nodeList.getSelectedItem()).contains("not selected"))
            {
                String selectedIndex = (String)nodeList.getSelectedItem();
                host = Integer.parseInt(selectedIndex.split(" ")[1]);
            }
        }
        if(!adminAuthenticated)
        {
            performAdminAuthentication();
        }
        if(adminAuthenticated)
        {
            if(action != null && (action.startsWith("delete") || action.startsWith("edit") || action.startsWith("addfed")) && host == -1)
            {
                JDialog errorMessage;
                JOptionPane errMess = new JOptionPane("Invalid host selection.", JOptionPane.ERROR_MESSAGE);
                errorMessage = errMess.createDialog(mainPanel, "Error");
                errorMessage.setVisible(true);
                return;
            }
            String oneVersion = VEPHelperMethods.getProperty("one.version",logger, vepProperties);
            oneHandle = null;
            one3Handle = null;
            if(oneVersion.startsWith("2.2"))
                oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:nodeActionActionPerformed");
            else if(oneVersion.startsWith("3.4"))
                one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:nodeActionActionPerformed");
            String clusterId = VEPHelperMethods.getProperty("contrail.cluster", logger, vepProperties);
            logger.debug("Contrail cluster ID from settings file: " + clusterId + ".");
            //check to see if cluster contrail is present or not
            int clusterid = -1;
            try
            {
                clusterid = Integer.parseInt(VEPHelperMethods.getProperty("contrail.cluster", logger, vepProperties));
            }
            catch(Exception ex)
            {
                clusterid = -1;
            }
            if(clusterId == null || clusterId.length() == 0 || Integer.parseInt(clusterId) < 0)
            {
                //test if the cluster already exists, if yes get the id and
                //put in the properties file, if not then
                //create a new cluster
                LinkedList<ONECluster> clusterList = null;
                if(oneHandle != null)
                    clusterList = oneHandle.getClusterList();
                else if(one3Handle != null)
                    clusterList = one3Handle.getClusterList();
                
                boolean found = false;
                
                for(int i=0; i< clusterList.size(); i++)
                {
                    if(clusterList.get(i).name.equalsIgnoreCase("contrail"))
                    {
                        found = true;
                        clusterid = clusterList.get(i).id;
                        break;
                    }
                }
                if(!found)
                {
                    logger.debug("Seems like contrail cluster does not exist, attempting to create one now.");
                    if(oneHandle != null)
                        clusterid = oneHandle.addCluster("contrail");
                    else if(one3Handle != null)
                        clusterid = one3Handle.addCluster("contrail");
                    if(clusterid == -1)
                    {
                        logger.error("Could not create the contrail cluster. Check ONE settings and try again.");
                    }
                    else
                    {
                        logger.debug("Created contrail cluster successfully. Adding the parameter to system properties file.");
                        boolean status = VEPHelperMethods.addProperty("contrail.cluster", Integer.toString(clusterid), logger, vepProperties);
                        if(status == false)
                        {
                            logger.error("Could not insert contrail.cluster property in the settings file.");
                        }
                        else
                        {
                            logger.debug("Inserted contrail.cluster property successfully into system settings file.");
                        }
                    }
                }
                else
                {
                    logger.debug("Found contrail cluster with id " + clusterid + ". Updating the vep properties file now.");
                    boolean status = VEPHelperMethods.addProperty("contrail.cluster", Integer.toString(clusterid), logger, vepProperties);
                    if(status == false)
                    {
                        logger.error("Could not insert contrail.cluster property in the settings file.");
                    }
                    else
                    {
                        logger.debug("Inserted contrail.cluster property successfully into VEP properties file.");
                    }
                }
            }
            //the above check does not test if the properties contains a value and if it really exists!!!
            
            if(action != null && action.startsWith("delete"))
            {
                Object[] options = {"Remove", "Don't remove"};
                int n = JOptionPane.showOptionDialog(mainPanel, "Do you wish to remove host " + host + " from federation use?", "Host Delete Operation",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null,     //do not use a custom Icon
                        options,  //the titles of buttons
                        options[1]); //default button title
                if(n == 0)
                {
                    //boolean status = oneHandle.deleteHost(host);
                    boolean status = false;
                    if(oneHandle != null)
                        status = oneHandle.removeFromCluster(host);
                    else if(one3Handle != null)
                        status = one3Handle.removeFromCluster(host, clusterid);
                    if(status)
                    {
                        //now remove this host detail from VEP-DB too
                        try
                        {
                            ResultSet rs = dbhandler.query("select", "cid", "computenode", "where clmgrtype='OpenNebula'");
                            boolean found = false;
                            int cid = -1;
                            int counter=0;
                            while(rs.next() && !found)
                            {
                                counter++;
                                cid = rs.getInt("cid");
                                ResultSet rs1 = dbhandler.query("select", "cid", "clmanager", "where hostid='" + host + "'");
                                if(rs1.next())
                                {
                                    rs1.close();
                                    found=true;
                                }
                                rs = dbhandler.query("select", "cid", "computenode", "where clmgrtype='OpenNebula'");
                                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                            }
                            rs.close();
                            if(found)
                            {
                                if(cid != -1)
                                {
                                    dbhandler.delete("clmanager", "where cid=" + cid);
                                    dbhandler.delete("computenode", "where cid=" + cid);
                                    logger.debug("Host " + host + " was removed from federation successfully. ONE and VEP-DB updated successfull");
                                    JDialog infoMessage;
                                    JOptionPane infoMess = new JOptionPane("Host " + host + " removed from federation successfully.", JOptionPane.INFORMATION_MESSAGE);
                                    infoMessage = infoMess.createDialog(mainPanel, "Success");
                                    infoMessage.setVisible(true);
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            logger.warn("[ONE Host:" + host + "] Exception caught while updating VEP-DB. Check logs and manually sync tables. Check: [computenode,clmanager]");
                            if(logger.isDebugEnabled())
                                ex.printStackTrace(System.err);
                            else
                                logger.warn(ex.getMessage());
                        }
                    }
                    else
                    {
                        logger.debug("Host " + host + " removal operation failed.");
                        JDialog errorMessage;
                        JOptionPane errMess = new JOptionPane("Operation failed. Check logs for more information.", JOptionPane.ERROR_MESSAGE);
                        errorMessage = errMess.createDialog(mainPanel, "Error");
                        errorMessage.setVisible(true);
                    }
                }
                return;
            }
            else if(action != null && action.startsWith("edit"))
            {
                
            }
            else if(action != null && action.startsWith("addfed"))
            {
                try
                {
                    clusterid = Integer.parseInt(VEPHelperMethods.getProperty("contrail.cluster", logger, vepProperties));
                }
                catch(Exception ex)
                {
                    clusterid = -1;
                }
                if(clusterid != -1)
                {
                    VEPComputeNodeForm temp1 = new VEPComputeNodeForm(this.getFrame(), vepCNode, dbhandler, logArea);
                    temp1.setLocationRelativeTo(mainPanel);
                    logger.trace("Generating form to gather extra node information for federation use.");
                    temp1.setVisible(true);
                    
                    if(vepCNode.operationStatus)
                    {
                        try
                        {
                            if(oneHandle != null)
                                oneHandle.addHostToCluster(clusterid, vepCNode.hostID);
                            else if(one3Handle != null)
                                one3Handle.addHostToCluster(clusterid, vepCNode.hostID);
                            logger.debug("Added host " + vepCNode.hostID + " to contrail cluster in OpenNebula.");
                        }
                        catch(Exception ex)
                        {
                            logger.warn("Host " + vepCNode.hostID + " could not be added to contrail cluster in OpenNebula.");
                            if(logger.isDebugEnabled())
                                ex.printStackTrace(System.err);
                            else
                                logger.warn(ex.getMessage());
                            return;
                        }
                    }
                }
                else
                {
                    logger.error("Something went wrong while determining the existence of the cluster contrail.");
                    logger.error("Compute node addition to federation failed. Contrail cluster\ninformation missing or properties entry incorrect. Try deleting the\n contrail cluster value in the properties and try again.");
                    JDialog errorMessage;
                    JOptionPane errMess = new JOptionPane("Operation failed. Contrail Cluster information could not be retrieved.\nCheck logs for more information.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(mainPanel, "Error");
                    errorMessage.setVisible(true);
                }    
            }
            else if(action != null && action.startsWith("add")) //this function simply adds a host to OpenNebula
            {
                AddEditNode temp = new AddEditNode(this.getFrame(), vepHost);
                temp.setLocationRelativeTo(mainPanel);
                temp.setVisible(true);
                if(vepHost.getValidity() == 1)
                {
                    boolean status = false;
                    if(oneHandle != null)
                        status = oneHandle.addHost(vepHost.hostName, vepHost.infoDriver, vepHost.virtDriver, vepHost.storageDriver);
                    else if(one3Handle != null)
                        status = one3Handle.addHost(vepHost.hostName, vepHost.infoDriver, vepHost.virtDriver, vepHost.networkDriver);
                    if(status)
                    {
                        JDialog infoMessage;
                        JOptionPane infoMess = new JOptionPane("Host " + vepHost.hostName + " added successfully.", JOptionPane.INFORMATION_MESSAGE);
                        infoMessage = infoMess.createDialog(mainPanel, "Success");
                        logger.trace("Host " + vepHost.hostName + " was added successfully.");
                        infoMessage.setVisible(true);
                    }
                    else
                    {
                        JDialog errorMessage;
                        JOptionPane errMess = new JOptionPane("Operation failed. Check logs for more information.", JOptionPane.ERROR_MESSAGE);
                        errorMessage = errMess.createDialog(mainPanel, "Error");
                        logger.error("Host " + vepHost.hostName + " addition failed.");
                        errorMessage.setVisible(true);
                    }
                    return;
                }
            }
        }
        else
        {
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("You do not have proper authorization.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(mainPanel, "Access Denied");
            logger.warn("Action could not be completed due to insufficient privilage.");
            errorMessage.setVisible(true);
        }
    }//GEN-LAST:event_nodeActionActionPerformed

private void dbStatusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dbStatusMouseClicked
// TODO add your handling code here:
    if(dbStatus.getBackground() == Color.orange)
    {
        logger.trace("Trying to reinitialize the database handle.");
        String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger, vepProperties);
        if(dbFile != null && dbFile.length() > 3)
        {
            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
            boolean driverLoaded = false;
            try
            {
                if(dbType.contains("sqlite"))
                    Class.forName("org.sqlite.JDBC");
                else
                    Class.forName("org.gjt.mm.mysql.Driver");
                driverLoaded = true;
            }
            catch(Exception ex)
            {
                driverLoaded = false;
            }
            
            try
            {
                if(driverLoaded)
                {
                    if(dbType.contains("sqlite"))
                    {
                        dbHandle = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
                    }
                    else
                    {
                        String mysqlUser = VEPHelperMethods.getProperty("mysql.user", logger, vepProperties);
                        String mysqlPass = VEPHelperMethods.getProperty("mysql.pass", logger, vepProperties);
                        String mysqlIP = VEPHelperMethods.getProperty("mysql.ip", logger, vepProperties);
                        String mysqlPort = VEPHelperMethods.getProperty("mysql.port", logger, vepProperties);
                        if(mysqlUser != null && mysqlPass != null)
                        {
                            dbHandle = DriverManager.getConnection("jdbc:mysql://" + mysqlIP + ":" + mysqlPort + "/vepdb?" + "user=" + mysqlUser + "&password=" + mysqlPass);
                        }
                    }
                    dbStatus.setBackground(Color.green);
                    dbStatus.setToolTipText("VEP DB connection is open.");
                    if(dbType.contains("sqlite"))
                        logger.trace("Database jdbc:sqlite connection created successfully.");
                    else
                        logger.trace("Database jdbc:mysql connection created successfully.");
                    logger.trace("Initiating the sanity check on the database.");
                    testDb();
                }
            }
            catch(SQLException sqlEx)
            {
                if(dbType.contains("sqlite"))
                    logger.error("Unable to create jdbc:sqlite connection to the database file.");
                else
                    logger.error("Unable to create jdbc:mysql connection to the database file.");
                dbHandle = null;
                dbStatus.setBackground(Color.red);
                dbStatus.setToolTipText("Unable to load VEP DB.");
                if(logger.isDebugEnabled())
                    sqlEx.printStackTrace(System.err);
                else
                    logger.warn(sqlEx.getMessage());
            }
        }
        else
        {
            dbStatus.setBackground(Color.orange);
            logger.warn("Database file not specified in the system settings. Specify the VEP db file and try again.");
            dbStatus.setToolTipText("<html>Database file not specified.<br>Please specify the db file in the system settings and click here.</html>");
        }
        if(dbHandle != null)
        {
            dbHandler.dbHandle = dbHandle;
            dbhandler = new dbHandler("VEP Main", dbType);
            if(dbType.contains("sqlite"))
                logger.trace("DB Handler object created successfully using the jdbc:sqlite handle.");
            else
                logger.trace("DB Handler object created successfully using the jdbc:mysql handle.");
        }
        else
        {
            logger.warn("DB handler object could not be created. Please check system settings and try again.");
            dbStatus.setBackground(Color.orange);
            dbStatus.setToolTipText("<html>Database file not specified.<br>Please specify the db file in the system settings and click here.</html>");
        }
        try
        {
            Thread.sleep(500);
        }
        catch(Exception ex)
        {
            
        }
        if(dbStatus.getBackground() == Color.green)
        {
            hostPool = new VEPHostPool();
            PeriodicThread tempThread = new PeriodicThread(60000, "vepdb-sync", "dosync", logArea, null, null, null, "", vepProperties, runningStat, dirtyFlag, hostPool);
            tempThread.start();
            logger.trace("VEPdbSync thread (periodic, periodicity 60 seconds) started.");
            pThreads.push(tempThread);
        }
    }
    else
    {
        //nothing to be done
    }
}//GEN-LAST:event_dbStatusMouseClicked

private void dbAuthStateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dbAuthStateMouseClicked
// TODO add your handling code here:
    if(dbAuthState.getToolTipText().startsWith("Authenticated"))
    {
        logger.trace("Transitioning from elevated permissions state to unauthenticated mode.");
        adminAuthenticated = false;
        authState1.setToolTipText("not authenticated");
        authState.setToolTipText("not authenticated");
        dbAuthState.setToolTipText("not authenticated");
        try
        {
            String icon = "Lock_open.png";
            URL url = getClass().getClassLoader().getResource(icon);            
            ImageIcon imageIcon = new ImageIcon(url);
            authState.setIcon(imageIcon);
            authState1.setIcon(imageIcon);
            dbAuthState.setIcon(imageIcon);
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught while resetting the auth-icon.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
}//GEN-LAST:event_dbAuthStateMouseClicked

private void dbActionSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbActionSelectorActionPerformed
// TODO add your handling code here:
    String type = dbActionSelector.getSelectedItem().toString();
    int index = dbActionSelector.getSelectedIndex();
    if(!adminAuthenticated)
    {
        performAdminAuthentication();
        //adminAuthenticated = true; //for testing
    }
    if(adminAuthenticated)
    {
        switch(index)
        {
            case 1: //user management
                dbContentPane.removeAll();
                userManagement uMgmt;
                if(dbhandler != null) uMgmt = new userManagement(dbhandler, logArea, vepProperties);
                else
                    uMgmt = new userManagement();
                dbContentPane.add(uMgmt);
                logger.trace("DB: User management form generated.");
                dbContentPane.getRootPane().revalidate();
                break;
            case 2: //datacenter management
                dbContentPane.removeAll();
                datacenterManagement dcenterMgmt;
                if(dbhandler != null) dcenterMgmt = new datacenterManagement(dbhandler, logArea);
                else
                    dcenterMgmt = new datacenterManagement();
                dbContentPane.add(dcenterMgmt);
                logger.trace("DB: Datacenter form generated.");
                dbContentPane.getRootPane().revalidate();
                break;
            case 3: //cluster management
                dbContentPane.removeAll();
                clusterManagement clusterMgmt;
                if(dbhandler != null) clusterMgmt = new clusterManagement(dbhandler, logArea);
                else
                    clusterMgmt = new clusterManagement();
                dbContentPane.add(clusterMgmt);
                logger.trace("DB: Cluster management form generated.");
                dbContentPane.getRootPane().revalidate();
                break;
            case 4: //rack management
                dbContentPane.removeAll();
                rackManagement rackMgmt;
                if(dbhandler != null) rackMgmt = new rackManagement(dbhandler, logArea);
                else
                    rackMgmt = new rackManagement();
                dbContentPane.add(rackMgmt);
                logger.trace("DB: Rack management form generated.");
                dbContentPane.getRootPane().revalidate();
                break;
            case 5: //virtual organization management
                //break;
            case 6: //virtual network management
                //break;
            default:
                dbContentPane.removeAll();
                emptyPane pane = new emptyPane();
                dbContentPane.add(pane);
                logger.trace("DB: Resetting the user interface.");
                dbContentPane.getRootPane().revalidate();
        }
    }
    else
    {
        JDialog errorMessage;
        JOptionPane errMess = new JOptionPane("You do not have proper authorization.", JOptionPane.ERROR_MESSAGE);
        errorMessage = errMess.createDialog(mainPanel, "Access Denied");
        logger.trace("DB - Action cancelled due to improper authorization.");
        errorMessage.setVisible(true);
    }
}//GEN-LAST:event_dbActionSelectorActionPerformed

    private void changeLogLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeLogLevelActionPerformed
// TODO add your handling code here:
        String logLevelValue = logLevelChoice.getSelectedItem().toString();
        logger.info("Changing j4log log level to " + logLevel);
        if(logLevelValue.equalsIgnoreCase("TRACE"))
            rootlogger.setLevel(Level.TRACE);
        else if(logLevelValue.equalsIgnoreCase("DEBUG"))
            rootlogger.setLevel(Level.DEBUG);
        else if(logLevelValue.equalsIgnoreCase("INFO"))
            rootlogger.setLevel(Level.INFO);
        else if(logLevelValue.equalsIgnoreCase("WARN"))
            rootlogger.setLevel(Level.WARN);
        else if(logLevelValue.equalsIgnoreCase("ERROR"))
            rootlogger.setLevel(Level.ERROR);
        else if(logLevelValue.equalsIgnoreCase("FATAL"))
            rootlogger.setLevel(Level.FATAL);
        else
            rootlogger.setLevel(Level.OFF);
    }//GEN-LAST:event_changeLogLevelActionPerformed

    private void viewAppLogMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewAppLogMouseClicked

        numberEntriesSelector.setEnabled(true);
    }//GEN-LAST:event_viewAppLogMouseClicked

    private void viewDbLogMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewDbLogMouseClicked

        numberEntriesSelector.setEnabled(true);
    }//GEN-LAST:event_viewDbLogMouseClicked

    @SuppressWarnings("ManualArrayToCollectionCopy")
    private void viewLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewLogActionPerformed
        if(viewAppLog.isSelected())
        {
            logViewPane.setText("");
            logger.trace("Preparing to display the application logs.");
//            String logFile = VEPHelperMethods.getProperty("veplog.file", logger, "vep.properties");
            String logFile = VEPHelperMethods.getProperty("veplog.file", logger, vepProperties);
            try
            {
                BufferedReader in = new BufferedReader(new FileReader(logFile));
                String line = "";
                logViewPane.setText("");
                //read the first line to determine splits and ask the user for display options
                line = in.readLine();
                if(line != null)
                {
                    String[] parts = line.split(" - ");
                    String[] infoParts = parts[0].split("\\s+");
                    String options[] = new String[infoParts.length + 1];
                    for(int i=0; i<infoParts.length; i++)
                    {
                        options[i] = infoParts[i];
                        //System.err.println(infoParts[i]);
                    }
                    //System.err.println(parts[1]);
                    options[infoParts.length] = parts[1];
                    String message = "";
                    message += "Full log entry:\n" + line + "\n\n";
                    message += "Enter the field numbers (comma separated) you want to be displayed:\n\n";
                    for(int i=0; i<options.length; i++)
                        message += (i+1) + " : " + options[i] + "\n";
                    message += "\n";
                    
                    String str = JOptionPane.showInputDialog(mainPanel, message,  "", 1);
                    parts = str.split(",");
                    int indexes[] = new int[parts.length];
                    for(int i=0; i<parts.length; i++)
                    {
                        try
                        {
                            indexes[i] = Integer.parseInt(parts[i].trim());
                        }
                        catch(Exception ex)
                        {
                            indexes[i] = -1;
                            logger.warn("Invalid index detected while parsing user options for log display.");
                        }
                    }
                    //now get how many lines to show
                    String choice = numberEntriesSelector.getSelectedItem().toString();
                    int count = 0;
                    if(choice.contains("10 ")) count = 10;
                    else if(choice.contains("25 ")) count = 25;
                    else if(choice.contains("50 ")) count = 50;
                    else if(choice.contains("100 ")) count = 100;
                    else count = -1;
                    
                    customLineBuffer tempBuffer = new customLineBuffer(count);
                    
                    //now array indexes contains which parts to display
                    String out = "";
                    for(int i=0; i<indexes.length; i++)
                    {
                        if(indexes[i]>=1 && (indexes[i] - 1) < options.length)
                        {
                            //logViewPane.append(options[indexes[i] - 1]);
                            out += options[indexes[i] - 1];
                        }
                        if(i < (indexes.length - 1))
                        {
                            //logViewPane.append(" ");
                            out += "[@]"; //using [@] as separator
                        }
                        else
                        {
                            //logViewPane.append("\n");
                            tempBuffer.add(out);
                            out = "";
                        }
                    }
                    
                    //now proceeding in the same manner for rest of the log entries
                    while((line = in.readLine()) != null)
                    {
//                        System.out.println("Read log line: " + line);
                        parts = line.split(" - ");
                        infoParts = parts[0].split("\\s+");
                        options = new String[infoParts.length + 1];
                        for(int i=0; i<infoParts.length; i++)
                        {
                            options[i] = infoParts[i];
                        }
                        if(parts.length > 1)
                            options[infoParts.length] = parts[1];
                        for(int i=0; i<indexes.length; i++)
                        {
                            if(indexes[i]>=1 && (indexes[i] - 1) < options.length)
                            {
                                //logViewPane.append(options[indexes[i] - 1]);
                                out += options[indexes[i] - 1];
                            }
                            if(i < (indexes.length - 1))
                            {
                                //logViewPane.append(" ");
                                out += "[@]"; //using [@] as separator
                            }
                            else
                            {
                                //logViewPane.append("\n");
                                tempBuffer.add(out);
                                out = "";
                            }
                        }
                    }
                    in.close();
                    //now display the buffer
                    String[] toPrint = tempBuffer.getDump();
                    for(int i=0; i< toPrint.length; i++)
                    {
                        String val = toPrint[i];
                        val = val.replace("[@]", " ");
                        logViewPane.append(val + "\n");
                    }
                    
                    //trying to create a table view of the printed logs
                    String[] columnNames = new String[indexes.length];
                    for(int i=0; i<indexes.length; i++)
                    {
                        columnNames[i] = "Element " + indexes[i];
                    }
                    String[][] data = new String[toPrint.length][indexes.length];
                    for(int i=0; i< toPrint.length; i++)
                    {
                        String[] elements = toPrint[i].split("\\[@\\]");
                        for(int j=0; j<elements.length; j++)
                            data[i][j] = elements[j];
                    }
                    JTable table = new JTable(data, columnNames);
                    table.setAutoCreateRowSorter(true);
                    table.setGridColor(Color.WHITE);
                    logTablePane.removeAll();
                    logTablePane.setLayout(new BorderLayout());
                    logTablePane.add(table.getTableHeader(), BorderLayout.PAGE_START);
                    logTablePane.add(table, BorderLayout.CENTER);
                    logTablePane.getRootPane().revalidate();
                }
                else
                    in.close();
                
                logger.trace("Closed application log file. Displayed the contents.");
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught while processing application log file. " + ex.toString());
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        else if(viewDbLog.isSelected())
        {
            
        }
    }//GEN-LAST:event_viewLogActionPerformed

    private void testONEitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testONEitemActionPerformed
// TODO add your handling code here:
        String idValue = idVal.getText().trim();
        int id = -1;
        logViewPane.setText("");
        String choice = oneItemList.getSelectedItem().toString();
        //System.err.println("GOT: id=" + idValue + " selection: " + choice);
        try
        {
            id = Integer.parseInt(idValue);
        }
        catch(Exception ex)
        {
            logger.warn("Invalid ID value found. Check entry and try again.");
            id = -1;
        }
        
        if(id != -1)
        {
//            oneIP = VEPHelperMethods.getProperty("one.ip", logger, "vep.properties");
//            oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, "vep.properties");
//            oneUser = VEPHelperMethods.getProperty("one.user", logger, "vep.properties");
//            onePass = VEPHelperMethods.getProperty("one.pass", logger, "vep.properties");
            oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
            oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
            oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
            onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
            try
            {
                String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
                
                ONExmlrpcHandler temp = null;
                ONE3xmlrpcHandler temp3 = null;
                if(oneVersion.startsWith("2.2"))
                    temp = new ONExmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:testONEitemActionPerformed");
                else if(oneVersion.startsWith("3.4"))
                    temp3 = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneUser, onePass, "VEPView:testONEitemActionPerformed");
                
                if(choice.contains("VM"))
                {
                    String result = "";
                    if(temp != null)
                        result = temp.getVmInfoString(id);
                    else if(temp3 != null)
                        result = temp3.getVmInfoString(id);
                    
                    result.replaceAll("><", "> <");
                    String[] output = VEPHelperMethods.wrapText(result, 78, false);
                    for(int i=0; i<output.length; i++)
                        logViewPane.append(output[i] + "\n");
                }
                else if(choice.contains("Cluster"))
                {

                }
                else if(choice.contains("Image"))
                {

                }
                else if(choice.contains("Host"))
                {

                }
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught in testONEitemAction: " + ex.getMessage());
            }
        }
    }//GEN-LAST:event_testONEitemActionPerformed

    private void setAdminAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setAdminAccountActionPerformed
        //must precreate at least one admin
        if(!adminAuthenticated)
        {
            performAdminAuthentication();
        }
        if(adminAuthenticated)
        {
            try
            {
                boolean status = false;
                String passHash = VEPHelperMethods.makeSHA1Hash(String.valueOf(adminPass.getPassword()));
                String userName = adminUser.getText().trim();
                //now check if user already exists, then update it else insert an entry
                ResultSet rs = dbhandler.query("select", "*", "vepadmin", "where username='" + userName + "'");
                if(rs.next())
                {
                    rs.close();
                    Object[] options = {"Update", "Cancel"};
                    int n = JOptionPane.showOptionDialog(this.getRootPane(), "This admin account already exists!\nDo you wish to update the password?", "VEP-Admin Update Operation",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                            null,     //do not use a custom Icon
                            options,  //the titles of buttons
                            options[1]); //default button title
                    if(n == 0)
                    {
                        status = dbhandler.update("vepadmin", "password='" + passHash + "'", "where username='" + userName + "'");
                    }
                    else
                    {
                        logger.trace("Admin " + userName + " account already exists. Canceling the password update.");
                    }
                }
                else
                {
                    rs.close();
                    status = dbhandler.insert("vepadmin", "('" + userName + "', '" + passHash + "')");
                }
                if(status)
                {
                    logger.debug("VEP admin account operation for user: " + userName + " succeeded.");
                }
                else
                {
                    logger.debug("VEP admin account operation for user: " + userName + " was either cancelled or failed.");
                }
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught while trying to add an admin account.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        else
        {
            //revert the state of the toggle switch
            logger.debug("Admin access control prevents this action from being performed.");
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("You do not have proper authorization.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(mainPanel, "Access Denied");
            errorMessage.setVisible(true);
        }
    }//GEN-LAST:event_setAdminAccountActionPerformed
    
    private void initializeDb()
    {
        if(dbHandle != null)
        {
            try
            {
                logger.trace("Initiating database (re)initialization process, deleteing any existing schema.");
                Statement stat = dbHandle.createStatement();
                stat.executeUpdate("drop table if exists user;");
                stat.executeUpdate("drop table if exists ugroup;"); 
                stat.executeUpdate("drop table if exists vorg;");
                stat.executeUpdate("drop table if exists datacenter;");
                stat.executeUpdate("drop table if exists cluster;");
                stat.executeUpdate("drop table if exists rack;");
                stat.executeUpdate("drop table if exists computenode;");
                stat.executeUpdate("drop table if exists vmachinetemplate;");
                stat.executeUpdate("drop table if exists vmachine;");
                stat.executeUpdate("drop table if exists vnet;");
                stat.executeUpdate("drop table if exists storage;");
                stat.executeUpdate("drop table if exists clmanager;");
                stat.executeUpdate("drop table if exists ipaddress;");
                stat.executeUpdate("drop table if exists history;");
                stat.executeUpdate("drop table if exists ovf;");
                stat.executeUpdate("drop table if exists ceeobj;");
                stat.executeUpdate("drop table if exists diskimage;");
                stat.executeUpdate("drop table if exists deployment;");
                stat.executeUpdate("drop table if exists ovfvmmap;");
                stat.executeUpdate("drop table if exists vepadmin;");
                
                logger.trace("Creating VEP DB schema again.");
                
                String createTable = "create table vepadmin "
                        + "(username varchar(40) NOT NULL, " 
                        + "password varchar(45) NOT NULL, "
                        + "PRIMARY KEY (username))";
                stat.executeUpdate(createTable);
                logger.trace("VEP table vepadmin created successfully.");
                
                createTable = "create table user "
                        + "(username varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "vid varchar(20) NOT NULL, "
                        + "oneuser varchar(45) NOT NULL, "
                        + "onepass varchar(45) NOT NULL, "
                        + "oneid integer NOT NULL, "
                        + "role varchar(20), PRIMARY KEY (username, vid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table user created successfully.");
                
                createTable = "create table ugroup "
                        + "(gname varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ugroup created successfully.");
                
                createTable = "create table vorg "
                        + "(vid varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "name varchar(40), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (vid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vorg created successfully.");
                
                //for datacenter entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table datacenter "
                        + "(did integer NOT NULL, " 
                        + "loc varchar(2) NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (did))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table datacenter created successfully.");
                
                //for cluster entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table cluster "
                        + "(clid integer NOT NULL, " 
                        + "nicid varchar(3), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "name varchar(40), "
                        + "PRIMARY KEY (clid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table cluster created successfully.");
                
                //for rack entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table rack "
                        + "(rid integer NOT NULL, " 
                        + "clid integer NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (rid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table rack created successfully.");
                
                //for computenode entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table computenode "
                        + "(cid integer NOT NULL, " 
                        + "mem integer(10) NOT NULL, "
                        + "cpu integer(5) NOT NULL, "
                        + "cpufreq integer(10), "
                        + "cpuarch varchar(40), "
                        + "rid integer, "
                        + "virt varchar(20) NOT NULL, "
                        + "hostname varchar(256) NOT NULL, "
                        + "clmgrtype varchar(256) NOT NULL, "
                        + "vid varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table computenode created successfully.");
                
                createTable = "create table vmachinetemplate "
                        + "(vmid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "appname varchar(128), "
                        + "nid varchar(40), "
                        + "ip varchar(15), "
                        + "state varchar(2), "
                        + "ovfsno integer, "
                        + "ovfid varchar(40), "
                        + "ceeid integer, "
                        + "descp varchar(2048), " //changed desc to descp
                        + "PRIMARY KEY (vmid, nid, ip, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachinetemplate created successfully.");
                
                createTable = "create table vmachine "
                        + "(id integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "controller varchar(45), "
                        + "onevmid integer, "
                        + "vmid integer, "
                        + "name varchar(128), "
                        + "state varchar(2), "
                        + "cid integer NOT NULL, "
                        + "ipaddress varchar(128) NOT NULL, "
                        + "vncport varchar(10), "
                        + "vncip varchar(15), "
                        + "fqdn varchar(128), "
                        + "PRIMARY KEY (id))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachine created successfully.");
                
                //for vnet entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table vnet "
                        + "(nid varchar(40) NOT NULL, " 
                        + "iprange varchar(256) NOT NULL, " //changed range to iprange
                        + "ispublic varchar(1) NOT NULL, "
                        + "vid varchar(40), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (nid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vnet created successfully.");
                
                //for storage entry, assume default access rights, admins can create, modify, others can only access
                createTable = "create table storage "
                        + "(sid varchar(40) NOT NULL, " 
                        + "type varchar(3) NOT NULL, "
                        + "vid varchar(40), "
                        + "size integer(10), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (sid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table storage created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table clmanager "
                        + "(cid integer NOT NULL, " 
                        + "hostid varchar(40) NOT NULL, "
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table clmanager created successfully.");
                
                createTable = "create table ipaddress "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "nid varchar(40) NOT NULL, "
                        + "address varchar(15) NOT NULL, "
                        + "isassigned varchar(1) NOT NULL, "
                        + "vmid integer)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ipaddress created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table history "
                        + "(time varchar(40) NOT NULL, "
                        + "username varchar(40), "
                        + "code varchar(4), "
                        + "sqlstring varchar(1024), "
                        + "descp varchar(1024))"; //changed desc to descp
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table history created successfully.");
                
                createTable = "create table ovf "
                        + "(ovfname varchar(40) NOT NULL, " 
                        + "sno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "state varchar(2), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "descp text(50240), " //changed desc to descp
                        + "PRIMARY KEY (ovfname, uid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovf created successfully.");
                
                createTable = "create table ceeobj "
                        + "(ceeid integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "slaobj BLOB, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (ceeid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ceeobj created successfully.");
                
                createTable = "create table diskimage "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "gafslink varchar(1024), "
                        + "name varchar(1024), "
                        + "oneimgname varchar(128), "
                        + "oneimgid integer, "
                        + "vmid integer, "
                        + "state varchar(2), "
                        + "oneimgdescp varchar(1024), "
                        + "localpath varchar(1024))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table diskimage created successfully.");
                
                createTable = "create table deployment "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "ovfsno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "content varchar(10240))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table deployment created successfully.");
                
                createTable = "create table ovfvmmap "
                        + "(ovfsno integer NOT NULL, " 
                        + "ovfid varchar(40) NOT NULL, "
                        + "vmid integer, PRIMARY KEY (ovfsno, ovfid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovfvmmap created successfully.");
                
                dbStatus.setBackground(Color.green);
                if(dbType.contains("sqlite"))
                    dbStatus.setToolTipText("Sqlite DB connection is open.");
                else
                    dbStatus.setToolTipText("Mysql DB connection is open.");
                dbActionSelector.setEnabled(true);
                logger.trace("Database (re)initialization process successful, enabling database panel actions.");
                wasDbInitialized = true;
            }
            catch(SQLException ex)
            {
                logger.error("Database (re)initialization process failed.");
                if(logger.isDebugEnabled())
                    ex.printStackTrace(System.err);
                else
                    logger.warn(ex.getMessage());
            }
        }
        else
        {
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("DB Initialization Failed!\nTry restarting the software.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(mainPanel, "Error");
            logger.trace("Database handler is not valid, initialization process was stopped. Check system parameters and restart the software.");
            errorMessage.setVisible(true);
        }
    }
    
    private void testDb()
    {
        try
        {   
            logger.trace("Running VEP database sanity checks (primitive checks) now.");
            if(dbHandle != null)
            {
                DatabaseMetaData dbm = dbHandle.getMetaData();
                String[] types = {"TABLE"};
                ResultSet rs = dbm.getTables(null,null,"%",types);
                int count=0;
                while (rs.next())
                {
                    count++;
                    String table = rs.getString("TABLE_NAME");
                }
                if(count < 20)
                {
                    logger.warn("VEP database file failed sanity checks. Asking user for reinitialization permission.");
                    dbStatus.setBackground(Color.orange);
                    dbActionSelector.setEnabled(false);
                    dbStatus.setToolTipText("Incorrect DB structure.");
                    Object[] options = {"Initialize", "Don't Initialize"};
                    int n = JOptionPane.showOptionDialog(mainPanel, "VEP Database internal structure seems incorrect.\n"
                        + "This message could be displayed in case the VEP db is not initialized.\n"
                        + "I can re-initialize the database to an empty db.\n"
                        + "Do you wish to re-initialize to fix errors?", "VEP DB Initialization",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null,     //do not use a custom Icon
                        options,  //the titles of buttons
                        options[1]); //default button title
                    if(n == 0)
                    {
                        logger.trace("Initiating database reinitialization process.");
                        //call initialize database function here
                        initializeDb();
                    }
                    else
                    {
                        //if(dbHandle != null) dbHandle.close();
                        //dbHandle = null;
                        JDialog infoMessage;
                        JOptionPane infoMess = new JOptionPane("VEP DB functionality will be disabled..", JOptionPane.INFORMATION_MESSAGE);
                        infoMessage = infoMess.createDialog(mainPanel, "Warning");
                        logger.warn("VEP database reinitialization permit denied. VEP database actions will be disabled.");
                        infoMessage.setVisible(true);
                    }
                }
                else
                {
                    logger.debug("VEP database sanity checks (primitive checks) were completed successfully.");
                    dbActionSelector.setEnabled(true);
                    logger.trace("VEP database actions are now enabled.");
                    //now test for table schema
//                    try
//                    {
//                        //if(dbhandler == null) dbhandler = new dbHandler("VEP Main");
//                        rs = dbhandler.query("SELECT", "did, name, loc, desc", "datacenter", "");
//                        rs = dbhandler.query("SELECT", "rid, clid, name, desc", "rack", "");
//                        rs = dbhandler.query("SELECT", "username, uid, vid, role", "user", "");
//                        rs = dbhandler.query("SELECT", "clid, nicid, did, desc, name", "cluster", "");
//                        rs = dbhandler.query("SELECT", "gname, uid", "ugroup", "");
//                        rs = dbhandler.query("SELECT", "cid, mem, no_cpu_cores, rid, virt, hostname, clmgrtype, vid, desc, cpuarch", "computenode", "");
//                        rs = dbhandler.query("SELECT", "cid, hostid", "clmanager", "");
//                        dbActionSelector.setEnabled(true);
//                    }
//                    catch(Exception ex)
//                    {
//                        ex.printStackTrace(System.out);
//                        dbStatus.setBackground(Color.orange);
//                        dbActionSelector.setEnabled(false);
//                        dbStatus.setToolTipText("Incorrect DB structure.");
//                        Object[] options = {"Initialize", "Don't Initialize"};
//                        int n = JOptionPane.showOptionDialog(mainPanel, "VEP Database internal structure seems incorrect.\n"
//                            + "This message could be displayed in case the VEP db is not initialized.\n"
//                            + "I can re-initialize the database to an empty db.\n"
//                            + "Do you wish to re-initialize to fix errors?", "VEP DB Initialization",
//                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
//                            null,     //do not use a custom Icon
//                            options,  //the titles of buttons
//                            options[1]); //default button title
//                        if(n == 0)
//                        {
//                            //call initialize database function here
//                            initializeDb();
//                        }
//                        else
//                        {
//                            //if(dbHandle != null) dbHandle.close();
//                            //dbHandle = null;
//                            JDialog infoMessage;
//                            JOptionPane infoMess = new JOptionPane("VEP DB functionality will be disabled..", JOptionPane.INFORMATION_MESSAGE);
//                            infoMessage = infoMess.createDialog(mainPanel, "Warning");
//                            infoMessage.setVisible(true);
//                        }
//                    }
                }
            }
            else
            {
                JDialog errorMessage;
                JOptionPane errMess = new JOptionPane("VEP database handle creation failed.\nCheck system properties and provide a valid VEP db file.", JOptionPane.ERROR_MESSAGE);
                errorMessage = errMess.createDialog(mainPanel, "Error");
                logger.warn("VEP database handler object is not valid. Terminating database sanity checks. Check system properties and try again.");
                errorMessage.setVisible(true);
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught during database sanity checks.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
    @SuppressWarnings("SleepWhileInLoop")
    private void cleanUp()
    {
        //TODO , add cleanup code here ... database connection close etc etc.
        logger.info("Initiating system housekeeping before termination.");
        File bkup = new File("tmp.xsd");
        if(bkup.exists())
        {
            bkup.delete();
            logger.info("DMTF OVF schema file cache cleanup successful");
        }
        //now terminating all periodic threads
        logger.info("Trying to end background processes gracefully.");
        while(pThreads.size() != 0)
        {
            PeriodicThread temp = pThreads.pop();
            temp.terminate();
            while(temp.isRunning())
            {
                try
                {
                    Thread.sleep(500);
                }
                catch(Exception ex)
                {
                    
                }
            }
        }
        cliserver.stopCLIserver();
        if(chartUpdater != null) chartUpdater.stop();
        if(dbHandle != null)
            try 
            {
                dbHandle.close();
                logger.info("Database handle closed properly.");
            } 
            catch (SQLException ex) 
            {
                logger.warn("Exception caught while closing database handle.");
                //Logger.getLogger(VEPView.class.getName()).log(Level.SEVERE, null, ex);
            }
        logger.info("All background processes terminated, terminating VEP software.");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ONELabel;
    private javax.swing.JPanel ONEPanel;
    private javax.swing.JButton OVFselector;
    private javax.swing.JLabel REPORTINGLabel;
    private javax.swing.JLabel RESTLabel;
    private javax.swing.JPanel RESTPanel;
    private javax.swing.JPanel VMPanel;
    private javax.swing.JRadioButton addNode;
    private javax.swing.JLabel adminMessage;
    private javax.swing.JPasswordField adminPass;
    private javax.swing.JTextField adminUser;
    private javax.swing.JPanel adminUserPanel;
    private javax.swing.JLabel authState;
    private javax.swing.JLabel authState1;
    private javax.swing.JLabel authWarning;
    private javax.swing.JButton changeLogLevel;
    private javax.swing.JInternalFrame chartFrame;
    private javax.swing.JComboBox chartSelector;
    private javax.swing.JCheckBox clientAuthSelected;
    private javax.swing.JPanel clusterMgmtPane;
    private javax.swing.JPanel clusterPanel;
    private javax.swing.JLabel contrailLogoLabel;
    private javax.swing.JTabbedPane controlPanel;
    private javax.swing.JComboBox dbActionSelector;
    private javax.swing.JLabel dbAuthState;
    private javax.swing.JLabel dbConnectionLabel;
    private javax.swing.JPanel dbContentPane;
    private javax.swing.JPanel dbPanel;
    private javax.swing.JLabel dbPanelLabel1;
    private javax.swing.JScrollPane dbScrollPane;
    private javax.swing.JPanel dbStatus;
    private javax.swing.JRadioButton deleteNode;
    private javax.swing.JPanel diagnosticPanel;
    private javax.swing.JRadioButton editNode;
    private javax.swing.JProgressBar eventBar;
    private javax.swing.JRadioButton federationAdd;
    private javax.swing.JLabel hostInfo;
    private javax.swing.JTextField idVal;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea logArea;
    private javax.swing.JComboBox logLevelChoice;
    private javax.swing.JPanel logPanel;
    private javax.swing.JScrollPane logScrollPane;
    private javax.swing.JPanel logTablePane;
    private javax.swing.JTextArea logViewPane;
    private javax.swing.JPanel loggingPanel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton nodeAction;
    private javax.swing.JScrollPane nodeInfoPane;
    private javax.swing.JComboBox nodeList;
    private javax.swing.JLabel nodeListLabel;
    private javax.swing.JScrollPane nodeListPane;
    private javax.swing.JComboBox numberEntriesSelector;
    private javax.swing.JComboBox oneItemList;
    private javax.swing.JPanel oneStatus;
    private javax.swing.JButton ovfDeploy;
    private javax.swing.JTextField ovfFileName;
    private javax.swing.JLabel plotTypeLabel;
    private javax.swing.JMenu preferenceMenu;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel progressbarLabel;
    private javax.swing.JTextField reportPeriod;
    private javax.swing.JPanel reportingPanel;
    private javax.swing.JPanel reportingServiceStatus;
    private javax.swing.JToggleButton reportingSwitch;
    private javax.swing.JTextField restHTTPPort;
    private javax.swing.JTextField restHTTPSPort;
    private javax.swing.JPanel restStatus;
    private javax.swing.JToggleButton restSwitch;
    private javax.swing.JCheckBox restSystemServiceCheck;
    private javax.swing.JPanel serverPanel;
    private javax.swing.JButton setAdminAccount;
    private javax.swing.JMenuItem setPreferences;
    private javax.swing.JMenuItem settingsMenu;
    private javax.swing.JButton slaChooser;
    private javax.swing.JTextField slaObjectName;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPane;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JButton testONEitem;
    private javax.swing.JRadioButton viewAppLog;
    private javax.swing.JRadioButton viewDbLog;
    private javax.swing.JButton viewLog;
    private javax.swing.JToggleButton xmlrpcSwitch;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private boolean adminAuthenticated;
    private JDialog aboutBox;
    private restServer rServer;
    private int restPortValue;
    private int restHTTPSPortValue;
    private settingsForm settings;
    private String oneIP;
    private String oneXmlPort;
    private String oneUser;
    private String onePass;
    private ovfValidator xmlHandle;
    private LinkedList<PeriodicThread> pThreads;
    private ButtonGroup nodeActions;
    private ButtonGroup logViewActions;
    private ONExmlrpcHandler oneHandle;
    private ONE3xmlrpcHandler one3Handle;
    private VEPHost vepHost;
    private Connection dbHandle;
    private dbHandler dbhandler;
    private VEPComputeNode vepCNode;
    private Logger rootlogger;
    private Logger logger;
    private String dbType;
    private boolean wasDbInitialized;
    private CLIServer cliserver;
    private VEPHelperMethods helperMethods;
    private SSLCertHandler certHandler;
    private ClusterStats runningStat;
    private ChartUpdater chartUpdater;
    private VEPAccessControl vepAccessControl;
    private VEPHostPool hostPool;
    private DirtyFlag dirtyFlag; //used to track configuration file changes
}
