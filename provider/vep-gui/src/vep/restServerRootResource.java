/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;

/**
 *
 * @author piyush
 */
public class restServerRootResource extends ServerResource 
{ 
//    @Get
//    public String display() 
//    {
//        String message = "Welcome to Contrail VEP REST Interface.\n";
//        message += "Try using Content-Type:application/json or Content-Type:text/html for a more structured rendering.\n\n";
//        message += "Below are the list of top level resource URLs:\n";
//        message += "Virtual Organization: /vo/\n";
//        message += "Network Resources: /network/\n";
//        message += "List of Hosts: /host/\n";
//        message += "Registered OVFs: /ovf/\n";
//        message += "Virtual Machines: /vm/\n";
//        message += "Storage: /storage/\n";
//        message += "Users: /user/\n";
//        return message;  
//    } 
    @Get("json")
    public Representation getValue() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml();
            else if(acceptType.contains("json"))
                response = toJson();
        }
        else
        {
            //default rendering ...
            response = toHtml();
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toHtml() throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true));
        stringBuilder.append("List of available resources<br>");
        stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
        //stringBuilder.append("<li>Virtual Organization <a href='vo/' title='to be implemented soon'>/vo/</a>");
        //stringBuilder.append("<li>Network Resources <a href='network/' title='to be implemented soon'>/network/</a>");
        stringBuilder.append("<li>List of Hosts <a href='host/' title='shows list of hosts at this provider, admin access controlled'>/host/</a>");
        stringBuilder.append("<li>Registered OVFs <a href='ovf/' title='list of OVFs accessible to a user'>/ovf/</a>");
        stringBuilder.append("<li>VM Templates <a href='template/' title='list of OVFs VM Templates accessible to a user'>/template/</a>");
        stringBuilder.append("<li>Virtual Machines <a href='vm/' title='filtered list of VMs, access controlled'>/vm/</a>");
        //stringBuilder.append("<li>Storage <a href='storage/' title='to be implemented soon'>/storage/</a>");
        stringBuilder.append("<li>Users <a href='user/' title='filtered list of users, access controlled'>/user/</a>");
        stringBuilder.append("</ul>");
        stringBuilder.append("Some of the resource access may need authentication. The site will prompt you for your credentials, please enter your valid Contrail credentials.<br><br>");
        stringBuilder.append("<table style='border:0px;background:#DCDFF2;font-family:Verdana;font-size:10pt;'>");
        stringBuilder.append("<tr><td><a href='appcontrol/'><img src='https://www.cise.ufl.edu/~pharsh/public/manage.png' width='64'></a><td>");
        stringBuilder.append("<td valign='center'>If you are looking for application management page, click the manage icon on the left. <i>Make sure your browser is properly configured to use your client certificate</i>.</td></tr></table>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
    
    public Representation toJson() throws ResourceException
    {
        JSONObject obj = new JSONObject();
        obj.put("title", "Contrail VEP REST Interface");
        JSONArray arr = new JSONArray();
        //arr.add("Virtual Organization");
        //arr.add("Network Resources");
        arr.add("Hosts");
        arr.add("Registered OVFs");
        arr.add("VM Templates");
        arr.add("Virtual Machines");
        //arr.add("Storage");
        arr.add("Users");
        obj.put("target names", arr);
        JSONArray link = new JSONArray();
        //link.add("/vo/");
        //link.add("/network/");
        link.add("/host/");
        link.add("/ovf/");
        link.add("/template/");
        link.add("/vm/");
        //link.add("/storage/");
        link.add("/user/");
        obj.put("links", link);
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
}
