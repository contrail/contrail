/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * userManagement.java
 *
 * Created on Aug 22, 2011, 4:41:54 PM
 */
package vep;

import java.awt.Dialog;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Random;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import org.apache.log4j.Logger;
import org.apache.commons.lang3.RandomStringUtils;
import eu.contrail.security.SecurityUtils;
import eu.contrail.security.DelegatedCertClient;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.X509Certificate;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
/**
 *
 * @author piyush
 */
public class userManagement extends javax.swing.JPanel {

    /** Creates new form userManagement */
    private dbHandler handle;
    private JTextArea logArea;
    private Logger logger;
    private boolean systemOut;
    private ONExmlrpcHandler oneHandle;
    private ONE3xmlrpcHandler one3Handle;
    private String oneIP;
    private String oneXmlPort;
    private String oneAdmin;
    private String oneAdminPass;
    private Random rand;
    private String vepProperties;
    
    public userManagement() {
        initComponents();
        systemOut = false;
        rand = new Random(100);
        logger = Logger.getLogger("VEP.userManagement");
    }
    
    public userManagement(dbHandler handler, JTextArea log, String vepProp) 
    {
        initComponents();
        systemOut = false;
        logger = Logger.getLogger("VEP.userManagement");
        logger.trace("Constructing DB interface panel for user management.");
        if(systemOut)
            System.out.println("Customized constructor in usermanagement.java");
        handle = handler;
        vepProperties = vepProp;
        logArea = log;
        try
        {
            //populate the choice list with list of usernames
            //userList.removeAll();
            ResultSet rs = handle.query("SELECT", "username", "user", "");
            userList.removeAllItems();
            int count = 0;
            while(rs.next())
            {
                if(count == 0)
                    userList.addItem("Select an user");
                String name = rs.getString(1);
                if(systemOut)
                System.out.println("Found username: " + name);
                logger.trace("Composing userlist: found user " + name);
                userList.addItem(name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No username was found.");
                logger.trace("Composing userlist: no user was found.");
                userList.addItem("No user found");
            }
            rs = handle.query("select DISTINCT", "vid, descp", "vorg", "");
            vidList.removeAllItems();
            count = 0;
            while(rs.next())
            {
                if(count == 0)
                    vidList.addItem("No vid selected");
                String vid = rs.getString("vid");
                String desc = rs.getString("descp");
                if(systemOut)
                System.out.println("Found vid: " + vid + " Description: " + desc);
                logger.trace("Composing vorg list: found vid " + vid + ", description: " + desc);
                vidList.addItem(vid);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No vid was found.");
                logger.trace("Composing vorg list: no vorg was found.");
                vidList.addItem("No vid selected");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
        Properties props = new Properties();
        try
        {
            oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
            oneXmlPort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
            oneAdmin = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
            oneAdminPass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
        }
        catch(Exception ex)
        {
            logger.error("vep.properties file not found. OpenNebula XML-RPC connection could not be completed.");
            oneIP = null;
            oneXmlPort = null;
            oneAdmin = null;
            oneAdminPass = null;
            if(systemOut)
            System.out.println("Unable to open properties file." + ex);
            JDialog errorMessage;
            JOptionPane errMess = new JOptionPane("<html>ONE XMLRPC parameters are not set correctly.<br>Go to <b>Edit -> Settings</b> and set parameters and try again.</html>", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(this.getRootPane(), "Action can not be completed");
            errorMessage.setVisible(true);
            return;
        }
        if(oneIP!=null && oneXmlPort!=null && oneAdmin!=null && oneAdminPass!=null)
        {
            String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
            oneHandle = null;
            one3Handle = null;
            if(oneVersion.startsWith("2.2"))
                oneHandle = new ONExmlrpcHandler(oneIP, oneXmlPort, oneAdmin, oneAdminPass, "userManagement:constructor");
            else if(oneVersion.startsWith("3.4"))
                one3Handle = new ONE3xmlrpcHandler(oneIP, oneXmlPort, oneAdmin, oneAdminPass, "userManagement:constructor");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        username = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        vidList = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        groupId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        groupsList = new javax.swing.JTextArea();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        userRole = new javax.swing.JTextField();
        userList = new javax.swing.JComboBox();
        refreshButton = new javax.swing.JButton();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vep.VEPApp.class).getContext().getResourceMap(userManagement.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setName("Form"); // NOI18N
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, 190, -1));

        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 20, 80, -1));

        username.setText(resourceMap.getString("username.text")); // NOI18N
        username.setName("username"); // NOI18N
        jPanel1.add(username, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 15, 230, -1));

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 51, 140, -1));

        vidList.setFont(resourceMap.getFont("vidList.font")); // NOI18N
        vidList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No vid selected" }));
        vidList.setName("vidList"); // NOI18N
        jPanel1.add(vidList, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 160, 20));

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 83, 120, -1));

        groupId.setText(resourceMap.getString("groupId.text")); // NOI18N
        groupId.setName("groupId"); // NOI18N
        jPanel1.add(groupId, new org.netbeans.lib.awtextra.AbsoluteConstraints(153, 78, 180, -1));

        jLabel5.setFont(resourceMap.getFont("jLabel5.font")); // NOI18N
        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 148, 120, -1));

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        groupsList.setColumns(20);
        groupsList.setLineWrap(true);
        groupsList.setRows(5);
        groupsList.setToolTipText(resourceMap.getString("groupsList.toolTipText")); // NOI18N
        groupsList.setWrapStyleWord(true);
        groupsList.setName("groupsList"); // NOI18N
        jScrollPane2.setViewportView(groupsList);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 330, 90));

        updateButton.setFont(resourceMap.getFont("updateButton.font")); // NOI18N
        updateButton.setText(resourceMap.getString("updateButton.text")); // NOI18N
        updateButton.setName("updateButton"); // NOI18N
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        jPanel1.add(updateButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 110, -1));

        deleteButton.setFont(resourceMap.getFont("deleteButton.font")); // NOI18N
        deleteButton.setText(resourceMap.getString("deleteButton.text")); // NOI18N
        deleteButton.setName("deleteButton"); // NOI18N
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel1.add(deleteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 270, 110, -1));

        addButton.setFont(resourceMap.getFont("addButton.font")); // NOI18N
        addButton.setText(resourceMap.getString("addButton.text")); // NOI18N
        addButton.setName("addButton"); // NOI18N
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jPanel1.add(addButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 270, 90, -1));

        jLabel6.setFont(resourceMap.getFont("jLabel6.font")); // NOI18N
        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 120, 50, -1));

        userRole.setText(resourceMap.getString("userRole.text")); // NOI18N
        userRole.setName("userRole"); // NOI18N
        jPanel1.add(userRole, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 115, 260, -1));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 350, 310));

        userList.setFont(resourceMap.getFont("userList.font")); // NOI18N
        userList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No user found" }));
        userList.setToolTipText(resourceMap.getString("userList.toolTipText")); // NOI18N
        userList.setBorder(null);
        userList.setName("userList"); // NOI18N
        userList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userListActionPerformed(evt);
            }
        });
        add(userList, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 140, 20));

        refreshButton.setFont(resourceMap.getFont("refreshButton.font")); // NOI18N
        refreshButton.setText(resourceMap.getString("refreshButton.text")); // NOI18N
        refreshButton.setName("refreshButton"); // NOI18N
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        add(refreshButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 100, -1));
    }// </editor-fold>//GEN-END:initComponents

private void userListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userListActionPerformed
// TODO add your handling code here:
    Object selectedItem = userList.getSelectedItem();
    String name = null;
    if(selectedItem != null)
        name = selectedItem.toString();
    if(name != null && (name.contains("Select an") || name.contains("No user")))
    {
        //do nothing
        username.setText("");
        username.setEditable(true);
        groupId.setText("");
        userRole.setText("");
        groupsList.setText("");
        deleteButton.setEnabled(false);
        updateButton.setEnabled(false);
        addButton.setEnabled(true);
    }
    else if(name != null && !(name.contains("Select an") || name.contains("No user")))
    {
        //populate the fields with selected user data
        try
        {
            ResultSet rs = handle.query("select", "*", "user", "where username='" + name + "'");
            if(rs.next())
            {
                username.setText(rs.getString("username"));
                username.setEditable(false);
                String vid = rs.getString("vid");
                if(vid.contains("-1"))
                    vidList.setSelectedItem("No vid selected");
                else
                {
                    int foundIndex = -1;
                    for(int i=0; i<vidList.getItemCount(); i++)
                    {
                        if(vidList.getItemAt(i).toString().compareToIgnoreCase(vid) == 0) foundIndex = i;
                    }
                    if(foundIndex != -1) vidList.setSelectedIndex(foundIndex);
                    else vidList.setSelectedItem("No vid selected");
                }
                userRole.setText(rs.getString("role"));
            }
            int uid = rs.getInt("uid");
            //find all the groups where this user is a member of
            rs = handle.query("select", "*", "ugroup", "where uid=" + uid);
            int count = 0;
            groupsList.setText("");
            while(rs.next())
            {
                if(count == 0) groupId.setText(rs.getString("gname"));
                else
                {
                    if(count == 1)
                        groupsList.append(rs.getString("gname"));
                    else
                        groupsList.append(", " + rs.getString("gname"));
                }
                count++;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
        deleteButton.setEnabled(true);
        updateButton.setEnabled(true);
        addButton.setEnabled(false);
    }
}//GEN-LAST:event_userListActionPerformed

private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
// TODO add your handling code here:
    try
        {
            //populate the choice list with list of usernames
            //userList.removeAll();
            ResultSet rs = handle.query("SELECT", "username", "user", "");
            userList.removeAllItems();
            int count = 0;
            while(rs.next())
            {
                if(count == 0)
                    userList.addItem("Select an user");
                String name = rs.getString(1);
                if(systemOut)
                System.out.println("Found username: " + name);
                logger.trace("Composing user list: found user " + name);
                userList.addItem(name);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No username was found.");
                logger.trace("Composing user list: no user was found.");
                userList.addItem("No user found");
            }
            rs = handle.query("select DISTINCT", "vid, descp", "vorg", "");
            vidList.removeAllItems();
            count = 0;
            while(rs.next())
            {
                if(count == 0)
                    vidList.addItem("No vid selected");
                String vid = rs.getString("vid");
                String desc = rs.getString("descp");
                if(systemOut)
                System.out.println("Found vid: " + vid + " Description: " + desc);
                logger.trace("Composing vorg list: found vid " + vid + ", description: " + desc);
                vidList.addItem(vid);
                count++;
            }
            if(count == 0)
            {
                if(systemOut)
                System.out.println("No vid was found.");
                logger.trace("Composing vorg list: no vorg was found.");
                vidList.addItem("No vid selected");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace(System.out);
        }
}//GEN-LAST:event_refreshButtonActionPerformed

private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
// TODO add your handling code here:
    //remove the username from user table and remove all groups in ugroup table
    Object[] options = {"Delete", "Don't Delete"};
    int n = JOptionPane.showOptionDialog(this.getRootPane(), "Do you wish to delete user " + username.getText() + "'s records?", "User Account Delete Operation",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
            null,     //do not use a custom Icon
            options,  //the titles of buttons
            options[1]); //default button title
    if(n == 0)
    {
        //user selected delete option
        try
        {
            ResultSet rs = handle.query("SELECT", "*", "user", "where username='" + username.getText() + "'");
            if(rs.next())
            {
                int uid = rs.getInt("uid");
                int oneid = rs.getInt("oneid");
                //first delete the opennebula corresponding user
                String name = rs.getString("username");
                //first check if running VMs are present
                rs = handle.query("select", "*", "vmachine", "where uid=" + uid + " and state <> 'FN'");
                if(rs.next())
                {
                    //One or more VMs are still not finished
                    Dialog errorMessage;
                    JOptionPane errMess = new JOptionPane("This user has VMs in unknown or running state.\nCan not delete this account now.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(this.getRootPane(), "Running VMs detected");
                    logger.warn("User delete request: running/unknown state VMs found.");
                    errorMessage.setVisible(true);
                } 
                else 
                {
                    //first get all vm ids from this user                         
                    //then delete all diskimages for these machines
                    rs=handle.query("select", "vmid", "vmachine", "where uid="+uid+" group by vmid");
                    int count = 0;
                    while(rs.next())
                    {
                        count++;
                        handle.delete("diskimage","where vmid="+rs.getInt("vmid"));
                        rs=handle.query("select", "vmid", "vmachine", "where uid="+uid+" group by vmid");
                        for(int i=0; i<count; i++) rs.next();
                    }
                    //then delete all vms;
                    handle.delete("vmachine", "where uid="+uid);
                    //then delete all vm templates
                    handle.delete("vmachinetemplate", "where uid="+uid);
                    //then delete all OVFs
                    handle.delete("ovf", "where uid="+uid);
                    
                    //first delete the opennebula corresponding user
                    boolean status = false;
                    if(oneHandle != null)
                        status = oneHandle.removeUser(oneid);
                    else if(one3Handle != null)
                        status = one3Handle.removeUser(oneid);
                    if(status)
                        logger.debug("Successfully removed corresponding user from OpenNebula cloud.");
                    else
                        logger.warn("Error while deleting corresponding opennebula user with uid: " + oneid);

                    handle.delete("user", "where username='" + name + "'");
                    //now delete all group data
                    handle.delete("ugroup", "where uid=" + uid);
                    //now refresh the UI
                    username.setText("");
                    userRole.setText("");
                    vidList.setSelectedItem("No vid selected");
                    groupId.setText("");
                    groupsList.setText("");
                    refreshButtonActionPerformed(null);
                    logArea.append("User " + name + " was deleted successfully.\n");
                    logger.debug("User " + name + " was deleted successfully.");
                }
            }
        }
        catch(Exception ex)
        {
            logger.warn("Exception was caught while trying to delete user.");
            ex.printStackTrace(System.out);
        }
    }
}//GEN-LAST:event_deleteButtonActionPerformed

private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
// TODO add your handling code here:
    String vid = "-1";
    String group = groupId.getText().trim();
    //by default a user belongs to its own group
    String groupList = groupsList.getText().trim();
    String role = userRole.getText().trim();
    String uname = username.getText().trim();
    if(group.length() == 0) group = uname;
    int uid = -1;
    
    //if any field is empty the corresponding values will not be updated in DB
    if(!vidList.getSelectedItem().toString().contains("No vid selected"))
        vid = vidList.getSelectedItem().toString();
    //now validate all fields here
    boolean proceed = true;
    if(!role.isEmpty() && !dbHandler.validateSingleWord(role)) proceed = false;
    if(!dbHandler.validateSingleWord(group)) proceed = false;
    String delims = "[ ,\n]+";
    String[] glist = groupList.split(delims);
    for(int i=0; i<glist.length; i++)
        if(glist[i].trim().length() > 0)
            if(!dbHandler.validateSingleWord(glist[i].trim())) proceed = false;
    
    try
    {
        ResultSet rs = handle.query("SELECT", "*", "user", "where username='" + uname + "'");
        if(rs.next())
        {
            uid = rs.getInt("uid");
            String updateList = "";
            updateList = "vid='" + vid + "'";
            if(!role.isEmpty()) updateList += ",role='" + role + "'";
            if(proceed && handle.beginTransaction())
            {
                handle.update("user", updateList, "where username='" + uname + "'");
                //now update the groups list
                handle.delete("ugroup", "where uid=" + uid);
                handle.insert("ugroup", "('" + uname + "'," + uid + ")");
                //now that self group has been inserted, insert entries for rest groups if they already do not exist
                rs = handle.query("SELECT", "*", "ugroup", "where gname='" + group + "' AND uid=" + uid);
                if(!rs.next())
                {
                    //insert the group
                    handle.insert("ugroup", "('" + group + "'," + uid + ")");
                }
                for(int i=0; i<glist.length; i++)
                {
                    if(glist[i].trim().length() > 0)
                    {
                        rs = handle.query("SELECT", "*", "ugroup", "where gname='" + glist[i].trim() + "' AND uid=" + uid);
                        if(!rs.next())
                        {
                            //insert the group
                            handle.insert("ugroup", "('" + glist[i].trim() + "'," + uid + ")");
                        }
                    }
                }
                if(handle.commitTransaction())
                {
                    if(systemOut)
                    System.out.println("user updated successfully ...");
                    logger.debug("User " + uname + " was updated successfully.");
                    logArea.append("User " + uname + " was updated successfully.\n");
                }
                else
                {
                    handle.endTransaction();
                    logger.warn("User " + uname + " update request was not completed properly. Transaction ended.");
                    logArea.append("User " + uname + " update request was not completed properly. Transaction ended.\n");
                }
            }
            else
            {
                if(!proceed)
                {
                    Dialog errorMessage;
                    JOptionPane errMess = new JOptionPane("Some fields are not valid, check for ', \", SPACE and try again.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(this.getRootPane(), "Invalid format");
                    logger.warn("User update request: some fields are not valid, check for ', \", SPACE and try again.");
                    errorMessage.setVisible(true);
                }
                else
                {
                    if(systemOut)
                    System.out.println("Could not begin transaction: userManagement.java");
                    logger.error("Could not begin transaction for user update process.");
                }
            }
        }
    }
    catch(Exception ex)
    {
        ex.printStackTrace(System.out);
    }
}//GEN-LAST:event_updateButtonActionPerformed

private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
// TODO add your handling code here:
    String uname = username.getText().trim();
    boolean isCATest = false;
    ////////////////////////test code for CA certificate
    if(isCATest)
    {
        try
        {
            Security.addProvider(new BouncyCastleProvider());
            String uriSpec = "https://one-test.contrail.rl.ac.uk:8443/ca/delegateduser";
            KeyPair keyPair = SecurityUtils.generateKeyPair("RSA", 2048);
            String signatureAlgorithm = "SHA256withRSA";
            /*
            * Input is userID (currently input parameter is not UUID)
            */    
            String userID = "1"; //this is the federation used id - UUID 
            String keystorePass = "client";
            DelegatedCertClient client = new DelegatedCertClient(uriSpec, true, "/home/piyush/Desktop/security-commons/keystore.p12",  keystorePass, "/home/piyush/Desktop/security-commons/ca-trusted-cert.jks");
            X509Certificate result = client.getCert(keyPair, signatureAlgorithm, userID, true);
            String certName = result.getSubjectX500Principal().getName();
            logger.info("Received a certificate with name: " + certName);
            String[] certParts = certName.split(",");
            String CN = "";
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    CN = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("Certificate CN field : " + CN);
            logger.debug("Certificate creation was successful!");
        }
        catch(Exception ex)
        {
            logger.warn("Error Caught while attempting to generate the user certificate!");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
        }
        return;
    }
    ////////////////////////test code ends
    try
    {
        ResultSet rs = handle.query("SELECT", "*", "user", "where username='" + uname + "'");
        if(rs.next())
        {
            Dialog errorMessage;
            JOptionPane errMess = new JOptionPane("Username already exist, choose a different username.", JOptionPane.ERROR_MESSAGE);
            errorMessage = errMess.createDialog(this.getRootPane(), "Username exists");
            logger.warn("User add - username " + uname + " already exists. Try another username.");
            errorMessage.setVisible(true);
        }
        else
        {
            boolean proceed = true;
            if(!dbHandler.validateSingleWord(uname)) proceed = false;
            String vid = "-1";
            String group = groupId.getText().trim();
            //by default a user belongs to its own group
            if(group.length() == 0) group = uname;
            String groupList = groupsList.getText().trim();
            String role = userRole.getText().trim();
            if(role.length() == 0) role = "user";
            int uid = 0;

            //if any field is empty the corresponding values will not be updated in DB
            if(!vidList.getSelectedItem().toString().contains("No vid selected"))
                vid = vidList.getSelectedItem().toString();
            rs = handle.query("select", "max(uid)", "user", "");
            if(rs.next())
            {
                uid = rs.getInt(1) + 1;
                if(systemOut)
                System.out.println("User ID to be assigned: " + uid);
                logger.debug("User id to be assigned: " + uid + ".");
            }
            rs.close();
            //now validate all fields here before beginning the transaction
            if(!dbHandler.validateSingleWord(role)) proceed = false;
            if(!dbHandler.validateSingleWord(group)) proceed = false;
            String delims = "[ ,\n]+";
            String[] glist = groupList.split(delims);
            for(int i=0; i<glist.length; i++)
                if(glist[i].trim().length() > 0)
                    if(!dbHandler.validateSingleWord(glist[i].trim())) proceed = false;
                        
            if(proceed && handle.beginTransaction())
            {
                int unameSize = 8;
                //unameSize  += rand.nextInt(10);
                //String oneUser = uname + "_vep_";
                String oneUser = RandomStringUtils.randomAlphanumeric(unameSize);
                //now test to see if this is already in use
                boolean genNext = true;
                while(genNext)
                {
                    logger.debug("Testing if opennebula mapping by id " + oneUser + " already exists.");
                    rs = handle.query("select", "*", "user", "where oneuser='" + oneUser + "'");
                    if(rs.next())
                    {
                        genNext = true;
                        logger.debug("Opennebula mapping by id " + oneUser + " already exists. Will try to create a new id.");
                    }
                    else
                    {
                        genNext = false;
                        logger.debug("Opennebula mapping by id " + oneUser + " does not exists. Will try to register with ONE daemon.");
                    }
                    rs.close();
                    if(genNext)
                        oneUser = RandomStringUtils.randomAlphanumeric(unameSize);
                }
                String onePass = RandomStringUtils.randomAlphanumeric(32);
                //first try to create the opennebula user for the vep user
                int oneId = -1;
                if(oneHandle != null)
                    oneId = oneHandle.addUser(oneUser, VEPHelperMethods.makeSHA1Hash(onePass));
                else if(one3Handle != null)
                    oneId = one3Handle.addUser(oneUser, onePass); //normal password
                if(oneId != -1)
                {
                    boolean status = handle.insert("user", "('" + uname + "', " + uid + ", '" + vid + "', '" + oneUser + "', '" + onePass + "', " + oneId + ",  '" + role + "')");
                    //now add the group data to the groups table
                    if(status)
                        handle.insert("ugroup", "('" + uname + "'," + uid + ")");
                    rs = handle.query("SELECT", "*", "ugroup", "where gname='" + group + "' AND uid=" + uid);
                    if(!rs.next())
                    {
                        //insert the group
                        if(status)
                            handle.insert("ugroup", "('" + group + "'," + uid + ")");
                    }
                    for(int i=0; i<glist.length; i++)
                    {
                        if(glist[i].trim().length() > 0)
                        {
                            rs = handle.query("SELECT", "*", "ugroup", "where gname='" + glist[i].trim() + "' AND uid=" + uid);
                            if(!rs.next())
                            {
                                //insert the group
                                if(status)
                                handle.insert("ugroup", "('" + glist[i].trim() + "'," + uid + ")");
                            }
                        }
                    }
                    if(handle.commitTransaction())
                    {
                        if(systemOut)
                        System.out.println("user updated successfully ...");
                        logger.debug("User " + uname + " was added successfully.");
                    }
                    else
                    {
                        handle.endTransaction();
                        logger.warn("User " + uname + " update request was not completed properly. Transaction ended.");
                    }
                    refreshButtonActionPerformed(null);
                    if(proceed)
                        logArea.append("User " + uname + " was added successfully.\n");
                }
                else
                {
                    handle.endTransaction();
                    logger.warn("Unable to create corresponding opennebula user. Try with a different username.");
                }
            }
            else
            {
                if(!proceed)
                {
                    Dialog errorMessage;
                    JOptionPane errMess = new JOptionPane("Some fields are not valid, check for presence of ', \", SPACE and try again.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(this.getRootPane(), "Invalid format");
                    logger.warn("User add request: some fields are not valid, check for ', \", SPACE and try again.");
                    errorMessage.setVisible(true);
                    return;
                }
                else
                {
                    if(systemOut)
                    System.out.println("Could not begin transaction: userManagement.java addButtonAction");
                    logger.error("Could not begin transaction for user add process.");
                }
            }
        }
    }
    catch(Exception ex)
    {
        if(logger.isDebugEnabled())
            ex.printStackTrace(System.err);
    }
}//GEN-LAST:event_addButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField groupId;
    private javax.swing.JTextArea groupsList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton updateButton;
    private javax.swing.JComboBox userList;
    private javax.swing.JTextField userRole;
    private javax.swing.JTextField username;
    private javax.swing.JComboBox vidList;
    // End of variables declaration//GEN-END:variables
}
