/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.sql.ResultSet;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 * VEP managed host pool that allocates next host for VM deployment in a
 * round robin manner. This class does not check if the host has enough
 * resource for the VM
 * 
 * @author piyush
 */
public class VEPHostPool 
{
    private static LinkedList<Host> hostList;
    private Logger logger;
    private dbHandler db;
    private String dbType;
    private String vepProperties;
    private long periodicity = 2000*60; //2 minutes 
    
    public VEPHostPool()
    {
        logger = Logger.getLogger("VEP.VEPHostPool");
        vepProperties = VEPHelperMethods.getPropertyFile();
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("VEPHostPool", dbType);
        hostList = new LinkedList<Host>();
        logger.debug("VEPHOSTPOOL object created");
    }
    
    /**
     * This method syncs the host pool with the VEP database
     */
    public void sync()
    {
        logger.debug("VEPHOSTPOOL SYNC PROCESS STARTED");
        ResultSet rs;
        try
        {
            rs = db.query("select", "*", "computenode", "");
            int count=0;
            while(rs.next())
            {
                count++;
                int cid = rs.getInt("cid");
                String hname = rs.getString("hostname");
                String clmgr = rs.getString("clmgrtype");
                int hid = -1;
                rs.close();
                rs = db.query("select", "hostid", "clmanager", "where cid=" + cid);
                if(rs.next())
                    hid = rs.getInt("hostid");
                rs.close();
                
                //now check if the host is already there, if not then add it to the list
                int loc = isHostinPool(hname,cid,clmgr);
                if(loc == -1 && hid != -1)
                {
                    Host temp = new Host();
                    temp.hostName = hname;
                    temp.clMgrType = clmgr;
                    temp.computeId = cid;
                    temp.clHostId = hid;
                    hostList.add(temp);
                    logger.debug("Added a new host to the pool: " + temp.hostName + ".");
                }
                else
                {
                    hostList.get(loc).isOrphan = false;
                    hostList.get(loc).lastRefreshed = System.currentTimeMillis();
                }
                
                rs = db.query("select", "cid, hostname, clmgrtype", "computenode", "");
                for(int i=0; i<count; i++) rs.next();
            }
            
            for(int i=0; i<hostList.size(); i++)
            {
                if((System.currentTimeMillis() - hostList.get(i).lastRefreshed) > 2*periodicity)
                    hostList.get(i).isOrphan = true;
            }
            //now purging all the orphaned entries
            count=0;
            while(count < hostList.size())
            {
                if(hostList.get(count).isOrphan)
                    hostList.remove(count);
                else
                    count++;
            }
        }
        catch(Exception ex)
        {
            logger.warn("VEP HostPool Sync operation encountered an exception: " + ex.getMessage());
            //ex.printStackTrace(System.err);
        }
    }
    
    /**
     * use to get the host id for VM deployment
     * 
     * @return  the host-id
     */
    public static int getNextHost()
    {
        int hid = 0;
        if(hostList.size() > 0)
        {
            //find first last used 
            int pos = -1;
            for(int i=0; i<hostList.size(); i++)
            {
                if(hostList.get(i).lastUsed)
                {
                    pos = i;
                    break;
                }
            }
            if(pos == -1)
            {
                hostList.get(0).lastUsed = true;
                hid = hostList.get(0).clHostId;
            }
            else
            {
                hostList.get(pos).lastUsed = false;
                int nextIndex = (pos+1)%hostList.size();
                hid = hostList.get(nextIndex).clHostId;
                hostList.get(nextIndex).lastUsed = true;
            }
        }
        else
            hid = -1;
        return hid;
    }
    
    /**
     * Determines whether the host is already in the host-pool data structure
     * 
     * @param hostname      the name of the machine
     * @param computeId     cid value of the VEP computenode table entry
     * @param clmgrtype     cloud controller name, <i>OpenNebule</i>, <i>OpenStack</i>, etc.
     * @return          the position in the host-pool queue if found, else returns a -1
     */
    public int isHostinPool(String hostname, int computeId, String clmgrtype)
    {
        int loc = -1;
        for(int i=0; i<hostList.size(); i++)
        {
            Host temp = hostList.get(i);
            if(temp.hostName.equalsIgnoreCase(hostname) && temp.computeId == computeId && temp.clMgrType.equalsIgnoreCase(clmgrtype))
            {
                loc = i;
                break;
            }
        }
        return loc;
    }
}

class Host
{
    public String hostName;
    public String clMgrType;
    public int computeId;
    public int clHostId;
    public boolean lastUsed;
    public boolean isOrphan;
    public long lastRefreshed;
    
    public Host()
    {
        lastUsed = false;
        isOrphan = false; //if in realtime the host was deleted by admin
        lastRefreshed = System.currentTimeMillis();
        computeId = -1;
        clHostId = -1;
        clMgrType = "";
        hostName = "";
    }
}