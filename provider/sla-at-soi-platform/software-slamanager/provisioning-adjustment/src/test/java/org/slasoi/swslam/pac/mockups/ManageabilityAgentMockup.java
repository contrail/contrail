/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.pac.mockups;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;

public class ManageabilityAgentMockup implements Runnable {

    private static final Logger logger = Logger.getLogger(ManageabilityAgentMockup.class.getName());
    private String messageId = null;
    private PubSubManager pubSubManager = null;
    private String channel = null;

    public ManageabilityAgentMockup(String id, String eventBusPropertiesFile, String channel) {
        this.messageId = id;
        this.channel = channel;
        configureMessaging(eventBusPropertiesFile, channel);
    }

    public void run() {
        // Wait a random amount of time
        // First simulation: sleep a little bit (between 0 and 10 seconds)
        int responseTime = (int) (Math.random() * 10000);

        logger.info(" going to sleep for " + responseTime + " milliseconds");

        try {
            Thread.sleep(responseTime);
        }
        catch (InterruptedException e) {
            logger.debug(" sleeping thread interrupted.");
        }

        // Build the message
        ManageabilityAgentMessage message = createMessage();

        // Send the message through the event bus
        PubSubMessage pubsubMessage = new PubSubMessage(channel, message.toXML());
        try {
            pubSubManager.publish(pubsubMessage);
        }
        catch (MessagingException e) {
            logger.debug("Error when publishing a ManageabilityAgent message");
            e.printStackTrace();
        }

    }

    private void configureMessaging(String propertiesFile, String channel) {

        // Create manager instance
        try {
            pubSubManager = PubSubFactory.createPubSubManager(propertiesFile);
        }
        catch (MessagingException e) {
            logger.error("PubSubManager could not be created" + e.getMessage());
            return;
        }
        catch (FileNotFoundException e) {
            logger.error("Properties file " + propertiesFile + " not found");
        }
        catch (IOException e) {
            logger.error("EventBusHandlingTask, IO error in configureMessaging");
            logger.error(e.getMessage());
        }

        logger.debug("EventBusHandlingTask::configureMessaging.PubSubManager created");

        // Channel creation
        try {
            if (!pubSubManager.isChannel(channel)) {
                pubSubManager.createChannel(new Channel(channel));
                logger.debug("Channel " + channel + " created");
            }
        }
        catch (MessagingException e) {
            logger.error("Error subscribing to channel " + channel);
            logger.error(e.getMessage());
        }

    }

    private ManageabilityAgentMessage createMessage() {
        logger.info("Creating new ManageabilityAgentMessage...");

        ManageabilityAgentMessage message = new ManageabilityAgentMessage();
        message.setTimestamp(System.currentTimeMillis());
        message.setEventType("Provision_Event");
        message.setValue("Id", messageId);
        message.setValue("ProvisioningStatus", "READY");

        logger.debug(message);

        return message;
    }

    public void start() {
        run();
    }

}
