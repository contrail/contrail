/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.pac.mockups;

import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

public class SLARegistryMockup implements SLARegistry, SLARegistry.IRegister, SLARegistry.IQuery, SLARegistry.IAdminUtils {

    // registry to store one SLA for the test :)
    private SLA sla;

    public IQuery getIQuery() {
        return this;
    }

    public IRegister getIRegister() {
        return this;
    }

    public IAdminUtils getIAdmin()
    {
        return null;
    }

    public UUID register(SLA sla, UUID[] uuids, SLAState arg2) throws RegistrationFailureException {
        this.sla = sla;
        return null;
    }

    public UUID sign(UUID uuid) throws UpdateFailureException {
        return uuid;
    }

    public UUID update(UUID uuid, SLA sla, UUID[] arg2, SLAState arg3) throws UpdateFailureException {
        this.sla = sla;
        return uuid;
    }

    public SLA[] getSLA(UUID[] arg0) throws InvalidUUIDException {
        SLA[] slas = new SLA[1];
        slas[0] = sla;
        return slas;
    }

    public UUID[] getDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getSLAsByState(SLAState[] arg0, boolean arg1) throws InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAStateInfo[] getStateHistory(UUID arg0, boolean arg1) throws InvalidUUIDException, InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getUpwardDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByParty(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByTemplateId(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public void clear( UUID[] ids )
    {
    }

    public void clearAll()
    {
    }
}
