/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.pac.mockups;

import org.apache.log4j.Logger;

//Should extend from SoftwareServiceManagerFacade, but it has a private constructor :(
public class SoftwareServiceManagerMockup {
    private static final Logger logger = Logger.getLogger(SoftwareServiceManagerMockup.class.getName());

    private boolean provisionCalled = false;

    private String eventBusProperties = null;
    private String channel = null;

    // just for testing purposes, this method is not implemented by the real ISM
    public void setEventBusProperties(String eventBusProperties, String channel) {
        this.eventBusProperties = eventBusProperties;
        this.channel = channel;
    }

    /*
     * public ProvisionResponseType provision(ProvisionRequestType request) throws DescriptorException {
     * logger.info("Infrastructure Service Manager, provision..."); logger.info("ProvisionRequestType: ");
     * setProvisionCalled(true); // First simulation: sleep a little bit (between 0 and 10 seconds) int responseTime =
     * (int) (Math.random() * 10000);
     * 
     * logger.info(" going to sleep for " + responseTime + " milliseconds");
     * 
     * try { Thread.sleep(responseTime); } catch (InterruptedException e) {
     * logger.debug(" sleeping thread interrupted."); }
     * 
     * ProvisionResponseType response = new ProvisionResponseType(); String infrastructureId =
     * java.util.UUID.randomUUID().toString(); response.setInfrastructureID(infrastructureId);
     * 
     * // ManageabilityAgent should now send a message to the event bus ManageabilityAgentMockup managAgent = new
     * ManageabilityAgentMockup(infrastructureId, eventBusProperties, channel); new Thread(managAgent).start();
     * 
     * return response; }
     */

    public void setProvisionCalled(boolean provisionCalled) {
        this.provisionCalled = provisionCalled;
    }

    public boolean isProvisionCalled() {
        return provisionCalled;
    }

}
