/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.swslam.pac;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.gslam.pac.ActionExecutionTask;
import org.slasoi.gslam.pac.ManagedElement;
import org.slasoi.gslam.pac.Task;
import org.slasoi.gslam.pac.TaskStatus;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;
import org.slasoi.swslam.commons.plan.SoftwareTask;

/**
 * Class in charge of executing a given action that in turn means an invocation to some external component (typically,
 * Service Manager)
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class SoftwareActionExecutionTask extends ActionExecutionTask {

    private static final String CHANNEL = "CHANNEL";
    private static final String BUS_PROPERTIES_FILE = "BUS_PROPERTIES_FILE";

    /**
     * @param config
     */
    public SoftwareActionExecutionTask(TaskConfiguration config, org.slasoi.gslam.commons.plan.Task action, Task parent) {
        super(config, action, parent);
        logger.debug("Creating SoftwareExecutionTask");
    }

    /**
     * Needed for RTTI purposes
     */
    public SoftwareActionExecutionTask() {
        logger.debug("Creating software ActionExecutionTask");
    }

    public void run() {

        if (action != null) {
            logger.info("Triggering action " + action.getActionName() + " to ServiceManager "
                    + action.getServiceManagerId());

            action.setStatus(TaskStatus.CREATED.toString());

            ManagedElement serviceManager = agent.getManagedElement(action.getServiceManagerId());

            if (serviceManager != null) {
                Object element = serviceManager.getElement();

                if (action instanceof SoftwareTask) {
                    logger.debug("Invoking provision on Software Service Manager...");
                    ServiceBuilder builder = ((SoftwareTask) action).getServiceBuilder();

                    // TODO
                    String busPropertiesFile =
                            System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                                    + config.getValue(BUS_PROPERTIES_FILE);
                    Settings settings;
                    String channel = config.getValue(CHANNEL);

                    try {
                        settings = new Settings(busPropertiesFile);
                        ((SoftwareServiceManagerFacade) element).startServiceInstance(builder, settings, channel);
                    }
                    catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (ServiceStartupException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    catch (MessagingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
            }

            parent.notify(this);
        }
    }

}
