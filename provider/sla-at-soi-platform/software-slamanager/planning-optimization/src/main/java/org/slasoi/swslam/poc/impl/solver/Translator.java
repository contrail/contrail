package org.slasoi.swslam.poc.impl.solver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.command.Command;
import org.drools.command.CommandFactory;
import org.drools.io.ResourceFactory;
import org.drools.runtime.ExecutionResults;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slasoi.slamodel.sla.SLATemplate;

public class Translator extends Base {

    private static Translator translator = null;
    private KnowledgeBuilder knowledgeBuilder1;
    private KnowledgeBase knowledgeBase1;
    private StatefulKnowledgeSession session1;
    private KnowledgeBuilder knowledgeBuilder2;
    private KnowledgeBase knowledgeBase2;
    private StatefulKnowledgeSession session2;
    private TemplateParser templateParser = new TemplateParser();
    private HashMap<String, SoftwareServiceOperation> mapOfSSOperations =
            new HashMap<String, SoftwareServiceOperation>();

    private Translator() {
        LOGGER.info("Inside Translator()");
        initRuleEngine1();
        initRuleEngine2();
    }

    public static Translator getInstance() {
        if(translator == null){
            translator = new Translator();
        }
        return translator;
    }

    public List<InfrastructureServiceTerms> queryKnowledgeBase(SLATemplate slaTemplate, int knowledgeBaseNumber) {
        LOGGER.info("Inside queryKnowledgeBase()-"+knowledgeBaseNumber);
        
        this.templateParser.ASSIGNMENT_MODE = false; // Now we want to extract values set by customer inside the
                                                     // template. true will overwrite them with our own quatsch.
        List<InfrastructureServiceTerms> listOfISTerms = new ArrayList<InfrastructureServiceTerms>();
        this.mapOfSSOperations = this.templateParser.getMapOfSSOperations(slaTemplate);

        // LOGGER.info("PRINTING SS Terms\n ");
        // LOGGER.info("bookSale= " + this.mapOfSSOperations.get("bookSale").toString());
        // LOGGER.info("gGetProductDetails = " + this.mapOfSSOperations.get("getProductDetails").toString());
        // LOGGER.info("PaymentServiceOperation = " +
        // this.mapOfSSOperations.get("PaymentServiceOperation").toString()+"\n\n");

        if (knowledgeBaseNumber == 1) {
            this.session1.setGlobal("bookSaleOperation", this.mapOfSSOperations.get("bookSale"));
            this.session1.setGlobal("productDetailsOperation", this.mapOfSSOperations.get("getProductDetails"));
            this.session1.setGlobal("paymentServiceOperation", this.mapOfSSOperations.get("PaymentServiceOperation"));
        }
        else if (knowledgeBaseNumber == 2) {
            this.session2.setGlobal("bookSaleOperation", this.mapOfSSOperations.get("bookSale"));
            this.session2.setGlobal("productDetailsOperation", this.mapOfSSOperations.get("getProductDetails"));
            this.session2.setGlobal("paymentServiceOperation", this.mapOfSSOperations.get("PaymentServiceOperation"));
        }

        Event initializeEvent;
        List<Event> listOfEvents = new ArrayList<Event>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            initializeEvent = new Event(EventName.QueryEvent);
            listOfEvents.removeAll(listOfEvents);

            switch (i) {
            case 0:
                initializeEvent.setRiskQuantifier(RiskQuantifier.NEGLIGIBLE);
                break;
            case 1:
                initializeEvent.setRiskQuantifier(RiskQuantifier.LOW);
                break;
            case 2:
                initializeEvent.setRiskQuantifier(RiskQuantifier.SOME);
                break;
            case 3:
                initializeEvent.setRiskQuantifier(RiskQuantifier.MEDIUM);
                break;
            case 4:
                initializeEvent.setRiskQuantifier(RiskQuantifier.HIGH);
                break;
            default:
                LOGGER.info("Default case");
                break;

            }

            listOfEvents.add(initializeEvent);
            if (knowledgeBaseNumber == 1) {
                listOfEvents = (List<Event>) process(listOfEvents, session1);
            }
            else if (knowledgeBaseNumber == 2) {
                listOfEvents = (List<Event>) process(listOfEvents, session2);
            }

            initializeEvent = listOfEvents.get(0);
            LOGGER
                    .info("ATTEMPT #" + (i + 1) + " RULE ENGINE RETURNED FEASIBLE SOLUTIONS = "
                            + initializeEvent.getListOfIsTerms().size() + " with RISK = "
                            + initializeEvent.getRiskQuantifier());
            
            int minCTBookSale = 0, maxCTBookSale = 0, minCTProductDetail = 0, maxCTProductDetail = 0, minCTPayment = 0, maxCTPayment = 0;
            
            for (InfrastructureServiceTerms isTerm : initializeEvent.getListOfIsTerms()) {
                LOGGER.info("Rule Name = " + isTerm.getRuleNumber());
                //initialize
                if(minCTBookSale == 0){
                    minCTBookSale = isTerm.getBookSaleOperation().getCompletionTime();
                }
                if(maxCTBookSale == 0){
                    maxCTBookSale = isTerm.getBookSaleOperation().getCompletionTime();
                }
                if(minCTProductDetail == 0){
                    minCTProductDetail = isTerm.getProductDetailsOperation().getCompletionTime();
                }
                if(maxCTProductDetail == 0){
                    maxCTProductDetail = isTerm.getProductDetailsOperation().getCompletionTime();
                }
                if(minCTPayment == 0){
                    minCTPayment = isTerm.getPaymentServiceOperation().getCompletionTime();
                }
                if(maxCTPayment == 0){
                    maxCTPayment = isTerm.getPaymentServiceOperation().getCompletionTime();
                }
                
                //Choose minimum & maximum values for this Query based on given risk
                if(isTerm.getBookSaleOperation().getCompletionTime() < minCTBookSale){
                    minCTBookSale = isTerm.getBookSaleOperation().getCompletionTime();
                }
                if(isTerm.getBookSaleOperation().getCompletionTime() > maxCTBookSale){
                    maxCTBookSale = isTerm.getBookSaleOperation().getCompletionTime();
                }
                if(isTerm.getProductDetailsOperation().getCompletionTime() < minCTProductDetail){
                    minCTProductDetail = isTerm.getProductDetailsOperation().getCompletionTime();
                }
                if(isTerm.getProductDetailsOperation().getCompletionTime() > maxCTProductDetail){
                    maxCTProductDetail = isTerm.getProductDetailsOperation().getCompletionTime();
                }
                if(isTerm.getPaymentServiceOperation().getCompletionTime() < minCTPayment){
                    minCTPayment = isTerm.getPaymentServiceOperation().getCompletionTime();
                }
                if(isTerm.getPaymentServiceOperation().getCompletionTime() > maxCTPayment){
                    maxCTPayment = isTerm.getPaymentServiceOperation().getCompletionTime();
                }
                
                listOfISTerms.add(isTerm);
            }
            //Output summary:
            LOGGER.info("minCTBookSale = " + minCTBookSale);
            LOGGER.info("maxCTBookSale = " + maxCTBookSale);
            LOGGER.info("minCTProductDetail = " + minCTProductDetail);
            LOGGER.info("maxCTProductDetail = " + maxCTProductDetail);
            LOGGER.info("minCTPayment = " + minCTPayment);
            LOGGER.info("maxCTPayment = " + maxCTPayment);
            
        }
        long endTime = System.currentTimeMillis();
        LOGGER.info("Query Rule Engine 5 times took = " + (endTime - startTime) + " milli-seconds");
        return listOfISTerms;
    }

    public void initRuleEngine1() {
        LOGGER.info("Inside init()");
        long startTime = System.currentTimeMillis();
        knowledgeBuilder1 = KnowledgeBuilderFactory.newKnowledgeBuilder();
        String pathToDRL = "";
        pathToDRL =
                pathToDRL =
                        System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "software-slamanager"
                                + System.getProperty("file.separator") + "planning-optimization"
                                + System.getProperty("file.separator") + "adaptive-kb"
                                + System.getProperty("file.separator") + "KB-A1WCT.drl";
        LOGGER.info(pathToDRL);
        FileInputStream fstream = null;
        try {
            //LOGGER.info("Trying to open this path = \n" + pathToDRL);
            fstream = new FileInputStream(pathToDRL);
        }
        catch (FileNotFoundException fnfe1) {
            LOGGER.info("Could not find file at above path.");
            fnfe1.printStackTrace();
        }
        try {
            if (fstream != null) {
                fstream.close();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }

        assert (knowledgeBuilder1 != null) : "KnowledgeBuilder is null";
        LOGGER.info("Loading Ruleset A (one time operation). This may take a few minutes depending on your system resources");
        knowledgeBuilder1.add(ResourceFactory.newFileResource(pathToDRL), ResourceType.DRL);

        assert (!knowledgeBuilder1.hasErrors()) : "KnowledgeBuilder has errors. Check syntax of Rule file!";
        if (knowledgeBuilder1.hasErrors()) {
            LOGGER.info("KnowledgeBuilder has errors: " + knowledgeBuilder1.getErrors());
            return;
        }

        knowledgeBase1 = KnowledgeBaseFactory.newKnowledgeBase();

        knowledgeBase1.addKnowledgePackages(knowledgeBuilder1.getKnowledgePackages());
        session1 = knowledgeBase1.newStatefulKnowledgeSession();
        // session.fireAllRules();//Just a test instruction.
        long endTime = System.currentTimeMillis();
        LOGGER.info("Rule Engine initialized. Time taken = " + (endTime - startTime) / 1000 + " seconds");
    }

    public void initRuleEngine2() {
        LOGGER.info("Inside init()");
        long startTime = System.currentTimeMillis();
        knowledgeBuilder2 = KnowledgeBuilderFactory.newKnowledgeBuilder();
        String pathToDRL = "";
        pathToDRL =
                pathToDRL =
                        System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "software-slamanager"
                                + System.getProperty("file.separator") + "planning-optimization"
                                + System.getProperty("file.separator") + "adaptive-kb"
                                + System.getProperty("file.separator") + "KB-A1WOCT.drl"; // e.g., "KB-A1WOCT.drl" or
                                                                                          // "KB-A1WCT.drl"
        // LOGGER.info(pathToDRL);
        FileInputStream fstream = null;
        try {
            //LOGGER.info("Trying to open this path = \n" + pathToDRL);
            fstream = new FileInputStream(pathToDRL);
        }
        catch (FileNotFoundException fnfe1) {
            LOGGER.info("Could not find file at above path.");
            fnfe1.printStackTrace();
        }
        try {
            if (fstream != null) {
                fstream.close();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }

        assert (knowledgeBuilder2 != null) : "KnowledgeBuilder is null";
        LOGGER.info("Loading Ruleset B (one time operation). This may take a few minutes depending on your system resources");
        knowledgeBuilder2.add(ResourceFactory.newFileResource(pathToDRL), ResourceType.DRL);

        assert (!knowledgeBuilder2.hasErrors()) : "KnowledgeBuilder has errors. Check syntax of Rule file!";
        if (knowledgeBuilder2.hasErrors()) {
            LOGGER.info("KnowledgeBuilder has errors: " + knowledgeBuilder2.getErrors());
            return;
        }

        knowledgeBase2 = KnowledgeBaseFactory.newKnowledgeBase();

        knowledgeBase2.addKnowledgePackages(knowledgeBuilder2.getKnowledgePackages());
        session2 = knowledgeBase2.newStatefulKnowledgeSession();
        long endTime = System.currentTimeMillis();
        LOGGER.info("Rule Engine initialized. Time taken = " + (endTime - startTime) / 1000 + " seconds");
    }

    public Object process(Object listOfEvents, StatefulKnowledgeSession session) {
        List<Event> eventsList = (List<Event>) listOfEvents;
        // Convert the listOfEvents received into Drools Commands and batch execute as follows:
        List<Command> commands = convertEventsToCommands(eventsList);
        ExecutionResults results = session.execute(CommandFactory.newBatchExecution(commands));
        int i = session.fireAllRules();// StatelessSession does this implicitly, use will override implicit call.
        // Now, convert all the received result objects back to Events, put them in a List and return.
        eventsList = convertResultsToEvents(results, eventsList.size());
        return eventsList;
    }

    public List<Command> convertEventsToCommands(List<Event> listOfEvents) {
        List<Command> commands = new ArrayList<Command>();
        if (listOfEvents != null && listOfEvents.size() > 0) {
            int counter = 0;
            for (Event e : listOfEvents) {
                commands.add(CommandFactory.newInsert(e, counter + ""));
                counter++;
            }
        }
        return commands;
    }

    public List<Event> convertResultsToEvents(ExecutionResults results, int length) {
        List<Event> listOfEvents = new ArrayList<Event>();
        for (int i = 0; i < length; i++) {
            listOfEvents.add((Event) results.getValue(i + ""));
        }
        return listOfEvents;
    }

    // This method to be called at the end of createAgreement() and cancelNegotiation().
    public void disposeWorkingMemory(int knowledgeBaseNumber) {
        // Call dispose on session
        if (knowledgeBaseNumber == 1) {
            session1.dispose();
        }
        else if (knowledgeBaseNumber == 2) {
            session2.dispose();
        }

    }
    
}
