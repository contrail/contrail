package org.slasoi.swslam.poc.impl.solver;

import java.util.ArrayList;
import java.util.List;

public class SoftwareServiceOperation {
    
    //Software params:
    private String serviceName;
    private String operationName;
    private String termId;
    
    private int completionTime;
    private int arrivalRate;
    private int availability; //not used yet
    private int reliability;  //not used yet
    
    //Min-Max Value Constraints for sw params:
    private int minArrivalRate;
    private int maxArrivalRate;
    private int minCompletionTime;
    private int maxCompletionTime;
    
    public SoftwareServiceOperation(){
        
    }
    
    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public int getMinArrivalRate() {
        return minArrivalRate;
    }

    public void setMinArrivalRate(int minArrivalRate) {
        this.minArrivalRate = minArrivalRate;
    }

    public int getMaxArrivalRate() {
        return maxArrivalRate;
    }

    public void setMaxArrivalRate(int maxArrivalRate) {
        this.maxArrivalRate = maxArrivalRate;
    }

    public int getMinCompletionTime() {
        return minCompletionTime;
    }

    public void setMinCompletionTime(int minCompletionTime) {
        this.minCompletionTime = minCompletionTime;
    }

    public int getMaxCompletionTime() {
        return maxCompletionTime;
    }

    public void setMaxCompletionTime(int maxCompletionTime) {
        this.maxCompletionTime = maxCompletionTime;
    }

    public int getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(int completionTime) {
        this.completionTime = completionTime;
    }

    public int getArrivalRate() {
        return arrivalRate;
    }

    public void setArrivalRate(int arrivalRate) {
        this.arrivalRate = arrivalRate;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public int getReliability() {
        return reliability;
    }

    public void setReliability(int reliability) {
        this.reliability = reliability;
    }
    
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
        
    @Override
    public String toString() {
        return "[ "+this.serviceName+"/"+this.operationName+" : "+"arrival_rate="
                +this.arrivalRate+", minArrivalRate="+this.minArrivalRate+", maxArrivalRate="+this.maxArrivalRate
                +", completion_time="+this.completionTime + ", minCompletionTime="+this.minCompletionTime
                + ", maxCompletionTime="+this.maxCompletionTime+" ]";
    }

}
