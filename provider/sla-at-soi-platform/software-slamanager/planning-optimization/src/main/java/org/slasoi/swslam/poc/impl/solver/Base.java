package org.slasoi.swslam.poc.impl.solver;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Base {

    public static final Logger LOGGER = Logger.getLogger(Base.class);

    public Base() {
        initializeLogger();
    }

    private void initializeLogger() {
        Properties logProperties = new Properties();
        try {
            logProperties.load(new FileReader(System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                    + "software-slamanager" + System.getProperty("file.separator") + "planning-optimization"
                    + System.getProperty("file.separator") + "log4j.properties"));
            PropertyConfigurator.configure(logProperties);
            LOGGER.info("Logging initialized.");
        }
        catch (IOException e) {
            throw new RuntimeException("Unable to load logging property ");
        }
    }

}
