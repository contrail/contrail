package org.slasoi.swslam.poc.impl.solver;

import java.util.ArrayList;
import java.util.List;

public class InfrastructureServiceTerms {

    private String termId;
    // Infrastructure params:
    private int vmQuantity;
    private int cpuCores;
    private int cpuSpeed;
    private int memory;
    private String image;
    // Min-Max Value Constraints for hw params:
    private int minCpuCores;
    private int maxCpuCores;
    private int minCpuSpeed;
    private int maxCpuSpeed;
    private int minMemory;
    private int maxMemory;
    private int minVMQuantity;
    private int availability;
    private int minAvailability;
    private int maxAvailability;
    private List<String> listOfImages = new ArrayList<String>();

    private RiskQuantifier riskQualifier;
    private int ruleNumber;

    private SoftwareServiceOperation bookSaleOperation = new SoftwareServiceOperation();
    private SoftwareServiceOperation productDetailsOperation = new SoftwareServiceOperation();
    private SoftwareServiceOperation paymentServiceOperation = new SoftwareServiceOperation();

    public InfrastructureServiceTerms() {

    }

    public RiskQuantifier getRiskQualifier() {
        return riskQualifier;
    }

    public void setRiskQualifier(RiskQuantifier riskQualifier) {
        this.riskQualifier = riskQualifier;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public int getMinAvailability() {
        return minAvailability;
    }

    public void setMinAvailability(int minAvailability) {
        this.minAvailability = minAvailability;
    }

    public int getMaxAvailability() {
        return maxAvailability;
    }

    public void setMaxAvailability(int maxAvailability) {
        this.maxAvailability = maxAvailability;
    }

    public int getVmQuantity() {
        return vmQuantity;
    }

    public void setVmQuantity(int vmQuantity) {
        this.vmQuantity = vmQuantity;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public int getMinCpuCores() {
        return minCpuCores;
    }

    public void setMinCpuCores(int minCpuCores) {
        this.minCpuCores = minCpuCores;
    }

    public int getMaxCpuCores() {
        return maxCpuCores;
    }

    public void setMaxCpuCores(int maxCpuCores) {
        this.maxCpuCores = maxCpuCores;
    }

    public int getMinCpuSpeed() {
        return minCpuSpeed;
    }

    public void setMinCpuSpeed(int minCpuSpeed) {
        this.minCpuSpeed = minCpuSpeed;
    }

    public int getMaxCpuSpeed() {
        return maxCpuSpeed;
    }

    public void setMaxCpuSpeed(int maxCpuSpeed) {
        this.maxCpuSpeed = maxCpuSpeed;
    }

    public int getMinMemory() {
        return minMemory;
    }

    public void setMinMemory(int minMemory) {
        this.minMemory = minMemory;
    }

    public int getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(int maxMemory) {
        this.maxMemory = maxMemory;
    }

    public int getCpuCores() {
        return cpuCores;
    }

    public void setCpuCores(int cpuCores) {
        this.cpuCores = cpuCores;
    }

    public int getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(int cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getListOfImages() {
        return listOfImages;
    }

    public void setListOfImages(List<String> listOfImages) {
        this.listOfImages = listOfImages;
    }

    public int getMinVMQuantity() {
        return minVMQuantity;
    }

    public void setMinVMQuantity(int minVMQuantity) {
        this.minVMQuantity = minVMQuantity;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    public SoftwareServiceOperation getBookSaleOperation() {
        return bookSaleOperation;
    }

    public void setBookSaleOperation(SoftwareServiceOperation bookSaleOperation) {
        this.bookSaleOperation = bookSaleOperation;
    }

    public SoftwareServiceOperation getProductDetailsOperation() {
        return productDetailsOperation;
    }

    public void setProductDetailsOperation(SoftwareServiceOperation productDetailsOperation) {
        this.productDetailsOperation = productDetailsOperation;
    }

    public SoftwareServiceOperation getPaymentServiceOperation() {
        return paymentServiceOperation;
    }

    public void setPaymentServiceOperation(SoftwareServiceOperation paymentServiceOperation) {
        this.paymentServiceOperation = paymentServiceOperation;
    }

    @Override
    public String toString() {
        return "[TermId=" + this.termId + " : " + "cpuCores=" + this.cpuCores + ", minCpuCores=" + this.minCpuCores
                + ", maxCpuCores" + this.maxCpuCores + ", cpuSpeed=" + this.cpuSpeed + ", minCpuSpeed="
                + this.minCpuSpeed + ", maxCpuSpeed=" + this.maxCpuSpeed + ", memory=" + this.memory + ", minMemory="
                + this.minMemory + ", maxMemory=" + this.maxMemory + ", " + "availability=" + this.availability
                + ", minAvailability=" + this.minAvailability + ", maxAvailability=" + this.maxAvailability + ", "
                + "image=" + this.image + " - list{" + this.getListOfImages().iterator().toString() + "} - list.size="
                + this.getListOfImages().size() + "]";
    }
}
