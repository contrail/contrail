/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.poc.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.negotiation.SLARegistry.IQuery;
import org.slasoi.gslam.core.negotiation.SLARegistry.IRegister;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Exception;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanNotFoundException;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Party.Operative;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.exceptions.BookingException;
import org.slasoi.softwareservicemanager.exceptions.ReservationException;
import org.slasoi.softwareservicemanager.impl.BookingManager;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;
import org.slasoi.softwareservicemanager.provisioning.ProvisionServiceStub;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;
import org.slasoi.swslam.poc.impl.solver.InfrastructureServiceTerms;
import org.slasoi.swslam.poc.impl.solver.Translator;
import org.slasoi.models.scm.Dependency;
import org.slasoi.slamodel.vocab.bnf;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;

import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.ISingleResult;
import org.slasoi.seval.prediction.service.ISoftwareServiceEvaluation;
import org.slasoi.seval.prediction.service.impl.SoftwareServiceEvaluator;
import org.slasoi.slamodel.primitives.TIME;

import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;

public class POCIAssessmentAndCustomize implements PlanningOptimization.IAssessmentAndCustomize {

    protected SLAManagerContext context;
    private final Logger LOGGER = Logger.getLogger(POCIAssessmentAndCustomize.class);
    private final STND DSP_NEG_ID = sla.dsp_neg_id;
    private final STND PLAN_ID = new STND("plan_id");
    private SoftwareServiceManagerFacade softwareServiceManagerFacade;

    protected void setSoftwareServiceManagerFacade(SoftwareServiceManagerFacade softwareServiceManagerFacade) {
        this.softwareServiceManagerFacade = softwareServiceManagerFacade;
    }

    public POCIAssessmentAndCustomize() {
        LOGGER.info("*** SWPOC - CONSTRUCTOR ***");
    }

    /**
     * @negotiationID : The ID of the Negotiation Session with this SWSP
     * @slaTemplate : The SLATemplate containing fixed values that the sender wants turned into an SLA.
     * 
     */
    public SLA createAgreement(String negotiationID, SLATemplate slaTemplate) {
        /**
         * 0) Check if createAgreement request is against Negtiation or Renegotiation operation. 1) Check if service_id
         * exists in slaTemplate? 2.1) If yes, it means we have been negotiating with the caller before. Get
         * serviceBuilder object from SSM, and updateReservation() of resources till sla.effectiveFrom. then
         * createAgreement with DependendtServiceProvider, if that fails, cancel own reservation. If succeeds, simply
         * make own SLA and return it. 2.2) If no, this is a first call, but if fixed values provided in slaTemplate are
         * acceptable, we do the whole service creation process here (as in negotiate) and reserve resources till
         * sla.effectiveFrom. 3) Return the SLA.
         */
        SLA slaCreatedWithSWServiceProvider = null;
        SLA slaCreatedWithDependentServiceProvider = null;

        // If an SLA exists in our SLAR with SLA.UUID = negotiationID -> invoke renegotiate on DSP,
        // otherwise initiate:
        boolean renegotiationFlag = false;
        SLA[] softwareSLAs = null;
        SLA softwareSLA = null;
        try {
            SLARegistry slaRegistry = this.context.getSLARegistry();
            softwareSLAs = slaRegistry.getIQuery().getSLA(new UUID[] { new UUID(negotiationID) });
            if (softwareSLAs != null && softwareSLAs.length > 0) {
                renegotiationFlag = true;
                softwareSLA = softwareSLAs[0];
                LOGGER.info("*** Software SLA found with ID = " + softwareSLA.getUuid().getValue());
            }
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDException e) {
            e.printStackTrace();
        }

        LOGGER.info("*** createAgreement() - renegotiationFlag = " + renegotiationFlag);

        String serviceID = slaTemplate.getPropertyValue(sla.service_id);
        // 2
        if (serviceID != null) {
            LOGGER.info("*** serviceID = " + serviceID);
            // 2.1
            ServiceBuilder serviceBuilder = this.softwareServiceManagerFacade.getBuilder(serviceID);
            assert (serviceBuilder != null) : "SoftwareServiceManager returned a null ServiceBuilder for provided Service ID.";
            // Reserve or updateReserve resources before returning:
            try {
                // Tentative reservation extended till possible sla.effectiveFrom - inorder to preempt possible timeout.
                LOGGER.info("*** Extending Tentative reservation for half hour before proceeding to create SLA");
                softwareServiceManagerFacade.updateReservation(serviceBuilder, new java.util.Date(System
                        .currentTimeMillis()), new java.util.Date());
            }
            catch (ReservationException e1) {
                LOGGER.info("*** Reservation Exception while trying to update existing reservation.");
                e1.printStackTrace();
            }
            // Check if there are dependencies or not
            if (serviceBuilder.getBindingsLength() > 0) {
                // Yes Dependency! Examine the Composition as per Acceptance Criteria (used at negotiation time) - if
                // passed, create Agreement.
                if (isCompositionAcceptableAsPerAcceptanceCriteria(slaTemplate, serviceBuilder)) {
                    // create Agreement
                    // First, creating an agreement with self, if successful, only then with DSP.
                    slaCreatedWithSWServiceProvider = createSLAFromSLAT(slaTemplate);
                    // Needed for Provisioning.
                    slaCreatedWithSWServiceProvider.setPropertyValue(sla.service_id, serviceID);
                    LOGGER.info("*** Agreement with MySelf reached. Proceeding with DSP ***");
                    // ORC specific single template.
                    SLATemplate dependentSLATemplate = serviceBuilder.getBindings()[0].getSlaTemplate();

                    String dependentNegId = dependentSLATemplate.getPropertyValue(DSP_NEG_ID);
                    assert (dependentNegId != null) : "DSP Negotiation ID should have been annotated but was not.";
                    if (dependentNegId != null) {
                        LOGGER.info("*** dsp_neg_id = " + dependentNegId);
                    }
                    else {
                        LOGGER.info("*** dsp_neg_id not annotated ***");
                    }
                    slaCreatedWithDependentServiceProvider =
                            agreeWithDependentServiceProvider(dependentNegId, dependentSLATemplate);
                    // Agreement with Dependent Provider could not be reached - therefore quitting negotiation by
                    // cancelling own agreement (reached above) and returning null.
                    if (slaCreatedWithDependentServiceProvider == null) {
                        LOGGER.info("*** Agreement with DSP could not be reached. No SLA Created ***");
                        slaCreatedWithSWServiceProvider = null;
                        // Cancel reserved resources:
                        try {
                            LOGGER.info("*** Cancelling reserved resources***");
                            softwareServiceManagerFacade.cancelReservation(serviceBuilder);
                        }
                        catch (ReservationException e) {
                            LOGGER
                                    .info("*** Reservation Exception while trying to cancel service builder reservation. ***");
                            e.printStackTrace();
                        }
                    }
                    else {
                        LOGGER.info("*** Agreement reached with DSP: SLA created with DSP***");
                        try {
                            IMonitoringManager manager = this.context.getMonitorManager();
                            MonitoringSystemConfiguration monitoringSystemConfiguration =
                                    manager.checkMonitorability(slaCreatedWithSWServiceProvider, serviceBuilder
                                            .getImplementation().getComponentMonitoringFeatures());
                            serviceBuilder.setMonitoringSystemConfiguration(monitoringSystemConfiguration);
                            LOGGER.info("*** Confirming/Booking reservation as SLA is created successfully!");
                            try {
                                // PAC can extract serviceBuilder by querying on service_id. POC does not require
                                // service endpoints!
                                LOGGER.info("*** Booking ServiceBuilder. ***");
                                softwareServiceManagerFacade.book(serviceBuilder);
                                LOGGER.info("*** ServiceBuilder Booked. ***");
                            }
                            catch (BookingException e) {
                                LOGGER.info("*** Booking service builder failed!");
                                e.printStackTrace();
                            }

                            SLARegistry registry = this.context.getSLARegistry();
                            IRegister register = registry.getIRegister();

                            // Register or Update after checking if this is Negotiation or Renegotiation!
                            if (renegotiationFlag == false) {
                                LOGGER.info("*** Before registering own SLA in SLAR ***");
                                register.register(slaCreatedWithSWServiceProvider,
                                        new UUID[] { slaCreatedWithDependentServiceProvider.getUuid() },
                                        SLAState.OBSERVED);
                                LOGGER.info("*** Own SLA registered successfully in SLAR ***");
                            }
                            else {
                                LOGGER.info("*** Before updating own SLA in SLAR ***");
                                register.update(softwareSLA.getUuid(), slaCreatedWithSWServiceProvider,
                                        new UUID[] { slaCreatedWithDependentServiceProvider.getUuid() },
                                        SLAState.OBSERVED);
                                LOGGER.info("*** Own SLA updated successfully in SLAR ***");
                            }
                        }
                        catch (SLAManagerContextException e) {
                            e.printStackTrace();
                        }
                        catch (RegistrationFailureException e) {
                            e.printStackTrace();
                        }
                        catch (UpdateFailureException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    LOGGER.info("*** Agreement could not be reached. No SLA Created ***");
                    // Cancel reserved resources:
                    try {
                        LOGGER.info("*** Cancelling reserved resources***");
                        softwareServiceManagerFacade.cancelReservation(serviceBuilder);
                    }
                    catch (ReservationException e) {
                        LOGGER
                                .info("*** Reservation Exception while trying to cancel service builder reservation. ***");
                        e.printStackTrace();
                    }
                }
            }
            else {
                // No Dependency.
                if (isAcceptableAsPerAcceptanceCriteria(slaTemplate)) {
                    // create Agreement
                    slaCreatedWithSWServiceProvider = createSLAFromSLAT(slaTemplate);
                    slaCreatedWithSWServiceProvider.setPropertyValue(sla.service_id, serviceID);// Needed for
                    // Provisioning.
                    LOGGER.info("*** Agreement reached. SLA created ***");
                    try {
                        IMonitoringManager manager = this.context.getMonitorManager();
                        MonitoringSystemConfiguration monitoringSystemConfiguration =
                                manager.checkMonitorability(slaCreatedWithSWServiceProvider, serviceBuilder
                                        .getImplementation().getComponentMonitoringFeatures());
                        serviceBuilder.setMonitoringSystemConfiguration(monitoringSystemConfiguration);
                        LOGGER.info("*** Confirming/Booking reservation as SLA is created successfully!");
                        try {
                            // PAC can extract serviceBuilder by querying on service_id. POC does not require service
                            // endpoints!
                            LOGGER.info("*** Booking ServiceBuilder. ***");
                            softwareServiceManagerFacade.book(serviceBuilder);
                        }
                        catch (BookingException e) {
                            LOGGER.info("*** Booking service builder failed!");
                            e.printStackTrace();
                        }

                        SLARegistry registry = this.context.getSLARegistry();
                        IRegister register = registry.getIRegister();
                        LOGGER.info("*** Before saving own SLA in SLAR ***");

                        // Register or Update after checking if this is Negotiation or Renegotiation!
                        if (renegotiationFlag == false) {
                            LOGGER.info("*** Before registering own SLA in SLAR ***");
                            register.register(slaCreatedWithSWServiceProvider, null, SLAState.OBSERVED);
                            LOGGER.info("*** Own SLA registered successfully in SLAR ***");
                        }
                        else {
                            LOGGER.info("*** Before updating own SLA in SLAR ***");
                            register.update(softwareSLA.getUuid(), slaCreatedWithSWServiceProvider, null,
                                    SLAState.OBSERVED);
                            LOGGER.info("*** Own SLA updated successfully in SLAR ***");
                        }
                    }
                    catch (SLAManagerContextException e) {
                        e.printStackTrace();
                    }
                    catch (RegistrationFailureException e) {
                        e.printStackTrace();
                    }
                    catch (UpdateFailureException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    LOGGER.info("*** Agreement could not be reached. No SLA Created ***");
                    // Cancel reserved resources:
                    try {
                        LOGGER.info("*** Cancelling reserved resources***");
                        softwareServiceManagerFacade.cancelReservation(serviceBuilder);
                    }
                    catch (ReservationException e) {
                        LOGGER
                                .info("*** Reservation Exception while trying to cancel service builder reservation. ***");
                        e.printStackTrace();
                    }
                }
            }
        }
        else {
            // 2.2 ( This may be needed iff the Protocol allows "First-And-Final-Offer" style of neogtiation )
            LOGGER.info("*** Invoking createAgreement() without preceeding negotiate() ***");
            ServiceBuilder serviceBuilder = null;

            if (renegotiationFlag == false) {
                assert (slaTemplate.getPropertyValue(sla.service_type) != null) : "SERVICE_TYPE annotation is not specified.";
                String serviceTypeID = slaTemplate.getPropertyValue(sla.service_type);
                LOGGER.info("*** negotiate() - slaTemplate.service_type = " + serviceTypeID);
                ServiceType serviceType = getServiceType(serviceTypeID);
                ServiceImplementation serviceImplementation = getServiceImplementation(serviceType);
                serviceBuilder = this.softwareServiceManagerFacade.createBuilder(serviceImplementation);
                assert (serviceBuilder != null) : "SoftwareServiceManager returned a null ServiceBuilder.";
                slaTemplate.setPropertyValue(sla.service_id, serviceBuilder.getUuid());
                LOGGER.info("*** serviceBuilder.getUuid() = " + serviceBuilder.getUuid());
                serviceBuilder = addServiceBindings(serviceBuilder);
            }
            else {
                // Reuse ServiceBuilder associated to the SLA being renegotiated:
                assert (softwareSLA.getPropertyValue(sla.service_id) != null) : "SERVICE_ID annotation is not specified in SLA being renegotiated.";
                serviceBuilder =
                        this.softwareServiceManagerFacade.getBuilder(softwareSLA.getPropertyValue(sla.service_id));
            }

            serviceBuilder = negotiateWithDependentServiceProvider(serviceBuilder, negotiationID);

            // From here on: same procedure as was above - check if there are dependencies:
            if (serviceBuilder.getBindingsLength() > 0) {
                // Yes Dependency! Examine the Composition as per Acceptance Criteria (used at negotiation time) - if
                // passed, create Agreement.
                if (isCompositionAcceptableAsPerAcceptanceCriteria(slaTemplate, serviceBuilder)) {
                    // create Agreement
                    // First, creating an agreement with self, if successful, only then with DSP.
                    slaCreatedWithSWServiceProvider = createSLAFromSLAT(slaTemplate);
                    // Needed for provisioning.
                    slaCreatedWithSWServiceProvider.setPropertyValue(sla.service_id, serviceID);
                    LOGGER.info("*** Agreement with MySelf reached. Proceeding with DSP ***");
                    // ORC specific single template.
                    SLATemplate dependentSLATemplate = serviceBuilder.getBindings()[0].getSlaTemplate();

                    String dependentNegId = dependentSLATemplate.getPropertyValue(DSP_NEG_ID);
                    if (dependentNegId != null) {
                        LOGGER.info("*** dsp_neg_id = " + dependentNegId);
                    }
                    else {
                        LOGGER.info("*** dsp_neg_id not annotated ***");
                    }
                    slaCreatedWithDependentServiceProvider =
                            agreeWithDependentServiceProvider(dependentNegId, dependentSLATemplate);
                    // Agreement with Dependent Provider could not be reached - therefore quitting negotiation by
                    // cancelling own agreement (reached above) and returning null.
                    if (slaCreatedWithDependentServiceProvider == null) {
                        LOGGER.info("*** Agreement with DSP could not be reached. No SLA Created ***");
                        slaCreatedWithSWServiceProvider = null;
                    }
                    else {
                        LOGGER.info("*** Agreement reached. SLA created ***");
                        try {
                            IMonitoringManager manager = this.context.getMonitorManager();
                            MonitoringSystemConfiguration monitoringSystemConfiguration =
                                    manager.checkMonitorability(slaCreatedWithSWServiceProvider, serviceBuilder
                                            .getImplementation().getComponentMonitoringFeatures());
                            serviceBuilder.setMonitoringSystemConfiguration(monitoringSystemConfiguration);

                            LOGGER.info("*** Confirming/Booking reservation as SLA is created successfully!");
                            try {
                                // PAC to extract serviceBuilder by querying on service_id. POC does not require service
                                // endpoints!
                                LOGGER.info("*** Booking ServiceBuilder. ***");
                                softwareServiceManagerFacade.book(serviceBuilder);
                            }
                            catch (BookingException e) {
                                LOGGER.info("*** Booking service builder failed!");
                                e.printStackTrace();
                            }

                            SLARegistry registry = this.context.getSLARegistry();
                            IRegister register = registry.getIRegister();

                            LOGGER.info("*** Before saving own SLA in SLAR ***");

                            // Register or Update after checking if this is Negotiation or Renegotiation!
                            if (renegotiationFlag == false) {
                                LOGGER.info("*** Before registering own SLA in SLAR ***");
                                register.register(slaCreatedWithSWServiceProvider,
                                        new UUID[] { slaCreatedWithDependentServiceProvider.getUuid() },
                                        SLAState.OBSERVED);
                                LOGGER.info("*** Own SLA registered successfully in SLAR ***");
                            }
                            else {
                                LOGGER.info("*** Before updating own SLA in SLAR ***");
                                register.update(softwareSLA.getUuid(), slaCreatedWithSWServiceProvider,
                                        new UUID[] { slaCreatedWithDependentServiceProvider.getUuid() },
                                        SLAState.OBSERVED);
                                LOGGER.info("*** Own SLA updated successfully in SLAR ***");
                            }
                        }
                        catch (SLAManagerContextException e) {
                            e.printStackTrace();
                        }
                        catch (RegistrationFailureException e) {
                            e.printStackTrace();
                        }
                        catch (UpdateFailureException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    LOGGER.info("*** Agreement could not be reached. No SLA Created ***");
                }
            }
            else {
                // No Dependency.
                if (isAcceptableAsPerAcceptanceCriteria(slaTemplate)) {
                    // create Agreement
                    slaCreatedWithSWServiceProvider = createSLAFromSLAT(slaTemplate);
                    // Needed for provisioning.
                    slaCreatedWithSWServiceProvider.setPropertyValue(sla.service_id, serviceID);
                    LOGGER.info("*** Agreement reached. SLA created ***");
                    try {
                        IMonitoringManager manager = this.context.getMonitorManager();
                        MonitoringSystemConfiguration monitoringSystemConfiguration =
                                manager.checkMonitorability(slaCreatedWithSWServiceProvider, serviceBuilder
                                        .getImplementation().getComponentMonitoringFeatures());
                        serviceBuilder.setMonitoringSystemConfiguration(monitoringSystemConfiguration);
                        LOGGER.info("*** Confirming/Booking reservation as SLA is created successfully!");
                        try {
                            // PAC to extract serviceBuilder by querying on service_id. POC does not require service
                            // endpoints!
                            LOGGER.info("*** Booking ServiceBuilder. ***");
                            softwareServiceManagerFacade.book(serviceBuilder);
                        }
                        catch (BookingException e) {
                            LOGGER.info("*** Booking service builder failed!");
                            e.printStackTrace();
                        }

                        SLARegistry registry = this.context.getSLARegistry();
                        IRegister register = registry.getIRegister();
                        LOGGER.info("*** Before saving own SLA in SLAR ***");

                        // Register or Update after checking if this is Negotiation or Renegotiation!
                        if (renegotiationFlag == false) {
                            LOGGER.info("*** Before registering own SLA in SLAR ***");
                            register.register(slaCreatedWithSWServiceProvider, null, SLAState.OBSERVED);
                            LOGGER.info("*** Own SLA registered successfully in SLAR ***");
                        }
                        else {
                            LOGGER.info("*** Before updating own SLA in SLAR ***");
                            register.update(softwareSLA.getUuid(), slaCreatedWithSWServiceProvider, null,
                                    SLAState.OBSERVED);
                            LOGGER.info("*** Own SLA updated successfully in SLAR ***");
                        }

                    }
                    catch (SLAManagerContextException e) {
                        e.printStackTrace();
                    }
                    catch (RegistrationFailureException e) {
                        e.printStackTrace();
                    }
                    catch (UpdateFailureException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    LOGGER.info("*** Agreement could not be reached. No SLA Created ***");
                }
            }
        }

        return slaCreatedWithSWServiceProvider;
    }

    public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate) {
        LOGGER.info("*** negotiate() ");
        SLATemplate[] counterOffers = new SLATemplate[] {};// Empty at the begining.
        ServiceBuilder serviceBuilder = null;
        // If an SLA exists in our SLAR with SLA.UUID = negotiationID -> invoke renegotiate on DSP,
        // otherwise initiate:
        boolean renegotiationFlag = false;
        SLA dependentSLA = null;
        SLA softwareSLA = null;
        SLA[] softwareSLAs = null;
        try {
//            LOGGER.info("Consulting KB-1");
//            List<InfrastructureServiceTerms> iSTerms = Translator.getInstance().queryKnowledgeBase(slaTemplate, 1);
//            if (iSTerms.size() > 0) {
//                LOGGER.info("Found following feasible Infrastructure solutions:");
//                for (InfrastructureServiceTerms iSTerm : iSTerms) {
//                    LOGGER.info("VM.cpuCores = " + iSTerm.getCpuCores());
//                    LOGGER.info("VM.cpuSpeed = " + iSTerm.getCpuSpeed());
//                    LOGGER.info("VM.memory = " + iSTerm.getMemory());
//                }
//            }
//            else {
//                LOGGER.info("Consulting KB-2");
//                iSTerms = Translator.getInstance().queryKnowledgeBase(slaTemplate, 2);
//                if (iSTerms.size() > 0) {
//                    LOGGER.info("Found following feasible Software counter-offers:");
//                    for (InfrastructureServiceTerms iSTerm : iSTerms) {
//                        LOGGER.info("PaymentServiceOperation.CompletionTime = "
//                                + iSTerm.getPaymentServiceOperation().getCompletionTime());
//                        LOGGER.info("ProductDetailsOperation.CompletionTime = "
//                                + iSTerm.getProductDetailsOperation().getCompletionTime());
//                        LOGGER.info("BookSaleOperation.CompletionTime = "
//                                + iSTerm.getBookSaleOperation().getCompletionTime());
//
//                        LOGGER.info("VM.cpuCores = " + iSTerm.getCpuCores());
//                        LOGGER.info("VM.cpuSpeed = " + iSTerm.getCpuSpeed());
//                        LOGGER.info("VM.memory = " + iSTerm.getMemory());
//                    }
//                }
//                else {
//                    LOGGER.info("No feasible Infrastructure or counter-offer solution found!");
//                }
//            }
            SLARegistry slaRegistry = this.context.getSLARegistry();
            softwareSLAs = slaRegistry.getIQuery().getSLA(new UUID[] { new UUID(negotiationID) });

            if (softwareSLAs != null && softwareSLAs.length > 0) {
                softwareSLA = softwareSLAs[0];
                renegotiationFlag = true;
                // Extract dependentSLA.
                UUID[] dependentUUIDs = slaRegistry.getIQuery().getDependencies(softwareSLA.getUuid());
                // Length of dependentUUIDs will always be max 1:
                if (dependentUUIDs != null && dependentUUIDs.length > 0) {
                    dependentSLA = slaRegistry.getIQuery().getSLA(dependentUUIDs)[0];
                }
            }
        }
        catch (OutOfMemoryError oOME) {
            LOGGER
                    .info("*** OutOfMemoryError - Make sure your Java Process uses mimimum settings:\n-Xms128m -Xmx764m -XX:PermSize=384m -XX:MaxPermSize=856");
            oOME.printStackTrace();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDException e) {
            e.printStackTrace();
        }

        LOGGER.info("*** negotiate() - renegotiationFlag = " + renegotiationFlag);

        if (renegotiationFlag == false) {
            // Create ServiceBuilder from the scratch:
            assert (slaTemplate.getPropertyValue(sla.service_type) != null) : "SERVICE_TYPE annotation is not specified in SLATemplae.";
            String serviceTypeID = slaTemplate.getPropertyValue(sla.service_type);
            LOGGER.info("*** negotiate() - slaTemplate.service_type = " + serviceTypeID);
            ServiceType serviceType = getServiceType(serviceTypeID);
            ServiceImplementation serviceImplementation = getServiceImplementation(serviceType);
            // The capacityCheck to be introduced only when more is known on licenses of B2-ORC software service.
            // if(this.softwareServiceManagerFacade.capacityCheck(serviceImplementation)>0)
            serviceBuilder = this.softwareServiceManagerFacade.createBuilder(serviceImplementation);
            LOGGER.info("*** serviceBuilder.getUuid() = " + serviceBuilder.getUuid());
            serviceBuilder = addServiceBindings(serviceBuilder);
        }
        else {
            // Reuse ServiceBuilder associated to the SLA being renegotiated:
            assert (softwareSLA.getPropertyValue(sla.service_id) != null) : "SERVICE_ID annotation is not specified in SLA being renegotiated.";
            serviceBuilder = this.softwareServiceManagerFacade.getBuilder(softwareSLA.getPropertyValue(sla.service_id));
        }

        assert (serviceBuilder != null) : "SoftwareServiceManager returned a null ServiceBuilder.";
        if (serviceBuilder != null) {
            slaTemplate.setPropertyValue(sla.service_id, serviceBuilder.getUuid());
            if (serviceBuilder.getBindings() != null && serviceBuilder.getBindings().length > 0) {
                LOGGER.info("*** serviceBuilder.getBindings().length = " + serviceBuilder.getBindings().length);
                // Contacting SE.Prediction - this provides homework before we actually go to Dependent Service's
                // Provider
                // to make an Offer.
                // If acceptable() as per own planning & evaluation from SSE: Create Counter Offer(s) - for MultiRound
                // Negotiation scenario only. for ORC, return 1 CounterOffer only.
                counterOffers = createCounterOfferPerServiceCompositionAcceptanceCriteria(serviceBuilder, slaTemplate);
                // For ORC usecase : negotiate now with Infrastructure Provider via PE. For now, we accept IT as such.
                // Proceed as Neg:1*initiate()->1*negotiate() expecting INF-POC to
                // reserve infrastructure resources we need.
                serviceBuilder = negotiateWithDependentServiceProvider(serviceBuilder, negotiationID);
            }
            else {
                // else means there is NO dependency, hence no Composition.
                counterOffers = createCounterOfferPerAcceptanceCriteria(slaTemplate);
            }
            // Reserve or updateReserve resources before returning:
            try {
                // Tentative reservation for half hour. No standard reservation period for such tentative
                LOGGER.info("*** Making Tentative reservation for half hour before returning Counter Offer");
                // Check first if resources were already booked?
                ServiceState serviceState = softwareServiceManagerFacade.queryServiceStatus(serviceBuilder);
                LOGGER.info("*** ServiceState = " + serviceState);
                if (serviceState == ServiceState.eRESOURCES_BOOKED) {
                    // cancel earlier booking, but dont deconfigure monitoring or stopservice yet.
                    try {
                        LOGGER.info("*** BEFORE cancelBooking");
                        this.softwareServiceManagerFacade.cancelBooking(serviceBuilder);
                        LOGGER.info("*** AFTER cancelBooking");
                        LOGGER.info("*** New ServiceState = "
                                + softwareServiceManagerFacade.queryServiceStatus(serviceBuilder));
                    }
                    catch (BookingException e) {
                        e.printStackTrace();
                    }
                }

                softwareServiceManagerFacade.reserve(serviceBuilder, new java.util.Date(System.currentTimeMillis()),
                        new java.util.Date(System.currentTimeMillis() + 1800000));

            }
            catch (ReservationException e1) {
                e1.printStackTrace();
            }
        }
        else {
            LOGGER.info("SoftwareServiceManager returned a null ServiceBuilder.");
        }
        return counterOffers;
    }

    public SLA provision(UUID slaID) {
        /**
         * 1) Get SLA(s) against slaID from local SLA Registry. 2) Provision dependent resources if any, then
         * provision() own resources via PAC. 3) Return the SLA - containing provisioned info (EPRs of configured
         * service instances) - ask Beatriz on this.
         */
        SLA slaCreatedWithSWServiceProvider = null;
        UUID[] dependentUUID = null;
        SLA slaCreatedWithDependentServiceProvider = null;
        ProvisioningAdjustment pac = null;
        try {
            SLARegistry registry = this.context.getSLARegistry();
            IQuery query = registry.getIQuery();
            slaCreatedWithSWServiceProvider = query.getSLA(new UUID[] { slaID })[0];
            dependentUUID = query.getDependencies(slaID);
            slaCreatedWithDependentServiceProvider = query.getSLA(dependentUUID)[0];
            pac = this.context.getProvisioningAdjustment();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDException e) {
            e.printStackTrace();
        }

        String serviceID = slaCreatedWithSWServiceProvider.getPropertyValue(sla.service_id);
        if (serviceID != null) {
            // Provision resources - First the Dependent and then own.
            if (dependentUUID != null && dependentUUID.length > 0) {
                // There are Dependencies. Invoke provision() on DSP:
                LOGGER.info("*** Provision DSP resources ***");
                slaCreatedWithDependentServiceProvider = provisionDependentServiceProvider(dependentUUID[0]);// ORC has
                // only 1
                // dependency.
                // Dependent-SLA is updated with provisioned (service instance) information. Hence, updating own copy in
                // SLAR:
                try {
                    SLARegistry registry = this.context.getSLARegistry();
                    IRegister register = registry.getIRegister();
                    register.update(slaCreatedWithDependentServiceProvider.getUuid(),
                            slaCreatedWithDependentServiceProvider, null, SLAState.OBSERVED);
                }
                catch (SLAManagerContextException e) {
                    e.printStackTrace();
                }
                catch (UpdateFailureException e) {
                    e.printStackTrace();
                }
            }
            LOGGER.info("*** Provision own resources ***");
            // 1- Prepare Plan for PAC
            // 2- PAC must do:
            // SoftwareServiceManagerFacade.book(serviceID)
            // Execute Plan

            // Now update SLA in SLAR:
            try {
                SLARegistry registry = this.context.getSLARegistry();
                IRegister register = registry.getIRegister();
                register.update(slaCreatedWithSWServiceProvider.getUuid(), slaCreatedWithSWServiceProvider,
                        new UUID[] { slaCreatedWithDependentServiceProvider.getUuid() }, SLAState.OBSERVED);
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
            }
            catch (UpdateFailureException e) {
                e.printStackTrace();
            }
        }
        else {
            LOGGER.info("*** SWPOC: SLA does not contain service_id annotation. Cannot proceed with provisioning.");
        }

        return slaCreatedWithSWServiceProvider;
    }

    public boolean terminate(UUID slaID, List<TerminationReason> terminationReasons) {
        boolean myResult = false;
        boolean dependentResult = false;
        /**
         * Program Logic: 1) Update SLA status to expired in SLARegistry. 2) Inform SSM to cancel booking, deconfigure
         * Monitoring and kill service instance. 3) Inform PAC to unprovision.
         */
        SLARegistry registry = null;
        ProvisioningAdjustment pac = null;
        SLA swSLAToTerminate = null;
        SLA dependentSLAToTerminate = null;
        ServiceBuilder serviceBuilder =
                this.softwareServiceManagerFacade.getBuilder(swSLAToTerminate.getPropertyValue(sla.service_id));

        // Retrieve SLA from SLAR and Terminate agreement with DSP if any:
        try {
            registry = this.context.getSLARegistry();
            swSLAToTerminate = registry.getIQuery().getSLA(new UUID[] { slaID })[0];

            UUID[] dependentUUIDs = registry.getIQuery().getDependencies(slaID); // Will always be 1.
            if (dependentUUIDs != null && dependentUUIDs.length > 0) {
                dependentSLAToTerminate = registry.getIQuery().getSLA(dependentUUIDs)[0];

                dependentResult =
                        terminateAgreementWithDependentServiceProvider(dependentSLAToTerminate.getUuid(),
                                terminationReasons);
            }
            else {
                dependentResult = true;
            }

            registry.getIRegister().update(slaID, swSLAToTerminate, dependentUUIDs, SLAState.EXPIRED);
            pac = this.context.getProvisioningAdjustment();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        catch (UpdateFailureException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDException e) {
            e.printStackTrace();
        }

        // Update SSM, MM and PAC:
        try {
            this.softwareServiceManagerFacade.cancelBooking(serviceBuilder);
        }
        catch (BookingException e) {
            e.printStackTrace();
        }
        this.softwareServiceManagerFacade.deconfigureMonitoring(serviceBuilder);
        this.softwareServiceManagerFacade.stopServiceInstance(serviceBuilder);
        try {
            pac.cancelExecution(swSLAToTerminate.getPropertyValue(this.PLAN_ID));
        }
        catch (PlanNotFoundException e) {
            e.printStackTrace();
        }

        // If this point is reached without Exceptions from SLAR,SSM,MM and PAC, SWPOC's own termination result is true.
        myResult = true;
        return (myResult & dependentResult);
    }

    /************************** Utility Methods *******************************/

    public SLAManagerContext getContext() {
        return context;
    }

    public void setContext(SLAManagerContext context) {
        this.context = context;
        LOGGER.info("*** setContext ***");
    }

    private ServiceBuilder negotiateWithDependentServiceProvider(ServiceBuilder serviceBuilder, String negotiationID) {
        SLATemplate[] infrastructureSLATemplates = null;
        INegotiation negotiationInterface = null;
        try {
            negotiationInterface = this.context.getProtocolEngine().getINegotiation();
        }
        catch (SLAManagerContextException e1) {
            e1.printStackTrace();
        }
        if (negotiationInterface != null) {
            LOGGER.info("*** SWPOC: NEGOTIATION INTERFACE = " + negotiationInterface);
            // Checking Scenario : Negotiation or Renegotiation
            boolean renegotiationFlag = false;
            SLA dependentSLA = null;
            SLA softwareSLA = null;
            SLA[] softwareSLAs = null;
            try {
                SLARegistry slaRegistry = this.context.getSLARegistry();
                softwareSLAs = slaRegistry.getIQuery().getSLA(new UUID[] { new UUID(negotiationID) });

                if (softwareSLAs != null && softwareSLAs.length > 0) {
                    softwareSLA = softwareSLAs[0];
                    LOGGER.info("*** SWPOC: SWSLA found in SLAR with UUID = " + softwareSLA.getUuid());
                    renegotiationFlag = true;
                    // Extract dependentSLA.
                    UUID[] dependentUUIDs = slaRegistry.getIQuery().getDependencies(softwareSLA.getUuid());
                    // Length of dependentUUIDs will always be max 1:
                    if (dependentUUIDs != null && dependentUUIDs.length > 0) {
                        dependentSLA = slaRegistry.getIQuery().getSLA(dependentUUIDs)[0];
                        LOGGER.info("*** SWPOC: DSLA found in SLAR with UUID = " + dependentSLA.getUuid());
                    }
                }
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
            }
            catch (InvalidUUIDException e) {
                e.printStackTrace();
            }

            LOGGER.info("*** negotiateWithDependentServiceProvider - renegotiationFlag = " + renegotiationFlag);

            try {
                SLATemplate dependentSLATemplate = serviceBuilder.getBindings()[0].getSlaTemplate();
                LOGGER.info("*** SWPOC: DEPENDENT TEMPLATE = " + dependentSLATemplate);
                Party[] parties = dependentSLATemplate.getParties();
                Party[] modifiedParties = new Party[parties.length + 1];
                int index = 0;
                for (Party party : parties) {
                    modifiedParties[index] = party;
                    index++;
                    if (party.getAgreementRole().equals(sla.provider)) {

                        assert (party.getPropertyValue(sla.gslam_epr) != null) : "Provider Party GSLAM_EPR was null.";
                        if (party.getPropertyValue(sla.gslam_epr) != null) {
                            LOGGER.info("***  SWPOC: Provider Party GSLAM_EPR : "
                                    + party.getPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr));
                        }
                        else {
                            /*
                             * LOGGER.info("***  SWPOC: setting party.setPropertyValue(gslam_epr) having ROLE = " +
                             * party.getAgreementRole().toString()); party.setPropertyValue(sla.gslam_epr,
                             * "http://localhost:8080/services/ISNegotiation?wsdl");
                             */
                            LOGGER.info("***  SWPOC: Provider Party GSLAM_EPR was null.");
                        }
                    }
                    else {
                        continue;
                    }
                }
                // Add own Party info; acting as a client to Infrastructure-Service-Provider:
                Party customerParty = null;
                try {
                    customerParty = new Party(new ID(this.context.getSLAManagerID() + "-Customer"), sla.customer);
                    customerParty.setPropertyValue(sla.gslam_epr, this.context.getEPR());
                }
                catch (SLAManagerContextException e) {
                    e.printStackTrace();
                }
                modifiedParties[index] = customerParty;
                dependentSLATemplate.setParties(modifiedParties);

                String internalNegID = null;
                if (renegotiationFlag) {
                    try {
                        LOGGER.info("*** SWPOC: BEFORE invoking DSP.renegotiate with DSLA-UUID = "
                                + dependentSLA.getUuid());
                        internalNegID = negotiationInterface.renegotiate(dependentSLA.getUuid());
                        LOGGER.info("*** SWPOC: AFTER invoking DSP.renegotiate : ReNeg-ID = " + internalNegID);
                    }
                    catch (SLANotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    LOGGER.info("*** SWPOC: BEFORE invoking DSP.initiateNegotiation ");
                    internalNegID = negotiationInterface.initiateNegotiation(dependentSLATemplate);
                    LOGGER.info("*** SWPOC: AFTER invoking DSP.initiateNegotiation : Neg-ID = " + internalNegID);
                    LOGGER.info("*** SWPOC: NEGOTIATION TEMPLATE USED = " + dependentSLATemplate);
                }

                assert (internalNegID != null) : "Null Negotiation ID received from DSP.";
                if (internalNegID != null) {
                    LOGGER.info("*** SWPOC: BEFORE invoking negotiate on DSP");
                    infrastructureSLATemplates = negotiationInterface.negotiate(internalNegID, dependentSLATemplate);// Expecting
                    // 1 SLATemplate (CounterOffer) - Single Round
                    assert (infrastructureSLATemplates != null) : "DSP.negotiate did not return any Counter Offers (Infrastructure SLATemplates.";
                    LOGGER.info("*** SWPOC: AFTER invoking negotiate on DSP");
                    if (infrastructureSLATemplates != null && infrastructureSLATemplates.length > 0) {
                        // Store this ID as annotation in the template of choice:
                        infrastructureSLATemplates[0].setPropertyValue(DSP_NEG_ID, internalNegID);
                        // Updating serviceBuilder
                        serviceBuilder.getBindings()[0].setSlaTemplate(infrastructureSLATemplates[0]);
                        LOGGER.info("*** SWPOC: DSP template annotated with DSP_NEG_ID");
                    }
                    else{
                        LOGGER.info("*** SWPOC: DSP.negotiate did not return any Counter Offers (Infrastructure SLATemplates.");
                    }
                }
                else {
                    LOGGER.info("*** SWPOC: Negotiation ID not received from DSP.");
                }
            }
            catch (OperationInProgressException e) {
                e.printStackTrace();
            }
            catch (OperationNotPossibleException e) {
                e.printStackTrace();
            }
            catch (InvalidNegotiationIDException e) {
                e.printStackTrace();
            }
            catch(java.lang.Exception e){
                e.printStackTrace();
            }
        }
        else {
            LOGGER.info("*** SWPOC: NEGOTIATION INTERFACE WAS NULL ***");
        }
        return serviceBuilder;
    }

    private SLA agreeWithDependentServiceProvider(String negotiationID, SLATemplate dependentSLATemplate) {
        SLA sla = null;
        INegotiation negotiationInterface = null;
        try {
            negotiationInterface = this.context.getProtocolEngine().getINegotiation();
        }
        catch (SLAManagerContextException e1) {
            e1.printStackTrace();
        }
        assert (negotiationInterface != null) : "Negotiation Interface was null. Cannot invoke createAgreement on DSP.";
        if (negotiationInterface != null) {
            LOGGER.info("*** SWPOC: NEGOTIATION INTERFACE = " + negotiationInterface);
            try {
                LOGGER.info("*** SWPOC: Before invoking DSP.createAgreement ");
                sla = negotiationInterface.createAgreement(negotiationID, dependentSLATemplate);
                LOGGER.info("*** SWPOC: After invoking DSP.createAgreement ");
            }
            catch (OperationInProgressException e) {
                e.printStackTrace();
            }
            catch (SLACreationException e) {
                e.printStackTrace();
            }
            catch (InvalidNegotiationIDException e) {
                e.printStackTrace();
            }
        }
        return sla;
    }

    private SLA provisionDependentServiceProvider(UUID slaID) {
        SLA provisionedDependentSLA = null;
        INegotiation negotiationInterface = null;
        try {
            negotiationInterface = this.context.getProtocolEngine().getINegotiation();
        }
        catch (SLAManagerContextException e1) {
            e1.printStackTrace();
        }
        if (negotiationInterface != null) {
            LOGGER.info("*** SWPOC: NEGOTIATION INTERFACE = " + negotiationInterface);
            try {
                LOGGER.info("*** SWPOC: Before invoking DSP.provision ");
                provisionedDependentSLA = negotiationInterface.provision(slaID);
                LOGGER.info("*** SWPOC: After invoking DSP.provision ");
            }
            catch (SLANotFoundException e) {
                e.printStackTrace();
            }
            catch (ProvisioningException e) {
                e.printStackTrace();
            }
        }
        return provisionedDependentSLA;
    }

    private boolean terminateAgreementWithDependentServiceProvider(UUID slaID,
            List<TerminationReason> terminationReasons) {
        boolean result = false;
        INegotiation negotiationInterface = null;
        try {
            negotiationInterface = this.context.getProtocolEngine().getINegotiation();
        }
        catch (SLAManagerContextException e1) {
            e1.printStackTrace();
        }
        if (negotiationInterface != null) {
            LOGGER.info("*** NEGOTIATION INTERFACE = " + negotiationInterface);
            try {
                result = negotiationInterface.terminate(slaID, terminationReasons);
            }
            catch (SLANotFoundException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private SLATemplate[] createCounterOfferPerAcceptanceCriteria(SLATemplate targetServiceTemplate) {
        /**
         * 1- Process Agreement Terms, Create and return CounterOffer(s).
         */
        if (isAcceptableAsPerAcceptanceCriteria(targetServiceTemplate)) {
            return new SLATemplate[] { targetServiceTemplate };
        }
        else {
            return null;
        }
    }

    private SLATemplate[] createCounterOfferPerServiceCompositionAcceptanceCriteria(ServiceBuilder serviceBuilder,
            SLATemplate targetServiceTemplate) {
        /**
         * 1- Process & Set Agreement Terms to create Composition(s). 2- Invoke SE using above Composition(s) & evaluate
         * its results. 3- Decide on one or more Compositions as Acceptable and return as CounterOffers.
         */
        Composition composition = createComposition(targetServiceTemplate, serviceBuilder);
        ISoftwareServiceEvaluation se = new SoftwareServiceEvaluator();
        Set<ServiceBuilder> serviceBuilderSet = new HashSet<ServiceBuilder>();
        Set<IEvaluationResult> resultSet = new HashSet<IEvaluationResult>();
        serviceBuilderSet.add(composition.getServiceBuilder());
        try {
            resultSet = se.evaluate(serviceBuilderSet, composition.getTargetServiceTemplate());
            LOGGER.info("*** resultSet.size = " + resultSet.size());
        }
        catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        // Process results and choose per own planning:
        if (resultSet != null && resultSet.size() > 0) {
            for (IEvaluationResult result : resultSet) {
                LOGGER.info("*** IEvaluationResult.class = " + result.getClass().getName());

                ServiceBuilder sb = result.getBuilder();
                LOGGER.info("*** IEvaluationResult.getBuilder().getUuid() = " + sb.getUuid());

                // IResultSet realResultSet = result.getResults();
                // LOGGER.info("*** IResultSet.class = " + realResultSet.getClass().getName());
                // if (realResultSet != null && realResultSet.getResults() != null) {
                List<ISingleResult> listOfSimpleResults = result.getResults();
                LOGGER.info("*** listOfSimpleResults.size = " + listOfSimpleResults.size());
                for (ISingleResult iSimpleResult : listOfSimpleResults) {
                    LOGGER.info("*** iSimpleResult.getTerm().getValue() = " + iSimpleResult.getTerm().getValue());
                    LOGGER.info("*** iSimpleResult.getValue() = " + iSimpleResult.getValue());
                }
                // }
            }
        }

        return new SLATemplate[] { targetServiceTemplate };// For ORC, we are sure of this.
    }

    private Composition createComposition(SLATemplate targetServiceTemplate, ServiceBuilder serviceBuilder) {
        if (isCompositionAcceptableAsPerAcceptanceCriteria(targetServiceTemplate, serviceBuilder)) {
            return new Composition(targetServiceTemplate, serviceBuilder);
        }
        return null;
    }

    private boolean isCompositionAcceptableAsPerAcceptanceCriteria(SLATemplate slaTemplate,
            ServiceBuilder serviceBuilder) {
        return true;
    }

    private boolean isAcceptableAsPerAcceptanceCriteria(SLATemplate slaTemplate) {
        return true;
    }

    protected ServiceType getServiceType(String serviceTypeID) {
        ServiceType requiredServiceType = null;
        List<ServiceType> serviceTypes = this.softwareServiceManagerFacade.queryServiceTypes();
        for (ServiceType serviceType : serviceTypes) {
            LOGGER.info("*** getServiceType - ID = " + serviceType.getID());
            LOGGER.info("*** getServiceType - ServiceTypeName = " + serviceType.getServiceTypeName());
            if (serviceType.getID().equals(serviceTypeID)) {
                requiredServiceType = serviceType;
                break;
            }
        }
        assert (requiredServiceType != null) : "SoftwareServiceManager does not support given SERVICE_TYPE";
        return requiredServiceType;
    }

    protected ServiceImplementation getServiceImplementation(ServiceType serviceType) {
        if (serviceType == null) {
            LOGGER
                    .info("*** ServiceType is NULL. Template did not provide service_type annotation. Cannot Process this Offer. Aborting ***");
            System.exit(-1);
        }
        List<ServiceImplementation> serviceImplementations =
                this.softwareServiceManagerFacade.queryServiceImplementations(serviceType);
        LOGGER.info("*** getServiceImplementation - length = " + serviceImplementations.size());
        LOGGER.info("*** getServiceImplementation - choosing = " + serviceImplementations.get(0).getDescription());
        assert (serviceImplementations != null) : "SoftwareServiceManager did not provide any ServiceImplementations for given SERVICE_TYPE";
        return serviceImplementations.get(0);// No preference criteria for now.
    }

    protected ServiceBuilder addServiceBindings(ServiceBuilder serviceBuilder) {
        List<Dependency> dependencies = serviceBuilder.getOpenDependencies();
        if (dependencies.size() > 0) {
            LOGGER.info("*** addServiceBindings - serviceBuilder.getOpenDependencies.size = "
                    + serviceBuilder.getOpenDependencies().size());
            Dependency openDependency = dependencies.get(0);
            LOGGER.info("*** dependencies.getID = " + openDependency.getID());
            LOGGER.info("*** dependencies.getName = " + openDependency.getName());
            LOGGER.info("*** dependencies.getTargetType = " + openDependency.getTargetType().getID());
            LOGGER.info("*** SWPOC - dependencies.getTargetURI = " + openDependency.getTargetURI());
        }
        else {
            LOGGER.info("*** No Dependencies");
        }

        for (Dependency dependency : dependencies) {
            ServiceType slaTemplateServiceType = dependency.getTargetType();
            String targetURI = dependency.getTargetURI();
            // Resolve Dependency : Retrieve SLATemplate of this dependency from SLATemplateRegistry.
            SLATemplate dependentSlaTemplate = getSLATemplateFromSLATRegistry(slaTemplateServiceType, targetURI);
            serviceBuilder.addBinding(dependency, dependentSlaTemplate);
        }
        return serviceBuilder;
    }

    protected SLATemplate getSLATemplateFromSLATRegistry(ServiceType slaTemplateServiceType, String targetURI) {
        LOGGER.info("*** SWPOC.getSLATemplateFromSLATRegistry - slaTemplateServiceType.getID() = "+slaTemplateServiceType.getID());
        // Query SLATemplate Registry:
        SLATemplateRegistry slaTemplateRegistry = null;
        SLATemplate orcINFSLATemplate = null;
        SLATemplate slaTemplate = null;
        assert (slaTemplateServiceType != null || targetURI != null) : "Either ServiceType OR TargetURI should be provided.";
        try {
            slaTemplateRegistry = this.context.getSLATemplateRegistry();
            LOGGER.info("*** SWPOC.slaTemplateRegistry = " + slaTemplateRegistry.getClass().getName());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        assert (slaTemplateRegistry != null) : "SLATR is not initialized.";
        if (slaTemplateRegistry != null) {
            LOGGER.info("*** SWPOC.slaTemplateRegistry != null");
            try {
                //Testing SLATR : Query ALL UUIDs i.e. ids of all stored Templates: //
//                UUID[] uuids = slaTemplateRegistry.query(null); 
//                LOGGER.info("*** GETTING ALL TEMPLATES FROM SLATR ***");
//                for(UUID uuid : uuids) {
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.uuid = "+uuid.getValue()); 
//                    SLATemplate fetchedSlaTemplate = slaTemplateRegistry.getSLATemplate(uuid);
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.getUuid = "+fetchedSlaTemplate.getUuid());
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.getDescr = "+fetchedSlaTemplate.getDescr());
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.uuidgetParties().length = " +fetchedSlaTemplate.getParties().length);
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.fetchedSlaTemplate.getPropertyValue(Metadata.registrar_id) = " +fetchedSlaTemplate.getPropertyValue(Metadata.registrar_id));
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.fetchedSlaTemplate.getPropertyValue(Metadata.service_type) = " +fetchedSlaTemplate.getPropertyValue(Metadata.service_type));
//                    LOGGER.info("*** SWPOC.slaTemplateRegistry.fetchedSlaTemplate.getPropertyValue(Metadata.template_uuid) = " +fetchedSlaTemplate.getPropertyValue(Metadata.template_uuid));
//               }
                // Querying SLATR on Metadata.service_type to extract dependent template:
                SimpleDomainExpr queryRHS = new SimpleDomainExpr(new ID(slaTemplateServiceType.getID()), core.equals);
                TypeConstraintExpr query = new TypeConstraintExpr(Metadata.service_type, queryRHS);
                UUID[] uuidsOfRequiredSLATemplates = slaTemplateRegistry.query(query);
                assert (uuidsOfRequiredSLATemplates != null && uuidsOfRequiredSLATemplates.length > 0) : "SLATR returned null or empty UUIDs for dependent templates!";

                if (uuidsOfRequiredSLATemplates != null && uuidsOfRequiredSLATemplates.length > 0) {
                    LOGGER.info("*** uuidsOfRequiredSLATemplates.length = " + uuidsOfRequiredSLATemplates.length);
                    // for(UUID uuID : uuidsOfRequiredSLATemplates){
                    // LOGGER.info("*** UUID = "+uuID.getValue());
                    // if(uuID != null && uuID.getValue() != null){
                    // if(!uuID.getValue().equalsIgnoreCase("ORC_InfrastructureSLAT")){
                    // //remove old template!
                    // slaTemplateRegistry.removeSLATemplate(uuID);
                    // LOGGER.info("*** AFTER REMOVING SLAT WITH UUID = "+uuID.getValue());
                    // }
                    // }
                    // }
                    // Extracting dependent template from SLATR:
                    orcINFSLATemplate = slaTemplateRegistry.getSLATemplate(uuidsOfRequiredSLATemplates[0]);
                    LOGGER.info("*** uuidsOfRequiredSLATemplates[0].ID = " + uuidsOfRequiredSLATemplates[0].getValue());
                }
                else {
                    LOGGER.info("*** SLATR returned null or empty UUIDs for dependent templates!");
//                    LOGGER.info("*** Loading SLAT from the disk");
//                    ISyntaxConverter sc = this.getContext().getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
//                    String templatePath = System.getenv("SLASOI_ORC_HOME") + System.getProperty("file.separator") + "I_SLATs" + System.getProperty("file.separator") + "ORCInfrastructure_SLA_template.xml";
//                    BufferedReader input = new BufferedReader(new FileReader(templatePath));
//                    String line = null;
//                    StringBuilder slaStringBuilder = new StringBuilder(); 
//                    while ((line = input.readLine()) != null) {
//                        slaStringBuilder.append(line); 
//                        slaStringBuilder.append(System.getProperty("line.separator")); 
//                    }
//                    orcINFSLATemplate = (SLATemplate)sc.parseSLATemplate(slaStringBuilder.toString());
//                    Metadata metaData = new Metadata();
//                    metaData.setPropertyValue(Metadata.provider_uuid, "ID-OF-PROVIDER-PARTY-GOES-HERE");
//                    metaData.setPropertyValue(Metadata.registrar_id, "MockRegistrar123");
//                    metaData.setPropertyValue(Metadata.service_type, "b2c3f591-f2ca-42b3-92a0-955ba2b36035");
//                    slaTemplateRegistry.addSLATemplate(orcINFSLATemplate, metaData);
//                    LOGGER.info("*** TEMPLATE ADDED SUCCESSFULLY IN SLATR *** ");                    
                }

                /*
                 * More complicated compound query (considering also targetURI): SimpleDomainExpr rhsSimpleDomainExpr1 =
                 * new SimpleDomainExpr(new ID(slaTemplateServiceType.getID()), core.equals); TypeConstraintExpr
                 * queryTypeConstraintExpr1 = new TypeConstraintExpr(Metadata.service_type, rhsSimpleDomainExpr1);
                 * 
                 * SimpleDomainExpr rhsSimpleDomainExpr2 = new SimpleDomainExpr(new ID(targetURI), core.equals);
                 * TypeConstraintExpr queryTypeConstraintExpr2 = new TypeConstraintExpr(Metadata.service_type,
                 * rhsSimpleDomainExpr2);
                 * 
                 * CompoundConstraintExpr queryCompoundConstraintExpr = new CompoundConstraintExpr(core.or, new
                 * ConstraintExpr[]{queryTypeConstraintExpr1,queryTypeConstraintExpr2}); UUID[]
                 * uuidsOfRequiredSLATemplates = slaTemplateRegistry.query(queryCompoundConstraintExpr);
                 * 
                 * LOGGER.info("*** Nr. of UUIDs retrieved from SLATR = "+uuidsOfRequiredSLATemplates.length);
                 */ 

                 
                slaTemplate = orcINFSLATemplate;
                LOGGER.info("*** SWPOC.slaTemplate.toString = " + slaTemplate);
            }
            catch (java.lang.Exception e) {
                e.printStackTrace();
            }

        }
        return slaTemplate;
    }

    private SLA createSLAFromSLAT(SLATemplate slaTemplate) {
        SLA sla = new SLA();
        sla = copy(sla, slaTemplate);
        return sla;
    }

    private SLA copy(SLA sla, SLATemplate slat) {
        // set Template ID
        assert (slat.getUuid() != null) : "SLAT should have a UUID";
        sla.setTemplateId(slat.getUuid());
        sla.setUuid(new UUID(java.util.UUID.randomUUID().toString()));
        // set AgreedAt
        sla.setAgreedAt(new TIME(getCurrentTime_yyyyMMMddHH_mm_ss()));
        // set effective From
        sla.setEffectiveFrom(new TIME(getEffectiveFromTime_yyyyMMMddHH_mm_ss()));
        // set effective Until
        sla.setEffectiveUntil(new TIME(getEffectiveUntilTime_yyyyMMMddHH_mm_ss()));
        // set parties
        assert (slat.getParties() != null) : "SLAT should have Parties";
        sla.setParties(slat.getParties());
        // set InterfaceDeclrs
        assert (slat.getInterfaceDeclrs() != null) : "SLAT should have InterfaceDeclrs";
        sla.setInterfaceDeclrs(slat.getInterfaceDeclrs());
        // set agreementTerms
        assert (slat.getAgreementTerms() != null) : "SLAT should have Agreement Terms.";
        sla.setAgreementTerms(slat.getAgreementTerms());

        sla.setPropertyValue(this.PLAN_ID, java.util.UUID.randomUUID().toString());
        return sla;
    }

    private Calendar getCurrentTime_yyyyMMMddHH_mm_ss() {
        final SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss", Locale.US);
        formatter.getCalendar().setTime(new Date());
        return formatter.getCalendar();
    }

    private Calendar getEffectiveFromTime_yyyyMMMddHH_mm_ss() {
        final SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss", Locale.US);
        Date date = new Date();// Current date
        formatter.getCalendar().setTime(date);
        return formatter.getCalendar();
    }

    private Calendar getEffectiveUntilTime_yyyyMMMddHH_mm_ss() {
        final SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss", Locale.US);
        Date date = new Date();
        date.setYear(date.getYear() + 2);
        formatter.getCalendar().setTime(date);
        return formatter.getCalendar();
    }

}
