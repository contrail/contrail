package org.slasoi.swslam.poc.impl.solver;

public enum RiskQuantifier {
    
    HIGH,       //"PERCENTILE_25";
    MEDIUM,     //"PERCENTILE_50";
    SOME,       //"PERCENTILE_75";
    LOW,        // "PERCENTILE_90";
    NEGLIGIBLE  //"PERCENTILE_95";
    
}
