package org.slasoi.swslam.poc.impl.solver.test;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;
import org.slasoi.seval.prediction.exceptions.UnboundDependencyException;
import org.slasoi.seval.prediction.exceptions.UnsupportedTermException;
import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.ISingleResult;
import org.slasoi.seval.prediction.service.ISingleResult.AggregationType;
import org.slasoi.seval.prediction.service.impl.SoftwareServiceEvaluator;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.swslam.poc.impl.solver.InfrastructureServiceTerms;
import org.slasoi.swslam.poc.impl.solver.SoftwareServiceOperation;
import org.slasoi.swslam.poc.impl.solver.TemplateParser;
import org.slasoi.swslam.poc.impl.solver.Translator;

public class CollectorTest
{
    public static final Logger LOGGER = Logger.getLogger(CollectorTest.class);
    private TemplateParser templateParser = new TemplateParser();
    
    private SLATemplate slaTemplate;
    public boolean ASSIGNMENT_MODE = false;
    Operation[] operations;
    private Landscape landscape;
    private HashMap<String, SoftwareServiceOperation> mapOfSSOperations =
            new HashMap<String, SoftwareServiceOperation>();

    // SW params:
    private int arrivalRateBookSale, completionTimeBookSale, arrivalRateGetProductDetails,
            completionTimeGetProductDetails, arrivalRatePaymentService, completionTimePaymentService;
    // INF params:
    private int cpuSpeed, cpuCores, memory, quantity;
    private String vmImage;

    private InfrastructureServiceTerms iSTerms = new InfrastructureServiceTerms();
    
    public CollectorTest() {
        initializeLogger();
        LOGGER.info("Inside CollectorTest()");
    }

    private void initializeLogger() {
        Properties logProperties = new Properties();
        try {
            logProperties.load(new FileReader(System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                    + "software-slamanager" + System.getProperty("file.separator") + "planning-optimization"
                    + System.getProperty("file.separator") + "log4j.properties"));
            PropertyConfigurator.configure(logProperties);
            LOGGER.info("Logging initialized.");
        }
        catch (IOException e) {
            throw new RuntimeException("Unable to load logging property ");
        }
    }

    /******* METHODS BELOW THIS LINE WILL BE USED TO BUILD ADAPTIVE KNOWLEDGE BASE IN OFFLINE MANNER *********/

    private SoftwareServiceEvaluator initServiceEvaluator() {
        LOGGER.info("initServiceEvaluator 1");
        SoftwareServiceEvaluator evaluator = new SoftwareServiceEvaluator();
        LOGGER.info("initServiceEvaluator 2");
        evaluator.setEvaluationMode(EvaluationMode.Server);
        evaluator.setEvaluationServerEndpoint("http://localhost:8082/services");
        return evaluator;
    }

    public void offlineCacheRoutine() {
        LOGGER.info("offlineCacheRoutine -  START 1");
        this.ASSIGNMENT_MODE = true;
        SoftwareServiceEvaluator evaluator = initServiceEvaluator();
        Set<ServiceBuilder> builders = initMyServiceBuilders();
        SLATemplate softwareSLATemplate = loadSLAT("software_SLA_template.xml");
        SLATemplate infrastructureSLATemplate = loadSLAT("infrastructure_SLA_template.xml");
        String header =
                "package solver;\n\n" + "import java.util.HashMap;\n" + "import java.util.List;\n"
                        + "import org.slasoi.swslam.poc.impl.solver.Event;\n"
                        + "import org.slasoi.swslam.poc.impl.solver.EventName;\n"
                        + "import org.slasoi.swslam.poc.impl.solver.RiskQuantifier;\n\n"
                        + "import org.slasoi.swslam.poc.impl.solver.SoftwareServiceOperation;\n"
                        + "import org.slasoi.swslam.poc.impl.solver.InfrastructureServiceTerms;\n\n" +

                        "global SoftwareServiceOperation bookSaleOperation;\n"
                        + "global SoftwareServiceOperation productDetailsOperation;\n"
                        + "global SoftwareServiceOperation paymentServiceOperation;\n\n";

        String initializeRule =
                "rule \"OutputEventParam_Rule\"\n\t salience 3 \n\t when \n\t\t event : Event(name == EventName.QueryEvent);"
                        + "\n\t then \n\t\t System.out.println(\"BookSaleOperation.getArrivalRate\"+bookSaleOperation.getArrivalRate());  "
                        + "\n\t\t System.out.println(\"ProductDetailsOperation.getArrivalRate\"+productDetailsOperation.getArrivalRate());"
                        + "\n\t\t System.out.println(\"PaymentServiceOperation.getArrivalRate\"+paymentServiceOperation.getArrivalRate());\nend\n\n";

        String endRule =
                "rule \"RetractEvent_Rule\"\n\t salience 1 \n\t when \n\t\t event : Event(name == EventName.QueryEvent);"
                        + "\n\t then \n\t\t retract(event);\nend\n";

        String rulesApproach1WithCT = header + initializeRule + endRule;
        String rulesApproach1WithoutCT = header + initializeRule + endRule;

        String currentRule1Approach1WithCT, currentRule2Approach1WithCT, currentRule3Approach1WithCT, currentRule4Approach1WithCT, currentRule5Approach1WithCT;
        String currentRule1Approach1WithoutCT, currentRule2Approach1WithoutCT, currentRule3Approach1WithoutCT, currentRule4Approach1WithoutCT, currentRule5Approach1WithoutCT;
        String bookSaleCTPendingThenCurrentRule1 = "", bookSaleCTPendingThenCurrentRule2 = "", bookSaleCTPendingThenCurrentRule3 =
                "", bookSaleCTPendingThenCurrentRule4 = "", bookSaleCTPendingThenCurrentRule5 = "", productDetailsCTPendingThenCurrentRule1 =
                "", productDetailsCTPendingThenCurrentRule2 = "", productDetailsCTPendingThenCurrentRule3 = "", productDetailsCTPendingThenCurrentRule4 =
                "", productDetailsCTPendingThenCurrentRule5 = "", paymentServiceCTPendingThenCurrentRule1 = "", paymentServiceCTPendingThenCurrentRule2 =
                "", paymentServiceCTPendingThenCurrentRule3 = "", paymentServiceCTPendingThenCurrentRule4 = "", paymentServiceCTPendingThenCurrentRule5 =
                "";

        String footer = "event.getListOfIsTerms().add(iServiceTerms);\nend\n";
        long startTime = System.currentTimeMillis();

        // Starting sw values
        //this.arrivalRateGetProductDetails = 50;
        //this.arrivalRateBookSale = 200;
        this.arrivalRatePaymentService = 50;
        int ruleNumber = 1;
        // Start loop A
        for (int arrivalRatePaymentServiceLoop = 1; arrivalRatePaymentServiceLoop < 2; arrivalRatePaymentServiceLoop++) { // loop
            // for
            // changes
            // in
            // arrivalRatePaymentService:
            // 1
            // iteration
            this.arrivalRateGetProductDetails = 50;
            for (int arrivalRateGetProductDetailsLoop = 1; arrivalRateGetProductDetailsLoop < 3; arrivalRateGetProductDetailsLoop++) {// loop
                // for
                // changes
                // in
                // arrivalRateGetProductDetails:
                // 2
                // iterations
                this.arrivalRateBookSale = 200;
                for (int arrivalRateBookSaleLoop = 1; arrivalRateBookSaleLoop < 5; arrivalRateBookSaleLoop++) {// loop
                    // for
                    // changes
                    // in
                    // arrivalRateBookSale:
                    // 4
                    // iterations

                    //this.templateParser.parseTemplate(softwareSLATemplate);
                    
                    // Starting hw values
                    //this.memory = 2048;
                    //this.cpuCores = 2;
                    this.cpuSpeed = 1;
                    // IM HERE;
                    for (int cpuSpeedLoop = 1; cpuSpeedLoop < 5; cpuSpeedLoop++) {// loop for changes in cpuSpeed: 5
                        
                        this.cpuCores = 2;
                        for (int cpuCoresLoop = 1; cpuCoresLoop < 6; cpuCoresLoop++) {// loop for changes in cpuCores: 6
                            
                            this.memory = 2048;
                            for (int memoryLoop = 1; memoryLoop < 5; memoryLoop++) { // loop for changes in memory: 5
                                // Setting values to SW and IF Templates:
                                
                                this.iSTerms.getPaymentServiceOperation().setArrivalRate(this.arrivalRatePaymentService);
                                this.iSTerms.getProductDetailsOperation().setArrivalRate(this.arrivalRateGetProductDetails);
                                this.iSTerms.getBookSaleOperation().setArrivalRate(this.arrivalRateBookSale);
                                
                                this.iSTerms.setCpuSpeed(this.cpuSpeed);
                                this.iSTerms.setCpuCores(this.cpuCores);
                                this.iSTerms.setMemory(this.memory);
                                
                                softwareSLATemplate = this.templateParser.assignQoSValuesToTemplate(softwareSLATemplate, iSTerms); //will parse automatically.
                                infrastructureSLATemplate = this.templateParser.assignQoSValuesToTemplate(infrastructureSLATemplate, iSTerms); //will parse automatically.
                                
                                /**************************************************************************/
                                /******** Testing Set Values in SW and INF Templates *******/
                                /**************************************************************************/
                                
                                this.mapOfSSOperations = this.templateParser.getMapOfSSOperations(softwareSLATemplate);
                                
                                LOGGER.info("/**************************************************************************/");
                                LOGGER.info("/********Testing Set Values in SW Templates*******/");
                                LOGGER.info("BookSale.ArrivalRate = " + mapOfSSOperations.get("bookSale").getArrivalRate());
                                LOGGER.info("GetProductDetails.ArrivalRate = " + mapOfSSOperations.get("getProductDetails").getArrivalRate());
                                LOGGER.info("PaymentServiceOperation.ArrivalRate = " + mapOfSSOperations.get("PaymentServiceOperation").getArrivalRate());
                                
                                LOGGER.info("/**************************************************************************/");
                                
                                LOGGER.info("/********Testing Set Values in INF Templates*******/");
                                InfrastructureServiceTerms tempISTerms;
                                tempISTerms = this.templateParser.getISTerms(infrastructureSLATemplate);
                                LOGGER.info("VM.cpuCores = " + tempISTerms.getCpuCores());
                                LOGGER.info("VM.cpuSpeed = " + tempISTerms.getCpuSpeed());
                                LOGGER.info("VM.memory = " + tempISTerms.getMemory());
                                
                                /**************************************************************************/
                                
                                //this.templateParser.parseTemplate(softwareSLATemplate);//Set SW params in SWTemplate
                                //this.templateParser.parseTemplate(infrastructureSLATemplate); // Set HW params in InfTemplate!
                                
                                bindDependentTemplateWithServiceBuilder(builders, infrastructureSLATemplate);
                                Set<IEvaluationResult> results = new HashSet<IEvaluationResult>();
                                try {
                                    LOGGER.info("############# EVALUATION STARTING ###############");
                                    results = evaluator.evaluate(builders, softwareSLATemplate);
                                }
                                catch (UnsupportedTermException e) {
                                    e.printStackTrace();
                                    fail(e.getClass().toString() + ": " + e.getMessage());
                                }
                                catch (UnboundDependencyException e) {
                                    e.printStackTrace();
                                    fail(e.getClass().toString() + ": " + e.getMessage());
                                }
                                catch (ModelNotFoundException e) {
                                    e.printStackTrace();
                                    fail(e.getClass().toString() + ": " + e.getMessage());
                                }

                                LOGGER.info("1- Size of Results = " + results.size());
                                IEvaluationResult temp = results.iterator().next();
                                List<ISingleResult> singleResults = temp.getResults();
                                LOGGER.info("2- Size of SingleResults = " + singleResults.size());
//                                SoftwareServiceOperation bookSaleOperation = this.mapOfSSOperations.get("bookSale"), 
//                                                         productDetailsOperation = this.mapOfSSOperations.get("getProductDetails"), 
//                                                         paymentServiceOperation = this.mapOfSSOperations.get("PaymentServiceOperation");
                                
                                if (singleResults != null && singleResults.size() > 0) {
                                    // Rule Name
                                    currentRule1Approach1WithCT =
                                            "\nrule \"" + arrivalRatePaymentServiceLoop + "-"
                                                    + arrivalRateGetProductDetailsLoop + "-" + arrivalRateBookSaleLoop
                                                    + "-" + cpuSpeedLoop + "-" + cpuCoresLoop + "-" + memoryLoop
                                                    + "-1_SN#" + ruleNumber + "_Rule\"\n\t salience 2 \n\t when \n\t\t";
                                    currentRule2Approach1WithCT =
                                            "\nrule \"" + arrivalRatePaymentServiceLoop + "-"
                                                    + arrivalRateGetProductDetailsLoop + "-" + arrivalRateBookSaleLoop
                                                    + "-" + cpuSpeedLoop + "-" + cpuCoresLoop + "-" + memoryLoop
                                                    + "-2_SN#" + (ruleNumber + 1)
                                                    + "_Rule\"\n\t salience 2 \n\t when \n\t\t";
                                    currentRule3Approach1WithCT =
                                            "\nrule \"" + arrivalRatePaymentServiceLoop + "-"
                                                    + arrivalRateGetProductDetailsLoop + "-" + arrivalRateBookSaleLoop
                                                    + "-" + cpuSpeedLoop + "-" + cpuCoresLoop + "-" + memoryLoop
                                                    + "-3_SN#" + (ruleNumber + 2)
                                                    + "_Rule\"\n\t salience 2 \n\t when \n\t\t";
                                    currentRule4Approach1WithCT =
                                            "\nrule \"" + arrivalRatePaymentServiceLoop + "-"
                                                    + arrivalRateGetProductDetailsLoop + "-" + arrivalRateBookSaleLoop
                                                    + "-" + cpuSpeedLoop + "-" + cpuCoresLoop + "-" + memoryLoop
                                                    + "-4_SN#" + (ruleNumber + 3)
                                                    + "_Rule\"\n\t salience 2 \n\t when \n\t\t";
                                    currentRule5Approach1WithCT =
                                            "\nrule \"" + arrivalRatePaymentServiceLoop + "-"
                                                    + arrivalRateGetProductDetailsLoop + "-" + arrivalRateBookSaleLoop
                                                    + "-" + cpuSpeedLoop + "-" + cpuCoresLoop + "-" + memoryLoop
                                                    + "-5_SN#" + (ruleNumber + 4)
                                                    + "_Rule\"\n\t salience 2 \n\t when \n\t\t";
                                    // Using same rule names in both rule files:
                                    currentRule1Approach1WithoutCT = currentRule1Approach1WithCT;
                                    currentRule2Approach1WithoutCT = currentRule2Approach1WithCT;
                                    currentRule3Approach1WithoutCT = currentRule3Approach1WithCT;
                                    currentRule4Approach1WithoutCT = currentRule4Approach1WithCT;
                                    currentRule5Approach1WithoutCT = currentRule5Approach1WithCT;

                                    for (ISingleResult result : singleResults) {
                                        Double value = Double.valueOf(result.getValue() * 1000); // converting from
                                        // seconds to
                                        // milliseconds.

                                        // BookSale Operation
                                        if (result.getServiceID().equalsIgnoreCase(
                                                "ORC_CustomerConstraintInventoryBookSale")) {
                                            // IF part
                                            if (result.getAggregationType().equals(AggregationType.PERCENTILE_25)) {
                                                String string1 =
                                                        "event : Event(name == EventName.QueryEvent, riskQuantifier == RiskQuantifier.HIGH);\n\t\t"
                                                                + "eval( bookSaleOperation != null && bookSaleOperation.getArrivalRate() > "
                                                                + this.arrivalRateBookSale + " && "
                                                                + "bookSaleOperation.getArrivalRate() <= "
                                                                + (200 + (arrivalRateBookSaleLoop * 200)) + "); \n\t\t";

                                                currentRule1Approach1WithCT += string1;
                                                currentRule1Approach1WithoutCT += string1;

                                                currentRule1Approach1WithCT +=
                                                        "eval( bookSaleOperation != null && bookSaleOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                // Pending Then part for bookSale for both Approach1WithoutCT and
                                                // Approach1WithCT rules
                                                bookSaleCTPendingThenCurrentRule1 =
                                                        "SoftwareServiceOperation bookSaleOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "bookSaleOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setBookSaleOperation(bookSaleOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_50)) {
                                                currentRule1Approach1WithCT +=
                                                        "bookSaleOperation.getCompletionTime() < " + value.intValue()
                                                                + "); \n\t\t";

                                                String string2 =
                                                        "event : Event(name == EventName.QueryEvent, riskQuantifier == RiskQuantifier.MEDIUM);\n\t\t"
                                                                + "eval( bookSaleOperation != null && bookSaleOperation.getArrivalRate() > "
                                                                + this.arrivalRateBookSale + " && "
                                                                + "bookSaleOperation.getArrivalRate() <= "
                                                                + (200 + (arrivalRateBookSaleLoop * 200)) + "); \n\t\t";

                                                currentRule2Approach1WithCT += string2;
                                                currentRule2Approach1WithoutCT += string2;

                                                currentRule2Approach1WithCT +=
                                                        "eval( bookSaleOperation != null && bookSaleOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                bookSaleCTPendingThenCurrentRule2 =
                                                        "SoftwareServiceOperation bookSaleOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "bookSaleOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setBookSaleOperation(bookSaleOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_75)) {
                                                currentRule2Approach1WithCT +=
                                                        "bookSaleOperation.getCompletionTime() < " + value.intValue()
                                                                + "); \n\t\t";

                                                String string3 =
                                                        "event : Event(name == EventName.QueryEvent, riskQuantifier == RiskQuantifier.SOME);\n\t\t"
                                                                + "eval( bookSaleOperation != null && bookSaleOperation.getArrivalRate() > "
                                                                + this.arrivalRateBookSale + " && "
                                                                + "bookSaleOperation.getArrivalRate() <= "
                                                                + (200 + (arrivalRateBookSaleLoop * 200)) + "); \n\t\t";

                                                currentRule3Approach1WithCT += string3;
                                                currentRule3Approach1WithoutCT += string3;

                                                currentRule3Approach1WithCT +=
                                                        "eval( bookSaleOperation != null && bookSaleOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                bookSaleCTPendingThenCurrentRule3 =
                                                        "SoftwareServiceOperation bookSaleOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "bookSaleOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setBookSaleOperation(bookSaleOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_90)) {
                                                currentRule3Approach1WithCT +=
                                                        "bookSaleOperation.getCompletionTime() < " + value.intValue()
                                                                + "); \n\t\t";

                                                String string4 =
                                                        "event : Event(name == EventName.QueryEvent, riskQuantifier == RiskQuantifier.LOW);\n\t\t"
                                                                + "eval( bookSaleOperation != null && bookSaleOperation.getArrivalRate() > "
                                                                + this.arrivalRateBookSale + " && "
                                                                + "bookSaleOperation.getArrivalRate() <= "
                                                                + (200 + (arrivalRateBookSaleLoop * 200)) + "); \n\t\t";

                                                currentRule4Approach1WithCT += string4;
                                                currentRule4Approach1WithoutCT += string4;

                                                currentRule4Approach1WithCT +=
                                                        "eval( bookSaleOperation != null && bookSaleOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                bookSaleCTPendingThenCurrentRule4 =
                                                        "SoftwareServiceOperation bookSaleOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "bookSaleOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setBookSaleOperation(bookSaleOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_95)) {
                                                currentRule4Approach1WithCT +=
                                                        "bookSaleOperation.getCompletionTime() < " + value.intValue()
                                                                + "); \n\t\t";

                                                String string5 =
                                                        "event : Event(name == EventName.QueryEvent, riskQuantifier == RiskQuantifier.NEGLIGIBLE);\n\t\t"
                                                                + "eval( bookSaleOperation != null && bookSaleOperation.getArrivalRate() > "
                                                                + this.arrivalRateBookSale + " && "
                                                                + "bookSaleOperation.getArrivalRate() <= "
                                                                + (200 + (arrivalRateBookSaleLoop * 200)) + "); \n\t\t";

                                                currentRule5Approach1WithCT += string5;
                                                currentRule5Approach1WithoutCT += string5;

                                                currentRule5Approach1WithCT +=
                                                        "eval( bookSaleOperation != null && bookSaleOperation.getCompletionTime() >= "
                                                                + value.intValue() + "); \n\t\t";// " && ";
                                                bookSaleCTPendingThenCurrentRule5 =
                                                        "SoftwareServiceOperation bookSaleOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "bookSaleOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setBookSaleOperation(bookSaleOperationRecommendedValues);\n\t\t";
                                            }

                                        }
                                        // GetProductDetails Operation
                                        else if (result.getServiceID().equalsIgnoreCase(
                                                "ORC_CustomerConstraintInventoryGetDetails")) {
                                            // IF part
                                            LOGGER.info("***" + result.getServiceID() + "****"
                                                    + result.getAggregationType() + "****** Int Value = "
                                                    + value.intValue());
                                            if (result.getAggregationType().equals(AggregationType.PERCENTILE_25)) {
                                                String string1 =
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getArrivalRate() > "
                                                                + this.arrivalRateGetProductDetails + " && "
                                                                + "productDetailsOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRateGetProductDetailsLoop * 125))
                                                                + "); \n\t\t";

                                                currentRule1Approach1WithCT += string1;
                                                currentRule1Approach1WithoutCT += string1;

                                                currentRule1Approach1WithCT +=
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getCompletionTime() >= "
                                                                + +value.intValue() + " && ";
                                                // Pending Then part for productDetails for both Approach1WithoutCT and
                                                // Approach1WithCT rules
                                                productDetailsCTPendingThenCurrentRule1 =
                                                        "SoftwareServiceOperation productDetailsOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "productDetailsOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setProductDetailsOperation(productDetailsOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_50)) {
                                                currentRule1Approach1WithCT +=
                                                        "productDetailsOperation.getCompletionTime() < "
                                                                + value.intValue() + "); \n\t\t";

                                                String string2 =
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getArrivalRate() > "
                                                                + this.arrivalRateGetProductDetails + " && "
                                                                + "productDetailsOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRateGetProductDetailsLoop * 125))
                                                                + "); \n\t\t";

                                                currentRule2Approach1WithCT += string2;
                                                currentRule2Approach1WithoutCT += string2;

                                                currentRule2Approach1WithCT +=
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                productDetailsCTPendingThenCurrentRule2 =
                                                        "SoftwareServiceOperation productDetailsOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "productDetailsOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setProductDetailsOperation(productDetailsOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_75)) {
                                                currentRule2Approach1WithCT +=
                                                        "productDetailsOperation.getCompletionTime() < "
                                                                + value.intValue() + "); \n\t\t";

                                                String string3 =
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getArrivalRate() > "
                                                                + this.arrivalRateGetProductDetails + " && "
                                                                + "productDetailsOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRateGetProductDetailsLoop * 125))
                                                                + "); \n\t\t";

                                                currentRule3Approach1WithCT += string3;
                                                currentRule3Approach1WithoutCT += string3;

                                                currentRule3Approach1WithCT +=
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                productDetailsCTPendingThenCurrentRule3 =
                                                        "SoftwareServiceOperation productDetailsOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "productDetailsOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setProductDetailsOperation(productDetailsOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_90)) {
                                                currentRule3Approach1WithCT +=
                                                        "productDetailsOperation.getCompletionTime() < "
                                                                + value.intValue() + "); \n\t\t";

                                                String string4 =
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getArrivalRate() > "
                                                                + this.arrivalRateGetProductDetails + " && "
                                                                + "productDetailsOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRateGetProductDetailsLoop * 125))
                                                                + "); \n\t\t";

                                                currentRule4Approach1WithCT += string4;
                                                currentRule4Approach1WithoutCT += string4;

                                                currentRule4Approach1WithCT +=
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                productDetailsCTPendingThenCurrentRule4 =
                                                        "SoftwareServiceOperation productDetailsOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "productDetailsOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setProductDetailsOperation(productDetailsOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_95)) {
                                                currentRule4Approach1WithCT +=
                                                        "productDetailsOperation.getCompletionTime() < "
                                                                + value.intValue() + "); \n\t\t";

                                                String string5 =
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getArrivalRate() > "
                                                                + this.arrivalRateGetProductDetails + " && "
                                                                + "productDetailsOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRateGetProductDetailsLoop * 125))
                                                                + "); \n\t\t";

                                                currentRule5Approach1WithCT += string5;
                                                currentRule5Approach1WithoutCT += string5;

                                                currentRule5Approach1WithCT +=
                                                        "eval( productDetailsOperation != null && productDetailsOperation.getCompletionTime() >= "
                                                                + value.intValue() + "); \n\t\t"; // " && " +
                                                productDetailsCTPendingThenCurrentRule5 =
                                                        "SoftwareServiceOperation productDetailsOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "productDetailsOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setProductDetailsOperation(productDetailsOperationRecommendedValues);\n\t\t";
                                            }
                                        }

                                        // PaymentService Operation
                                        else if (result.getServiceID()
                                                .equalsIgnoreCase("ORC_CustomerConstraintPayment")) {
                                            // IF part
                                            LOGGER.info("***" + result.getServiceID() + "****"
                                                    + result.getAggregationType() + "****** Int Value = "
                                                    + value.intValue());
                                            if (result.getAggregationType().equals(AggregationType.PERCENTILE_25)) {
                                                String string1 =
                                                        "eval( paymentServiceOperation != null && paymentServiceOperation.getArrivalRate() > "
                                                                + this.arrivalRatePaymentService + " && "
                                                                + "paymentServiceOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRatePaymentServiceLoop * 50)) + ");";

                                                currentRule1Approach1WithCT += string1;
                                                currentRule1Approach1WithoutCT += string1;

                                                currentRule1Approach1WithCT +=
                                                        "\n\t\teval( paymentServiceOperation != null && paymentServiceOperation.getCompletionTime() >= "
                                                                + +value.intValue() + " && ";
                                                // Pending Then part for paymentService for both Approach1WithoutCT and
                                                // Approach1WithCT rules
                                                paymentServiceCTPendingThenCurrentRule1 =
                                                        "SoftwareServiceOperation paymentServiceOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "paymentServiceOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setPaymentServiceOperation(paymentServiceOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_50)) {
                                                currentRule1Approach1WithCT +=
                                                        "paymentServiceOperation.getCompletionTime() < "
                                                                + value.intValue() + ");";

                                                String string2 =
                                                        "eval( paymentServiceOperation != null && paymentServiceOperation.getArrivalRate() > "
                                                                + this.arrivalRatePaymentService + " && "
                                                                + "paymentServiceOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRatePaymentServiceLoop * 50)) + ");";

                                                currentRule2Approach1WithCT += string2;
                                                currentRule2Approach1WithoutCT += string2;

                                                currentRule2Approach1WithCT +=
                                                        "\n\t\teval( paymentServiceOperation != null && paymentServiceOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                paymentServiceCTPendingThenCurrentRule2 =
                                                        "SoftwareServiceOperation paymentServiceOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "paymentServiceOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setPaymentServiceOperation(paymentServiceOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_75)) {
                                                currentRule2Approach1WithCT +=
                                                        "paymentServiceOperation.getCompletionTime() < "
                                                                + value.intValue() + ");";

                                                String string3 =
                                                        "eval( paymentServiceOperation != null && paymentServiceOperation.getArrivalRate() > "
                                                                + this.arrivalRatePaymentService + " && "
                                                                + "paymentServiceOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRatePaymentServiceLoop * 50)) + ");";

                                                currentRule3Approach1WithCT += string3;
                                                currentRule3Approach1WithoutCT += string3;

                                                currentRule3Approach1WithCT +=
                                                        "\n\t\teval( paymentServiceOperation != null && paymentServiceOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                paymentServiceCTPendingThenCurrentRule3 =
                                                        "SoftwareServiceOperation paymentServiceOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "paymentServiceOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setPaymentServiceOperation(paymentServiceOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_90)) {
                                                currentRule3Approach1WithCT +=
                                                        "paymentServiceOperation.getCompletionTime() < "
                                                                + value.intValue() + ");";

                                                String string4 =
                                                        "eval( paymentServiceOperation != null && paymentServiceOperation.getArrivalRate() > "
                                                                + this.arrivalRatePaymentService + " && "
                                                                + "paymentServiceOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRatePaymentServiceLoop * 50)) + ");";

                                                currentRule4Approach1WithCT += string4;
                                                currentRule4Approach1WithoutCT += string4;

                                                currentRule4Approach1WithCT +=
                                                        "\n\t\teval( paymentServiceOperation != null && paymentServiceOperation.getCompletionTime() >= "
                                                                + value.intValue() + " && ";
                                                paymentServiceCTPendingThenCurrentRule4 =
                                                        "SoftwareServiceOperation paymentServiceOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "paymentServiceOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setPaymentServiceOperation(paymentServiceOperationRecommendedValues);\n\t\t";
                                            }
                                            else if (result.getAggregationType().equals(AggregationType.PERCENTILE_95)) {
                                                currentRule4Approach1WithCT +=
                                                        "paymentServiceOperation.getCompletionTime() < "
                                                                + value.intValue() + ");";

                                                String string5 =
                                                        "eval( paymentServiceOperation != null && paymentServiceOperation.getArrivalRate() > "
                                                                + this.arrivalRatePaymentService + " && "
                                                                + "paymentServiceOperation.getArrivalRate() <= "
                                                                + (50 + (arrivalRatePaymentServiceLoop * 50)) + ");";

                                                currentRule5Approach1WithCT += string5;
                                                currentRule5Approach1WithoutCT += string5;

                                                currentRule5Approach1WithCT +=
                                                        "\n\t\teval( paymentServiceOperation != null && paymentServiceOperation.getCompletionTime() >= "
                                                                + value.intValue() + ");"; // " && " +
                                                paymentServiceCTPendingThenCurrentRule5 =
                                                        "SoftwareServiceOperation paymentServiceOperationRecommendedValues = new SoftwareServiceOperation();\n\t\t"
                                                                + "paymentServiceOperationRecommendedValues.setCompletionTime("
                                                                + value.intValue()
                                                                + ");\n\t\t"
                                                                + "iServiceTerms.setPaymentServiceOperation(paymentServiceOperationRecommendedValues);\n\t\t";
                                            }
                                        }

                                    }
                                    // THEN part
                                    String then1 =
                                            "\nthen \n\t\t"
                                                    + "InfrastructureServiceTerms iServiceTerms = new InfrastructureServiceTerms();\n\t\t"
                                                    + "iServiceTerms.setRiskQualifier(RiskQuantifier.HIGH);\n\t\t"
                                                    + "iServiceTerms.setCpuCores(" + this.iSTerms.getCpuCores()
                                                    + ");\n\t\t" + "iServiceTerms.setCpuSpeed("
                                                    + this.iSTerms.getCpuSpeed() + ");\n\t\t"
                                                    + "iServiceTerms.setMemory(" + this.iSTerms.getMemory()
                                                    + ");\n\t\t" + "iServiceTerms.setRuleNumber(" + ruleNumber
                                                    + ");\n\t\t";

                                    currentRule1Approach1WithCT += then1 + footer;// bookSaleCTPendingThenCurrentRule1 +
                                    // productDetailsCTPendingThenCurrentRule1
                                    // +
                                    // paymentServiceCTPendingThenCurrentRule1
                                    // +
                                    currentRule1Approach1WithoutCT +=
                                            then1 + bookSaleCTPendingThenCurrentRule1
                                                    + productDetailsCTPendingThenCurrentRule1
                                                    + paymentServiceCTPendingThenCurrentRule1 + footer;

                                    String then2 =
                                            "\n\t then \n\t\t"
                                                    + "InfrastructureServiceTerms iServiceTerms = new InfrastructureServiceTerms();\n\t\t"
                                                    + "iServiceTerms.setRiskQualifier(RiskQuantifier.MEDIUM);\n\t\t"
                                                    + "iServiceTerms.setCpuCores(" + this.iSTerms.getCpuCores()
                                                    + ");\n\t\t" + "iServiceTerms.setCpuSpeed("
                                                    + this.iSTerms.getCpuSpeed() + ");\n\t\t"
                                                    + "iServiceTerms.setMemory(" + this.iSTerms.getMemory()
                                                    + ");\n\t\t" + "iServiceTerms.setRuleNumber(" + (ruleNumber + 1)
                                                    + ");\n\t\t";

                                    currentRule2Approach1WithCT += then2 + footer;// bookSaleCTPendingThenCurrentRule2 +
                                    // productDetailsCTPendingThenCurrentRule2
                                    // +
                                    // paymentServiceCTPendingThenCurrentRule2
                                    // +
                                    currentRule2Approach1WithoutCT +=
                                            then2 + bookSaleCTPendingThenCurrentRule2
                                                    + productDetailsCTPendingThenCurrentRule2
                                                    + paymentServiceCTPendingThenCurrentRule2 + footer;

                                    String then3 =
                                            "\n\t then \n\t\t"
                                                    + "InfrastructureServiceTerms iServiceTerms = new InfrastructureServiceTerms();\n\t\t"
                                                    + "iServiceTerms.setRiskQualifier(RiskQuantifier.SOME);\n\t\t"
                                                    + "iServiceTerms.setCpuCores(" + this.iSTerms.getCpuCores()
                                                    + ");\n\t\t" + "iServiceTerms.setCpuSpeed("
                                                    + this.iSTerms.getCpuSpeed() + ");\n\t\t"
                                                    + "iServiceTerms.setMemory(" + this.iSTerms.getMemory()
                                                    + ");\n\t\t" + "iServiceTerms.setRuleNumber(" + (ruleNumber + 2)
                                                    + ");\n\t\t";

                                    currentRule3Approach1WithCT += then3 + footer;// bookSaleCTPendingThenCurrentRule3 +
                                    // productDetailsCTPendingThenCurrentRule3
                                    // +
                                    // paymentServiceCTPendingThenCurrentRule3
                                    // +
                                    currentRule3Approach1WithoutCT +=
                                            then3 + bookSaleCTPendingThenCurrentRule3
                                                    + productDetailsCTPendingThenCurrentRule3
                                                    + paymentServiceCTPendingThenCurrentRule3 + footer;

                                    String then4 =
                                            "\n\t then \n\t\t"
                                                    + "InfrastructureServiceTerms iServiceTerms = new InfrastructureServiceTerms();\n\t\t"
                                                    + "iServiceTerms.setRiskQualifier(RiskQuantifier.LOW);\n\t\t"
                                                    + "iServiceTerms.setCpuCores(" + this.iSTerms.getCpuCores()
                                                    + ");\n\t\t" + "iServiceTerms.setCpuSpeed("
                                                    + this.iSTerms.getCpuSpeed() + ");\n\t\t"
                                                    + "iServiceTerms.setMemory(" + this.iSTerms.getMemory()
                                                    + ");\n\t\t" + "iServiceTerms.setRuleNumber(" + (ruleNumber + 3)
                                                    + ");\n\t\t";

                                    currentRule4Approach1WithCT += then4 + footer;// bookSaleCTPendingThenCurrentRule4 +
                                    // productDetailsCTPendingThenCurrentRule4
                                    // +
                                    // paymentServiceCTPendingThenCurrentRule4
                                    // +
                                    currentRule4Approach1WithoutCT +=
                                            then4 + bookSaleCTPendingThenCurrentRule4
                                                    + productDetailsCTPendingThenCurrentRule4
                                                    + paymentServiceCTPendingThenCurrentRule4 + footer;

                                    String then5 =
                                            "\n\t then \n\t\t"
                                                    + "InfrastructureServiceTerms iServiceTerms = new InfrastructureServiceTerms();\n\t\t"
                                                    + "iServiceTerms.setRiskQualifier(RiskQuantifier.NEGLIGIBLE);\n\t\t"
                                                    + "iServiceTerms.setCpuCores(" + this.iSTerms.getCpuCores()
                                                    + ");\n\t\t" + "iServiceTerms.setCpuSpeed("
                                                    + this.iSTerms.getCpuSpeed() + ");\n\t\t"
                                                    + "iServiceTerms.setMemory(" + this.iSTerms.getMemory()
                                                    + ");\n\t\t" + "iServiceTerms.setRuleNumber(" + (ruleNumber + 4)
                                                    + ");\n\t\t";

                                    currentRule5Approach1WithCT += then5 + footer;// bookSaleCTPendingThenCurrentRule5 +
                                    // productDetailsCTPendingThenCurrentRule5
                                    // +
                                    // paymentServiceCTPendingThenCurrentRule5
                                    // +
                                    currentRule5Approach1WithoutCT +=
                                            then5 + bookSaleCTPendingThenCurrentRule5
                                                    + productDetailsCTPendingThenCurrentRule5
                                                    + paymentServiceCTPendingThenCurrentRule5 + footer;

                                    ruleNumber += 5; // increment rule counter
                                    // APPEND rule to appropriate file
                                    rulesApproach1WithCT +=
                                            currentRule1Approach1WithCT +=
                                                    currentRule2Approach1WithCT +=
                                                            currentRule3Approach1WithCT +=
                                                                    currentRule4Approach1WithCT +=
                                                                            currentRule5Approach1WithCT;
                                    rulesApproach1WithoutCT +=
                                            currentRule1Approach1WithoutCT +=
                                                    currentRule2Approach1WithoutCT +=
                                                            currentRule3Approach1WithoutCT +=
                                                                    currentRule4Approach1WithoutCT +=
                                                                            currentRule5Approach1WithoutCT;
                                }
                                LOGGER.info(temp.toString());
                                // Clear InfSLAT values
                                LOGGER.info("Clearing iSTerms ");
                                this.iSTerms = new InfrastructureServiceTerms();

                                // increment hw params : scheme = starting_unit plus memoryLoop times incremental_unit
                                this.memory = 2048 + (memoryLoop * 2048);
                            }// Close memoryLoop

                            this.cpuCores = 2 + (cpuCoresLoop * 2);
                        }// Close cpuCoresLoop

                        // Periodically flush data to disk (after every 675 rules) so sizeof rulesApproach1WithCT &
                        // rulesApproach1WithoutCT would'nt go out of memory.
                        createRuleFile(rulesApproach1WithCT, "KB-A1WCT.drl");
                        rulesApproach1WithCT = null;
                        rulesApproach1WithCT = "";
                        // System.gc(); //Request release of memory occupied by unused resources.
                        createRuleFile(rulesApproach1WithoutCT, "KB-A1WOCT.drl");
                        rulesApproach1WithoutCT = null;
                        rulesApproach1WithoutCT = "";
                        System.gc(); // Request release of memory occupied by unused resources.

                        this.cpuSpeed = 1 + cpuSpeedLoop;
                    }// Close cpuSpeedLoop

                    // increment sw params : scheme = starting_unit plus i times incremental_unit
                    this.arrivalRateBookSale = 200 + (200 * arrivalRateBookSaleLoop);
                    // Clear SWSLAT values
                    this.mapOfSSOperations.clear();
                    LOGGER.info("Clearing mapOfSSOperations - Size = " + this.mapOfSSOperations.size());

                }// Close arrivalRateBookSaleLoop

                this.arrivalRateGetProductDetails = 50 + (arrivalRateGetProductDetailsLoop * 125);
            }// Close arrivalRateGetProductDetailsLoop

            this.arrivalRatePaymentService = 50 + (arrivalRatePaymentServiceLoop * 50);
        }// Close arrivalRatePaymentServiceLoop

        long endTime = System.currentTimeMillis();
        LOGGER.info("Time taken = " + (endTime - startTime) / 1000 + " seconds");
    }

    private Set<ServiceBuilder> initMyServiceBuilders() {

        Set<ServiceBuilder> builders = new HashSet<ServiceBuilder>();
        ServiceConstructionModelFactory factory = ServiceConstructionModel.getFactory();
        String landscapePath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "ORC.scm";
        this.landscape = (Landscape) ServiceConstructionModel.loadFromXMI(landscapePath);
        // ServiceBinding binding = factory.createServiceBinding(); //never used.
        // binding.setSlaTemplate(infrastructureSLATemplate); //never used.

        ServiceBuilderExtended serviceBuilder = new ServiceBuilderExtended();

        serviceBuilder.setUuid("ORC_AllInOne");
        serviceBuilder.setImplementation(landscape.getImplementations(0));
        // Dependency dep = landscape.getImplementations(0).getDependencies().get(0);
        // serviceBuilder.addBinding(dep, infrastructureSLATemplate);

        landscape.addBuilder(serviceBuilder);

        builders.add(serviceBuilder);

        return builders;
    }

    private void bindDependentTemplateWithServiceBuilder(Set<ServiceBuilder> builders,
            SLATemplate infrastructureSLATemplate) {
        ServiceBuilder serviceBuilder = builders.iterator().next(); // there will be just one service builder.
        // remove any previous bindings first:
        List<ServiceBinding> list = serviceBuilder.getBindingsList();
        LOGGER.info("1- Total bindings available in ServiceBuilder = " + list.size());
        list.removeAll(list); // hope this removes all previous bindings!
        LOGGER.info("2- After removing bindings from the list, = " + serviceBuilder.getBindingsList().size());
        Dependency dep = this.landscape.getImplementations(0).getDependencies().get(0);
        // dep.setName("AllInOne Infrastructure");
        serviceBuilder.addBinding(dep, infrastructureSLATemplate);
        LOGGER.info("3- After adding bindings to the list, = " + serviceBuilder.getBindingsList().size());
    }

    public SLATemplate loadSLAT(String fileName) {
        SLATemplate slat = null;
        try {
            FileReader read;
            String path = // System.getenv("SLASOI_ORC_HOME") + File.separator + "SW_SLATs" + File.separator + fileName;
                    System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "orc"
                            + File.separator + "infrastructure-templates" // this is just a folder name
                            + File.separator + fileName;
            read = new FileReader(path);
            LOGGER.info("Reading Template from " + path);

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }

            SLASOITemplateParser slasoitParser = new SLASOITemplateParser();
            slat = slasoitParser.parseTemplate(sb.toString());
            // LOGGER.info(slat.toString());
            // SLATemplateRegistryImpl.offlineValidate(slat);

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return slat;
    }

    /*********************** METHODS BELOW THIS LINE ARE USED TO READ/WRITE RESULTS ON DISK & FROM TEMPLATE ***************************/

    public int createRuleFile(String rules, String fileName) {
        LOGGER.info("Inside createRuleFile - Will Generate " + fileName);
        boolean result = false;
        String pathToDRL = "";
        try {
            FileInputStream fstream = null;
            pathToDRL =
                    System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "software-slamanager"
                            + System.getProperty("file.separator") + "planning-optimization"
                            + System.getProperty("file.separator") + "adaptive-kb";
            LOGGER.info(pathToDRL);
            File file = new File(pathToDRL);
            file.mkdirs();
            pathToDRL += System.getProperty("file.separator") + fileName;
            FileWriter fileWriter = new FileWriter(pathToDRL, true);// true meant to append file and not overwrite!
            BufferedWriter out = new BufferedWriter(fileWriter);
            out.write(rules);
            // Close the output stream
            out.close();
            result = true;
        }
        catch (Exception e) {
            LOGGER.info("Error: " + e.getMessage());
        }
        return (result == true ? 1 : -1);
    }
    
    public void printSSOp() {
        HashMap<String, SoftwareServiceOperation> mapOfSSOperations = new HashMap<String, SoftwareServiceOperation>();
        mapOfSSOperations =
                this.templateParser.getMapOfSSOperations(this.loadSLAT("software_SLA_template.xml"));
        LOGGER.info("BookSale.ArrivalRate = " + mapOfSSOperations.get("bookSale").getArrivalRate());
        LOGGER.info("BookSale.CompletionTime = " + mapOfSSOperations.get("bookSale").getCompletionTime());
        LOGGER.info("GetProductDetails.ArrivalRate = "
                + mapOfSSOperations.get("getProductDetails").getArrivalRate());
        LOGGER.info("GetProductDetails.CompletionTime = "
                + mapOfSSOperations.get("getProductDetails").getCompletionTime());
        LOGGER.info("PaymentServiceOperation.ArrivalRate = "
                + mapOfSSOperations.get("PaymentServiceOperation").getArrivalRate());
        LOGGER.info("PaymentServiceOperation.CompletionTime = "
                + mapOfSSOperations.get("PaymentServiceOperation").getCompletionTime());
    }

    public void printISTerms() {
        InfrastructureServiceTerms iSTerms;
        iSTerms = this.templateParser.getISTerms(this.loadSLAT("infrastructure_SLA_template.xml"));
        LOGGER.info("VM.cpuCores = " + iSTerms.getCpuCores());
        LOGGER.info("VM.cpuSpeed = " + iSTerms.getCpuSpeed());
        LOGGER.info("VM.memory = " + iSTerms.getMemory());
    }

    public static void main(String a[]) {
        CollectorTest test = new CollectorTest();
        test.printSSOp();
        //test.printISTerms();
        //test.offlineCacheRoutine();
        //Translator.getInstance().queryKnowledgeBase(test.loadSLAT("software_SLA_template.xml"), 1);
        //Translator.getInstance().queryKnowledgeBase(test.loadSLAT("software_SLA_template.xml"), 2);
        //Translator.getInstance().disposeWorkingMemory(1);
        //Translator.getInstance().disposeWorkingMemory(2);
    }
}
