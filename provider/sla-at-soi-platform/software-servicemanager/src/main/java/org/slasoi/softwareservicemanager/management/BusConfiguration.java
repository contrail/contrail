/**
 *  SVN FILE: $Id: BusConfiguration.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/management/BusConfiguration.java $
 */

package org.slasoi.softwareservicemanager.management;

/**
 * BusConfiguration contains the messaging configurations.
 * 
 * @author Jens Happe
 * 
 */
public class BusConfiguration {
    /**
     * Resource.
     */
    private String resource;
    /**
     * Username.
     */
    private String username;
    /**
     * Password.
     */
    private String password;
    /**
     * Host.
     */
    private String host;
    /**
     * Service.
     */
    private String service;
    /**
     * Pubsub.
     */
    private String pubsub;
    /**
     * Pubsub service.
     */
    private String xmpppPubsubservice;
    /**
     * XMPP port..
     */
    private String xmppPort;

    /**
     * Setter for Resource.
     * 
     * @param resource
     *            new resource
     */
    public final void setResource(final String resource) {
        this.resource = resource;
    }

    /**
     * Getter for resource.
     * 
     * @return Returns resource.
     */
    public final String getResource() {
        return resource;
    }

    /**
     * Setter for username.
     * 
     * @param username
     *            new username
     */
    public final void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Getter for username.
     * 
     * @return Returns username.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * Setter for password.
     * 
     * @param password
     *            new password
     */
    public final void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Getter for password.
     * 
     * @return Returns password.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Setter for host.
     * 
     * @param host
     *            new host
     */
    public final void setHost(final String host) {
        this.host = host;
    }

    /**
     * Getter for host.
     * 
     * @return Returns host.
     */
    public final String getHost() {
        return host;
    }

    /**
     * Setter for service.
     * 
     * @param service
     *            new service
     */
    public final void setService(final String service) {
        this.service = service;
    }

    /**
     * Getter for service.
     * 
     * @return Returns service.
     */
    public final String getService() {
        return service;
    }

    /**
     * Setter for pubsub.
     * 
     * @param pubsub
     *            new pubsub
     */
    public final void setPubsub(final String pubsub) {
        this.pubsub = pubsub;
    }

    /**
     * Getter for pubsub.
     * 
     * @return Returns pubsub.
     */
    public final String getPubsub() {
        return pubsub;
    }

    /**
     * Setter for xmppp_pubsubservice.
     * 
     * @param xmpppPubsubservice
     *            new xmppp_pubsubservice
     */
    public final void setXmpppPubsubservice(final String xmpppPubsubservice) {
        this.xmpppPubsubservice = xmpppPubsubservice;
    }

    /**
     * Getter for xmppp_pubsubservice.
     * 
     * @return Returns xmppp_pubsubservice.
     */
    public final String getXmpppPubsubservice() {
        return xmpppPubsubservice;
    }

    /**
     * Setter for xmpp_port.
     * 
     * @param xmppPort
     *            new xmpp_port
     */
    public final void setXmppPort(final String xmppPort) {
        this.xmppPort = xmppPort;
    }

    /**
     * Getter for xmpp_port.
     * 
     * @return Returns xmpp_port.
     */
    public final String getXmppPort() {
        return xmppPort;
    }

}
