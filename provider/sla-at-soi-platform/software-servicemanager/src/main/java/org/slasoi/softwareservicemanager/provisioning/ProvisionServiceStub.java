/**
 *  SVN FILE: $Id: ProvisionServiceStub.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/provisioning/ProvisionServiceStub.java $
 */

package org.slasoi.softwareservicemanager.provisioning;

import java.util.ArrayList;
import java.util.List;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;

import org.slasoi.common.messaging.pubsub.PubSubManager;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;

/**
 * A dummy ProvisiongManager.
 *
 * @author Alexander Wert
 *
 */
public class ProvisionServiceStub implements IProvisioning {

    /**
     * pubSub Manager.
     */
    private static PubSubManager pubSubManager = null;

    /**
     * @param builder
     *            ServiceBuilder of interest
     * @return endpoints for passed ServiceBuilder object.
     */
    public final List<Endpoint> getEndpoints(final ServiceBuilder builder) {
        List<Endpoint> resultList = new ArrayList<Endpoint>();
        Endpoint p = new Endpoint(new ID("my_endpoint_1"), new STND("http://www.slaatsoi.org/slamodel#HTTP)"));
        p.setLocation(new UUID("localhost:8080/my-service/service"));
        p.setDescr("My Service");
        resultList.add(p);
        return resultList;
    }

    /**
     * Creates and starts provisioning of the service for the passed ServiceBuilder object.
     *
     * @param builder
     *            ServiceBuilder instance describing the service which should be provisioned.
     * @param connectionSettings
     *            Settings containing information about messaging configuration.
     * @param notificationChannel
     *            Messaging channel which should be used to inform about completed provisioning, since this function is
     *            executed asynchronously in an own thread.
     *
     * @throws ServiceStartupException
     *             ServiceStartupException is thrown if starting/provisioning the service fails.
     * @throws MessagingException
     *             MessagingException is thrown if notifying about provisioning raises errors.
     */
    public final synchronized void startServiceInstance(final ServiceBuilder builder,
            final Settings connectionSettings, final String notificationChannel) throws ServiceStartupException,
            MessagingException {

        // Start VM, .... when ready notify
        // Example code is uncommented, since no messaging broker is available during test runs.

         // if (pubSubManager == null) { connectionSettings.setSetting(Setting.xmpp_username, "user3");
         // connectionSettings.setSetting(Setting.xmpp_password, "user3"); pubSubManager =
         // PubSubFactory.createPubSubManager(connectionSettings); }

         //  PubSubMessage message = new PubSubMessage(notificationChannel, "stub: Provisioned <ID>" + builder.getUuid() +
         //  "</ID>"); pubSubManager.publish(message); System.out.println("provisioned - type: " +
         //  builder.getImplementation().getType().getServiceTypeName() + "  implementation: " +
         //  builder.getImplementation().getServiceImplementationName() + "  with ID: " + builder.getUuid());

    }

    /**
     * Dummy stop instance function.
     *
     * @param builder
     *            ServiceBuilder
     */
    public final void stopServiceInstance(final ServiceBuilder builder) {

    }

    /**
     * Dummy getManagibilityAgent function.
     *
     * @param builder
     *            ServiceBuilder
     * @return Returns null.
     */
    public final IManageabilityAgentFacade getManagibilityAgent(final ServiceBuilder builder) {

        return null;
    }

}