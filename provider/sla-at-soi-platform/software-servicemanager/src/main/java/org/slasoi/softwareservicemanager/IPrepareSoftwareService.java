/**
 *  SVN FILE: $Id: IPrepareSoftwareService.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/IPrepareSoftwareService.java $
 */

package org.slasoi.softwareservicemanager;

import java.util.Date;
import java.util.List;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.softwareservicemanager.exceptions.BookingException;
import org.slasoi.softwareservicemanager.exceptions.ReservationException;

/**
 * The interface IPrepareSoftwareService subsumes functionality that is necessary to find implementations for a
 * particular service type as well as retrieving information about a service implementation or service instance.
 * 
 * @author Jens Happe
 * 
 */
public interface IPrepareSoftwareService {

    /**
     * This operation returns the list of service types supported by the ServiceManager. The service types are mapped to
     * SLA(T)s by the SLA Manager.
     * 
     * @return List of ServiceTypes supported by the SLA Manager
     */
    List<ServiceType> queryServiceTypes();

    /**
     * The operation is used to query for the service implementations available for a given service type. It throws an
     * exception if an invalid service type is provided.
     * 
     * @param type
     *            The ServiceType for which the implementations are requested.
     * @return A list of ServiceImplementations for the given type.
     */
    List<ServiceImplementation> queryServiceImplementations(ServiceType type);

    /**
     * This operation enables the SoftwarePOC to perform a capacity check on the resource availability for example the
     * software licenses available to support the software service provisioning.
     * 
     * @param implementation
     *            ServiceImplementation whose capacity needs to be chekced
     * @return The number of available instances.
     */
    int capacityCheck(ServiceImplementation implementation);

    /**
     * This operation creates and returns an instance of ServiceBuilder which can be used by the SoftwarePOC to
     * configure the service instances.
     * 
     * @param implementation
     *            The implementation for which a ServiceBuilder is needed.
     * @return A new instance of a ServiceBuilder.
     */
    ServiceBuilder createBuilder(ServiceImplementation implementation);

    /**
     * This operation returns an instance of ServiceBuilder that has already been created. It can be used by the
     * SoftwarePOC to configure the service instances.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            avaliabel after provisioning.
     * @return A new instance of a ServiceBuilder.
     */
    ServiceBuilder getBuilder(String serviceID);

    /**
     * This operation returns true if a ServiceBuilder with the given serviceID has already been created.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            available after provisioning.
     * @return a boolean value
     */
    boolean hasBuilder(String serviceID);

    /**
     * This operation is used to reserve the required resources to be used in the future during the software service
     * provisioning process.
     * 
     * @param builder
     *            Builder that contains all information for the service instance under construction and allows the
     *            ServiceManager to reserve all the necessary resources.
     * @param from
     *            Time from which the reservation shall be effective.
     * @param until
     *            Time until the reservation shall be effective.
     * @return True if the reservation is successful, false otherwise. Please note the transactional semantic: Either
     *         all resources are reserved (success) or none (failure).
     * @throws ReservationException
     *             Thrown if the ServiceBuilder is invalid.
     */
    boolean reserve(ServiceBuilder builder, Date from, Date until) throws ReservationException;

    /**
     * This operation is used to update previously reserved resources.
     * 
     * @param builder
     *            Builder that contains all information for the service instance under construction and allows the
     *            ServiceManager to reserve all the necessary resources.
     * @param from
     *            Time from which the reservation shall be effective.
     * @param until
     *            Time until the reservation shall be effective.
     * @return True if the reservation is successful, false otherwise. Please note the transactional semantic: Either
     *         all resources are reserved (success) or none (failure).
     * @throws ReservationException
     *             Thrown if the ServiceBuilder is invalid.
     */
    boolean updateReservation(ServiceBuilder builder, Date from, Date until) throws ReservationException;

    /**
     * This operation is used to update previously reserved resources.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            avaliabel after provisioning.
     * @param from
     *            Time from which the reservation shall be effective.
     * @param until
     *            Time until the reservation shall be effective.
     * @return True if the reservation is successful, false otherwise. Please note the transactional semantic: Either
     *         all resources are reserved (success) or none (failure).
     * @throws ReservationException
     *             Thrown if the ServiceBuilder is invalid.
     */
    boolean updateReservation(String serviceID, Date from, Date until) throws ReservationException;

    /**
     * This operation is used to cancel the previously reserved resources.
     * 
     * @param builder
     *            ServiceBuilder whose reserved resources are to be released.
     * @return True if the release was successful, false otherwise.
     * @throws ReservationException
     *             Thrown if the ServiceBuilder is invalid.
     */
    boolean cancelReservation(ServiceBuilder builder) throws ReservationException;

    /**
     * This operation is used to cancel the previously reserved resources.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            avaliabel after provisioning.
     * @return True if the release was successful, false otherwise.
     * @throws ReservationException
     *             Thrown if the ServiceBuilder is invalid.
     */
    boolean cancelReservation(String serviceID) throws ReservationException;

    /**
     * This operation is used by the SoftwarePOC to confirm previously reserved resources.
     * 
     * @param builder
     *            ServiceBuilder whose previously reserved resources are to be booked.
     * @return the EndPoints of the service
     * @throws BookingException
     *             This exception is thrown if booking fails.
     */
    List<Endpoint> book(ServiceBuilder builder) throws BookingException;

    /**
     * This operation is used by the SoftwarePOC to confirm previously reserved resources.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            avaliabel after provisioning.
     * @return the EndPoints of the service
     * @throws BookingException
     *             This exception is thrown if booking fails.
     */
    List<Endpoint> book(String serviceID) throws BookingException;

    /**
     * This operation is used to cancelling the bookings of resources for certain service implementation.
     * 
     * @param builder
     *            ServiceBuilder whose previously booked resources are to be released.
     * @return True if the release has been successful, false otherwise.
     * @throws BookingException
     *             This exception is thrown if booking fails.
     */
    boolean cancelBooking(ServiceBuilder builder) throws BookingException;

    /**
     * This operation is used to cancelling the bookings of resources for certain service implementation.
     * 
     * @param serviceID
     *            ID of the builder used to define the instance. This ID is similar to the ID of the service instance
     *            avaliabel after provisioning.
     * @return True if the release has been successful, false otherwise.
     * @throws BookingException
     *             This exception is thrown if booking fails.
     */
    boolean cancelBooking(String serviceID) throws BookingException;
}
