/**
 *  SVN FILE: $Id: IManageSoftwareServices.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/IManageSoftwareServices.java $
 */

package org.slasoi.softwareservicemanager;

import java.util.Date;
import java.util.List;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.management.IEffectorAction;
import org.slasoi.softwareservicemanager.management.IEffectorResult;
import org.slasoi.softwareservicemanager.management.SensorSubscriptionData;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;

/**
 * The interface IManagerSoftwareservices subsumes all functionality related to the provisioning and management of a
 * software service. It allows SoftwareSLAManagers to create new service instances and carry out service life-cycle
 * management related operations. This interface complements the functionality enabled through interface
 * IPrepareSoftwareService.
 * 
 * @author Jens Happe
 * 
 */
public interface IManageSoftwareServices {

    /**
     * This method triggers the immediate provisioning of a new service instance. The instance will be associated with a
     * unique Id. The builder specifies all bindings to external services, configurations, and monitoring options. The
     * notificationChannel points to the message bus and topic where status related events of the framework are to be
     * posted.
     * 
     * @param builder
     *            ServiceBuilder containing all information necessary to instantiate a service.
     * @param notificationChannel
     *            Callback channel for notifications of the SLA Manager.
     * @param connectionSettings
     *            Contains the information about messaging connection.
     * @throws ServiceStartupException
     *             In case the startup fails (e.g., an incomplete or invalid builder is passed) a
     *             ServiceStartupException is thrown.
     * @throws MessagingException
     *             This exception is thrown if an problem with messaging occurs.
     */
    void startServiceInstance(ServiceBuilder builder, Settings connectionSettings, String notificationChannel)
            throws ServiceStartupException, MessagingException;

    /**
     * This method triggers the provisioning of a new service instance at a particular time. The instance will be
     * associated with a unique Id. The builder specifies all bindings to external services, configurations, and
     * monitoring options. The notificationChannel points to the message bus and topic where status related events of
     * the framework are to be posted.
     * 
     * @param builder
     *            ServiceBuilder containing all information necessary to instantiate a service.
     * @param notificationChannel
     *            Callback channel for notifications of the SLA Manager.
     * @param startTime
     *            Defines the time when the service instance is to be available.
     * @param connectionSettings
     *            Contains the information about messaging connection.
     * @throws ServiceStartupException
     *             In case the startup fails (e.g., an incomplete or invalid builder is passed) a
     *             ServiceStartupException is thrown.
     * @throws MessagingException
     *             This exception is thrown if an problem with messaging occurs.
     */
    void startServiceInstance(ServiceBuilder builder, Settings connectionSettings, String notificationChannel,
            Date startTime) throws ServiceStartupException, MessagingException;

    /**
     * This operation triggers the unprovisioning of the service instance. This operation takes effect immediately.
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     */
    void stopServiceInstance(ServiceBuilder builder);

    /**
     * This operation is used for triggering a scheduled decommissioning of the service instance. Contrary to the
     * previously defined operation, this operation doesn't take effect immediately rather the decommissioning is
     * initiated at the time specified according to the operation argument.
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     * @param stopTime
     *            Time when the service instance is to be stopped.
     */
    void stopServiceInstance(ServiceBuilder builder, Date stopTime);

    /**
     * This operation allows the SoftwareSLAManager to query for the status of a particular service instance.
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     * @return String containing information about the current status of the service instance.
     */
    ServiceState queryServiceStatus(ServiceBuilder builder);

    /**
     * This operation allows the SoftwareSLAManager to query for the status of a particular service instance.
     * 
     * @param serviceID
     *            ID of the service instance. The ID of a service is similar to the ID of the builder that has been used
     *            to define the instance.
     * @return Information about the current status of the service instance.
     */
    ServiceState queryServiceStatus(String serviceID);

    /**
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     * @return Returns a list of available sensors and where to find their data.
     */
    List<SensorSubscriptionData> getSensorSubscriptionData(ServiceBuilder builder);

    /**
     * @param serviceID
     *            ID of the service instance. The ID of a service is similar to the ID of the builder that has been used
     *            to define the instance.
     * @return Returns a list of available sensors and where to find their data.
     */
    List<SensorSubscriptionData> getSensorSubscriptionData(String serviceID);

    /**
     * Applies the given configuration to the monitoring system of the managed ServiceInstance. The given configuration
     * replaces the previous one (if any).
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance. The ServiceBuilder contains the new
     *            monitoring configuration to used.
     */
    void configureMonitoringSystem(ServiceBuilder builder);

    /**
     * Disable the current configuration.
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     */
    void deconfigureMonitoring(ServiceBuilder builder);

    /**
     * Disable the current configuration.
     * 
     * @param serviceID
     *            ID of the service instance. The ID of a service is similar to the ID of the builder that has been used
     *            to define the instance.
     */
    void deconfigureMonitoring(String serviceID);

    /**
     * Execute domain-specific action for the adjustment of a ServiceInstance.
     * 
     * @param builder
     *            ServiceBuilder that has been used to define the instance.
     * @param action
     *            Domain-specific implementation of the IEffectorAction interface that contains information about what
     *            action to execute and its parameters.
     * @return The result of the action in a domain-specific format.
     */
    IEffectorResult executeAction(ServiceBuilder builder, IEffectorAction action);

    /**
     * Execute domain-specific action for the adjustment of a ServiceInstance.
     * 
     * @param serviceID
     *            ID of the service instance. The ID of a service is similar to the ID of the builder that has been used
     *            to define the instance.
     * @param action
     *            Domain-specific implementation of the IEffectorAction interface that contains information about what
     *            action to execute and its parameters.
     * @return The result of the action in a domain-specific format.
     */
    IEffectorResult executeAction(String serviceID, IEffectorAction action);
}
