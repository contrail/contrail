/**
 *  SVN FILE: $Id: IManageabilityAgentFacade.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/management/IManageabilityAgentFacade.java $
 */

package org.slasoi.softwareservicemanager.management;

import java.util.List;

import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;

/**
 * This interface provides a generic access point for the SoftwareServiceManager to use case specific
 * ManageabilityAgents. To correctly implement this interface, you need to consider the following:
 * 
 * There exists exactly (!) one IManageabilityAgentFacade per service instance. If multiple service instances are
 * managed by a single agent, the requests are dispatched by the Facade.
 * 
 * The methods of the Facade are kept on a general level. Basically, all more specific actions of a domain-specific
 * ManageabilityAgent have to be realised through the "executeAction" method.
 * 
 * @author Sam, Davide, Miha, Jens
 * 
 **/
public interface IManageabilityAgentFacade {

    /**
     * @return Returns the ServiceInstance that is controlled by the ManageabilityAgent.
     */
    ServiceInstance getInstance();

    /**
     * @return Returns a list of available sensors and where to find their data.
     */
    List<SensorSubscriptionData> getSensorSubscriptionData();

    /**
     * Applies the given configuration to the monitoring system of the managed ServiceInstance. The given configuration
     * replaces the previous one (if any).
     * 
     * @param configuration
     *            Configuration to be applied.
     */
    void configureMonitoringSystem(MonitoringSystemConfiguration configuration);

    /**
     * Disable the current configuration.
     */
    void deconfigureMonitoring();

    /**
     * Execute domain-specific action for the adjustment of a ServiceInstance.
     * 
     * @param action
     *            Domain-specific implementation of the IEffectorAction interface that contains information about what
     *            action to execute and its parameters.
     * @return The result of the action in a domain-specific format.
     */
    IEffectorResult executeAction(IEffectorAction action);

}
