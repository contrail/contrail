/**
 *  SVN FILE: $Id: ServiceInfoManager.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/impl/ServiceInfoManager.java $
 */

package org.slasoi.softwareservicemanager.impl;

import java.util.HashMap;
import java.util.UUID;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;

/**
 * This class manages all ServiceBuilders and their additional information.
 * 
 */
public class ServiceInfoManager {

    /**
     * Map containing meta data about the available services.
     */
    private HashMap<String, ServiceInformation> serviceInformationMap;

    /**
     * Create a new ServiceInfoManager.
     */
    public ServiceInfoManager() {
        serviceInformationMap = new HashMap<String, ServiceInformation>();
    }

    /**
     * @param serviceID
     *            ID of the service to be inquired.
     * @return The current status of the service.
     */
    public final ServiceState getServiceStatus(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        return si.getState();
    }

    /**
     * Change the status of the service with the given ID.
     * 
     * @param serviceID
     *            ID of the service whose status is to be changed.
     * @param state
     *            New state of the service.
     */
    public final void setServiceStatus(final String serviceID, final ServiceState state) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        si.setState(state);
    }

    /**
     * Creates a builder object for the passed implementation.
     * 
     * @param implementation
     *            Target implementation for the new builder.
     * @param facade
     *            SoftwareManagerFacade which responsible for invocation of this function.
     * @return Returns a new ServiceBuilder object.
     */
    public final ServiceBuilder createBuilder(final ServiceImplementation implementation,
            final SoftwareServiceManagerFacade facade) {
        ServiceBuilder builder = ServiceConstructionModel.getFactory().createServiceBuilder();
        builder.setImplementation(implementation);
        builder.setUuid(UUID.randomUUID().toString());
        serviceInformationMap.put(builder.getUuid(), new ServiceInformation(builder));
        ((ServiceBuilderExtended) builder).setManager(facade);
        return builder;

    }

    /**
     * 
     * @param serviceID
     *            Identifier of the service/builder the manageabilityAgent is needed for.
     * @return Returns the responsible ManageabilityAgent for the given service/builder identifier.
     */
    public final IManageabilityAgentFacade getManageabilityAgent(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        IManageabilityAgentFacade ma = si.getManageabilityAgent();
        if (ma == null) {
            throw new IllegalStateException("No ManageabilityAgent avaliable for Service: " + serviceID);
        }

        return ma;
    }

    /**
     * Sets the ManageabilityAgent for a Service/ServiceBuilder.
     * 
     * @param serviceID
     *            Identifier of the service/ ServiceBuilder.
     * @param agent
     *            Target manageabilityAgent.
     */
    public final void setManageabilityAgent(final String serviceID, final IManageabilityAgentFacade agent) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        si.setManageabilityAgent(agent);
    }

    /**
     * 
     * @param serviceID
     *            Identifier of the service of interest.
     * @return Returns the service instance for the passed service identifier.
     */
    public final ServiceInstance getServiceInstance(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        ServiceInstance instance = si.getServiceInstance();
        if (instance == null) {
            throw new IllegalStateException("No Service Instance has been assigned to ID: " + serviceID);
        }

        return instance;
    }

    /**
     * Setter for serviceInstance.
     * 
     * @param instance
     *            Target service instance.
     */
    public final void setServiceInstance(final ServiceInstance instance) {
        String serviceID = instance.getID();
        if (serviceID == null || serviceID.trim().length() == 0) {
            throw new IllegalArgumentException("ID of the ServiceInstance has not been set. "
                    + "Please set the instance's ID to the ID of its builder object.");
        }

        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        si.setServiceInstance(instance);
    }

    /**
     * Removes the manageabilityAgent for the service identified by the passed serviceID.
     * 
     * @param serviceID
     *            Identifier of the service of interest.
     */
    public final void removeManageabilityAgent(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        si.setManageabilityAgent(null);
    }

    /**
     * Removes the service instance of the service identified by the passed serviceID.
     * 
     * @param serviceID
     *            Identifier of the service of interest.
     */
    public final void removeServiceInstance(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        si.setServiceInstance(null);
    }

    /**
     * 
     * @param serviceID
     *            Identifier of the service the builder is needed for.
     * @return Returns the builder object for the passed serviceID.
     */
    public final ServiceBuilder getBuilder(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);
        if (si == null) {
            throw new IllegalArgumentException("Invalid Service-ID: " + serviceID);
        }

        return si.getServiceBuilder();
    }

    /**
     * 
     * @param serviceID
     *            Identifier of the service.
     * @return Returns true if a builder object for the passed serviceID exists.
     */
    public final boolean hasBuilder(final String serviceID) {
        ServiceInformation si = serviceInformationMap.get(serviceID);

        return !(si == null);

    }
}
