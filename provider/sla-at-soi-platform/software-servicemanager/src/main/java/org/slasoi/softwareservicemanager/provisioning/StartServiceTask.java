/**
 *  SVN FILE: $Id: StartServiceTask.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/provisioning/StartServiceTask.java $
 */

package org.slasoi.softwareservicemanager.provisioning;

import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;

/**
 * StartServiceTask provisions a service.
 * 
 * @author Alexander Wert
 * 
 */
public class StartServiceTask extends AbstractProvisioningTask {
    /**
     * Connection settings used for communication with invoker.
     */
    private Settings connectionSettings;
    /**
     * Notification channel used for notifications.
     */
    private String notificationChannel;
    /**
     * Responsible softwareServiceManager.
     */
    private SoftwareServiceManagerFacade ssm;

    /**
     * Constructor.
     * 
     * @param provManager
     *            ProvisioningManager responsible for the provisioning.
     * @param builder
     *            ServiceBuilder describing service to be started
     * @param settings
     *            Connection settings used for communication with invoker.
     * @param notificationChannel
     *            Notification channel used for notifications.
     * @param ssm
     *            Responsible softwareServiceManager.
     */
    public StartServiceTask(final IProvisioning provManager, final ServiceBuilder builder, final Settings settings,
            final String notificationChannel, final SoftwareServiceManagerFacade ssm) {
        super(provManager, builder);
        this.connectionSettings = settings;
        this.notificationChannel = notificationChannel;
        this.ssm = ssm;
    }

    /**
     * Starts the task.
     */
    public final void run() {
        try {
            getProvManager().startServiceInstance(getBuilder(), connectionSettings, notificationChannel);
            onProvisioningFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when provisioning is completed.
     */
    public final void onProvisioningFinished() {
        ssm.initializeServiceInstance(getBuilder());
    }

}
