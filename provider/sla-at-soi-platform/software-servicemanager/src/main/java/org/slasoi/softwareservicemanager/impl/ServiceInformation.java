/**
 *  SVN FILE: $Id: ServiceInformation.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/impl/ServiceInformation.java $
 */

package org.slasoi.softwareservicemanager.impl;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;

/**
 * This class host information about a service. That is the current state of the provisioning, its ManageabilityAgent
 * and service instance.
 * 
 */
public class ServiceInformation {
    /**
     * Current state of the service.
     */
    private ServiceState state;

    /**
     * ManageabilityAgent responsible for the service.
     */
    private IManageabilityAgentFacade manageabilityAgent;

    /**
     * Metadata describing the service instance.
     */
    private ServiceInstance serviceInstance;

    /**
     * Metadata describing the configuration & setup of the service.
     */
    private ServiceBuilder serviceBuilder;

    /**
     * Create a new information object for a given builder.
     * 
     * @param builder
     *            Builder used to create the service.
     */
    public ServiceInformation(final ServiceBuilder builder) {
        state = ServiceState.eBUILDER_CREATED;
        this.serviceBuilder = builder;
    }

    /**
     * Getter for the private state variable.
     * 
     * @return State variable.
     */
    public final ServiceState getState() {
        return state;
    }

    /**
     * Setter for the private state variable.
     * 
     * @param state
     *            New state.
     */
    public final void setState(final ServiceState state) {
        this.state = state;
    }

    /**
     * Getter for private ManageabilityAgent.
     * 
     * @return ManageabilityAgent.
     */
    public final IManageabilityAgentFacade getManageabilityAgent() {
        return manageabilityAgent;
    }

    /**
     * Setter for private ManageabilityAgent.
     * 
     * @param manageabilityAgent
     *            New manageabilityAgent.
     */
    public final void setManageabilityAgent(final IManageabilityAgentFacade manageabilityAgent) {
        this.manageabilityAgent = manageabilityAgent;
    }

    /**
     * Getter for private ServiceInstance.
     * 
     * @return ServiceInstance.
     */
    public final ServiceInstance getServiceInstance() {
        return serviceInstance;
    }

    /**
     * Setter for private ServiceInstance.
     * 
     * @param serviceInstance
     *            New ServiceInstance.
     */
    public final void setServiceInstance(final ServiceInstance serviceInstance) {
        this.serviceInstance = serviceInstance;
    }

    /**
     * Getter for private ServiceBuilder.
     * 
     * @return ServiceBuilder
     */
    public final ServiceBuilder getServiceBuilder() {
        return serviceBuilder;
    }
}
