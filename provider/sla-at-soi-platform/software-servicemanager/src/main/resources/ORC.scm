<?xml version="1.0" encoding="UTF-8"?>
<scm:Landscape xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:scm="http://scm.slasoi.org/1.0">
  <providedTypes ServiceTypeName="ORC_ServiceType" Description="ORC_Services">
    <Interfaces>PaymentService</Interfaces>
    <Interfaces>ORCInventoryService</Interfaces>
  </providedTypes>
  <implementations type="//@providedTypes.0" ServiceImplementationName="ORC_AllInOne" Description="ORC_AllInOne" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" Name="ORC_AllInOneServer" Desciption="ORC_AllInOneServer" HyperVisor="undefined" Version="1" HypervisorVersion="undefined" ApplianceType="X86" ImageFormat="to be defined by Intel">
      <dependencies Name="AllInOne Infrastructure" TargetURI="" targetType="//@requiredTypes.0" ID=""/>
      <serviceFeatures ID="" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="5" bootOrder="//@implementations.0/@artefacts.0"/>
  </implementations>
  <implementations type="//@providedTypes.0" ServiceImplementationName="ORC_Distributed" Description="ORC_Distributed" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" Name="ORC_DB" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="DB Infrastructure" Description="" TargetURI="" targetType="//@requiredTypes.0" ID=""/>
    </artefacts>
    <artefacts xsi:type="scm:VirtualAppliance" Name="ORC_Services" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="Application Server Infrastructure" Description="" TargetURI="" targetType="//@requiredTypes.0" ID=""/>
      <serviceFeatures ID="" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="20" bootOrder="//@implementations.1/@artefacts.0 //@implementations.1/@artefacts.1"/>
  </implementations>
  <requiredTypes ServiceTypeName="Infrastructure" Description="Host for Virtual Machines">
    <Interfaces>CPU</Interfaces>
    <Interfaces>HDD</Interfaces>
    <Interfaces>MEM</Interfaces>
    <Interfaces>NET</Interfaces>
  </requiredTypes>
</scm:Landscape>
