/**
 *  SVN FILE: $Id: SoftwareServiceManagerFacadeTest.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/test/java/org/slasoi/softwareservicemanager/impl/test/SoftwareServiceManagerFacadeTest.java $
 */

package org.slasoi.softwareservicemanager.impl.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.models.scm.extended.LandscapeExtended;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.exceptions.BookingException;
import org.slasoi.softwareservicemanager.exceptions.ReservationException;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.impl.BookingManager;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;
import org.slasoi.softwareservicemanager.provisioning.ProvisionServiceStub;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;

/**
 * This class provides test cases for SoftwareServiceManagerFacade class.
 * 
 * @author Alexander Wert
 * 
 */
public class SoftwareServiceManagerFacadeTest {

    /**
     * softwareServiceManagerFacade instance to be tested.
     */
    private SoftwareServiceManagerFacade ssm;

    /**
     * ProvisioningManager instance for the SoftwareServiceManager.
     */
    private IProvisioning provisioningManager;

    /**
     * Creates a softwareServiceManagerFacade instance using a landscape loaded from a file.
     */
    @Before
    public final void setUp() {
        provisioningManager = new ProvisionServiceStub();
        ssm = SoftwareServiceManagerFacade.createSoftwareServiceManager("src/test/resources/ORC.scm",
                provisioningManager);
    }

    /**
     * Tests the case when the creation of a SoftwareServiceManagerFacade is used not properly.
     */
    @Test
    public final void testInvalidSsmCreation() {
        boolean exception = false;
        try {
            SoftwareServiceManagerFacade.createSoftwareServiceManager("invalidpath", null);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
    }

    /**
     * Tests public constructor of SoftwareServiceManager.
     */
    @Test
    public final void testPublicConstructor() {
        assertNotNull(new SoftwareServiceManagerFacade());
    }

    /**
     * Tests reservation when capacity is expired.
     */
    @Test
    public final void testReservationFails() {
        ServiceImplementation impl = getAServiceImplementation();
        int cap = ssm.capacityCheck(impl);

        boolean resStatus = true;
        for (int i = 0; i < cap + 1; i++) {
            try {
                ServiceBuilder builder = createBuilder(impl);
                resStatus = ssm.reserve(builder, new Date(), new Date());
            } catch (ReservationException e) {
                assertTrue(false);
            }
        }
        assertFalse(resStatus);
    }

    /**
     * Tests validate function of SoftwareServiceManagerFacade.
     */
    @Test
    public final void testValidate() {
        try {
            ssm.validate();
        } catch (Exception e) {
            assertTrue(false);
        }

        // validation of ssm with an invalid booking manager
        BookingManager oldBookingManager = ssm.getBookingManager();
        ssm.setBookingManager(null);
        boolean exception = false;
        try {
            ssm.validate();
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
        ssm.setBookingManager(oldBookingManager);

        // validation of ssm with an invalid provisioning manager
        IProvisioning oldProvisioningManager = ssm.getProvisioningManager();
        ssm.setProvisioningManager(null);
        exception = false;
        try {
            ssm.validate();
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
        ssm.setProvisioningManager(oldProvisioningManager);

        // validation of a ssm with an invalid landscape
        ssm.setLandscape(null);
        exception = false;
        try {
            ssm.validate();
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
    }

    /**
     * Tests cloneBuilder function.
     */
    @Test
    public final void testCloneBuilder() {
        ServiceImplementation impl = getAServiceImplementation();
        ServiceBuilder builder = createBuilder(impl);
        assertTrue(ssm.hasBuilder(builder.getUuid()));
        builder.getBindingsList().add(ServiceConstructionModel.getFactory().createServiceBinding());
        builder.getServiceConfigurationsList()
                .add(ServiceConstructionModel.getFactory().createConfigurationDirective());
        ServiceBuilder clone = ssm.cloneBuilder(builder);
        assertTrue(builder.getImplementation().equals(clone.getImplementation()));
        assertFalse(builder.getUuid().equals(clone.getUuid()));
    }

    /**
     * Tests Landscape queries.
     */
    @Test
    public final void testWorkingScenario() {

        ServiceImplementation impl = getAServiceImplementation();

        assertTrue(ssm.capacityCheck(impl) > 0);

        ServiceBuilder builder = createBuilder(impl);

        Date startTime = reserve(builder);

        book(builder);

        try {
            provision(builder, startTime);
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        stop(builder);

    }

    /**
     * Tests the case trying to create a builder for an invalid service implementation.
     */
    @Test
    public final void testInvalidBuilderCreation() {
        // Try to create a builder for an invalid service implementation
        ServiceImplementation invalidImpl = ServiceConstructionModel.getFactory().createServiceImplementation();
        invalidImpl.setType(ServiceConstructionModel.getFactory().createServiceType());
        boolean exception = false;
        try {
            ssm.createBuilder(invalidImpl);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
    }

    /**
     * Tests cancel operations.
     */
    @Test
    public final void testCancelOperations() {
        ServiceImplementation impl = getAServiceImplementation();

        assertTrue(ssm.capacityCheck(impl) > 0);

        ServiceBuilder builder = createBuilder(impl);
        reserve(builder);

        try {
            // test update reservation
            assertTrue(ssm.updateReservation(builder.getUuid(), null, null));
            // test cancel reservation
            assertTrue(ssm.cancelReservation(builder.getUuid()));
        } catch (ReservationException e) {
            assertFalse(true);
        }

        // Try to cancel booking without having booked before.
        // canceling should throw an exception
        boolean exception = false;
        try {
            ssm.cancelBooking(builder);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        book(builder);

        try {
            assertTrue(ssm.cancelBooking(builder.getUuid()));
        } catch (BookingException e) {
            assertFalse(true);
        }

    }

    /**
     * Tests getter and setter functions of SoftwareServiceManager.
     */
    @Test
    public final void testGetterAndSetter() {
        IProvisioning provisioning = ssm.getProvisioningManager();
        assertEquals(provisioningManager, provisioning);

        assertTrue(ssm.getLandscape() instanceof LandscapeExtended);

        ServiceBuilder builder = createBuilder(getAServiceImplementation());

        ServiceBuilder queriedBuilder = ssm.getBuilder(builder.getUuid());

        assertEquals(builder, queriedBuilder);
        assertNotNull(ssm.getBookingManager());

        ssm.setLandscape(new LandscapeExtended());
        ssm.setBookingManager(new BookingManager());
        ssm.setProvisioningManager(new ProvisionServiceStub());
        ssm.setManageabilityAgent(builder, new DummyManageabilityAgent());

        // test setManageabilityAgent with an invalid ServiceBuilder instance
        boolean exception = false;
        try {
            ssm.setManageabilityAgent(new ServiceBuilderExtended(), new DummyManageabilityAgent());
        } catch (Exception e) {
            exception = true;
        }

    }

    /**
     * Gets a service implementation from the landscape.
     * 
     * @return service implementation.
     */
    private ServiceImplementation getAServiceImplementation() {
        List<ServiceType> types = ssm.queryServiceTypes();
        assertFalse(types.isEmpty());
        List<ServiceImplementation> impls = ssm.queryServiceImplementations(types.get(0));
        assertFalse(impls.isEmpty());
        assertNotNull(impls.get(0));
        return impls.get(0);
    }

    /**
     * Creates a ServiceBuilder using SoftwareServiceManager.
     * 
     * @param impl
     *            ServiceImplementation for which the builder should be created.
     * @return new builder.
     */
    private ServiceBuilder createBuilder(final ServiceImplementation impl) {
        ServiceBuilder b = ssm.createBuilder(impl);
        assertNotNull(b);
        assertEquals(impl, b.getImplementation());
        return b;
    }

    /**
     * Reserves resources for the service represented by the passed builder object.
     * 
     * @param builder
     *            ServiceBuilder representing the service for which resources should be reserved.
     * @return Returns the start time of the reservation.
     */
    private Date reserve(final ServiceBuilder builder) {
        // reservation
        Date startTime = new Date();
        Date stopTime = new Date();
        final int startTimeOffset = 10;
        final int endTimeOffset = 100;
        startTime.setTime(startTime.getTime() + startTimeOffset);
        stopTime.setTime(stopTime.getTime() + endTimeOffset);

        try {
            ssm.reserve(builder, startTime, stopTime);
        } catch (ReservationException re) {
            fail("Reservatino failed!");
        }
        assertEquals(ServiceState.eRESOURCES_RESERVED, ssm.queryServiceStatus(builder.getUuid()));
        return startTime;
    }

    /**
     * Books the service represented by the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder object representing the service.
     */
    private void book(final ServiceBuilder builder) {
        try {
            ssm.book(builder.getUuid());
        } catch (BookingException be) {
            fail("Booking Failed!");
        }

        assertEquals(ServiceState.eRESOURCES_BOOKED, ssm.queryServiceStatus(builder));
    }

    /**
     * Starts the provisioning of the service represented by the passed ServiceBuilder object at the given time.
     * 
     * @param builder
     *            ServiceBuilder object representing the service.
     * @param startTime
     *            Time at which the provisioning should be started.
     * @throws MessagingException
     *             Thrown if errors with sending notifications occur.
     */
    private void provision(final ServiceBuilder builder, final Date startTime) throws MessagingException {
        try {
            // start service instance in one second (1000 milliseconds)
            ssm.startServiceInstance(builder, null, "");

            // wait one second for provisioning being completed
            try {
                final int second = 1000;
                Thread.sleep(second);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // query service status
            ServiceState serviceStatus = ssm.queryServiceStatus(builder);
            assertEquals(serviceStatus, ServiceState.eSERVICE_RUNNING);

        } catch (ServiceStartupException e) {

            fail("ServiceStartupException: Service couldn't be started!");
            assertTrue(false);
        }
    }

    /**
     * Stops the execution of the service represented by the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBu8ilder object representing the service.
     */
    private void stop(final ServiceBuilder builder) {
        // stop service instance
        ssm.stopServiceInstance(builder);
        // query service status
        ServiceState serviceStatus = ssm.queryServiceStatus(builder);
        assertEquals(serviceStatus, ServiceState.eSERVICE_STOPPED);
    }

}
