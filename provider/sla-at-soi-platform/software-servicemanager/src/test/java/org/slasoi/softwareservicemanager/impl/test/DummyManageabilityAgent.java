/**
 *  SVN FILE: $Id: DummyManageabilityAgent.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/test/java/org/slasoi/softwareservicemanager/impl/test/DummyManageabilityAgent.java $
 */

package org.slasoi.softwareservicemanager.impl.test;

import java.util.List;

import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.softwareservicemanager.management.IEffectorAction;
import org.slasoi.softwareservicemanager.management.IEffectorResult;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;
import org.slasoi.softwareservicemanager.management.SensorSubscriptionData;

/**
 * This is a dummy ManageabilityAgent used only for testing.
 * 
 * @author Alexander Wert
 * 
 */
public class DummyManageabilityAgent implements IManageabilityAgentFacade {

    /**
     * Dummy method.
     * 
     * @return Returns null.
     */
    public final ServiceInstance getInstance() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Dummy method.
     * 
     * @return Returns null.
     */
    public final List<SensorSubscriptionData> getSensorSubscriptionData() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Dummy method.
     * 
     * @param configuration
     *            Dummy parameter.
     */
    public final void configureMonitoringSystem(final MonitoringSystemConfiguration configuration) {
        // TODO Auto-generated method stub

    }

    /**
     * Dummy method.
     */
    public final void deconfigureMonitoring() {
        // TODO Auto-generated method stub

    }

    /**
     * Dummy method.
     * 
     * @param action
     *            Dummy parameter.
     * @return Returns null.
     */
    public final IEffectorResult executeAction(final IEffectorAction action) {
        // TODO Auto-generated method stub
        return null;
    }

}
