/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.internal.test;

import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;

@SuppressWarnings("deprecation")
public class Utils{
    
    public static SLATemplate createTemplate(String id) {
        try {
            SLATemplate template = new SLATemplate();
            UUID templateID = new UUID(id);
            template.setUuid(templateID);

            Party parties[] = new Party[2];
            parties[0] = new Party();
            parties[1] = new Party();
            STND STNDpartyA = new STND();
            STND STNDpartyB = new STND();

            STNDpartyA.setValue("party.A");
            STNDpartyB.setValue("party.B");

            parties[0].setAgreementRole(STNDpartyA);
            parties[1].setAgreementRole(STNDpartyB);

            template.setParties(parties);

            return template;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void dump(SLATemplate template) {
        try {
            System.out.println(template.getUuid().getValue());

            Party[] parties = template.getParties();
            if (parties != null && parties.length != 0) {
                for (Party p : parties) {
                    System.out.println("\tParty '" + p.getAgreementRole().getValue() + "'");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
