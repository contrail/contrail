/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.server.internal.brokers;

import java.util.ArrayList;
import java.util.List;

import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.util.TraceBrokerPathPlugin;
import org.apache.activemq.security.AuthenticationUser;
import org.apache.activemq.security.SimpleAuthenticationPlugin;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slasoi.sa.pss4slam.core.PSS4SlamConfig;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementConstants;

/**
 * ActiveMQ-based BrokerService for the AdvertisementsService
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class PSS4SLAMBroker implements ServiceAdvertisementConstants {
    private static final Log LOGGER = LogFactory.getLog(PSS4SLAMBroker.class);

    /**
     * Default constructor.. Invokes the init() method.
     */
    public PSS4SLAMBroker() {
        init();
    }

    /**
     * Initializes the ActiveMQ-based BrokerService for the AdvertisementsService
     */
    protected void init() {
        try {
            LOGGER.info("Initializing PSS4SLAMBroker...");
            broker = new BrokerService();
            broker.setBrokerName(Broker.NAME);
            broker.setDataDirectory(Broker.DATA_DIR);
            broker.setPersistent(true);

            LOGGER.info("Setting users for PSS4SLAMBroker...");
            SimpleAuthenticationPlugin authentication = new SimpleAuthenticationPlugin();

            List<AuthenticationUser> users = new ArrayList<AuthenticationUser>();
            users.add(new AuthenticationUser("admin", "pss4slam.admin.password", "admins,publishers,consumers"));
            users.add(new AuthenticationUser("publisher", "pss4slam.publisher.password", "publishers,consumers"));
            users.add(new AuthenticationUser("consumer", "pss4slam.consumer.password", "consumers"));
            users.add(new AuthenticationUser("guest", "pss4slam.password", "guests"));
            authentication.setUsers(users);

            final TraceBrokerPathPlugin tracer = new TraceBrokerPathPlugin();

            broker.setPlugins(new BrokerPlugin[] { authentication, tracer });

            // LOGGER.info( "Linking PSS4SLAMBroker to '" + TransportConnector.TC_PPS4SLAM_HTTP + "'..." );
            // broker.addConnector( new HttpTransportServer( new URI( TransportConnector.TC_PPS4SLAM_HTTP ) ) );

            String url = PSS4SlamConfig.getInstance().get(TransportConnector.TC_PPS4SLAM_URL);

            LOGGER.info("Linking PSS4SLAMBroker to '" + url + "'...");
            broker.addConnector(url);

            broker.getManagementContext().setCreateConnector(false);
            broker.setUseJmx(false);

            broker.start();

            LOGGER.info("PSS4SLAMBroker initialized.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BrokerService broker() {
        return broker;
    }

    public void stop() throws Exception
    {
        broker.stop();
    }

    public static void main(String[] args) {
        new PSS4SLAMBroker();

        try {
            while (true)
            {
                Thread.sleep(1000);
                if ( args == null ) return;
            }
        }
        catch (Exception e) {
        }
    }

    protected BrokerService broker;
}
