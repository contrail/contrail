/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.internal.test;

import java.util.Arrays;
import java.util.Vector;

import javax.jms.JMSException;
import javax.jms.Message;

import junit.framework.TestCase;

import org.slasoi.sa.pss4slam.client.impl.DestinationFactory;
import org.slasoi.sa.pss4slam.client.impl.Publisher;
import org.slasoi.sa.pss4slam.client.impl.Subscriber;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;

public class PSS4SLAMClientTest extends TestCase {
    public PSS4SLAMClientTest() {
    }

    protected Subscriber[][] getSubscribers() {
        try {
            Subscriber array[][] = new Subscriber[2][MAX_SUBSCRIBERS];
            for (int i = 0; i < MAX_SUBSCRIBERS; i++) {
                array[0][i] =
                        new Subscriber(ServiceAdvertisementChannel.SLAMODEL, new SubscriberChild("child-slamodel-" + i));
                array[1][i] = new Subscriber(ServiceAdvertisementChannel.WSAG, new SubscriberChild("child-wsag-" + i));
            }

            return array;

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected Vector<Template<String>> getTemplates() {
        final int SIZE = MAX_TEMPLATES * 2;
        Vector<Template<String>> result = new Vector<Template<String>>(SIZE);

        for (int i = 0; i < SIZE; i++) {
            if (i < MAX_TEMPLATES) {
                result.add(new Template<String>("SLASOI-tmpl-#" + System.nanoTime()));
            }
            else {
                result.add(new Template<String>("WSAG-tmpl-#" + System.nanoTime()));
            }
        }

        return result;
    }

    public void testSubscribers() {
        try {
            Subscriber array[][] = getSubscribers();

            // close all connections
            for (int i = 0; i < MAX_SUBSCRIBERS; i++) {
                array[0][i].close();
                array[1][i].close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testSubscribersVersionII() {
        try {
            Subscriber array[][] = getSubscribers();

            // close all connections
            for (int i = 0; i < MAX_SUBSCRIBERS; i++) {
                array[0][i].close();
                array[1][i].close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testPublishers() {
        try {
            Publisher SLAMODELpublisher = new Publisher();
            Publisher WSAGpublisher = new Publisher();

            Subscriber[][] subscribers = getSubscribers();
            Vector<Template<String>> templates = getTemplates();

            final int SIZE = templates.size() / 2;

            for (int i = 0; i < templates.size(); i++) {
                if (i < SIZE) {
                    SLAMODELpublisher.publishTemplates(new Template<?>[] { templates.elementAt(i) },
                            ServiceAdvertisementChannel.SLAMODEL);
                }
                else {
                    WSAGpublisher.publishTemplates(new Template<?>[] { templates.elementAt(i) },
                            ServiceAdvertisementChannel.WSAG);
                }
            }

            Thread.sleep(10000); // wait 10 secs for spread templates

            // close all connections
            for (int i = 0; i < MAX_SUBSCRIBERS; i++) {
                subscribers[0][i].close();
                subscribers[1][i].close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        
        assertTrue(true);
    }

    public void testOnMessage() {
        try {
            class PubDecorator extends Publisher {
                public PubDecorator() throws JMSException {
                    super();
                }

                public Message createMessage(String[][] msg) throws JMSException {
                    return super.createTopicMessage(msg);
                }
            }
            ;

            PubDecorator dec = new PubDecorator();
            Message msg =
                    dec.createMessage(new String[][] { { "cnt", "param1", "param2" }, { "cnt", "param1", "param2" } });
            
            Subscriber array[][] = getSubscribers();
            array[0][0].onMessage(msg);
        }
        catch (Exception e) {
            assertTrue(false);
        }
        
        assertTrue(true);
    }
    
    public void testDestinationFactory()
    {
        try {
            DestinationFactory fact = new DestinationFactory();
            if ( fact != null )
            {
                assertTrue(true);
            }
        }
        catch (Exception e) {
            assertTrue(false);
        }
    }

    class SubscriberChild implements ISubscriber {
        public SubscriberChild(String name) {
            this.name = name;
        }

        public void notify(Template<?>[] templates) {
            try {
                System.out.println("\t subscriber '" + name + "' #templates:" + templates.length);
                System.out.println("\t\t >> " + Arrays.toString(templates));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        String name;
    }

    final int MAX_TEMPLATES = 20;
    final int MAX_SUBSCRIBERS = 30;
}
