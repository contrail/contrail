/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam;

import java.util.Arrays;
import java.util.Hashtable;

import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.sa.pss4slam.client.impl.Publisher;
import org.slasoi.sa.pss4slam.client.impl.Subscriber;
import org.slasoi.sa.pss4slam.core.IPublishable;
import org.slasoi.sa.pss4slam.core.ISubscribable;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementConstants;
import org.slasoi.sa.pss4slam.core.Template;

/**
 * Client of the Advertisements Service.  This bundle is responsible for providing the
 * advertisement of templates in the SLA@SOI Framework as client in any SLA-Manager.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class ServiceAdvertisementBroker implements ServiceAdvertisement, ServiceAdvertisementConstants, IPublishable,
        ISubscribable, SLAMContextAware {
    
    /**
     * Default constructor. This invokes internally the init() method.
     */
    public ServiceAdvertisementBroker() {
        init();
    }
    
    /**
     * Initializes the AdvertisementsService client, publisher and subscriber by default.
     */
    protected void init() {
        try {
            publisher = new Publisher();
            subscribers = new Hashtable<String, Subscriber>();
        }
        catch (Exception e) {
        }
    }

    public IPublishable getIPublishable() {
        return this;
    }

    public ISubscribable getISubscribable() {
        return this;
    }

    /**
     * Injection method for SLAManagerContext ( called only by the SLAManager - 'owner')
     */
    public void setSLAManagerContext(SLAManagerContext context) {
        this.context = context;
    }

    // ----------------------------------------------------------------------

    /**
     * Publishes an array of templates in the AdvertisementsService via the specified 'channel'
     * 
     */
    public void publish(Template<?>[] slatemplates, ServiceAdvertisementChannel channel) {
        try {
            publisher.publishTemplates(slatemplates, channel);
        }
        catch (Exception e) {
        }
    }

    /**
     * Subscribes to the AdvertisementsService using the specified 'channel'
     *
     * @return Returns a Subscription-ID.  This can be reused for unsubscribing purposes.
     */
    public String subscribe(ISubscriber subsc, ServiceAdvertisementChannel channel) {
        try {
            return subscribe(new Subscriber(channel, subsc));
        }
        catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    /**
     * Unsubscribes the specified SessionID
     */
    public boolean unsubscribe(String subscriptionID) {
        Subscriber subscriber = subscribers.get(subscriptionID);

        if (subscriber == null)
            return false;

        try {
            subscriber.close();
            subscribers.remove(subscriptionID);

            return true;
        }
        catch (Exception e) {
        }

        return false;
    }

    /**
     * Helper method
     */
    protected String subscribe(Subscriber subsc) {
        try {
            if (subsc != null) {
                long time = System.currentTimeMillis();
                String result = IDENTIFIER + "-" + time;
                subscribers.put(result, subsc);
                IDENTIFIER++;

                return result;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected Hashtable<String, Subscriber> subscribers;
    protected SLAManagerContext context;

    protected Publisher publisher;
    protected Subscriber subscriber;

    protected long IDENTIFIER = 0;

    /**
     * Helper Inner class.
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class SubscriberChild implements ISubscriber {
        public SubscriberChild(String name) {
            this.name = name;
        }

        public void notify(Template<?>[] templates) {
            try {
                if ( templates == null ) return;
                
                System.out.println("\t subscriber '" + name + "' #templates:" + templates.length);
                System.out.println("\t\t >> " + Arrays.toString(templates));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        String name;
    }
}
