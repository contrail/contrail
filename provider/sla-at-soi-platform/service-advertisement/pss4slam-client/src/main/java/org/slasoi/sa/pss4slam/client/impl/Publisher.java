/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.client.impl;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageProducer;

import org.apache.activemq.command.ActiveMQMapMessage;
import org.apache.activemq.command.ActiveMQTopic;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementConstants;
import org.slasoi.sa.pss4slam.core.Template;

public class Publisher implements ServiceAdvertisementConstants {
    public Publisher() throws JMSException {
        init();
    }

    protected void init() throws JMSException {
        activeMQ = new ActiveMQ("publisher", "pss4slam.publisher.password");
        producer = activeMQ.createProducer(null);
    }

    public void publishTemplates(Template<?>[] templates, ServiceAdvertisementChannel channel) throws JMSException {
        Message message = createTopicMessageWithTemplates(templates);
        ActiveMQTopic destination = DestinationFactory.getInstance().destination(channel);
        _dump_("Sending: " + ((ActiveMQMapMessage) message).getContentMap() + " on channel: " + destination);
        producer.send(destination, message);
    }

    public void close() throws JMSException {
        producer.close();
        activeMQ.close();
    }

    protected void publishMessage(String topic, String[][] content) throws JMSException {
        Destination destination = activeMQ.createTopic(topic);
        Message message = createTopicMessage(content);
        _dump_("Sending: " + ((ActiveMQMapMessage) message).getContentMap() + " on destination: " + destination);
        producer.send(destination, message);
    }

    protected void publishMessage(Destination destination, String[][] content) throws JMSException {
        Message message = createTopicMessage(content);
        _dump_("Sending: " + ((ActiveMQMapMessage) message).getContentMap() + " on destination: " + destination);
        producer.send(destination, message);
    }

    protected Message createTopicMessage(String[][] content) throws JMSException {
        MapMessage message = activeMQ.createMapMessage();

        for (String[] msg : content)
            message.setString(msg[0], msg[1]);

        return message;
    }

    protected Message createTopicMessageWithTemplates(Template<?>[] templates) throws JMSException {
        MapMessage message = activeMQ.createMapMessage();

        String templatesAsString = null;
        try {
            templatesAsString = Base64Utils.encode(templates);
        }
        catch (Exception e) {
        }
        
        message.setObject(Constants.TEMPLATES, templatesAsString);

        return message;
    }

    protected Destination createDestination(String topic) throws JMSException {
        return activeMQ.createTopic(topic);
    }

    protected void _dump_(String str) {
        System.out.println(str);
    }

    protected ActiveMQ activeMQ;
    protected transient MessageProducer producer;
}
