/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.client.impl;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;

import org.apache.activemq.command.ActiveMQTopic;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementConstants;
import org.slasoi.sa.pss4slam.core.Template;

public class Subscriber implements ServiceAdvertisementConstants, MessageListener {
    public Subscriber(ServiceAdvertisementChannel channel, ISubscriber sa) throws JMSException {
        init(channel, sa);
    }

    protected void init(ServiceAdvertisementChannel channel, ISubscriber sa) throws JMSException {
        activeMQ = new ActiveMQ("consumer", "pss4slam.consumer.password");

        ActiveMQTopic destination = DestinationFactory.getInstance().destination(channel);
        consumer = activeMQ.createConsumer(destination);
        consumer.setMessageListener(this);

        subscriber = sa;
    }

    public void close() throws JMSException {
        consumer.close();
        activeMQ.close();
    }

    // called by ActiveMQ Broker
    public void onMessage(Message content) {
        if (subscriber != null) {
            try {
                MapMessage map = (MapMessage) content;
                String contentAsString = (String) map.getObject(Constants.TEMPLATES);

                if ( contentAsString != null && !contentAsString.equals( "" ) ){
                    Template<?>[] decodeTemplates = Base64Utils.decodeTemplates(contentAsString);
                    subscriber.notify(decodeTemplates);
                }
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    
    private transient MessageConsumer consumer;
    private transient ISubscriber subscriber;
    protected ActiveMQ activeMQ;
}
