--------------------------------------------------------------------------------
              SLA@SOI Contrail Provider Level
--------------------------------------------------------------------------------

Support: For any question about the integration send an email to:
christian.temporale@hp.com


Introduction
------------
Contrail uses the SLA@SOI framework for providing SLA negotiation and
enforcement functionalities, both at provider and at federation level. This file
describes the installation, configuration and running of the SLA@SOI framework
customized for Contrail providers. Customization includes bugs found in SLA@SOI,
which prevented the custom Contrail SLA manager (and default ones) to function
properly.



Requirements
------------
Before installing the framework, the following software is required:
- JDK version 6.
- Apache Maven version 3.0.2 or later. Previous versions won't work. The build
was successfully tested with versions 3.0.2 and 3.0.3. Latest version (3.0.4)
should work too, even though it has not been tested yet.
- MySQL Community, version 5.1.41 or later
- Pax Runner version 1.7.1
- Rabbit MQ Server, version 2.7.1 or later.



DB installation
---------------
Download the sofware from http://dev.mysql.com/downloads/mysql/5.1.html
and follow the standard installation procedure.



DB configuraton
---------------
1. Create a slasoi user and grant him admin privileges (the framework 
configuration provided in the sections below uses �slasoi� as password).

SQL instruction example:
  create user 'slasoi'@'localhost' identified by 'slasoi';
  grant all privileges on *.* to 'slasoi'@'%' with grant option;
  grant all privileges on *.* to 'slasoi'@'%' identified by 'slasoi'
  grant all privileges on *.* to 'slasoi'@'localhost' identified by 'slasoi'


2. Create the SLA registry DB, by running the script:
generic-slamanager/sla-registry/db/model.sql

3. Create the SLA Template registries DB, by running these scripts:
common/osgi-config/Integration/templates/db/business/dbmodel.sql
common/osgi-config/Integration/templates/db/business/datamodel.sql

4. Create the main DB, by running the script:
business-manager/DB/bm-datamodel.sql



Pax runner installation
-----------------------
1. Download the software from:
svnroot/contrail/trunk/provider/sla/3rd-party/
pax-runner-assembly-1.7.1-jdk15.zip

2. Extract the archive to an arbitrary directory.

3. Change your PATH variable to include the bin subdirectory.
For example, add this to your ~/.bashrc file:
PATH=$PATH:/home/adcontrail/pax-runner-1.7.1/bin
export PATH



RabbitMq installation
---------------------

1. Download the software from http://www.rabbitmq.com/download.html and follow
the installation guide.

2. Test the server installation by running the command (from root):
# rabbitmqctl status



RabbitMq configuration
----------------------
1. Create the user "slasoi" with password "slasoi":
# rabbitmqctl add_user slasoi slasoi
You should see the new user with the command:
# rabbitmqctl list_users

2. Create new virtual host "slasoi":
# rabbitmqctl add_vhost /slasoi
You should see the new virtual host:
# rabbitmqctl list_vhosts

3. Grant user slasoi full permissions to /slasoi and default (/) virtualhosts:
# rabbitmqctl set_permissions -p /slasoi slasoi ".*" ".*" ".*"
# rabbitmqctl set_permissions -p / slasoi ".*" ".*" ".*"
Check the user permissions:
# rabbitmqctl list_user_permissions slasoi



Framework Installation
----------------------

1. Download the content of sla-at-soi-platform into the directory you want
SLA@SOI to be installed. We will refer to this directory as "main directory"
from now on.

2. Configure SLASOI_HOME in your environment variables. It must contain the
sub-path common/osgi-config. Example:
$ export
SLASOI_HOME="/home/adcontrail/contrail-svn/sla-at-soi/platform/trunk/common/osgi
-config"

3. Go to the main directory and compile the framework:
$ mvn clean install -DskipTests
The build process will take some hours.



Framework configuration
-----------------------

1. DB Configuration. Update the DB configuration (credentials, server address)
in the following files:
common/osgi-config/generic-slamanager/sla-registry/db/gslam-slaregistry.cfg.xml
common/osgi-config/generic-slamanager/template-registry/db.properties
common/osgi-config/BusinessManager/db.properties
common/osgi-config/bmanager-postsale-reporting/DB/db.properties

2. AMQP bus configuration. Configure the bus parameters (username, password,
host, virtualhost) in:
common/osgi-config/contrail-slamanager/provisioning-adjustment/eventbus.properti
es

3. Contrail SLAM endpoint. In the URL of variable slam.epr.negotiation, replace 
localhost with the current host name, in the configuration file: 
common/osgi-config/slams/contrail.instance1.cfg

4. OVF repo (Contrail SLAM PAC configuration). Configure the ovf_repo_path 
variable, containing the system path where to find the OVFs, in: 
common/osgi-config/contrail-slamanager/provisioning-adjustment/provisioning_adju
stment.properties

5. Provisioning manager URI (Contrail SLAM PAC configuration). Configure the 
URI_base variable, containing in the: 
common/osgi-config/contrail-slamanager/provisioning-adjustment/provisioning_adju
stment.properties



VEP mock
--------------
If VEP is not available and you just want to test the framework, you must 
install the mock VEP. Download the content of vepMock in the directory you want 
it to be run and compile it: $ mvn clean package -DskipTests



Contrail SLAM Installation and configuration
--------------------------------------------
1. Download the content of provider-sla on the directory you want the Contrail
SLAM to be installed.

2. VEP integration. POC requires a Vep endpoint to compute the available 
resources to negotiate. VEP connection parameters are provided to POC by adding a 
new file: common/osgi-config/contrail-slamanager/planning-optimization/src/main/
resources/vep.config 
it must contain the following parameters:

vep-username = username
vep-address = ipaddress
vep-port = portnumber
vep-base-path = basepath

the previous parameters allows to configure: username, ip address, port, and 
base path on which the vep is located. If no VEP installation is available it is 
possible to use a mock version of vep provided by the package vepMock.

3. Build the Contrail SLAM:
$ mvn clean install -DskipTests



Starting the framework
----------------------

1. Start the syntax converter. Go to generic-slamanager/syntax-converter
directory and start the syntax converter broker:
$ ./syc-broker

2. If VEP is not available and you just want to test the framework, you must 
start the mock VEP previously compiled (see VEP mock). To start the vepMock, go 
to vepMock directory and do: 
$ ./start.sh

3. Start the OSGI platform. Go to pax-runner directory and run:
$ pax-run







