package org.slasoi.common.messaging.pubsub;

import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class PubSubManagerInMemoryTest extends PubSubManagerTestCommon {
    final String PUBSUBMANAGER1_CONF_FILE = "src/test/resources/test1.properties";
    final String PUBSUBMANAGER2_CONF_FILE = "src/test/resources/test2.properties";

    @Override
    Properties loadPubSubManager1Properties() throws IOException {
        Properties props = loadPropertiesFile(PUBSUBMANAGER1_CONF_FILE);
        props.setProperty("pubsub", "in_memory");
        return props;
    }

    @Override
    Properties loadPubSubManager2Properties() throws IOException {
        Properties props = loadPropertiesFile(PUBSUBMANAGER2_CONF_FILE);
        props.setProperty("pubsub", "in_memory");
        return props;
    }

    @Test
    public void testCreatePubSubManager() throws IOException, MessagingException {
        Settings settings = new Settings();
        settings.setSetting(Setting.pubsub, "in_memory");
        settings.setSetting(Setting.xmpp_username, "test1");
        settings.setSetting(Setting.xmpp_password, "test1");
        settings.setSetting(Setting.xmpp_service, "slasoi");
        settings.setSetting(Setting.xmpp_resource, "test1");
        PubSubManager pubSubManager = PubSubFactory.createPubSubManager(settings);
        Settings actual = pubSubManager.getSettings();
        assertEquals(settings.getSetting(Setting.pubsub), actual.getSetting(Setting.pubsub));
        assertEquals(settings.getSetting(Setting.xmpp_username), actual.getSetting(Setting.xmpp_username));
        assertEquals(settings.getSetting(Setting.xmpp_password), actual.getSetting(Setting.xmpp_password));
        assertEquals(settings.getSetting(Setting.xmpp_service), actual.getSetting(Setting.xmpp_service));
        assertEquals(settings.getSetting(Setting.xmpp_resource), actual.getSetting(Setting.xmpp_resource));
        pubSubManager.close();
    }
}
