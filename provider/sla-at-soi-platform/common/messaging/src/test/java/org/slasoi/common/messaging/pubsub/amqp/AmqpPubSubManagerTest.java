package org.slasoi.common.messaging.pubsub.amqp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.common.messaging.pubsub.Subscription;

@Ignore
public class AmqpPubSubManagerTest {

	PubSubManager pubSubManager1;
	PubSubManager pubSubManager2;
	
	private static final String messageBody = "this is a test message";
	private static final String channel = "test-channel";
	private static final String channel2 = "test-channel2";
	
	private boolean received1;
	private boolean received2;
	
	@Before
	public void setup() {
		received1 = false;
		received2 = false;
	}
	
	@Test
	public void testApp() throws FileNotFoundException, MessagingException, IOException, InterruptedException {
		
		pubSubManager1 = PubSubFactory.createPubSubManager("test1.properties");
		pubSubManager2 = PubSubFactory.createPubSubManager("test2.properties");
		
		pubSubManager1.createChannel(new Channel(channel));
		pubSubManager1.createChannel(new Channel(channel2));
		pubSubManager1.subscribe(channel2);
		pubSubManager2.subscribe(channel);
		
		List<Subscription> subscriptions = pubSubManager2.getSubscriptions();
		Assert.assertEquals(subscriptions.size(), 1);
		Assert.assertEquals(subscriptions.get(0).getName(), channel);
		
		pubSubManager1.addMessageListener(new MessageListener() {
			
			@Override
			public void processMessage(MessageEvent messageEvent) {
				
				Assert.assertEquals(messageBody, messageEvent.getMessage().getPayload());
				received2 = true;
			}
		});
		
		pubSubManager2.addMessageListener(new MessageListener() {
			
			@Override
			public void processMessage(MessageEvent messageEvent) {
				
				Assert.assertEquals(messageBody, messageEvent.getMessage().getPayload());
				received1 = true;
			}
		}, new String[]{channel});
		
		PubSubMessage message = new PubSubMessage(channel, messageBody);
		pubSubManager1.publish(message);
		
		Thread.sleep(2000); // wait for message
		
		pubSubManager1.close();
		pubSubManager2.close();
		
		Assert.assertFalse(received2);
		Assert.assertTrue(received1);
	}
}
