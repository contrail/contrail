package org.slasoi.common.messaging.pubsub;

public enum PubSubEngine {
    XMPP,
    JMS,
    IN_MEMORY,
    AMQP,
}
