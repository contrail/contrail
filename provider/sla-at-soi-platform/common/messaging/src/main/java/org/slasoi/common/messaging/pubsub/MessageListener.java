package org.slasoi.common.messaging.pubsub;

public interface MessageListener extends org.slasoi.common.messaging.MessageListener {

    void processMessage(MessageEvent messageEvent);
}
