package org.slasoi.common.messaging.pubsub;

import java.io.Serializable;

/**
 * Represents a channel to publish messages and to which can be subscribed.
 */
public class Channel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    /**
     * Gets the name of the channel.
     *
     * @return name of the channel.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the channel.
     *
     * @param name name of the channel.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Default constructor.
     *
     * @param name name of the channel.
     */
    public Channel(String name) {
        setName(name);
    }

    /**
     * Default constructor.
     */
    public Channel() {
        this("");
    }
}
