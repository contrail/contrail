package org.slasoi.common.messaging;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Settings {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String XML = "xml";
    public static final String BASE64 = "base64";

    private static final Logger log = Logger.getLogger(Settings.class);

    private Properties properties;
    private String filePath;
    private boolean filePathUsed;

    public Settings() {
        filePathUsed = false;
        properties = new Properties();
    }

    public Settings(String filePath) throws FileNotFoundException, IOException {
        this();
        filePathUsed = true;
        this.filePath = filePath;
        properties.load(new FileInputStream(filePath));
    }

    public Settings(Properties properties) {
        filePathUsed = false;
        this.properties = properties;
    }

    public static Settings createSettings(String filePath) throws FileNotFoundException, IOException {
        return new Settings(filePath);
    }

    public static Settings createSettings(Properties properties) {
        return new Settings(properties);
    }

    public void readSettings(String filePath) {
        this.filePath = filePath;
        try {
            properties.load(new FileInputStream(filePath));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void flushSettings(String comment) {
        if (filePathUsed) {
            try {
                FileOutputStream fos = new FileOutputStream(filePath);
                properties.store(fos, comment);
                fos.flush();
                fos.close();
            }
            catch (FileNotFoundException e) {
                log.error(e.getMessage(), e);
            }
            catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String getSetting(Setting setting) {
        String property;

        switch (setting) {
        case messaging:
            return properties.getProperty("messaging");
        case pubsub:
            return properties.getProperty("pubsub");
        case encryption:
            property = properties.getProperty("encryption");
            if (property == null || property.equals("")) {
                // Default.
                return Settings.BASE64;
            }
            else {
                return properties.getProperty("encryption");
            }
        case xmpp_username:
            return properties.getProperty("xmpp_username");
        case xmpp_password:
            return properties.getProperty("xmpp_password");
        case xmpp_host:
            return properties.getProperty("xmpp_host");
        case xmpp_port:
            return properties.getProperty("xmpp_port");
        case xmpp_service:
            return properties.getProperty("xmpp_service");
        case xmpp_resource:
            return properties.getProperty("xmpp_resource");
        case xmpp_pubsubservice:
            return properties.getProperty("xmpp_pubsubservice");
        case xmpp_publish_echo_enabled:
            property = properties.getProperty("xmpp_publish_echo_enabled");
            if (property == null || property.equals("")) {
                return Settings.FALSE;
            }
            else {
                return properties.getProperty("xmpp_publish_echo_enabled");
            }
        case jms_host:
            return properties.getProperty("jms_host");
        case jms_port:
            return properties.getProperty("jms_port");
        case jms_username:
            return properties.getProperty("jms_username");
        case jms_password:
            return properties.getProperty("jms_password");
        case amqp_username:
        	return properties.getProperty("amqp_username");
        case amqp_password:
        	return properties.getProperty("amqp_password");
        case amqp_virtualhost:
        	return properties.getProperty("amqp_virtualhost");
        case amqp_federation_forwarding_channel:
        	return properties.getProperty("amqp_federation_forwarding_channel");
        case amqp_host:
        	return properties.getProperty("amqp_host");
        case amqp_port:
        	return properties.getProperty("amqp_port");
        default:
            return null;
        }
    }

    public void setSetting(Setting setting, String value) {
        switch (setting) {
        case messaging:
            properties.setProperty("messaging", value);
            break;
        case pubsub:
            properties.setProperty("pubsub", value);
            break;
        case encryption:
            properties.setProperty("encryption", value);
        case xmpp_username:
            properties.setProperty("xmpp_username", value);
            break;
        case xmpp_password:
            properties.setProperty("xmpp_password", value);
            break;
        case xmpp_host:
            properties.setProperty("xmpp_host", value);
            break;
        case xmpp_port:
            properties.setProperty("xmpp_port", value);
            break;
        case xmpp_service:
            properties.setProperty("xmpp_service", value);
            break;
        case xmpp_resource:
            properties.setProperty("xmpp_resource", value);
            break;
        case xmpp_pubsubservice:
            properties.setProperty("xmpp_pubsubservice", value);
            break;
        case xmpp_publish_echo_enabled:
            properties.setProperty("xmpp_publish_echo_enabled", value);
        case jms_host:
            properties.setProperty("jms_host", value);
            break;
        case jms_port:
            properties.setProperty("jms_port", value);
            break;
        case jms_username:
            properties.setProperty("jms_username", value);
            break;
        case jms_password:
            properties.setProperty("jms_password", value);
            break;
        case amqp_username:
            properties.setProperty("amqp_username", value);
            break;
        case amqp_password:
            properties.setProperty("amqp_password", value);
            break;
        case amqp_virtualhost:
            properties.setProperty("amqp_virtualhost", value);
            break;
        case amqp_host:
            properties.setProperty("amqp_host", value);
            break;
        case amqp_port:
            properties.setProperty("amqp_port", value);
            break;
        default:
            break;
        }
    }
}
