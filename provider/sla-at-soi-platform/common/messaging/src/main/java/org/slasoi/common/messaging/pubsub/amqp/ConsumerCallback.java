package org.slasoi.common.messaging.pubsub.amqp;

import java.io.IOException;

import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubMessage;

import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.client.AMQP.BasicProperties;

public class ConsumerCallback implements Consumer {
	
	private IMessageEventHandler messageEventHanlder;
	private String id;
	
	public ConsumerCallback(IMessageEventHandler messageEventHandler, String id) {
		this.messageEventHanlder = messageEventHandler;
		this.id = id;
	}

	@Override
	public void handleCancelOk(String consumerTag) {
	}

	@Override
	public void handleConsumeOk(String consumerTag) {
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope,
			BasicProperties properties, byte[] body) throws IOException {

		PubSubMessage message = new PubSubMessage();
		message.setChannelName(envelope.getExchange());
		message.setFrom(id);
		message.setPayload(new String(body));

		MessageEvent messageEvent = new org.slasoi.common.messaging.pubsub.MessageEvent(
				this, message);
		
		messageEventHanlder.fireMessageEvent(messageEvent);
	}

	@Override
	public void handleRecoverOk() {
	}

	@Override
	public void handleShutdownSignal(String consumerTag,
			ShutdownSignalException sig) {
	}
}
