package org.slasoi.common.messaging.pubsub;

public class Subscription {

    private final String subscriptionID;
    private final String channelName;

    public Subscription(String subscriptionID, String channelName) {
        this.subscriptionID = subscriptionID;
        this.channelName = channelName;
    }

    public String getSubscriptionID() {
        return subscriptionID;
    }

    public String getName() {
        return channelName;
    }
}
