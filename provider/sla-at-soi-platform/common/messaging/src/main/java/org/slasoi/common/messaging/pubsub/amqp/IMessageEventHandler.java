package org.slasoi.common.messaging.pubsub.amqp;

import org.slasoi.common.messaging.pubsub.MessageEvent;

public interface IMessageEventHandler {

	public void fireMessageEvent(MessageEvent messageEvent);
}
