package org.slasoi.common.messaging;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class MessagingActivator implements BundleActivator {
    public void start(BundleContext context) throws Exception {
        System.out.println("------------------------------- Messaging started -------------------------------------");

    }

    public void stop(BundleContext context) throws Exception {
        System.out.println("------------------------------- Messaging stopped -------------------------------------");
    }
}
