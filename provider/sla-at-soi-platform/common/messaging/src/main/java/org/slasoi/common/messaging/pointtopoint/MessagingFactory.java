package org.slasoi.common.messaging.pointtopoint;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class MessagingFactory {

    public static Messaging createMessaging(String filePath) throws MessagingException, FileNotFoundException,
            IOException {
        return createMessaging(Settings.createSettings(filePath));
    }

    public static Messaging createMessaging(Settings settings) throws MessagingException {
        MessagingEngine messagingEngine = null;
        MessagingEngine[] values = MessagingEngine.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].toString().toLowerCase().equals(settings.getSetting(Setting.messaging).toLowerCase())) {
                messagingEngine = values[i];
            }
        }

        switch (messagingEngine) {
            case XMPP:
                return new org.slasoi.common.messaging.pointtopoint.xmpp.Messaging(settings);
        }

        return null;
    }

    public static Messaging createMessaging(Properties properties) throws MessagingException {
        return createMessaging(Settings.createSettings(properties));
    }
}
