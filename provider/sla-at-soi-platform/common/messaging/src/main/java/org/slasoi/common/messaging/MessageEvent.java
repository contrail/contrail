package org.slasoi.common.messaging;

import java.util.EventObject;

public abstract class MessageEvent extends EventObject {

    private static final long serialVersionUID = 580754887271121676L;

    public abstract Message getMessage();

    public MessageEvent(Object source) {
        super(source);
    }
}
