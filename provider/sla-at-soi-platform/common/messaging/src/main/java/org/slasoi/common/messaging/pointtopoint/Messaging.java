package org.slasoi.common.messaging.pointtopoint;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

import javax.swing.event.EventListenerList;

public abstract class Messaging {

    private Settings settings;

    protected EventListenerList listenerList;

    public Messaging(Settings settings) throws MessagingException {
        listenerList = new EventListenerList();
        this.settings = settings;
    }

    public abstract void close();

    public abstract void addMessageListener(MessageListener messageListener);

    public abstract void sendMessage(Message message);

    public abstract String getAddress();

    protected String getSetting(Setting setting) {
        return settings.getSetting(setting);
    }

    protected void fireMessageEvent(MessageEvent messageEvent) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = 0; i < listeners.length; i += 2) {
            if (listeners[i] == MessageListener.class) {
                ((MessageListener) listeners[i + 1]).processMessage(messageEvent);
            }
        }
    }

    public void removeMessageListener(MessageListener messageListener) {
        listenerList.remove(MessageListener.class, messageListener);
    }
}
