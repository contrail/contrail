package org.slasoi.common.messaging.pubsub.xmpp;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.UUID;


public class CredentialValidator extends Observable {

    private static final Logger log = Logger.getLogger(CredentialValidator.class);

    private final XMPPConnection connection;
    private Settings settings;
    private String user;
    private String password;
    private SecureRandom prng;

    public CredentialValidator(XMPPConnection connection, Settings settings) {
        this.connection = connection;
        this.settings = settings;
        try {
            prng = SecureRandom.getInstance("SHA1PRNG");
            // Only java6 compatible, important fix for now
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void checkCredentials(String existingUser, String existingPassword) {

        if (existingUser.length() == 0) {

            // The entry does not exist - lets create a user account!
            log.info("There are no credentials associated with this node. Temporary credentials will be auto-generated and registered.");
            AccountManager accountManger = connection.getAccountManager();

            Map<String, String> additionalInfo = new HashMap<String, String>();
            additionalInfo.put("misc",
                    "This is an automatically created account. Please contact the administrator for further information.");

            // added random prefix as UUID is equal on cloned virtual machines (at least on KVM)
            String newUserRnd = new Integer(prng.nextInt()).toString();
            String newUserPass = new Integer(prng.nextInt()).toString();
            String newUser = newUserRnd + "-UUUU-" + UUID.randomUUID().toString();
            String newPassword = newUserPass + "-PPPP-" + UUID.randomUUID().toString();

            try {
                accountManger.createAccount(newUser, newPassword, additionalInfo);
            }
            catch (XMPPException accountCreationError) {
                accountCreationError.printStackTrace();
            }

            this.user = newUser + "@" + connection.getServiceName();
            this.password = newPassword;

            log.info("Saving credentials to configuration file...");

            settings.setSetting(Setting.xmpp_username, user);
            settings.setSetting(Setting.xmpp_password, password);
//            settings.flushSettings("Added username and password");	// do not store temporary credentials

            // notify that nodeId and nodeAuth has changed
            setChanged();
            notifyObservers();
        }
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
