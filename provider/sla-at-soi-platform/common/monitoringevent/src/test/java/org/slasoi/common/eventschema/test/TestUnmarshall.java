/**
 * 
 */
package org.slasoi.common.eventschema.test;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date Apr 13, 2011
 */
public class TestUnmarshall {
	private static Logger logger = Logger.getLogger(TestUnmarshall.class);
	static {
		PropertyConfigurator.configure("./conf/log4j.properties");
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	@Test
	public void testUnmarshall() {
		String eventXML = getEventInstanceXML();
		
		MonitoringEventServiceImpl monitoringEventService = new MonitoringEventServiceImpl();
		EventInstance event = null;
	
		logger.debug(eventXML);
		
		try {
			event = monitoringEventService.unmarshall(eventXML);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		logger.debug(event);
		
		Assert.assertNotNull(event);
		Assert.assertEquals("TreatmentReservationId", event.getEventPayload().getInteractionEvent().getParameters().getArgument().get(0).getSimple().getArgName());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * Reads the event XML file and returns it
	 * @return The context of event XML file
	 */
	private String getEventInstanceXML() {
        FileReader read;
        StringBuffer sb = new StringBuffer();
        
        try {
        	String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        	
        	slasoiOrcHome = "/Users/davide/Documents/CITY/SLA@SOI/svn-sourceforge/platform/trunk/common/monitoringevent";
        	
        	if (slasoiOrcHome == null) {
        		logger.error("SLASOI_HOME variabile not set");
        		System.exit(1);
        	}
        	
        	//read = new FileReader(slasoiOrcHome + "/src/test/resources/Test_SLA_1_Events_My_2.xml");
        	read = new FileReader(slasoiOrcHome + "/src/test/resources/event-01.xml");
        	
            BufferedReader br = new BufferedReader(read);
            sb = new StringBuffer();
            String row;
            
            // reads file
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
        } catch (IOException e) {
        	logger.error(e);
        	e.printStackTrace();
        }
        
        return sb.toString();
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
