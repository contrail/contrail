/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Davide Lorenzoli - Davide.Lorenzoli.1@soi.city.ac.uk, George Spanoudakis - G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.common.eventschema.test;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date Sep 9, 2010
 */
public class MonitoringEventServiceImplTest extends TestCase {
	private static Logger logger = Logger.getLogger(MonitoringEventServiceImplTest.class);
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// initialise logging
    	PropertyConfigurator.configure("./conf/log4j.properties");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws JAXBException
	 */
	public void testMarshall() throws Exception {
		MonitoringEventServiceImpl service = new MonitoringEventServiceImpl();
		
		EventInstance eventInstance = service.unmarshall(getEventInstanceXML());
		String eventInstanceXML = service.marshall(eventInstance);
		
		assertTrue(eventInstanceXML.indexOf("<ns3:ID>7182340883285100550</ns3:ID>") > -1);
		assertTrue(eventInstanceXML.indexOf("<ns3:Timestamp>1282206835812</ns3:Timestamp>") > -1);
		assertTrue(eventInstanceXML.indexOf("<ns3:IP>138.40.95.191</ns3:IP>") > -1);
	}
	
	/**
	 * @throws JAXBException
	 */
	public void testUnmarshall() throws Exception {
		MonitoringEventServiceImpl service = new MonitoringEventServiceImpl();
		
		EventInstance eventInstance = service.unmarshall(getEventInstanceXML());
		
		assertEquals(7182340883285100550L, eventInstance.getEventID().getID());
		assertEquals("ServiceOperationCallStartEvent", eventInstance.getEventID().getEventTypeID());
		assertEquals(1282206835812L, eventInstance.getEventContext().getTime().getTimestamp());
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * Reads the event XML file and returns it
	 * @return The context of event XML file
	 */
	private String getEventInstanceXML() {
        FileReader read;
        StringBuffer sb = new StringBuffer();
        
        try {
        	String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        	
        	if (slasoiOrcHome == null) {
        		logger.error("SLASOI_HOME variabile not set");
        		System.exit(1);
        	}
        	
        	read = new FileReader(slasoiOrcHome + "/common/monitoringevent/test/event-01.xml");
        	
            BufferedReader br = new BufferedReader(read);
            sb = new StringBuffer();
            String row;
            
            // reads file
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
        } catch (IOException e) {
        	logger.error(e);
        	e.printStackTrace();
        }
        
        return sb.toString();
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
