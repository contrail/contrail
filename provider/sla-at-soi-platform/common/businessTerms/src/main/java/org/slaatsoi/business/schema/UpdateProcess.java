//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.06.17 at 11:09:27 AM CEST 
//


package org.slaatsoi.business.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateProcess">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.slaatsoi.org/BusinessSchema}BusinessParameter">
 *       &lt;sequence>
 *         &lt;element name="Frecuency" type="{http://www.slaatsoi.org/BusinessSchema}DurationType"/>
 *         &lt;element name="DistributionMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InstalationDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateProcess", propOrder = {
    "frecuency",
    "distributionMethod",
    "instalationDescription"
})
public class UpdateProcess
    extends BusinessParameter
    implements Serializable
{

    @XmlElement(name = "Frecuency", required = true)
    protected DurationType frecuency;
    @XmlElement(name = "DistributionMethod", required = true)
    protected String distributionMethod;
    @XmlElement(name = "InstalationDescription", required = true)
    protected String instalationDescription;

    /**
     * Gets the value of the frecuency property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getFrecuency() {
        return frecuency;
    }

    /**
     * Sets the value of the frecuency property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setFrecuency(DurationType value) {
        this.frecuency = value;
    }

    /**
     * Gets the value of the distributionMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionMethod() {
        return distributionMethod;
    }

    /**
     * Sets the value of the distributionMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionMethod(String value) {
        this.distributionMethod = value;
    }

    /**
     * Gets the value of the instalationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstalationDescription() {
        return instalationDescription;
    }

    /**
     * Sets the value of the instalationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstalationDescription(String value) {
        this.instalationDescription = value;
    }

}
