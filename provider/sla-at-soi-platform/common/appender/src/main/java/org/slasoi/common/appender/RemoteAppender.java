package org.slasoi.common.appender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubMessage;

public class RemoteAppender extends AppenderSkeleton
{
	private PubSubManager pubSubManager;
	private String channel;
	private String pubsub;
	private String xmpp_username;
	private String xmpp_password;
	private String xmpp_host;
	private String xmpp_port;
	private String xmpp_service;
	private String xmpp_resource;
	private String xmpp_pubsubservice;

	public void setChannel(String channel)  { this.channel = channel; }
	public void setPubsub(String pubsub)  { this.pubsub = pubsub; }
	public void setXmpp_username(String xmpp_username)  { this.xmpp_username = xmpp_username; }
	public void setXmpp_password(String xmpp_password)  { this.xmpp_password = xmpp_password; }
	public void setXmpp_host(String xmpp_host)  { this.xmpp_host = xmpp_host; }
	public void setXmpp_port(String xmpp_port)  { this.xmpp_port = xmpp_port; }
	public void setXmpp_service(String xmpp_service)  { this.xmpp_service = xmpp_service; }
	public void setXmpp_resource(String xmpp_resource)  { this.xmpp_resource = xmpp_resource; }
	public void setXmpp_pubsubservice(String xmpp_pubsubservice)  { this.xmpp_pubsubservice = xmpp_pubsubservice; }

	public boolean requiresLayout(){ return true; }

	public void activateOptions()
	{  
		try
		{
			Settings settings = new Settings();
			settings.setSetting(Setting.pubsub, this.pubsub);
			settings.setSetting(Setting.xmpp_username, this.xmpp_username);
			settings.setSetting(Setting.xmpp_password, this.xmpp_password);
			settings.setSetting(Setting.xmpp_host, this.xmpp_host);
			settings.setSetting(Setting.xmpp_port, this.xmpp_port);
			settings.setSetting(Setting.xmpp_service, this.xmpp_service);
			settings.setSetting(Setting.xmpp_resource, this.xmpp_resource);
			settings.setSetting(Setting.xmpp_pubsubservice, this.xmpp_pubsubservice);

			pubSubManager = PubSubFactory.createPubSubManager(settings);
			if(!pubSubManager.isChannel(channel))
			{
				pubSubManager.createChannel(new Channel(channel));
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}

	public synchronized void append( LoggingEvent event )
	{   
		PubSubMessage message = new PubSubMessage(channel, this.layout.format(event));
		try
		{
			pubSubManager.publish(message);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}

	public synchronized void close()
	{   try
	{   
		pubSubManager.close();
	}
	catch( Exception e )
	{   errorHandler.error("Exception while closing: " + e);
	}
	}
}

