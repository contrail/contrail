package org.slasoi.common.slautil;

import java.io.File;
import java.io.FileInputStream;

import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.sla;

import eu.slaatsoi.slamodel.SLATemplateDocument;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class SlaUtilTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SlaUtilTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SlaUtilTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    /*private void testSLATemplate(SLATemplate template, String newValue)
    {
    	SlaUtil app = new SlaUtil();
		AgreementTerm term = getRandomAgreementTerm(template);
		Guaranteed.State state = getFirstState(term);
		
		//get value from SLATempalte
		CONST outValue = app.getValue(term, state);
		
		//change that value
		outValue.setValue(newValue);
		
		//changes should already be seen in template
    }

    private AgreementTerm getRandomAgreementTerm(SLATemplate slat)
	{
		AgreementTerm[] terms= slat.getAgreementTerms();
		if (terms == null || terms.length == 0)
		{
			return null;
		}
		
		int count = 50;
		AgreementTerm term = null;
		boolean good;
		do
		{
			term = terms[random(terms.length)];
			good = term != null && getFirstState(term) != null;
		} 
		while (!good && count > 0);
		return (good) ? term : null;
	}
	
	private Guaranteed.State getFirstState(AgreementTerm term)
	{
		Guaranteed[] guarantees = term.getGuarantees();
		Guaranteed.State state = null;
		
		for (Guaranteed guaranteed: guarantees)
		{
			if (guaranteed instanceof Guaranteed.State)
			{
				state = (Guaranteed.State) guaranteed;
			}
		}
		return state;
	}
	
	private int random(int i)
	{
		return (int)(Math.random()*i);
	}*/
	
	public static void main(String[] args)
	{
		System.out.println("TEST");
		SlaUtilTest tests = new SlaUtilTest(null);
		
		tests.perform();
	}
	
	public void perform()
	{
		try
		{	
			SLATemplate template = constructTemplateFromFile();
			SlaUtil.setPrice(template, business.per_month, 500);
			
			
			Evaluator evaluator = new Evaluator(template);
			
			CONST value = new CONST("70", units.tx_per_s);
			FunctionalExpr expr = new FunctionalExpr(common.arrival_rate, new ValueExpr[] { new ID("ORCPaymentService/PaymentServiceOperation")});
			
			for (int i=0; i<15; i++)
			{
				evaluator.addNotify(expr, value);
			}
			
			while(true)
			{
				Violation violation = evaluator.getViolation();
				
				if (violation == null)
				{
					break;
				}
				
				STND role = (sla.customer.equals(new STND(violation.getPartyID().toString()))) ? sla.customer: sla.provider;
				System.out.println(role.toString() + "=" + violation);
			}	
			
			
			//SLASOITemplateRenderer renderer = new SLASOITemplateRenderer();
			//String s = renderer.renderSLATemplate(template);
			//System.out.println(s);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public SLATemplate constructTemplateFromFile() throws Exception 
	{
		FileInputStream in = new FileInputStream(new File("/home/jure_med/Documents/slasoi/sla-at-soi/studio/trunk/orc-data/BSLATs/ORC-SLAT.xml"));
		SLATemplateDocument slaTemplateXml = SLATemplateDocument.Factory.parse(in);
		SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();
		SLATemplate slaTemplate = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());
		return slaTemplate;
	}
}
