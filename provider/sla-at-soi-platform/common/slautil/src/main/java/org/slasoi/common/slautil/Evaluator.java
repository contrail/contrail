package org.slasoi.common.slautil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.business.Penalty;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;

/*
 * This evaluator would be some kind of state machine which would store all violations internally
 * There should be some complex action on adding violation
 * 	violation_count++
 * 		- check all conditions that i violate after incrementing violation_count (business.*)
 * 
 * Even better solution would be if i would make a state machine as separed class and those would only be helper functions
 * 		- i think that it could be done
 * 			- need to expose all Maps ...
 * 
 * CREATE: AgreementTerm, IPENALTY_REPORT(void report_event(all_description))
 * IN: Map<FunctionalExpr, CONST> values
 * OUT: void
 */

/*by JSON: 
	SLA_ID -> SLA
	AGREEMENT TERM
	OPERATOR
*/

class FunctionValue
{
	FunctionalExpr expr;
	CONST value;
	
	public FunctionValue(FunctionalExpr expr, CONST value)
	{
		this.expr = expr;
		this.value = value;
	}
	
	public FunctionalExpr getExpresion()
	{
		return this.expr;		
	}
	
	public CONST getValue()
	{
		return this.value;
	}
}


/*
 * At least addNotify() and getViolations() are NOT thread safe
 * so all calls to that two methods MUST be called from SAME thread  
 */

/*
 * It would be very nice if I would have map action-> term to determine which agreement term was violated
 * 
 * Need to find system for managing violations, currently it is not implemented
 * (moving from viol to violHist)
 * 	-> i i implement it in QUEUE i can just use poll and push methods
 * 		-> pull x from viol
 * 		-> push x to hist
 * 		-> return x
 * 	-> on user end while queue != empty
 * 		-> get it -> inform parties
 *  -> some additional work would be needed to sort them by time
 */

/*
 *  Parties and violations are needed for validation of SLA -> but this should be done before evaluation
 *  - is is partly implemented in framework, otherwise it can be implemented in another class here
 *  - then stages would be 1.validate, 2.evaluate 
 * 
 */
public class Evaluator
{
	/*private Party[] parties;
	private InterfaceDeclr[] interfaces;*/
	private AgreementTerm[] terms;
	private List<Environment> environments;
	private Map<Guaranteed.Action, List<Date>> violationHistory;
	private Queue<Violation> violations;	
	private Queue<FunctionValue> events;
	private Map<ID, Integer> sumOfPenalties;
	private boolean customerViolationHappened;
	
	/* what about list of states:
	 * started/stopped -> violation count make that decisions
	 * 	- i return termination Action, now PAC should do something about it
	 * 	- or it can be deleted from SLA now
	 * 		- but then we need to check if it even exists  
	 */
	
	public Evaluator(SLATemplate slaTemplate)
	{	
		/*interfaces = slaTemplate.getInterfaceDeclrs();
		parties = slaTemplate.getParties();*/
		terms = slaTemplate.getAgreementTerms();
		environments = new ArrayList<Environment>();
		events = new LinkedList<FunctionValue>();
		
		sumOfPenalties = new HashMap<ID, Integer>();
		for (AgreementTerm term: terms)
		{
			environments.add(new Environment(term));
		}
		
		violations = new LinkedList<Violation>();
		violationHistory = new HashMap<Guaranteed.Action, List<Date>>();
	}
	
	public Violation getViolation()
	{
		return violations.poll();
	}
	
	private List<Guaranteed.Action> notifyState(FunctionalExpr expr, CONST value, boolean react)
	{
		Map<FunctionalExpr, CONST> values = new HashMap<FunctionalExpr, CONST>();
		values.put(expr, value);
		
		List<Guaranteed.Action> result = new ArrayList<Guaranteed.Action>();
		for (Environment env: environments)
		{
			Set<TypeConstraintExpr> constraints = env.actions.keySet();
			for (TypeConstraintExpr constraint: constraints)
			{
				if (!env.isSame(expr,env.getVariableForConstraint(constraint)))
					continue;
				
				boolean evaluationResult = env.evaluate(constraint, values); 
				boolean isCustomerConstraint = expr.getOperator().equals(common.arrival_rate);
				if(isCustomerConstraint){
					if(!evaluationResult){
						this.customerViolationHappened = true;	
					} else {
						this.customerViolationHappened = false;
					}
				}
							
				for (Guaranteed.Action action: env.actions.get(constraint))
				{
					
					// mostly it is always core.violated
					STND operator = action.getPrecondition().getOperator();
					if (operator.equals(core.violated) && !evaluationResult)
					{
						result.add(action);
						if (react == true)
						{
							if((!this.customerViolationHappened && !isCustomerConstraint) || isCustomerConstraint){
								addViolation(env, expr, action, operator);
							}
						}
					}
				}
			}
		}
		return result;
	}
	
	private void addViolation(Environment environment, FunctionalExpr func, Guaranteed.Action action, STND operator)
	{
		// save violation into evaluator
		List<Date> dates = null;
		if (violationHistory.containsKey(action))
		{
			dates = violationHistory.get(action);
		}
		else
		{
			dates = new ArrayList<Date>();
		}
		Date date = new Date();
		dates.add(date);
		violationHistory.put(action, dates);
		
		CONST penalty = environment.getPrice(action);
		ID actorID = action.getActorRef();
		int penaltyValue = 0;
		if (penalty != null)
		{
			Integer sum = 0;
			if (sumOfPenalties.containsKey(actorID))
			{
				sum = sumOfPenalties.get(actorID); 
			}
			penaltyValue = Integer.parseInt(penalty.getValue());
			sum += penaltyValue;
			sumOfPenalties.put(actorID, sum);
		}
		violations.add(new Violation(environment, func, action, date, actorID, penaltyValue, sumOfPenalties.get(actorID)));
		
		/*build and add all actions  
		 * -> they will be automaticly called in this session
		 * -> that is because evaluation will not stop until all actions in list are evaluated
		 * */ 
		if (operator.equals(core.violated))
		{
			//(now only violation_count, there can be any business.*)
			FunctionalExpr expr = new FunctionalExpr(business.violation_count, new ValueExpr[] { action.getId() });
			addNotify(expr, new CONST(Integer.toString(dates.size()), null), false);	
		}
	}
	
	private void addNotify(FunctionalExpr expr, CONST value, boolean execute)
	{
		events.add(new FunctionValue(expr, value));
		if (execute)
		{	
			while (!events.isEmpty())
			{
				FunctionValue fv = events.poll();
				notifyState(fv.getExpresion(), fv.getValue(), true);
			}
		}
	}
	
	// Trigger event in our state machine...
	public void addNotify(FunctionalExpr expr, CONST value)
	{
		addNotify(expr, value, true);
	}
	
	// Just check if value violates SLA (it is need for evaluation purposes)
	public List<Guaranteed.Action> checkState(FunctionalExpr expr, CONST value)
	{
		return notifyState(expr, value, false);
	}
	
	public Map<FunctionalExpr, CONST> getConstraints()
	{
		Map<FunctionalExpr, CONST> result = new HashMap<FunctionalExpr, CONST>();
		for (Environment env: this.environments)
		{
			for (FunctionValue constraint: env.getConstraints())
			{
				result.put(constraint.expr, constraint.value);
			}
		}
		// maybe there will be need to clean duplicates but not at this moment	
		return result;
	}
}

class Environment 
{	
	private Map<ID, VariableDeclr> vars;
	private Map<ID, Guaranteed> guarantees;
	private Map<ID, TypeConstraintExpr> constraints;
	private AgreementTerm term;
	public Map<TypeConstraintExpr, Set<Guaranteed.Action>> actions;
	
	public Environment(AgreementTerm term)
	{
		this.term = term;
		
		//variables
		vars = new HashMap<ID, VariableDeclr>();
		VariableDeclr[] items = term.getVariableDeclrs();
		if (items != null)
		{
			for (VariableDeclr item: items)
			{
				vars.put(item.getVar(), item);
			}
		}
		
		// guarantees
		guarantees = new HashMap<ID, Guaranteed>();
		for (Guaranteed item: term.getGuarantees())
		{
			guarantees.put(item.getId(), item);
		}
		
		// constraints
		constraints = new HashMap<ID, TypeConstraintExpr>();
		for (Guaranteed item: term.getGuarantees())
		{
			// TODO what if constraint is not instance of TCE -> (it is DCE).
			if (item instanceof Guaranteed.State)
			{
				Guaranteed.State state = (Guaranteed.State) item;
				constraints.put(state.getId(), (TypeConstraintExpr)state.getState());
			}
		}
		
		// actions
		actions = new HashMap<TypeConstraintExpr, Set<Guaranteed.Action>>();
		for (Guaranteed item: term.getGuarantees())
		{			
			if (item instanceof Guaranteed.Action)
			{
				Guaranteed.Action action = (Guaranteed.Action) item;
				EventExpr event = action.getPrecondition();
				
				TypeConstraintExpr tce = getTypeConstraintExpr(event);
				if (tce != null)
				{
					Set<Guaranteed.Action> actionList = actions.get(tce);
					if (actionList == null)
					{
						actionList = new HashSet<Guaranteed.Action>();
					}
					actionList.add(action);
					actions.put(tce, actionList);
				}
			}
		}
	}

	public List<FunctionValue> getConstraints()
	{
		List<FunctionValue> result = new ArrayList<FunctionValue>();
		for (TypeConstraintExpr tce : constraints.values())
		{
			result.add(extractExpr(tce));	
		}
		return result;
	}
	
	public AgreementTerm getTerm()
	{
		return this.term;
	}
	
	public FunctionalExpr buildFunctionalExpr(STND op, ID id)
	{
		return new FunctionalExpr(op, new ValueExpr[] { id });
	}
	
	public boolean evaluate(TypeConstraintExpr expr, Map<FunctionalExpr, CONST> values)
	{
		DomainExpr domainExpr = expr.getDomain();
		ValueExpr valueExpr = expr.getValue();
		//STND op = getOperator(expr);
		
		if (valueExpr instanceof FunctionalExpr)
		{
			CONST e1 = getValue((FunctionalExpr) valueExpr, values);
			//there could be no violation if we do not check for this operation
			if (e1 == null)
			{
				throw new RuntimeException(String.format("Can not get variables to evaluate: %s", expr.toString()));
			}
			return evaluate(domainExpr, e1);
		}				
		return false;
	}
	
	
	private CONST getValue(ID id)
	{
		VariableDeclr var = vars.get(id);
		if (var != null)
		{
			if (var instanceof Customisable)
			{
				return ((Customisable)var).getValue();
			}
			else
			{
				Expr v2 = var.getExpr();
				return (CONST)v2;
			}
		}
		return null;
	}
	
	private CONST getValue(ValueExpr e)
	{
		if (e instanceof ID)
		{
			return getValue((ID) e);
		}
		else if (e instanceof CONST)
		{
			return (CONST)e;
		}
		else if (e instanceof Customisable)
		{
			return ((Customisable)e).getValue();
		}
		
		return null;
	}
	
	private FunctionValue extractExpr(TypeConstraintExpr expr)
	{
		SimpleDomainExpr domain = (SimpleDomainExpr) expr.getDomain();
		FunctionalExpr func = (FunctionalExpr)expr.getValue();

		return new FunctionValue(func, getValue(domain.getValue()));
	}
	
	public boolean evaluate(DomainExpr expr, CONST value)
	{
		if (expr instanceof SimpleDomainExpr)
		{
			SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) expr;
			ValueExpr e2 = simpleDomainExpr.getValue();	
			if (e2 instanceof ID)
			{
				CONST v2 = getValue((ID)e2);
				return evaluate(value, v2, simpleDomainExpr.getComparisonOp());
				/*VariableDeclr var = vars.get(e2);
				if (var != null)
				{
					if (var instanceof Customisable)
					{
						return evaluate(value, ((Customisable)var).getValue(), simpleDomainExpr.getComparisonOp());
					}
					else
					{
						Expr v2 = var.getExpr();
						if (v2 instanceof CONST)
						{
							return evaluate(value, (CONST)v2, simpleDomainExpr.getComparisonOp());
						}
					}
				}*/
				
			}
			else if (e2 instanceof CONST)
			{
				return evaluate(value, (CONST)e2, simpleDomainExpr.getComparisonOp());
			}
			else if (e2 instanceof Customisable)
			{
				return evaluate(value, ((Customisable) e2).getValue(), simpleDomainExpr.getComparisonOp());
			}
		}		
		return false;
	}
	
	public boolean evaluate(CONST e1, CONST e2, STND op)
	{
		if (e1 == null || e2 == null)
		{
			return false;
		}
		
		if (e1.getDatatype() != null)
		{
			if (! e1.getDatatype().equals(e2.getDatatype()))
			{
				return false;
			}
		}
		else if (e2.getDatatype() != null)
		{
			if (!e2.getDatatype().equals(e1.getDatatype()))
			{
				return false;
			}
		}
		
		float v1 = Float.parseFloat(e1.getValue());
		float v2 = Float.parseFloat(e2.getValue());
		
		if (core.identical_to.equals(op))
		{
			return e1 == e2;
		}
		else if (core.equals.equals(op))
		{
			return e1.equals(e2) ;
		}
		else if (core.not_equals.equals(op))
		{
			return v1 != v2;
		}
		else if (core.less_than.equals(op))
		{
			return v1 < v2;
		}
		else if (core.less_than_or_equals.equals(op))
		{
			return v1 <= v2;
		}
		else if (core.greater_than.equals(op))
		{
			return v1 > v2;
		}
		else if (core.greater_than_or_equals.equals(op))
		{
			return v1 >= v2;
		}
		return false;
	}
	
	private CONST getValue(FunctionalExpr expr, Map<FunctionalExpr, CONST> values)
	{
		// they should have same operator
		STND op = expr.getOperator();
		Set<FunctionalExpr> responses = values.keySet();
		
		for (FunctionalExpr item: responses)
		{
			if (item.getOperator().equals(op))
			{
				ID id1 = getID(expr);
				ID id2 = getID(item);
				
				if (id1 != null && id1.equals(id2))
				{
					return values.get(item);
				}
			}
		}
		return null;
	}
	
	private ID getID(FunctionalExpr expr)
	{
		if (expr == null)
			return null;
		
		ValueExpr[] parameters = expr.getParameters();
		for (ValueExpr item: parameters)
		{
			if (item instanceof ID)
			{
				return (ID) item;
			}
		}		
		return null;
	}
	
	private TypeConstraintExpr getTypeConstraintExpr(EventExpr eventExpr)
	{
		Expr[] exprs = eventExpr.getParameters();
		if (exprs == null)
			return null;
		
		for (Expr expr: exprs)
		{
			if (expr instanceof TypeConstraintExpr)
			{
				return (TypeConstraintExpr) expr;
			}
			else if (expr instanceof ID)
			{
				return constraints.get(expr);
			}
		}
		return null;
	}
	
	/*private STND getOperator(TypeConstraintExpr expr)
	{
		ValueExpr value = expr.getValue();
		if (value instanceof FunctionalExpr)
		{
			FunctionalExpr func = (FunctionalExpr) value;
			return func.getOperator();
		}	
		return null;
	}*/
	
	public FunctionalExpr getVariableForConstraint(TypeConstraintExpr expr)
	{
		ValueExpr value = expr.getValue();
		if (value instanceof FunctionalExpr)
		{
			FunctionalExpr func = (FunctionalExpr) value;
			return func;
		}	
		return null;
	}
	
	public boolean isSame(FunctionalExpr e1, FunctionalExpr e2)
	{
		if (e1.getOperator() == null || e2.getOperator() == null)
		{
			return false;
		}
		else if (e1.getOperator().equals(e2.getOperator()))
		{
			ID id1 = getID(e1);
			ID id2 = getID(e2);
			
			if (id1 != null)
			{
				return id1.equals(id2);
			}
		}
		return false;
	}
	
	public CONST getPrice(Guaranteed.Action action)
	{
		Defn defn = action.getPostcondition();
		if (defn instanceof Penalty)
		{
			Penalty penalty = (Penalty) defn;
			return penalty.getPrice();
		}
		return null;
	}
}

