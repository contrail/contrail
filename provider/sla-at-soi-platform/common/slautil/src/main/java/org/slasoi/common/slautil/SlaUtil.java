package org.slasoi.common.slautil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.business.TerminationClause;

/**
 * Hello world!
 *
 */

public class SlaUtil 
{	
	public static CONST getValue(AgreementTerm term, Guaranteed.State state)
	{
		ID id = getID(state);
		VariableDeclr var = term.getVariableDeclr(id.getValue());
		if (var instanceof Customisable)
		{
			return ((Customisable)var).getValue();
		}
		return null;
	}
	
	public static void setValue(AgreementTerm term, Guaranteed.State state, CONST value)
	{
		ID id = getID(state);
		VariableDeclr var = term.getVariableDeclr(id.getValue());
		if (var instanceof Customisable)
		{
			Customisable cvar = (Customisable) var;
			cvar.setValue(value);
		}
	}
	
	public static void setPrice(SLATemplate template, STND priceType,  int value)
	{
		if (template == null)
		{
			return;
		}
		
		for (AgreementTerm term: template.getAgreementTerms())
		{
			for (Guaranteed guaranteed: term.getGuarantees())
			{
				if (guaranteed instanceof Guaranteed.Action)
				{
					Guaranteed.Action action = (Guaranteed.Action) guaranteed;
					Defn defn = action.getPostcondition();
					if (defn instanceof ProductOfferingPrice)
					{
						ProductOfferingPrice offer = (ProductOfferingPrice) defn;
						ComponentProductOfferingPrice[] prices = offer.getComponentProductOfferingPrices();
						
						for (ComponentProductOfferingPrice price: prices)
						{
							if (price.getPriceType().equals(priceType))
							{
								CONST priceValue = price.getPrice();
								priceValue.setValue(Integer.toString(value));
							}
						}
						return;
					}
				}
			}
		}
	}
	
	public static boolean isTerminationAction(Guaranteed.Action action)
	{
		return (action.getPostcondition() instanceof TerminationClause);
	}
	
	public static CONST buildValue(String value, String unit)
	{
		return new CONST(value, new STND(unit));
	}
	
	public static SLATemplate cloneSLATemplate(SLATemplate template)
	{
		SLATemplate result = new SLATemplate();
		
		try 
		{
			ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
			ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
			os.writeObject(template);
			ByteArrayInputStream bytesInput = new ByteArrayInputStream(bytesOutput.toByteArray());
			ObjectInputStream is = new ObjectInputStream(bytesInput);
			result = (SLATemplate)is.readObject();
			
			is.close();
			os.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	
	}
	
	private static ID getID(Guaranteed.State state)
	{
		ConstraintExpr expr = state.getState();
		if (expr instanceof TypeConstraintExpr)
		{
			TypeConstraintExpr typeExpr = (TypeConstraintExpr) expr;
			DomainExpr domainExpr = typeExpr.getDomain();
			
			if (domainExpr instanceof SimpleDomainExpr)
			{
				SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) domainExpr;
				ValueExpr value = simpleDomainExpr.getValue();
			
				if (value instanceof ID)
				{
					return (ID)value;
				}
			}		
		}	
		return null;
	}
}
