
Infrastructure SLA Templates for Unit tests
-------------------------------------------

This directory contains infrastructure SLA templates for the unit tests of the SoftwareServiceEvaluation component.

They are originally taken from the ORC repository at 
https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/orc-data/I_SLATs
They were copied to not break the unit tests every time a commit has been done on this directory.