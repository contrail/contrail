/*
SQLyog Community v8.32 
MySQL - 5.1.45-community : Database - LLMS_DB
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`LLMS_DB` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `DB_LLMS`;

/*Table structure for table `metric` */

DROP TABLE IF EXISTS `metric`;

CREATE TABLE `metric` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `metricURL` text,
  `name` tinytext,
  `unit` tinytext,
  `metricClass` tinytext,
  `dataType` tinytext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `metricURL` (`metricURL`(512))
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=latin1;

/*Table structure for table `metricvalue` */

DROP TABLE IF EXISTS `metricvalue`;

CREATE TABLE `metricvalue` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `metricID` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `value` text,
  `count` int(11) DEFAULT '1',
  `timeLast` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `metricID` (`metricID`),
  KEY `time` (`time`),
  KEY `metricID_time` (`metricID`,`time`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
