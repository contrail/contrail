package protocol;

#import classes here.

import org.slasoi.gslam.protocolengine.impl.State;
import org.slasoi.gslam.protocolengine.impl.StateName;
import org.slasoi.gslam.protocolengine.impl.StateStatus;
import org.slasoi.gslam.protocolengine.impl.Event;
import org.slasoi.gslam.protocolengine.impl.EventName;
import org.slasoi.slamodel.sla.SLATemplate;

import java.util.List;

#Globals come here:


rule "BootstrapStateMachine_Rule"
    when
    	//An empty would also run the then part.
    	event : Event(eventName == EventName.StartNegotiationEvent);
    then
    	insert(new State(StateName.START));
    	event.setProcessedSuccessfully(true);
    	retract(event);
        System.out.println("BootstrapStateMachine_Rule fired" );
end

rule "CreateState_Rule"
    when
    	startState : State(name == StateName.START, status == StateStatus.NOT_STARTED);
    then
        System.out.println("CreateState_Rule fired" );
        System.out.println("-State Name :" +startState.getName()+ " has Status :" +startState.getStatus());
        startState.setStatus( StateStatus.RUNNING );
        update(startState);
        System.out.println("-State changed its status to :" +startState.getStatus());
end

rule "StartState_OnEntry_Rule" 
	salience 3
	when 
		startState : State(name == StateName.START, status == StateStatus.RUNNING);		
	then 
		startState.onEntry();
        System.out.println("StartState_OnEntry_Rule fired");
end 

rule "Start_To_Negotiate_Transition_Rule" 
	salience 2
	when 
		startState : State(name == StateName.START, status == StateStatus.RUNNING);
	then 
		startState.setStatus(StateStatus.READY_TO_TRANSIT); //we dont need to modify this fact due to PropertyChangeListener.
		System.out.println("State Name :" +startState.getName()+ " has Status :" +startState.getStatus());		
		insert(new State(StateName.NEGOTIATE)); //instantiating new State, without any hardcoding inside Platform components!
        System.out.println("Start_To_Negotiate_Transition_Rule fired");
end 

rule "NegotiateState_CancelNegotiation_Rule" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, eval(status == StateStatus.NOT_STARTED || status == StateStatus.RUNNING || status == StateStatus.READY_TO_TRANSIT));
		event0 : Event(eventName == EventName.CancelNegotiationEvent);
	then 
		negotiateState.setStatus(StateStatus.STOPPED);
		negotiateState.onExit();
		negotiateState.setCurrentHop(1);
		event0.setProcessedSuccessfully(true);
		retract(event0);
		update(negotiateState);
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());		
        System.out.println("NegotiateState_CancelNegotiation_Rule fired.");
end 

rule "NegotiateState_SendProposal_Rule" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.NOT_STARTED);
		event0 : Event(eventName == EventName.SendProposalEvent);//eval(offer.getParties() != null)
	then 
		negotiateState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.
		negotiateState.onEntry();
		negotiateState.setNumberOfHopsAllowed(3);
		negotiateState.setCurrentHop(1);
		event0.setProcessedSuccessfully(true);
		//System.out.println("offer.getParties().length = "+event0.getOffer().getParties().length);
		retract(event0);
		update(negotiateState);
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());		
        System.out.println("NegotiateState_SendProposal_Rule fired..");
end 

rule "NegotiateState_ProposalArrived_Rule" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.NOT_STARTED);
		event01 : Event(eventName == EventName.ProposalArrivedEvent);
	then 
		negotiateState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.
		negotiateState.onEntry();
		negotiateState.setNumberOfHopsAllowed(3);
		negotiateState.setCurrentHop(1);
		event01.setProcessedSuccessfully(true);
		retract(event01);
		update(negotiateState);
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());
        System.out.println("NegotiateState_ProposalArrived_Rule fired");
end

rule "NegotiateState_Evaluate_SendProposalEvent_Rule" 
	when
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop < numberOfHopsAllowed );
		event12 : Event(eventName == EventName.SendProposalEvent);
	then 
		negotiateState.setCurrentHop(negotiateState.getCurrentHop()+1);
		update(negotiateState);//This update is essential coz without it, the Working Memory wont execute last statement.
		event12.setProcessedSuccessfully(true);
		retract(event12);//Remove event as soon as it is processed.
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus()+" HopCount :"+negotiateState.getCurrentHop());		
        System.out.println("NegotiateState_Evaluate_SendProposalEvent_Rule fired");
end

rule "NegotiateState_Evaluate_ProposalArrivedEvent_Rule" 
	when
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop < numberOfHopsAllowed );
		event1 : Event(eventName == EventName.ProposalArrivedEvent);
	then 
		negotiateState.setCurrentHop(negotiateState.getCurrentHop()+1);
		update(negotiateState);//This update is essential coz without it, the Working Memory wont execute last statement.
		event1.setProcessedSuccessfully(true);
		retract(event1);//Remove event as soon as it is processed.
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus()+" HopCount :"+negotiateState.getCurrentHop());		
        System.out.println("NegotiateState_Evaluate_ProposalArrivedEvent_Rule fired");
end 

rule "NegotiateState_Evaluate_Event_Rule2" 
	when		
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop >= numberOfHopsAllowed );
	then 
		negotiateState.setStatus(StateStatus.READY_TO_TRANSIT);
		update(negotiateState);
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus()+" HopCount :"+negotiateState.getCurrentHop());		
        System.out.println("NegotiateState_Evaluate_Event_Rule2 fired");
end 

rule "Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );
		event : Event(eventName == EventName.RequestAgreementEvent);//, eval(offer != null)
	then 
		negotiateState.onExit();
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());
		event.setProcessedSuccessfully(true);
		insert(new State(StateName.DECIDE));
		//System.out.println("event.offer.getParties().length = "+event.getOffer().getParties().length);
		retract(event);
        System.out.println("Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule fired");
end 

rule "Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule2" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, eval(status == StateStatus.RUNNING && currentHop < numberOfHopsAllowed));
		event : Event(eventName == EventName.RequestAgreementEvent);
	then 
		negotiateState.onExit();
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());
		event.setProcessedSuccessfully(true);
		insert(new State(StateName.DECIDE));
		//System.out.println("event.offer.getParties().length = "+event.getOffer().getParties().length);
		retract(event);
        System.out.println("Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule2 fired");
end 

rule "Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );
		event : Event(eventName == EventName.AgreementRequestedEvent);
	then 
		negotiateState.onExit();
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());
		event.setProcessedSuccessfully(true);
		insert(new State(StateName.DECIDE));
		retract(event);
        System.out.println("Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule fired");
end 

rule "Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule2" 
	when 
		negotiateState : State(name == StateName.NEGOTIATE, eval(status == StateStatus.RUNNING && currentHop < numberOfHopsAllowed) );
		event : Event(eventName == EventName.AgreementRequestedEvent);
	then 
		negotiateState.onExit();
		System.out.println("State Name :" +negotiateState.getName()+ " has Status :" +negotiateState.getStatus());
		event.setProcessedSuccessfully(true);
		insert(new State(StateName.DECIDE));
		retract(event);
        System.out.println("Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule2 fired");
end

rule "DecideState_OnEntry_Rule" 
	when 
		decideState : State(name == StateName.DECIDE, status == StateStatus.NOT_STARTED);
	then 
		decideState.setStatus(StateStatus.RUNNING);
		decideState.onEntry();
		update(decideState);
        System.out.println("DecideState_OnEntry_Rule fired" + decideState.getStatus());
end