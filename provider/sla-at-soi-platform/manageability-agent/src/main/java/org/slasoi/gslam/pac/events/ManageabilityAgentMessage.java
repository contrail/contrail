package org.slasoi.gslam.pac.events;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class ManageabilityAgentMessage {

    private static final Logger logger = Logger.getLogger(ManageabilityAgentMessage.class.getName());

    /**
     * Date information
     */
    private long timestamp;

    /**
     * Event type
     */
    private String eventType = null;

    /**
     * The message parameters.
     */
    private HashMap<String, String> messageParameters = new HashMap<String, String>();

    /**
     * Default constructor, needed for RTTI
     */
    public ManageabilityAgentMessage() {

    }

    /**
     * Set the message parameters.
     * 
     * @param messageParameters
     *            the messageParameters to set
     */
    public final void setMessageParameters(final HashMap<String, String> messageParameters) {
        this.messageParameters = messageParameters;
    }

    /**
     * Get the message parameters.
     * 
     * @return the messageParameters
     */
    public final HashMap<String, String> getMessageParameters() {
        return messageParameters;
    }

    /**
     * To return the value of the parameter. Return blank (empty string) if not found.
     * 
     * @param name
     *            the parameter name.
     * @return the value of the parameter with the given name
     */

    public final String getValue(final String name) {
        if (this.messageParameters.containsKey(name)) {
            return this.messageParameters.get(name);
        }

        return "";
    }

    /**
     * Set the value of a parameter. If a parameter already exist, overwrite it.
     * 
     * @param name
     *            The name of the parameter
     * @param value
     *            The value of the parameter
     */
    public final void setValue(final String name, final String value) {
        if (this.messageParameters.containsKey(name)) {
            this.messageParameters.remove(name);
        }

        this.messageParameters.put(name, value);
    }

    /**
     * Print the message parameters in string.
     * 
     * @return The message parameters in string.
     */
    public final String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("\nTimestamp: " + timestamp + "\n");
        sb.append("Event type: " + eventType + "\n");
        sb.append("Message Parameters: ");
        for (String name : this.messageParameters.keySet()) {
            sb.append("\r\n\tParameter " + name + " = " + this.messageParameters.get(name));
        }
        return sb.toString();
    }

    public String toXML() {
        logger.debug("ManageabilityAgentMessage to XML");
        logger.debug(this);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class.getSimpleName(),
                org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class);
        String messageStr = xstream.toXML(this);
        logger.debug(messageStr);

        return (messageStr);
    }

    public static ManageabilityAgentMessage fromXML(String messageStr) {
        logger.debug("ManageabilityAgentMessage from XML");
        logger.debug(messageStr);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class.getSimpleName(),
                org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class);

        ManageabilityAgentMessage message = new ManageabilityAgentMessage();

        try {
            message = (ManageabilityAgentMessage) xstream.fromXML(messageStr);
        }

        catch (Exception e) {
            return null;
        }

        return (message);
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

}
