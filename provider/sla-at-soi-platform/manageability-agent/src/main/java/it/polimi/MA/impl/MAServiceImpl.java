package it.polimi.MA.impl;

import it.polimi.MA.impl.doe.DOEManageabilityAgentFacade;
import it.polimi.MA.service.IManageabilityAgentFacade;
import it.polimi.MA.service.MAService;
import it.polimi.MA.service.exceptions.ServiceStartupException;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.slamodel.sla.Endpoint;


/**
 * @author sam
 *
 */
public class MAServiceImpl implements MAService {
	
	private HashMap<String, IManageabilityAgentFacade> facades = new HashMap<String, IManageabilityAgentFacade>();
	
    public MAServiceImpl(){
    	System.out.println("[MA] Constructing MAServiceImpl");
    }
    
    // IProvision methods follow
    
	public List<Endpoint> getEndpoints(ServiceBuilder builder) {
		// TODO Auto-generated method stub
		
		List<Endpoint> list = new ArrayList<Endpoint>();
		
		//Gather endpoints for the service builder param
		
		return list;
	}

	
	/* (non-Javadoc)
	 * @see it.polimi.MA.service.IProvisioning#getManagibilityAgent(org.slasoi.models.scm.ServiceBuilder)
	 */
	public IManageabilityAgentFacade getManagibilityAgentFacade(ServiceBuilder builder) {
		// TODO Auto-generated method stub
		
		String uuid = null;
		if (builder != null) {
			uuid = builder.getUuid();
			System.out.println("[MA - Facade] got uuid from builder");
		}
		else {
			uuid = UUID.randomUUID().toString();
		}
		
		IManageabilityAgentFacade returnFacade = null;
		
		if (facades.containsKey(uuid)) {
			returnFacade =  facades.get(uuid);
			System.out.println("[MA - Facade] got returnFacade from map");
		}

		return returnFacade;
	}

	public void startServiceInstance(ServiceBuilder builder,
			Settings connectionSettings, String notificationChannel)
			throws ServiceStartupException, MessagingException {
		
		System.out.println("[MA] notificationChannel = "  + notificationChannel);
		
		/*
		 * Perform deployment/initial provisioning
		 * ORC does not require any initial provisioning?
		 */
		
		/*
		 * Prepare facade for the new Service
		 */
		
		String uuid = null;
		if (builder != null) {
			uuid = builder.getUuid();
			System.out.println("[MA] Got uuid from builder");
		}
		else {
			uuid = UUID.randomUUID().toString();
		}
		
		System.out.println("[MA] builder uuid: " + uuid);
		
		//Must discover type of facade needed - currently we only support DOEFacades
		IManageabilityAgentFacade serviceFacade = null;
		try {
			serviceFacade = new DOEManageabilityAgentFacade(builder);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.facades.put(uuid, serviceFacade);

		//Communicate notification via event bus
		
		ManageabilityAgentMessage notificationMessage = createMessage(uuid);		
		String output = notificationMessage.toXML();	
		
		System.out.println("[MA] NOTIFICATION: " + output);
	
		
		//Settings settings = this.getPubSubSettings();
		
    	PubSubManager manager = null;
		try {
			manager = PubSubFactory.createPubSubManager(connectionSettings);
			System.out.println("[MA] Manager creation success");
		} catch (MessagingException e) {
			System.out.println("[MA] Manager creation failure");
			e.printStackTrace();
		};
		
		boolean isChannel = false;
		try {
			isChannel = manager.isChannel(notificationChannel);
			System.out.println("[MA] isChannel success");
		}
		catch (MessagingException e) {
			System.out.println("[MA] isChannel failure");
			e.printStackTrace();
		}
		
		if (isChannel == false) {
			try {
				manager.createChannel(new Channel(notificationChannel));
				System.out.println("[MA] Channel creation success");
			}
			catch (MessagingException e) {
				System.out.println("[MA] Channel creation failure");
				e.printStackTrace();
			}
		}
		
		try {
			PubSubMessage pubSubMessage = new PubSubMessage(notificationChannel, output);		
			manager.publish(pubSubMessage);
			System.out.println("[MA] Message publication success");
		} catch (MessagingException e) {
			System.out.println("[MA] Message publication failure");
			e.printStackTrace();
		};
		
		//manager.deleteChannel(notificationChannel);
		
		//manager.close();
		
		
	}

	public void stopServiceInstance(ServiceBuilder builder) {
		// TODO Auto-generated method stub
		
	}
	
	//support methods
	
	private ManageabilityAgentMessage createMessage(String provisionID) {
        ManageabilityAgentMessage message = new ManageabilityAgentMessage();
        message.setTimestamp(System.currentTimeMillis());
        message.setEventType("SoftwareProvisionEvent");
        message.setValue("ProvisionID", provisionID);
        message.setValue("ProvisioningStatus", "READY");
        return message; 
    }
	

	
	/*private Settings getPubSubSettings() {
		Settings settings = new Settings();
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_username, "primitive-ecf");
		settings.setSetting(Setting.xmpp_password, "primitive-ecf");
		settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_port, "5222");
		settings.setSetting(Setting.messaging, "xmpp");
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_resource, "test");
		settings.setSetting(Setting.xmpp_pubsubservice, "pubsub.testbed.sla-at-soi.eu");
		
		return settings;
	} */
	
	

	



}