//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.09.21 at 06:37:51 PM CEST 
//


package it.polimi.MA.impl.doe.pdd;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for partnerRoleEndpointReferenceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="partnerRoleEndpointReferenceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="static"/>
 *     &lt;enumeration value="dynamic"/>
 *     &lt;enumeration value="invoker"/>
 *     &lt;enumeration value="principal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum PartnerRoleEndpointReferenceType {

    @XmlEnumValue("dynamic")
    DYNAMIC("dynamic"),
    @XmlEnumValue("invoker")
    INVOKER("invoker"),
    @XmlEnumValue("principal")
    PRINCIPAL("principal"),
    @XmlEnumValue("static")
    STATIC("static");
    private final String value;

    PartnerRoleEndpointReferenceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartnerRoleEndpointReferenceType fromValue(String v) {
        for (PartnerRoleEndpointReferenceType c: PartnerRoleEndpointReferenceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
