package it.polimi.MA.service;

import org.slasoi.models.scm.ServiceBuilder;

/**
 * Action to be executed by the ManageabilityAgent. For example, the action can
 * be described by the operation's name and its parameters.
 * 
 */
public interface IEffectorAction {
	
	/**
	 * Types of action
	 */
    public static enum Types{
		UPDATE_BINDIG,
	}
	
	public Types getActionType();
	public ServiceBuilder getServiceBuilder();

}
