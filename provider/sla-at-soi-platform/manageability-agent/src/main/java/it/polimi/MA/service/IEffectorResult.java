package it.polimi.MA.service;

/**
 * Result of an action executed by the ManageabilityAgent. 
 *
 */
public class IEffectorResult {
	
	private boolean result;

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
