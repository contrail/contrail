package it.polimi.MA.impl;

import it.polimi.MA.service.IEffectorAction;
import it.polimi.MA.service.IEffectorResult;
import it.polimi.MA.service.IManageabilityAgentFacade;
import it.polimi.MA.service.SensorSubscriptionData;

import java.util.List;

import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;

public abstract class ManageabilityAgentFacade implements
		IManageabilityAgentFacade {

	public void configureMonitoringSystem(
			MonitoringSystemConfiguration configuration) {
		// TODO Auto-generated method stub
	}

	public void deconfigureMonitoring() {
		// TODO Auto-generated method stub

	}

	public IEffectorResult executeAction(IEffectorAction action) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public ServiceInstance getInstance() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<SensorSubscriptionData> getSensorSubscriptionData() {
		// TODO Auto-generated method stub
		return null;
	}

}
