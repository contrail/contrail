package it.polimi.MA.service;

import static org.junit.Assert.assertTrue;
import it.polimi.MA.impl.MAServiceImpl;
import it.polimi.MA.impl.doe.UpdateBinding;
import it.polimi.MA.service.exceptions.ServiceStartupException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;

public class DOETests {
	
	private static ServiceBuilder builder = null;
	private static String uuid = null;
	private static Settings settings = null;
	
	@BeforeClass
	public static void init(){
		//Initialize the builder...
		uuid = "testUUID";
		builder = new ServiceBuilderExtended();
		builder.setUuid(uuid);
		//Initialize settings...
		settings = new Settings();
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_username, "primitive-ecf");
		settings.setSetting(Setting.xmpp_password, "primitive-ecf");
		settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_port, "5222");
		settings.setSetting(Setting.messaging, "xmpp");
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_resource, "test");
		settings.setSetting(Setting.xmpp_pubsubservice, "pubsub.testbed.sla-at-soi.eu");
	}
	
	@Test
	public void executeAction_update_bindings(){
		MAService service = new MAServiceImpl();
		Settings connectionSettings = settings;
		String notificationChannel = "test-DOE-Paolo";	
		
		try {
			service.startServiceInstance(builder, connectionSettings, notificationChannel);
		} catch (ServiceStartupException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		//The action to execute should come from the SSM...
		//I assume the SSM has verified a bindings update is needed...
		IEffectorAction action = new UpdateBinding(builder);
		//The SSM get the manageability agent facade specific for the builder...(a DOEManageabilityAgentFacade in our case)
		IManageabilityAgentFacade facade = service.getManagibilityAgentFacade(builder);

		IEffectorResult effectorResult = null;
		try {
			effectorResult = facade.executeAction(action);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(effectorResult.isResult());
		
	}

}
