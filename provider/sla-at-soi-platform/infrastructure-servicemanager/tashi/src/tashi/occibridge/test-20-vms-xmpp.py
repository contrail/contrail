import xmpp, json

live = True
username = 'tashi-scheduler@slasoi-testbed.xlab.si'
passwd = 'tashi-scheduler'
to='tashi@slasoi-testbed.xlab.si'


client = xmpp.Client('slasoi-testbed.xlab.si')
client.connect(server=('slasoi-testbed.xlab.si',5222))
client.auth(username, passwd, 'slasoi-testbed.xlab.si')
client.sendInitPresence()

for i in range(0, 1):
	#msg = 'createcompute [{"class":"eu.slasoi.infrastructure.model.infrastructure.Compute","id":1,"cpu_speed":1,"boot_vol_type":null,"status":{"class":"Status","id":2},"provisionRequest":{"class":"ProvisionRequest","id":1},"imageIdentifier":"tashi.img","userId":1000, "groupName":"testServiceName", "downspeed":500, "upspeed":200, "diskBandwidthWeight": 500, "auditability": 1, "location":"si", "sas70":0, "ccr":0, "dataClass":1, "hwLevel":1, "diskThroughput": 1, "netThroughput":1, "dataEncryption":0, "isolation":0, "dataRetention":7, "deleteMethod":1, "powerCapping":0, "snapshotBackup":0, "snapshotRetention":0, "memory_size":512,"hostname":"tizzy'+str(i)+'","provisioningSystemId":null,"links":null,"type":"occi_compute","memory_reliability":null,"memory_speed":2048,"resourceId":"a626bbb4-6c92-4f0e-a117-ae11ec502a6e'+str(i)+'","cpu_cores":1,"categories":[{"class":"Category","id":9},{"class":"Category","id":8},{"class":"Category","id":1},{"class":"Category","id":11},{"class":"Category","id":13}],"cpu_arch":null},[{"class":"eu.slasoi.infrastructure.model.Category","id":null,"scheme":"http://purl.org/occi/kind#","term":"compute","label":"Compute Resource"},{"class":"eu.slasoi.infrastructure.model.Category","id":13,"scheme":"http://provider.com/category#templates","term":"ubuntu-9.10","label":"Ubuntu Linux 9.10"},{"class":"eu.slasoi.infrastructure.model.Category","id":null,"scheme":"http://provider.com/category#sla","term":"gold","label":"SLA@SOI Gold SLA"},{"class":"eu.slasoi.infrastructure.model.Category","id":null,"scheme":"http://provider.com/category#locations","term":"ie","label":"Ireland"}],[]]'
	#msg = 'createmanycompute [{"class":"eu.slasoi.infrastructure.model.infrastructure.Compute","id":1,"cpu_speed":1,"boot_vol_type":null,"status":{"class":"Status","id":2},"provisionRequest":{"class":"ProvisionRequest","id":1},"imageIdentifier":"orc.img","userId":1000, "groupName":"testServiceName", "downspeed":500, "upspeed":200, "diskBandwidthWeight": 500, "auditability": 1, "location":"si", "sas70":0, "ccr":0, "dataClass":1, "hwLevel":1, "diskThroughput": 1, "netThroughput":1, "dataEncryption":0, "isolation":0, "dataRetention":7, "deleteMethod":1, "powerCapping":0, "snapshotBackup":0, "snapshotRetention":0, "memory_size":512,"hostname":"tizzy'+str(i)+'","provisioningSystemId":null,"links":null,"type":"occi_compute","memory_reliability":null,"memory_speed":2048,"resourceId":"a626bbb4-6c92-4f0e-a117-ae11ec502a6e'+str(i)+'","cpu_cores":1,"categories":[]}]'
	#msg = 'deletecompute [{"fqdn":"tizzy0.xlab.tst"}]'
	#msg = 'restartcompute [{"fqdn":"vm2.xlab.tst"}]'
	#msg = 'adaptcpucompute [{"fqdn":"vm1.xlab.tst", "share":2000}]'
	#msg = 'adaptmemorycompute [{"fqdn":"vm1.xlab.tst", "limit":256}]'
	#msg = 'migratecompute [{"vm_fqdn":"vm1.xlab.tst", "host_fqdn":"muca01.xlab.tst"}]'
	msg = 'deletemanycompute [{"fqdn":"test1.xlab.tst"}, {"fqdn":"test2.xlab.tst"}]'
	#print msg
	if(live):
		message = xmpp.Message(to, msg)
		message.setAttr('type', 'chat')
		print "Sending message"
		client.send(message)

client.disconnect()