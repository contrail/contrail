#!/usr/bin/env python

# Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Intel Performance Learning Solutions Ltd. nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Andrew Edmonds - andrewx.edmonds@intel.com, Miha Stopar - miha.stopar@xlab.si
# @version        $Rev: 3038 $
# @lastrevision   $Date: 2011-08-30 14:07:20 +0200 (tor, 30 avg 2011) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/occibridge/tashi_commands.py $

import sys
import os
import subprocess
from multiprocessing import Process
import random
import time
import json
import traceback
import glob
import socket
from xml.dom.minidom import Document, parse, Node

from tashi.rpycservices.rpyctypes import *
from tashi import createClient, getConfig, createSchedulerClient
from tashi.resources import *
from tashi.util import instantiateImplementation, getFqdn
import tashi.messaging.xmppsensor
from tashi.messaging.xmppsensor import XmppSensor
from tashi.occibridge.utils import *
from tashi.scheduler.schedulerconf import SchedulerConf

all_commands={}

class BaseCommand(object):
        """Base class for command processors."""

        def __get_extended_help(self):
                if self.__extended_help:
                        return self.__extended_help
                else:
                        return self.help

        def __set_extended_help(self, v):
                self.__extended_help=v

        extended_help=property(__get_extended_help, __set_extended_help)

        def __init__(self, name, help=None, extended_help=None):
                self.name=name
                self.help=help
                self.extended_help=extended_help

        def __call__(self, user, prot, args, session):
                raise NotImplementedError()

class HelpCommand(BaseCommand):

        def __init__(self):
                super(HelpCommand, self).__init__('help', 'You need help.')

        def __call__(self, user, prot, args):

                rv=[]
                if args:
                        c=all_commands.get(args.strip().lower(), None)
                        if c:
                                rv.append("Help for %s:\n" % c.name)
                                rv.append(c.extended_help)
                        else:
                                rv.append("Unknown command %s." % args)
                else:
                        for k in sorted(all_commands.keys()):
                                rv.append('%s\t%s' % (k, all_commands[k].help))
                rv.append("\nFor more help, ask Andy!")
                prot.send_result(content="\n".join(rv), endpoint=user)

class CreateComputeCommand(BaseCommand):

        def __init__(self):
                super(CreateComputeCommand, self).__init__('createcompute', 'Creates a compute resource based on the supplied model')

        def __call__(self, user, prot, args):
                print "CreateComputeCommand called"
                print "Received request:"
                print args

                decoder = json.JSONDecoder()
                req, end = decoder.raw_decode(args)
                
                (config, configFiles) = getConfig(["Client"])
                resourceReq = req # req[0]
            
                i = createInstance(resourceReq, config, prot, user)
                print "Provisioning:\n" + str(i)
                
                try:
                    client = createClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi ----------------------"

                try:
                    queuedInstance = client.createVm(i)
                    instance = checkIfVmIsRunning(client, queuedInstance)
                    if not instance:
                    	error = {}
                        error['message'] = 'requesting the provisioning failed: VM is not in running state yet'
                        error['exception'] = ''
                        error['resource'] = resourceReq
                        prot.send_error("@error " + str(error), user)
                        return
                    
                    # we've to send this message plus any id used as a reference that the ISM tracks it
                    instance.hints['resource_id'] = resourceReq['resourceId']
                    host = filter(lambda h : h.id == instance.hostId, client.getHosts())[0].name
                    fqdn = getFqdn(host)
                    instance.hints['host'] = fqdn
                    print "Sending the following message: " + "@updatevm " + str(instance) + " To: " + str(user)
                    prot.send_result("@createvm " + str(instance), user)
                    
                except Exception as e:
                    print e
                    error = {}
                    error['message'] = 'The provisioning failed'
                    error['exception'] = str(e)
                    error['resource'] = resourceReq
                    prot.send_error("@error " + str(error), user)
                    
class CreateManyComputeCommand(BaseCommand):

        def __init__(self):
                super(CreateManyComputeCommand, self).__init__('createmanycompute', 'Creates a number of compute resources based on the supplied models')

        def __call__(self, user, prot, args):
                print "CreateManyComputeCommand called"
                print "Received request:"
                print args

                decoder = json.JSONDecoder()
                req, end = decoder.raw_decode(args)
                (config, configFiles) = getConfig(["Client"])
                try:
                    client = createClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi ----------------------"
                    
                resources = []
                for resourceReq in req:
                    i = createInstance(resourceReq, config, prot, user)
                    print "Provisioning:\n" + str(i)
                    try:
                        queuedInstance = client.createVm(i)
                        instance = checkIfVmIsRunning(client, queuedInstance)
                        if not instance:
                            msg = {}
                            msg["messageType"] = "createManyComputeResponse"
                            msg["status"] = "error"
                            msg["message"] = "VM was started, but is not in running state. Most probably there are not enough resources at the moment."
                            prot.send_error(json.dumps(msg), user)
                        else:
                            fqdn = getFqdn(instance.name)
                            resources.append(fqdn)
                    except Exception as e:
                        print e
                        msg = {}
                        msg["messageType"] = "createManyComputeResponse"
                        msg["status"] = "error"
                        msg["message"] = str(e)
                        prot.send_error(json.dumps(msg), user)
                        return
                msg = {}
                msg["messageType"]="createManyComputeResponse"
                msg["resources"] = resources
                if len(resources) == len(req):
                    msg["status"] = "ok"
                    message = json.dumps(msg) # str(msg) returns single quotes instead double that are required by json format
                    prot.send_result(message, user)
                else:
                    msg["status"] = "error"
                    message = json.dumps(msg) # str(msg) returns single quotes instead double that are required by json format
                    prot.send_result(message, user)

class UpdateComputeCommand(BaseCommand):

        def __init__(self):
                super(UpdateComputeCommand, self).__init__('updatecompute', 'Updates a compute resource by reprovisioning the VM')

        def __call__(self, user, prot, args):
                print "UpdateComputeCommand called"

                print "Received request:"
                print args

                decoder = json.JSONDecoder()
                req, end = decoder.raw_decode(args)    
                resourceReq = req[0]
                
                #todo: userId
                
                name = None
                if 'hostname' in resourceReq:
                    name = resourceReq['hostname']
    
                cores = None
                if 'cpu_cores' in resourceReq:
                    cores = resourceReq['cpu_cores']
             
                cpu_speed = None
                if 'cpu_speed' in resourceReq:
                    cpu_speed = int(resourceReq['cpu_speed'] * 1000) # needed in Hz as integer
                    
                memory = None
                if 'memory_size' in resourceReq:
                    memory = resourceReq['memory_size']
                
                try:
                    (config, configFiles) = getConfig(["Client"])
                    client = createClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi ----------------------"

                try:
                    instances = client.getInstances()
                    
                    migrating = False                    
                    instance = None
                    for i in instances:
                        #todo: check instances by id
                        if(i.name == name):
                            instance = i
                            if(memory > getFreeMemory(client, instance.hostId) or cpu_speed > getFreeCpu(client, instance.hostId)): 
                                #migration needed
                                print "Migration is needed for re-provisioning"
                                hId = getHostToMigrate(client, memory, cpu_speed, instance.hostId)
                                if(hId != None):
                                    print "The host %s is available for migration" % hId
                                    client.migrateVm(instance.id, hId)
                                    migrating = True
                                else:
                                    print "There is no host available for migration"
                                    prot.send_error("requesting the re-provisioning failed: no available resources", user)
                                    return
                            else: # migration not needed   
                                print "Migration is NOT needed for re-provisioning"
                                if(cpu_speed != None):
                                    client.adaptCpuShare(instance.id, int(cpu_speed))
                                if(memory != None):
                                    client.adaptMemoryLimit(instance.id, memory)
                                if(cores != None):
                                    #todo: hotpluggable cpus
                                    pass
                            break
                        
                    while migrating:
                        time.sleep(5)
                        # get refreshed instance (with new state)
                        for i in client.getInstances():
                            #todo: check instances by id
                            if(i.name == name):
                                instance = i
                                break
                            
                        print "The instance state is %s" % instance.state
                        if (instance.state == InstanceState.Running):
                            print "The instance was migrated, now the adaption steps will be taken"
                            migrating = False
                            # now do the reprovisioning on the new host
                            if(cpu_speed != None):
                                client.adaptCpuShare(instance.id, int(cpu_speed))
                            if(memory != None):
                                client.adaptMemoryLimit(instance.id, memory)
                            if(cores != None):
                                #todo: hotpluggable cpus
                                pass
                        else:
                            print "Migration is still going on"
                            
                    # get refreshed instance (with new memory... values)
                    for i in client.getInstances():
                        #todo: check instances by id
                        if(i.name == name):
                            instance = i
                            break
                        
                    print "The reprovisioned instance memory: %s, cpu speed: %s, cores: %s" % (instance.memory, instance.cpuShare, instance.cores)
                    instance.hints['resource_id'] = resourceReq['resourceId']
                    instance.hints['host'] = filter(lambda h : h.id == instance.hostId, client.getHosts())[0].name
                    prot.send_result("@updatevm " + str(instance), user)  
                except Exception as e:
                    print e
                    error = {}
                    error['message'] = 'The reprovisioning failed'
                    error['exception'] = str(e)
                    error['resource'] = resourceReq
                    prot.send_error("@error " + str(error), user)

class DeleteComputeCommand(BaseCommand):

        def __init__(self):
                super(DeleteComputeCommand, self).__init__('deletecompute', 'Deletes a compute resource based on the supplied model')

        def __call__(self, user, prot, args):
            print "DeleteComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                error = {}
                error['message'] = 'VM destroy failed: Id for a VM to be destroyed was not found'
                error['exception'] = ''
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                return

            try:
                client.destroyVm(vmId)
                print "Completed deletion of VM id: " + str(vmId)
                print "Sending the following message: " + "@deletevm " + str(args) + " To: " + str(user)
                prot.send_result("@deletevm " + str(vmId), user)
            except Exception as e:
                print e
                error = {}
                error['message'] = 'VM destroy failed'
                error['exception'] = str(e)
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                
class DeleteManyComputeCommand(BaseCommand):

        def __init__(self):
                super(DeleteManyComputeCommand, self).__init__('deletemanycompute', 'Deletes a compute resource based on the supplied model')

        def __call__(self, user, prot, args):
            print "DeleteManyComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)
            
            for resourceReq in req:
                vmId = parseIdFromMessage(resourceReq, client, "fqdn")
                if vmId == None:
                    msg = {}
                    msg["messageType"] = "deleteManyComputeResponse"
                    msg["status"] = "error"
                    msg["message"] = 'VM destroy failed: Id for a VM to be destroyed was not found'
                    prot.send_error(json.dumps(msg), user)
                    return

                try:
                    client.destroyVm(vmId)
                    print "Completed deletion of VM id: " + str(vmId)
                    print "Sending the following message: " + "@deletevm " + str(args) + " To: " + str(user)
                    prot.send_result("@deletevm " + str(vmId), user)
                except Exception as e:
                    print e
                    msg = {}
                    msg["messageType"] = "deleteManyComputeResponse"
                    msg["status"] = "error"
                    msg["message"] = str(e)
                    prot.send_error(json.dumps(msg), user)
                    return
            msg = {}
            msg["messageType"]= "deleteManyComputeResponse"
            msg["status"] = "ok"
            message = json.dumps(msg) # str(msg) returns single quotes instead double that are required by json format
            prot.send_result(message, user)
                
class MigrateComputeCommand(BaseCommand):

        def __init__(self):
                super(MigrateComputeCommand, self).__init__('migratecompute', 'Migrate a VM')

        def __call__(self, user, prot, args):
            print "MigrateComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "vm_fqdn")
            hostId = parseIdFromMessage(resourceReq, client, "host_fqdn")
            if vmId == None or hostId == None:
                error = {}
                error['message'] = 'VM migration failed: Id for a VM was not found'
                error['exception'] = ''
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                return
            try:
                client.migrateVm(vmId, hostId)
                print "VM %s migrated to host %s" % (vmId, hostId)
                prot.send_result("@migrateVm " + str(vmId), user)
            except Exception as e:
                print e
                error = {}
                error['message'] = 'VM migration failed'
                error['exception'] = str(e)
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                
class AdaptCpuComputeCommand(BaseCommand):

        def __init__(self):
                super(AdaptCpuComputeCommand, self).__init__('adaptcpucompute', 'Adapts CPU share')

        def __call__(self, user, prot, args):
            print "AdaptCpuComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                error = {}
                error['message'] = 'VM adapting CPU failed: Id for a VM was not found'
                error['exception'] = ''
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                return
            if 'share' in resourceReq:
                share = int(resourceReq['share'])
            else:
                print "share attribute not found in AdaptCpuComputeCommand"
                share = 1024
            try:
                client.adaptCpuShare(vmId, share)
                print "Completed CPU adaptation for VM id: " + str(vmId)
                prot.send_result("@adaptCpu " + str(vmId), user)
            except Exception as e:
                print e
                error = {}
                error['message'] = 'VM CPU adaptation failed'
                error['exception'] = str(e)
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                
class AdaptMemoryComputeCommand(BaseCommand):

        def __init__(self):
                super(AdaptMemoryComputeCommand, self).__init__('adaptmemorycompute', 'Adapts CPU share')

        def __call__(self, user, prot, args):
            print "AdaptMemoryComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                error = {}
                error['message'] = 'VM adapting memory limit failed: Id for a VM was not found'
                error['exception'] = ''
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)
                return
            if 'limit' in resourceReq:
                limit = int(resourceReq['limit'])
            else:
                print "limit attribute not found in AdaptCpuComputeCommand"
                limit = 512
            try:
                client.adaptMemoryLimit(vmId, limit)
                print "Completed memory adaptation for VM id: " + str(vmId)
                prot.send_result("@adaptMemory " + str(vmId), user)
            except Exception as e:
                print e
                error = {}
                error['message'] = 'VM memory adaptation failed'
                error['exception'] = str(e)
                error['resource'] = resourceReq
                prot.send_error("@error " + str(error), user)

class ResumeComputeCommand(BaseCommand):

        def __init__(self):
            super(ResumeComputeCommand, self).__init__('resumecompute', 'Resume a compute resource. The compute may be stopped or suspended')

        def __call__(self, user, prot, args):
            print "ResumeComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM resume failed: Id for a VM to be resumed was not found", user)
                return

            try:
                client.resumeVm(vmId)
                print "Sending the following message: " + "@resumevm " + str(args) + " To: " + str(user)
                prot.send_result("@resumevm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM resume failed: " + str(e), user)

class StopComputeCommand(BaseCommand):

        def __init__(self):
            super(StopComputeCommand, self).__init__('stopcompute', 'Stops a compute resource. The compute may only be started.')

        def __call__(self, user, prot, args):
            print "StopComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM shutdown failed: Id for a VM to be stopped was not found", user)
                return

            try:
                client.shutdownVm(vmId)
                print "Sending the following message: " + "@shutdownvm " + str(args) + " To: " + str(user)
                prot.send_result("@shutdownvm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM shutdown failed: " + str(e), user)
            
class RestartComputeCommand(BaseCommand):

        def __init__(self):
            super(RestartComputeCommand, self).__init__('restartcompute', 'Restarts a compute resource. The compute may only be started.')

        def __call__(self, user, prot, args):
            print "RestartComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM restart failed: Id for a VM to be restarted was not found", user)
                return 
            
            try:
                client.restartVm(vmId)
                print "Sending the following message: " + "@restartvm " + str(args) + " To: " + str(user)
                prot.send_result("@restartvm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM restart failed: " + str(e), user)

class SuspendComputeCommand(BaseCommand):

        def __init__(self):
            super(SuspendComputeCommand, self).__init__('suspendcompute', 'Suspends a compute resource. The compute may be only started.')

        def __call__(self, user, prot, args):
            print "SuspendComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM suspend failed: Id for a VM to be suspended was not found", user)
                return

            try:
                client.suspendVm(vmId)
                print "Sending the following message: " + "@suspendvm " + str(args) + " To: " + str(user)
                prot.send_result("@suspendvm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM suspend failed: " + str(e), user)
                
class PauseComputeCommand(BaseCommand):

        def __init__(self):
            super(PauseComputeCommand, self).__init__('pausecompute', 'Pauses a compute resource. The compute may be only started.')

        def __call__(self, user, prot, args):
            print "PauseComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM pause failed: Id for a VM to be paused was not found", user)
                return
                
            try:
                client.pauseVm(vmId)
                print "Sending the following message: " + "@pausevm " + str(args) + " To: " + str(user)
                prot.send_result("@pausevm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM pause failed: " + str(e), user)
                
class UnpauseComputeCommand(BaseCommand):

        def __init__(self):
            super(UnpauseComputeCommand, self).__init__('unpausecompute', 'Pauses a compute resource. The compute may be only started.')

        def __call__(self, user, prot, args):
            print "UnpauseComputeCommand called"
            try:
                (config, configFiles) = getConfig(["Client"])
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
                
            decoder = json.JSONDecoder()
            req, end = decoder.raw_decode(args)    
            resourceReq = req[0]
            
            vmId = parseIdFromMessage(resourceReq, client, "fqdn")
            if vmId == None:
                prot.send_error("@error VM unpause failed: Id for a VM to be unpaused was not found", user)
                return
                
            try:
                client.unpauseVm(vmId)
                print "Sending the following message: " + "@unpausevm " + str(args) + " To: " + str(user)
                prot.send_result("@unpausevm " + str(vmId), user)
            except Exception as e:
                print e
                prot.send_error("@error VM unpause failed: " + str(e), user)

class RegisterUserCommand(BaseCommand):

        def __init__(self):
                super(RegisterUserCommand, self).__init__('registeruser', 'registers user in the Tashi database')

        def __call__(self, user, prot, args):
                print "RegisterUserCommand called"
                
                try:
                    (config, configFiles) = getConfig(["Client"])
                    client = createClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi ----------------------"
                    pass

                try:
                    #todo: ISM message should contain name, passwd, priority
                    name = None
                    passwd = None
                    priority = None
                    id = client.registerUser(name, passwd, priority)
                    print "User with %s id registered" % id
                except Exception as e:
                    print e
                    prot.send_error("@error Registering user failed: " + str(e), user)

class UnregisterUserCommand(BaseCommand):

        def __init__(self):
                super(UnregisterUserCommand, self).__init__('unregisteruser', 'unregisters user in the Tashi database')

        def __call__(self, user, prot, args):
                print "UnregisterUserCommand called"
                
                try:
                    (config, configFiles) = getConfig(["Client"])
                    client = createClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi ----------------------"
                    pass

                try:
                    #todo: ISM message should contain user id
                    client.unregisterUser(id)
                    print "User with %s id unregistered" % id
                except Exception as e:
                    print e
                    prot.send_error("@error Registering user failed: " + str(e), user)

class ListImagesCommand(BaseCommand):
        def __init__(self):
                super(ListImagesCommand, self).__init__('listimages', 'Lists the available images on the tashi cluster')
        
        def __call__(self, user, prot, args):
            
            image_store = self.config.get("Vfs", "prefix") + '/images/'
            image_list = glob.glob(image_store+'*.img')
            
            prot.send_result(str(image_list), user)
            
class ListClusterConfigCommand(BaseCommand):
        def __init__(self):
                super(ListClusterConfigCommand, self).__init__('listclusterconfig', 'Lists cluster configuration')
        
        def __call__(self, user, prot, args):
            (config, configFiles) = getConfig(["Client"])
            try:
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
            conf = client.getClusterConfig()
            prot.send_result(conf, user)        
            
class ClusterConfigCommand(BaseCommand):
        def __init__(self):
                super(ClusterConfigCommand, self).__init__('clusterconfig', 'Lists cluster configuration')
        
        def __call__(self, user, prot, args):
            
            (config, configFiles) = getConfig(["Client"])
            try:
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
            
            decoder = json.JSONDecoder()
            reqs, end = decoder.raw_decode(args)
            for confReq in reqs:   
                if 'elemName' not in confReq:
                    prot.send_error("@error trying to configure cluster settings", user)
                    return
                else:
                    elemName = confReq['elemName']  
                if 'hostId' not in confReq:
                    prot.send_error("@error trying to configure cluster settings", user)
                    return
                else:
                    hostId = confReq['hostId']   
                if 'value' not in confReq:
                    prot.send_error("@error trying to configure cluster settings", user)
                    return
                else:
                    value = confReq['value']
            
                client.setHostConfig(hostId, elemName, value)
            
class ListVmLayoutCommand(BaseCommand):
        def __init__(self):
                super(ListVmLayoutCommand, self).__init__('listvmlayout', 'Lists VM layout')
        
        def __call__(self, user, prot, args):
            (config, configFiles) = getConfig(["Client"])
            try:
                client = createClient(config)
            except:
                print "ERROR -------------- could not connect to tashi ----------------------"
            layout = client.getVmLayout()
            prot.send_result(layout, user)      
            
class ListSchedulerConfigCommand(BaseCommand):
        def __init__(self):
                super(ListSchedulerConfigCommand, self).__init__('listschedulerconfig', 'Lists scheduler configuration')
        
        def __call__(self, user, prot, args):
            (config, configFiles) = getConfig(["Client"])
            try:
                client = createSchedulerClient(config)
            except:
                print "ERROR -------------- could not connect to tashi scheduler ----------------------"
                
            msg = {}
            msg["messageType"] = "schedulerSettingsResponse"
            try:
                conf = client.listConfiguration()
                msg["status"] = "ok"
                msg["scheduler"] = conf
                message = json.dumps(msg)
                prot.send_result(message, user)          
            except Exception as e:
                print e
                msg["status"] = "error"
                msg["message"] = str(e)
                prot.send_error(json.dumps(msg), user)
        
class ReconfigureSchedulerCommand(BaseCommand):

        def __init__(self):
                super(ReconfigureSchedulerCommand, self).__init__('reconfigurescheduler', 'reconfigures Tashi scheduler')

        def __call__(self, user, prot, args):
                print "ReconfigureSchedulerCommand called"
                
                (config, configFiles) = getConfig(["Client"])
                try:
                    client = createSchedulerClient(config)
                except:
                    print "ERROR -------------- could not connect to tashi scheduler ----------------------"
                
                decoder = json.JSONDecoder()
                req, end = decoder.raw_decode(args)
                
                resourceReq = req[0]
                
                if 'scheduler' not in resourceReq:
                    schedulerImpl = "tashi.scheduler.ConsolidatedScheduler"
                else:
                    schedulerImpl = resourceReq['scheduler']
                
                if 'schedulerDelay' not in resourceReq:
                    schedulerDelay = 2
                else:
                    schedulerDelay = resourceReq['schedulerDelay'] 
                    
                if 'rescheduleAfterStart' not in resourceReq:
                    rescheduleAfterStart = "True"
                else:
                    rescheduleAfterStart = resourceReq['rescheduleAfterStart']  
                    
                if 'cpuCoresOverRate' not in resourceReq:
                    cpuCoresOverRate = "True"
                else:
                    cpuCoresOverRate = resourceReq['cpuCoresOverRate'] 
                    
                if 'cpuSpeedOverRate' not in resourceReq:
                    cpuSpeedOverRate = "True"
                else:
                    cpuSpeedOverRate = resourceReq['cpuSpeedOverRate'] 
                    
                if 'cpuSpeedOverRate' not in resourceReq:
                    loadExpression = "True"
                else:
                    loadExpression = resourceReq['loadExpression'] 
                    
                if 'autoMigrations' not in resourceReq:
                    autoMigrations = "True"
                else:
                    autoMigrations = resourceReq['autoMigrations']
                    
                if 'guaranteedServers' not in resourceReq:
                    guaranteedServers = 10
                else:
                    guaranteedServers = resourceReq['guaranteedServers']  
                    
                if 'optimalConsolidation' not in resourceReq:
                    optimalConsolidation = "False"
                else:
                    optimalConsolidation = resourceReq['optimalConsolidation']
                    
                if 'minimalVmMemory' not in resourceReq:
                    minimalVmMemory = 64
                else:
                    minimalVmMemory = resourceReq['minimalVmMemory']    
                    
                if 'autoPowering' not in resourceReq:
                    autoPowering = False
                else:
                    autoPowering = resourceReq['autoPowering']
                
                schedulerConf = SchedulerConf() 
                schedulerConf.setFromParams(schedulerImpl, schedulerDelay, rescheduleAfterStart, cpuCoresOverRate, cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers, optimalConsolidation, minimalVmMemory, autoPowering)
                
                try:
                    client.configure(schedulerConf)               
                except Exception as e:
                    print e
                    prot.send_error("@error listing scheduler configuration failed: " + str(e), user)
                    
class RestartSchedulerCommand(BaseCommand):

        def __init__(self):
                super(RestartSchedulerCommand, self).__init__('restartscheduler', 'restarts Tashi scheduler')

        def __call__(self, user, prot, args):
                print "RestartSchedulerCommand called"
                
                decoder = json.JSONDecoder()
                req, end = decoder.raw_decode(args)
                
                resourceReq = req[0]
                
                if 'scheduler' not in resourceReq:
                    schedulerImpl = "tashi.scheduler.ConsolidatedScheduler"
                else:
                    schedulerImpl = resourceReq['scheduler']
                
                if 'schedulerDelay' not in resourceReq:
                    schedulerDelay = 2
                else:
                    schedulerDelay = resourceReq['schedulerDelay'] 
                    
                if 'rescheduleAfterStart' not in resourceReq:
                    rescheduleAfterStart = "True"
                else:
                    rescheduleAfterStart = resourceReq['rescheduleAfterStart']  
                    
                if 'cpuCoresOverRate' not in resourceReq:
                    cpuCoresOverRate = "True"
                else:
                    cpuCoresOverRate = resourceReq['cpuCoresOverRate'] 
                    
                if 'cpuSpeedOverRate' not in resourceReq:
                    cpuSpeedOverRate = "True"
                else:
                    cpuSpeedOverRate = resourceReq['cpuSpeedOverRate'] 
                    
                if 'cpuSpeedOverRate' not in resourceReq:
                    loadExpression = "True"
                else:
                    loadExpression = resourceReq['loadExpression'] 
                    
                if 'autoMigrations' not in resourceReq:
                    autoMigrations = "True"
                else:
                    autoMigrations = resourceReq['autoMigrations']
                    
                if 'guaranteedServers' not in resourceReq:
                    guaranteedServers = 10
                else:
                    guaranteedServers = resourceReq['guaranteedServers']  
                    
                if 'optimalConsolidation' not in resourceReq:
                    optimalConsolidation = "False"
                else:
                    optimalConsolidation = resourceReq['optimalConsolidation']
                    
                if 'minimalVmMemory' not in resourceReq:
                    minimalVmMemory = 64
                else:
                    minimalVmMemory = resourceReq['minimalVmMemory']    
                    
                if 'autoPowering' not in resourceReq:
                    autoPowering = False
                else:
                    autoPowering = resourceReq['autoPowering']
                
                output = os.popen("ps ax -o pid,command | grep schedulermanager.py").read()
                lines = output.split("\n")
                line = ""
                for i in lines:
                    if not 'grep' in i and 'schedulermanager.py' in i:
                        line = i    
                        break

                pid = line.strip().split(" ")[0]
                print "pid to kill: %s" % pid
                try:
                    subprocess.call("kill -9 %s" % pid, shell=True)
                except:
                    print "kill failed"
                    
                p = Process(target = startScheduler, args=(schedulerImpl, schedulerDelay, rescheduleAfterStart, cpuCoresOverRate, cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers, optimalConsolidation, minimalVmMemory, autoPowering), name = "startScheduler")
                p.start()
                    
                    
#This collects all commands and makes them available in all_commands
for __t in (t for t in globals().values() if isinstance(type, type(t))):
        if BaseCommand in __t.__mro__:
                try:
                        i = __t()
                        all_commands[i.name] = i
                except TypeError:
                        # Ignore abstract bases
                        pass
