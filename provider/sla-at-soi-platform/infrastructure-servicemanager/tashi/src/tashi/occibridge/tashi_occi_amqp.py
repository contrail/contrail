#!/usr/bin/env python

# Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Intel Performance Learning Solutions Ltd. nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Andrew Edmonds - andrewx.edmonds@intel.com
# @version        $Rev: 527 $
# @lastrevision   $Date: 2011-01-26 16:53:56 +0100 (sre, 26 jan 2011) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/occibridge/tashi_occi_amqp.py $

import pika
import tashi.occibridge.tashi_commands
from tashi.util import getConfig

class TashiAMQP():
    
    def __init__(self, config):
        
        self.host = config.get('AMQP', 'host')
        self.user = config.get('AMQP', 'user')
        self.password = config.get('AMQP', 'password')
        self.command_queue = config.get('AMQP', 'command_queue')
        self.result_queue = config.get('AMQP', 'result_queue')
        self.warn_queue = config.get('AMQP', 'warn_queue')
        self.error_queue = config.get('AMQP', 'error_queue')
        
        self.connection = pika.AsyncoreConnection(pika.ConnectionParameters(host=self.host, credentials=pika.PlainCredentials(self.user, self.password)))
        self.channel = self.connection.channel()
        
        #setup the queues
        self.channel.queue_declare(queue=self.command_queue)
        self.channel.queue_declare(queue=self.result_queue)
        self.channel.queue_declare(queue=self.warn_queue)
        self.channel.queue_declare(queue=self.error_queue)
        
        #get a list of valid commands that we can process
        self.commands = tashi.occibridge.tashi_commands.all_commands
        
        #listen on the command queue for incoming requests
        self.channel.basic_consume(self.executeCommand, queue=self.command_queue, no_ack=True)

    def _handleCommand(self, msg, cmd, args):
        self.commands[cmd.lower()](msg, self, args)
                
    def executeCommand(self, ch, method, header, body):        
        a=str(body).split(' ', 1)
        args = a[1] if len(a) > 1 else None
        
        if self.commands.has_key(a[0].lower()):
            self._handleCommand(self.result_queue, a[0], args)
        else:
            self.send_error(content='No such command: ' + a[0])
    
    def send_result(self, content, endpoint=None):
        self.__send_plain(self.result_queue, content)
    
    def send_warn(self, content, endpoint=None):
        self.__send_plain(self.warn_queue, content)
    
    def send_error(self, content, endpoint=None):
        self.__send_plain(self.error_queue, content)
        
    def __send_plain(self, endpoint, content):
        self.channel.basic_publish(exchange='', routing_key=endpoint, body=content)
    
    def run(self):
        print 'Waiting for OCCI commands to handle. To exit press CTRL+C'
        pika.asyncore_loop()

if __name__ == '__main__':
    (config, configFiles) = getConfig(['ClusterManager'])
    TashiAMQP(config).run()
    