#!/usr/bin/env python

# Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Intel Performance Learning Solutions Ltd. nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Andrew Edmonds - andrewx.edmonds@intel.com
# @version        $Rev: 822 $
# @lastrevision   $Date: 2011-02-24 13:20:00 +0100 (čet, 24 feb 2011) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/occibridge/tashi_protocol.py $

from wokkel.xmppim import MessageProtocol, AvailablePresence
from twisted.words.xish import domish

import tashi.occibridge.tashi_commands

class TashiProtocol(MessageProtocol):

        def __init__(self, config):
                self.config = config

        def connectionMade(self):
                print "Connected!"
                # poulate the bot's supported commands
                self.commands = tashi.occibridge.tashi_commands.all_commands
                # send initial presence
                self.send(AvailablePresence())
                # send the machine spec if running in node mode?

        def connectionLost(self, reason):
                print "Disconnected!"

        def _handleCommand(self, msg, cmd, args):
                self.commands[cmd.lower()](msg['from'], self, args)

        def send_result(self, content, endpoint=None):
            self.__send_plain(content, jid=endpoint)
        
        def send_warn(self, content, endpoint=None):
            self.__send_plain(content, jid=endpoint)
        
        def send_error(self, content, endpoint=None):
            self.__send_plain(content, jid=endpoint)
        
        def __send_plain(self, content, jid):
                msg = domish.Element((None, "message"))
                msg["to"] = jid
                msg["from"] = self.config.get('OCCIBridge', 'jid')
                msg["type"] = 'chat'
                msg.addElement("body", content=content)

                self.xmlstream.send(msg)

        def onMessage(self, msg):
                # checking message type removed, because this seems not to be set when using Java
                if hasattr(msg, "body") and msg.body:
                        #a=unicode(msg.body).split(' ', 1)
                        a=str(msg.body).split(' ', 1)
                        args = a[1] if len(a) > 1 else None
                        if self.commands.has_key(a[0].lower()):
                                self._handleCommand(msg, a[0], args)
                        else:
                                self.__send_plain('No such command: ' + a[0], msg['from'])
