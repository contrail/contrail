import xmpp, json

live = True
username = 'tashi-scheduler@slasoi-testbed.xlab.si'
passwd = 'tashi-scheduler'
to='tashi@slasoi-testbed.xlab.si'


client = xmpp.Client('slasoi-testbed.xlab.si')
client.connect(server=('slasoi-testbed.xlab.si',5222))
client.auth(username, passwd, 'slasoi-testbed.xlab.si')
client.sendInitPresence()

for i in range(0, 1):
	#msg = 'restartscheduler [{"scheduler":"tashi.scheduler.ConsolidatedScheduler","schedulerDelay":3,"densePack":"False","cpuCoresOverRate":2,"cpuSpeedOverRate":2,"rescheduleAfterStart":"True","loadExpression":"memUsage+coresUsage*512+cpuUsage","autoMigrations":"True","guaranteedServers":10,"optimalConsolidation":"False", "minimalVmMemory":64, "autoPowering":"False"}]'
	msg = 'reconfigurescheduler [{"scheduler":"tashi.scheduler.ConsolidatedScheduler","schedulerDelay":3,"densePack":"False","cpuCoresOverRate":2,"cpuSpeedOverRate":2,"rescheduleAfterStart":"True","loadExpression":"memUsage+coresUsage*512+cpuUsage","autoMigrations":"True","guaranteedServers":10,"optimalConsolidation":"False", "minimalVmMemory":64, "autoPowering":"False"}]'

	
	#check parsing as it is needed in tashi_commands:
	a=str(msg).split(' ', 1)
	args = a[1] if len(a) > 1 else None
                      
	decoder = json.JSONDecoder()
	req, end = decoder.raw_decode(args)
	resourceReq = req[0]
	print resourceReq			
	if(not resourceReq['densePack']):
		densePack = "False"
	else:
		densePack = resourceReq['densePack']
              
    
	if(live):
		message = xmpp.Message(to, msg)
		message.setAttr('type', 'chat')
		print "Sending message"
		client.send(message)

client.disconnect()