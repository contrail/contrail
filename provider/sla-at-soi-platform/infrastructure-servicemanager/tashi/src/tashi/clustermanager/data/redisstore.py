# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.    

# This file contains contributions by Intel Performance Learning Solutions Ltd. 
# (Copyright (c) 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Andy Edmonds - andrewx.edmonds@intel.com
# @version $Rev: 2432 $
# @lastrevision $Date: 2011-06-30 01:29:57 +0200 (čet, 30 jun 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/clustermanager/data/redisstore.py $

from redis import Redis
from redis.exceptions import ResponseError, ConnectionError
from tashi.clustermanager.data import DataInterface
from tashi.rpycservices.rpyctypes import *
import ast #python 2.6+
import logging
import threading

# Throughout this module print statements are used as rudimentary logging.
# TODO: integrate with tashi's logging system
# TODO: allow for authenticated use of redis

class RedisUtils():

	@staticmethod
	def get(redisConn, CLASS, id=None):
		"""
		Will retreive the list of instances, if id is not None 
		then will retreive specific instance
		Return for all objects is a list of dicts that maps to tashi types
		Return for specified object: a dict that maps to tashi types
			This dict needs to be passed to the relevant tashi type constructor
		Return for not found: None
		"""
		#print "Getting all instances of type: " + CLASS
		if id:
			#print "Id supplied"
			if redisConn.exists(CLASS+':'+str(id)):
				redisInstance = redisConn.hgetall(CLASS+':'+str(id))
				
				for key, value in redisInstance.items():
					if value[0] == '{' or value[0] == '}' or value[0]=='[' and value[len(value)-1] == ']':
						redisInstance[key] = ast.literal_eval(value)

				# this is an untyped object, however it can be supplied to the constructor
				# of any tashi defined type (see rpyctypes.py)
				return redisInstance
			else:
				#print "Not found. Object ID: " + str(id) + ". Type: " + str(CLASS)
				return None
		else:
			#print "No id supplied... listing all"
			instanceIds = redisConn.smembers(CLASS+'.all')
			if(instanceIds and len(instanceIds)>0):
				#use a non-transactional pipe to batch the hgetall
				pipe = redisConn.pipeline(False)
				for id in instanceIds:
					pipe.hgetall(CLASS+':'+str(id))
				
				res = pipe.execute()
				
				# 2 for loops - anyway to reduce this?
				for item in res:
					for key, value in item.items():
						if value[0] == '[' or value[0] == '{': #if the 1st character starts with...
							item[key] = ast.literal_eval(value) #convert from string to object
				return res 
			else:
				#print "No objects of the specified type exist. Type: " + str(CLASS)
				return []
	
	@staticmethod
	def getNewId(redisConn, CLASS):
		"""
		Returns a unique id for tracking resources
		"""
		id = redisConn.incr(CLASS+".next_id")
		# store the instance id in a set - is this better in getNewId()?
		redisConn.sadd(CLASS+'.all', id)
		return id

class RedisInstance():
	"""
	Represents a tashi managed resource in redis
	"""
	
	#XXX SLA@SOI: for the usage with ISM and tashi this
	#    must be set before the use of any of the classes methods
	CLASS = '' 
	
	def __init__(self, instance, redisConn, CLASS):
		
		self.redisConn = redisConn
		self.instance = instance
		
		#print "Instance class is: " + str(type(self.instance))
		RedisInstance.CLASS = CLASS
					
	def save(self):
		"""
		Will save a new instance if not found, otherwise it will update the exising in redis
		Note: for usage with grails' redis plugin it adds indices on all values
		TODO: make indices configurable
		"""
		# pipe used to queue commands that will be executed inside a transaction
		pipe = self.redisConn.pipeline()

		if not self.instance.id:
			#print "No ID associated with this instance. Will associate one"
			self.instance.id = RedisUtils.getNewId(self.redisConn, self.CLASS)

		#if self.instance.id and not RedisUtils.get(self.redisConn, self.CLASS, self.instance.id):
		#	print "Instance has ID but it is not saved to store"

		existingInstance = RedisUtils.get(self.redisConn, self.CLASS, self.instance.id)
		if existingInstance: 
			pass
			#print "Instance already exists in store. Updating..."

		# add/update instance record
		pipe.hmset(self.CLASS+':'+str(self.instance.id), self.instance.__dict__)
	
		# add indices
		#print "Adding/updating indices"
		# existing ones need to be deleted first
		if existingInstance:
			for key, value in self.__dict__.items():
				pipe.delete(self.CLASS+":"+str(key)+":"+str(value))
		
		for key, value in self.instance.__dict__.items():
			#print "Adding as index: " + self.CLASS+":"+str(key)+":"+str(value) + " for: " + str(self.instance.id)
			pipe.sadd(self.CLASS+":"+str(key)+":"+str(value), self.instance.id)

		pipe.zadd(self.CLASS+":id:sorted", "3.0", self.instance.id)
		
		try:
			pipe.execute()
		except ResponseError:
			print "Failed to save/update."
		except ConnectionError:
			print "The redis client cannot connect"
	
	def delete(self):
		"""
		will delete the instance from redis
		"""

		pipe = self.redisConn.pipeline()
		#remove indices
		for key, value in self.instance.__dict__.items():
			pipe.delete(self.CLASS+":"+str(key)+":"+str(value))
			
		pipe.delete(self.CLASS+":id:sorted", "3.0", self.instance.id)
		pipe.delete(self.CLASS+":"+str(self.instance.id))

		#remove from set of tracked resources for the type
		pipe.srem(self.CLASS+'.all', self.instance.id)

		res = None

		try:
			#print "Removing instance from store."
			res = pipe.execute()
		except Exception:
			print "Deletion failed."
		return res

# ---------- Tashi Data Driver ----------

class RedisStore(DataInterface):
	"""
	Interface for a functional data access mechanism
	Implements a data driver to persist tashi state in redis
	See: http://code.google.com/p/redis/
	"""
	def __init__(self, config=None):
		
		self.log = logging.getLogger(__name__)
		
		if config:
			self.config = config
			DataInterface.__init__(self, config)
			
			#get the redis server to connect to
			self.conn = Redis(self.config.get("Redis", "host"))
			
			#get the namespace and class names that are used for other ORM mapping technologies
			self.HOST_CLASS=self.config.get("Redis", "hostClass")
			self.INSTANCE_CLASS=self.config.get("Redis", "instanceClass")
			self.NETWORK_CLASS=self.config.get("Redis", "networkClass")
			self.USER_CLASS=self.config.get("Redis", "userClass")
		else:
			self.conn = Redis('slug', password=None)
			self.HOST_CLASS='be.edmonds.Host'
			self.INSTANCE_CLASS='be.edmonds.Instance'
			self.NETWORK_CLASS='be.edmonds.Network'
			self.USER_CLASS='be.edmonds.User'

		#locks used for exclusive access - may not be necessary with the redis client
		self.instanceLock = threading.Lock()
		self.instanceIdLock = threading.Lock()
		self.instanceLocks = {}
		self.hostLock = threading.Lock()
		self.hostLocks = {}
		self.userLock = threading.Lock()
		self.idLock = threading.Lock()
		self.sqlLock = threading.Lock()

# ---------- Physical Host Management ----------

	def registerHost(self, hostname, memory, cores, cpuSpeed, version):
		self.hostLock.acquire()
		
		hosts = self.getHosts()
		if(len(hosts) > 0):
			for host in hosts.values():
				if host.name == hostname:
					#update
					host.memory = memory
					host.cores = cores
					host.cpuSpeed = cpuSpeed
					host.version = version
					
					hi = RedisInstance(host, self.conn, self.HOST_CLASS)
					hi.save()
					
					self.hostLock.release()
					return host.id, True 
		
		id = RedisUtils.getNewId(self.conn, self.HOST_CLASS)
		host = Host(d={'id': id, 'up': 0, 'decayed': 0, 'state': 1, 'name': hostname, 'memory':memory, 'cores': cores, 'cpuSpeed':cpuSpeed, 'version':version, 'poweredOn':1, 'auditability': 1, 'location':IsoCountryCode.Slovenia, 'sas70':0, 'ccr':0, 'dataClass': DataClassification.Public, 'hwLevel': HardwareRedundancyLevel.Standard, 'diskThroughput': DiskThroughputLevel.Standard, 'netThroughput': NetThroughputLevel.Standard, 'dataEncryption': 0})
		redisHost = RedisInstance(host, self.conn, self.HOST_CLASS)
		redisHost.save()
		
		self.hostLock.release()

		return redisHost.instance.id, False
	
	def unregisterHost(self, hostId):
		self.hostLock.acquire()
		
		host = RedisUtils.get(self.conn, self.HOST_CLASS, hostId)
		host = Host(d=host)
		host = RedisInstance(host, self.conn, self.HOST_CLASS)
		host.delete()
		
		self.hostLock.release()

	def acquireHost(self, hostId):
		
		self.hostLock.acquire()
		
		host = self.getHost(hostId)
		
		host._lock = threading.Lock()
		self.hostLocks[host.id] = self.hostLocks.get(host.id, threading.Lock())
		self.hostLock.release()
		host._lock = self.hostLocks[host.id]
		host._lock.acquire()
		
		return host
	
	def releaseHost(self, host):
		redisHost = RedisInstance(host, self.conn, self.HOST_CLASS)
		redisHost.save()
		
		host._lock.release()
	
	def getHosts(self):
		rhosts = RedisUtils.get(self.conn, self.HOST_CLASS)
		hosts = {}
		for host in rhosts:
			host = Host(host)
			hosts[host.id] = host
		return hosts
	
	def getHost(self, id):
		host = RedisUtils.get(self.conn, self.HOST_CLASS, id)
		
		if host:
			host = Host(host)
		
		return host

# ---------- Virtual Machine Management ----------

	def registerInstance(self, instance):
		self.instanceLock.acquire()
		try:
			if instance.id: # and (instance.id not in self.getInstances()):
				raise Exception('Instance Id is present - the redis subsystem sets these not the client!')
				#XXX - is this necessary given features in redis?
				#self.instanceIdLock.acquire()
				#if (instance.id >= self.getMaxInstanceId()):
				#	self.maxInstanceId = self.incrementMaxInstanceId()
				#self.instanceIdLock.release()
			else:
				instance.id = RedisUtils.getNewId(self.conn, self.INSTANCE_CLASS)
			
			instance._lock = threading.Lock()
			self.instanceLocks[instance.id] = instance._lock
			instance._lock.acquire()
			redisInstance = RedisInstance(instance, self.conn, self.INSTANCE_CLASS)
			redisInstance.save()
		finally:
			self.instanceLock.release()
		return instance
	
	def acquireInstance(self, instanceId):
		self.instanceLock.acquire()
		try:
			instance = self.getInstance(instanceId)
			
			instance._lock = threading.Lock()
			self.instanceLocks[instance.id] = self.instanceLocks.get(instance.id, threading.Lock())
			instance._lock = self.instanceLocks[instance.id]
			instance._lock.acquire()
		finally:
			self.instanceLock.release()
		return instance
	
	def releaseInstance(self, instance):
		self.instanceLock.acquire()
		try:
			redisInstance = RedisInstance(instance, self.conn, self.INSTANCE_CLASS)
			redisInstance.save()
			instance._lock.release()
		finally:
			self.instanceLock.release()
	
	def removeInstance(self, instance):
		self.instanceLock.acquire()
		try:
			redisInstance = RedisInstance(instance, self.conn, self.INSTANCE_CLASS)
			redisInstance.delete()
			instance._lock.release()
			del self.instanceLocks[instance.id]
		finally:
			self.instanceLock.release()
	
	def getInstances(self):
		rInstances = RedisUtils.get(self.conn, self.INSTANCE_CLASS)

		instances = {}
		
		for instance in rInstances:
			instance = Instance(instance)
			instances[instance.id] = instance
		return instances

	def getInstance(self, id):
		instance = RedisUtils.get(self.conn, self.INSTANCE_CLASS, id)
		
		if instance:
			instance = Instance(instance)
		
		return instance

# ---------- Networking ----------

	def getNetworks(self):
		rNetworks = RedisUtils.get(self.conn, self.NETWORK_CLASS)
		networks = {}
		for network in rNetworks:
			network = Network(network)
			networks[network.id] = network
		return networks
	
	def getNetwork(self, id):
		network = RedisUtils.get(self.conn, self.NETWORK_CLASS, id)
		return Network(network)

# ---------- Users ----------

	def getUsers(self):
		rUsers = RedisUtils.get(self.conn, self.USER_CLASS)
		users = {}
		for user in rUsers:
			user = User(user)
			users[user.id] = user
		return users
	
	def getUser(self, id):
		rUser = RedisUtils.get(self.conn, self.USER_CLASS, id)
		if rUser:
			rUser = User(rUser)
		return rUser
	
	def registerUser(self, name, passwd, priority):
		self.userLock.acquire()
		
		id = RedisUtils.getNewId(self.conn, self.USER_CLASS)
		user = User(d={'id': id, 'name': name, 'passwd': passwd, 'priority' : priority})
		redisUser = RedisInstance(user, self.conn, self.USER_CLASS)
		redisUser.save()
		
		self.userLock.release()
		return id
	
	def unregisterUser(self, userId):
		self.userLock.acquire()
		
		redisUser = RedisUtils.get(self.conn, self.USER_CLASS, userId)
		u = RedisInstance(User(redisUser), self.conn, self.USER_CLASS)
		u.delete()
		
		self.userLock.release()
