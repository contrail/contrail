# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.    

# This file contains modifications by XLAB d.o.o. (Copyright (c) 
# 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 304 $
# @lastrevision $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/clustermanager/data/datainterface.py $

class DataInterface(object):
	"""Interface for a functional data access mechanism"""
	def __init__(self, config):
		if (self.__class__ is DataInterface):
			raise NotImplementedError
		self.config = config
	
	def registerInstance(self, instance):
		raise NotImplementedError
	
	def acquireInstance(self, instanceId):
		raise NotImplementedError
	
	def releaseInstance(self, instance):
		raise NotImplementedError
	
	def removeInstance(self, instance):
		raise NotImplementedError
	
	def acquireHost(self, hostId):
		raise NotImplementedError
	
	def releaseHost(self, host):
		raise NotImplementedError
	
	def getHosts(self):
		raise NotImplementedError
	
	def getHost(self, id):
		raise NotImplementedError
	
	def getInstances(self):
		raise NotImplementedError
	
	def getInstance(self, id):
		raise NotImplementedError
	
	def getNetworks(self):
		raise NotImplementedError
	
	def getNetwork(self, id):
		raise NotImplementedError
	
	def getUsers(self):
		raise NotImplementedError
	
	def getUser(self, id):
		raise NotImplementedError
	
	def registerUser(self, name, passwd, priority):
		# only sql implementation supports this for now
		raise NotImplementedError
	
	def unregisterUser(self, userId):
		# only sql implementation supports this for now
		raise NotImplementedError
	
	def registerHost(self, hostname, memory, cores, cpuSpeed, version):
		raise NotImplementedError
	
	def unregisterHost(self, hostId):
		raise NotImplementedError
