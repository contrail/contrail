# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.    

# This file contains modifications by XLAB d.o.o. (Copyright (c) 
# 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 3002 $
# @lastrevision $Date: 2011-08-24 09:57:21 +0200 (sre, 24 avg 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/clustermanager/clustermanagerservice.py $

from datetime import datetime
from random import randint
import logging
import threading
import time

import tashi
from tashi.rpycservices.rpyctypes import Errors, InstanceState, HostState, TashiException
from tashi import boolean, convertExceptions, ConnectionManager, vmStates, timed, version, scrubString
from tashi.messaging.xmppsensor import XmppSensor
from tashi.util import instantiateImplementation, getFqdn

class ClusterManagerService(object):
	"""RPC service for the ClusterManager"""
	
	def __init__(self, config, data, dfs):
		self.config = config
		self.data = data
		self.authAndEncrypt = boolean(config.get('Security', 'authAndEncrypt'))
		if self.authAndEncrypt:
			self.username = config.get('AccessNodeManager', 'username')
			self.password = config.get('AccessNodeManager', 'password')
		else:
			self.username = None
			self.password = None
		self.proxy = ConnectionManager(self.username, self.password, int(self.config.get('ClusterManager', 'nodeManagerPort')))
		self.dfs = dfs
		self.convertExceptions = boolean(config.get('ClusterManagerService', 'convertExceptions'))
		self.log = logging.getLogger(__name__)
		self.lastContacted = {}
		self.decayedHosts = {}
		self.decayedInstances = {}
		self.expireHostTime = float(self.config.get('ClusterManagerService', 'expireHostTime'))
		self.allowDecayed = float(self.config.get('ClusterManagerService', 'allowDecayed'))
		self.allowMismatchedVersions = boolean(self.config.get('ClusterManagerService', 'allowMismatchedVersions'))
		self.maxMemory = int(self.config.get('ClusterManagerService', 'maxMemory'))
		self.maxCores = int(self.config.get('ClusterManagerService', 'maxCores'))
		self.allowDuplicateNames = boolean(self.config.get('ClusterManagerService', 'allowDuplicateNames'))
		now = time.time()
		for instance in self.data.getInstances().itervalues():
			instanceId = instance.id
			instance = self.data.acquireInstance(instanceId)
			instance.decayed = False
			self.stateTransition(instance, None, InstanceState.Orphaned)
			self.data.releaseInstance(instance)
		for host in self.data.getHosts().itervalues():
			hostId = host.id
			host = self.data.acquireHost(hostId)
			host.up = False
			host.decayed = False
			self.data.releaseHost(host)
		self.decayLock = threading.Lock()
		threading.Thread(target=self.monitorHosts).start()
		self.monitoringSensor = instantiateImplementation(config.get("Monitoring", "sensor"), config)

	def stateTransition(self, instance, old, cur):
		if (old and instance.state != old):
			self.data.releaseInstance(instance)
			raise TashiException(d={'errno':Errors.IncorrectVmState,'msg':"VmState is not %s - it is %s" % (vmStates[old], vmStates[instance.state])})
		instance.state = cur

	def updateDecay(self, set, obj):
		now = time.time()
		self.decayLock.acquire()
		if (obj.decayed and obj.id not in set):
			set[obj.id] = now
		elif (not obj.decayed and obj.id in set):
			del set[obj.id]
		self.decayLock.release()
		

	def monitorHosts(self):
		# XXX: retry multiple hosts (iterate through them even with an exception)
		while True:
			now = time.time()
			sleepFor = min(self.expireHostTime, self.allowDecayed)
			try:
				for k in self.lastContacted.keys():
					if (self.lastContacted[k] < (now-self.expireHostTime)):
						host = self.data.acquireHost(k)
						try: 
							self.log.warning('Host %s has expired after %f seconds' % (host.name, now-self.expireHostTime))
							for instanceId in [instance.id for instance in self.data.getInstances().itervalues() if instance.hostId == host.id]:
								instance = self.data.acquireInstance(instanceId)
								instance.decayed = True
								self.stateTransition(instance, None, InstanceState.Orphaned)
								self.data.releaseInstance(instance)
							host.up = False
							host.decayed = False
						finally:
							self.data.releaseHost(host)
						del self.lastContacted[k]
					else:
						sleepFor = min(self.lastContacted[k] + self.expireHostTime - now, sleepFor)
				for hostId in self.decayedHosts.keys():
					if (self.decayedHosts[hostId] < (now-self.allowDecayed)):
						host = self.data.getHost(hostId)
						self.log.warning('Fetching state from host %s because it is decayed' % (host.name))
						hostProxy = self.proxy[host.name]
						oldInstances = [i for i in self.data.getInstances().values() if i.hostId == host.id]
						instances = [hostProxy.getVmInfo(vmId) for vmId in hostProxy.listVms()]
						instanceIds = [i.id for i in instances]
						for instance in instances:
							if (instance.id not in self.data.getInstances()):
								instance.hostId = host.id
								instance = self.data.registerInstance(instance)
								self.data.releaseInstance(instance)
						for instance in oldInstances:
							if (instance.id not in instanceIds):
								instance = self.data.acquireInstance(instance.id)
								self.data.removeInstance(instance)
						self.decayedHosts[hostId] = now
					else:
						sleepFor = min(self.decayedHosts[hostId] + self.allowDecayed - now, sleepFor)
				for instanceId in self.decayedInstances.keys():
					try:
						if (self.decayedInstances[instanceId] < (now-self.allowDecayed)):
							self.log.warning('Fetching state on instance %d because it is decayed' % (instanceId))
							try:
								instance = self.data.getInstance(instanceId)
							except TashiException, e:
								if (e.errno == Errors.NoSuchInstanceId):
									del self.decayedInstances[instanceId]
									continue
								else:
									raise
							host = self.data.getHost(instance.hostId)
							hostProxy = self.proxy[host.name]
							instance = hostProxy.getVmInfo(instance.vmId)
							oldInstance = self.data.acquireInstance(instanceId)
							oldInstance.state = instance.state
							self.data.releaseInstance(oldInstance)
							self.decayedInstances[instanceId] = now
						else:
							sleepFor = min(self.decayedInstances[instanceId] + self.allowDecayed - now, sleepFor)
					except Exception, e:
						self.log.exception('Exception in monitorHosts trying to get instance information')
			except Exception, e:
				self.log.exception('Exception in monitorHosts')
			time.sleep(sleepFor)
	
	def normalize(self, instance):
		instance.id = None
		instance.vmId = None
		instance.hostId = None
		instance.decayed = False
		instance.name = scrubString(instance.name, allowed="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.")
		instance.state = InstanceState.Pending
		# At some point, check userId
		if (not self.allowDuplicateNames):
			for i in self.data.getInstances().itervalues():
				if (i.name == instance.name):
					raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"The name %s is already in use" % (instance.name)})
		if (instance.cores < 1):
			raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Number of cores must be >= 1"})
		if (instance.cores > self.maxCores):
			raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Number of cores must be <= %d" % (self.maxCores)})
		if (instance.memory < 1):
			raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Amount of memory must be >= 1"})
		if (instance.memory > self.maxMemory):
			raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Amount of memory must be <= %d" % (self.maxMemory)})
		# Make sure disk spec is valid
		# Make sure network spec is valid
		# Ignore hints
		for hint in instance.hints:
			if (hint.startswith("__")):
				del instance.hints[hint]
		return instance
	
	def createVm(self, instance):
		"""Function to add a VM to the list of pending VMs"""
		instance = self.normalize(instance)
		instance = self.data.registerInstance(instance)
		self.data.releaseInstance(instance)
		return instance
	
	def shutdownVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Running, InstanceState.ShuttingDown)
		self.data.releaseInstance(instance)
		hostname = self.data.getHost(instance.hostId).name
		try:
			self.proxy[hostname].shutdownVm(instance.vmId)
		except Exception:
			self.log.exception('shutdownVm failed for host %s vmId %d' % (instance.name, instance.vmId))
			raise
		self.monitoringSensor.publishVmLayout()
		fqdn = getFqdn(instance.name)
		self.monitoringSensor.publishAction("VM with id %s on host %s shutdown" % (instance.id, instance.hostId), instance.userId, fqdn)
		return
	
	def destroyVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		if (instance.state is InstanceState.Pending or instance.state is InstanceState.Held):
			self.data.removeInstance(instance)
		elif (instance.state is InstanceState.Activating):
			self.stateTransition(instance, InstanceState.Activating, InstanceState.Destroying)
			self.data.releaseInstance(instance)
		else:
			self.stateTransition(instance, None, InstanceState.Destroying)
			self.data.releaseInstance(instance)
			hostname = self.data.getHost(instance.hostId).name
			try:
				self.proxy[hostname].destroyVm(instance.vmId)
				self.dfs.eraseDisks(instance)
			except Exception:
				self.log.exception('destroyVm failed for host %s vmId %d' % (hostname, instance.vmId))
				raise
		self.monitoringSensor.publishVmLayout()
		fqdn = getFqdn(instance.name)
		self.monitoringSensor.publishAction("VM %s on host %s destroyed" % (fqdn, instance.hostId), instance.userId, fqdn)
		return
	
	def restartVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Running, InstanceState.Restarting)
		self.data.releaseInstance(instance)
		hostname = self.data.getHost(instance.hostId).name
		try:
			self.proxy[hostname].restartVm(instance.vmId)
		except Exception:
			self.log.exception('restartVm failed for host %s vmId %d' % (instance.name, instance.vmId))
			raise
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Restarting, InstanceState.Running)
		self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		fqdn = getFqdn(instance.name)
		self.monitoringSensor.publishAction("VM with id %s on host %s restarted" % (instance.id, instance.hostId), instance.userId, fqdn)
		return
	
	def suspendVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Running, InstanceState.Suspending)
		self.data.releaseInstance(instance)
		hostname = self.data.getHost(instance.hostId).name
		destination = "suspend/%d_%s" % (instance.id, instance.name)
		try:
			self.proxy[hostname].suspendVm(instance.vmId, destination)
		except:
			self.log.exception('suspendVm failed for host %s vmId %d' % (hostname, instance.vmId))
			raise TashiException(d={'errno':Errors.UnableToSuspend, 'msg':'Failed to suspend %s' % (instance.name)})
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Suspending, InstanceState.Suspended)
		self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		host_fqdn = getFqdn(hostname)
		self.monitoringSensor.publishAction("VM %s on host %s suspended" % (vm_fqdn, host_fqdn), instance.userId, vm_fqdn)
		return
	
	def resumeVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Suspended, InstanceState.Pending)
		source = "suspend/%d_%s" % (instance.id, instance.name)
		instance.hints['__resume_source'] = source
		self.data.releaseInstance(instance)
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Pending, InstanceState.Running)
		self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		host_fqdn = getFqdn(self.data.getHost(instance.hostId).name)
		self.monitoringSensor.publishAction("VM %s on host %s resumed" % (vm_fqdn, host_fqdn), instance.userId, vm_fqdn)
		return instance
	
	def migrateVm(self, instanceId, targetHostId):
		instance = self.data.acquireInstance(instanceId)
		try:
			# FIXME: should these be acquire/release host?
			targetHost = self.data.getHost(targetHostId)
			sourceHost = self.data.getHost(instance.hostId)
			# FIXME: Are these the correct state transitions?
		except:
			self.data.releaseInstance(instance)
			raise
		self.stateTransition(instance, InstanceState.Running, InstanceState.MigratePrep)
		self.data.releaseInstance(instance)
		try:
			# Set source instance state
			self.log.info("migrateVm: prepSourceVm will be executed on the target host")
			self.proxy[sourceHost.name].prepSourceVm(instance.vmId)
			# Prepare the target
			self.log.info("migrateVm: prepReceiveVm will be executed on the target host")
			cookie = self.proxy[targetHost.name].prepReceiveVm(instance, sourceHost)
		except Exception, e:
			self.log.exception('prepReceiveVm failed')
			raise
		instance = self.data.acquireInstance(instance.id)
		self.stateTransition(instance, InstanceState.MigratePrep, InstanceState.MigrateTrans)
		self.data.releaseInstance(instance)
		try:
			# Send the VM
			self.log.info("migrateVm: migrateVm function will be executed on the source host")
			self.proxy[sourceHost.name].migrateVm(instance.vmId, targetHost, cookie)
		except Exception, e:
			self.log.exception('migrateVm failed')
			raise
		#instance = self.data.acquireInstance(instance.id)
		#try:
		#	instance.hostId = targetHost.id
		#finally:
		#	self.data.releaseInstance(instance)
		try:
			# Notify the target
			self.log.info("migrateVm: receiveVm function will be executed on the target host")
			vmId = self.proxy[targetHost.name].receiveVm(instance, cookie)
		except Exception, e:
			self.log.exception('receiveVm failed')
			raise
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		target_host_fqdn = getFqdn(targetHost.name)
		source_host_fqdn = getFqdn(sourceHost.name)
		self.monitoringSensor.publishAction("VM %s migrated from host to %s host %s" % (vm_fqdn, source_host_fqdn, target_host_fqdn), instance.userId, vm_fqdn)
		return
	
	def pauseVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Running, InstanceState.Pausing)
		self.data.releaseInstance(instance)
		hostname = self.data.getHost(instance.hostId).name
		try:
			self.proxy[hostname].pauseVm(instance.vmId)
		except Exception:
			self.log.exception('pauseVm failed on host %s with vmId %d' % (hostname, instance.vmId))
			raise
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Pausing, InstanceState.Paused)
		self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		host_fqdn = getFqdn(hostname)
		self.monitoringSensor.publishAction("VM %s on host %s paused" % (vm_fqdn, host_fqdn), instance.userId, vm_fqdn)
		return

	def unpauseVm(self, instanceId):
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Paused, InstanceState.Unpausing)
		self.data.releaseInstance(instance)
		hostname = self.data.getHost(instance.hostId).name
		try:
			self.proxy[hostname].unpauseVm(instance.vmId)
		except Exception:
			self.log.exception('unpauseVm failed on host %s with vmId %d' % (hostname, instance.vmId))
			raise
		instance = self.data.acquireInstance(instanceId)
		self.stateTransition(instance, InstanceState.Unpausing, InstanceState.Running)
		self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		host_fqdn = getFqdn(hostname)
		self.monitoringSensor.publishAction("VM %s on host %s unpaused" % (vm_fqdn, host_fqdn), instance.userId, vm_fqdn)
		return
	
	def getHosts(self):
		return self.data.getHosts().values()
	
	def getNetworks(self):
		return self.data.getNetworks().values()
	
	def getUsers(self):
		return self.data.getUsers().values()
	
	def getInstances(self):
		return self.data.getInstances().values()
	
	def vmmSpecificCall(self, instanceId, arg):
		instance = self.data.getInstance(instanceId)
		hostname = self.data.getHost(instance.hostId).name
		try:
			res = self.proxy[hostname].vmmSpecificCall(instance.vmId, arg)
		except Exception:
			self.log.exception('vmmSpecificCall failed on host %s with vmId %d' % (hostname, instance.vmId))
			raise
		return res
	
#	@timed
	def registerNodeManager(self, host, instances):
		"""Called by the NM every so often as a keep-alive/state polling -- state changes here are NOT AUTHORITATIVE"""
		if (host.id == None):
			hostList = [h for h in self.data.getHosts().itervalues() if h.name == host.name]
			if (len(hostList) != 1):
				raise TashiException(d={'errno':Errors.NoSuchHost, 'msg':'A host with name %s is not identifiable' % (host.name)})
			host.id = hostList[0].id
		oldHost = self.data.acquireHost(host.id)
		if (oldHost.name != host.name):
			self.data.releaseHost(oldHost)
			raise TashiException(d={'errno':Errors.NoSuchHostId, 'msg':'Host id and hostname mismatch'})
		try:
			try:
				self.lastContacted[host.id] = time.time()
				oldHost.version = host.version
				oldHost.memory = host.memory
				oldHost.cores = host.cores
				oldHost.up = True
				oldHost.decayed = False
				if (host.version != version and not self.allowMismatchedVersions):
					oldHost.state = HostState.VersionMismatch
				if (host.version == version and oldHost.state == HostState.VersionMismatch):
					oldHost.state = HostState.Normal
				for instance in instances:
					try:
						oldInstance = self.data.acquireInstance(instance.id)
					except TashiException, e:
						if (e.errno == Errors.NoSuchInstanceId):
							self.log.info('Host %s reported an instance %d that did not previously exist (decay)' % (host.name, instance.id))
							oldHost.decayed = True
							continue
							#oldInstance = self.data.registerInstance(instance)
						else:
							raise
					try:
						if (oldInstance.hostId != host.id):
							self.log.info('Host %s is claiming instance %d actually owned by hostId %s (decay)' % (host.name, oldInstance.id, str(oldInstance.hostId)))
							oldHost.decayed = True
							continue
						oldInstance.decayed = (oldInstance.state != instance.state)
						self.updateDecay(self.decayedInstances, oldInstance)
						if (oldInstance.decayed):
							self.log.info('State reported as %s instead of %s for instance %d on host %s (decay)' % (vmStates[instance.state], vmStates[oldInstance.state], instance.id, host.name))
					finally:
						self.data.releaseInstance(oldInstance)
				instanceIds = [instance.id for instance in instances]
				for instance in self.data.getInstances().itervalues():
					if instance.hostId == host.id:
						if instance.id not in instanceIds:
							if instance.state == InstanceState.PreparingDisk:
								continue
							self.log.info('instance %d was not reported by host %s as expected (decay)' % (instance.id, host.name))
							instance.decayed = True
							self.updateDecay(self.decayedInstances, instance)
							oldHost.decayed = True
			except Exception, e:
				oldHost.decayed = True
				raise
		finally:
			self.updateDecay(self.decayedHosts, oldHost)
			self.data.releaseHost(oldHost)
		return host.id
	
	def vmUpdate(self, instanceId, instance, oldState):
		try:
			oldInstance = self.data.acquireInstance(instanceId)
		except TashiException, e:
			if (e.errno == Errors.NoSuchInstanceId):
				self.log.exception('Got vmUpdate for unknown instanceId %d' % (instanceId))
				return
			else:
				raise
		if (instance.state == InstanceState.Exited):
			oldInstance.decayed = False
			self.updateDecay(self.decayedInstances, oldInstance)
			hostname = self.data.getHost(oldInstance.hostId).name
			if (oldInstance.state not in [InstanceState.ShuttingDown, InstanceState.Destroying, InstanceState.Suspending]):
				self.log.warning('Unexpected exit on %s of instance %d (vmId %d)' % (hostname, instanceId, oldInstance.vmId))
			if (oldInstance.state == InstanceState.Suspending):
				self.stateTransition(oldInstance, InstanceState.Suspending, InstanceState.Suspended)
				oldInstance.hostId = None
				oldInstance.vmId = None
				self.data.releaseInstance(oldInstance)
			else:
				self.data.removeInstance(oldInstance)
		else:
			if (instance.state):
				if (oldState and oldInstance.state != oldState):
					self.log.warning('Got vmUpdate of state from %s to %s, but the instance was previously %s' % (vmStates[oldState], vmStates[instance.state], vmStates[oldInstance.state]))
				oldInstance.state = instance.state
			if (instance.vmId):
				oldInstance.vmId = instance.vmId
			if (instance.hostId):
				oldInstance.hostId = instance.hostId
			if (instance.nics):
				for nic in instance.nics:
					if (nic.ip):
						for oldNic in oldInstance.nics:
							if (oldNic.mac == nic.mac):
								oldNic.ip = nic.ip
			oldInstance.decayed = False
			self.updateDecay(self.decayedInstances, oldInstance)
			self.data.releaseInstance(oldInstance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(oldInstance.name)
		try:
			host_fqdn = getFqdn(self.data.getHost(instance.hostId).name)
		except Exception, e:
			host_fqdn = None
		self.monitoringSensor.publishAction("VM %s on host %s changed state to: %s" % (vm_fqdn, host_fqdn, vmStates[oldInstance.state]), oldInstance.userId, vm_fqdn)
		return
	
	def activateVm(self, instanceId, host):
		dataHost = self.data.acquireHost(host.id)
		if (dataHost.name != host.name):
			self.data.releaseHost(dataHost)
			raise TashiException(d={'errno':Errors.HostNameMismatch,'msg':"Mismatched target host"})
		if (not dataHost.up):
			self.data.releaseHost(dataHost)
			raise TashiException(d={'errno':Errors.HostNotUp,'msg':"Target host is not up"})
		if (dataHost.state != HostState.Normal):
			self.data.releaseHost(dataHost)
			raise TashiException(d={'errno':Errors.HostStateError,'msg':"Target host state is not normal"})
		self.data.releaseHost(dataHost)
		instance = self.data.acquireInstance(instanceId)
		if ('__resume_source' in instance.hints):
			self.stateTransition(instance, InstanceState.Pending, InstanceState.Resuming)
		else:
			self.stateTransition(instance, InstanceState.Pending, InstanceState.Activating)
		instance.hostId = host.id
		self.data.releaseInstance(instance)
		try:
			if ('__resume_source' in instance.hints):
				vmId = self.proxy[host.name].resumeVm(instance, instance.hints['__resume_source'])
			else:
				# prepare new image if needed:
				copyingDiskNeeded = False
				for disk in instance.disks:
					if disk.persistent:
						copyingDiskNeeded = True
				if copyingDiskNeeded:
					instance = self.data.acquireInstance(instanceId)
					self.stateTransition(instance, InstanceState.Activating, InstanceState.PreparingDisk)
					self.data.releaseInstance(instance) # save new state, needed for example in registerNodeManager
					vm_fqdn = getFqdn(instance.name)
					self.monitoringSensor.publishAction("Disk will be prepared for a VM %s. This may take some time." % vm_fqdn, instance.userId, vm_fqdn)
					self.monitoringSensor.publishVmLayout()
					instance = self.data.acquireInstance(instanceId)
					self.dfs.createNewDisks(instance)
					self.stateTransition(instance, InstanceState.PreparingDisk, InstanceState.Activating)
					self.data.releaseInstance(instance)
					self.monitoringSensor.publishAction("Disk is now prepared for a VM %s." % vm_fqdn, instance.userId, vm_fqdn)
					self.monitoringSensor.publishVmLayout()
				vmId = self.proxy[host.name].instantiateVm(instance)
		except Exception, e:
			instance = self.data.acquireInstance(instanceId)
			if (instance.state is InstanceState.Destroying): # Special case for if destroyVm is called during initialization and initialization fails
				self.data.removeInstance(instance)
			else:
				self.stateTransition(instance, None, InstanceState.Held)
				instance.hostId = None
				self.data.releaseInstance(instance)
			raise
		instance = self.data.acquireInstance(instanceId)
		instance.vmId = vmId
		if (instance.state is InstanceState.Destroying): # Special case for if destroyVm is called during initialization
			self.data.releaseInstance(instance)
			try:
				self.proxy[host.name].destroyVm(vmId)
			except Exception:
				self.log.exception('destroyVm failed for host %s vmId %d' % (host.name, instance.vmId))
				raise
		else:
			if ('__resume_source' not in instance.hints):
				self.stateTransition(instance, InstanceState.Activating, InstanceState.Running)
			self.data.releaseInstance(instance)
		self.monitoringSensor.publishVmLayout()
		vm_fqdn = getFqdn(instance.name)
		host_fqdn = getFqdn(host.name)
		self.monitoringSensor.publishAction("VM %s on host %s started" % (vm_fqdn, host_fqdn), instance.userId, vm_fqdn)
		return
	
	def adaptCpuShare(self, instanceId, share):
		instance = self.data.acquireInstance(instanceId)
		hostname = self.data.getHost(instance.hostId).name
		try:
			status = self.proxy[hostname].adaptCpuShare(instance.vmId, share)
			if status == 0:
				instance.cpuShare = share
				self.monitoringSensor.publishVmLayout()
				vm_fqdn = getFqdn(instance.name)
				host_fqdn = getFqdn(hostname)
				self.monitoringSensor.publishAction("CPU share for a VM %s on host %s adapted to %s" % (vm_fqdn, host_fqdn, share), instance.userId, vm_fqdn)
			else:
				self.log.error("There were problems setting CPU share for instance %s" % instanceId)
		except Exception:
			self.log.exception('adaptCpuShare failed for host %s vmId %d' % (instance.name, instanceId))
			raise
		self.data.releaseInstance(instance)
		return
	
	def adaptMemoryLimit(self, instanceId, limit):
		instance = self.data.acquireInstance(instanceId)
		hostname = self.data.getHost(instance.hostId).name
		try:
			self.proxy[hostname].adaptMemoryLimit(instance.vmId, limit)
			instance.memory = limit
			self.monitoringSensor.publishVmLayout()
			vm_fqdn = getFqdn(instance.name)
			host_fqdn = getFqdn(hostname)
			self.monitoringSensor.publishAction("Memory for a VM %s on host %s adapted to %s" % (vm_fqdn, host_fqdn, limit), instance.userId, vm_fqdn)
		except Exception:
			self.log.exception('adaptMemoryLimit failed for host %s vmId %d' % (instance.name, instanceId))
			raise
		self.data.releaseInstance(instance)
		return
	
	def adaptNetworkBandwidth(self, instanceId, downspeed, upspeed):
		instance = self.data.acquireInstance(instanceId)
		hostname = self.data.getHost(instance.hostId).name
		try:
			status = self.proxy[hostname].adaptNetworkBandwidth(instance.vmId, downspeed, upspeed)
			if status == 0:
				instance.downspeed = downspeed
				instance.upspeed = upspeed
				self.monitoringSensor.publishVmLayout()
				vm_fqdn = getFqdn(instance.name)
				host_fqdn = getFqdn(hostname)
				self.monitoringSensor.publishAction("Network downspeed for a VM %s on host %s adapted to %s, upspeed to %s" % (vm_fqdn, host_fqdn, downspeed, upspeed), instance.userId, vm_fqdn)
			else:
				self.log.error("There were problems setting network bandwidth for at least one of the interfaces of the instance %s" % instanceId)
		except Exception:
			self.log.exception('adaptNetworkBandwidth failed for host %s vmId %d' % (instance.name, instanceId))
			raise
		self.data.releaseInstance(instance)
		return
	
	def adaptDiskBandwidth(self, instanceId, weight):
		instance = self.data.acquireInstance(instanceId)
		hostname = self.data.getHost(instance.hostId).name
		try:
			status = self.proxy[hostname].adaptDiskBandwidth(instance.vmId, weight)
			if status == 0:
				instance.diskBandwidthWeight = weight
				self.monitoringSensor.publishVmLayout()
				vm_fqdn = getFqdn(instance.name)
				host_fqdn = getFqdn(hostname)
				self.monitoringSensor.publishAction("Disk bandwidth share for a VM %s on host %s adapted to %s" % (vm_fqdn, host_fqdn, weight), instance.userId, vm_fqdn)
			else:
				self.log.error("There were problems setting disk bandwidth for instance %s" % instanceId)
		except Exception:
			self.log.exception('adaptDiskBandwidth failed for host %s vmId %d' % (instance.name, instanceId))
			raise
		self.data.releaseInstance(instance)
		return
	
	def registerUser(self, name, passwd, priority):
		id = self.data.registerUser(name, passwd, priority)
		self.log.info("A new user with id %s registered." % id)
		self.monitoringSensor.publishVmLayout()
		self.monitoringSensor.publishAction("A new user with id %s registered." % id, "admin", id)
		return id
	
	def unregisterUser(self, userId):
		id = self.data.unregisterUser(userId)
		self.log.info("A user with id %s unregistered." % userId)
		self.monitoringSensor.publishVmLayout()
		self.monitoringSensor.publishAction("A user with id %s unregistered." % id, "admin", id)
		return
	
	def registerHost(self, hostname, memory, cores, cpuSpeed, version):
		hostId, alreadyRegistered = self.data.registerHost(hostname, memory, cores, cpuSpeed, version)
		if alreadyRegistered:
			self.log.info("Host %s is already registered, it was updated now" % hostname)
		else:
			self.log.info("A host was registered - hostname: %s, version: %s, memory: %s, cores: %s, cpuSpeed: %s" % (hostname, version, memory, cores, cpuSpeed))
			self.monitoringSensor.publishVmLayout()
			host_fqdn = getFqdn(hostname)
			self.monitoringSensor.publishAction("Host %s registered" % host_fqdn, "admin", host_fqdn)
		return hostId
	
	def unregisterHost(self, hostId):
		host = self.data.getHost(hostId)
		self.data.unregisterHost(hostId)
		self.log.info("Host %s was unregistered" % hostId)
		self.monitoringSensor.publishVmLayout()
		host_fqdn = getFqdn(host.name)
		self.monitoringSensor.publishAction("Host %s unregistered" % host_fqdn, "admin", host_fqdn)
		return
	
	def powerOnHost(self, hostId):
		host = self.data.acquireHost(hostId)
		self.log.info("Host %s will be powered on" % hostId)
		#todo: power on host
		#todo: wait for the host to be on and nodemanager to be running
		host.poweredOn = True
		self.log.info("Host %s is on" % hostId)
		self.data.releaseHost(host)
		self.monitoringSensor.publishVmLayout()
		host_fqdn = getFqdn(host.name)
		self.monitoringSensor.publishAction("Host %s was powered on" % host_fqdn, "admin", host_fqdn)
		return
		
	def powerOffHost(self, hostId):
		host = self.data.acquireHost(hostId)
		self.log.info("Host %s will be powered off" % hostId)
		#todo: power off host
		#todo: wait for the host to be off
		host.poweredOn = False
		self.log.info("Host %s is powered off" % hostId)
		self.data.releaseHost(host)
		self.monitoringSensor.publishVmLayout()
		host_fqdn = getFqdn(host.name)
		self.monitoringSensor.publishAction("Host %s was powered off" % host_fqdn, "admin", host_fqdn)
		return
		
	def setHostConfig(self, hostId, elemName, value):
		host = self.data.acquireHost(hostId)
		if elemName in host.__dict__.keys():
			setattr(host, elemName, value)
		self.data.releaseHost(host)
		
	def getClusterConfig(self):
		return self.monitoringSensor.getClusterConfig()
		
	def getVmLayout(self):
		return self.monitoringSensor.createVmLayout()
		
		
		
		
		
		