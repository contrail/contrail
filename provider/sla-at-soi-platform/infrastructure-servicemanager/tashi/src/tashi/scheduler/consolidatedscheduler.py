# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 304 $
# @lastrevision $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/priorityscheduler.py $

import ConfigParser
import sys
import xmpp
import time 
import logging

from tashi.rpycservices.rpyctypes import HostState, InstanceState
from schedulercommon import SchedulerCommon
from tashi.util import boolean

class ConsolidatedScheduler(SchedulerCommon):
    """
    When Vms are provisioned, each physical host is filled to its capacity before moving 
    to the next one. Hosts with highest efficiency rating are filled first. VMs priorities
    come into account when an unsuccessful provisioning request happens - in this case 
    and if provisioning request is of a higher priority - resources are taken from the VMs 
    with the lowest priority to provide resources for a request. When some resources are released 
    the VMs with the highest priority are migrated to better hosts if possible. When rescheduling 
    is executed the scheduler tries to consolidate VMs distribution across the servers. In general
    too many migrations are avoided, because migrations besides other effects also require 
    two VMs instead of one (source and destination VM) for the time of migration.
    """
    def __init__(self, client, config, schedulerConf):
        SchedulerCommon.__init__(self, client, config, schedulerConf)

    def determineHost(self, hosts, load, instances, inst, targetHost, allowElsewhere):
        """
        This function determines the most efficient server that has enough resources. It 
        powers on an appropriate host if the guaranteed servers are not powered on.
        """
        self.vmPriorities[inst.id] = self.getVmPriority(inst.userId)
        minMaxHost = None
        if (targetHost != None):
            minMaxHost = self.checkTargetHost(hosts, load, instances, inst, targetHost)
        if ((targetHost == None or allowElsewhere) and minMaxHost == None):
            availableHost = -1
            # get hosts with the highest efficiency first:
            sHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k], reverse=True)
            for hsId in sHosts:
                h = hosts[hsId]
                if self.isHostAppropriate(inst, instances, h, load, False) and len(h.reserved) == 0:
                    msg = "Vm can be provisioned on a host with id (sorted by efficiency rating): %s" % h.id
                    self.log.info(msg)
                    availableHost = h.id
                    break 
            if availableHost > -1:
                hostId = availableHost     
                self.log.info("Instance %s will be provisioned on host %s" % (inst.id, hostId))
                minMaxHost = hosts[hostId]
            else: 
                if not self.guaranteedServersOn(hosts):
                    pHost = self.powerOnAppropriateHost(hosts, inst, instances, load)
                    if pHost != None:
                        minMaxHost = pHost
                    else:
                        self.log.info("There are no resources available for provisioning instance %s - over-provisioning will be checked or some host will be powered on if available" % inst.id) 
        return minMaxHost
        
    def reschedule(self, instances, hosts, load):
        """
        This function checks if distribution of instances across servers complies to the
        Consolidated scheduler type and triggers migrations if needed to comply to this scheduler 
        type. Be aware that this does not take into account VM priorities, because for this
        too many migrations would be needed in some cases. Only the hosts that are powered on are
        used for rescheduling.
        """
        hostsFreeMem = {}
        hostsFreeCores = {}
        hostsFreeCpu = {}
        hostsUsage = {}
        for h in hosts.values():
            if (h.poweredOn == True and h.up == True and h.state == HostState.Normal):
                memUsage = reduce(lambda x, y: x + instances[y].memory, load[h.id], 0)
                coreUsage = reduce(lambda x, y: x + instances[y].cores, load[h.id], 0)
                cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, load[h.id], 0)
                hostsFreeMem[h.id] = h.memory - memUsage
                hostsFreeCores[h.id] = self.cpuCoresOverRate * h.cores - coreUsage
                hostsFreeCpu[h.id] = self.cpuSpeedOverRate * h.cpuSpeed - cpuUsage
                hostsUsage[h.id] = self.getLoad(memUsage, coreUsage, cpuUsage)
                
        if len(hostsUsage) == 0:
            return
        
        targetHosts = []
        # hosts with the smallest load / usage will be attempted to be released first:
        # or
        # hosts with the smallest efficiency:
        if(self.optimalConsolidation):
            sHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k])
        else:
            sHosts = sorted(hostsUsage.keys(), key=lambda k: hostsUsage[k]) 
            
        # filter our hosts which are not powered on:
        sHosts = filter(lambda x : hosts[x].poweredOn == True, sHosts)
            
        for hId in sHosts:
            # continue if host doesn't have VMs and if optimalConsolidation is disabled:
            if hostsUsage[hId] == 0 and not self.optimalConsolidation:
                continue
            if hId in targetHosts:
                self.log.info("Host %s is already used as a target host for some migrations and it won't be considered as a host which can be released" % (hId))
                continue
            # sort hosts to be hosts with the heaviest load / usage at the beginning of the list -
            # that way the instances from host hId will be attempted to move to the hosts which already 
            # have a heavy load, which should prevent a potential never ending migration loops that 
            # could happen if instances would be attempted to be moved to the hosts with the most
            # free resources (which can be the same hosts that have the smallest load)
            # or:
            # sort hosts to be the most efficient hosts first (optimal but expensive rescheduling)
            sortedHosts = reversed(sHosts) 
            num = 0 # number of instances from host hId that already have a potential target host 
            toMigrate = [] # [[vmId1, targetHostId1], [v2, t2]] - collecting VMs that can be migrated
            for hostTarget in sortedHosts: # check on which host the instances can be migrated
                if hostTarget == hId or (hostsUsage[hostTarget] == 0 and not self.optimalConsolidation):
                    continue
                self.log.info("Host %s has the following load: %s" % (hostTarget, hostsUsage[hostTarget]))
                for iId in load[hId]:
                    i = instances[iId]
                    if i.memory <= hostsFreeMem[hostTarget] and i.cores <= hostsFreeCores[hostTarget] and i.cpuShare <= hostsFreeCpu[hostTarget]:
                        self.log.info("Instance %s can be potentially migrated on %s" % (iId, hostTarget))
                        toMigrate.append([iId, hostTarget])
                        hostsFreeMem[hostTarget] -= i.memory
                        hostsFreeCores[hostTarget] -= i.cores
                        hostsFreeCpu[hostTarget] -= i.cpuShare
                        num += 1
                if num == len(load[hId]) and num != 0:
                    self.log.info("All instances on host %s can be migrated to other hosts and host %s can be released" % (hId, hId))
                    break
            if len(toMigrate) == 0:
                self.log.info("No migrations to be executed.")
                return
            self.log.info("%s from %s instances on host %s have a potential target host" % (num, len(load[hId]), hId))
            if num < len(load[hId]):
                self.log.info("NOT all instances on host %s have a potential target host - that means that host %s cannot be fully released. Migrations won't be executed." % (hId, hId))
            else:
                self.log.info("Migrations will be executed and host %s will be released" % hId)
                for i in toMigrate:
                    self.toMigrate.append(i)
                    targetHosts.append(i[1])
                    
