# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 898 $
# @lastrevision $Date: 2011-03-08 08:59:14 +0100 (Tue, 08 Mar 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/schedulerinterface.py $

from tashi.util import boolean

class SchedulerConf(object):

    def __init__(self):
        print "initializing scheduler configuration object"
   
    def setFromArgv(self, argv):
        self.schedulerImpl = argv[1]
        self.scheduleDelay = float(argv[2])
        self.rescheduleAfterStart = boolean(argv[3])
        self.cpuCoresOverRate = boolean(argv[4])
        self.cpuSpeedOverRate = boolean(argv[5])
        self.loadExpression = self.checkexpr(argv[6])
        self.autoMigrations = argv[7]
        self.guaranteedServers = argv[8]
        self.optimalConsolidation = argv[9]
        self.minimalVmMemory = argv[10]
        self.autoPowering = argv[11]
        
    def setFromConfig(self, config):
        self.schedulerImpl = config.get("Scheduler", "scheduler")
        self.scheduleDelay = float(config.get("Scheduler", "scheduleDelay"))
        self.rescheduleAfterStart = boolean(config.get("Scheduler", "rescheduleAfterStart")) 
        self.cpuCoresOverRate = int(config.get("Scheduler", "cpuCoresOverRate")) 
        self.cpuSpeedOverRate = int(config.get("Scheduler", "cpuSpeedOverRate")) 
        self.loadExpression = self.checkexpr(config.get("Scheduler", "loadExpression"))
        self.autoMigrations = boolean(config.get("Scheduler", "autoMigrations"))
        self.guaranteedServers = int(config.get("Scheduler", "guaranteedServers")) 
        self.optimalConsolidation = boolean(config.get("Scheduler", "optimalConsolidation"))
        self.minimalVmMemory = int(config.get("Scheduler", "minimalVmMemory"))  
        self.autoPowering = boolean(config.get("Scheduler", "autoPowering"))
        
    def setFromParams(self, scheduler, scheduleDelay, rescheduleAfterStart, cpuCoresOverRate,\
                      cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers,\
                      optimalConsolidation, minimalVmMemory, autoPowering):
        self.schedulerImpl = scheduler
        self.scheduleDelay = scheduleDelay
        self.rescheduleAfterStart = rescheduleAfterStart 
        self.cpuCoresOverRate = cpuCoresOverRate 
        self.cpuSpeedOverRate = cpuSpeedOverRate 
        self.loadExpression = loadExpression
        self.autoMigrations = autoMigrations
        self.guaranteedServers = guaranteedServers
        self.optimalConsolidation = optimalConsolidation
        self.minimalVmMemory = minimalVmMemory  
        self.autoPowering = autoPowering
        
    def checkexpr(self, expr):
        try:
            # replace placeholders with dummy values:
            r = expr.replace("memUsage", str(1)).replace("coresUsage", str(1)).replace("cpuUsage", str(1))
            eval(r)
            return expr
        except:
            print "Reading loadExpression failed. Default value will be used (memUsage + coresUsage * 512 + cpuUsage)."
            return "memUsage + coresUsage * 512 + cpuUsage"
    
    