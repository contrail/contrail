# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 304 $
# @lastrevision $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/priorityscheduler.py $

import ConfigParser
import sys
import time 
import logging

from tashi.rpycservices.rpyctypes import HostState, InstanceState
from schedulerinterface import SchedulerInterface
from tashi.util import boolean

class SchedulerCommon(SchedulerInterface):
    """
    This class is for common functionality for schedulers that take into account 
    VM priorities and server efficiency rating. It should be used only as parent class
    of actual scheduler implementations.
    """
    def __init__(self, client, config, schedulerConf):
        self.client = client
        self.config = config
        f = "%(asctime)s - %(levelname)s - %(message)s"
        logging.basicConfig(format=f,level=logging.INFO)
        self.log = logging.getLogger(__file__)

        self.vmPriorities = {}
        self.fillVmPriorities()
        self.serversEfficiency = {}
        self.fillServersEfficiency()
        
        self.cpuCoresOverRate = schedulerConf.cpuCoresOverRate
        self.cpuSpeedOverRate = schedulerConf.cpuSpeedOverRate
        self.loadExpression = schedulerConf.loadExpression
        self.guaranteedServers = schedulerConf.guaranteedServers
        self.optimalConsolidation = schedulerConf.optimalConsolidation
        self.minimalVmMemory = schedulerConf.minimalVmMemory
        self.autoPowering = schedulerConf.autoPowering
        self.toMigrate = [] # contains VMs to be migrated, last VM will be migrated first: [[vmId1, targetHostId1], [vmId2, targetHostId2]]
        self.migratedVm = ""
        self.adaptedMem = {} # vmId : adaptedMem amount
        self.adaptedCpu = {} # vmId : adaptedCpu amount
        self.suspendedVms = []
    
    def getLoad(self, memoryUsage, coresUsage, cpuUsage):
        """
        Returns integer that indicates load / usage of VM or host.
        """
        expr = self.loadExpression.replace("memUsage", str(memoryUsage)).\
            replace("coresUsage", str(coresUsage)).replace("cpuUsage", str(cpuUsage))
        load = eval(expr)
        return load
    
    def getVmPriority(self, userId):
        users = self.client.getUsers()
        return filter(lambda x : x.id == userId, users)[0].priority
    
    def fillVmPriorities(self):
        instances = self.client.getInstances()
        for i in instances:
            self.vmPriorities[i.id] = self.getVmPriority(i.userId)
            
    def fillServersEfficiency(self):
        hosts = []
        for h in self.client.getHosts():
            hosts.append(h.id)
        for (name, value) in self.config.items("Servers"):
            if int(name) in hosts:
                self.serversEfficiency[int(name)] = int(value)
        for hId in hosts:
            if hId not in self.serversEfficiency:
                raise ValueError, "Host %s does not have efficiency rating specified" % hId
            
    def getUpdatedInstances(self):
        _instances = self.client.getInstances()
        instances = {}
        for i in _instances:
            instances[i.id] = i
        return instances    
            
    def getUsageByMetric(self, inst, instances, load, hostId):
        # don't count VMs that are suspended or suspending:
        filteredVms = filter(lambda x : not (instances[x].state == InstanceState.Suspended or instances[x].state == InstanceState.Suspending), load[hostId])
        memUsage = reduce(lambda x, y: x + instances[y].memory, filteredVms, inst.memory)
        coreUsage = reduce(lambda x, y: x + instances[y].cores, filteredVms, inst.cores)
        cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, filteredVms, inst.cpuShare)
        return memUsage, coreUsage, cpuUsage
    
    def isHostAppropriate(self, inst, instances, host, load, usingOverprovisioning = True):
        isAppropriate = self.checkHost(host, inst, instances, load) and host.poweredOn == True and host.up == True and host.state == HostState.Normal
        if not isAppropriate:
            return False
        memUsage, coreUsage, cpuUsage = self.getUsageByMetric(inst, instances, load, host.id)
        self.log.info("Host %s would have %s memory, %s CPU cores and %s CPU speed usage." % (host.id, memUsage, coreUsage, cpuUsage))
        self.log.info("Available resources on host %s: %s memory, %s CPU cores, %s CPU speed " % (host.id, host.memory, host.cores, host.cpuSpeed))
        self.log.info("Available resources on host %s (using over-provisioning): %s memory, %s CPU cores, %s CPU speed " % (host.id, host.memory, self.cpuCoresOverRate * host.cores, self.cpuSpeedOverRate * host.cpuSpeed))
        if usingOverprovisioning:
            availableCores = self.cpuCoresOverRate * host.cores
            availableCpu = self.cpuSpeedOverRate * host.cpuSpeed
        else:
            availableCores = host.cores
            availableCpu = host.cpuSpeed
        if (memUsage <= host.memory and coreUsage <= availableCores and cpuUsage <= availableCpu):
            return True
        else:
            return False
        
    def guaranteedServersOn(self, hosts):
        """
        Returns True if at least the guaranteed number of servers are powered on. 
        """
        poweredOnHosts = 0
        poweredOffHosts = []
        for h in hosts.values():
            if h.poweredOn == True:
                poweredOnHosts += 1
            else:
                poweredOffHosts.append(h.id)
        self.log.info("Hosts that are currently powered off: %s" % poweredOffHosts)
        return poweredOnHosts >= self.guaranteedServers
        
    def checkHost(self, host, inst, instances, load):
        # inst.ccr means if inst can be started in CCR, host.ccr means if host is in CCR
        if(inst.ccr != None and (not inst.ccr and host.ccr)):
            self.log.info("Instance %s cannot be provisioned on host %s because of CCR incompatibility" % (inst.id, host.id))
            return False
        elif(inst.auditability != None and (inst.auditability and not host.auditability)):
            self.log.info("Instance %s cannot be provisioned on host %s because of auditability incompatibility" % (inst.id, host.id))
            return False
        elif(inst.location != None and (inst.location.lower() != host.location.lower())):
            self.log.info("Instance %s cannot be provisioned on host %s because of location incompatibility" % (inst.id, host.id))
            return False
        elif(inst.sas70 != None and (inst.sas70 and not host.sas70)):
            self.log.info("Instance %s cannot be provisioned on host %s because of SAS70 incompatibility" % (inst.id, host.id))
            return False
        elif(inst.dataClass != None and (inst.dataClass > host.dataClass)):
            self.log.info("Instance %s cannot be provisioned on host %s because of data classification incompatibility" % (inst.id, host.id))
            return False
        elif(inst.hwLevel != None and (inst.hwLevel > host.hwLevel)):
            self.log.info("Instance %s cannot be provisioned on host %s because of hardware redundancy level incompatibility" % (inst.id, host.id))
            return False
        elif(inst.diskThroughput != None and (inst.diskThroughput > host.diskThroughput)):
            self.log.info("Instance %s cannot be provisioned on host %s because of disk throughput level incompatibility" % (inst.id, host.id))
            return False
        elif(inst.netThroughput != None and (inst.netThroughput > host.netThroughput)):
            self.log.info("Instance %s cannot be provisioned on host %s because of net throughput level incompatibility" % (inst.id, host.id))
            return False
        elif(inst.isolation):
            for id in load[host.id]:
                if instances[id].groupName == inst.groupName:
                    self.log.info("Instance %s cannot be provisioned on host %s because of isolation incompatibility" % (inst.id, host.id))
                    return False
        else:
            return True
    
    def checkTargetHost(self, hosts, load, instances, inst, targetHost):
        # todo: should we check the reserved instances on a host when calculating
        # how much resources are free on a host (when choosing host for a VM)?
        minMaxHost = None
        self.log.info("Provisioning request has a target host hint: %s" % targetHost)
        for h in hosts.values():
            if ((str(h.id) == targetHost or h.name == targetHost)):
                poweredOn = False
                if not (h.poweredOn == True and h.up == True and h.state == HostState.Normal):
                    poweredOn = self.powerOnHost(h)
                if poweredOn and len(h.reserved) == 0 and self.isHostAppropriate(inst, instances, h, load, True):
                    self.log.info("Instance %s can be provisioned on host %s" % (inst.id, h.id))
                    minMaxHost = h  
                # If a host machine is reserved, only allow if userId is in reserved list
                if poweredOn and len(h.reserved) > 0 and inst.userId in h.reserved:
                    if self.isHostAppropriate(inst, instances, h, load, True):
                        self.log.info("Instance %s can be provisioned on host %s" % (inst.id, h.id))
                        minMaxHost = h
        if minMaxHost != None:
            self.log.info("VM can be provisioned on a target host %s" % targetHost)
        return minMaxHost
        
    def getMostSuitableHost(self, hosts, load, instances, inst):
        """
        Finds the most suitable host if no host has enough resources without 
        over-provisioning CPU resources.
        """
        if self.cpuCoresOverRate == 1 and self.cpuSpeedOverRate == 1:
            return
        self.log.info("CPU over-provisioning possibilities will be checked")
        hostsOverLoad = {}
        for hId in hosts.keys():
            hostsOverLoad[hId] = self.getLoad(0, \
            max(reduce(lambda x, y: x + instances[y].cores, load[hId], inst.cores) - hosts[hId].cores, 0),\
            max(reduce(lambda x, y: x + instances[y].cpuShare, load[hId], inst.cpuShare) - hosts[hId].cpuSpeed, 0))
        self.log.info("Hosts and their over-provisioning load would be: %s" % hostsOverLoad)
        # hosts with the smallest over-provisioning load first
        sortedHosts = sorted(hosts.keys(), key = lambda k : hostsOverLoad[k])
        for hostId in sortedHosts:
            h = hosts[hostId]
            if self.isHostAppropriate(inst, instances, h, load):
                self.log.info("Instance %s can be provisioned on host %s" % (inst.id, hostId))
                return h
            
    def setVmsForMigration(self, hosts, instances, freedHostId, load):
        """
        Determines the VMs that can be migrated to more efficient host because some resources
        were released on this host. Only VMs that are on the hosts with lower efficiency rating
        towards the efficiency rating of the host were some resources were released are checked.
        VMs with the highest priority are chosen. These migrations are not necessary, but 
        since the resources are not to be released to frequently, this migrations shouldn't 
        be harmful and they could bring more appropriate distribution of VMs across the hosts 
        according to VM priorities.
        """ 
        # Free memory and cores on the host where VM has exited
        freeMem = hosts[freedHostId].memory - reduce(lambda x, y: x + instances[y].memory, load[freedHostId], 0)
        freeCores = hosts[freedHostId].cores - reduce(lambda x, y: x + instances[y].cores, load[freedHostId], 0)
        freeCpu = hosts[freedHostId].cpuSpeed - reduce(lambda x, y: x + instances[y].cpuShare, load[freedHostId], 0)
        msg = "Host with id %s has %s memory available, %s cores available, %s cpuSpeed available" % (freedHostId, freeMem, freeCores, freeCpu)
        self.log.info(msg)
        msg = "Checking if some VMs can be migrated to host %s" % str(freedHostId)
        self.log.info(msg)
            
        migrationCandidates = [] # collect all VMs that are on the hosts with lower efficiency rating than the released host
        
        sHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k])
        for hsId in sHosts:
            h = hosts[hsId]
            # Check only VMs on the hosts that have lower rating than host where VM has exited
            if (h.id == freedHostId):
                break # after this only hosts with higher rating are left in for loop 
            msg = "Checking if some VMs from host %s can be migrated to a better host" % str(h.id)
            self.log.info(msg)
            for vmId in load[h.id]:
                if self.isHostAppropriate(instances[vmId], instances, hosts[freedHostId], load) and instances[vmId].state == InstanceState.Running:
                    migrationCandidates.append(vmId)
                    self.log.info("VM %s added to the candidates for a migration to the host %s" % (vmId, freedHostId))
            
        # check the VMs with the highest priority first:
        for vm in sorted(migrationCandidates, key=lambda k: self.vmPriorities[k], reverse=True):
            if (instances[vm].memory <= freeMem and instances[vm].cores <= freeCores and instances[vm].cpuShare <= freeCpu and instances[vm].state == InstanceState.Running):
                self.toMigrate.append([vm, freedHostId])
                msg = "Vm with id %s added to the toMigrate list" % str(vm)
                self.log.info(msg)
                freeMem = freeMem - instances[vm].memory
                freeCores = freeCores - instances[vm].cores
                freeCpu = freeCpu - instances[vm].cpuShare
                msg = "Vm with id %s on the host with id %s will be migrated to host %s" % (vm, instances[vm].hostId, freedHostId)
                self.log.info(msg)
                msg = "After migration host %s will have %s memory available, %s CPU cores and %s CPU speed available" % (freedHostId, freeMem, freeCores, freeCpu)
                self.log.info(msg)
                        
    def handleMigration(self, instances):
        """
        It triggers the next migration from toMigrate list. It returns True if there are further
        migrations to be executed, it returns False if there are no more migrations in toMigrate list.
        """
        if (len(self.toMigrate) == 0):
            return False
        if self.migratedVm != "":    
            msg = "VMs that was or is being migrated has status: %s" % str(instances[self.migratedVm].state)
            self.log.info(msg)
        # check if there is no migration currently running:
        if (self.migratedVm == "" or (instances[self.migratedVm].state == InstanceState.Running and instances[self.migratedVm].hostId == self.migrationTarget)):
            msg = "VMs to be migrated [vm id, target host id]: %s" % str(self.toMigrate)
            self.log.info(msg)
            # get next VM to be migrated:
            [vId, targetId] = self.toMigrate[len(self.toMigrate) - 1]
            self.migratedVm = vId
            self.migrationTarget = targetId
            self.toMigrate.pop()
            # todo: implement state checking
            try:
                msg = "Migration of the VM with id %s to the host with id %s will be started, time: %s" % (vId, targetId, time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
                self.log.info(msg)
                self.client.migrateVm(vId, targetId)
            except:
                msg = "Migration failed: %s" % sys.exc_info()[1]
                self.log.info(msg)
            if len(self.toMigrate) == 0:
                self.log.info("There are no more migrations to be executed")
                self.migratedVm = ""
                return False
            return True

    def freeUpResources(self, inst, instances, hosts, load):
        """
        This function is triggered only when unsuccessful provisioning happened because of lack of 
        resources. It takes some memory from VMs with smaller priority to prepare  
        resources for a new provisioning request with higher priority. If this is not 
        enough some VMs with a low priority are suspended.
        """
        self.log.info("freeUpResources: instance %s was not provisioned, some resources need to be freed up. Instance memory: %s, cores: %s" % (inst.id, inst.memory, inst.cores))
        hostsWithFreeCores = [] # contains hosts which have enough available cores for the instance 
        # that needs to be provisioned and info about available memory
        hostsFreeMem = {}
        hostsFreeCpu = {}
        hostsFreeCores = {}
 
        appropriateHosts = {}       
        for h in hosts.values():
            if h.up == True and h.state == HostState.Normal and self.checkHost(h, inst, instances, load):
                appropriateHosts[h.id] = h
        
        for h in appropriateHosts.values():
            memUsage = reduce(lambda x, y: x + instances[y].memory, load[h.id], 0)
            coreUsage = reduce(lambda x, y: x + instances[y].cores, load[h.id], 0)
            cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, load[h.id], 0)
            hostsFreeMem[h.id] = h.memory - memUsage
            hostsFreeCpu[h.id] = h.cpuSpeed * self.cpuSpeedOverRate - cpuUsage
            hostsFreeCores[h.id] = h.cores * self.cpuCoresOverRate - coreUsage
            if(inst.cores <= h.cores * self.cpuCoresOverRate - coreUsage):
                hostsWithFreeCores.append(h.id) 
                    
        # check only hosts with enough available cores - because we don't have a function to change the number of cores for a VM
        # check if some VM with low priority can be adapted
        quota = 64
        for i in range(1, 11): # make 10 iterations, each time increase the amount of adapted memory and CPU
            for hId in hostsWithFreeCores:
                diff = quota * i
                numOfMemAdaptedVmsNeeded = 0
                numOfCpuAdaptedVmsNeeded = 0
                vmSmallerPriority = filter(lambda x : self.vmPriorities[x] < self.vmPriorities[inst.id], load[hId])
                # check if all machines in vmSmallerPriority can be adapted:
                adaptableVms = filter(lambda x : instances[x].memory - diff >= self.minimalVmMemory, vmSmallerPriority)
                if len(adaptableVms) == 0:
                    continue
                self.log.info("VMs with smaller priority on host %s: %s" % (hId, vmSmallerPriority))
                self.log.info("VMs that can be adapted on host %s: %s (minimal memory amount for a VM is %s)" % (hId, adaptableVms, self.minimalVmMemory))
                if hostsFreeMem[hId] <= inst.memory:
                    self.log.info("On host %s the following amount of memory can be freed up: %s, that means %s free memory on this host" % (hId, len(adaptableVms) * diff, hostsFreeMem[hId] + len(adaptableVms) * diff))
                    numOfMemAdaptedVmsNeeded = (abs(hostsFreeMem[hId] - inst.memory) / diff) + 1
                    if abs(hostsFreeMem[hId] - inst.memory) % diff == 0:
                        numOfMemAdaptedVmsNeeded -= 1
                    self.log.info("Number of required adapted VMs is: %s, that means %s free memory on this host" % (numOfMemAdaptedVmsNeeded, hostsFreeMem[hId] + numOfMemAdaptedVmsNeeded * diff))
                
                if hostsFreeCpu[hId] <= inst.cpuShare:
                    self.log.info("On the host %s the following amount of CPU can be freed up: %s" % (hId, len(adaptableVms) * diff))
                    numOfCpuAdaptedVmsNeeded = (abs(hostsFreeCpu[hId] - inst.cpuShare) / diff) + 1
                    if abs(hostsFreeCpu[hId] - inst.cpuShare) % diff == 0:
                        numOfCpuAdaptedVmsNeeded -= 1
                
                if numOfMemAdaptedVmsNeeded <= len(adaptableVms) and numOfCpuAdaptedVmsNeeded <= len(adaptableVms):
                    # adapt VMs that have the smallest priority
                    sortedVms = sorted(adaptableVms, key=lambda k: self.vmPriorities[k])
                    k = 0
                    for iId in sortedVms:
                        k += 1
                        if k <= numOfMemAdaptedVmsNeeded:
                            self.client.adaptMemoryLimit(iId, instances[iId].memory - diff)
                            self.log.info("Instance %s memory limit was lowered for %s MB" % (iId, diff))
                            if iId in self.adaptedMem:
                                self.adaptedMem[iId] += diff
                            else:
                                self.adaptedMem[iId] = diff
                        if k <= numOfCpuAdaptedVmsNeeded:
                            self.client.adaptCpuShare(iId, instances[iId].cpuShare - diff)
                            self.log.info("Instance %s CPU share was lowered for %s MHz" % (iId, diff))
                            if iId in self.adaptedCpu:
                                self.adaptedCpu[iId] += diff
                            else:
                                self.adaptedCpu[iId] = diff
                    return True
                else:
                    self.log.info("Adapting the memory and CPU speed (for %s amount of memory in MB and CPU speed in MHz) for the VMs with smaller priority was not enough" % diff)
        self.log.info("No resources were freed up using memory and CPU speed adaptations of the VMs with smaller priority, VMs with smaller priority will be suspended to enable the provisioning of the VMs with higher priority")
        # suspend VMs with smaller priority:
        # don't use pause, because a paused system continues to reside in memory
        for h in appropriateHosts.values():
            vmSmallerPriority = filter(lambda x : self.vmPriorities[x] < self.vmPriorities[inst.id], load[h.id])
            freeMem = hostsFreeMem[h.id]
            freeCpu = hostsFreeCpu[h.id]
            freeCores = hostsFreeCores[h.id]
            vmsToBeSuspended = []
            sortedVms = sorted(vmSmallerPriority, key=lambda k: self.vmPriorities[k])
            for id in sortedVms:
                freeMem += instances[id].memory
                freeCpu += instances[id].cpuShare
                freeCores += instances[id].cores
                vmsToBeSuspended.append(id)
                if inst.memory <= freeMem and inst.cpuShare <= freeCpu and inst.cores <= freeCores:
                    break
            if inst.memory <= freeMem and inst.cpuShare <= freeCpu and inst.cores <= freeCores:
                self.log.info("VMs %s on host %s will be suspended to enable the provisioning of the VM with higher priority." % (vmsToBeSuspended, h.id))
                for id in vmsToBeSuspended:
                    self.client.suspendVm(id)
                    self.log.info("VM %s is suspended" % id)
                    self.suspendedVms.append(id)
                return True
            else:
                self.log.info("Not enough VMs that can be suspended on host %s" % h.id)

    def checkAdaptedVms(self, hosts, load):
        """
        Adapts back the virtual machines that were adapted in freeUpResources method. 
        """
        unsuspended = []
        for i in self.suspendedVms:
            # get updated instances:
            instances = self.getUpdatedInstances()
            inst = instances[i]
            hostId = inst.hostId
            freeMem = hosts[hostId].memory - reduce(lambda x, y: x + instances[y].memory, load[hostId], 0)
            freeCpu = hosts[hostId].cpuSpeed * self.cpuSpeedOverRate - reduce(lambda x, y: x + instances[y].cpuShare, load[hostId], 0)
            if inst.memory <= freeMem and inst.cpuShare <= freeCpu:
                self.client.resumeVm(i)
                self.log.info("Instance %s is resumed" % i)
                unsuspended.append(i)
        for i in unsuspended:
            self.suspendedVms.remove(i)  
            
        cpuBack = []
        for i in self.adaptedCpu.keys():
            # get updated instances:
            instances = self.getUpdatedInstances()
            inst = instances[i]
            hostId = inst.hostId
            diff = self.adaptedCpu[i] 
            cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, load[hostId], 0)
            if diff <= hosts[hostId].cpuSpeed * self.cpuSpeedOverRate - cpuUsage:
                self.client.adaptCpuShare(inst.id, inst.cpuShare + diff)
                self.log.info("Instance %s got back %s amount of CPU share" % (inst.id, diff))
                cpuBack.append(i)
        for i in cpuBack:
            del self.adaptedCpu[i]
        
        memoryBack = []
        for i in self.adaptedMem.keys():
            # get updated instances:
            instances = self.getUpdatedInstances()
            inst = instances[i]
            hostId = inst.hostId
            diff = self.adaptedMem[i] 
            memUsage = reduce(lambda x, y: x + instances[y].memory, load[hostId], 0)
            if diff <= hosts[hostId].memory - memUsage:
                self.client.adaptMemoryLimit(inst.id, inst.memory + diff)
                self.log.info("Instance %s got back %s amount of memory" % (inst.id, diff))
                memoryBack.append(i)
        for i in memoryBack:
            del self.adaptedMem[i]
            
    def powerOffHosts(self, hosts, load):
        """
        Powers off all hosts that don't have virtual machines.
        """
        if not self.autoPowering:
            return
        for h in hosts.values():
            if (load[h.id] == 0 or len(load[h.id]) == 0) and h.poweredOn == True:
                self.log.info("Host %s does not have virtual machines and will be switched off" % h.id)
                self.client.powerOffHost(h.id)
                h.poweredOn = False # if not simulation, the host object poweredOn property is set in the clusterm manager, but not here in scheduler
                
    def powerOnHost(self, host):
        if not self.autoPowering:
            return False
        self.log.info("scheduler: host %s will be powered on" % host.id)
        self.client.powerOnHost(host.id)
        host.poweredOn = True # if not simulation, the host object poweredOn property is set in the clusterm manager, but not here in scheduler
        return True
                
    def powerOnAppropriateHost(self, hosts, inst, instances, load):
        """
        Powers on a host that meet all instance requirements and that 
        has the highest efficiency rating.
        """
        if not self.autoPowering:
            return
        sHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k], reverse=True)
        filteredHosts = filter(lambda x : self.checkHost(hosts[x], inst, instances, load), sHosts)
        for hsId in filteredHosts:
            h = hosts[hsId]
            if h.poweredOn == False and self.checkHost(h, inst, instances, load):
                self.log.info("scheduler: host %s will be powered on" % hsId)
                self.client.powerOnHost(hsId)
                h.poweredOn = True # if not simulation, the host object poweredOn property is set in the clusterm manager, but not here in scheduler
                return h  


