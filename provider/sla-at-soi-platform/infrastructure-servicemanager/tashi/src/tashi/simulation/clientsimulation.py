#! /usr/bin/env python
# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Miha Stopar - miha.stopar@xlab.si
# @version        $Rev$
# @lastrevision   $Date$
# @filesource     $URL$

import os, subprocess
import logging
import time
import ConfigParser

from tashi.util import scrubString
from tashi.rpycservices.rpyctypes import *

class Client(object):
    def __init__(self, config):
        f = "%(asctime)s - %(levelname)s - %(message)s"
        logging.basicConfig(format=f,level=logging.INFO)
        self.log = logging.getLogger(__file__)
        self.instances = {}
        self.hosts = {}
        self.users = {}
        self.users[0] = User(d={'id':3,'name':'user3','priority':3})
        self.username = "user3"
        self.allowDuplicateNames = False
        self.maxCores = 8
        self.maxMemory = 8192
        for (name, value) in config.items("FromConfig"):
            name = name.lower()
            if (name.startswith("host")):
                host = eval(value)
                if (host.__class__ is not Host):
                    raise ValueError, "Entry %s is not a Host" % (name)
                self.hosts[host.id] = host
            if (name.startswith("user")):
                user = eval(value)
                if (user.__class__ is not User):
                    raise ValueError, "Entry %s is not a User" % (name)
                self.users[user.id] = user
            if (name.startswith("instance")):
                instance = eval(value)
                if (instance.__class__ is not Instance):
                    raise ValueError, "Entry %s is not an Instance" % (name)
                self.instances[instance.id] = instance
        self.maxInstanceId = len(self.instances) + 1
                
    def normalize(self, instance):
        instance.id = None
        instance.vmId = None
        instance.hostId = None
        instance.decayed = False
        instance.name = scrubString(instance.name, allowed="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.")
        instance.state = InstanceState.Pending
        # At some point, check userId
        if (not self.allowDuplicateNames):
            for i in self.instances.itervalues():
                if (i.name == instance.name):
                    raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"The name %s is already in use" % (instance.name)})
        if (instance.cores < 1):
            raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Number of cores must be >= 1"})
        if (instance.cores > self.maxCores):
            raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Number of cores must be <= %d" % (self.maxCores)})
        if (instance.memory < 1):
            raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Amount of memory must be >= 1"})
        if (instance.memory > self.maxMemory):
            raise TashiException(d={'errno':Errors.InvalidInstance,'msg':"Amount of memory must be <= %d" % (self.maxMemory)})
        # Make sure disk spec is valid
        # Make sure network spec is valid
        return instance
    
    def createVm(self, instance):
        """Function to add a VM to the list of pending VMs"""
        instance = self.normalize(instance)
        if (instance.id is not None and instance.id not in self.instances):
            if (instance.id >= self.maxInstanceId):
                self.maxInstanceId = instance.id + 1
        else:
            instance.id = self.getNewInstanceId()   
        self.instances[instance.id] = instance
        self.saveEntities("instance")
        return instance
    
    def migrateVm(self, instanceId, targetHostId):
        inst = self.instances[instanceId]
        inst.hostId = targetHostId
        self.saveEntities("instance")
        
    def destroyVm(self, instanceId):
        del self.instances[instanceId]
        self.saveEntities("instance")
        
    def activateVm(self, instanceId, host):
        instance = self.instances[instanceId]
        instance.hostId = host.id
        instance.state = InstanceState.Running
        self.saveEntities("instance")
        
    def adaptCpuShare(self, instanceId, share):
        inst = self.instances[instanceId]
        inst.cpuShare = share
        self.saveEntities("instance")
    
    def adaptMemoryLimit(self, instanceId, limit):
        inst = self.instances[instanceId]
        inst.memory = limit
        self.saveEntities("instance")
        
    def adaptNetworkBandwidth(self, instanceId, downspeed, upspeed):
        inst = self.instances[instanceId]
        inst.downspeed = downspeed
        inst.upspeed = upspeed
        self.saveEntities("instance")
        
    def adaptDiskBandwidth(self, instanceId, weight):
        inst = self.instances[instanceId]
        inst.diskBandwidthWeight = weight
        self.saveEntities("instance")
        
    def pauseVm(self, instanceId):
        inst = self.instances[instanceId]
        inst.state = InstanceState.Paused
        self.saveEntities("instance")
        
    def unpauseVm(self, instanceId):
        inst = self.instances[instanceId]
        inst.state = InstanceState.Running
        self.saveEntities("instance")
        
    def suspendVm(self, instanceId):
        inst = self.instances[instanceId]
        inst.state = InstanceState.Suspended
        self.saveEntities("instance")
        
    def resumeVm(self, instanceId):
        inst = self.instances[instanceId]
        inst.state = InstanceState.Running
        self.saveEntities("instance")
    
    def getInstances(self):
        # read instances from file, because some new instance could be inserted
        # there by createVm from simulation.tashi-client.py and needs to be provisioned
        config = ConfigParser.ConfigParser()
        config.read("Simulation.cfg")
        instances = {}
        for (name, value) in config.items("FromConfig"):
            name = name.lower()
            if (name.startswith("instance")):
                instance = eval(value)
                if (instance.__class__ is not Instance):
                    raise ValueError, "Entry %s is not an Instance" % (name)
                instances[instance.id] = instance
        self.instances = instances
        return self.instances.values()
    
    def getUsers(self):
        return self.users.values()
    
    def getHosts(self):
        return self.hosts.values()
    
    def powerOnHost(self, hostId):
        host = self.hosts[hostId]
        self.log.info("Host %s will be powered on" % hostId)
        time.sleep(5)
        host.poweredOn = 1
        host.up = 1
        self.saveEntities("host")
        self.log.info("Host %s is on" % hostId)
        
    def powerOffHost(self, hostId):
        self.log.info("Host %s will be powered off" % hostId)
        host = self.hosts[hostId]
        host.poweredOn = 0
        host.up = 0
        self.log.info("Host %s is powered off" % hostId)
        self.saveEntities("host")
        
    def setHostConfig(self, hostId, elemName, value):
        host = self.hosts[hostId]
        if elemName in host.__dict__.keys():
            setattr(host, elemName, value)
        self.saveEntities("host")
    
    def getNetworks(self):
        networks = {}
        for i in range(3):
            network = Network(d={'id':i, 'name':'network %s' % i})
            networks[network.id] = network
        return networks.values()
    
    def getNewInstanceId(self):
        instanceId = self.maxInstanceId
        self.maxInstanceId = self.maxInstanceId + 1
        return instanceId
            
    def saveEntities(self, entity):
        # entity: host or instance
        fileName = "Simulation.cfg"
        parser = ConfigParser.ConfigParser()
        parser.read(fileName)
        
        if not parser.has_section("FromConfig"):
            parser.add_section("FromConfig")
        
        entitiesInFile = []
        for (name, value) in parser.items("FromConfig"):
            name = name.lower()
            if (name.startswith(entity)):
                entitiesInFile.append(name)
                
        for i in entitiesInFile:
            parser.remove_option("FromConfig", i)
            
        if entity == "host":
            for hId in self.hosts.keys():
                h = self.hosts[hId]
                hostPresentation = "Host(d={'id': %s, 'poweredOn':%s, 'up': 1, 'decayed': 0, 'state': 1, 'name': '%s', 'memory':%s, 'cores': %s, 'cpuSpeed':%s, 'version':%s, 'auditability':%s, 'location':'%s', 'sas70':%s, 'ccr':%s,\'dataClass':%s, 'hwLevel':%s, 'diskThroughput':%s, 'netThroughput':%s, 'dataEncryption':%s})" % (hId, int(h.poweredOn),\
                h.name, h.memory, h.cores, h.cpuSpeed, h.version, h.auditability, h.location, h.sas70,\
                h.ccr, h.dataClass, h.hwLevel, h.diskThroughput, h.netThroughput, h.dataEncryption)
                parser.set("FromConfig", "host%s" % hId, hostPresentation)
        elif entity == "instance":
            for iId in self.instances.keys():
                inst = self.instances[iId]
                instPresentation = "Instance(d={'id':%s,'name':'%s','userId':%s,'hostId':%s,'cores':%s,'cpuShare':%s,'memory':%s,'disks':%s,'disk':'','hints':%s,'state':%s,'groupName':'%s', 'downspeed':%s,\
                    'upspeed':%s, 'diskBandwidthWeight':%s,\
                    'auditability':%s, 'location':'%s', 'sas70':%s, 'ccr':%s, 'dataClass':%s, 'hwLevel':%s,\
                    'diskThroughput':%s, 'netThroughput':%s, 'isolation':%s, 'dataRetention':%s,\
                    'deleteMethod':%s, 'powerCapping':%s, 'snapshotBackup':%s, 'snapshotRetention':%s})" % (iId, inst.name, inst.userId, inst.hostId, inst.cores, inst.cpuShare, inst.memory, inst.disks, inst.hints, inst.state, inst.groupName, inst.downspeed,\
                    inst.upspeed, inst.diskBandwidthWeight, inst.auditability, inst.location, inst.sas70, inst.ccr,\
                    inst.dataClass, inst.hwLevel, inst.diskThroughput, inst.netThroughput, inst.isolation, inst.dataRetention,\
                    inst.deleteMethod, inst.powerCapping, inst.snapshotBackup, inst.snapshotRetention)
                parser.set("FromConfig", "instance%s" % iId, instPresentation)
            
        with open(fileName, 'wb') as configfile:
            parser.write(configfile)
