SCHEDULER IMPLEMENTATIONS

There are currently three scheduler implementations available (namely Primitive, Balanced, 
Consolidated schedulers). All of them implement SchedulerInterface methods. PrimitiveScheduler
extends directly SchedulerInterface and offe the same as primitive scheduler in
official version of Tashi. 

Balanced and Consolidated schedulers both extend SchedulerCommon.
SchedulerCommon provides some common functionality for schedulers that take into account 
VM priorities and server efficiency rating. It should be used only as parent class
of actual scheduler implementations.

In short - when VMs are provisioned via BalancedScheduler, they are spread evenly across 
the number of available hosts, while when VMs are provisioned via ConsolidatedScheduler, 
each physical host is filled to its capacity before moving to the next one. 

NEW SCHEDULER IMPLEMENTATIONS AND SCHEDULER SIMULATION

New scheduler types can be easily developed using SchedulerInterface and SchedulerManager, 
which executes methods in a specific workflow from a chosen scheduler.

So what is this simulation all about?
Simulation tools (schedulersimulation.py, clientsimulation.py, tashi-client.py) in this 
directory enable running and testing schedulers completely without Tashi. You don't need
cluster manager running, you don't need node managers, and you don't need DHCP and DNS agents
running! Moreover, this scheduler simulation uses some Tashi classes for Host, Instance 
presentations and so on, but it is completely separated from Tashi semantics and
scheduler can be applied to other systems.

Let's say that we would like to test ConsolidatedScheduler! 

- First, download Tashi sources:
svn co https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/simulation

- Go into tashi directory and move to the simulation directory: cd src/tashi/simulation

- Make sure that you have Tashi src folder included in PYTHONPATH, 
for example: export PYTHONPATH=/Users/mihastopar/sourceforge/tashi/src

- Configure scheduler options in Simulation.cfg:

[Scheduler]
hook1 = tashi.scheduler.DhcpDns
scheduleDelay = 2.0
scheduler = tashi.agents.ConsolidatedScheduler
rescheduleAfterStart = False
autoMigrations = True
loadExpression = memUsage + coresUsage * 512 + cpuUsage
# if it is 1, the CPU cores over-provisioning is not allowed
cpuCoresOverRate = 1
# if it is 1, the CPU speed over-provisioning is not allowed
cpuSpeedOverRate = 2 
# minimum number of servers that are always ready for provisioning
# without trying to use over-provisioning
guaranteedServers = 3
optimalConsolidation = False
minimalVmMemory = 64
autoPowering = True

- Configure imaginary physical servers in Simulation.cfg. Later on you can play with modifying 
with different number of servers and different server attributes (memory, cores...) to see how 
VMs are scheduled in different host landscapes.

- You can start scheduler now: python schedulersimulation.py

- Now you can create a (virtual) virtual machine :) using tashi-client.py script.
Virtual machine will exist only in the Simulation.cfg file, 
but scheduler will handle with it as if it were a running virtual machine. This 
is a slightly modified version of tashi-client.py (see client directory for original version)
which uses ClientSimulation instead of rpycservices.client.
Let's create it: python tashi-client.py createVm --userId 1 --name miha2 --disks tashi.img

Note: You must specify userId (when using original tashi-client.py this is not
necessary). Imaginary users are defined in Simulation.cfg

- At any time you can check your instances or hosts: python tashi-client.py getInstances, python tashi-client.py getHosts

- You can also migrate, destroy, adapt... your virtual virtual machines and observe reactions of scheduler.




