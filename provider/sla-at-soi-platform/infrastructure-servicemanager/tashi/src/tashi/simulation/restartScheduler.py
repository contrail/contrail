# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 685 $
# @lastrevision $Date: 2011-02-11 13:37:41 +0100 (Fri, 11 Feb 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/infrastructure-servicemanager/tashi/src/tashi/resources.py $

import os, subprocess

output = os.popen("ps ax -o pid,command | grep schedulersimulation.py").read()
lines = output.split("\n")
line = ""
for i in lines:
    if not 'grep' in i and 'schedulersimulation.py' in i:
        line = i	
        break

pid = line.strip().split(" ")[0]
print "pid to kill: %s" % pid
try:
    subprocess.call("kill -9 %s" % pid, shell=True)
except:
    print "kill failed"

subprocess.call("python schedulersimulation.py tashi.scheduler.ConsolidatedScheduler 2 True 0 2 memUsage+coresUsage*512+cpuUsage True 6 False 64 False", shell=True) # argumets:scheduler schedulerDelay, reschedulerAfterStart, cpuCoresOverRate, cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers, optimalConsolidation, minimalVmMemory, autoPowering
