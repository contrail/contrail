# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 685 $
# @lastrevision $Date: 2011-02-11 13:37:41 +0100 (pet, 11 feb 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/resources.py $

from tashi.rpycservices.rpyctypes import *

def getFreeMemory(client, hostId):
    for h in client.getHosts():
        if h.id == hostId:
            instances = getInstancesOnHost(client, h.id)
            freeMem = h.memory - reduce(lambda x, y: x + y.memory, instances, 0)
            print "Free memory on host %s: %s" % (h.id, freeMem)
            return freeMem
        
def getFreeCpu(client, hostId):
    for h in client.getHosts():
        if h.id == hostId:
            instances = getInstancesOnHost(client, h.id)
            freeCpu = h.cpuSpeed - reduce(lambda x, y: x + y.cpuShare, instances, 0)
            print "Free CPU on host %s: %s" % (h.id, freeCpu)
            return freeCpu
        
def getHostToMigrate(client, memoryNeeded, cpuNeeded, hostId):
    for h in client.getHosts():
        if hostId == h.id:
            continue
        instances = getInstancesOnHost(client, h.id)
        freeMem = h.memory - reduce(lambda x, y: x + y.memory, instances, 0)
        freeCpu = h.cpuSpeed - reduce(lambda x, y: x + y.cpuShare, instances, 0)
        if freeMem >= memoryNeeded and freeCpu >= cpuNeeded and h.up == True and h.state == HostState.Normal and len(h.reserved) == 0:
            return h.id

def getInstancesOnHost(client, hostId):
    hostInstances = []
    for i in client.getInstances():
        if i.hostId == hostId:
            hostInstances.append(i)
    return hostInstances
    