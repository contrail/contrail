# This file contains modifications by XLAB d.o.o. (Copyright (c) 
# 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 1726 $
# @lastrevision $Date: 2011-05-16 10:01:59 +0200 (pon, 16 maj 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/rpycservices/rpyctypes.py $ 

class Errors(object):
	ConvertedException = 1
	NoSuchInstanceId = 2
	NoSuchVmId = 3
	IncorrectVmState = 4
	NoSuchHost = 5
	NoSuchHostId = 6
	InstanceIdAlreadyExists = 7
	HostNameMismatch = 8
	HostNotUp = 9
	HostStateError = 10
	InvalidInstance = 11
	UnableToResume = 12
	UnableToSuspend = 13

class InstanceState(object):
	Pending = 1
	Activating = 2
	Running = 3
	Pausing = 4
	Paused = 5
	Unpausing = 6
	Suspending = 7
	Resuming = 8
	MigratePrep = 9
	MigrateTrans = 10
	ShuttingDown = 11
	Destroying = 12
	Orphaned = 13
	Held = 14
	Exited = 15
	Suspended = 16
	PreparingDisk = 17
	Restarting = 18

class HostState(object):
	Normal = 1
	Drained = 2
	VersionMismatch = 3
	
class DiskThroughputLevel(object):
	Standard, Better, Best = range(1, 4)
	
class NetThroughputLevel(object):
	Standard, Better, Best = range(1, 4)
	
class DataClassification(object):
	Public, Confidential, TopSecret = range(1, 4)
	
class IsoCountryCode(object):
	Slovenia, Ireland, Germany, China = "si", "ie", "de", "cn"
	
class DataDeleteMethod(object):
	Standard, Secure = 1, 2
	
class HardwareRedundancyLevel(object):
	Standard, Better, Best = range(1, 4)

class TashiException(Exception):
	def __init__(self, d=None):
		self.errno = None
		self.msg = None
		if isinstance(d, dict):
			if 'errno' in d:
				self.errno = d['errno']
			if 'msg' in d:
				self.msg = d['msg']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class Host(object):
	def __init__(self, d=None):
		self.id = None
		self.name = None
		self.up = None
		self.decayed = None
		self.state = None
		self.memory = None
		self.cores = None
		self.cpuSpeed = None
		self.version = None
		self.notes = None
		self.reserved = []
		self.poweredOn = None
		self.auditability = None
		self.location = None
		self.sas70 = None
		self.ccr = None
		self.dataClass = None
		self.hwLevel = None
		self.diskThroughput = None
		self.netThroughput = None
		self.dataEncryption = None
		if isinstance(d, dict):
			if 'id' in d:
				if d['id'] == 'None':
					self.id = None
				elif isinstance(d['id'], basestring):
					self.id = int(d['id'])
				else:
					self.id = d['id']
			
			if 'name' in d:
				if d['name'] == 'None':
					self.name = None
				else:
					self.name = d['name']
			
			if 'up' in d:
				if d['up'] == 'None':
					self.up = None
				elif isinstance(d['up'], basestring):
					self.up = bool(d['up'])
				else:
					self.up = d['up']
			
			if 'decayed' in d:
				if d['decayed'] == 'None':
					self.decayed = None
				elif isinstance(d['decayed'], basestring):
					self.decayed = bool(d['decayed'])
				else:
					self.decayed = d['decayed']
			
			if 'state' in d:
				if d['state'] == 'None':
					self.state = None
				elif isinstance(d['state'], basestring):
					self.state = int(d['state'])
				else:
					self.state = d['state']
				
			if 'memory' in d:
				if d['memory'] == 'None':
					self.memory = None
				elif isinstance(d['memory'], basestring):
					self.memory = int(d['memory'])
				else:
					self.memory = d['memory']
				
			if 'cores' in d:
				if d['cores'] == 'None':
					self.cores = None
				elif isinstance(d['cores'], basestring):
					self.cores = int(d['cores'])
				else:
					self.cores = d['cores']
				
			if 'cpuSpeed' in d:
				if d['cpuSpeed'] == 'None':
					self.cpuSpeed = None
				elif isinstance(d['cpuSpeed'], basestring):
					self.cpuSpeed = int(d['cpuSpeed'])
				else:
					self.cpuSpeed = d['cpuSpeed']
				
			if 'version' in d:
				if d['version'] == 'None':
					self.version = None
				else:
					self.version = d['version']
				
			if 'notes' in d:
				if d['notes'] == 'None':
					self.notes = None
				else:
					self.notes = d['notes']
				
			if 'reserved' in d:
				if d['reserved'] == 'None':
					self.reserved = None
				else:
					self.reserved = d['reserved']
					
			if 'poweredOn' in d:
				if d['poweredOn'] == 'None':
					self.poweredOn = None
				else:
					self.poweredOn = d['poweredOn']
					
			if 'auditability' in d:
				if d['auditability'] == 'None':
					self.auditability = None
				else:
					self.auditability = d['auditability']
					
			if 'location' in d:
				if d['location'] == 'None':
					self.location = None
				else:
					self.location = d['location']
		
			if 'sas70' in d:
				if d['sas70'] == 'None':
					self.sas70 = None
				else:
					self.sas70 = d['sas70']
		
			if 'ccr' in d:
				if d['ccr'] == 'None':
					self.ccr = None
				else:
					self.ccr = d['ccr']
	
			if 'dataClass' in d:
				if d['dataClass'] == 'None':
					self.dataClass = None
				else:
					self.dataClass = d['dataClass']
					
			if 'hwLevel' in d:
				if d['hwLevel'] == 'None':
					self.hwLevel = None
				else:
					self.hwLevel = d['hwLevel']
	
			if 'diskThroughput' in d:
				if d['diskThroughput'] == 'None':
					self.diskThroughput = None
				else:
					self.diskThroughput = d['diskThroughput']
		
			if 'netThroughput' in d:
				if d['netThroughput'] == 'None':
					self.netThroughput = None
				else:
					self.netThroughput = d['netThroughput']
					
			if 'dataEncryption' in d:
				if d['dataEncryption'] == 'None':
					self.dataEncryption = None
				else:
					self.dataEncryption = d['dataEncryption']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class Network(object):
	def __init__(self, d=None):
		self.id = None
		self.name = None
		if isinstance(d, dict):
			if 'id' in d:
				if d['id'] == 'None':
					self.id = None
				elif isinstance(d['id'], basestring):
					self.id = int(d['id'])
				else:
					self.id = d['id']
			if 'name' in d:
				if d['name'] == 'None':
					self.name = None
				else:
					self.name = d['name']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class User(object):
	def __init__(self, d=None):
		self.id = None
		self.name = None
		self.passwd = None
		self.priority = None
		if isinstance(d, dict):
			if 'id' in d:
				if d['id'] == 'None':
					self.id = None
				elif isinstance(d['priority'], basestring):
					self.id = int(d['id'])
				else:
					self.id = d['id']
			
			if 'name' in d:
				if d['name'] == 'None':
					self.name = None
				else: 
					self.name = d['name']
			
			if 'passwd' in d:
				if d['passwd'] == 'None':
					self.passwd = None
				else:
					self.passwd = d['passwd']
			
			if 'priority' in d:
				if d['priority'] == 'None':
					self.priority = None
				elif isinstance(d['priority'], basestring):
					self.priority = int(d['priority'])
				else:
					self.priority = d['priority']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class DiskConfiguration(object):
	def __init__(self, d=None):
		self.uri = None
		self.persistent = None
		if isinstance(d, dict):
			if 'uri' in d:
				if d['uri'] == 'None':
					self.uri = None
				else:
					self.uri = d['uri']
					
			if 'persistent' in d:
				if d['persistent'] == 'None':
					self.persistent = None
				elif isinstance(d['persistent'], basestring):
					self.persistent = bool(d['persistent'])
				else:
					self.persistent = d['persistent']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class NetworkConfiguration(object):
	def __init__(self, d=None):
		self.network = None
		self.mac = None
		self.ip = None
		if isinstance(d, dict):
			if 'network' in d:
				if d['network'] == 'None':
					self.network = None
				else:
					self.network = d['network']
					
			if 'mac' in d:
				if d['mac'] == 'None':
					self.mac = None
				else:
					self.mac = d['mac']
			
			if 'ip' in d:
				if d['ip'] == 'None':
					self.ip = None
				else:
					self.ip = d['ip']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class Instance(object):
	def __init__(self, d=None):
		self.id = None
		self.vmId = None
		self.hostId = None
		self.decayed = None
		self.state = None
		self.userId = None
		self.name = None
		self.cores = None
		self.cpuShare = None
		self.memory = None
		self.disks = None
		#Quick fix so self.nics is not None
		self.nics = []
		self.hints = None
		self.groupName = None
		self.downspeed = None
		self.upspeed = None
		self.diskBandwidthWeight = None
		self.auditability = None
		self.location = None
		self.sas70 = None
		self.ccr = None
		self.dataClass = None
		self.hwLevel = None
		self.diskThroughput = None
		self.netThroughput = None
		self.dataEncryption = None
		self.isolation = None # when True all VMs from the same group need to be on a separated machines
		self.dataRetention = None # number of days to maintain the customers hard drive files after the SLA ceases
		self.deleteMethod = None
		self.powerCapping = None # enable for energy saving
		self.snapshotBackup = None # is it required to take regular snapshots of the running VM 
		self.snapshotRetention = None # how many days maintain an online copy of the most recent snapshot
		if isinstance(d, dict):

			if 'id' in d:
				if d['id'] == 'None':
					self.id = None
				elif isinstance(d['id'], basestring):
					self.id = int(d['id'])
				else:
					self.id = d['id']

			if 'vmId' in d:
				if d['vmId'] == 'None':
					self.vmId = None
				elif isinstance(d['vmId'], basestring):
					self.vmId = int(d['vmId'])
				else:
					self.vmId = d['vmId']
			
			if 'hostId' in d:
				if d['hostId'] == 'None':
					self.hostId = None
				elif isinstance(d['hostId'], basestring):
					self.hostId = int(d['hostId'])
				else:
					self.hostId = d['hostId']
					
			if 'decayed' in d:
				if d['decayed'] == 'None':
					self.decayed = None
				elif isinstance(d['decayed'], basestring):
					self.decayed = bool(d['decayed'])
				else:
					self.decayed = d['decayed']
			
			if 'state' in d:
				if d['state'] == 'None':
					self.state = None
				elif isinstance(d['state'], basestring):
					self.state = int(d['state'])
				else:
					self.state = d['state']
			
			if 'userId' in d:
				if d['userId'] == 'None':
					self.userId = None
				elif isinstance(d['userId'], basestring):
					self.userId = int(d['userId'])
				else:
					self.userId = d['userId']
			
			if 'name' in d:
				if d['name'] == 'None':
					self.name = None
				else:
					self.name = d['name']
				
			if 'cores' in d:
				if d['cores'] == 'None':
					self.cores = None
				elif isinstance(d['cores'], basestring):
					self.cores = int(d['cores'])
				else:
					self.cores = d['cores']
			
			if 'cpuShare' in d:
				if d['cpuShare'] == 'None':
					self.cpuShare = None
				elif isinstance(d['cpuShare'], basestring):
					self.cpuShare = int(d['cpuShare'])
				else:
					self.cpuShare = d['cpuShare']
			
			if 'memory' in d:
				if d['memory'] == 'None':
					self.memory = None
				elif isinstance(d['memory'], basestring):
					self.memory = int(d['memory'])
				else:
					self.memory = d['memory']
			
			if 'disks' in d and d['disks'] != None:
				for entry in d['disks']:
					if isinstance(entry, dict):
						dc = DiskConfiguration(entry)
						d['disks'].remove(entry)
						d['disks'].append(dc)
				self.disks = d['disks']
				
			if 'nics' in d:
				for entry in d['nics']:
					if isinstance(entry, dict):
						nc = NetworkConfiguration(entry)
						d['nics'].remove(entry)
						d['nics'].append(nc)
				self.nics = d['nics']
				
			if 'hints' in d:
				self.hints = d['hints']
				
			if 'groupName' in d:
				if d['groupName'] == 'None':
					self.groupName = None
				else:
					self.groupName = d['groupName']
				
			if 'downspeed' in d:
				if d['downspeed'] == 'None':
					self.downspeed = None
				elif isinstance(d['downspeed'], basestring):
					self.downspeed = int(d['downspeed'])
				else:
					self.downspeed = d['downspeed']
					
			if 'upspeed' in d:
				if d['upspeed'] == 'None':
					self.upspeed = None
				elif isinstance(d['upspeed'], basestring):
					self.upspeed = int(d['upspeed'])
				else:
					self.upspeed = d['upspeed']
					
			if 'diskBandwidthWeight' in d:
				if d['diskBandwidthWeight'] == 'None':
					self.diskBandwidthWeight = None
				elif isinstance(d['diskBandwidthWeight'], basestring):
					self.diskBandwidthWeight = int(d['diskBandwidthWeight'])
				else:
					self.diskBandwidthWeight = d['diskBandwidthWeight']
					
			if 'auditability' in d:
				if d['auditability'] == 'None':
					self.auditability = None
				elif isinstance(d['auditability'], basestring):
					self.auditability = bool(d['auditability'])
				else:
					self.auditability = d['auditability']
					
			if 'location' in d:
				if d['location'] == 'None':
					self.location = None
				else:
					self.location = d['location']
		
			if 'sas70' in d:
				if d['sas70'] == 'None':
					self.sas70 = None
				elif isinstance(d['sas70'], basestring):
					self.sas70 = bool(d['sas70'])
				else:
					self.sas70 = d['sas70']
		
			if 'ccr' in d:
				if d['ccr'] == 'None':
					self.ccr = None
				elif isinstance(d['ccr'], basestring):
					self.ccr = bool(d['ccr'])
				else:
					self.ccr = d['ccr']
	
			if 'dataClass' in d:
				if d['dataClass'] == 'None':
					self.dataClass = None
				elif isinstance(d['dataClass'], basestring):
					self.dataClass = int(d['dataClass'])
				else:
					self.dataClass = d['dataClass']
					
			if 'hwLevel' in d:
				if d['hwLevel'] == 'None':
					self.hwLevel = None
				elif isinstance(d['hwLevel'], basestring):
					self.hwLevel = int(d['hwLevel'])
				else:
					self.hwLevel = d['hwLevel']
	
			if 'diskThroughput' in d:
				if d['diskThroughput'] == 'None':
					self.diskThroughput = None
				elif isinstance(d['diskThroughput'], basestring):
					self.diskThroughput = int(d['diskThroughput'])
				else:
					self.diskThroughput = d['diskThroughput']
		
			if 'netThroughput' in d:
				if d['netThroughput'] == 'None':
					self.netThroughput = None
				elif isinstance(d['netThroughput'], basestring):
					self.netThroughput = int(d['netThroughput'])
				else:
					self.netThroughput = d['netThroughput']
					
			if 'dataEncryption' in d:
				if d['dataEncryption'] == 'None':
					self.dataEncryption = None
				elif isinstance(d['dataEncryption'], basestring):
					self.dataEncryption = bool(d['dataEncryption'])
				else:
					self.dataEncryption = d['dataEncryption']
					
			if 'isolation' in d:
				if d['isolation'] == 'None':
					self.isolation = None
				elif isinstance(d['isolation'], basestring):
					self.isolation = bool(d['isolation'])
				else:
					self.isolation = d['isolation']
					
			if 'dataRetention' in d:
				if d['dataRetention'] == 'None':
					self.dataRetention = None
				elif isinstance(d['dataRetention'], basestring):
					self.dataRetention = int(d['dataRetention'])
				else:
					self.dataRetention = d['dataRetention']
			
			if 'deleteMethod' in d:
				if d['deleteMethod'] == 'None':
					self.deleteMethod = None
				elif isinstance(d['deleteMethod'], basestring):
					self.deleteMethod = int(d['deleteMethod'])
				else:
					self.deleteMethod = d['deleteMethod']
					
			if 'powerCapping' in d:
				if d['powerCapping'] == 'None':
					self.powerCapping = None
				elif isinstance(d['powerCapping'], basestring):
					self.powerCapping = bool(d['powerCapping'])
				else:
					self.powerCapping = d['powerCapping']
			
			if 'snapshotBackup' in d:
				if d['snapshotBackup'] == 'None':
					self.snapshotBackup = None
				elif isinstance(d['snapshotBackup'], basestring):
					self.snapshotBackup = bool(d['snapshotBackup'])
				else:
					self.snapshotBackup = d['snapshotBackup']	
					
			if 'snapshotRetention' in d:
				if d['snapshotRetention'] == 'None':
					self.snapshotRetention = None
				elif isinstance(d['snapshotRetention'], basestring):
					self.snapshotRetention = int(d['snapshotRetention'])
				else:
					self.snapshotRetention = d['snapshotRetention']		

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class Key(object):
	def __init__(self, d=None):
		self.userId = None
		self.keyName = None
		self.fingerprint = None
		self.pubkey = None
		self.privkey = None
		if isinstance(d, dict):
			if 'userId' in d:
				self.userId = d['userId']
			if 'keyName' in d:
				self.keyName = d['keyName']
			if 'fingerprint' in d:
				self.fingerprint = d['fingerprint']
			if 'pubkey' in d:
				self.pubkey = d['pubkey']
			if 'privkey' in d:
				self.privkey = d['privkey']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.userId == other.userId and self.keyName == other.keyName

	def __ne__(self, other):
		return not (self == other)

class Group(object):
	def __init__(self, d=None):
		self.userId = None
		self.groupName = None
		self.groupDescription = None
		self.ipPermissions = []
		if isinstance(d, dict):
			if 'userId' in d:
				self.userId = d['userId']
			if 'groupName' in d:
				self.groupName = d['groupName']
			if 'groupDescription' in d:
				self.groupDescription = d['groupDescription']
			if 'ipPermissions' in d:
				self.ipPermissions = d['ipPermissions']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.userId == other.userId and self.groupName == other.groupName

	def __ne__(self, other):
		return not (self == other)

class GroupPermission(object):
	def __init__(self, d=None):
		self.targetUserId = None
		self.groupName = None
		if isinstance(d, dict):
			if 'targetUserId' in d:
				self.targetUserId = d['targetUserId']
			if 'groupName' in d:
				self.groupName = d['groupName']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

	def __ne__(self, other):
		return not (self == other)

class IpPermission(object):
	def __init__(self, d=None):
		self.userId = None
		self.groupName = None
		self.ipProtocol = None
		self.fromPort = None
		self.toPort = None
		self.cidrIp = None
		self.groupPermissions = []
		if isinstance(d, dict):
			if 'userId' in d:
				self.userId = d['userId']
			if 'groupName' in d:
				self.groupName = d['groupName']
			if 'ipProtocol' in d:
				self.ipProtocol = d['ipProtocol']
			if 'fromPort' in d:
				self.fromPort = d['fromPort']
			if 'toPort' in d:
				self.toPort = d['toPort']
			if 'cidrIp' in d:
				self.cidrIp = d['cidrIp']
			if 'groupPermissions' in d:
				self.groupPermissions = d['groupPermissions']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.groupName == other.groupName and self.userId == other.userId and self.ipProtocol == other.ipProtocol and self.fromPort == other.fromPort and self.toPort == other.toPort and self.cidrIp == other.cidrIp

	def __ne__(self, other):
		return not (self == other)

class Image(object):
	def __init__(self, d=None):
		self.userId = None
		self.imageId = None
		self.isPublic = False
		self.explicitUserIds = []
		self.s3path = None
		self.productCode = ''
		if isinstance(d, dict):
			if 'userId' in d:
				self.userId = d['userId']
			if 'imageId' in d:
				self.imageId = d['imageId']
			if 'isPublic' in d:
				self.isPublic = d['isPublic']
			if 'explicitUserIds' in d:
				self.explicitUserIds = d['explicitUserIds']
			if 's3path' in d:
				self.s3path = d['s3path']
			if 'productCode' in d:
				self.productCode = d['productCode']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.imageId == other.imageId and self.userId == other.userId

	def __ne__(self, other):
		return not (self == other)

class Address(object):
	def __init__(self, d=None):
		self.userId = None
		self.publicIp = None
		self.instanceId = None
		if 'userId' in d:
			self.userId = d['userId']
		if 'publicIp' in d:
			self.publicIp = d['publicIp']
		if 'instanceId' in d:
			self.instanceId = d['instanceId']

	def __str__(self): 
		return str(self.__dict__)

	def __repr__(self): 
		return repr(self.__dict__)

	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.userId == other.userId and self.publicIp == other.publicIp

	def __ne__(self, other):
		return not (self == other)
