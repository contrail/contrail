# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Miha Stopar - miha.stopar@xlab.si
# @version        $Rev: 304 $
# @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/tests/forkAndSubprocessQemu.py $

import os
import subprocess
import sys
from multiprocessing import Process

def main():
    (pipe_r, pipe_w) = os.pipe()
    pid = os.fork()
    if (pid == 0):
        print 'Forked process...'
        pid = os.getpid()
        os.setpgid(pid, pid)
        os.close(pipe_r)
        os.dup2(pipe_w, sys.stderr.fileno())
        for i in [sys.stdin.fileno(), sys.stdout.fileno()]:
            try:
                os.close(i)
            except:
                pass
        for i in xrange(3, os.sysconf("SC_OPEN_MAX")):
            try:
                os.close(i)
            except:
                pass
        startQemu()
        sys.exit(-1)
    os.close(pipe_w)
        
def qemuProcess():
    print 'pppp:', os.getppid()
    print 'ppp:', os.getpid()
    cmd = ["/usr/bin/qemu", "/home/mihas/qemu-miha/debian1.img"]
    os.execl("/usr/bin/qemu", *cmd)
    
def startQemu():
    print 'parent process:', os.getppid()
    print 'process id:', os.getpid() 
    pid = os.getpid()
    
    if not os.path.exists("/dev/cpu/tasks"):
        print "mounting Cgroups..."
        subprocess.call("sudo mkdir -p /dev/cpu", shell=True)
        subprocess.call("mount -t cgroup -ocpu cpu /dev/cpu", shell=True)
    # create a cgroup for this virtual machine:
    subprocess.call("sudo mkdir -p /dev/cpu/test%s" % pid, shell=True)    
    print "Cgroup test%s created" % pid
    # specify a share for this group:
    subprocess.call("echo 1024 | sudo tee /dev/cpu/test%s/cpu.shares" % pid, shell=True)
    subprocess.call("echo %s | sudo tee /dev/cpu/test%s/tasks" % (pid, pid), shell=True)
    
    p = Process(target = qemuProcess)
    p.start()
   

if __name__ == "__main__":
    main()
