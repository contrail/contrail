# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Miha Stopar - miha.stopar@xlab.si
# @version        $Rev: 304 $
# @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/tests/forkAndSubprocess.py $

import subprocess
from multiprocessing import Process
import os, sys, time

def main():
	(pipe_r, pipe_w) = os.pipe()
	pid = os.fork()
	if pid == 0:
		pid = os.getpid()
		os.setpgid(pid, pid)
		print "forked process...: %s" % pid
		p = Process(target = qemuProcess, args=(pipe_r, pipe_w,))		
		p.start()
		print "PID of the VM: %s" % p.pid
		sys.exit(-1) # kill parent process
			
	print "PID of the nodemanager: %s" % pid
	output = os.popen("ps -o pid,ppid ax | grep %s" % pid).read()	
	lines = output.split("\n")
	childrenLines = filter(lambda x: x.endswith(str(pid)) == True, lines)
	childrenPids = map(lambda x : int(x.strip(str(pid)).strip(" ")), childrenLines)
	print "childrenPids: %s" % childrenPids
	childrenPids.sort()
	vmPid = childrenPids[0]
	print "pid: %s" % vmPid

def qemuProcess(pipe_r, pipe_w):
	os.close(pipe_r)
	os.dup2(pipe_w, sys.stderr.fileno())
	print "bla"
#	for i in [ss.stdin.fileno(), sys.stdout.fileno()]:
#		try:
#			os.close(i)
#		except:
#			pass
#	for i in xrange(3, os.sysconf("SC_OPEN_MAX")):
#		try:
#			os.close(i)
#		except:
#			pass
	cmd = ["/usr/bin/qemu", "/home/mihas/qemu-miha/debian1.img"]
	os.execl("/usr/bin/qemu", *cmd)


if __name__ == "__main__":
	main()
