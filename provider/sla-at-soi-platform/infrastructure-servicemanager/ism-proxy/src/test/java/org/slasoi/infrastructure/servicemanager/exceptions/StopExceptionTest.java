package org.slasoi.infrastructure.servicemanager.exceptions;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Exception thrown when there is a problem with stop.
 * 
 * @author Patrick Cheevers
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class StopExceptionTest {
    private static final Logger LOGGER = Logger.getLogger(StopException.class.getName());

    @Test
    public void testthrow() {

        try {
            throw new StopException();
        } catch (Exception e) {
            if (!(e instanceof StopException))

            {
                LOGGER.error("Exception is not Instance of StopException");
                fail();
            }
            // if (!Stop_EXCEPTION_TEST.equalsIgnoreCase(e.getMessage()))
            //
            // {
            // logger.error("Message doesnt equate to "
            // + Stop_EXCEPTION_TEST);
            // fail();
            // }
        }

    }

}
