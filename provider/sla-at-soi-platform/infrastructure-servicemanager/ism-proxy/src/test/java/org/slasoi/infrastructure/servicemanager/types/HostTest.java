package org.slasoi.infrastructure.servicemanager.types;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.types.Host.STATE;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The Host Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class HostTest {

    private static final String ADDRESS = "address";
    private static final String ID = "id";
    private Host host;

    @Before
    public void initialize() {
        host = new Host();

    }

    @Test
    public void computeSetGettest() {

        host.setAddress(ADDRESS);
        host.setId(ID);
        host.setState(STATE.DOWN);
        host.setTimestamp(Long.MIN_VALUE);

        assertEquals(host.getState(), STATE.DOWN);
        assertEquals(host.getAddress(), ADDRESS);
        assertEquals(host.getId(), ID);
        assertEquals(host.getTimestamp(), Long.MIN_VALUE);

    }
}
