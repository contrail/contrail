package org.slasoi.infrastructure.servicemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests The Infrastructure API
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class QueryCapacitytest {

     private static final Logger LOGGER = Logger.getLogger(QueryCapacitytest.class.getName());

    @Autowired
    private org.slasoi.infrastructure.servicemanager.Infrastructure infrastructure;

    @Autowired
    private org.slasoi.common.messaging.pubsub.PubSubManager pubSubManager;

   // @Ignore
    @Test
    public void testremoveall() {
        LOGGER.info("testRemoveall - " + infrastructure);

        HashSet<String> services = (infrastructure.listServicesOrResources(Infrastructure.SERVICE_KEY));
        assertNotNull(services);
        int countServices = 0;
        LOGGER.debug("Computes size=" + services.size());

        for (String service : services) {
            LOGGER.debug("Removing service");
            try {
                infrastructure.release(service);
                countServices++;
            } catch (UnknownIdException e) {
                LOGGER.error(e);
                fail();
            }
            try {
                infrastructure.getDetails(service);
            } catch (UnknownIdException e) {
                countServices--;
                LOGGER.debug("Service Sucessfully deleted");

            }

        }

        HashSet<String> computes = (infrastructure.listServicesOrResources(Infrastructure.COMPUTE_KEY));
        assertNotNull(computes);
        int countComputes = 0;
        LOGGER.debug("Computes size=" + computes.size());
        for (String compute : computes) {
            LOGGER.debug("Removing service");
            try {
                infrastructure.release(compute);
                countComputes++;
            } catch (UnknownIdException e) {
                LOGGER.error(e);
                fail();
            }
            try {
                infrastructure.getDetails(compute);
            } catch (UnknownIdException e) {
                countComputes--;
                LOGGER.debug("Service Sucessfully deleted");

            }

        }
        assertEquals(countComputes, 0);

        HashSet<String> reservations = (infrastructure.listServicesOrResources(Infrastructure.RESERVE_KEY));
        assertNotNull(reservations);
        int countReserves = 0;
        LOGGER.debug("Reserves size=" + reservations.size());

        for (String reservation : reservations) {
            LOGGER.debug("Removing reservation " + reservation);
            try {
                infrastructure.release(reservation);
                countReserves++;
            } catch (UnknownIdException e) {
                LOGGER.error(e);
                fail();
            }
            try {
                infrastructure.getDetails(reservation);
            } catch (UnknownIdException e) {
                countReserves--;
                LOGGER.debug("Reservation Sucessfully deleted");

            }

        }
        assertEquals(countReserves, 0);
    }

    @Test
    public void testqueryCapacity() {

        assertNotNull(infrastructure);
        try {
            List<CapacityResponseType> capacityList = infrastructure.queryCapacity();
            assertNotNull(capacityList);
            assertTrue(capacityList.size() > 0);
            for (CapacityResponseType capacity : capacityList) {
                LOGGER.debug(capacity.toString());
            }

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        } catch (DescriptorException e) {
            LOGGER.error(e);
            fail();
        }
        LOGGER.info("infrastructure - " + infrastructure);
    }

    @Test
    public void testlistServicesOrResources() {
        LOGGER.info("infrastructure - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            HashSet<String> services = infrastructure.listServicesOrResources(Infrastructure.SERVICE_KEY);
            assertNotNull(services);
            for (String service : services) {
                LOGGER.debug(service);
            }
            HashSet<String> resources = infrastructure.listServicesOrResources(Infrastructure.COMPUTE_KEY);
            assertNotNull(resources);
            for (String resource : resources) {
                LOGGER.debug(resource);
            }

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

}
