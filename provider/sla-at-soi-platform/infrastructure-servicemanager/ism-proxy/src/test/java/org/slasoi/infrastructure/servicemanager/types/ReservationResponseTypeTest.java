package org.slasoi.infrastructure.servicemanager.types;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The Reservation Response Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class ReservationResponseTypeTest {

    private static final String INFRASTRUCTURE_ID = "infrastructureId";
    private ReservationResponseType res;

    @Before
    public void initialize() {
        res = new ReservationResponseType();

    }

    @Test
    public void computeSetGettest() {

        res.setInfrastructureID(INFRASTRUCTURE_ID);
        res.setTimeStamp(Long.MIN_VALUE);
        assertEquals(res.getInfrastructureID(), INFRASTRUCTURE_ID);
        assertEquals(res.getTimeStamp(), Long.MIN_VALUE);

    }
}
