package org.slasoi.infrastructure.servicemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.registry.Registry;
import org.slasoi.infrastructure.servicemanager.types.EndPoint;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.slasoi.infrastructure.model.Category;
import eu.slasoi.infrastructure.model.Kind;
import eu.slasoi.infrastructure.model.ProvisionRequest;
import eu.slasoi.infrastructure.model.infrastructure.Compute;
import eu.slasoi.infrastructure.model.infrastructure.Service;

/**
 * Tests The Infrastructure API
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class InfrastructureTest {

    private static String notificationURI = "adjustment@testbed.sla-at-soi.eu.cheevo";
    
    private static final Logger LOGGER = Logger.getLogger(InfrastructureTest.class.getName());

    @Autowired
    org.slasoi.infrastructure.servicemanager.Infrastructure infrastructure;

    @Autowired
    org.slasoi.common.messaging.pubsub.PubSubManager pubSubManager;

    private String monitoringRequest = "";

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        return sb.toString();
    }

    
    public static void removeSpecificProvision(Logger logger, Infrastructure infrastructure,
            ProvisionResponseType provisionResponseType) {
        if ((provisionResponseType == null) || (provisionResponseType.getInfrastructureID() == null)) {
            logger.info("No ids to remove");
        } else {
            logger.info("testRemove Specific - " + provisionResponseType.getInfrastructureID());

            try {
                infrastructure.release(provisionResponseType.getInfrastructureID());
            } catch (UnknownIdException e) {
                logger.error(e);
                fail();
            }
        }
    }

    public static void remove(Logger logger, Infrastructure infrastructure, String serviceId) {
        logger.info("testRemove Specific - " + serviceId);

        try {
            infrastructure.release(serviceId);
        } catch (UnknownIdException e) {
            logger.error(e);
            fail();
        }
    }

    public static void removeSpecificReservation(Logger logger, Infrastructure infrastructure,
            ReservationResponseType responseType) {
        if ((responseType == null) || (responseType.getInfrastructureID() == null)) {
            logger.info("No ids to remove");
        } else {
            logger.info("testRemove Specific - " + responseType.getInfrastructureID());

            try {
                infrastructure.release(responseType.getInfrastructureID());
            } catch (UnknownIdException e) {
                logger.error(e);
                fail();
            }
        }
    }

    @Test
    public void testgetOsregistry() {
        LOGGER.info("testgetOsregistry - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getOsregistry());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory());
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

    @Test
    public void testgetLocregistry() {
        LOGGER.info("testgetLocregistry - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getLocregistry());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory());
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

    @Test
    public void testgetMetricregistry() {
        LOGGER.info("testgetMetricregistry() - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getMetricregistry());
            assertNotNull(infrastructure.getMetricregistry().getDefaultCategory());
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

    @Test
    public void testgetImageregistry() {
        LOGGER.info("testgetImageregistry - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getImageregistry());
            assertNotNull(infrastructure.getImageregistry().getDefaultCategory());
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

   
    public static String getServiceId(Logger logger, ProvisionRequest provisionRequest) {
        Set<Kind> kinds = provisionRequest.getKinds();

        String provisionId = null;
        for (Kind kind : kinds) {

            if (kind instanceof Service) {

                Service serviceKind = (Service) kind;
                provisionId = serviceKind.getServiceId().toString();
                break;

            }
        }
        logger.info("Initial Request service Id - infrastructureID - ");

        return provisionId;
    }

    public static Set<String> getComputeServiceIds(Logger logger, ProvisionRequest provisionRequest) {
        Set<Kind> kinds = provisionRequest.getKinds();

        Set<String> resourceIds = new HashSet<String>();
        String resourceId = null;
        for (Kind kind : kinds) {

            if (kind instanceof Compute) {

                Compute computeKind = (Compute) kind;
                resourceId = computeKind.getUniqueId().toString();

            }
            if (kind instanceof Service) {

                Service serviceKind = (Service) kind;
                resourceId = serviceKind.getServiceId().toString();
            }
            resourceIds.add(resourceId);
        }
        logger.info("GetCompueteIds - " + resourceIds);

        return resourceIds;
    }

    @Test
    public void testlistServicesOrResources() {
        LOGGER.info("infrastructure - " + infrastructure);

        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.listServicesOrResources(Infrastructure.SERVICE_KEY));
            assertNotNull(infrastructure.listServicesOrResources(Infrastructure.COMPUTE_KEY));
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

    @Test
    public void testStopStartResource() {
        ProvisionResponseType provisionResponseType = null;
        try {
            ProvisionRequestType provisionRequest =
                    createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
            provisionResponseType = provision(provisionRequest);

            ProvisionResponseType type = infrastructure.getDetails(provisionResponseType.getInfrastructureID());
            assertNotNull(type);
            LOGGER.info(type.getInfrastructureID());
            List<EndPoint> endPoints = type.getEndPoints();
            for (Iterator<EndPoint> iterator1 = endPoints.iterator(); iterator1.hasNext();) {
                EndPoint endPoint = iterator1.next();

                infrastructure.stopResource(endPoint.getResourceUrl().getPath());

            }
            for (Iterator<EndPoint> iterator2 = endPoints.iterator(); iterator2.hasNext();) {
                EndPoint endPoint = iterator2.next();

                infrastructure.startResource(endPoint.getResourceUrl().getPath());

            }

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        } catch (UnknownIdException e) {
            LOGGER.error(e);
            fail();
        } catch (DescriptorException e) {
            LOGGER.error(e);
            fail();
        } catch (ProvisionException e) {
            LOGGER.error(e);
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);
        }

    }

    @Test
    public void testStopStartService() {
        ProvisionResponseType provisionResponseType = null;
        try {
            ProvisionRequestType provisionRequest =
                    createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
            provisionResponseType = provision(provisionRequest);
            infrastructure.stopResource(provisionResponseType.getInfrastructureID());
            infrastructure.startResource(provisionResponseType.getInfrastructureID());

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        } catch (UnknownIdException e) {
            LOGGER.error(e);
            fail();
        } catch (DescriptorException e) {
            LOGGER.error(e);
            fail();
        } catch (ProvisionException e) {
            LOGGER.error(e);
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);
        }

    }

    @Test
    public void testProvision() {

        LOGGER.info("infrastructure - " + infrastructure);

        ProvisionResponseType provisionResponseType = null;

        assertNotNull(infrastructure);
        try {
            ProvisionRequestType provisionRequest =
                    createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
            provisionResponseType = provision(provisionRequest);

            if (provisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info("provision - infrastructureID - " + provisionResponseType.getInfrastructureID());
            List<EndPoint> endPoints = provisionResponseType.getEndPoints();
            assertNotNull(endPoints);
            Set<String> resourceIds = getComputeServiceIds(LOGGER, provisionRequest);
            for (Iterator<EndPoint> iterator = endPoints.iterator(); iterator.hasNext();) {
                EndPoint endPoint = iterator.next();
                LOGGER.info("	EndPoint - getHostName - " + endPoint.getHostName());
                LOGGER.info("	EndPoint - getResourceUrl - " + endPoint.getResourceUrl());
                String[] ids = endPoint.getId().split("/");
                assertTrue(resourceIds.contains(ids[2]));

            }

        } catch (DescriptorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } catch (ProvisionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);

        }
    }

    ProvisionResponseType provision(ProvisionRequestType provisionRequest) throws DescriptorException,
            ProvisionException {

        LOGGER.info(provisionRequest);
        ProvisionResponseType provisionResponseType = infrastructure.provision(provisionRequest);
        assertNotNull(provisionResponseType);
        assertNotNull(provisionResponseType.getInfrastructureID());
        assertEquals(provisionResponseType.getInfrastructureID(), Infrastructure.SERVICE_KEY
                + getServiceId(LOGGER, provisionRequest));
        return provisionResponseType;

    }

    @Test
    public void testReProvision() {

        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
        String id = null;
        LOGGER.info(provisionRequest);
        ProvisionResponseType provisionResponseType = null;
        assertNotNull(infrastructure);
        try {
            provisionResponseType = infrastructure.provision(provisionRequest);
            id = provisionResponseType.getInfrastructureID();
            assertNotNull(provisionResponseType);
            assertNotNull(provisionResponseType.getInfrastructureID());
            assertEquals(provisionResponseType.getInfrastructureID(), Infrastructure.SERVICE_KEY
                    + getServiceId(LOGGER, provisionRequest));

            if (provisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info("provision - infrastructureID - " + provisionResponseType.getInfrastructureID());
            List<EndPoint> endPoints = provisionResponseType.getEndPoints();
            assertNotNull(endPoints);
            Set<String> resourceIds = getComputeServiceIds(LOGGER, provisionRequest);
            for (Iterator<EndPoint> iterator = endPoints.iterator(); iterator.hasNext();) {
                EndPoint endPoint = iterator.next();
                LOGGER.info("	EndPoint - getHostName - " + endPoint.getHostName());
                LOGGER.info("	EndPoint - getResourceUrl - " + endPoint.getResourceUrl());
                String[] ids = endPoint.getId().split("/");
                assertTrue(resourceIds.contains(ids[2]));

            }
            Set<Kind> kinds = provisionRequest.getKinds();
            Hashtable<String, String> extras = null;
            Registry metricReg = infrastructure.getMetricregistry();
            Category sizeTemplateCategory = metricReg.getDefaultCategory();
            Category updatedsizeTemplateCategory =
                    (Category) metricReg.getList().get(metricReg.getList().size() - 1);

            for (Kind kind : kinds) {

                if (kind instanceof Compute) {
                    Compute val = (Compute) kind;
                    Set<Category> cats = val.getCategories();
                    cats.add(updatedsizeTemplateCategory);
                    cats.remove(sizeTemplateCategory);
                    for (Category cat : cats) {
                        if (cat.equals(sizeTemplateCategory)) {
                            cats.remove(cat);
                            break;
                        }
                    }

                }

            }
            ProvisionResponseType reprovisionResponseType = infrastructure.reprovision(id, provisionRequest);
            assertNotNull(reprovisionResponseType);
            assertNotNull(reprovisionResponseType.getInfrastructureID());
            assertEquals(reprovisionResponseType.getInfrastructureID(), Infrastructure.SERVICE_KEY
                    + getServiceId(LOGGER, provisionRequest));

            if (reprovisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info("reprovision - infrastructureID - " + reprovisionResponseType.getInfrastructureID());
            List<EndPoint> reendPoints = reprovisionResponseType.getEndPoints();
            assertNotNull(reendPoints);
            Set<String> reresourceIds = getComputeServiceIds(LOGGER, provisionRequest);
            for (Iterator<EndPoint> iterator = reendPoints.iterator(); iterator.hasNext();) {
                EndPoint endPoint = iterator.next();
                LOGGER.info("	EndPoint - getHostName - " + endPoint.getHostName());
                LOGGER.info("	EndPoint - getResourceUrl - " + endPoint.getResourceUrl());
                String[] ids = endPoint.getId().split("/");
                assertTrue(resourceIds.contains(ids[2]));

            }
        } catch (DescriptorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } catch (ProvisionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } catch (UnknownIdException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);

        }
    }

    @Test
    public void testCreateVirtualMachineConfiguration() {

        LOGGER.info("infrastructure - " + infrastructure);
        Compute compute = createComputeConfiguraton1();

        assertNotNull(compute);
        Set<Category> categories = compute.getCategories();
        assertNotNull(categories);

        int numOfCategories = 0;
        for (Iterator<Category> iterator = categories.iterator(); iterator.hasNext();) {
            Category category = iterator.next();
            // TODO inspect inside
            numOfCategories++;

        }
        if (numOfCategories == 0)
            fail();

    }

    // @Test
    public void testGetDetails() {

        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
        assertNotNull(infrastructure);
        ProvisionResponseType provisionResponseType = null;
        try {
            provisionResponseType = infrastructure.provision(provisionRequest);
            assertNotNull(provisionResponseType);
            assertNotNull(provisionResponseType.getInfrastructureID());
            assertEquals(provisionResponseType.getInfrastructureID(), Infrastructure.SERVICE_KEY
                    + getServiceId(LOGGER, provisionRequest));

            if (provisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info(provisionResponseType.getInfrastructureID());
            ProvisionResponseType getDetailsResponse =
                    infrastructure.getDetails(provisionResponseType.getInfrastructureID());
            assertNotNull(getDetailsResponse);
            assertNotNull(getDetailsResponse.getInfrastructureID());

        } catch (DescriptorException e) {
            e.printStackTrace();
            LOGGER.error(e);
            fail();
        } catch (UnknownIdException e) {
            LOGGER.error(e);
            fail();
        } catch (ProvisionException e) {

            e.printStackTrace();
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);

        }

    }

    @Test
    public void testcreateProvisionRequestType() {
        createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);

    }

    public static ProvisionRequestType createProvisionRequestType(Logger logger, Infrastructure infrastructure,
            String monitoringRequest) {
        logger.info("infrastructure - " + infrastructure);
        ProvisionRequestType type = null;
        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getOsregistry());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory().getTerm());

            assertNotNull(infrastructure.getMetricregistry());
            assertNotNull(infrastructure.getMetricregistry().getDefaultCategory());
            assertNotNull(infrastructure.getMetricregistry().getDefaultCategory().getTerm());

            assertNotNull(infrastructure.getLocregistry());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory().getTerm());

            type =
                    infrastructure.createProvisionRequestType(

                    infrastructure.getOsregistry().getDefaultCategory().getTerm(), infrastructure
                            .getMetricregistry().getDefaultCategory().getTerm(), infrastructure.getLocregistry()
                            .getDefaultCategory().getTerm(), monitoringRequest, notificationURI);
            assertNotNull(type);
        } catch (Error e) {

            logger.error(e);
            fail();
        }

        return type;

    }

    @Test
    public void testcreateComputeConfiguraton1() {
        createComputeConfiguraton1();
    }

    public Compute createComputeConfiguraton1() {
        Compute compute = null;
        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getOsregistry());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory().getTerm());

            assertNotNull(infrastructure.getMetricregistry());
            assertNotNull(infrastructure.getMetricregistry().getDefaultCategory());
            assertNotNull(infrastructure.getMetricregistry().getDefaultCategory().getTerm());

            assertNotNull(infrastructure.getLocregistry());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory().getTerm());

            compute =
                    infrastructure.createComputeConfiguration(infrastructure.getOsregistry().getDefaultCategory()
                            .getTerm(), infrastructure.getMetricregistry().getDefaultCategory().getTerm(),
                            infrastructure.getLocregistry().getDefaultCategory().getTerm(),
                            new Hashtable<String, String>());

            assertNotNull(compute);
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

        return compute;

    }

    @Test
    public void testcreateComputeConfiguraton2() {
        createComputeConfiguraton2();
    }

    public Compute createComputeConfiguraton2() {
        Compute compute = null;
        try {
            assertNotNull(infrastructure);
            assertNotNull(infrastructure.getOsregistry());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory());
            assertNotNull(infrastructure.getOsregistry().getDefaultCategory().getTerm());

            assertNotNull(infrastructure.getMetricregistry());
            Category metricCat = infrastructure.getMetricregistry().getDefaultCategory();
            if (infrastructure.getMetricregistry().getList().size() > 1)
                ;
            {
                metricCat = (Category) infrastructure.getMetricregistry().getList().get(1);

            }
            assertNotNull(metricCat);
            assertNotNull(metricCat.getTerm());

            assertNotNull(infrastructure.getLocregistry());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory());
            assertNotNull(infrastructure.getLocregistry().getDefaultCategory().getTerm());

            compute =
                    infrastructure.createComputeConfiguration(infrastructure.getOsregistry().getDefaultCategory()
                            .getTerm(), metricCat.getTerm(), infrastructure.getLocregistry().getDefaultCategory()
                            .getTerm(), new Hashtable<String, String>());

            assertNotNull(compute);
        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

        return compute;

    }

    @Test
    public void test2ComputeProvision() {

        Compute vmConfiguration1 = createComputeConfiguraton1();
        Compute vmConfiguration2 = createComputeConfiguraton2();
        Set<Compute> computeConfigurations = new HashSet<Compute>();
        computeConfigurations.add(vmConfiguration1);
        computeConfigurations.add(vmConfiguration2);
        LOGGER.info(vmConfiguration1);
        LOGGER.info(vmConfiguration2);
        ProvisionRequestType provisionRequestType =null;
        ProvisionResponseType provisionResponseType =null;
        try {
            provisionRequestType =
                    infrastructure.createProvisionRequestType(monitoringRequest, computeConfigurations);
            LOGGER.info(provisionRequestType);
           provisionResponseType = infrastructure.provision(provisionRequestType);

            assertNotNull(provisionResponseType);
            assertNotNull(provisionResponseType.getInfrastructureID());
            LOGGER.info(provisionResponseType);
            if (provisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info("provision - infrastructureID - " + provisionResponseType.getInfrastructureID());
            List<EndPoint> endPoints = provisionResponseType.getEndPoints();
            assertNotNull(endPoints);
            for (Iterator<EndPoint> iterator = endPoints.iterator(); iterator.hasNext();) {
                EndPoint endPoint = iterator.next();
                LOGGER.info("	EndPoint - getHostName - " + endPoint.getHostName());
                LOGGER.info("	EndPoint - getResourceUrl - " + endPoint.getResourceUrl());

            }

        } catch (DescriptorException e) {
            e.printStackTrace();
            fail();
        } catch (ProvisionException e) {
            e.printStackTrace();
            fail();
        } finally {
            removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);;

        }

    }

    @Test
    public void testlistReservations() {
        LOGGER.info("List reservations - " + infrastructure);

        try {
            assertNotNull(infrastructure);

            HashSet<String> reservations = infrastructure.listServicesOrResources(Infrastructure.RESERVE_KEY);
            for (String reservation : reservations) {
                LOGGER.debug("Reservation :" + reservation);
            }

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        }

    }

    @Test
    public void testreserve() {
        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
        assertNotNull(infrastructure);
        ReservationResponseType reservationResponseType = null;
        ;
        try {
            reservationResponseType = infrastructure.reserve(provisionRequest);
            assertNotNull(reservationResponseType);
            LOGGER.info("reservationResponseType - " + reservationResponseType);
            assertNotNull(reservationResponseType.getInfrastructureID());
            LOGGER.info("reservationResponseType - infrastructureID - "
                    + reservationResponseType.getInfrastructureID());

            assertEquals(reservationResponseType.getInfrastructureID(), Infrastructure.RESERVE_KEY
                    + getServiceId(LOGGER, provisionRequest));

        } catch (Error e) {

            LOGGER.error(e);
            fail();
        } catch (DescriptorException e) {
            LOGGER.error(e);
            fail();
        } finally {
            removeSpecificReservation(LOGGER, infrastructure, reservationResponseType);

        }

    }

    @Test
    public void testreserveCommit() {
        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);

        ReservationResponseType reservationResponseType = null;

        try {
            reservationResponseType = infrastructure.reserve(provisionRequest);
        } catch (DescriptorException e) {
            // TODO Auto-generated catch block
            LOGGER.error(reservationResponseType);
            fail();
        }
        assertNotNull(reservationResponseType);
        LOGGER.info("reservationResponseType - " + reservationResponseType);

        assertNotNull(reservationResponseType.getInfrastructureID());
        assert (reservationResponseType.getInfrastructureID().startsWith(Infrastructure.RESERVE_KEY));

        LOGGER
                .info("reservationResponseType - infrastructureID - "
                        + reservationResponseType.getInfrastructureID());

        try {
            reservationResponseType = infrastructure.commit(reservationResponseType.getInfrastructureID());
        } catch (UnknownIdException e) {
            // TODO Auto-generated catch block
            LOGGER.error(e);
            fail();
        }
        // **************Note the reservation has become a provisioned
        // service******************
        assert (reservationResponseType.getInfrastructureID().startsWith(Infrastructure.SERVICE_KEY));
        LOGGER.info("reservationResponseType - " + reservationResponseType);
        assertNotNull(reservationResponseType.getInfrastructureID());
        LOGGER
                .info("reservationResponseType - infrastructureID - "
                        + reservationResponseType.getInfrastructureID());

        removeSpecificReservation(LOGGER, infrastructure, reservationResponseType);

    }

    @Test
    public void testReserveRelease() {
        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);

        LOGGER.info(provisionRequest);

        assertNotNull(infrastructure);

        ReservationResponseType reservationResponseType = null;

        try {
            reservationResponseType = infrastructure.reserve(provisionRequest);
        } catch (DescriptorException e) {
            // TODO Auto-generated catch block
            LOGGER.error(reservationResponseType);
            fail();
        }
        assertNotNull(reservationResponseType);
        LOGGER.info("reservationResponseType - " + reservationResponseType);
        assertNotNull(reservationResponseType.getInfrastructureID());
        LOGGER
                .info("reservationResponseType - infrastructureID - "
                        + reservationResponseType.getInfrastructureID());

        LOGGER.info("reservationResponseType - " + reservationResponseType);
        assertNotNull(reservationResponseType.getInfrastructureID());

        try {
            infrastructure.release(reservationResponseType.getInfrastructureID());
        } catch (UnknownIdException e) {
            // TODO Auto-generated catch block
            LOGGER.error(e);
            fail();
        }
    }

}
