package org.slasoi.infrastructure.servicemanager.exceptions;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test Exception thrown when the a problem is found with the format of a request.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class DescriptorExceptionTest {
    private static final String DESCRIPTEST = "DESCRIPTOR Exception Test";
    private static final Logger LOGGER = Logger.getLogger(DescriptorExceptionTest.class.getName());

    @Test
    public void testthrow() {

        try {
            throw new DescriptorException(DESCRIPTEST);
        } catch (Exception e) {
            if (!(e instanceof DescriptorException))

            {
                LOGGER.error("Exception is not Instance of DESCRIPTORException");
                fail();
            }
            if (!DESCRIPTEST.equalsIgnoreCase(e.getMessage()))

            {
                LOGGER.error("Message doesnt equate to " + DESCRIPTEST);
                fail();
            }
        }

    }

}
