package org.slasoi.infrastructure.servicemanager.util.occi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The CategoryPayload Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class CatpayloadTest {

    private static final String RESERVATION_CAT_KEY = "reservationCatKey";
    private static final String RESERVATION_CAT_PAY_LOAD = "reservationCatPayLoad";
    private static final String TYPE = "type";
    private static final String SEVICE_CAT_PAY_LOAD = "seviceCatPayLoad";
    private static final String SEVICE_CAT_KEY = "seviceCatKey";
    private CategoryPayload payLoad1;
    private CategoryPayload payLoad2;
    private HashMap<String, String> computeCatPayloadList1;

    @Before
    public void initialize() {
        payLoad1 = new CategoryPayload();
        payLoad2 = new CategoryPayload();
        computeCatPayloadList1 = new HashMap<String, String>();
        computeCatPayloadList1.put("arg0", "arg1");
        payLoad1.setComputeCatPayloadList(computeCatPayloadList1);
        payLoad1.setReservationCatKey(RESERVATION_CAT_KEY);
        payLoad1.setReservationCatPayLoad(RESERVATION_CAT_PAY_LOAD);
        payLoad1.setSeviceCatKey(SEVICE_CAT_KEY);
        payLoad1.setSeviceCatPayLoad(SEVICE_CAT_PAY_LOAD);
        payLoad1.setType(TYPE);

    }

    @Test
    public void computeSetGettest() {

        assertEquals(payLoad1.getComputeCatPayloadList(), computeCatPayloadList1);
        assertEquals(payLoad1.getReservationCatKey(), RESERVATION_CAT_KEY);
        assertEquals(payLoad1.getReservationCatPayLoad(), RESERVATION_CAT_PAY_LOAD);
        assertEquals(payLoad1.getSeviceCatKey(), SEVICE_CAT_KEY);
        assertEquals(payLoad1.getSeviceCatPayLoad(), SEVICE_CAT_PAY_LOAD);
        assertEquals(payLoad1.getType(), TYPE);

    }

    @Test
    public void equalstest() {

        payLoad2.setComputeCatPayloadList(computeCatPayloadList1);
        payLoad2.setReservationCatKey(RESERVATION_CAT_KEY);
        payLoad2.setReservationCatPayLoad(RESERVATION_CAT_PAY_LOAD);
        payLoad2.setSeviceCatKey(SEVICE_CAT_KEY);
        payLoad2.setSeviceCatPayLoad(SEVICE_CAT_PAY_LOAD);
        payLoad2.setType(TYPE);

        assertEquals(payLoad1, payLoad2);
        assertTrue(payLoad1.equals(payLoad2));

    }
}
