package org.slasoi.infrastructure.servicemanager.prediction;

/**
 * Test Prediction Query Implemenattion class.
 * 
 * @author Patrick Cheevers
 *
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class PredictionQueryImplTest {

    private static final String RESOURCE2 = "resource";
    private static final String HOSTNAME2 = "hostname";
    private static final int PORT2 = 8080;
    private static final Logger LOGGER = Logger.getLogger(PredictionQueryImpl.class.getName());
    PredictionQueryImpl prediction = null;

    @Before
    public void initialize() {
        prediction = new PredictionQueryImpl();
        prediction.setHostname(HOSTNAME2);
        prediction.setPort(PORT2);
        prediction.setResource(RESOURCE2);

    }

    @Test
    public void getPortTest() {
        assertNotNull(prediction);
        assertNotNull(prediction.getPort());
        assertEquals(prediction.getPort(), PORT2);
    }

    @Test
    public void getHostTest() {
        assertNotNull(prediction);
        assertNotNull(prediction.getHostname());
        assertEquals(prediction.getHostname(), HOSTNAME2);
    }

    @Test
    public void getResourceTest() {
        assertNotNull(prediction);
        assertNotNull(prediction.getResource());
        assertEquals(prediction.getResource(), RESOURCE2);
    }

}
