package org.slasoi.infrastructure.servicemanager.exceptions;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test Exception thrown when there is a problem with stopping a resource.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class StopResourceExceptionTest {
    private static final Logger LOGGER = Logger.getLogger(StopResourceExceptionTest.class.getName());

    @Test
    public void testthrow() {

        try {
            throw new StopResourceException();
        } catch (Exception e) {
            if (!(e instanceof StopResourceException))

            {
                LOGGER.error("Exception is not Instance of StopResourceExceptionException");
                fail();
            }

        }

    }

}
