package org.slasoi.infrastructure.servicemanager.types;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.slasoi.infrastructure.model.infrastructure.Compute;

/**
 * Test Appliance Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class ApplianceTest {

    private Appliance<Object> app;
    private Compute compute;
    private Object image;

    @Before
    public void initialize() {
        app = new Appliance<Object>();
        compute = new Compute();
        image = new Object();

    }

    @Test
    public void computeSetGettest() {

        app.setCompute(compute);
        app.setImage(image);

        assertEquals(app.getCompute(), compute);
        assertEquals(app.getImage(), image);

    }
}
