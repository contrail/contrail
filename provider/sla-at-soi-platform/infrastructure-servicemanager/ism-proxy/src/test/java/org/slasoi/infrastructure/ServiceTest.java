package org.slasoi.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.slasoi.infrastructure.model.Category;
import eu.slasoi.infrastructure.model.infrastructure.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
/**
 * Test The service Class.
 * 
 * @author Patrick Cheevers
 *
 */
public class ServiceTest {
    private static final String SCHEME = "scheme";
    private static final String TERM = "term";
    private static final String CORECLASS = "coreclass";
    private Service service1 = null;
    private Service service2 = null;
    private Category cat = null;
    private UUID uniqueId = null;

    @Before
    public void initialize() {
        service1 = new Service();
        Set<Category> categories1 = new HashSet<Category>();
        cat = new Category();
        cat.setScheme(SCHEME);
        cat.setTerm(TERM);
        cat.setCoreClass(CORECLASS);
        categories1.add(cat);
        service1.setCategories(categories1);
        service2 = new Service();
        uniqueId = UUID.randomUUID();

    }

    @Test
    public void serviceIdUniquetest() {
        assert (service1 != null);
        assert (service1.getServiceId() != null);
        assert (service1.getServiceId() instanceof UUID);

        assert (service2 != null);
        assert (service2.getServiceId() != null);
        assert (service2.getServiceId() instanceof UUID);
        assertTrue(service1.getServiceId() != service2.getServiceId());

    }

    @Test
    public void serviceCatgegoiresTest() {
        assert (service1 != null);
        assert (service1.getCategories() != null);
        assert (service1.getCategories().size() == 1);
        assert (service1.getCategories().contains(cat));

    }

    @Test
    public void computeSetGettest() {

        service1.setServiceId(uniqueId);

        assert (service1 != null);
        assertEquals(service1.getServiceId(), uniqueId);

    }
}
