package org.slasoi.infrastructure.servicemanager.exceptions;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test Exception thrown when there is a problem with a query.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class QueryExceptionTest {
    private static final String QUERY_EXCEPTION_TEST = "Query Exception Test";
    private static final Logger LOGGER = Logger.getLogger(QueryExceptionTest.class.getName());

    @Test
    public void testthrow() {

        try {
            throw new QueryException(QUERY_EXCEPTION_TEST);
        } catch (Exception e) {
            if (!(e instanceof QueryException))

            {
                LOGGER.error("Exception is not Instance of QueryException");
                fail();
            }
            if (!QUERY_EXCEPTION_TEST.equalsIgnoreCase(e.getMessage()))

            {
                LOGGER.error("Message doesnt equate to " + QUERY_EXCEPTION_TEST);
                fail();
            }
        }

    }

}
