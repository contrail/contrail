package org.slasoi.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.slasoi.infrastructure.model.Category;
import eu.slasoi.infrastructure.model.infrastructure.Compute;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
/**
 * Test The compute Class.
 * 
 * @author Patrick Cheevers
 *
 */
public class ComputeTest {
    private static final String VALUE = "value";
    private static final String KEY = "key";
    private static final String MAC_ADDRESS = "MacAddress";
    private static final String STATUS = "status";
    private static final String MEMORY_RELIABILITY = "Memory_Reliability";
    private static final float _2 = 2;
    private static final int _1 = 1;
    private static final String CPU_ARCH = "cpu_arch";
    private static final String BOOT_VOL_TYPE = "boot_Vol_type";
    private static final String SCHEME = "scheme";
    private static final String TERM = "term";
    private static final String CORECLASS = "coreclass";
    private Compute compute1 = null;
    private Compute compute2 = null;
    private Category cat = null;
    private UUID uniqueId = null;
    Hashtable<String, String> extras = null;

    @Before
    public void initialize() {
        compute1 = new Compute();
        Set<Category> categories1 = new HashSet<Category>();
        cat = new Category();
        cat.setScheme(SCHEME);
        cat.setTerm(TERM);
        cat.setCoreClass(CORECLASS);
        categories1.add(cat);
        compute1.setCategories(categories1);
        compute2 = new Compute();
        uniqueId = UUID.randomUUID();
        extras = new Hashtable<String, String>();
        extras.put(KEY, VALUE);

    }

    @Test
    public void computeIdUniquetest() {
        assert (compute1 != null);
        assert (compute1.getUniqueId() != null);
        assert (compute1.getUniqueId() instanceof UUID);

        assert (compute2 != null);
        assert (compute2.getUniqueId() != null);
        assert (compute2.getUniqueId() instanceof UUID);
        assertTrue(compute1.getUniqueId() != compute2.getUniqueId());

    }

    @Test
    public void computeCatgegoiresTest() {
        assert (compute1 != null);
        assert (compute1.getCategories() != null);
        assert (compute1.getCategories().size() == 1);
        assert (compute1.getCategories().contains(cat));

    }

    @Test
    public void computeSetGettest() {
        // compute1.setBoot_vol_type(BOOT_VOL_TYPE);
        // compute1.setCpu_arch(CPU_ARCH);
        // compute1.setCpu_cores(_1);
        // compute1.setCpu_speed(Float.MIN_VALUE);
        compute1.setUniqueId(uniqueId);
        // compute1.setMemory_reliability(MEMORY_RELIABILITY);
        // compute1.setMemory_size(Float.MIN_VALUE);
        // compute1.setMemory_speed(Float.MIN_VALUE);
        compute1.setVersion(Long.MIN_VALUE);
        compute1.setStatus(STATUS);
        compute1.setMacAddress(MAC_ADDRESS);

        compute1.setExtras(extras);

        assert (compute1 != null);
        // assertEquals(compute1.getBoot_vol_type(), BOOT_VOL_TYPE);
        // assertEquals(compute1.getCpu_arch(), CPU_ARCH);
        // assertEquals(compute1.getCpu_cores(), _1);
        // assertTrue(compute1.getCpu_speed()==Float.MIN_VALUE);
        assertEquals(compute1.getUniqueId(), uniqueId);
        // assertEquals(compute1.getMemory_reliability(), MEMORY_RELIABILITY);
        // assertTrue(compute1.getMemory_size()==Float.MIN_VALUE);
        // assertTrue(compute1.getMemory_speed()==Float.MIN_VALUE);
        assertTrue(compute1.getVersion() == Long.MIN_VALUE);
        assertEquals(compute1.getStatus(), STATUS);
        assertEquals(compute1.getMacAddress(), MAC_ADDRESS);
        assertEquals(compute1.getExtras(), extras);

    }

}
