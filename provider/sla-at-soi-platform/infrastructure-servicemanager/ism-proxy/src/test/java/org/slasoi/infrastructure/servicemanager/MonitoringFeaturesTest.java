package org.slasoi.infrastructure.servicemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class MonitoringFeaturesTest {

    private static final Logger LOGGER = Logger.getLogger(MonitoringFeaturesTest.class.getName());

    @Autowired
    org.slasoi.infrastructure.servicemanager.Infrastructure infrastructure;

    @Autowired
    org.slasoi.common.messaging.pubsub.PubSubManager pubSubManager;

    @Test
    public void testGetMonitoringFeatures() {

        try {

            ComponentMonitoringFeatures[] cmf = infrastructure.getMonitoringFeatures(3000);
            assertNotNull(cmf);
            LOGGER.debug("Checking MonitoringFeaturesResponse...");
            assertEquals(cmf.length, 3);
            assertEquals(cmf[0].getType(), "REASONER");
            assertEquals(cmf[1].getType(), "SENSOR");
            assertEquals(cmf[2].getType(), "SENSOR");

            assertEquals(cmf[0].getMonitoringFeaturesList().size(), 45);
            assertEquals(cmf[1].getMonitoringFeaturesList().size(), 2);
            assertEquals(cmf[2].getMonitoringFeaturesList().size(), 15);

            MonitoringFeature mf = cmf[0].getMonitoringFeatures(0);
            assertEquals(mf.getName(), "http://www.slaatsoi.org/coremodel#series");
            assertEquals(mf.getDescription(), "Reasoner for Series of BOOLEAN");
            LOGGER.debug("testPubSubCommunication() finished successfully.");

        } catch (DescriptorException e) {
            e.printStackTrace();
            fail();
        } catch (ProvisionException e) {
            e.printStackTrace();
            fail();
        } catch (Exception e) {

            e.printStackTrace();
            fail();
        }

    }

}
