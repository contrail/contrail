package org.slasoi.infrastructure.servicemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.types.EndPoint;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests The Infrastructure API
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class ProvisionWithMonitoringTest {

     private static final Logger LOGGER = Logger.getLogger(ProvisionWithMonitoringTest.class.getName());

    @Autowired
    private org.slasoi.infrastructure.servicemanager.Infrastructure infrastructure;

    @Autowired
   private  org.slasoi.common.messaging.pubsub.PubSubManager pubSubManager;

    private String monitoringRequest = "";

    @Before
    public void initialize() {
        try {
            InputStream is =
                    ProvisionWithMonitoringTest.class.getResourceAsStream("MonitoringSystemConfigurationImpl.ser");

            monitoringRequest = convertStreamToString(is);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        return sb.toString();
    }

    @Test
    public void testProvision() {

        LOGGER.info("infrastructure - " + infrastructure);
        ProvisionRequestType provisionRequest =
                InfrastructureTest.createProvisionRequestType(LOGGER, infrastructure, monitoringRequest);
        String id = null;
        LOGGER.info(provisionRequest);
        ProvisionResponseType provisionResponseType =null;
        assertNotNull(infrastructure);
        try {
           provisionResponseType = infrastructure.provision(provisionRequest);

            assertNotNull(provisionResponseType);
            assertNotNull(provisionResponseType.getInfrastructureID());
            assertEquals(provisionResponseType.getInfrastructureID(), Infrastructure.SERVICE_KEY
                    + InfrastructureTest.getServiceId(LOGGER, provisionRequest));

            if (provisionResponseType.getEndPoints().size() == 0) {
                fail();
            }
            LOGGER.info("provision - infrastructureID - " + provisionResponseType.getInfrastructureID());
            List<EndPoint> endPoints = provisionResponseType.getEndPoints();
            assertNotNull(endPoints);
            Set<String> resourceIds = InfrastructureTest.getComputeServiceIds(LOGGER, provisionRequest);
            for (Iterator<EndPoint> iterator = endPoints.iterator(); iterator.hasNext();) {
                EndPoint endPoint = iterator.next();
                LOGGER.info("	EndPoint - getHostName - " + endPoint.getHostName());
                LOGGER.info("	EndPoint - getResourceUrl - " + endPoint.getResourceUrl());
                String[] ids = endPoint.getId().split("/");
                assertTrue(resourceIds.contains(ids[2]));

            }
           
        } catch (DescriptorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        } catch (ProvisionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        }finally
        {
            InfrastructureTest.removeSpecificProvision(LOGGER, infrastructure, provisionResponseType);

        }
    }

}
