package eu.slasoi.infrastructure.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The CapacityResponse Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class CapacityResponseTest {

    private static final String TYPE2 = "type2";
    private static final String TYPE = "type";
    private static final int AVAIL = 4;

    private static final int CORES = 2;

    private CapacityResponse capacityResponse2;
    private CapacityResponse capacityResponse1;

    @Before
    public void initialize() {

        capacityResponse1 = new CapacityResponse(TYPE, Float.MAX_VALUE, CORES, Float.MIN_VALUE, AVAIL);
        capacityResponse2 = new CapacityResponse(TYPE, Float.MAX_VALUE, CORES, Float.MIN_VALUE, AVAIL);

    }

    @Test
    public void computeSetGettest() {
        assertEquals(capacityResponse1.getAvail(), AVAIL);
        assertEquals(capacityResponse1.getCores(), CORES);
        assertTrue(Float.MAX_VALUE == capacityResponse1.getMemory());
        assertTrue(Float.MIN_VALUE == capacityResponse1.getSpeed());
        assertEquals(capacityResponse1.getType(), TYPE);
        assertEquals(capacityResponse1, capacityResponse2);
        assertTrue(capacityResponse1.equals(capacityResponse2));
        assertEquals(capacityResponse1.toString(), capacityResponse2.toString());

        capacityResponse2.setType(TYPE2);
        capacityResponse2.setAvail(Integer.MAX_VALUE);

        assertEquals(capacityResponse2.getType(), TYPE2);
        assertEquals(capacityResponse2.getAvail(), Integer.MAX_VALUE);

    }
}
