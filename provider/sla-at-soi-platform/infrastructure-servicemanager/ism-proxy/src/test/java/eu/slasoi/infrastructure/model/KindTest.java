package eu.slasoi.infrastructure.model;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The Kind Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class KindTest {

    private static final String TYPE = "type";
    private static final String RESOURCE_ID = "resourceId";
    private static final String HOSTNAME = "hostname";
    private static final String ADDRESS = "address";
    private static final String ID = "id";
    private Kind kind;
    private Set<Category> categories;
    private Category cat;

    @Before
    public void initialize() {
        kind = new Kind();
        categories = new HashSet<Category>();
        cat = new Category();
        categories.add(cat);
    }

    @Test
    public void computeSetGettest() {

        kind.setCategories(categories);
        kind.setHostname(HOSTNAME);
        kind.setResourceId(RESOURCE_ID);
        kind.setType(TYPE);

        assertEquals(kind.getCategories(), categories);
        assertEquals(kind.getHostname(), HOSTNAME);
        assertEquals(kind.getResourceId(), RESOURCE_ID);
        assertEquals(kind.getType(), TYPE);

    }
}
