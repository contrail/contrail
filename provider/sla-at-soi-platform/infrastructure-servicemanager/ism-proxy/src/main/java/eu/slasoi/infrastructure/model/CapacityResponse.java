package eu.slasoi.infrastructure.model;

/**
 * ProvisionRequest representation. This object is a domain specific representation of a @CapacityResponse. The object
 * is designed to facilitate the representation and translation of SLA functional capacity terms to their equivalent
 * OCCI representation.
 * 
 * @author Patrick Cheevers
 */
public class CapacityResponse extends MetricImpl implements IMetric {

    /**
     * Constructor
     * 
     * @param type
     * @param memory
     * @param cores
     * @param speed
     * @param avail
     */
    public CapacityResponse(final String type, final float memory, final int cores, final float speed,
            final int avail) {
        super(memory, cores, speed);

        this.type = type;

        this.avail = avail;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + avail;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CapacityResponse))
            return false;
        CapacityResponse other = (CapacityResponse) obj;
        if (avail != other.avail)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CapacityResponse [avail=" + avail + ", type=" + type + ", getCores()=" + getCores()
                + ", getMemory()=" + getMemory() + ", getSpeed()=" + getSpeed() + ", toString()="
                + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getAvail() {
        return avail;
    }

    public void setAvail(final int avail) {
        this.avail = avail;
    }

    private String type; // occi.small.available, occi.medium.available, occi.large.available;

    private int avail;

}
