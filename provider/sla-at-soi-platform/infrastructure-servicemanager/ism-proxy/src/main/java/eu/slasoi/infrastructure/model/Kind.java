package eu.slasoi.infrastructure.model;

import java.util.Set;

/**
 * Model representation. Each @ProvisionRequest will contain a @Set of @Kind.
 * 
 * @author sla
 * 
 */
public class Kind {

    /**
     * the host name associated with this @Kind.
     */
    private String hostname;

    /**
     * The @Category @Set associated with this @Kind.
     */
    private Set<Category> categories;

    /**
     * The resource ID of this @Kind.
     */
    private String resourceId;

    /**
     * The type of this @Kind.
     */
    private String type;

    /**
     * Returns the @Set of @Category associated with this @Kind.
     * 
     * @return a @Set of @Category
     */
    public Set<Category> getCategories() {
        return categories;
    }

    /**
     * Sets the @Category @Set associated with this @Kind.
     * 
     * @param categories
     */
    public void setCategories(final Set<Category> categories) {
        this.categories = categories;
    }

    /**
     * Gets the host name associated with this @Kind.
     * 
     * @return hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Sets the host name associated with this @Kind.
     * 
     * @param hostname
     *            The host name.
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * Gets the resource ID associated with this @Kind.
     * 
     * @return resourceId;
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the resource ID associated with this @Kind.
     * 
     * @param resourceId
     * 
     *            The resource id.
     */
    public void setResourceId(final String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * Gets the type associated with this @Kind.
     * 
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type associated with this @Kind.
     * 
     * @param type
     *            The type.
     */
    public void setType(final String type) {
        this.type = type;
    }

}
