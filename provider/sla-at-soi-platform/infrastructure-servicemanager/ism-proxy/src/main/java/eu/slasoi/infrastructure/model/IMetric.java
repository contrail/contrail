package eu.slasoi.infrastructure.model;

/**
 * Interface which describes the metrics of a compute resource.
 * 
 * @author pcheev1X
 * 
 */
public interface IMetric {

    public abstract float getMemory();

    public abstract void setMemory(float memory);

    public abstract int getCores();

    public abstract void setCores(int cores);

    public abstract float getSpeed();

    public abstract void setSpeed(float speed);

}
