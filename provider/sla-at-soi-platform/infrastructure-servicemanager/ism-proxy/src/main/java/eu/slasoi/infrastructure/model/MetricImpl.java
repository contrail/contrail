package eu.slasoi.infrastructure.model;

/**
 * ProvisionRequest representation. This object is a domain specific representation of a @Compute Metric. The object is
 * designed to facilitate the representation and translation of SLA functional capacity terms to their equivalent OCCI
 * representation.
 * 
 * @author sla
 */
public class MetricImpl implements Comparable<IMetric>, IMetric {

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + cores;
        result = prime * result + Float.floatToIntBits(memory);
        result = prime * result + Float.floatToIntBits(speed);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof MetricImpl))
            return false;
        MetricImpl other = (MetricImpl) obj;
        if (cores != other.cores)
            return false;
        if (Float.floatToIntBits(memory) != Float.floatToIntBits(other.memory))
            return false;
        if (Float.floatToIntBits(speed) != Float.floatToIntBits(other.speed))
            return false;
        return true;
    }

    public MetricImpl(float memory, int cores, float speed) {

        this.memory = memory;
        this.cores = cores;
        this.speed = speed;

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MetricImpl [cores=" + cores + ", memory=" + memory + ", speed=" + speed + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#getMemory()
     */
    public float getMemory() {
        return memory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#setMemory(float)
     */
    public void setMemory(final float memory) {
        this.memory = memory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#getCores()
     */
    public int getCores() {
        return cores;
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#setCores(int)
     */
    public void setCores(final int cores) {
        this.cores = cores;
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#getSpeed()
     */
    public float getSpeed() {
        return speed;
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.slasoi.infrastructure.model.IMetric#setSpeed(float)
     */
    public void setSpeed(final float speed) {
        this.speed = speed;
    }

    private float memory; // {float}=0.5
    private int cores; // {int}=1
    private float speed; // {float}=2.4

    @Override
    public int compareTo(final IMetric obj) {

        // Very Rough comparator
        // Necessary becuae OCCI Server cannott handle calculating capacity in reservation system
        // unless simple small medium large requests are defined
        if (this == obj)
            return 0;
        if (obj == null)
            return 1;
        if (!(obj instanceof MetricImpl))
            return 1;
        MetricImpl other = (MetricImpl) obj;
        int coresResult = 0;
        if (cores < other.cores) {
            coresResult = -1;
        } else {
            coresResult = +1;
        }
        int memoryResult = 0;
        if (Float.floatToIntBits(memory) < Float.floatToIntBits(other.memory)) {
            memoryResult = -1;
        } else {
            memoryResult = 1;
        }

        int speedResult = 0;
        if (Float.floatToIntBits(speed) < Float.floatToIntBits(other.speed)) {
            speedResult = -1;
        } else {
            speedResult = 1;
        }
        int overAllResult = 0;

        switch (speedResult + coresResult + memoryResult) {
        case 3:
            overAllResult = 1;
            break;
        case -3:
            overAllResult = -1;
            break;

        default:
            overAllResult = 0;

        }
        return overAllResult;
    }

}
