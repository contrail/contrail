package org.slasoi.infrastructure.servicemanager.types;

import eu.slasoi.infrastructure.model.ProvisionRequest;

/**
 * @author Victor
 *
 */
public class ProvisionRequestType extends ProvisionRequest {
    private static final long serialVersionUID = -191393358407254682L;
}