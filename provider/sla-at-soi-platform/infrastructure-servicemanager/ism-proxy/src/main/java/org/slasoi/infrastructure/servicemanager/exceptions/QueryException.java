package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with a query.
 * 
 * @author sla
 * 
 */
public class QueryException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7832945944239655667L;

    /**
     * Creates a new QueryException.
     * 
     * @param error
     *            if detected, the possible description of the error.
     */
    public QueryException(final String error) {
        super(error);
    }

}
