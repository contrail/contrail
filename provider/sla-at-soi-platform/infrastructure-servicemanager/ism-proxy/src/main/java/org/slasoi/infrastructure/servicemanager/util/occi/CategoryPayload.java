package org.slasoi.infrastructure.servicemanager.util.occi;

import java.util.HashMap;

/**
 * This a helper Class used to store information about the pay-load that will be posted to the restful web service.
 * 
 * @author Patrick Cheevers
 * 
 */
public class CategoryPayload {
    private HashMap<String, String> computeCatPayloadList = new HashMap<String, String>();
    private String seviceCatPayLoad = null;
    private String seviceCatKey = null;
    private String reservationCatPayLoad = null;
    private String reservationCatKey = null;

    /**
     * @return the seviceCatKey
     */
    public String getSeviceCatKey() {
        return seviceCatKey;
    }

    /**
     * @param seviceCatKey
     *            the seviceCatKey to set
     */
    public void setSeviceCatKey(final String seviceCatKey) {
        this.seviceCatKey = seviceCatKey;
    }

    /**
     * @return the reservationCatKey
     */
    public String getReservationCatKey() {
        return reservationCatKey;
    }

    /**
     * @param reservationCatKey
     *            the reservationCatKey to set
     */
    public void setReservationCatKey(final String reservationCatKey) {
        this.reservationCatKey = reservationCatKey;
    }

    private String type = null;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the reservationCatPayLoad
     */
    public String getReservationCatPayLoad() {
        return reservationCatPayLoad;
    }

    /**
     * @param reservationCatPayLoad
     *            the reservationCatPayLoad to set
     */
    public void setReservationCatPayLoad(final String reservationCatPayLoad) {
        this.reservationCatPayLoad = reservationCatPayLoad;
    }

    /**
     * @return the computeCatPayloadList
     */
    public HashMap<String, String> getComputeCatPayloadList() {
        return computeCatPayloadList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((computeCatPayloadList == null) ? 0 : computeCatPayloadList.hashCode());
        result = prime * result + ((reservationCatKey == null) ? 0 : reservationCatKey.hashCode());
        result = prime * result + ((reservationCatPayLoad == null) ? 0 : reservationCatPayLoad.hashCode());
        result = prime * result + ((seviceCatKey == null) ? 0 : seviceCatKey.hashCode());
        result = prime * result + ((seviceCatPayLoad == null) ? 0 : seviceCatPayLoad.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CategoryPayload))
            return false;
        CategoryPayload other = (CategoryPayload) obj;
        if (computeCatPayloadList == null) {
            if (other.computeCatPayloadList != null)
                return false;
        } else if (!computeCatPayloadList.equals(other.computeCatPayloadList))
            return false;
        if (reservationCatKey == null) {
            if (other.reservationCatKey != null)
                return false;
        } else if (!reservationCatKey.equals(other.reservationCatKey))
            return false;
        if (reservationCatPayLoad == null) {
            if (other.reservationCatPayLoad != null)
                return false;
        } else if (!reservationCatPayLoad.equals(other.reservationCatPayLoad))
            return false;
        if (seviceCatKey == null) {
            if (other.seviceCatKey != null)
                return false;
        } else if (!seviceCatKey.equals(other.seviceCatKey))
            return false;
        if (seviceCatPayLoad == null) {
            if (other.seviceCatPayLoad != null)
                return false;
        } else if (!seviceCatPayLoad.equals(other.seviceCatPayLoad))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CategoryPayload [computeCatPayloadList=" + computeCatPayloadList + ", reservationCatKey="
                + reservationCatKey + ", reservationCatPayLoad=" + reservationCatPayLoad + ", seviceCatKey="
                + seviceCatKey + ", seviceCatPayLoad=" + seviceCatPayLoad + ", type=" + type + "]";
    }

    /**
     * @param computeCatPayloadList
     *            the computeCatPayloadList to set
     */
    public void setComputeCatPayloadList(final HashMap<String, String> computeCatPayloadList) {
        this.computeCatPayloadList = computeCatPayloadList;
    }

    /**
     * @return the seviceCatPayLoad
     */
    public String getSeviceCatPayLoad() {
        return seviceCatPayLoad;
    }

    /**
     * @param seviceCatPayLoad
     *            the seviceCatPayLoad to set
     */
    public void setSeviceCatPayLoad(final String seviceCatPayLoad) {
        this.seviceCatPayLoad = seviceCatPayLoad;
    }

}
