package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with stop.
 * 
 * @author sla
 * 
 */
public class StopException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5698991381751800988L;

}
