package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with a provision.
 * 
 * @author sla
 * 
 */
public class ProvisionException extends Exception {
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7832945944239655667L;

    /**
     * Creates a new ProvisionException.
     * 
     * @param error
     *            If detected, the possible description of the error.
     */
    public ProvisionException(final String error) {
        super(error);
    }

}
