package org.slasoi.infrastructure.servicemanager.types;

/**
 * @author Victor
 *
 */
public class Host {
    private String id;
    private String address;
    private long timestamp;

    public static enum STATE {
        UP,
        DOWN,
        NEW,
        UNKNOWN
    };

    private STATE state;

    public Host() {

    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public STATE getState() {
        return state;

    }

    public void setState(STATE state) {
        this.state = state;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}