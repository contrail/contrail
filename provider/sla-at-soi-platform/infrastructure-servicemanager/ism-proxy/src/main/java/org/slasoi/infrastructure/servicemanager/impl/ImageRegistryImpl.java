package org.slasoi.infrastructure.servicemanager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.io.ICategoryMetadata;

import eu.slasoi.infrastructure.model.Category;

/**
 * Prototype Implementation of a Image Registry.
 * 
 * @author sla
 * 
 */
public final class ImageRegistryImpl extends Reg {
    private static final Logger LOGGER = Logger.getLogger(ImageRegistryImpl.class.getName());

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#setup(org.jclouds.entities.domain.IPageSet)
     */
    @Override
    public void setup(final IPageSet<? extends IGrpResourceAvailMetadata> resources) {
        List<Category> catList = new ArrayList<Category>();
        Category cat = null;

        for (IGrpResourceAvailMetadata resourceMd : resources) {

            String groupId = resourceMd.getName();
            LOGGER.debug("listing avail resources " + groupId);
            ICategoryMetadata md = resourceMd.getCategoryMetaData();
            cat = new Category();
            // cat.setId(id);//later
            // cat.setLabel(label)//later
            cat.setCoreClass(md.getCoreClass());
            cat.setScheme(md.getScheme());
            cat.setTerm(md.getTerm());
            cat.setTitle(md.getTitle());
            catList.add(cat);

            LOGGER.debug("Category metadata :" + md.toString());

        }
        LOGGER.debug("Template List" + catList.toString());

        this.setList(catList);
    }

}
