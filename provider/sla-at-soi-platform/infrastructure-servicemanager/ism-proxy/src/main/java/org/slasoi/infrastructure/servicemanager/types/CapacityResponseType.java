package org.slasoi.infrastructure.servicemanager.types;

import java.io.Serializable;

import eu.slasoi.infrastructure.model.CapacityResponse;

/**
 * Capacity Response
 * @author Patrick cheevers
 *
 */
public class CapacityResponseType extends CapacityResponse implements Serializable {

    public CapacityResponseType(final String type, final float memory, final int cores, final float speed, final int avail) {
        super(type, memory, cores, speed, avail);

    }

    /**
     * 
     */
    private static final long serialVersionUID = 3416407468152803269L;

}