package org.slasoi.infrastructure.servicemanager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.io.ICategoryMetadata;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import eu.slasoi.infrastructure.model.Category;

/**
 * Registry of metrics.
 * @author Patrick cheevers
 * 
 */
public class MetricRegistryImpl extends Reg {
    private static final Logger LOGGER = Logger.getLogger(MetricRegistryImpl.class.getName());
    private MetricDetailRegistryImpl metricDetailReg = new MetricDetailRegistryImpl();

    /*
     * Populates the list with all metric Types details
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#setup(org. jclouds.entities.domain.IPageSet)
     */
    public void setup(final IPageSet<? extends IGrpResourceAvailMetadata> resources) {

        String scheme = null;
        String coreClass = null;
        String term = null;
        Category cat = null;
        List<Category> newlist = new ArrayList<Category>();
        for (IGrpResourceAvailMetadata resourceMd : resources) {
            ICategoryMetadata metaData = resourceMd.getCategoryMetaData();
            scheme = metaData.getScheme();
            coreClass = metaData.getCoreClass();
            term = metaData.getTerm();

            if ((SchemaConstants.HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_RES_TEMPLATE.equalsIgnoreCase(scheme))
                    &&

                    (("small".equalsIgnoreCase(term)) || ("medium".equalsIgnoreCase(term)) || ("large"
                            .equalsIgnoreCase(term)))) {

                cat = new Category();
                cat.setCoreClass(coreClass);
                cat.setScheme(scheme);
                cat.setTerm(term);
                cat.setTitle(metaData.getTitle());
                newlist.add(cat);

            }
            LOGGER.debug("OS Templates" + newlist.toString());

            this.setList(newlist);
            metricDetailReg.setup(resources);

        }

    }

    public Category getDefaultCategory() {
        Category defaultCat = null;
        for (Category cat : this.getList()) {
            if (cat.getTerm().equals(this.metricDetailReg.getDefaultType())) {
                defaultCat = cat;
                LOGGER.debug("Default Category is " + defaultCat.toString());
                break;

            }

        }
        return defaultCat;

    }

    /**
     * @return the metricDetailReg
     */
    public MetricDetailRegistryImpl getMetricDetailReg() {
        return metricDetailReg;
    }

    /**
     * @param metricDetailReg
     *            the metricDetailReg to set
     */
    public void setMetricDetailReg(final MetricDetailRegistryImpl metricDetailReg) {
        this.metricDetailReg = metricDetailReg;
    }
}
