package org.slasoi.infrastructure.servicemanager.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IService;
import org.jclouds.entities.domain.IServiceMetadata;
import org.jclouds.entities.io.IActionMetadata;
import org.slasoi.infrastructure.servicemanager.types.EndPoint;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import com.google.common.base.Function;

/**
 * Converts a service @See IService to ProvisionResponseType @See IProvisionrResponseType.
 * 
 * @author Patrick Cheevers
 */
public class Service2ProvisionResponseType implements Function<IService, ProvisionResponseType> {
    private static final Logger LOGGER = Logger.getLogger(Service2ProvisionResponseType.class.getName());
    
    /*
     * Converts a service to ProvisionResponseType
     * 
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public ProvisionResponseType apply(final IService from) {
        if (from == null)
            return null;

        List<EndPoint> endPoints = new ArrayList<EndPoint>();

        EndPoint endPoint = new EndPoint();
        IServiceMetadata meta = from.getMetadata();
        ProvisionResponseType provisionResponseType = new ProvisionResponseType();

        Set<IActionMetadata> actionlist = meta.getActionMetaDataList();
        for (IActionMetadata actionMd : actionlist) {

            // System.out.printf("actionData %s %n", actionMd.toString());
            String rel = actionMd.getRel();
            if (rel.equalsIgnoreCase(SchemaConstants.HTTP_SCHEMAS_OGF_ORG_OCCI_INFRASTRUCTURE_COMPUTE)) {

                endPoint = createEndPoint(actionMd.getTarget());
                if (endPoint != null) {
                    endPoints.add(endPoint);
                }
            }
            if (rel.equalsIgnoreCase(SchemaConstants.HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_START)) {

                endPoint = createEndPoint(actionMd.getTarget());
                if (endPoint != null) {
                    endPoints.add(endPoint);
                }
                // provisionResponseType.setInfrastructureID(endPoint.getResourceUrl().getPath());
            }

        }

        provisionResponseType.setEndPoints(endPoints);

        return provisionResponseType;

    }

   public static EndPoint createEndPoint(final String location) {
        URL urlLocation = null;
        EndPoint endPoint = null;
        try {
            urlLocation = new URL(location);
            endPoint = new EndPoint();
            endPoint.setId(urlLocation.getPath());
            endPoint.setHostName(urlLocation.getHost());
            endPoint.setResourceUrl(urlLocation);
        } catch (MalformedURLException e) {
            LOGGER.error("Create EndPoint failed error location" + e.getMessage());
        }

        return endPoint;
    }
}