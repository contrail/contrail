package org.slasoi.infrastructure.servicemanager.types;

import java.net.URL;

/**
 * @author Victor
 *
 */
public class EndPoint {
    @Override
    public String toString() {
        return "EndPoint [appliance=" + appliance + ", hostName=" + hostName + ", id=" + id + ", resourceUrl="
                + resourceUrl + "]";
    }

    private String id;
    private Appliance<String> appliance;
    private String hostName;
    private URL resourceUrl;

    public URL getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(final URL resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public Appliance<String> getAppliance() {
        return appliance;
    }

    public void setAppliance(final Appliance<String> appliance) {
        this.appliance = appliance;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(final String hostName) {
        this.hostName = hostName;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}