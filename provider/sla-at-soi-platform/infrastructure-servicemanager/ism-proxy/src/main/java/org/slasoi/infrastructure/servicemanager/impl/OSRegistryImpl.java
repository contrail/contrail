package org.slasoi.infrastructure.servicemanager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.io.ICategoryMetadata;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import eu.slasoi.infrastructure.model.Category;

/**
 * Prototype Implementation of a OS Registry.
 * 
 * @author Patrick Cheevers
 * 
 */
public final class OSRegistryImpl extends Reg {
    private static final Logger LOGGER = Logger.getLogger(OSRegistryImpl.class.getName());

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#setup(org.jclouds.entities.domain.IPageSet)
     */
    public void setup(final IPageSet<? extends IGrpResourceAvailMetadata> resources) {

        String scheme = null;
        String coreClass = null;
        String term = null;
        Category cat = null;
        List<Category> newlist = new ArrayList<Category>();
        for (IGrpResourceAvailMetadata resourceMd : resources) {
            ICategoryMetadata md = resourceMd.getCategoryMetaData();
            // rel = md.getRel();
            scheme = md.getScheme();
            coreClass = md.getCoreClass();
            term = md.getTerm();

            if (SchemaConstants.HTTP_SLA_AT_SOI_EU_OCCI_OS_TEMPLATE.equalsIgnoreCase(scheme)) {
                cat = new Category();
                cat.setCoreClass(coreClass);
                cat.setScheme(scheme);
                cat.setTerm(term);
                cat.setTitle(md.getTitle());
                newlist.add(cat);
            }

        }
        LOGGER.debug("OS Templates List" + newlist.toString());

        this.setList(newlist);

    }

}
