package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when an ID used for interacting with the service manager does not exist.
 * 
 * @author sla
 * 
 */
public class UnknownIdException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5698991381751800988L;

}
