package org.slasoi.infrastructure.servicemanager.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.servicemanager.registry.Registry;

import eu.slasoi.infrastructure.model.Category;

/**
 *  A registry contains a list of Category s.
 * @author patrick cheevers
 *
 */
public abstract class Reg implements Registry<Category, String> {
    private List<Category> categoryList = null;
    private static final Logger LOGGER = Logger.getLogger(Reg.class.getName());

    /**
     * Constructor.
     */
    public Reg() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#getCategoryByID(java.lang.String)
     */
    public Category getCategoryByID(final String imageID) {
        LOGGER.debug("Looking for categorid " + imageID);

        Category retCat = null;

        if (categoryList != null) {
            for (Category category : categoryList) {
                LOGGER.info("Comparing with category " + category.getTerm());
                if (category.getTerm().equalsIgnoreCase(imageID)) {
                    retCat = new Category();
                    retCat.setScheme(category.getScheme());
                    retCat.setTerm(category.getTerm());
                    retCat.setTitle(category.getTitle());
                    retCat.setCoreClass(category.getCoreClass());
                    LOGGER.info("Catgory Found  - " + category);
                    // return category;
                    break;
                }

            }

        } else {
            LOGGER.debug("Catgeory List is Empty ");

        }

        return retCat;
    }

    /* (non-Javadoc)
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#setList(java.util.List)
     */
    public void setList(final List<Category> newlist) {

        categoryList = newlist;
    }

    /* (non-Javadoc)
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#getList()
     */
    public List<Category> getList() {

        return categoryList;
    }

    /* (non-Javadoc)
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#getDefaultCategory()
     */
    public Category getDefaultCategory() {
        Category defaultCat = null;
        for (Category cat : categoryList) {
            defaultCat = cat;
            break;
        }
        return defaultCat;

    }
}
