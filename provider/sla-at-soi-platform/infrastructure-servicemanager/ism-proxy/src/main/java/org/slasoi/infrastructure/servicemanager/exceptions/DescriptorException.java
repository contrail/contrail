package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when the a problem is found with the format of a request.
 * 
 * @author sla
 * 
 */
public class DescriptorException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 9140396749703448139L;

    /**
     * Creates a new @DescriptionException.
     * 
     * @param error
     *            If detected, the possible description of the error.
     */
    public DescriptorException(final String error) {
        super(error);
    }

}