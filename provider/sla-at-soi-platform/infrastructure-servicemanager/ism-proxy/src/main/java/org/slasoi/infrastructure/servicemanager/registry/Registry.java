package org.slasoi.infrastructure.servicemanager.registry;

import java.util.List;

import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;

import eu.slasoi.infrastructure.model.Category;

/**
 * Generic interface to implement a Registry such as Images, Users ,etc.
 * 
 * @author Patrick cheevers
 * 
 * @param <T>
 * @param <F>
 * 
 */
public interface Registry<T, F> {
    /**
     * Gets the @Category associated with the ID.
     * 
     * @param categoryId
     *            The category identifier
     * 
     * @return The @Category
     */
    Category getCategoryByID(String categoryId);

    /**
     * Gets the Default @Category .
     * 
     * 
     * @return The @Category
     */
    Category getDefaultCategory();

    /**
     * Gets a filtered @List.
     * 
     * @param filter
     *            The filter.
     * @return The @List containing matching the elements.
     */
    List<T> getList();

    void setList(List<Category> list);

    void setup(IPageSet<? extends IGrpResourceAvailMetadata> resources);

}
