package org.slasoi.infrastructure.servicemanager.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.io.ICategoryMetadata;
import org.slasoi.infrastructure.servicemanager.registry.IMetricRegistry;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import eu.slasoi.infrastructure.model.MetricImpl;

/**
 * Prototype Implementation of a Metric Registry.
 * 
 * @author Patrick Cheevers
 * 
 */
public final class MetricDetailRegistryImpl implements IMetricRegistry<Object, Object> {

    private static final Logger LOGGER = Logger.getLogger(MetricDetailRegistryImpl.class.getName());

    private List<CapacityResponseType> capacityList = null;

    private String defaultType;

    /*
     * Given memory cores and speed try and find a Metric Id Which closely match's your requirements
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.IMetricRegistry#getMetricID (float, int, float)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.IMetricRegistry#getMetricID(float, int, float)
     */
    public String getMetricID(float memory, int cores, float speed) {
        LOGGER.debug("MetricDetailRegistryImpl looking for close match");
        LOGGER.debug("param memory" + memory);
        LOGGER.debug("param cores" + cores);
        LOGGER.debug("param speed" + speed);

        MetricImpl metric = new MetricImpl(memory, cores, speed);
        LOGGER.debug("Search Metric" + metric);
        CapacityResponseType minCapacity = Collections.min(capacityList);
        String metricID = minCapacity.getType();
        LOGGER.debug("Default min Capacity id" + metricID);
        for (CapacityResponseType cap : capacityList) {
            LOGGER.debug("Comparing apacity response type" + cap);
            int res = (((MetricImpl) cap).compareTo(metric));
            if (res >= 0) {
                metricID = cap.getType();
                LOGGER.debug("Greater or Equal to mettric required" + cap);

            }

        }
        LOGGER.debug("Returning Metric Id" + metricID);
        return metricID;
    }

    /**
     * Populates a list Containing Capacity ResponseTypes
     * 
     * @param resources
     * @return List<CapacityResponseType>
     * @see org.slasoi.infrastructure.servicemanager.types.CapacityResponseType
     */
    public List<CapacityResponseType> setup(IPageSet<? extends IGrpResourceAvailMetadata> resources) {

        List<CapacityResponseType> capacityList = new ArrayList<CapacityResponseType>();
        CapacityResponseType capacityType = null;
        float memory = 0;
        String type = null;
        int avail = 0;
        float speed = 0;
        int cores = 0;
        String value = null;
        String key = null;
        try {
            for (IGrpResourceAvailMetadata resourceMd : resources) {
                ICategoryMetadata md = resourceMd.getCategoryMetaData();

                if (SchemaConstants.HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_RES_TEMPLATE.equalsIgnoreCase(md
                        .getScheme())) {
                    type = md.getTerm();
                    if ((SchemaConstants.SMALL.equalsIgnoreCase(type))
                            || (SchemaConstants.MEDIUM.equalsIgnoreCase(type))
                            || (SchemaConstants.LARGE.equalsIgnoreCase(type))) {

                        HashMap<String, String> attribs = md.getAttributes();
                        LOGGER.debug("Found " + type);
                        capacityType = new CapacityResponseType(type, memory, cores, speed, avail);

                        for (Map.Entry<String, String> entry : attribs.entrySet()) {
                            key = entry.getKey();
                            value = (String) entry.getValue();
                            setCapacity(type, key, value, capacityType);

                        }
                        LOGGER.debug("Building Response Type type " + type);
                        LOGGER.debug("Building Response Type memory " + memory);
                        LOGGER.debug("Building Response Type cores " + cores);
                        LOGGER.debug("Building Response Type speed " + speed);
                        LOGGER.debug("Building Response Type avail " + avail);

                        LOGGER.debug("Adding " + capacityType);
                        if (capacityType.getAvail() > 0) {
                            defaultType = capacityType.getType();
                        }
                        capacityList.add(capacityType);
                    }

                }
            }
        } catch (NumberFormatException e) {

            LOGGER.error("Query Capacity  setup: " + e.getMessage());

        }
        Collections.sort(capacityList);
        LOGGER.debug("Final List" + capacityList);
        return capacityList;
    }

    public static void setCapacity(String type, String key, String value, CapacityResponseType capType) {
        if (key.startsWith(type + "." + SchemaConstants.OCCI_COMPUTE_CORES)) {

            if (value != null) {
                capType.setCores(Integer.valueOf(value));
            }
        }
        if (key.startsWith(type + "." + SchemaConstants.OCCI_COMPUTE_SPEED)) {
            if (value != null) {
                capType.setSpeed(Float.valueOf(value));
            }
        }
        if (key.startsWith(type + "." + SchemaConstants.OCCI_COMPUTE_MEMORY)) {

            if (value != null) {
                capType.setMemory(Float.valueOf(value));
            }
        }
        if (key.startsWith(type + "." + SchemaConstants.OCCI_AVAILABLE)) {

            if (value != null) {
                capType.setAvail(Integer.valueOf(value));

            }
        }
    }

    /**
     * @return the defaultType
     */
    public String getDefaultType() {
        return defaultType;
    }

    /**
     * @param defaultType
     *            the defaultType to set
     */
    public void setDefaultType(String newType) {
        this.defaultType = newType;
    }

}
