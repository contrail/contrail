package org.slasoi.infrastructure.servicemanager.types;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a @ProvisionResponseType.
 * 
 * @author Victor
 */
public class ProvisionResponseType {

    @Override
    public String toString() {
        return "ProvisionResponseType [endPoints=" + endPoints + ", infrastructureID=" + infrastructureID + "]";
    }

    private static final long serialVersionUID = 6132757273368220089L;

    /**
     * the infrastructure ID associated with this provision
     */
    private String infrastructureID;

    /**
     * The @List of @EndPoint in this @ProvisionResponseType.
     */
    private List<EndPoint> endPoints = new ArrayList<EndPoint>();

    /**
     * Returns the @List of @Endpoint in this @ProvisionResponseType.
     * 
     * @return @List
     */
    public List<EndPoint> getEndPoints() {
        return endPoints;
    }

    /**
     * Sets the @List of @Endpoint.
     * 
     * @param endPoints
     * @List of @EndPoint
     */
    public void setEndPoints(final List<EndPoint> endPoints) {
        this.endPoints = endPoints;
    }

    /**
     * Gets the infrastructure ID associated with this @ProvisionResponseType.
     * 
     * @return the infrastructure ID
     */
    public String getInfrastructureID() {
        return infrastructureID;
    }

    /**
     * Sets the infrastructure ID associated with this @ProvisionResponseType.
     * 
     * @param infrastructureID String
     */
    public void setInfrastructureID(final String infrastructureID) {
        this.infrastructureID = infrastructureID;
    }

}