package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with starting up a resource.
 * 
 * @author sla
 * 
 */
public class StartResourceException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5692994381754800988L;

}
