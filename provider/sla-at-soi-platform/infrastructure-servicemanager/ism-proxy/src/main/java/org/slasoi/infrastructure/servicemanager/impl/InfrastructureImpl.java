package org.slasoi.infrastructure.servicemanager.impl;

import static org.jclouds.io.Payloads.newPayload;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jclouds.entities.IServices;
import org.jclouds.entities.IServicesContext;
import org.jclouds.entities.ServicesContextFactory;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.domain.IResource;
import org.jclouds.entities.domain.IResourceMetadata;
import org.jclouds.entities.domain.IService;
import org.jclouds.entities.domain.IServiceStatus;
import org.jclouds.entities.domain.IWorkMetadata;
import org.jclouds.entities.domain.internal.MutableResourceMetadataImpl;
import org.jclouds.logging.log4j.config.Log4JLoggingModule;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.servicemanager.Infrastructure;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.StopException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.prediction.PredictionQuery;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.infrastructure.servicemanager.util.monitoring.MonitoringUtil;
import org.slasoi.infrastructure.servicemanager.util.occi.CategoryPayload;
import org.slasoi.infrastructure.servicemanager.util.occi.InfrastructureUtil;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

import com.google.common.collect.ImmutableSet;

import eu.slasoi.infrastructure.model.infrastructure.Compute;

/**
 * Implementation of a Infrastructure Service Manager.
 * 
 * @author Patrick Cheevers
 * 
 */
public class InfrastructureImpl implements Infrastructure {
    private static final Service2ProvisionResponseType GRP2PROVISIONRESP = new Service2ProvisionResponseType();
    private InfrastructureUtil util = new InfrastructureUtil();
    private MonitoringUtil monitoringUtil = new MonitoringUtil();
    private static Log4JLoggingModule log4jModule = (new Log4JLoggingModule());

    private IServices groupings = null;
    private IServicesContext context = null;
    private String restFileName;
    private static final Logger LOGGER = Logger.getLogger(InfrastructureImpl.class.getName());

    // spring injected
    /**
     * The Image Registry.
     */
    // private Registry<?, ?> imageregistry;
    /**
     * Location Register.
     */
    // private Registry<?, ?> locregistry;

    /**
     * Location Register.
     */
    // private Registry<?, ?> osregistry;
    /**
     * Location Register.
     */
    // private Registry<?, ?> metricregistry;
    /**
     * @return the metricregistry
     */
    public org.slasoi.infrastructure.servicemanager.registry.Registry getMetricregistry() {
        return util.getMetricReg();
    }

    /**
     * @param metricregistry
     *            the metricregistry to set
     */
    public void setMetricregistry(org.slasoi.infrastructure.servicemanager.registry.Registry metricregistry) {
        util.setMetricReg( metricregistry);
    }

    /**
     * Prediction Instrumentation.
     */
    private PredictionQuery predictionQuery;
    // ////////////////End Spring

    //
    private String configurationFilesPath = System.getenv("SLASOI_HOME") + System.getProperty("file.separator");

    // //Spring Injected
    public void setImageregistry(org.slasoi.infrastructure.servicemanager.registry.Registry imageregistry) {
        util.setImageReg(imageregistry);

    }

    public org.slasoi.infrastructure.servicemanager.registry.Registry getImageregistry() {
        initialiseReg();
        return util.getImageReg();
    }

    /**
     * Initialise the registrry so that one can obtain valid os,metric registry when you start to provision a service
     * later
     */
    void initialiseReg() {
        // Currently all pulled from server same list
        LOGGER.debug("Initailaise Registry :");
        if (util.getImageReg().getList() == null) {
            setupAvailableCats(groupings);

        }

    }

    public void setOsregistry(org.slasoi.infrastructure.servicemanager.registry.Registry osregistry) {
        util.setOsReg(osregistry);

    }

    public org.slasoi.infrastructure.servicemanager.registry.Registry getOsregistry() {
        initialiseReg();
        return util.getOsReg();
    }

    public void setLocregistry(org.slasoi.infrastructure.servicemanager.registry.Registry locationregistry) {
        util.setLocReg(locationregistry);

    }

    public org.slasoi.infrastructure.servicemanager.registry.Registry getLocregistry() {
        initialiseReg();
        return util.getLocReg();
    }

    /**
     * Gets the @PredictionQuery Instrumentation .
     * 
     * @return The @PredictionQuery.
     */
    public PredictionQuery getPredictionQuery() {
        return predictionQuery;
    }

    /**
     * Sets the @PredictionQuery.
     * 
     * @param predictionQuery
     *            The @PredictionQuery
     */
    public void setPredictionQuery(PredictionQuery predictionQuery) {
        this.predictionQuery = predictionQuery;
        util.setPredictionQuery(predictionQuery);
    }

    private PubSubManager pubSubManager;

    /**
     * @return the pubSubManager
     */
    public PubSubManager getPubSubManager() {
        return pubSubManager;
    }

    /**
     * @param pubSubManager
     *            the pubSubManager to set
     */
    public void setPubSubManager(PubSubManager pubSubManager) {
        this.pubSubManager = pubSubManager;
        monitoringUtil.setPubSubManager(pubSubManager);
    }

    // ---------------- End Spring injected
    // ----------------------------------------

    /**
     * @return the restFileName
     */
    public String getRestFileName() {
        return restFileName;
    }

    /**
     * @param restFileName
     *            the restFileName to set
     */
    public void setRestFileName(String restFileName) {
        this.restFileName = restFileName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#predictionQuery(java.lang.String, java.lang.String,
     * int)
     */
    public String predictionQuery(String id, String metricName, int minutes) throws UnknownIdException {

        return util.predictionQuery(id, metricName, minutes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#commit(java.lang.String)
     */
    public ReservationResponseType commit(String infrastructureID) throws UnknownIdException {
        LOGGER.debug("commit id=" + infrastructureID);
        assert (infrastructureID.startsWith(RESERVE_KEY));
        IServiceStatus status = groupings.postServiceAction(infrastructureID, "commit");
        ReservationResponseType resp = util.verifyReserveStatus(status != null, infrastructureID);
        // After a successful commit a reservation becomes a service
        String newId = infrastructureID.replace(RESERVE_KEY, SERVICE_KEY);
        resp.setInfrastructureID(newId);
        return resp;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#listServicesOrResources(java.lang.String)
     */
    public HashSet<String> listServicesOrResources(String path) {
        LOGGER.debug("listServicesOrResources Path: " + path);

        HashSet<String> keys = new HashSet<String>();
        String key = null;
        for (IWorkMetadata resourceMd : groupings.listKeys(path)) {
            String groupId = resourceMd.getName();
            key = path + groupId;
            LOGGER.debug("Service Id : " + key);

            keys.add(key);

        }
        return keys;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#stop(java.lang.String)
     */
    public void stop(String id) throws UnknownIdException, StopException {
        LOGGER.debug("stop id=" + id);
        String key = id;
        IServiceStatus result = groupings.postServiceAction(key, "stop");
        if (result == null) {
            throw new UnknownIdException();
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#release(java.lang.String)
     */
    public void release(String id) throws UnknownIdException {
        LOGGER.debug("release id=" + id);

        // assert (id.startsWith(RESERVE_KEY));
        groupings.removeService(id);

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#reprovision(java.lang.String,
     * org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType)
     */
    public ProvisionResponseType reprovision(String id, ProvisionRequestType provisioningRequestType)
            throws DescriptorException, UnknownIdException, ProvisionException {
        LOGGER.debug("reprovision id=" + id);
        LOGGER.debug("reprovision ProvisionRequestType = " + provisioningRequestType);
        return encompassUpdate(id, SERVICE_KEY, provisioningRequestType);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.infrastructure.servicemanager.Infrastructure#reserve(org.slasoi.infrastructure.servicemanager.types
     * .ProvisionRequestType)
     */
    public ReservationResponseType reserve(ProvisionRequestType provisionRequest) throws DescriptorException {
        LOGGER.debug("reserve provisionRequest=" + provisionRequest);

        ProvisionResponseType result = null;
        String id = null;
        ReservationResponseType reserveResult = null;
        try {
            result = encompassCreate(RESERVE_KEY, provisionRequest);
            id = (result != null ? result.getInfrastructureID() : null);
            reserveResult = util.verifyReserveStatus(result != null, id);
        } catch (ProvisionException e) {

            e.printStackTrace();
            throw new DescriptorException(e.getLocalizedMessage());
        } catch (UnknownIdException e) {
            throw new DescriptorException(e.getLocalizedMessage());
        }
        return reserveResult;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#getMonitoringFeatures(int)
     */
    public ComponentMonitoringFeatures[] getMonitoringFeatures(int sleep) throws Exception {
        LOGGER.debug("GetMonitoring Features sleep=" + sleep);
        // response is not coming back quick enough
        int newsleep = (sleep * 10);
        LOGGER.debug("GetMonitoring Features sleep Multiplied=" + sleep);

        ComponentMonitoringFeatures[] features = monitoringUtil.getMonitoringFeatures(newsleep);
        if (features == null) {
            LOGGER.debug("****##Features NOT Found");
        } else {
            LOGGER.debug("################## :-)Features Found");
            LOGGER.debug("Feature length" + features.length);

        }
        return features;
    }

    /**
     * Initialise the implementation.
     */
    public InfrastructureImpl() {
        super();
        String propsFile = configurationFilesPath + "ismocci/ismocci.properties";
        LOGGER.debug("Props File=" + propsFile);

        Properties overrides = InfrastructureUtil.getPropertiesFromResource(propsFile);
        LOGGER.debug("Overrides from Props File=" + overrides);

        context = new ServicesContextFactory().createContext("occi", ImmutableSet.of(log4jModule), overrides);

        groupings = context.getServices();

    }

    /**
     * Initialise the location os and metric registrys.
     * 
     * @param groupings
     */
    public void setupAvailableCats(final IServices groupings) {

        LOGGER.debug("Set Up available categories");

        IPageSet<? extends IGrpResourceAvailMetadata> resources = groupings.listAvailResources("");

        util.getMetricReg().setup(resources);
        util.getOsReg().setup(resources);
        util.getLocReg().setup(resources);
        util.getImageReg().setup(resources);

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#getDetails(java.lang.String)
     */
    public ProvisionResponseType getDetails(String infrastructureID) throws UnknownIdException {
        LOGGER.debug("Get Details : " + infrastructureID);

        IService group = groupings.getService(infrastructureID);
        if (group == null) {
            UnknownIdException exception = new UnknownIdException();
            throw exception;
        } else {
            ProvisionResponseType response = GRP2PROVISIONRESP.apply(group);
            response.setInfrastructureID(infrastructureID);
            LOGGER.debug("Get Details Response: " + response.toString());

            return response;
        }
    }

    // 'occi.small.available{immutable int}=60 occi.compute.cores{int}=1
    // occi.compute.speed{float}=2.4 occi.compute.memory{float}=0.5

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#queryCapacity()
     */
    public List<CapacityResponseType> queryCapacity() throws DescriptorException {
        List<CapacityResponseType> result = util.queryCapacity(groupings);
        if (result == null || result.size() == 0) {
            throw new DescriptorException("Capacity Response List is Empty");
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#startResource(java.lang.String)
     */
    public void startResource(final String resourceID) throws UnknownIdException {
        LOGGER.debug("Start Resource=" + resourceID);

        IServiceStatus result = groupings.postServiceAction(resourceID, "start");
        if (result == null) {
            throw new UnknownIdException();
        }

    }

    /**
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#startResource(java.lang.String)
     * 
     * @param infrastructureID
     * @throws UnknownIdException
     * @throws StopException
     */
    public void startService(final String infrastructureID) throws UnknownIdException, StopException {
        LOGGER.debug("Start Service=" + infrastructureID);

        IServiceStatus result = groupings.postServiceAction(infrastructureID, "start");
        if (result == null) {
            throw new UnknownIdException();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#stopResource(java.lang.String)
     */
    public void stopResource(final String resourceID) throws UnknownIdException {
        LOGGER.debug("Stop Resource=" + resourceID);

        IServiceStatus result = groupings.postServiceAction(resourceID, "stop");
        if (result == null) {
            throw new UnknownIdException();
        }

    }

    /**
     * Removes a service.
     * 
     * @param resourceID
     * @throws UnknownIdException
     */
    public void releaseResource(final String resourceID) throws UnknownIdException {
        LOGGER.debug("release Resource=" + resourceID);

        groupings.removeService(resourceID);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.infrastructure.servicemanager.Infrastructure#provision(org.slasoi.infrastructure.servicemanager.types
     * .ProvisionRequestType)
     */
    public ProvisionResponseType provision(final ProvisionRequestType provisioningRequestType)
            throws DescriptorException, ProvisionException {
        LOGGER.debug("Provision=");
        LOGGER.debug("Provision RequestType=" + provisioningRequestType);

        return encompassCreate(SERVICE_KEY, provisioningRequestType);

    }

    /**
     * Used by provision and reserve to create a service /reservation.
     * 
     * @param controller
     * @param provisioningRequestType
     * @return
     * @throws DescriptorException
     * @throws ProvisionException
     */
    public ProvisionResponseType encompassCreate(final String controller,
            final ProvisionRequestType provisioningRequestType) throws DescriptorException, ProvisionException {
        LOGGER.debug("Encompass");
        if (provisioningRequestType == null) {
            LOGGER.error("ProvisionRequestType cannot be null");
            throw new DescriptorException("ProvisionRequestType cannot be null");
        }

        LOGGER.debug("Provision RequestType :" + provisioningRequestType.toString());

        IService iService;
        CategoryPayload catPayLoad = util.getCatgeorysForPayload(provisioningRequestType);
        catPayLoad.setType(controller);
        URI groupingResponse = null;
        ProvisionResponseType response = null;

        iService = newServiceWithMD5("provision", catPayLoad, groupings);
        groupingResponse = groupings.createService(controller, iService);
        util.validateResponse(groupingResponse, provisioningRequestType);
        String key = groupingResponse.getPath();
        IService group = groupings.getService(key);

        response = GRP2PROVISIONRESP.apply(group);
        response.setInfrastructureID(key);

        LOGGER.debug("EncompassCreate Result" + response);
        return response;

    }

    /**
     * Used by Provision and reservation to Update a service /reservation.
     * 
     * @param key
     * @param controller
     * @param provisioningRequestType
     * @return ProvisionResponseType
     * @throws DescriptorException
     * @throws ProvisionException
     */
    public ProvisionResponseType encompassUpdate(final String key, final String controller,
            final ProvisionRequestType provisioningRequestType) throws DescriptorException, ProvisionException {

        LOGGER.debug("Encompass Update ");
        if (provisioningRequestType == null) {
            LOGGER.error("ProvisionRequestType cannot be null");
            throw new DescriptorException("ProvisionRequestType cannot be null");
        }

        LOGGER.debug("Provision RequestType :" + provisioningRequestType.toString());

        IService iService;
        CategoryPayload catPayLoad = util.getCatgeorysForPayload(provisioningRequestType);
        catPayLoad.setType(controller);
        IServiceStatus groupingResponse = null;
        ProvisionResponseType response = new ProvisionResponseType();

        response.setInfrastructureID(key);
        IResource resource = null;
        HashMap<String, String> map = catPayLoad.getComputeCatPayloadList();
        for (Entry<String, String> entry : map.entrySet()) {
            resource = newComputeResourceWithMD5(entry.getKey(), entry.getValue(), groupings);

            groupingResponse = groupings.updateServicePart(COMPUTE_KEY + entry.getKey(), resource);
            util.validateUpdateResponse(groupingResponse, provisioningRequestType);

        }
        try {
            response = getDetails(key);
        } catch (UnknownIdException e) {
            LOGGER.error("After Update Cannot find details for" + key);
            throw new ProvisionException("Obtain service details after Update was not successful");
        }
        LOGGER.debug("EncompassUpdate Result " + response);
        return response;

    }

    /**
     * Used to create a resource type and possibly sign it also
     * 
     * @param name
     * @param value
     * @param groupings
     * @return
     */
    public static IResource newComputeResourceWithMD5(final String name, final Object value,
            final IServices groupings) {
        IResource resource = groupings.newResource(name);
        resource.setPayload(newPayload(value));
        resource.getPayload().getContentMetadata().setContentType("text/plain");

        // try {
        // Payloads.calculateMD5(IService, crypto.md5());
        // } catch (IOException e) {
        // Throwables.propagate(e);
        // }
        return resource;
    }

    /**
     * Used to craete a new Service and possibly sign it also
     * 
     * @param name
     * @param catPayLoad
     * @param groupings
     * @return
     */
    public static IService newServiceWithMD5(final String name, final CategoryPayload catPayLoad,
            final IServices groupings) {
        IService iService = groupings.newService(name);

        Set<IResourceMetadata> mdParts = new HashSet<IResourceMetadata>();
        MutableResourceMetadataImpl part = null;
        HashMap<String, String> map = catPayLoad.getComputeCatPayloadList();

        for (Entry<String, String> entry : map.entrySet()) {
            part = new MutableResourceMetadataImpl();
            part.setId(entry.getKey());
            part.setValue(entry.getValue());
            mdParts.add(part);
        }
        part = new MutableResourceMetadataImpl();
        part.setId(catPayLoad.getSeviceCatKey());
        part.setValue(catPayLoad.getSeviceCatPayLoad());
        mdParts.add(part);

        if (catPayLoad.getType().equalsIgnoreCase(RESERVE_KEY)) {
            part = new MutableResourceMetadataImpl();
            part.setId(catPayLoad.getReservationCatKey());
            part.setValue(catPayLoad.getReservationCatPayLoad());
            mdParts.add(part);
        }

        iService.getMetadata().setResourcesMetadata(mdParts);

        // try {
        // Payloads.calculateMD5(IService, crypto.md5());
        // } catch (IOException e) {
        // Throwables.propagate(e);
        // }
        return iService;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#createComputeConfiguration(java.lang.String,
     * java.lang.String, java.lang.String, java.util.Hashtable)
     */
    @Override
    public Compute createComputeConfiguration(final String osID, final String metricID, final String locTemplateID,

    Hashtable<String, String> extras) {
        initialiseReg();
        return util.createComputeConfiguration(osID, metricID, locTemplateID,

        extras);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#createComputeConfiguration(int, float, int,
     * java.lang.String, java.lang.String, java.util.Hashtable)
     */
    @Override
    public Compute createComputeConfiguration(final int cores, final float speed, final int memory,
            final String osID, final String locTemplateID, final Hashtable<String, String> extras) {
        initialiseReg();
        String metricID =
                ((MetricRegistryImpl) util.getMetricReg()).getMetricDetailReg().getMetricID(memory, cores, speed);
        return createComputeConfiguration(osID, metricID, locTemplateID,

        extras);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#createProvisionRequestType(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public ProvisionRequestType createProvisionRequestType(

    String osID, String metricID, String locTemplateID, String monitoringRequest, String notificationURI) {
        initialiseReg();

        return util.createProvisionRequestType(getImageregistry(), osID, metricID, locTemplateID,
                monitoringRequest, notificationURI);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.Infrastructure#createProvisionRequestType(java.lang.String,
     * java.util.Set)
     */
    public ProvisionRequestType createProvisionRequestType(String monitoringRequest, Set<Compute> computeResources) {
        initialiseReg();

        return util.createProvisionRequestType(monitoringRequest, computeResources);
    }

}
