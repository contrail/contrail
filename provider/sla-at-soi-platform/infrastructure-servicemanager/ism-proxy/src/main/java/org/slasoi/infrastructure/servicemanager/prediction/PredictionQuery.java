package org.slasoi.infrastructure.servicemanager.prediction;

import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;

public interface PredictionQuery {
    String query(String id, String metric, int minutes) throws UnknownIdException;

}
