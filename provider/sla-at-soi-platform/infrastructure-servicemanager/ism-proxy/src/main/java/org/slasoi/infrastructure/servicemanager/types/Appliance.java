package org.slasoi.infrastructure.servicemanager.types;

import eu.slasoi.infrastructure.model.infrastructure.Compute;

/**
 * @author Victor
 *
 * @param <S>
 */
public class Appliance<S> {
    private S image;
    private Compute compute;

    public S getImage() {
        return image;
    }

    public void setImage(final S image) {
        this.image = image;
    }

    public void setCompute(final Compute compute) {
        this.compute = compute;
    }

    public Compute getCompute() {
        return compute;
    }

}