package org.slasoi.infrastructure.servicemanager.util.monitoring;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.listeners.ConfigurationChannelListener;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesResponse;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

/**
 * Used to get Monitoring Features
 * 
 * @author Patrick Cheevers
 * 
 */
public class MonitoringUtil {
    private MonitoringFeaturesResponse monFeaturesResp;
    private static final Logger LOGGER = Logger.getLogger(MonitoringUtil.class);

    // look at
    // https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/
    // infrastructure-monitoring/infrastructure-monitoring-agent
    // src/main/resources/META-INF/spring/infrastructure-monitoring-agent-context.xml
    private String channelName = "INF_MON_AGENT_CONFIG";
    private Channel configurationChannel;
    private PubSubManager pubSubManager;

    public ComponentMonitoringFeatures[] getMonitoringFeatures(final int sleep) throws Exception {
        this.configurationChannel = new Channel(channelName);
        pubSubManager.addMessageListener(new ConfigurationChannelListener(), new String[] { configurationChannel
                .getName() });
        LOGGER.debug("Subscribing to configuration channel.");
        try {
            registerPubSubListener(pubSubManager);
        } catch (Exception e) {
            LOGGER.error("Unable to Subscribe to configuration Channel :" + channelName);
            e.printStackTrace();
        }
        MonitoringFeaturesRequest monFeaturesRequest = new MonitoringFeaturesRequest();
        monFeaturesRequest.setMessageId(UUID.randomUUID().toString());
        LOGGER.debug("New Monitoring Request created id" + monFeaturesRequest.getMessageId());
        LOGGER.debug("Channel Name" + channelName);

        PubSubMessage pubSubMessage = new PubSubMessage(channelName, monFeaturesRequest.toJson());
        LOGGER.debug("Publish Message to pubSubManager" + pubSubManager);

        pubSubManager.publish(pubSubMessage);

        Thread.sleep(sleep);
        ComponentMonitoringFeatures[] cmf = null;
        if (monFeaturesResp == null) {
            LOGGER.error("MonitoringFeaturesResponse was not received.");
        } else {
            LOGGER.debug("## MonitoringFeaturesResponse Get Components  ##");

            cmf = monFeaturesResp.getComponentMonitoringFeatures();
            LOGGER.debug("##  Components  are ##" + cmf.toString());

        }

        try {
            pubSubManager.unsubscribe(configurationChannel.getName());
        } catch (MessagingException e) {
            LOGGER.warn("Failed to unsubscribe from configuration channel: " + e.getMessage());
        }
        return cmf;
    }

    private void registerPubSubListener(final PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        // log.trace("Subscribing to the configuration channel.");
        pubSubManager.subscribe(channelName);

        pubSubManager.addMessageListener(new MessageListener() {
            public void processMessage(final MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                LOGGER.debug("#######################NEW MESSAGE #################");

                LOGGER.debug("New message arrived length: " + payload.length());

                LOGGER.debug("Message starts with: " + payload.substring(100) + "...");
                try {
                    JSONObject o = new JSONObject(payload);
                    PubSubResponse response = PubSubResponse.fromJson(payload);
                    String responseType = response.getResponseType();
                    if (responseType.equalsIgnoreCase("MonitoringFeaturesResponse")) {
                        LOGGER.debug("## NEW MESSAGE is MonitoringFeaturesResponse  ##");

                        monFeaturesResp = MonitoringFeaturesResponse.fromJson(payload);
                        LOGGER.debug("##MonitoringFeaturesResponse  JSON Message ##");

                    }
                } catch (Exception e) {
                    LOGGER.error("Invalid MonitoringFeaturesResponse message.");
                }
            }
        });
    }

    // public final void publishEventMonitoredEventChannel() {
    //
    // try {
    // // String busProps = System.getenv("SLASOI_HOME")
    // // + System.getProperty("file.separator") + "Integration"
    // // + System.getProperty("file.separator") + "bus.properties";
    //
    // PubSubManager pubSubManager1 = PubSubFactory
    // .createPubSubManager(busProps);
    // for (int i = 0; i < 20; i++) {
    // Thread.sleep(2000);
    // pubSubManager1.publish(new PubSubMessage(channelName,
    // "test event"));
    // }
    //
    // } catch (MessagingException e) {
    // e.printStackTrace();
    //
    // } catch (InterruptedException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (FileNotFoundException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }

    public void setPubSubManager(PubSubManager pubSubManager) {
        this.pubSubManager = pubSubManager;

    }
}
