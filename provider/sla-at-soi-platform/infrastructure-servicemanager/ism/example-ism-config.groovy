import grails.plugins.springsecurity.SecurityConfigType

/*
SVN FILE: $Id: example-ism-config.groovy 2773 2011-07-22 15:31:38Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2773 $
@lastrevision   $Date: 2011-07-22 17:31:38 +0200 (pet, 22 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/example-ism-config.groovy $
*/

import grails.plugins.springsecurity.SecurityConfigType

//SLA@SOI ISM configuration

def messageBusHost = 'MESSAGE_BUS_HOST'

eu{
  slasoi{
    infrastructure{

      domain = 'SERVICE_DOMAIN' //e.g. myservice.com
      renderFullTypes = true
      copyReservationIdToService = true

			//these values are only read on the 1st execution of the ISM
			//TODO update the DB model when this changes

			resources = [
				enabled:[
					'compute':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
						title:'Compute Type',
						location:'/compute/',
						attributes:[
							'occi.compute.architecture':[default:'x86', required:false, mutable:true, type:'string', render:true],
							'occi.compute.cores':[default:'1', required:false, mutable:true, type:'int', render:true],
							'occi.compute.memory':[default:'0.5', required:false, mutable:true, type:'float', render:true],
							'occi.compute.speed':[default:'2.4', required:false, mutable:true, type:'float', render:true],
							'occi.compute.hostname':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.compute.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
						],
						actions:[
							'start':[
								scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Start Compute Action'
							],
							'stop':[
								scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Stop Compute Action',
								attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
							],
							'restart':[
								scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Restart Compute Action',
								attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
							],
							'suspend':[
								scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Suspend Compute Action',
								attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
							]
						]
					],
					'service':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
						title:'Service Type',
						location:'/service/',
						attributes:[
							'eu.slasoi.infrastructure.service.name':[default:'', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.service.monitoring.config':[default:'', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.service.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
						],
						actions:[
							'start':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Start Service Action'],
							'stop':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Stop Service Action'],
							'restart':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Restart Service Action'],
							'suspend':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Suspend Service Action']
						]
					],
          'reservation':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
						title:'Reservation Type',
						location:'/reservation/',
						attributes:[
							'eu.slasoi.infrastructure.reservation.leasePeriod':[default:'60', required:false, mutable:true, type:'int', render:true],
							'eu.slasoi.infrastructure.reservation.created':[default:'', required:false, mutable:true, type:'int', render:true]
						],
						actions:[
							'commit':[scheme:'http://sla-at-soi.eu/occi/infrastructure/reservation#', title:'Commit Reservation Action']
						]
					]
				],
				disabled:[
					'network':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
						title:'Network type',
						location:'/network/',
						attributes:[
							'occi.network.vlan':[default:'0', required:false, mutable:true, type:'int', render:true],
							'occi.network.label':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.network.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
						],
						actions:[
							'up':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Up Network Action'],
							'down':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Down Network Action']
						]
					],
					'storage':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
						title:'Storage type',
						location:'/storage/',
						attributes:[
							'occi.storage.size':[default:'10.0', required:false, mutable:true, type:'float', render:true],
							'occi.storage.state':[default:'offline', required:false, mutable:false, type:'enum', render:true]
						],
						actions:[
							'online':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Online Storage Action'],
							'offline':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Offline Storage Action'],
							'backup':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Backup Storage Action'],
							'snapshot':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Snapshot Storage Action'],
							'resize':[
								scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Resize Storage Action',
								attributes:['size':[default:'', required:false, mutable:true, type:'float', render:true]]
							],
							'degrade':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Degrade Storage Action']
						]
					],
				]
			]

			links = [
				enabled:[
					'storagelink':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
						title:'Storage Link',
						location:'/storage/link/',
						attributes:[
							'occi.storagelink.deviceid':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.storagelink.mountpoint':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.storagelink.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
						]
					],
					'networkinterface':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
						title:'Network Interface Link',
						location:'/network/link/',
						attributes:[
							'occi.networkinterface.interface':[default:'eth0', required:false, mutable:true, type:'string', render:true],
							'occi.networkinterface.mac':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.networkinterface.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
						]
					]
				],
				disabled:[]
			]

			mixins = [
				enabled:[
					'os_tpl':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'', kind:''],
						title:'OS Template Mixin',
						location:'',
						attributes:[]
					],
					'metric':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure#',
						rel:[parent:'', kind:''],
						title:'Metric Mixin',
						location:'',
						attributes:[
							'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.metric.samplerate':[default:'', required:false, mutable:true, type:'int', render:true],
							'eu.slasoi.infrastructure.metric.resolution':[default:'', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.metric.observing':[default:'', required:false, mutable:true, type:'string', render:true]
						]
					],
					'user':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure/metric/compute/cpu#',
						rel:[parent:'http://sla-at-soi.eu/occi/infrastructure#metric', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Metric Mixin',
						location:'/metric/compute/cpu/user/',
						attributes:[
							'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.metric.samplerate':[default:'60', required:false, mutable:true, type:'int', render:true],
							'eu.slasoi.infrastructure.metric.resolution':[default:'M', required:false, mutable:true, type:'string', render:true],
							'eu.slasoi.infrastructure.metric.observing':[default:'occi.compute.cpu', required:false, mutable:true, type:'string', render:true]
						]
					],
					'resource_tpl':[
						scheme:'http://schemas.ogf.org/occi/infrastructure#',
						rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Resource Template Mixin',
						location:'/template/',
						attributes:[]
					],
					'ie':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure/location#',
						rel:[parent:"", kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Ireland Geo Location Template',
						location:'/location/ie/',
					    attributes:[]
					],
					'small':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
						rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Small Compute Resource Template',
						location:'/template/small/',
						attributes:[
							'small.occi.compute.cores':[default:'1', required:false, mutable:false, type:'int', render:true],
							'small.occi.compute.speed':[default:'1.0', required:false, mutable:false, type:'float', render:true],
							'small.occi.compute.memory':[default:'1.0', required:false, mutable:false, type:'float', render:true],
							'small.occi.available':[default:'60', required:false, mutable:false, type:'int', render:true]
						]
					],
					'medium':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
						rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Medium Compute Resource Template',
						location:'/template/medium/',
						attributes:[
							'medium.occi.compute.cores':[default:'2', required:false, mutable:false, type:'int', render:true],
							'medium.occi.compute.speed':[default:'2.0', required:false, mutable:false, type:'float', render:true],
							'medium.occi.compute.memory':[default:'2.0', required:false, mutable:false, type:'float', render:true],
							'medium.occi.available':[default:'30', required:false, mutable:false, type:'int', render:true]
						]
					],
					'large':[
						scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
						rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Large Compute Resource Template',
						location:'/template/large/',
						attributes:[
							'large.occi.compute.cores':[default:'4', required:false, mutable:false, type:'int', render:true],
							'large.occi.compute.speed':[default:'4.0', required:false, mutable:false, type:'float', render:true],
							'large.occi.compute.memory':[default:'4.0', required:false, mutable:false, type:'float', render:true],
							'large.occi.available':[default:'15', required:false, mutable:false, type:'int', render:true]
						]
					]
				],
				disabled:[
					'ipnetwork':[
						scheme:'http://schemas.ogf.org/occi/infrastructure/network#',
						rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#network'],
						title:'IP Network Mixin',
						location:'/ipnetwork/',
						attributes:[
							'occi.network.address':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.network.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.network.allocation':[default:'dynamic', required:false, mutable:true, type:'enum', render:true]
						]
					],
					'ipnetworkinterface':[
						scheme:'http://schemas.ogf.org/occi/infrastructure/networkinterface#',
						rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#networkinterface'],
						title:'IP Network Interface Mixin',
						location:'/ipnetwork/ipnetworkinterface/',
						attributes:[
							'occi.networkinterface.address':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.networkinterface.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
							'occi.networkinterface.allocation':[default:'dynamic', required:false, mutable:true, type:'string', render:true]
						]
					]
				]
			]

			templates = [
				enabled:[
					'ubuntu_9-10':[
						scheme:'http://sla-at-soi.eu/occi/os_templates#',
						rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Base Ubuntu 9.10 LTS operating system',
						location:'/os_tpl/ubuntu_9-10/',
						attributes:[
							'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
						]
					],
					'ubuntu_10-11':[
						scheme:'http://sla-at-soi.eu/occi/os_templates#',
						rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
						title:'Base Ubuntu 10.11 LTS operating system',
						location:'/os_tpl/ubuntu_10-11/',
						attributes:[
							'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
						]
					]
				],
				disabled:[]
			]

 			provisioning{
 				//why use this when the lease kill time is specified in the reservation resource type?
 				leaseKillTime = 60 //seconds
				defaultOS = 'http://sla-at-soi.eu/occi/os_templates#ubuntu_10-11'
				enable = false
				defaultUserId = 1000
				agentURI = "tashi@${messageBusHost}"
				/* Not currently used
				notification {
					uiURI = "" //or: = "ui@localhost" //set this to blank or null if you do not require notifications sent to the UI
					perResource = true
				}*/
			}

     		monitoring{
        		enabled = true
        		llmsChannel = 'llms@${messageBusHost}'
                llms{
	        		pubsubType = 'in_memory'
	        		xmpp_username = 'test3'
	        		xmpp_password = 'test3'
	        		xmpp_host = 'slasoi.dnsalias.net'
	        		xmpp_port = '5222'
	        		xmpp_service = 'slasoi.dnsalias.net'
	        		xmpp_resource = 'test3'
	        		xmpp_pubsubservice = 'slasoi.dnsalias.net'
	        	}
      		}
    }
  }
}

//XMPP Config
xmpp{
  autoStartup = false
  username = "ISM_USERNAME"
  password = "IS_PASSWORD"
  connection{
    host = messageBusHost
    port = 5222
    service = ""
    SASLAuthenticationEnabled = true
  }
}

//AMQP Config
rabbitmq {
  connectionfactory {
    username = 'RABBIT_MQ_USERNAME'
    password = 'RABBIT_MQ_PASSWORD'
    hostname = messageBusHost
    consumers = 5
  }
  queues = {
    result()
    warn()
    error()
  }
}

//Redis Config
grails{
  redis{
    host = "REDIS_HOST"
    //port = 6380
    //password = "REDIS_PASSWORD"
    //pooled = true
    //resources = 15
    //timeout = 5000
  }
}

//Security & LDAP Config
grails{
  plugins{
    springsecurity{
      securityConfigType = SecurityConfigType.InterceptUrlMap
      /*AUTHENTICATION TURNED ON
          We allow the public call the query interface
          We allow the public access the authentication mechanism
          We REQUIRE authentication on everything else

      interceptUrlMap = [
              '/-/': ['IS_AUTHENTICATED_ANONYMOUSLY'],
              '/auth/': ['IS_AUTHENTICATED_ANONYMOUSLY'],
              '/*': ['IS_AUTHENTICATED_FULLY']
      ]
*/
      //AUTHENTICATION TURNED OFF
      interceptUrlMap = [
              '/-/': ['IS_AUTHENTICATED_ANONYMOUSLY'],
              '/auth/': ['IS_AUTHENTICATED_ANONYMOUSLY'],
              '/*': ['IS_AUTHENTICATED_ANONYMOUSLY']
      ]
      ldap{
        context{
          managerDn = 'MANAGER_DN' //e.g. cn=admin,dc=edmonds,dc=com
          managerPassword = 'MANAGER_PASSWORD'
          server = 'ldap://MANAGER_HOST:389'
        }
        authorities{
          groupSearchBase = 'dc=edmonds,dc=com'
        }
        search{
          base = 'dc=edmonds,dc=com'
        }
				mapper{
					userDetailsClass = 'eu.slasoi.infrastructure.authentication.ISMUserDetailsContextMapper'
				}
      }
    }
  }
}

environments {

  development {

    grails.serverURL = "http://localhost:8080"

    log4j = {
      // in development mode, let's see all my log messages
      debug 'grails.app'
      error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'

      warn  'org.mortbay.log'
    }
  }

  test {
    grails.serverURL = "http://localhost:8080"
  }

  production {

    grails.serverURL = "http://SERVICE_HOST:8080"

    def catalinaBase = System.properties.getProperty('catalina.base')
    if (!catalinaBase) catalinaBase = '.'   // just in case
    def logDirectory = "${catalinaBase}/logs"

    log4j = {
      appenders {
        // set up a log file in the standard tomcat area; be sure to use .toString() with ${}
        rollingFile name: 'tomcatLog', file: "${logDirectory}/IAM.log".toString(), maxFileSize: '100KB'
        'null' name: 'stacktrace'
      }

      root {
        // change the root logger to my tomcatLog file
        error 'tomcatLog'
        additivity = true
      }

      // example for sending stacktraces to my tomcatLog file
      //error tomcatLog:'StackTrace'

      // set level for my messages; this uses the root logger (and thus the tomcatLog file)
      debug 'grails.app'
    }
  }
}


