/*
SVN FILE: $Id: ComputeTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/infrastructure/ComputeTests.groovy $
*/

package occi.infrastructure

import grails.test.GrailsUnitTestCase
import occi.infrastructure.actions.compute.RestartComputeAction
import occi.infrastructure.actions.compute.StartComputeAction
import occi.infrastructure.actions.compute.StopComputeAction
import occi.infrastructure.actions.compute.SuspendComputeAction
import occi.lexpar.OcciParser
import occi.core.*

class ComputeTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
        mockDomain Category
        mockDomain Kind
        mockDomain AttributeType
        mockDomain AttributeValue
        mockDomain Action
        mockDomain Compute
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def compute = new Compute()
        assertFalse compute.validate();
        assertEquals 'nullable', compute.errors['kind']

        compute = new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        assertTrue compute.validate()
        assertNotNull compute.save()

        assertEquals 'compute', Compute.term
    }

    void testConstruction(){

        def compK = createComputeKind()

        Compute comp = new Compute(o_id: UUID.randomUUID().toString(), kind: compK)
        comp.addToAttrvals new AttributeValue(value: Compute.occi_compute_arch_X86, entity: comp,
                attributeInfo: AttributeType.findByName(Compute.occi_compute_arch)).save()
        comp.addToAttrvals new AttributeValue(value: 2, entity: comp,
                attributeInfo: AttributeType.findByName(Compute.occi_compute_cores)).save()

        assertNotNull comp.save()

        compK.addToEntities comp
        assertNotNull compK.save()


        comp = Compute.findByO_id(comp.o_id)
        assertNotNull comp
        
        assertTrue comp.kind.entities.size() == 1
        assertTrue comp.attrvals.size() == 2
    }

    void testSerialisation(){

        def compK = createComputeKind()
        Compute comp = new Compute(o_id: UUID.randomUUID().toString(), kind: compK)

        def ser = comp.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    void testRelatedTypes(){

        def comp = new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        assertNotNull comp.save()
        
        assertTrue comp.kind.related.size() == 1
        comp.kind.related.each {
            assertEquals Resource.term, it.term

            assertTrue it.related.size() == 1
            it.related.each {
                assertEquals Entity.term, it.term
                assertNull it.related
            }
        }
    }

    void testAttributes(){

        def comp = new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        assertNotNull comp.save()

        assertTrue comp.kind.attributes.size() == 6
    }

    void testActions(){
        def comp = new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        assertNotNull comp.save()

        assertTrue comp.kind.actions.size() == 4
    }


    void testConstructionViaAttrs(){

        //test the creation of a compute instance using attributes
        //rather than explicit class members

        //create the basic
        Kind compKind = createComputeKind()
        Compute comp = new Compute(o_id: UUID.randomUUID().toString(), kind: compKind)
        assertNotNull comp.save()
        
        //add attribute values
    }

    private Kind createComputeKind() {

        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def resKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '')
        resKind.addToRelated entKind
        assertNotNull resKind.save()

        //Add kind (type info)
        def compKind = new Kind(term: Compute.term, scheme: Compute.scheme, title: 'Compute type', location: '/compute/')
        compKind.addToRelated resKind

        addKindAttributes(compKind)
        addKindActions(compKind)

        assertNotNull compKind.save()

        return compKind
    }

    private def addKindActions(Kind compKind) {

        def startAction = new Action(term: StartComputeAction.term, scheme: StartComputeAction.scheme, title: 'Start Compute Action', location: '').save()
        compKind.addToActions startAction

        def stopAction = new Action(term: StopComputeAction.term, scheme: StopComputeAction.scheme, title: 'Stop Compute Action', location: '').save()
        def attr = new AttributeType(name: StopComputeAction.graceful, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.graceful}")
        stopAction.addToAttributes attr
        attr = new AttributeType(name: StopComputeAction.acpioff, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.acpioff}").save()
        stopAction.addToAttributes attr
        attr = new AttributeType(name: StopComputeAction.poweroff, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.poweroff}").save()
        stopAction.addToAttributes attr
        stopAction.save()
        compKind.addToActions stopAction

        def restartAction = new Action(term: RestartComputeAction.term, scheme: RestartComputeAction.scheme, title: 'Restart Compute Action', location: '').save()
        attr = new AttributeType(name: RestartComputeAction.graceful, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.graceful}").save()
        restartAction.addToAttributes attr
        attr = new AttributeType(name: RestartComputeAction.warm, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.warm}").save()
        restartAction.addToAttributes attr
        attr = new AttributeType(name: RestartComputeAction.cold, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.cold}").save()
        restartAction.addToAttributes attr
        restartAction.save()
        compKind.addToActions restartAction

        def suspendAction = new Action(term: SuspendComputeAction.term, scheme: SuspendComputeAction.scheme, title: 'Suspend Compute Action', location: '').save()
        attr = new AttributeType(name: SuspendComputeAction.hibernate, type: AttributeType.STRING, defaultValueExpr: "${SuspendComputeAction.hibernate}").save()
        suspendAction.addToAttributes attr
        attr = new AttributeType(name: SuspendComputeAction.suspend, type: AttributeType.STRING, defaultValueExpr: "${SuspendComputeAction.suspend}").save()
        suspendAction.addToAttributes attr
        suspendAction.save()
        compKind.addToActions suspendAction
    }

    private def addKindAttributes(Kind compKind) {

        def attr = new AttributeType(name: Compute.occi_compute_arch, type: AttributeType.STRING, defaultValueExpr: "${Compute.occi_compute_arch_X86}").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_cores, type: AttributeType.INT, defaultValueExpr: "0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_speed, type: AttributeType.FLOAT, defaultValueExpr: "0.0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_memory, type: AttributeType.FLOAT, defaultValueExpr: "0.0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_hostname, type: AttributeType.STRING, defaultValueExpr: "localhost").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_state, type: AttributeType.STRING, defaultValueExpr: "${Compute.occi_compute_state_inactive}").save()
        compKind.addToAttributes attr
    }

}
