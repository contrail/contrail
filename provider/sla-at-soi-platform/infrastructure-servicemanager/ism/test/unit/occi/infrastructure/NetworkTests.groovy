/*
SVN FILE: $Id: NetworkTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/infrastructure/NetworkTests.groovy $
*/

package occi.infrastructure

import grails.test.GrailsUnitTestCase
import occi.infrastructure.actions.network.DownNetworkAction
import occi.infrastructure.actions.network.UpNetworkAction
import occi.infrastructure.mixins.IPNetworkMixin
import occi.lexpar.OcciParser
import occi.core.*

class NetworkTests extends GrailsUnitTestCase {

    protected void setUp() {

        super.setUp()
        mockDomain Network
        mockDomain Kind
        mockDomain AttributeType
        mockDomain Category
        mockDomain Action
        mockDomain Mixin
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def network = new Network()
        assertFalse network.validate()
        assertEquals 'nullable', network.errors['kind']

        network = new Network(o_id: UUID.randomUUID().toString(), kind: createNetworkKind())
        assertTrue network.validate()
        assertNotNull network.save()

        assertEquals 'network', Network.term
    }

    void testConstruction(){

        def netK = createNetworkKind()
        def netM = createIpNetworkMixin(netK)


        def net = new Network(o_id: UUID.randomUUID().toString(), kind: netK)
        net.addToMixins netM
        assertNotNull net.save()

        netK.addToEntities net
        assertNotNull netK.save()


        net = Network.list()[0]
        assertNotNull net
        assertTrue net.kind.entities.size() == 1
        assertTrue net.mixins.size() == 1
    }

    void testSerialisation(){

        def netK = createNetworkKind()
        def net = new Network(o_id: UUID.randomUUID().toString(), kind: netK)
        assertNotNull net.save()

        def ser = net.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    void testSerialisationMixin(){

        def netK = createNetworkKind()
        def netM = createIpNetworkMixin(netK)
        def net = new Network(o_id: UUID.randomUUID().toString(), kind: netK)
        net.addToMixins netM
        assertNotNull net.save()

        def ser = net.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    void testRelatedTypes(){

        def net = new Network(o_id: UUID.randomUUID().toString(), kind: createNetworkKind())
        assertNotNull net.save()
        
        assertTrue net.kind.related.size() == 1
        net.kind.related.each {
            assertEquals Resource.term, it.term

            assertTrue it.related.size() == 1
            it.related.each {
                assertEquals Entity.term, it.term
                assertNull it.related
            }
        }
    }

    void testAttributes(){

        def net = new Network(o_id: UUID.randomUUID().toString(), kind: createNetworkKind())
        assertNotNull net.save()

        assertTrue net.kind.attributes.size() == 3
    }

    void testActions(){
        
        def net = new Network(o_id: UUID.randomUUID().toString(), kind: createNetworkKind())
        assertNotNull net.save()

        assertTrue net.kind.actions.size() == 2
    }

    private Kind createNetworkKind(){

        //create parent kinds
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def resKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '')
        resKind.addToRelated entKind
        assertNotNull resKind.save()

        //create network kind and associate it with parent kind
        def netKind = new Kind(term: Network.term, scheme: Network.scheme, title: 'Network type', location: '/network/')
        netKind.addToRelated resKind
        
        addKindAttributes(netKind)
        addKindActions(netKind)

        assertNotNull netKind.save()
        
        return netKind
    }

    private Mixin createIpNetworkMixin(applicableKind) {

        def ipMixin = new Mixin(term: IPNetworkMixin.term, scheme: IPNetworkMixin.scheme,
                title: "An IP Networking Mixin", applicableKind: applicableKind, location: '/network')

        //add attributes
        def attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_address, type: AttributeType.STRING,
                defaultValueExpr: "0.0.0.0").save()
        ipMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_gateway, type: AttributeType.STRING,
                defaultValueExpr: "255.255.255.255").save()
        ipMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_allocation, type: AttributeType.STRING,
                defaultValueExpr: "dynamic").save()
        ipMixin.addToAttributes attr

        ipMixin.save()
        assertNotNull ipMixin
        return ipMixin
    }

    private def addKindActions(Kind netKind) {

        def upAction = new Action(term: UpNetworkAction.term, scheme: UpNetworkAction.scheme, title: 'Up Network Action', location: '').save()
        netKind.addToActions upAction

        def downAction = new Action(term: DownNetworkAction.term, scheme: DownNetworkAction.scheme, title: 'Down Network Action', location: '').save()
        netKind.addToActions downAction
    }

    private def addKindAttributes(Kind netKind) {

        def attr

        attr = new AttributeType(name: Network.occi_network_vlan, type: AttributeType.INT, defaultValueExpr: "-1").save()
        netKind.addToAttributes attr
        attr = new AttributeType(name: Network.occi_network_label, type: AttributeType.STRING, defaultValueExpr: "NONE").save()
        netKind.addToAttributes attr
        attr = new AttributeType(name: Network.occi_network_state, type: AttributeType.STRING, defaultValueExpr: "${Network.occi_network_inactive}").save()
        netKind.addToAttributes attr
    }
}
