/*
SVN FILE: $Id: NetworkLinkTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/infrastructure/NetworkLinkTests.groovy $
*/

package occi.infrastructure

import grails.test.GrailsUnitTestCase

//TODO implement
class NetworkLinkTests extends GrailsUnitTestCase {
    
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSerialisaton() {

        
    }
}

/*//create a compute with a network link
    void testCreateComputeAndNetLink() {

        mockDomain Category
        mockDomain Compute
        mockDomain NetworkLink
        mockDomain Network

        //create categories
        assertEquals(0, Category.count())
        def compCat = new Category(term: ISMConstants.OCCI_INFRA_COMPUTE_TERM,
                scheme: ISMConstants.OCCI_INFRA_SCHEME,
                title: "a title")
        assertNotNull compCat.save()

        def linkCat = new Category(term: ISMConstants.OCCI_INFRA_NETWORK_TERM,
                scheme: ISMConstants.OCCI_INFRA_SCHEME,
                title: "a title")
        assertNotNull linkCat.save()

        def netLinkCat = new Category(term: ISMConstants.SLASOI_INFRA_NETLINK_TERM,
                scheme: ISMConstants.SLASOI_INFRA_SCHEME,
                title: "A network interface link")
        assertNotNull netLinkCat.save()

        def netCat = new Category(
                term: ISMConstants.OCCI_INFRA_NETWORK_TERM,
                scheme: ISMConstants.OCCI_INFRA_SCHEME,
                title: "The default shared network")
        assertNotNull netCat.save()
        assertEquals(4, Category.count())

        //create network link
        def nl = new NetworkLink(
                resourceId: "ff958879-4865-4bb7-8c69-92e32c508695",
                state: ISMConstants.ACTIVE,
                ip: "192.168.1.10",
                iface: "eth0",
                macAddress: "12:12:12:12:12:12"
        )

        nl.baseCategory = netLinkCat
        nl.addToCategories(netLinkCat)
        nl.addToCategories(linkCat)
        assertNotNull nl.save()

        //create compute
        assertEquals(0, Compute.count())
        def expensiveCompute = new Compute(
                hostname: "expensive.com",
                cpu_arch: "x64",
                cpu_cores: 4,
                cpu_speed: 2.4,
                memory_size: 4096,
                state: ISMConstants.INACTIVE
        )
        expensiveCompute.addToLinks(nl)
        expensiveCompute.baseCategory = compCat
        expensiveCompute.addToCategories(compCat)
        assertNotNull expensiveCompute.save()

        //create a Network to link to
        def defaultNetwork = new Network(
                state: ISMConstants.ACTIVE,
                resourceId: "8bed01a2-b3a5-4c7e-85a9-b51f37f1a998",
                network_vlan: "0",
                network_label: "default_public",
                network_address: "192.168.1.0/24",
                network_allocation: "DHCP"
        )

        defaultNetwork.baseCategory = netCat
        defaultNetwork.addToCategories(netCat)
        assertNotNull defaultNetwork.save()

        //Connect compute to network via netlink
        nl.sourceResource = expensiveCompute
        nl.targetResource = defaultNetwork
        assertNotNull nl.save()

        def header = 'Link: ' + nl.linkHeaderValue
        println header
    }*/
