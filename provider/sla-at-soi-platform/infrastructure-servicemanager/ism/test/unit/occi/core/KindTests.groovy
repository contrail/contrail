/*
SVN FILE: $Id: KindTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/KindTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.infrastructure.Compute
import occi.infrastructure.Network
import occi.infrastructure.actions.network.DownNetworkAction
import occi.infrastructure.actions.network.UpNetworkAction
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class KindTests extends GrailsUnitTestCase {

    def networkKind
    def vlanAttr
    def labelAttr
    def stateAttr
    //def upActionCat
    //def downActionCat
    def upAction
    def downAction

    protected void setUp() {
        super.setUp()
        mockDomain Kind
        mockDomain Category
        mockDomain Action

        networkKind = new Kind(term: Network.term, scheme: Network.scheme, title: "A network type", location: '/network/')
        vlanAttr = new AttributeType(name: Network.occi_network_vlan, type: AttributeType.INT)
        labelAttr = new AttributeType(name: Network.occi_network_label, type: AttributeType.STRING)
        stateAttr = new AttributeType(name: Network.occi_network_state, type: AttributeType.STRING)

        //upActionCat = new Category()
        //downActionCat = new Category()

        upAction = new Action(term: UpNetworkAction.term, scheme: UpNetworkAction.scheme, title: 'Up network action', location: '')
        downAction = new Action(term: DownNetworkAction.term, scheme: DownNetworkAction.scheme, title: 'Up network action', location: '')

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.renderFullTypes = true
        ConfigurationHolder.config = mockedConfig
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        // Validation should fail if categories are null.
        def kind = new Kind()
        assertFalse kind.validate()
        assertEquals "nullable", kind.errors["term"]
        assertEquals "nullable", kind.errors["scheme"]
        assertEquals '', kind.title


        kind = new Kind(term: Compute.term, scheme: Compute.scheme, title: "A compute type", location: '/compute/')
        assertTrue kind.validate()
    }

    void testConstruction() {

        persistNetworkKind()

        //look up the network kind and verify
        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)

        assertNotNull nKind
    }

    void testSerialisationWithAttrs() {

        persistNetworkKind()

        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)

        def kindStr = nKind.toString()
        assertNotNull kindStr

        def deser = OcciParser.getParser(kindStr).headers()
        assertNotNull deser
    }

    void testSerialisationWithoutAttrs() {

        //false indicates not to attach attributes
        persistNetworkKind(false)

        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)

        def kindStr = nKind.toString()
        assertNotNull kindStr

        def deser = OcciParser.getParser(kindStr).headers()
        assertNotNull deser
    }

    void testSerialisationWithActions() {

        persistNetworkKind()

        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        
        def kindStr = nKind.toString()
        assertNotNull kindStr

        def deser = OcciParser.getParser(kindStr).headers()
        assertNotNull deser
    }

    void testSerialisationWithoutActions() {

        persistNetworkKind(true, false)

        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        
        def kindStr = nKind.toString()
        assertNotNull kindStr

        def deser = OcciParser.getParser(kindStr).headers()
        assertNotNull deser
    }

    //Tree hierarchy - model currently allows for multiple inheritance - bad!
    void testRelatedTypes() {

        persistNetworkKind()

        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        assertEquals nKind.related.size(), 1
        nKind.related.each {
            assertEquals Resource.term, it.term

            assertEquals it.related.size(), 1
            it.related.each {
                assertEquals Entity.term, it.term

                assertNull it.related
            }
        }
    }

    void testAttributes(){
        persistNetworkKind()
        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        assertTrue nKind.attributes.size() == 3
    }

    void testActions(){
        persistNetworkKind()
        def nKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        assertTrue nKind.actions.size() == 2
    }
    
    private void persistNetworkKind(addAttrs = true, addActions = true) {

        //add parent kind
        def entity = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'An OCCI Entity type', location: '')
        assertNotNull entity.save()

        def resourceKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'An OCCI Resource type', location: '')
        resourceKind.addToRelated entity
        assertNotNull resourceKind.save()
        
        networkKind.addToRelated resourceKind

        //add the required attributes
        if (addAttrs) {
            networkKind.addToAttributes vlanAttr
            networkKind.addToAttributes labelAttr
            networkKind.addToAttributes stateAttr
            assertTrue networkKind.validate()
        }

        //persist the network kind
        //assertNotNull networkKind.save()

        //now add network actions {up, down}
        if (addActions) {
            //assertNotNull upActionCat.save()
            assertNotNull upAction.save()
            //assertNotNull downActionCat.save()
            assertNotNull downAction.save()

            //add actions to network
            networkKind.addToActions upAction
            networkKind.addToActions downAction
        }

        //save the network kind
        assertTrue networkKind.validate()
        assertNotNull networkKind.save()
    }
}
