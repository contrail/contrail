/*
SVN FILE: $Id: AttributeTypeTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/AttributeTypeTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class AttributeTypeTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
        mockDomain AttributeType
        mockDomain AttributeValue
        mockDomain Kind
        mockDomain Entity

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.renderFullTypes = true
        ConfigurationHolder.config = mockedConfig
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        //this should fail
        def attr = new AttributeType()
        assertFalse attr.validate()
        assertEquals "nullable", attr.errors["name"]
        assertEquals "nullable", attr.errors["type"]
        assertEquals "nullable", attr.errors["defaultValueExpr"]

        //this should fail
        attr = new AttributeType(name: "andy.test")
        assertFalse attr.validate()
        assertEquals "nullable", attr.errors["type"]
        assertEquals "nullable", attr.errors["defaultValueExpr"]

        //this should fail
        attr = new AttributeType(type: AttributeType.INT)
        assertFalse attr.validate()
        assertEquals "nullable", attr.errors["name"]

        //this should fail
        attr = new AttributeType(name: "andy", type: "some_unknown_type")
        assertFalse attr.validate()
        assertEquals "inList", attr.errors["type"]

        //this should fail
        //TODO reenable
//        attr = new AttributeType(name: "�()!@@@", type: AttributeType.INT)
//        assertFalse attr.validate()
//        assertEquals "matches", attr.errors["name"]

        //this should fail
        attr = new AttributeType(name: "andy", type: AttributeType.STRING, defaultValueExpr: "andy")
        assertTrue attr.validate()

        //this should pass
        attr = new AttributeType(name: "andy", type: AttributeType.STRING, defaultValueExpr: "andy")
        def attrval = new AttributeValue(entity: createEntity(), value: 'just a value', attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertTrue attr.validate()
    }

    void testConstruction(){
        
        def attr = new AttributeType(name: "andy", type: AttributeType.STRING, defaultValueExpr: "andy")
        def attrval = new AttributeValue(entity: createEntity(), value: 'just a value', attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertNotNull attr.save()

        attr = AttributeType.findByName('andy')
        assertNotNull attr
        assertEquals attr.name, 'andy'
        assertEquals attr.attrVals.size(), 1
    }

    void testDefaultValueEval(){

        AttributeType attr = new AttributeType(name: "andy.attr1", type: AttributeType.STRING, defaultValueExpr: "andy")
        def attrval = new AttributeValue(entity: createEntity(), value: 'just a value', attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertNotNull attr.save()
        assertEquals "andy", attr.defaultValueExpr

        attr = new AttributeType(name: "andy.attr2", type: AttributeType.INT, defaultValueExpr: "12")
        attrval = new AttributeValue(entity: createEntity(), value: 122, attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertNotNull attr.save()
        assertEquals "12", attr.defaultValueExpr

        attr = new AttributeType(name: "andy.attr3", type: AttributeType.FLOAT, defaultValueExpr: "12.12")
        attrval = new AttributeValue(entity: createEntity(), value: 12.13, attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertNotNull attr.save()
        assertEquals "12.12", attr.defaultValueExpr
        
    }

    void testSerialisation() {
        
        AttributeType attr = new AttributeType(name: "andy.attr", type: AttributeType.STRING, defaultValueExpr: "andy")
        def attrval = new AttributeValue(entity: createEntity(), value: 'just a value', attributeInfo: attr).save()
        attr.addToAttrVals attrval
        assertNotNull attr.save()
        assertEquals "andy.attr{immutable string}=andy", attr.toString()
    }

    private Entity createEntity(){
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def ent = new Entity(kind: entKind).save()
        return ent
    }
}
