/*
SVN FILE: $Id: MixinTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/MixinTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.infrastructure.Network
import occi.infrastructure.mixins.IPNetworkInterfaceMixin
import occi.infrastructure.mixins.IPNetworkMixin
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class MixinTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
        mockDomain Mixin
        mockDomain AttributeType
        mockDomain Action
        mockDomain Category
        mockDomain Kind

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.renderFullTypes = true
        ConfigurationHolder.config = mockedConfig
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def mixin = new Mixin()
        assertFalse mixin.validate()
        assertEquals 'nullable', mixin.errors['term']
        assertEquals 'nullable', mixin.errors['scheme']
        //assertEquals 'nullable', mixin.errors['applicableKind']
        assertEquals '', mixin.title

        mixin = new Mixin(term: IPNetworkMixin.term, scheme: IPNetworkMixin.scheme, title: "A IP Networking Mixin", location: '/network/')
        mixin.applicableKind = new Kind(term: Network.term, scheme: Network.scheme, location: '/network/').save()
        assertTrue mixin.validate()
    }

    void testConstruction(){


        persistIpNetworkMixin()

        def ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull ipM

    }

    //Loose hierarchy
    void testRelatedTypes() {

        persistIpNetworkMixin()

        def ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNull ipM.related

        def relM = new Mixin(term: "fancypantsipnetwork", scheme: "http://schemas.ogf.org/occi/infrastructure/network#", title: "A Fancier IP Networking Mixin", location: '/fancy/')
        relM.applicableKind = ipM.applicableKind
        
        assertNotNull relM.save()
        ipM.addToRelated relM 
        assertNotNull ipM.save()

        ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertTrue ipM.related.size() == 1
    }

    void testActions() {

        //let's just use the ip network mixin - doesn't matter which - each has no actions by spec
        persistIpNetworkMixin()
        
        def ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNull ipM.actions

        //just as a test, let's add an action

        ipM.addToActions new Action(category: new Category(term: 'random', scheme: 'http://schemas.ogf.org/occi/infrastructure/network/action#', title: 'Random action', location: '').save()).save()
        assertNotNull ipM.save()

        ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertTrue ipM.actions.size() == 1
    }

    void testAttributes(){
        
        persistIpNetworkMixin()

        def ipM = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertTrue ipM.attributes.size() == 3

        persistIpNetworkInterfaceMixin()

        ipM = Mixin.findByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)
        assertTrue ipM.attributes.size() == 3
    }

    void testSerialisation(){
        def mixin = persistIpNetworkMixin()

        def ser = mixin.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    private def persistIpNetworkMixin() {

        def kind = new Kind(term: Network.term, scheme: Network.scheme, location: '/network/').save()
        def ipMixin = new Mixin(term: IPNetworkMixin.term, scheme: IPNetworkMixin.scheme,
                title: "An IP Networking Mixin", location: '/network/')

        ipMixin.applicableKind = kind

        //add attributes
        def attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_address, type: AttributeType.STRING,
                defaultValueExpr: "0.0.0.0").save()
        ipMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_gateway, type: AttributeType.STRING,
                defaultValueExpr: "255.255.255.255").save()
        ipMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkMixin.occi_network_allocation, type: AttributeType.STRING,
                defaultValueExpr: "dynamic").save()
        ipMixin.addToAttributes attr

        //add actions
        // - none


        assertNotNull ipMixin.save()

        return ipMixin
    }

    private def persistIpNetworkInterfaceMixin() {

        def kind = new Kind(term: Network.term, scheme: Network.scheme, location: '/network/').save()
        def ipInterfaceMixin = new Mixin(term: IPNetworkInterfaceMixin.term, scheme: IPNetworkInterfaceMixin.scheme,
                title: "An IP Network Interface Mixin", location: '/network/')

        ipInterfaceMixin.applicableKind = kind

        //add attributes
        def attr
        attr = new AttributeType(name: IPNetworkInterfaceMixin.occi_networkinterface_address,
                type: AttributeType.STRING, defaultValueExpr: "0.0.0.0").save()
        ipInterfaceMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkInterfaceMixin.occi_networkinterface_gateway,
                type: AttributeType.STRING, defaultValueExpr: "255.255.255.255").save()
        ipInterfaceMixin.addToAttributes attr
        attr = new AttributeType(name: IPNetworkInterfaceMixin.occi_networkinterface_allocation,
                type: AttributeType.STRING, defaultValueExpr: "dynamic").save()
        ipInterfaceMixin.addToAttributes attr

        //add actions
        // - none

        ipInterfaceMixin.save()

        return ipInterfaceMixin
    }
}
