package occi.core

import grails.test.GrailsUnitTestCase
import occi.lexpar.OcciParser

class AttributeValueTests extends GrailsUnitTestCase {
    
    protected void setUp() {
        super.setUp()
        mockDomain AttributeValue
        mockDomain AttributeType
        mockDomain Kind
        mockDomain Entity
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def attrval = new AttributeValue()
        assertFalse attrval.validate()
        assertEquals 'nullable', attrval.errors['value']
        assertEquals 'nullable', attrval.errors['attributeInfo']
        assertEquals 'nullable', attrval.errors['entity']

        def mockKind = new Kind(term: 'mock', scheme: 'http://my.mock.com/test#')
        def mockEntity = new Entity(kind: mockKind)

        def attrInfo = new AttributeType(name: 'myattr', type: AttributeType.STRING)
        attrval = new AttributeValue(value: 'myval', attributeInfo: attrInfo, entity: mockEntity)
        assertTrue attrval.validate()

        attrInfo = new AttributeType(name: 'myattr', type: AttributeType.INT)
        attrval = new AttributeValue(value: (12).toString(), attributeInfo: attrInfo, entity: mockEntity)
        attrval.validate()
        assertTrue attrval.validate()

        attrInfo = new AttributeType(name: 'myattr', type: AttributeType.FLOAT)
        attrval = new AttributeValue(value: (12.12).toString(), attributeInfo: attrInfo, entity: mockEntity)
        assertTrue attrval.validate()
    }

    void testQueryByAttrName(){
        
        //create a value (value: 'a value') and type (name: my.attr)
        def mockKind = new Kind(term: 'mock', scheme: 'http://my.mock.com/test#', location: '')
        assertNotNull mockKind.save()
        def mockEntity = new Entity(o_id: UUID.randomUUID().toString(), kind: mockKind)
        assertNotNull mockEntity.save()

        //create the attribute type
        def attrInfo1 = new AttributeType(name: 'my.attr', type: AttributeType.STRING, defaultValueExpr: 'a string')
        //create the value and associate it with it's type and it's instance (owner)
        def attrval = new AttributeValue(value: 'a value', attributeInfo: attrInfo1, entity: mockEntity).save()
        attrInfo1.addToAttrVals attrval
        assertNotNull attrInfo1.save()
        mockEntity.addToAttrvals attrval

        def attrInfo2 = new AttributeType(name: 'my.another.attr', type: AttributeType.STRING, defaultValueExpr: 'a string')
        attrval = new AttributeValue(value: 'another value', attributeInfo: attrInfo2, entity: mockEntity).save()
        attrInfo2.addToAttrVals attrval
        assertNotNull attrInfo2.save()
        mockEntity.addToAttrvals attrval

        assertNotNull mockEntity.save()

        //lookup the value by name
        def entity = Entity.findByO_id(mockEntity.o_id)
        assertNotNull entity

        def value = entity.getAttrValue('my.attr')
        assertNotNull value
        assertEquals 'a value', value.value
    }

    void testSerialisation(){

        def mockKind = new Kind(term: 'mock', scheme: 'http://my.mock.com/test#', location: '')
        assertNotNull mockKind.save()
        def mockEntity = new Entity(o_id: UUID.randomUUID().toString(), kind: mockKind)
        assertNotNull mockEntity.save()

        //create the attribute type
        def attrInfo1 = new AttributeType(name: 'my.attr', type: AttributeType.STRING, defaultValueExpr: 'a string')
        //create the value and associate it with it's type and it's instance (owner)
        def attrval = new AttributeValue(value: 'a value', attributeInfo: attrInfo1, entity: mockEntity).save()
        attrInfo1.addToAttrVals attrval
        assertNotNull attrInfo1.save()
        mockEntity.addToAttrvals attrval

        assertNotNull mockEntity.save()

        def ser = attrval.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }
}
