/*
SVN FILE: $Id: CategoryTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/CategoryTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.infrastructure.Compute
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class CategoryTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
        mockDomain Category
        mockDomain AttributeType

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.renderFullTypes = true
        ConfigurationHolder.config = mockedConfig
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        Category existingCategory = new Category(term: Compute.term, scheme: Compute.scheme, title: "a title", location: '/compute/')
        assertTrue existingCategory.validate()

        // Validation should fail if properties are null. Title is set to "" in the domain class
        def cat = new Category()
        assertFalse cat.validate()
        assertEquals "nullable", cat.errors["term"]
        assertEquals "nullable", cat.errors["scheme"]
        assertEquals "nullable", cat.errors["location"]

        //not allowed to have term and scheme blank!
        cat = new Category(term: "", scheme: "")
        assertFalse cat.validate()
        assertEquals "blank", cat.errors["term"]
        assertEquals "blank", cat.errors["scheme"]

        //term can't contain things other than [a-z0-9-_]+ scheme must be a URL
        // and title can't be set to null
        cat = new Category(term: "@�!!!!not_term", scheme: "notAURL", title: null, location: '')
        assertFalse cat.validate()
        assertEquals "matches", cat.errors["term"]
        assertEquals "url", cat.errors["scheme"]

        //scheme must end in a '#' (fragment)
        cat = new Category(term: Compute.term, scheme: 'http://www.test.com/scheme', title: "", location: '')
        assertFalse cat.validate()
        assertEquals "validator", cat.errors["scheme"]

        //this is the correct creation of a category - note '#'
        cat = new Category(term: Compute.term, scheme: Compute.scheme, title: "A compute type", location: '/compute/')
        assertTrue cat.validate()
    }

    void testConstruction(){
        
        createCategory()

        def cat = Category.findByTerm(Compute.term)

        assertEquals Compute.term, cat.term
        assertEquals Compute.scheme, cat.scheme
        assertEquals "A compute type", cat.title
    }

    void testAttributes() {

        createCategory()

        def cat = Category.findByScheme(Compute.scheme)
        assertEquals cat.attributes.size(), 2
    }

    void testSerialisationWithAttributes() {

        createCategory()
        def cat = Category.findByScheme(Compute.scheme)
        def ser = cat.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
        
    }

    void testSerialisationWithoutAttributes(){

        createCategory(false)
        def cat = Category.findByScheme(Compute.scheme)
        assertNotNull cat.toString()
    }

    private void createCategory(attributes=true) {

        Category cat = new Category(term: Compute.term, scheme: Compute.scheme, title: "A compute type", location: '/compute/')

        if(attributes){
            AttributeType arch = new AttributeType(name: Compute.occi_compute_arch, type: AttributeType.STRING)
            cat.addToAttributes arch
            AttributeType cores = new AttributeType(name: Compute.occi_compute_cores, type: AttributeType.INT)
            cat.addToAttributes cores
        }

        assertNotNull cat.save()
    }
}
