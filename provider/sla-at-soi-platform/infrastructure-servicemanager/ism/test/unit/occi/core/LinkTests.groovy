/*
SVN FILE: $Id: LinkTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/LinkTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class LinkTests extends GrailsUnitTestCase {

    protected void setUp() {
        
        super.setUp()
        mockDomain Link
        mockDomain Kind
        mockDomain AttributeType
        mockDomain Resource

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.render.fatlinks = true
        ConfigurationHolder.config = mockedConfig
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def link = new Link()
        assertFalse link.validate()

        assertEquals 'nullable', link.errors['sou_rce']
        assertEquals 'nullable', link.errors['tar_get']
        assertEquals 'nullable', link.errors['kind']

        def sourceResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())

        link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: sourceResource, tar_get: targetResource, kind: createLinkKind())
        assertTrue link.validate()
    }

    void testCreation(){

        assertTrue Link.list().size() == 0

        def sourceResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull sourceResource.save()
        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull targetResource.save()

        def lk = createLinkKind()

        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: sourceResource, tar_get: targetResource, kind: lk)
        assertNotNull link.save()

        lk.addToEntities link
        assertNotNull lk.save()

        assertTrue Link.list().size() == 1

        link = Link.list()[0]
        assertNotNull link

        assertTrue lk.entities.size() == 1
        assertTrue link.kind.entities.size() == 1
    }

    void testSerialisation(){

        def sourceResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull sourceResource.save()
        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull targetResource.save()
        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: sourceResource, tar_get: targetResource, kind: createLinkKind())
        assertNotNull link.save()

        def ser = link.toString()
        assertNotNull ser
        
        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    void testRelatedTypes(){

        def sourceResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull sourceResource.save()
        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull targetResource.save()
        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: sourceResource, tar_get: targetResource, kind: createLinkKind())
        assertNotNull link.save()

        assertTrue link.kind.related.size() == 1

        link.kind.related.each {
            assertEquals 'entity', it.term
            assertNull it.related
        }
        
    }

    void testAttributes(){

        def sourceResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull sourceResource.save()
        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull targetResource.save()
        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: sourceResource, tar_get: targetResource, kind: createLinkKind())
        assertNotNull link.save()

        assertTrue link.kind.attributes.size() == 2
    }

    private Kind createResourceKind() {

        //create parent kinds
        //we won't bother adding the attributes - see EntityTests
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '/entity/').save()
        def resKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '/resource/')
        resKind.addToRelated entKind

        //add attributes of resource
        def attr = new AttributeType(name: Resource.occi_core_summary, type: AttributeType.STRING,
                defaultValueExpr: "Summary").save()
        resKind.addToAttributes attr

        assertNotNull resKind.save()

        return resKind
    }

    private Kind createLinkKind(){
        
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '/entity/').save()
        def linkKind = new Kind(term: Link.term, scheme: Link.scheme, title: 'A Link Type', location: '/link/')
        linkKind.addToRelated entKind

        def attr = new AttributeType(name:Link.occi_link_source, type:AttributeType.STRING, defaultValueExpr: "NONE").save()
        linkKind.addToAttributes attr
        attr = new AttributeType(name:Link.occi_link_target, type:AttributeType.STRING, defaultValueExpr: "NONE").save()
        linkKind.addToAttributes attr

        assertNotNull linkKind.save()
        return linkKind
    }
}
