/*
SVN FILE: $Id: InfrastructureInstanceFactoryServiceTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/factory/InfrastructureInstanceFactoryServiceTests.groovy $
*/

package occi.factory

import eu.slasoi.infrastructure.model.Service
import eu.slasoi.infrastructure.model.mixins.SmallComputeResourceTemplate
import grails.test.GrailsUnitTestCase
import occi.infrastructure.mixins.IPNetworkInterfaceMixin
import occi.infrastructure.mixins.IPNetworkMixin
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import occi.core.*
import occi.infrastructure.*

//TODO more post-creation validation
class InfrastructureInstanceFactoryServiceTests extends GrailsUnitTestCase {

    InfrastructureInstanceFactoryService infrastructureInstanceFactory
    InfrastructureTypeFactoryService infrastructureTypeFactory
    
    protected void setUp() {

        super.setUp()
        mockDomain Kind
        mockDomain AttributeType
        mockDomain AttributeValue
        mockDomain Action
        mockDomain occi.core.Category
        mockDomain Compute
        mockDomain Network
        mockDomain Storage
        mockDomain occi.core.Mixin
        mockDomain Link
        mockDomain Service
        mockLogging InfrastructureTypeFactoryService
        mockLogging InfrastructureInstanceFactoryService

        def mockedConfig = new ConfigObject()
        mockedConfig.eu.slasoi.infrastructure.render.fatlinks = true
        mockedConfig.eu.slasoi.infrastructure.provisioning.defaultOS = 'http://sla-at-soi.eu/occi/os_templates#ubuntu_10-11'
        
        mockedConfig.eu.slasoi.infrastructure.resources = [
            enabled:[
                'compute':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Compute Type',
                    location:'/compute/',
                    attributes:[
                        'occi.compute.architecture':[default:'x86', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.cores':[default:'1', required:false, mutable:true, type:'int', render:true],
                        'occi.compute.memory':[default:'0.5', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.speed':[default:'2.4', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.hostname':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Start Compute Action'
                        ],
                        'stop':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Stop Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'restart':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Restart Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'suspend':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Suspend Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ]
                    ]
                ],
                'service':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Service Type',
                    location:'/service/',
                    attributes:[
                        'eu.slasoi.infrastructure.service.name':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.monitoring.config':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Start Service Action'],
                        'stop':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Stop Service Action'],
                        'restart':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Restart Service Action'],
                        'suspend':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Suspend Service Action']
                    ]
                ],
                'reservation':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Reservation Type',
                    location:'/reservation/',
                    attributes:[
                        //'eu.slasoi.infrastructure.reservation.state':[default:'active', required:false, mutable:false, type:'enum', render:true],
                        'eu.slasoi.infrastructure.reservation.leasePeriod':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.reservation.created':[default:'', required:false, mutable:true, type:'int', render:true]
                    ],
                    actions:[
                        'commit':[scheme:'http://sla-at-soi.eu/occi/infrastructure/reservation#', title:'Commit Reservation Action']
                    ]
                ]
            ],
            disabled:[
                'network':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Network type',
                    location:'/network/',
                    attributes:[
                        'occi.network.vlan':[default:'0', required:false, mutable:true, type:'int', render:true],
                        'occi.network.label':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'up':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Up Network Action'],
                        'down':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Down Network Action']
                    ]
                ],
                'storage':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Storage type',
                    location:'/storage/',
                    attributes:[
                        'occi.storage.size':[default:'10.0', required:false, mutable:true, type:'float', render:true],
                        'occi.storage.state':[default:'offline', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'online':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Online Storage Action'],
                        'offline':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Offline Storage Action'],
                        'backup':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Backup Storage Action'],
                        'snapshot':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Snapshot Storage Action'],
                        'resize':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Resize Storage Action',
                            attributes:['size':[default:'', required:false, mutable:true, type:'float', render:true]]
                        ],
                        'degrade':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Degrade Storage Action']
                    ]
                ],
            ]
        ]
        
        mockedConfig.eu.slasoi.infrastructure.links = [
            enabled:[
                'storagelink':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Storage Link',
                    location:'/storage/link/',
                    attributes:[
                        'occi.storagelink.deviceid':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.mountpoint':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ],
                'networkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Network Interface Link',
                    location:'/network/link/',
                    attributes:[
                        'occi.networkinterface.interface':[default:'eth0', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.mac':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]

        mockedConfig.eu.slasoi.infrastructure.mixins = [
            enabled:[
                'os_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'OS Template Mixin',
                    location:'',
                    attributes:[]
                ],
                'resource_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Resource Template Mixin',
                    location:'/template/',
                    attributes:[]
                ],
                'small':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Small Compute Resource Template',
                    location:'/template/small/',
                    attributes:[
                        'small.occi.compute.cores':[default:'1', required:false, mutable:false, type:'int', render:true],
                        'small.occi.compute.speed':[default:'1.0', required:false, mutable:false, type:'float', render:true],
                        'small.occi.compute.memory':[default:'1.0', required:false, mutable:false, type:'float', render:true],
                        'small.occi.available':[default:'60', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'medium':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Medium Compute Resource Template',
                    location:'/template/medium/',
                    attributes:[
                        'medium.occi.compute.cores':[default:'2', required:false, mutable:false, type:'int', render:true],
                        'medium.occi.compute.speed':[default:'2.0', required:false, mutable:false, type:'float', render:true],
                        'medium.occi.compute.memory':[default:'2.0', required:false, mutable:false, type:'float', render:true],
                        'medium.occi.available':[default:'30', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'large':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Large Compute Resource Template',
                    location:'/template/large/',
                    attributes:[
                        'large.occi.compute.cores':[default:'4', required:false, mutable:false, type:'int', render:true],
                        'large.occi.compute.speed':[default:'4.0', required:false, mutable:false, type:'float', render:true],
                        'large.occi.compute.memory':[default:'4.0', required:false, mutable:false, type:'float', render:true],
                        'large.occi.available':[default:'15', required:false, mutable:false, type:'int', render:true]
                    ]
                ]
            ],
            disabled:[
                'ipnetwork':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/network#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#network'],
                    title:'IP Network Mixin',
                    location:'/ipnetwork/',
                    attributes:[
                        'occi.network.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.allocation':[default:'dynamic', required:false, mutable:true, type:'enum', render:true]
                    ]
                ],
                'ipnetworkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/networkinterface#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#networkinterface'],
                    title:'IP Network Interface Mixin',
                    location:'/ipnetwork/ipnetworkinterface/',
                    attributes:[
                        'occi.networkinterface.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.allocation':[default:'dynamic', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'metric':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'Metric Mixin',
                    location:'',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'user':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/metric/compute/cpu#',
                    rel:[parent:'http://sla-at-soi.eu/occi/infrastructure#metric', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Metric Mixin',
                    location:'/metric/compute/cpu/user/',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'M', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'occi.compute.cpu', required:false, mutable:true, type:'string', render:true]
                    ]
                ]
            ]
        ]
        
        mockedConfig.eu.slasoi.infrastructure.templates = [
            enabled:[
                'ubuntu_9-10':[
                    scheme:'http://sla-at-soi.eu/occi/os_templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 9.10 LTS operating system',
                    location:'/os_tpl/ubuntu_9-10/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ],
                'ubuntu_10-11':[
                    scheme:'http://sla-at-soi.eu/occi/os_templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 10.11 LTS operating system',
                    location:'/os_tpl/ubuntu_10-11/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]

        ConfigurationHolder.config = mockedConfig

        infrastructureInstanceFactory = new InfrastructureInstanceFactoryService()
        infrastructureInstanceFactory.infrastructureTypeFactoryService = new InfrastructureTypeFactoryService()
        infrastructureInstanceFactory.infrastructureTypeFactoryService.initDefaults()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testComputeConstruction(){

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp
        assertEquals 9, comp.attrvals.size()

        assertEquals 'This a brilliant summary of things!', comp.getAttrValue(Compute.occi_core_summary).getTypedValue()
        assertEquals Compute.occi_compute_arch_X64, comp.getAttrValue(Compute.occi_compute_arch).getTypedValue()
        assertEquals 2, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 'myhost', comp.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
        assertEquals 2.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
        assertEquals 2.4, comp.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals Compute.occi_compute_state_inactive, comp.getAttrValue(Compute.occi_compute_state).getTypedValue()

        def ser = comp.toString()
        assertNotNull ser
    }

    void testSmallComputeConstruction(){

        def computeAttributes = [:]
        def smallMixin = occi.core.Mixin.findBySchemeAndTerm('http://sla-at-soi.eu/occi/infrastructure/res_template#','small')
        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes, [smallMixin])

        assertNotNull comp

        assertEquals 1.0, comp.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 1, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 1.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testMediumComputeConstruction(){

        def computeAttributes = [:]
        def mediumMixin = occi.core.Mixin.findBySchemeAndTerm('http://sla-at-soi.eu/occi/infrastructure/res_template#','medium')
        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes, [mediumMixin])

        assertNotNull comp

        assertEquals 2.0, comp.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 2, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 2.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testLargeComputeConstruction(){

        def computeAttributes = [:]
        def largeMixin = occi.core.Mixin.findBySchemeAndTerm('http://sla-at-soi.eu/occi/infrastructure/res_template#','large')
        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes, [largeMixin])

        assertNotNull comp

        assertEquals 4.0, comp.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 4, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 4.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testNetworkConstruction(){

        def networkAttributes = [:]
        networkAttributes[Network.occi_network_vlan] = 23
        networkAttributes[Network.occi_network_label] = 'private'
//        networkAttributes[Network.occi_network_state] = Network.occi_network_active

        Network net = infrastructureInstanceFactory.createNetwork(networkAttributes)
        assertNotNull net
        assertEquals 6, net.attrvals.size()

        assertEquals 23, net.getAttrValue(Network.occi_network_vlan).getTypedValue()
        assertEquals 'private', net.getAttrValue(Network.occi_network_label).getTypedValue()
        assertEquals Network.occi_network_inactive, net.getAttrValue(Network.occi_network_state).getTypedValue()

        def ser = net.toString()
        assertNotNull ser
    }

    void testNetworkAndMixinConstruction(){

        def networkAttributes = [:]
        networkAttributes[Network.occi_network_vlan] = 23
        networkAttributes[Network.occi_network_label] = 'private'
//        networkAttributes[Network.occi_network_state] = Network.occi_network_active
        networkAttributes[IPNetworkMixin.occi_network_address] = '192.168.1.10'
        networkAttributes[IPNetworkMixin.occi_network_gateway] = '255.255.255.0'
        networkAttributes[IPNetworkMixin.occi_network_allocation] = IPNetworkMixin.occi_network_allocation_dynamic

        def mixin = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull mixin

        Network net = infrastructureInstanceFactory.createNetwork(networkAttributes, [mixin])
        assertNotNull net
        assertEquals 9, net.attrvals.size()

        //check type info
        assertEquals Network.term, net.kind.term
        assertTrue net.mixins.size() == 1

        //check attributes
        assertEquals 23, net.getAttrValue(Network.occi_network_vlan).getTypedValue()
        assertEquals 'private', net.getAttrValue(Network.occi_network_label).getTypedValue()
        assertEquals Network.occi_network_inactive, net.getAttrValue(Network.occi_network_state).getTypedValue()

        assertEquals '192.168.1.10', net.getAttrValue(IPNetworkMixin.occi_network_address).getTypedValue()
        assertEquals '255.255.255.0', net.getAttrValue(IPNetworkMixin.occi_network_gateway).getTypedValue()
        assertEquals IPNetworkMixin.occi_network_allocation_dynamic, net.getAttrValue(IPNetworkMixin.occi_network_allocation).getTypedValue()

        def ser = net.toString()
        assertNotNull ser
    }

    void testStorageConstruction(){

        def storageAttributes = [:]
        storageAttributes[Storage.occi_storage_size] = 10.0
//        storageAttributes[Storage.occi_storage_state] = Storage.occi_storage_state_online

        Storage stor = infrastructureInstanceFactory.createStorage(storageAttributes)
        assertNotNull stor
        assertEquals 5, stor.attrvals.size()

        assertEquals 10.0, stor.getAttrValue(Storage.occi_storage_size).getTypedValue()
//        assertEquals Storage.occi_storage_state_online, stor.getAttrValue(Storage.occi_storage_state).getTypedValue()

        def ser = stor.toString()
        assertNotNull ser
    }

    void testServiceConstruction(){
        //create 2 resources and encapsulate in a service
        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
        //computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp1 = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp1

        computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
        //computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp2 = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp2

        def svcAttrs = [:]
        svcAttrs[Service.eu_slasoi_infrastructure_service_name] = 'kittenz service'
        svcAttrs[Resource.occi_core_summary] = 'awww they\'re cute and fluffy'
        svcAttrs[Service.eu_slasoi_infrastructure_service_monitoringconfig] = ''
        //svcAttrs[Service.eu_slasoi_infrastructure_service_state] = Service.eu_slasoi_infrastructure_service_state_active
        
        Service svc = infrastructureInstanceFactory.createServiceI([comp1, comp2], svcAttrs)

        assertNotNull svc
        assertEquals 6, svc.attrvals.size()
        assertEquals 2, svc.resources.size()

        assertEquals 'kittenz service', svc.getAttrValue(Service.eu_slasoi_infrastructure_service_name).getTypedValue()
        assertEquals 'awww they\'re cute and fluffy', svc.getAttrValue(Resource.occi_core_summary).getTypedValue()
        //assertEquals Service.eu_slasoi_infrastructure_service_state_active, svc.getAttrValue(Service.eu_slasoi_infrastructure_service_state).getTypedValue()

        def ser = svc.toString()
        assertNotNull ser
    }

    void testOsTemplateConstruction(){

        infrastructureInstanceFactory.createOsTemplate('ubuntu-9_10', 'http://sla-at-soi.eu/occi/templates/os#',
                'This is an ubuntu OS template')

        def template = Mixin.findByTermAndScheme('ubuntu-9_10', 'http://sla-at-soi.eu/occi/templates/os#')
        assertNotNull template
        assertEquals 'ubuntu-9_10', template.term

        def ser = template.toString()
        assertNotNull ser
    }

    void testComputeResourceTemplateConstruction(){

        def resourceTemplateAttrs = []
        //attributes may be immutable as they're related to template
        resourceTemplateAttrs << new AttributeType(name: SmallComputeResourceTemplate.small_cpucores,
                                                        type: AttributeType.INT, defaultValueExpr: '1',
                                                        renderme: true, mutable: true, required: false).save()
        resourceTemplateAttrs << new AttributeType(name: SmallComputeResourceTemplate.small_cpuspeed,
                                                        type: AttributeType.FLOAT, defaultValueExpr: '1.8',
                                                        renderme: true, mutable: true, required: false).save()
        resourceTemplateAttrs << new AttributeType(name: SmallComputeResourceTemplate.small_memory,
                                                        type: AttributeType.FLOAT, defaultValueExpr: '0.5',
                                                        renderme: true, mutable: true, required: false).save()

        def applicableKind = Kind.findByTermAndScheme(Compute.term, Compute.scheme)
        
        infrastructureInstanceFactory.createResourceTemplate(SmallComputeResourceTemplate.term, SmallComputeResourceTemplate.scheme,
                'This is similar to an EC2 small instance', applicableKind, resourceTemplateAttrs)

        def template = Mixin.findByTermAndScheme(SmallComputeResourceTemplate.term, SmallComputeResourceTemplate.scheme)
        assertNotNull template
        assertEquals SmallComputeResourceTemplate.term, template.term
        assertEquals 3, template.attributes.size()

        def ser = template.toString()
        assertNotNull ser
    }

    void testBasicResourceLinkage(){

        //create the two resources to link
        def networkAttributes = [:]
        networkAttributes[Network.occi_network_vlan] = 23
        networkAttributes[Network.occi_network_label] = 'private'
//        networkAttributes[Network.occi_network_state] = Network.occi_network_active
        networkAttributes[IPNetworkMixin.occi_network_address] = '192.168.1.10'
        networkAttributes[IPNetworkMixin.occi_network_gateway] = '255.255.255.0'
        networkAttributes[IPNetworkMixin.occi_network_allocation] = IPNetworkMixin.occi_network_allocation_dynamic

        def mixin = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull mixin

        Network net = infrastructureInstanceFactory.createNetwork(networkAttributes, [mixin])
        assertNotNull net

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp

        //create the link between the 2 entities
        Link link = infrastructureInstanceFactory.link(comp, net)
        assertNotNull link

        assertEquals Link.term, link.kind.term
        assertEquals 1, comp.links.size()

        def ser = link.toString() //No attrs?!
        assertNotNull ser
        def deSer = deserialise(ser)
        assertNotNull deSer

        ser = comp.toString()
        assertNotNull ser
        deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 'This a brilliant summary of things!', deSer[OcciParser.occi_attributes][0][Compute.occi_core_summary]
        assertEquals Compute.occi_compute_arch_X64,         deSer[OcciParser.occi_attributes][0][Compute.occi_compute_arch]
        assertEquals 2,                                   deSer[OcciParser.occi_attributes][0][Compute.occi_compute_cores]
        assertEquals 'myhost',                              deSer[OcciParser.occi_attributes][0][Compute.occi_compute_hostname]
        assertEquals 2.0,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_memory]
        assertEquals 2.4,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_speed]
//        assertEquals Compute.occi_compute_state_active,     deSer[OcciParser.occi_attributes][0][Compute.occi_compute_state]

        ser = net.toString()
        assertNotNull ser
        deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 23, deSer[OcciParser.occi_attributes][0][Network.occi_network_vlan]
        assertEquals 'private', deSer[OcciParser.occi_attributes][0][Network.occi_network_label]
//        assertEquals Network.occi_network_active, deSer[OcciParser.occi_attributes][0][Network.occi_network_state]
        assertEquals '192.168.1.10', deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_address]
        assertEquals '255.255.255.0', deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_gateway]
        assertEquals IPNetworkMixin.occi_network_allocation_dynamic, deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_allocation]
    }

    void testNetworkLinkage(){

        //create the two resources to link
        def networkAttributes = [:]
        networkAttributes[Network.occi_network_vlan] = 23
        networkAttributes[Network.occi_network_label] = 'private'
//        networkAttributes[Network.occi_network_state] = Network.occi_network_active
        networkAttributes[IPNetworkMixin.occi_network_address] = '192.168.1.10'
        networkAttributes[IPNetworkMixin.occi_network_gateway] = '255.255.255.0'
        networkAttributes[IPNetworkMixin.occi_network_allocation] = IPNetworkMixin.occi_network_allocation_dynamic

        def mixin = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull mixin

        Network net = infrastructureInstanceFactory.createNetwork(networkAttributes, [mixin])
        assertNotNull net

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp

        //create the link between the 2 entities
        def linkAttrs = [:]
        linkAttrs[NetworkLink.occi_networkinterface_interface] = 'eth0'
        linkAttrs[NetworkLink.occi_networkinterface_mac] = 'DE:AD:BE:EF:00:00'
//        linkAttrs[NetworkLink.occi_networkinterface_state] = NetworkLink.occi_networkinterface_state_inactive
        linkAttrs[IPNetworkInterfaceMixin.occi_networkinterface_address] = ''
        linkAttrs[IPNetworkInterfaceMixin.occi_networkinterface_gateway] = ''
        linkAttrs[IPNetworkInterfaceMixin.occi_networkinterface_allocation] = IPNetworkInterfaceMixin.occi_networkinterface_allocation_dynamic

        mixin = Mixin.findByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)
        assertNotNull mixin

        Link link = infrastructureInstanceFactory.linkNetwork(comp, net, linkAttrs, [mixin])
        assertNotNull link

        assertEquals NetworkLink.term, link.kind.term
        assertEquals 1, link.mixins.size()
        assertEquals 1, comp.links.size()

        def ser = link.toString()
        assertNotNull ser
        def deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 'eth0', deSer[OcciParser.occi_attributes][0][NetworkLink.occi_networkinterface_interface]
        assertEquals 'DE:AD:BE:EF:00:00', deSer[OcciParser.occi_attributes][0][NetworkLink.occi_networkinterface_mac]
//        assertEquals NetworkLink.occi_networkinterface_state_inactive, deSer[OcciParser.occi_attributes][0][NetworkLink.occi_networkinterface_state]
        assertEquals '', deSer[OcciParser.occi_attributes][0][IPNetworkInterfaceMixin.occi_networkinterface_address]
        assertEquals '', deSer[OcciParser.occi_attributes][0][IPNetworkInterfaceMixin.occi_networkinterface_gateway]
        assertEquals IPNetworkInterfaceMixin.occi_networkinterface_allocation_dynamic, deSer[OcciParser.occi_attributes][0][IPNetworkInterfaceMixin.occi_networkinterface_allocation]

        ser = net.toString()
        assertNotNull ser
        deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 23, deSer[OcciParser.occi_attributes][0][Network.occi_network_vlan]
        assertEquals 'private', deSer[OcciParser.occi_attributes][0][Network.occi_network_label]
//        assertEquals Network.occi_network_active, deSer[OcciParser.occi_attributes][0][Network.occi_network_state]
        assertEquals '192.168.1.10', deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_address]
        assertEquals '255.255.255.0', deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_gateway]
        assertEquals IPNetworkMixin.occi_network_allocation_dynamic, deSer[OcciParser.occi_attributes][0][IPNetworkMixin.occi_network_allocation]

        ser = comp.toString()
        assertNotNull ser
        deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 'This a brilliant summary of things!', deSer[OcciParser.occi_attributes][0][Compute.occi_core_summary]
        assertEquals Compute.occi_compute_arch_X64,         deSer[OcciParser.occi_attributes][0][Compute.occi_compute_arch]
        assertEquals 2,                                   deSer[OcciParser.occi_attributes][0][Compute.occi_compute_cores]
        assertEquals 'myhost',                              deSer[OcciParser.occi_attributes][0][Compute.occi_compute_hostname]
        assertEquals 2.0,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_memory]
        assertEquals 2.4,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_speed]
//        assertEquals Compute.occi_compute_state_active,     deSer[OcciParser.occi_attributes][0][Compute.occi_compute_state]
    }

    void testStorageLinkage(){

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute comp = infrastructureInstanceFactory.createComputeI(computeAttributes)
        assertNotNull comp

        def storageAttributes = [:]
        storageAttributes[Storage.occi_storage_size] = 10.0
//        storageAttributes[Storage.occi_storage_state] = Storage.occi_storage_state_online

        Storage stor = infrastructureInstanceFactory.createStorage(storageAttributes)
        assertNotNull stor

        def linkAttrs = [:]
        linkAttrs[StorageLink.occi_storagelink_deviceid] = '123-123-123'
        linkAttrs[StorageLink.occi_storagelink_mountpoint] = '/mnt/123-123-123'
//        linkAttrs[StorageLink.occi_storagelink_state] = StorageLink.occi_storagelink_state_inactive
        
        Link link = infrastructureInstanceFactory.linkStorage(comp, stor, linkAttrs)
        assertNotNull link

        assertEquals StorageLink.term, link.kind.term
        assertEquals 1, comp.links.size()

        def ser = link.toString()
        assertNotNull ser

        def deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals '/mnt/123-123-123', deSer[OcciParser.occi_attributes][0][StorageLink.occi_storagelink_mountpoint]
        assertEquals '123-123-123', deSer[OcciParser.occi_attributes][0][StorageLink.occi_storagelink_deviceid]
//        assertEquals StorageLink.occi_storagelink_state_inactive, deSer[OcciParser.occi_attributes][0][StorageLink.occi_storagelink_state]
        
        ser = stor.toString()
        assertNotNull ser

        deSer = deserialise(ser)
        assertNotNull deSer
        assertEquals 10.0, deSer[OcciParser.occi_attributes][0][Storage.occi_storage_size]
//        assertEquals Storage.occi_storage_state_online, deSer[OcciParser.occi_attributes][0][Storage.occi_storage_state]

        ser = comp.toString()
        assertNotNull ser

        deSer = deserialise(ser)
        assertNotNull deSer

        assertEquals 'This a brilliant summary of things!', deSer[OcciParser.occi_attributes][0][Compute.occi_core_summary]
        assertEquals Compute.occi_compute_arch_X64,         deSer[OcciParser.occi_attributes][0][Compute.occi_compute_arch]
        assertEquals 2,                                   deSer[OcciParser.occi_attributes][0][Compute.occi_compute_cores]
        assertEquals 'myhost',                              deSer[OcciParser.occi_attributes][0][Compute.occi_compute_hostname]
        assertEquals 2.0,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_memory]
        assertEquals 2.4,                                 deSer[OcciParser.occi_attributes][0][Compute.occi_compute_speed]
//        assertEquals Compute.occi_compute_state_active,     deSer[OcciParser.occi_attributes][0][Compute.occi_compute_state]
    }

    private def deserialise(String serialised){
        def deSerialised
        try{
            OcciParser p = OcciParser.getParser(serialised)
            deSerialised = p.headers();

            return  deSerialised
        }
        catch (Exception e){
            println e.toString()
        }
    }
}
