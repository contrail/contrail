/*
SVN FILE: $Id: InfrastructureServiceTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/eu/slasoi/infrastructure/services/InfrastructureServiceTests.groovy $
*/

package eu.slasoi.infrastructure.services

import grails.test.GrailsUnitTestCase

class InfrastructureServiceTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCreateCompute() {

        //mock the config
        /*def config = new ConfigObject()
        config.eu.slasoi.infrastructure.provisioning.enable = false
        ConfigurationHolder.config = config

        mockDomain Category
        mockDomain Compute
        mockDomain Task
        mockDomain Resource
        mockDomain Network
        mockDomain NetworkLink
        mockLogging InfrastructureService

        //create category
        assertEquals(0, Category.count())
        def cat = new Category(term: "compute",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull cat.save()
        def ncat = new Category(term: "network",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull ncat.save()
        def svcCat = new Category(term: "service",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull svcCat.save()

        def netLinkCat = new Category(term: ISMConstants.SLASOI_INFRA_NETLINK_TERM, scheme: ISMConstants.SLASOI_INFRA_SCHEME, title: '')
        assertNotNull netLinkCat.save()
        def linkCat = new Category(term: ISMConstants.OCCI_CORE_LINK_TERM, scheme: ISMConstants.OCCI_CORE_SCHEME, title: '')
        assertNotNull linkCat.save()

        assertEquals(5, Category.count())

        //create default network
        def defaultNetwork = new Network(
                state: ISMConstants.ACTIVE,
                network_vlan: "0",
                network_label: ISMConstants.DEFAULT_NETWORK,
                network_address: "192.168.1.0/24",
                network_allocation: "DHCP"
        )
        defaultNetwork.baseCategory = ncat
        defaultNetwork.addToCategories(ncat)
        defaultNetwork.save()
        assertNotNull defaultNetwork

        //create compute
        assertEquals(0, Compute.count())
        def expensiveCompute = new Compute(
                hostname: "expensive.com",
                cpu_arch: "x64",
                cpu_cores: 4,
                cpu_speed: 2.4,
                memory_size: 4096,
                state: ISMConstants.INACTIVE
        )
        expensiveCompute.baseCategory = cat
        expensiveCompute.task = new Task(state: ISMConstants.ACTIVE, notificationUri: "andy@home.edmonds.be", resource: expensiveCompute)
        expensiveCompute.addToCategories(cat)
        assertNotNull expensiveCompute.save()

        def parser = mockFor(OcciHttpHeaderParserService)
        parser.demand.getResource(1..1) {-> return expensiveCompute}
        parser.demand.getCategories(1..1) {-> return [cat, svcCat, new Category(term: "task", scheme: "http://purl.org/occi/kind#", title: "a title")]}
        parser.demand.getAttributes(1..1) {->
            def attrs = [:]
            attrs[Task.NOTIFICATION_URI] = 'andy@edmonds.be'
            return attrs
        }

        def msgMock = mockFor(MessageDispatcher)
        msgMock.demand.sendMessage(1..2) { String to, String msg -> }

        InfrastructureService svc = new InfrastructureService()
        svc.occiHttpHeaderParserService = parser.createMock()
        svc.messagingDispatchService = msgMock.createMock()

        def compute = svc.getCompute()

        assertNotNull compute
        assertEquals 'andy@edmonds.be', compute.task.notificationUri*/
    }

    void testUpdateCompute() {

        /*mockLogging InfrastructureService
        mockDomain Compute
        mockDomain Resource
        mockDomain Task
        mockDomain Category

        //mock the config
        def config = new ConfigObject()
        config.eu.slasoi.infrastructure.provisioning.enable = false
        ConfigurationHolder.config = config

        def cat = new Category(term: "compute",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull cat.save()

        //create compute
        assertEquals(0, Compute.count())
        def expensiveCompute = new Compute(
                hostname: "expensive.com",
                cpu_arch: "x64",
                cpu_cores: 4,
                cpu_speed: 2.4,
                memory_size: 4096,
                state: ISMConstants.INACTIVE
        )
        expensiveCompute.baseCategory = cat
        expensiveCompute.task = new Task(state: ISMConstants.INACTIVE, resource: expensiveCompute)
        expensiveCompute.addToCategories(cat)
        assertNotNull expensiveCompute.save()

        assertEquals(1, Compute.count())

        def parser = mockFor(OcciHttpHeaderParserService)
        parser.demand.getAttributes(1..1) {->
            def attrs = [:]
            attrs[Compute.occi_compute.occi_compute_speed] = 2.6
            attrs[Compute.occi_compute_memory] = 2
            attrs[Compute.occi_compute_cores] = 6
            return attrs
        }

        def msgMock = mockFor(MessageDispatcher)
        msgMock.demand.sendMessage(1..2) { String to, String msg ->

        }

        InfrastructureService svc = new InfrastructureService()
        svc.occiHttpHeaderParserService = parser.createMock()
        svc.messagingDispatchService = msgMock.createMock()

        assertNotNull svc.updateCompute(expensiveCompute.resourceId)*/
    }

    void testDeleteCompute() {

        /*mockLogging InfrastructureService
        mockDomain Compute
        mockDomain Task
        mockDomain Category

        //mock the config
        def config = new ConfigObject()
        config.eu.slasoi.infrastructure.provisioning.enable = false
        ConfigurationHolder.config = config

        def cat = new Category(term: "compute",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull cat.save()

        //create compute
        assertEquals(0, Compute.count())
        def expensiveCompute = new Compute(
                hostname: "expensive.com",
                cpu_arch: "x64",
                cpu_cores: 4,
                cpu_speed: 2.4,
                memory_size: 4096,
                state: ISMConstants.INACTIVE
        )
        expensiveCompute.baseCategory = cat
        expensiveCompute.addToCategories(cat)
        assertNotNull expensiveCompute.save()

        assertEquals(1, Compute.count())

        InfrastructureService svc = new InfrastructureService()

        expensiveCompute.delete()
        svc.deleteCompute(expensiveCompute)

        assertEquals(0, Compute.count())*/
    }

    void testCreateService() {

//    //mock the config
//    def config = new ConfigObject()
//    config.eu.slasoi.infrastructure.provisioning.enable = false
//    ConfigurationHolder.config = config
//
//    mockDomain Category
//    mockDomain Compute
//    mockDomain Task
//    mockDomain Resource
//    mockDomain Service
//    mockLogging InfrastructureService
//
//    def infraSvcMock = mockFor(InfrastructureService)
//    infraSvcMock.demand.getResource(1..1){ -> return service}
//    infraSvcMock.demand.getAttributes(1..1){ ->
//      def attrs = [:]
//      attrs[Task.NOTIFICATION_URI] = 'andy@edmonds.be'
//      return attrs
//    }
//    infraSvcMock.demand.getCategories(1..1){ -> return [svcCat]}
//
//    def parsermock = mockFor(OcciHttpHeaderParserService)
//    parsermock.demand.parseBody(1..1){ServletRequest request -> ['']}
    }
}
