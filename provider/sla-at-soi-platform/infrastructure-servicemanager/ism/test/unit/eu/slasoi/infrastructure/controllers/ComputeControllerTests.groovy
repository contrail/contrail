/*
SVN FILE: $Id: ComputeControllerTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/eu/slasoi/infrastructure/controllers/ComputeControllerTests.groovy $
*/

package eu.slasoi.infrastructure.controllers

//import eu.slasoi.infrastructure.model.Task
//import eu.slasoi.infrastructure.model.mixins.SlaSoiComputeMixin
import grails.test.ControllerUnitTestCase
import occi.factory.InfrastructureInstanceFactoryService
import occi.factory.InfrastructureTypeFactoryService
import occi.infrastructure.Compute
import occi.lexpar.OcciParser
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import occi.core.*

class ComputeControllerTests extends ControllerUnitTestCase {

//    InfrastructureInstanceFactoryService iifs
    def cc
    
    protected void setUp() {
        super.setUp()

        def config = new ConfigObject()
        config.grails.serverURL = 'http://localhost:8080'

        config.eu.slasoi.infrastructure.provisioning.defaultOS = 'http://sla-at-soi.eu/occi/os_templates#ubuntu_10-11'

        config.eu.slasoi.infrastructure.resources = [
            enabled:[
                'compute':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Compute Type',
                    location:'/compute/',
                    attributes:[
                        'occi.compute.architecture':[default:'x86', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.cores':[default:'1', required:false, mutable:true, type:'int', render:true],
                        'occi.compute.memory':[default:'0.5', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.speed':[default:'2.4', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.hostname':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Start Compute Action'
                        ],
                        'stop':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Stop Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'restart':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Restart Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'suspend':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Suspend Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ]
                    ]
                ],
                'service':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Service Type',
                    location:'/service/',
                    attributes:[
                        'eu.slasoi.infrastructure.service.name':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.monitoring.config':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Start Service Action'],
                        'stop':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Stop Service Action'],
                        'restart':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Restart Service Action'],
                        'suspend':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Suspend Service Action']
                    ]
                ],
                'reservation':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Reservation Type',
                    location:'/reservation/',
                    attributes:[
                        //'eu.slasoi.infrastructure.reservation.state':[default:'active', required:false, mutable:false, type:'enum', render:true],
                        'eu.slasoi.infrastructure.reservation.leasePeriod':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.reservation.created':[default:'', required:false, mutable:true, type:'int', render:true]
                    ],
                    actions:[
                        'commit':[scheme:'http://sla-at-soi.eu/occi/infrastructure/reservation#', title:'Commit Reservation Action']
                    ]
                ]
            ],
            disabled:[
                'network':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Network type',
                    location:'/network/',
                    attributes:[
                        'occi.network.vlan':[default:'0', required:false, mutable:true, type:'int', render:true],
                        'occi.network.label':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'up':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Up Network Action'],
                        'down':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Down Network Action']
                    ]
                ],
                'storage':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Storage type',
                    location:'/storage/',
                    attributes:[
                        'occi.storage.size':[default:'10.0', required:false, mutable:true, type:'float', render:true],
                        'occi.storage.state':[default:'offline', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'online':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Online Storage Action'],
                        'offline':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Offline Storage Action'],
                        'backup':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Backup Storage Action'],
                        'snapshot':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Snapshot Storage Action'],
                        'resize':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Resize Storage Action',
                            attributes:['size':[default:'', required:false, mutable:true, type:'float', render:true]]
                        ],
                        'degrade':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Degrade Storage Action']
                    ]
                ],
            ]
        ]

        config.eu.slasoi.infrastructure.links = [
            enabled:[
                'storagelink':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Storage Link',
                    location:'/storage/link/',
                    attributes:[
                        'occi.storagelink.deviceid':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.mountpoint':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ],
                'networkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Network Interface Link',
                    location:'/network/link/',
                    attributes:[
                        'occi.networkinterface.interface':[default:'eth0', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.mac':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]

        config.eu.slasoi.infrastructure.mixins = [
            enabled:[
                'os_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'OS Template Mixin',
                    location:'',
                    attributes:[]
                ],
                'resource_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Resource Template Mixin',
                    location:'/template/',
                    attributes:[]
                ],
                'small':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Small Compute Resource Template',
                    location:'/template/small/',
                    attributes:[
                        'small.occi.compute.cores':[default:'1', required:false, mutable:false, type:'int', render:true],
                        'small.occi.compute.speed':[default:'1.0', required:false, mutable:false, type:'float', render:true],
                        'small.occi.compute.memory':[default:'1.0', required:false, mutable:false, type:'float', render:true],
                        'small.occi.available':[default:'60', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'medium':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Medium Compute Resource Template',
                    location:'/template/medium/',
                    attributes:[
                        'medium.occi.compute.cores':[default:'2', required:false, mutable:false, type:'int', render:true],
                        'medium.occi.compute.speed':[default:'2.0', required:false, mutable:false, type:'float', render:true],
                        'medium.occi.compute.memory':[default:'2.0', required:false, mutable:false, type:'float', render:true],
                        'medium.occi.available':[default:'30', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'large':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/res_template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Large Compute Resource Template',
                    location:'/template/large/',
                    attributes:[
                        'large.occi.compute.cores':[default:'4', required:false, mutable:false, type:'int', render:true],
                        'large.occi.compute.speed':[default:'4.0', required:false, mutable:false, type:'float', render:true],
                        'large.occi.compute.memory':[default:'4.0', required:false, mutable:false, type:'float', render:true],
                        'large.occi.available':[default:'15', required:false, mutable:false, type:'int', render:true]
                    ]
                ]
            ],
            disabled:[
                'ipnetwork':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/network#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#network'],
                    title:'IP Network Mixin',
                    location:'/ipnetwork/',
                    attributes:[
                        'occi.network.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.allocation':[default:'dynamic', required:false, mutable:true, type:'enum', render:true]
                    ]
                ],
                'ipnetworkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/networkinterface#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#networkinterface'],
                    title:'IP Network Interface Mixin',
                    location:'/ipnetwork/ipnetworkinterface/',
                    attributes:[
                        'occi.networkinterface.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.allocation':[default:'dynamic', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'metric':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'Metric Mixin',
                    location:'',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'user':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/metric/compute/cpu#',
                    rel:[parent:'http://sla-at-soi.eu/occi/infrastructure#metric', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Metric Mixin',
                    location:'/metric/compute/cpu/user/',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'M', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'occi.compute.cpu', required:false, mutable:true, type:'string', render:true]
                    ]
                ]
            ]
        ]

        config.eu.slasoi.infrastructure.templates = [
            enabled:[
                'ubuntu_9-10':[
                    scheme:'http://sla-at-soi.eu/occi/os_templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 9.10 LTS operating system',
                    location:'/os_tpl/ubuntu_9-10/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ],
                'ubuntu_10-11':[
                    scheme:'http://sla-at-soi.eu/occi/os_templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 10.11 LTS operating system',
                    location:'/os_tpl/ubuntu_10-11/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]
        
        ConfigurationHolder.config = config

        mockLogging ComputeController
        mockLogging InfrastructureTypeFactoryService
        mockLogging InfrastructureInstanceFactoryService
        mockDomain Compute
        mockDomain occi.core.Category
//        mockDomain Task
        mockDomain Kind
        mockDomain AttributeType
        mockDomain Action
        mockDomain occi.core.Mixin
        mockDomain AttributeValue

        cc = new ComputeController()
        cc.infrastructureInstanceFactoryService = new InfrastructureInstanceFactoryService()
        cc.infrastructureInstanceFactoryService.infrastructureTypeFactoryService = new InfrastructureTypeFactoryService()
        cc.infrastructureInstanceFactoryService.infrastructureTypeFactoryService.initDefaults()
    }

    protected void tearDown() {
        super.tearDown()
    }

// ---------- Version ---------

//    void testVersion(){
//        def cc = new ComputeController()
//
//        cc.request.format = 'all'
//        cc.doGet()
//        def version = cc.response.getHeader('Server')
//        assertNotNull version
//        assertEquals 'OCCI/1.1', version
//    }

// ---------- List ---------

    void testComputeEmptyList(){

        assertEquals 0, Compute.count()

        cc.request.format = 'all'
        cc.doGet()

        def comps = cc.response.getHeader('Category')
        assertNull comps
    }

    void testComputeListAsAll(){

        assertEquals(0, Compute.count())
        createCompute()
        createCompute('anotherhost')
        assertEquals 2, Compute.count()

        cc.request.format = 'all'
        cc.request.setContentType('*/*')
        cc.doGet()
        def comps = cc.response.contentAsString
        assertNotNull comps
        assertEquals 'text/plain', cc.response.getContentType()

        def deser = OcciParser.getParser(comps).headers()
        assertNotNull deser
    }

    void testComputeListAsPlain(){

        assertEquals(0, Compute.count())
        createCompute()
        createCompute('anotherhost')
        assertEquals 2, Compute.count()

        cc.request.format = 'text'
        cc.request.setContentType('text/plain')
        cc.doGet()
        def comps = cc.response.contentAsString
        assertNotNull comps
        assertEquals 'text/plain', cc.response.getContentType()

        def deser = OcciParser.getParser(comps).headers()
        assertNotNull deser
    }

    void testComputeListAsOcci() {

        assertEquals(0, Compute.count())
        createCompute()
        createCompute('anotherhost')
        assertEquals 2, Compute.count()

        cc.request.format = 'occi'
        cc.request.setContentType('text/occi')
        cc.doGet()
        def comps = cc.response.getHeader('X-OCCI-Location')
        assertNotNull comps
        assertEquals 'text/occi', cc.response.getContentType()

        def deser = OcciParser.getParser('X-OCCI-Location: '+comps).headers()
        assertNotNull deser
    }

    void testComputeListAsUriList(){
        
        assertEquals(0, Compute.count())
        def c1 = createCompute()
        def c2 = createCompute('anotherhost')
        assertEquals 2, Compute.count()

        cc.request.format = 'uriList'
        cc.request.setContentType('text/uri-list')
        cc.doGet()

        String comps = cc.response.contentAsString
        assertNotNull comps
        assertEquals 'text/uri-list', cc.response.getContentType()

        assertTrue comps.contains(c1)
        assertTrue comps.contains(c2)
    }

// ---------- Details ---------

    void testComputeDetailsAsAll() {

        assertEquals(0, Compute.count())
        def compId = createCompute()
        assertEquals 1, Compute.count()

        cc.params.computeId = compId
        cc.request.format = 'all'
        cc.request.setContentType('*/*')

        cc.doGet()
        assertEquals 'text/plain', cc.response.getContentType()

        def res = cc.response.contentAsString
        assertNotNull res
        def deser = OcciParser.getParser(res).headers()
        assertNotNull deser
    }

    void testComputeDetailsAsPlain(){

        assertEquals(0, Compute.count())
        def compId = createCompute()
        assertEquals 1, Compute.count()

        cc.params.computeId = compId
        cc.request.format = 'text'
        cc.request.setContentType('text/plain')

        cc.doGet()
        assertEquals cc.request.getContentType(), cc.response.getContentType()

        def res = cc.response.contentAsString
        assertNotNull res
        def deser = OcciParser.getParser(res).headers()
        assertNotNull deser
    }

    void testComputeDetailsAsOcci(){

        assertEquals(0, Compute.count())
        def compId = createCompute()
        assertEquals 1, Compute.count()

        cc.params.computeId = compId
        cc.request.format = 'occi'
        cc.request.setContentType('text/occi')

        cc.doGet()

        assertEquals cc.request.getContentType(), cc.response.getContentType()

        def cats = cc.response.getHeader('Category')
        assertNotNull cats
        def deser = OcciParser.getParser('Category: ' + cats).headers()
        assertNotNull deser

        def attrs = cc.response.getHeader('X-OCCI-Attribute')
        assertNotNull attrs
        deser = OcciParser.getParser('X-OCCI-Attribute: ' + attrs).headers()
        assertNotNull deser

        def lnks = cc.response.getHeader('Link')
        assertNotNull lnks
        deser = OcciParser.getParser('Link' + lnks).headers()
        assertNotNull deser

        assertTrue cc.response.contentAsString.contains('ok')
    }

    void testComputeDetailsAsUriList(){

        assertEquals(0, Compute.count())
        def compId = createCompute()
        assertEquals 1, Compute.count()

        cc.params.computeId = compId
        cc.request.format = 'uriList'
        cc.request.setContentType('text/uri-list')
        cc.doGet()
        assertEquals HttpURLConnection.HTTP_NOT_ACCEPTABLE, cc.response.status
    }

// ---------- Create ---------
// NOW TESTED IN INTEGRATION

/*    void testCreateBasicComputeAsPlain() {

        assertEquals(0, Compute.count())

        //most basic compute creation request - 'give me the default'
        String body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind'"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.doPost()

        def res = cc.response.contentAsString
        assertNotNull res
        assertEquals 1, Compute.count()
    }

    void testCreateComputeWithAttrAsPlain() {

        assertEquals 0, Compute.count()

        //most basic compute creation request - 'give me the default'
        String body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind'"
        body += "\nX-OCCI-Attribute: occi.core.summary = 'this is a summary', occi.compute.architecture = 'x86', occi.compute.cores = 2, occi.compute.hostname = 'mycoolhost', occi.compute.speed = 2.9, occi.compute.memory = 2.0"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.doPost()

        def res = cc.response.contentAsString
        assertNotNull res

        assertEquals 1, Compute.count()

        res = Compute.list()[0]
        assertEquals 'this is a summary', res.getAttrValue(Resource.occi_core_summary).getTypedValue()
        assertEquals Compute.occi_compute_arch_X86, res.getAttrValue(Compute.occi_compute_arch).getTypedValue()
        assertEquals 2, res.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 'mycoolhost', res.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
        assertEquals 2.9, res.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 2.0, res.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testCreateComputeWithAttrAndMixinAsPlain() {

        assertEquals 0, Compute.count()

        String body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind', ${SlaSoiComputeMixin.term}; scheme='${SlaSoiComputeMixin.scheme}'; class='mixin'"
        body += "\nX-OCCI-Attribute: ${Resource.occi_core_summary} = 'this is a summary', ${Compute.occi_compute_arch} = 'x86', ${Compute.occi_compute_cores} = 2, ${Compute.occi_compute_hostname} = 'mycoolhost', ${Compute.occi_compute_speed} = 2.9, ${Compute.occi_compute_memory} = 2.0"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.doPost()

        def res = cc.response.contentAsString
        assertNotNull res

        assertEquals 1, Compute.count()

        res = Compute.list()[0]
        assertEquals 1, res.mixins.size()
        
        assertEquals 'this is a summary', res.getAttrValue(Resource.occi_core_summary).getTypedValue()
        assertEquals Compute.occi_compute_arch_X86, res.getAttrValue(Compute.occi_compute_arch).getTypedValue()
        assertEquals 2, res.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 'mycoolhost', res.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
        assertEquals 2.9, res.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 2.0, res.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testCreateComputeAsOcci() {

        assertEquals(0, Compute.count())

        cc.request.addHeader('Category',
                "${Compute.term}; scheme='${Compute.scheme}'; class='kind'")

        cc.request.contentType = 'text/occi'
        cc.request.format = 'occi'

        cc.doPost()

        def res = cc.response.getHeader("Location")
        assertNotNull res
    }

    void testCreateComputeWithAttrAsOcci() {

        assertEquals(0, Compute.count())

        cc.request.addHeader('Category',
                "${Compute.term}; scheme='${Compute.scheme}'; class='kind'")
        cc.request.addHeader('X-OCCI-Attribute', "occi.core.summary = 'this is a summary', occi.compute.architecture = 'x86', occi.compute.cores = 2, occi.compute.hostname = 'mycoolhost', occi.compute.speed = 2.9, occi.compute.memory = 2.0")

        cc.request.contentType = 'text/occi'
        cc.request.format = 'occi'

        cc.doPost()

        assertEquals 1, Compute.count()
        def c = Compute.list()[0]

        def res = cc.response.getHeader("Location")
        assertNotNull res
        res.contains(c.o_id)

        assertEquals 'this is a summary', c.getAttrValue(Resource.occi_core_summary).getTypedValue()
        assertEquals Compute.occi_compute_arch_X86, c.getAttrValue(Compute.occi_compute_arch).getTypedValue()
        assertEquals 2, c.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 'mycoolhost', c.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
        assertEquals 2.9, c.getAttrValue(Compute.occi_compute_speed).getTypedValue()
        assertEquals 2.0, c.getAttrValue(Compute.occi_compute_memory).getTypedValue()
        assertEquals Compute.occi_compute_state_inactive, c.getAttrValue(Compute.occi_compute_state).getTypedValue()
    }

    void testCreateComputeAsUriList() {

        assertEquals(0, Compute.count())

        String body = "http://www.ahost.com/compute/123123123"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/uri-list'
        cc.request.format = 'uriList'

        cc.doPost()
        assertEquals HttpURLConnection.HTTP_NOT_ACCEPTABLE, cc.response.status
    }*/

// ---------- Action ---------
// TESTED IN INTEGRATION
    /*void testActionCompute(){
        assertEquals 0, Compute.count()

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4

        Compute compX = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)
        assertEquals 1, Compute.count()
        assertNotNull compX

        cc.params.computeId = compX.o_id
        cc.request.queryString = 'action=start'
        cc.request.contentType = 'text/plain'
        cc.request.format = 'plain'

        cc.doPost()

    }

    void testActionAllComputes(){
        assertEquals 0, Compute.count()

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute compX = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)
        assertEquals 1, Compute.count()
        assertNotNull compX

        cc.request.queryString = 'action=start'
        cc.request.contentType = 'text/plain'
        cc.request.format = 'plain'

        cc.doPost()

    }*/
// ---------- Update ---------
//TESTED IN INTEGRATION
    /*void testUpdateComputeAttrAsPlain(){

        assertEquals 0, Compute.count()

        String body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind', ${SlaSoiComputeMixin.term}; scheme='${SlaSoiComputeMixin.scheme}'; class='mixin'"
        body += "\nX-OCCI-Attribute: ${Resource.occi_core_summary} = 'this is a summary', ${Compute.occi_compute_arch} = 'x86', ${Compute.occi_compute_cores} = 2, ${Compute.occi_compute_hostname} = 'mycoolhost', ${Compute.occi_compute_speed} = 2.9, ${Compute.occi_compute_memory} = 2.0"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.doPost()

        def res = cc.response.contentAsString
        assertNotNull res

        assertEquals 1, Compute.count()
        def compId = Compute.list()[0].o_id

        def update = "X-OCCI-Attribute: a.ran.dom.attr='12', ${Resource.occi_core_summary} = 'this is an updated summary', ${Compute.occi_compute_cores} = 4, ${Compute.occi_compute_memory} = 3.0, ${Compute.occi_compute_state} = 'active'"
        cc.request.setContent(update.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.params.computeId = compId

        cc.doPut()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status

        def comp = Compute.list()[0]
        assertNotNull comp
        assertEquals 'this is an updated summary', comp.getAttrValue(Resource.occi_core_summary).getTypedValue()
        assertEquals 4, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 3.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }

    void testUpdateComputeAttrAsOcci(){

        assertEquals 0, Compute.count()

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
        //computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active

        Compute compX = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)
        assertEquals 1, Compute.count()
        assertNotNull compX
        
        cc.request.addHeader('X-OCCI-Attribute', "${Resource.occi_core_summary} = 'this is an updated summary', ${Compute.occi_compute_cores} = 4, ${Compute.occi_compute_memory} = 3.0")
        cc.request.contentType = 'text/occi'
        cc.request.format = 'occi'
        cc.params.computeId = compX.o_id

        cc.doPut()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status

        def comp = Compute.list()[0]
        assertNotNull comp
        assertEquals 'this is an updated summary', comp.getAttrValue(Resource.occi_core_summary).getTypedValue()
        assertEquals 4, comp.getAttrValue(Compute.occi_compute_cores).getTypedValue()
        assertEquals 3.0, comp.getAttrValue(Compute.occi_compute_memory).getTypedValue()
    }*/

// ---------- Delete ---------
// TESTED IN INTEGRATION
    /*void testDeleteComputeAsPlain(){

        assertEquals 0, Compute.count()

        String body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind', ${SlaSoiComputeMixin.term}; scheme='${SlaSoiComputeMixin.scheme}'; class='mixin'"
        body += "\nX-OCCI-Attribute: ${Resource.occi_core_summary} = 'this is a summary', ${Compute.occi_compute_arch} = 'x86', ${Compute.occi_compute_cores} = 2, ${Compute.occi_compute_hostname} = 'mycoolhost', ${Compute.occi_compute_speed} = 2.9, ${Compute.occi_compute_memory} = 2.0"
        cc.request.setContent(body.getBytes())
        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.doPost()

        def res = cc.response.contentAsString
        assertNotNull res

        assertEquals 1, Compute.count()
        def compId = Compute.list()[0].o_id

        cc.request.contentType = 'text/plain'
        cc.request.format = 'text'

        cc.params.computeId = compId

        cc.doDelete()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
        assertEquals 0, Compute.count()
    }*/

    private def createCompute(host='') {

        def compKind = Kind.findByTermAndScheme(Compute.term, Compute.scheme)
        assertNotNull compKind

        def computeAttributes = [:]
        computeAttributes[Entity.occi_core_title] = 'Ooo a title - and we\'ve summary!'
        computeAttributes[Resource.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch] = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores] = 2
        if(host.length()>0)
            computeAttributes[Compute.occi_compute_hostname] = host
        else
            computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_active
        def comp = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)
        return comp.o_id
    }
}
