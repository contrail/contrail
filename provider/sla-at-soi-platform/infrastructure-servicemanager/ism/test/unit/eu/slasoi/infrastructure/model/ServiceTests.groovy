/*
SVN FILE: $Id: ServiceTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/eu/slasoi/infrastructure/model/ServiceTests.groovy $
*/

package eu.slasoi.infrastructure.model

import grails.test.GrailsUnitTestCase
import occi.infrastructure.Compute
import occi.infrastructure.actions.compute.RestartComputeAction
import occi.infrastructure.actions.compute.StartComputeAction
import occi.infrastructure.actions.compute.StopComputeAction
import occi.infrastructure.actions.compute.SuspendComputeAction
import occi.lexpar.OcciParser
import occi.core.*

class ServiceTests extends GrailsUnitTestCase {
    
    protected void setUp() {
        
        super.setUp()
        mockDomain Service
        mockDomain Compute
        mockDomain Kind
        mockDomain Action
        mockDomain AttributeType
    }

    protected void tearDown() {
        
        super.tearDown()
    }

    void testConstraints() {

        def svc = new Service()
        
        assertFalse svc.validate()
        assertEquals "nullable", svc.errors["resources"]
        assertEquals "nullable", svc.errors["kind"]
        assertEquals "nullable", svc.errors["o_id"]


        def svcKind = new Kind(term: 'service', scheme: 'http://sla-at-soi.eu/', title: '', location: '/service/')
        svc.kind = svcKind

        assertFalse svc.validate()
        assertEquals "nullable", svc.errors["resources"]
        assertEquals "nullable", svc.errors["o_id"]

        svc.o_id = UUID.randomUUID().toString()
        assertFalse svc.validate()
        assertEquals "nullable", svc.errors["resources"]


        def comp = new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        assertTrue comp.validate()

        svc.addToResources comp
        comp.owningService = svc

        assertTrue svc.validate()

        assertTrue true 

    }

    void testAttributes(){

        //The attributes of monitoring config and service name should be present
        //create a service
        def svc = createService()

        assertNotNull svc.kind.attributes
    }

    void testSerialisation(){

        //Should only test for the rendering of the Service resource and not the children of it
        def svc = createService()


        def ser = svc.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    private Service createService(){
        Service svc = new Service(o_id:UUID.randomUUID().toString(),
                kind: createServiceKind())
        svc.addToResources new Compute(o_id: UUID.randomUUID().toString(), kind: createComputeKind())
        return svc
    }

    private Kind createServiceKind(){

        def svcKind = new Kind(term: 'service', scheme: 'http://sla-at-soi.eu/', title: '', location: '/service/')

        def attr = new AttributeType(name: 'eu.slasoi.service.monitoring.config', type: AttributeType.STRING, defaultValueExpr: "").save()
        svcKind.addToAttributes attr

        attr = new AttributeType(name: 'eu.slasoi.service.name', type: AttributeType.STRING, defaultValueExpr: "").save()
        svcKind.addToAttributes attr

        return svcKind
    }

    
    private Kind createComputeKind() {

        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def resKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '')
        resKind.addToRelated entKind
        assertNotNull resKind.save()

        //Add kind (type info)
        def compKind = new Kind(term: Compute.term, scheme: Compute.scheme, title: 'Compute type', location: '/compute/')
        compKind.addToRelated resKind

        addKindAttributes(compKind)
        addKindActions(compKind)

        assertNotNull compKind.save()

        return compKind
    }

    private def addKindActions(Kind compKind) {

        def startAction = new Action(term: StartComputeAction.term, scheme: StartComputeAction.scheme, title: 'Start Compute Action', location: '').save()
        compKind.addToActions startAction

        def stopAction = new Action(term: StopComputeAction.term, scheme: StopComputeAction.scheme, title: 'Stop Compute Action', location: '').save()
        def attr = new AttributeType(name: StopComputeAction.graceful, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.graceful}")
        stopAction.addToAttributes attr
        attr = new AttributeType(name: StopComputeAction.acpioff, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.acpioff}").save()
        stopAction.addToAttributes attr
        attr = new AttributeType(name: StopComputeAction.poweroff, type: AttributeType.STRING, defaultValueExpr: "${StopComputeAction.poweroff}").save()
        stopAction.addToAttributes attr
        stopAction.save()
        compKind.addToActions stopAction

        def restartAction = new Action(term: RestartComputeAction.term, scheme: RestartComputeAction.scheme, title: 'Restart Compute Action', location: '').save()
        attr = new AttributeType(name: RestartComputeAction.graceful, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.graceful}").save()
        restartAction.addToAttributes attr
        attr = new AttributeType(name: RestartComputeAction.warm, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.warm}").save()
        restartAction.addToAttributes attr
        attr = new AttributeType(name: RestartComputeAction.cold, type: AttributeType.STRING, defaultValueExpr: "${RestartComputeAction.cold}").save()
        restartAction.addToAttributes attr
        restartAction.save()
        compKind.addToActions restartAction

        def suspendAction = new Action(term: SuspendComputeAction.term, scheme: SuspendComputeAction.scheme, title: 'Suspend Compute Action', location: '').save()
        attr = new AttributeType(name: SuspendComputeAction.hibernate, type: AttributeType.STRING, defaultValueExpr: "${SuspendComputeAction.hibernate}").save()
        suspendAction.addToAttributes attr
        attr = new AttributeType(name: SuspendComputeAction.suspend, type: AttributeType.STRING, defaultValueExpr: "${SuspendComputeAction.suspend}").save()
        suspendAction.addToAttributes attr
        suspendAction.save()
        compKind.addToActions suspendAction
    }

    private def addKindAttributes(Kind compKind) {

        def attr = new AttributeType(name: Compute.occi_compute_arch, type: AttributeType.STRING, defaultValueExpr: "${Compute.occi_compute_arch_X86}").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_cores, type: AttributeType.INT, defaultValueExpr: "0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_speed, type: AttributeType.FLOAT, defaultValueExpr: "0.0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_memory, type: AttributeType.FLOAT, defaultValueExpr: "0.0").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_hostname, type: AttributeType.STRING, defaultValueExpr: "localhost").save()
        compKind.addToAttributes attr
        attr = new AttributeType(name: Compute.occi_compute_state, type: AttributeType.STRING, defaultValueExpr: "${Compute.occi_compute_state_inactive}").save()
        compKind.addToAttributes attr
    }
}

/*

void testFind() {

        def testInstances = []
        mockDomain(Category, testInstances)

        //create category
        assertEquals(0, Category.count())
        def cat = new Category(term: "compute",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull cat.save()
        def svcCat = new Category(term: "service",
                scheme: "http://purl.org/occi/kind#",
                title: "a title")
        assertNotNull svcCat.save()
        assertEquals(2, Category.count())


        mockDomain(Compute, testInstances)
        //create compute
        assertEquals(0, Compute.count())
        def expensiveCompute = new Compute(
                hostname: "expensive.com",
                cpu_arch: "x64",
                cpu_cores: 4,
                cpu_speed: 2.4,
                memory_size: 4096,
                state: ISMConstants.INACTIVE
        )
        expensiveCompute.baseCategory = cat
        expensiveCompute.addToCategories(cat)
        assertNotNull expensiveCompute.save()

        def regularCompute = new Compute(
                hostname: "regular.com",
                cpu_arch: "x86",
                cpu_cores: 2,
                cpu_speed: 2.4,
                memory_size: 2048,
                state: ISMConstants.INACTIVE
        )
        regularCompute.baseCategory = cat
        regularCompute.addToCategories(cat)
        assertNotNull regularCompute.save()

        assertEquals(2, Compute.count())

        mockDomain(Service, testInstances)
        assertEquals 0, Service.count()

        def svc = new Service(
                name: "my service",
                state: ISMConstants.INACTIVE
        )
        svc.baseCategory = svcCat
        svc.addToCategories(svcCat)
        svc.addToResources(regularCompute)
        svc.addToResources(expensiveCompute)

        regularCompute.service = svc
        expensiveCompute.service = svc

        assertNotNull svc.save()
        assertEquals 1, Service.count()

        def x = Compute.findByHostname("regular.com")

        assertEquals "my service", x.service.name
    }
    */
