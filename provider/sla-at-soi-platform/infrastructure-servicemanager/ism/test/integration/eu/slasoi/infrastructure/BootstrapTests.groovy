package eu.slasoi.infrastructure

/*
SVN FILE: $Id:$ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author:$
@version        $Rev:$
@lastrevision   $Date:$
@filesource     $URL:$
*/

import grails.test.GrailsUnitTestCase
import occi.core.Kind
import occi.core.Link
import occi.core.Mixin
import occi.core.Resource
import occi.infrastructure.Compute
import occi.infrastructure.Network
import occi.infrastructure.Storage
import occi.infrastructure.mixins.IPNetworkInterfaceMixin
import occi.infrastructure.mixins.IPNetworkMixin
import occi.infrastructure.NetworkLink

class BootstrapTests  extends GrailsUnitTestCase {

    protected void setUp() {

        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    //General sanity tests
    void testKindsAvailable(){

        def kinds = Kind.list()
        assertNotNull kinds
        assertTrue kinds.size() > 0
    }

    void testMixinsAvailable(){
        
        def mixins = Mixin.list()
        assertNotNull mixins
        assertTrue mixins.size() > 0
    }


    /*
        OCCI Core:
        "a client MUST be able to, at a minimum, deduce the following information:
        1. The Entity sub-types available from the service provider,
            including core model extensions. This information is provided
            through the Kind instances of the OCCI implementation.

        2. The attributes defined for each Entity sub-type. The
            identifying Kind instance provide this information.

        3. The invocable operations, i.e. Actions, defined for each
            Entity sub-type. The identifying Kind instance provide
            this information."
     */

    void testOcciKindsAvailable(){

        def resource = Kind.findByTermAndScheme(Resource.term, Resource.scheme)
        assertNotNull resource
        assertEquals Resource.term, resource.term
        assertEquals Resource.scheme, resource.scheme
        assertEquals 0, resource.actions.size()
        assertEquals 1, resource.attributes.size()

        def link = Kind.findByTermAndScheme(Link.term, Link.scheme)
        assertNotNull link
        assertEquals Link.term, link.term
        assertEquals Link.scheme, link.scheme
        assertEquals 0, link.actions.size()
        assertEquals 2, link.attributes.size()

        def comp = Kind.findByTermAndScheme(Compute.term, Compute.scheme)
        assertNotNull comp
        assertEquals Compute.term, comp.term
        assertEquals Compute.scheme, comp.scheme
        assertEquals 4, comp.actions.size()
        assertEquals 6, comp.attributes.size()

        def net = Kind.findByTermAndScheme(Network.term, Network.scheme)
        assertNotNull net
        assertEquals Network.term, net.term
        assertEquals Network.scheme, net.scheme
        assertEquals 2, net.actions.size()
        assertEquals 3, net.attributes.size()

        def stor = Kind.findByTermAndScheme(Storage.term, Storage.scheme)
        assertNotNull stor
        assertEquals Storage.term, stor.term
        assertEquals Storage.scheme, stor.scheme
        assertEquals 6, stor.actions.size()
        assertEquals 2, stor.attributes.size()
    }

    /*
    OCCI Core:
        "a client MUST be able to, at a minimum, deduce the following information:
        1. Any Mixin instances that can be associated to resource
            instances.

        2. Additional capabilities defined by a particular Mixin
            instance, i.e. attributes and Actions."
     */
    
    void testOcciMixinsAvailable(){
        
        def networkMixin = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull networkMixin
        assertEquals IPNetworkMixin.term, networkMixin.term
        assertEquals IPNetworkMixin.scheme, networkMixin.scheme
        assertEquals 0, networkMixin.actions.size()
        assertEquals 3, networkMixin.attributes.size()
        assertEquals Network.term, networkMixin.applicableKind.term
        assertEquals Network.scheme, networkMixin.applicableKind.scheme

        def networkIfaceMixin = Mixin.findByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)
        assertNotNull networkIfaceMixin
        assertEquals IPNetworkInterfaceMixin.term, networkIfaceMixin.term
        assertEquals IPNetworkInterfaceMixin.scheme, networkIfaceMixin.scheme
        assertEquals 0, networkIfaceMixin.actions.size()
        assertEquals 3, networkIfaceMixin.attributes.size()
        assertEquals NetworkLink.term, networkIfaceMixin.applicableKind.term
        assertEquals NetworkLink.scheme, networkIfaceMixin.applicableKind.scheme
    }
}
