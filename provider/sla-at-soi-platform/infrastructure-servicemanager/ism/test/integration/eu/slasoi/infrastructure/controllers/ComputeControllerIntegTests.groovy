package eu.slasoi.infrastructure.controllers

/*
SVN FILE: $Id:$ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author:$
@version        $Rev:$
@lastrevision   $Date:$
@filesource     $URL:$
*/

import grails.test.ControllerUnitTestCase
import occi.core.Mixin
import occi.factory.InfrastructureInstanceFactoryService
import occi.factory.InfrastructureTypeFactoryService
import occi.infrastructure.Compute
import occi.infrastructure.Network
import org.springframework.context.ApplicationListener

//These tests use text/plain as the format
class ComputeControllerIntegTests extends ControllerUnitTestCase implements ApplicationListener<ComputeCreateEvent>{

    ComputeController cc
    Compute comp
    Mixin mxn

    void onApplicationEvent(ComputeCreateEvent e) {
        
        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
        assertEquals 2, Compute.count()
        assertTrue cc.response.contentAsString.contains('Location: http://')
    }


    protected void setUp(){

        cc = new ComputeController()
        cc.request.contentType = 'text/plain'

        cc.infrastructureInstanceFactoryService = new InfrastructureInstanceFactoryService()
        cc.infrastructureInstanceFactoryService.infrastructureTypeFactoryService = new InfrastructureTypeFactoryService()
        cc.infrastructureInstanceFactoryService.infrastructureTypeFactoryService.initDefaults()

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4

        comp = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)
    }
    
    protected void tearDown(){

    }

    void testGETComputeInstance(){

        cc.params.computeId = comp.o_id
        cc.doGet()
        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
    }

    void testGETNonExistingComputeInstance(){
        cc.params.computeId = 'huh-say-what'
        cc.doGet()

        assertEquals HttpURLConnection.HTTP_NOT_FOUND, cc.response.status
    }

    void testGETComputeInstanceList(){
        cc.request.setRequestURI("/compute/")
        cc.doGet()
        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
    }

    void testGETComputeInstanceEmptyList(){

        comp.delete()
        assertEquals 0, Compute.list().size()

        cc.request.setRequestURI("/compute/")
        cc.doGet()
        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
    }

    //Execute action upon resource
    //POST /entity_id?action=xxx
    void testPOSTComputeAction(){

        cc.params.computeId = comp.o_id
        cc.request.queryString = 'action=start'

        cc.doPost()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
    }

    //TODO action on a group of computes
    void testPOSTComputeUnknownAction(){

        cc.params.computeId = comp.o_id
        cc.request.queryString = 'action=uncle_bob'

        cc.doPost()

        assertEquals HttpURLConnection.HTTP_NOT_FOUND, cc.response.status
    }

    void testPOSTComputeCreateErrorNoBody(){

        cc.doPost()
        assertEquals HttpURLConnection.HTTP_BAD_REQUEST, cc.response.status
        assertEquals 1, Compute.count()
    }

    //create by only specifying the type but with a full rendering
    void testPOSTComputeCreateWithFullCat(){

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#';class='kind'; title='Compute Resource';rel='http://schemas.ogf.org/occi/core#compute';location=/storage/;attributes='occi.compute.size occi.compute.state';actions='http://schemas.ogf.org/occi/infrastructure/compute/action#start'"
        cc.request.content = content.getBytes()

        cc.doPost()
//        assertEquals 2, Compute.count()
//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertTrue cc.response.contentAsString.contains('Location: http://')
    }

    void testPOSTComputeCreateBasicAllDefaults(){
        String content = "Category: ${Compute.term}; scheme ='${Compute.scheme}'; class='kind'"
        cc.request.content = content.getBytes()

        cc.doPost()
//        assertEquals 2, Compute.count()
//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertTrue cc.response.contentAsString.contains('Location: http://')
    }

    //create by only specifying the type but in correctly formatted
    void testPOSTComputeCreateBasicAllDefaultsWithError(){

        String content = "Category: ${Compute.term}; scheme ='${Compute.scheme}'; claasdasdadasdasdasdss='kind'"
        cc.request.content = content.getBytes()

        cc.doPost()
        assertEquals HttpURLConnection.HTTP_BAD_REQUEST, cc.response.status
        assertEquals 1, Compute.count()
    }

    //create by specifying type & particular X-OCCI-Attributes
    void testPOSTComputeCreateWithAttrs(){

        def content = "Category: ${Compute.term}; scheme ='${Compute.scheme}'; class='kind'\nX-OCCI-Attribute: occi.compute.cores=4"
        cc.request.content = content.getBytes()
        
        cc.doPost()
//        assertEquals 2, Compute.count()
//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertTrue cc.response.contentAsString.contains('Location: http://')
    }

    //create by specifying type & mixin
    void testPOSTComputeCreateWithMixin(){

        def content = "Category: ${Compute.term}; scheme ='${Compute.scheme}'; class='kind', ubuntu_10-11; scheme='http://sla-at-soi.eu/templates/#'; class='mixin'"
        cc.request.content = content.getBytes()

        cc.doPost()
//        assertEquals 2, Compute.count()
//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertTrue cc.response.contentAsString.contains('Location: http://')
    }

    //create by specifying type & mixin & attribute
    void testPOSTComputeCreateWithMixinAndAttr(){

        def content = "Category: ${Compute.term}; scheme ='${Compute.scheme}'; class='kind', ubuntu_10-11; scheme='http://sla-at-soi.eu/templates/#'; class='mixin'\nX-OCCI-Attribute: occi.compute.cores=4"
        cc.request.content = content.getBytes()

        cc.doPost()
//        assertEquals 2, Compute.count()
//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertTrue cc.response.contentAsString.contains('Location: http://')
    }

    //Partial update of resource
    //POST /entity_id
    void testPOSTComputePartialUpdateInstanceWithAttr(){

        cc.params.computeId = comp.o_id
        def body = "X-OCCI-Attribute: ${Compute.occi_compute_cores} = 4"
        cc.request.setContent(body.getBytes())
        cc.doPost()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status

        Compute c = Compute.findByO_id(comp.o_id)

        assertEquals '4', c.getAttrValue(Compute.occi_compute_cores).value
    }

    void testPOSTComputePartialUpdateInstanceNewMixin(){

        cc.params.computeId = comp.o_id
        def body = "Category: ubuntu_9-10; scheme='http://sla-at-soi.eu/occi/os_templates#'; class='mixin'"
        cc.request.setContent(body.getBytes())
        cc.doPost()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
        
        Compute c = Compute.findByO_id(comp.o_id)
        assertEquals 2, c.mixins.size()
    }

    void testPOSTComputePartialUpdateInstanceWithMixinAndOrigType(){

        cc.params.computeId = comp.o_id
        def body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind', ubuntu_9-10; scheme='http://sla-at-soi.eu/occi/os_templates#'; class='mixin'"
        cc.request.setContent(body.getBytes())
        cc.doPost()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status

        Compute c = Compute.findByO_id(comp.o_id)
        assertEquals 2, c.mixins.size()
    }

    void testPOSTComputePartialUpdateInstanceWithCatChange(){

        cc.params.computeId = comp.o_id
        def body = "Category: ${Network.term}; scheme='${Network.scheme}'; class='kind'"
        cc.request.setContent(body.getBytes())
        cc.doPost()

        assertEquals HttpURLConnection.HTTP_BAD_REQUEST, cc.response.status

        Compute c = Compute.findByO_id(comp.o_id)
        assertEquals 1, c.mixins.size()
    }

    //Creation of resource via PUT
    void testPUTComputeCreateInstance(){

        def c_id = UUID.randomUUID().toString()
        cc.params.computeId = c_id
        def body = "Category: ${Compute.term}; scheme='${Compute.scheme}'; class='kind'\nX-OCCI-Attribute: occi.compute.architecture='x64', occi.compute.speed=2.4, occi.compute.memory=2.0, occi.core.summary='This a brilliant summary of things!', occi.compute.hostname='myhost2', occi.compute.cores=4, occi.core.title='Title'"
        cc.request.setContent(body.getBytes())
        cc.doPut()

//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertEquals 2, Compute.list().size()
//        def result = cc.response.contentAsString
//        assertEquals "X-OCCI-Location: http://localhost:8080/compute/${c_id}", result
    }

    //TODO Complete update of resource via PUT
    void testPUTComputeUpdateInstance(){

//        //get the existing resource
//        Compute compute = Compute.list()[0]
//
//        //modify the resource locally
//        def attrToUpdate = compute.getAttrValue(Compute.occi_compute_cores)
//        attrToUpdate.value = 6
//        attrToUpdate.save()
//        attrToUpdate = compute.getAttrValue(Compute.occi_compute_memory)
//        attrToUpdate.value = 16.0
//        attrToUpdate.save()

        def body = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', ubuntu_10-11; scheme='http://sla-at-soi.eu/occi/os_templates#'; class='mixin'\n"
        body += "X-OCCI-Attribute: occi.compute.cores=6, occi.core.title='Title', occi.compute.architecture='x64', occi.compute.hostname='myhost', occi.core.summary='This a brilliant summary of things!', occi.compute.speed=2.4, occi.compute.memory=16.0\n"
        body += "Link: </compute/${comp.o_id}?action=suspend>; rel='http://schemas.ogf.org/occi/infrastructure/compute/action#suspend', </compute/${comp.o_id}?action=restart>; rel='http://schemas.ogf.org/occi/infrastructure/compute/action#restart', </compute/${comp.o_id}?action=start>; rel='http://schemas.ogf.org/occi/infrastructure/compute/action#start', </compute/${comp.o_id}?action=stop>; rel='http://schemas.ogf.org/occi/infrastructure/compute/action#stop'\n"

        //submit for modification
        //def body = compute.toString()
        cc.request.setContent(body.getBytes())
        cc.params.computeId = comp.o_id
        cc.doPut()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
        assertEquals 1, Compute.list().size()

        def compute = Compute.list()[0]
        assertEquals '16.0', compute.getAttrValue(Compute.occi_compute_memory).value
        assertEquals '6', compute.getAttrValue(Compute.occi_compute_cores).value
        //assertEquals "Location: http://localhost:8080/compute/${c_id}", cc.response.contentAsString
    }

    //Execute acton against a collection of resources
    void testPOSTComputeLocationAction(){

        cc.request.queryString = 'action=start'

        cc.doPost()

        assertEquals HttpURLConnection.HTTP_OK, cc.response.status
    }

    //Creation of resource via POST
    void testPOSTSingleComputeLocation(){

        def body = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind'\nX-OCCI-Attribute: occi.compute.architecture='x64', occi.compute.speed=2.4, occi.compute.memory=2.0, occi.core.summary='This a brilliant summary of things!', occi.compute.hostname='myhost2', occi.compute.cores=4, occi.core.title='Title'"
        cc.request.setContent(body.getBytes())
        cc.doPost()

//        assertEquals HttpURLConnection.HTTP_CREATED, cc.response.status
//        assertEquals 2, Compute.list().size()
//
//        String content = cc.response.contentAsString
//        assertTrue content.contains("Location") 
    }

    void testPOSTMultiComputeLocation(){
        //pass
    }

    void testPUTMultiComputeLocation(){
        //pass
    }

    void testDELETEComputeSingle(){

        cc.params.computeId = comp.o_id
        cc.doDelete()

        def c = Compute.findByO_id(comp.o_id)
        assertNull c
    }

    void testDELETEComputeAndLinkage(){

        //assertEquals 1, Compute.count() TODO reenable

        //create another compute

        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4
//        computeAttributes[Compute.occi_compute_state] = Compute.occi_compute_state_inactive

        Compute comp2 = cc.infrastructureInstanceFactoryService.createComputeI(computeAttributes)

//        assertEquals 2, Compute.count() TODO reenable

        cc.infrastructureInstanceFactoryService.link(comp, comp2)

        assertEquals 1, comp.links.size()
        assertNull comp2.links


        if(comp.links.size() > 0){
            //find the link associated with the 2 resources
            cc.infrastructureInstanceFactoryService.unlink(comp, comp2)
        }
       
        cc.params.computeId = comp2.o_id
        cc.doDelete()

        assertEquals 1, Compute.count()

        cc.params.computeId = comp.o_id
        cc.doDelete()

        assertEquals 0, Compute.count()
    }
}
