/*
SVN FILE: $Id: ServiceControllerTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/integration/eu/slasoi/infrastructure/controllers/ServiceControllerTests.groovy $
*/

package eu.slasoi.infrastructure.controllers

import eu.slasoi.infrastructure.model.Service
import grails.test.ControllerUnitTestCase
import occi.infrastructure.Compute
import org.springframework.context.ApplicationListener

class ServiceControllerTests extends ControllerUnitTestCase  implements ApplicationListener<ComputeCreateEvent>{

    ServiceController sc

    protected void setUp() {

        super.setUp()
        sc = new ServiceController()
        sc.request.contentType = 'text/plain'
    }

    protected void tearDown() {
        super.tearDown()
    }

    void onApplicationEvent(ComputeCreateEvent e) {

        assertEquals 1, Service.count()
        assertEquals '666-333-222', Service.list()[0].o_id
        assertEquals 2, Compute.count()
    }

    void testCreateServicePost(){

        def content ='--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="1"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: eu.slasoi.infrastructure.service.name='myService', eu.slasoi.infrastructure.service.monitoring.config='SOME_SERIALISED_STRING', occi.core.id='666-333-222'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="2"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind'\r\n" +
                     "X-OCCI-Attribute: occi.compute.hostname='multi2', occi.compute.cores=1, occi.compute.memory=256, occi.core.id='123-123-123'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="3"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind'\r\n" +
                     "X-OCCI-Attribute: occi.compute.hostname='multi2', occi.compute.cores=1, occi.compute.memory=256, occi.core.id='234-234-234'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn--\r\n'

        sc.request.content = content.getBytes()
        sc.request.contentType = 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn'
        sc.request.addHeader('content-type', 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn')

        sc.doPost()
    }

    void testCreateServicePut(){

        def content ='--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="1"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: eu.slasoi.infrastructure.service.name='myService', eu.slasoi.infrastructure.service.monitoring.config='SOME_SERIALISED_STRING', occi.core.id='666-333-222'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="2"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind'\r\n" +
                     "X-OCCI-Attribute: occi.compute.hostname='multi2', occi.compute.cores=1, occi.compute.memory=256, occi.core.id='123-123-123'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="3"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind'\r\n" +
                     "X-OCCI-Attribute: occi.compute.hostname='multi2', occi.compute.cores=1, occi.compute.memory=256, occi.core.id='234-234-234'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn--\r\n'

        sc.request.content = content.getBytes()
        sc.request.contentType = 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn'
        sc.request.addHeader('content-type', 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn')

        sc.params.instanceId = '666-333-222'
        sc.doPut()
    }

}
//    void testDetailsService() {
//    }
//
//    void testListService() {
//    }
//
//    void testCreateService() {
//    }
//
//    void testUpdateService() {
//    }
//
//    void testDeleteService() {
//    }

//    void testCreateServicePostJclouds(){
//        def content = """----JCLOUDS--
//Content-Disposition: form-data; name="container2"
//Content-Type: application/unknown
//
//Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'
//X-OCCI-Attribute: occi.core.id='acf50aa4-37c3-42af-bccf-ff198b3b7e43', eu.slasoi.infrastructure.service.name='myService'
//----JCLOUDS--
//Content-Disposition: form-data; name="container0"
//Content-Type: application/unknown
//
//Category: ubuntu_9-10; scheme ='http://sla-at-soi.eu/occi/templates#'; class ='mixin', compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class = 'kind'
//X-OCCI-Attribute: occi.core.id='2892db9e-879e-42a1-b729-c12a6ff69266', occi.core.title='Title'
//----JCLOUDS--
//Content-Disposition: form-data; name="container1"
//Content-Type: application/unknown
//
//Category: ubuntu_9-10; scheme='http://sla-at-soi.eu/occi/templates#'; class ='mixin', compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class = 'kind'
//X-OCCI-Attribute: occi.core.id='b2881f09-e518-4dbd-bd41-1bc8e621ef45', occi.core.title='Title'
//----JCLOUDS----
//"""
//        sc.request.content = content.getBytes()
//        sc.request.contentType = 'multipart/form-data; boundary=--JCLOUDS--'
//        sc.request.addHeader('content-type', 'multipart/form-data; boundary=--JCLOUDS--')
//
//        sc.doPost()
//
//        assertEquals 1, Service.count()
//        assertEquals 'acf50aa4-37c3-42af-bccf-ff198b3b7e43', Service.list()[0].o_id
//        assertEquals 2, Compute.count()
//
//    }
