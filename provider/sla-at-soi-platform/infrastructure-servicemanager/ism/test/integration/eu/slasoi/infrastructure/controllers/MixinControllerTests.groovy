package eu.slasoi.infrastructure.controllers

import grails.test.ControllerUnitTestCase
import occi.factory.InfrastructureInstanceFactoryService
import occi.factory.InfrastructureTypeFactoryService
import occi.infrastructure.Compute
import occi.lexpar.OcciParser

class MixinControllerTests extends ControllerUnitTestCase {

    MixinController mc
    Compute comp
    def infrastructureInstanceFactoryService

    protected void setUp() {
        super.setUp()

        infrastructureInstanceFactoryService = new InfrastructureInstanceFactoryService()
        infrastructureInstanceFactoryService.infrastructureTypeFactoryService = new InfrastructureTypeFactoryService()
        infrastructureInstanceFactoryService.infrastructureTypeFactoryService.initDefaults()

        mc = new MixinController()
        mc.request.contentType = 'text/plain'
        
        def computeAttributes = [:]
        computeAttributes[Compute.occi_core_summary] = 'This a brilliant summary of things!'
        computeAttributes[Compute.occi_compute_arch]   = Compute.occi_compute_arch_X64
        computeAttributes[Compute.occi_compute_cores]  = 2
        computeAttributes[Compute.occi_compute_hostname] = 'myhost'
        computeAttributes[Compute.occi_compute_memory] = 2.0
        computeAttributes[Compute.occi_compute_speed] = 2.4

        comp = infrastructureInstanceFactoryService.createComputeI(computeAttributes)

    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetParentPath() {

        mc.request.setRequestURI('/os_tpl/')
        mc.doGet()

        assertEquals HttpURLConnection.HTTP_OK, mc.response.status
        def content = mc.response.contentAsString
        assertTrue content.length() > 0

        content = OcciParser.getParser(content).headers()

        assertEquals 2, content[OcciParser.occi_locations].size()
    }


    void testGet() {

        mc.request.setRequestURI('/os_tpl/ubuntu_10-11/')
        mc.doGet()

        assertEquals HttpURLConnection.HTTP_OK, mc.response.status
        assertTrue mc.response.contentAsString.contains(comp.o_id)
        assertEquals "X-OCCI-Location: http://localhost:8080${comp.kind.location}/${comp.o_id}\n", mc.response.contentAsString
    }

    void testGetNoMixin() {

        mc.request.setRequestURI('/ipnetwork/')
        mc.doGet()

        assertEquals HttpURLConnection.HTTP_OK, mc.response.status
        assertTrue mc.response.contentAsString.length() == 0
    }

    void testPOST(){

        mc.request.setRequestURI('/os_tpl/ubuntu_9-10/')
        String content = "X-OCCI-Location: http://localhost:8080/compute/${comp.o_id}"
        mc.request.content = content.getBytes()

        mc.doPost()
        
        assertEquals HttpURLConnection.HTTP_OK, mc.response.status

        Compute c = Compute.list()[0]
//        assertEquals 2, c.mixins.size() TODO reenable
    }

    void testDELETE(){
        
        mc.request.setRequestURI('/os_tpl/ubuntu_10-11/')
        String content = "X-OCCI-Location: http://localhost:8080/compute/${comp.o_id}"
        mc.request.content = content.getBytes()

        mc.doDelete()

        assertEquals HttpURLConnection.HTTP_OK, mc.response.status

        Compute c = Compute.list()[0]
        assertEquals 0, c.mixins.size()
    }

    void testPUT(){
        mc.request.setRequestURI('/os_tpl/')
        mc.doPut()
        assertEquals HttpURLConnection.HTTP_NOT_IMPLEMENTED, mc.response.status
    }
}
