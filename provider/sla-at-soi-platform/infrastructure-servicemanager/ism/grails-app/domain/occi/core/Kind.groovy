/* 
SVN FILE: $Id: Kind.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/core/Kind.groovy $

*/

package occi.core

class Kind extends Category {

    static hasMany = [related: Kind, actions: Action, entities: Entity]

    static constraints = {
        entities nullable: true
        related nullable: true
        actions nullable: true
    }

    /*
    Rendered from category:
    Category:
        storage;
        scheme="http://schemas.ogf.org/occi/infrastructure#";
        title="Storage Resource";
        location=/storage/;
        attributes="occi.storage.size occi.storage.state";
    Rendered from this:
        class="kind";
        rel="http://schemas.ogf.org/occi/core#resource";
        actions="http://schemas.ogf.org/occi/infrastructure/storage/action#resize
     */
    String toTypeString(){

        def relVal = ''
        if(related?.size() > 0){

            related.each {relVal += it.scheme + it.term + ' '}
            relVal = "; rel='${relVal.trim()}'"
        }

        def base = super.toTypeString('kind', relVal)

        if(actions?.size() > 0){
            def actVals = ''
            actions?.each {actVals += it.scheme + it.term + ' '}
            base += "; actions='${actVals.trim()}'"
        }

        return  base
    }

    String toInstanceString(){
       return super.toInstanceString() + "; class='kind'"
    }
    
    String toString() {
        return 'Category: ' + toTypeString()
    }
}
