/* 
SVN FILE: $Id: Resource.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/core/Resource.groovy $

*/

package occi.core

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.model.Service

class Resource extends Entity {

    static String term = 'resource'
    static String scheme = 'http://schemas.ogf.org/occi/core#'
    static String occi_core_summary = 'occi.core.summary'
    static transients = ['term', 'scheme', 'occi_core_summary']

    String physicalHostFqdn = ''
    static hasMany = [links: Link]
    static mappedBy = [links:'sou_rce']

    static belongsTo = [owningService: Service]

    //define validation
    static constraints = {
        owningService       nullable:true
        physicalHostFqdn    nullable: false
    }

    /*
    </compute/123?action=start>;
    rel="http://schemas.ogf.org/occi/infrastructure/compute/action#start"
     */
    String toActionsString(){

        def acts = ''
        
        kind.actions.each {
            acts += ", <${kind.location}${o_id}?action=${it.term}>; rel='${it.scheme}${it.term}'"
        }

        mixins.each {
            it.actions.each{
                acts += ", <${kind.location}${o_id}?action=${it.category.term}>; rel='${it.category.scheme}${it.category.term}'"
            }
        }

        return acts
    }
    
    /*
    Link: </storage/disk03>;
        rel="http://example.com/occi/resource#storage";
        self="/link/456-456-456";
        title="Storage Disk 03";
        category="http://example.com/occi/link#disk_drive";
        com.example.drive.interface="ide"
    */
    def toLinkString(){

        def lnks = ''
        if(links?.size() > 0){
            links.each {
                def lnk = "<${it?.tar_get?.kind?.location}${it?.tar_get?.o_id}>; rel='${it.tar_get?.kind?.scheme}${it.tar_get?.kind?.term}'; self='${it.kind?.location}${it.o_id}'; category='${it.kind?.scheme}${it.kind?.term}'"

                if(CH.config.eu.slasoi.infrastructure.render.fatlinks ?: false){
                    def attrs = it.toAttributesString()
                    if(attrs.length() > 0)
                        lnk +='; ' + attrs
                }

                lnks += lnk + ', '
            }
            lnks = lnks.trim()[0..-2]
        }

        def act = toActionsString()

        if(act.length() > 0){
            if(lnks.length() > 0)
                lnks += act
            else
                lnks += act[1..-1].trim() //remove preceeding comma and space
        }
        
        return lnks
    }

    def toHeaderArray(){
        
        def headers = [:]

        headers['links'] = toLinkString()
        headers['categories'] = toTypeString()
        headers['attributes'] = toAttributesString()
        return headers
    }

    def String toString() {
        
        def ser = super.toString()

        def lnks = toLinkString()
        if(lnks.length() > 0)
            ser += '\n' + 'Link: ' + lnks

        return ser
    }
}

//summary                 nullable: false, blank: true
//provisioningSystemId    nullable: true, blank: true, unique: true
//String provisioningSystemId
//String summary = ''
//links nullable: true
//service nullable: true
//task nullable: true
//baseCategory nullable: false
//state nullable: false, blank: false
//static belongsTo = [service: Service]
//Category baseCategory
//Task task
//String state //validation rules for this are in the subclasses
//not part of the OCCI model - used to link the IAM to the PS resource
