/*
SVN FILE: $Id: Entity.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/core/Entity.groovy $

*/

package occi.core

class Entity {

    static String term = 'entity'
    static String scheme = 'http://schemas.ogf.org/occi/core#'
    static String occi_core_id = 'occi.core.id'
    static String occi_core_title = 'occi.core.title'
    static transients = ['term', 'scheme', 'occi_core_id', 'occi_core_title']

    String o_id //this is id in the occi model
    String title = ''
    Date dateCreated
    Date lastUpdated
    Kind kind
    static hasMany = [mixins: Mixin, attrvals: AttributeValue]

//    static mapping = {
//        tablePerHierarchy false
//    }
    
    static constraints = {
        o_id nullable: false, blank: false, unique: true
        title nullable: false, blank: true
        kind nullable: false
        mixins nullable: true
        attrvals nullable: true

        id column:'o_id'
    }

    def getAttrValue(String attrName){

        return attrvals.find {
            it.attributeInfo.name == attrName
        }
    }

    def hasAttr(String name){

        def attr = kindAttr(kind, name)
        if(!attr){
            mixins.each {
                attr = mixinAttr(it, name)
            }
        }
        return attr
    }

    protected def kindAttr(Kind kind, String name){
        
        if(!kind)
            return null

        def attr = kind.attributes.find{
            it.name == name
        }

        if (!attr){
            kind.related.each {
                attr = kindAttr(it, name)
            }
        }

        if(attr)
            return attr
        else
            return null
    }

    protected def mixinAttr(Mixin mixin, String name){

        if(!mixin)
            return null

        def attr = mixin.attributes.find{
            it.name == name
        }

        if (!attr){
            mixin.related.each {
                attr = mixinAttr(it, name)
            }
        }

        if(attr)
            return attr
        else
            return null
    }

    String toTypeString(){

        def typeStr = kind?.toInstanceString() ?: ''

        if(mixins?.size() > 0){
            def mxns = ''
            mixins.each{mxns += ", ${it.toInstanceString()}"}
            typeStr += mxns.trim()
        }

        return typeStr
    }

    String toAttributesString(exclusionFilter=null){

        def attrs = ''
        if(attrvals?.size() > 0){
            attrvals.each {
                if(it.attributeInfo.renderme)
                    attrs += it.toString() + ', '
            }
            attrs = attrs.trim()[0..-2]
        }
        return attrs
    }
    
    String toString(){

        def ret = 'Category: ' + toTypeString()

        def attrs = toAttributesString()
        if(attrs.length() > 0)
            ret += '\nX-OCCI-Attribute: ' + attrs

        return ret
    }
}
