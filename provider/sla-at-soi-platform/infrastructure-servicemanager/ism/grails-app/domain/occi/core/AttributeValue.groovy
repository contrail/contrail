package occi.core

class AttributeValue {

    static belongsTo = [Entity]
    
    String value
    Entity entity
    AttributeType attributeInfo

    Date dateCreated
    Date lastUpdated

    static constraints = {
        entity  nullable: false
        attributeInfo  nullable: false
    }

    def getTypedValue(){

        def retVal

        switch (attributeInfo.type) {
            case AttributeType.STRING:
                retVal = value
                break
            case AttributeType.ENUM:
                retVal = value
                break
            case AttributeType.INT:
                retVal = Integer.parseInt(value)
                break
            case AttributeType.FLOAT:
                retVal = new BigDecimal(value)
                break
            case AttributeType.BOOLEAN:
                retVal = new Boolean(value)
                break
        }
        return retVal
    }

    String toAttributeString(){

        def str

        if((attributeInfo.type == AttributeType.STRING) 
						|| (attributeInfo.type == AttributeType.ENUM)
						|| (attributeInfo.type == AttributeType.BOOLEAN))
            	str = attributeInfo.name + "='" + value + "'"
        else
            str = attributeInfo.name + '=' + value

        return str
    }

    String toString(){
        return toAttributeString()
    }
}

/*value   nullable: false, validator: {val, obj ->
    //ensure that the attribute value type matches that of the OCCI declared type
    if (
        ((obj.attributeInfo.type == AttributeType.STRING || obj.attributeInfo.type == AttributeType.ENUM) && val instanceof String) ||
        (obj.attributeInfo.type == AttributeType.INT && val instanceof Integer) ||
        (obj.attributeInfo.type == AttributeType.FLOAT && (val instanceof Float || val instanceof BigDecimal))
        ){
            return true
        }
    else{
            return false
    }
}*/
