/* 
SVN FILE: $Id: Action.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/core/Action.groovy $

*/

package occi.core

import occi.infrastructure.actions.compute.RestartComputeAction
import occi.infrastructure.actions.compute.StopComputeAction
import occi.infrastructure.actions.compute.SuspendComputeAction

//This inherits from Category as there were issues when only holding a reference to Category.
//It was seen that when a Action was associated with a Kind the component category would change
//from the action category to the kind category.
class Action extends Category{

    /*
    Category:
        stop;
        scheme="http://schemas.ogf.org/occi/infrastructure/action#";
        title="Stop Action";
        location=/actions;
        attributes="method";
        class="action";
     */

    String toTypeString(){

        def str = ""
        str = "${term}; scheme='${scheme}'; class='action'; title='${title}'"

//        if(location?.length() > 0){
//            str += "; location=${location}"
//        }

        if(attributes?.size() > 0){
            def attrs = ''
            attributes?.each {
                //TODO this is a hack
                if(it.toString().equalsIgnoreCase(RestartComputeAction.method) ||
                    it.toString().equalsIgnoreCase(StopComputeAction.method) ||
                    it.toString().equalsIgnoreCase(SuspendComputeAction.method))
                    attrs += 'method' + " "
                else
                    attrs += it.toString() + " "
            }
            str += "; attributes='${attrs.trim()}'"
        }

        return str
    }

    String toString(){
        return 'Category: ' + toTypeString()
    }
}
