/*
SVN FILE: $Id: AttributeType.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/core/AttributeType.groovy $

*/

package occi.core

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

class AttributeType {

    static String STRING = "string"
    static String FLOAT = "float"
    static String INT = "int"
    static String BOOLEAN = "boolean"
    static String ENUM = "enum"
    static transients = ['STRING', 'FLOAT', 'INT', 'BOOLEAN', 'ENUM']


    Boolean mutable
    Boolean required

    Boolean renderme = true
    
    String name;
    String type; 
    String defaultValueExpr; 
    Category category //TODO decide if this should be used
    Date dateCreated
    Date lastUpdated
    
    static belongsTo = Category
    static hasMany = [attrVals:AttributeValue, categories:Category]

    static constraints = {
        // TODO would be good to write a custom validator that checked for attributes in the form of xx.yy.zz
        // TODO reenable the regex check
        name                nullable: false//, unique: true//, matches: "([a-z0-9.-_]+)"
        type                nullable: false, inList: [AttributeType.STRING, AttributeType.FLOAT, AttributeType.INT, AttributeType.BOOLEAN, AttributeType.ENUM]
        defaultValueExpr    nullable: false
        attrVals            nullable: true
        category            nullable: true
        mutable             nullable: true
        required            nullable: true
        renderme            nullable: true
    }

    def getDefaultValue(){
        return defaultValueExpr
    }

    String toString() {

        def retVal = ''
        if(!mutable)
            retVal += 'immutable '

        if(required)
            retVal += 'required '

        if(CH.config.eu.slasoi.infrastructure.renderFullTypes){
            retVal += type + ' '
        }

        if(retVal.length() > 0)
            return name + '{' + retVal.trim() + '}' + '=' + defaultValue
        else
            return name
    }
}

/*def val = defaultValueExpr

if(val){
    GroovyShell shell = new GroovyShell();
    val = shell.evaluate(val);

}
return val*/

/*, validator: {strValue, obj ->
    //Evaluate the default value statement
    GroovyShell shell = new GroovyShell();
    Object val = shell.evaluate(strValue);
    //Ensure that the default value matches its declared type
    return ((obj.type == AttributeType.STRING || obj.type == AttributeType.ENUM) && val instanceof String) ||
        (obj.type == AttributeType.INT && val instanceof Integer) ||
        (obj.type == AttributeType.FLOAT && (val instanceof Float || val instanceof BigDecimal))
 }*/
