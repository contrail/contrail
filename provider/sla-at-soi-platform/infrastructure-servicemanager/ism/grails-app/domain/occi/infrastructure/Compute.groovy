/* 
SVN FILE: $Id: Compute.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/domain/occi/infrastructure/Compute.groovy $

*/

package occi.infrastructure

import occi.core.Resource

class Compute extends Resource {

    static String term = 'compute'
    static String scheme = 'http://schemas.ogf.org/occi/infrastructure#'
    static String occi_compute_arch     = "occi.compute.architecture"
    static String occi_compute_arch_X86 = "x86"
    static String occi_compute_arch_X64 = "x64"
    static String occi_compute_cores    = "occi.compute.cores"
    static String occi_compute_hostname = "occi.compute.hostname"
    static String occi_compute_speed    = "occi.compute.speed"
    static String occi_compute_memory   = "occi.compute.memory"
    static String occi_compute_state    = "occi.compute.state"
    static String occi_compute_state_active     = 'active'
    static String occi_compute_state_inactive   = 'inactive'
    static String occi_compute_state_suspended  = 'suspended'

    static Integer slasoi_infrastructure_unixId = 0

    static transients = ['term', 'scheme', 'occi_compute_arch_X86', 'occi_compute_arch_X64', 'occi_compute_arch',
                            'occi_compute_cores', 'occi_compute_hostname', 'occi_compute_speed', 'occi_compute_memory',
                            'occi_compute_state', 'slasoi_infrastructure_unixId', 'occi_compute_state_active',
                            'occi_compute_state_inactive', 'occi_compute_state_suspended', 'slasoi_infrastructure_unixId']


    //Service owningService

    static constraints = {
        //owningService   nullable: true
    }

    def String toString() {
        return super.toString()
    }
}


//constraints
//        hostname    nullable: false, blank: true
//        cpu_arch    nullable: false, blank: false, inList: [occi_compute_arch_X86, occi_compute_arch_X64]
//        cpu_cores   nullable: false, blank: false
//        cpu_speed   nullable: false, blank: false
//        memory_size nullable: false, blank: false
//        state       nullable: false, blank: false, inList: [occi_compute_state_active, occi_compute_state_inactive, occi_compute_state_suspended]

        //SLA@SOI
//        hostingPhyMachine blank: true, nullable: true

//define attributes
//    String cpu_arch     = occi_compute_arch_X86
//    Integer cpu_cores   = 0
//    String hostname     = ''
//    Float cpu_speed     = 0
//    Float memory_size   = 0
//    String state        = occi_compute_state_inactive

//SLA@SOI specific
//    String hostingPhyMachine
//    String imageIdentifier = ''
