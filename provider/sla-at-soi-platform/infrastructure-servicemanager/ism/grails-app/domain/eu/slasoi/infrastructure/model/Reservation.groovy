package eu.slasoi.infrastructure.model

import occi.core.Resource

/*
        POST /reservation/ HTTP/1.1

        Category: reservation; [...]
        Category: small; [...]
        Category: ubuntu; [...]

  ---------------------------------------

        POST /reservation/ HTTP/1.1
        Content-type: text/multipart; boundary=123

        --123
        Header: xxx

        Category: reservation; [...]
        --123
        Header: xxx

        Category: service; [...]
        --123
        Header: xxx

        Category: small; [...]
        Category: ubuntu; [...]
        --123
        Header: xxx

        Category: small; [...]
        Category: ubuntu; [...]
        --123--

 */
class Reservation extends Resource{

    static String term = 'reservation'
    static String scheme = 'http://sla-at-soi.eu/occi/infrastructure#'

    static String eu_slasoi_infrastructure_reservation_leasePeriod = "eu.slasoi.infrastructure.reservation.leasePeriod"
    static String eu_slasoi_infrastructure_reservation_leaseCreated = "eu.slasoi.infrastructure.reservation.created"
    static String eu_slasoi_infrastructure_reservation_state = "eu.slasoi.infrastructure.reservation.state"
    static String eu_slasoi_infrastructure_reservation_state_active = "active"
    static String eu_slasoi_infrastructure_reservation_state_inactive = "inactive"

    static transients = ['term', 'scheme', 'eu_slasoi_infrastructure_reservation_state', 'eu_slasoi_infrastructure_reservation_state_active', 'eu_slasoi_infrastructure_reservation_state_inactive']

    //the provisioning request that should be executed if reservation is committed to
    String provisioningRequest
    Boolean isMultipart = false
    
    static constraints = {
        provisioningRequest nullable: false, blank: false
    }
}
