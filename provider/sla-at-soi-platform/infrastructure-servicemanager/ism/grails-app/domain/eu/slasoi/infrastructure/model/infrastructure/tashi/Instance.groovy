package eu.slasoi.infrastructure.model.infrastructure.tashi

class Instance {

    static mapWith = "redis"

    String vmId
    String hostId
    String decayed
    String state
    String userId
    String name
    String cores
    String cpuShare
    String memory
    String disks
    String nics
    String hints
    String groupName

    static constraints = {
    }
}
