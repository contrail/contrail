package eu.slasoi.infrastructure.model.infrastructure.tashi

class Host {

    static mapWith = "redis"

    String name
    String up
    String decayed
    String state
    String memory
    String cores
    String cpuSpeed
    String version
    String notes
    String reserved

    static constraints = {
    }

    String toString(){
        return "name:$name - up:$up - decayed:$decayed - state:$state - memory:$memory - cores:$cores - CPU speed:$cpuSpeed - version:$version - notes:$notes - reserved:$reserved"
    }
}
