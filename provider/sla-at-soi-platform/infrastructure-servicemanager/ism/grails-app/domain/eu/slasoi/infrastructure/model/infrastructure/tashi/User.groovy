package eu.slasoi.infrastructure.model.infrastructure.tashi

class User {

    static mapWith = "redis"

    String name
    String passwd
    String priority

    static constraints = {
    }
}
