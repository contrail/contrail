package eu.slasoi.infrastructure.controllers

class ErrorHandlerController {

    static def OCCI_VERSION = 'OCCI/1.1'

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }

    def internalError = {

        def errorMsg = request.'javax.servlet.error.message'

        log.error errorMsg + "\nInternal Error: " + HttpURLConnection.HTTP_INTERNAL_ERROR
        response.contentType = 'text/plain'
        response.status = HttpURLConnection.HTTP_INTERNAL_ERROR
        render errorMsg + "\nInternal Error: " + HttpURLConnection.HTTP_INTERNAL_ERROR
    }

    def notFound = {

        def errorMsg = request.'javax.servlet.error.message'

        log.error errorMsg + "\nNot Found: " + HttpURLConnection.HTTP_NOT_FOUND
        response.contentType = 'text/plain'
        response.status = HttpURLConnection.HTTP_NOT_FOUND
        render errorMsg + "\nNot Found: " + HttpURLConnection.HTTP_NOT_FOUND
    }
    
    def notAcceptable = {

        log.error "Not acceptable: " + HttpURLConnection.HTTP_NOT_ACCEPTABLE

        response.contentType = 'text/plain'
        response.status = HttpURLConnection.HTTP_NOT_ACCEPTABLE
        render "Not acceptable: " + HttpURLConnection.HTTP_NOT_ACCEPTABLE
    }

    def badRequest = {

        def errorMsg = request.'javax.servlet.error.message'

        log.error errorMsg + "\nBad Request: " + HttpURLConnection.HTTP_BAD_REQUEST
        response.status = HttpURLConnection.HTTP_BAD_REQUEST
        response.contentType = 'text/plain'
        render errorMsg + "\nBad Request: " + HttpURLConnection.HTTP_BAD_REQUEST
    }

    def conflict = {

        def errorMsg = request.'javax.servlet.error.message'

        log.error errorMsg + "\nConflicting resources: " + HttpURLConnection.HTTP_CONFLICT
        response.contentType = 'text/plain'
        response.status = HttpURLConnection.HTTP_CONFLICT
        render errorMsg + "\nConflicting resources: " + HttpURLConnection.HTTP_CONFLICT
    }

    def notImplemented = {

        def errorMsg = request.'javax.servlet.error.message'

        log.error errorMsg + "\nFeature not implemented: " + HttpURLConnection.HTTP_NOT_IMPLEMENTED
        response.contentType = 'text/plain'
        response.status = HttpURLConnection.HTTP_NOT_IMPLEMENTED
        render errorMsg + "\nFeature not implemented: " + HttpURLConnection.HTTP_NOT_IMPLEMENTED
    }
}
