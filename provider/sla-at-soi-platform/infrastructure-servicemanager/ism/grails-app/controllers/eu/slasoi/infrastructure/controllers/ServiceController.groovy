/* 
SVN FILE: $Id: ServiceController.groovy 2774 2011-07-22 15:47:44Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2774 $
@lastrevision   $Date: 2011-07-22 17:47:44 +0200 (pet, 22 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/controllers/eu/slasoi/infrastructure/controllers/ServiceController.groovy $

*/

package eu.slasoi.infrastructure.controllers

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.exceptions.IsmHttpException
import eu.slasoi.infrastructure.model.Service

//TODO merge with compute controller into a KindController

class ServiceController {

    def computeService
    def infrastructureInstanceFactoryService
    def multipartService

    static def OCCI_VERSION = 'OCCI/1.1'

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }
    
    def doGet = {

        if (params.instanceId) {

            log.info "A service instance detailing has been requested..."

            def svc = Service.findByO_id(params.instanceId)

            if (svc) {
                withFormat {
                    all{ serviceDetailsAsText(svc) }
                    text{ serviceDetailsAsText(svc) }
                    occi{ serviceDetailsAsOcci(svc) }
                    uriList{ serviceDetailsAsUriList(svc) }
                    multipartForm{ serviceAsMultipart(svc) }
                }
            }
            else {
                log.warn "There is no service instances running with the id of ${params.instanceId}."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "There is no service instances running with the id of ${params.instanceId}."
            }
        }
        else {
            log.info "A service listing has been requested..."

            def svcs = Service.list()

            if(svcs.size() <= 0){
                response.status = HttpURLConnection.HTTP_NO_CONTENT
                response.contentType = request.contentType
                render ""
            }
            else{
                withFormat {
                    all{serviceListAsText(svcs)}
                    text{serviceListAsText(svcs)}
                    occi{serviceListAsOcci(svcs)}
                    uriList{serviceListAsUriList(svcs)}
                }
            }
        }
    }

    //render instances
    private def serviceDetailsAsText(svc){

        response.status = HttpURLConnection.HTTP_OK
        response.setHeader("Content-type", "text/plain")

        //This removes the monitoring value - if the content is XML it will break the parser
        def monAttr = svc.getAttrValue('eu.slasoi.infrastructure.service.monitoring.config')

        def svcStr = svc.toString()
        if(!svcStr.endsWith("\n") || !svcStr.endsWith("\r\n"))
            svcStr = svcStr + '\r\n'

        def content = svcStr + monAttr.value + '\r\n'

        content = content.replaceAll("\\s*,\\s*eu.slasoi.infrastructure.service.monitoring.config\\s*=['+|\"+].*?['+|\"+]", "")

        render content
    }

    private def serviceDetailsAsOcci(svc){

        response.status = HttpURLConnection.HTTP_OK
        response.contentType = 'text/occi'

        def headers = svc.toHeaderArray()

        response.setHeader('Category', headers['categories'])
        response.setHeader('X-OCCI-Attribute', headers['attributes'])
        response.setHeader('Link', headers['links'])

        render "ok\r\n"
    }

    private def serviceDetailsAsUriList(svc){

        log.warn "Requested content type text/occi for a service instance. Not acceptable."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for a service instance. Not acceptable."
    }

    //render lists
    private def serviceListAsText(svcs){

        response.status = HttpURLConnection.HTTP_OK
        response.setHeader("Content-type", "text/plain")

        svcs.each {
            render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.kind.location + it.o_id + "\n"
        }
    }

    private def serviceListAsOcci(svcs){

        response.status = HttpURLConnection.HTTP_OK
        response.setHeader("Content-type", "text/occi")

        def header = ''
        svcs.each {
              header += CH.config.grails.serverURL + it.kind.location + it.o_id + ", "
        }
        
        response.setHeader('X-OCCI-Location', header[0..header.length()-2])
    }

    private def serviceListAsUriList(svcs){

        response.status = HttpURLConnection.HTTP_OK
        response.setHeader("Content-type", "text/uri-list")

        svcs.each {
            render "${CH.config.grails.serverURL}${it.location}${it.o_id}\n"
        }
    }

    private def serviceAsMultipart(Service service){

        log.info "Render all computes as multipart"
        def ctType = request.getHeader('accept')
        def boundary = ctType?.split('boundary=')

        if(boundary && boundary.size() >= 2)
            boundary = boundary[1]
        else
            boundary = Long.toHexString(Double.doubleToLongBits(Math.random()))

        response.contentType = "multipart/form-data; boundary=${boundary}"
        response.status = HttpURLConnection.HTTP_OK

        def body = multipartService.createMultipart(service.resources, boundary)

        render body
    }

    def doPost = {

        //this should be in fact a negotiation and then provisioning style process
        if (params.instanceId) {
            if(request?.queryString?.startsWith('action=')){

                def actionName = request.queryString.split('action=')[1]
                def svc = Service.findByO_id(params.instanceId)
                serviceExecuteAction(svc, actionName)
            }
            else{
                //it's a partial update
                log.error "Partial updates to a service is not yet supported."
                response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, "Partial updates to a service is not yet supported."
            }
        }
        else {

            //get the service request, create the service instance
            Service service = createService(request)

            if(service){
                response.status = HttpURLConnection.HTTP_CREATED
                response.setHeader("Location", "${service.kind.location}${service.o_id}")
                render ""
            }
            else{
                log.error "The result of creating the service is null. Internal Error"
                response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "The result of creating the service is null. Internal Error"
            }
        }
    }

    private def serviceExecuteAction(Service service, String actionName){
        if(service){

            def act = service.kind.actions.find{it.term == actionName}

            if(act){
                log.info "Executing the ${actionName} on all the service's compute resources."
                
                service.resources.each { comp ->
                    publishEvent(new ComputeCreateEvent([resource:comp, action:actionName]))
                }

                response.status = HttpURLConnection.HTTP_OK
                render 'ok\r\n'
            }
            else{
                log.error "Unknown action received for execution: ${act}"
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Unknown action ${act} for instance ${service.o_id} received for execution"
            }
        }
        else{
            response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Compute resource ${params.instanceId} not found to execute action (${actionName}) on."
            log.error "Compute resource ${params.instanceId} not found to execute action (${actionName}) on."
        }
    }

    def doPut = {

        if (params.instanceId) {
            def svc = Service.findByO_id(params.instanceId)
            if(svc){
                log.info "Updating the service collection is not yet supported"
                response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, "Updating the service collection is not yet supported"
                render ""
            }
            else{

                Service service = createService(request, params.instanceId)
                
                if(service){
                    response.status = HttpURLConnection.HTTP_CREATED
                    response.setHeader("Location", "${CH.config.grails.serverURL}${service.kind.location}${service.o_id}")
                    render ""
                }
                else{
                    log.error "The result of creating the service is null. Internal Error"
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "The result of creating the service is null. Internal Error"
                }
            }
        }
    }

    private Service createService(request, serviceId=null)  {

        log.info "Requested batch compute creation"

        def contentHeader = request.getHeader('content-type')
        def boundary = contentHeader.split('boundary=')
        if(boundary && boundary.size() >= 2)
            boundary = boundary[1]
        else{
            log.error "The multipart content-type value did not include a boundary value."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "The multipart content-type value did not include a boundary value."
        }


        def content = request.reader?.text
        content = multipartService.extractContent(content, boundary)

        if(content.size() <= 0){
            log.error "There was no body-content in the request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "There was no body-content in the request."
        }
        else{

            Service svc
            try{
                svc = infrastructureInstanceFactoryService.createService(content, serviceId)
            }catch(IsmHttpException ie){
                log.error ie.message
                response.sendError ie.httpErrorCode, ie.message
                return
            }

            svc.resources.each{
                publishEvent(new ComputeCreateEvent([resource:it]))
            }

            return svc
        }
    }

    def doDelete = {

        if (params.instanceId) {
            log.info "A service has been requested to be deleted... id: " + params.instanceId

            Service s = Service.findByO_id(params.instanceId)

            if (s) {

                s.resources.each { res ->
                    computeService.delete(res)
                }

                infrastructureInstanceFactoryService.deleteService(s)

                response.status = HttpURLConnection.HTTP_OK
                log.info "The service and associated resources is now deleted from the IL and is being removed from the provisioning system. ID: " + params.serviceId
                render "ok\r\n"
            }
            else {
                log.warn "The requested service was not found for deletion. ID: " + params.serviceId
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "The requested service was not found for deletion. ID: " + params.serviceId
            }
        }
        else {
            log.error "Cannot delete the parent container of all services."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Cannot delete the parent container of all services."
        }
    }
}
