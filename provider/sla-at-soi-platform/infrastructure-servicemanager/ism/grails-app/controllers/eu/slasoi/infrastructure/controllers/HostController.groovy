package eu.slasoi.infrastructure.controllers

import eu.slasoi.infrastructure.model.infrastructure.tashi.Host

class HostController {

    def doGet = {
        
        Host.list().each {
            render it.toString()
        }
    }
}

