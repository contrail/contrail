/* 
SVN FILE: $Id: ComputeController.groovy 2770 2011-07-22 14:37:13Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2770 $
@lastrevision   $Date: 2011-07-22 16:37:13 +0200 (pet, 22 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/controllers/eu/slasoi/infrastructure/controllers/ComputeController.groovy $

*/

package eu.slasoi.infrastructure.controllers

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import occi.core.Entity
import occi.infrastructure.Compute
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException
import org.springframework.context.ApplicationEvent
import eu.slasoi.infrastructure.exceptions.IsmHttpException

//TODO extract to own class file?
class ComputeCreateEvent extends ApplicationEvent {
    
	ComputeCreateEvent(Map source) {
		super(source)
	}
}

//TODO rename this controller to KindController
//TODO where a save is executed, check for success and handle errors with controller, allow for transactions
//TODO implement filters

class ComputeController {

    static def OCCI_VERSION = 'OCCI/1.1'

    def infrastructureInstanceFactoryService
    def computeService
    def multipartService

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }

    def doGet = {

        if (params.computeId) {
            log.info "Getting details on requested compute kind: " + params.computeId
            def comp = Compute.findByO_id(params.computeId)
            if(comp){
                withFormat {
                    all{ computeDetailsAsText(comp) }
                    text{ computeDetailsAsText(comp) }
                    occi{ computeDetailsAsOcci(comp) }
                    uriList{ computeDetailsAsUriList(comp) }
                }
            }else{
                log.warn "There is no compute instances running with the id of ${params.computeId}."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "There is no compute instances running with the id of ${params.computeId}."
            }
        }
        else {
            def computes=null

            log.info "Getting details on all compute kinds"

            computes = Compute.list()

            if(computes?.size() > 0){
                withFormat {
                    all{ computeListAsText(computes) }
                    text{ computeListAsText(computes) }
                    occi{ computeListAsOcci(computes) }
                    uriList{ computeListAsUrlList(computes) }
                    multipartForm{computeListAsMultipart(computes)}
                }
            }else {
                log.warn "There are no compute instances available to list."
                response.sendError HttpURLConnection.HTTP_OK
            }
        }
    }

    private def computeListAsText(list) {
        response.contentType = 'text/plain'
        list.each{
            render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.kind.location + it.o_id + "\n"
        }
    }

    private def computeListAsOcci(list) {

        response.contentType = 'text/occi'
        def value = ''
        list.each{
            value += CH.config.grails.serverURL + it.kind.location + it.o_id + ", "
        }
        response.setHeader('X-OCCI-Location', value.trim()[0..-2])
    }

    private def computeListAsUrlList(list) {

        response.contentType = 'text/uri-list'
        list.each{
            def out = CH.config.grails.serverURL + it.kind.location + it.o_id + "\n"
            render out
        }
    }

    private def computeDetailsAsText(compute) {

        response.status = HttpURLConnection.HTTP_OK
        response.contentType = 'text/plain'
        render compute.toString()
    }

    private def computeDetailsAsOcci(compute) {

        response.status = HttpURLConnection.HTTP_OK
        response.contentType = 'text/occi'

        def headers = compute.toHeaderArray()

        response.setHeader('Category', headers['categories'])
        response.setHeader('X-OCCI-Attribute', headers['attributes'])
        response.setHeader('Link', headers['links'])

        render "ok\r\n"
    }

    private def computeDetailsAsUriList(compute){
        
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for an instance. Not acceptable."
        log.warn "Requested content type text/occi for an instance. Not acceptable."
    }

    private def computeListAsMultipart(compute){

        log.info "Render all computes as multipart"

        def ctType = request.getHeader('accept')
        def boundary = ctType?.split('boundary=')

        if (boundary && boundary.size() >= 2)
            boundary = boundary[1]

        response.contentType = "multipart/form-data; boundary=${boundary}"
        response.status = HttpURLConnection.HTTP_OK

        render multipartService.createMultipart(compute, boundary)
    }

    def doPost = {

        //if an action directed at a resource instance
        if(params.computeId){
            if(request?.queryString?.startsWith('action=')){

                def actionName = request.queryString.split('action=')[1]
                def comp = Compute.findByO_id(params.computeId)
                computeExecuteAction(comp, actionName)
            }
            else{
                //it's a partial update
                doPut()
            }
        }
        else{
            //if an action directed at a resource collection
            if(request?.queryString?.startsWith('action=')){

                def actionName = request.queryString.split('action=')[1]
                log.info "Action to be executed on all compute resources: ${actionName}"
                //TODO perform action type check here
                def comp = Compute.list()

                comp.each {
                    computeExecuteAction(it, actionName)
                }
            }
            //otherwise its a creation request
            else{
                log.info "Request for compute resource creation received."
                withFormat {
                    all{ computeCreateAsText() }
                    text{ computeCreateAsText() }
                    occi{ computeCreateAsOcci() }
                    uriList{ computeCreateAsUriList() }
                    multipartForm{computeCreateAsMultipart()}
                }
            }
        }
    }

    private def computeCreateAsText(String computeId=''){

        def content = request.reader?.text
        
        if(!content){
            log.error "Bad Request: No content supplied in request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Bad Request: No content supplied in request."
        }
        else{
            
            def parsedAttrs
            try{
                parsedAttrs = OcciParser.getParser(content).headers()
            }catch (OcciParserException pe){
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                return
            }

            if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Compute.term, Compute.scheme)){

                if(computeId)
                    parsedAttrs[OcciParser.occi_attributes][0][Entity.occi_core_id] = computeId

                Compute comp
                try{
                    comp = infrastructureInstanceFactoryService.createCompute(parsedAttrs)
                }catch(IsmHttpException ie){
                    response.sendError ie.httpErrorCode, ie.message
                }


                if(!comp){
                    log.error "The creation of the ISM compute instance failed."
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "The creation of the ISM compute instance failed."
                }
                else{
                    publishEvent(new ComputeCreateEvent([resource:comp]))
                    response.status = HttpURLConnection.HTTP_CREATED
                    response.setContentType('text/plain')
                    render "X-OCCI-Location: ${CH.config.grails.serverURL}${comp.kind.location}${comp.o_id}"
                }
            }
            else{
                log.error 'There was no compute category detected in the request.'
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, 'There was no compute category detected in the request.'
            }
        }
    }

    private def computeCreateAsOcci(String computeId=''){

        def content = request.getHeader('Category')
        
        if(!content || content.length() <= 0){
            log.error "Bad Request: No category header supplied in request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Bad Request: No category header supplied in request."
        }
        else{
            content = 'Category: ' + content
            def attr = request.getHeader('X-OCCI-Attribute') ?: ''

            if(attr.length() > 0)
                content = content + '\n' + 'X-OCCI-Attribute: ' + attr

            def parsedAttrs
            try{
                parsedAttrs = OcciParser.getParser(content).headers()
            }catch (OcciParserException pe){
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                return
            }

            if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Compute.term, Compute.scheme)){

                if(computeId)
                    parsedAttrs[OcciParser.occi_attributes][0][Entity.occi_core_id] = computeId

                Compute comp
                try{
                    comp = infrastructureInstanceFactoryService.createCompute(parsedAttrs)
                }catch(IsmHttpException ie){
                    log.error ie.message
                    response.sendError ie.httpErrorCode, ie.message
                }


                if(!comp){
                    log.error "The creation of the ISM compute instance failed."
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "The creation of the ISM compute instance failed."
                }
                else{

                    publishEvent(new ComputeCreateEvent([resource:comp]))
                    response.status = HttpURLConnection.HTTP_CREATED
                    response.setContentType(request.getContentType())
                    response.setHeader("Location", "${CH.config.grails.serverURL}${comp.kind.location}${comp.o_id}")
                    render "ok\r\n"
                }
            }
            else{
                log.error 'There was no compute category detected in the request.'
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, 'There was no compute category detected in the request.'
            }
        }
    }

    private def computeCreateAsUriList(){

        log.warn "Requested content type text/occi for creating an instance. Not acceptable."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for creating an instance. Not acceptable."
    }

    private def computeCreateAsMultipart(){

        //split up the multipart data
        //loop over it and supply to computeCreateAsText

        //it makes no sense for the content in the body to be presented as text/occi
        //it's text/plain by the very fact it's in the body! makes placing it in the header redundant
        log.info "Requested batch compute creation"
        def contentHeader = request.getHeader('content-type')
        def boundary = contentHeader.split('boundary=')
        if(boundary && boundary.size() >= 2)
            boundary = boundary[1]
        else
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "The multipart content-type value did not include a boundary value."

        def content = request.reader?.text
        content = multipartService.extractContent(content, boundary)

        if(content.size() <= 0)
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "There was no body-content in the request."
        else{

            def comps = []

            //START TRANSACTION
            content.each{ c ->

                def parsedAttrs
                try{
                    parsedAttrs = OcciParser.getParser(c.value[0]).headers()
                }catch (OcciParserException pe){
                    def err = pe.toString()
                    response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                    return
                }
                if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Compute.term, Compute.scheme)){

                    Compute comp
                    try{
                        comp = infrastructureInstanceFactoryService.createCompute(parsedAttrs)
                    }catch(IsmHttpException ie){
                        response.sendError ie.httpErrorCode, ie.message
                    }


                    if(!comp){
                        log.error "There was a problem creating the compute:\n${parsedAttrs}"
                        response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "There was a problem creating the compute:\n${parsedAttrs}"
                    }
                    else{
                        comps.add comp
                    }
                }
                else{
                    log.error 'There was no compute category detected in the request.'
                    response.sendError HttpURLConnection.HTTP_BAD_REQUEST, 'There was no compute category detected in the request.'
                }
            }
            //END TRANSACTION
            
            if(content.size() == comps.size()){
                
                //submit to backend
                comps.each{
                    //computeService.create(it)
                    publishEvent(new ComputeCreateEvent([resource:it]))
                }

                //respond to the client

                response.status = HttpURLConnection.HTTP_CREATED
                response.contentType = 'text/plain'
                def location = ''
                comps.each { c ->
                    location += "${CH.config.grails.serverURL}${c.kind.location}${c.o_id}, "
                }

                render "X-OCCI-Location: ${location.trim()[0..-2]}"
            }
            else{
                response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "There was a problem fulfulling the batch request. Check server logs."
                log.error "There was a problem fulfulling the batch request. Check server logs."
            }
        }
    }

    private def computeExecuteAction(comp, actionName){

        if(comp){

            def act = comp.kind.actions.find{it.term == actionName}

            if(act){
                log.info "Executing the ${actionName} on the compute resource."

                def hostname = comp.getAttrValue(Compute.occi_compute_hostname).getTypedValue()

                if(!hostname || hostname?.length() <= 0){
                    log.error "Cannnot execute action on the instance as there is no hostname associated with it."
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "Cannnot execute action on the instance as there is no hostname associated with it."
                }
                else{
                    publishEvent(new ComputeCreateEvent([resource:hostname, action:actionName]))
                    response.status = HttpURLConnection.HTTP_OK
                    render 'ok\r\n'
                }
            }
            else{
                log.error "Unknown action received for execution: ${act}"
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Unknown action ${act} for instance ${comp.o_id} received for execution"
            }
        }
        else{
            response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Compute resource ${params.computeId} not found to execute action (${actionName}) on."
            log.error "Compute resource ${params.computeId} not found to execute action (${actionName}) on."
        }

    }

    def doPut = {
        
        if(params.computeId){

            def comp = Compute.findByO_id(params.computeId)

            if(comp){
                log.info "Updating a compute resource ${params.computeId}"
                withFormat {
                    all{ computeUpdateAsText(comp) }
                    text{ computeUpdateAsText(comp) }
                    occi{ computeUpdateAsOcci(comp) }
                    uriList{ computeUpdateAsUriList() }
                    multipartForm{computeUpdateAsMultipart()}
                }
            }
            else{
                log.info "Creating a compute resource via PUT"

                if(!Compute.findByO_id(params.computeId)){
                    log.info "Creating a compute resource ${params.computeId}"
                    withFormat {
                        all{ computeCreateAsText(params.computeId) }
                        text{ computeCreateAsText(params.computeId) }
                        occi{ computeCreateAsOcci(params.computeId) }
                        uriList{ computeCreateAsUriList() }
                        multipartForm{computePutCreateAsMultipart()}
                    }
                }
                else{
                    log.error "The requested ID (${params.computeId}) for creating the compute already exists"
                    response.sendError HttpURLConnection.HTTP_CONFLICT, "The requested ID (${params.computeId}) for creating the compute already exists"
                }
            }

        }
        else{
            response.status = HttpURLConnection.HTTP_BAD_REQUEST
        }
    }

    //TODO
    private def computePutCreateAsMultipart(){
        log.info "Received request for batch creation via PUT. Not currently implemented"
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, "Received request for batch creation via PUT. Not currently implemented"
    }

    //TODO
    private def computeUpdateAsMultipart(){
        log.info "Received request for batch update via PUT. Not currently implemented"
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, "Received request for batch creation via PUT. Not currently implemented"
    }

    private def computeUpdateAsText(Compute comp){

        def content = request.reader?.text

        if(!content){
            log.error "Bad Request: No content supplied in request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Bad Request: No content supplied in request."
        }
        else{
            def parsedAttrs
            try{
                parsedAttrs = OcciParser.getParser(content).headers()
            }catch (OcciParserException pe){
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                return
            }

            comp = update(comp, parsedAttrs)

            if(comp){
                log.info "Updated compute resource ${comp.o_id}"
                response.status = HttpURLConnection.HTTP_OK
                render 'ok\r\n'
            }
        }
    }

    private def computeUpdateAsOcci(Compute comp){

        //assemble the content
        def content = request.getHeader('Category') ?: ''
        if(content.length() > 0)
            content = 'Category: ' + content
        
        def attr = request.getHeader('X-OCCI-Attribute') ?: ''
        if(attr.length() > 0)
            content = content + '\n' + 'X-OCCI-Attribute: ' + attr

        //parse the content
        def parsedAttrs
        try{
            parsedAttrs = OcciParser.getParser(content).headers()
        }catch (OcciParserException pe){
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
            return
        }

        //update the compute resource
        comp = update(comp, parsedAttrs)

        if(comp){
            log.info "Updated compute resource ${comp.o_id}"
            response.status = HttpURLConnection.HTTP_OK
            render 'ok\r\n'
        }
    }

    private def computeUpdateAsUriList(){

        log.warn "Requested content type text/occi for updating an instance. Not acceptable."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for updating an instance. Not acceptable."
    }

    private Compute update(Compute comp, parsedAttrs){

        if(parsedAttrs[OcciParser.occi_categories][0]?.size() > 0){
            log.info "Updating instance type construction"
            try{
                comp = infrastructureInstanceFactoryService.updateType(comp, parsedAttrs[OcciParser.occi_categories][0])
            }catch(IsmHttpException ie){
                response.sendError ie.httpErrorCode, ie.message
                return
            }
        }

        if(parsedAttrs[OcciParser.occi_links][0]?.size() > 0)
            log.warn "Links supplied in update - ignoring these"

        if(parsedAttrs[OcciParser.occi_attributes][0]?.size() > 0){
            log.info "Updating instance attributes"
            comp = infrastructureInstanceFactoryService.updateAttributes(comp, parsedAttrs[OcciParser.occi_attributes][0])
        }

        if(comp)
            computeService.update(comp)
        return comp
    }



    def doDelete = {
        
        if(params.computeId){
            log.info "Requesting the deletion of compute: ${params.computeId}"
            withFormat {
                all{ computeDeleteAsText() }
                text{ computeDeleteAsText() }
                occi{ computeDeleteAsOcci() }
                uriList{ computeDeleteAsUriList() }
            }
        }
        else{
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Cannot delete collections of computes"
        }
    }

    private def computeDeleteAsText(){
        deleteCompute()
    }

    private def computeDeleteAsOcci(){
        deleteCompute()
    }

    private def computeDeleteAsUriList(){

        log.error "Requested content type text/occi for deleting an instance. Not acceptable."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for deleting an instance. Not acceptable."
    }

    private def deleteCompute(){

        def comp = Compute.findByO_id(params.computeId)

        if(comp){ //handles the case where a compute is associated with a service

            infrastructureInstanceFactoryService.deleteCompute(comp)

            computeService.delete(comp)

            log.info "Deleted compute instance: ${params.computeId}"

            response.status = HttpURLConnection.HTTP_OK
            render 'ok\r\n'
        }
        else{
            log.warn "The requested compute ${params.computeId} was not found."
            response.sendError HttpURLConnection.HTTP_NOT_FOUND, "The requested compute ${params.computeId} was not found."
        }
    }
}
