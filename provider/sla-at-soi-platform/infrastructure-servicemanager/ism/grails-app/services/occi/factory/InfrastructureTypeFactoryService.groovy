
/*
SVN FILE: $Id: InfrastructureTypeFactoryService.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/services/occi/factory/InfrastructureTypeFactoryService.groovy $
*/

package occi.factory

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import occi.core.Kind
import occi.core.Mixin

class InfrastructureTypeFactoryService extends CoreTypeFactoryService{

    //This method will setup all the kinds according to the OCCI specification
    def initDefaults(){

        createCoreTypes()
        createTypes(CH.config.eu.slasoi.infrastructure.resources, 'kind')
        createTypes(CH.config.eu.slasoi.infrastructure.links, 'kind')
        createTypes(CH.config.eu.slasoi.infrastructure.mixins, 'mixin')
        createTypes(CH.config.eu.slasoi.infrastructure.templates, 'mixin')
    }

    def createCoreTypes(){
        createResourceKind()
        createLinkKind()
    }

    def createTypes(types, cat_type){

        def create = { type, isEnabled ->

            def cat = null

            if(cat_type == 'kind'){
                cat = Kind.findBySchemeAndTerm(type.value.scheme, type.key)
            }
            else if(cat_type == 'mixin'){
                cat = Mixin.findBySchemeAndTerm(type.value.scheme, type.key)
            }
            else{
                log.warn "Unrecognised type creation: ${cat_type}"
            }

            if(!cat){
                createType(type, cat_type, isEnabled)
            }
            else
                log.info "The requested category already exists: ${type.value.scheme}${type.key}"
        }

        types.enabled.each{ type ->
            create(type, true)
        }

        types.disabled.each{ type ->
            create(type, false)
        }

        log.info "done"
    }

    private def createType(res, String cat_type, Boolean isEnabled){

        def kind = null

        log.info "Creating ${res.value.scheme}${res.key}"

        if(cat_type.equalsIgnoreCase('kind')){

            kind = new Kind(term: res.key, scheme: res.value.scheme, title: res.value.title,
                    location: res.value.location, isEnabled: isEnabled)

            def rel = res.value.rel?.parent?.split('#')

            if(rel?.size() == 2){
                def superKind = Kind.findBySchemeAndTerm(rel[0]+'#', rel[1])
                if(superKind){
                    kind.addToRelated superKind
                    kind.save()
                    checkErrors(kind)
                }
                else{
                    log.error "The types super kind was not found ${rel[0]+'#'}${rel[1]}"
                    throw new Exception("The types super kind was not found ${rel[0]+'#'}${rel[1]}")
                }
            }
            else{
                log.info "Type has no super type"
            }
        }

        else if(cat_type.equalsIgnoreCase('mixin')){

            kind = new Mixin(term: res.key, scheme: res.value.scheme, title: res.value.title,
                    location: res.value.location, isEnabled: isEnabled)

            def rel = res.value.rel?.kind?.split('#')

            if(rel?.size() == 2){
                def applicableKind = Kind.findBySchemeAndTerm(rel[0]+'#', rel[1])
                if(applicableKind){
                    kind.applicableKind = applicableKind
                }
                else{
                    log.error "The mixin's applicable kind was not found ${rel[0]+'#'}${rel[1]}"
                    throw new Exception("The mixin's applicable kind was not found ${rel[0]+'#'}${rel[1]}")
                }
            }

            rel = res.value.rel?.parent?.split('#')

            if(rel?.size() == 2){
                def superKind = Mixin.findBySchemeAndTerm(rel[0]+'#', rel[1])
                if(superKind){
                    kind.addToRelated superKind
                }
                else{
                    log.error "The types super kind was not found ${rel[0]+'#'}${rel[1]}"
                    throw new Exception("The types super kind was not found ${rel[0]+'#'}${rel[1]}")
                }
            }
            else{
                log.info "Type has no super type"
            }
            
            kind.save()
            checkErrors(kind)
        }

        //add the attributes
        res.value.attributes.each{ res_attr ->
            createAttributeType(kind, res_attr.key, res_attr.value.type, res_attr.value.default,
                                        res_attr.value.render, res_attr.value.mutable, res_attr.value.required)
        }

        //add the actions
        res.value.actions.each{ act ->
            def action = createAction(act.key, act.value.scheme, act.value.title)

            act?.value?.attributes?.each{ act_attr ->
                createAttributeType(action, act_attr.key, act_attr.value.type, act_attr.value.default,
                                        act_attr.value.render, act_attr.value.mutable, act_attr.value.required)
            }

            kind.addToActions action
        }

        kind.save()
        checkErrors(kind)
    }

    // --------- Templates ---------
//    public Mixin createOsTemplateMixin(){
//
//        def osTpl = Mixin.findByTermAndScheme(OsTemplateMixin.term, OsTemplateMixin.scheme)
//
//        if(!osTpl){
//            def compKind = createComputeKind()
//            osTpl = createMixin(OsTemplateMixin.term, OsTemplateMixin.scheme, "An OS Template Mixin", '', compKind)
//        }
//
//        return osTpl
//    }
//
//    public createOsMixins(){
//
//        def os_tpls = CH.config.eu.slasoi.infrastructure.templates.os
//
//        def compKind = createComputeKind()
//
//        os_tpls.each { tpl ->
//
//            def os_tpl = Mixin.findByTermAndScheme(tpl['term'], tpl['scheme'])
//            if(!os_tpl){
//                os_tpl = createMixin(tpl['term'], tpl['scheme'], tpl['title'] ?: '', tpl['location'], compKind)
//                os_tpl.addToRelated createOsTemplateMixin()
//                os_tpl.save()
//                checkErrors(os_tpl)
//            }
//        }
//    }
    /*
    //        def types = CH.config.eu.slasoi.infrastructure.types
//        def mixins = CH.config.eu.slasoi.infrastructure.mixins
//
//        if(types.compute.enable){
//            log.info "OCCI Compute Type is enabled"
//            createComputeKind(types.compute)
//            createOsTemplateMixin()
//            //createDefaultOsMixin()
//            createOsMixins()
//            //createResourceTemplateMixin()
//        }
//
//
//        if(types.network.enable){
//            log.info "OCCI Network Types are enabled"
//            createNetworkKind(types.network)
//            addIPNetworkMixin(mixins.ipnetwork)
//            createNetworkLinkKind(types.networkinterface)
//            addIPNetworkLinkMixin(mixins.ipnetworklink)
//        }
//
//        if(types.storage.enable){
//            log.info "OCCI Storage Types are enabled"
//            createStorageKind(types.storage)
//            createStorageLinkKind(types.storagelink)
//        }
//
//
//        if(types.slasoi.enable){
//            log.info "SLA@SOI specific type extensions are enabled"
//            createServiceKind(types.slasoi)
//            addSlaSoiComputeMixin(mixins.slasoicompute)
//        }
// ---------- Compute ----------
    public Kind createComputeKind(config=CH.config.eu.slasoi.infrastructure.types.compute){

        log.info("Creating the compute kind")
        def computeKind = Kind.findByTermAndScheme(Compute.term, Compute.scheme)

        if(!computeKind){
            def resourceKind = createResourceKind()

            computeKind = new Kind(term: Compute.term, scheme: Compute.scheme, title: 'Compute type',
                    location: config.defaults.location ?: '/compute')
            computeKind.addToRelated resourceKind

            computeKind.save()
            checkErrors(computeKind)

            addComputeKindAttributes(computeKind, config)
            addComputeKindActions(computeKind, config)

            computeKind.save()
            checkErrors(computeKind)
        }
        return computeKind
    }

    private def addComputeKindAttributes(Kind compKind, config) {

        log.info("Adding the compute kind's attributes")

        createAttributeType(compKind, Compute.occi_compute_arch, AttributeType.STRING,
                    config.defaults.arch ?: "${Compute.occi_compute_arch_X86}",
                    true, true, false)

        createAttributeType(compKind, Compute.occi_compute_cores, AttributeType.INT,
                    config.defaults.cores ?: "1",
                    true, true, false)

        createAttributeType(compKind, Compute.occi_compute_speed, AttributeType.FLOAT,
                    config.defaults.speed ?: "2.4",
                    true, true, false)

        createAttributeType(compKind, Compute.occi_compute_memory, AttributeType.FLOAT,
                    config.defaults.memory ?: "0.5",
                    true, true, false)

        createAttributeType(compKind, Compute.occi_compute_hostname, AttributeType.STRING,
                    '', true, true, false)

        createAttributeType(compKind, Compute.occi_compute_state, AttributeType.STRING,
                    "${Compute.occi_compute_state_inactive}",
                    true, false, false)
    }

    private def addComputeKindActions(Kind compKind, config) {

        log.info("Adding the compute kind's actions")

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('start')){
            
            log.info "Start Action added to compute kind"
            def startAction = createAction(StartComputeAction.term, StartComputeAction.scheme, 'Start Compute Action')
            
            compKind.addToActions startAction
            compKind.save()
            checkErrors(compKind)
        }
        else
            log.info "No compute start action will be available"

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('stop')){

            log.info "Stop Action added to compute kind"
            def stopAction = createAction(StopComputeAction.term, StopComputeAction.scheme, 'Stop Compute Action')

            createAttributeType(stopAction, StopComputeAction.method, AttributeType.STRING, "",
                                                true, true, false)
            compKind.addToActions stopAction
        }
        else
            log.info "No compute stop action will be available"

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('restart')){

            def restartAction = createAction(RestartComputeAction.term, RestartComputeAction.scheme, 'Restart Compute Action')
            createAttributeType(restartAction, RestartComputeAction.method, AttributeType.STRING, "",
                                        true, true, false)
            compKind.addToActions restartAction
        }
        else
            log.info "No compute restart action will be available"

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('suspend')){

            log.info "Suspend Action added to compute kind"
            def suspendAction = createAction(SuspendComputeAction.term, SuspendComputeAction.scheme, 'Suspend Compute Action')
            createAttributeType(suspendAction, SuspendComputeAction.method, AttributeType.STRING, "",
                                        true, true, false)
            compKind.addToActions suspendAction
        }
        else
            log.info "No compute suspend action will be available"
    }

// ---------- Network ----------
    public Kind createNetworkKind(config=CH.config.eu.slasoi.infrastructure.types.network){

        log.info("Creating the network kind")
        def networkKind = Kind.findByTermAndScheme(Network.term, Network.scheme)
        
        if(!networkKind){

            def resourceKind = createResourceKind()
            networkKind = new Kind(term: Network.term, scheme: Network.scheme, title: 'Network type',
                    location: config.defaults.location ?: '/network')
            networkKind.addToRelated resourceKind
            networkKind.save()
            checkErrors(networkKind)

            addNetworkKindAttributes(networkKind, config)
            addNetworkKindActions(networkKind, config)

            networkKind.save()
            checkErrors(networkKind)
        }
        return networkKind
    }

    private def addNetworkKindAttributes(Kind networkKind, config) {

        log.info("Adding the network kind's attributes")

        createAttributeType(networkKind, Network.occi_network_vlan, AttributeType.INT,
                    config.defaults.vlan ?: "",
                    true, true, false)

        createAttributeType(networkKind, Network.occi_network_label, AttributeType.STRING,
                    config.defaults.label ?: "",
                    true, true, false)

        createAttributeType(networkKind, Network.occi_network_state, AttributeType.STRING,
                    "${Network.occi_network_inactive}",
                    true, false, false)
    }

    private def addNetworkKindActions(Kind netKind, config) {

        log.info("Adding the network kind's actions")

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('up')){
            log.info "Network up action enabled"

            def upAction = createAction(UpNetworkAction.term, UpNetworkAction.scheme, 'Up Network Action')
            netKind.addToActions upAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('down')){

            log.info "Network down action enabled"
            def downAction = createAction(DownNetworkAction.term, DownNetworkAction.scheme, 'Down Network Action')
            netKind.addToActions downAction
        }
    }

    public Mixin addIPNetworkMixin(config) {

        log.info("Creating the IP network kind. Associating it with the Network kind.")
        def ipMixin = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)

        if(!ipMixin){

            def networkKind = createNetworkKind()

            ipMixin = createMixin(IPNetworkMixin.term, IPNetworkMixin.scheme, "An IP Networking Mixin",
                    config.defaults.location ?: '/network', networkKind)

            //add attributes
            createAttributeType(ipMixin, IPNetworkMixin.occi_network_address, AttributeType.STRING,
                        config.defaults.networkaddress ?: "",
                        true, true, false)

            createAttributeType(ipMixin, IPNetworkMixin.occi_network_gateway, AttributeType.STRING,
                        config.defaults.networkgateway ?: "",
                        true, true, false)

            createAttributeType(ipMixin, IPNetworkMixin.occi_network_allocation, AttributeType.STRING,
                        config.defaults.networkallocation ?: "dynamic",
                        true, true, false)

            ipMixin.save()
            checkErrors(ipMixin)
        }

        return ipMixin
    }
    
// ---------- Storage ----------
    public Kind createStorageKind(config=CH.config.eu.slasoi.infrastructure.types.storage){

        log.info("Creating the storage kind")
        def storageKind = Kind.findByTermAndScheme(Storage.term, Storage.scheme)

        if(!storageKind){
            def resourceKind = createResourceKind()

            storageKind = new Kind(term: Storage.term, scheme: Storage.scheme, title: 'Storage type',
                    location: config.defaults.location ?: '/storage')
            storageKind.addToRelated resourceKind
            storageKind.save()
            checkErrors(storageKind)
            
            addStorageKindAttributes(storageKind, config)
            addStorageKindActions(storageKind, config)

            storageKind.save()
            checkErrors(storageKind)
        }

        return storageKind
    }

    private def addStorageKindAttributes(Kind storageKind, config) {

        log.info("Adding the storage kind's attributes")

        createAttributeType(storageKind, Storage.occi_storage_size, AttributeType.FLOAT,
                    config.defaults.size ?: "0.0",
                    true, true, false)

        createAttributeType(storageKind, Storage.occi_storage_state, AttributeType.STRING,
                    "${Storage.occi_storage_state_offline}",
                    true, true, false)
    }

    private def addStorageKindActions(Kind storageKind, config){

        log.info("Adding the storage kind's actions")

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('online')){
            log.info "Storage online action enabled"
            def onlineAction = createAction(OnlineStorageAction.term, OnlineStorageAction.scheme, 'Online Storage Action')
            storageKind.addToActions onlineAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('offline')){
            log.info "Storage offline action enabled"
            def offlineAction = createAction(OfflineStorageAction.term, OfflineStorageAction.scheme, 'Offline Storage Action')
            storageKind.addToActions offlineAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('backup')){
            log.info "Storage backup action enabled"
            def backupAction = createAction(BackupStorageAction.term, BackupStorageAction.scheme, 'Backup Storage Action')
            storageKind.addToActions backupAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('snapshot')){
            log.info "Storage snapshot action enabled"
            def snapshotAction = createAction(SnapshotStorageAction.term, SnapshotStorageAction.scheme, 'Snapshot Storage Action')
            storageKind.addToActions snapshotAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('resize')){
            log.info "Storage resize action enabled"
            def resizeAction = createAction(ResizeStorageAction.term, ResizeStorageAction.scheme, 'Resize Storage Action')
            createAttributeType(resizeAction, ResizeStorageAction.size, AttributeType.FLOAT, "",
                                        true, true, false)
            
            storageKind.addToActions resizeAction
        }

        if(config.defaults.actions.contains('all') || config.defaults.actions.contains('degrade')){
            log.info "Storage degrade action enabled"
            def degradeAction = createAction(DegradeStorageAction.term, DegradeStorageAction.scheme, 'Degrade Storage Action')
            storageKind.addToActions degradeAction
        }
    }

// ----------- Link ------------
    public Kind createNetworkLinkKind(config=CH.config.eu.slasoi.infrastructure.types.networkinterface){

        log.info("Creating the network interface link")
        def networkLinkKind = Kind.findByTermAndScheme(NetworkLink.term, NetworkLink.scheme)

        if(!networkLinkKind){
            
            Kind linkKind = createLinkKind()

            networkLinkKind = new Kind(term: NetworkLink.term, scheme: NetworkLink.scheme, title:'A network interface link',
                    location: config.defaults.location ?: '/link')
            networkLinkKind.addToRelated linkKind
            networkLinkKind.save()
            checkErrors(networkLinkKind)

            addNetworkLinkAttributes(networkLinkKind, config)

            networkLinkKind.save()
            checkErrors(networkLinkKind)
        }

        return networkLinkKind
    }

    private addNetworkLinkAttributes(Kind networkLinkKind, config){

        createAttributeType(networkLinkKind, NetworkLink.occi_networkinterface_interface, AttributeType.STRING,
                    config.defaults.iface ?: "eth0",
                    true, true, false)

        createAttributeType(networkLinkKind, NetworkLink.occi_networkinterface_mac, AttributeType.STRING,
                    config.defaults.mac ?: "00:00:00:00:00:00",
                    true, true, false)

        createAttributeType(networkLinkKind, NetworkLink.occi_networkinterface_state, AttributeType.STRING,
                    "${NetworkLink.occi_networkinterface_state_inactive}",
                    true, false, false)
    }

    public Kind createStorageLinkKind(config){

        log.info("Creating the storage link")
        def storageLinkKind = Kind.findByTermAndScheme(StorageLink.term, StorageLink.scheme)
        
        if(!storageLinkKind){

            def linkKind = createLinkKind()

            storageLinkKind = new Kind(term: StorageLink.term, scheme: StorageLink.scheme, title:'A storage link',
                    location: config.defaults.location ?: '/link')
            storageLinkKind.addToRelated linkKind
            storageLinkKind.save()
            checkErrors(storageLinkKind)
            
            addStorageLinkAttributes(storageLinkKind, config)

            storageLinkKind.save()
            checkErrors(storageLinkKind)
        }
        return storageLinkKind
    }

    private addStorageLinkAttributes(Kind storageLinkKind, config){

        createAttributeType(storageLinkKind, StorageLink.occi_storagelink_deviceid, AttributeType.STRING,
                    config.defaults.devid ?: "NONE",
                    true, true, false)

        createAttributeType(storageLinkKind, StorageLink.occi_storagelink_mountpoint, AttributeType.STRING,
                    config.defaults.mountpoint ?: "",
                    true, true, false)

        createAttributeType(storageLinkKind, StorageLink.occi_storagelink_state, AttributeType.STRING,
                    "${StorageLink.occi_storagelink_state_inactive}",
                    true, false, false)
    }

    public Mixin addIPNetworkLinkMixin(config){

        log.info("Creating the IP network link kind. Associating it with the network link kind.")
        def ipNetworkLinkMixin = Mixin.findByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)

        if(!ipNetworkLinkMixin){

            def networkLinkKind = createNetworkLinkKind()
            ipNetworkLinkMixin = createMixin(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme, "An IP Network Interface Mixin",
                    config.defaults.location ?: '/link', networkLinkKind)

            //add attributes
            createAttributeType(ipNetworkLinkMixin, IPNetworkInterfaceMixin.occi_networkinterface_address, AttributeType.STRING,
                    config.defaults.networkaddress ?: "",
                    true, true, false)

            createAttributeType(ipNetworkLinkMixin, IPNetworkInterfaceMixin.occi_networkinterface_gateway, AttributeType.STRING,
                    config.defaults.networkgateway ?: "",
                    true, true, false)

            createAttributeType(ipNetworkLinkMixin, IPNetworkInterfaceMixin.occi_networkinterface_allocation, AttributeType.STRING,
                    config.defaults.networkallocation ?: "dynamic",
                    true, true, false)

            ipNetworkLinkMixin.save()
            checkErrors(ipNetworkLinkMixin)
        }

        return ipNetworkLinkMixin
    }
    
// --------- Templates ---------
    public Mixin createOsTemplateMixin(){

        def osTpl = Mixin.findByTermAndScheme(OsTemplateMixin.term, OsTemplateMixin.scheme)

        if(!osTpl){
            def compKind = createComputeKind()
            osTpl = createMixin(OsTemplateMixin.term, OsTemplateMixin.scheme, "An OS Template Mixin", '', compKind)
        }

        return osTpl
    }

    public createOsMixins(){
        
        def os_tpls = CH.config.eu.slasoi.infrastructure.templates.os

        def compKind = createComputeKind()

        os_tpls.each { tpl ->

            def os_tpl = Mixin.findByTermAndScheme(tpl['term'], tpl['scheme'])
            if(!os_tpl){
                os_tpl = createMixin(tpl['term'], tpl['scheme'], tpl['title'] ?: '', tpl['location'], compKind)
                os_tpl.addToRelated createOsTemplateMixin()
                os_tpl.save()
                checkErrors(os_tpl)
            }
        }
    }

    public Mixin createResourceTemplateMixin(){

        def resourceKind = createResourceKind()
        return createMixin(ResourceTemplateMixin.term, ResourceTemplateMixin.scheme, "A Resource Template Mixin", '/resource_tpl', resourceKind)
    }

// --------- SLA@SOI Specifics ---------

    public Kind createServiceKind(config){

        log.info("Creating the service kind")
        def serviceKind = Kind.findByTermAndScheme(Service.term, Service.scheme)

        if(!serviceKind){

            def resourceKind = createResourceKind()

            serviceKind = new Kind(term: Service.term, scheme: Service.scheme, title: 'Service type',
                    location: config.defaults.location ?: '/service')
            serviceKind.addToRelated resourceKind
            serviceKind.save()
            checkErrors(serviceKind)

            //Add attributes
            createAttributeType(serviceKind, Service.eu_slasoi_infrastructure_service_name, AttributeType.STRING,
                    "NONE", true, true, false)

            createAttributeType(serviceKind, Service.eu_slasoi_infrastructure_service_monitoringconfig, AttributeType.STRING,
                    "NONE",true, true, false)

            createAttributeType(serviceKind, Service.eu_slasoi_infrastructure_service_state, AttributeType.STRING,
                    "${Service.eu_slasoi_infrastructure_service_state_inactive}",
                    true, false, false)

            //Add actions
            if(config.defaults.actions.contains('all') || config.defaults.actions.contains('stop')){

                def stopAction = createAction(StopServiceAction.term, StopServiceAction.scheme, 'A Stop Service Action')
                serviceKind.addToActions stopAction
            }

            if(config.defaults.actions.contains('all') || config.defaults.actions.contains('start')){
                def startAction = createAction(StartServiceAction.term, StartServiceAction.scheme, 'A Start Service Action')
                serviceKind.addToActions startAction
            }

            if(config.defaults.actions.contains('all') || config.defaults.actions.contains('restart')){
                def restartAction = createAction(RestartServiceAction.term, RestartServiceAction.scheme, 'A Restart Service Action')
                serviceKind.addToActions restartAction
            }

            if(config.defaults.actions.contains('all') || config.defaults.actions.contains('suspend')){
                def suspendAction = createAction(SuspendServiceAction.term, SuspendServiceAction.scheme, 'A Suspend Service Action')
                serviceKind.addToActions suspendAction
            }

            serviceKind.save()
            checkErrors(serviceKind)
        }

        return serviceKind
    }

    public Mixin addSlaSoiComputeMixin(config){

        log.info("Creating the SLA@SOI compute mixin")
        def slaSoiComputeMixin = Mixin.findByTermAndScheme(SlaSoiComputeMixin.term, SlaSoiComputeMixin.scheme)

        if(!slaSoiComputeMixin){
            
            def computeKind = Kind.findByTermAndScheme(Compute.term, Compute.scheme) ?: createComputeKind()

            slaSoiComputeMixin = createMixin(SlaSoiComputeMixin.term, SlaSoiComputeMixin.scheme, "A SLA@SOI specific compute",
                    config.defaults.location ?: '/compute', computeKind)

            //add attributes
            def attr = AttributeType.findByName(SlaSoiComputeMixin.eu_slaatsoi_physicalhost_id) ?:
                new AttributeType(name: SlaSoiComputeMixin.eu_slaatsoi_physicalhost_id, type: AttributeType.INT,
                    defaultValueExpr: "-1",
                    renderme: true, mutable: true, required: false)
            attr.save()
            checkErrors(attr)
            slaSoiComputeMixin.addToAttributes attr

            attr = AttributeType.findByName(SlaSoiComputeMixin.eu_slaatsoi_virtualhost_id) ?:
                new AttributeType(name: SlaSoiComputeMixin.eu_slaatsoi_virtualhost_id, type: AttributeType.INT,
                    defaultValueExpr: "-1",
                    renderme: true, mutable: true, required: false)
            attr.save()
            checkErrors(attr)
            slaSoiComputeMixin.addToAttributes attr

            attr = AttributeType.findByName(SlaSoiComputeMixin.eu_slaatsoi_imageIdentifier) ?:
                new AttributeType(name: SlaSoiComputeMixin.eu_slaatsoi_imageIdentifier, type: AttributeType.STRING,
                    defaultValueExpr: "tashi-xxx.img",
                    renderme: true, mutable: true, required: false)
            attr.save()
            checkErrors(attr)
            slaSoiComputeMixin.addToAttributes attr

            slaSoiComputeMixin.save()
        }

        return slaSoiComputeMixin
    }*/
}

