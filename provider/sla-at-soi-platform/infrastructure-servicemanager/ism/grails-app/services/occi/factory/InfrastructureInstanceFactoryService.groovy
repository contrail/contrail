/*
SVN FILE: $Id: InfrastructureInstanceFactoryService.groovy 2770 2011-07-22 14:37:13Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2770 $
@lastrevision   $Date: 2011-07-22 16:37:13 +0200 (pet, 22 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/services/occi/factory/InfrastructureInstanceFactoryService.groovy $
*/

package occi.factory

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.exceptions.IsmHttpException
import eu.slasoi.infrastructure.model.Reservation
import eu.slasoi.infrastructure.model.Service
import occi.infrastructure.mixins.OsTemplateMixin
import occi.infrastructure.mixins.ResourceTemplateMixin
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException
import occi.core.*
import occi.infrastructure.*
import occi.core.Mixin
import occi.core.Category
import eu.slasoi.infrastructure.services.AdmissionService
import org.springframework.context.ApplicationContextAware
import org.apache.catalina.core.ApplicationContext
import org.springframework.beans.factory.NoSuchBeanDefinitionException

//This class to be tested via an integration test
//Normally the type info is setup in Bootstrap.groovy but
// for the purposes of testing this should be done in the setup() method
class InfrastructureInstanceFactoryService {

    static transactional = true

    InfrastructureTypeFactoryService infrastructureTypeFactoryService
    def multipartService
    
    ApplicationContext applicationContext
    def admissionServiceBean
    def grailsApplication


    //this is required rather than grails DI, as there's a circular dependency between this and admissionservice
    def private AdmissionService getAdmissionService() throws IsmHttpException {
        
        def bean
        
        try{
            bean = (AdmissionService)grailsApplication.mainContext.getBean('admissionService')
        }
        catch (NoSuchBeanDefinitionException nbdf){
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, "The Admission Service could not be found. The spring bean that implements it is not present in the application context.")
        }
        
        return bean
    }

    public Compute createCompute(HashMap parsedAttrs) throws IsmHttpException {

        //TODO First ensure the resources are available and decrement accordingly
        //admissionService.decrementResources(parsedAttrs)

        def mixins = []
        def cats = parsedAttrs[OcciParser.occi_categories][0]

        //TODO remove string literals
        def x = cats.findAll{
            it['occi.core.class'] == 'mixin'
        }

        x.each{
            def mixin = Mixin.findByTermAndScheme(it['occi.core.term'], it['occi.core.scheme'])
            if (mixin) {
                mixins.add mixin
            }
        }

        def compAttrs = parsedAttrs[OcciParser.occi_attributes][0] ?: [:]
        def comp = createComputeI(compAttrs, mixins)

        return comp
    }

    public Compute createComputeI(computeAttributes=null, mixins=[]) throws IsmHttpException{

        //1 Get the compute kind
        //2 Create attributes based on what is defined in the instance's Kind
        //  If no value is supplied for an expected attribute, set a default
        //3 associate each attribute with its type
        //4 save!

        def computeKind = Kind.findBySchemeAndTerm(Compute.scheme, Compute.term)

        if(!computeKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The compute kind was not found. Has it been declared in the ISM configuration file?')

        Compute compute

        if(computeAttributes[Compute.occi_compute_hostname]?.length() <= 0)
            computeAttributes[Compute.occi_compute_hostname] = UUID.randomUUID().toString()

        if(computeAttributes[Entity.occi_core_id]?.length() > 0)
            compute = new Compute(o_id: computeAttributes[Entity.occi_core_id], kind: computeKind)
        else
            compute = new Compute(o_id: UUID.randomUUID().toString(), kind: computeKind)

        compute.save()
        checkErrors(compute)

        addMixins(mixins, compute)
        addAttributesValues(computeAttributes, compute)

        applyResourceTemplate(compute)

        compute.save()
        checkErrors(compute)

        return compute
    }

    public deleteCompute(comp){
        
        if(comp.owningService){
            log.info "This compute is associated with a service and so will be unlinked first."
            unlink(comp.owningService, comp)
            comp.owningService.removeFromResources comp
        }
        comp.delete()
    }
    
    //TODO refactor - this is a hack!
    private def applyResourceTemplate(Compute compute) {

        def templates = compute.mixins.findAll {
            //TODO remove literal
            it.scheme == 'http://sla-at-soi.eu/occi/infrastructure/res_template#'
        }

        if (templates.size() == 0)
            return
        
        if (templates.size() > 1)   
            throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, 'There was more than one resource template supplied in the request.')

        if (templates.size() == 1) {

            templates = templates.toList()
            templates[0].attributes.each {

                if (it.name == "${templates[0].term}.${Compute.occi_compute_memory}"){
                    compute.getAttrValue(Compute.occi_compute_memory).value = it.defaultValue
                    def attrVal = compute.getAttrValue("${templates[0].term}.${Compute.occi_compute_memory}")
                    compute.removeFromAttrvals attrVal
                    attrVal.delete()
                }

                else if (it.name == "${templates[0].term}.${Compute.occi_compute_cores}"){
                    compute.getAttrValue(Compute.occi_compute_cores).value = it.defaultValue
                    def attrVal = compute.getAttrValue("${templates[0].term}.${Compute.occi_compute_cores}")
                    compute.removeFromAttrvals attrVal
                    attrVal.delete()
                }

                else if (it.name == "${templates[0].term}.${Compute.occi_compute_speed}"){
                    compute.getAttrValue(Compute.occi_compute_speed).value = it.defaultValue
                    def attrVal = compute.getAttrValue("${templates[0].term}.${Compute.occi_compute_speed}")
                    compute.removeFromAttrvals attrVal
                    attrVal.delete()
                }
            }
            def attrVal = compute.getAttrValue("${templates[0].term}.occi.available")
            compute.removeFromAttrvals attrVal
            attrVal.delete()
        }

        compute.save(flush:true)
        checkErrors(compute)
    }

    public Network createNetwork(networkAttributes=null, mixins=[]) throws IsmHttpException{

        def networkKind = Kind.findBySchemeAndTerm(Network.scheme, Network.term)

        if(!networkKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The network kind was not found. Has it been declared in the ISM configuration file?')

        Network network
        if(networkAttributes[Entity.occi_core_id]?.length() > 0)
            network = new Network(o_id: UUID.randomUUID().toString(), kind: networkKind)
        else
            network = new Network(o_id: UUID.randomUUID().toString(), kind: networkKind)

        network.save()
        checkErrors(network)

        addMixins(mixins, network)
        addAttributesValues(networkAttributes, network)

        network.save()
        checkErrors(network)

        return network
    }

    public Storage createStorage(storageAttributes=null, mixins=[]) throws IsmHttpException{
        
        def storageKind = Kind.findBySchemeAndTerm(Storage.scheme, Storage.term)

        if(!storageKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The storage kind was not found. Has it been declared in the ISM configuration file?')

        Storage storage
        if(storageAttributes[Entity.occi_core_id]?.length() > 0)
            storage = new Storage(o_id: storageAttributes[Entity.occi_core_id], kind: storageKind)
        else
            storage = new Storage(o_id: UUID.randomUUID().toString(), kind: storageKind)


        storage.save()
        checkErrors(storage)

        addMixins(mixins, storage)
        addAttributesValues(storageAttributes, storage)

        storage.save()
        checkErrors(storage)

        return storage
    }

    public def createService(occiContent, String serviceId) throws IsmHttpException {
        //1. validate that the service category is present in one of the parts
            //2. for each resource supplied verify that the supplied IDs are not known to the system
            //3. create the two computes
            //4. create the service object
            //5. execute the service against infrastructure

            def comps = []
            def svcAttrs = [:]
            def validSvcReq = false

            occiContent.each{ c ->

                def parsedAttrs

                //ensure it is only ever XML by agreement
                def monXML = ""
                if(c.contains('<') && (c.contains('/>') || c.contains('</'))){

                    log.debug "The service content contains XML. Assuming this to be the monitoring configuration."

                    //strips out the XML that is the monitoring config
                    def lines = c.tokenize("\n\r\f")
                    c = ""
                    
                    lines.each {
                        if(it.startsWith('Category') || it.startsWith('Link') ||
                           it.startsWith('X-OCCI-Attribute') || it.startsWith('X-OCCI-Location')){
                            //its OCCI content
                            c += it + "\r\n"
                        }
                        else{
                            //its XML
                            monXML += it
                        }
                    }

//                    log.debug "Have seperated OCCI and Monitoring Content"
//                    log.debug "OCCI content: ${c}"
//                    log.debug "Monitoring content: ${monXML}"
                }

                try{
                    parsedAttrs = OcciParser.getParser(c).headers()
                }catch (OcciParserException pe){
                    throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, pe.toString())
                }

                //First ensure the resources are available and decrement accordingly
                admissionService.decrementResources(parsedAttrs)
                
                if(containsCategory(parsedAttrs, Service.term, Service.scheme)){

                    svcAttrs = parsedAttrs

                    if(serviceId || serviceId?.length() > 0)
                        parsedAttrs[OcciParser.occi_attributes][0][Entity.occi_core_id] = serviceId

                    if(monXML.length() > 0){
                        log.info "Setting monitoring configuration to received XML found in body."
                        if(monXML.endsWith("\n"))
                            parsedAttrs[OcciParser.occi_attributes][0]['eu.slasoi.infrastructure.service.monitoring.config'] = monXML[0..-2]
                        else if (monXML.endsWith("\r\n"))
                            parsedAttrs[OcciParser.occi_attributes][0]['eu.slasoi.infrastructure.service.monitoring.config'] = monXML[0..-3]
                        else
                            parsedAttrs[OcciParser.occi_attributes][0]['eu.slasoi.infrastructure.service.monitoring.config'] = monXML
                    }


                    validSvcReq = true
                }
                
                if(containsCategory(parsedAttrs, Compute.term, Compute.scheme)){

                    //extract supplied occi.core.id and ensure it's unique
                    def compAttrs = parsedAttrs[OcciParser.occi_attributes][0] ?: [:]
                    if(compAttrs[Entity.occi_core_id]){
                        def comp = Compute.findByO_id(compAttrs[Entity.occi_core_id] as String)
                        if(comp){
                            //clear the comps temp array as we're in a roll back condition
                            if(comps.size() > 0){
                                //remove these
                                comps.each{
                                    it.delete()
                                }
                            }

                            log.error "A compute supplied with the occi.core.id = ${compAttrs[Entity.occi_core_id]} already exists. Service request failed."
                            throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "A compute supplied with the occi.core.id = ${compAttrs[Entity.occi_core_id]} already exists. Service request failed.")
                        }
                    }

                    Compute comp = createCompute(parsedAttrs)

                    if(!comp){
                        log.error "There was a problem creating the compute:\n${parsedAttrs}"
                        throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, "There was a problem creating the compute:\n${parsedAttrs}")
                    }
                    else{
                        comps.add comp
                    }
                }
            }

            Service svc = null
            if(!validSvcReq){
                log.error "There was no service category in the request."
                throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "There was no service category in the request.")
            }
            else{

                svc = createServiceI(comps, svcAttrs[OcciParser.occi_attributes][0] ?: [:])

                if(!svc){
                    log.error "There was a problem creating the service:\n${svcAttrs}"
                    throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, "There was a problem creating the service:\n${svcAttrs}")
                }
            }
            return svc
    }

    private Service createServiceI(resources, attributes) throws IsmHttpException{

        def svcKind = Kind.findBySchemeAndTerm(Service.scheme, Service.term)

        if(!svcKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The service kind was not found. Has it been declared in the ISM configuration file?')
        
        def service
        if(attributes[Entity.occi_core_id]?.length()>0)
            service = new Service(o_id: attributes[Entity.occi_core_id], kind: svcKind, resources: new ArrayList())
        else
            service = new Service(o_id: UUID.randomUUID().toString(), kind: svcKind, resources: new ArrayList())

        service.save()
        checkErrors(service)
        
        resources.each{ res ->
            service.addToResources res
            res.owningService = service

            link(service, res)
        }

        service.save()
        checkErrors(service)
        
        addAttributesValues(attributes, service)

        service.save()
        checkErrors(service)

        return service
    }

    public deleteService(Service s){

        s.resources.each { res ->
            unlink(s, res)
            admissionService.incrementResource(res)
        }

        s.delete(flush: true)
    }

    public Reservation createReservation(String provisionRequest, resources, resId=null) throws IsmHttpException{

        //decrement of resources here
        resources.each {
            admissionService.decrementResources(it)
        }

        def resKind = Kind.findBySchemeAndTerm(Reservation.scheme, Reservation.term)

        if(!resKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The reservation kind was not found. Has it been declared in the ISM configuration file?')

        if(resId && Reservation.findByO_id(resId))
            throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "The client specified reservation ID ${resId} already exists on the system")

        def res = new Reservation(o_id: resId ?: UUID.randomUUID().toString(), kind: resKind, provisioningRequest: provisionRequest, dateCreated: new Date())

        res.save()
        checkErrors(res)

        def attrs = [:]
        def leaseTime = CH.config.eu.slasoi.infrastructure.provisioning.leaseKillTime
        attrs[Reservation.eu_slasoi_infrastructure_reservation_leaseCreated] = res.dateCreated.getTime()
        attrs[Reservation.eu_slasoi_infrastructure_reservation_leasePeriod] = leaseTime
        addAttributesValues(attrs, res)

        return res
    }

    public def deleteReservation(resvId) {

        def resv = Reservation.findByO_id(resvId)
        def rawContent, multipart
        if (resv) {
            rawContent = resv.provisioningRequest
            multipart = resv.isMultipart
            resv.delete(flush: true)
        }
        else
            throw new IsmHttpException(HttpURLConnection.HTTP_NOT_FOUND, "No reservation with id of ${resvId} was found")

        log.info "Number of Reservations after removal of lease ${resvId}: ${Reservation.count()}"

        def occiContent = []
        if (multipart) {
            def boundary = rawContent.split('\r\n')[0][2..-1]
            occiContent = multipartService.extractContent(rawContent, boundary)
        }
        else
            occiContent.add rawContent

        //filter only resources with resource templates associated
        occiContent = occiContent.findAll {
            it.contains('http://sla-at-soi.eu/occi/infrastructure/res_template#')
        }

        //move to infrastructureInstanceFactoryService
        occiContent.each {
            def parsedAttrs = OcciParser.getParser(it).headers()
            admissionService.incrementResources(parsedAttrs)
        }
    }

    public Link link(Resource source, Resource target, attributes=null, mixins=null){

        def linkKind = infrastructureTypeFactoryService.createLinkKind()
        return createLink(source, target, linkKind, attributes, mixins)
    }

    public def unlink (Resource source, Resource target){
        
        def link = source.links.find { lnk ->

            lnk.tar_get.o_id == target.o_id
        }

        if(link){
            source.removeFromLinks link
            link.delete(flush:true)
        }
        else{
            log.error "Link to remove was not found."
        }
    }
    
    public Link linkStorage(source, target, attributes, mixins=null) throws IsmHttpException{

        def linkKind = Kind.findBySchemeAndTerm(StorageLink.scheme, StorageLink.term)

        if(!linkKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The link kind was not found. Has it been declared in the ISM configuration file?')

        return createLink(source, target, linkKind, attributes, mixins)
    }

    public Link linkNetwork(source, target, attributes, mixins=null) throws IsmHttpException{

        def linkKind = Kind.findBySchemeAndTerm(NetworkLink.scheme, NetworkLink.term)

        if(!linkKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The link kind was not found. Has it been declared in the ISM configuration file?')

        return createLink(source, target, linkKind, attributes, mixins)
    }

    public Mixin createOsTemplate(term, scheme, title) throws IsmHttpException{

        def osTemplateMixin = Mixin.findBySchemeAndTerm(OsTemplateMixin.scheme, OsTemplateMixin.term)

        if(!osTemplateMixin)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The OS Template Mixin was not found. Has it been declared in the ISM configuration file?')

        def applicableKind = Kind.findByTermAndScheme(Compute.term, Compute.scheme)

        if(!applicableKind)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The compute (applicable) kind was not found. Has it been declared in the ISM configuration file?')

        def os = new Mixin(term: term, scheme: scheme, title: title, location: '/os_tpls/', applicableKind: applicableKind)
        os.addToRelated osTemplateMixin

        os.save()
        checkErrors(os)

        return os
    }

    public Mixin createResourceTemplate(term, scheme, title, applicableKind, resourceTemplateAttrs) throws IsmHttpException{

        def resourceTemplateMixin = Mixin.findByTermAndScheme(ResourceTemplateMixin.term, ResourceTemplateMixin.scheme)

        if(!resourceTemplateMixin)
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'The Resource Template Mixin was not found. Has it been declared in the ISM configuration file?')

        def resource = new Mixin(term: term, scheme: scheme, title: title, location: '/resource_tpl/', applicableKind: applicableKind)
        resource.addToRelated resourceTemplateMixin

        resourceTemplateAttrs?.each {
            resource.addToAttributes it
        }

        resource.save()
        checkErrors(resource)

        return resource
    }

    private Link createLink(source, target, linkKind, attributes, mixins){

        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: source, tar_get: target, kind: linkKind)

        link.save()
        checkErrors(link)

        attributes = attributes ?: [:]
        attributes[Link.occi_link_source] = "${source.kind.location}${source.o_id}"
        attributes[Link.occi_link_target] = "${target.kind.location}${target.o_id}"

        addMixins(mixins, link)
        addAttributesValues(attributes, link)
        
        link.save()
        checkErrors(link)

        source.addToLinks link
        source.save()
        checkErrors(source)

        return link
    }

    def addAttrVals(Category kind, Entity specialisedResource, attributes){
        
        if(!kind)
            return null

        //log.info "Adding attribute values for ${kind.scheme}${kind.term}"

        kind.attributes.each{

            def val = attributes[it.name]

            if(val){
                val = val.toString()
                attributes?.remove(it.name)
            }
            else
                val = it.defaultValue

            AttributeValue attrVal = new AttributeValue(
                                            value: val,
                                            entity: specialisedResource,
                                            attributeInfo: it)
            attrVal.save()
            checkErrors(attrVal)
            specialisedResource.addToAttrvals attrVal
        }

        if(kind instanceof Kind || kind instanceof Mixin)    
            kind.related.each {
                addAttrVals(it, specialisedResource, attributes)
            }

//        specialisedResource.mixins.each {
//            addAttrVals(it, specialisedResource, attributes)
//        }
    }

    //refactor try not to rely on this class wide var
    def required = []
    private def requiredAttrs(Kind kind){

        if(!kind)
            return null

        def req = kind.attributes.findAll {
            //log.info "is ${it.name} required: " + it.required
            it.required
        }

        required = required + req

        kind.related.each {
            requiredAttrs(it)
        }
    }

    private def addAttributesValues(attributes, Entity specialisedResource) {

        //add default values
        //check that:
        //  the supplied attribute list is not missing required attributes
        //  the supplied attribute can be associated based on the target and super kinds
        //  the supplied attribute list does not contain attributes that are immutable

        requiredAttrs(specialisedResource.kind)

        required.each { reqAttr ->
            if(!attributes.containsKey(reqAttr.name)){
                log.error "Required attribute missing. The client must specify ${reqAttr.name}"
                throw new Exception ("Required attribute missing. The client must specify ${reqAttr.name}")
            }
        }

        if (attributes?.size() > 0) {
            attributes.each { key, val ->

                AttributeType attribute = AttributeType.findByName(key)

                if(attribute){
//                    if(attribute.category.term != specialisedResource.kind.term &&
//                        attribute.category.scheme != specialisedResource.kind.scheme){
//                        log.error "Supplied attribute (${key} : ${val}) is not applicable for the type (${specialisedResource.kind.scheme}${specialisedResource.kind.term})"
//                    }


                    if(!attribute.mutable){
                        log.error "Attempting to modify a server side managed attribute (${attribute.name}). It is read-only for clients."
                        throw new Exception ("Attempting to modify a server side managed attribute (${attribute.name}). It is read-only for clients.")
                    }
                }
                else{
                    log.error "An unknown attribute was supplied: ${key} : ${val}"
                    throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "An unknown attribute was supplied: ${key} : ${val}")
                }
            }
        }

        //add supplied ID - either user supplied (PUT) or server generated (POST)
        if(specialisedResource.o_id)
            attributes[Entity.occi_core_id] = specialisedResource.o_id

        addAttrVals(specialisedResource.kind, specialisedResource, attributes)

        specialisedResource.mixins.each {
            addAttrVals(it, specialisedResource, attributes)
        }

        required = []
    }
    
    //if there are mixins
    //  those mixins are resource templates
    //      apply the values contained
    //TODO if the mixin is a resource template then those values take higher precedence
    //TODO if a mixin is associated with the entity, their attribute default values must be added
    private def addMixins(mixins, resource){

        def osPresent = false

        if(mixins?.size() > 0)
            mixins.each { mixin ->
                
                resource.addToMixins mixin
                mixin.addToEntities resource

                //if the mixin has values, associate them to the resource
//                mixin.attributes.each{
//                    AttributeValue attrVal = new AttributeValue(
//                                            value: it.defaultValue,
//                                            entity: resource,
//                                            attributeInfo: it)
//                    attrVal.save()
//                    checkErrors(attrVal)
//                    resource.addToAttrvals attrVal
//                }
                
                if (mixinIsOs(mixin))
                    osPresent = true
            }

        if (!osPresent && resource instanceof Compute)
            addDefaultOsMixin(mixins, resource)
    }

    private def mixinIsOs(Mixin mixin){

        def res = mixin.related.find{it.term == OsTemplateMixin.term && it.scheme == OsTemplateMixin.scheme}
        if(res) return true
        else return false
    }

    private def addDefaultOsMixin(mixins, resource){

        def defOs = CH.config.eu.slasoi.infrastructure.provisioning.defaultOS?.split('#')

        if(defOs.size() == 2){
            def defaultOs = Mixin.findBySchemeAndTerm(defOs[0]+'#', defOs[1])
            if(defaultOs){
                mixins.add defaultOs
                defaultOs.addToEntities resource
            }

            else
                log.error "The system default OS to provision could not be found"
        }
        else
            log.error "Default OS is not specified in the config file. No OS will be provisioned."
    }

    private checkErrors(persistedObject){

        if(persistedObject.hasErrors()) {
            log.error("There was an error persisting an object")
            def err = ''
            persistedObject.errors.each {
                err += it
                log.error(it)
            }
            throw new Exception("There was an error persisting an object: " + err)
        }
    }

    def Compute updateAttributes(comp, attrs){

        attrs.each{key, val ->

            //confirm that those attributes are already part of the resource
            def k_attr = comp.hasAttr(key)

            //execute changes
            if(k_attr){
                def attrval = comp.getAttrValue(key)
                attrval.value = attrs[key]
                attrval.save()
            }
            else{
                log.warn "Unknown attribute name/value supplied: ${key} : ${val}"
            }
        }
        //as we're updating set the compute instance state to inactive until changes are made on the backend
        def val = comp.getAttrValue(Compute.occi_compute_state)
        val.value = Compute.occi_compute_state_inactive
        val.save()
        comp.save()
        return comp
    }

    def Compute updateType(compute, parsedCats) throws IsmHttpException {

        //disallow the addition of kinds - it's ok to supply the target instance's kind
        if(parsedCats.findAll {
            it['occi.core.class'] == 'kind' && (it['occi.core.term'] != compute.kind.term && it['occi.core.term'] != compute.kind.scheme)
        }.size() > 0){
            compute = null
            log.error "Differing kind supplied. An instance cannot change its assigned kind."
            throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "Differing kind supplied. An instance cannot change its assigned kind.")
        }
        else{ //allow the addition of mixins
            def newMixins = parsedCats.findAll{
                it['occi.core.class'] == 'mixin'
            }

            //if adding a mixin it might have mandatory attributes
            if(newMixins.size() > 0){
                log.info "adding new capabilities via mixin"
                newMixins.each { mixin ->
                    def mxn = Mixin.findByTermAndScheme(mixin['occi.core.term'], mixin['occi.core.scheme'])
                    if(mxn){
                        compute.addToMixins mxn
                        mxn.addToEntities compute
                        mxn.save()
                    }
                    else{
                        compute = null
                        log.error "Cannot add the requested mixin. The requested mixin was not found."
                        throw new IsmHttpException(HttpURLConnection.HTTP_NOT_FOUND, "Cannot add the requested mixin. The requested mixin was not found.")
                    }
                }
                compute?.save()
            }
            else{
                log.info "No mixins found to add - why am I here?"
            }
        }
        return compute
    }

    //probably should be moved into TypeFactoryService
    public def containsCategory(attrs, term='', scheme=''){
        //ensure that categories contain at least the compute category
        def cats = attrs[OcciParser.occi_categories][0]
        def x = cats.findAll{
            if(term == '')
                it['occi.core.scheme'] == scheme
            else if (scheme == '')
                it['occi.core.term'] == term
            else
                (it['occi.core.term'] == term) && (it['occi.core.scheme'] == scheme)
        }

        if(x.size() == 1)
            return true
        else
            return false
    }
}
