package eu.slasoi.infrastructure.services

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.LeaseRemovalEvent
import eu.slasoi.infrastructure.LeaseRemovalTask
import eu.slasoi.infrastructure.exceptions.IsmHttpException
import eu.slasoi.infrastructure.model.Reservation
import eu.slasoi.infrastructure.model.Service
import occi.core.AttributeType
import occi.infrastructure.Compute
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException
import org.springframework.context.ApplicationListener

class ReservationService implements ApplicationListener<LeaseRemovalEvent>{

    static transactional = true
    def leaseTimers = [:]

    def infrastructureInstanceFactoryService
    def computeService
    def multipartService
    def grailsApplication

    def reserve(content, resources, resvId=null) throws IsmHttpException{

        def resv = infrastructureInstanceFactoryService.createReservation(content, resources, resvId)

        def defaultLeaseTime = {
                log.warn "Returning the default lease time (60 secs) - none specified in the configfile (eu.slasoi.infrastructure.provisioning.leaseKillTime)"
                return 60
        }

        //TODO use the default lease time contained in the Reservation Resource Type??
        def leaseTime = CH.config.eu.slasoi.infrastructure.provisioning.leaseKillTime ?: defaultLeaseTime()
        
        def timer = new Timer()
        timer.schedule(new LeaseRemovalTask(resvId: resv.o_id, grailsApplication: grailsApplication), leaseTime*1000)
        leaseTimers[resv.o_id] = timer
        
        return resv
    }

    void onApplicationEvent(LeaseRemovalEvent event) {
        
        try{
            release event.source.resvId
        }catch(IsmHttpException ie){
            log.error "HTTP error ${ie.httpErrorCode} occurred: ${ie.message}"
        }
	}

    def release(resvId) throws IsmHttpException{

        log.info "Lease requested for removal: $resvId"
        log.info "Number of Reservations before removal of lease ${resvId}: ${Reservation.count()}"

        Timer lease = leaseTimers.get(resvId)
        lease.cancel()
        lease.purge()

        leaseTimers.remove(resvId)

        infrastructureInstanceFactoryService.deleteReservation(resvId)
    }

    //Gotta check to see if the reservation is a compute or service
    def commitLease(resvId) throws IsmHttpException, OcciParserException {

        Reservation resv = Reservation.findByO_id(resvId)
        if(!resv)
            throw new IsmHttpException(HttpURLConnection.HTTP_NOT_FOUND, "Reservation not found with id ${resvId}")

        Timer t = leaseTimers[resvId]
        t.cancel()
        t.purge()

        leaseTimers.remove(resvId)

        def contentToExecute = resv.provisioningRequest
        resv.delete(flush:true)

        log.info "Will provision:\n ${contentToExecute}"

        if(resv.isMultipart){
            log.info "Creating a service"

            def boundary = contentToExecute.split('\r\n')[0][2..-1]
            def occiContent = multipartService.extractContent(contentToExecute, boundary)

            //now that you have the occi content, iterate through and create the service and associated comps

            if(occiContent.size() <= 0)
                throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "There was no body-content in the request.")
            else{

                Service svc = infrastructureInstanceFactoryService.createService(occiContent, '')
                if(CH.config.eu.slasoi.infrastructure.copyReservationIdToService){
                    svc.o_id = resvId
                    svc.save()
                }
                
                svc.resources.each{
                    computeService.create(it)
                }
                return svc
            }
        }
        else{
            log.info "Creating a compute"

            def parsedAttrs = OcciParser.getParser(contentToExecute).headers()
            Compute comp = infrastructureInstanceFactoryService.createCompute(parsedAttrs)

            if(!comp){
                throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, "No compute created")
            }
            else{
                if(CH.config.eu.slasoi.infrastructure.copyReservationIdToService){
                    comp.o_id = resvId
                    comp.save()
                }
                computeService.create(comp)
            }
            return comp
        }
    }
}
