package eu.slasoi.infrastructure.services

import eu.slasoi.infrastructure.exceptions.IsmHttpException
import occi.core.AttributeType
import occi.core.Resource
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException

class AdmissionService {

    static transactional = true
    def infrastructureInstanceFactoryService

    def incrementResource(Resource res) throws IsmHttpException {

        def parsedAttrs
        try{
            parsedAttrs = OcciParser.getParser(res.toString()).headers()
        }catch (OcciParserException pe){
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, pe.toString())
            return
        }

        incrementResources(parsedAttrs)
    }

    //TODO need to deal with provisionings specified by X-OCCI-Attribute
    // pessimistic approximation of compute resource to resource template types
    def incrementResources(HashMap parsedAttrs) throws IsmHttpException {

        log.info "Attempting to incrementing the number of resources."

        if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'small', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {
            def attr = AttributeType.findByName('small.occi.available')
            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) + 1).toString()
            attr.save()
            log.info "Current number of small computes now ${attr.defaultValueExpr}"
        }
        else if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'medium', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {
            def attr = AttributeType.findByName('medium.occi.available')
            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) + 1).toString()
            attr.save()
            log.info "Current number of medium computes now ${attr.defaultValueExpr}"
        }
        else if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'large', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {
            def attr = AttributeType.findByName('large.occi.available')
            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) + 1).toString()
            attr.save()
            log.info "Current number of large computes now ${attr.defaultValueExpr}"
        }
        else {
            log.warn "The request contained no resource template that was recognised: ${parsedAttrs.toString()}"
            log.info "It is possible the request is provisioning resources by X-OCCI-Attribute. No change to total resources."
            //throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "The request contained no resource template that was recognised: ${parsedAttrs.toString()}")
        }
    }

    //TODO need to deal with de-provisionings specified by X-OCCI-Attribute
    // pessimistic approximation of compute resource to resource template types
    def decrementResources(Resource res) throws IsmHttpException {
        
        def parsedAttrs
        try{
            parsedAttrs = OcciParser.getParser(res.toString()).headers()
        }catch (OcciParserException pe){
            throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, pe.toString())
            return
        }
        
        decrementResources(parsedAttrs)
    }

    def decrementResources(HashMap parsedAttrs) throws IsmHttpException {

        log.info "Attempting to decrement the number of resources."

        def checkAvailable = { attr ->
            if(Integer.parseInt(attr.defaultValueExpr) < 1){
                log.error 'There are no more available resources that can be provisioned.'
                throw new IsmHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, 'There are no more available resources that can be provisioned.')
            }
            else{
                log.info "Resources available"
            }
        }

        if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'small', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {

            def attr = AttributeType.findByName('small.occi.available')

            checkAvailable attr

            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) - 1).toString()
            attr.save()
            log.info "Current number of small computes now ${attr.defaultValueExpr}"
        }
        else if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'medium', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {
            def attr = AttributeType.findByName('medium.occi.available')

            checkAvailable attr

            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) - 1).toString()
            attr.save()
            log.info "Current number of medium computes now ${attr.defaultValueExpr}"
        }
        else if (infrastructureInstanceFactoryService.containsCategory(parsedAttrs, 'large', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')) {

            def attr = AttributeType.findByName('large.occi.available')

            checkAvailable attr

            attr.defaultValueExpr = (Integer.parseInt(attr.defaultValueExpr) - 1).toString()
            attr.save()
            log.info "Current number of large computes now ${attr.defaultValueExpr}"
        }
        else {
            log.warn "The request contained a resource template that was recognised: ${parsedAttrs.toString()}"
            log.info "It is possible the request is de-provisioning resources by X-OCCI-Attribute. No change to total resources."
            //throw new IsmHttpException(HttpURLConnection.HTTP_BAD_REQUEST, "The request contained no resource template that was recognised: ${parsedAttrs.toString()}")
        }
    }
}
