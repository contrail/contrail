package eu.slasoi.infrastructure.services

import org.apache.commons.fileupload.MultipartStream

class MultipartService {

    static transactional = false

    public def extractContent(String content, String boundary) {

        def occiContent = []
        
        def x = new ByteArrayInputStream(content.getBytes())
        MultipartStream s = new MultipartStream(x, boundary.getBytes())

        boolean nextPart = s.skipPreamble();
        while (nextPart) {

            String headers = s.readHeaders()

            ByteArrayOutputStream data = new ByteArrayOutputStream()
            s.readBodyData(data)
            occiContent.add data.toString()

            nextPart = s.readBoundary()
        }
        return occiContent
    }

    public def createMultipart(resourceList, String boundary = '') {

        def counter = 0
        if(!boundary || boundary?.length() <= 0)
            boundary = Long.toHexString(Double.doubleToLongBits(Math.random()))
        
        def body = ''
        resourceList.each { c ->
            body += "--${boundary}\r\n"
            body += "Content-Disposition: form-data; name=\"$counter\"\r\n"
            body += "Content-Type: text/plain; charset=US-ASCII\r\n"
            body += "Content-Transfer-Encoding: 8bit\r\n\r\n"
            body += c.toString() + '\r\n'
            counter++
        }

        body += "--${boundary}--\r\n"
        
        return body
    }
}
