package eu.slasoi.infrastructure.services

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.controllers.ComputeCreateEvent
import eu.slasoi.infrastructure.messaging.MessageDispatcher
import eu.slasoi.infrastructure.model.Service
import grails.converters.JSON
import grails.web.JSONBuilder
import occi.core.Resource
import occi.infrastructure.Compute
import occi.infrastructure.actions.compute.RestartComputeAction
import occi.infrastructure.actions.compute.StartComputeAction
import occi.infrastructure.actions.compute.StopComputeAction
import occi.infrastructure.actions.compute.SuspendComputeAction
import occi.infrastructure.mixins.OsTemplateMixin
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.context.ApplicationListener

class ComputeService implements ApplicationListener<ComputeCreateEvent>{

    /*
        Methods that take a subclass of Resource send the request to the provisioning system
        Methods that take a String react to messages sent from the provisioning system
     */
    
    static transactional = true

    def springSecurityService
    def grailsApplication
    def monitoringService

    void onApplicationEvent(ComputeCreateEvent e) {
        create(e.source.resource, e.source.action)
    }

    def create(compute, String action='') {

        if(action || action?.length() > 1)
            createAction(compute, action)
        else
            createCompute(compute)

    }

    def createAction(compute, action){

        if(messageDispatchService && CH.config.eu.slasoi.infrastructure.provisioning.enable){

            log.info "The action ${action} will be carried out on resource ${compute}"

            switch (action){
                case StartComputeAction.term:
                    messageDispatchService.sendMessage("resumecompute [{\"fqdn\":\"${compute}\"}]")
                    break
                case StopComputeAction.term:
                    messageDispatchService.sendMessage("stopcompute [{\"fqdn\":\"${compute}\"}]")
                    break
                case RestartComputeAction.term:
                    messageDispatchService.sendMessage("restartcompute [{\"fqdn\":\"${compute}\"}]")
                    break
                case SuspendComputeAction.term:
                    messageDispatchService.sendMessage("suspendcompute [{\"fqdn\":\"${compute}\"}]")
                    break
            }
        }
        else{
            log.info "The action ${action} was carried out on resource ${compute.o_id}"
        }
    }

    def createCompute(Compute compute){
        
        /*
        create the JSON representation of compute
        send the command & JSON to tashi
        createcompute
        [
            {
                "cpu_arch":"x64",
                "cpu_cores":2,
                "cpu_speed":2,
                "hostname":"tizzy",
                "memory_size":512,      <- note conversion
                "userId":"1000"
                "imageIdentifier": "tashi.img"
                "resourceId": "123123-123-1231231-123"
            }
        ]
         */

        if(messageDispatchService && CH.config.eu.slasoi.infrastructure.provisioning.enable){

            def builder = new JSONBuilder()
            def result = builder.build {
                cpu_arch = compute.getAttrValue(Compute.occi_compute_arch).getTypedValue()
                cpu_cores = compute.getAttrValue(Compute.occi_compute_cores).getTypedValue()
                cpu_speed = compute.getAttrValue(Compute.occi_compute_speed).getTypedValue()
                hostname = compute.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
                memory_size = 1024 * compute.getAttrValue(Compute.occi_compute_memory).getTypedValue()
                userId = userId
                imageIdentifier = getImageId(compute)
                resourceId = compute.o_id
            }
            
            log.info "Sending to provisioning systems: createcompute ${result.toString()}"
            messageDispatchService.sendMessage("createcompute ${result.toString()}")
        }
        else{
            def builder = new JSONBuilder()
            def result = builder.build {
                hints{
                    resource_id = compute.o_id
                    host = 'NO_HOST_NAME_TESTMODE'
                }
            }
            create(result.toString())
        }
    }

    def create(String computeJson){

        log.info "Received create compute update message from the provisioning system."

        def computeJsonObj = JSON.parse(computeJson)
        String resId = extractResourceId(computeJsonObj)

        log.info "Updating the state of the following compute ID:${resId}"
        Compute compute = Compute.findByO_id(resId)

        if(compute){

            log.info "The compute resource is now active."
            def attrval = compute.getAttrValue(Compute.occi_compute_state)
            attrval.value = Compute.occi_compute_state_active
            attrval.save()

            if(computeJsonObj?.hints.host.endsWith("L")){
                log.info "The virtual machine's physical host id is ${computeJsonObj.hints.host.subSequence(0, computeJsonObj.hints.host.length() - 1)}"
                compute.physicalHostFqdn = computeJsonObj.hints.host.subSequence(0, computeJsonObj.hints.host.length() - 1)
            }

            else{
                log.info "The virtual machine system id is ${computeJsonObj.hints.host}"
                compute.physicalHostFqdn = computeJsonObj.hints.host
            }
            compute.save()
        }
        else{
            log.error "Received an create-update command from the provisioning system for an non-existant compute: ${resId}"
        }

        if(compute.owningService){
            //when a compute is created we need to check if it is part of a service
            //if it is and all computes within the service are complete then set the status of the service
            def svcProvisioned = true
            compute.owningService?.resources.each { res ->
                if(res instanceof Compute){
                    log.info "compute asso with service"

                    def attrval = compute.getAttrValue(Compute.occi_compute_state)

                    if(attrval.value == Compute.occi_compute_state_inactive){
                        log.debug "there still is an inactive compute, I'm not reporting this service as provisioned."
                        svcProvisioned = false
                    }
                }
                else{
                    log.warn "Unrecognised resource associated with the service"
                }
            }
            if(svcProvisioned){

                //now that the service is provisioned, issue a provisioning request on the LLMS
                def monitoringEnabled = CH.config.eu.slasoi.infrastructure.monitoring.enabled ?: false
                def attrval = compute.owningService.getAttrValue(Service.eu_slasoi_infrastructure_service_state)

                if(monitoringEnabled && attrval.value == Service.eu_slasoi_infrastructure_service_state_inactive){
                    log.info "Monitoring is enabled."
                    monitoringService.provisionMonitoring(compute.owningService)
                }

                log.info "Service ${compute.owningService.o_id} has been completely provisioned."
                attrval = compute.owningService.getAttrValue(Service.eu_slasoi_infrastructure_service_state)
                attrval.value = Service.eu_slasoi_infrastructure_service_state_active
                attrval.save()
            }
        }
    }

    def update(Compute compute) {

        /*
        create the JSON representation of compute
        send the command & JSON to tashi

        updatecompute
        [
            {
                "cpu_cores":2,
                "cpu_speed":2,
                "hostname":"tizzy",
                "memory_size":0.5,
            }
        ]
         */

        if(messageDispatchService && CH.config.eu.slasoi.infrastructure.provisioning.enable){
            def computeJson = new StringWriter();
            new grails.util.JSonBuilder(computeJson).json {
                cpu_cores: compute.getAttrValue(Compute.occi_compute_cores).getTypedValue()
                cpu_speed: compute.getAttrValue(Compute.occi_compute_speed).getTypedValue()
                hostname: compute.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
                memory_size: 1024 * compute.getAttrValue(Compute.occi_compute_memory).getTypedValue()
            }

            def request = "updatecompute ${computeJson.toString()}"
            messageDispatchService.sendMessage(request)
        }
        else{
            def builder = new JSONBuilder()
            def result = builder.build {
                hints{
                    resource_id = compute.o_id
                    host = 'NO_HOST_NAME_TESTMODE'
                }
            }
            update(result.toString())
        }

    }

    def update(String computeJson){

        log.info "Received update compute update message from the provisioning system."

        def computeJsonObj = JSON.parse(computeJson)
        String resId = extractResourceId(computeJsonObj)

        log.info "Updating the state of the following compute ID:${resId}"
        Compute compute = Compute.findByO_id(resId)

        if(compute){

            log.info "The compute resource is now active."
            def attrval = compute.getAttrValue(Compute.occi_compute_state)
            attrval.value = Compute.occi_compute_state_active
            attrval.save()
            compute.save()
        }
        else{
            log.error "Received an update-update command from the provisioning system for an non-existant compute: ${resId}"
        }
    }

    def delete(Compute compute) {

        def hostname = compute.getAttrValue(Compute.occi_compute_hostname).getTypedValue()
        if(!hostname || hostname?.length() <= 0)
            log.error "Cannnot delete the instance as there is no hostname associated with it."
        else{

            def request = "deletecompute [{\"fqdn\":\"${hostname}\"}]"
            if(messageDispatchService && CH.config.eu.slasoi.infrastructure.provisioning.enable){
                log.info "Sending to provisioning system: ${request}"
                messageDispatchService.sendMessage(request)
            }
            else{
                def builder = new JSONBuilder()
                def result = builder.build {
                    hints{
                        resource_id = compute.o_id
                    }
                }
                delete(result.toString())
            }
        }
    }

    def delete(String computeJson){
        log.info "The provisioning system confirmed deletion."
    }

    private String extractResourceId(provisioning) {

        if (!provisioning)
            log.error("The deserialisation of the provisioning response failed")

        //let's find the task object associated with the resource
        String resId = provisioning.hints["resource_id"]
        if (!resId)
            log.error "The provisioning system returned a provisioned object without its resourceId. Cannot update the resource's task"

        if (resId.startsWith("u'")) {
            resId = resId.subSequence(2, resId.length() - 1)
            log.info "Resource ID recieved was a python UTF8 string and so needed parsing"
        }
        return resId
    }

    private def getUserId(){

        def principal = springSecurityService.principal

        if(!(principal instanceof String) && principal?.unixId){
            log.info "Using LDAP supplied user ID: ${principal.unixId}"
            return principal.unixId
        }
        else{
            log.warn "There was no LDAP supplied user ID. Using default of: ${CH.config.eu.slasoi.infrastructure.provisioning.defaultUserId ?: '1000'}"
            return CH.config.eu.slasoi.infrastructure.provisioning.defaultUserId ?: '1000'
        }
    }

    private def getImageId(compute){

        //TODO better if the query searched on parent mixin
        def osTpl = compute.mixins.find{
            //it.scheme == OsTemplateMixin.scheme
            it.scheme == 'http://sla-at-soi.eu/occi/os_templates#'
        }

        if(osTpl)
            return osTpl.term + '.img'
        else
            return 'tashi.img'
    }

    private MessageDispatcher getMessageDispatchService(){

        MessageDispatcher bean

        try{
            bean = (MessageDispatcher)grailsApplication.mainContext.getBean('messageDispatchService')
        }
        catch (NoSuchBeanDefinitionException nbdf){
            log.warn "No MessageDispatcher bean found."
            bean = null
        }
        return bean
    }
}
