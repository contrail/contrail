package eu.slasoi.infrastructure.services.messaging

/*
SVN FILE: $Id: XmppMessageHandlerService.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/services/eu/slasoi/infrastructure/services/messaging/XmppMessageHandlerService.groovy $

*/

import eu.slasoi.infrastructure.messaging.MessageHandler
import eu.slasoi.infrastructure.services.ComputeService

class XmppMessageHandlerService extends CommandDispatchService implements MessageHandler {

    static expose = ['xmpp']
    static listenerMethod = "handleMessage"

    private def handlers = []
    
    def handleMessage(message) {

        log.debug "XmppMessageHandlerService handling incoming message..."

        String msgStr = message.body.toString()

        if (msgStr.startsWith('@createvm')){
            computeService.create(msgStr.substring('@createvm'.length()))
        }
        else if (msgStr.startsWith('@updatevm')){
            computeService.update(msgStr.substring('@updatevm'.length()))
        }
        else if(msgStr.startsWith('@deletevm')){
            computeService.delete(msgStr.substring('@deletevm'.length()))
        }
        else if(msgStr.startsWith('@error')){
            log.error "There has been an error raised from the provisioning system: ${msgStr}"

            //create a log entry
            //set flag on resource (id supplied in the error message) that its in error
            //when rendered it should redirect 3XX and Location to log entry
        }
        else{
            log.warn "Unrecognised command received from the provisioning system: received: ${msgStr}"
        }

        handlers.each{ handler ->
            handler message
        }
    }

    def registerHandler(handler){
        this.handlers.add handler
    }
}
