/* 
SVN FILE: $Id: BootStrap.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/conf/BootStrap.groovy $

*/

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

class BootStrap {

    def grailsApplication
    def infrastructureTypeFactoryService
    def grailsUrlMappingsHolder
    //def log

    def init = {servletContext ->

        //this workaround is fixed in 1.4M2
        grailsApplication.config.'grails.web.disable.multipart' = true

        checkBeans()

        selectBus()
        
        environments {
            production {
                infrastructureTypeFactoryService.initDefaults()
            }
            development {
                infrastructureTypeFactoryService.initDefaults()
            }
            test {
                infrastructureTypeFactoryService.initDefaults()
            }
        }
    }

    def testTypeConfig(){

        def count = 0
        def type = CH.config.eu.slasoi.infrastructure.types.getProperty('type'+count)
        while(type){
            println type.term
            count++
            type = CH.config.eu.slasoi.infrastructure.types.getProperty('type'+count)
        }
        println 'done'
    }

    def destroy = {
//    if (CH.config.eu.slasoi.infrastructure.monitoring.enable)
//      monitoringService.destroy()
    }

    def checkBeans(){

        if(!grailsApplication.mainContext.getBean('pubSubDispatcher'))
            log.error "There is no PubSub dispatcher available. This is an error with the application's spring configuration."
        else if(!grailsApplication.mainContext.getBean('ldapUserDetailsMapper'))
            log.error "There is no LDAP Details Mapper available. This is an error with the application's spring configuration."
        else
            log.info "PubSub Dispatcher & LDAP Details Mapper are present and are loaded."

    }

    def selectBus() {

        if(CH.config.eu.slasoi.infrastructure.provisioning.enable){
            //NOTE:
            // these aliases cannot be directly injected into the service classes that may use them
            // to access them you need to access them via grailsApplication.mainContext.getBean($NAME)
            if(grailsApplication.applicationMeta.containsKey('plugins.rabbitmq')){
                log.info "Configuring the provisioning to use AMQP"
                grailsApplication.mainContext.registerAlias('rabbitMessageHandlerService', 'messageHandlerService')
                grailsApplication.mainContext.registerAlias('rabbitMessageDispatchService', 'messageDispatchService')
            }
            else if(grailsApplication.applicationMeta.containsKey('plugins.xmpp')){
                log.info "Configuring the provisioning to use XMPP"
                grailsApplication.mainContext.registerAlias('xmppMessageHandlerService', 'messageHandlerService')
                grailsApplication.mainContext.registerAlias('xmppMessageDispatchService', 'messageDispatchService')
            }
            else{
                log.warn "There is no message bus configured to work with the ISM.\nThe ISM will operate in standalone mode."
            }
        }
        else{
            log.warn "Provisioning is not enabled.\nThe ISM will operate in standalone mode."
        }
    }
}
