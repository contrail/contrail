/* 
SVN FILE: $Id: Config.groovy 2704 2011-07-19 15:59:41Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2704 $
@lastrevision   $Date: 2011-07-19 17:59:41 +0200 (tor, 19 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/conf/Config.groovy $

*/

//---------------------------------------------
//---------- External configuration -----------
//-- Note: Only .groovy file configs allowed -- 
//---------------------------------------------
//---------------------------------------------

def ENV_NAME = "${appName}_CONFIG"

if (!grails.config.locations || !(grails.config.locations instanceof List)) {
    grails.config.locations = []
}

println "--------------------------------------------------------"
println "---------------- Loading ISM Config --------------------"
println "--------------------------------------------------------"
println "ISM config shell variable: " + ENV_NAME

// Supply config file on command line e.g.
// grails -D-DISM_CONFIG=/Users/andy/ism-config.groovy run-app
if (System.getProperty(ENV_NAME) && new File(System.getProperty(ENV_NAME)).exists()) {
    println "Including configuration file specified on command line: " + System.getProperty(ENV_NAME)
    grails.config.locations << "file:" + System.getProperty(ENV_NAME)
}

// If no config file by command line then look for a config file in the users home directory
else if (new File("${userHome}/${appName}-config.groovy").exists()) {
    println "Using config file found at: file:${userHome}/${appName}-config.groovy. ***"
    grails.config.locations = ["file:${userHome}/${appName}-config.groovy"]
}

// If no config file in the home directory, look for one pointed to by an environment variable (e.g. shell)
else if (System.getenv(ENV_NAME) && new File(System.getenv(ENV_NAME)).exists()) {
    println("Using system environment specified config file: " + System.getenv(ENV_NAME))
    grails.config.locations << "file:" + System.getenv(ENV_NAME)
}

else if(new File('example-ism-config.groovy').exists()){
    println("Using example config file: example-ism-config.groovy")
    grails.config.locations << "file:example-ism-config.groovy"
}

else {
    println "*** No external configuration file defined. ***"
    println "*** Exiting tout suite! ***"
    throw new Exception("*** No external configuration file defined. ***")
}

println "grails.config.locations = ${grails.config.locations}"
println "--------------------------------------------------------"

//----------------------------------------------------------------
//Default grails configs
//----------------------------------------------------------------
grails.mime.file.extensions = false // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = true
grails.mime.types = [
        //html: ['text/html', 'application/xhtml+xml'],
        //xml: ['text/xml', 'application/xml'],
        text: 'text/plain',
        uriList: 'text/uri-list',
        occi: 'text/occi',
        all: '*/*',
        //json: ['application/json', 'text/json'],
        //form: 'application/x-www-form-urlencoded',
        multipartForm: 'multipart/form-data'
]

//See http://jira.grails.org/browse/GRAILS-7565
//See http://grails.1312388.n4.nabble.com/Disable-MultipartResolver-td3560222.html
//grails.web.disable.multipart = true
grails.disableCommonsMultipart = true

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable fo AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
