<html>

<!--
	
	/* 
	SVN FILE: $Id: index.gsp 2754 2011-07-22 10:56:50Z andy-edmonds $ 

	Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	    * Redistributions of source code must retain the above copyright
	      notice, this list of conditions and the following disclaimer.
	    * Redistributions in binary form must reproduce the above copyright
	      notice, this list of conditions and the following disclaimer in the
	      documentation and/or other materials provided with the distribution.
	    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
	      names of its contributors may be used to endorse or promote products
	      derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	@author         $Author: andy-edmonds $
	@version        $Rev: 2754 $
	@lastrevision   $Date: 2011-07-22 12:56:50 +0200 (pet, 22 jul 2011) $
	@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/views/index.gsp $

	*/

-->

<head>
    <title>SLA@SOI Infrastructure Service Manager</title>
    <meta name="layout" content="main"/>
    <style type="text/css" media="screen">

    #nav {
        margin-top: 20px;
        margin-left: 30px;
        width: 228px;
        float: left;
    }

    .homePagePanel * {
        margin: 0px;
    }

    .homePagePanel .panelBody ul {
        list-style-type: none;
        margin-bottom: 10px;
    }

    .homePagePanel .panelBody h1 {
        text-transform: uppercase;
        font-size: 1.1em;
        margin-bottom: 10px;
    }

    h2 {
        margin-top: 15px;
        margin-bottom: 15px;
        font-size: 1.2em;
    }

    #pageBody {
        /*margin-left: 280px;*/
        margin-right: 20px;
    }
    </style>
</head>
<body>

<div id="pageBody">
    <div id="occiLogo" class="logo" align="center">
        <a href="http://www.occi-wg.org"><img src="${resource(dir: 'images', file: 'occi.png')}" alt="Open Cloud Computing Interface" border="0"/></a>
        <h1><a href ="https://sourceforge.net/apps/trac/sla-at-soi/wiki/InfrastructureServiceManager">More about this...</a></h1>
    </div>
</div>
</body>
</html>
