package org.slasoi.ism.occi;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.ops4j.pax.exam.CoreOptions.knopflerfish;
import static org.ops4j.pax.exam.CoreOptions.equinox;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.provision;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.cleanCaches;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.springframework.test.context.ContextConfiguration;

@RunWith(JUnit4TestRunner.class)
//@ContextConfiguration(locations = { "classpath:/org/slasoi/ism/occi/context-TestInfrastructureMockup.xml" })

public class BundleContextTest {

	@Inject
	private static BundleContext bundleContext;
	private static org.slasoi.ism.occi.IsmOcciService service;

	private static String myBundle = "ISMOCCIBundle";

	// TODO: This id corresponds to a test VM that is producing test data. This
	// test will not work if the VM is not running and publishing data to the
	// PredictionService
	private static String id = "e0cb4eb02f47";
	private static String metricName = "cpu";
	private static int minutes = 2;
	private static String hostname = "itbpax";
	private String monitoringRequest;
	private String notificationURI;

	@Configuration
	public Option[] configure() {
		return options(
				equinox(),
				profile("spring.dm"),
				cleanCaches(),
				provision(

						wrappedBundle(mavenBundle().groupId(
								"org.apache.httpcomponents").artifactId(
								"httpclient").version("4.0.1").start()),
						wrappedBundle(mavenBundle().groupId(
								"org.apache.httpcomponents").artifactId(
								"httpcore").version("4.0.1").start()),
						wrappedBundle(mavenBundle().groupId(
								"org.apache.httpcomponents").artifactId(
								"httpcore-nio").version("4.0.1").start()),
						wrappedBundle(mavenBundle().groupId(
								"org.apache.httpcomponents").artifactId(
								"httpmime").version("4.0.1").start()),
						wrappedBundle(mavenBundle().groupId("commons-codec")
								.artifactId("commons-codec").version("1.4")
								.start()),
						wrappedBundle(mavenBundle().groupId("org.apache.james")
								.artifactId("apache-mime4j").version("0.6")
								.start()),
						wrappedBundle(mavenBundle().groupId(
								"com.thoughtworks.xstream").artifactId(
								"xstream").version("1.3.1").start()),

						wrappedBundle(mavenBundle().groupId(
								"com.google.code.guice").artifactId("guice")
								.version("2.1-r1201").start()),

						wrappedBundle(mavenBundle().groupId("javax.inject")
								.artifactId("inject").version("1.0").start()),

						wrappedBundle(mavenBundle().groupId("org.json")
								.artifactId("json").version("20070829").start()),

						wrappedBundle(mavenBundle().groupId(
								"com.google.code.gson").artifactId("gson")
								.version("1.7.1").start()),

						wrappedBundle(mavenBundle().groupId(
								"com.google.code.gson").artifactId("gson")
								.version("1.7.1").start()),

						wrappedBundle(mavenBundle().groupId("commons-logging")
								.artifactId("commons-logging").version("1.1.1")
								.start()), mavenBundle().groupId(
								"org.slasoi.infrastructure-monitoring")
								.artifactId("infrastructure-monitoring-agent")
								.version("0.1-SNAPSHOT").start(),

						mavenBundle().groupId(
								"org.slasoi.infrastructure-slamanager.core")
								.artifactId("isslam-core").version(
										"0.1-SNAPSHOT").start(),

						mavenBundle().groupId("com.google.inject.extensions")
								.artifactId("guice-assistedinject").version(
										"3.0").start(),

						wrappedBundle(mavenBundle().groupId("com.google.guava")
								.artifactId("guava").version("r09").start()),

						mavenBundle().groupId("org.slasoi.ISMOCCIBundle")
								.artifactId("ISMOCCIBundle").version(
										"0.1-SNAPSHOT").start()

				));
	}

	@Before
	public void listBundles() throws Exception {
		Thread.sleep(5000);
		System.out.println("********************************");
		ArrayList<ServiceReference> myServiceReferences = new ArrayList<ServiceReference>();
		boolean foundMyBundle = false;
		for (Bundle bundle : bundleContext.getBundles()) {
			System.out.println("Bundle - ID - SymName - "
					+ bundle.getBundleId() + " : " + bundle.getSymbolicName());
			System.out.println("Bundle - State - " + bundle.getState());
			if (org.osgi.framework.Bundle.ACTIVE == bundle.getState()) {
				System.out.println("    Bundle - ACTIVE");
			} else if (org.osgi.framework.Bundle.UNINSTALLED == bundle
					.getState()) {
				System.out.println("    Bundle - UNINSTALLED");
			} else if (org.osgi.framework.Bundle.INSTALLED == bundle.getState()) {
				System.out.println("    Bundle - INSTALLED");
			} else if (org.osgi.framework.Bundle.RESOLVED == bundle.getState()) {
				System.out.println("    Bundle - RESOLVED");
			} else if (org.osgi.framework.Bundle.STOPPING == bundle.getState()) {
				System.out.println("    Bundle - STOPPING");
			} else if (org.osgi.framework.Bundle.UNINSTALLED == bundle
					.getState()) {
				System.out.println("    Bundle - UNINSTALLED");
			} else if (org.osgi.framework.Bundle.STARTING == bundle.getState()) {
				System.out.println("    Bundle - STARTING");
			}

			ServiceReference[] serviceReferences = bundle
					.getRegisteredServices();
			if (serviceReferences != null) {
				for (int i = 0; i < serviceReferences.length; i++) {
					if (bundle.getSymbolicName().equals(myBundle)) {
						System.out
								.println("                serviceReference - "
										+ serviceReferences[i]);
						myServiceReferences.add(serviceReferences[i]);
						foundMyBundle = true;
					}
				}
			}
		}
		assertTrue(foundMyBundle);
		boolean founMyService = false;
		boolean predictionQueryExecuted = false;
		String infrastructureId = null;
		System.out.println("Num of References for my Service - "
				+ myServiceReferences.size());
		for (Iterator<ServiceReference> iterator = myServiceReferences
				.iterator(); iterator.hasNext();) {
			ServiceReference serviceReference = iterator.next();
			System.out.println("		found This Reference"
					+ serviceReference.toString());
			String[] propertyKeys = serviceReference.getPropertyKeys();
			for (int i = 0; i < propertyKeys.length; i++) {

				System.out.println(" 	property/value - " + propertyKeys[i]
						+ "/" + serviceReference.getProperty(propertyKeys[i]));

			}
			// We try to find the service now

			ServiceTracker tracker = new ServiceTracker(bundleContext,
					serviceReference, null);
			tracker.open();
			Object o = tracker.waitForService(50000);
			System.out.println("Found - " + o);
			System.out.println("	Found - " + o.getClass().getName());
			if (o instanceof org.slasoi.ism.occi.IsmOcciServiceImpl) {
				System.out.println("		Found - occi Implementation - ");
				service = (org.slasoi.ism.occi.IsmOcciService) o;
				founMyService = true;

				// We can call the service now
				// <<prepare>> interaction
				testQuery();
				testRelease();
				testQuery();
				testCommit();
				testRelease();

				// <<provision>> interaction
				testProvision();
				testPredictionQuery();

			} else if (o instanceof org.slasoi.ism.occi.IsmOcciService) {
				System.out.println("		Found - Interface - ");

			}
			assertTrue(founMyService);

			Thread.sleep(1000);
			tracker.close();

		}

		// We search the service by class name by name now
		// This does not seem to find the class
		/*
		 * ServiceTracker tracker = new ServiceTracker(bundleContext,
		 * org.slasoi.ism.occi.IsmOcciServiceImpl.class.getName(), null);
		 * tracker.open(); org.slasoi.ism.occi.IsmOcciServiceImpl service =
		 * (org.slasoi.ism.occi.IsmOcciServiceImpl)
		 * tracker.waitForService(50000); tracker.close();
		 * System.out.println("Found Service ByClassName - " + service);
		 */
	}
@Ignore
	@Test
	public void testCheck() {
		// This is for executing the test
		System.out.println("Check");

	}

	public void testProvision() {
		ProvisionRequestType provisionRequest = createProvisionRequestType();
		ProvisionResponseType provisionResponseType = null;
		try {
			provisionResponseType = service.provision(provisionRequest);
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		} catch (ProvisionException e) {
			e.printStackTrace();
			fail();
		}
		assertNotNull(provisionResponseType);
		// If successful, we should get an infrastructureId
		System.out.println(" InfrastructureId - "
				+ provisionResponseType.getInfrastructureID());
		String infrastructureId = provisionResponseType.getInfrastructureID();
		assertNotNull(infrastructureId);

		//
	}

	public void testReProvision() {
		ProvisionRequestType provisionRequest = createProvisionRequestType();
		ProvisionResponseType provisionResponseType = null;
		try {
			provisionResponseType = service.provision(provisionRequest);
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		} catch (ProvisionException e) {
			e.printStackTrace();
			fail();
		}
		assertNotNull(provisionResponseType);
		// If successful, we should get an infrastructureId
		System.out.println(" InfrastructureId - "
				+ provisionResponseType.getInfrastructureID());
		String infrastructureId = provisionResponseType.getInfrastructureID();
		assertNotNull(infrastructureId);

		try {
			assertNotNull(service.reprovision(provisionResponseType
					.getInfrastructureID(), provisionRequest));
		} catch (DescriptorException e) {
			e.printStackTrace();
			fail();
		} catch (UnknownIdException e) {
			e.printStackTrace();
			fail();
		} catch (ProvisionException e) {
			e.printStackTrace();
			fail();
		}

		//
	}

	public void testPredictionQuery() {
		// We test the predictionQuery
		String result = null;
		try {
			result = service.predictionQuery(id, metricName, minutes);
		} catch (UnknownIdException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
		assertNotNull(result);
		if (result != null) {

			// TODO change this when changing to the proper object model (the
			// result), and we will be able to iterate the array for elements

		} else {
			fail();
		}
		System.out.println("Prediction Query -------------------");
		System.out.println(result);

	}

	public void testQuery() {
		IsmOcciService infrastructure = service;

		assertNotNull(infrastructure);
		try {
			assertNotNull(infrastructure.queryCapacity());
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}

	}

	public void testReserve() {
		IsmOcciService infrastructure = service;

		ProvisionRequestType provisionRequest = createProvisionRequestType();

		assertNotNull(infrastructure);
		try {
			assertNotNull(infrastructure.queryCapacity());
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}

		ReservationResponseType reservationResponseType = null;
		try {
			reservationResponseType = infrastructure.reserve(provisionRequest);
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}
		assertNotNull(reservationResponseType);

		assertNotNull(reservationResponseType.getInfrastructureID());

	}

	public void testCommit() {
		IsmOcciService infrastructure = service;

		ProvisionRequestType provisionRequest = createProvisionRequestType();

		assertNotNull(infrastructure);
		try {
			assertNotNull(infrastructure.queryCapacity());
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}

		ReservationResponseType reservationResponseType = null;

		try {
			reservationResponseType = infrastructure.reserve(provisionRequest);
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}
		assertNotNull(reservationResponseType);

		assertNotNull(reservationResponseType.getInfrastructureID());

		try {
			reservationResponseType = infrastructure
					.commit(reservationResponseType.getInfrastructureID());
		} catch (UnknownIdException e) {
			// TODO Auto-generated catch block

			fail();
		}

		assertNotNull(reservationResponseType.getInfrastructureID());

	}

	public void testRelease() {
		IsmOcciService infrastructure = service;
		ProvisionRequestType provisionRequest = createProvisionRequestType();

		assertNotNull(infrastructure);
		try {
			assertNotNull(infrastructure.queryCapacity());
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}

		ReservationResponseType reservationResponseType = null;

		try {
			reservationResponseType = infrastructure.reserve(provisionRequest);
		} catch (DescriptorException e) {
			// TODO Auto-generated catch block

			fail();
		}
		assertNotNull(reservationResponseType);

		assertNotNull(reservationResponseType.getInfrastructureID());

		try {
			reservationResponseType = infrastructure
					.commit(reservationResponseType.getInfrastructureID());
		} catch (UnknownIdException e) {
			// TODO Auto-generated catch block

			fail();
		}

		assertNotNull(reservationResponseType.getInfrastructureID());

		try {
			infrastructure.release(reservationResponseType
					.getInfrastructureID());
		} catch (UnknownIdException e) {
			// TODO Auto-generated catch block

			fail();
		}
	}

	public ProvisionRequestType createProvisionRequestType() {

		ProvisionRequestType type = null;
		try {
			assertNotNull(service);
			assertNotNull(service.getOsregistry());
			assertNotNull(service.getOsregistry().getDefaultCategory());
			assertNotNull(service.getOsregistry().getDefaultCategory()
					.getTerm());

			assertNotNull(service.getMetricregistry());
			assertNotNull(service.getMetricregistry().getDefaultCategory());
			assertNotNull(service.getMetricregistry().getDefaultCategory()
					.getTerm());

			assertNotNull(service.getLocregistry());
			assertNotNull(service.getLocregistry().getDefaultCategory());
			assertNotNull(service.getLocregistry().getDefaultCategory()
					.getTerm());

			type = service.createProvisionRequestType(

			service.getOsregistry().getDefaultCategory().getTerm(), service
					.getMetricregistry().getDefaultCategory().getTerm(),
					service.getLocregistry().getDefaultCategory().getTerm(),
					monitoringRequest, notificationURI);
			assertNotNull(type);
		} catch (Error e) {

			fail();
		}

		return type;

	}
}
