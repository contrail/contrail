################
INSTALL
################

- Install JAVA 6
- Install Pax-runner 1.7.1
- MySQL Community Server 5.1.43
- Event bus
    * AMQP ([install http://www.rabbitmq.com/install.html]) OR
    * OpenFire([install http://www.igniterealtime.org/builds...ll-guide.html])
- After extracting the binary zip to local file system. Following hierachy will be available

	|-- DB/
	|-- osgi-config/
	|-- pax-run/
	|-- profiles/
	|-- ext-syc-broker.bat
	|-- ReadMe.txt
	|-- startup.bat
	'-- syc-broker.jar
	
	
################
DB CONFIGURATION
################

setup and populate the local database instance with all the required schemas of framework. 

1. Please run the scripts inside DB folder of the release on your db instance in order given below.
	1.  \DB\sla-registry\model.sql
	2.  \DB\template-registry\business\dbmodel.sql
	3.  \DB\template-registry\business\datamodel.sql
	4.  \DB\template-registry\software\dbmodel.sql
	5.  \DB\template-registry\software\datamodel.sql
	6.  \DB\template-registry\infrastructure\dbmodel.sql
	7.  \DB\template-registry\infrastructure\datamodel.sql
	8.  \DB\everest-rcg\createRCGDatabaseAndTables.sql
	9.  \DB\everest-rcg\createUser.sql
	10  \DB\everest-core\createSERENITYDatabaseAndTables
	11. \DB\everest-core\createUser.sql
	12. \DB\bmanager-postsale-reporting\dbModel.sql
	13. \DB\business-manager\bm-datamodel.sql
	
	
	
2. Update the database configuration inside

	1. osgi-config\BusinessManager\db.properties (Business Manager database configuration)
	2. osgi-config\generic-slamanager\template-registry\db.properties (GSLAM template registry configuration)
	3. osgi-config\generic-slamanager\sla-registry\db\gslam-slaregistry.cfg.xml (GSLAM sla registry configuration)	
	4. osgi-config\monitoring-system\sla-level-monitoring\city\rcg.database
	5. osgi-config\bmanager-postsale-reporting\DB\db.properties
	
         
#################
RUN
#################

- Setup and configure event bus instance as explained on following link.

	* https://sourceforge.net/apps/trac/sla-at-soi/wiki/ExecutePlatform#EventBus

- Setup and configure provisioning infrastructure as explained on following link.

	* https://sourceforge.net/apps/trac/sla-at-soi/wiki/ExecutePlatform#ProvisioningLayerConfiguration

- Run the SyntaxConverter broker console by double clicking on ext-syc-broker.bat. You will end up in a console with following state      
	
	*********************************
	Starting SyntaxConverterBroker...
	SyntaxConverterBroker [port=7077] waiting for job ...
	*********************************

- Run the platform by double clicking the startup.batch file.

Note: If the statup.bat console stopped after few seconds, relaunch it again by double clicking the startup.bat file. 
Possible reasons can be local connectivity issues.

It will take some time before the entire SLA@SOI platform will be launched. The platform consist of more than 200 bundles that will get loaded after this command.
In order to verify the completion of the launch of platform, after sometime type type the OSGi console command 'ss'.

You should be able to see all the bundles in ACTIVE state except for 
	* FragmentBundles (not meant for ACTIVE state)
	* org.ops4j.pax.configmanager (the logging control bundle) 
	
- (Optional) to avoid detailed log prints, start the bundle org.ops4j.pax.configmanager by typing at the OSGi console:
	start <n>
	where <n> is the number corresponding to the  org.ops4j.pax.configmanager bundle (n=95 in current release)

- After some time the SLA@SOI platform is up and running waiting for requests to be fulfilled.