@@@@@@@@@@@@@@@@@@@@@@@@@@
SLA@SOI PLATFORM
@@@@@@@@@@@@@@@@@@@@@@@@@@

######
BUILD
######

These are the complete instructions for building/launching the platform from scratch.


- Install Java 1.6.
- Install Maven 3.0.2:
- settings.xml:
	A sample settings.xml file can be inside 'mvn' folder. The important sections to change in settings.xml are:
	* <localRepository> , adjust here the path the local maven repo
	* <server>, define the servers used for download artifacts.
	* <proxy>, only if you are behind proxy, you must write the proxy configuration in this section.
 
- set the environment variables (M2_HOME, M2_REPO, MAVEN_OPTS):
	
	********************************************
	M2_HOME=<mavenDist>\apache-maven-3.0.2
	M2_REPO=<mavenDist>\apache-maven-3.0.2\repo
	********************************************

- Extract the source zip to local file system.
- Run the Maven command from extracted folder /trunk to install all the platform components to the local Maven repository. 
  The precise command to run is:
	
	**********************************************
	mvn clean install -fae -Dmaven.test.skip=true
	**********************************************


####################
INSTALL & CONFIGURE
####################

Some more installation and configuration steps are needed:

- Install Pax-runner 1.7.1.

- MySQL Community Server 5.1.43

- Event bus
    * AMQP ([install http://www.rabbitmq.com/install.html]) OR
    * OpenFire([install http://www.igniterealtime.org/builds...ll-guide.html])

- Set the following environment variables:
	
	SLASOI_HOME (must point to the folder trunk/common/osgi-config )
	
	* The purpose of SLASOI_HOME variable is to track all the configuration files required by different platform components in
	  a common place. This allows for a complete portability of the platform code, in particular with respect to its integration 
	  within the SLA@SOI platform. A common folder is created inside the trunk\common\osgi-config to contain all the 
	  configuration files.
	
	SLASOI_ORC_HOME (must point to the folder /trunk/orc-data)
	 
	* This variable is created with the goal of referencing ORC related data from the source code. This folder hosts the templates 
	 and other configuration files related to ORC. This folder is referenced from within the source code of SLA@SOI Platform in order
	 to load the ORC related configuration data.
	 
- setup and populate the local database instance with all the required schemas of framework. 

  * run the following set of scripts in order given below
	1. trunk\generic-slamanager\sla-registry\db\model.sql
	2. $SLASOI_HOME\Integration\templates\db\business\dbmodel.sql
	3. $SLASOI_HOME\Integration\templates\db\business\datamodel.sql
	4. $SLASOI_HOME\Integration\templates\db\software\dbmodel.sql
	5. $SLASOI_HOME\Integration\templates\db\software\datamodel.sql
	6. $SLASOI_HOME\Integration\templates\db\infrastructure\dbmodel.sql
	7. $SLASOI_HOME\Integration\templates\db\infrastructure\datamodel.sql
	8. trunk\monitoring-system\sla-level-monitoring\city\everest-rcg\batch\mySQL\createRCGDatabaseAndTables.sql
	9. trunk\monitoring-system\sla-level-monitoring\city\everest-rcg\batch\mySQL\createUser.sql
	10 trunk\monitoring-system\sla-level-monitoring\city\everest-core\batch\mysql\createSERENITYDatabaseAndTables
	11.trunk\monitoring-system\sla-level-monitoring\city\everest-core\batch\mysql\createUser.sql
	12.$SLASOI_HOME\bmanager-postsale-reporting\DB\dbModel.sql
	13.trunk\business-manager\DB\bm-datamodel.sql


 2. Update the database configuration inside

	1. $SLASOI_HOME\BusinessManager\db.properties (Business Manager database configuration)
	2. $SLASOI_HOME\generic-slamanager\template-registry\db.properties (GSLAM template registry configuration)
	3. $SLASOI_HOME\generic-slamanager\sla-registry\db\gslam-slaregistry.cfg.xml (GSLAM sla registry configuration)	
	4. $SLASOI_HOME\monitoring-system\sla-level-monitoring\city\rcg.database
	5. $SLASOI_HOME\bmanager-postsale-reporting\DB\db.properties.


@@@@@@@@@@@@@@@@@@@@@@@@@@
INTEGRATION BUNDLE
@@@@@@@@@@@@@@@@@@@@@@@@@@

This bundle is require to demonstrate the execution of Integration test scenarios.


########
COMPILE
########

Go to /trunk/IntegrationTestModule and run the following command:

	**********************************************
	mvn clean install -fae -Dmaven.test.skip=true
	**********************************************

####
RUN
####

- Setup and configure event bus instance as explained on following link.

	* https://sourceforge.net/apps/trac/sla-at-soi/wiki/ExecutePlatform#EventBus

- Setup and configure provisioning infrastructure as explained on following link.

	* https://sourceforge.net/apps/trac/sla-at-soi/wiki/ExecutePlatform#ProvisioningLayerConfiguration
	
- Launch the SyntaxConverter, which currently is available as a service that is external with respect to the platform:
	* change to directory to:
	/trunk/generic-slamanager/syntax-converter

- Start the syc-broker application using either the Linux script (SycBroker) or the Windows one (SycBroker.bat).

- Prepare Pax-runner configuration:
	* change directory to:
	/trunk/pax-runner
	
- Launch the platform

	* From within the same directory simply type the command:
	  pax-run
	  
	  
It will take some time before the entire SLA@SOI platform will be launched. The platform consist of more than 200 bundles that will get loaded after this command.
In order to verify the completion of the launch of platform, after sometime type type the OSGi console command 'ss'.

You should be able to see all the bundles in ACTIVE state except for 
	* FragmentBundles (not meant for ACTIVE state)
	* org.ops4j.pax.configmanager (the logging control bundle) 
	
- (Optional) to avoid detailed log prints, start the bundle org.ops4j.pax.configmanager by typing at the OSGi console:
	start <n>
	where <n> is the number corresponding to the  org.ops4j.pax.configmanager bundle (n=95 in current release)

- After some time the SLA@SOI platform is up and running waiting for requests to be fulfilled.