@ECHO OFF


set package-version=v2
set platform-version=0.1-SNAPSHOT


ECHO creation of platfrom component source toolkit for business-manager....
CD ..\..\
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/business-manager/src-distribution.xml -Dmaven.test.skip=true -fae

:loop1
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip business-manager-%package-version%-src.zip
    GOTO cycle2
) ELSE (
   GOTO loop1
)

:cycle2
ECHO creation of platfrom component source toolkit for business-slamanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/business-slamanager/src-distribution.xml -Dmaven.test.skip=true -fae

:loop2
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip business-slamanager-%package-version%-src.zip
    GOTO cycle3
) ELSE (
   GOTO loop2
)

:cycle3
ECHO creation of platfrom component source toolkit for business-wstypes....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/business-wstypes/src-distribution.xml -Dmaven.test.skip=true -fae
:loop3
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip business-wstypes-%package-version%-src.zip
    GOTO cycle4
) ELSE (
   GOTO loop3
)

:cycle4
ECHO creation of platfrom component source toolkit for generic-slamanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/generic-slamanager/src-distribution.xml -Dmaven.test.skip=true -fae
:loop4
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip generic-slamanager-%package-version%-src.zip
    GOTO cycle5
) ELSE (
   GOTO loop4
)

:cycle5
ECHO creation of platfrom component source toolkit for infrastructure-monitoring....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/infrastructure-monitoring/src-distribution.xml -Dmaven.test.skip=true -fae
:loop5
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip infrastructure-monitoring-%package-version%-src.zip
    GOTO cycle6
) ELSE (
   GOTO loop5
)

:cycle6
ECHO creation of platfrom component source toolkit for infrastructure-servicemanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/infrastructure-servicemanager/src-distribution.xml -Dmaven.test.skip=true -fae
:loop6
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip infrastructure-servicemanager-%package-version%-src.zip
    GOTO cycle7
) ELSE (
   GOTO loop6
)

:cycle7
ECHO creation of platfrom component source toolkit for infrastructure-slamanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/infrastructure-slamanager/src-distribution.xml -Dmaven.test.skip=true -fae
:loop7
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip infrastructure-slamanager-%package-version%-src.zip
    GOTO cycle8
) ELSE (
   GOTO loop7
)

:cycle8
ECHO creation of platfrom component source toolkit for manageability-agent....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/manageability-agent/src-distribution.xml -Dmaven.test.skip=true -fae
:loop8
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip manageability-agent-%package-version%-src.zip
    GOTO cycle9
) ELSE (
   GOTO loop8
)


:cycle9
ECHO creation of platfrom component source toolkit for monitoring-system....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/monitoring-system/src-distribution.xml -Dmaven.test.skip=true -fae
:loop9
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip monitoring-system-%package-version%-src.zip
    GOTO cycle10
) ELSE (
   GOTO loop9
)

:cycle10
ECHO creation of platfrom component source toolkit for scm....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/scm/src-distribution.xml -Dmaven.test.skip=true -fae
:loop10
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip scm-%package-version%-src.zip
    GOTO cycle11
) ELSE (
   GOTO loop10
)

:cycle11
ECHO creation of platfrom component source toolkit for service-advertisement....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/service-advertisement/src-distribution.xml -Dmaven.test.skip=true -fae
:loop11
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip service-advertisement-%package-version%-src.zip
    GOTO cycle12
) ELSE (
   GOTO loop11
)

:cycle12
ECHO creation of platfrom component source toolkit for service-evaluation....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/service-evaluation/src-distribution.xml -Dmaven.test.skip=true -fae
:loop12
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip service-evaluation-%package-version%-src.zip
    GOTO cycle13
) ELSE (
   GOTO loop12
)

:cycle13
ECHO creation of platfrom component source toolkit for slamodel....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/slamodel/src-distribution.xml -Dmaven.test.skip=true -fae
:loop13
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip slamodel-%package-version%-src.zip
    GOTO cycle14
) ELSE (
   GOTO loop13
)

:cycle14
ECHO creation of platfrom component source toolkit for software-servicemanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/software-servicemanager/src-distribution.xml -Dmaven.test.skip=true -fae
:loop14
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip software-servicemanager-%package-version%-src.zip
    GOTO cycle15
) ELSE (
   GOTO loop14
)

:cycle15
ECHO creation of platfrom component source toolkit for software-slamanager....
cmd /C mvn assembly:assembly -Ddescriptor=resources/components-distribution/software-slamanager/src-distribution.xml -Dmaven.test.skip=true -fae
:loop15
IF EXIST target\slasoi-%platform-version%-src.zip (
    TIMEOUT /T 5  
    rename target\slasoi-%platform-version%-src.zip software-slamanager-%package-version%-src.zip
    GOTO END
) ELSE (
   GOTO loop15
)

:END
