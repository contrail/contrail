/**
 *  SVN FILE: $Id: VirtualMachineArtefact.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/VirtualMachineArtefact.java $
 */

package org.slasoi.models.scm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Virtual Machine Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.VirtualMachineArtefact#getNumberOfCores <em>Number Of Cores</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualMachineArtefact#getMemorySize <em>Memory Size</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuSpeed <em>Cpu Speed</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuType <em>Cpu Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualMachineArtefact()
 * @model
 * @generated
 */
public interface VirtualMachineArtefact extends ResourceArtefact {
	/**
	 * Returns the value of the '<em><b>Number Of Cores</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Cores</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Cores</em>' attribute.
	 * @see #setNumberOfCores(int)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualMachineArtefact_NumberOfCores()
	 * @model
	 * @generated
	 */
	int getNumberOfCores();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualMachineArtefact#getNumberOfCores <em>Number Of Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Cores</em>' attribute.
	 * @see #getNumberOfCores()
	 * @generated
	 */
	void setNumberOfCores(int value);

	/**
	 * Returns the value of the '<em><b>Cpu Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Speed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Speed</em>' attribute.
	 * @see #setCpuSpeed(double)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualMachineArtefact_CpuSpeed()
	 * @model
	 * @generated
	 */
	double getCpuSpeed();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuSpeed <em>Cpu Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Speed</em>' attribute.
	 * @see #getCpuSpeed()
	 * @generated
	 */
	void setCpuSpeed(double value);

	/**
	 * Returns the value of the '<em><b>Cpu Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Type</em>' attribute.
	 * @see #setCpuType(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualMachineArtefact_CpuType()
	 * @model
	 * @generated
	 */
	String getCpuType();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuType <em>Cpu Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Type</em>' attribute.
	 * @see #getCpuType()
	 * @generated
	 */
	void setCpuType(String value);

	/**
	 * Returns the value of the '<em><b>Memory Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Size</em>' attribute.
	 * @see #setMemorySize(double)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualMachineArtefact_MemorySize()
	 * @model
	 * @generated
	 */
	double getMemorySize();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualMachineArtefact#getMemorySize <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memory Size</em>' attribute.
	 * @see #getMemorySize()
	 * @generated
	 */
	void setMemorySize(double value);

} // VirtualMachineArtefact
