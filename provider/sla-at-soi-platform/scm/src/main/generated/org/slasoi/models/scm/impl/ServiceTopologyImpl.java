/**
 *  SVN FILE: $Id: ServiceTopologyImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceTopologyImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.ServiceLandscapeElement;
import org.slasoi.models.scm.ServiceTopology;
import org.slasoi.models.scm.ServiceType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Topology</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceTopologyImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceTopologyImpl#getServiceTopologyName <em>Service Topology Name</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceTopologyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceTopologyImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceTopologyImpl#getLandscapeElementsList <em>Landscape Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceTopologyImpl extends EObjectImpl implements ServiceTopology {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getServiceTopologyName() <em>Service Topology Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceTopologyName()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVICE_TOPOLOGY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServiceTopologyName() <em>Service Topology Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceTopologyName()
	 * @generated
	 * @ordered
	 */
	protected String serviceTopologyName = SERVICE_TOPOLOGY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ServiceType type;

	/**
	 * The cached value of the '{@link #getLandscapeElementsList() <em>Landscape Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLandscapeElementsList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceLandscapeElement> landscapeElements;

	/**
	 * The empty value for the '{@link #getLandscapeElements() <em>Landscape Elements</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLandscapeElements()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceLandscapeElement[] LANDSCAPE_ELEMENTS_EEMPTY_ARRAY = new ServiceLandscapeElement [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceTopologyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.SERVICE_TOPOLOGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getServiceTopologyName() {
		return serviceTopologyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceTopologyName(String newServiceTopologyName) {
		String oldServiceTopologyName = serviceTopologyName;
		serviceTopologyName = newServiceTopologyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME, oldServiceTopologyName, serviceTopologyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ServiceType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ServiceType newType) {
		ServiceType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceLandscapeElement[] getLandscapeElements() {
		if (landscapeElements == null || landscapeElements.isEmpty()) return LANDSCAPE_ELEMENTS_EEMPTY_ARRAY;
		BasicEList<ServiceLandscapeElement> list = (BasicEList<ServiceLandscapeElement>)landscapeElements;
		list.shrink();
		return (ServiceLandscapeElement[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceLandscapeElement getLandscapeElements(int index) {
		return getLandscapeElementsList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLandscapeElementsLength() {
		return landscapeElements == null ? 0 : landscapeElements.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLandscapeElements(ServiceLandscapeElement[] newLandscapeElements) {
		((BasicEList<ServiceLandscapeElement>)getLandscapeElementsList()).setData(newLandscapeElements.length, newLandscapeElements);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLandscapeElements(int index, ServiceLandscapeElement element) {
		getLandscapeElementsList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceLandscapeElement> getLandscapeElementsList() {
		if (landscapeElements == null) {
			landscapeElements = new EObjectContainmentEList<ServiceLandscapeElement>(ServiceLandscapeElement.class, this, ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS);
		}
		return landscapeElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS:
				return ((InternalEList<?>)getLandscapeElementsList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__ID:
				return getID();
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME:
				return getServiceTopologyName();
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__DESCRIPTION:
				return getDescription();
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS:
				return getLandscapeElementsList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__ID:
				setID((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME:
				setServiceTopologyName((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE:
				setType((ServiceType)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS:
				getLandscapeElementsList().clear();
				getLandscapeElementsList().addAll((Collection<? extends ServiceLandscapeElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__ID:
				setID(ID_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME:
				setServiceTopologyName(SERVICE_TOPOLOGY_NAME_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE:
				setType((ServiceType)null);
				return;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS:
				getLandscapeElementsList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME:
				return SERVICE_TOPOLOGY_NAME_EDEFAULT == null ? serviceTopologyName != null : !SERVICE_TOPOLOGY_NAME_EDEFAULT.equals(serviceTopologyName);
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__TYPE:
				return type != null;
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS:
				return landscapeElements != null && !landscapeElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ID: ");
		result.append(id);
		result.append(", ServiceTopologyName: ");
		result.append(serviceTopologyName);
		result.append(", Description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //ServiceTopologyImpl
