/**
 *  SVN FILE: $Id: FeaturesPackage.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/monitoring/common/features/FeaturesPackage.java $
 */

package org.slasoi.monitoring.common.features;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.slasoi.monitoring.common.features.FeaturesFactory
 * @model kind="package"
 * @generated
 */
public interface FeaturesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "features";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://monitoring.slasoi.org/common/features/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "features";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeaturesPackage eINSTANCE = org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.MonitoringFeatureImpl <em>Monitoring Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.MonitoringFeatureImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getMonitoringFeature()
	 * @generated
	 */
	int MONITORING_FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_FEATURE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_FEATURE__DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>Monitoring Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_FEATURE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.BasicImpl <em>Basic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.BasicImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getBasic()
	 * @generated
	 */
	int BASIC = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC__NAME = MONITORING_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC__DESCRIPTION = MONITORING_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC__TYPE = MONITORING_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_FEATURE_COUNT = MONITORING_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.FunctionImpl <em>Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.FunctionImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getFunction()
	 * @generated
	 */
	int FUNCTION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__NAME = MONITORING_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__DESCRIPTION = MONITORING_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Input</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__INPUT = MONITORING_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__OUTPUT = MONITORING_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_FEATURE_COUNT = MONITORING_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl <em>Component Monitoring Features</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getComponentMonitoringFeatures()
	 * @generated
	 */
	int COMPONENT_MONITORING_FEATURES = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MONITORING_FEATURES__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MONITORING_FEATURES__UUID = 1;

	/**
	 * The feature id for the '<em><b>Monitoring Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES = 2;

	/**
	 * The number of structural features of the '<em>Component Monitoring Features</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MONITORING_FEATURES_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.PrimitiveImpl <em>Primitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.PrimitiveImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getPrimitive()
	 * @generated
	 */
	int PRIMITIVE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE__NAME = BASIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE__DESCRIPTION = BASIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE__TYPE = BASIC__TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE__UNIT = BASIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_FEATURE_COUNT = BASIC_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.features.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.features.impl.EventImpl
	 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = BASIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DESCRIPTION = BASIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TYPE = BASIC__TYPE;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = BASIC_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.MonitoringFeature <em>Monitoring Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring Feature</em>'.
	 * @see org.slasoi.monitoring.common.features.MonitoringFeature
	 * @generated
	 */
	EClass getMonitoringFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.MonitoringFeature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.slasoi.monitoring.common.features.MonitoringFeature#getName()
	 * @see #getMonitoringFeature()
	 * @generated
	 */
	EAttribute getMonitoringFeature_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.MonitoringFeature#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.slasoi.monitoring.common.features.MonitoringFeature#getDescription()
	 * @see #getMonitoringFeature()
	 * @generated
	 */
	EAttribute getMonitoringFeature_Description();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.Basic <em>Basic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic</em>'.
	 * @see org.slasoi.monitoring.common.features.Basic
	 * @generated
	 */
	EClass getBasic();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.Basic#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.slasoi.monitoring.common.features.Basic#getType()
	 * @see #getBasic()
	 * @generated
	 */
	EAttribute getBasic_Type();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.Function <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function</em>'.
	 * @see org.slasoi.monitoring.common.features.Function
	 * @generated
	 */
	EClass getFunction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.slasoi.monitoring.common.features.Function#getInputList <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input</em>'.
	 * @see org.slasoi.monitoring.common.features.Function#getInputList()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_Input();

	/**
	 * Returns the meta object for the containment reference '{@link org.slasoi.monitoring.common.features.Function#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output</em>'.
	 * @see org.slasoi.monitoring.common.features.Function#getOutput()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_Output();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.ComponentMonitoringFeatures <em>Component Monitoring Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Monitoring Features</em>'.
	 * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
	 * @generated
	 */
	EClass getComponentMonitoringFeatures();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getType()
	 * @see #getComponentMonitoringFeatures()
	 * @generated
	 */
	EAttribute getComponentMonitoringFeatures_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getUuid()
	 * @see #getComponentMonitoringFeatures()
	 * @generated
	 */
	EAttribute getComponentMonitoringFeatures_Uuid();

	/**
	 * Returns the meta object for the containment reference list '{@link org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getMonitoringFeaturesList <em>Monitoring Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Monitoring Features</em>'.
	 * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures#getMonitoringFeaturesList()
	 * @see #getComponentMonitoringFeatures()
	 * @generated
	 */
	EReference getComponentMonitoringFeatures_MonitoringFeatures();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.Primitive <em>Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive</em>'.
	 * @see org.slasoi.monitoring.common.features.Primitive
	 * @generated
	 */
	EClass getPrimitive();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.features.Primitive#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see org.slasoi.monitoring.common.features.Primitive#getUnit()
	 * @see #getPrimitive()
	 * @generated
	 */
	EAttribute getPrimitive_Unit();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.features.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.slasoi.monitoring.common.features.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeaturesFactory getFeaturesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.MonitoringFeatureImpl <em>Monitoring Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.MonitoringFeatureImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getMonitoringFeature()
		 * @generated
		 */
		EClass MONITORING_FEATURE = eINSTANCE.getMonitoringFeature();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_FEATURE__NAME = eINSTANCE.getMonitoringFeature_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_FEATURE__DESCRIPTION = eINSTANCE.getMonitoringFeature_Description();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.BasicImpl <em>Basic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.BasicImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getBasic()
		 * @generated
		 */
		EClass BASIC = eINSTANCE.getBasic();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC__TYPE = eINSTANCE.getBasic_Type();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.FunctionImpl <em>Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.FunctionImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getFunction()
		 * @generated
		 */
		EClass FUNCTION = eINSTANCE.getFunction();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__INPUT = eINSTANCE.getFunction_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__OUTPUT = eINSTANCE.getFunction_Output();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl <em>Component Monitoring Features</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getComponentMonitoringFeatures()
		 * @generated
		 */
		EClass COMPONENT_MONITORING_FEATURES = eINSTANCE.getComponentMonitoringFeatures();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_MONITORING_FEATURES__TYPE = eINSTANCE.getComponentMonitoringFeatures_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_MONITORING_FEATURES__UUID = eINSTANCE.getComponentMonitoringFeatures_Uuid();

		/**
		 * The meta object literal for the '<em><b>Monitoring Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES = eINSTANCE.getComponentMonitoringFeatures_MonitoringFeatures();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.PrimitiveImpl <em>Primitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.PrimitiveImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getPrimitive()
		 * @generated
		 */
		EClass PRIMITIVE = eINSTANCE.getPrimitive();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE__UNIT = eINSTANCE.getPrimitive_Unit();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.features.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.features.impl.EventImpl
		 * @see org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

	}

} //FeaturesPackage
