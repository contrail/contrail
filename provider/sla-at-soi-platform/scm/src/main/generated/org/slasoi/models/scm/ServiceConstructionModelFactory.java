/**
 *  SVN FILE: $Id: ServiceConstructionModelFactory.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/ServiceConstructionModelFactory.java $
 */

package org.slasoi.models.scm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage
 * @generated
 */
public interface ServiceConstructionModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ServiceConstructionModelFactory eINSTANCE = org.slasoi.models.scm.impl.ServiceConstructionModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Service Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Type</em>'.
	 * @generated
	 */
	ServiceType createServiceType();

	/**
	 * Returns a new object of class '<em>Service Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Implementation</em>'.
	 * @generated
	 */
	ServiceImplementation createServiceImplementation();

	/**
	 * Returns a new object of class '<em>Service Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Builder</em>'.
	 * @generated
	 */
	ServiceBuilder createServiceBuilder();

	/**
	 * Returns a new object of class '<em>Service Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Binding</em>'.
	 * @generated
	 */
	ServiceBinding createServiceBinding();

	/**
	 * Returns a new object of class '<em>Service Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Instance</em>'.
	 * @generated
	 */
	ServiceInstance createServiceInstance();

	/**
	 * Returns a new object of class '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dependency</em>'.
	 * @generated
	 */
	Dependency createDependency();

	/**
	 * Returns a new object of class '<em>Virtual Appliance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Virtual Appliance</em>'.
	 * @generated
	 */
	VirtualAppliance createVirtualAppliance();

	/**
	 * Returns a new object of class '<em>Software Archive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Archive</em>'.
	 * @generated
	 */
	SoftwareArchive createSoftwareArchive();

	/**
	 * Returns a new object of class '<em>Configurable Service Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configurable Service Feature</em>'.
	 * @generated
	 */
	ConfigurableServiceFeature createConfigurableServiceFeature();

	/**
	 * Returns a new object of class '<em>Service Topology</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Topology</em>'.
	 * @generated
	 */
	ServiceTopology createServiceTopology();

	/**
	 * Returns a new object of class '<em>Service Landscape Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Landscape Element</em>'.
	 * @generated
	 */
	ServiceLandscapeElement createServiceLandscapeElement();

	/**
	 * Returns a new object of class '<em>Software Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Element</em>'.
	 * @generated
	 */
	SoftwareElement createSoftwareElement();

	/**
	 * Returns a new object of class '<em>Web Service Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Web Service Element</em>'.
	 * @generated
	 */
	WebServiceElement createWebServiceElement();

	/**
	 * Returns a new object of class '<em>Composite Web Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Web Service</em>'.
	 * @generated
	 */
	CompositeWebService createCompositeWebService();

	/**
	 * Returns a new object of class '<em>Atomic Web Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atomic Web Service</em>'.
	 * @generated
	 */
	AtomicWebService createAtomicWebService();

	/**
	 * Returns a new object of class '<em>Application Software</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Application Software</em>'.
	 * @generated
	 */
	ApplicationSoftware createApplicationSoftware();

	/**
	 * Returns a new object of class '<em>Execution Software</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Software</em>'.
	 * @generated
	 */
	ExecutionSoftware createExecutionSoftware();

	/**
	 * Returns a new object of class '<em>Configuration Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration Directive</em>'.
	 * @generated
	 */
	ConfigurationDirective createConfigurationDirective();

	/**
	 * Returns a new object of class '<em>Landscape</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Landscape</em>'.
	 * @generated
	 */
	Landscape createLandscape();

	/**
	 * Returns a new object of class '<em>Provisioning Information</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provisioning Information</em>'.
	 * @generated
	 */
	ProvisioningInformation createProvisioningInformation();

	/**
	 * Returns a new object of class '<em>Virtual Machine Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Virtual Machine Artefact</em>'.
	 * @generated
	 */
	VirtualMachineArtefact createVirtualMachineArtefact();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ServiceConstructionModelPackage getServiceConstructionModelPackage();

} //ServiceConstructionModelFactory
