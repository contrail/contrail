/**
 *  SVN FILE: $Id: ServiceBindingImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceBindingImpl.java $
 */

package org.slasoi.models.scm.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.ServiceConstructionModelPackage;

import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBindingImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBindingImpl#getSlaTemplate <em>Sla Template</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceBindingImpl extends EObjectImpl implements ServiceBinding {
	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected Dependency dependency;

	/**
	 * The default value of the '{@link #getSlaTemplate() <em>Sla Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlaTemplate()
	 * @generated
	 * @ordered
	 */
	protected static final SLATemplate SLA_TEMPLATE_EDEFAULT = (SLATemplate)ServiceConstructionModelFactory.eINSTANCE.createFromString(ServiceConstructionModelPackage.eINSTANCE.getSLATemplate(), "");

	/**
	 * The cached value of the '{@link #getSlaTemplate() <em>Sla Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlaTemplate()
	 * @generated
	 * @ordered
	 */
	protected SLATemplate slaTemplate = SLA_TEMPLATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.SERVICE_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency getDependency() {
		if (dependency != null && dependency.eIsProxy()) {
			InternalEObject oldDependency = (InternalEObject)dependency;
			dependency = (Dependency)eResolveProxy(oldDependency);
			if (dependency != oldDependency) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY, oldDependency, dependency));
			}
		}
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency basicGetDependency() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependency(Dependency newDependency) {
		Dependency oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY, oldDependency, dependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SLATemplate getSlaTemplate() {
		return slaTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlaTemplate(SLATemplate newSlaTemplate) {
		SLATemplate oldSlaTemplate = slaTemplate;
		slaTemplate = newSlaTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BINDING__SLA_TEMPLATE, oldSlaTemplate, slaTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY:
				if (resolve) return getDependency();
				return basicGetDependency();
			case ServiceConstructionModelPackage.SERVICE_BINDING__SLA_TEMPLATE:
				return getSlaTemplate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY:
				setDependency((Dependency)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_BINDING__SLA_TEMPLATE:
				setSlaTemplate((SLATemplate)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY:
				setDependency((Dependency)null);
				return;
			case ServiceConstructionModelPackage.SERVICE_BINDING__SLA_TEMPLATE:
				setSlaTemplate(SLA_TEMPLATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BINDING__DEPENDENCY:
				return dependency != null;
			case ServiceConstructionModelPackage.SERVICE_BINDING__SLA_TEMPLATE:
				return SLA_TEMPLATE_EDEFAULT == null ? slaTemplate != null : !SLA_TEMPLATE_EDEFAULT.equals(slaTemplate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (slaTemplate: ");
		result.append(slaTemplate);
		result.append(')');
		return result.toString();
	}

} //ServiceBindingImpl
