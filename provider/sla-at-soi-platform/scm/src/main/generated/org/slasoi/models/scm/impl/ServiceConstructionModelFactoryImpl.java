/**
 *  SVN FILE: $Id: ServiceConstructionModelFactoryImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceConstructionModelFactoryImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.UUID;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.slasoi.models.scm.*;

import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServiceConstructionModelFactoryImpl extends EFactoryImpl implements ServiceConstructionModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ServiceConstructionModelFactory init() {
		try {
			ServiceConstructionModelFactory theServiceConstructionModelFactory = (ServiceConstructionModelFactory)EPackage.Registry.INSTANCE.getEFactory("http://scm.slasoi.org/1.0"); 
			if (theServiceConstructionModelFactory != null) {
				return theServiceConstructionModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ServiceConstructionModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceConstructionModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ServiceConstructionModelPackage.SERVICE_TYPE: return createServiceType();
			case ServiceConstructionModelPackage.SERVICE_IMPLEMENTATION: return createServiceImplementation();
			case ServiceConstructionModelPackage.SERVICE_BUILDER: return createServiceBuilder();
			case ServiceConstructionModelPackage.SERVICE_BINDING: return createServiceBinding();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE: return createServiceInstance();
			case ServiceConstructionModelPackage.DEPENDENCY: return createDependency();
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE: return createVirtualAppliance();
			case ServiceConstructionModelPackage.SOFTWARE_ARCHIVE: return createSoftwareArchive();
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE: return createConfigurableServiceFeature();
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY: return createServiceTopology();
			case ServiceConstructionModelPackage.SERVICE_LANDSCAPE_ELEMENT: return createServiceLandscapeElement();
			case ServiceConstructionModelPackage.SOFTWARE_ELEMENT: return createSoftwareElement();
			case ServiceConstructionModelPackage.WEB_SERVICE_ELEMENT: return createWebServiceElement();
			case ServiceConstructionModelPackage.COMPOSITE_WEB_SERVICE: return createCompositeWebService();
			case ServiceConstructionModelPackage.ATOMIC_WEB_SERVICE: return createAtomicWebService();
			case ServiceConstructionModelPackage.APPLICATION_SOFTWARE: return createApplicationSoftware();
			case ServiceConstructionModelPackage.EXECUTION_SOFTWARE: return createExecutionSoftware();
			case ServiceConstructionModelPackage.CONFIGURATION_DIRECTIVE: return createConfigurationDirective();
			case ServiceConstructionModelPackage.LANDSCAPE: return createLandscape();
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION: return createProvisioningInformation();
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT: return createVirtualMachineArtefact();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ServiceConstructionModelPackage.CONFIG_TYPE:
				return createConfigTypeFromString(eDataType, initialValue);
			case ServiceConstructionModelPackage.DEPENDENCY_TYPE:
				return createDependencyTypeFromString(eDataType, initialValue);
			case ServiceConstructionModelPackage.SLA_TEMPLATE:
				return createSLATemplateFromString(eDataType, initialValue);
			case ServiceConstructionModelPackage.ENDPOINT:
				return createEndpointFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ServiceConstructionModelPackage.CONFIG_TYPE:
				return convertConfigTypeToString(eDataType, instanceValue);
			case ServiceConstructionModelPackage.DEPENDENCY_TYPE:
				return convertDependencyTypeToString(eDataType, instanceValue);
			case ServiceConstructionModelPackage.SLA_TEMPLATE:
				return convertSLATemplateToString(eDataType, instanceValue);
			case ServiceConstructionModelPackage.ENDPOINT:
				return convertEndpointToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceType createServiceType() {
		ServiceTypeImpl serviceType = new ServiceTypeImpl();
        serviceType.setID(UUID.randomUUID().toString());
        return serviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceImplementation createServiceImplementation() {
		ServiceImplementationImpl serviceImplementation = new ServiceImplementationImpl();
        serviceImplementation.setID(UUID.randomUUID().toString());
        return serviceImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceBuilder createServiceBuilder() {
		ServiceBuilderImpl serviceBuilder = new ServiceBuilderImpl();
        serviceBuilder.setUuid(UUID.randomUUID().toString());
        return serviceBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceBinding createServiceBinding() {
		ServiceBindingImpl serviceBinding = new ServiceBindingImpl();
		return serviceBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceInstance createServiceInstance() {
		ServiceInstanceImpl serviceInstance = new ServiceInstanceImpl();
        serviceInstance.setID(UUID.randomUUID().toString());
        return serviceInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Dependency createDependency() {
		DependencyImpl dependency = new DependencyImpl();
        dependency.setID(UUID.randomUUID().toString());
        return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public VirtualAppliance createVirtualAppliance() {
		VirtualApplianceImpl virtualAppliance = new VirtualApplianceImpl();
        virtualAppliance.setID(UUID.randomUUID().toString());
        return virtualAppliance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SoftwareArchive createSoftwareArchive() {
		 SoftwareArchiveImpl softwareArchive = new SoftwareArchiveImpl();
         softwareArchive.setID(UUID.randomUUID().toString());
         return softwareArchive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ConfigurableServiceFeature createConfigurableServiceFeature() {
		   ConfigurableServiceFeatureImpl configurableServiceFeature = new ConfigurableServiceFeatureImpl();
           configurableServiceFeature.setID(UUID.randomUUID().toString());
           return configurableServiceFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceTopology createServiceTopology() {
		ServiceTopologyImpl serviceTopology = new ServiceTopologyImpl();
        serviceTopology.setID(UUID.randomUUID().toString());
        return serviceTopology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ServiceLandscapeElement createServiceLandscapeElement() {
		ServiceLandscapeElementImpl serviceLandscapeElement = new ServiceLandscapeElementImpl();
        serviceLandscapeElement.setID(UUID.randomUUID().toString());
        return serviceLandscapeElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SoftwareElement createSoftwareElement() {
		SoftwareElementImpl softwareElement = new SoftwareElementImpl();
        softwareElement.setID(UUID.randomUUID().toString());
        return softwareElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public WebServiceElement createWebServiceElement() {
		 WebServiceElementImpl webServiceElement = new WebServiceElementImpl();
         webServiceElement.setID(UUID.randomUUID().toString());
         return webServiceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CompositeWebService createCompositeWebService() {
		CompositeWebServiceImpl compositeWebService = new CompositeWebServiceImpl();
        compositeWebService.setID(UUID.randomUUID().toString());
        return compositeWebService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public AtomicWebService createAtomicWebService() {
		 AtomicWebServiceImpl atomicWebService = new AtomicWebServiceImpl();
         atomicWebService.setID(UUID.randomUUID().toString());
         return atomicWebService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ApplicationSoftware createApplicationSoftware() {
		ApplicationSoftwareImpl applicationSoftware = new ApplicationSoftwareImpl();
        applicationSoftware.setID(UUID.randomUUID().toString());
        return applicationSoftware;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ExecutionSoftware createExecutionSoftware() {
		ExecutionSoftwareImpl executionSoftware = new ExecutionSoftwareImpl();
        executionSoftware.setID(UUID.randomUUID().toString());
        return executionSoftware;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ConfigurationDirective createConfigurationDirective() {
		ConfigurationDirectiveImpl configurationDirective = new ConfigurationDirectiveImpl();
        return configurationDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Landscape createLandscape() {
		LandscapeImpl landscape = new LandscapeImpl();
		return landscape;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ProvisioningInformation createProvisioningInformation() {
		ProvisioningInformationImpl provisioningInformation = new ProvisioningInformationImpl();
		return provisioningInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VirtualMachineArtefact createVirtualMachineArtefact() {
		VirtualMachineArtefactImpl virtualMachineArtefact = new VirtualMachineArtefactImpl();
		return virtualMachineArtefact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigType createConfigTypeFromString(EDataType eDataType, String initialValue) {
		ConfigType result = ConfigType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConfigTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType createDependencyTypeFromString(EDataType eDataType, String initialValue) {
		DependencyType result = DependencyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDependencyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SLATemplate createSLATemplateFromString(EDataType eDataType, String initialValue) {
		return null; //(SLATemplate)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSLATemplateToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Endpoint createEndpointFromString(EDataType eDataType, String initialValue) {
		return (Endpoint)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEndpointToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceConstructionModelPackage getServiceConstructionModelPackage() {
		return (ServiceConstructionModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ServiceConstructionModelPackage getPackage() {
		return ServiceConstructionModelPackage.eINSTANCE;
	}

} //ServiceConstructionModelFactoryImpl
