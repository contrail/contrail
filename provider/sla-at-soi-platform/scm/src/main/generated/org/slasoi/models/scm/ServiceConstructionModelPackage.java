/**
 *  SVN FILE: $Id: ServiceConstructionModelPackage.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/ServiceConstructionModelPackage.java $
 */

package org.slasoi.models.scm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.slasoi.models.scm.ServiceConstructionModelFactory
 * @model kind="package"
 * @generated
 */
public interface ServiceConstructionModelPackage extends EPackage {
    /**
     * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "scm";

    /**
     * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://scm.slasoi.org/1.0";

    /**
     * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "scm";

    /**
     * The singleton instance of the package. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    ServiceConstructionModelPackage eINSTANCE = org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl.init();

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceTypeImpl <em>Service Type</em>}' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceTypeImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceType()
     * @generated
     */
    int SERVICE_TYPE = 0;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TYPE__ID = 0;

    /**
     * The feature id for the '<em><b>Service Type Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TYPE__SERVICE_TYPE_NAME = 1;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TYPE__DESCRIPTION = 2;

    /**
     * The feature id for the '<em><b>Interfaces</b></em>' attribute list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TYPE__INTERFACES = 3;

    /**
     * The number of structural features of the '<em>Service Type</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TYPE_FEATURE_COUNT = 4;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceImplementationImpl
     * <em>Service Implementation</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceImplementationImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceImplementation()
     * @generated
     */
    int SERVICE_IMPLEMENTATION = 1;

    /**
     * The feature id for the '<em><b>Type</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__TYPE = 0;

    /**
     * The feature id for the '<em><b>Artefacts</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__ARTEFACTS = 1;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__ID = 2;

    /**
     * The feature id for the '<em><b>Service Implementation Name</b></em>' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__SERVICE_IMPLEMENTATION_NAME = 3;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__DESCRIPTION = 4;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__VERSION = 5;

    /**
     * The feature id for the '<em><b>Provisioning Information</b></em>' containment reference. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__PROVISIONING_INFORMATION = 6;

    /**
     * The feature id for the '<em><b>Component Monitoring Features</b></em>' containment reference list. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION__COMPONENT_MONITORING_FEATURES = 7;

    /**
     * The number of structural features of the '<em>Service Implementation</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_IMPLEMENTATION_FEATURE_COUNT = 8;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceBuilderImpl <em>Service Builder</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceBuilderImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceBuilder()
     * @generated
     */
    int SERVICE_BUILDER = 2;

    /**
     * The feature id for the '<em><b>Implementation</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER__IMPLEMENTATION = 0;

    /**
     * The feature id for the '<em><b>Bindings</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER__BINDINGS = 1;

    /**
     * The feature id for the '<em><b>Service Configurations</b></em>' containment reference list. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER__SERVICE_CONFIGURATIONS = 2;

    /**
     * The feature id for the '<em><b>Uuid</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER__UUID = 3;

    /**
     * The feature id for the '<em><b>Monitoring System Configuration</b></em>' containment reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION = 4;

    /**
     * The number of structural features of the '<em>Service Builder</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BUILDER_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceBindingImpl <em>Service Binding</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceBindingImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceBinding()
     * @generated
     */
    int SERVICE_BINDING = 3;

    /**
     * The feature id for the '<em><b>Dependency</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BINDING__DEPENDENCY = 0;

    /**
     * The feature id for the '<em><b>Sla Template</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BINDING__SLA_TEMPLATE = 1;

    /**
     * The number of structural features of the '<em>Service Binding</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_BINDING_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceInstanceImpl <em>Service Instance</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceInstanceImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceInstance()
     * @generated
     */
    int SERVICE_INSTANCE = 4;

    /**
     * The feature id for the '<em><b>Implementation</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__IMPLEMENTATION = 0;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__ID = 1;

    /**
     * The feature id for the '<em><b>Service Instance Name</b></em>' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__SERVICE_INSTANCE_NAME = 2;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__DESCRIPTION = 3;

    /**
     * The feature id for the '<em><b>Instanced On</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__INSTANCED_ON = 4;

    /**
     * The feature id for the '<em><b>Endpoints</b></em>' attribute list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE__ENDPOINTS = 5;

    /**
     * The number of structural features of the '<em>Service Instance</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_INSTANCE_FEATURE_COUNT = 6;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl
     * <em>Implementation Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ImplementationArtefactImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getImplementationArtefact()
     * @generated
     */
    int IMPLEMENTATION_ARTEFACT = 5;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT__ID = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT__NAME = 1;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT__DESCIPTION = 2;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT__DEPENDENCIES = 3;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES = 4;

    /**
     * The number of structural features of the '<em>Implementation Artefact</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPLEMENTATION_ARTEFACT_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.DependencyImpl <em>Dependency</em>}' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.DependencyImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDependency()
     * @generated
     */
    int DEPENDENCY = 6;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__NAME = 0;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__DESCRIPTION = 1;

    /**
     * The feature id for the '<em><b>Target URI</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__TARGET_URI = 2;

    /**
     * The feature id for the '<em><b>Target Type</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__TARGET_TYPE = 3;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__ID = 4;

    /**
     * The feature id for the '<em><b>Dep Type</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY__DEP_TYPE = 5;

    /**
     * The number of structural features of the '<em>Dependency</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     * @ordered
     */
    int DEPENDENCY_FEATURE_COUNT = 6;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.DeploymentArtefactImpl
     * <em>Deployment Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.DeploymentArtefactImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDeploymentArtefact()
     * @generated
     */
    int DEPLOYMENT_ARTEFACT = 7;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT__ID = IMPLEMENTATION_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT__NAME = IMPLEMENTATION_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT__DESCIPTION = IMPLEMENTATION_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT__DEPENDENCIES = IMPLEMENTATION_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT__SERVICE_FEATURES = IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES;

    /**
     * The number of structural features of the '<em>Deployment Artefact</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DEPLOYMENT_ARTEFACT_FEATURE_COUNT = IMPLEMENTATION_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.DataArtefactImpl <em>Data Artefact</em>}' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.DataArtefactImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDataArtefact()
     * @generated
     */
    int DATA_ARTEFACT = 8;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT__ID = IMPLEMENTATION_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT__NAME = IMPLEMENTATION_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT__DESCIPTION = IMPLEMENTATION_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT__DEPENDENCIES = IMPLEMENTATION_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT__SERVICE_FEATURES = IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES;

    /**
     * The number of structural features of the '<em>Data Artefact</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DATA_ARTEFACT_FEATURE_COUNT = IMPLEMENTATION_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.VirtualApplianceImpl <em>Virtual Appliance</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.VirtualApplianceImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getVirtualAppliance()
     * @generated
     */
    int VIRTUAL_APPLIANCE = 9;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__ID = DEPLOYMENT_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__NAME = DEPLOYMENT_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__DESCIPTION = DEPLOYMENT_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__DEPENDENCIES = DEPLOYMENT_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__SERVICE_FEATURES = DEPLOYMENT_ARTEFACT__SERVICE_FEATURES;

    /**
     * The feature id for the '<em><b>Hyper Visor</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__HYPER_VISOR = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__VERSION = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Hypervisor Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__HYPERVISOR_VERSION = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Appliance Type</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__APPLIANCE_TYPE = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Image Format</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE__IMAGE_FORMAT = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>Virtual Appliance</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_APPLIANCE_FEATURE_COUNT = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 5;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.SoftwareArchiveImpl <em>Software Archive</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.SoftwareArchiveImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSoftwareArchive()
     * @generated
     */
    int SOFTWARE_ARCHIVE = 10;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__ID = DEPLOYMENT_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__NAME = DEPLOYMENT_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__DESCIPTION = DEPLOYMENT_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__DEPENDENCIES = DEPLOYMENT_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__SERVICE_FEATURES = DEPLOYMENT_ARTEFACT__SERVICE_FEATURES;

    /**
     * The feature id for the '<em><b>Archive Format</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE__ARCHIVE_FORMAT = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Software Archive</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ARCHIVE_FEATURE_COUNT = DEPLOYMENT_ARTEFACT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl
     * <em>Configurable Service Feature</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigurableServiceFeature()
     * @generated
     */
    int CONFIGURABLE_SERVICE_FEATURE = 11;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE__ID = 0;

    /**
     * The feature id for the '<em><b>Config Type</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE = 1;

    /**
     * The feature id for the '<em><b>Config File</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE = 2;

    /**
     * The feature id for the '<em><b>Parameter Identifier</b></em>' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER = 3;

    /**
     * The feature id for the '<em><b>Default Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE = 4;

    /**
     * The number of structural features of the '<em>Configurable Service Feature</em>' class. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURABLE_SERVICE_FEATURE_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceTopologyImpl <em>Service Topology</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceTopologyImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceTopology()
     * @generated
     */
    int SERVICE_TOPOLOGY = 12;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY__ID = 0;

    /**
     * The feature id for the '<em><b>Service Topology Name</b></em>' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME = 1;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY__DESCRIPTION = 2;

    /**
     * The feature id for the '<em><b>Type</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY__TYPE = 3;

    /**
     * The feature id for the '<em><b>Landscape Elements</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS = 4;

    /**
     * The number of structural features of the '<em>Service Topology</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_TOPOLOGY_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ServiceLandscapeElementImpl
     * <em>Service Landscape Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ServiceLandscapeElementImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceLandscapeElement()
     * @generated
     */
    int SERVICE_LANDSCAPE_ELEMENT = 13;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_LANDSCAPE_ELEMENT__ID = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_LANDSCAPE_ELEMENT__NAME = 1;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_LANDSCAPE_ELEMENT__DESRIPTION = 2;

    /**
     * The number of structural features of the '<em>Service Landscape Element</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.SoftwareElementImpl <em>Software Element</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.SoftwareElementImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSoftwareElement()
     * @generated
     */
    int SOFTWARE_ELEMENT = 14;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__ID = SERVICE_LANDSCAPE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__NAME = SERVICE_LANDSCAPE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__DESRIPTION = SERVICE_LANDSCAPE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__VERSION = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__REQUIRES = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Provider</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT__PROVIDER = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Software Element</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOFTWARE_ELEMENT_FEATURE_COUNT = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.WebServiceElementImpl <em>Web Service Element</em>}
     * ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.WebServiceElementImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getWebServiceElement()
     * @generated
     */
    int WEB_SERVICE_ELEMENT = 15;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WEB_SERVICE_ELEMENT__ID = SERVICE_LANDSCAPE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WEB_SERVICE_ELEMENT__NAME = SERVICE_LANDSCAPE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WEB_SERVICE_ELEMENT__DESRIPTION = SERVICE_LANDSCAPE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WEB_SERVICE_ELEMENT__REQUIRES = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Web Service Element</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WEB_SERVICE_ELEMENT_FEATURE_COUNT = SERVICE_LANDSCAPE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.CompositeWebServiceImpl
     * <em>Composite Web Service</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.CompositeWebServiceImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getCompositeWebService()
     * @generated
     */
    int COMPOSITE_WEB_SERVICE = 16;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE__ID = WEB_SERVICE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE__NAME = WEB_SERVICE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE__DESRIPTION = WEB_SERVICE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE__REQUIRES = WEB_SERVICE_ELEMENT__REQUIRES;

    /**
     * The feature id for the '<em><b>Contains</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE__CONTAINS = WEB_SERVICE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Composite Web Service</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPOSITE_WEB_SERVICE_FEATURE_COUNT = WEB_SERVICE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.AtomicWebServiceImpl <em>Atomic Web Service</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.AtomicWebServiceImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getAtomicWebService()
     * @generated
     */
    int ATOMIC_WEB_SERVICE = 17;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ATOMIC_WEB_SERVICE__ID = WEB_SERVICE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ATOMIC_WEB_SERVICE__NAME = WEB_SERVICE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ATOMIC_WEB_SERVICE__DESRIPTION = WEB_SERVICE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ATOMIC_WEB_SERVICE__REQUIRES = WEB_SERVICE_ELEMENT__REQUIRES;

    /**
     * The number of structural features of the '<em>Atomic Web Service</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ATOMIC_WEB_SERVICE_FEATURE_COUNT = WEB_SERVICE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ApplicationSoftwareImpl
     * <em>Application Software</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ApplicationSoftwareImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getApplicationSoftware()
     * @generated
     */
    int APPLICATION_SOFTWARE = 18;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__ID = SOFTWARE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__NAME = SOFTWARE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__DESRIPTION = SOFTWARE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__VERSION = SOFTWARE_ELEMENT__VERSION;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__REQUIRES = SOFTWARE_ELEMENT__REQUIRES;

    /**
     * The feature id for the '<em><b>Provider</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE__PROVIDER = SOFTWARE_ELEMENT__PROVIDER;

    /**
     * The number of structural features of the '<em>Application Software</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int APPLICATION_SOFTWARE_FEATURE_COUNT = SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ExecutionSoftwareImpl <em>Execution Software</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ExecutionSoftwareImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getExecutionSoftware()
     * @generated
     */
    int EXECUTION_SOFTWARE = 19;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__ID = SOFTWARE_ELEMENT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__NAME = SOFTWARE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Desription</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__DESRIPTION = SOFTWARE_ELEMENT__DESRIPTION;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__VERSION = SOFTWARE_ELEMENT__VERSION;

    /**
     * The feature id for the '<em><b>Requires</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__REQUIRES = SOFTWARE_ELEMENT__REQUIRES;

    /**
     * The feature id for the '<em><b>Provider</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE__PROVIDER = SOFTWARE_ELEMENT__PROVIDER;

    /**
     * The number of structural features of the '<em>Execution Software</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_SOFTWARE_FEATURE_COUNT = SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ConfigurationDirectiveImpl
     * <em>Configuration Directive</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ConfigurationDirectiveImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigurationDirective()
     * @generated
     */
    int CONFIGURATION_DIRECTIVE = 20;

    /**
     * The feature id for the '<em><b>Parameter Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURATION_DIRECTIVE__PARAMETER_VALUE = 0;

    /**
     * The feature id for the '<em><b>Service Feature</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURATION_DIRECTIVE__SERVICE_FEATURE = 1;

    /**
     * The number of structural features of the '<em>Configuration Directive</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONFIGURATION_DIRECTIVE_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.LandscapeImpl <em>Landscape</em>}' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.LandscapeImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getLandscape()
     * @generated
     */
    int LANDSCAPE = 21;

    /**
     * The feature id for the '<em><b>Instances</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__INSTANCES = 0;

    /**
     * The feature id for the '<em><b>Provided Types</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__PROVIDED_TYPES = 1;

    /**
     * The feature id for the '<em><b>Implementations</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__IMPLEMENTATIONS = 2;

    /**
     * The feature id for the '<em><b>Topologies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__TOPOLOGIES = 3;

    /**
     * The feature id for the '<em><b>Builders</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__BUILDERS = 4;

    /**
     * The feature id for the '<em><b>Required Types</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE__REQUIRED_TYPES = 5;

    /**
     * The number of structural features of the '<em>Landscape</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     * @ordered
     */
    int LANDSCAPE_FEATURE_COUNT = 6;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ProvisioningInformationImpl
     * <em>Provisioning Information</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ProvisioningInformationImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getProvisioningInformation()
     * @generated
     */
    int PROVISIONING_INFORMATION = 22;

    /**
     * The feature id for the '<em><b>Lead Time</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROVISIONING_INFORMATION__LEAD_TIME = 0;

    /**
     * The feature id for the '<em><b>Boot Order</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROVISIONING_INFORMATION__BOOT_ORDER = 1;

    /**
     * The number of structural features of the '<em>Provisioning Information</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROVISIONING_INFORMATION_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.ResourceArtefactImpl <em>Resource Artefact</em>}'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.ResourceArtefactImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getResourceArtefact()
     * @generated
     */
    int RESOURCE_ARTEFACT = 23;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT__ID = IMPLEMENTATION_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT__NAME = IMPLEMENTATION_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT__DESCIPTION = IMPLEMENTATION_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT__DEPENDENCIES = IMPLEMENTATION_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT__SERVICE_FEATURES = IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES;

    /**
     * The number of structural features of the '<em>Resource Artefact</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_ARTEFACT_FEATURE_COUNT = IMPLEMENTATION_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl
     * <em>Virtual Machine Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.impl.VirtualMachineArtefactImpl
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getVirtualMachineArtefact()
     * @generated
     */
    int VIRTUAL_MACHINE_ARTEFACT = 24;

    /**
     * The feature id for the '<em><b>ID</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__ID = RESOURCE_ARTEFACT__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__NAME = RESOURCE_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Desciption</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__DESCIPTION = RESOURCE_ARTEFACT__DESCIPTION;

    /**
     * The feature id for the '<em><b>Dependencies</b></em>' containment reference list. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__DEPENDENCIES = RESOURCE_ARTEFACT__DEPENDENCIES;

    /**
     * The feature id for the '<em><b>Service Features</b></em>' containment reference list. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__SERVICE_FEATURES = RESOURCE_ARTEFACT__SERVICE_FEATURES;

    /**
     * The feature id for the '<em><b>Number Of Cores</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES = RESOURCE_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Memory Size</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE = RESOURCE_ARTEFACT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Cpu Speed</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED = RESOURCE_ARTEFACT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Cpu Type</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE = RESOURCE_ARTEFACT_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Virtual Machine Artefact</em>' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIRTUAL_MACHINE_ARTEFACT_FEATURE_COUNT = RESOURCE_ARTEFACT_FEATURE_COUNT + 4;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.ConfigType <em>Config Type</em>}' enum. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.ConfigType
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigType()
     * @generated
     */
    int CONFIG_TYPE = 25;

    /**
     * The meta object id for the '{@link org.slasoi.models.scm.DependencyType <em>Dependency Type</em>}' enum. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.models.scm.DependencyType
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDependencyType()
     * @generated
     */
    int DEPENDENCY_TYPE = 26;

    /**
     * The meta object id for the '<em>SLA Template</em>' data type. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.slamodel.sla.SLATemplate
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSLATemplate()
     * @generated
     */
    int SLA_TEMPLATE = 27;

    /**
     * The meta object id for the '<em>Endpoint</em>' data type. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.slasoi.slamodel.sla.Endpoint
     * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getEndpoint()
     * @generated
     */
    int ENDPOINT = 28;

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceType <em>Service Type</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Type</em>'.
     * @see org.slasoi.models.scm.ServiceType
     * @generated
     */
    EClass getServiceType();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceType#getID <em>ID</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ServiceType#getID()
     * @see #getServiceType()
     * @generated
     */
    EAttribute getServiceType_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceType#getServiceTypeName
     * <em>Service Type Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Type Name</em>'.
     * @see org.slasoi.models.scm.ServiceType#getServiceTypeName()
     * @see #getServiceType()
     * @generated
     */
    EAttribute getServiceType_ServiceTypeName();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceType#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.slasoi.models.scm.ServiceType#getDescription()
     * @see #getServiceType()
     * @generated
     */
    EAttribute getServiceType_Description();

    /**
     * Returns the meta object for the attribute list '{@link org.slasoi.models.scm.ServiceType#getInterfacesList
     * <em>Interfaces</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Interfaces</em>'.
     * @see org.slasoi.models.scm.ServiceType#getInterfacesList()
     * @see #getServiceType()
     * @generated
     */
    EAttribute getServiceType_Interfaces();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceImplementation
     * <em>Service Implementation</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Implementation</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation
     * @generated
     */
    EClass getServiceImplementation();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ServiceImplementation#getType
     * <em>Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Type</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getType()
     * @see #getServiceImplementation()
     * @generated
     */
    EReference getServiceImplementation_Type();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ServiceImplementation#getArtefactsList <em>Artefacts</em>}'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Artefacts</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getArtefactsList()
     * @see #getServiceImplementation()
     * @generated
     */
    EReference getServiceImplementation_Artefacts();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceImplementation#getID <em>ID</em>}
     * '. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getID()
     * @see #getServiceImplementation()
     * @generated
     */
    EAttribute getServiceImplementation_ID();

    /**
     * Returns the meta object for the attribute '
     * {@link org.slasoi.models.scm.ServiceImplementation#getServiceImplementationName
     * <em>Service Implementation Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Implementation Name</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getServiceImplementationName()
     * @see #getServiceImplementation()
     * @generated
     */
    EAttribute getServiceImplementation_ServiceImplementationName();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceImplementation#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getDescription()
     * @see #getServiceImplementation()
     * @generated
     */
    EAttribute getServiceImplementation_Description();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceImplementation#getVersion
     * <em>Version</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getVersion()
     * @see #getServiceImplementation()
     * @generated
     */
    EAttribute getServiceImplementation_Version();

    /**
     * Returns the meta object for the containment reference '
     * {@link org.slasoi.models.scm.ServiceImplementation#getProvisioningInformation <em>Provisioning Information</em>}
     * '. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Provisioning Information</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getProvisioningInformation()
     * @see #getServiceImplementation()
     * @generated
     */
    EReference getServiceImplementation_ProvisioningInformation();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ServiceImplementation#getComponentMonitoringFeaturesList
     * <em>Component Monitoring Features</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Component Monitoring Features</em>'.
     * @see org.slasoi.models.scm.ServiceImplementation#getComponentMonitoringFeaturesList()
     * @see #getServiceImplementation()
     * @generated
     */
    EReference getServiceImplementation_ComponentMonitoringFeatures();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceBuilder <em>Service Builder</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Builder</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder
     * @generated
     */
    EClass getServiceBuilder();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ServiceBuilder#getImplementation
     * <em>Implementation</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Implementation</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder#getImplementation()
     * @see #getServiceBuilder()
     * @generated
     */
    EReference getServiceBuilder_Implementation();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ServiceBuilder#getBindingsList <em>Bindings</em>}'. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Bindings</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder#getBindingsList()
     * @see #getServiceBuilder()
     * @generated
     */
    EReference getServiceBuilder_Bindings();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ServiceBuilder#getServiceConfigurationsList <em>Service Configurations</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Service Configurations</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder#getServiceConfigurationsList()
     * @see #getServiceBuilder()
     * @generated
     */
    EReference getServiceBuilder_ServiceConfigurations();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceBuilder#getUuid <em>Uuid</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Uuid</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder#getUuid()
     * @see #getServiceBuilder()
     * @generated
     */
    EAttribute getServiceBuilder_Uuid();

    /**
     * Returns the meta object for the containment reference '
     * {@link org.slasoi.models.scm.ServiceBuilder#getMonitoringSystemConfiguration
     * <em>Monitoring System Configuration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Monitoring System Configuration</em>'.
     * @see org.slasoi.models.scm.ServiceBuilder#getMonitoringSystemConfiguration()
     * @see #getServiceBuilder()
     * @generated
     */
    EReference getServiceBuilder_MonitoringSystemConfiguration();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceBinding <em>Service Binding</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Binding</em>'.
     * @see org.slasoi.models.scm.ServiceBinding
     * @generated
     */
    EClass getServiceBinding();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ServiceBinding#getDependency
     * <em>Dependency</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Dependency</em>'.
     * @see org.slasoi.models.scm.ServiceBinding#getDependency()
     * @see #getServiceBinding()
     * @generated
     */
    EReference getServiceBinding_Dependency();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceBinding#getSlaTemplate
     * <em>Sla Template</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sla Template</em>'.
     * @see org.slasoi.models.scm.ServiceBinding#getSlaTemplate()
     * @see #getServiceBinding()
     * @generated
     */
    EAttribute getServiceBinding_SlaTemplate();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceInstance <em>Service Instance</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Instance</em>'.
     * @see org.slasoi.models.scm.ServiceInstance
     * @generated
     */
    EClass getServiceInstance();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ServiceInstance#getImplementation
     * <em>Implementation</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Implementation</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getImplementation()
     * @see #getServiceInstance()
     * @generated
     */
    EReference getServiceInstance_Implementation();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceInstance#getID <em>ID</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getID()
     * @see #getServiceInstance()
     * @generated
     */
    EAttribute getServiceInstance_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceInstance#getServiceInstanceName
     * <em>Service Instance Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Instance Name</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getServiceInstanceName()
     * @see #getServiceInstance()
     * @generated
     */
    EAttribute getServiceInstance_ServiceInstanceName();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceInstance#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getDescription()
     * @see #getServiceInstance()
     * @generated
     */
    EAttribute getServiceInstance_Description();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceInstance#getInstancedOn
     * <em>Instanced On</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Instanced On</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getInstancedOn()
     * @see #getServiceInstance()
     * @generated
     */
    EAttribute getServiceInstance_InstancedOn();

    /**
     * Returns the meta object for the attribute list '{@link org.slasoi.models.scm.ServiceInstance#getEndpointsList
     * <em>Endpoints</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Endpoints</em>'.
     * @see org.slasoi.models.scm.ServiceInstance#getEndpointsList()
     * @see #getServiceInstance()
     * @generated
     */
    EAttribute getServiceInstance_Endpoints();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ImplementationArtefact
     * <em>Implementation Artefact</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Implementation Artefact</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact
     * @generated
     */
    EClass getImplementationArtefact();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ImplementationArtefact#getID <em>ID</em>}
     * '. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact#getID()
     * @see #getImplementationArtefact()
     * @generated
     */
    EAttribute getImplementationArtefact_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ImplementationArtefact#getName
     * <em>Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact#getName()
     * @see #getImplementationArtefact()
     * @generated
     */
    EAttribute getImplementationArtefact_Name();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ImplementationArtefact#getDesciption
     * <em>Desciption</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Desciption</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact#getDesciption()
     * @see #getImplementationArtefact()
     * @generated
     */
    EAttribute getImplementationArtefact_Desciption();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ImplementationArtefact#getDependenciesList <em>Dependencies</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Dependencies</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact#getDependenciesList()
     * @see #getImplementationArtefact()
     * @generated
     */
    EReference getImplementationArtefact_Dependencies();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ImplementationArtefact#getServiceFeaturesList <em>Service Features</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Service Features</em>'.
     * @see org.slasoi.models.scm.ImplementationArtefact#getServiceFeaturesList()
     * @see #getImplementationArtefact()
     * @generated
     */
    EReference getImplementationArtefact_ServiceFeatures();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.Dependency <em>Dependency</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Dependency</em>'.
     * @see org.slasoi.models.scm.Dependency
     * @generated
     */
    EClass getDependency();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.Dependency#getName <em>Name</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.slasoi.models.scm.Dependency#getName()
     * @see #getDependency()
     * @generated
     */
    EAttribute getDependency_Name();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.Dependency#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.slasoi.models.scm.Dependency#getDescription()
     * @see #getDependency()
     * @generated
     */
    EAttribute getDependency_Description();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.Dependency#getTargetURI
     * <em>Target URI</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Target URI</em>'.
     * @see org.slasoi.models.scm.Dependency#getTargetURI()
     * @see #getDependency()
     * @generated
     */
    EAttribute getDependency_TargetURI();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.Dependency#getTargetType
     * <em>Target Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Target Type</em>'.
     * @see org.slasoi.models.scm.Dependency#getTargetType()
     * @see #getDependency()
     * @generated
     */
    EReference getDependency_TargetType();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.Dependency#getID <em>ID</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.Dependency#getID()
     * @see #getDependency()
     * @generated
     */
    EAttribute getDependency_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.Dependency#getDepType <em>Dep Type</em>}
     * '. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Dep Type</em>'.
     * @see org.slasoi.models.scm.Dependency#getDepType()
     * @see #getDependency()
     * @generated
     */
    EAttribute getDependency_DepType();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.DeploymentArtefact <em>Deployment Artefact</em>}
     * '. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Deployment Artefact</em>'.
     * @see org.slasoi.models.scm.DeploymentArtefact
     * @generated
     */
    EClass getDeploymentArtefact();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.DataArtefact <em>Data Artefact</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Data Artefact</em>'.
     * @see org.slasoi.models.scm.DataArtefact
     * @generated
     */
    EClass getDataArtefact();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.VirtualAppliance <em>Virtual Appliance</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Virtual Appliance</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance
     * @generated
     */
    EClass getVirtualAppliance();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualAppliance#getHyperVisor
     * <em>Hyper Visor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Hyper Visor</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance#getHyperVisor()
     * @see #getVirtualAppliance()
     * @generated
     */
    EAttribute getVirtualAppliance_HyperVisor();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualAppliance#getVersion
     * <em>Version</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance#getVersion()
     * @see #getVirtualAppliance()
     * @generated
     */
    EAttribute getVirtualAppliance_Version();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualAppliance#getHypervisorVersion
     * <em>Hypervisor Version</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Hypervisor Version</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance#getHypervisorVersion()
     * @see #getVirtualAppliance()
     * @generated
     */
    EAttribute getVirtualAppliance_HypervisorVersion();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualAppliance#getApplianceType
     * <em>Appliance Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Appliance Type</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance#getApplianceType()
     * @see #getVirtualAppliance()
     * @generated
     */
    EAttribute getVirtualAppliance_ApplianceType();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualAppliance#getImageFormat
     * <em>Image Format</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Image Format</em>'.
     * @see org.slasoi.models.scm.VirtualAppliance#getImageFormat()
     * @see #getVirtualAppliance()
     * @generated
     */
    EAttribute getVirtualAppliance_ImageFormat();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.SoftwareArchive <em>Software Archive</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Software Archive</em>'.
     * @see org.slasoi.models.scm.SoftwareArchive
     * @generated
     */
    EClass getSoftwareArchive();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.SoftwareArchive#getArchiveFormat
     * <em>Archive Format</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Archive Format</em>'.
     * @see org.slasoi.models.scm.SoftwareArchive#getArchiveFormat()
     * @see #getSoftwareArchive()
     * @generated
     */
    EAttribute getSoftwareArchive_ArchiveFormat();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ConfigurableServiceFeature
     * <em>Configurable Service Feature</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Configurable Service Feature</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature
     * @generated
     */
    EClass getConfigurableServiceFeature();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ConfigurableServiceFeature#getID
     * <em>ID</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature#getID()
     * @see #getConfigurableServiceFeature()
     * @generated
     */
    EAttribute getConfigurableServiceFeature_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ConfigurableServiceFeature#getConfigType
     * <em>Config Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Config Type</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature#getConfigType()
     * @see #getConfigurableServiceFeature()
     * @generated
     */
    EAttribute getConfigurableServiceFeature_ConfigType();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ConfigurableServiceFeature#getConfigFile
     * <em>Config File</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Config File</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature#getConfigFile()
     * @see #getConfigurableServiceFeature()
     * @generated
     */
    EAttribute getConfigurableServiceFeature_ConfigFile();

    /**
     * Returns the meta object for the attribute '
     * {@link org.slasoi.models.scm.ConfigurableServiceFeature#getParameterIdentifier <em>Parameter Identifier</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Parameter Identifier</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature#getParameterIdentifier()
     * @see #getConfigurableServiceFeature()
     * @generated
     */
    EAttribute getConfigurableServiceFeature_ParameterIdentifier();

    /**
     * Returns the meta object for the attribute '
     * {@link org.slasoi.models.scm.ConfigurableServiceFeature#getDefaultValue <em>Default Value</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Default Value</em>'.
     * @see org.slasoi.models.scm.ConfigurableServiceFeature#getDefaultValue()
     * @see #getConfigurableServiceFeature()
     * @generated
     */
    EAttribute getConfigurableServiceFeature_DefaultValue();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceTopology <em>Service Topology</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Topology</em>'.
     * @see org.slasoi.models.scm.ServiceTopology
     * @generated
     */
    EClass getServiceTopology();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceTopology#getID <em>ID</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ServiceTopology#getID()
     * @see #getServiceTopology()
     * @generated
     */
    EAttribute getServiceTopology_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceTopology#getServiceTopologyName
     * <em>Service Topology Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Topology Name</em>'.
     * @see org.slasoi.models.scm.ServiceTopology#getServiceTopologyName()
     * @see #getServiceTopology()
     * @generated
     */
    EAttribute getServiceTopology_ServiceTopologyName();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceTopology#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.slasoi.models.scm.ServiceTopology#getDescription()
     * @see #getServiceTopology()
     * @generated
     */
    EAttribute getServiceTopology_Description();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ServiceTopology#getType <em>Type</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Type</em>'.
     * @see org.slasoi.models.scm.ServiceTopology#getType()
     * @see #getServiceTopology()
     * @generated
     */
    EReference getServiceTopology_Type();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.ServiceTopology#getLandscapeElementsList <em>Landscape Elements</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Landscape Elements</em>'.
     * @see org.slasoi.models.scm.ServiceTopology#getLandscapeElementsList()
     * @see #getServiceTopology()
     * @generated
     */
    EReference getServiceTopology_LandscapeElements();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ServiceLandscapeElement
     * <em>Service Landscape Element</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Service Landscape Element</em>'.
     * @see org.slasoi.models.scm.ServiceLandscapeElement
     * @generated
     */
    EClass getServiceLandscapeElement();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceLandscapeElement#getID
     * <em>ID</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>ID</em>'.
     * @see org.slasoi.models.scm.ServiceLandscapeElement#getID()
     * @see #getServiceLandscapeElement()
     * @generated
     */
    EAttribute getServiceLandscapeElement_ID();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceLandscapeElement#getName
     * <em>Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.slasoi.models.scm.ServiceLandscapeElement#getName()
     * @see #getServiceLandscapeElement()
     * @generated
     */
    EAttribute getServiceLandscapeElement_Name();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ServiceLandscapeElement#getDesription
     * <em>Desription</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Desription</em>'.
     * @see org.slasoi.models.scm.ServiceLandscapeElement#getDesription()
     * @see #getServiceLandscapeElement()
     * @generated
     */
    EAttribute getServiceLandscapeElement_Desription();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.SoftwareElement <em>Software Element</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Software Element</em>'.
     * @see org.slasoi.models.scm.SoftwareElement
     * @generated
     */
    EClass getSoftwareElement();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.SoftwareElement#getVersion
     * <em>Version</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see org.slasoi.models.scm.SoftwareElement#getVersion()
     * @see #getSoftwareElement()
     * @generated
     */
    EAttribute getSoftwareElement_Version();

    /**
     * Returns the meta object for the reference list '{@link org.slasoi.models.scm.SoftwareElement#getRequiresList
     * <em>Requires</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Requires</em>'.
     * @see org.slasoi.models.scm.SoftwareElement#getRequiresList()
     * @see #getSoftwareElement()
     * @generated
     */
    EReference getSoftwareElement_Requires();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.SoftwareElement#getProvider
     * <em>Provider</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Provider</em>'.
     * @see org.slasoi.models.scm.SoftwareElement#getProvider()
     * @see #getSoftwareElement()
     * @generated
     */
    EAttribute getSoftwareElement_Provider();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.WebServiceElement <em>Web Service Element</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Web Service Element</em>'.
     * @see org.slasoi.models.scm.WebServiceElement
     * @generated
     */
    EClass getWebServiceElement();

    /**
     * Returns the meta object for the reference list '{@link org.slasoi.models.scm.WebServiceElement#getRequiresList
     * <em>Requires</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Requires</em>'.
     * @see org.slasoi.models.scm.WebServiceElement#getRequiresList()
     * @see #getWebServiceElement()
     * @generated
     */
    EReference getWebServiceElement_Requires();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.CompositeWebService
     * <em>Composite Web Service</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Composite Web Service</em>'.
     * @see org.slasoi.models.scm.CompositeWebService
     * @generated
     */
    EClass getCompositeWebService();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.CompositeWebService#getContainsList <em>Contains</em>}'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Contains</em>'.
     * @see org.slasoi.models.scm.CompositeWebService#getContainsList()
     * @see #getCompositeWebService()
     * @generated
     */
    EReference getCompositeWebService_Contains();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.AtomicWebService <em>Atomic Web Service</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Atomic Web Service</em>'.
     * @see org.slasoi.models.scm.AtomicWebService
     * @generated
     */
    EClass getAtomicWebService();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ApplicationSoftware
     * <em>Application Software</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Application Software</em>'.
     * @see org.slasoi.models.scm.ApplicationSoftware
     * @generated
     */
    EClass getApplicationSoftware();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ExecutionSoftware <em>Execution Software</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Execution Software</em>'.
     * @see org.slasoi.models.scm.ExecutionSoftware
     * @generated
     */
    EClass getExecutionSoftware();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ConfigurationDirective
     * <em>Configuration Directive</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Configuration Directive</em>'.
     * @see org.slasoi.models.scm.ConfigurationDirective
     * @generated
     */
    EClass getConfigurationDirective();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ConfigurationDirective#getParameterValue
     * <em>Parameter Value</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Parameter Value</em>'.
     * @see org.slasoi.models.scm.ConfigurationDirective#getParameterValue()
     * @see #getConfigurationDirective()
     * @generated
     */
    EAttribute getConfigurationDirective_ParameterValue();

    /**
     * Returns the meta object for the reference '{@link org.slasoi.models.scm.ConfigurationDirective#getServiceFeature
     * <em>Service Feature</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Service Feature</em>'.
     * @see org.slasoi.models.scm.ConfigurationDirective#getServiceFeature()
     * @see #getConfigurationDirective()
     * @generated
     */
    EReference getConfigurationDirective_ServiceFeature();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.Landscape <em>Landscape</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Landscape</em>'.
     * @see org.slasoi.models.scm.Landscape
     * @generated
     */
    EClass getLandscape();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getInstancesList <em>Instances</em>}'. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Instances</em>'.
     * @see org.slasoi.models.scm.Landscape#getInstancesList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_Instances();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getProvidedTypesList <em>Provided Types</em>}'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Provided Types</em>'.
     * @see org.slasoi.models.scm.Landscape#getProvidedTypesList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_ProvidedTypes();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getImplementationsList <em>Implementations</em>}'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Implementations</em>'.
     * @see org.slasoi.models.scm.Landscape#getImplementationsList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_Implementations();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getTopologiesList <em>Topologies</em>}'. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Topologies</em>'.
     * @see org.slasoi.models.scm.Landscape#getTopologiesList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_Topologies();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getBuildersList <em>Builders</em>}'. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Builders</em>'.
     * @see org.slasoi.models.scm.Landscape#getBuildersList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_Builders();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.slasoi.models.scm.Landscape#getRequiredTypesList <em>Required Types</em>}'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Required Types</em>'.
     * @see org.slasoi.models.scm.Landscape#getRequiredTypesList()
     * @see #getLandscape()
     * @generated
     */
    EReference getLandscape_RequiredTypes();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ProvisioningInformation
     * <em>Provisioning Information</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Provisioning Information</em>'.
     * @see org.slasoi.models.scm.ProvisioningInformation
     * @generated
     */
    EClass getProvisioningInformation();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.ProvisioningInformation#getLeadTime
     * <em>Lead Time</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Lead Time</em>'.
     * @see org.slasoi.models.scm.ProvisioningInformation#getLeadTime()
     * @see #getProvisioningInformation()
     * @generated
     */
    EAttribute getProvisioningInformation_LeadTime();

    /**
     * Returns the meta object for the reference list '
     * {@link org.slasoi.models.scm.ProvisioningInformation#getBootOrderList <em>Boot Order</em>}'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Boot Order</em>'.
     * @see org.slasoi.models.scm.ProvisioningInformation#getBootOrderList()
     * @see #getProvisioningInformation()
     * @generated
     */
    EReference getProvisioningInformation_BootOrder();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.VirtualMachineArtefact
     * <em>Virtual Machine Artefact</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Virtual Machine Artefact</em>'.
     * @see org.slasoi.models.scm.VirtualMachineArtefact
     * @generated
     */
    EClass getVirtualMachineArtefact();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualMachineArtefact#getNumberOfCores
     * <em>Number Of Cores</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Number Of Cores</em>'.
     * @see org.slasoi.models.scm.VirtualMachineArtefact#getNumberOfCores()
     * @see #getVirtualMachineArtefact()
     * @generated
     */
    EAttribute getVirtualMachineArtefact_NumberOfCores();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuSpeed
     * <em>Cpu Speed</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Cpu Speed</em>'.
     * @see org.slasoi.models.scm.VirtualMachineArtefact#getCpuSpeed()
     * @see #getVirtualMachineArtefact()
     * @generated
     */
    EAttribute getVirtualMachineArtefact_CpuSpeed();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualMachineArtefact#getCpuType
     * <em>Cpu Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Cpu Type</em>'.
     * @see org.slasoi.models.scm.VirtualMachineArtefact#getCpuType()
     * @see #getVirtualMachineArtefact()
     * @generated
     */
    EAttribute getVirtualMachineArtefact_CpuType();

    /**
     * Returns the meta object for the attribute '{@link org.slasoi.models.scm.VirtualMachineArtefact#getMemorySize
     * <em>Memory Size</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Memory Size</em>'.
     * @see org.slasoi.models.scm.VirtualMachineArtefact#getMemorySize()
     * @see #getVirtualMachineArtefact()
     * @generated
     */
    EAttribute getVirtualMachineArtefact_MemorySize();

    /**
     * Returns the meta object for class '{@link org.slasoi.models.scm.ResourceArtefact <em>Resource Artefact</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Resource Artefact</em>'.
     * @see org.slasoi.models.scm.ResourceArtefact
     * @generated
     */
    EClass getResourceArtefact();

    /**
     * Returns the meta object for enum '{@link org.slasoi.models.scm.ConfigType <em>Config Type</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Config Type</em>'.
     * @see org.slasoi.models.scm.ConfigType
     * @generated
     */
    EEnum getConfigType();

    /**
     * Returns the meta object for enum '{@link org.slasoi.models.scm.DependencyType <em>Dependency Type</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Dependency Type</em>'.
     * @see org.slasoi.models.scm.DependencyType
     * @generated
     */
    EEnum getDependencyType();

    /**
     * Returns the meta object for data type '{@link org.slasoi.slamodel.sla.SLATemplate <em>SLA Template</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>SLA Template</em>'.
     * @see org.slasoi.slamodel.sla.SLATemplate
     * @model instanceClass="org.slasoi.slamodel.sla.SLATemplate"
     * @generated
     */
    EDataType getSLATemplate();

    /**
     * Returns the meta object for data type '{@link org.slasoi.slamodel.sla.Endpoint <em>Endpoint</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Endpoint</em>'.
     * @see org.slasoi.slamodel.sla.Endpoint
     * @model instanceClass="org.slasoi.slamodel.sla.Endpoint"
     * @generated
     */
    EDataType getEndpoint();

    /**
     * Returns the factory that creates the instances of the model. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ServiceConstructionModelFactory getServiceConstructionModelFactory();

    /**
     * <!-- begin-user-doc --> Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceTypeImpl <em>Service Type</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceTypeImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceType()
         * @generated
         */
        EClass SERVICE_TYPE = eINSTANCE.getServiceType();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TYPE__ID = eINSTANCE.getServiceType_ID();

        /**
         * The meta object literal for the '<em><b>Service Type Name</b></em>' attribute feature. <!-- begin-user-doc
         * --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TYPE__SERVICE_TYPE_NAME = eINSTANCE.getServiceType_ServiceTypeName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TYPE__DESCRIPTION = eINSTANCE.getServiceType_Description();

        /**
         * The meta object literal for the '<em><b>Interfaces</b></em>' attribute list feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TYPE__INTERFACES = eINSTANCE.getServiceType_Interfaces();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceImplementationImpl
         * <em>Service Implementation</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceImplementationImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceImplementation()
         * @generated
         */
        EClass SERVICE_IMPLEMENTATION = eINSTANCE.getServiceImplementation();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_IMPLEMENTATION__TYPE = eINSTANCE.getServiceImplementation_Type();

        /**
         * The meta object literal for the '<em><b>Artefacts</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_IMPLEMENTATION__ARTEFACTS = eINSTANCE.getServiceImplementation_Artefacts();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_IMPLEMENTATION__ID = eINSTANCE.getServiceImplementation_ID();

        /**
         * The meta object literal for the '<em><b>Service Implementation Name</b></em>' attribute feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_IMPLEMENTATION__SERVICE_IMPLEMENTATION_NAME =
                eINSTANCE.getServiceImplementation_ServiceImplementationName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_IMPLEMENTATION__DESCRIPTION = eINSTANCE.getServiceImplementation_Description();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_IMPLEMENTATION__VERSION = eINSTANCE.getServiceImplementation_Version();

        /**
         * The meta object literal for the '<em><b>Provisioning Information</b></em>' containment reference feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_IMPLEMENTATION__PROVISIONING_INFORMATION =
                eINSTANCE.getServiceImplementation_ProvisioningInformation();

        /**
         * The meta object literal for the '<em><b>Component Monitoring Features</b></em>' containment reference list
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_IMPLEMENTATION__COMPONENT_MONITORING_FEATURES =
                eINSTANCE.getServiceImplementation_ComponentMonitoringFeatures();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceBuilderImpl
         * <em>Service Builder</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceBuilderImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceBuilder()
         * @generated
         */
        EClass SERVICE_BUILDER = eINSTANCE.getServiceBuilder();

        /**
         * The meta object literal for the '<em><b>Implementation</b></em>' reference feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_BUILDER__IMPLEMENTATION = eINSTANCE.getServiceBuilder_Implementation();

        /**
         * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_BUILDER__BINDINGS = eINSTANCE.getServiceBuilder_Bindings();

        /**
         * The meta object literal for the '<em><b>Service Configurations</b></em>' containment reference list feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_BUILDER__SERVICE_CONFIGURATIONS = eINSTANCE.getServiceBuilder_ServiceConfigurations();

        /**
         * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_BUILDER__UUID = eINSTANCE.getServiceBuilder_Uuid();

        /**
         * The meta object literal for the '<em><b>Monitoring System Configuration</b></em>' containment reference
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION =
                eINSTANCE.getServiceBuilder_MonitoringSystemConfiguration();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceBindingImpl
         * <em>Service Binding</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceBindingImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceBinding()
         * @generated
         */
        EClass SERVICE_BINDING = eINSTANCE.getServiceBinding();

        /**
         * The meta object literal for the '<em><b>Dependency</b></em>' reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_BINDING__DEPENDENCY = eINSTANCE.getServiceBinding_Dependency();

        /**
         * The meta object literal for the '<em><b>Sla Template</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_BINDING__SLA_TEMPLATE = eINSTANCE.getServiceBinding_SlaTemplate();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceInstanceImpl
         * <em>Service Instance</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceInstanceImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceInstance()
         * @generated
         */
        EClass SERVICE_INSTANCE = eINSTANCE.getServiceInstance();

        /**
         * The meta object literal for the '<em><b>Implementation</b></em>' reference feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_INSTANCE__IMPLEMENTATION = eINSTANCE.getServiceInstance_Implementation();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_INSTANCE__ID = eINSTANCE.getServiceInstance_ID();

        /**
         * The meta object literal for the '<em><b>Service Instance Name</b></em>' attribute feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_INSTANCE__SERVICE_INSTANCE_NAME = eINSTANCE.getServiceInstance_ServiceInstanceName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_INSTANCE__DESCRIPTION = eINSTANCE.getServiceInstance_Description();

        /**
         * The meta object literal for the '<em><b>Instanced On</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_INSTANCE__INSTANCED_ON = eINSTANCE.getServiceInstance_InstancedOn();

        /**
         * The meta object literal for the '<em><b>Endpoints</b></em>' attribute list feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_INSTANCE__ENDPOINTS = eINSTANCE.getServiceInstance_Endpoints();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl
         * <em>Implementation Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ImplementationArtefactImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getImplementationArtefact()
         * @generated
         */
        EClass IMPLEMENTATION_ARTEFACT = eINSTANCE.getImplementationArtefact();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute IMPLEMENTATION_ARTEFACT__ID = eINSTANCE.getImplementationArtefact_ID();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute IMPLEMENTATION_ARTEFACT__NAME = eINSTANCE.getImplementationArtefact_Name();

        /**
         * The meta object literal for the '<em><b>Desciption</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute IMPLEMENTATION_ARTEFACT__DESCIPTION = eINSTANCE.getImplementationArtefact_Desciption();

        /**
         * The meta object literal for the '<em><b>Dependencies</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference IMPLEMENTATION_ARTEFACT__DEPENDENCIES = eINSTANCE.getImplementationArtefact_Dependencies();

        /**
         * The meta object literal for the '<em><b>Service Features</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES = eINSTANCE.getImplementationArtefact_ServiceFeatures();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.DependencyImpl <em>Dependency</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.DependencyImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDependency()
         * @generated
         */
        EClass DEPENDENCY = eINSTANCE.getDependency();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute DEPENDENCY__NAME = eINSTANCE.getDependency_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute DEPENDENCY__DESCRIPTION = eINSTANCE.getDependency_Description();

        /**
         * The meta object literal for the '<em><b>Target URI</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute DEPENDENCY__TARGET_URI = eINSTANCE.getDependency_TargetURI();

        /**
         * The meta object literal for the '<em><b>Target Type</b></em>' reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference DEPENDENCY__TARGET_TYPE = eINSTANCE.getDependency_TargetType();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute DEPENDENCY__ID = eINSTANCE.getDependency_ID();

        /**
         * The meta object literal for the '<em><b>Dep Type</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute DEPENDENCY__DEP_TYPE = eINSTANCE.getDependency_DepType();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.DeploymentArtefactImpl
         * <em>Deployment Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.DeploymentArtefactImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDeploymentArtefact()
         * @generated
         */
        EClass DEPLOYMENT_ARTEFACT = eINSTANCE.getDeploymentArtefact();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.DataArtefactImpl <em>Data Artefact</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.DataArtefactImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDataArtefact()
         * @generated
         */
        EClass DATA_ARTEFACT = eINSTANCE.getDataArtefact();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.VirtualApplianceImpl
         * <em>Virtual Appliance</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.VirtualApplianceImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getVirtualAppliance()
         * @generated
         */
        EClass VIRTUAL_APPLIANCE = eINSTANCE.getVirtualAppliance();

        /**
         * The meta object literal for the '<em><b>Hyper Visor</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_APPLIANCE__HYPER_VISOR = eINSTANCE.getVirtualAppliance_HyperVisor();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_APPLIANCE__VERSION = eINSTANCE.getVirtualAppliance_Version();

        /**
         * The meta object literal for the '<em><b>Hypervisor Version</b></em>' attribute feature. <!-- begin-user-doc
         * --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_APPLIANCE__HYPERVISOR_VERSION = eINSTANCE.getVirtualAppliance_HypervisorVersion();

        /**
         * The meta object literal for the '<em><b>Appliance Type</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_APPLIANCE__APPLIANCE_TYPE = eINSTANCE.getVirtualAppliance_ApplianceType();

        /**
         * The meta object literal for the '<em><b>Image Format</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_APPLIANCE__IMAGE_FORMAT = eINSTANCE.getVirtualAppliance_ImageFormat();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.SoftwareArchiveImpl
         * <em>Software Archive</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.SoftwareArchiveImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSoftwareArchive()
         * @generated
         */
        EClass SOFTWARE_ARCHIVE = eINSTANCE.getSoftwareArchive();

        /**
         * The meta object literal for the '<em><b>Archive Format</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOFTWARE_ARCHIVE__ARCHIVE_FORMAT = eINSTANCE.getSoftwareArchive_ArchiveFormat();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl
         * <em>Configurable Service Feature</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigurableServiceFeature()
         * @generated
         */
        EClass CONFIGURABLE_SERVICE_FEATURE = eINSTANCE.getConfigurableServiceFeature();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURABLE_SERVICE_FEATURE__ID = eINSTANCE.getConfigurableServiceFeature_ID();

        /**
         * The meta object literal for the '<em><b>Config Type</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE = eINSTANCE.getConfigurableServiceFeature_ConfigType();

        /**
         * The meta object literal for the '<em><b>Config File</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE = eINSTANCE.getConfigurableServiceFeature_ConfigFile();

        /**
         * The meta object literal for the '<em><b>Parameter Identifier</b></em>' attribute feature. <!-- begin-user-doc
         * --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER =
                eINSTANCE.getConfigurableServiceFeature_ParameterIdentifier();

        /**
         * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE = eINSTANCE.getConfigurableServiceFeature_DefaultValue();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceTopologyImpl
         * <em>Service Topology</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceTopologyImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceTopology()
         * @generated
         */
        EClass SERVICE_TOPOLOGY = eINSTANCE.getServiceTopology();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TOPOLOGY__ID = eINSTANCE.getServiceTopology_ID();

        /**
         * The meta object literal for the '<em><b>Service Topology Name</b></em>' attribute feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME = eINSTANCE.getServiceTopology_ServiceTopologyName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_TOPOLOGY__DESCRIPTION = eINSTANCE.getServiceTopology_Description();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_TOPOLOGY__TYPE = eINSTANCE.getServiceTopology_Type();

        /**
         * The meta object literal for the '<em><b>Landscape Elements</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS = eINSTANCE.getServiceTopology_LandscapeElements();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ServiceLandscapeElementImpl
         * <em>Service Landscape Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ServiceLandscapeElementImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getServiceLandscapeElement()
         * @generated
         */
        EClass SERVICE_LANDSCAPE_ELEMENT = eINSTANCE.getServiceLandscapeElement();

        /**
         * The meta object literal for the '<em><b>ID</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_LANDSCAPE_ELEMENT__ID = eINSTANCE.getServiceLandscapeElement_ID();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_LANDSCAPE_ELEMENT__NAME = eINSTANCE.getServiceLandscapeElement_Name();

        /**
         * The meta object literal for the '<em><b>Desription</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SERVICE_LANDSCAPE_ELEMENT__DESRIPTION = eINSTANCE.getServiceLandscapeElement_Desription();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.SoftwareElementImpl
         * <em>Software Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.SoftwareElementImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSoftwareElement()
         * @generated
         */
        EClass SOFTWARE_ELEMENT = eINSTANCE.getSoftwareElement();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOFTWARE_ELEMENT__VERSION = eINSTANCE.getSoftwareElement_Version();

        /**
         * The meta object literal for the '<em><b>Requires</b></em>' reference list feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SOFTWARE_ELEMENT__REQUIRES = eINSTANCE.getSoftwareElement_Requires();

        /**
         * The meta object literal for the '<em><b>Provider</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOFTWARE_ELEMENT__PROVIDER = eINSTANCE.getSoftwareElement_Provider();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.WebServiceElementImpl
         * <em>Web Service Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.WebServiceElementImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getWebServiceElement()
         * @generated
         */
        EClass WEB_SERVICE_ELEMENT = eINSTANCE.getWebServiceElement();

        /**
         * The meta object literal for the '<em><b>Requires</b></em>' reference list feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference WEB_SERVICE_ELEMENT__REQUIRES = eINSTANCE.getWebServiceElement_Requires();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.CompositeWebServiceImpl
         * <em>Composite Web Service</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.CompositeWebServiceImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getCompositeWebService()
         * @generated
         */
        EClass COMPOSITE_WEB_SERVICE = eINSTANCE.getCompositeWebService();

        /**
         * The meta object literal for the '<em><b>Contains</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMPOSITE_WEB_SERVICE__CONTAINS = eINSTANCE.getCompositeWebService_Contains();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.AtomicWebServiceImpl
         * <em>Atomic Web Service</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.AtomicWebServiceImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getAtomicWebService()
         * @generated
         */
        EClass ATOMIC_WEB_SERVICE = eINSTANCE.getAtomicWebService();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ApplicationSoftwareImpl
         * <em>Application Software</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ApplicationSoftwareImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getApplicationSoftware()
         * @generated
         */
        EClass APPLICATION_SOFTWARE = eINSTANCE.getApplicationSoftware();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ExecutionSoftwareImpl
         * <em>Execution Software</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ExecutionSoftwareImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getExecutionSoftware()
         * @generated
         */
        EClass EXECUTION_SOFTWARE = eINSTANCE.getExecutionSoftware();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ConfigurationDirectiveImpl
         * <em>Configuration Directive</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ConfigurationDirectiveImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigurationDirective()
         * @generated
         */
        EClass CONFIGURATION_DIRECTIVE = eINSTANCE.getConfigurationDirective();

        /**
         * The meta object literal for the '<em><b>Parameter Value</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONFIGURATION_DIRECTIVE__PARAMETER_VALUE = eINSTANCE.getConfigurationDirective_ParameterValue();

        /**
         * The meta object literal for the '<em><b>Service Feature</b></em>' reference feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONFIGURATION_DIRECTIVE__SERVICE_FEATURE = eINSTANCE.getConfigurationDirective_ServiceFeature();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.LandscapeImpl <em>Landscape</em>}' class.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.LandscapeImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getLandscape()
         * @generated
         */
        EClass LANDSCAPE = eINSTANCE.getLandscape();

        /**
         * The meta object literal for the '<em><b>Instances</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__INSTANCES = eINSTANCE.getLandscape_Instances();

        /**
         * The meta object literal for the '<em><b>Provided Types</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__PROVIDED_TYPES = eINSTANCE.getLandscape_ProvidedTypes();

        /**
         * The meta object literal for the '<em><b>Implementations</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__IMPLEMENTATIONS = eINSTANCE.getLandscape_Implementations();

        /**
         * The meta object literal for the '<em><b>Topologies</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__TOPOLOGIES = eINSTANCE.getLandscape_Topologies();

        /**
         * The meta object literal for the '<em><b>Builders</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__BUILDERS = eINSTANCE.getLandscape_Builders();

        /**
         * The meta object literal for the '<em><b>Required Types</b></em>' containment reference list feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LANDSCAPE__REQUIRED_TYPES = eINSTANCE.getLandscape_RequiredTypes();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ProvisioningInformationImpl
         * <em>Provisioning Information</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ProvisioningInformationImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getProvisioningInformation()
         * @generated
         */
        EClass PROVISIONING_INFORMATION = eINSTANCE.getProvisioningInformation();

        /**
         * The meta object literal for the '<em><b>Lead Time</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute PROVISIONING_INFORMATION__LEAD_TIME = eINSTANCE.getProvisioningInformation_LeadTime();

        /**
         * The meta object literal for the '<em><b>Boot Order</b></em>' reference list feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference PROVISIONING_INFORMATION__BOOT_ORDER = eINSTANCE.getProvisioningInformation_BootOrder();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl
         * <em>Virtual Machine Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.VirtualMachineArtefactImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getVirtualMachineArtefact()
         * @generated
         */
        EClass VIRTUAL_MACHINE_ARTEFACT = eINSTANCE.getVirtualMachineArtefact();

        /**
         * The meta object literal for the '<em><b>Number Of Cores</b></em>' attribute feature. <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES = eINSTANCE.getVirtualMachineArtefact_NumberOfCores();

        /**
         * The meta object literal for the '<em><b>Cpu Speed</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED = eINSTANCE.getVirtualMachineArtefact_CpuSpeed();

        /**
         * The meta object literal for the '<em><b>Cpu Type</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE = eINSTANCE.getVirtualMachineArtefact_CpuType();

        /**
         * The meta object literal for the '<em><b>Memory Size</b></em>' attribute feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE = eINSTANCE.getVirtualMachineArtefact_MemorySize();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.impl.ResourceArtefactImpl
         * <em>Resource Artefact</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.impl.ResourceArtefactImpl
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getResourceArtefact()
         * @generated
         */
        EClass RESOURCE_ARTEFACT = eINSTANCE.getResourceArtefact();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.ConfigType <em>Config Type</em>}' enum. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.ConfigType
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getConfigType()
         * @generated
         */
        EEnum CONFIG_TYPE = eINSTANCE.getConfigType();

        /**
         * The meta object literal for the '{@link org.slasoi.models.scm.DependencyType <em>Dependency Type</em>}' enum.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.models.scm.DependencyType
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getDependencyType()
         * @generated
         */
        EEnum DEPENDENCY_TYPE = eINSTANCE.getDependencyType();

        /**
         * The meta object literal for the '<em>SLA Template</em>' data type. <!-- begin-user-doc --> <!-- end-user-doc
         * -->
         * 
         * @see org.slasoi.slamodel.sla.SLATemplate
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getSLATemplate()
         * @generated
         */
        EDataType SLA_TEMPLATE = eINSTANCE.getSLATemplate();

        /**
         * The meta object literal for the '<em>Endpoint</em>' data type. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see org.slasoi.slamodel.sla.Endpoint
         * @see org.slasoi.models.scm.impl.ServiceConstructionModelPackageImpl#getEndpoint()
         * @generated
         */
        EDataType ENDPOINT = eINSTANCE.getEndpoint();

    }

} // ServiceConstructionModelPackage
