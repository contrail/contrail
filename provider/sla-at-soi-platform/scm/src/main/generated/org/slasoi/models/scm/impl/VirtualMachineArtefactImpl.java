/**
 *  SVN FILE: $Id: VirtualMachineArtefactImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/VirtualMachineArtefactImpl.java $
 */

package org.slasoi.models.scm.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.VirtualMachineArtefact;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Virtual Machine Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl#getNumberOfCores <em>Number Of Cores</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl#getMemorySize <em>Memory Size</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl#getCpuSpeed <em>Cpu Speed</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualMachineArtefactImpl#getCpuType <em>Cpu Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VirtualMachineArtefactImpl extends ResourceArtefactImpl implements VirtualMachineArtefact {
	/**
	 * The default value of the '{@link #getNumberOfCores() <em>Number Of Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfCores()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CORES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfCores() <em>Number Of Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfCores()
	 * @generated
	 * @ordered
	 */
	protected int numberOfCores = NUMBER_OF_CORES_EDEFAULT;

	/**
	 * The default value of the '{@link #getMemorySize() <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemorySize()
	 * @generated
	 * @ordered
	 */
	protected static final double MEMORY_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMemorySize() <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemorySize()
	 * @generated
	 * @ordered
	 */
	protected double memorySize = MEMORY_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCpuSpeed() <em>Cpu Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final double CPU_SPEED_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCpuSpeed() <em>Cpu Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuSpeed()
	 * @generated
	 * @ordered
	 */
	protected double cpuSpeed = CPU_SPEED_EDEFAULT;

	/**
	 * The default value of the '{@link #getCpuType() <em>Cpu Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuType()
	 * @generated
	 * @ordered
	 */
	protected static final String CPU_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCpuType() <em>Cpu Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuType()
	 * @generated
	 * @ordered
	 */
	protected String cpuType = CPU_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VirtualMachineArtefactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.VIRTUAL_MACHINE_ARTEFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfCores() {
		return numberOfCores;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberOfCores(int newNumberOfCores) {
		int oldNumberOfCores = numberOfCores;
		numberOfCores = newNumberOfCores;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES, oldNumberOfCores, numberOfCores));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCpuSpeed() {
		return cpuSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuSpeed(double newCpuSpeed) {
		double oldCpuSpeed = cpuSpeed;
		cpuSpeed = newCpuSpeed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED, oldCpuSpeed, cpuSpeed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCpuType() {
		return cpuType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuType(String newCpuType) {
		String oldCpuType = cpuType;
		cpuType = newCpuType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE, oldCpuType, cpuType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMemorySize() {
		return memorySize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemorySize(double newMemorySize) {
		double oldMemorySize = memorySize;
		memorySize = newMemorySize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE, oldMemorySize, memorySize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES:
				return getNumberOfCores();
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE:
				return getMemorySize();
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED:
				return getCpuSpeed();
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE:
				return getCpuType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES:
				setNumberOfCores((Integer)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE:
				setMemorySize((Double)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED:
				setCpuSpeed((Double)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE:
				setCpuType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES:
				setNumberOfCores(NUMBER_OF_CORES_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE:
				setMemorySize(MEMORY_SIZE_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED:
				setCpuSpeed(CPU_SPEED_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE:
				setCpuType(CPU_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES:
				return numberOfCores != NUMBER_OF_CORES_EDEFAULT;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE:
				return memorySize != MEMORY_SIZE_EDEFAULT;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED:
				return cpuSpeed != CPU_SPEED_EDEFAULT;
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE:
				return CPU_TYPE_EDEFAULT == null ? cpuType != null : !CPU_TYPE_EDEFAULT.equals(cpuType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numberOfCores: ");
		result.append(numberOfCores);
		result.append(", memorySize: ");
		result.append(memorySize);
		result.append(", cpuSpeed: ");
		result.append(cpuSpeed);
		result.append(", cpuType: ");
		result.append(cpuType);
		result.append(')');
		return result.toString();
	}

} //VirtualMachineArtefactImpl
