/**
 *  SVN FILE: $Id: ConfigurationPackage.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/monitoring/common/configuration/ConfigurationPackage.java $
 */

package org.slasoi.monitoring.common.configuration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.slasoi.monitoring.common.configuration.ConfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "configuration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://monitoring.slasoi.org/common/configuration/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "configuration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationPackage eINSTANCE = org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.MonitoringSystemConfigurationImpl <em>Monitoring System Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.MonitoringSystemConfigurationImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getMonitoringSystemConfiguration()
	 * @generated
	 */
	int MONITORING_SYSTEM_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_SYSTEM_CONFIGURATION__UUID = 0;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_SYSTEM_CONFIGURATION__COMPONENTS = 1;

	/**
	 * The number of structural features of the '<em>Monitoring System Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_SYSTEM_CONFIGURATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.ComponentConfigurationImpl <em>Component Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.ComponentConfigurationImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getComponentConfiguration()
	 * @generated
	 */
	int COMPONENT_CONFIGURATION = 1;

	/**
	 * The feature id for the '<em><b>Configuration Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__CONFIGURATION_ID = 0;

	/**
	 * The number of structural features of the '<em>Component Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.EffectorConfigurationImpl <em>Effector Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.EffectorConfigurationImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getEffectorConfiguration()
	 * @generated
	 */
	int EFFECTOR_CONFIGURATION = 2;

	/**
	 * The feature id for the '<em><b>Configuration Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECTOR_CONFIGURATION__CONFIGURATION_ID = COMPONENT_CONFIGURATION__CONFIGURATION_ID;

	/**
	 * The number of structural features of the '<em>Effector Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECTOR_CONFIGURATION_FEATURE_COUNT = COMPONENT_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.SensorConfigurationImpl <em>Sensor Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.SensorConfigurationImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getSensorConfiguration()
	 * @generated
	 */
	int SENSOR_CONFIGURATION = 3;

	/**
	 * The feature id for the '<em><b>Configuration Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION__CONFIGURATION_ID = COMPONENT_CONFIGURATION__CONFIGURATION_ID;

	/**
	 * The feature id for the '<em><b>Output Receivers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION__OUTPUT_RECEIVERS = COMPONENT_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Configured Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION__CONFIGURED_FEATURE = COMPONENT_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sensor Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION_FEATURE_COUNT = COMPONENT_CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl <em>Reasoner Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getReasonerConfiguration()
	 * @generated
	 */
	int REASONER_CONFIGURATION = 4;

	/**
	 * The feature id for the '<em><b>Configuration Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONER_CONFIGURATION__CONFIGURATION_ID = COMPONENT_CONFIGURATION__CONFIGURATION_ID;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONER_CONFIGURATION__SPECIFICATION = COMPONENT_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Receivers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONER_CONFIGURATION__OUTPUT_RECEIVERS = COMPONENT_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reasoner Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONER_CONFIGURATION_FEATURE_COUNT = COMPONENT_CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.OutputReceiverImpl <em>Output Receiver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.OutputReceiverImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getOutputReceiver()
	 * @generated
	 */
	int OUTPUT_RECEIVER = 5;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RECEIVER__UUID = 0;

	/**
	 * The feature id for the '<em><b>Event Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RECEIVER__EVENT_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Output Receiver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RECEIVER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.slasoi.monitoring.common.configuration.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.slasoi.monitoring.common.configuration.impl.ComponentImpl
	 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 6;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__UUID = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CONFIGURATIONS = 2;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = 3;


	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration <em>Monitoring System Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring System Configuration</em>'.
	 * @see org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration
	 * @generated
	 */
	EClass getMonitoringSystemConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration#getUuid()
	 * @see #getMonitoringSystemConfiguration()
	 * @generated
	 */
	EAttribute getMonitoringSystemConfiguration_Uuid();

	/**
	 * Returns the meta object for the containment reference list '{@link org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration#getComponentsList <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration#getComponentsList()
	 * @see #getMonitoringSystemConfiguration()
	 * @generated
	 */
	EReference getMonitoringSystemConfiguration_Components();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.ComponentConfiguration <em>Component Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Configuration</em>'.
	 * @see org.slasoi.monitoring.common.configuration.ComponentConfiguration
	 * @generated
	 */
	EClass getComponentConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.ComponentConfiguration#getConfigurationId <em>Configuration Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configuration Id</em>'.
	 * @see org.slasoi.monitoring.common.configuration.ComponentConfiguration#getConfigurationId()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EAttribute getComponentConfiguration_ConfigurationId();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.EffectorConfiguration <em>Effector Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Effector Configuration</em>'.
	 * @see org.slasoi.monitoring.common.configuration.EffectorConfiguration
	 * @generated
	 */
	EClass getEffectorConfiguration();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.SensorConfiguration <em>Sensor Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sensor Configuration</em>'.
	 * @see org.slasoi.monitoring.common.configuration.SensorConfiguration
	 * @generated
	 */
	EClass getSensorConfiguration();

	/**
	 * Returns the meta object for the reference list '{@link org.slasoi.monitoring.common.configuration.SensorConfiguration#getOutputReceiversList <em>Output Receivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Output Receivers</em>'.
	 * @see org.slasoi.monitoring.common.configuration.SensorConfiguration#getOutputReceiversList()
	 * @see #getSensorConfiguration()
	 * @generated
	 */
	EReference getSensorConfiguration_OutputReceivers();

	/**
	 * Returns the meta object for the reference '{@link org.slasoi.monitoring.common.configuration.SensorConfiguration#getConfiguredFeature <em>Configured Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Configured Feature</em>'.
	 * @see org.slasoi.monitoring.common.configuration.SensorConfiguration#getConfiguredFeature()
	 * @see #getSensorConfiguration()
	 * @generated
	 */
	EReference getSensorConfiguration_ConfiguredFeature();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.ReasonerConfiguration <em>Reasoner Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reasoner Configuration</em>'.
	 * @see org.slasoi.monitoring.common.configuration.ReasonerConfiguration
	 * @generated
	 */
	EClass getReasonerConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.ReasonerConfiguration#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specification</em>'.
	 * @see org.slasoi.monitoring.common.configuration.ReasonerConfiguration#getSpecification()
	 * @see #getReasonerConfiguration()
	 * @generated
	 */
	EAttribute getReasonerConfiguration_Specification();

	/**
	 * Returns the meta object for the reference list '{@link org.slasoi.monitoring.common.configuration.ReasonerConfiguration#getOutputReceiversList <em>Output Receivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Output Receivers</em>'.
	 * @see org.slasoi.monitoring.common.configuration.ReasonerConfiguration#getOutputReceiversList()
	 * @see #getReasonerConfiguration()
	 * @generated
	 */
	EReference getReasonerConfiguration_OutputReceivers();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.OutputReceiver <em>Output Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Receiver</em>'.
	 * @see org.slasoi.monitoring.common.configuration.OutputReceiver
	 * @generated
	 */
	EClass getOutputReceiver();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.OutputReceiver#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see org.slasoi.monitoring.common.configuration.OutputReceiver#getUuid()
	 * @see #getOutputReceiver()
	 * @generated
	 */
	EAttribute getOutputReceiver_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.OutputReceiver#getEventType <em>Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Type</em>'.
	 * @see org.slasoi.monitoring.common.configuration.OutputReceiver#getEventType()
	 * @see #getOutputReceiver()
	 * @generated
	 */
	EAttribute getOutputReceiver_EventType();

	/**
	 * Returns the meta object for class '{@link org.slasoi.monitoring.common.configuration.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see org.slasoi.monitoring.common.configuration.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.Component#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see org.slasoi.monitoring.common.configuration.Component#getUuid()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link org.slasoi.monitoring.common.configuration.Component#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.slasoi.monitoring.common.configuration.Component#getType()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.slasoi.monitoring.common.configuration.Component#getConfigurationsList <em>Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configurations</em>'.
	 * @see org.slasoi.monitoring.common.configuration.Component#getConfigurationsList()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Configurations();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigurationFactory getConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.MonitoringSystemConfigurationImpl <em>Monitoring System Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.MonitoringSystemConfigurationImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getMonitoringSystemConfiguration()
		 * @generated
		 */
		EClass MONITORING_SYSTEM_CONFIGURATION = eINSTANCE.getMonitoringSystemConfiguration();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_SYSTEM_CONFIGURATION__UUID = eINSTANCE.getMonitoringSystemConfiguration_Uuid();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORING_SYSTEM_CONFIGURATION__COMPONENTS = eINSTANCE.getMonitoringSystemConfiguration_Components();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.ComponentConfigurationImpl <em>Component Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.ComponentConfigurationImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getComponentConfiguration()
		 * @generated
		 */
		EClass COMPONENT_CONFIGURATION = eINSTANCE.getComponentConfiguration();

		/**
		 * The meta object literal for the '<em><b>Configuration Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_CONFIGURATION__CONFIGURATION_ID = eINSTANCE.getComponentConfiguration_ConfigurationId();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.EffectorConfigurationImpl <em>Effector Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.EffectorConfigurationImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getEffectorConfiguration()
		 * @generated
		 */
		EClass EFFECTOR_CONFIGURATION = eINSTANCE.getEffectorConfiguration();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.SensorConfigurationImpl <em>Sensor Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.SensorConfigurationImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getSensorConfiguration()
		 * @generated
		 */
		EClass SENSOR_CONFIGURATION = eINSTANCE.getSensorConfiguration();

		/**
		 * The meta object literal for the '<em><b>Output Receivers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SENSOR_CONFIGURATION__OUTPUT_RECEIVERS = eINSTANCE.getSensorConfiguration_OutputReceivers();

		/**
		 * The meta object literal for the '<em><b>Configured Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SENSOR_CONFIGURATION__CONFIGURED_FEATURE = eINSTANCE.getSensorConfiguration_ConfiguredFeature();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl <em>Reasoner Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getReasonerConfiguration()
		 * @generated
		 */
		EClass REASONER_CONFIGURATION = eINSTANCE.getReasonerConfiguration();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REASONER_CONFIGURATION__SPECIFICATION = eINSTANCE.getReasonerConfiguration_Specification();

		/**
		 * The meta object literal for the '<em><b>Output Receivers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REASONER_CONFIGURATION__OUTPUT_RECEIVERS = eINSTANCE.getReasonerConfiguration_OutputReceivers();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.OutputReceiverImpl <em>Output Receiver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.OutputReceiverImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getOutputReceiver()
		 * @generated
		 */
		EClass OUTPUT_RECEIVER = eINSTANCE.getOutputReceiver();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_RECEIVER__UUID = eINSTANCE.getOutputReceiver_Uuid();

		/**
		 * The meta object literal for the '<em><b>Event Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_RECEIVER__EVENT_TYPE = eINSTANCE.getOutputReceiver_EventType();

		/**
		 * The meta object literal for the '{@link org.slasoi.monitoring.common.configuration.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.slasoi.monitoring.common.configuration.impl.ComponentImpl
		 * @see org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__UUID = eINSTANCE.getComponent_Uuid();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__TYPE = eINSTANCE.getComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__CONFIGURATIONS = eINSTANCE.getComponent_Configurations();

	}

} //ConfigurationPackage
