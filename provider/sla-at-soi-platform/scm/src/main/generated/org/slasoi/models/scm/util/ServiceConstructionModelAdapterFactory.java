/**
 *  SVN FILE: $Id: ServiceConstructionModelAdapterFactory.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/util/ServiceConstructionModelAdapterFactory.java $
 */

package org.slasoi.models.scm.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.slasoi.models.scm.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage
 * @generated
 */
public class ServiceConstructionModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ServiceConstructionModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceConstructionModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ServiceConstructionModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceConstructionModelSwitch<Adapter> modelSwitch =
		new ServiceConstructionModelSwitch<Adapter>() {
			@Override
			public Adapter caseServiceType(ServiceType object) {
				return createServiceTypeAdapter();
			}
			@Override
			public Adapter caseServiceImplementation(ServiceImplementation object) {
				return createServiceImplementationAdapter();
			}
			@Override
			public Adapter caseServiceBuilder(ServiceBuilder object) {
				return createServiceBuilderAdapter();
			}
			@Override
			public Adapter caseServiceBinding(ServiceBinding object) {
				return createServiceBindingAdapter();
			}
			@Override
			public Adapter caseServiceInstance(ServiceInstance object) {
				return createServiceInstanceAdapter();
			}
			@Override
			public Adapter caseImplementationArtefact(ImplementationArtefact object) {
				return createImplementationArtefactAdapter();
			}
			@Override
			public Adapter caseDependency(Dependency object) {
				return createDependencyAdapter();
			}
			@Override
			public Adapter caseDeploymentArtefact(DeploymentArtefact object) {
				return createDeploymentArtefactAdapter();
			}
			@Override
			public Adapter caseDataArtefact(DataArtefact object) {
				return createDataArtefactAdapter();
			}
			@Override
			public Adapter caseVirtualAppliance(VirtualAppliance object) {
				return createVirtualApplianceAdapter();
			}
			@Override
			public Adapter caseSoftwareArchive(SoftwareArchive object) {
				return createSoftwareArchiveAdapter();
			}
			@Override
			public Adapter caseConfigurableServiceFeature(ConfigurableServiceFeature object) {
				return createConfigurableServiceFeatureAdapter();
			}
			@Override
			public Adapter caseServiceTopology(ServiceTopology object) {
				return createServiceTopologyAdapter();
			}
			@Override
			public Adapter caseServiceLandscapeElement(ServiceLandscapeElement object) {
				return createServiceLandscapeElementAdapter();
			}
			@Override
			public Adapter caseSoftwareElement(SoftwareElement object) {
				return createSoftwareElementAdapter();
			}
			@Override
			public Adapter caseWebServiceElement(WebServiceElement object) {
				return createWebServiceElementAdapter();
			}
			@Override
			public Adapter caseCompositeWebService(CompositeWebService object) {
				return createCompositeWebServiceAdapter();
			}
			@Override
			public Adapter caseAtomicWebService(AtomicWebService object) {
				return createAtomicWebServiceAdapter();
			}
			@Override
			public Adapter caseApplicationSoftware(ApplicationSoftware object) {
				return createApplicationSoftwareAdapter();
			}
			@Override
			public Adapter caseExecutionSoftware(ExecutionSoftware object) {
				return createExecutionSoftwareAdapter();
			}
			@Override
			public Adapter caseConfigurationDirective(ConfigurationDirective object) {
				return createConfigurationDirectiveAdapter();
			}
			@Override
			public Adapter caseLandscape(Landscape object) {
				return createLandscapeAdapter();
			}
			@Override
			public Adapter caseProvisioningInformation(ProvisioningInformation object) {
				return createProvisioningInformationAdapter();
			}
			@Override
			public Adapter caseResourceArtefact(ResourceArtefact object) {
				return createResourceArtefactAdapter();
			}
			@Override
			public Adapter caseVirtualMachineArtefact(VirtualMachineArtefact object) {
				return createVirtualMachineArtefactAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceType <em>Service Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceType
	 * @generated
	 */
	public Adapter createServiceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceImplementation <em>Service Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceImplementation
	 * @generated
	 */
	public Adapter createServiceImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceBuilder <em>Service Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceBuilder
	 * @generated
	 */
	public Adapter createServiceBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceBinding <em>Service Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceBinding
	 * @generated
	 */
	public Adapter createServiceBindingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceInstance <em>Service Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceInstance
	 * @generated
	 */
	public Adapter createServiceInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ImplementationArtefact <em>Implementation Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ImplementationArtefact
	 * @generated
	 */
	public Adapter createImplementationArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.Dependency
	 * @generated
	 */
	public Adapter createDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.DeploymentArtefact <em>Deployment Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.DeploymentArtefact
	 * @generated
	 */
	public Adapter createDeploymentArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.DataArtefact <em>Data Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.DataArtefact
	 * @generated
	 */
	public Adapter createDataArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.VirtualAppliance <em>Virtual Appliance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.VirtualAppliance
	 * @generated
	 */
	public Adapter createVirtualApplianceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.SoftwareArchive <em>Software Archive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.SoftwareArchive
	 * @generated
	 */
	public Adapter createSoftwareArchiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ConfigurableServiceFeature <em>Configurable Service Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ConfigurableServiceFeature
	 * @generated
	 */
	public Adapter createConfigurableServiceFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceTopology <em>Service Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceTopology
	 * @generated
	 */
	public Adapter createServiceTopologyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ServiceLandscapeElement <em>Service Landscape Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ServiceLandscapeElement
	 * @generated
	 */
	public Adapter createServiceLandscapeElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.SoftwareElement <em>Software Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.SoftwareElement
	 * @generated
	 */
	public Adapter createSoftwareElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.WebServiceElement <em>Web Service Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.WebServiceElement
	 * @generated
	 */
	public Adapter createWebServiceElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.CompositeWebService <em>Composite Web Service</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.CompositeWebService
	 * @generated
	 */
	public Adapter createCompositeWebServiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.AtomicWebService <em>Atomic Web Service</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.AtomicWebService
	 * @generated
	 */
	public Adapter createAtomicWebServiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ApplicationSoftware <em>Application Software</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ApplicationSoftware
	 * @generated
	 */
	public Adapter createApplicationSoftwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ExecutionSoftware <em>Execution Software</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ExecutionSoftware
	 * @generated
	 */
	public Adapter createExecutionSoftwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ConfigurationDirective <em>Configuration Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ConfigurationDirective
	 * @generated
	 */
	public Adapter createConfigurationDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.Landscape <em>Landscape</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.Landscape
	 * @generated
	 */
	public Adapter createLandscapeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ProvisioningInformation <em>Provisioning Information</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ProvisioningInformation
	 * @generated
	 */
	public Adapter createProvisioningInformationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.VirtualMachineArtefact <em>Virtual Machine Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.VirtualMachineArtefact
	 * @generated
	 */
	public Adapter createVirtualMachineArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.slasoi.models.scm.ResourceArtefact <em>Resource Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.slasoi.models.scm.ResourceArtefact
	 * @generated
	 */
	public Adapter createResourceArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ServiceConstructionModelAdapterFactory
