/**
 *  SVN FILE: $Id: VirtualApplianceImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/VirtualApplianceImpl.java $
 */

package org.slasoi.models.scm.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.VirtualAppliance;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Virtual Appliance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualApplianceImpl#getHyperVisor <em>Hyper Visor</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualApplianceImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualApplianceImpl#getHypervisorVersion <em>Hypervisor Version</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualApplianceImpl#getApplianceType <em>Appliance Type</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.VirtualApplianceImpl#getImageFormat <em>Image Format</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VirtualApplianceImpl extends DeploymentArtefactImpl implements VirtualAppliance {
	/**
	 * The default value of the '{@link #getHyperVisor() <em>Hyper Visor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHyperVisor()
	 * @generated
	 * @ordered
	 */
	protected static final String HYPER_VISOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHyperVisor() <em>Hyper Visor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHyperVisor()
	 * @generated
	 * @ordered
	 */
	protected String hyperVisor = HYPER_VISOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getHypervisorVersion() <em>Hypervisor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHypervisorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String HYPERVISOR_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHypervisorVersion() <em>Hypervisor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHypervisorVersion()
	 * @generated
	 * @ordered
	 */
	protected String hypervisorVersion = HYPERVISOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getApplianceType() <em>Appliance Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplianceType()
	 * @generated
	 * @ordered
	 */
	protected static final String APPLIANCE_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getApplianceType() <em>Appliance Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplianceType()
	 * @generated
	 * @ordered
	 */
	protected String applianceType = APPLIANCE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getImageFormat() <em>Image Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_FORMAT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImageFormat() <em>Image Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageFormat()
	 * @generated
	 * @ordered
	 */
	protected String imageFormat = IMAGE_FORMAT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VirtualApplianceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.VIRTUAL_APPLIANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHyperVisor() {
		return hyperVisor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHyperVisor(String newHyperVisor) {
		String oldHyperVisor = hyperVisor;
		hyperVisor = newHyperVisor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPER_VISOR, oldHyperVisor, hyperVisor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHypervisorVersion() {
		return hypervisorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHypervisorVersion(String newHypervisorVersion) {
		String oldHypervisorVersion = hypervisorVersion;
		hypervisorVersion = newHypervisorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPERVISOR_VERSION, oldHypervisorVersion, hypervisorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getApplianceType() {
		return applianceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplianceType(String newApplianceType) {
		String oldApplianceType = applianceType;
		applianceType = newApplianceType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__APPLIANCE_TYPE, oldApplianceType, applianceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImageFormat() {
		return imageFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageFormat(String newImageFormat) {
		String oldImageFormat = imageFormat;
		imageFormat = newImageFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__IMAGE_FORMAT, oldImageFormat, imageFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPER_VISOR:
				return getHyperVisor();
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__VERSION:
				return getVersion();
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPERVISOR_VERSION:
				return getHypervisorVersion();
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__APPLIANCE_TYPE:
				return getApplianceType();
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__IMAGE_FORMAT:
				return getImageFormat();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPER_VISOR:
				setHyperVisor((String)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__VERSION:
				setVersion((String)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPERVISOR_VERSION:
				setHypervisorVersion((String)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__APPLIANCE_TYPE:
				setApplianceType((String)newValue);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__IMAGE_FORMAT:
				setImageFormat((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPER_VISOR:
				setHyperVisor(HYPER_VISOR_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPERVISOR_VERSION:
				setHypervisorVersion(HYPERVISOR_VERSION_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__APPLIANCE_TYPE:
				setApplianceType(APPLIANCE_TYPE_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__IMAGE_FORMAT:
				setImageFormat(IMAGE_FORMAT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPER_VISOR:
				return HYPER_VISOR_EDEFAULT == null ? hyperVisor != null : !HYPER_VISOR_EDEFAULT.equals(hyperVisor);
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__HYPERVISOR_VERSION:
				return HYPERVISOR_VERSION_EDEFAULT == null ? hypervisorVersion != null : !HYPERVISOR_VERSION_EDEFAULT.equals(hypervisorVersion);
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__APPLIANCE_TYPE:
				return APPLIANCE_TYPE_EDEFAULT == null ? applianceType != null : !APPLIANCE_TYPE_EDEFAULT.equals(applianceType);
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE__IMAGE_FORMAT:
				return IMAGE_FORMAT_EDEFAULT == null ? imageFormat != null : !IMAGE_FORMAT_EDEFAULT.equals(imageFormat);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (HyperVisor: ");
		result.append(hyperVisor);
		result.append(", Version: ");
		result.append(version);
		result.append(", HypervisorVersion: ");
		result.append(hypervisorVersion);
		result.append(", ApplianceType: ");
		result.append(applianceType);
		result.append(", ImageFormat: ");
		result.append(imageFormat);
		result.append(')');
		return result.toString();
	}

} //VirtualApplianceImpl
