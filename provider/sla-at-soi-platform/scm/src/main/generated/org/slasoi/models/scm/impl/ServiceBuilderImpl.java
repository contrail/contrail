/**
 *  SVN FILE: $Id: ServiceBuilderImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceBuilderImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.slasoi.models.scm.ConfigurationDirective;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBuilderImpl#getImplementation <em>Implementation</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBuilderImpl#getBindingsList <em>Bindings</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBuilderImpl#getServiceConfigurationsList <em>Service Configurations</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBuilderImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceBuilderImpl#getMonitoringSystemConfiguration <em>Monitoring System Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceBuilderImpl extends EObjectImpl implements ServiceBuilder {
	/**
	 * The cached value of the '{@link #getImplementation() <em>Implementation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementation()
	 * @generated
	 * @ordered
	 */
	protected ServiceImplementation implementation;

	/**
	 * The cached value of the '{@link #getBindingsList() <em>Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingsList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceBinding> bindings;

	/**
	 * The empty value for the '{@link #getBindings() <em>Bindings</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceBinding[] BINDINGS_EEMPTY_ARRAY = new ServiceBinding [0];

	/**
	 * The cached value of the '{@link #getServiceConfigurationsList() <em>Service Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceConfigurationsList()
	 * @generated
	 * @ordered
	 */
	protected EList<ConfigurationDirective> serviceConfigurations;

	/**
	 * The empty value for the '{@link #getServiceConfigurations() <em>Service Configurations</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceConfigurations()
	 * @generated
	 * @ordered
	 */
	protected static final ConfigurationDirective[] SERVICE_CONFIGURATIONS_EEMPTY_ARRAY = new ConfigurationDirective [0];

	/**
	 * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMonitoringSystemConfiguration() <em>Monitoring System Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringSystemConfiguration()
	 * @generated
	 * @ordered
	 */
	protected MonitoringSystemConfiguration monitoringSystemConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.SERVICE_BUILDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation getImplementation() {
		if (implementation != null && implementation.eIsProxy()) {
			InternalEObject oldImplementation = (InternalEObject)implementation;
			implementation = (ServiceImplementation)eResolveProxy(oldImplementation);
			if (implementation != oldImplementation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION, oldImplementation, implementation));
			}
		}
		return implementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation basicGetImplementation() {
		return implementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementation(ServiceImplementation newImplementation) {
		ServiceImplementation oldImplementation = implementation;
		implementation = newImplementation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION, oldImplementation, implementation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceBinding[] getBindings() {
		if (bindings == null || bindings.isEmpty()) return BINDINGS_EEMPTY_ARRAY;
		BasicEList<ServiceBinding> list = (BasicEList<ServiceBinding>)bindings;
		list.shrink();
		return (ServiceBinding[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceBinding getBindings(int index) {
		return getBindingsList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBindingsLength() {
		return bindings == null ? 0 : bindings.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindings(ServiceBinding[] newBindings) {
		((BasicEList<ServiceBinding>)getBindingsList()).setData(newBindings.length, newBindings);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindings(int index, ServiceBinding element) {
		getBindingsList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceBinding> getBindingsList() {
		if (bindings == null) {
			bindings = new EObjectContainmentEList<ServiceBinding>(ServiceBinding.class, this, ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationDirective[] getServiceConfigurations() {
		if (serviceConfigurations == null || serviceConfigurations.isEmpty()) return SERVICE_CONFIGURATIONS_EEMPTY_ARRAY;
		BasicEList<ConfigurationDirective> list = (BasicEList<ConfigurationDirective>)serviceConfigurations;
		list.shrink();
		return (ConfigurationDirective[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationDirective getServiceConfigurations(int index) {
		return getServiceConfigurationsList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getServiceConfigurationsLength() {
		return serviceConfigurations == null ? 0 : serviceConfigurations.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceConfigurations(ConfigurationDirective[] newServiceConfigurations) {
		((BasicEList<ConfigurationDirective>)getServiceConfigurationsList()).setData(newServiceConfigurations.length, newServiceConfigurations);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceConfigurations(int index, ConfigurationDirective element) {
		getServiceConfigurationsList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConfigurationDirective> getServiceConfigurationsList() {
		if (serviceConfigurations == null) {
			serviceConfigurations = new EObjectContainmentEList<ConfigurationDirective>(ConfigurationDirective.class, this, ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS);
		}
		return serviceConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUuid(String newUuid) {
		String oldUuid = uuid;
		uuid = newUuid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BUILDER__UUID, oldUuid, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringSystemConfiguration getMonitoringSystemConfiguration() {
		return monitoringSystemConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMonitoringSystemConfiguration(MonitoringSystemConfiguration newMonitoringSystemConfiguration, NotificationChain msgs) {
		MonitoringSystemConfiguration oldMonitoringSystemConfiguration = monitoringSystemConfiguration;
		monitoringSystemConfiguration = newMonitoringSystemConfiguration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION, oldMonitoringSystemConfiguration, newMonitoringSystemConfiguration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoringSystemConfiguration(MonitoringSystemConfiguration newMonitoringSystemConfiguration) {
		if (newMonitoringSystemConfiguration != monitoringSystemConfiguration) {
			NotificationChain msgs = null;
			if (monitoringSystemConfiguration != null)
				msgs = ((InternalEObject)monitoringSystemConfiguration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION, null, msgs);
			if (newMonitoringSystemConfiguration != null)
				msgs = ((InternalEObject)newMonitoringSystemConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION, null, msgs);
			msgs = basicSetMonitoringSystemConfiguration(newMonitoringSystemConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION, newMonitoringSystemConfiguration, newMonitoringSystemConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validate() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBinding(Dependency serviceDependency, SLATemplate slaTemplate) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getOpenDependencies() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS:
				return ((InternalEList<?>)getBindingsList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS:
				return ((InternalEList<?>)getServiceConfigurationsList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION:
				return basicSetMonitoringSystemConfiguration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION:
				if (resolve) return getImplementation();
				return basicGetImplementation();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS:
				return getBindingsList();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS:
				return getServiceConfigurationsList();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__UUID:
				return getUuid();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION:
				return getMonitoringSystemConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION:
				setImplementation((ServiceImplementation)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS:
				getBindingsList().clear();
				getBindingsList().addAll((Collection<? extends ServiceBinding>)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS:
				getServiceConfigurationsList().clear();
				getServiceConfigurationsList().addAll((Collection<? extends ConfigurationDirective>)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__UUID:
				setUuid((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION:
				setMonitoringSystemConfiguration((MonitoringSystemConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION:
				setImplementation((ServiceImplementation)null);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS:
				getBindingsList().clear();
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS:
				getServiceConfigurationsList().clear();
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__UUID:
				setUuid(UUID_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION:
				setMonitoringSystemConfiguration((MonitoringSystemConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_BUILDER__IMPLEMENTATION:
				return implementation != null;
			case ServiceConstructionModelPackage.SERVICE_BUILDER__BINDINGS:
				return bindings != null && !bindings.isEmpty();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__SERVICE_CONFIGURATIONS:
				return serviceConfigurations != null && !serviceConfigurations.isEmpty();
			case ServiceConstructionModelPackage.SERVICE_BUILDER__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case ServiceConstructionModelPackage.SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION:
				return monitoringSystemConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (uuid: ");
		result.append(uuid);
		result.append(')');
		return result.toString();
	}

} //ServiceBuilderImpl
