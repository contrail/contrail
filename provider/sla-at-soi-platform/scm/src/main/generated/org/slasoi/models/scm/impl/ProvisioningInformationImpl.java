/**
 *  SVN FILE: $Id: ProvisioningInformationImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ProvisioningInformationImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.ProvisioningInformation;
import org.slasoi.models.scm.ServiceConstructionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provisioning Information</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ProvisioningInformationImpl#getLeadTime <em>Lead Time</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ProvisioningInformationImpl#getBootOrderList <em>Boot Order</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProvisioningInformationImpl extends EObjectImpl implements ProvisioningInformation {
	/**
	 * The default value of the '{@link #getLeadTime() <em>Lead Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeadTime()
	 * @generated
	 * @ordered
	 */
	protected static final int LEAD_TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLeadTime() <em>Lead Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeadTime()
	 * @generated
	 * @ordered
	 */
	protected int leadTime = LEAD_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBootOrderList() <em>Boot Order</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBootOrderList()
	 * @generated
	 * @ordered
	 */
	protected EList<ImplementationArtefact> bootOrder;

	/**
	 * The empty value for the '{@link #getBootOrder() <em>Boot Order</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBootOrder()
	 * @generated
	 * @ordered
	 */
	protected static final ImplementationArtefact[] BOOT_ORDER_EEMPTY_ARRAY = new ImplementationArtefact [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvisioningInformationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.PROVISIONING_INFORMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLeadTime() {
		return leadTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeadTime(int newLeadTime) {
		int oldLeadTime = leadTime;
		leadTime = newLeadTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.PROVISIONING_INFORMATION__LEAD_TIME, oldLeadTime, leadTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationArtefact[] getBootOrder() {
		if (bootOrder == null || bootOrder.isEmpty()) return BOOT_ORDER_EEMPTY_ARRAY;
		BasicEList<ImplementationArtefact> list = (BasicEList<ImplementationArtefact>)bootOrder;
		list.shrink();
		return (ImplementationArtefact[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationArtefact getBootOrder(int index) {
		return getBootOrderList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBootOrderLength() {
		return bootOrder == null ? 0 : bootOrder.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBootOrder(ImplementationArtefact[] newBootOrder) {
		((BasicEList<ImplementationArtefact>)getBootOrderList()).setData(newBootOrder.length, newBootOrder);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBootOrder(int index, ImplementationArtefact element) {
		getBootOrderList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationArtefact> getBootOrderList() {
		if (bootOrder == null) {
			bootOrder = new EObjectResolvingEList<ImplementationArtefact>(ImplementationArtefact.class, this, ServiceConstructionModelPackage.PROVISIONING_INFORMATION__BOOT_ORDER);
		}
		return bootOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__LEAD_TIME:
				return getLeadTime();
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__BOOT_ORDER:
				return getBootOrderList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__LEAD_TIME:
				setLeadTime((Integer)newValue);
				return;
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__BOOT_ORDER:
				getBootOrderList().clear();
				getBootOrderList().addAll((Collection<? extends ImplementationArtefact>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__LEAD_TIME:
				setLeadTime(LEAD_TIME_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__BOOT_ORDER:
				getBootOrderList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__LEAD_TIME:
				return leadTime != LEAD_TIME_EDEFAULT;
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION__BOOT_ORDER:
				return bootOrder != null && !bootOrder.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (LeadTime: ");
		result.append(leadTime);
		result.append(')');
		return result.toString();
	}

} //ProvisioningInformationImpl
