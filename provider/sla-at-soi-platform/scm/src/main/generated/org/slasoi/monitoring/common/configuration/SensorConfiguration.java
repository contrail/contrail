/**
 *  SVN FILE: $Id: SensorConfiguration.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/monitoring/common/configuration/SensorConfiguration.java $
 */

package org.slasoi.monitoring.common.configuration;

import org.eclipse.emf.common.util.EList;
import org.slasoi.monitoring.common.features.MonitoringFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensor Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.slasoi.monitoring.common.configuration.SensorConfiguration#getOutputReceiversList <em>Output Receivers</em>}</li>
 *   <li>{@link org.slasoi.monitoring.common.configuration.SensorConfiguration#getConfiguredFeature <em>Configured Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.slasoi.monitoring.common.configuration.ConfigurationPackage#getSensorConfiguration()
 * @model
 * @generated
 */
public interface SensorConfiguration extends ComponentConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OutputReceiver[] getOutputReceivers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OutputReceiver getOutputReceivers(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getOutputReceiversLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setOutputReceivers(OutputReceiver[] newOutputReceivers);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setOutputReceivers(int index, OutputReceiver element);

	/**
	 * Returns the value of the '<em><b>Output Receivers</b></em>' reference list.
	 * The list contents are of type {@link org.slasoi.monitoring.common.configuration.OutputReceiver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Receivers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Receivers</em>' reference list.
	 * @see org.slasoi.monitoring.common.configuration.ConfigurationPackage#getSensorConfiguration_OutputReceivers()
	 * @model
	 * @generated
	 */
	EList<OutputReceiver> getOutputReceiversList();

	/**
	 * Returns the value of the '<em><b>Configured Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configured Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configured Feature</em>' reference.
	 * @see #setConfiguredFeature(MonitoringFeature)
	 * @see org.slasoi.monitoring.common.configuration.ConfigurationPackage#getSensorConfiguration_ConfiguredFeature()
	 * @model
	 * @generated
	 */
	MonitoringFeature getConfiguredFeature();

	/**
	 * Sets the value of the '{@link org.slasoi.monitoring.common.configuration.SensorConfiguration#getConfiguredFeature <em>Configured Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configured Feature</em>' reference.
	 * @see #getConfiguredFeature()
	 * @generated
	 */
	void setConfiguredFeature(MonitoringFeature value);

} // SensorConfiguration
