/**
 *  SVN FILE: $Id: ServiceBuilder.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/ServiceBuilder.java $
 */

package org.slasoi.models.scm;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.ServiceBuilder#getImplementation <em>Implementation</em>}</li>
 *   <li>{@link org.slasoi.models.scm.ServiceBuilder#getBindingsList <em>Bindings</em>}</li>
 *   <li>{@link org.slasoi.models.scm.ServiceBuilder#getServiceConfigurationsList <em>Service Configurations</em>}</li>
 *   <li>{@link org.slasoi.models.scm.ServiceBuilder#getUuid <em>Uuid</em>}</li>
 *   <li>{@link org.slasoi.models.scm.ServiceBuilder#getMonitoringSystemConfiguration <em>Monitoring System Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder()
 * @model
 * @generated
 */
public interface ServiceBuilder extends EObject {
	/**
	 * Returns the value of the '<em><b>Implementation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation</em>' reference.
	 * @see #setImplementation(ServiceImplementation)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder_Implementation()
	 * @model required="true"
	 * @generated
	 */
	ServiceImplementation getImplementation();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.ServiceBuilder#getImplementation <em>Implementation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation</em>' reference.
	 * @see #getImplementation()
	 * @generated
	 */
	void setImplementation(ServiceImplementation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ServiceBinding[] getBindings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ServiceBinding getBindings(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getBindingsLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setBindings(ServiceBinding[] newBindings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setBindings(int index, ServiceBinding element);

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.slasoi.models.scm.ServiceBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder_Bindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServiceBinding> getBindingsList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationDirective[] getServiceConfigurations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationDirective getServiceConfigurations(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getServiceConfigurationsLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setServiceConfigurations(ConfigurationDirective[] newServiceConfigurations);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setServiceConfigurations(int index, ConfigurationDirective element);

	/**
	 * Returns the value of the '<em><b>Service Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link org.slasoi.models.scm.ConfigurationDirective}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Configurations</em>' containment reference list.
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder_ServiceConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConfigurationDirective> getServiceConfigurationsList();

	/**
	 * Returns the value of the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uuid</em>' attribute.
	 * @see #setUuid(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder_Uuid()
	 * @model id="true"
	 * @generated
	 */
	String getUuid();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.ServiceBuilder#getUuid <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uuid</em>' attribute.
	 * @see #getUuid()
	 * @generated
	 */
	void setUuid(String value);

	/**
	 * Returns the value of the '<em><b>Monitoring System Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monitoring System Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitoring System Configuration</em>' containment reference.
	 * @see #setMonitoringSystemConfiguration(MonitoringSystemConfiguration)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getServiceBuilder_MonitoringSystemConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	MonitoringSystemConfiguration getMonitoringSystemConfiguration();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.ServiceBuilder#getMonitoringSystemConfiguration <em>Monitoring System Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitoring System Configuration</em>' containment reference.
	 * @see #getMonitoringSystemConfiguration()
	 * @generated
	 */
	void setMonitoringSystemConfiguration(MonitoringSystemConfiguration value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean validate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model slaTemplateDataType="org.slasoi.models.scm.SLATemplate"
	 * @generated
	 */
	void addBinding(Dependency serviceDependency, SLATemplate slaTemplate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Dependency> getOpenDependencies();

} // ServiceBuilder
