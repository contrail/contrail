/**
 *  SVN FILE: $Id: VirtualAppliance.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/VirtualAppliance.java $
 */

package org.slasoi.models.scm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Virtual Appliance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.VirtualAppliance#getHyperVisor <em>Hyper Visor</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualAppliance#getVersion <em>Version</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualAppliance#getHypervisorVersion <em>Hypervisor Version</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualAppliance#getApplianceType <em>Appliance Type</em>}</li>
 *   <li>{@link org.slasoi.models.scm.VirtualAppliance#getImageFormat <em>Image Format</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance()
 * @model extendedMetaData="name='HypervisorVersion'"
 * @generated
 */
public interface VirtualAppliance extends DeploymentArtefact {
	/**
	 * Returns the value of the '<em><b>Hyper Visor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hyper Visor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hyper Visor</em>' attribute.
	 * @see #setHyperVisor(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance_HyperVisor()
	 * @model required="true"
	 *        extendedMetaData="name='HypervisorVersion'"
	 * @generated
	 */
	String getHyperVisor();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualAppliance#getHyperVisor <em>Hyper Visor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hyper Visor</em>' attribute.
	 * @see #getHyperVisor()
	 * @generated
	 */
	void setHyperVisor(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance_Version()
	 * @model extendedMetaData="name='HypervisorVersion'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualAppliance#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Hypervisor Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hypervisor Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hypervisor Version</em>' attribute.
	 * @see #setHypervisorVersion(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance_HypervisorVersion()
	 * @model
	 * @generated
	 */
	String getHypervisorVersion();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualAppliance#getHypervisorVersion <em>Hypervisor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hypervisor Version</em>' attribute.
	 * @see #getHypervisorVersion()
	 * @generated
	 */
	void setHypervisorVersion(String value);

	/**
	 * Returns the value of the '<em><b>Appliance Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Appliance Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Appliance Type</em>' attribute.
	 * @see #setApplianceType(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance_ApplianceType()
	 * @model extendedMetaData="name='HypervisorVersion'"
	 * @generated
	 */
	String getApplianceType();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualAppliance#getApplianceType <em>Appliance Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Appliance Type</em>' attribute.
	 * @see #getApplianceType()
	 * @generated
	 */
	void setApplianceType(String value);

	/**
	 * Returns the value of the '<em><b>Image Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Format</em>' attribute.
	 * @see #setImageFormat(String)
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#getVirtualAppliance_ImageFormat()
	 * @model extendedMetaData="name='HypervisorVersion'"
	 * @generated
	 */
	String getImageFormat();

	/**
	 * Sets the value of the '{@link org.slasoi.models.scm.VirtualAppliance#getImageFormat <em>Image Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Format</em>' attribute.
	 * @see #getImageFormat()
	 * @generated
	 */
	void setImageFormat(String value);

} // VirtualAppliance
