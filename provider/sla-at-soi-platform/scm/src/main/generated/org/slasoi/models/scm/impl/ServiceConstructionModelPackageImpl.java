/**
 *  SVN FILE: $Id: ServiceConstructionModelPackageImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceConstructionModelPackageImpl.java $
 */

package org.slasoi.models.scm.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.slasoi.models.scm.ApplicationSoftware;
import org.slasoi.models.scm.AtomicWebService;
import org.slasoi.models.scm.CompositeWebService;
import org.slasoi.models.scm.ConfigType;
import org.slasoi.models.scm.ConfigurableServiceFeature;
import org.slasoi.models.scm.ConfigurationDirective;
import org.slasoi.models.scm.DataArtefact;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.DependencyType;
import org.slasoi.models.scm.DeploymentArtefact;
import org.slasoi.models.scm.ExecutionSoftware;
import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ProvisioningInformation;
import org.slasoi.models.scm.ResourceArtefact;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.models.scm.ServiceLandscapeElement;
import org.slasoi.models.scm.ServiceTopology;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.models.scm.SoftwareArchive;
import org.slasoi.models.scm.SoftwareElement;
import org.slasoi.models.scm.VirtualAppliance;
import org.slasoi.models.scm.VirtualMachineArtefact;
import org.slasoi.models.scm.WebServiceElement;
import org.slasoi.monitoring.common.configuration.ConfigurationPackage;
import org.slasoi.monitoring.common.configuration.impl.ConfigurationPackageImpl;
import org.slasoi.monitoring.common.features.FeaturesPackage;
import org.slasoi.monitoring.common.features.impl.FeaturesPackageImpl;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServiceConstructionModelPackageImpl extends EPackageImpl implements ServiceConstructionModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceBuilderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass implementationArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deploymentArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass virtualApplianceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareArchiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurableServiceFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceTopologyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceLandscapeElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass webServiceElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeWebServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atomicWebServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicationSoftwareEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionSoftwareEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass landscapeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass provisioningInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass virtualMachineArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum configTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dependencyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType slaTemplateEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType endpointEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.slasoi.models.scm.ServiceConstructionModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ServiceConstructionModelPackageImpl() {
		super(eNS_URI, ServiceConstructionModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ServiceConstructionModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ServiceConstructionModelPackage init() {
		if (isInited) return (ServiceConstructionModelPackage)EPackage.Registry.INSTANCE.getEPackage(ServiceConstructionModelPackage.eNS_URI);

		// Obtain or create and register package
		ServiceConstructionModelPackageImpl theServiceConstructionModelPackage = (ServiceConstructionModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ServiceConstructionModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ServiceConstructionModelPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		FeaturesPackageImpl theFeaturesPackage = (FeaturesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FeaturesPackage.eNS_URI) instanceof FeaturesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FeaturesPackage.eNS_URI) : FeaturesPackage.eINSTANCE);
		ConfigurationPackageImpl theConfigurationPackage = (ConfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConfigurationPackage.eNS_URI) instanceof ConfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConfigurationPackage.eNS_URI) : ConfigurationPackage.eINSTANCE);

		// Create package meta-data objects
		theServiceConstructionModelPackage.createPackageContents();
		theFeaturesPackage.createPackageContents();
		theConfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theServiceConstructionModelPackage.initializePackageContents();
		theFeaturesPackage.initializePackageContents();
		theConfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theServiceConstructionModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ServiceConstructionModelPackage.eNS_URI, theServiceConstructionModelPackage);
		return theServiceConstructionModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceType() {
		return serviceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceType_ID() {
		return (EAttribute)serviceTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceType_ServiceTypeName() {
		return (EAttribute)serviceTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceType_Description() {
		return (EAttribute)serviceTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceType_Interfaces() {
		return (EAttribute)serviceTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceImplementation() {
		return serviceImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceImplementation_Type() {
		return (EReference)serviceImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceImplementation_Artefacts() {
		return (EReference)serviceImplementationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceImplementation_ID() {
		return (EAttribute)serviceImplementationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceImplementation_ServiceImplementationName() {
		return (EAttribute)serviceImplementationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceImplementation_Description() {
		return (EAttribute)serviceImplementationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceImplementation_Version() {
		return (EAttribute)serviceImplementationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceImplementation_ProvisioningInformation() {
		return (EReference)serviceImplementationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceImplementation_ComponentMonitoringFeatures() {
		return (EReference)serviceImplementationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceBuilder() {
		return serviceBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceBuilder_Implementation() {
		return (EReference)serviceBuilderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceBuilder_Bindings() {
		return (EReference)serviceBuilderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceBuilder_ServiceConfigurations() {
		return (EReference)serviceBuilderEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceBuilder_Uuid() {
		return (EAttribute)serviceBuilderEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceBuilder_MonitoringSystemConfiguration() {
		return (EReference)serviceBuilderEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceBinding() {
		return serviceBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceBinding_Dependency() {
		return (EReference)serviceBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceBinding_SlaTemplate() {
		return (EAttribute)serviceBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceInstance() {
		return serviceInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceInstance_Implementation() {
		return (EReference)serviceInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceInstance_ID() {
		return (EAttribute)serviceInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceInstance_ServiceInstanceName() {
		return (EAttribute)serviceInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceInstance_Description() {
		return (EAttribute)serviceInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceInstance_InstancedOn() {
		return (EAttribute)serviceInstanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceInstance_Endpoints() {
		return (EAttribute)serviceInstanceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImplementationArtefact() {
		return implementationArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImplementationArtefact_ID() {
		return (EAttribute)implementationArtefactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImplementationArtefact_Name() {
		return (EAttribute)implementationArtefactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImplementationArtefact_Desciption() {
		return (EAttribute)implementationArtefactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImplementationArtefact_Dependencies() {
		return (EReference)implementationArtefactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImplementationArtefact_ServiceFeatures() {
		return (EReference)implementationArtefactEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependency_Name() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependency_Description() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependency_TargetURI() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDependency_TargetType() {
		return (EReference)dependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependency_ID() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependency_DepType() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeploymentArtefact() {
		return deploymentArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataArtefact() {
		return dataArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVirtualAppliance() {
		return virtualApplianceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualAppliance_HyperVisor() {
		return (EAttribute)virtualApplianceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualAppliance_Version() {
		return (EAttribute)virtualApplianceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualAppliance_HypervisorVersion() {
		return (EAttribute)virtualApplianceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualAppliance_ApplianceType() {
		return (EAttribute)virtualApplianceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualAppliance_ImageFormat() {
		return (EAttribute)virtualApplianceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareArchive() {
		return softwareArchiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftwareArchive_ArchiveFormat() {
		return (EAttribute)softwareArchiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurableServiceFeature() {
		return configurableServiceFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurableServiceFeature_ID() {
		return (EAttribute)configurableServiceFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurableServiceFeature_ConfigType() {
		return (EAttribute)configurableServiceFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurableServiceFeature_ConfigFile() {
		return (EAttribute)configurableServiceFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurableServiceFeature_ParameterIdentifier() {
		return (EAttribute)configurableServiceFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurableServiceFeature_DefaultValue() {
		return (EAttribute)configurableServiceFeatureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceTopology() {
		return serviceTopologyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceTopology_ID() {
		return (EAttribute)serviceTopologyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceTopology_ServiceTopologyName() {
		return (EAttribute)serviceTopologyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceTopology_Description() {
		return (EAttribute)serviceTopologyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceTopology_Type() {
		return (EReference)serviceTopologyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceTopology_LandscapeElements() {
		return (EReference)serviceTopologyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceLandscapeElement() {
		return serviceLandscapeElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceLandscapeElement_ID() {
		return (EAttribute)serviceLandscapeElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceLandscapeElement_Name() {
		return (EAttribute)serviceLandscapeElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServiceLandscapeElement_Desription() {
		return (EAttribute)serviceLandscapeElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareElement() {
		return softwareElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftwareElement_Version() {
		return (EAttribute)softwareElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareElement_Requires() {
		return (EReference)softwareElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftwareElement_Provider() {
		return (EAttribute)softwareElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWebServiceElement() {
		return webServiceElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebServiceElement_Requires() {
		return (EReference)webServiceElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeWebService() {
		return compositeWebServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeWebService_Contains() {
		return (EReference)compositeWebServiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtomicWebService() {
		return atomicWebServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicationSoftware() {
		return applicationSoftwareEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionSoftware() {
		return executionSoftwareEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationDirective() {
		return configurationDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfigurationDirective_ParameterValue() {
		return (EAttribute)configurationDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationDirective_ServiceFeature() {
		return (EReference)configurationDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLandscape() {
		return landscapeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_Instances() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_ProvidedTypes() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_Implementations() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_Topologies() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_Builders() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLandscape_RequiredTypes() {
		return (EReference)landscapeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvisioningInformation() {
		return provisioningInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProvisioningInformation_LeadTime() {
		return (EAttribute)provisioningInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvisioningInformation_BootOrder() {
		return (EReference)provisioningInformationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVirtualMachineArtefact() {
		return virtualMachineArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualMachineArtefact_NumberOfCores() {
		return (EAttribute)virtualMachineArtefactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualMachineArtefact_CpuSpeed() {
		return (EAttribute)virtualMachineArtefactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualMachineArtefact_CpuType() {
		return (EAttribute)virtualMachineArtefactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualMachineArtefact_MemorySize() {
		return (EAttribute)virtualMachineArtefactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceArtefact() {
		return resourceArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConfigType() {
		return configTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDependencyType() {
		return dependencyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSLATemplate() {
		return slaTemplateEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEndpoint() {
		return endpointEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceConstructionModelFactory getServiceConstructionModelFactory() {
		return (ServiceConstructionModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		serviceTypeEClass = createEClass(SERVICE_TYPE);
		createEAttribute(serviceTypeEClass, SERVICE_TYPE__ID);
		createEAttribute(serviceTypeEClass, SERVICE_TYPE__SERVICE_TYPE_NAME);
		createEAttribute(serviceTypeEClass, SERVICE_TYPE__DESCRIPTION);
		createEAttribute(serviceTypeEClass, SERVICE_TYPE__INTERFACES);

		serviceImplementationEClass = createEClass(SERVICE_IMPLEMENTATION);
		createEReference(serviceImplementationEClass, SERVICE_IMPLEMENTATION__TYPE);
		createEReference(serviceImplementationEClass, SERVICE_IMPLEMENTATION__ARTEFACTS);
		createEAttribute(serviceImplementationEClass, SERVICE_IMPLEMENTATION__ID);
		createEAttribute(serviceImplementationEClass, SERVICE_IMPLEMENTATION__SERVICE_IMPLEMENTATION_NAME);
		createEAttribute(serviceImplementationEClass, SERVICE_IMPLEMENTATION__DESCRIPTION);
		createEAttribute(serviceImplementationEClass, SERVICE_IMPLEMENTATION__VERSION);
		createEReference(serviceImplementationEClass, SERVICE_IMPLEMENTATION__PROVISIONING_INFORMATION);
		createEReference(serviceImplementationEClass, SERVICE_IMPLEMENTATION__COMPONENT_MONITORING_FEATURES);

		serviceBuilderEClass = createEClass(SERVICE_BUILDER);
		createEReference(serviceBuilderEClass, SERVICE_BUILDER__IMPLEMENTATION);
		createEReference(serviceBuilderEClass, SERVICE_BUILDER__BINDINGS);
		createEReference(serviceBuilderEClass, SERVICE_BUILDER__SERVICE_CONFIGURATIONS);
		createEAttribute(serviceBuilderEClass, SERVICE_BUILDER__UUID);
		createEReference(serviceBuilderEClass, SERVICE_BUILDER__MONITORING_SYSTEM_CONFIGURATION);

		serviceBindingEClass = createEClass(SERVICE_BINDING);
		createEReference(serviceBindingEClass, SERVICE_BINDING__DEPENDENCY);
		createEAttribute(serviceBindingEClass, SERVICE_BINDING__SLA_TEMPLATE);

		serviceInstanceEClass = createEClass(SERVICE_INSTANCE);
		createEReference(serviceInstanceEClass, SERVICE_INSTANCE__IMPLEMENTATION);
		createEAttribute(serviceInstanceEClass, SERVICE_INSTANCE__ID);
		createEAttribute(serviceInstanceEClass, SERVICE_INSTANCE__SERVICE_INSTANCE_NAME);
		createEAttribute(serviceInstanceEClass, SERVICE_INSTANCE__DESCRIPTION);
		createEAttribute(serviceInstanceEClass, SERVICE_INSTANCE__INSTANCED_ON);
		createEAttribute(serviceInstanceEClass, SERVICE_INSTANCE__ENDPOINTS);

		implementationArtefactEClass = createEClass(IMPLEMENTATION_ARTEFACT);
		createEAttribute(implementationArtefactEClass, IMPLEMENTATION_ARTEFACT__ID);
		createEAttribute(implementationArtefactEClass, IMPLEMENTATION_ARTEFACT__NAME);
		createEAttribute(implementationArtefactEClass, IMPLEMENTATION_ARTEFACT__DESCIPTION);
		createEReference(implementationArtefactEClass, IMPLEMENTATION_ARTEFACT__DEPENDENCIES);
		createEReference(implementationArtefactEClass, IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES);

		dependencyEClass = createEClass(DEPENDENCY);
		createEAttribute(dependencyEClass, DEPENDENCY__NAME);
		createEAttribute(dependencyEClass, DEPENDENCY__DESCRIPTION);
		createEAttribute(dependencyEClass, DEPENDENCY__TARGET_URI);
		createEReference(dependencyEClass, DEPENDENCY__TARGET_TYPE);
		createEAttribute(dependencyEClass, DEPENDENCY__ID);
		createEAttribute(dependencyEClass, DEPENDENCY__DEP_TYPE);

		deploymentArtefactEClass = createEClass(DEPLOYMENT_ARTEFACT);

		dataArtefactEClass = createEClass(DATA_ARTEFACT);

		virtualApplianceEClass = createEClass(VIRTUAL_APPLIANCE);
		createEAttribute(virtualApplianceEClass, VIRTUAL_APPLIANCE__HYPER_VISOR);
		createEAttribute(virtualApplianceEClass, VIRTUAL_APPLIANCE__VERSION);
		createEAttribute(virtualApplianceEClass, VIRTUAL_APPLIANCE__HYPERVISOR_VERSION);
		createEAttribute(virtualApplianceEClass, VIRTUAL_APPLIANCE__APPLIANCE_TYPE);
		createEAttribute(virtualApplianceEClass, VIRTUAL_APPLIANCE__IMAGE_FORMAT);

		softwareArchiveEClass = createEClass(SOFTWARE_ARCHIVE);
		createEAttribute(softwareArchiveEClass, SOFTWARE_ARCHIVE__ARCHIVE_FORMAT);

		configurableServiceFeatureEClass = createEClass(CONFIGURABLE_SERVICE_FEATURE);
		createEAttribute(configurableServiceFeatureEClass, CONFIGURABLE_SERVICE_FEATURE__ID);
		createEAttribute(configurableServiceFeatureEClass, CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE);
		createEAttribute(configurableServiceFeatureEClass, CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE);
		createEAttribute(configurableServiceFeatureEClass, CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER);
		createEAttribute(configurableServiceFeatureEClass, CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE);

		serviceTopologyEClass = createEClass(SERVICE_TOPOLOGY);
		createEAttribute(serviceTopologyEClass, SERVICE_TOPOLOGY__ID);
		createEAttribute(serviceTopologyEClass, SERVICE_TOPOLOGY__SERVICE_TOPOLOGY_NAME);
		createEAttribute(serviceTopologyEClass, SERVICE_TOPOLOGY__DESCRIPTION);
		createEReference(serviceTopologyEClass, SERVICE_TOPOLOGY__TYPE);
		createEReference(serviceTopologyEClass, SERVICE_TOPOLOGY__LANDSCAPE_ELEMENTS);

		serviceLandscapeElementEClass = createEClass(SERVICE_LANDSCAPE_ELEMENT);
		createEAttribute(serviceLandscapeElementEClass, SERVICE_LANDSCAPE_ELEMENT__ID);
		createEAttribute(serviceLandscapeElementEClass, SERVICE_LANDSCAPE_ELEMENT__NAME);
		createEAttribute(serviceLandscapeElementEClass, SERVICE_LANDSCAPE_ELEMENT__DESRIPTION);

		softwareElementEClass = createEClass(SOFTWARE_ELEMENT);
		createEAttribute(softwareElementEClass, SOFTWARE_ELEMENT__VERSION);
		createEReference(softwareElementEClass, SOFTWARE_ELEMENT__REQUIRES);
		createEAttribute(softwareElementEClass, SOFTWARE_ELEMENT__PROVIDER);

		webServiceElementEClass = createEClass(WEB_SERVICE_ELEMENT);
		createEReference(webServiceElementEClass, WEB_SERVICE_ELEMENT__REQUIRES);

		compositeWebServiceEClass = createEClass(COMPOSITE_WEB_SERVICE);
		createEReference(compositeWebServiceEClass, COMPOSITE_WEB_SERVICE__CONTAINS);

		atomicWebServiceEClass = createEClass(ATOMIC_WEB_SERVICE);

		applicationSoftwareEClass = createEClass(APPLICATION_SOFTWARE);

		executionSoftwareEClass = createEClass(EXECUTION_SOFTWARE);

		configurationDirectiveEClass = createEClass(CONFIGURATION_DIRECTIVE);
		createEAttribute(configurationDirectiveEClass, CONFIGURATION_DIRECTIVE__PARAMETER_VALUE);
		createEReference(configurationDirectiveEClass, CONFIGURATION_DIRECTIVE__SERVICE_FEATURE);

		landscapeEClass = createEClass(LANDSCAPE);
		createEReference(landscapeEClass, LANDSCAPE__INSTANCES);
		createEReference(landscapeEClass, LANDSCAPE__PROVIDED_TYPES);
		createEReference(landscapeEClass, LANDSCAPE__IMPLEMENTATIONS);
		createEReference(landscapeEClass, LANDSCAPE__TOPOLOGIES);
		createEReference(landscapeEClass, LANDSCAPE__BUILDERS);
		createEReference(landscapeEClass, LANDSCAPE__REQUIRED_TYPES);

		provisioningInformationEClass = createEClass(PROVISIONING_INFORMATION);
		createEAttribute(provisioningInformationEClass, PROVISIONING_INFORMATION__LEAD_TIME);
		createEReference(provisioningInformationEClass, PROVISIONING_INFORMATION__BOOT_ORDER);

		resourceArtefactEClass = createEClass(RESOURCE_ARTEFACT);

		virtualMachineArtefactEClass = createEClass(VIRTUAL_MACHINE_ARTEFACT);
		createEAttribute(virtualMachineArtefactEClass, VIRTUAL_MACHINE_ARTEFACT__NUMBER_OF_CORES);
		createEAttribute(virtualMachineArtefactEClass, VIRTUAL_MACHINE_ARTEFACT__MEMORY_SIZE);
		createEAttribute(virtualMachineArtefactEClass, VIRTUAL_MACHINE_ARTEFACT__CPU_SPEED);
		createEAttribute(virtualMachineArtefactEClass, VIRTUAL_MACHINE_ARTEFACT__CPU_TYPE);

		// Create enums
		configTypeEEnum = createEEnum(CONFIG_TYPE);
		dependencyTypeEEnum = createEEnum(DEPENDENCY_TYPE);

		// Create data types
		slaTemplateEDataType = createEDataType(SLA_TEMPLATE);
		endpointEDataType = createEDataType(ENDPOINT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FeaturesPackage theFeaturesPackage = (FeaturesPackage)EPackage.Registry.INSTANCE.getEPackage(FeaturesPackage.eNS_URI);
		ConfigurationPackage theConfigurationPackage = (ConfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ConfigurationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		deploymentArtefactEClass.getESuperTypes().add(this.getImplementationArtefact());
		dataArtefactEClass.getESuperTypes().add(this.getImplementationArtefact());
		virtualApplianceEClass.getESuperTypes().add(this.getDeploymentArtefact());
		softwareArchiveEClass.getESuperTypes().add(this.getDeploymentArtefact());
		softwareElementEClass.getESuperTypes().add(this.getServiceLandscapeElement());
		webServiceElementEClass.getESuperTypes().add(this.getServiceLandscapeElement());
		compositeWebServiceEClass.getESuperTypes().add(this.getWebServiceElement());
		atomicWebServiceEClass.getESuperTypes().add(this.getWebServiceElement());
		applicationSoftwareEClass.getESuperTypes().add(this.getSoftwareElement());
		executionSoftwareEClass.getESuperTypes().add(this.getSoftwareElement());
		resourceArtefactEClass.getESuperTypes().add(this.getImplementationArtefact());
		virtualMachineArtefactEClass.getESuperTypes().add(this.getResourceArtefact());

		// Initialize classes and features; add operations and parameters
		initEClass(serviceTypeEClass, ServiceType.class, "ServiceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServiceType_ID(), ecorePackage.getEString(), "ID", "", 1, 1, ServiceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceType_ServiceTypeName(), ecorePackage.getEString(), "ServiceTypeName", null, 1, 1, ServiceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceType_Description(), ecorePackage.getEString(), "Description", null, 0, 1, ServiceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceType_Interfaces(), ecorePackage.getEString(), "Interfaces", null, 0, -1, ServiceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceImplementationEClass, ServiceImplementation.class, "ServiceImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceImplementation_Type(), this.getServiceType(), null, "type", null, 1, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceImplementation_Artefacts(), this.getImplementationArtefact(), null, "artefacts", null, 1, -1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceImplementation_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceImplementation_ServiceImplementationName(), ecorePackage.getEString(), "ServiceImplementationName", null, 1, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceImplementation_Description(), ecorePackage.getEString(), "Description", null, 0, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceImplementation_Version(), ecorePackage.getEString(), "Version", null, 0, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceImplementation_ProvisioningInformation(), this.getProvisioningInformation(), null, "provisioningInformation", null, 0, 1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceImplementation_ComponentMonitoringFeatures(), theFeaturesPackage.getComponentMonitoringFeatures(), null, "componentMonitoringFeatures", null, 0, -1, ServiceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(serviceImplementationEClass, this.getDependency(), "getDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(serviceImplementationEClass, this.getConfigurableServiceFeature(), "getConfigurableServiceFeatures", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(serviceBuilderEClass, ServiceBuilder.class, "ServiceBuilder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceBuilder_Implementation(), this.getServiceImplementation(), null, "implementation", null, 1, 1, ServiceBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceBuilder_Bindings(), this.getServiceBinding(), null, "bindings", null, 0, -1, ServiceBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceBuilder_ServiceConfigurations(), this.getConfigurationDirective(), null, "serviceConfigurations", null, 0, -1, ServiceBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceBuilder_Uuid(), ecorePackage.getEString(), "uuid", null, 0, 1, ServiceBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceBuilder_MonitoringSystemConfiguration(), theConfigurationPackage.getMonitoringSystemConfiguration(), null, "monitoringSystemConfiguration", null, 0, 1, ServiceBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(serviceBuilderEClass, ecorePackage.getEBoolean(), "validate", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(serviceBuilderEClass, null, "addBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDependency(), "serviceDependency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSLATemplate(), "slaTemplate", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(serviceBuilderEClass, this.getDependency(), "getOpenDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(serviceBindingEClass, ServiceBinding.class, "ServiceBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceBinding_Dependency(), this.getDependency(), null, "dependency", null, 1, 1, ServiceBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceBinding_SlaTemplate(), this.getSLATemplate(), "slaTemplate", "", 0, 1, ServiceBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceInstanceEClass, ServiceInstance.class, "ServiceInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceInstance_Implementation(), this.getServiceImplementation(), null, "implementation", null, 1, 1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInstance_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInstance_ServiceInstanceName(), ecorePackage.getEString(), "ServiceInstanceName", null, 1, 1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInstance_Description(), ecorePackage.getEString(), "Description", null, 0, 1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInstance_InstancedOn(), ecorePackage.getEDate(), "InstancedOn", null, 1, 1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInstance_Endpoints(), this.getEndpoint(), "Endpoints", null, 0, -1, ServiceInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(implementationArtefactEClass, ImplementationArtefact.class, "ImplementationArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImplementationArtefact_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ImplementationArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImplementationArtefact_Name(), ecorePackage.getEString(), "Name", null, 1, 1, ImplementationArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImplementationArtefact_Desciption(), ecorePackage.getEString(), "Desciption", null, 0, 1, ImplementationArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImplementationArtefact_Dependencies(), this.getDependency(), null, "dependencies", null, 0, -1, ImplementationArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImplementationArtefact_ServiceFeatures(), this.getConfigurableServiceFeature(), null, "serviceFeatures", null, 0, -1, ImplementationArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dependencyEClass, Dependency.class, "Dependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDependency_Name(), ecorePackage.getEString(), "Name", null, 1, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Description(), ecorePackage.getEString(), "Description", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_TargetURI(), ecorePackage.getEString(), "TargetURI", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDependency_TargetType(), this.getServiceType(), null, "targetType", null, 1, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_ID(), ecorePackage.getEString(), "ID", null, 1, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_DepType(), this.getDependencyType(), "DepType", "SingleBindingDep", 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deploymentArtefactEClass, DeploymentArtefact.class, "DeploymentArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataArtefactEClass, DataArtefact.class, "DataArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(virtualApplianceEClass, VirtualAppliance.class, "VirtualAppliance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVirtualAppliance_HyperVisor(), ecorePackage.getEString(), "HyperVisor", null, 1, 1, VirtualAppliance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualAppliance_Version(), ecorePackage.getEString(), "Version", null, 0, 1, VirtualAppliance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualAppliance_HypervisorVersion(), ecorePackage.getEString(), "HypervisorVersion", null, 0, 1, VirtualAppliance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualAppliance_ApplianceType(), ecorePackage.getEString(), "ApplianceType", null, 0, 1, VirtualAppliance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualAppliance_ImageFormat(), ecorePackage.getEString(), "ImageFormat", null, 0, 1, VirtualAppliance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softwareArchiveEClass, SoftwareArchive.class, "SoftwareArchive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoftwareArchive_ArchiveFormat(), ecorePackage.getEString(), "ArchiveFormat", null, 0, 1, SoftwareArchive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(configurableServiceFeatureEClass, ConfigurableServiceFeature.class, "ConfigurableServiceFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConfigurableServiceFeature_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ConfigurableServiceFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigurableServiceFeature_ConfigType(), this.getConfigType(), "ConfigType", null, 0, 1, ConfigurableServiceFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigurableServiceFeature_ConfigFile(), ecorePackage.getEString(), "ConfigFile", null, 0, 1, ConfigurableServiceFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigurableServiceFeature_ParameterIdentifier(), ecorePackage.getEString(), "ParameterIdentifier", null, 0, 1, ConfigurableServiceFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigurableServiceFeature_DefaultValue(), ecorePackage.getEString(), "DefaultValue", null, 0, 1, ConfigurableServiceFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceTopologyEClass, ServiceTopology.class, "ServiceTopology", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServiceTopology_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ServiceTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceTopology_ServiceTopologyName(), ecorePackage.getEString(), "ServiceTopologyName", null, 1, 1, ServiceTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceTopology_Description(), ecorePackage.getEString(), "Description", null, 0, 1, ServiceTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceTopology_Type(), this.getServiceType(), null, "type", null, 1, 1, ServiceTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceTopology_LandscapeElements(), this.getServiceLandscapeElement(), null, "landscapeElements", null, 0, -1, ServiceTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceLandscapeElementEClass, ServiceLandscapeElement.class, "ServiceLandscapeElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServiceLandscapeElement_ID(), ecorePackage.getEString(), "ID", null, 1, 1, ServiceLandscapeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceLandscapeElement_Name(), ecorePackage.getEString(), "Name", null, 1, 1, ServiceLandscapeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceLandscapeElement_Desription(), ecorePackage.getEString(), "Desription", null, 0, 1, ServiceLandscapeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softwareElementEClass, SoftwareElement.class, "SoftwareElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoftwareElement_Version(), ecorePackage.getEString(), "Version", null, 0, 1, SoftwareElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareElement_Requires(), this.getSoftwareElement(), null, "requires", null, 0, -1, SoftwareElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftwareElement_Provider(), ecorePackage.getEString(), "Provider", null, 0, 1, SoftwareElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(webServiceElementEClass, WebServiceElement.class, "WebServiceElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWebServiceElement_Requires(), this.getSoftwareElement(), null, "requires", null, 0, -1, WebServiceElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositeWebServiceEClass, CompositeWebService.class, "CompositeWebService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeWebService_Contains(), this.getAtomicWebService(), null, "contains", null, 1, -1, CompositeWebService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(atomicWebServiceEClass, AtomicWebService.class, "AtomicWebService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(applicationSoftwareEClass, ApplicationSoftware.class, "ApplicationSoftware", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(executionSoftwareEClass, ExecutionSoftware.class, "ExecutionSoftware", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(configurationDirectiveEClass, ConfigurationDirective.class, "ConfigurationDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConfigurationDirective_ParameterValue(), ecorePackage.getEString(), "ParameterValue", null, 0, 1, ConfigurationDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigurationDirective_ServiceFeature(), this.getConfigurableServiceFeature(), null, "serviceFeature", null, 1, 1, ConfigurationDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(landscapeEClass, Landscape.class, "Landscape", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLandscape_Instances(), this.getServiceInstance(), null, "instances", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLandscape_ProvidedTypes(), this.getServiceType(), null, "providedTypes", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLandscape_Implementations(), this.getServiceImplementation(), null, "implementations", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLandscape_Topologies(), this.getServiceTopology(), null, "topologies", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLandscape_Builders(), this.getServiceBuilder(), null, "builders", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLandscape_RequiredTypes(), this.getServiceType(), null, "requiredTypes", null, 0, -1, Landscape.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(landscapeEClass, null, "addBuilder", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getServiceBuilder(), "builder", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(landscapeEClass, null, "addInstance", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getServiceInstance(), "instance", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(landscapeEClass, this.getServiceImplementation(), "queryServiceImplementations", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getServiceType(), "type", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(landscapeEClass, this.getServiceType(), "queryServiceTypes", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(provisioningInformationEClass, ProvisioningInformation.class, "ProvisioningInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProvisioningInformation_LeadTime(), ecorePackage.getEInt(), "LeadTime", null, 0, 1, ProvisioningInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvisioningInformation_BootOrder(), this.getImplementationArtefact(), null, "bootOrder", null, 0, -1, ProvisioningInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceArtefactEClass, ResourceArtefact.class, "ResourceArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(virtualMachineArtefactEClass, VirtualMachineArtefact.class, "VirtualMachineArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVirtualMachineArtefact_NumberOfCores(), ecorePackage.getEInt(), "numberOfCores", null, 0, 1, VirtualMachineArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualMachineArtefact_MemorySize(), ecorePackage.getEDouble(), "memorySize", null, 0, 1, VirtualMachineArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualMachineArtefact_CpuSpeed(), ecorePackage.getEDouble(), "cpuSpeed", null, 0, 1, VirtualMachineArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVirtualMachineArtefact_CpuType(), ecorePackage.getEString(), "cpuType", null, 0, 1, VirtualMachineArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(configTypeEEnum, ConfigType.class, "ConfigType");
		addEEnumLiteral(configTypeEEnum, ConfigType.ENV_VAR);
		addEEnumLiteral(configTypeEEnum, ConfigType.PROPERTY_FILE);
		addEEnumLiteral(configTypeEEnum, ConfigType.OTHER);
		addEEnumLiteral(configTypeEEnum, ConfigType.XML_FILE);

		initEEnum(dependencyTypeEEnum, DependencyType.class, "DependencyType");
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.SINGLE_BINDING_DEP);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.MULTIPLE_BINDINGS_DEP);

		// Initialize data types
		initEDataType(slaTemplateEDataType, SLATemplate.class, "SLATemplate", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(endpointEDataType, Endpoint.class, "Endpoint", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
		// null
		createNullAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (virtualApplianceEClass, 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (getVirtualAppliance_HyperVisor(), 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (getVirtualAppliance_Version(), 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (getVirtualAppliance_ApplianceType(), 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (getVirtualAppliance_ImageFormat(), 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (softwareArchiveEClass, 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });		
		addAnnotation
		  (getSoftwareArchive_ArchiveFormat(), 
		   source, 
		   new String[] {
			 "name", "HypervisorVersion"
		   });					
	}

	/**
	 * Initializes the annotations for <b>null</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createNullAnnotations() {
		String source = null;									
		addAnnotation
		  (slaTemplateEDataType, 
		   source, 
		   new String[] {
		   });		
		addAnnotation
		  (slaTemplateEDataType, 
		   source, 
		   new String[] {
		   });		
		addAnnotation
		  (slaTemplateEDataType, 
		   source, 
		   new String[] {
		   });		
		addAnnotation
		  (slaTemplateEDataType, 
		   source, 
		   new String[] {
		   });		
		addAnnotation
		  (slaTemplateEDataType, 
		   source, 
		   new String[] {
		   });
	}

} //ServiceConstructionModelPackageImpl
