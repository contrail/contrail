/**
 *  SVN FILE: $Id: ServiceBuilderExtended.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/extended/ServiceBuilderExtended.java $
 */

package org.slasoi.models.scm.extended;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.slasoi.models.scm.ConfigurableServiceFeature;
import org.slasoi.models.scm.ConfigurationDirective;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.DependencyType;
import org.slasoi.models.scm.ISoftwareServiceManager;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.exception.InvalidConfigurationException;
import org.slasoi.models.scm.impl.ServiceBuilderImpl;
import org.slasoi.monitoring.common.configuration.Component;
import org.slasoi.monitoring.common.configuration.ComponentConfiguration;
import org.slasoi.monitoring.common.configuration.ConfigurationFactory;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.configuration.SensorConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * A ServiceBuilder collects all information necessary to create a new ServiceInstance, in other words, to provision a
 * service.
 *
 * @author Jens Happe
 *
 */
public class ServiceBuilderExtended extends ServiceBuilderImpl {
    /**
     * ISoftwareServiceManager instance providing service management functionality.
     */
    private ISoftwareServiceManager manager;
    /**
     * String containing information about possible errors during validation of a builder object.
     */
    private String errorString;

    /**
     * Create a new builder.
     *
     * @param serviceImplementation
     */
    public ServiceBuilderExtended() {
        super();
        errorString = null;
    }

    /**
     * Getter for manager variable.
     *
     * @return Returns manager.
     */
    public final ISoftwareServiceManager getManager() {
        return manager;
    }

    /**
     * Setter for manager variable.
     *
     * @param manager
     *            Target reference to which this.manager has to be set.
     */
    public final void setManager(final ISoftwareServiceManager manager) {
        this.manager = manager;
    }

    /**
     * Binds a ServiceDependency of the ServiceImplementation to a new SLA(Template).
     *
     * @param dependency
     *            a valid dependency to a ServiceType needed.
     * @param slaTemplate
     *            describes the service that is to be used to resolve the dependency.
     */
    public final void addBinding(final Dependency dependency, final SLATemplate slaTemplate) {
        // assert that the dependency exists
        ServiceImplementation impl = this.getImplementation();
        List<Dependency> depList = impl.getDependencies();
        if (!depList.contains(dependency)) {
            throw new IllegalArgumentException("The dependency '" + dependency + "' is unknown to the implementation.");
        }

        // assert that the dependency has not be bound to another service
        // if it is a SingleBinding-DependencyType
        for (ServiceBinding sb : this.getBindingsList()) {
            if ((sb.getDependency().getDepType().equals(DependencyType.SINGLE_BINDING_DEP))
                    && (sb.getDependency().equals(dependency))) {
                throw new IllegalArgumentException("The dependency '" + dependency
                        + "' is already bound to an SLA(Template).");
            }
        }

        ServiceBinding binding = ServiceConstructionModel.getFactory().createServiceBinding();
        binding.setDependency(dependency);
        binding.setSlaTemplate(slaTemplate);
        this.getBindingsList().add(binding);
    }

    /**
     * Sets the configuration of the monitoring. If the configuration is invalid with respect to the available
     * monitoring features, an exception is thrown.
     *
     * @param monitoringConfiguration
     *            Configuration to be used for the service's monitoring.
     */
    public final void setMonitoringConfiguration(final MonitoringSystemConfiguration monitoringConfiguration) {
        super.setMonitoringSystemConfiguration(monitoringConfiguration);
    }

    /**
     * Overwrites the all bindings of the builder by the passed list of bindings.
     *
     * @param newBindings
     *            Array containing new bindings.
     */
    public final void setBindings(final ServiceBinding[] newBindings) {
        for (ServiceBinding binding : newBindings) {
            if (!this.getImplementation().getDependencies().contains(binding.getDependency())) {
                throw new IllegalArgumentException("The dependency '" + binding.getDependency() + "' of the binding '"
                        + binding + "' is unknown to the implementation.");
            }
        }
        super.setBindings(newBindings);
    }

    /**
     * Overwrites the binding at position index in the bindings list by the passed binding instance element.
     *
     * @param index
     *            Position in the array at wich the binding has to be overwritten.
     * @param element
     *            New binding object.
     */
    public final void setBindings(final int index, final ServiceBinding element) {
        if (!this.getImplementation().getDependencies().contains(element.getDependency())) {
            throw new IllegalArgumentException("The Dependency '" + element.getDependency() + "' of the binding '"
                    + element + "'is unknown to the implementation ");
        }
        super.setBindings(index, element);
    }

    /**
     * Sets the configuration of the service. If the configuration is invalid with respect to the available features of
     * the service, an exception is thrown.
     *
     * @param newServiceConfigurations
     *            Configuration to be used for the service.
     *
     */
    public final void setServiceConfigurations(final ConfigurationDirective[] newServiceConfigurations) {
        EList<ConfigurableServiceFeature> implFeatureList = this.getImplementation().getConfigurableServiceFeatures();
        for (ConfigurationDirective config : newServiceConfigurations) {
            if (!implFeatureList.contains(config.getServiceFeature())) {
                throw new InvalidConfigurationException("The service configuration '" + config
                        + "' is unknown to the implementation.");
            }
        }

        super.setServiceConfigurations(newServiceConfigurations);
    }

    /**
     * Setter for service configurations.
     *
     * @param index
     *            Position of the new configuration in the array.
     * @param element
     *            New configuration directive.
     */
    public final void setServiceConfigurations(final int index, final ConfigurationDirective element) {
        if (!this.getImplementation().getConfigurableServiceFeatures().contains(element.getServiceFeature())) {
            throw new InvalidConfigurationException("The service configuration '" + element.getServiceFeature()
                    + "' is unknown to the implementation.");
        }
        super.setServiceConfigurations(index, element);
    }

    /**
     * Validates the builder. Validation succeeds only if all dependencies are resolved, all configurations are known to
     * the implementation and each feature has at maximum one configuration directive.
     *
     * @return Returns true if builder is valid.
     *
     */
    public final boolean validate() {

        // Verify that all dependencies are resolved
        if (!getOpenDependencies().isEmpty()) {
            errorString =
                    "Validation failed! There are open " + "dependencies which have to be resolved before validation";
            return false;
        }

        // check for valid ServiceFeatureConfigurations

        List<ConfigurableServiceFeature> referencedFeatures = new ArrayList<ConfigurableServiceFeature>();
        for (ConfigurationDirective directive : getServiceConfigurationsList()) {
            if (!this.getImplementation().getConfigurableServiceFeatures().contains(directive.getServiceFeature())) {
                errorString =
                        "Validation failed! Invalid reference from ConfigurationDirective '" + directive
                                + "' to the ConfigurableServiceFeature '" + directive.getServiceFeature() + "'.";
                return false;
            }
            // fail if there's more than one configurationDirective for the same
            // ServiceFeature
            if (referencedFeatures.contains(directive.getServiceFeature())) {
                errorString =
                        "Validation failed! There is more than one ConfigurationDirective for the same ServiceFeature '"
                                + directive.getServiceFeature() + "'";
                return false;
            } else {
                referencedFeatures.add(directive.getServiceFeature());
            }
        }
        return true;
    }

    /**
     *
     * @return Returns a list of all unresolved dependencies.
     */
    public final EList<Dependency> getOpenDependencies() {
        EList<Dependency> resultList = new BasicEList<Dependency>();
        EList<Dependency> bindingDependencies = new BasicEList<Dependency>();

        for (ServiceBinding binding : this.getBindings()) {
            bindingDependencies.add(binding.getDependency());
        }

        for (Dependency dep : this.getImplementation().getDependencies()) {
            if (!bindingDependencies.contains(dep)) {
                resultList.add(dep);
            }
        }
        return resultList;
    }

    /**
     * Getter for errorString.
     *
     * @return Returns the errorString, providing information about failed validation.
     */
    public final String getErrorString() {
        return errorString;
    }

    /**
     * Enables Monitoring for feature mf.
     *
     * @param mf
     *            specified monitoring feature
     * @param termID
     *            specified termId
     */
    public final void enableMonitoring(final MonitoringFeature mf, final String termID) {
        SensorConfiguration sc = ConfigurationFactory.eINSTANCE.createSensorConfiguration();
        sc.setConfigurationId(termID);
        sc.setConfiguredFeature(mf);
        Component mc = getMonitoringComponent();
        mc.getConfigurationsList().add(sc);
    }

    /**
     *
     * Get the Component containing all MonitoringFeatures.
     *
     * @return Returns the main monitoring component.
     */
    private Component getMonitoringComponent() {
        MonitoringSystemConfiguration msc = this.getMonitoringSystemConfiguration();
        if (msc == null) {
            msc = ConfigurationFactory.eINSTANCE.createMonitoringSystemConfiguration();
            this.setMonitoringConfiguration(msc);
        }

        if (msc.getComponentsLength() == 0) {
            Component mc = ConfigurationFactory.eINSTANCE.createComponent();
            msc.getComponentsList().add(mc);
        }

        return msc.getComponents()[0];
    }

    /**
     *
     * @return Returns all MonitoringFeatures registered to this builder.
     */
    public final List<MonitoringFeature> getMonitoringFeatures() {
        List<MonitoringFeature> result = new ArrayList<MonitoringFeature>();

        for (ComponentMonitoringFeatures cmf : this.getImplementation().getComponentMonitoringFeaturesList()) {
            result.addAll(cmf.getMonitoringFeaturesList());
        }

        return result;
    }

    /**
     *
     * @return Returns the string representation of all Metrics that need to be observed (e.g. RESPONSE_TIME)
     */
    public final List<String> getMetricTypeIDs() {
        List<String> result = new ArrayList<String>();

        List<MonitoringFeature> mfList = getMonitoringFeatures();

        for (MonitoringFeature mf : mfList) {
            if (mf instanceof Event) {
                result.add(((Event) mf).getType());
            }
        }

        return result;
    }

    /**
     *
     * @param metricTypeID
     *            Metric type of interest (e.g., RESPONSE_TIME)
     * @return Returns the ID of the sensor collecting (or displaying) that metric.
     */
    public final String getSensorID(final String metricTypeID) {
        for (ComponentConfiguration cc : this.getMonitoringComponent().getConfigurations()) {
            if (cc instanceof SensorConfiguration) {
                SensorConfiguration sc = (SensorConfiguration) cc;
                if (sc.getConfiguredFeature() instanceof Event) {
                    if (((Event) sc.getConfiguredFeature()).getType().equals(metricTypeID)) {
                        return sc.getConfigurationId();
                    }
                }
            }
        }
        return "";
    }

    /**
     * Get the MonitoringFeature for a given type (e.g., RESPONSE_TIME).
     *
     * @param typeID
     *            Type of interest.
     * @return The MonitoringFeature of that type.
     */
    public final MonitoringFeature getMonitoringFeatureByType(final String typeID) {
        List<MonitoringFeature> mfList = getMonitoringFeatures();
        for (MonitoringFeature mf : mfList) {
            if (mf instanceof Event) {
                Event event = (Event) mf;
                if (event.getType().equals(typeID)) {
                    return event;
                }
            }
        }
        return null;
    }

}