/**
 *  SVN FILE: $Id: ServiceImplementationExtended.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/extended/ServiceImplementationExtended.java $
 */

package org.slasoi.models.scm.extended;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.slasoi.models.scm.ConfigurableServiceFeature;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.impl.ServiceImplementationImpl;

/**
 *
 * @author Alexander Wert
 *
 *         ServiceImplementationExtended extends the class ServiceImplementation by some additional functions.
 *         ServiceImplementation represents an implementation of a ServiceType.
 */
public class ServiceImplementationExtended extends ServiceImplementationImpl {

    /**
     * Returns a list of all dependencies.
     *
     * @return Returns a list of all dependencies.
     */
    @Override
    public final EList<Dependency> getDependencies() {
        EList<Dependency> result = new BasicEList<Dependency>();
        if (this.getArtefactsList() != null) {
            for (ImplementationArtefact ia : this.getArtefactsList()) {
                result.addAll(ia.getDependenciesList());
            }
        }
        return result;
    }

    /**
     * Returns a list of all configurable service features.
     *
     * @return Returns a list of all configurable service features.
     */
    @Override
    public final EList<ConfigurableServiceFeature> getConfigurableServiceFeatures() {
        EList<ConfigurableServiceFeature> result = new BasicEList<ConfigurableServiceFeature>();

        if (this.getArtefactsList() != null) {
            for (ImplementationArtefact ia : this.getArtefactsList()) {
                result.addAll(ia.getServiceFeaturesList());
            }
        }
        return result;
    }

}