/**
 *  SVN FILE: $Id: DependencyExtended.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/extended/DependencyExtended.java $
 */

package org.slasoi.models.scm.extended;

import org.slasoi.models.scm.impl.DependencyImpl;

/**
 * A ServiceDependency represents the explicit dependency on a ServiceType.
 *
 * Currently just a dummy implementation!!
 *
 * @author Jens Happe
 *
 */
public class DependencyExtended extends DependencyImpl {

    /**
     * Creates a new dependency (with a unique identifier) to a service type.
     *
     */
    public DependencyExtended() {
        super();
    }

    /**
     * @param o
     *            Object to be compared with.
     * @return Returns true if this object is equal to o.
     */
    @Override
    public final boolean equals(final Object o) {
        if (o instanceof DependencyExtended) {
            DependencyExtended d = (DependencyExtended) o;
            return d.getTargetType().equals(this.getTargetType()) && d.getID().equals(this.getID());
        }
        return false;
    }

    /**
     * Creates and returns the hashCode for a DependencyExtended Object.
     *
     * @return Returns an integer representing the hash-code of this object..
     */
    public final int hashCode() {
        int result = 0;
        if (this.getID() != null) {
            for (char c : this.getID().toCharArray()) {
                result += c;
            }
        }
        if (this.getTargetType() != null) {
            result += this.getTargetType().hashCode();
        }
        return result;
    }

}