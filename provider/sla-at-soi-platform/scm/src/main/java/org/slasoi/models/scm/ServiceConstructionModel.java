/**
 *  SVN FILE: $Id: ServiceConstructionModel.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/ServiceConstructionModel.java $
 */

package org.slasoi.models.scm;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.slasoi.models.scm.extended.ServiceConstructionModelFactoryExtended;

/**
 * Single entry point for the Service Construction Model.
 *
 * This class gives access to a singleton factory that allows instantiating all necessary objects.
 *
 * it allow
 *
 * @author Jens Happe
 *
 */
public final class ServiceConstructionModel {
    /**
     * This is the private constructor of ServiceConstructionModel class. Since all functions of
     * ServiceConstructionModel are static, no public constructor or creation function is needed.
     */
    private ServiceConstructionModel() {

    }

    /**
     * This is the singleton instance of the ServiceConstructionModelFactory.
     */
    private static ServiceConstructionModelFactory factory = null;

    /**
     * This function returns the singleton instance of the factory that allows instantiating all necessary objects.
     *
     * @return Returns singleton instance of ServiceConstructionModelFactory
     */
    public static synchronized ServiceConstructionModelFactory getFactory() {
        if (factory == null) {
            factory = new ServiceConstructionModelFactoryExtended();
            ServiceConstructionModelPackage.eINSTANCE.setEFactoryInstance(factory);
        }
        return factory;
    }

    /**
     * This operation loads an instance of EObject from file specified by the parameter fileName.
     *
     * @param fileName
     *            File name of the file, where the Object has to be loaded from.
     * @return Returns an instance of the loaded Object.
     */
    public static EObject loadFromXMI(final String fileName) {
        // initialize the correct factory;
        getFactory();

        // Create a resource set to hold the resources.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Register the appropriate resource factory to handle all file
        // extentions.
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
                Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

        // Register the package to ensure it is available during loading.
        resourceSet.getPackageRegistry().put(ServiceConstructionModelPackage.eNS_URI,
                ServiceConstructionModelPackage.eINSTANCE);

        // Construct the URI for the instance file.
        // The argument is treated as a file path only if it denotes an existing
        // file. Otherwise, it's directly treated as a URL.
        File file = new File(fileName);

        URI uri = null;
        if (file.isFile()) {
            uri = URI.createFileURI(file.getAbsolutePath());
        } else {
            uri = URI.createURI(fileName);
        }
        System.out.println(uri);

        // Demand load resource for this file.
        Resource resource = resourceSet.getResource(uri, true);

        EObject eObject = (EObject) resource.getContents().iterator().next();
        return EcoreUtil.getRootContainer(eObject);
    }

    /**
     * This operation saves an instance of EObject to a XMI-file.
     *
     * @param objectToSave
     *            Object which has to be saved.
     * @param fileName
     *            File name of the file, where the Objects has to be saved.
     * @throws IOException
     *             Throws an IOException
     */
    public static void saveToXMI(final EObject objectToSave, final String fileName) throws IOException {
        // initialize the correct factory;
        getFactory();

        // Create a resource set.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Register the default resource factory -- only needed for stand-alone!
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
                Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

        // Get the URI of the model file.
        URI fileURI = URI.createFileURI(new File(fileName).getAbsolutePath());

        // Create a resource for this file.
        Resource resource = resourceSet.createResource(fileURI);

        // Add the book and writer objects to the contents.
        resource.getContents().add(objectToSave);

        // Save the contents of the resource to the file system.
        resource.save(Collections.EMPTY_MAP);
    }

}