/**
 *  SVN FILE: $Id: LandscapeExtended.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/extended/LandscapeExtended.java $
 */

package org.slasoi.models.scm.extended;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.models.scm.impl.LandscapeImpl;

/**
 * This class extends the generated Landscape Class by some additional methods. A Landscape is the knowledge source for
 * all required and provided ServiceTypes, ServiceImplementations, ServiceInstances and ServiceBuilder objects.
 *
 * @author Alexander Wert
 *
 */
public class LandscapeExtended extends LandscapeImpl {

    /**
     * This method attaches a builder instance to the landscape.
     *
     * @param builder
     *            Builder to be added to the landscape.
     */
    public final void addBuilder(final ServiceBuilder builder) {
        if (builder == null) {
            throw new IllegalArgumentException("Value null as builder parameter is not permitted!");
        }
        if (!this.getBuildersList().contains(builder)) {
            this.builders.add(builder);
        }
    }

    /**
     * This method attaches a service instance object to the landscape.
     *
     * @param instance
     *            ServiceInstance to be attached
     */
    public final void addInstance(final ServiceInstance instance) {
        if (instance == null) {
            throw new IllegalArgumentException("Value null as instance parameter is not permitted!");
        }
        if (!this.getInstancesList().contains(instance)) {
            this.instances.add(instance);
        }
    }

    /**
     * Returns a list of service implementations for the passed service type.
     *
     * @param type
     *            Specifies the type of the implementations.
     * @return Returns a list of service implementations.
     */
    public final EList<ServiceImplementation> queryServiceImplementations(final ServiceType type) {
        // check whether type is a valid ServieType
        EList<ServiceType> typeList = queryServiceTypes();

        boolean typeValid = false;
        for (ServiceType t : typeList) {
            if (t.getServiceTypeName().equals(type.getServiceTypeName())) {
                typeValid = true;
            }
        }

        if (typeValid) {
            EList<ServiceImplementation> resultList = new BasicEList<ServiceImplementation>();
            for (ServiceImplementation impl : getImplementations()) {
                if (impl.getType().getServiceTypeName().equals(type.getServiceTypeName())) {
                    resultList.add(impl);
                }
            }
            return resultList;
        } else {
            throw new IllegalArgumentException("The ServiceType '" + type + "' is unknown to the SoftwareLandscape!");
        }

    }

    /**
     * @return Returns a list of all service types known to the landscape.
     */
    public final EList<ServiceType> queryServiceTypes() {
        return getProvidedTypesList();
    }

}