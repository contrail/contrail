/**
 *  SVN FILE: $Id: ServiceTypeTest.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/test/java/org/slasoi/models/scm/extended/test/ServiceTypeTest.java $
 */

package org.slasoi.models.scm.extended.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.extended.ServiceTypeExtended;

/**
 * This class provides test cases for ServiceTypeExtended class.
 * 
 * @author Alexander Wert
 * 
 */
public class ServiceTypeTest {

    /**
     * Private instance of ServiceTypeExtended class. This instance is the test target.
     */
    private static ServiceTypeExtended type;
    /**
     * Private instance of ServiceTypeExtended class. This instance should be equal to the test target.
     */
    private static ServiceTypeExtended equalType;
    /**
     * Private instance of ServiceTypeExtended class. This instance must not be equal to the test target.
     */
    private static ServiceTypeExtended anotherType;

    /**
     * Sets up the test environment for each test case. This method creates the test target, an equal service type
     * object and another service type object.
     */
    @BeforeClass
    public static void setUp() {
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI("src/test/resources/ORC.scm");
        type = (ServiceTypeExtended) landscape.getProvidedTypes(0);
        // create equal but not same serviceType
        equalType = (ServiceTypeExtended) ServiceConstructionModel.getFactory().createServiceType();
        equalType.setInterfaces(type.getInterfaces());
        // create a different service type
        anotherType = (ServiceTypeExtended) landscape.getRequiredTypes(0);
    }

    /**
     * Tests the loading of ServiceType from file.
     */
    @Test
    public final void testServiceType() {
        assertNotNull(type);
        assertNotNull(equalType);
        assertNotNull(anotherType);
    }

    /**
     * Tests toString() function of ServiceTypeExtended class.
     */
    @Test
    public final void testToString() {
        assertTrue(this.toString().length() > 0);
    }

    /**
     * Tests equals() function of ServiceTypeExtended class.
     */
    @Test
    public final void testEquals() {
        assertTrue(type.equals(equalType));
        assertFalse(type.equals(anotherType));
    }

    /**
     * Tests hashCode() function of ServiceTypeExtended class.
     */
    @Test
    public final void testHashCode() {
        assertTrue(type.hashCode() == equalType.hashCode());
        assertFalse(type.hashCode() == anotherType.hashCode());
    }
}
