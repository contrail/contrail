/**
 *  SVN FILE: $Id: ServiceDependencyTest.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/test/java/org/slasoi/models/scm/extended/test/ServiceDependencyTest.java $
 */

package org.slasoi.models.scm.extended.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.models.scm.extended.DependencyExtended;

/**
 * This class provides test cases for ServiceDependencyExtended class.
 * 
 * @author Alexander Wert
 * 
 */
public class ServiceDependencyTest {

    /**
     * Private instance of DependencyExtended class. This instance is the test target.
     */
    private DependencyExtended dependency;
    /**
     * Private instance of DependencyExtended class. This instance should be equal to the test target.
     */
    private DependencyExtended equalDependency;
    /**
     * Private instance of DependencyExtended class. This instance must not be equal to the test target.
     */
    private DependencyExtended anotherDependency;
    /**
     * Private instance of ServiceType class.
     */
    private ServiceType seviceType;

    /**
     * Sets up the test environment for each test case. This method creates the test target, an equal dependency object
     * and another dependency object.
     */
    @Before
    public final void setUp() {
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI("src/test/resources/ORC.scm");
        this.dependency = (DependencyExtended) landscape.getImplementations(0).getDependencies().get(0);
        this.equalDependency = (DependencyExtended) landscape.getImplementations(0).getDependencies().get(0);
        this.anotherDependency = (DependencyExtended) landscape.getImplementations(1).getDependencies().get(0);
        this.seviceType = landscape.getRequiredTypes(0);
    }

    /**
     * Tests the loading of ServiceDependency from file.
     */
    @Test
    public final void testServiceDependency() {
        assertNotNull(this.dependency);
    }

    /**
     * Tests getType() function of ServiceDependencyExtended class.
     */
    @Test
    public final void testGetType() {
        assertEquals(this.dependency.getTargetType(), this.seviceType);
    }

    /**
     * Tests equals() function of ServiceDependencyExtended class.
     */
    @Test
    public final void testEquals() {
        assertTrue(this.dependency.equals(equalDependency));
        assertFalse(this.dependency.equals(anotherDependency));
    }

    /**
     * Tests hashCode() function of ServiceDependencyExtended class.
     */
    @Test
    public final void testHashCode() {
        assertTrue(this.dependency.hashCode() == equalDependency.hashCode());
        assertFalse(this.dependency.hashCode() == anotherDependency.hashCode());
    }
}
