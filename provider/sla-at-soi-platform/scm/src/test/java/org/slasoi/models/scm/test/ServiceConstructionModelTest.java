/**
 *  SVN FILE: $Id: ServiceConstructionModelTest.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/test/java/org/slasoi/models/scm/test/ServiceConstructionModelTest.java $
 */

package org.slasoi.models.scm.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.extended.LandscapeExtended;
import org.slasoi.models.scm.extended.ServiceConstructionModelFactoryExtended;

/**
 * This class provides test cases for ServiceConstructionModel facade class.
 * 
 * @author Alexander Wert
 * 
 */
public class ServiceConstructionModelTest {

    /**
     * This method tests both, the loadFromXMI() function of ServiceConstructionModel class and usage of
     * ServiceConstructionModelFactoryExtended.
     * 
     * @throws IOException
     *             Throws an IOException if loading the XMI-file for the landscape instance does not succeed.
     */
    @Test
    public final void testModelFactoryDecorator() throws IOException {
        // load model
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI("src/test/resources/ORC.scm");
        Assert.assertTrue(landscape instanceof LandscapeExtended);
    }

    /**
     * This method tests the getFactory() function of the ServiceConstructionModel class.
     */
    @Test
    public final void testGetFactory() {
        // load model
        ServiceConstructionModelFactory factory = ServiceConstructionModel.getFactory();
        Assert.assertTrue(factory instanceof ServiceConstructionModelFactoryExtended);

    }

    /**
     * This method tests the SaveToXMI() method of the ServiceConstructionModel class.
     * 
     * @throws IOException
     *             Throws an IOException if loading the XMI-file for the landscape instance does not succeed.
     */
    @Test
    public final void testSaveToXMI() throws IOException {
        // load model
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI("src/test/resources/ORC.scm");
        ServiceConstructionModel.saveToXMI(landscape, "src/test/resources/ORC.scm");

    }

}
