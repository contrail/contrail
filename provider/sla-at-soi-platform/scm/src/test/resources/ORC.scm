<?xml version="1.0" encoding="ASCII"?>
<scm:Landscape xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:scm="http://scm.slasoi.org/1.0">
  <providedTypes ID="8da354b9-1212-4f03-8425-c04cc3e1ea97" ServiceTypeName="ORC_ServiceType" Description="ORC_Services">
    <Interfaces>PaymentService</Interfaces>
    <Interfaces>ORCInventoryService</Interfaces>
  </providedTypes>
  <implementations type="8da354b9-1212-4f03-8425-c04cc3e1ea97" ServiceImplementationName="ORC_AllInOne" Description="ORC_AllInOne" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" ID="0c12cf09-d9f5-4359-b977-b4e0bd33a36d" Name="ORC_AllInOneServer" Desciption="ORC_AllInOneServer" HyperVisor="undefined" Version="1" HypervisorVersion="undefined" ApplianceType="X86" ImageFormat="to be defined by Intel">
      <dependencies Name="AllInOne Infrastructure" TargetURI="" targetType="58aefd1a-8b54-4f0e-bade-bea0ea8f628e" ID="1"/>
      <serviceFeatures ID="serviceFeature_1" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="5" bootOrder="0c12cf09-d9f5-4359-b977-b4e0bd33a36d"/>
  </implementations>
  <implementations type="8da354b9-1212-4f03-8425-c04cc3e1ea97" ServiceImplementationName="ORC_Distributed" Description="ORC_Distributed" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" ID="21d8b4c3-997c-4165-a0f1-deada826c1f9" Name="ORC_DB" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="DB Infrastructure" Description="" TargetURI="" targetType="58aefd1a-8b54-4f0e-bade-bea0ea8f628e" ID="2"/>
    </artefacts>
    <artefacts xsi:type="scm:VirtualAppliance" ID="e9f11c20-4c20-4362-b2af-ad7de7077228" Name="ORC_Services" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="Application Server Infrastructure" Description="" TargetURI="" targetType="58aefd1a-8b54-4f0e-bade-bea0ea8f628e" ID="3"/>
      <serviceFeatures ID="serviceFeature_2" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="20" bootOrder="21d8b4c3-997c-4165-a0f1-deada826c1f9 e9f11c20-4c20-4362-b2af-ad7de7077228"/>
  </implementations>
  <requiredTypes ID="58aefd1a-8b54-4f0e-bade-bea0ea8f628e" ServiceTypeName="Infrastructure" Description="Host for Virtual Machines">
    <Interfaces>CPU</Interfaces>
    <Interfaces>HDD</Interfaces>
    <Interfaces>MEM</Interfaces>
    <Interfaces>NET</Interfaces>
  </requiredTypes>
</scm:Landscape>
