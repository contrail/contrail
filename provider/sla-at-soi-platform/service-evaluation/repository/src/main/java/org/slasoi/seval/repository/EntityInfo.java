package org.slasoi.seval.repository;

/**
 * This class stores information about an Entity.
 * 
 * An Entity is a QoS meta-model element with a name and an identifier.
 * 
 * @author brosch
 * 
 */
public class EntityInfo {

    /**
     * The entity's id.
     */
    private String id = null;

    /**
     * The entity's name.
     */
    private String name = null;

    /**
     * Retrieves the entity's id.
     * 
     * @return the entity's id
     */
    public String getId() {
        return id;
    }

    /**
     * Retrieves the entity's name.
     * 
     * @return the entity's name
     */
    public String getName() {
        return name;
    }

    /**
     * The constructor.
     * 
     * @param id
     *            the entity's id
     * @param name
     *            the entity's name
     */
    public EntityInfo(final String id, final String name) {
        super();
        this.id = id;
        this.name = name;
    }
}
