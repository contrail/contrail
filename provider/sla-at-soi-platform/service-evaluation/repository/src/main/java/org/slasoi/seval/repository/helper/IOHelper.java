package org.slasoi.seval.repository.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

/**
 * This class provides auxiliary functionality for working with the file system.
 * 
 * @author brosch
 * 
 */
public final class IOHelper {

    /**
     * Log4j functionality.
     */
    private static final Logger LOGGER = Logger.getLogger(IOHelper.class.getName());

    /**
     * A singleton pattern ensures the existence of only one IOHelper.
     */
    private static IOHelper singletonInstance = new IOHelper();

    /**
     * Retrieves the singleton instance.
     * 
     * @return the singleton instance
     */
    public static IOHelper getHelper() {
        return singletonInstance;
    }

    /**
     * The constructor is made private according to the singleton pattern.
     */
    private IOHelper() {
    }

    /**
     * Copies all files from a source directory to a target directory.
     * 
     * @param sourceDir
     *            the source directory
     * @param targetDir
     *            the target directory
     * @throws IOException Identifies that a model could not be copied.
     */
    public void copyFiles(final File sourceDir, final File targetDir) throws IOException {
        if (sourceDir.exists()) {
            File[] files = sourceDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].isDirectory()) {
                    copyFile(files[i], new File(targetDir, files[i].getName()));
                }
            }
        }
    }

    /**
     * Copies a file from a source location to a target location.
     * 
     * @param sourcePath
     *            the source location
     * @param targetPath
     *            the target location
     * @throws IOException Identifies that a model could not be copied.
     */
    public void copyFile(final File sourcePath, final File targetPath) throws IOException {

        // Create new directory and file, if not already existent:
        targetPath.getParentFile().mkdir();
        targetPath.createNewFile();

        // Copy contents of original file into new file:
        FileInputStream in = new FileInputStream(sourcePath);
        FileOutputStream out = new FileOutputStream(targetPath);
        copyStream(in, out);

        // It's done:
        in.close();
        out.close();

        LOGGER.debug("Copied " + sourcePath + " to " + targetPath);
    }

    /**
     * Copies stream contents from a source stream to a target stream.
     * 
     * @param sourceStream
     *            the source stream
     * @param targetStream
     *            the target stream
     * @throws IOException Identifies that a data stream could not be written.
     */
    public void copyStream(final InputStream sourceStream, final OutputStream targetStream) throws IOException {
        int c;
        while ((c = sourceStream.read()) != -1) {
            targetStream.write(c);
        }
    }

    /**
     * Deletes a directory and all its contents.
     * 
     * @param dir
     *            the directory to delete
     * @return true if directory could successfully be deleted
     */
    public boolean deleteDir(final File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * Fetches the content of a file and returns it as a string.
     * 
     * @param path
     *            the full file path, including the file name
     * @return the file's content as string
     * @throws IOException Identifies that the requested file could not be read.
     */
    public String getFileContents(final String path) throws IOException {
        File f = new File(path);
        assert (f.exists());
        FileReader reader = null;
        try {
            reader = new FileReader(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int buf;
        buf = reader.read();
        StringBuffer buff = new StringBuffer();
        while (buf > -1) {
            buff.append((char) buf);
            buf = reader.read();
        }
        reader.close();
        return buff.toString();
    }
}
