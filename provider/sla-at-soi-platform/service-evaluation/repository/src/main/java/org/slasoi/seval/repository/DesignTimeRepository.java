package org.slasoi.seval.repository;

import java.io.File;

import org.apache.log4j.Logger;
import org.slasoi.seval.repository.container.ResourceTypeModelContainer;

/**
 * Represents the design-time repository.
 * 
 * The repository is realized as a map from model names to software models, assuming that the names are unique. It
 * implements the singleton pattern.
 * 
 * <strong>Note:</strong> This repository has been refactored to work in standalone mode right after initialization. By
 * default, it contains the test orc software model contained in the bundle. It is accessible with the model id
 * "ORC_AllInOne". It is no longer required to externally trigger the resource helper and to fill the repository.
 * 
 * @author Benjamin Klatt
 * @author brosch
 * @author kuester
 */
public final class DesignTimeRepository implements Repository {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** Log4j functionality. */
    private static final Logger LOGGER = Logger.getLogger(DesignTimeRepository.class.getName());

    /** As a serializable class, the repository defines a serial version UID. */
    private static final long serialVersionUID = -9017991856080543301L;

    /** A singleton pattern ensures the existence of only one design-time repository within the SLA@SOI framework. */
    private static DesignTimeRepository singletonInstance = new DesignTimeRepository();

    /** The resource type model container to access the common pcm model container. */
    private ResourceTypeModelContainer resourceTypeModelContainer = null;

    /** The root path for the prediction model sub directories. */
    private String repositoryRootPath = null;

    // ////////////////////////////////
    // CONSTRUCTORS
    // ////////////////////////////////

    /**
     * Retrieves the singleton repository instance.
     * 
     * @return the singleton repository instance
     */
    public static DesignTimeRepository getRepo() {
        return singletonInstance;
    }

    /**
     * The constructor is made private according to the singleton pattern.
     * 
     * @throws
     */
    private DesignTimeRepository() {

        // init the repository root path
        repositoryRootPath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator
                        + "quality_model_repository";

        // initially load the models from disc to check their validity
        File repositoryDir = new File(repositoryRootPath);
        if (!repositoryDir.exists() || !repositoryDir.isDirectory()) {
            LOGGER.error("Invalid prediction model repository path: " + repositoryDir.getName());
        }

        // load the common pcm model
        String commonPCMModelPath =
            System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator
                    + "CommonPCMModels";
        File pcmModelDir = new File(commonPCMModelPath);
        for (File pcmFile : pcmModelDir.listFiles()) {
            if (pcmFile.getName().equals("Palladio.resourcetype")) {
                this.resourceTypeModelContainer = new ResourceTypeModelContainer(pcmFile);
            }
        }
    }

    // ////////////////////////////////
    // BUSINESS METHODS
    // ////////////////////////////////

    /**
     * Get a SoftwareModel from the repository. This overrides the original HashMap implementation of this method to
     * ensure an instance of SoftwareModel is returned.
     * 
     * @param key
     *            The key to search the repository for.
     * @return The associated software model or null.
     */
    public SoftwareModel get(final Object key) {

        // init the result
        SoftwareModel model = null;

        try {
            // build the path of the specific prediction model
            String modelDirPath = repositoryRootPath + File.separator + key.toString();
            File modelDirectory = new File(modelDirPath);

            // load the model if the path is valid
            if (modelDirectory.exists() && modelDirectory.isDirectory()) {
                String modelID = modelDirectory.getName();
                model = new SoftwareModel(modelID, modelDirectory.listFiles());
            }
        } catch (Exception e) {
            LOGGER.warn("Prediction model could not be loaded for model id: " + key);
        }

        // return what has been found
        return model;
    }

    /**
     * {@inheritDoc}
     */
    public SoftwareModel put(final String key, final SoftwareModel value) {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    public SoftwareModel query(final String systemId) {
        return this.get(systemId);
    }

    // ////////////////////////////////
    // GETTER / SETTER
    // ////////////////////////////////

    /**
     * Set the resource type model container.
     * 
     * @param resourceTypeModelContainer
     *            the resourceTypeModelContainer to set
     */
    public void setResourceTypeModelContainer(final ResourceTypeModelContainer resourceTypeModelContainer) {
        this.resourceTypeModelContainer = resourceTypeModelContainer;
    }

    /**
     * Get the resource type model container.
     * 
     * @return the resourceTypeModelContainer
     */
    public ResourceTypeModelContainer getResourceTypeModelContainer() {
        return resourceTypeModelContainer;
    }

}
