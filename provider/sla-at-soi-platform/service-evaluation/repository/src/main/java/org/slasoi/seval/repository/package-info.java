/**
* Repository sub component to load and store prediction models.
*
* @author Benjamin Klatt
* @author Franz Brosch
*/
package org.slasoi.seval.repository;
