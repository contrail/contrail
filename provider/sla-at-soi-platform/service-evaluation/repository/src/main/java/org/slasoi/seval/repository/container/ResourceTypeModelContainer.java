package org.slasoi.seval.repository.container;

import java.io.File;

import org.eclipse.emf.ecore.EPackage;

import de.uka.ipd.sdq.pcm.resourcetype.CommunicationLinkResourceType;
import de.uka.ipd.sdq.pcm.resourcetype.ProcessingResourceType;
import de.uka.ipd.sdq.pcm.resourcetype.ResourceRepository;
import de.uka.ipd.sdq.pcm.resourcetype.ResourceType;
import de.uka.ipd.sdq.pcm.resourcetype.ResourcetypePackage;

/**
 * This class represents a model container for a resource type model.
 * 
 * @author brosch
 * 
 */
public class ResourceTypeModelContainer extends EMFModelContainer<ResourceRepository> {

    /**
     * Standard constructor.
     */
    public ResourceTypeModelContainer() {
        super();
    }

    /**
     * Constructor requiring to set the resource type model in the container.
     * @param resourceTypeModel The resource type model to set.
     */
    public ResourceTypeModelContainer(final ResourceRepository resourceTypeModel) {
        super(resourceTypeModel);
    }

    /**
     * Constructor requiring the file with the persisted resource type model.
     * @param resourceTypeModelFile The file to read the resource type model from.
     */
    public ResourceTypeModelContainer(final File resourceTypeModelFile) {
        super(resourceTypeModelFile);
    }

    @Override
    protected EPackage getEInstance() {
        return ResourcetypePackage.eINSTANCE;
    }

    @Override
    protected String getENsURI() {
        return ResourcetypePackage.eNS_URI;
    }

    @Override
    protected EMFModelContainer<ResourceRepository> newInstance(final File targetFile) {
        return new ResourceTypeModelContainer(targetFile);
    }

    // --------------------------- Getter --------------------------//

    /**
     * Returns the {@link CommunicationLinkResourceType} used to specify LAN.
     * 
     * @return The LAN element
     */
    public CommunicationLinkResourceType getLANElement() {
        // Go through the list of ResourceTypes and return the one with the specified id.
        for (ResourceType type : this.getModel().getAvailableResourceTypes_ResourceRepository()) {
            if (type.getId().equals("_o3sScH2AEdyH8uerKnHYug")) {
                return (CommunicationLinkResourceType) type;
            }
        }

        // This point should not be reached as the resourceType repository is known and should contain the LAN-element
        assert false : "The element LAN was not found in the resourceType repository";
        return null;
    }

    /**
     * Returns the {@link ProcessingResourceType} used to specify CPU.
     * 
     * @return The CPU element
     */
    public ProcessingResourceType getCPUElement() {
        // Go through the list of ResourceTypes and return the one with the specified id.
        for (ResourceType type : this.getModel().getAvailableResourceTypes_ResourceRepository()) {
            if (type.getId().equals("_oro4gG3fEdy4YaaT-RYrLQ")) {
                return (ProcessingResourceType) type;
            }
        }

        // This point should not be reached as the resourceType repository is known and should contain the LAN-element
        assert false : "The element CPU was not found in the resourceType repository";
        return null;
    }

    /**
     * Returns the {@link ProcessingResourceType} used to specify HDD.
     * 
     * @return The HDD element
     */
    public ProcessingResourceType getHDDElement() {
        // Go through the list of ResourceTypes and return the one with the specified id.
        for (ResourceType type : this.getModel().getAvailableResourceTypes_ResourceRepository()) {
            if (type.getId().equals("_BIjHoQ3KEdyouMqirZIhzQ")) {
                return (ProcessingResourceType) type;
            }
        }

        // This point should not be reached as the resourceType repository is known and should contain the LAN-element
        assert false : "The element HDD was not found in the resourceType repository";
        return null;
    }

    /**
     * Returns the {@link ProcessingResourceType} used to specify DELAY.
     * 
     * @return The DELAY element
     */
    public ProcessingResourceType getDELAYElement() {
        // Go through the list of ResourceTypes and return the one with the specified id.
        for (ResourceType type : this.getModel().getAvailableResourceTypes_ResourceRepository()) {
            if (type.getId().equals("_nvHX4KkREdyEA_b89s7q9w")) {
                return (ProcessingResourceType) type;
            }
        }

        // This point should not be reached as the resourceType repository is known and should contain the LAN-element
        assert false : "The element DELAY was not found in the resourceType repository";
        return null;
    }
}
