package org.slasoi.seval.repository.container;

import java.io.File;

import org.eclipse.emf.ecore.EPackage;

import de.uka.ipd.sdq.pcm.repository.Repository;
import de.uka.ipd.sdq.pcm.repository.RepositoryPackage;

/**
 * This class represents a model container for a component model.
 * 
 * @author brosch
 * 
 */
public class ComponentModelContainer extends EMFModelContainer<Repository> {

    /**
     * Standard constructor.
     */
    public ComponentModelContainer() {
        super();
    }

    /**
     * Constructor requiring to set the repository model in the container.
     * @param repositoryModel The repository model to set.
     */
    public ComponentModelContainer(final Repository repositoryModel) {
        super(repositoryModel);
    }

    /**
     * Constructor requiring the file with the persisted repository model.
     * @param repositoryModelFile The file to read the repository model from.
     */
    public ComponentModelContainer(final File repositoryModelFile) {
        super(repositoryModelFile);
    }

    @Override
    protected EPackage getEInstance() {
        return RepositoryPackage.eINSTANCE;
    }

    @Override
    protected String getENsURI() {
        return RepositoryPackage.eNS_URI;
    }

    @Override
    protected EMFModelContainer<Repository> newInstance(final File targetFile) {
        return new ComponentModelContainer(targetFile);
    }
}
