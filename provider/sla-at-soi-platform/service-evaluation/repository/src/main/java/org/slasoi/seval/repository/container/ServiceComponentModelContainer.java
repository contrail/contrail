package org.slasoi.seval.repository.container;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.uka.ipd.sdq.pcm.repository.RepositoryComponent;

/**
 * Represents a model of the service components and their composition within a service-oriented architecture.
 * 
 * @author brosch
 * 
 */
public class ServiceComponentModelContainer extends AbstractModelContainer {

    /**
     * The list of component models.
     */
    private ArrayList<ComponentModelContainer> componentModelsContainers = new ArrayList<ComponentModelContainer>();

    /**
     * The architecture model.
     */
    private ArchitectureModelContainer architectureModelContainer = null;

    /**
     * Retrieves the list of component models.
     * 
     * @return the list of component models
     */
    public ArrayList<ComponentModelContainer> getComponentModelsContainers() {
        return componentModelsContainers;
    }

    /**
     * Retrieves the architecture model.
     * 
     * @return the architecture model
     */
    public ArchitectureModelContainer getArchitectureModelContainer() {
        return architectureModelContainer;
    }

    /**
     * Sets the architecture model.
     * 
     * @param architectureModelContainer
     *            the architecture model to set
     */
    public void setArchitectureModelContainer(final ArchitectureModelContainer architectureModelContainer) {
        this.architectureModelContainer = architectureModelContainer;
    }

    /**
     * Loads the service component model contents into memory.
     */
    public void load() {
        for (ComponentModelContainer cmc : componentModelsContainers) {
            cmc.load();
        }
        architectureModelContainer.load();
    }

    /**
     * Stores the service component model to the file system.
     */
    public void store() {
        for (ComponentModelContainer cmc : componentModelsContainers) {
            cmc.store();
        }
        architectureModelContainer.store();
    }

    /**
     * Saves the service component model to the given target prediction scenario directory.
     * 
     * @param scenarioPath
     *            the target prediction scenario directory
     * @throws IOException Identifies that the file could not be written.
     */
    public void storeTo(final File scenarioPath) throws IOException {

        // Go through the list of component model containers:
        for (ComponentModelContainer cmc : componentModelsContainers) {
            // Save the component model container to the target location:
            cmc.storeTo(new File(scenarioPath, cmc.getModelFileName()));
        }

        // Also copy the architecture model container:
        architectureModelContainer.storeTo(new File(scenarioPath, this.architectureModelContainer
                .getModelFileName()));
    }

    /**
     * Copies the service component model to the given target prediction scenario directory.
     * 
     * @param scenarioPath
     *            the target prediction scenario directory
     * @throws IOException Identifies that the file could not be copied.
     */
    public void copyTo(final File scenarioPath) throws IOException {

        // Go through the list of component model containers:
        for (ComponentModelContainer cmc : componentModelsContainers) {
            // Copy the component model container to the target location:
            cmc.copyTo(new File(scenarioPath, cmc.getModelFileName()));
        }

        // Also copy the architecture model container:
        architectureModelContainer
                .copyTo(new File(scenarioPath, this.architectureModelContainer.getModelFileName()));
    }

    /**
     * Retrieves the name of a service component by id.
     * 
     * @param id
     *            the id
     * @return the name of the service component
     */
    public String getServiceComponentNameById(final String id) {

        // Go through all component model containers:
        for (ComponentModelContainer container : getComponentModelsContainers()) {

            // Go through all components of this container:
            for (RepositoryComponent component : container.getModel().getComponents__Repository()) {

                // Check the id of this component:
                if (component.getId().equals(id)) {
                    return component.getEntityName();
                }
            }
        }

        // Nothing found:
        assert false : "Component with id \"" + id + "\" not found!";
        return null;
    }
}
