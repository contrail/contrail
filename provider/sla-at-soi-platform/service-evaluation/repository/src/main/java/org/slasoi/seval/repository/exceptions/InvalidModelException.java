package org.slasoi.seval.repository.exceptions;

/**
 * An exception about an invalid model.
 * @author Benjamin Klatt
 *
 */
public class InvalidModelException extends IllegalArgumentException {

    /**
     * Serial version UID for the seralizable class.
     */
    private static final long serialVersionUID = 5144908111909927145L;

    /**
     * Creates the exception with the given message.
     * 
     * @param message
     *            the message
     */
    public InvalidModelException(final String message) {
        super(message);
    }
}
