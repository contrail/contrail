package org.slasoi.seval.repository;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slasoi.seval.repository.container.AllocationModelContainer;
import org.slasoi.seval.repository.container.ArchitectureModelContainer;
import org.slasoi.seval.repository.container.ComponentModelContainer;
import org.slasoi.seval.repository.container.InfrastructureModelContainer;
import org.slasoi.seval.repository.container.ResourceTypeModelContainer;
import org.slasoi.seval.repository.container.ServiceComponentModelContainer;
import org.slasoi.seval.repository.container.UsageModelContainer;
import org.slasoi.seval.repository.exceptions.InvalidModelException;

/**
 * This class represents a software model of a service-oriented architecture. It hides the actual storage of the model
 * from the user. A couple of {@link java.io.File}s can be passed, which are deserialized (and eventually put into the
 * model repository).
 * 
 * @author kuester, brosch
 */
public class SoftwareModel {

    /**
     * Repository model file extension.
     */
    private static final String EXTREPO = "repository";

    /**
     * System model file extension.
     */
    private static final String EXTSYSTEM = "system";

    /**
     * Resource environment file extension.
     */
    private static final String EXTRESOURCE = "resourceenvironment";

    /**
     * Resource type file extension.
     */
    private static final String EXTRESOURCETYPE = "resourcetype";

    /**
     * Allocation model file extension.
     */
    private static final String EXTALLOC = "allocation";

    /**
     * Usage model file extension.
     */
    private static final String EXTUSAGE = "usagemodel";

    /**
     * Holds the repository and system models.
     */
    private ServiceComponentModelContainer serviceComponentModelContainer;

    /**
     * Holds the resource environment model.
     */
    private InfrastructureModelContainer infrastructureModelContainer;

    /**
     * Holds the resource type model.
     */
    private ResourceTypeModelContainer resourceTypeModelContainer;

    /**
     * Holds the allocation model.
     */
    private AllocationModelContainer allocationModelContainer;

    /**
     * Holds the usage model.
     */
    private UsageModelContainer usageModelContainer;

    /**
     * The ID of this software model.
     */
    private String modelId;

    /**
     * Creates a software model from a list of given files in the file system.
     * 
     * @param modelId
     *            the model id
     * @param files
     *            the model files
     * @throws IOException
     *             in case a model file does not exist
     */
    public SoftwareModel(final String modelId, final File... files) throws IOException {
        this.modelId = modelId;
        for (File f : files) {
            if (!f.exists()) {
                throw new IOException("File " + f + " does not exist.");
            }
            this.addModel(f);
        }
        validate();
    }

    /**
     * Adds a file representing a model part to the container.
     * 
     * Currently, the decision about the type of model is based on the file name extension.
     * 
     * @param modelFile
     *            file where the model (XMI) can be found.
     * @throws IOException
     *             If the model files could not be loaded successfully.
     */
    protected void addModel(final File modelFile) throws IOException {

        // Retrieve the path name of the model file:
        String path = modelFile.getPath();

        // Create the corresponding model container:
        if (path.endsWith(EXTREPO)) {
            ServiceComponentModelContainer container = null;
            if (serviceComponentModelContainer == null) {
                container = new ServiceComponentModelContainer();
            } else {
                container = serviceComponentModelContainer;
            }
            container.getComponentModelsContainers().add(new ComponentModelContainer(modelFile));
            serviceComponentModelContainer = container;

        } else if (path.endsWith(EXTSYSTEM)) {

            ServiceComponentModelContainer container = null;
            if (serviceComponentModelContainer == null) {
                container = new ServiceComponentModelContainer();
            } else {
                container = serviceComponentModelContainer;
            }

            if (container.getArchitectureModelContainer() != null) {
                throw new InvalidModelException("Multiple architecture models specified");
            }
            container.setArchitectureModelContainer(new ArchitectureModelContainer(modelFile));
            serviceComponentModelContainer = container;
        } else if (path.endsWith(EXTRESOURCE)) {
            if (infrastructureModelContainer != null) {
                throw new InvalidModelException("Multiple infrastructure models defined");
            }
            infrastructureModelContainer = new InfrastructureModelContainer(modelFile);
        } else if (path.endsWith(EXTALLOC)) {
            if (allocationModelContainer != null) {
                throw new InvalidModelException("Multiple allocation models defined");
            }
            allocationModelContainer = new AllocationModelContainer(modelFile);
        } else if (path.endsWith(EXTUSAGE)) {
            if (usageModelContainer != null) {
                throw new InvalidModelException("Multiple usage models defined");
            }
            usageModelContainer = new UsageModelContainer(modelFile);
        } else if (path.endsWith(EXTRESOURCETYPE)) {
            if (resourceTypeModelContainer != null) {
                throw new InvalidModelException("Multiple repository type models defined");
            }
            resourceTypeModelContainer = new ResourceTypeModelContainer(modelFile);
        }
    }

    /**
     * Validates the software model.
     * 
     * Currently, validation is only based on the presence of all required model parts.
     */
    private void validate() {
        if (serviceComponentModelContainer == null) {
            throw new InvalidModelException("No service component model defined");
        }
        if (serviceComponentModelContainer.getComponentModelsContainers().size() == 0) {
            throw new InvalidModelException("No component models defined");
        }
        if (serviceComponentModelContainer.getArchitectureModelContainer() == null) {
            throw new InvalidModelException("No architecture model defined");
        }
        if (infrastructureModelContainer == null) {
            throw new InvalidModelException("No infrastructure model defined");
        }
        if (allocationModelContainer == null) {
            throw new InvalidModelException("No allocation model defined");
        }
        if (usageModelContainer == null) {
            throw new InvalidModelException("No usage model defined");
        }
    }

    /**
     * Retrieves the model ID.
     * 
     * @return the model ID
     */
    public String getModelId() {
        return modelId;
    }

    /**
     * Retrieves the service component model container.
     * 
     * @return the service component model container
     */
    public ServiceComponentModelContainer getServiceComponentModelContainer() {
        return serviceComponentModelContainer;
    }

    /**
     * Retrieves the infrastructure model container.
     * 
     * @return the infrastructure model container
     */
    public InfrastructureModelContainer getInfrastructureModelContainer() {
        return infrastructureModelContainer;
    }

    /**
     * Retrieves the allocation model container.
     * 
     * @return the allocation model container
     */
    public AllocationModelContainer getAllocationModelContainer() {
        return allocationModelContainer;
    }

    /**
     * Retrieves the usage model container.
     * 
     * @return the usage model container
     */
    public UsageModelContainer getUsageModelContainer() {
        return usageModelContainer;
    }

    /**
     * Retrieves the component model containers.
     * 
     * @return the list of component model containers
     */
    public List<ComponentModelContainer> getComponentModelContainers() {
        return serviceComponentModelContainer.getComponentModelsContainers();
    }

    /**
     * Retrieves the architecture model container.
     * 
     * @return the architecture model container
     */
    public ArchitectureModelContainer getArchitectureModelContainer() {
        return serviceComponentModelContainer.getArchitectureModelContainer();
    }
}
