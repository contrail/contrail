package org.slasoi.seval.repository.container;

import java.io.File;
import java.io.IOException;

/**
 * Provides common container functionality for a design-time repository model.
 * 
 * @author brosch
 */
public abstract class AbstractModelContainer {

    /**
     * Initializes an empty model container.
     */
    public AbstractModelContainer() {
    }

    /**
     * Loads model contents into memory.
     * 
     * Must be implemented by sub classes.
     */
    public abstract void load();

    /**
     * Saves model contents to the file system.
     * 
     * Must be implemented by sub classes.
     */
    public abstract void store();

    /**
     * Copies the model to a new location.
     * 
     * A new model container is returned and bound to the new location. Must be implemented by sub classes.
     * 
     * @param targetFile
     *            the target location
     * @throws IOException Exception that the model file could not be stored successfully.
     * 
     */
    public abstract void storeTo(File targetFile) throws IOException;
}
