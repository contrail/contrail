/**
* Container package to wrap different types of prediction sub-models
* in the repository sub component.
*
* @author Benjamin Klatt
* @author Franz Brosch
*/
package org.slasoi.seval.repository.container;
