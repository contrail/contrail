package org.slasoi.seval.repository.container;

import java.io.File;

import org.eclipse.emf.ecore.EPackage;

import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceEnvironment;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceenvironmentPackage;

/**
 * This class represents a model container for an infrastructure model.
 * 
 * @author brosch
 * 
 */
public class InfrastructureModelContainer extends EMFModelContainer<ResourceEnvironment> {

    /**
     * Standard constructor.
     */
    public InfrastructureModelContainer() {
        super();
    }

    /**
     * Constructor requiring the file with the persisted resource environment model.
     * 
     * @param resourceEnvironmentModelFile
     *            The file to read the resource environment model from.
     */
    public InfrastructureModelContainer(final File resourceEnvironmentModelFile) {
        super(resourceEnvironmentModelFile);
    }

    /**
     * Constructor requiring to set the resource environment model in the container.
     * 
     * @param resourceEnvironmentModel
     *            The resource environment model to set.
     */
    public InfrastructureModelContainer(final ResourceEnvironment resourceEnvironmentModel) {
        super(resourceEnvironmentModel);
    }

    @Override
    protected EPackage getEInstance() {
        return ResourceenvironmentPackage.eINSTANCE;
    }

    @Override
    protected String getENsURI() {
        return ResourceenvironmentPackage.eNS_URI;
    }

    @Override
    protected EMFModelContainer<ResourceEnvironment> newInstance(final File targetFile) {
        return new InfrastructureModelContainer(targetFile);
    }

    /**
     * Returns a {@link ResourceContainer} from the model, which is (according to its name) to be assigned on an
     * {@link Appliance} with the given String as its DiskImage.
     * 
     * @param diskImage
     *            Given String standing for an {@link Appliance}s DiskImage
     * @return Corresponding {@link ResourceContainer}
     */
    public ResourceContainer getResourceContainer(final String diskImage) {
        assert diskImage != "" : "The given String is empty - no ResourceContainer will be foud!";

        for (ResourceContainer container : this.getModel().getResourceContainer_ResourceEnvironment()) {
            if (container.getEntityName().equals(diskImage)) {
                return container;
            }
        }

        assert false : "No ResourceContainer was build for the given DiskImage";
        return null;
    }
}
