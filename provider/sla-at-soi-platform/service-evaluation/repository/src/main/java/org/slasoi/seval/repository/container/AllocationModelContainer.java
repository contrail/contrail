package org.slasoi.seval.repository.container;

import java.io.File;

import org.eclipse.emf.ecore.EPackage;

import de.uka.ipd.sdq.pcm.allocation.Allocation;
import de.uka.ipd.sdq.pcm.allocation.AllocationPackage;

/**
 * This class represents a model container for an allocation model.
 * 
 * @author brosch
 * 
 */
public class AllocationModelContainer extends EMFModelContainer<Allocation> {

    /**
     * Standard constructor.
     */
    public AllocationModelContainer() {
        super();
    }

    /**
     * Constructor requiring to set the allocation model in the container.
     * @param allocationModel The allocation model to set.
     */
    public AllocationModelContainer(final Allocation allocationModel) {
        super(allocationModel);
    }

    /**
     * Constructor requiring the file with the persisted allocation model.
     * @param allocationModelFile The file to read the allocation model from.
     */
    public AllocationModelContainer(final File allocationModelFile) {
        super(allocationModelFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EPackage getEInstance() {
        return AllocationPackage.eINSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getENsURI() {
        return AllocationPackage.eNS_URI;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EMFModelContainer<Allocation> newInstance(final File targetFile) {
        return new AllocationModelContainer(targetFile);
    }

}
