package org.slasoi.seval.repository;

import org.slasoi.seval.repository.container.ResourceTypeModelContainer;

/**
 * Repository to store and retrieve software models.
 * 
 * @author Benjamin Klatt
 * 
 */
public interface Repository {

    /**
     * Store a model in the repository.
     * 
     * @param modeId
     *            The string identifier of the model.
     * @param model
     *            The model itself.
     * @return The recently stored SoftwareModel.
     */
    SoftwareModel put(String modeId, SoftwareModel model);

    /**
     * Query the repository for a model identified by its id.
     * 
     * @param modelId
     *            The id of the model.
     * @return the appropriate software model.
     */
    SoftwareModel query(String modelId);

    /**
     * Get the resource type model container.
     * 
     * @return the resourceTypeModelContainer
     */
    ResourceTypeModelContainer getResourceTypeModelContainer();

}
