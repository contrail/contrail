package org.slasoi.seval.repository.container;

import java.io.File;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;
import org.slasoi.seval.repository.EntityInfo;

import de.uka.ipd.sdq.pcm.core.composition.AssemblyConnector;
import de.uka.ipd.sdq.pcm.core.composition.AssemblyContext;
import de.uka.ipd.sdq.pcm.core.composition.ProvidedDelegationConnector;
import de.uka.ipd.sdq.pcm.repository.BasicComponent;
import de.uka.ipd.sdq.pcm.repository.OperationProvidedRole;
import de.uka.ipd.sdq.pcm.repository.ProvidedRole;
import de.uka.ipd.sdq.pcm.repository.RepositoryComponent;
import de.uka.ipd.sdq.pcm.repository.Role;
import de.uka.ipd.sdq.pcm.repository.Signature;
import de.uka.ipd.sdq.pcm.repository.SourceRole;
import de.uka.ipd.sdq.pcm.seff.AbstractAction;
import de.uka.ipd.sdq.pcm.seff.AbstractBranchTransition;
import de.uka.ipd.sdq.pcm.seff.AbstractLoopAction;
import de.uka.ipd.sdq.pcm.seff.BranchAction;
import de.uka.ipd.sdq.pcm.seff.ExternalCallAction;
import de.uka.ipd.sdq.pcm.seff.ForkAction;
import de.uka.ipd.sdq.pcm.seff.ForkedBehaviour;
import de.uka.ipd.sdq.pcm.seff.ServiceEffectSpecification;
import de.uka.ipd.sdq.pcm.seff.impl.ResourceDemandingSEFFImpl;
import de.uka.ipd.sdq.pcm.system.System;
import de.uka.ipd.sdq.pcm.system.SystemPackage;

/**
 * This class represents a model container for an architecture model (PCM System model).
 * 
 * @author brosch
 * 
 */
public class ArchitectureModelContainer extends EMFModelContainer<de.uka.ipd.sdq.pcm.system.System> {

    /**
     * Standard constructor.
     */
    public ArchitectureModelContainer() {
        super();
    }

    /**
     * Constructor requiring to set the architecture model in the container.
     * 
     * @param system
     *            The architecture model (system model) to set.
     */
    public ArchitectureModelContainer(final System system) {
        super(system);
    }

    /**
     * Constructor requiring the file with the persisted architecture model(PCM system model).
     * 
     * @param architectureModelFile
     *            The file to read the architecture model (PCM system model) from.
     */
    public ArchitectureModelContainer(final File architectureModelFile) {
        super(architectureModelFile);
    }

    @Override
    protected EPackage getEInstance() {
        return SystemPackage.eINSTANCE;
    }

    @Override
    protected String getENsURI() {
        return SystemPackage.eNS_URI;
    }

    @Override
    protected EMFModelContainer<System> newInstance(final File targetFile) {
        return new ArchitectureModelContainer(targetFile);
    }

    /**
     * Gets the service component that provides the given service.
     * 
     * The service is determined by a response time sensor name (temporary solution).
     * 
     * @param sensorName
     *            the response time sensor name
     * @return the service component
     */
    public EntityInfo getServiceComponentInfo(final String sensorName) {

        // Get the providing service component:
        RepositoryComponent component = getProvidingServiceComponent(sensorName);
        if (component == null) {
            return null;
        }

        // Return the result:
        return new EntityInfo(component.getId(), component.getEntityName());
    }

    /**
     * Gets the name of the service operation that is referenced by a response time sensor name (temporary solution).
     * 
     * @param sensorName
     *            the response time sensor name
     * @return the service operation name
     */
    public String getServiceOperationName(final String sensorName) {

        // Get the associated ExternalCallAction:
        ExternalCallAction call = getCall(sensorName);
        if (call == null) {
            return null;
        }

        // Return the signature name:
        return call.getCalledService_ExternalService().getEntityName();
    }

    /**
     * Gets the component that provides service according to a response time sensor name (temporary solution).
     * 
     * @param sensorName
     *            the response time sensor name
     * @return the providing service component
     */
    private RepositoryComponent getProvidingServiceComponent(final String sensorName) {

        // Search for the ExternalCallAction that is connected to the sensor:
        ExternalCallAction call = getCall(sensorName);
        if (call == null) {
            return null;
        }

        // Get the required role that is connected to the call:
        Role requiredRole = call.getRole_ExternalService();

        // Search the list of assembly connectors to find the associated providing component:
        for (AssemblyConnector connector : this.getModel().getAssemblyConnectors_ComposedStructure()) {
            if (connector.getRequiredRole_AssemblyConnector().getId().equals(requiredRole.getId())) {
                return connector.getProvidingAssemblyContext_AssemblyConnector()
                        .getEncapsulatedComponent__AssemblyContext();
            }
        }

        // Nothing found:
        return null;
    }

    /**
     * Gets an ExternalCall according to a response time sensor name (temporary solution).
     * 
     * The sensor name contains the IDs of the requiring assembly context and the call. This information is used to
     * determine the call.
     * 
     * @param sensorName
     *            the response time sensor name
     * @return the ExternalCallAction
     */
    private ExternalCallAction getCall(final String sensorName) {

        // Go through the list of (service) components:

        for (AssemblyContext context : this.getModel().getAssemblyContexts__ComposedStructure()) {
            if (sensorName.contains(context.getId())) {
                RepositoryComponent component = context.getEncapsulatedComponent__AssemblyContext();
                if (component instanceof BasicComponent) {
                    ExternalCallAction result = getCall(sensorName, (BasicComponent) component);
                    if (result != null) {
                        return result;
                    }
                }
            }
        }

        // Nothing found?
        return null;
    }

    /**
     * Gets an ExternalCall according to a response time sensor name (temporary solution) and the given (service)
     * component.
     * 
     * The sensor name contains the IDs of the requiring assembly context and the call. This information is used to
     * determine the call.
     * 
     * @param sensorName
     *            the response time sensor name
     * @param component
     *            the component
     * @return the ExternalCallAction
     */
    private ExternalCallAction getCall(final String sensorName, final BasicComponent component) {

        // Search through all ServiceEffectSpecifications of the component:
        for (ServiceEffectSpecification seff : component.getServiceEffectSpecifications__BasicComponent()) {
            if (seff instanceof ResourceDemandingSEFFImpl) {
                ExternalCallAction result =
                        getCall(sensorName, ((ResourceDemandingSEFFImpl) seff).getSteps_Behaviour());
                if (result != null) {
                    return result;
                }
            }
        }

        // Nothing found?
        return null;
    }

    /**
     * Searches the action chain of a ServiceEffectSpecification or a ResourceDemandingBehaviour for an
     * ExternalCallAction with the given ID.
     * 
     * The ID is given as part of the name of the response time sensor (temporary solution).
     * 
     * @param sensorName
     *            the response time sensor name
     * @param steps
     *            the steps of the action chain
     * @return the external call (if such a call exists)
     */
    private ExternalCallAction getCall(final String sensorName, final EList<AbstractAction> steps) {

        // Go through the list of actions in the SEFF:
        for (AbstractAction action : steps) {
            ExternalCallAction result = null;
            if (action instanceof ExternalCallAction) {
                if (sensorName.contains(action.getId())) {
                    return (ExternalCallAction) action;
                }
            } else if (action instanceof AbstractLoopAction) {
                result =
                        getCall(sensorName, ((AbstractLoopAction) action).getBodyBehaviour_Loop()
                                .getSteps_Behaviour());
                if (result != null) {
                    return result;
                }
            } else if (action instanceof BranchAction) {
                for (AbstractBranchTransition branch : ((BranchAction) action).getBranches_Branch()) {
                    result = getCall(sensorName, branch.getBranchBehaviour_BranchTransition().getSteps_Behaviour());
                    if (result != null) {
                        return result;
                    }
                }
            } else if (action instanceof ForkAction) {
                for (ForkedBehaviour behaviour : ((ForkAction) action).getAsynchronousForkedBehaviours_ForkAction()) {
                    result = getCall(sensorName, behaviour.getSteps_Behaviour());
                    if (result != null) {
                        return result;
                    }
                }
                for (ForkedBehaviour behaviour : ((ForkAction) action).getSynchronisingBehaviours_ForkAction()
                        .getSynchronousForkedBehaviours_SynchronisationPoint()) {
                    result = getCall(sensorName, behaviour.getSteps_Behaviour());
                    if (result != null) {
                        return result;
                    }
                }
            }
        }

        // Nothing found:
        return null;
    }

    /**
     * Returns an {@link AssemblyContext} contained in the {@link ArchitectureModelContainer}, if this
     * {@link AssemblyContext} encapsulates a Component with the given String as its entityName.
     * 
     * @param serviceName
     *            Name of the encapsulated Component
     * @return {@link AssemblyContext} encapsulating the specified Component
     */
    public AssemblyContext getAssemblyContext(final String serviceName) {
        for (AssemblyContext context : getModel().getAssemblyContexts__ComposedStructure()) {
            if (context.getEncapsulatedComponent__AssemblyContext().getEntityName().equals(serviceName)) {
                return context;
            }
        }

        // "No AssemblyContext could be found for a component named "
        return null;
    }

    /**
     * Returns the {@link System}s {@link ProvidedRole} that fits to the given serviceName, which represents the name of
     * a component. <br>
     * The Role fits, if it contains an Interface with an entityName that equals the serviceName.
     * 
     * @param serviceName
     *            Name of the {@link Interface}
     * @return fitting {@link ProvidedRole}
     */
    public ProvidedRole getProvidedRole(final String serviceName) {
        for (ProvidedRole role : this.getModel().getProvidedRoles_InterfaceProvidingEntity()) {
            if (checkForDelegationConnector(role, serviceName)) {
                return role;
            }
        }

        // "No ProvidedRole contains an interface named "
        return null;
    }

    /**
     * Checks if there is a {@link DelegationConnector} between the {@link AssemblyContext} of a Component (specified by
     * the given parameter serviceName) and the given {@link ProvidedRole}.
     * 
     * @param providedRole
     *            The outer provided Role of a System
     * @param serviceName
     *            A string specifying a component used in the system
     * @return true if the required connector exists, false if not.
     */
    private boolean checkForDelegationConnector(final ProvidedRole providedRole, final String serviceName) {
        // Check every ProvidedDelegationConnector in the system...
        for (ProvidedDelegationConnector connector : this.getModel()
                .getProvidedDelegationConnectors_ComposedStructure()) {
            // See if it delegates from the given ProvidedRole, if so check if it delegates to an AssemblyConetxt which
            // encapsulates the component fitting to the given string
            if ((connector.getOuterProvidedRole_ProvidedDelegationConnector() == providedRole)
                    && connector.getAssemblyContext_ProvidedDelegationConnector()
                            .getEncapsulatedComponent__AssemblyContext().getEntityName().equals(serviceName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a Signature fitting to the given operationName from the interface referenced in the given ProvidedRole.
     * 
     * @param providedRole
     *            The given ProvidedRole
     * @param operationName
     *            The string used to identify the correct Signature
     * @return The found signature, null if none matches.
     */
    public Signature getSignature(final ProvidedRole providedRole, final String operationName) {
        // Go through all Signatures and return one if its name fits
        if (providedRole instanceof OperationProvidedRole) {
            for (Signature signature : ((OperationProvidedRole) providedRole)
                    .getProvidedInterface__OperationProvidedRole().getSignatures__OperationInterface()) {
                if (signature.getEntityName().equals(operationName)) {
                    return signature;
                }
            }
        } else if (providedRole instanceof SourceRole) {
            for (Signature signature : ((SourceRole) providedRole).getEventGroup__SourceRole()
                    .getEventTypes__EventGroup()) {
                if (signature.getEntityName().equals(operationName)) {
                    return signature;
                }
            }
        }
        return null;
    }
}
