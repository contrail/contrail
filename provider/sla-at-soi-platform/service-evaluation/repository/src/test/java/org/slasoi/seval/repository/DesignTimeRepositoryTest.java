package org.slasoi.seval.repository;

import junit.framework.TestCase;

import org.junit.Test;

import de.uka.ipd.sdq.pcm.usagemodel.UsageModel;

public class DesignTimeRepositoryTest extends TestCase {

    /**
     * Basic test that the DesignTimeRepository provides the estimated models.
     */
    @Test
    public void testGetObject() {
        DesignTimeRepository repository = DesignTimeRepository.getRepo();
        SoftwareModel allInOneModel = repository.get("ORC_AllInOne");
        
        assertNotNull(allInOneModel);
        assertNotNull(allInOneModel.getAllocationModelContainer());
        assertNotNull(allInOneModel.getInfrastructureModelContainer());
        assertNotNull(allInOneModel.getUsageModelContainer());
        assertNotNull(allInOneModel.getArchitectureModelContainer());
        assertNotNull(allInOneModel.getComponentModelContainers());
        assertEquals(1, allInOneModel.getComponentModelContainers().size());
        
        
    }
    
    /**
     * This test ensures, that the repository provides individual model instances 
     * so that a change on a model is atomic and the next request for the same model
     * gets the original one and not the one modified before.
     */
    public void testIndividualModelInstances(){
    	
    	DesignTimeRepository repository = DesignTimeRepository.getRepo();
        
    	String modelId = "ORC_AllInOne";
    	
        // get the model and modify it
        SoftwareModel allInOneModel_modified = repository.get(modelId);
        UsageModel usageModel = allInOneModel_modified.getUsageModelContainer().getModel();
        int originalUsageScenarioCount = usageModel.getUsageScenario_UsageModel().size();
        usageModel.getUsageScenario_UsageModel().clear();

        // ensure that the test model makes sense for this test
        assertTrue("The original usage model does not contain any usage scenarios so it does not fit for this test", 
        				(originalUsageScenarioCount > 0));
        
        
        // get the model a second time and prove the value
        SoftwareModel allInOneModel_test = repository.get(modelId);
        UsageModel testUsageModel = allInOneModel_test.getUsageModelContainer().getModel();
        assertEquals("Wrong number of usage scenarios", originalUsageScenarioCount, testUsageModel.getUsageScenario_UsageModel().size());
    	
    }

}
