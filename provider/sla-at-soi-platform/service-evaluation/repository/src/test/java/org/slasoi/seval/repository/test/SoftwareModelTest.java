/**
 * 
 */
package org.slasoi.seval.repository.test;

import java.io.File;
import java.io.IOException;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.seval.repository.exceptions.InvalidModelException;

/**
 * @author kuester, brosch
 * 
 */
public class SoftwareModelTest extends TestCase {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Tests the creation of an empty software model.
     * 
     * @throws IOException
     *             should not happen in this test case.
     */
    @Test(expected = InvalidModelException.class)
    public void testEmptyModelCreation() throws IOException {
        boolean exceptionThrown = false;
        try {
            new SoftwareModel("ORC");
        } catch (InvalidModelException e) {
            exceptionThrown = true;  
        }
        assertTrue("InvalidModelException should have been thrown",exceptionThrown);
    }

    /**
     * Tests the creation of an empty software model.
     * 
     * @throws IOException
     *             should not happen in this test case.
     */
    @Test(expected = IOException.class)
    public void testModelCreationWithNonExistentFile() throws IOException {
        boolean exceptionThrown = false;
        File f = new File("C://myNonExistingDir//myNonExistingModelFile.repository");
        assert (f.exists() == false);
        try {
            new SoftwareModel("ORC", f);
        } catch (IOException e) {
            exceptionThrown = true;  
        }
        assertTrue("IOException should have been thrown",exceptionThrown);
        
    }
}
