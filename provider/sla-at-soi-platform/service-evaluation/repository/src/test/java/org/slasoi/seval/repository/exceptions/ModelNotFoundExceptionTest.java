package org.slasoi.seval.repository.exceptions;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test case for the exception type.
 * @author Benjamin Klatt
 *
 */
public class ModelNotFoundExceptionTest {

    @Test
    public void ModelNotFoundException() {
        ModelNotFoundException exception = new ModelNotFoundException("modelID");
        assertNotNull(exception);
    }
}
