package org.slasoi.seval.repository.exceptions;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test case for the exception type.
 * @author Benjamin Klatt
 *
 */
public class ContainerNotFoundExceptionTest {

    @Test
    public void testContainerNotFoundException() {
        ContainerNotFoundException exception = new ContainerNotFoundException("testMessage");
        assertNotNull(exception);
    }
}
