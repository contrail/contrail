package org.slasoi.seval.prediction.service.settings;

import java.util.List;

/**
 * Settings object for an evaluation run. This data object encapsulates general settings for an evaluation run.
 * 
 * @author Benjamin Klatt
 * 
 */
public interface EvaluationSettings {

    /**
     * Set the flag if the evaluation should consider responsibility effects or not.
     * 
     * @param analyzeResponsibility
     *            true or false to consider responsibility or not.
     */
    void setAnalyzeResponsibility(boolean analyzeResponsibility);

    /**
     * Get the flag if the evaluation should consider responsibility effects or not.
     * 
     * @return true or false to consider responsibility or not.
     */
    boolean isAnalyzeResponsibility();

    /**
     * Retrieves the maximum measurements count constraint.
     * 
     * @return the maximum measurements count constraint
     */
    long getMaxMeasurementsCount();

    /**
     * Sets the maximum measurements count constraint.
     * 
     * @param maxMeasurementsCount
     *            the maximum measurements count constraint
     */
    void setMaxMeasurementsCount(long maxMeasurementsCount);

    /**
     * Retrieves the maximum simulation time constraint.
     * 
     * @return the maximum simulation time constraint
     */
    long getMaxSimTime();

    /**
     * Sets the maximum simulation time constraint.
     * 
     * @param maxSimTime
     *            the maximum simulation time constraint
     */
    void setMaxSimTime(long maxSimTime);

    /**
     * Retrieves the value of the verboseLogging option.
     * 
     * @return the value of verboseLogging
     */
    boolean isVerboseLogging();

    /**
     * Sets the value of the verboseLogging option.
     * 
     * @param verboseLogging
     *            the value of verboseLogging
     */
    void setVerboseLogging(boolean verboseLogging);

    /**
     * Get the list of sensor names to be returned by the prediction.
     * 
     * Getting the list of sensors and adding new ones to the list [getSensorNames().add("sensorxy")] enables the
     * prediction to return only results for the sensors specified. If no sensors have been defined, predictions results
     * for the external system calls are returned, which are the results of the service calls in the usage scenario.
     * 
     * For specified sensors, the complete sensor name returned by the prediction is also returned to identify the
     * result (serviceId). If the default usage scenario calls are returned, the serviceId represents the name of the
     * service call which is optimized for human readability.
     * 
     * @return The list of sensor names to be returned.
     */
    List<String> getSensorNames();

}
