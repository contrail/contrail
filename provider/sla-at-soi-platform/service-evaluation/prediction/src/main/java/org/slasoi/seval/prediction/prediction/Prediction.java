package org.slasoi.seval.prediction.prediction;

import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.repository.SoftwareModel;

/**
 * Interface of a prediction component to analyze a software model.
 * 
 * @author Benjamin Klatt
 * 
 */
public interface Prediction {

    /**
     * Main design-time prediction method that is invoked by negotiation / translation component.
     * 
     * @param model
     *            representing the model to be simulated
     * @param evaluationMode
     *            the {@link EvaluationMode} (real or mock-up)
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @param evaluationServerEndpoint
     *            the web service endpoint of the evaluation server
     * @return The result of the prediction.
     */
    IEvaluationResult predict(SoftwareModel model, EvaluationMode evaluationMode, EvaluationSettings settings,
            String evaluationServerEndpoint);
}
