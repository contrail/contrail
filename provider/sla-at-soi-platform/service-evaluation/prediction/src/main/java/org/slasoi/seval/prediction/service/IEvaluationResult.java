package org.slasoi.seval.prediction.service;

import java.util.List;

import org.slasoi.models.scm.ServiceBuilder;

/**
 * Represents the results of one evaluated service realization.
 * 
 * @author kuester, brosch
 */
public interface IEvaluationResult {

    /**
     * Sets single results that constitute the overall evaluation result.
     * 
     * @param singleResult
     *            the single results to set
     * @deprecated use getResults().add(ISingleResult result) instead
     */
    void addResult(ISingleResult singleResult);

    /**
     * Returns the service builder that describes the service realization.
     * 
     * @return the service builder
     */
    ServiceBuilder getBuilder();

    /**
     * Sets the service builder that describes the service realization.
     * 
     * @param builder
     *            The service builder to set.
     */
    void setBuilder(ServiceBuilder builder);

    /**
     * Retrieves the set of single results that constitute the overall evaluation result.
     * 
     * @return the set of single results
     * @deprecated Use getResponseTimeResults() instead.
     */
    List<ISingleResult> getResults();

    /**
     * Retrieves the set of single results that constitute the overall response time evaluation result.
     * 
     * @return the set of single response time results
     */
    List<ISingleResult> getResponseTimeResults();

    /**
     * Retrieves the set of single results that constitute the overall reliability evaluation result.
     * 
     * @return the set of single reliability results
     */
    List<ISingleResult> getReliabilityResults();

    /**
     * Sorts the list of result entries.
     */
    void sort();
}
