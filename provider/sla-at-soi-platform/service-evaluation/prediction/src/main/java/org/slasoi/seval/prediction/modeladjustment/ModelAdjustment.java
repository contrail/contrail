package org.slasoi.seval.prediction.modeladjustment;

import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.repository.SoftwareModel;

/**
 * Component to adjust a source model according to a given service prediction setup.
 * 
 * @author Benjamin Klatt
 * 
 */
public interface ModelAdjustment {

    /**
     * Adjust a software model to a specific service prediction setup.
     * 
     * <strong>Note:</strong> This adjustment manipulates the original model files. Make sure that you are provide a
     * model that is no longer needed in it's original state!
     * 
     * 
     * @param sourceModel
     *            The software model to adjust.
     * @param servicePredictionSetup
     *            The prediction setup to adjust to.
     * @return The adjusted software model.
     */
    SoftwareModel adjustModel(SoftwareModel sourceModel, PredictionModelModification servicePredictionSetup);
}
