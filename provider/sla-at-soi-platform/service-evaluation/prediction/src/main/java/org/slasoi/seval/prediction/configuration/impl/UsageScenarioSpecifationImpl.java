package org.slasoi.seval.prediction.configuration.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.seval.prediction.configuration.SystemCall;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;

/**
 * The specification on how a specific service is used. This includes the services identifier as well as the information
 * how this service is used.
 * 
 * @author Benjamin Klatt
 * 
 */
public class UsageScenarioSpecifationImpl implements UsageScenarioSpecification {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The name of the usage scenario to execute. */
    private String usageScenarioID = null;

    /** The arrival time describing the rate of new incoming requests. */
    private String interArrivalTime = null;

    /** The system calls that should be executed on the system. */
    private List<SystemCall> systemCalls = new LinkedList<SystemCall>();

    // ////////////////////////////////
    // Constructor
    // ////////////////////////////////

    /**
     * The default constructor.
     */
    public UsageScenarioSpecifationImpl() {
        super();
    }

    /**
     * The enhanced constructor to set the name.
     * 
     * @param id
     *            The id of the scenario.
     */
    public UsageScenarioSpecifationImpl(final String id) {
        this.usageScenarioID = id;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.impl.UsageScenarioSpecification#getUsageScenario()
     */
    public final String getUsageScenarioID() {
        return usageScenarioID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.impl.UsageScenarioSpecification#setUsageScenario(java.lang.String)
     */
    public final void setUsageScenarioID(final String usageScenarioID) {
        this.usageScenarioID = usageScenarioID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.impl.UsageScenarioSpecification#getInterArrivalTime()
     */
    public final String getInterArrivalTime() {
        return interArrivalTime;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.impl.UsageScenarioSpecification#setInterArrivalTime(java.lang.String)
     */
    public final void setInterArrivalTime(final String interArrivalTime) {
        this.interArrivalTime = interArrivalTime;
    }

    /**
     * Set the system calls to modify.
     * 
     * @param systemCalls
     *            The list of system calls.
     */
    public final void setSystemCalls(final List<SystemCall> systemCalls) {
        this.systemCalls = systemCalls;
    }

    /**
     * Get the list of system calls to modify.
     * 
     * @return The list of system calls to be modified.
     */
    public final List<SystemCall> getSystemCalls() {
        return systemCalls;
    }

    /**
     * Add a single system call to the usage scenario specification.
     * 
     * @param systemCall
     *            The system call specification to add.
     */
    public final void addSystemCall(final SystemCall systemCall) {
        if (systemCalls == null) {
            systemCalls = new LinkedList<SystemCall>();
        }
        systemCalls.add(systemCall);
    }
}
