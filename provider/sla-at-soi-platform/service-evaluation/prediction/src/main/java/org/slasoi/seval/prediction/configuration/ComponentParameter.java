package org.slasoi.seval.prediction.configuration;

/**
 * This component parameter object describes the adjustment of a parameter model
 * element within a component it belongs to.
 *
 * @author Benjamin Klatt
 *
 */
public interface ComponentParameter {

    /**
     * Get the name of the parameter.
     *
     * @return The name.
     */
    String getName();

    /**
     * Set the name of the parameter.
     *
     * @param name
     *            The name to be set.
     */
    void setName(String name);

    /**
     * Get the value of the parameter.
     *
     * @return The value of the parameter.
     */
    String getValue();

    /**
     * Set the value of the parameter.
     *
     * @param value
     *            The value to be set.
     */
    void setValue(String value);

}
