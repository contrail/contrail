package org.slasoi.seval.prediction.configuration.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.seval.prediction.configuration.SystemCall;
import org.slasoi.seval.prediction.configuration.SystemCallVariable;

/**
 * The specification of a system call.
 * 
 * @author Benjamin Klatt
 * 
 */
public class SystemCallImpl implements SystemCall {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The name for the system call. */
    private String name = null;

    /** The list of call variables. */
    private List<SystemCallVariable> variables = new LinkedList<SystemCallVariable>();

    // ////////////////////////////////
    // Constructor
    // ////////////////////////////////

    /**
     * The default constructor.
     */
    public SystemCallImpl() {
        super();
    }

    /**
     * The enhanced constructor to set the name.
     * 
     * @param name
     *            The name of the variable.
     */
    public SystemCallImpl(final String name) {
        this.name = name;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.SystemCall#getName()
     */
    public final String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.SystemCall#setName(java.lang.String)
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.SystemCall#getVariables()
     */
    public final List<SystemCallVariable> getVariables() {
        return variables;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.SystemCall#setVariables(java.util.List)
     */
    public final void setVariables(final List<SystemCallVariable> variables) {
        this.variables = variables;
    }

    /**
     * {@inheritDoc}
     */
    public final void addVariable(final SystemCallVariable variable) {
        if (this.variables == null) {
            variables = new LinkedList<SystemCallVariable>();
        }
        variables.add(variable);
    }
}
