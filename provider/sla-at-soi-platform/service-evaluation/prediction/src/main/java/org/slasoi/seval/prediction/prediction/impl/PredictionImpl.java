package org.slasoi.seval.prediction.prediction.impl;

import org.apache.log4j.Logger;
import org.slasoi.seval.prediction.prediction.Prediction;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceParams;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceResultStatus;
import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.impl.EvaluationResultMock;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.repository.SoftwareModel;

/**
 * This class represents the design-time prediction service within the SLA@SOI framework.
 * 
 * Internally, it acts as a client that communicates with a prediction server over a web service interface.
 * 
 * @author brosch
 * 
 */
public final class PredictionImpl implements Prediction {

    /**
     * Log4j functionality.
     */
    private static final Logger LOGGER = Logger.getLogger(PredictionImpl.class);

    /**
     * A singleton pattern ensures the existence of only one design-time prediction component within the SLA@SOI
     * framework.
     */
    private static Prediction singletonInstance = new PredictionImpl();

    /**
     * Retrieves the singleton prediction instance.
     * 
     * @return the singleton prediction instance
     */
    public static Prediction getInstance() {
        return singletonInstance;
    }

    /**
     * References the prediction result helper.
     */
    private PredictionResultHelper resultHelper = PredictionResultHelper.getHelper();

    /**
     * References the service client helper.
     */
    private SimuClientHelper serviceClientHelper = SimuClientHelper.getHelper();

    /**
     * The constructor is made private according to the singleton pattern.
     */
    private PredictionImpl() {
    }

    /**
     * Execute the prediction of a software model and prepare the results.
     * 
     * @param model
     *            representing the model to be simulated
     * @param evaluationMode
     *            the {@link EvaluationMode} (real or mock-up)
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @param evaluationServerEndpoint
     *            the web service endpoint of the evaluation server
     * @return The result of the prediction.
     */
    public IEvaluationResult predict(final SoftwareModel model, final EvaluationMode evaluationMode,
            final EvaluationSettings settings, final String evaluationServerEndpoint) {

        LOGGER.info("Design-time prediction: start");

        // Configure the service client:
        serviceClientHelper.setEvaluationServerEndpoint(evaluationServerEndpoint);

        boolean useServer = detectServerUsage(evaluationMode);

        // Evaluate the scenario or use mock-up result
        IEvaluationResult predictionResult = null;
        if (useServer) {
            predictionResult = evaluateScenario(model, settings);
        } else {
            predictionResult = new EvaluationResultMock(model);
        }

        LOGGER.info("Design-time prediction: finished");

        // Return the prediction result:
        return predictionResult;
    }

    /**
     * Detect if the remote server should be used for the prediction or not.
     * 
     * @param evaluationMode
     *            The pre configured evaluation mode.
     * @return true/false if the remote server should be used or not.
     */
    private boolean detectServerUsage(final EvaluationMode evaluationMode) {
        if (evaluationMode == EvaluationMode.Auto) {
            return serviceClientHelper.isConnectionAvailable();
        } else {
            return (evaluationMode == EvaluationMode.Server);
        }
    }

    /**
     * Evaluates a prediction scenario and aggregates the results.
     * 
     * @param softwareModel
     *            the model to be simulated.
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return the aggregated results
     */
    private IEvaluationResult evaluateScenario(final SoftwareModel softwareModel, final EvaluationSettings settings) {

        // This variable will hold status information about simulation success:
        SimuServiceResultStatus status = null;

        LOGGER.info("Evaluating prediction scenario");
        SimuServiceParams params = null;
        params = serviceClientHelper.createParams(softwareModel, settings);
        status = serviceClientHelper.callPredictionService(params);

        // Build the result model according to the evaluated prediction scenario:
        return resultHelper.buildPredictionResult(status);
    }
}
