/**
 * The default implementation package for the prediction configuration.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.configuration.impl;

