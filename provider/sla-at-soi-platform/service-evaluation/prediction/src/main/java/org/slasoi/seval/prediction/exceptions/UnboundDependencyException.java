package org.slasoi.seval.prediction.exceptions;

import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ServiceBuilder;

/**
 * Exception thrown if a service builder refers to unbound dependencies.
 * 
 * @author kuester, brosch
 */
public class UnboundDependencyException extends IllegalArgumentException {

    /**
     * A serial version UID for the serializable class.
     */
    private static final long serialVersionUID = 2626711131933368558L;

    /**
     * To be thrown when a dependency needed is not bound.
     * 
     * @param builder
     *            the service builder
     * @param dependency
     *            the unbound dependency
     */
    public UnboundDependencyException(final ServiceBuilder builder, final Dependency dependency) {
        super("Dependency [" + dependency + "] of service builder [" + builder + "] is unbound!");
    }
}
