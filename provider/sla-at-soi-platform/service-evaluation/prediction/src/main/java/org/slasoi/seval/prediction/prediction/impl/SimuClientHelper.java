package org.slasoi.seval.prediction.prediction.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.slasoi.seval.prediction.exceptions.InternalEvaluationException;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.IsAvailable;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.IsAvailableResponse;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceParams;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceResultStatus;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Simulate;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.seval.repository.container.AllocationModelContainer;
import org.slasoi.seval.repository.container.ArchitectureModelContainer;
import org.slasoi.seval.repository.container.InfrastructureModelContainer;
import org.slasoi.seval.repository.container.ServiceComponentModelContainer;
import org.slasoi.seval.repository.container.UsageModelContainer;
import org.slasoi.seval.repository.exceptions.ContainerNotFoundException;

/**
 * Helper class to communicate with the remote prediction server.
 * 
 * @author Benjamin Klatt
 * @author brosch
 * @author kuester
 */
public final class SimuClientHelper {

    // ////////////////////////////////
    // CONSTANTS
    // ////////////////////////////////

    /** Temporary path that may have been placed in the model by the software service evaluation. */
    private static final String TEMPORARY_MODEL_PATH = "CommonPCMModels/";

    /**
     * Log4j functionality.
     */
    private static final Logger LOGGER = Logger.getLogger(SimuClientHelper.class.getName());

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /**
     * A singleton pattern ensures the existence of only one service helper.
     */
    private static SimuClientHelper singletonInstance = new SimuClientHelper();

    /**
     * Determines the endpoint where the evaluation server is expected to run.
     */
    private String evaluationServerEndpoint = "http://localhost:8082/services";

    // ////////////////////////////////
    // CONSTRUCTOR
    // ////////////////////////////////

    /**
     * Retrieves the singleton instance.
     * 
     * @return the singleton instance
     */
    public static SimuClientHelper getHelper() {
        return singletonInstance;
    }

    /**
     * The constructor is made private according to the singleton pattern.
     */
    private SimuClientHelper() {
    }

    // ////////////////////////////////
    // BUSINESS METHODS
    // ////////////////////////////////

    /**
     * Issues a simulation service call.
     * 
     * @param params
     *            parameters for the simulation service call
     * @return the return status of the SimuService call
     */
    public SimuServiceResultStatus callPredictionService(final SimuServiceParams params) {

        LOGGER.info("Calling SimuService...");

        SimuServiceServiceStub serviceStub = null;
        try {
            serviceStub = new SimuServiceServiceStub(evaluationServerEndpoint);
        } catch (Exception e) {
            LOGGER.warn("Calling SimuService: " + e.getMessage());
            SimuServiceResultStatus status = new SimuServiceResultStatus();
            if (e.getCause() instanceof ConnectException) {
                status.setException("CONNECT");
            } else {
                status.setException(e.getMessage());
            }
            return status;
        }

        // Call service:
        Simulate sim = new Simulate();
        sim.setArg0(params);
        SimuServiceServiceCallbackHandlerImpl callbackHandler = new SimuServiceServiceCallbackHandlerImpl();
        try {
            serviceStub.startsimulate(sim, callbackHandler);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        SimuServiceResultStatus status =
                ((SimuServiceServiceCallbackHandlerImpl) callbackHandler).getResult().get_return();
        try {
            serviceStub.cleanup();
        } catch (AxisFault e) {
            LOGGER.info("Webservice call clean up failed");
        }

        validateSimulationResult(status);
        return status;
    }

    /**
     * Creates parameters for the SimuService call.
     * 
     * This class reloads the original models from disk and does not reflect the in memory models
     * that have been adjusted. The infrastructure model is the first one that is serialized from the adjusted instance.
     * 
     * @param softwareModel
     *            The software model to predict
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return the parameters for the SimuService call
     */
    public SimuServiceParams createParams(final SoftwareModel softwareModel, final EvaluationSettings settings) {
        // Declare the result object:
        SimuServiceParams params = new SimuServiceParams();

        try {
            // Determine parameters from the given software model:
            AllocationModelContainer amc = softwareModel.getAllocationModelContainer();
            params.setAllocationModelFileName(amc.getModelFileName());
            params.setAllocationModelXMI(transformModelToXMIString(amc.getModel()));

            ServiceComponentModelContainer scmc = softwareModel.getServiceComponentModelContainer();
            int numberRepositories = scmc.getComponentModelsContainers().size();
            String[] names = new String[numberRepositories];
            String[] xmis = new String[numberRepositories];
            for (int i = 0; i < numberRepositories; i++) {
                names[i] = scmc.getComponentModelsContainers().get(i).getModelFileName();
                xmis[i] = transformModelToXMIString(scmc.getComponentModelsContainers().get(i).getModel());
            }
            params.setRepositoryModelFileNames(names);
            params.setRepositoryModelXMIs(xmis);

            InfrastructureModelContainer imc = softwareModel.getInfrastructureModelContainer();
            params.setResourceEnvironmentFileName(imc.getModelFileName());
            params.setResourceEnvironmentModelXMI(transformModelToXMIString(imc.getModel()));

            ArchitectureModelContainer archmc = softwareModel.getArchitectureModelContainer();
            params.setSystemModelFileName(archmc.getModelFileName());
            params.setSystemModelXMI(transformModelToXMIString(archmc.getModel()));

            UsageModelContainer umc = softwareModel.getUsageModelContainer();
            params.setUsageModelFileName(umc.getModelFileName());
            params.setUsageModelXMI(transformModelToXMIString(umc.getModel()));

            // Determine parameters from the evaluation settings
            params.setMaxMeasurementsCount(settings.getMaxMeasurementsCount());
            params.setMaxSimTime(settings.getMaxSimTime());
            params.setVerboseLogging(settings.isVerboseLogging());
            params.setSimulateFailures(settings.isAnalyzeResponsibility());
            if (settings.getSensorNames().size() > 0) {
                String[] sensorArray =
                        settings.getSensorNames().toArray(new String[settings.getSensorNames().size()]);
                params.setSensorNames(sensorArray);
            }
        } catch (ContainerNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set further parameters:
        params.setClearLogging(true);
        params.setVerboseLogging(false);

        // Return the result:
        return params;
    }

    /**
     * Transform a model represented by an EObject instance to an XMI string.
     * 
     * Note: This method also cleans up any temporary URIs to general path map references.
     * 
     * @param model
     *            The model to transform.
     * @return The XMI string. This will be empty if anything failed.
     * @throws IOException Identifying a problem to read the model from the file system.
     */
    private String transformModelToXMIString(final EObject model) throws IOException {
        Resource resource = model.eResource();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String xmi = "";
        try {
            resource.save(outputStream, null);
            xmi = outputStream.toString();
        } catch (IOException e) {
            LOGGER.error("Unable to transform model to XMI", e);
            throw e;
        }

        // if the model had be modified by the software service evaluation
        // we have to relocate the local model references to pathmap URIs
        // this requires to first identify the temporary path and then replacing all occurences
        int indexOfTemporaryModelPath = xmi.indexOf(TEMPORARY_MODEL_PATH);
        if (indexOfTemporaryModelPath != -1) {
            int pathStartingIndex = xmi.lastIndexOf("\"", indexOfTemporaryModelPath);
            String completeTemporaryModelPath =
                    xmi.substring(pathStartingIndex + 1, indexOfTemporaryModelPath + TEMPORARY_MODEL_PATH.length());
            xmi = xmi.replaceAll(completeTemporaryModelPath, "pathmap://PCM_MODELS/");
        }

        return xmi;
    }

    /**
     * Checks if the evaluation resulted in any unwanted condition.
     * 
     * @param status
     *            the simulation results
     */
    private void validateSimulationResult(final SimuServiceResultStatus status) {
        if ((status.getException() != null) || !status.getWorkflowCreated()
                || !status.getWorkflowParamsConfigured() || !status.getWorkflowSuccessful()) {
            throw new InternalEvaluationException(status);
        }
    }

    /**
     * Checks whether a connection to the server can be established.
     * 
     * @return true, if connection attempt is successful; false otherwise
     */
    public boolean isConnectionAvailable() {

        boolean connectionAvailable = false;

        try {
            SimuServiceServiceStub service = new SimuServiceServiceStub(evaluationServerEndpoint);
            IsAvailable isAvailable = new IsAvailable();
            IsAvailableResponse response = service.isAvailable(isAvailable);
            if (response != null && response.get_return()) {
                connectionAvailable = true;
                LOGGER.info("Prediction Server Connection Available.");
            } else {
                connectionAvailable = false;
                LOGGER.info("Prediction Server Connection NOT Available.");
            }
        } catch (RemoteException e) {
            connectionAvailable = false;
            LOGGER.info("Prediction Server Connection NOT Available.");
        }
        return connectionAvailable;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * Sets the endpoint of the evaluation web server.
     * 
     * @param evaluationServerEndpoint
     *            the endpoint
     */
    public void setEvaluationServerEndpoint(final String evaluationServerEndpoint) {
        this.evaluationServerEndpoint = evaluationServerEndpoint;
    }

    /**
     * Retrieves the endpoint where the evaluation server is expected to run.
     * 
     * @return the evaluation server endpoint.
     */
    public String getEvaluationServerEndpoint() {
        return evaluationServerEndpoint;
    }
}
