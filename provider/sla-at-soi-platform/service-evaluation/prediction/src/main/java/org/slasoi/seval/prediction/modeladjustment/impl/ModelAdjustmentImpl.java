package org.slasoi.seval.prediction.modeladjustment.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.slasoi.seval.prediction.configuration.ComponentParameter;
import org.slasoi.seval.prediction.configuration.ComponentSpecification;
import org.slasoi.seval.prediction.configuration.ResourceContainerSpecification;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.SystemCall;
import org.slasoi.seval.prediction.configuration.SystemCallVariable;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.seval.prediction.modeladjustment.ModelAdjustment;
import org.slasoi.seval.repository.Repository;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.seval.repository.container.ComponentModelContainer;
import org.slasoi.seval.repository.container.InfrastructureModelContainer;

import de.uka.ipd.sdq.pcm.core.CoreFactory;
import de.uka.ipd.sdq.pcm.core.PCMRandomVariable;
import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisation;
import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisationType;
import de.uka.ipd.sdq.pcm.parameter.VariableUsage;
import de.uka.ipd.sdq.pcm.repository.BasicComponent;
import de.uka.ipd.sdq.pcm.repository.RepositoryComponent;
import de.uka.ipd.sdq.pcm.resourceenvironment.ProcessingResourceSpecification;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceenvironmentFactory;
import de.uka.ipd.sdq.pcm.resourceenvironment.SchedulingPolicy;
import de.uka.ipd.sdq.pcm.resourcetype.ProcessingResourceType;
import de.uka.ipd.sdq.pcm.usagemodel.AbstractUserAction;
import de.uka.ipd.sdq.pcm.usagemodel.EntryLevelSystemCall;
import de.uka.ipd.sdq.pcm.usagemodel.OpenWorkload;
import de.uka.ipd.sdq.pcm.usagemodel.ScenarioBehaviour;
import de.uka.ipd.sdq.pcm.usagemodel.UsageModel;
import de.uka.ipd.sdq.pcm.usagemodel.UsageScenario;
import de.uka.ipd.sdq.pcm.usagemodel.Workload;

/**
 * Model adjustment based on a manual manipulation of the source model.
 * 
 * @author Benjamin Klatt
 * 
 */
public class ModelAdjustmentImpl implements ModelAdjustment {

    // ////////////////////////////////
    // CONSTANTS
    // ////////////////////////////////

    /** The factor to calculate the disk speed from sla parameter to pcm representation. */
    private static final double DISK_SPEED_FACTOR = 1000.0;

    /** The cpu speed up rate per additional core. */
    private static final double PROCESSING_RATE_PER_CORE = 0.75;

    /** LOGGER. **/
    private static final Logger LOGGER = Logger.getLogger(ModelAdjustmentImpl.class);

    /** The id of the single resource container. **/
    public static final String RESOURCE_CONTAINER_SINGLE_ID = "AllInOne Infrastructure";

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The repository to use to access additional model information. */
    private Repository repository = null;

    // ////////////////////////////////
    // CONSTRUCTOR
    // ////////////////////////////////

    /**
     * Constructor adding the required components to the model adjustment component.
     * 
     * @param repository
     *            The repository to link.
     */
    public ModelAdjustmentImpl(final Repository repository) {
        this.repository = repository;
    }

    // ////////////////////////////////
    // BUSINESS METHODS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * <strong>Note:</strong> This adjustment manipulates the original model files. Make sure that you are provide a
     * model that is no longer needed in it's original state!
     */
    public final SoftwareModel adjustModel(final SoftwareModel softwareModel,
            final PredictionModelModification servicePredictionSetup) {

        // adjust resource containers
        List<ResourceContainerSpecification> resourceContainerSpecs =
                servicePredictionSetup.getResourceContainerSpecifications();
        for (ResourceContainerSpecification resourceContainerSpecification : resourceContainerSpecs) {
            adjustInfrastructureContainer(softwareModel, resourceContainerSpecification);
        }

        // adjust usage profiles
        adjustUsageProfiles(softwareModel, servicePredictionSetup.getUsageScenarioSpecifications());

        // adjust components
        List<ComponentSpecification> componentSpecs = servicePredictionSetup.getComponentSpecifications();
        for (ComponentSpecification componentSpecification : componentSpecs) {
            adjustComponent(softwareModel, componentSpecification);
        }

        return softwareModel;
    }

    /**
     * Adjust a specific component within the model.
     * 
     * @param softwareModel
     *            The model containing the component to be adjusted.
     * @param componentSpecification
     *            The specification of the component to be adjusted.
     */
    private void adjustComponent(final SoftwareModel softwareModel,
            final ComponentSpecification componentSpecification) {

        // find and adjust the component
        for (ComponentModelContainer componentModelContainer : softwareModel.getComponentModelContainers()) {
            de.uka.ipd.sdq.pcm.repository.Repository pcmRepository = componentModelContainer.getModel();
            BasicComponent component = findBasicComponentByName(pcmRepository, componentSpecification.getName());

            // find and adjust the parameters
            for (ComponentParameter parameterSpec : componentSpecification.getParameters()) {
                VariableUsage variable = findVariableUsageByName(component, parameterSpec.getName());
                if (variable != null) {
                    VariableCharacterisation value = findValueCharacterization(variable);
                    value.getSpecification_VariableCharacterisation().setSpecification(parameterSpec.getValue());
                } else {
                    LOGGER.info("ComponentParameter to adjust not found for name: " + parameterSpec.getName());
                }
            }
        }
    }

    /**
     * Adjust the usage profile within the software model. The is method will work directly on the input software model
     * object.
     * 
     * @param softwareModel
     *            The software model to adjust.
     * @param scenarioSpecifications
     *            The list of scenario specifications to get the parameters from.
     */
    private void adjustUsageProfiles(final SoftwareModel softwareModel,
            final List<UsageScenarioSpecification> scenarioSpecifications) {

        UsageModel usageModel = softwareModel.getUsageModelContainer().getModel();
        EList<UsageScenario> scenarios = usageModel.getUsageScenario_UsageModel();

        List<UsageScenario> scenariosToBePredicted = new LinkedList<UsageScenario>();

        for (UsageScenario scenario : scenarios) {
            String scenarioName = scenario.getEntityName();

            // get the specification for a scenario
            UsageScenarioSpecification specification =
                    findUsageSpecificationById(scenarioName, scenarioSpecifications);
            if (specification != null) {
                adjustScenario(scenario, specification);
                scenariosToBePredicted.add(scenario);
            }
        }

        // ensure only those usage scenarios are included that should be predicted
        usageModel.getUsageScenario_UsageModel().clear();
        for (UsageScenario usageScenario : scenariosToBePredicted) {
            usageModel.getUsageScenario_UsageModel().add(usageScenario);
        }
    }

    /**
     * Adjust a single usage scenario according to a provided specification. This will change the workload defined in
     * this scenario. This method modifies the scenario object directly.
     * 
     * @param scenario
     *            The scenario to adjust
     * @param specification
     *            The specification of the parameters to set.
     */
    private void adjustScenario(final UsageScenario scenario, final UsageScenarioSpecification specification) {
        adjustSystemCalls(scenario, specification);
        adjustWorkload(scenario, specification);
    }

    /**
     * Adjust the entry level system calls of a usage scenario.
     * 
     * @param scenario
     *            The scenario to adjust.
     * @param scenarionSpecification
     *            The specification including the system call variables.
     */
    private void adjustSystemCalls(final UsageScenario scenario,
            final UsageScenarioSpecification scenarionSpecification) {

        // find and adjust the behaviours
        ScenarioBehaviour behaviour = scenario.getScenarioBehaviour_UsageScenario();
        for (SystemCall systemCall : scenarionSpecification.getSystemCalls()) {

            // find and adjust the system calls
            EntryLevelSystemCall call = findEntryLevelSystemCallByName(behaviour, systemCall.getName());
            for (SystemCallVariable callVariable : systemCall.getVariables()) {

                // find and adjust the system call variable values
                VariableUsage variable = findVariableUsageByName(call, callVariable.getName());
                for (VariableCharacterisation characterisation : variable
                        .getVariableCharacterisation_VariableUsage()) {
                    if (characterisation.getType() == VariableCharacterisationType.VALUE) {
                        characterisation.getSpecification_VariableCharacterisation().setSpecification(
                                callVariable.getValue());
                    }
                }
            }
        }
    }

    /**
     * Adjust the workload definition of an usage scenario.
     * 
     * @param scenario
     *            The scenario to adjust the workload of.
     * @param specification
     *            The specification of the adjustment.
     */
    private void adjustWorkload(final UsageScenario scenario, final UsageScenarioSpecification specification) {

        if (specification.getInterArrivalTime() != null) {
            Workload workload = scenario.getWorkload_UsageScenario();
            if (workload instanceof OpenWorkload) {
                OpenWorkload openWorkload = (OpenWorkload) workload;

                String value = specification.getInterArrivalTime();
                if (value.indexOf('.') == -1) {
                    value = value + ".0";
                }
                openWorkload.getInterArrivalTime_OpenWorkload().setSpecification("Exp(1.0/" + value + ")");

            } else {
                LOGGER.error("Scenario with a closed workload defined which can not be adjusted. ("
                        + scenario.getEntityName() + ")");
            }
        }
    }

    /**
     * Get a usage specification from a list of specification by it's ID.
     * 
     * @param usageID
     *            The id of the usage scenario
     * @param usageSpecificationList
     *            The list to look up the specification in.
     * @return The matching specification or null if none has been found
     */
    public final UsageScenarioSpecification findUsageSpecificationById(final String usageID,
            final List<UsageScenarioSpecification> usageSpecificationList) {

        UsageScenarioSpecification specification = null;

        for (UsageScenarioSpecification usageScenarioSpecification : usageSpecificationList) {
            if (usageScenarioSpecification.getUsageScenarioID().equals(usageID)) {
                specification = usageScenarioSpecification;
            }
        }

        return specification;
    }

    /**
     * Find an EntryLevelSystemCall in a scenario behavior identified by the name of the entry level system call.
     * 
     * Note: If there are multiple calls with the same name, the first one will be returned.
     * 
     * @param behaviour
     *            The scenario behaviour to scan for the call.
     * @param name
     *            The name of the call to look for.
     * @return The found call or null if there is no matching one.
     */
    public final EntryLevelSystemCall findEntryLevelSystemCallByName(final ScenarioBehaviour behaviour,
            final String name) {
        for (AbstractUserAction action : behaviour.getActions_ScenarioBehaviour()) {
            if (action instanceof EntryLevelSystemCall && action.getEntityName().equals(name)) {
                return (EntryLevelSystemCall) action;
            }
        }
        return null;
    }

    /**
     * Find a basic component in a PCM repository by it's name.
     * 
     * @param pcmRepository
     *            The repository to check.
     * @param name
     *            The name of the variable usage to search for.
     * @return The found variable usage or null if there is no matching one.
     */
    public final BasicComponent findBasicComponentByName(
            final de.uka.ipd.sdq.pcm.repository.Repository pcmRepository, final String name) {
        for (RepositoryComponent component : pcmRepository.getComponents__Repository()) {
            if (component instanceof BasicComponent && component.getEntityName().equals(name)) {
                return (BasicComponent) component;
            }
        }
        return null;
    }

    /**
     * Find a variable usage in an entry level system call by it's name.
     * 
     * @param callAction
     *            The action to check.
     * @param name
     *            The name of the variable usage to search for.
     * @return The found variable usage or null if there is no matching one.
     */
    public final VariableUsage findVariableUsageByName(final EntryLevelSystemCall callAction, final String name) {
        for (VariableUsage variable : callAction.getInputParameterUsages_EntryLevelSystemCall()) {
            if (variable.getNamedReference__VariableUsage().getReferenceName().equals(name)) {
                return variable;
            }
        }
        return null;
    }

    /**
     * Find a variable usage in an entry level system call by it's name.
     * 
     * @param component
     *            The action to check.
     * @param name
     *            The name of the variable usage to search for.
     * @return The found variable usage or null if there is no matching one.
     */
    public final VariableUsage findVariableUsageByName(final BasicComponent component, final String name) {
        for (VariableUsage variable : component.getComponentParameterUsage_ImplementationComponentType()) {
            if (variable.getNamedReference__VariableUsage().getReferenceName().equals(name)) {
                return variable;
            }
        }
        return null;
    }

    /**
     * Find the VALUE characterization of a variable usage.
     * 
     * @param variable
     *            The variable usage to analyze.
     * @return The VALUE characterization element or null if not set.
     */
    public final VariableCharacterisation findValueCharacterization(final VariableUsage variable) {
        for (VariableCharacterisation characterisation : variable.getVariableCharacterisation_VariableUsage()) {
            if (characterisation.getType() == VariableCharacterisationType.VALUE) {
                return characterisation;
            }
        }
        return null;
    }

    /**
     * Adjust a resource container in the source model according to the provided specification.
     * 
     * @param softwareModel
     *            The software model containing the infrastructure container to be adjusted.
     * @param specification
     *            The specification of the resource container to adjust.
     */
    private void adjustInfrastructureContainer(final SoftwareModel softwareModel,
            final ResourceContainerSpecification specification) {

        InfrastructureModelContainer infrastructureContainer = softwareModel.getInfrastructureModelContainer();
        ResourceContainer resourceContainer =
                infrastructureContainer.getResourceContainer(specification.getContainerID());

        if (resourceContainer == null) {
            LOGGER.error("Resource Container with ID [" + specification.getContainerID()
                    + "] not specified in in the resource environment");
            resourceContainer = infrastructureContainer.getResourceContainer(RESOURCE_CONTAINER_SINGLE_ID);
        }

        // clear the active resource specification list
        resourceContainer.getActiveResourceSpecifications_ResourceContainer().clear();

        addCPU(resourceContainer, specification);
        addHDD(resourceContainer, specification);
        addDELAY(resourceContainer, specification);
        addLAN(resourceContainer, specification);
    }

    /**
     * Create a CPU resource specification. This takes a resource container specification into account. Furthermore some
     * default settings are used:
     * <ul>
     * <li>Scheduling: SchedulingPolicy.PROCESSOR_SHARING</li>
     * </ul>
     * 
     * This CPU creation calculates the cpu processing rate based on the requested cpu speed and number of cores.
     * processingRate = cpuSpeed * (1+((cpucores - 1)*0.75))
     * 
     * NOTE: When the new PCM release is integrated, the number of cores will be respected by the prediction server
     * 
     * @param resourceContainer
     *            The resource container to place the cpu in.
     * @param specification
     *            The specification that defines the cpu parameters.
     */
    private void addCPU(final ResourceContainer resourceContainer,
            final ResourceContainerSpecification specification) {

        // calculate the processing rate
        Double cpuSpeed = Double.parseDouble(specification.getCpuSpeed().getValue());
        Integer cpuNumberCores = Integer.parseInt(specification.getCpuCores().getValue());
        Double cpuCoreFactor = 1.0 + (cpuNumberCores - 1) * PROCESSING_RATE_PER_CORE;
        Double cpuProcessingRate = cpuSpeed * cpuCoreFactor;

        // initialize the factories
        ResourceenvironmentFactory factory = ResourceenvironmentFactory.eINSTANCE;
        CoreFactory coreFactory = CoreFactory.eINSTANCE;

        // prepare the settings
        ProcessingResourceType resourceType = repository.getResourceTypeModelContainer().getCPUElement();
        PCMRandomVariable processingRate = coreFactory.createPCMRandomVariable();
        processingRate.setSpecification(cpuProcessingRate.toString());

        // build the cpu
        ProcessingResourceSpecification cpuSpecification = factory.createProcessingResourceSpecification();
        cpuSpecification.setResourceContainer_ProcessingResourceSpecification(resourceContainer);
        cpuSpecification.setSchedulingPolicy(SchedulingPolicy.PROCESSOR_SHARING);
        cpuSpecification.setActiveResourceType_ActiveResourceSpecification(resourceType);
        processingRate.setProcessingResourceSpecification_processingRate_PCMRandomVariable(cpuSpecification);
        cpuSpecification.setProcessingRate_ProcessingResourceSpecification(processingRate);

        // add the cpu to the container
        resourceContainer.getActiveResourceSpecifications_ResourceContainer().add(cpuSpecification);
    }

    /**
     * Create a HDD resource specification. This takes a resource container specification into account. Furthermore some
     * default settings are used:
     * <ul>
     * <li>Processing Rate: 100</li>
     * </ul>
     * 
     * @param resourceContainer
     *            The resource container to place the hdd in.
     * @param specification
     *            The specification that defines the hdd parameters.
     */
    private void addHDD(final ResourceContainer resourceContainer,
            final ResourceContainerSpecification specification) {

        // disc speed conversion
        Double hddSpeed = Double.parseDouble(specification.getMemory().getValue()) / DISK_SPEED_FACTOR;

        // init the factories
        ResourceenvironmentFactory factory = ResourceenvironmentFactory.eINSTANCE;
        CoreFactory coreFactory = CoreFactory.eINSTANCE;

        // prepare the settings
        ProcessingResourceType resourceType = repository.getResourceTypeModelContainer().getHDDElement();
        PCMRandomVariable processingRate = coreFactory.createPCMRandomVariable();
        processingRate.setSpecification(hddSpeed.toString());

        // build the hdd
        ProcessingResourceSpecification hddSpecification = factory.createProcessingResourceSpecification();
        hddSpecification.setResourceContainer_ProcessingResourceSpecification(resourceContainer);
        hddSpecification.setActiveResourceType_ActiveResourceSpecification(resourceType);
        processingRate.setProcessingResourceSpecification_processingRate_PCMRandomVariable(hddSpecification);
        hddSpecification.setProcessingRate_ProcessingResourceSpecification(processingRate);

        // add the hdd to the container
        resourceContainer.getActiveResourceSpecifications_ResourceContainer().add(hddSpecification);
    }

    /**
     * Create a DELAY resource specification. This takes a resource container specification into account. Furthermore
     * some default settings are used:
     * <ul>
     * <li>Processing Rate: 0</li>
     * </ul>
     * 
     * @param resourceContainer
     *            The resource container to place the delay in.
     * @param specification
     *            The specification that defines the delay parameters.
     */
    private void addDELAY(final ResourceContainer resourceContainer,
            final ResourceContainerSpecification specification) {

        // init the factories
        ResourceenvironmentFactory factory = ResourceenvironmentFactory.eINSTANCE;
        CoreFactory coreFactory = CoreFactory.eINSTANCE;

        // prepare the settings
        ProcessingResourceType resourceType = repository.getResourceTypeModelContainer().getDELAYElement();
        PCMRandomVariable processingRate = coreFactory.createPCMRandomVariable();
        processingRate.setSpecification("0");

        // build the hdd
        ProcessingResourceSpecification delaySpecification = factory.createProcessingResourceSpecification();
        delaySpecification.setResourceContainer_ProcessingResourceSpecification(resourceContainer);
        delaySpecification.setActiveResourceType_ActiveResourceSpecification(resourceType);
        processingRate.setProcessingResourceSpecification_processingRate_PCMRandomVariable(delaySpecification);
        delaySpecification.setProcessingRate_ProcessingResourceSpecification(processingRate);

        // add the cpu to the container
        resourceContainer.getActiveResourceSpecifications_ResourceContainer().add(delaySpecification);
    }

    /**
     * Create a DELAY resource specification. This takes a resource container specification into account. Furthermore
     * some default settings are used:
     * <ul>
     * <li>Processing Rate: 0</li>
     * </ul>
     * 
     * @param resourceContainer
     *            The resource container to place the delay in.
     * @param specification
     *            The specification that defines the delay parameters.
     */
    private void addLAN(final ResourceContainer resourceContainer, final ResourceContainerSpecification specification) {

        // init the factories
        ResourceenvironmentFactory factory = ResourceenvironmentFactory.eINSTANCE;
        CoreFactory coreFactory = CoreFactory.eINSTANCE;

        // prepare the settings
        ProcessingResourceType resourceType = repository.getResourceTypeModelContainer().getLANElement();
        PCMRandomVariable processingRate = coreFactory.createPCMRandomVariable();
        processingRate.setSpecification("0");

        // build the hdd
        ProcessingResourceSpecification lanSpecification = factory.createProcessingResourceSpecification();
        lanSpecification.setResourceContainer_ProcessingResourceSpecification(resourceContainer);
        lanSpecification.setActiveResourceType_ActiveResourceSpecification(resourceType);
        processingRate.setProcessingResourceSpecification_processingRate_PCMRandomVariable(lanSpecification);
        lanSpecification.setProcessingRate_ProcessingResourceSpecification(processingRate);

        // add the cpu to the container
        resourceContainer.getActiveResourceSpecifications_ResourceContainer().add(lanSpecification);
    }
}
