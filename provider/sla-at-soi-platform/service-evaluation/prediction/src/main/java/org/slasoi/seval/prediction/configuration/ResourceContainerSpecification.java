package org.slasoi.seval.prediction.configuration;

import org.slasoi.seval.prediction.extractor.SLATemplateValue;

/**
 * The specification for a resource container including the container identifier
 * and the properties of it's configuration.
 *
 * @author Benjamin Klatt
 */
public interface ResourceContainerSpecification {

    /**
     * Get the identifier for the container.
     * @return The identifier of the container.
     */
    String getContainerID();

    /**
     * Set the identifier for the container.
     *
     * @param containerID
     *            The id to be set.
     */
    void setContainerID(String containerID);

    /**
     * Get the definition of the containers cpu speed.
     *
     * @return The defined cpu speed.
     */
    SLATemplateValue getCpuSpeed();

    /**
     * Set the definition of the containers cpu speed.
     *
     * @param cpuSpeed
     *            The cpu speed to be set.
     */
    void setCpuSpeed(SLATemplateValue cpuSpeed);

    /**
     * Get the definition of the containers number of cpu cores.
     *
     * @return The defined number of cpu cores.
     */
    SLATemplateValue getCpuCores();

    /**
     * Set the definition of the containers number of cpu cores.
     *
     * @param cpuCores
     *            The number of cores to be set.
     */
    void setCpuCores(SLATemplateValue cpuCores);

    /**
     * Get the definition of the containers number of cpu cores.
     *
     * @return The defined amount of memory.
     */
    SLATemplateValue getMemory();

    /**
     * Set the definition of the containers number of cpu cores.
     *
     * @param memory
     *            The amount of memory to be set.
     */
    void setMemory(SLATemplateValue memory);

}
