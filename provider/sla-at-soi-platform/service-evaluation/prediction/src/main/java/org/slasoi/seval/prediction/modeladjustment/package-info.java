/**
 * Sub component to adjust a prediction model according to the provided parameters.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.modeladjustment;

