package org.slasoi.seval.prediction.service;

import java.util.Set;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <p>
 * Common interface for model-based design-time evaluation of services.
 * </p>
 * <p>
 * For a detailed description of the main interface method, see {@link IServiceEvaluation#evaluate(Set, SLATemplate)}
 * </p>
 * 
 * @author kuester
 */
public interface IServiceEvaluation {

    /**
     * This method evaluates expected quality properties (performance and reliability) of software services.
     * 
     * <h1>Rationale</h1>
     * <p>
     * The central method for access to the WP A6 design time prediction component. When called with a set of
     * {@link ServiceBuilder}s the different realizations are considered as options for the same {@link SLATemplate}
     * passed as second argument. The method evaluates QoS properties of the specified system under consideration and
     * returns, for each realization, an {@link IEvaluationResult}.
     * </p>
     * <h1>Example</h1>
     * <p>
     * Given a service called <b>FooService</b> (for which a design-time model be present) with interface method
     * <b>IFoo.doIt()</b> and the SLA(T) containing an agreement term like <blockquote>
     * <tt><b>mean(completion_time(doIt) &lt; 100 ms</b></tt></blockquote> the realizations are fed into the model
     * assembly and based on this, the prediction service returns the expected mean completion time for method
     * <b>doIt()</b>. This is represented as an {@link IAggregateResult} having an arbitrary number of
     * {@link ISingleResult}s mapping the term {@link common#completion_time} to the respective predicted values.
     * </p>
     * <p>
     * There might occur two types of unchecked exceptions:
     * <ul>
     * <li>UnsupportedTermException - In case no QoS terms are part of the agreement</li>
     * <li>UnboundDependencyException - In case there are unbound dependencies (see
     * {@link ServiceBuilder#getOpenDependencies()}).</li>
     * </ul>
     * </p>
     * 
     * @param realizations
     *            A set containing at least one
     * @param template
     *            The {@link SLATemplate} associated with the service to be evaluated.
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return set of {@link IEvaluationResult}s. Contains one evaluation result for each service realization.
     * @throws ModelNotFoundException
     *             in case the design time repository does not hold the required QoS model
     */
    Set<IEvaluationResult> evaluate(Set<ServiceBuilder> realizations, SLATemplate template,
            EvaluationSettings settings) throws ModelNotFoundException;

    /**
     * This method evaluates expected quality properties (performance and reliability) of software services. It makes
     * use of default settings for the EvaluationSettings object.
     * <p>
     * There might occur two types of unchecked exceptions:
     * <ul>
     * <li>UnsupportedTermException - In case no QoS terms are part of the agreement</li>
     * <li>UnboundDependencyException - In case there are unbound dependencies (see
     * {@link ServiceBuilder#getOpenDependencies()}).</li>
     * </ul>
     * </p>
     * 
     * @param realizations
     *            A set containing at least one
     * @param template
     *            The {@link SLATemplate} associated with the service to be evaluated.
     * @return set of {@link IEvaluationResult}s. Contains one evaluation result for each service realization.
     * @throws ModelNotFoundException
     *             in case the design time repository does not hold the required QoS model
     */
    Set<IEvaluationResult> evaluate(Set<ServiceBuilder> realizations, SLATemplate template)
            throws ModelNotFoundException;

    /**
     * Tests a set of {@link ServiceBuilder}s for being valid, i.e. being non-empty and having no unbound service
     * dependencies.
     * <p>
     * There might occur two types of unchecked exceptions:
     * <ul>
     * <li>UnsupportedTermException - In case no QoS terms are part of the agreement</li>
     * <li>UnboundDependencyException - In case there are unbound dependencies (see
     * {@link ServiceBuilder#getOpenDependencies()}).</li>
     * </ul>
     * </p>
     * 
     * @param builders
     *            the set of {@link ServiceBuilder}s to validate
     */
    void validate(Set<ServiceBuilder> builders);
}
