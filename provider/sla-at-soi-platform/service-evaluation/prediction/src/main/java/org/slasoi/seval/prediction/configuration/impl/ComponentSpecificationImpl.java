package org.slasoi.seval.prediction.configuration.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.seval.prediction.configuration.ComponentParameter;
import org.slasoi.seval.prediction.configuration.ComponentSpecification;

/**
 * The specification of component properties to modify.
 * 
 * @author Benjamin Klatt
 */
public class ComponentSpecificationImpl implements ComponentSpecification {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The name for the system call. */
    private String name = null;

    /** The list of call variables. */
    private List<ComponentParameter> parameters = new LinkedList<ComponentParameter>();

    // ////////////////////////////////
    // Constructor
    // ////////////////////////////////

    /**
     * The default constructor.
     */
    public ComponentSpecificationImpl() {
        super();
    }

    /**
     * Constructor directly accepting the name of the component.
     * 
     * @param name
     *            The name of the component.
     */
    public ComponentSpecificationImpl(final String name) {
        super();
        this.name = name;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentSpecification#getName()
     */
    public final String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentSpecification#setName(java.lang.String)
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentSpecification#getParameters()
     */
    public final List<ComponentParameter> getParameters() {
        return parameters;
    }
}
