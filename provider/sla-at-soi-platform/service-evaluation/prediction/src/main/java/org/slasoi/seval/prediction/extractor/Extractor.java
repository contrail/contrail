package org.slasoi.seval.prediction.extractor;

import java.util.List;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Extractor to get prediction relevant information out of SLA information.
 * 
 * @author Benjamin Klatt
 * 
 */
public interface Extractor {

    // ////////////////////////////////
    // BUSINESS METHODS
    // ////////////////////////////////

    /**
     * Extract the setup for the service prediction out of a specific template and builder.
     * 
     * @param template
     *            the {@link SLATemplate}, representing the target service quality requirements
     * @param builder
     *            a {@link ServiceBuilder}, representing one concrete target service realization
     * @return The configuration object with the prediction relevant information.
     */
    PredictionModelModification extractServicePredictionSetup(SLATemplate template, ServiceBuilder builder);

    /**
     * Extract the cpu speed definition from an infrastructure SLA template.
     * 
     * @param slaTemplate
     *            The SLA template to read the value from.
     * @return The extracted value or null if none could be found.
     */
    SLATemplateValue extractCPUSpeed(SLATemplate slaTemplate);

    /**
     * Extract the cpu core definition from an infrastructure SLA template.
     * 
     * @param slaTemplate
     *            The SLA template to read the value from.
     * @return The extracted value or null if none could be found.
     */
    SLATemplateValue extractCPUCores(SLATemplate slaTemplate);

    /**
     * Extract the memory definition from an infrastructure SLA template.
     * 
     * @param slaTemplate
     *            The SLA template to read the value from.
     * @return The extracted value or null if none could be found.
     */
    SLATemplateValue extractMemory(SLATemplate slaTemplate);

    /**
     * Extract the availability definition from an infrastructure SLA template.
     * 
     * @param slaTemplate
     *            The SLA template to read the value from.
     * @return The extracted value or null if none could be found.
     */
    SLATemplateValue extractAvailability(SLATemplate slaTemplate);

    /**
     * Extract the usage scenario specifications from the SLA template.
     * 
     * @param template
     *            The SLA template to be analyzed.
     * @return The list of detected usage scenario specifications.
     */
    List<UsageScenarioSpecification> extractUsageScenarioSpecifications(SLATemplate template);

}
