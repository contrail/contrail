package org.slasoi.seval.prediction.configuration.impl;

import org.slasoi.seval.prediction.configuration.SystemCallVariable;

/**
 * Specification of a parameter that should be set in a system call.
 * 
 * @author Benjamin Klatt
 */
public class SystemCallVariableImpl implements SystemCallVariable {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The name for the system call. */
    private String name = null;

    /** The value of the system call. */
    private String value = null;

    // ////////////////////////////////
    // Constructor
    // ////////////////////////////////

    /**
     * The default constructor.
     */
    public SystemCallVariableImpl() {
        super();
    }

    /**
     * The enhanced constructor to set the name and value of the variable.
     * 
     * @param name
     *            The name of the variable.
     * @param value
     *            The value of the variable.
     */
    public SystemCallVariableImpl(final String name, final String value) {
        this.name = name;
        this.value = value;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.SystemCallVariable#getName()
     */
    public final String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.SystemCallVariable#setName(java.lang.String)
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.SystemCallVariable#getValue()
     */
    public final String getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.SystemCallVariable#setValue(java.lang.String)
     */
    public final void setValue(final String value) {
        this.value = value;
    }
}
