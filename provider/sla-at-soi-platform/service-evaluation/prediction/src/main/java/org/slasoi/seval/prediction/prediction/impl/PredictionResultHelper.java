package org.slasoi.seval.prediction.prediction.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type0;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type1;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.ReliabilitySimulationResult;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.ResponseTimeSimulationResult;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceResultStatus;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimulationResult;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.ISingleResult.AggregationType;
import org.slasoi.seval.prediction.service.impl.EvaluationResult;
import org.slasoi.seval.prediction.service.impl.SingleResult;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.vocab.common;

/**
 * This class provides auxiliary functionality to work with prediction results.
 * 
 * @author brosch, patejdl
 * 
 */
public final class PredictionResultHelper {

    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_99 = 99;
    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_95 = 95;
    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_90 = 90;
    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_75 = 75;
    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_50 = 50;
    /** Integer value of the percentile rate. */
    private static final int PERCENTILE_VALUE_25 = 25;
    /** Integer value of the percentile rate. */
    private static final double PERCENTILE_CONVERSION_RATE = 100.0;
    /**
     * A singleton pattern ensures the existence of only one result helper.
     */
    private static PredictionResultHelper singletonInstance = new PredictionResultHelper();

    /**
     * Retrieves the singleton instance.
     * 
     * @return the singleton instance
     */
    public static PredictionResultHelper getHelper() {
        return singletonInstance;
    }

    /**
     * The constructor is made private according to the singleton pattern.
     */
    private PredictionResultHelper() {
    }

    /**
     * Builds an evaluation result for an evaluated service realization.
     * 
     * @param status
     *            the results returned by the evaluation server
     * @return the created evaluation result
     */
    public IEvaluationResult buildPredictionResult(final SimuServiceResultStatus status) {
        IEvaluationResult resultSet = new EvaluationResult();
        ResponseTimeSimulationResult[] responseTimeResults = status.getResponseTimeResults();
        if (responseTimeResults != null) {
            for (ResponseTimeSimulationResult responseTimeResult : responseTimeResults) {
                resultSet.getResponseTimeResults().addAll(createSingleResults(responseTimeResult));
            }
        }
        ReliabilitySimulationResult[] reliabilityResults = status.getReliabilityResults();
        if (reliabilityResults != null) {
            for (ReliabilitySimulationResult reliabilityResult : reliabilityResults) {
                resultSet.getReliabilityResults().addAll(createSingleResults(reliabilityResult));
            }
        }
        resultSet.sort();
        return resultSet;
    }

    /**
     * Extracts the service operation ID from the given simulation result.
     * 
     * @param simResult
     *            the simulation result
     * @return the service operation ID
     */
    private String createServiceID(final SimulationResult simResult) {
        String serviceId = null;
        if (simResult.getSensorName().indexOf("<") > -1) {
            serviceId = simResult.getSensorName();
        } else {
            String[] parts = simResult.getSensorName().split(" ");
            serviceId = parts[parts.length - 1];
        }
        return serviceId;
    }

    /**
     * Creates single results for the given simulation result.
     * 
     * @param simResult
     *            the source simulation result.
     * @return The prepared single result.
     */
    private List<SingleResult> createSingleResults(final ResponseTimeSimulationResult simResult) {
        List<SingleResult> resultList = new LinkedList<SingleResult>();
        STND term = getResponseTimeTerm(simResult);
        String serviceID = createServiceID(simResult);
        for (Entry_type1 percentilesEntry : simResult.getPercentiles().getEntry()) {
            AggregationType aggregationType = getAggregationType(percentilesEntry.getKey());
            if (aggregationType != AggregationType.UNKNOWN && aggregationType != AggregationType.SUCCESS
                    && aggregationType != AggregationType.FAILURE) {
                resultList.add(new SingleResult(serviceID, term, aggregationType, percentilesEntry.getValue()));
            }
        }
        return resultList;
    }

    /**
     * Creates single results for the given reliability simulation result.
     * 
     * @param reliabilityResult
     *            the target evaluation result
     * @return The prepared single result
     */
    private List<SingleResult> createSingleResults(final ReliabilitySimulationResult reliabilityResult) {
        List<SingleResult> resultList = new LinkedList<SingleResult>();
        STND term = getReliabilityTerm(reliabilityResult);
        String serviceID = createServiceID(reliabilityResult);

        for (Entry_type0 frequencyEntry : reliabilityResult.getFrequencies().getEntry()) {
            AggregationType aggregationType = getAggregationType(frequencyEntry.getKey());
            if (aggregationType == AggregationType.SUCCESS || aggregationType == AggregationType.FAILURE) {
                resultList.add(new SingleResult(serviceID, term, aggregationType, frequencyEntry.getValue()));
            }
        }
        return resultList;
    }

    /**
     * Creates a standard QoS term for a given simulation result.
     * 
     * @param simResult
     *            the simulation result
     * @return the standard QoS term
     */
    private STND getResponseTimeTerm(final ResponseTimeSimulationResult simResult) {
        return common.completion_time;
    }

    /**
     * Creates a standard QoS term for a given simulation result.
     * 
     * @param simResult
     *            the simulation result
     * @return the standard QoS term
     */
    private STND getReliabilityTerm(final ReliabilitySimulationResult simResult) {
        return common.availability;
    }

    /**
     * Transforms a percentile key into an aggregation type.
     * 
     * @param percentileKey
     *            the percentile key
     * @return the aggregation type
     */
    private AggregationType getAggregationType(final double percentileKey) {
        int key = (int) Math.round(percentileKey * PERCENTILE_CONVERSION_RATE);
        switch (key) {
        case PERCENTILE_VALUE_25:
            return AggregationType.PERCENTILE_25;
        case PERCENTILE_VALUE_50:
            return AggregationType.PERCENTILE_50;
        case PERCENTILE_VALUE_75:
            return AggregationType.PERCENTILE_75;
        case PERCENTILE_VALUE_90:
            return AggregationType.PERCENTILE_90;
        case PERCENTILE_VALUE_95:
            return AggregationType.PERCENTILE_95;
        case PERCENTILE_VALUE_99:
            return AggregationType.PERCENTILE_99;
        default:
            return AggregationType.UNKNOWN;
        }

    }

    /**
     * Transforms a string key into an aggregation type.
     * 
     * @param failureResultKey
     *            the percentile key
     * @return the aggregation type
     */
    private AggregationType getAggregationType(final String failureResultKey) {

        if (failureResultKey.toLowerCase().indexOf("success") != -1) {
            return AggregationType.SUCCESS;
        } else if (failureResultKey.toLowerCase().indexOf("failure") != -1) {
            return AggregationType.FAILURE;
        }
        return AggregationType.UNKNOWN;
    }
}
