package org.slasoi.seval.prediction.extractor.impl;

import org.slasoi.seval.prediction.extractor.SLATemplateValue;

/**
 * An individual extracted value provided by the extractor.
 * 
 * @author Benjamin Klatt
 * 
 */
public class SLATemplateValueImpl implements SLATemplateValue {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The id of the value (i.e. VM_CPU_SPEED) */
    private String id = null;

    /** The extracted value itself. */
    private String value = null;

    /** The unit of the value (i.e. GHz) */
    private String unit = null;

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#getId()
     */
    public final String getId() {
        return id;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#setId(java.lang.String)
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#getValue()
     */
    public final String getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#setValue(java.lang.String)
     */
    public final void setValue(final String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#getUnit()
     */
    public final String getUnit() {
        return unit;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.extractor.SLATemplateValue#setUnit(java.lang.String)
     */
    public final void setUnit(final String unit) {
        this.unit = unit;
    }

}
