package org.slasoi.seval.prediction.extractor;

/**
 * A specific template value extracted from an slat.
 * @author Benjamin Klatt
 */
public interface SLATemplateValue {

    // ////////////////////////////////
    // CONSTANTS
    // ////////////////////////////////

    /** The identifier for the cpu speed to be used within an SLA template. */
    String ID_CPU_SPEED = "CPU_SPEED_VALUE";

    /** The identifier for the cpu speed to be used within an SLA template. */
    String ID_CPU_CORES = "CPU_CORES_VALUE";

    /** The identifier for the cpu speed to be used within an SLA template. */
    String ID_MEMORY = "MEMORY_VALUE";

    /** The identifier for the MTTR (mean time to failure) to be used within an SLA template. */
    String ID_AVAILABILITY = "Overall_Availability_VAR";

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * Get the id of the value (i.e. VM_CPU_SPEED).
     * 
     * @return The id.
     */
    String getId();

    /**
     * Set the id of the value (i.e. VM_CPU_SPEED).
     * 
     * @param id
     *            The id to be set.
     */
    void setId(String id);

    /**
     * Get the extracted value itself.
     * 
     * @return The value as string.
     */
    String getValue();

    /**
     * Set the extracted value itself.
     * 
     * @param value
     *            The value to be set.
     */
    void setValue(String value);

    /**
     * Get the unit of the value (i.e. GHz).
     * 
     * @return The unit.
     */
    String getUnit();

    /**
     * Set the unit of the value (i.e. GHz).
     * 
     * @param unit
     *            The unit to be set.
     */
    void setUnit(String unit);

}
