package org.slasoi.seval.prediction.configuration.impl;

import org.slasoi.seval.prediction.configuration.ResourceContainerSpecification;
import org.slasoi.seval.prediction.extractor.SLATemplateValue;

/**
 * Parameter class with the settings for one specific resource container.
 * 
 * @author Benjamin Klatt
 * 
 */
public class ResourceContainerSpecificationImpl implements ResourceContainerSpecification {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The identifier for the container. */
    private String containerID = null;

    /** The definition of the containers cpu speed. */
    private SLATemplateValue cpuSpeed = null;

    /** The definition of the containers number of cpu cores. */
    private SLATemplateValue cpuCores = null;

    /** The definition of the containers memory amount. */
    private SLATemplateValue memory = null;

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     */
    public final String getContainerID() {
        return containerID;
    }

    /**
     * {@inheritDoc}
     */
    public final void setContainerID(final String containerID) {
        this.containerID = containerID;
    }

    /**
     * {@inheritDoc}
     */
    public final SLATemplateValue getCpuSpeed() {
        return cpuSpeed;
    }

    /**
     * {@inheritDoc}
     */
    public final void setCpuSpeed(final SLATemplateValue cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    /**
     * {@inheritDoc}
     */
    public final SLATemplateValue getCpuCores() {
        return cpuCores;
    }

    /**
     * {@inheritDoc}
     */
    public final void setCpuCores(final SLATemplateValue cpuCores) {
        this.cpuCores = cpuCores;
    }

    /**
     * {@inheritDoc}
     */
    public final SLATemplateValue getMemory() {
        return memory;
    }

    /**
     * {@inheritDoc}
     */
    public final void setMemory(final SLATemplateValue memory) {
        this.memory = memory;
    }
}
