package org.slasoi.seval.prediction.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.exceptions.MissingSLATemplateException;
import org.slasoi.seval.prediction.exceptions.UnboundDependencyException;
import org.slasoi.seval.prediction.extractor.Extractor;
import org.slasoi.seval.prediction.extractor.impl.ExtractorImpl;
import org.slasoi.seval.prediction.modeladjustment.ModelAdjustment;
import org.slasoi.seval.prediction.modeladjustment.impl.ModelAdjustmentImpl;
import org.slasoi.seval.prediction.prediction.Prediction;
import org.slasoi.seval.prediction.prediction.impl.PredictionImpl;
import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.ISoftwareServiceEvaluation;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.prediction.service.settings.impl.EvaluationSettingsImpl;
import org.slasoi.seval.repository.DesignTimeRepository;
import org.slasoi.seval.repository.Repository;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * <p>
 * Software-specific implementation of the service evaluator. Provides the functionality for interaction
 * <tt>&laquo;evaluate&raquo;</tt> between POC and SSE.
 * </p>
 * 
 * @author kuester, brosch, klatt
 * @see org.slasoi.seval.prediction.service.IServiceEvaluation#evaluate(Set, SLATemplate)
 */
public final class SoftwareServiceEvaluator implements ISoftwareServiceEvaluation {

    /**
     * Determines the endpoint where the evaluation server is expected to run.
     */
    private String evaluationServerEndpoint = "http://localhost:8082/services";

    /**
     * Determines the evaluation mode of the service evaluator.
     */
    private EvaluationMode evaluationMode = EvaluationMode.Auto;

    /** The extractor to get sla specifications from the prediction request. */
    private Extractor extractor = null;

    /** The repository to load the models from. */
    private Repository repository = null;

    /** The model adjustment to use to manipulate the software models. */
    private ModelAdjustment modelAdjustment = null;

    /** The prediction component to be used. */
    private Prediction prediction = null;

    /**
     * Constructor initializing a default service setup.
     */
    public SoftwareServiceEvaluator() {
        this.extractor = new ExtractorImpl();
        this.repository = DesignTimeRepository.getRepo();
        this.modelAdjustment = new ModelAdjustmentImpl(this.repository);
        this.prediction = PredictionImpl.getInstance();
    }

    /**
     * This method evaluates expected quality properties (performance and reliability) of software services.
     * 
     * <h1>Rationale</h1>
     * <p>
     * The central method for access to the WP A6 design time prediction component. When called with a set of
     * {@link ServiceBuilder}s the different realizations are considered as options for the same {@link SLATemplate}
     * passed as second argument. The method evaluates QoS properties of the specified system under consideration and
     * returns, for each realization, an {@link IEvaluationResult}.
     * </p>
     * <h1>Example</h1>
     * <p>
     * Given a service called <b>FooService</b> (for which a design-time model be present) with interface method
     * <b>IFoo.doIt()</b> and the SLA(T) containing an agreement term like <blockquote>
     * <tt><b>mean(completion_time(doIt) &lt; 100 ms</b></tt></blockquote> the realizations are fed into the model
     * assembly and based on this, the prediction service returns the expected mean completion time for method
     * <b>doIt()</b>. This is represented as an {@link IAggregateResult} having an arbitrary number of
     * {@link ISingleResult}s mapping the term {@link common#completion_time} to the respective predicted values.
     * </p>
     * <p>
     * This method can return one of the following unchecked exceptions:
     * <ul>
     * <li>UnsupportedTermException - In case no QoS terms are part of the agreement</li>
     * <li>UnboundDependencyException- In case there are unbound dependencies (see
     * {@link ServiceBuilder#getOpenDependencies()}).</li>
     * </ul>
     * </p>
     * 
     * @param builders
     *            A set containing at least one builder describing an implementation
     * @param template
     *            The {@link SLATemplate} associated with the service to be evaluated.
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return set of {@link IEvaluationResult}s. Contains one evaluation result for each service realization.
     * @throws ModelNotFoundException
     *             in case the design time repository does not hold the required QoS model
     */
    public Set<IEvaluationResult> evaluate(final Set<ServiceBuilder> builders, final SLATemplate template,
            final EvaluationSettings settings) throws ModelNotFoundException {

        validate(builders);
        Set<IEvaluationResult> results = new HashSet<IEvaluationResult>();

        for (ServiceBuilder builder : builders) {

            // extract the service prediction setup
            PredictionModelModification servicePredictionSetup =
                    extractor.extractServicePredictionSetup(template, builder);

            System.out.println("Prediction Setup extracted: "
                    + servicePredictionSetup.getResourceContainerSpecifications().get(0).getCpuSpeed());

            // load the software model
            String modelId = builder.getImplementation().getServiceImplementationName();

            // trigger the core evaluation process
            IEvaluationResult evaluationResult = evaluate(modelId, servicePredictionSetup, settings);

            // enhance and return the results
            evaluationResult.setBuilder(builder);
            results.add(evaluationResult);
        }

        return results;
    }

    /**
     * This method evaluates expected quality properties (performance and reliability) of software services. It makes
     * use of default settings for the EvaluationSettings object.
     * <p>
     * This method can return one of the following unchecked exceptions:
     * <ul>
     * <li>UnsupportedTermException - In case no QoS terms are part of the agreement</li>
     * <li>UnboundDependencyException- In case there are unbound dependencies (see
     * {@link ServiceBuilder#getOpenDependencies()}).</li>
     * </ul>
     * </p>
     * 
     * @param builders
     *            A set containing at least one builder
     * @param template
     *            The {@link SLATemplate} associated with the service to be evaluated.
     * @return set of {@link IEvaluationResult}s. Contains one evaluation result for each service realization.
     * @throws ModelNotFoundException
     *             in case the design time repository does not hold the required QoS model
     */
    public Set<IEvaluationResult> evaluate(final Set<ServiceBuilder> builders, final SLATemplate template)
            throws ModelNotFoundException {
        return this.evaluate(builders, template, new EvaluationSettingsImpl());
    }

    /**
     * Evaluate a quality prediction model identified by a model id and configured by a service prediction setup object.
     * The prediction setup configuration enables the partial adjustment of the originally registered software model
     * with request specific attributes.
     * 
     * @param modelId
     *            The id of the model to predict.
     * @param predictionModelModification
     *            The parameters how to modify the quality model before the prediction.
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return An evaluation result object.
     * @throws ModelNotFoundException
     *             An exception that no model could be found for the provided modelid
     */
    public IEvaluationResult evaluate(final String modelId,
            final PredictionModelModification predictionModelModification, final EvaluationSettings settings)
            throws ModelNotFoundException {

        // Load the model
        SoftwareModel originalSoftwareModel = repository.query(modelId);
        if (originalSoftwareModel == null) {
            throw new ModelNotFoundException(modelId);
        }

        // adjust the software model
        SoftwareModel adjustedSoftwareModel =
                modelAdjustment.adjustModel(originalSoftwareModel, predictionModelModification);

        // trigger the prediction and register the result
        IEvaluationResult evaluationResult =
                prediction.predict(adjustedSoftwareModel, evaluationMode, settings, evaluationServerEndpoint);
        return evaluationResult;
    }

    /**
     * Evaluate a quality prediction model identified by a model id and configured by a service prediction setup object.
     * The prediction setup configuration enables the partial adjustment of the originally registered software model
     * with request specific attributes.
     * 
     * @param modelId
     *            The id of the model to predict.
     * @param predictionModelModificationMap
     *            A map of parameter objects how to modify the quality model before the prediction.
     * @param settings
     *            The {@link EvaluationSettings} containing settings for the evaluation.
     * @return The evaluation results for the requested prediction model modifications.
     * @throws ModelNotFoundException
     *             An exception that no model could be found for the provided modelid
     */
    public Map<Object, IEvaluationResult> evaluate(final String modelId,
            final Map<Object, PredictionModelModification> predictionModelModificationMap,
            final EvaluationSettings settings) throws ModelNotFoundException {
        Map<Object, IEvaluationResult> resultMap = new HashMap<Object, IEvaluationResult>();

        for (Object key : predictionModelModificationMap.keySet()) {
            resultMap.put(key, evaluate(modelId, predictionModelModificationMap.get(key), settings));
        }

        return resultMap;
    }

    /**
     * Tests a set of {@link ServiceBuilder}s for being valid, i.e. being non-empty and having no unbound service
     * dependencies.
     * <p>
     * This method can return one of the following unchecked exceptions:
     * <ul>
     * <li>IllegalArgumentException - in case the set is empty or contains NULL elements.</li>
     * </ul>
     * </p>
     * 
     * @param builders
     *            the set of {@link ServiceBuilder}s to validate
     */
    public void validate(final Set<ServiceBuilder> builders) {
        if (builders == null) {
            throw new IllegalArgumentException("set of builders must not be null");
        }
        for (ServiceBuilder b : builders) {
            if (b == null) {
                throw new IllegalArgumentException("builder must not be null");
            }
            if (!b.getOpenDependencies().isEmpty()) {
                Dependency openDep = (Dependency) b.getOpenDependencies().iterator().next();
                throw new UnboundDependencyException(b, openDep);
            }
            for (ServiceBinding binding : b.getBindings()) {
                if (binding.getSlaTemplate() == null) {
                    throw new MissingSLATemplateException(b, binding);
                }
            }
        }
    }

    /**
     * Retrieves the endpoint where the evaluation server is expected to run.
     * 
     * @return the evaluation server endpoint.
     */
    public String getEvaluationServerEndpoint() {
        return evaluationServerEndpoint;
    }

    /**
     * Sets the endpoint of the evaluation web server.
     * 
     * @param evaluationServerEndpoint
     *            the endpoint to communicate with
     */
    public void setEvaluationServerEndpoint(final String evaluationServerEndpoint) {
        this.evaluationServerEndpoint = evaluationServerEndpoint;
    }

    /**
     * Retrieves the evaluation mode of the service evaluator.
     * 
     * @return the evaluation mode
     */
    public EvaluationMode getEvaluationMode() {
        return evaluationMode;
    }

    /**
     * Sets the evaluation mode of the service evaluator.
     * 
     * @param evaluationMode
     *            the evaluation mode
     */
    public void setEvaluationMode(final EvaluationMode evaluationMode) {
        this.evaluationMode = evaluationMode;
    }

    /**
     * Set the extractor to be used.
     * 
     * @param extractor
     *            The extractor to be used.
     */
    public void setExtractor(final ExtractorImpl extractor) {
        this.extractor = extractor;
    }

    /**
     * Set the model repository to work with.
     * 
     * @param repository
     *            The repository.
     */
    public void setRepository(final Repository repository) {
        this.repository = repository;
    }

    /**
     * Set the model adjustment component to work with.
     * 
     * @param modelAdjustment
     *            The component to be set.
     */
    public void setModelAdjustment(final ModelAdjustment modelAdjustment) {
        this.modelAdjustment = modelAdjustment;
    }

}
