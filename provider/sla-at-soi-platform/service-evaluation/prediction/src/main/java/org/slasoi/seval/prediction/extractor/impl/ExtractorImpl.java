package org.slasoi.seval.prediction.extractor.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.seval.prediction.configuration.ResourceContainerSpecification;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.seval.prediction.configuration.impl.ResourceContainerSpecificationImpl;
import org.slasoi.seval.prediction.configuration.impl.PredictionModelModificationImpl;
import org.slasoi.seval.prediction.configuration.impl.UsageScenarioSpecifationImpl;
import org.slasoi.seval.prediction.exceptions.SLATemplateEvaluationException;
import org.slasoi.seval.prediction.extractor.Extractor;
import org.slasoi.seval.prediction.extractor.SLATemplateValue;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;

/**
 * Extractor to get prediction relevant information out of sla information. This extractor takes use of manual
 * investigation into the builder and template objects.
 * 
 * @author Benjamin Klatt
 * @author kuester
 */
public class ExtractorImpl implements Extractor {

    // ////////////////////////////////
    // CONSTANTS
    // ////////////////////////////////

    /** LOGGER. **/
    private static final Logger LOGGER = Logger.getLogger(ExtractorImpl.class);

    /** The identifier of the agreement to search for cpu speed. */
    private static final String ID_AGREEMENT_TERM_SYSTEM_CONFIGURATION = "all-in-one";

    /** The identifier of the agreement to search for cpu speed. */
    private static final String ID_AGREEMENT_TERM_SYSTEM_AVAILABILITY = "OverAllAvailability";

    /** The identifier for the data type identifying a service usage. */
    private static final String USAGE_DATA_TYPE = "http://www.slaatsoi.org/coremodel/units#tx_per_s";

    // ////////////////////////////////
    // BUSINESS METHODS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     */
    public final PredictionModelModification extractServicePredictionSetup(final SLATemplate template,
            final ServiceBuilder builder) {

        PredictionModelModification predictionSetup = new PredictionModelModificationImpl();

        // process resource container
        List<ResourceContainerSpecification> containerSpecifications = extractResourceContainerSpecification(builder);
        predictionSetup.getResourceContainerSpecifications().addAll(containerSpecifications);

        // process the usage profiles
        List<UsageScenarioSpecification> usageSpecifications = extractUsageScenarioSpecifications(template);
        predictionSetup.getUsageScenarioSpecifications().addAll(usageSpecifications);

        return predictionSetup;
    }

    /**
     * Extract all resource container specifications out of the dependencies and bindings from a builder.
     * 
     * @param builder
     *            The builder to extract the specifications from.
     * @return The list of resource container specifications.
     */
    private List<ResourceContainerSpecification> extractResourceContainerSpecification(final ServiceBuilder builder) {

        List<ResourceContainerSpecification> containerSpecifications = new LinkedList<ResourceContainerSpecification>();

        Map<Dependency, ServiceBinding> bindingsMap = buildBindingRegistry(builder);

        // process all dependencies
        ServiceImplementation implementation = builder.getImplementation();
        for (ImplementationArtefact artefact : implementation.getArtefacts()) {

            for (Dependency dependency : artefact.getDependencies()) {
                ServiceBinding binding = bindingsMap.get(dependency);
                ResourceContainerSpecification specification =
                        buildResourceContainerSpecification(dependency, binding.getSlaTemplate());
                containerSpecifications.add(specification);
            }
        }
        return containerSpecifications;
    }

    /**
     * {@inheritDoc}
     */
    public final List<UsageScenarioSpecification> extractUsageScenarioSpecifications(final SLATemplate template) {

        List<UsageScenarioSpecification> specifications = new LinkedList<UsageScenarioSpecification>();

        // Check all agreement terms for usage scenario specifications
        for (AgreementTerm agreementTerm : template.getAgreementTerms()) {

            // check the variable declarations for the one that defines a usage load
            for (VariableDeclr variableDeclr : agreementTerm.getVariableDeclrs()) {
                Customisable customisable = (Customisable) variableDeclr;
                STND stnd = customisable.getValue().getDatatype();
                if (USAGE_DATA_TYPE.equals(stnd.getValue())) {

                    String agreementId = agreementTerm.getId().getValue();
                    UsageScenarioSpecification specification =
                            buildUsageScenarioSpecification(template, agreementId, customisable);

                    if (specification != null) {
                        specifications.add(specification);
                    }
                }
            }
        }

        return specifications;
    }

    /**
     * Build the usage scenario specification based on a variable declaration.
     * 
     * @param template
     *            The SLA template to extract the data from.
     * @param agreementTermId
     *            The id of the agreement term.
     * @param customisable
     *            The customisable object to get the value from.
     * 
     * @return The prepared usage scenario specification. Or null if the specification could not be extracted correctly.
     *         A warning will be created in the logs in the latter case.
     */
    private UsageScenarioSpecification buildUsageScenarioSpecification(final SLATemplate template,
            final String agreementTermId, final Customisable customisable) {
        // initialize the specification
        UsageScenarioSpecification specification = null;

        try {
            // identify variable
            String variableId = customisable.getVar().getValue();

            // build properties
            String usageScenarioID = agreementTermId;
            SLATemplateValue templateValue = extractValue(template, agreementTermId, variableId);
            String arrivalRate = templateValue.getValue();

            // build specification
            specification = new UsageScenarioSpecifationImpl();
            specification.setUsageScenarioID(usageScenarioID);
            specification.setInterArrivalTime(arrivalRate);

        } catch (NullPointerException npe) {
            LOGGER.error("Usage Scenario could not be extracted correctly from agreement term " + agreementTermId);
        }

        // return the specification
        return specification;
    }

    /**
     * Build up a resource container specification for a dependency and an SLA template.
     * 
     * @param dependency
     *            The dependency to analyze.
     * @param slaTemplate
     *            The SLA template to analyze.
     * @return The prepared resource container specification.
     */
    public final ResourceContainerSpecification buildResourceContainerSpecification(final Dependency dependency,
            final SLATemplate slaTemplate) {

        // extract the necessary sla information
        SLATemplateValue cpuSpeed = extractCPUSpeed(slaTemplate);
        SLATemplateValue cpuCores = extractCPUCores(slaTemplate);
        SLATemplateValue memory = extractMemory(slaTemplate);

        // build the specification
        ResourceContainerSpecification specification = new ResourceContainerSpecificationImpl();
        specification.setContainerID(dependency.getName());
        specification.setCpuSpeed(cpuSpeed);
        specification.setCpuCores(cpuCores);
        specification.setMemory(memory);

        return specification;
    }

    /**
     * Build a registry that links all dependencies with the binding pointing to it. If there is a dependency without a
     * binding, there will be no entry for it in this registry.
     * 
     * @param builder
     *            The builder to read the bindings from.
     * @return The prepared registry with associations between dependencies and the binding that points to it.
     */
    private Map<Dependency, ServiceBinding> buildBindingRegistry(final ServiceBuilder builder) {
        Map<Dependency, ServiceBinding> registry = new HashMap<Dependency, ServiceBinding>();
        for (ServiceBinding binding : builder.getBindings()) {
            registry.put(binding.getDependency(), binding);
        }
        return registry;
    }

    /**
     * Extract the cpu_speed from an SLA template. This implementation checks for variable declarations with the name
     * CPU_SPEED.
     * 
     * @param slaTemplate
     *            The sla template to scan.
     * @return The value object or null if the value could not be found.
     */
    public final SLATemplateValue extractCPUSpeed(final SLATemplate slaTemplate) {
        return extractValue(slaTemplate, ID_AGREEMENT_TERM_SYSTEM_CONFIGURATION, SLATemplateValue.ID_CPU_SPEED);
    }

    /**
     * Extract the cpu_speed from an SLA template. This implementation checks for variable declarations with the name
     * CPU_SPEED.
     * 
     * @param slaTemplate
     *            The sla template to scan.
     * @return The value object or null if the value could not be found.
     */
    public final SLATemplateValue extractCPUCores(final SLATemplate slaTemplate) {
        return extractValue(slaTemplate, ID_AGREEMENT_TERM_SYSTEM_CONFIGURATION, SLATemplateValue.ID_CPU_CORES);
    }

    /**
     * Extract the memory from an SLA template. This implementation checks for variable declarations with the name
     * MEMORY.
     * 
     * @param slaTemplate
     *            The sla template to scan.
     * @return The value object or null if the value could not be found.
     */
    public final SLATemplateValue extractMemory(final SLATemplate slaTemplate) {
        return extractValue(slaTemplate, ID_AGREEMENT_TERM_SYSTEM_CONFIGURATION, SLATemplateValue.ID_MEMORY);
    }

    /**
     * Extract the availability from an SLA template. This implementation checks for variable declarations with the name
     * MTTF.
     * 
     * @param slaTemplate
     *            The SLA template to scan.
     * @return The value object or null if the value could not be found.
     */
    public final SLATemplateValue extractAvailability(final SLATemplate slaTemplate) {
        return extractValue(slaTemplate, ID_AGREEMENT_TERM_SYSTEM_AVAILABILITY, SLATemplateValue.ID_AVAILABILITY);
    }

    /**
     * Extract a value from an sla template identified by the ids of the agreement term and the variable.
     * 
     * @param slaTemplate
     *            The sla template to scan.
     * @param agreementId
     *            The id of the agreement term to look for.
     * @param valueId
     *            The id of the value to look for.
     * @return The value object or null if the value could not be found.
     */
    private SLATemplateValue extractValue(final SLATemplate slaTemplate, final String agreementId,
            final String valueId) {

        String valueString = null;
        try {
            // get the variable declaration
            AgreementTerm agreement = slaTemplate.getAgreementTerm(agreementId);
            if (agreement == null) {
                throw new SLATemplateEvaluationException("Required Agreement Term [" + agreementId
                        + "] does not exist in SLA Template!");
            }
            VariableDeclr declaration = agreement.getVariableDeclr(valueId);
            if (declaration == null || !(declaration instanceof Customisable)) {
                throw new SLATemplateEvaluationException("Variable declaration in Agreement Term [" + agreementId
                        + "] does not exist in SLA Template!");
            }
            Customisable customisable = (Customisable) declaration;
            valueString = customisable.getValue().getValue();

        } catch (SLATemplateEvaluationException e) {

            LOGGER.error("Expected SLA Template variable is missing." + e.getMessage());

            if (SLATemplateValue.ID_CPU_CORES.equals(valueId)) {
                valueString = "1";
            } else if (SLATemplateValue.ID_CPU_SPEED.equals(valueId)) {
                valueString = "1.5";
            } else if (SLATemplateValue.ID_MEMORY.equals(valueId)) {
                valueString = "1024";
            } else if (SLATemplateValue.ID_AVAILABILITY.equals(valueId)) {
                valueString = "70";
            }
        }

        // build the value
        SLATemplateValue value = new SLATemplateValueImpl();
        value.setId(valueId);
        value.setValue(valueString);

        return value;
    }

}
