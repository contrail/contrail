/**
 * Interface of the prediction subcomponent of the software service evaluation component.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.service;

