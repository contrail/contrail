/**
 * The extractor sub-component responsible to extract the prediction relevant information from the SLAs.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.extractor;

