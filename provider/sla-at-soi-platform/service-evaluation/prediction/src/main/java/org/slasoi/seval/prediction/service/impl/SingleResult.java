package org.slasoi.seval.prediction.service.impl;

import org.slasoi.seval.prediction.service.ISingleResult;
import org.slasoi.slamodel.primitives.STND;

/**
 * Represents a single result within an evaluation result.
 * 
 * @author kuester, brosch
 * 
 */
public class SingleResult implements ISingleResult {

    /**
     * Further specification of the result type.
     */
    private AggregationType aggregationType;

    /**
     * The standard QoS term.
     */
    private STND term;

    /**
     * The result value.
     */
    private double value;

    /**
     * The service operation ID.
     */
    private String serviceID;

    /**
     * The constructor.
     * 
     * @param serviceID
     *            the standard QoS term
     * @param term
     *            the result type
     * @param aggregationType
     *            the service operation ID
     * @param value
     *            the result value
     */
    public SingleResult(final String serviceID, final STND term, final AggregationType aggregationType,
            final double value) {
        this.term = term;
        this.value = value;
        this.aggregationType = aggregationType;
        this.serviceID = serviceID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.ISingleResult#getAggregationType()
     */
    public AggregationType getAggregationType() {
        return aggregationType;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.common.ISimpleResult#getTerm()
     */
    public STND getTerm() {
        return term;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.common.ISimpleResult#getValue()
     */
    public double getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "[" + this.serviceID + "; " + this.getTerm().getValue() + "; " + this.aggregationType.toString()
                + "; " + this.getValue() + "]";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.ISingleResult#getServiceID()
     */
    public String getServiceID() {
        return serviceID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(final ISingleResult o) {
        if (!this.serviceID.equals(o.getServiceID())) {
            return this.serviceID.compareTo(o.getServiceID());
        } else if (!this.term.getValue().equals(o.getTerm().getValue())) {
            return this.term.getValue().compareTo(o.getTerm().getValue());
        } else {
            return this.aggregationType.compareTo(o.getAggregationType());
        }
    }

}
