/**
 * Interface of the sub component to manage the prediction itself.
 * This takes care for the communication with the remote prediction server.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.prediction;

