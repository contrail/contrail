package org.slasoi.seval.prediction.configuration;

import java.util.List;

/**
 * A setup that describes a specific service setup that should be predicted. An
 * instance of this class contains the prediction relevant information provided
 * to the prediction component.
 *
 * @author Benjamin Klatt
 *
 */
public interface PredictionModelModification {

    /**
     * Get the resource container specifications of the service setup for the
     * prediction.
     *
     * @return The available resource container specifications.
     */
    List<ResourceContainerSpecification> getResourceContainerSpecifications();

    /**
     * Get the usage scenario specifications of the service setup for the
     * prediction.
     *
     * @return the usageScenarioSpecifications
     */
    List<UsageScenarioSpecification> getUsageScenarioSpecifications();

    /**
     * Get the component specifications.
     *
     * @return The list of component specifications.
     */
    List<ComponentSpecification> getComponentSpecifications();

}
