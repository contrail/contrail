package org.slasoi.seval.prediction.exceptions;

import org.slasoi.slamodel.sla.SLATemplate;

/**
 * This exception is thrown from service evaluation if the service to be evaluated does not contain any terms that are
 * QoS related and can be predicted via design time prediction.
 * 
 * @author kuester
 * 
 */
public class UnsupportedTermException extends IllegalArgumentException {

    /**
     * A serial version UID for the serializable class.
     */
    private static final long serialVersionUID = 2671198260689699470L;

    /**
     * Constructor requiring a reference to the template this exception is about.
     * @param template The template to reference.
     */
    public UnsupportedTermException(final SLATemplate template) {
        super("Template " + template.getUuid() + "Does not contain supported QoS agreements");
    }

}
