package org.slasoi.seval.prediction.service;

import org.slasoi.slamodel.primitives.STND;

/**
 * Represents a single result as part of an evaluation result.
 * 
 * @author kuester, brosch
 */
public interface ISingleResult extends Comparable<ISingleResult> {

    /**
     * Represents an aggregation type for a single result.
     * 
     * This is a temporary solution. In the long term, the aggregation type must determined in terms of the SLA model.
     * 
     * @author kuester
     * 
     */
    public enum AggregationType {
        /** Aggregation type representing the maximum value. */
        MAX,
        /** Aggregation type calculating the mean value. */
        MEAN,
        /** Aggregation type calculating the median value. */
        MEDIAN,
        /** Aggregation type representing the minimum value. */
        MIN,
        /** Aggregation type calculating the 25% percentile. */
        PERCENTILE_25,
        /** Aggregation type calculating the 50% percentile. */
        PERCENTILE_50,
        /** Aggregation type calculating the 75% percentile. */
        PERCENTILE_75,
        /** Aggregation type calculating the 90% percentile. */
        PERCENTILE_90,
        /** Aggregation type calculating the 95% percentile. */
        PERCENTILE_95,
        /** Aggregation type calculating the 99% percentile. */
        PERCENTILE_99,
        /** Aggregation type calculating the standard deviation. */
        STDDEV,
        /** Aggregation type representing success full service requests. */
        SUCCESS,
        /** Aggregation type representing failed service requests. */
        FAILURE,
        /** An unknown type of aggregation. */
        UNKNOWN,
    }

    /**
     * Retrieves the aggregation type of the result.
     * 
     * @return the aggregation type.
     */
    AggregationType getAggregationType();

    /**
     * Retrieves the service operation ID.
     * 
     * @return the service operation ID
     */
    String getServiceID();

    /**
     * Retrieves the standard QoS term that describes the type of the result.
     * 
     * @return the standard QoS term
     */
    STND getTerm();

    /**
     * Retrieves the value of the result.
     * 
     * @return the result value
     */
    double getValue();
}
