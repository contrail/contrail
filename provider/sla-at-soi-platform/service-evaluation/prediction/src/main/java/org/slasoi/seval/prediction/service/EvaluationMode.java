package org.slasoi.seval.prediction.service;

/**
 * Represents different modes of operation of the service evaluator.
 * 
 * @author brosch
 * 
 */
public enum EvaluationMode {

    /**
     * Delivers only mock-up results; no real evaluation.
     */
    Mockup,

    /**
     * Enforces real evaluation using a running evaluation server.
     */
    Server,

    /**
     * Delivers real evaluation if an evaluation server is running, and mock-up results if not.
     */
    Auto,
}
