package org.slasoi.seval.prediction.configuration;

import java.util.List;

/**
 * This component object identifies a component model element in the prediction
 * model that should be adjusted.
 *
 * @author Benjamin Klatt
 *
 */
public interface ComponentSpecification {

    /**
     * Get the name of the component this specification is about.
     *
     * @return Get the name of the component.
     */
    String getName();

    /**
     * Set the name of the component.
     *
     * @param name
     *            The name to be set.
     */
    void setName(String name);

    /**
     * Get the component parameters to be specified.
     *
     * @return The list of component parameter specifications.
     */
    List<ComponentParameter> getParameters();

}
