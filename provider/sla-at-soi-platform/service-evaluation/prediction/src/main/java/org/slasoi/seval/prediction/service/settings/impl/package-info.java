/**
 * This package contains the default implementation of the prediction sub-component's settings definition.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.service.settings.impl;

