package org.slasoi.seval.prediction.exceptions;

import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;

/**
 * Exception thrown if a service builder contains a binding without a quality description (i.e. an SLATemplate).
 * 
 * @author brosch
 */
public class MissingSLATemplateException extends IllegalArgumentException {

    /**
     * A serial version UID for the serializable class.
     */
    private static final long serialVersionUID = 3692385222262014193L;

    /**
     * To be thrown when a dependency needed is not bound.
     * 
     * @param builder
     *            the service builder
     * @param binding
     *            the service binding
     */
    public MissingSLATemplateException(final ServiceBuilder builder, final ServiceBinding binding) {
        super("Service binding [" + binding + "] of service builder [" + builder
                + "] does not refer to an SLATemplate!");
    }
}
