/**
 * This package contains the interface definition of the prediction sub-component's settings definition.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.service.settings;

