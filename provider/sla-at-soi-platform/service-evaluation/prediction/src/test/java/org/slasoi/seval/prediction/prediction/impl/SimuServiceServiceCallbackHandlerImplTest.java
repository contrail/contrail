package org.slasoi.seval.prediction.prediction.impl;

import org.junit.Test;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimulateResponse;

public class SimuServiceServiceCallbackHandlerImplTest {

    @Test
    public void testGetResult() {
        SimuServiceServiceCallbackHandlerImpl handler = new SimuServiceServiceCallbackHandlerImpl();
        handler.receiveResultsimulate(new SimulateResponse());
        handler.getResult();
    }

    @Test
    public void testReceiveErrorsimulateException() {
        SimuServiceServiceCallbackHandlerImpl handler = new SimuServiceServiceCallbackHandlerImpl();
        handler.receiveResultsimulate(new SimulateResponse());
        handler.receiveErrorsimulate(new Exception());
    }

    @Test
    public void testReceiveResultsimulateSimulateResponse() {
        SimuServiceServiceCallbackHandlerImpl handler = new SimuServiceServiceCallbackHandlerImpl();
        handler.receiveResultsimulate(new SimulateResponse());
    }

}
