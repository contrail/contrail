package org.slasoi.seval.prediction.adjustment.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.EList;
import org.junit.Test;
import org.slasoi.seval.prediction.configuration.ComponentSpecification;
import org.slasoi.seval.prediction.configuration.ResourceContainerSpecification;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.SystemCall;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.seval.prediction.configuration.impl.ComponentParameterImpl;
import org.slasoi.seval.prediction.configuration.impl.ComponentSpecificationImpl;
import org.slasoi.seval.prediction.configuration.impl.ResourceContainerSpecificationImpl;
import org.slasoi.seval.prediction.configuration.impl.PredictionModelModificationImpl;
import org.slasoi.seval.prediction.configuration.impl.SystemCallImpl;
import org.slasoi.seval.prediction.configuration.impl.SystemCallVariableImpl;
import org.slasoi.seval.prediction.configuration.impl.UsageScenarioSpecifationImpl;
import org.slasoi.seval.prediction.extractor.SLATemplateValue;
import org.slasoi.seval.prediction.extractor.impl.SLATemplateValueImpl;
import org.slasoi.seval.prediction.modeladjustment.ModelAdjustment;
import org.slasoi.seval.prediction.modeladjustment.impl.ModelAdjustmentImpl;
import org.slasoi.seval.prediction.test.PredictionTestUtil;
import org.slasoi.seval.repository.DesignTimeRepository;
import org.slasoi.seval.repository.Repository;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.seval.repository.container.ComponentModelContainer;
import org.slasoi.seval.repository.container.InfrastructureModelContainer;
import org.slasoi.seval.repository.container.UsageModelContainer;

import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisation;
import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisationType;
import de.uka.ipd.sdq.pcm.parameter.VariableUsage;
import de.uka.ipd.sdq.pcm.repository.BasicComponent;
import de.uka.ipd.sdq.pcm.resourceenvironment.ProcessingResourceSpecification;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceEnvironment;
import de.uka.ipd.sdq.pcm.resourcetype.ProcessingResourceType;
import de.uka.ipd.sdq.pcm.usagemodel.EntryLevelSystemCall;
import de.uka.ipd.sdq.pcm.usagemodel.OpenWorkload;
import de.uka.ipd.sdq.pcm.usagemodel.ScenarioBehaviour;
import de.uka.ipd.sdq.pcm.usagemodel.UsageModel;
import de.uka.ipd.sdq.pcm.usagemodel.UsageScenario;
import de.uka.ipd.sdq.pcm.usagemodel.Workload;

/**
 * @author Benjamin KLatt
 * 
 */
public class ModelAdjustmentTest extends TestCase {

    // ////////////////////////////////
    // TEST METHODS
    // ////////////////////////////////

    /**
     * Test the adjustment of a resource container in the software model.
     * 
     * @throws IOException
     *             Exception that the test model could not be loaded
     */
    @Test
    public void testResourceContainerAdjustment() throws IOException {

        // get the software model from the repository
        Repository repository = DesignTimeRepository.getRepo();
        SoftwareModel sourceModel = repository.query("ORC_AllInOne");

        // build setup
        PredictionModelModification servicePredictionSetup = initServicePredictionSetup();

        // trigger model adjustment
        ModelAdjustment modelAdjustment = new ModelAdjustmentImpl(repository);
        SoftwareModel adjustedModel = modelAdjustment.adjustModel(sourceModel, servicePredictionSetup);

        // check the model for valid modifications
        InfrastructureModelContainer infrastructureModelContainer = adjustedModel.getInfrastructureModelContainer();
        ResourceEnvironment environment = infrastructureModelContainer.getModel();
        EList<ResourceContainer> containerList = environment.getResourceContainer_ResourceEnvironment();
        for (ResourceContainer container : containerList) {

            // check the settings for the single container
            if (container.getEntityName().equals(PredictionTestUtil.RESOURCE_CONTAINER_SINGLE_ID)) {
                EList<ProcessingResourceSpecification> resourceSpecificationList =
                        container.getActiveResourceSpecifications_ResourceContainer();

                // detect the processing resources
                ProcessingResourceSpecification cpuSpecification = null;
                ProcessingResourceSpecification hddSpecification = null;
                ProcessingResourceSpecification delaySpecification = null;
                ProcessingResourceSpecification lanSpecification = null;
                for (ProcessingResourceSpecification processingResourceSpecification : resourceSpecificationList) {
                    ProcessingResourceType type =
                            processingResourceSpecification.getActiveResourceType_ActiveResourceSpecification();
                    if (type.getEntityName().equals("CPU")) {
                        cpuSpecification = processingResourceSpecification;
                    } else if (type.getEntityName().equals("HDD")) {
                        hddSpecification = processingResourceSpecification;
                    } else if (type.getEntityName().equals("DELAY")) {
                        delaySpecification = processingResourceSpecification;
                    } else if (type.getEntityName().equals("LAN")) {
                        lanSpecification = processingResourceSpecification;
                    }
                }

                // prove the specifications
                assertNotNull("CPU resource not defined.", cpuSpecification);
                assertNotNull("HDD resource not defined.", hddSpecification);
                assertNotNull("DELAY resource not defined.", delaySpecification);
                assertNotNull("LAN resource not defined.", lanSpecification);

                // prove the settings
                // processingRate = cpuSpeed * (1+((cpucores - 1)*0.75))
                // processingRate = 2 * (1+((4 - 1)*0.75)) = 6.5
                assertEquals("6.5", cpuSpecification.getProcessingRate_ProcessingResourceSpecification()
                        .getSpecification());
            }
        }
    }

    /**
     * Test the adjustment of a workload in the usage profile of the software model.
     * 
     * @throws IOException
     *             Exception that the test model could not be loaded
     */
    @Test
    public void testUsageProfileWorkloadAdjustment() throws IOException {

        // get the software model from the repository
        Repository repository = DesignTimeRepository.getRepo();
        SoftwareModel sourceModel = repository.query("ORC_AllInOne");

        // build setup
        PredictionModelModification servicePredictionSetup = initServicePredictionSetup();

        // trigger model adjustment
        ModelAdjustmentImpl modelAdjustment = new ModelAdjustmentImpl(repository);
        SoftwareModel adjustedModel = modelAdjustment.adjustModel(sourceModel, servicePredictionSetup);

        // check the model for valid modifications
        UsageModelContainer usageModelContainer = adjustedModel.getUsageModelContainer();
        UsageModel usageModel = usageModelContainer.getModel();
        EList<UsageScenario> scenarioList = usageModel.getUsageScenario_UsageModel();
        List<UsageScenarioSpecification> usageSpecificationList =
                servicePredictionSetup.getUsageScenarioSpecifications();

        // check that the number of scenarios in the adjusted model
        // matches the number of specified scenarios
        assertEquals(usageSpecificationList.size(), scenarioList.size());

        // check that all three scenarios are satisfied
        for (UsageScenario usageScenario : scenarioList) {

            UsageScenarioSpecification specification =
                    modelAdjustment.findUsageSpecificationById(usageScenario.getEntityName(),
                            usageSpecificationList);
            assertNotNull("Usage scenario in model that is not specified: " + usageScenario.getEntityName(),
                    specification);

            Workload workload = usageScenario.getWorkload_UsageScenario();
            if (!(workload instanceof OpenWorkload)) {
                fail("Only OpenWorkloads are allowed. Rule broken by " + usageScenario.getEntityName());
            }
            OpenWorkload openWorkload = (OpenWorkload) workload;
            String interArrivalTimeSpec = openWorkload.getInterArrivalTime_OpenWorkload().getSpecification();
            assertEquals("Usage Scenario workload not set correctly (Scenario: " + usageScenario.getEntityName()
                    + ")", "Exp(1.0/" + specification.getInterArrivalTime() + ".0)", interArrivalTimeSpec);
        }
    }

    /**
     * Test the adjustment of a system call variable.
     * 
     * @throws IOException
     *             Exception that the test model could not be loaded
     */
    @Test
    public void testUsageProfileSystemCallVariableAdjustment() throws IOException {

        // get the software model from the repository
        Repository repository = DesignTimeRepository.getRepo();
        SoftwareModel sourceModel = repository.query("b6_eGovernment");

        // build setup
        PredictionModelModification servicePredictionSetup = initServicePredictionSetupSystemCallVariable();

        // trigger model adjustment
        ModelAdjustmentImpl modelAdjustment = new ModelAdjustmentImpl(repository);
        SoftwareModel adjustedModel = modelAdjustment.adjustModel(sourceModel, servicePredictionSetup);

        // check the model for valid modifications
        UsageModelContainer usageModelContainer = adjustedModel.getUsageModelContainer();
        UsageModel usageModel = usageModelContainer.getModel();
        EList<UsageScenario> scenarioList = usageModel.getUsageScenario_UsageModel();
        List<UsageScenarioSpecification> usageSpecificationList =
                servicePredictionSetup.getUsageScenarioSpecifications();

        // check that the number of scenarios in the adjusted model
        // matches the number of specified scenarios
        assertEquals(usageSpecificationList.size(), scenarioList.size());

        // check that all three scenarios are satisfied
        for (UsageScenario usageScenario : scenarioList) {

            UsageScenarioSpecification specification =
                    modelAdjustment.findUsageSpecificationById(usageScenario.getEntityName(),
                            usageSpecificationList);
            assertNotNull("Usage scenario in model that is not specified: " + usageScenario.getEntityName(),
                    specification);

            ScenarioBehaviour behaviour = usageScenario.getScenarioBehaviour_UsageScenario();
            EntryLevelSystemCall callAction =
                    modelAdjustment.findEntryLevelSystemCallByName(behaviour, "BookingCall");
            VariableUsage testVariable =
                    modelAdjustment.findVariableUsageByName(callAction, "customerWaitingTolerance");

            assertNotNull(testVariable);

            String testValue = extractValue(testVariable);
            assertEquals("Variable value not adjusted probably", "600", testValue);
        }
    }

    /**
     * Test the adjustment of a system call variable.
     * 
     * @throws IOException
     *             Exception that the test model could not be loaded
     */
    @Test
    public void testComponentParameterAdjustment() throws IOException {

        // get the software model from the repository
        Repository repository = DesignTimeRepository.getRepo();
        SoftwareModel sourceModel = repository.query("b6_eGovernment");

        // build setup
        PredictionModelModification servicePredictionSetup = initServicePredictionSetupComponentParameter();

        // trigger model adjustment
        ModelAdjustmentImpl modelAdjustment = new ModelAdjustmentImpl(repository);
        SoftwareModel adjustedModel = modelAdjustment.adjustModel(sourceModel, servicePredictionSetup);

        // check the repository component adjustment
        BasicComponent testComponent = null;
        VariableUsage testVariable = null;
        for (ComponentModelContainer componentModelContainer : adjustedModel.getComponentModelContainers()) {
            de.uka.ipd.sdq.pcm.repository.Repository pcmRepository = componentModelContainer.getModel();
            testComponent = modelAdjustment.findBasicComponentByName(pcmRepository, "OperatorBackend");
            testVariable = modelAdjustment.findVariableUsageByName(testComponent, "OperatorPoolCapacity");
        }
        assertNotNull(testVariable);

        String variableValue = extractValue(testVariable);
        assertEquals("Component parameter value not adjusted probably", "25", variableValue);
    }

    // ////////////////////////////////
    // TEST OBJECT INITIALIZATIONS
    // ////////////////////////////////

    /**
     * Extract the value characterization of a variable usage element.
     * 
     * @param variable
     *            The variable to extract the value of.
     * @return The extracted VALUE characterization or null if none was found.
     */
    private String extractValue(final VariableUsage variable) {
        String value = null;
        for (VariableCharacterisation characterisation : variable.getVariableCharacterisation_VariableUsage()) {
            if (characterisation.getType() == VariableCharacterisationType.VALUE) {
                value = characterisation.getSpecification_VariableCharacterisation().getSpecification();
            }
        }
        return value;
    }

    /**
     * Init the service prediction setup to be used for the model adjustment.
     * 
     * @return the service prediction setup ready to use.
     */
    private PredictionModelModification initServicePredictionSetup() {

        // --------------------------------
        // Resource Container Specification
        // --------------------------------
        ResourceContainerSpecification rcSpecification = new ResourceContainerSpecificationImpl();
        rcSpecification.setContainerID(PredictionTestUtil.RESOURCE_CONTAINER_SINGLE_ID);

        // set the number of cores
        SLATemplateValue cpuCores = new SLATemplateValueImpl();
        cpuCores.setId(SLATemplateValue.ID_CPU_CORES);
        cpuCores.setValue("4");
        rcSpecification.setCpuCores(cpuCores);

        // set the number of cores
        SLATemplateValue cpuSpeed = new SLATemplateValueImpl();
        cpuSpeed.setId(SLATemplateValue.ID_CPU_SPEED);
        cpuSpeed.setValue("2");
        cpuSpeed.setUnit("GHz");
        rcSpecification.setCpuSpeed(cpuSpeed);

        // set the number of cores
        SLATemplateValue memory = new SLATemplateValueImpl();
        memory.setId(SLATemplateValue.ID_MEMORY);
        memory.setValue("2048");
        memory.setUnit("MB");
        rcSpecification.setMemory(memory);

        // --------------------------------
        // Usage Scenario Specification
        // --------------------------------

        List<UsageScenarioSpecification> usageSpecifications = new LinkedList<UsageScenarioSpecification>();

        UsageScenarioSpecification specification1 = new UsageScenarioSpecifationImpl();
        specification1.setUsageScenarioID("ORC_CustomerConstraintPayment");
        specification1.setInterArrivalTime("70");
        usageSpecifications.add(specification1);

        UsageScenarioSpecification specification2 = new UsageScenarioSpecifationImpl();
        specification2.setUsageScenarioID("ORC_CustomerConstraintInventoryGetDetails");
        specification2.setInterArrivalTime("500");
        usageSpecifications.add(specification2);

        UsageScenarioSpecification specification3 = new UsageScenarioSpecifationImpl();
        specification3.setUsageScenarioID("ORC_CustomerConstraintInventoryBookSale");
        specification3.setInterArrivalTime("100");
        usageSpecifications.add(specification3);

        // --------------------------------
        // Build and return the setup
        // --------------------------------
        PredictionModelModification setup = new PredictionModelModificationImpl();
        setup.getResourceContainerSpecifications().add(rcSpecification);
        setup.getUsageScenarioSpecifications().addAll(usageSpecifications);
        return setup;
    }

    /**
     * Init the service prediction setup to be used for the model adjustment.
     * 
     * @return the service prediction setup ready to use.
     */
    private PredictionModelModification initServicePredictionSetupSystemCallVariable() {
        PredictionModelModification servicePredictionSetup = new PredictionModelModificationImpl();

        // specify the system call
        SystemCall systemCall = new SystemCallImpl("BookingCall");
        systemCall.addVariable(new SystemCallVariableImpl("customerWaitingTolerance", "600"));

        // specify the usage scenario (including workload definition)
        UsageScenarioSpecification usageScenario =
                new UsageScenarioSpecifationImpl("HealthAndMobilityBookingScenario");
        usageScenario.addSystemCall(systemCall);
        servicePredictionSetup.getUsageScenarioSpecifications().add(usageScenario);

        // return the prepared prediction setup
        return servicePredictionSetup;
    }

    /**
     * Init the service prediction setup to be used for the model adjustment.
     * 
     * @return the service prediction setup ready to use.
     */
    private PredictionModelModification initServicePredictionSetupComponentParameter() {
        PredictionModelModification servicePredictionSetup = new PredictionModelModificationImpl();

        // specify the service components
        ComponentSpecification operatorBackend = new ComponentSpecificationImpl("OperatorBackend");
        operatorBackend.getParameters().add(new ComponentParameterImpl("OperatorPoolCapacity", "25"));
        servicePredictionSetup.getComponentSpecifications().add(operatorBackend);

        // return the prepared prediction setup
        return servicePredictionSetup;
    }

}
