package org.slasoi.seval.prediction.extractor.impl;

import java.io.File;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.VirtualMachineArtefact;
import org.slasoi.seval.prediction.configuration.ResourceContainerSpecification;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.seval.prediction.extractor.Extractor;
import org.slasoi.seval.prediction.extractor.SLATemplateValue;
import org.slasoi.seval.prediction.test.PredictionTestUtil;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Test the general extractor implementation with functional and result tests.
 * 
 * @author Benjamin Klatt
 * 
 */
public class ExtractorTest extends TestCase {

    // ////////////////////////////////
    // TEST METHODS
    // ////////////////////////////////

    /**
     * Check the environment settings. Due to the SLA@SOI guidelines there is a general resource path accessible by a
     * system environment variable.
     */
    @Before
    public void checkEnvironment() {
        if (System.getenv("SLASOI_HOME") == null || System.getenv("SLASOI_HOME").equals("")) {
            fail("Evironment Variable SLASOI_HOME not set in the system");
        }
        File slasoihome = new File(System.getenv("SLASOI_HOME"));
        if (!slasoihome.exists() && !slasoihome.isDirectory()) {
            fail("SLASOI_HOME setting is not valid.");
        }
    }

    /**
     * Build a simple SLA template and builder and try to extract the cpu_speed property of the sla template.
     * 
     * @throws Exception
     *             Identifies an arbitrary exception occured during the test.
     */
    @Test
    public void testInfrastructureSlaTemplateValueExtraction() throws Exception {

        // read in and parse the sla template file
        String slaFilePath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "orc"
                        + File.separator + "infrastructure-templates" + File.separator
                        + "infrastructure_SLA_template.xml";
        PredictionTestUtil testUtil = new PredictionTestUtil();
        SLATemplate slaTemplate = testUtil.initSLATemplate(slaFilePath);

        // init the extractor
        Extractor extractor = new ExtractorImpl();

        // test to extract the cpu speed
        SLATemplateValue cpuSpeedValue = extractor.extractCPUSpeed(slaTemplate);
        assertNotNull(cpuSpeedValue);
        assertEquals(SLATemplateValue.ID_CPU_SPEED, cpuSpeedValue.getId());
        assertEquals("2", cpuSpeedValue.getValue());

        // test to extract the cpu cores
        SLATemplateValue cpuCoreValue = extractor.extractCPUCores(slaTemplate);
        assertNotNull(cpuCoreValue);
        assertEquals(SLATemplateValue.ID_CPU_CORES, cpuCoreValue.getId());
        assertEquals("1", cpuCoreValue.getValue());

        // test to extract the memory
        SLATemplateValue memoryValue = extractor.extractMemory(slaTemplate);
        assertNotNull(memoryValue);
        assertEquals(SLATemplateValue.ID_MEMORY, memoryValue.getId());
        assertEquals("2048", memoryValue.getValue());

        // test to extract the mttf
        SLATemplateValue mttfValue = extractor.extractAvailability(slaTemplate);
        assertNotNull(mttfValue);
        assertEquals(SLATemplateValue.ID_AVAILABILITY, mttfValue.getId());
        assertEquals("70", mttfValue.getValue());
    }

    /**
     * Build a simple SLA template and builder and try to extract the cpu_speed property of the sla template.
     * 
     * @throws Exception
     *             Identifies an arbitrary exception occured during the test.
     */
    @Test
    public void testSoftwareSlaTemplateValueExtraction() throws Exception {

        // read in and parse the sla template file
        String slaFilePath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "orc"
                        + File.separator + "infrastructure-templates" + File.separator
                        + "software_SLA_template.xml";
        PredictionTestUtil testUtil = new PredictionTestUtil();
        SLATemplate slaTemplate = testUtil.initSLATemplate(slaFilePath);

        // init the extractor
        Extractor extractor = new ExtractorImpl();

        // test to extract the memory
        List<UsageScenarioSpecification> usageSpecifications =
                extractor.extractUsageScenarioSpecifications(slaTemplate);
        assertNotNull(usageSpecifications);
        assertEquals(3, usageSpecifications.size());

        // check specification 1
        UsageScenarioSpecification specification1 = usageSpecifications.get(0);
        assertEquals("ORC_CustomerConstraintPayment", specification1.getUsageScenarioID());
        assertEquals("70", specification1.getInterArrivalTime());

        // check specification 2
        UsageScenarioSpecification specification2 = usageSpecifications.get(1);
        assertEquals("ORC_CustomerConstraintInventoryGetDetails", specification2.getUsageScenarioID());
        assertEquals("500", specification2.getInterArrivalTime());

        // check specification 3
        UsageScenarioSpecification specification3 = usageSpecifications.get(2);
        assertEquals("ORC_CustomerConstraintInventoryBookSale", specification3.getUsageScenarioID());
        assertEquals("100", specification3.getInterArrivalTime());

    }

    /**
     * Test the complete extraction of a service prediction setup. This is done for a single container setup.
     * 
     * @throws Exception
     *             test failed
     */
    @Test
    public void testExtractServicePredictionSetup() throws Exception {

        ServiceConstructionModelFactory scmFactory = ServiceConstructionModelFactory.eINSTANCE;

        PredictionTestUtil testUtil = new PredictionTestUtil();

        // read in and parse the infrastructure sla template file
        String slaFilePath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "orc"
                        + File.separator + "infrastructure-templates" + File.separator
                        + "infrastructure_SLA_template.xml";
        SLATemplate slaTemplate = testUtil.initSLATemplate(slaFilePath);
        ServiceBuilder builder = initServiceBuilder(slaTemplate, scmFactory);

        // read in and parse the software SLA template file
        String softwareSlaFilePath =
                System.getenv("SLASOI_HOME") + File.separator + "software-servicemanager" + File.separator + "orc"
                        + File.separator + "infrastructure-templates" + File.separator
                        + "software_SLA_template.xml";
        SLATemplate softwareSlaTemplate = testUtil.initSLATemplate(softwareSlaFilePath);

        // run the test itself
        Extractor extractor = new ExtractorImpl();
        PredictionModelModification setup = extractor.extractServicePredictionSetup(softwareSlaTemplate, builder);
        assertNotNull(setup);

        // check extracted container specifications
        assertEquals(1, setup.getResourceContainerSpecifications().size());
        ResourceContainerSpecification rcSpecification = setup.getResourceContainerSpecifications().get(0);
        assertEquals(PredictionTestUtil.RESOURCE_CONTAINER_SINGLE_ID, rcSpecification.getContainerID());

        // check extracted usage specifications
        assertEquals(3, setup.getUsageScenarioSpecifications().size());
        assertNotNull(setup.getUsageScenarioSpecifications().get(0));
        assertNotNull(setup.getUsageScenarioSpecifications().get(1));
        assertNotNull(setup.getUsageScenarioSpecifications().get(2));
    }

    // ////////////////////////////////
    // TEST OBJECT INITIALIZATIONS
    // ////////////////////////////////

    /**
     * Initialize a service builder to run extractor tests with.
     * 
     * @param slaTemplate
     *            The sla template to reference by service bindings.
     * @param scmFactory
     *            The factory to create scm elements.
     * @return The initialized builder.
     */
    private ServiceBuilder initServiceBuilder(final SLATemplate slaTemplate,
            final ServiceConstructionModelFactory scmFactory) {

        // dependency
        Dependency dependency = scmFactory.createDependency();
        dependency.setName(PredictionTestUtil.RESOURCE_CONTAINER_SINGLE_ID);
        dependency.setDescription("ORC_AllInOne");

        // ImplementationArtefact -> Dependency
        VirtualMachineArtefact vmArtefact = scmFactory.createVirtualMachineArtefact();
        vmArtefact.setDependencies(new Dependency[] {dependency});

        // ServiceImplementation -> ImplementationArtefact -> Dependency
        ServiceImplementation serviceImplementation = scmFactory.createServiceImplementation();
        serviceImplementation.setServiceImplementationName("ORC_AllInOne");
        serviceImplementation.setArtefacts(new ImplementationArtefact[] {vmArtefact});

        // Binding -> Dependency
        ServiceBinding binding = scmFactory.createServiceBinding();
        binding.setDependency(dependency);
        binding.setSlaTemplate(slaTemplate);

        // Builder
        ServiceBuilder builder = scmFactory.createServiceBuilder();
        builder.setImplementation(serviceImplementation);
        builder.setBindings(new ServiceBinding[] {binding});
        return builder;
    }

}
