package org.slasoi.seval.prediction.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.slasoi.seval.prediction.configuration.ComponentSpecification;
import org.slasoi.seval.prediction.configuration.PredictionModelModification;
import org.slasoi.seval.prediction.configuration.SystemCall;
import org.slasoi.seval.prediction.configuration.UsageScenarioSpecification;
import org.slasoi.seval.prediction.configuration.impl.ComponentParameterImpl;
import org.slasoi.seval.prediction.configuration.impl.ComponentSpecificationImpl;
import org.slasoi.seval.prediction.configuration.impl.PredictionModelModificationImpl;
import org.slasoi.seval.prediction.configuration.impl.SystemCallImpl;
import org.slasoi.seval.prediction.configuration.impl.SystemCallVariableImpl;
import org.slasoi.seval.prediction.configuration.impl.UsageScenarioSpecifationImpl;
import org.slasoi.seval.prediction.exceptions.UnboundDependencyException;
import org.slasoi.seval.prediction.exceptions.UnsupportedTermException;
import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.impl.SoftwareServiceEvaluator;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.prediction.service.settings.impl.EvaluationSettingsImpl;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;

/**
 * Test case for the b6 related requirements.
 * 
 * Required features are:
 * <ul>
 * 	<li>Trigger evaluation based on setup data model not on SLA templates and builders</li>
 *  <li>Adjust Workload</li>
 *  <li>Adjust external call variable usages in the prediction model</li>
 *  <li>Adjust component parameters in the repository</li>
 * </ul>
 * 
 * @author Benjamin Klatt
 *
 */
public class B6TestCase extends TestCase {

    /**
     * The main test routine.
     * @throws Exception 
     */
    @Test
    public void testEvaluate() throws Exception {
        
        // Logging:
        System.out.println("B6TestCase.testEvaluate() START");
        
        // ----------------------------------------------------------
        // STEP 1: prepare the required components for the evaluator
        // ----------------------------------------------------------
        SoftwareServiceEvaluator evaluator = initServiceEvaluator();
        
        // ----------------------------------------------------------
        // STEP 2: build up the service specification
        // ----------------------------------------------------------
        PredictionModelModification servicePredictionSetup = initPredictionSetup();
        

        // ----------------------------------------------------------
        // STEP 3: Perform the evaluation
        // ----------------------------------------------------------
        
        // activate reliability prediction and request a reliability and a response time sensor
        // otherwise only the response time of the external service calls will be returned
        EvaluationSettings evaluationSettings = new EvaluationSettingsImpl();
        evaluationSettings.setAnalyzeResponsibility(true);
        evaluationSettings.getSensorNames().add("Execution result of UsageScenario HealthAndMobilityBookingScenario <_8XfAgEPfEeCBiZcjlTVKeA>");
        evaluationSettings.getSensorNames().add("Response Time of HealthAndMobilityBookingScenario");
        
        IEvaluationResult result = null; 
        try {
        	result = evaluator.evaluate("b6_eGovernment", servicePredictionSetup, evaluationSettings);
        } catch (UnsupportedTermException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        } catch (UnboundDependencyException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        } catch (ModelNotFoundException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        }


        // ----------------------------------------------------------
        // STEP 4: Print the results
        // ----------------------------------------------------------

        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                + "####################################################"
                + "#################################################\n"
                + "Component Name: Service Evaluation\n"
                + "Interface Name: evaluate\n"
                + "Operation Name: evaluate\n"
                + "Input:Type "
                + "void"
                + "\n"
                + "####################################################"
                + "#################################################\n"
                + "######################################## RESPONSE"
                + "############################################\n\n"
                + "####################################################"
                + "#################################################\n");

        System.out.println(result.toString());
        
        // Logging:
        System.out.println("B6TestCase.testEvaluate() SUCCESS");
    }

    
    //////////////////////////////////
    // TEST OBJECT INITIALIZATION
    //////////////////////////////////

    /**
     * Initialize the prediction setup.
     * @return The prediction setup to test against.
     */
    private PredictionModelModification initPredictionSetup() {
        PredictionModelModification servicePredictionSetup = new PredictionModelModificationImpl();
        
        // specify the service components
        ComponentSpecification callCenterControl = new ComponentSpecificationImpl("CallCenterControl");
        callCenterControl.getParameters().add(new ComponentParameterImpl("PhoneLinePoolCapacity", "100"));
        servicePredictionSetup.getComponentSpecifications().add(callCenterControl);
        
        ComponentSpecification operatorBackend = new ComponentSpecificationImpl("OperatorBackend");
        operatorBackend.getParameters().add(new ComponentParameterImpl("OperatorPoolCapacity", "42"));
        servicePredictionSetup.getComponentSpecifications().add(operatorBackend);

        // specify the system call
        SystemCall systemCall = new SystemCallImpl("BookingCall");
        systemCall.addVariable(new SystemCallVariableImpl("healthCareBookingRequestInternal","DoublePMF[ (120.0;0.9) (160.0;0.08) (600.0;0.02) ]"));
        systemCall.addVariable(new SystemCallVariableImpl("healthCareBookingRequestDelegated","300.0"));
        systemCall.addVariable(new SystemCallVariableImpl("mobilityBookingRequestInternal","DoublePMF[ (0.0;0.9) (120.0;0.09) (600.0;0.01) ]"));
        systemCall.addVariable(new SystemCallVariableImpl("mobilityBookingRequestDelegated","300.0"));
        systemCall.addVariable(new SystemCallVariableImpl("delegate","BoolPMF[ (true;0.2) (false;0.8) ]"));
        systemCall.addVariable(new SystemCallVariableImpl("operatorWatingTimeDelegated","100.0"));
        systemCall.addVariable(new SystemCallVariableImpl("findBusyLineDelegated","BoolPMF[ (true;0.02) (false;0.98) ]"));
        systemCall.addVariable(new SystemCallVariableImpl("loseCallDelegated","BoolPMF[ (true;0.11) (false;0.89) ]"));
        systemCall.addVariable(new SystemCallVariableImpl("customerWaitingTolerance","666.0"));
        
        // specify the usage scenario (including workload definition)
        UsageScenarioSpecification usageScenario = new UsageScenarioSpecifationImpl("HealthAndMobilityBookingScenario");
        usageScenario.addSystemCall(systemCall);
        usageScenario.setInterArrivalTime("100");
        servicePredictionSetup.getUsageScenarioSpecifications().add(usageScenario);
        
        // return the prepared prediction setup
		return servicePredictionSetup;
	}


	/**
     * Initialize the evaluation component before its use.
     * @return The prepared software service evaluation component.
     */
    private SoftwareServiceEvaluator initServiceEvaluator() {
        SoftwareServiceEvaluator evaluator = new SoftwareServiceEvaluator();
        evaluator.setEvaluationMode(EvaluationMode.Auto);
        evaluator.setEvaluationServerEndpoint("http://localhost:8082/services");
        return evaluator;
    }
}
