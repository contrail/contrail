package org.slasoi.seval.prediction.test;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.seval.prediction.exceptions.UnboundDependencyException;
import org.slasoi.seval.prediction.exceptions.UnsupportedTermException;
import org.slasoi.seval.prediction.service.EvaluationMode;
import org.slasoi.seval.prediction.service.IEvaluationResult;
import org.slasoi.seval.prediction.service.impl.SoftwareServiceEvaluator;
import org.slasoi.seval.prediction.service.settings.EvaluationSettings;
import org.slasoi.seval.prediction.service.settings.impl.EvaluationSettingsImpl;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Tests the main software service evaluation method.
 * 
 * @author Benjamin Klatt
 * @author kuester, brosch
 * 
 */
public class InteractionTest extends TestCase {

    
    //////////////////////////////////
    // TEST METHODS
    //////////////////////////////////
    
    /**
     * Check the environment settings.
     * Due to the SLA@SOI guidelines there is a general resource path accessible
     * by a system environment variable.
     */
    @Before
    public void checkEnvironment(){
        if(System.getenv("SLASOI_HOME") == null || System.getenv("SLASOI_HOME").equals("")){
            fail("Evironment Variable SLASOI_HOME not set in the system");
        }
        File slasoihome = new File(System.getenv("SLASOI_HOME"));
        if(!slasoihome.exists() && !slasoihome.isDirectory()){
            fail("SLASOI_HOME setting is not valid.");
        }
    }

    /**
     * The main test routine.
     * @throws Exception 
     */
    @Test
    public void testEvaluate() throws Exception {
        
        // Logging:
        System.out.println("InteractionTest.testEvaluate() START");
        
        // ----------------------------------------------------------
        // STEP 1: prepare the required components for the evaluator
        // ----------------------------------------------------------
        SoftwareServiceEvaluator evaluator = initServiceEvaluator();
        
        // ----------------------------------------------------------
        // STEP 2: prepare the input builder and request SLA template
        // ----------------------------------------------------------

        // read in and parse the sla template file
        String slaFilePath = System.getenv("SLASOI_HOME") 
                                    + File.separator 
                                    + "software-servicemanager"
                                    + File.separator 
                                    + "orc"
                                    + File.separator 
                                    + "infrastructure-templates"
                                    + File.separator 
                                    + "software_SLA_template.xml";
        PredictionTestUtil testUtil = new PredictionTestUtil();
        SLATemplate customerRequestSLATemplate = testUtil.initSLATemplate(slaFilePath);
        Set<ServiceBuilder> builders = testUtil.initServiceBuilders();

        // ----------------------------------------------------------
        // STEP 3: Perform the evaluation
        // ----------------------------------------------------------
        Set<IEvaluationResult> results = new HashSet<IEvaluationResult>();
        try {
        	EvaluationSettings settings = new EvaluationSettingsImpl();
        	settings.getSensorNames().add("Response Time of Call CardValidationServiceIf.validateCard0 <Component: Tradingsystem.Webservices.PaymentService, AssemblyCtx: _JSl5ACKVEd6EO6qHYg-W9w, CallID: _2XoOYCKVEd6EO6qHYg-W9w>");
        	settings.setMaxMeasurementsCount(100);
        	settings.setMaxSimTime(-1);
            results = evaluator.evaluate(builders, customerRequestSLATemplate,settings);
        }
        catch (UnsupportedTermException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        }
        catch (UnboundDependencyException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        }
        catch (ModelNotFoundException e) {
            fail(e.getClass().toString() + ": " + e.getMessage());
        }


        // ----------------------------------------------------------
        // STEP 4: Print the results
        // ----------------------------------------------------------

        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                + "####################################################"
                + "#################################################\n"
                + "Component Name: Service Evaluation\n"
                + "Interface Name: evaluate\n"
                + "Operation Name: evaluate\n"
                + "Input:Type "
                + "void"
                + "\n"
                + "####################################################"
                + "#################################################\n"
                + "######################################## RESPONSE"
                + "############################################\n\n"
                + "####################################################"
                + "#################################################\n");

        IEvaluationResult temp = results.iterator().next();
        System.out.println(temp.toString());
        
        // Logging:
        System.out.println("InteractionTest.testEvaluate() SUCCESS");
    }

    
    //////////////////////////////////
    // TEST OBJECT INITIALIZATION
    //////////////////////////////////

    /**
     * Initialize the evaluation component before its use.
     * @return The prepared software service evaluation component.
     */
    private SoftwareServiceEvaluator initServiceEvaluator() {
        SoftwareServiceEvaluator evaluator = new SoftwareServiceEvaluator();
        evaluator.setEvaluationMode(EvaluationMode.Auto);
        evaluator.setEvaluationServerEndpoint("http://localhost:8082/services");
        return evaluator;
    }
}
