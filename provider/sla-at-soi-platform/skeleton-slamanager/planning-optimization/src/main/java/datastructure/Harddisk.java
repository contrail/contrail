package datastructure;

import utils.Constant;


/**
 * <code>Harddisk</code> represents Harddisk in resource request.
 * 
 */
public class Harddisk extends Resource {

    public Harddisk() {
        this.setResourceName(Constant.Harddisk);
    }
}
