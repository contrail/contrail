package datastructure;

import utils.Constant;


/**
 * <code>Bandwidth</code> represents Bandwidth in resource request.
 * 
 */
public class Bandwidth extends Resource {
    public Bandwidth() {
        this.setResourceName(Constant.Bandwidth);
    }
}
