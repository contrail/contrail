package skeletonslam.main;

import java.io.File;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.codehaus.plexus.archiver.UnArchiver;
import org.codehaus.plexus.archiver.manager.ArchiverManager;

/**
 * Generate the basic structure of Skeleton Manager in the source directory.
 * 
 * @goal generate
 * @requiresDependencyResolution runtime
 * @threadSafe
 * @version $Id: SkeletonGenerateMojo 2011-01-15 08:15:16Z Miguel Rojas $
 */
public class SkeletonGenerateMojo extends AbstractSkeletonMojo
{
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            getLog().info( "Skeleton will generate directories and resources..." );
            createResources();
            getLog().info( "directories and resources created." );
            
            getLog().info( "Skeleton will adjust names according to DS-SLAM..." );
            refactoringResources();
            getLog().info( "names have been adjusted." );
            
            getLog().info( "Skeleton will adjust namespaces for DS-SLAM..." );
            refactoringPackages();
            getLog().info( "namespaces udpated." );
            
            getLog().info( "Skeleton will adjust OSGi-profiles for DS-SLAM..." );
            refactoringProfiles();
            refactoringProfilesContent();
            getLog().info( "profiles udpated." );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    protected void createResources() throws Exception
    {
        String mavenArtifact = getMavenRepoDirectory().getAbsolutePath() + SKELETON_ARTIFACT;
        ArchiverManager am = getArchiverManager();
        
        String tmpPath = System.getProperty( "java.io.tmpdir" );
        if ( !(tmpPath.endsWith("/") || tmpPath.endsWith("\\")) )
            tmpPath = tmpPath + System.getProperty( "file.separator" );
        tmpPath = tmpPath + "skeleton";

        File ftmp = new File( tmpPath );
        if ( !ftmp.exists() ) ftmp.mkdirs();
        
        getLog().info( "from '" + mavenArtifact + "'" );
        getLog().info( "\t... generating temporal resources '" + tmpPath + "!" );
        UnArchiver artifact = am.getUnArchiver( "jar" );
        artifact.setSourceFile( new File( mavenArtifact ) );
        artifact.setDestDirectory( new File( tmpPath ) );
        artifact.setOverwrite( true );
        artifact.extract();
        
        UnArchiver unArchiver = am.getUnArchiver( "jar" );
        File target = new File( generateDir.getAbsolutePath() + "/" + dsSLAMName );
        if ( !target.exists() ) target.mkdirs();
        
        unArchiver.setSourceFile( new File( tmpPath + "/skeleton-slamanager.zip" ) );
        unArchiver.setDestDirectory( target );
        unArchiver.setOverwrite( true );
        unArchiver.extract(); 
    }
    
    protected void refactoringResources() throws Exception
    {
        getLog().info( "Skeleton will adjust resources for DS-SLAM using '" + getDsSLAMName() + "'..." );
        
        String base = generateDir.getAbsolutePath() + "/" + dsSLAMName ;
        String _NAMESPACE = namespace.replace( ".", "/" );
        for ( String module : SKL_MODULES )
        {
            String newPath = base + "/" + module + "/src";
            String _root = newPath + "/main/java/__root";
            String _test = newPath + "/test/java/__root";
            
            String _ROOT = newPath + "/main/java/" + _NAMESPACE;
            String _TEST = newPath + "/test/java/" + _NAMESPACE;
            
            FileUtils.moveDirectory( new File( _root ), new File( _ROOT ) );
            FileUtils.moveDirectory( new File( _test ), new File( _TEST ) );
        }
    }
    
    protected void refactoringPackages() throws Exception
    {
        getLog().info( "Skeleton will adjust namespaces for DS-SLAM using '" + getNamespace() + "'..." );
        
        refactoringJava();
        refactoringXml ();
    }
    
    protected void refactoringJava() throws Exception
    {
        String base = generateDir.getAbsolutePath() + "/" + dsSLAMName ;
        String[] filter = { "java" };
        Collection<File> listFiles = FileUtils.listFiles( new File( base ), filter, true );
        Iterator<File> iterator = listFiles.iterator();
        Vector<String> content = new Vector<String>( 50, 10 );
        String PREFIX = "__prefix";
        while ( iterator.hasNext() )
        {
            content.clear();
            File f = iterator.next();
            int index = 0;
            LineIterator lines = FileUtils.lineIterator( f );
            while ( lines.hasNext() )
            {
                String current = lines.nextLine();
                if ( current != null )
                {
                    for ( String[] key : SKL_JAVAKEYWORDS )
                    {
                        if ( current.contains( key[ 0 ] ) )
                        {
                            current = current.replaceAll( key[ 0 ], key[ 1 ] + namespace );
                        }
                    }
                    if ( current.contains( PREFIX ) )
                    {
                        current = current.replaceAll( PREFIX, dsSLAMName );
                    }
                    
                    content.add( current );
                }
                
                index++;
            }
            
            // save new file
            FileUtils.writeLines( f, content );
        }
    }
    
    protected void refactoringXml() throws Exception
    {
        String base = generateDir.getAbsolutePath() + "/" + dsSLAMName ;
        String[] filter = { "xml", "txt" };
        
        Hashtable<String, String> ht = new Hashtable<String, String>();
        ht.put( SKL_XMLKEYWORDS[ 0 ], namespace  );
        ht.put( SKL_XMLKEYWORDS[ 1 ], dsSLAMName );
        
        Collection<File> listFiles = FileUtils.listFiles( new File( base ), filter, true );
        Iterator<File> iterator = listFiles.iterator();
        Vector<String> content = new Vector<String>( 50, 10 );
        while ( iterator.hasNext() )
        {
            content.clear();
            File f = iterator.next();
            int index = 0;
            LineIterator lines = FileUtils.lineIterator( f );
            while ( lines.hasNext() )
            {
                String current = lines.nextLine();
                if ( current != null )
                {
                    for ( String key : SKL_XMLKEYWORDS )
                    {
                        if ( current.contains( key ) )
                        {
                            current = current.replaceAll( key, ht.get( key ) );
                        }
                    }
                    content.add( current );
                }
                
                index++;
            }
            
            // save new file
            
            FileUtils.writeLines( f, content );
        }
    }
    
    protected void refactoringProfiles() throws Exception
    {
        getLog().info( "Skeleton will adjust OSGi-profiles for DS-SLAM using '" + getDsSLAMName() + "'..." );
        
        String base = generateDir.getAbsolutePath() + "/" + dsSLAMName + "/profile4osgi";
        String _NAMESPACE = namespace.replace( ".", "/" );

        String _root = base + "/__root";
        String _ROOT = base + "/" + _NAMESPACE + "/" + dsSLAMName;
        
        FileUtils.moveDirectory( new File( _root ), new File( _ROOT ) );
        
        // now rename the profiles
        String[] filter = { "composite" };
        Collection<File> listFiles = FileUtils.listFiles( new File( _ROOT ), filter, true );
        Iterator<File> iterator = listFiles.iterator();
        String __PREFIX = "__prefix";
        while ( iterator.hasNext() )
        {
            File f = iterator.next();
            String fn = f.getAbsolutePath();
            if ( fn.contains( __PREFIX ) )
            {
                String newFN = fn.replaceAll( __PREFIX, dsSLAMName );
                FileUtils.moveFile( new File( fn ), new File( newFN ) );
            }
        }
    }
    
    protected void refactoringProfilesContent() throws Exception
    {
        String base = generateDir.getAbsolutePath() + "/" + dsSLAMName + "/profile4osgi";
        String[] filter = { "composite" };
        
        Hashtable<String, String> ht = new Hashtable<String, String>();
        ht.put( SKL_XMLKEYWORDS[ 0 ], namespace  );
        ht.put( SKL_XMLKEYWORDS[ 1 ], dsSLAMName );
        
        Collection<File> listFiles = FileUtils.listFiles( new File( base ), filter, true );
        Iterator<File> iterator = listFiles.iterator();
        Vector<String> content = new Vector<String>( 50, 10 );
        while ( iterator.hasNext() )
        {
            content.clear();
            File f = iterator.next();
            int index = 0;
            LineIterator lines = FileUtils.lineIterator( f );
            while ( lines.hasNext() )
            {
                String current = lines.nextLine();
                if ( current != null )
                {
                    for ( String key : SKL_XMLKEYWORDS )
                    {
                        if ( current.contains( key ) )
                        {
                            current = current.replaceAll( key, ht.get( key ) );
                        }
                    }
                    content.add( current );
                }
                
                index++;
            }
            
            // save new file
            
            FileUtils.writeLines( f, content );
        }
    }
    
    public File getGenerateDir()
    {
        return generateDir;
    }

    public void setGenerateDir( File generateDir )
    {
        this.generateDir = generateDir;
    }
    
    public String getDsSLAMName()
    {
        return dsSLAMName;
    }

    public void setDsSLAMName( String dsSLAMName )
    {
        this.dsSLAMName = dsSLAMName;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace( String namespace )
    {
        this.namespace = namespace;
    }

    /**
     * The path to extract all resources.
     * 
     * @parameter expression="${skeleton.directory.generate}"
     */
    protected File generateDir;
    
    /**
     * The path to extract all resources.
     * 
     * @parameter expression="${skeleton.dsslam.name}"
     */
    protected String dsSLAMName;
    
    /**
     * The path to extract all resources.
     * 
     * @parameter expression="${skeleton.dsslam.namespace}"
     */
    protected String namespace;
    
    protected static final String[]   SKL_MODULES      = { "core", "main", "planning-optimization", "provisioning-adjustment" };
    protected static final String[][] SKL_JAVAKEYWORDS = { { "package __root", "package " },
                                                           { "import __root" , "import "  }
                                                         };
    
    protected final String[] SKL_XMLKEYWORDS = { "__namespace",
                                                 "__prefix"   };
 
    protected static final String SKELETON_ARTIFACT = "/org/slasoi/slam/factory/maven-slam-plugin/0.1-SNAPSHOT/maven-slam-plugin-0.1-SNAPSHOT.jar";
}
