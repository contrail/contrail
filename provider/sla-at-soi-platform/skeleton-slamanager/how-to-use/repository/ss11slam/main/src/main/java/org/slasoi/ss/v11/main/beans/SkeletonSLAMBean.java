package org.slasoi.ss.v11.main.beans;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.builder.ProvisioningAdjustmentBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.GenericSLAManagerServices.SLAMConfiguration;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.ism.occi.IsmOcciService;
import org.slasoi.ss.v11.core.OCCIAware;
import org.slasoi.ss.v11.main.beans.client.NegotiationHelper;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

public class SkeletonSLAMBean implements BundleContextAware
{
    public SkeletonSLAMBean()
    {
    }
    
    /*
     * Starts the ssv11 SLA manager.
     */
    public void start()
    {
        try
        {
            INSTANCES++;
            SLAMConfiguration isConfig = gslamServices.loadConfigurationFrom( "ssv11.instance1.cfg" );

            isContext = gslamServices.createContext( osgiContext, isConfig.name + "-" + INSTANCES, isConfig.epr, isConfig.group, isConfig.wsPrefix );
            isContext.setProperties( isConfig.properties );

            // Inject the ISM into isPOC
            injectOCCI( this.isPOC );
            // Inject the ISM into isPAC
            injectOCCI( this.isPAC );

            // Inject the PAC into the isContext
            isContext.setProvisioningAdjustment( isPAC );
            injectIntoContext( isPAC, isContext );

            // Inject the POC into the isContext
            isContext.setPlanningOptimization( isPOC );
            injectIntoContext( isPOC, isContext );

            LOGGER.info( "\n\n \t*** :: start :: gslamServices >> \n" + gslamServices );
            LOGGER.info( "\n\n \t*** :: start :: ssv11SLAMBean >> \n" + this.isContext );

        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    /**
     * Stops the ssv11 SLA manager.
     */
    public void stop()
    {
    }

    /**
     * Sets the OSGi context of ssv11 SLA manager.
     */
    public void setBundleContext( BundleContext osgiContext )
    {
        assert (osgiContext != null) : "The OSGi context of ssv11 SLA manager != null.";
        this.osgiContext = osgiContext;

        SSv11SLAMTracer tracer = new SSv11SLAMTracer();
        osgiContext.registerService( tracer.getClass().getName(), tracer, null );
    }

    /**
     * Injects the domain specific components into SLA manager context. e.g.,
     * IPOC and IPAC.
     */
    protected void injectIntoContext( Object obj, SLAManagerContext context )
    {
        assert (context != null && obj != null) : "The context of ssv11 SLA manager != null and the object that is injecting into SLAM context !=null.";
        if ( obj instanceof SLAMContextAware )
        {
            ((SLAMContextAware)obj).setSLAManagerContext( context );
        }
    }

    /**
     * Injects the infrastructure service manager into SLA manager context.
     */
    protected void injectOCCI( Object obj )
    {
        assert (obj != null) : "The OCCI (ISM) context that is injecting into ssv11 SLA manager context != null.";
        if ( obj instanceof OCCIAware )
        {
            ((OCCIAware)obj).setOCCI( _ISM_ );
        }
    }

    /**
     * Sets the generic SLA manager services.
     */
    @ServiceReference
    public void setGslamServices( GenericSLAManagerServices gslamServices )
    {
        assert (gslamServices != null) : "Generic SLAM services != null.";
        LOGGER.info( "generic-slam injected successfully into is-slam" );
        this.gslamServices = gslamServices;
    }

    /**
     * Sets the infrastructure service manager.
     */
    @ServiceReference
    public void setInfrastructureServiceManager( IsmOcciService iServiceManager )
    {
        LOGGER.info( "ISM injected successfully into ssv11-slam." );
        assert (iServiceManager != null) : "Infrastructure Service Manager != null.";
        this._ISM_ = iServiceManager;
    }

    /**
     * Sets IPOC.
     */
    @ServiceReference( filter = "(proxy=ss11slam-poc)" )
    public void setPOC( PlanningOptimizationBuilder pocBuilder )
    {
        LOGGER.info( "is-POC injected successfully into ssv11-slam" );
        assert (pocBuilder != null) : "PlanningOptimizationBuilder instance != null.";
        this.isPOC = pocBuilder.create();
    }

    /**
     * Sets IPAC.
     */
    @ServiceReference( filter = "(proxy=ss11slam-pac)" )
    public void setPAC( ProvisioningAdjustmentBuilder pacBuilder )
    {
        LOGGER.info( "is-PAC injected successfully into ssv11-slam" );
        assert (pacBuilder != null) : "ProvisioningAdjustmentBuilder instance != null.";
        this.isPAC = pacBuilder.create();
    }

    protected SLAManagerContext isContext;

    protected PlanningOptimization isPOC;
    protected ProvisioningAdjustment isPAC;
    protected IsmOcciService _ISM_;

    protected GenericSLAManagerServices gslamServices;

    protected BundleContext osgiContext;

    private static int INSTANCES = 0;

    private static final Logger LOGGER = Logger.getLogger( SkeletonSLAMBean.class );

    public class SSv11SLAMTracer
    {
        /**
         * method to be invoked from osgi-console via 'echo' command
         */
        public void context()
        {
            System.out.println( isContext );
        }

        public void ism()
        {
            System.out.println( _ISM_ );
        }

        /**
         * method to be invoked from osgi-console via 'echo' command
         */
        public void slamID()
        {
            try
            {
                System.out.println( "\t\t SLAM-ID = " + isContext.getSLAManagerID() );
            }
            catch ( Exception e )
            {
            }
        }

        /**
         * method to be invoked from osgi-console via 'invoke' command
         */
        public void values( Hashtable<String, String> params )
        {
            assert (params != null) : "SLAM-params != null.";
            System.out.println( "\t\t SLAM-params = " + params );
        }

        /**
         * method to test Negotiation
         */
        public void test()
        {
            try
            {
                NegotiationHelper helper = new NegotiationHelper();
                helper.run( isContext );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }
}
