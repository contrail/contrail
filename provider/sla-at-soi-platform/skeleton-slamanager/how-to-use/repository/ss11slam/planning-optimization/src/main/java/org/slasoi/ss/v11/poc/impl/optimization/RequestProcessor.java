package org.slasoi.ss.v11.poc.impl.optimization;

import java.util.Collection;
import java.util.Iterator;

import org.slasoi.ss.v11.poc.impl.datastructure.Request;
import org.slasoi.ss.v11.poc.impl.exceptions.RequestNotCorrectException;

/**
 * Each <code>RequestProcessor</code> represents an infrastructure provider.
 */
public class RequestProcessor
{
    private String providerName;
    private Collection<Request> requestList;
    private double failureRate;
    private boolean isSlaChanged;

    public RequestProcessor(
                             String providerName,
                             double failureRate )
    {
        this.providerName = providerName;
        this.failureRate = failureRate;
    }

    /**
     * Gets the name of provider.
     */
    public String getProviderName()
    {
        return providerName;
    }

    /**
     * Gets the request.
     */
    public Collection<Request> getRequest()
    {
        return requestList;
    }

    /**
     * Gets the failure rate.
     */
    public double getFailureRate()
    {
        return failureRate;
    }

    public void startProcess( Collection<Request> requestList ) throws RequestNotCorrectException
    {
        this.requestList = requestList;
        if ( this.requestList == null )
        {
            throw new RequestNotCorrectException( "The incoming request is either null or incorrect format." );
        }

        // set the client type
        Iterator<Request> it = this.requestList.iterator();
        while ( it.hasNext() )
        {
            Request request = (Request)it.next();
            // start process
            this.process( request );
        }
    }

    private void process( Request request )
    {
        this.processQoS();
        this.processFinalRisk();
        this.processFinalCost();
        this.processFinalProfit();
    }

    private void processFinalProfit()
    {
    }

    private void processFinalCost()
    {

    }

    private void processFinalRisk()
    {

    }

    private void processQoS()
    {

    }

    /**
     * To evaluate whether the SLA is changed or not.
     */
    public boolean isSlaChanged()
    {
        return isSlaChanged;
    }

}
