package org.slasoi.ss.v11.poc.impl.datastructure;

import org.slasoi.ss.v11.poc.impl.utils.Constant;

/**
 * <code>Bandwidth</code> represents Bandwidth in resource request.
 */
public class Bandwidth extends Resource
{
    public Bandwidth()
    {
        this.setResourceName( Constant.Bandwidth );
    }
}
