NOTES about this artifact
-------------------------

* this artifact contains the implementation of domain specific POC  

* due to an OSGi technical constraint, the creation of
the domain specific POC and its linking to the ss11slam-slam4osgi, has to 
be done via PlanningOptimizationServices::create() 

