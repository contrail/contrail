@echo off
SET PLUGIN=org.slasoi.slam.factory:maven-slam-plugin
SET GOAL=generate
SET POM=-f./pom-skl.xml

mvn %PLUGIN%:%GOAL% %POM% %PARAM1% %PARAM2% %PARAM3% -Pexample
