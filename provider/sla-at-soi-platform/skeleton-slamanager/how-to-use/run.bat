@echo off
SET PLUGIN=org.slasoi.slam.factory:maven-slam-plugin
SET GOAL=generate
SET POM=-f./pom-skl.xml
SET PARAM1=-Dskeleton.directory.generate=./repository
SET PARAM2=-Dskeleton.dsslam.name=ss11slam
SET PARAM3=-Dskeleton.dsslam.namespace=org.slasoi.ss.v11

mvn %PLUGIN%:%GOAL% %POM% %PARAM1% %PARAM2% %PARAM3%
