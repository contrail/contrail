/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-slamanager/businessslam-main/src/main/java/org/slasoi/bslamanager/main/beans/BSLAMBean.java $
 */

package org.slasoi.bslamanager.main.beans;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.gslam.core.authorization.Authorization;
import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.builder.ProvisioningAdjustmentBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.GenericSLAManagerServices.SLAMConfiguration;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

public class BSLAMBean implements BundleContextAware{
    protected SLAManagerContext bContext;
    protected GenericSLAManagerServices gslamServices;
    protected PlanningOptimization bPOCServices;
    //protected ProvisioningAdjustment bPACServices;
    
    //protected ProductDiscovery discovery;
    //protected ICustomerRelations customerRelations;
    //rotected ISLATComposition slatComposition;
    protected Authorization bAuthorization;
    protected BundleContext osgiContext;
    private static int INSTANCES = 0;
    private static final Logger LOGGER = Logger.getLogger(BSLAMBean.class);

    @Autowired
    private BusinessContextService businessContextService;
        
    public void start() {
        try {

            INSTANCES++;
            SLAMConfiguration bConfig = gslamServices.loadConfigurationFrom("bslam.instance1.cfg");
            bContext =
                    gslamServices.createContext(osgiContext, bConfig.name + "-" + INSTANCES, bConfig.epr,
                            bConfig.group, bConfig.wsPrefix);

			bContext.setProperties( bConfig.properties );
            
            businessContextService.setBusinessContext(bContext);
            
            // Inject the POC into the swContext
            LOGGER.info("inject Context Into poc");
            bContext.setPlanningOptimization(bPOCServices);
            injectIntoContext(bPOCServices, bContext);
            
           // Inject the PAC into the swContext
/*
            LOGGER.info("inject Context Into pac");
            bContext.setProvisioningAdjustment(bPACServices);
            injectIntoContext(bPACServices, bContext);
*/
            // Inject the POC Authorization the swContext
            LOGGER.info("inject Context Into Authorization");
            bContext.setAuthorization(bAuthorization);

            // Inject Context into productDiscovery
            
            /* removed deps from BM
            LOGGER.info("inject Context Into discovery");
            injectIntoContext(discovery, bContext);
            
         // Inject Context into slatComposition
            LOGGER.info("inject Context Into SLATCmposition");
            injectIntoContext(slatComposition, bContext);

            // Inject Context into postSale
            LOGGER.info("inject Context Into postSale");
            injectIntoContext(customerRelations, bContext);
            */
            
            //templates sender
            //This is only for test purpose
            /*DynTemplatePublisher ghost = new DynTemplatePublisher( bContext );
              ghost.start();*/            
            //Templates Subscriber
            //DynTemplateSubscriber watcher = new DynTemplateSubscriber( bContext );
            //watcher.start();
            //
            LOGGER.info("\n\n \t*** :: start :: gslamServices >> \n" + gslamServices);
            LOGGER.info("\n\n \t*** :: start :: BSLAMBean >> \n" + bContext);
        }
        catch (Exception e) {
            LOGGER.debug(e);
            e.printStackTrace();
        }
    }

    public void setBundleContext( BundleContext osgiContext )
    {
        this.osgiContext = osgiContext;
        
        BusinessSLAMTracer tracer = new BusinessSLAMTracer();
        osgiContext.registerService( tracer.getClass().getName(), tracer, null );
    }

    public void stop() {
    }

    protected void injectIntoContext(Object obj, SLAManagerContext context) {
        if (obj instanceof SLAMContextAware) {
            ((SLAMContextAware) obj).setSLAManagerContext(context);
        }else{
            LOGGER.error("Context not insert into: "+obj);
        }
    }

    @ServiceReference
    public void setGslamServices(GenericSLAManagerServices gslamServices) {
        LOGGER.info("generic-slam injected successfully into business-slam");
        this.gslamServices = gslamServices;
    }
    
    @ServiceReference(filter = "(proxy=business-poc)")
    public void setPOC( PlanningOptimizationBuilder builder )
    {
        LOGGER.info( "business-POC injected successfully into sw-slam" );
        
        this.bPOCServices  = builder.create();
    }

    @ServiceReference(filter = "(proxy=business-auth)")
    public void setBAuthorization(Authorization bAuthorization) {
        LOGGER.info("business-Auth injected successfully into business-slam");
        this.bAuthorization = bAuthorization;
    }

    /* removed deps from BM
    @ServiceReference
    public void setICustomerRelations(ICustomerRelations customerRelations) {
        LOGGER.info("ICustomerRelations injected successfully into business-slam");
        this.customerRelations = customerRelations;
    }

    @ServiceReference
    public void setSlatComposition(ISLATComposition slatComposition) {
        LOGGER.info("SLATCompostion injected successfully into business-slam");
        this.slatComposition = slatComposition;
    }
    
    @ServiceReference
    public void setProductDiscovery(ProductDiscovery discovery) {
        LOGGER.info("ProductDiscovery injected successfully into business-slam");
        this.discovery = discovery;
    }
    */
    
/*    
    @ServiceReference(filter = "(proxy=business-pac)")
    public void setPACService(ProvisioningAdjustmentBuilder builder) {
        LOGGER.info("business-PAC injected successfully into business-slam");
        this.bPACServices = builder.create();
    }
*/
    
    public class BusinessSLAMTracer
    {
        /*
         *  method to be invoked from osgi-console via 'echo' command
         */
        public void context()
        {
            System.out.println( bContext );
        }
        
        /*
         *  method to be invoked from osgi-console via 'echo' command
         */
        public void slamID()
        {
            try
            {
                System.out.println( "\t\t businessSLAM ID = "+bContext.getSLAManagerID() );
            }
            catch ( Exception e )
            {
            }
        }
        
        /*
         *  method to be invoked from osgi-console via 'invoke' command
         */
        public void values( Hashtable<String, String> params )
        {
            System.out.println( "\t\t bSLAM-params = " + params );
        }
        
        public void assertions()
        {
            String a = null;
            assert( a != null ) : "assertions ON";
            
            System.out.println( "assertions OFF" );
        }
    }


}
