/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-slamanager/businessslam-main/src/main/java/org/slasoi/bslamanager/main/beans/DynTemplateSubscriber.java $
 */

package org.slasoi.bslamanager.main.beans;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.sa.pss4slam.core.ISubscribable;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;
import org.slasoi.slamodel.sla.SLATemplate;

public class DynTemplateSubscriber implements Runnable
{  private static final Logger log = Logger.getLogger(DynTemplateSubscriber.class);
    public DynTemplateSubscriber( SLAManagerContext ctx )
    {
        context = ctx;
    }
    
    public void start()
    {
        Thread tr = new Thread( this );
        tr.start();
    }
    
    public void run()
    {
        try
        {
            ServiceAdvertisement sa = context.getServiceAdvertisement();
            ISubscribable iSubscribable = sa.getISubscribable();
            iSubscribable.subscribe( new ISubscriber()
            {
                public void notify( Template<?>[] templates )
                {
                    try
                    {
                       log.info("DynTemplateSubscriber ::  getting >> " + templates.length + " templates!" );
                       log.info( "\t\t" + Arrays.toString( templates ) );
                        
                        SLATemplateRegistry slaTemplateRegistry = context.getSLATemplateRegistry();
                        
                        for ( int i = 0; i < templates.length; i++ )
                        {
                            Template<SLATemplate> tmp = (Template<SLATemplate>)templates[ i ];
                            log.info( "########Adding Template:"+tmp.getContent().getUuid().toString());
                            Metadata metadata = new Metadata();
                            String bslatId = tmp.getContent().getUuid().getValue();
                            String partyId = tmp.getContent().getParties()[0].getId().getValue();
                            log.info( "########Provider uuid:"+partyId);
                            log.info( "########registrar_id:"+bslatId);
                            metadata.setPropertyValue(Metadata.provider_uuid, "{"+partyId+"}");
                            metadata.setPropertyValue(Metadata.registrar_id,"{"+bslatId+"}");
                            slaTemplateRegistry.addSLATemplate( tmp.getContent(), metadata );
                            log.info( "########Template added:"+bslatId);
                        }
                    }
                    catch ( Exception e )
                    {
                        log.error("Error:"+e.toString());
                        e.printStackTrace();
                    }
                }
                
            }, ServiceAdvertisementChannel.SLAMODEL );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    protected String namesAsString( Template<?>[] templates )
    {
        StringBuffer sb = new StringBuffer();
        for ( Template<?> curr : templates )
        {
            SLATemplate t = (SLATemplate)curr.getContent();
            sb.append( t.getUuid().getValue() + ", " );
        }
        
        return sb.toString();
    }
    
    protected SLAManagerContext context;
}
