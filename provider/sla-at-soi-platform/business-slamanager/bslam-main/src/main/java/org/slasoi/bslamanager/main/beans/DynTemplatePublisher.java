/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-slamanager/businessslam-main/src/main/java/org/slasoi/bslamanager/main/beans/DynTemplatePublisher.java $
 */

package org.slasoi.bslamanager.main.beans;

import java.util.Calendar;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.sa.pss4slam.core.IPublishable;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;

public class DynTemplatePublisher implements Runnable
{
    public DynTemplatePublisher( SLAManagerContext ctx )
    {
        context = ctx;
    }
    
    public void start()
    {
        Thread tr = new Thread( this );
        tr.start();
    }
    
    public void run()
    {
        try
        {
            Thread.sleep( 5000 ); // estimated time for starting up other SLAMs
            
            ServiceAdvertisement sa = context.getServiceAdvertisement();
            IPublishable iPublishable = sa.getIPublishable();
            SLATemplate temp = context.getSLATemplateRegistry().getSLATemplate(new UUID("ORCBUSINESSSLAT1"));
            temp.setUuid(new UUID("new_UUID"));
             Template<?>[] arrayAux = new Template<?>[] { new Template<SLATemplate>( temp)};
             System.out.println( "########################################Testing my templates..." );
            iPublishable.publish(arrayAux, ServiceAdvertisementChannel.SLAMODEL );
           /* for ( Template<?>[] tpl : array )
            {
                System.out.println( "Business-SLAM publishing following templates: " + namesAsString( tpl ) + "..." );
                iPublishable.publish( tpl, ServiceAdvertisementChannel.SLAMODEL );
                
                Thread.sleep( 3000 ); // estimated time for spreading templates ;)
            }*/
        }
        catch ( Exception e )
        {
        }
    }
    
    protected static Template<SLATemplate> generateTemplate( int idx )
    {
        try
        {
            SLATemplate result = new SLATemplate();
            result.setUuid( new UUID( "SW-TEMPLATE-ID::"+idx ) );
            
            Calendar cal = Calendar.getInstance();
            cal.add( Calendar.DAY_OF_YEAR, -1 );
            
            Party[] parties = new Party[ 1 ];
            parties[ 0 ] = new Party( new ID  ( "swPARTY-" + idx ), 
                                      new STND( "swSLAM-" + System.currentTimeMillis() ) );
            result.setParties( parties );
            
            return new Template<SLATemplate>( result );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        
        return null;
    }
    
    protected String namesAsString( Template<?>[] templates )
    {
        StringBuffer sb = new StringBuffer();
        for ( Template<?> curr : templates )
        {
            SLATemplate t = (SLATemplate)curr.getContent();
            sb.append( t.getUuid().getValue() + ", " );
        }
        
        return sb.toString();
    }
    
    protected Template<?>[][] array = new Template<?>[][]
    {
        { generateTemplate( 1 ), generateTemplate( 2 ) },
        { generateTemplate( 3 ), generateTemplate( 4 ), generateTemplate( 5 ) },
        { generateTemplate( 6 ) }
    };
    
    protected SLAManagerContext context;
}
