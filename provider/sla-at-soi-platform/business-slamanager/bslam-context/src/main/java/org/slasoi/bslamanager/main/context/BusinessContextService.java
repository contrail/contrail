package org.slasoi.bslamanager.main.context;

import org.slasoi.gslam.core.context.SLAManagerContext;

public interface BusinessContextService {

    public SLAManagerContext getBusinessContext();
    
    public void setBusinessContext(SLAManagerContext context);
    
}
