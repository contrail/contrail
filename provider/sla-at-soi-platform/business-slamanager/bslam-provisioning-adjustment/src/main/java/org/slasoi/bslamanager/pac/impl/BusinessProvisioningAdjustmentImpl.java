/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/BusinessProvisioningAdjustmentImpl.java $
 */

/**
 * 
 */
package org.slasoi.bslamanager.pac.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.drools.builder.ResourceType;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.bslamanager.pac.IBusinessProvisioningAdjustment;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.common.ws.types.TerminationEvent;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.pac.Agent;
import org.slasoi.gslam.pac.Event;
import org.slasoi.gslam.pac.EventType;
import org.slasoi.gslam.pac.Identifier;
import org.slasoi.gslam.pac.ProvisioningAndAdjustment;
import org.slasoi.gslam.pac.SharedKnowledgePlane;
import org.slasoi.gslam.pac.config.Configuration;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
@Service(value="businessProvisioningAdjustmentService")
public class BusinessProvisioningAdjustmentImpl extends ProvisioningAndAdjustment implements
        IBusinessProvisioningAdjustment {

    private static final String DEFAULT_CONFIG_FILE="bpac.properties";
   
    private static Logger logger = Logger.getLogger(BusinessProvisioningAdjustmentImpl.class.getName());

    @Autowired
    private BusinessContextService businessContextService;
    
    public BusinessProvisioningAdjustmentImpl(String configFile) {
        //escamez super(configFile);
        //override init() from super   
        this.configurationFile = DEFAULT_CONFIG_FILE;
        this.pacID = BUSINESS_PAC_ID;
        this.init();
        logger.info("Creating Business PAC...");        
    }

    /*
     * Constructor to be invoked from a spring context file. The init() method will be invoked by spring
     */
    public BusinessProvisioningAdjustmentImpl() {
        logger.info("Creating default Business PAC...");
        this.pacID = BUSINESS_PAC_ID;
    }

    /**
     * Performs initialization actions:
     * 
     * - load properties
     * 
     * - create agents
     * 
     * - initialize drools knowledge base.
     */
    public void init() {

        InputStream is =null;
        Configuration config = null;
        
        logger.info("ProvisioningAndAdjustment, init() method");
        try {
            properties.load(BusinessProvisioningAdjustmentImpl.class.getClassLoader().getResourceAsStream(configurationFile));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Properties file loaded...");

        logger.info("\t loading Drools Log Mode...");
        SharedKnowledgePlane.getInstance(this.pacID).initKnowledgeBase((String)properties.getProperty(LOG_MODE_DROOLS));
                
        try {
            logger.info("\t loading Agent Configuration File...");
            is = BusinessProvisioningAdjustmentImpl.class.getClassLoader().getResourceAsStream(properties.getProperty(AGENT_CONFIGURATION_FILE));
            config = new Configuration(is);
            
            // At the moment, only one agent is created
            
            manager = new Agent(config.getAgentConfiguration());

        }        
        catch (Exception e) {
            logger.error("BusinessProvisioningAdjustmentImp# init# Exception# "+e.getMessage());
        }
        finally{
            if (is!=null){
                try {
                    is.close();
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.info("BusinessProvisioningAdjustmentImp# init# finally# Exception# Error closing input stream...");
                }
            }
            logger.info("BusinessProvisioningAdjustmentImp #init #Agent intialized...");
        }
        
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().insert(manager);
        addManagedElements(manager);
        new Thread(manager).start();
        
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().insert(this);
        SharedKnowledgePlane.getInstance(this.pacID).addRulesFile(properties.getProperty(INITIAL_RULES_FILE), ResourceType.DRL);
   
        super.setSLAManagerContext(businessContextService.getBusinessContext());
    }    
    
    public void setSLAManagerContext(SLAManagerContext context) {
        super.setSLAManagerContext(context);      
    }

    // Specific interface for Business Adjustment
    public void trackEvent(List<AdjustmentNotificationType> violationList) {

        logger.info("Business PAC, trackEvent");
        if (violationList != null) {
            logger.info("Received: ");
            for (AdjustmentNotificationType violation : violationList) {
                logger.info(violation.toString()); // Ask Alberto to add this method
                logger.info("SLA ID =" + violation.getSlaID());
                logger.info("NotificationId = " + violation.getNotificationId());
                logger.info("Notifier = " + violation.getNotifier());
                logger.info("Event type = " + violation.getType());

                // Retrieves SLA from registry and inserts it into kb
                UUID[] id = new UUID[1];
                id[0] = new UUID(violation.getSlaID());
                SLA[] slas = null;
                
                try {
                    logger.info("Connecting to SLA Registry....");
                    logger.info("Querying for SLA with slaId = " + id.toString());
                    slas = sLAManagerContext.getSLARegistry().getIQuery().getSLA(id);
                    logger.info("Got " + slas.length + " slas");
                }
                catch (InvalidUUIDException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SLAManagerContextException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Insert SLA in kb
                for (SLA sla : slas) {
                    logger.info("Inserting sla " + sla.getUuid() + " in kb");
                    SharedKnowledgePlane.getInstance(BUSINESS_PAC_ID).getStatefulSession().insert(sla);
                }

                // Creates event and inserts it into drools knowledge base
                switch(violation.getType()){
                case RECOVERY :{
                    sendEvent(new Event(EventType.ViolationRecoveryEvent, Identifier.PAC, violation));
                    break;
                }
                case NOTIFICATION:{
                    sendEvent(new Event(EventType.WarningEvent, Identifier.PAC, violation));
                    break;
                }
                default:{
                    sendEvent(new Event(EventType.ViolationEvent, Identifier.PAC, violation));
                }
                }
               
            }
        }
        else {
            logger.debug("Received null violation list.");
        }

    }

    public void notifyEvent(Event event) {
        EventType type = event.getType();
        logger.debug("BPAC::notifyEvent " + type.toString());

        if (type.equals(EventType.SendReportEvent)) {
            // ask the reporting component to send a report to the customer
            logger.info("SendReportEvent received by PAC");
            // Preguntar cómo acceder al CustomerRelations dentro del context
            // sLAManagerContext.getCustomerRelations().sendReport(event.getEventContent());
        }
        else if (type.equals(EventType.SendTerminationEvent)) {
            logger.info("SendTerminationEvent received in PAC");
            TerminationEvent term = (TerminationEvent) event.getEventContent();
            try {
                logger.info(sLAManagerContext.getPlanningOptimization());
                // logger.info(sLAManagerContext.getPlanningOptimization().getIAssessmentAndCustomize());
                // sLAManagerContext.getPlanningOptimization().getIAssessmentAndCustomize().terminate(
                // new UUID(term.getSlaId()), term.getTerminationReasons());
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
