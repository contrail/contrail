/**
 * Copyright (c) 2008-2010, Telefonica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/PoliciesInterfaceTest.java $
 */

/**
 * 
 */
package org.slasoi.bslamanager.pac;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.pac.impl.BusinessProvisioningAdjustmentImpl;
import org.slasoi.gslam.core.control.Policy;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class PoliciesInterfaceTest extends AbstractTransactionalSpringContextTests{
   
    private static final Logger logger = Logger.getLogger(PoliciesInterfaceTest.class.getName());

    private static final String POLICY_NAME_1 = "Policy1";
    private static final long ID_POLICY_1 = 100;
    private static final String POLICY_CONTENT_1 =
            "rule \"Policy1\" \n when \n eval(true) \nthen \n System.out.println(\"Hello, here is the first test policy\"); \n end";
    private static final String POLICY_NAME_2 = "Policy2";
    private static final long ID_POLICY_2 = 200;
    private static final String POLICY_CONTENT_2 =
            "rule \"Policy2\" \n when \n eval(true) \nthen \n System.out.println(\"Hello, here is the second test policy\"); \n end";


    @Resource
    private BusinessProvisioningAdjustmentImpl businessPAC;
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PoliciesInterfaceTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PoliciesInterfaceTest.class );
    }
   
    public void testPolicies() {
        logger.info("getting policies");
        
        boolean exists = false;
        
        // First, get the original policies
        Policy[] policies = businessPAC.getPolicies("Adjustment");
        
        if (policies!=null && policies.length>0){
            exists = true;
            printPolicies(policies);
        }
                
        // Now, set policies for testing
        Policy[] testPolicies = createPolicies();
        logger.debug("Policies for testing:");
        printPolicies(testPolicies);
        businessPAC.setPolicies("Adjustment", testPolicies);

        // Retrieve the policies to make some checks
        Policy[] test = businessPAC.getPolicies("Adjustment");
        assertPolicies(testPolicies,test);

        if (exists){
            // Restore the original policies
            businessPAC.setPolicies("Adjustment", policies);
            businessPAC.stop();
        }
    }

    private Policy[] createPolicies() {
        Policy[] policies = new Policy[2];

        policies[0] = new Policy();
        policies[0].setId(ID_POLICY_1);
        policies[0].setName(POLICY_NAME_1);
        policies[0].setRule(POLICY_CONTENT_1);

        policies[1] = new Policy();
        policies[1].setId(ID_POLICY_2);
        policies[1].setName(POLICY_NAME_2);
        policies[1].setRule(POLICY_CONTENT_2);

        return policies;
    }

    private void printPolicies(Policy[] policies) {
        for (Policy policy : policies) {
            logger.info("Policy id = " + policy.getId());
            logger.info("Policy name " + policy.getName());
            logger.info("Policy content  = " + policy.getRule());
        }
    }

    private void assertPolicies(Policy[] policies1, Policy[] policies2) {
        // assertEquals(policies1.length, policies2.length);
        for (int i = 0; i < policies1.length; i++) {
            long id = policies1[i].getId();
            // search for the policy with the same id
            for (int j = 0; j < policies2.length; j++) {
                if (policies2[j].getId() == id) {
                    assertEquals(policies1[i].getName(), policies2[j].getName());
                    assertEquals(policies1[i].getRule(), policies2[j].getRule());
                }
            }
        }
    }

}
