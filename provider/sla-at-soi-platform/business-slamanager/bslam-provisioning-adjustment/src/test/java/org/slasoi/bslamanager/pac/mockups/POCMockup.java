/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/mockups/POCMockup.java $
 */

package org.slasoi.bslamanager.pac.mockups;

import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class POCMockup implements PlanningOptimization, PlanningOptimization.IAssessmentAndCustomize {

    private static final Logger logger = Logger.getLogger(POCMockup.class.getName());
    private boolean terminated = false;

    public POCMockup() {
        logger.info("Creating Mockup POC");
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public IAssessmentAndCustomize getIAssessmentAndCustomize() {
        // TODO Auto-generated method stub
        return this;
    }

    public INotification getINotification() {
        // TODO Auto-generated method stub
        return null;
    }

    public IPlanStatus getIPlanStatus() {
        // TODO Auto-generated method stub
        return null;
    }

    public IReplan getIReplan() {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA createAgreement(String arg0, SLATemplate arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    public SLATemplate[] negotiate(String arg0, SLATemplate arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA provision(UUID arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean terminate(UUID arg0, List<TerminationReason> arg1) {
        terminated = true;
        System.out.println("Terminating");
        return true;
    }

}
