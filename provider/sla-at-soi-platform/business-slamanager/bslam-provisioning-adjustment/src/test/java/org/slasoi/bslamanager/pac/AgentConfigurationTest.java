/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (Mon, 07 Mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/AgentConfigurationTest.java $
 */

package org.slasoi.bslamanager.pac;


import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;

import org.slasoi.gslam.pac.config.AgentConfiguration;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.springframework.test.AbstractTransactionalSpringContextTests;

import com.thoughtworks.xstream.XStream;

public class AgentConfigurationTest extends AbstractTransactionalSpringContextTests{

    private static final Logger logger = Logger.getLogger(AgentConfigurationTest.class.getName());

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     * 
     * @param testName
     *            name of the test case
     */
    public AgentConfigurationTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AgentConfigurationTest.class);
    }
    
    
    public void testSerialization() {
        // creates an agentConfiguration object, serializes it, deserializes the output and checks the new object is
        // equal to the original one

        AgentConfiguration config = createConfiguration();
        String str = serialize(config);
        AgentConfiguration configAfter = deserialize(str);

        assertEquals(config.toString(), configAfter.toString());
    }

    private AgentConfiguration createConfiguration() {
        logger.info("Creating new AgentConfiguration...");

        AgentConfiguration config = new AgentConfiguration();
        config.setAgentId("Manager");

        TaskConfiguration task1 =
                new TaskConfiguration(
                        "Analysis",
                        "org.slasoi.gslam.pac.AnalysisTaskorg.slasoi.businessManager.violationPenalty.BusinessAnalysisTask",
                        null, "BPAC");
        config.addTaskConfiguration(task1);

        return config;
    }

    private String serialize(AgentConfiguration config) {
        logger.info("Serializing...");

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);
        String configStr = xstream.toXML(config);

        logger.info(configStr);

        return (configStr);
    }

    private AgentConfiguration deserialize(String configStr) {
        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);

        AgentConfiguration agentConfig = new AgentConfiguration();

        try {
            agentConfig = (AgentConfiguration) xstream.fromXML(configStr);
        }

        catch (Exception e) {
            logger.info("Unable to deseserialised Agent Configuration. \n" + e.getMessage());
            e.printStackTrace();
        }

        logger.info(agentConfig);
        return (agentConfig);
    }
}
