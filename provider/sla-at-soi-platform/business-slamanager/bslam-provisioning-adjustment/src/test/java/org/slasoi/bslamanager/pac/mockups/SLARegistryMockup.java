/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/mockups/SLARegistryMockup.java $
 */

package org.slasoi.bslamanager.pac.mockups;

import java.io.FileInputStream;
import java.io.IOException;

import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.bnf;

public class SLARegistryMockup implements SLARegistry, SLARegistry.IRegister, SLARegistry.IQuery {
    
    private static final String BUSINESS_SLA_FILE = "ORC_Business_SLA.xml";
    
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    
    // registry to store one SLA for the test :)
    private SLA sla;

    public IQuery getIQuery() {
        return this;
    }

    public IRegister getIRegister() {
        return this;
    }

    public UUID register(SLA sla, UUID[] uuids, SLAState arg2) throws RegistrationFailureException {
        this.sla = sla;
        return null;
    }

    public UUID update(UUID uuid, SLA sla, UUID[] arg2, SLAState arg3) throws UpdateFailureException {
        this.sla = sla;
        return uuid;
    }

    public SLA[] getSLA(UUID[] arg0) throws InvalidUUIDException {
        SLA[] slas = new SLA[1];
        try {
            slas[0] = createSLA();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return slas;
    }

    public UUID[] getDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getSLAsByState(SLAState[] arg0, boolean arg1) throws InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAStateInfo[] getStateHistory(UUID arg0, boolean arg1) throws InvalidUUIDException, InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getUpwardDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByParty(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByTemplateId(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    private SLA createSLA() throws IOException {
        // Read SLA from xml, and convert to Java object (using Syntax Converter)
        
        String slaXml = readFile("src"+FILE_SEPARATOR+"test"+FILE_SEPARATOR+"resources"+FILE_SEPARATOR+BUSINESS_SLA_FILE);
        
        SLA sla = null;
        try {
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(slaXml);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(bnf.render(sla, true));

        return sla;
    }

    private String readFile(String filename) throws IOException {
        String content = null;

        FileInputStream file = new FileInputStream(filename);
        byte[] b = new byte[file.available()];
        file.read(b);
        file.close();
        content = new String(b);

        return content;
    }

    public UUID sign(UUID arg0) throws UpdateFailureException {
        // TODO Auto-generated method stub
        return null;
    }

    public IAdminUtils getIAdmin() {
        // TODO Auto-generated method stub
        return null;
    }
}
