package org.slasoi.businessManager.violationPenalty;

import org.slasoi.gslam.pac.*;
import org.slasoi.businessManager.violationPenalty.AdjustmentNotificationTypeService;
import org.slasoi.businessManager.violationPenalty.GTStatisticsService;
import org.slasoi.businessManager.violationPenalty.GTStatusService;
import org.slasoi.businessManager.violationPenalty.SlaStatisticsService;

import org.slasoi.slamodel.sla.*;
import org.slasoi.businessManager.common.model*;
import org.slasoi.businessManager.common.ws.types*;

import org.slasoi.businessManager.violationPenalty.utils.GuaranteeTermParser;

import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;

import java.util.List;
//import org.slasoi.slamodel.sla.business.*;

/*
rule "Store_notification_event_in_DB"
@id(2010)
@name(Store_notification_event_in_DB)
@rulebody(rule "Store_notification_event_in_DB" 
	salience 10000
	when
		Event(type == EventType.ViolationEvent, $violation: eventContent) or
		Event(type == EventType.ViolationRecoveryEvent, $violation: eventContent) or
		Event(type == EventType.WarningEvent, $violation: eventContent);
	then
		AdjustmentNotificationTypeDBManager.saveOrUpdate((AdjustmentNotificationType)$violation);
 end)
	salience 10000
	when
		Event(type == EventType.ViolationEvent, $violation: eventContent) or
		Event(type == EventType.ViolationRecoveryEvent, $violation: eventContent) or
		Event(type == EventType.WarningEvent, $violation: eventContent);
	then
		AdjustmentNotificationTypeDBManager.saveOrUpdate((AdjustmentNotificationType)$violation);
end


//Counts the number of penalties in a given GuaranteeTerm of a template.
//If the number of penalties 100 in a week, then find the most suitable value for the failing KPI,
//and renegotiate the affected agreements. 
rule "Check penalties per template and guaranteeTerm"
salience 9000
when
	$penalty: Penalty($violation: violation, $slaid: $violation.slaid, $gt: $violation.guaranteeTermId);
	$sla: SLA (uuid == $slaid, $templateId: templateId);

    Number($penaltyCount: intValue) from accumulate(
	$penalty2: Penalty($violation2 : violation,  $violation.slaid == $slaid, $violation.guaranteeTermId == $gt),
	           SLA (uuid == $slaid, $templateId: templateId)
	over window:time(7d) , count( $penalty2 ))
	
	$slaCount: SLARegistry.getIQuery().getSLAsByTemplateId($templateId).size();
	
	$penaltyCount/$slaCount > 0.2;
then
	Guaranteed.State newGT = StatisticCalculator.calculateBestKPI($templateId, $gt);
	NegotiationManager.renegotiate($templateId, newGT);
end


rule "Evaluate penalty"
@id(2020)
@name(Evaluate penalty)
@rulebody(rule "Evaluate penalty" 
salience 9000
 	when
    	$event: Event(type == EventType.ViolationEvent, $violation : eventContent, $violationId : eventContent.notificationId, $slaid : eventContent.slaID, $gt: eventContent.notification.violationEvent.guaranteeTerm);
    	$sla: SLA (uuid == $slaid);
    then
        System.out.println("" );
        System.out.println("penalty for SLAID = " + $slaid + " being calculated in drools");
        
		Penalty $penalty = GuaranteeTermParser.getPenaltiesFromSLA((SLA)$sla, (String)$gt);
		SLAViolation $violationDB = AdjustmentNotificationTypeDBManager.getSLAViolationById((String)$violationId).get(0);
		$penalty.setViolation($violationDB);
		
		PenaltyDBManager.saveOrUpdate((Penalty)$penalty);
		
		//send notification to customer
		Event $notif = new Event(EventType.SendReportEvent, Identifier.AnalysisTask, Identifier.PAC, $violationId);
		insert($notif);
 end)
 salience 9000
 	when
    	$event: Event(type == EventType.ViolationEvent, $violation : eventContent, $violationId : eventContent.notificationId, $slaid : eventContent.slaID, $gt: eventContent.notification.violationEvent.guaranteeTerm);
    	$sla: SLA (uuid == $slaid);
    then
        System.out.println("" );
        System.out.println("penalty for SLAID = " + $slaid + " being calculated in drools");
        
		Penalty $penalty = GuaranteeTermParser.getPenaltiesFromSLA((SLA)$sla, (String)$gt);
		SLAViolation $violationDB = AdjustmentNotificationTypeDBManager.getSLAViolationById((String)$violationId).get(0);
		$penalty.setViolation($violationDB);
		
		PenaltyDBManager.saveOrUpdate((Penalty)$penalty);
		
		//send notification to customer
		Event $notif = new Event(EventType.SendReportEvent, Identifier.AnalysisTask, Identifier.PAC, $violationId);
		insert($notif);
end

rule "Check termination"
@id(2030)
@name(Check termination)
@rulebody(rule "Check termination" 
salience 8000
 	when
    	$event: Event(type == EventType.ViolationEvent, $violation : eventContent, $violationId : eventContent.notificationId, $slaid : eventContent.slaID, $gt: eventContent.notification.violationEvent.guaranteeTerm);
    	$sla: SLA (uuid == $slaid);
    then
        System.out.println("" );
        System.out.println("checking termination for " + $slaid );
        
		boolean $termination = GuaranteeTermParser.checkTermination((SLA)$sla, (String)$gt);
		
		TerminationEvent $term = new TerminationEvent((String)$slaid, TerminationReason.BUSINESS_DECISION); 
		Event $notif = new Event(EventType.SendTerminationEvent, Identifier.AnalysisTask, Identifier.PAC, $term);
		insert($notif);
 end)
 salience 8000
 	when
    	$event: Event(type == EventType.ViolationEvent, $violation : eventContent, $violationId : eventContent.notificationId, $slaid : eventContent.slaID, $gt: eventContent.notification.violationEvent.guaranteeTerm);
    	$sla: SLA (uuid == $slaid);
    then
        System.out.println("" );
        System.out.println("checking termination for " + $slaid );
        
		boolean $termination = GuaranteeTermParser.checkTermination((SLA)$sla, (String)$gt);

		TerminationEvent $term = new TerminationEvent((String)$slaid, TerminationReason.BUSINESS_DECISION); 
		Event $notif = new Event(EventType.SendTerminationEvent, Identifier.AnalysisTask, Identifier.PAC, $term);
		insert($notif);
end


rule "Retract events"
@id(2040)
@name(Retract events)
@rulebody(rule "Retract events" 
salience 7000
 	when
    	$event: Event(type == EventType.ViolationEvent, $slaid : eventContent.slaID);
    	$sla: SLA (uuid == $slaid);
    then
		retract($sla);
		retract($event);
 end)
 salience 7000
 	when
    	$event: Event(type == EventType.ViolationEvent, $slaid : eventContent.slaID);
    	$sla: SLA (uuid == $slaid);
    then
		retract($sla);
		retract($event);
end


rule "Send violation notification"
@id(2050)
@name(Send violation notification)
@rulebody(rule "Send violation notification" 
	salience 7000
	when
		$event : Event(type == EventType.SendReportEvent, $violationId: eventContent);
		$pac: ProvisioningAndAdjustment();
	then
		$pac.notifyEvent($event);
        retract($event);
 end)
	salience 7000
	when
		$event: Event(type == EventType.SendReportEvent, $violationId: eventContent);
		$pac: ProvisioningAndAdjustment();
	then
        $pac.notifyEvent($event);
        retract($event);
end



rule "PrintViolationEvent"
@id(2060)
@name(PrintViolationEvent)
@rulebody(rule "PrintViolationEvent" 
salience 100
    when
    	$event : Event(type == EventType.ViolationEvent); 
    then
    	System.out.println("A violation event has been inserted:");
        System.out.println($event.toString());
 end)
salience 100
    when
    	$event : Event(type == EventType.ViolationEvent); 
    then
    	System.out.println("A violation event has been inserted:");
        System.out.println($event.toString());
end

rule "PrintSLA"
@id(2070)
@name(PrintSLA)
@rulebody(rule "PrintSLA" 
salience 100
    when
    	$sla : SLA($slaid : uuid); 
    then
    	System.out.println("SLA "+ $slaid + " has been inserted:");
        System.out.println($sla.toString());
 end)
salience 100
    when
    	$sla : SLA($slaid : uuid); 
    then
    	System.out.println("SLA "+ $slaid + " has been inserted:");
        System.out.println($sla.toString());
end
*/