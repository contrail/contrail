/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.slasoi.monitoring.core;

import java.util.ArrayList;

import org.slasoi.monitoring.common.configuration.ReasonerConfiguration;

import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date 25 May 2010
 */
public interface ReasoningComponentGateway {

	/**
	 * @return
	 */
	public ComponentMonitoringFeatures getComponentMonitoringFeatures();
	
	/**
	 * @param componentMonitoringFeatures
	 */
	public void setComponentMonitoringFeatures(ComponentMonitoringFeatures componentMonitoringFeatures); 
	
	/**
	 * @return
	 */
	public String getUuid();

	/**
	 * @param value
	 */
	public void setUuid(String value);

	/**
	 * @return
	 */
	public ArrayList<ReasonerConfiguration> getConfigurations();
	
	/**
	 * @param configurationId
	 */
	public void removeConfiguration(String configurationId);
	
	/**
	 * @param reasonerConfiguration
	 */
	public void addConfiguration(ReasonerConfiguration reasonerConfiguration);

	/**
	 * @param configurationId
	 * @return
	 */
	boolean startMonitoring(String configurationId);

	/**
	 * @param configurationId
	 * @return
	 */
	boolean stopMonitoring(String configurationId);

}
