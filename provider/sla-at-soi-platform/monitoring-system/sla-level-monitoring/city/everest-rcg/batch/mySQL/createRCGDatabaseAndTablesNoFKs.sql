SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
CREATE SCHEMA IF NOT EXISTS `rcg` ;
USE `mydb` ;
USE `rcg` ;

-- -----------------------------------------------------
-- Table `rcg`.`agreement_term`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`agreement_term` (
  `monitoring_configuration_sla_uuid` VARCHAR(200) NOT NULL ,
  `agreement_term_id` VARCHAR(200) NOT NULL ,
  `agreement_term_hash_code` VARCHAR(200) NOT NULL ,
  `agreement_term_theory_object` LONGBLOB NOT NULL ,
  PRIMARY KEY USING BTREE (`agreement_term_id`, `monitoring_configuration_sla_uuid`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `rcg`.`functional_expr_operator`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`functional_expr_operator` (
  `agreement_term_monitoring_configuration_sla_uuid` VARCHAR(200) NOT NULL ,
  `agreement_term_agreement_term_id` VARCHAR(200) NOT NULL ,
  `func_expr_oper_hash_code` VARCHAR(200) NOT NULL ,
  `guaranteed_state_id` VARCHAR(200) NOT NULL ,
  `variable_declaration_id` VARCHAR(200) NULL ,
  `functional_expr_operator` VARCHAR(200) NOT NULL ,
  `func_expr_oper_path` BLOB NOT NULL ,
  PRIMARY KEY (`func_expr_oper_hash_code`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `rcg`.`functional_expr_operator_parametric_template`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`functional_expr_operator_parametric_template` (
  `functional_expr_operator` VARCHAR(200) NOT NULL ,
  `parametric_theory_template` TEXT NOT NULL ,
  `conditions_holdsAt_formula_pointer` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`functional_expr_operator`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `rcg`.`monitoring_configuration`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`monitoring_configuration` (
  `sla_uuid` VARCHAR(200) NOT NULL ,
  `configuration_id` VARCHAR(200) NOT NULL ,
  `configuration_object` BLOB NOT NULL ,
  PRIMARY KEY (`sla_uuid`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `rcg`.`slasoi_interaction_event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`slasoi_interaction_event` (
  `slasoi_event_id` VARCHAR(200) NOT NULL ,
  `slasoi_interaction_event_xml` TEXT NOT NULL ,
  `everest_event_xml` TEXT NOT NULL ,
  PRIMARY KEY (`slasoi_event_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `rcg`.`slasoi_mon_result_event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rcg`.`slasoi_mon_result_event` (
  `sla_uuid` VARCHAR(200) NOT NULL ,
  `slasoi_mon_result_id` VARCHAR(200) NOT NULL ,
  `timestamp` BIGINT(10) NOT NULL ,
  `slasoi_mon_result_event_xml` TEXT NOT NULL ,
  `everest_mon_result_id` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`slasoi_mon_result_id`, `sla_uuid`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
