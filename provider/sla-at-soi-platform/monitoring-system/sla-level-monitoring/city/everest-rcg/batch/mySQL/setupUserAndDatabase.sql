-- DROP USER 'rcg'@'localhost';
CREATE USER 'rcg''@'localhost' IDENTIFIED BY 'rcg';

-- DROP DATABASE rcg
CREATE DATABASE rcg;

USE rcg;

SOURCE ./createTables.sql

GRANT ALL ON rcg.* TO 'rcg'@'localhost';