/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * @author Theocharis Tsigkritis
 * 
 * 26 Jul 2010
 */
package org.slasoi.monitoring.city.impl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.impl.InteractionEventManager;
import org.slasoi.monitoring.city.translator.Translator;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.bse.Formula;

/**
 * @author t7t
 *
 */
public class FeedEVERESTTest {

	/**
	 * @param args
	 */
	
	/*
	public static void main(String[] args) {
		// initialise logging
    	PropertyConfigurator.configure("./conf/log4j.properties");
    	Logger logger = Logger.getLogger(FeedEVERESTTest.class.getName());
    	
    	String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
    	String everestHome = System.getenv().get("EVEREST_HOME");
    	
    	 // initialise EVEREST
        NewDataAnalyzerEC analyser = new NewDataAnalyzerEC(everestHome);
    	
        // initiate db manager
        DatabaseManagerInterface databaseManager = DatabaseManagerFactory.getDatabaseManager(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties");
        // get entity manager factory
        EntityManagerFactoryInterface entityManagerFactory = databaseManager.getEntityManagerFactory();
    	
    	AgreementTermEntityManager rcgDBManager = (AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class);
//    	
//    	// initialise serenity db manager
//    	Connection serenityConnection = DatabaseManagerFactory.getDatabaseManager("./conf/uk.ac.city.soi.everest.database.properties").getConnectionManager().getConnection();
//    	EventEntityManager serenityDBManager = new EventEntityManager(serenityConnection);
//    	
//    	// initialise serenity template db manager
//    	TemplateMySQLDatabase templateManager = new TemplateMySQLDatabase(serenityConnection);
    	
    	
    	
    	// truncate tables
    	logger.debug("Truncate db tables");
    	rcgDBManager.execute("TRUNCATE rcg.agreement_term");
    	rcgDBManager.execute("TRUNCATE rcg.operator");
    	rcgDBManager.execute("TRUNCATE rcg.monitoring_configuration");
    	
//    	try {
//			serenityDBManager.executeQuery("TRUNCATE beliefgraph");
//			serenityDBManager.executeQuery("TRUNCATE event");
//			serenityDBManager.executeQuery("TRUNCATE fluent");
//			serenityDBManager.executeQuery("TRUNCATE genuineness");
//			serenityDBManager.executeQuery("TRUNCATE predicate");
//			serenityDBManager.executeQuery("TRUNCATE statistic");
//			serenityDBManager.executeQuery("TRUNCATE template");
//			serenityDBManager.executeQuery("TRUNCATE templatedefined");
//			serenityDBManager.executeQuery("TRUNCATE threatlikelihood");
//			serenityDBManager.executeQuery("TRUNCATE confirmation_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 0_01_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 01_02_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 02_03_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 03_04_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 04_05_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 05_06_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 06_07_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 07_08_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 08_09_correctness");
//			serenityDBManager.executeQuery("TRUNCATE 09_1_correctness");
//		} catch (SQLException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
		
    	
    	// use SLA example from org.slasoi.slamodel              
    	SLA sla = null;
        FileReader read;
        
        LinkedHashMap<String, LinkedHashMap<String, Formula>> agreementTermsTheories = null;
        
        try {
            //read = new FileReader("./docs/SLAs/A4_SLA-04-06-2010.xml");
            //read = new FileReader("./docs/SLAs/B4_SLA(Validated).xml");
            read = new FileReader("./docs/SLAs/B6SLA.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(sb.toString());
            
            // log the sla description as an entry 
            logger.debug("Using SLA: "+sla.getDescr());
            logger.debug("SLA object: "+sla);
            
            // initialise translator
            Translator translator = new Translator(sla, analyser);
            
            // translate
            translator.translate();
            
            System.out.println("END!!!");
            
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	
       
        // print agreement term theories
        //logger.debug("agreement term theories: "+agreementTermsTheories);
        
        
        // retrieve availability theory
//        LinkedHashMap<String, Formula> theory = rcgDBManager.getAgreementTermTheory("98989893947", "governance");
//        String agreementHashCode = rcgDBManager.getAgreementTermHashCode("98989893947", "governance");
//       
//        
//        // feed uk.ac.city.soi.everest with monitoring theory
//        try {
//			analyser.receiveFormulas(theory);
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		// initialise event manager
//		InteractionEventManager eventManager = new InteractionEventManager();
//		
//		
//		ArrayList<String> slasoiEvents = null;
//		try {
//			slasoiEvents = EventLoader.loadSLASOIEvents("./docs/B6EventsList.xml");
//			for(String slasoiEvent : slasoiEvents){
//
//				logger.debug("Event is in SLA@SOI Event Schema format");
//				logger.debug(slasoiEvent);
//				
//				String coreMessage = eventManager.toCoreMonitorFormatString(slasoiEvent);
//				
//				logger.debug("Translation of the event to CITY core event format:");
//				logger.debug(coreMessage);
//				
//				// feed uk.ac.city.soi.everest with event
//				analyser.recordEvent(coreMessage);
//				
//				
//				TypeOfEvent coreMessage = eventManager.toCoreMonitorFormatObject(slasoiEvent);
//				
//				// feed uk.ac.city.soi.everest with event
//				analyser.receiveTypeOfEventObject(coreMessage);
//				
//			}
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//        
//		
//		// get monitoring results from templateDefined serenity db table
//		// for current agreement term
//		ArrayList<Template> violationTemplates = templateManager
//				.getMonitoringResults(Constants.STATUS_SAT,
//						0l, agreementHashCode);
//		violationTemplates.addAll(templateManager
//				.getMonitoringResults(Constants.STATUS_IRB,
//						0l, agreementHashCode));
//		
//		//ArrayList<Template> violationTemplates = templateManager.selectByStatus(Constants.STATUS_SAT);
//		
//		// initialise monitoringResultGenerator
//		MonitoringResultGenerator monResultGenerator = new MonitoringResultGenerator(rcgConnection); 
//		
//		for(Template violationTemplate : violationTemplates){
//			
//			// generate a violation event instance
//			EventInstance violationEventInstance = monResultGenerator.generateAgreementTermMonResultEventInstance("TwoVMTypeTemplate", "ServiceWideGuarantees", violationTemplate);
//			
//			// de-serialise the violationEventInstance
//			MonitoringEventServiceImpl monitoringEventServiceImpl = new MonitoringEventServiceImpl();
//			String violationEventInstanceString = null;
//			try {
//				violationEventInstanceString = monitoringEventServiceImpl.marshall(violationEventInstance);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			logger.debug("SLA@SOI monitoringresult: "+violationEventInstanceString);
		}
		
	}
*/
}
