/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.impl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.translator.Parser;
import org.slasoi.monitoring.city.utils.ASTNode;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.everest.database.EntityManagerUtil;

public class NewParserTest {

	/**
	 * @param args
	 */
	/*
	public static void main(String[] args) {
		// initialise logging
    	PropertyConfigurator.configure(".\\conf\\log4j.properties");
    	Logger logger = Logger.getLogger(NewParserTest.class.getName());
        
    	// initialise rcg db manager
    	Connection rcgConnection = DatabaseManagerFactory.getDatabaseManager("./conf/rcg.database.properties").getConnectionManager().getConnection();
    	AgreementTermEntityManager rcgDBManager = new AgreementTermEntityManager(rcgConnection);
    	
    	// initialise an entityManagerUtil i.e. serenity db manager
        Connection serenityConnection = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager().getConnectionManager().getConnection();
        EntityManagerUtil serenityDBManager = new EntityManagerUtil(serenityConnection);
    	
    	// truncate tables
    	logger.debug("Truncate db tables");
    	rcgDBManager.execute("TRUNCATE rcg.agreement_term");
    	rcgDBManager.execute("TRUNCATE rcg.operator");
    	rcgDBManager.execute("TRUNCATE rcg.monitoring_configuration");
    	try {
			serenityDBManager.executeQuery("TRUNCATE beliefgraph");
			serenityDBManager.executeQuery("TRUNCATE event");
			serenityDBManager.executeQuery("TRUNCATE fluent");
			serenityDBManager.executeQuery("TRUNCATE genuineness");
			serenityDBManager.executeQuery("TRUNCATE predicate");
			serenityDBManager.executeQuery("TRUNCATE statistic");
			serenityDBManager.executeQuery("TRUNCATE template");
			serenityDBManager.executeQuery("TRUNCATE templatedefined");
			serenityDBManager.executeQuery("TRUNCATE threatlikelihood");
			serenityDBManager.executeQuery("TRUNCATE confirmation_correctness");
			serenityDBManager.executeQuery("TRUNCATE 0_01_correctness");
			serenityDBManager.executeQuery("TRUNCATE 01_02_correctness");
			serenityDBManager.executeQuery("TRUNCATE 02_03_correctness");
			serenityDBManager.executeQuery("TRUNCATE 03_04_correctness");
			serenityDBManager.executeQuery("TRUNCATE 04_05_correctness");
			serenityDBManager.executeQuery("TRUNCATE 05_06_correctness");
			serenityDBManager.executeQuery("TRUNCATE 06_07_correctness");
			serenityDBManager.executeQuery("TRUNCATE 07_08_correctness");
			serenityDBManager.executeQuery("TRUNCATE 08_09_correctness");
			serenityDBManager.executeQuery("TRUNCATE 09_1_correctness");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	// use SLA example from org.slasoi.slamodel              
    	SLA sla = null;
        FileReader read;
        try {
            //read = new FileReader("./docs/SLAs/A4_SLA-04-06-2010.xml");
            //read = new FileReader("./docs/SLAs/B4_SLA(Validated).xml");
            read = new FileReader("./docs/SLAs/B6SLA.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(sb.toString());
            
            // log the sla description as an entry 
            logger.debug("Using SLA: "+sla.getDescr());
            logger.debug("SLA object: "+sla);
            
            // initialise RCG translator parser
            Parser parser = new Parser(sla);
            
            AgreementTerm[] slaATs = sla.getAgreementTerms();
            
            //for(AgreementTerm aT : slaATs){
            AgreementTerm aT = sla.getAgreementTerms()[0];
            
        	logger.debug("input agreement term: "+ aT.getId());
        
        	ASTNode ast = parser.parse(((Object)aT),null);
        
        	logger.debug("output ast: "+ ast.preOrderPrint(ast,""));
        	
        	logger.debug("qosTermsPaths: "+parser.getQosTermsPaths());
        	
        	ArrayList<ASTNode> initialFrontier = new ArrayList<ASTNode>();
        	
        	initialFrontier.add(ast);
        	
        	ArrayList<ASTNode> resolvedTrees = parser.resolveOrIncrementally(initialFrontier, initialFrontier, initialFrontier);
        	
        	for(ASTNode resolvedTree : resolvedTrees){
        		logger.debug("output resolvedTree: "+ resolvedTree.preOrderPrint(resolvedTree,""));
        	}
        	
        	//parser.generateQoSTermsCases();
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    */
}
