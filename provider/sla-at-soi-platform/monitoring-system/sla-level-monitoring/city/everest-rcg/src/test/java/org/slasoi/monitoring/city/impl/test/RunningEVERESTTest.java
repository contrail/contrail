/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * @author Theocharis Tsigkritis
 * 
 * 26 Jul 2010
 */
package org.slasoi.monitoring.city.impl.test;

import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface;
import org.slasoi.monitoring.city.impl.InteractionEventManager;
import org.slasoi.monitoring.city.impl.MonitoringResultGenerator;
import org.slasoi.monitoring.city.utils.EventLoader;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.database.EntityManagerUtil;
import uk.ac.city.soi.everest.database.event.EventEntityManagerInterface;
import uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface;
import uk.ac.city.soi.everest.monitor.Template;

/**
 * @author t7t
 *
 */
public class RunningEVERESTTest {

    /**
     * @param args
     */
	/*
    public static void main(String[] args) {
        // initialise logging
        PropertyConfigurator.configure("./conf/log4j.properties");
        Logger logger = Logger.getLogger(RunningEVERESTTest.class.getName());
        
        String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        String everestHome = System.getenv().get("EVEREST_HOME");
        
        // initialise EVEREST
        NewDataAnalyzerEC analyser = new NewDataAnalyzerEC(everestHome);
        
        // initialise RCG
        EverestRCGWrapperForTesting rcg = new EverestRCGWrapperForTesting();
        
        // initialise serenity db manager
        EventEntityManagerInterface serenityDBManager = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager()
        .getEntityManagerFactory().getEventEntityManager();
        
        // initialise serenity template db manager
        TemplateEntityManagerInterface templateManager = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager()
        .getEntityManagerFactory().getTemplateEntityManager();
        
        Connection connection = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager().getConnectionManager().getConnection();
        EntityManagerUtil entityManagerUtil = new EntityManagerUtil(connection);
        
        // truncate tables
        logger.debug("Truncate db tables");
        try {
            entityManagerUtil.executeQuery("TRUNCATE beliefgraph");
            entityManagerUtil.executeQuery("TRUNCATE event");
            entityManagerUtil.executeQuery("TRUNCATE fluent");
            entityManagerUtil.executeQuery("TRUNCATE genuineness");
            entityManagerUtil.executeQuery("TRUNCATE predicate");
            entityManagerUtil.executeQuery("TRUNCATE statistic");
            entityManagerUtil.executeQuery("TRUNCATE template");
            entityManagerUtil.executeQuery("TRUNCATE templatedefined");
            entityManagerUtil.executeQuery("TRUNCATE threatlikelihood");
            entityManagerUtil.executeQuery("TRUNCATE confirmation_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 0_01_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 01_02_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 02_03_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 03_04_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 04_05_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 05_06_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 06_07_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 07_08_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 08_09_correctness");
            entityManagerUtil.executeQuery("TRUNCATE 09_1_correctness");
        } catch (SQLException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        
        
        
        // initiate db manager
        DatabaseManagerInterface databaseManager = DatabaseManagerFactory.getDatabaseManager(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties");
        // get entity manager factory
        EntityManagerFactoryInterface entityManagerFactory = databaseManager.getEntityManagerFactory();
        
        // initiate agreementTermStorer
        AgreementTermEntityManagerInterface agreementTermStorer = (AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class);
        //LinkedHashMap<String, Formula> theory = rcgDBManager.getAgreementTermTheory("98989893947", "governance");
        
        // retrieve availability theory
        LinkedHashMap<String, Formula> theory = agreementTermStorer.getAgreementTermTheory("TwoVMTypeTemplate", "ServiceWideGuarantees");
        String availabilityAgreementHashCode = agreementTermStorer.getAgreementTermHashCode("TwoVMTypeTemplate", "ServiceWideGuarantees");
        
        // feed uk.ac.city.soi.everest with monitoring theory
        try {
            analyser.receiveFormulas(theory);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // initialise event manager
        InteractionEventManager eventManager = new InteractionEventManager();
        
        
        ArrayList<String> slasoiEvents = null;
        try {
            slasoiEvents = EventLoader.loadSLASOIEvents(".\\docs\\a4Event.xml");
            for(String slasoiEvent : slasoiEvents){

                logger.debug("Event is in SLA@SOI Event Schema format");
                logger.debug(slasoiEvent);
                
                TypeOfEvent coreMessage = eventManager.toCoreMonitorFormatObject(slasoiEvent);
                
                // feed uk.ac.city.soi.everest with event
                analyser.receiveTypeOfEventObject(coreMessage);
                
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        
        ArrayList<Template> violationTemplates = templateManager.selectByStatus(Constants.STATUS_IRB);
        
        // initialise monitoringResultGenerator
        MonitoringResultGenerator monResultGenerator = new MonitoringResultGenerator(); 
        
        for(Template violationTemplate : violationTemplates){
            
            // generate a violation event instance
            EventInstance violationEventInstance = monResultGenerator.generateAgreementTermMonResultEventInstance("TwoVMTypeTemplate", "ServiceWideGuarantees", violationTemplate);
            
            // de-serialise the violationEventInstance
            MonitoringEventServiceImpl monitoringEventServiceImpl = new MonitoringEventServiceImpl();
            String violationEventInstanceString = null;
            try {
                violationEventInstanceString = monitoringEventServiceImpl.marshall(violationEventInstance);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            logger.debug("SLA@SOI monitoringresult: "+violationEventInstanceString);
        }
        
    }
	*/
}
