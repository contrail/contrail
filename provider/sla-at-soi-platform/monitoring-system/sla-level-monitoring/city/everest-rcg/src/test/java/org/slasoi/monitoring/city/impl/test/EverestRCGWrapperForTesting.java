/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.monitoring.city.impl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.city.impl.EverestRCG;
import org.slasoi.monitoring.city.translator.Translator;
import org.slasoi.slamodel.sla.SLA;

/**
 * @author Davide Lorenzoli
 * 
 * @date Nov 25, 2010
 */
public class EverestRCGWrapperForTesting extends EverestRCG {

	public EverestRCGWrapperForTesting() {
		super();
	}
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * This method has been overwritten for testing purpose, it receives in input
	 * a file location of a SLA XML, instead of a configuration id.
	 * 
	 * @see org.slasoi.monitoring.city.impl.EverestRCG#startMonitoring(java.lang.String)
	 */
	@Override
	public boolean startMonitoring(String slaXmlLocation) {
        SLA sla = null;
        FileReader read = null;

        try {
            read = new FileReader(slaXmlLocation);

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            //System.out.println("999999999 " + sb.toString());
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(sb.toString());

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        /*
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		*/
        // initialise rcg translator
        setTranslator(new Translator(sla, getNdaec()));

        // translate sla agreement terms
        getTranslator().translate();

        // get sla uuid
        String sla_uuid = sla.getUuid().toString();

        // feedEverestWithMonitoringSpecifications
        this.feedEverestWithMonitoringSpecifications(sla_uuid);
        
        return true;
    }
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
