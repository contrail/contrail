/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.service.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.impl.test.EverestRCGWrapperForTesting;
import org.slasoi.monitoring.city.utils.EventLoader;
import org.slasoi.monitoring.city.utils.ParametricTemplateHandler;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everest.database.EntityManagerUtil;

public class CityRCGA4Test extends TestCase {
	/*
	private Logger logger = Logger.getLogger(CityRCGA4Test.class.getName());
    private EverestRCGWrapperForTesting rcg;

    public void setUp() {
       
        String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        
        rcg = new EverestRCGWrapperForTesting();
        
        // initialise logging
        PropertyConfigurator.configure(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/log4j.properties");
        Logger logger = Logger.getLogger(CityRCGA4Test.class.getName());

        // initiate db manager
        DatabaseManagerInterface databaseManager =
                DatabaseManagerFactory.getDatabaseManager(slasoiOrcHome
                        + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties");
        // get entity manager factory
        EntityManagerFactoryInterface entityManagerFactory = databaseManager.getEntityManagerFactory();

        AgreementTermEntityManager rcgDBManager =
                (AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class);

        // truncate tables
        logger.debug("Truncate db tables");
        rcgDBManager.execute("TRUNCATE rcg.agreement_term");
        rcgDBManager.execute("TRUNCATE rcg.operator");
        rcgDBManager.execute("TRUNCATE rcg.monitoring_configuration");
        rcgDBManager.execute("TRUNCATE rcg.functional_expr_operator_parametric_template");
        rcgDBManager.execute("TRUNCATE rcg.slasoi_interaction_event");
        rcgDBManager.execute("TRUNCATE rcg.slasoi_mon_result_event");
        
        // initialise an entityManagerUtil i.e. serenity db manager
        Connection connection = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager().getConnectionManager().getConnection();
        EntityManagerUtil entityManagerUtil = new EntityManagerUtil(connection);
        
        // truncate serenity db tables
        logger.debug("Truncate db tables");
        try {
            entityManagerUtil.executeQuery("TRUNCATE event");
            entityManagerUtil.executeQuery("TRUNCATE fluent");
            entityManagerUtil.executeQuery("TRUNCATE predicate");
            entityManagerUtil.executeQuery("TRUNCATE statistic");
            entityManagerUtil.executeQuery("TRUNCATE template");
            entityManagerUtil.executeQuery("TRUNCATE templatedefined");
        } catch (SQLException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

    }

    public void testCityRcg() {
        String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        
        // initialise logging
        PropertyConfigurator.configure(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/log4j.properties");

        ParametricTemplateHandler handler = new ParametricTemplateHandler();
        handler.insertQoSTerm(common.mttr.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricMTTRTheory.xml",
                "A5.MTTR");
        handler.insertQoSTerm(core.time_is.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricTimeIsTheory.xml",
                "A1.TimeIs");
        handler.insertQoSTerm(core.day_is.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricDayIsTheory.xml",
                "A1.DayIs");
        handler.insertQoSTerm(core.series.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricSeriesTheory.xml",
                "A2.Series");
        handler.insertQoSTerm(common.availability.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricAvailabilityTheory.xml", "A6.Availability");

        assertTrue(rcg.startMonitoring(slasoiOrcHome
                + "/monitoring-system/sla-level-monitoring/city/test/A4_SLA-04-06-2010.xml"));

        
         //test RCG event translation capabilities
         

        // prepare MessageEvent objects
        ArrayList<MessageEvent> messageEvents = new ArrayList<MessageEvent>();

        ArrayList<String> slasoiEvents;
        try {
            slasoiEvents = EventLoader.loadSLASOIEvents(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/test/a4Event.xml");

            for (String slasoiEvent : slasoiEvents) {
                PubSubMessage pubSubMessage = new PubSubMessage("myChannel", slasoiEvent);
                Object source = "mySource";
                MessageEvent messageEvent = new MessageEvent(source, pubSubMessage);
                messageEvents.add(messageEvent);
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (MessageEvent mesEvent : messageEvents) {
            rcg.feedEverestWithEvent(mesEvent);
        }

        
         // End of testing RCG event translation capabilities
         
    }
    */
}
