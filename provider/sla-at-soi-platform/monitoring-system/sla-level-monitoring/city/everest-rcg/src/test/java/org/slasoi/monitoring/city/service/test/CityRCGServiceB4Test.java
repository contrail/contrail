/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.service.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManager;
import org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManagerInterface;
import org.slasoi.monitoring.city.database.slasoiMonitoringResultEvent.SlasoiMonitoringResultEventEntityManager;
import org.slasoi.monitoring.city.database.slasoiMonitoringResultEvent.SlasoiMonitoringResultEventEntityManagerInterface;
import org.slasoi.monitoring.city.impl.MonitoringResultGenerator;
import org.slasoi.monitoring.city.impl.test.EverestRCGWrapperForTesting;
import org.slasoi.monitoring.city.utils.EventLoader;
import org.slasoi.monitoring.city.utils.ParametricTemplateHandler;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.database.EntityManagerUtil;
import uk.ac.city.soi.everest.database.event.EventEntityManagerInterface;
import uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface;
import uk.ac.city.soi.everest.monitor.Template;

public class CityRCGServiceB4Test extends TestCase {
	private ApplicationContext ac;

	public void setUp() {
		ac =
            new FileSystemXmlApplicationContext(
                    "src/main/resources/META-INF/spring/cityrcgservice.xml");
		
		String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        
        // initialise logging
        PropertyConfigurator.configure(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/log4j.properties");
        Logger logger = Logger.getLogger(CityRCGServiceB4Test.class.getName());
        
		// initiate rcg db manager
        DatabaseManagerInterface databaseManager = DatabaseManagerFactory.getDatabaseManager(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties");
        // get entity manager factory
        EntityManagerFactoryInterface entityManagerFactory = databaseManager.getEntityManagerFactory();
        
        AgreementTermEntityManager rcgDBManager = (AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class);
        
        // truncate rcg db tables
        logger.debug("Truncate RCG db tables");
        rcgDBManager.execute("TRUNCATE agreement_term");
        rcgDBManager.execute("TRUNCATE operator");
        rcgDBManager.execute("TRUNCATE monitoring_configuration");
        rcgDBManager.execute("TRUNCATE functional_expr_operator_parametric_template");
        rcgDBManager.execute("TRUNCATE slasoi_interaction_event");
        rcgDBManager.execute("TRUNCATE slasoi_mon_result_event");
        
        // initialise an entityManagerUtil i.e. serenity db manager
        Connection connection = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager().getConnectionManager().getConnection();
        EntityManagerUtil entityManagerUtil = new EntityManagerUtil(connection);
        
        // truncate serenity db tables
        logger.debug("Truncate Serenity db tables");
        try {
            entityManagerUtil.executeQuery("TRUNCATE event");
            entityManagerUtil.executeQuery("TRUNCATE fluent");
            entityManagerUtil.executeQuery("TRUNCATE predicate");
            entityManagerUtil.executeQuery("TRUNCATE statistic");
            entityManagerUtil.executeQuery("TRUNCATE template");
            entityManagerUtil.executeQuery("TRUNCATE templatedefined");
        } catch (SQLException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        
	}
	
	public void testCityRcg() {
	    String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
        
        // initialise logging
	    
        PropertyConfigurator.configure(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/log4j.properties");
        Logger logger = Logger.getLogger(CityRCGServiceB4Test.class.getName());
        
		EverestRCGWrapperForTesting service = new EverestRCGWrapperForTesting();
		
		/*
		ParametricTemplateHandler handler = new ParametricTemplateHandler();
        handler.insertQoSTerm(common.mttr.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricMTTRTheory.xml",
                "A5.MTTR");
//        handler.insertQoSTerm(core.time_is.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricTimeIsTheory.xml",
//                "A1.TimeIs");
//        handler.insertQoSTerm(core.day_is.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricDayIsTheory.xml",
//                "A1.DayIs");
//        handler.insertQoSTerm(core.series.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricSeriesTheory.xml",
//                "A2.Series");
        handler.insertQoSTerm(common.availability.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricAvailabilityTheory.xml", "A6.Availability");
        
        handler.insertQoSTerm(common.throughput.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricThroughputTheory.xml",
				"A4.Throughput");         
        handler.insertQoSTerm(common.mttf.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricMTTFTheory.xml",
        		"A4.MTTF");
        handler.insertQoSTerm(common.reliability.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricReliabilityTheory.xml", "R4.Reliability");
        handler.insertQoSTerm(common.completion_time.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricCompletionTimeTheory.xml", "A1.completion_time");
        handler.insertQoSTerm(common.arrival_rate.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricArrivalRateTheory.xml", "R4.arrival_rate");
        handler.insertQoSTerm(common.accessibility.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricAccessibilityTheory.xml", "R5.Accessibility");
        handler.insertQoSTerm(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE, slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricDefaultTheory.xml",
        "A1.default_template");
	*/
        		
		assertNotNull(service);
		
		assertTrue(service.startMonitoring(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/test/Test_SLA_1_My.xml"));
		
		//assertTrue(service.startMonitoring(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/test/Test_SLA_2_My.xml"));
		
		//assertTrue(service.startMonitoring(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/test/Test_B6SLA1.xml"));
				
		//assertTrue(service.startMonitoring(slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/test/ORC_Business-SLAT (corrected).xml"));
		
		/*
         * test RCG event translation capabilities
         */

        // prepare MessageEvent objects
        ArrayList<MessageEvent> messageEvents = new ArrayList<MessageEvent>();

        ArrayList<String> slasoiEvents;
        try {
            slasoiEvents = EventLoader.loadSLASOIEvents(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/test/Test_SLA_1_Events_My_1.xml");
            //System.out.println("4444444 Size of events " + slasoiEvents.size());
        	//slasoiEvents = EventLoader.loadSLASOIEvents(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/test/Test_B6SLA1_Events.xml");

            for (String slasoiEvent : slasoiEvents) {
                PubSubMessage pubSubMessage = new PubSubMessage("myChannel", slasoiEvent);
                Object source = "mySource";
                MessageEvent messageEvent = new MessageEvent(source, pubSubMessage);
                messageEvents.add(messageEvent);
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        int ec = 0;
        for (MessageEvent mesEvent : messageEvents) {
        	logger.debug("$$$$$$$$ Sending Event no " + ++ec);
            service.feedEverestWithEvent(mesEvent);
        }

        //service.stopMonitoring("1234");
        /*
         * End of testing RCG event translation capabilities
         */
		
        // test monitoring result translation
        // initialise serenity db manager
        EventEntityManagerInterface serenityDBManager = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager()
        .getEntityManagerFactory().getEventEntityManager();
        
        // initialise serenity template db manager
        TemplateEntityManagerInterface templateManager = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager()
        .getEntityManagerFactory().getTemplateEntityManager();
        
        // initialise slasoiMonitoringResultEventStorer
        SlasoiMonitoringResultEventEntityManagerInterface slasoiMonitoringResultEventStorer = (SlasoiMonitoringResultEventEntityManager)RCGProperties.getProperty(RCGConstants.SLASOI_MONITORING_RESULT_EVENT_STORER_KEY);
        
        ArrayList<Template> violationTemplates = templateManager.selectByStatus(Constants.STATUS_IRB);
        
        // initialise monitoringResultGenerator
        MonitoringResultGenerator monResultGenerator = new MonitoringResultGenerator(); 
        
        for(Template violationTemplate : violationTemplates){
            
            // generate a violation event instance
            //EventInstance violationEventInstance = monResultGenerator.generateAgreementTermMonResultEventInstance("TwoVMTypeTemplate", "ServiceWideGuarantees", violationTemplate);
            EventInstance violationEventInstance = monResultGenerator.generateAgreementTermMonResultEventInstance("98989893947", "Monitorable", violationTemplate);
            
            // de-serialise the violationEventInstance
            MonitoringEventServiceImpl monitoringEventServiceImpl = new MonitoringEventServiceImpl();
            String violationEventInstanceString = null;
            try {
                violationEventInstanceString = monitoringEventServiceImpl.marshall(violationEventInstance);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            logger.debug("SLA@SOI monitoringresult: "+violationEventInstanceString);
            
            // store slasoi monitoring result event to db
            slasoiMonitoringResultEventStorer.addMonResultEvent("test", ((Integer)violationEventInstance.hashCode()).toString(), System.currentTimeMillis(), "test", "test");
         
        }
        
        // test method for retrieving everest events objects from db
        // initialise slasoiMonitoringResultEventStorer
        SlasoiInteractionEventEntityManagerInterface slasoiInteractionEventStorer = (SlasoiInteractionEventEntityManager)RCGProperties.getProperty(RCGConstants.SLASOI_INTERACTION_EVENT_STORER_KEY);
        
        ArrayList<TypeOfEvent> everestEvents = slasoiInteractionEventStorer.getAllEverestEvents();
        
        logger.debug("everestEvents: "+everestEvents);
        
        // test checkPeriodicallyForMonitoringResults
        //service.checkPeriodicallyForMonitoringResults("TwoVMTypeTemplate", service.getNdaec());
        //service.checkPeriodicallyForMonitoringResults();
        
	}
	
	public static void main(String[] args){
		CityRCGServiceB4Test test = new CityRCGServiceB4Test();
		test.setUp();
		test.testCityRcg();
	}
}

