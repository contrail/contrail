/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.impl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.city.translator.ASTTranslator;
import org.slasoi.monitoring.city.translator.Instantiator;
import org.slasoi.monitoring.city.translator.Parser;
import org.slasoi.monitoring.city.utils.ASTNode;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.everest.NewDataAnalyzerEC;


public class EVERESTRCGTranslatorTest {

	/**
	 * @param args
	 */
	/*
	public static void main(String[] args) {
		// initialise logging
    	PropertyConfigurator.configure("./conf/log4j.properties");
    	Logger logger = Logger.getLogger(EVERESTRCGTranslatorTest.class.getName());
        
    	String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
    	String everestHome = System.getenv().get("EVEREST_HOME");
    	
    	 // initialise EVEREST
        NewDataAnalyzerEC analyser = new NewDataAnalyzerEC(everestHome);
    	
    	// use SLA example from org.slasoi.slamodel              
    	SLA sla = null;
        FileReader read;
        try {
            //read = new FileReader("./docs/SLAs/A4_SLA-04-06-2010.xml");
            //read = new FileReader("./docs/SLAs/B4_SLA.xml");
            read = new FileReader("./docs/SLAs/B6SLA.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(sb.toString());
            
            // log the sla description as an entry 
            logger.debug("Using SLA: "+sla.getDescr());
            logger.debug("SLA object: "+sla);
            
            // initialise RCG translator parser
            Parser parser = new Parser(sla);
            
            AgreementTerm[] slaATs = sla.getAgreementTerms();
            
            //for(AgreementTerm aT : slaATs){
            AgreementTerm aT = sla.getAgreementTerms()[0];
            
        	logger.debug("input agreement term: "+ aT.getId());
        
        	ASTNode ast = parser.parse(((Object)aT),null);
        
        	logger.debug("output ast: "+ ast.preOrderPrint(ast,""));
        	
        	logger.debug("qosTermsPaths: "+parser.getQosTermsPaths());
            //}
            
        	parser.generateQoSTermsCases();
        	
        	// initialise RCG translator AST translator
            ASTTranslator astTranslator = new ASTTranslator(parser.getQosTermsPaths(), parser.getQosTermsCases(), sla, analyser);
            
            // get parametric templates for the output ast
            astTranslator.retrieveParametricTemplates(ast, ast);
            logger.debug("ast parametricTemplates[]: "+astTranslator.getParametricTemplates());
            
            
//            if(astTranslator.getParametricTemplates().size()>0){
//	            // initialise instantiator
//	            Instantiator instantiator = new Instantiator(sla,
//	            		astTranslator.getParametricTemplates(), parser.getQosTermsCases(),
//	        			"ftercs");
//	            
//	            // instantiate the parametric templates
//	            instantiator.generateInstantiatedFormulasFromParametricTemplates();
//	            
//	            // get formulas
//	            logger.debug("Nodes = Formulas: "+instantiator.getQosTermsFormulas().toString());
//	            
//	            // compile monitoring rule
//	            logger.debug("Monitoring rule info: ");
//	            astTranslator.compileMonitoringRule(ast, ast, instantiator.getQosTermsFormulas());
//	            
//	            // get monitoring rule body
//	            logger.debug("Monitoring rule body: "+ astTranslator.getMonitoringRuleBody().toString());
//	            
//	            // get monitoring rule head
//	            logger.debug("Monitoring rule head: "+ astTranslator.getMonitoringRuleHead().toString());
//	            
//	            // get monitoring rule
//	            logger.debug("Monitoring rule: "+ astTranslator.getMonitoringRule().toString());
//	            
//	            // generate assigned expression case 
//	            parser.generateAssignedExpressionsCases();
//	          
//	            // compile operationalMonitoringTheory
//	            logger.debug("operationalMonitoringTheory: " + astTranslator.compileOperationalMonitoringTheory((String)parser.getAssignedExpressionsCases().get(sla), 
//	            		instantiator.getQosTermsFormulas()));
//	            }
//	            
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }               

	}
	*/

}
