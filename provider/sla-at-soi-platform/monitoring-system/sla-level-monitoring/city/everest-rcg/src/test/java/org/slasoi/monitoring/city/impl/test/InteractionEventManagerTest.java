/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * @author Theocharis Tsigkritis
 * 
 * 22 Jul 2010
 */
package org.slasoi.monitoring.city.impl.test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.monitoring.city.impl.InteractionEventManager;
import org.slasoi.monitoring.city.utils.EventLoader;

import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;


/**
 * @author t7t
 *
 */
public class InteractionEventManagerTest {

	
	/**
	 * @param args
	 */
	/*
	public static void main(String[] args) {
		
		// initialise logging
		PropertyConfigurator.configure(".\\conf\\log4j.properties");
		Logger logger = Logger.getLogger(InteractionEventManagerTest.class.getName());
		
		// initialise event manager
		InteractionEventManager eventManager= new InteractionEventManager();
		
		
		ArrayList<String> slasoiEvents = null;
		try {
			slasoiEvents = EventLoader.loadSLASOIEvents(".\\docs\\B6EventsList.xml");
			for(String slasoiEvent : slasoiEvents){

				logger.debug("Event is in SLA@SOI Event Schema format");
				logger.debug(slasoiEvent);
				
				TypeOfEvent coreMessage = eventManager.toCoreMonitorFormatObject(slasoiEvent);
				logger.debug("coreMessage: "+coreMessage);
				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	*/
}
