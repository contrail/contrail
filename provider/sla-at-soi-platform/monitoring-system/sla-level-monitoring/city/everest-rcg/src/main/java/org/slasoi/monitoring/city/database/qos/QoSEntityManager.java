/**
 * 
 */
package org.slasoi.monitoring.city.database.qos;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 3, 2011
 */
public class QoSEntityManager extends EntityManagerCommons implements EntityManagerInterface<QoS> {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private final static String EVEREST_RCG_DATABASE = "slasoi-everest-rcg";
	private final static String EVEREST_CORE_DATABASE = "slasoi-everest-core";
	private final static String EVEREST_RCG_OPERATIOR_TABLE = "operator";
	private final static String EVEREST_CORE_FLUENT_TABLE = "fluent";
		
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public QoSEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}

	/**
	 * @param connection
	 */
	public QoSEntityManager(Connection connection) {
		super(connection, EVEREST_RCG_OPERATIOR_TABLE);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * Returns the last inserted QoS with timestamp less equal to 'timestamp',
	 * <code>null</code> if no QoS is found.
	 * 
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @param timestamp
	 * @return
	 */
	public QoS selectLastByTimestamp(String slaId, String guaranteedId, String qosId, long timestamp) {
		QoS result = null;
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT fluent.predicateObject AS predicateObject " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0 AND timestamp <= ? " +
					   "ORDER BY timestamp DESC LIMIT 1";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);
            pstmt.setLong(4, timestamp);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				Blob blob = resultSet.getBlob("predicateObject");
				Predicate predicate = (Predicate) toObject(blob);

				try {
					result = new QoS(Double.parseDouble(predicate.getFluent().getVariable().getValue().toString()), Double.class);
				} catch (ClassCastException e) {
					logger.debug(e.getMessage(), e);
				} catch (NumberFormatException e) {
					logger.debug(e.getMessage(), e);
				}
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;		
	}
	
	/**
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @param numberOfQoS
	 * @return
	 */
	public ArrayList<QoS> selectLastN(String slaId, String guaranteedId, String qosId, long numberOfQoS) {
		ArrayList<QoS> result = new ArrayList<QoS>();
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT fluent.predicateObject AS predicateObject " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0 " +
					   "ORDER BY timestamp DESC LIMIT ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);
            pstmt.setLong(4, numberOfQoS);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				Blob blob = resultSet.getBlob("predicateObject");
				Predicate predicate = (Predicate) toObject(blob);
				
				//System.out.println(predicate.getFluent().getFluentName() + ":and value:" + predicate.getFluent().getVariable().getValue());

				try {
					result.add(new QoS(Double.parseDouble(predicate.getFluent().getVariable().getValue().toString()), Double.class));
				} catch (ClassCastException e) {
					logger.debug(e.getMessage(), e);
				} catch (NumberFormatException e) {
					logger.debug(e.getMessage(), e);
				}
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;	
	}
	
	/**
	 * Returns the last inserted QoS according to qosName and specificationIds
	 * 
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @return
	 */
	public QoS selectLast(String slaId, String guaranteedId, String qosId) {
		QoS result = null;
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT fluent.predicateObject AS predicateObject " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0 " +
					   "ORDER BY timestamp DESC LIMIT 1";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				Blob blob = resultSet.getBlob("predicateObject");
				Predicate predicate = (Predicate) toObject(blob);

				try {
					result = new QoS(Double.parseDouble(predicate.getFluent().getVariable().getValue().toString()), Double.class);
				} catch (ClassCastException e) {
					logger.debug(e.getMessage(), e);
				} catch (NumberFormatException e) {
					logger.debug(e.getMessage(), e);
				}
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @param timestamp
	 * @param limit
	 * @return
	 */
	public ArrayList<QoS> selectByName(String slaId, String guaranteedId, String qosId, long timestamp, int limit) {
		ArrayList<QoS> result = new ArrayList<QoS>();
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT fluent.predicateObject AS predicateObject " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0 AND timestamp <= ? " +
					   "ORDER BY timestamp DESC LIMIT ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);
            pstmt.setLong(4, timestamp);
            pstmt.setInt(5, limit);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				Blob blob = resultSet.getBlob("predicateObject");
				Predicate predicate = (Predicate) toObject(blob);

				try {
					result.add(new QoS(Double.parseDouble(predicate.getFluent().getVariable().getValue().toString()), Double.class));
				} catch (ClassCastException e) {
					logger.debug(e.getMessage(), e);
				} catch (NumberFormatException e) {
					logger.debug(e.getMessage(), e);
				}
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * Counts inserted QoS with timestamp less equal to 'timestamp'
	 * 
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @param timestamp
	 * @return
	 */
	public int count(String slaId, String guaranteedId, String qosId, long timestamp) {
		int result = 0;
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT COUNT(fluent.predicateObject) " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0 AND timestamp <= ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);
            pstmt.setLong(4, timestamp);

            logger.debug(pstmt);
            
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = rs.getInt("COUNT(fluent.predicateObject)");
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * @param slaId
	 * @param guaranteedId
	 * @param qosId
	 * @return
	 */
	public int count(String slaId, String guaranteedId, String qosId) {
		int result = 0;
		// the qosId is converted to the format used by everest-rcg
		String regularExpression = qosId.substring(qosId.lastIndexOf("#")+1) + "$";
		
		PreparedStatement pstmt;
		
		String query = "SELECT COUNT(fluent.predicateObject) " +
				       "FROM `" + EVEREST_RCG_DATABASE + "`.`" + EVEREST_RCG_OPERATIOR_TABLE + "` AS operator JOIN `" + EVEREST_CORE_DATABASE + "`.`" +  EVEREST_CORE_FLUENT_TABLE + "` AS fluent " +
					   "WHERE fluent.name REGEXP operator.operator_hash_code AND status = 'INITIATE' AND " +
					   "      operator.agreement_term_monitoring_configuration_sla_uuid = ? AND " +
					   "      guaranteed_state_id = ? AND fluent.name REGEXP ? AND timestamp > 0";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, regularExpression);

            logger.debug(pstmt);
            
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = rs.getInt("COUNT(fluent.predicateObject)");
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;		
	}
	
	// ------------------------------------------------------------------------
	// 							NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(QoS arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	public ArrayList<QoS> executeQuery(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	public int executeUpdate(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#getDatabaseTable()
	 */
	public String getDatabaseTable() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(QoS arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<QoS> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public QoS selectByPrimaryKey(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#setDatabaseTable(java.lang.String)
	 */
	public void setDatabaseTable(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(QoS arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
