/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.utils;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI check style: YES
 *        JavaDoc: YES
 */

import java.util.ArrayList;

/**
 * ASTNode is a utility class for modelling abstract syntax trees.
 * 
 */
public class ASTNode {

    private String type;
    private String label;
    private Object object;
    private ArrayList<ASTNode> children;

    /**
     * Empty constructor.
     */
    public ASTNode() {
        this.type = null;
        this.label = null;
        this.object = null;
        this.children = new ArrayList<ASTNode>();
    }

    /**
     * Retrieves the node type.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the node type.
     * 
     * @param type -
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Retrieves the node label.
     * 
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the node label.
     * 
     * @param label -
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Retrieves the node object.
     * 
     * @return
     */
    public Object getObject() {
        return object;
    }

    /**
     * Sets the node object.
     * 
     * @param object -
     *            the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }

    /**
     * Retrieves the node children.
     * 
     * @return
     */
    public ArrayList<ASTNode> getChildren() {
        return children;
    }

    /**
     * Sets the node children.
     * 
     * @param children -
     *            the array list of the node children to set
     */
    public void setChildren(ArrayList<ASTNode> children) {
        this.children = children;
    }

    /**
     * Adds a child to the node children list.
     * 
     * @param node -
     *            the node child to set
     */
    public void addChild(ASTNode node) {
        this.children.add(node);
    }

    /**
     * Returns a boolean indicating whether this node has as child the input node.
     * 
     * @param node-
     *            the input node
     * @return
     */
    public boolean hasChild(ASTNode node) {
        boolean hasChild = false;

        if (this.getChildren().contains(node)) {
            hasChild = true;
        }

        return hasChild;
    }

    /**
     * Returns a boolean indicating whether this node has as descendent the input node.
     * 
     * @param node -
     *            the input node
     * @return
     */
    public boolean hasDescendent(ASTNode node) {
        boolean hasDescendendent = false;

        if (this.getChildren().contains(node)) {
            hasDescendendent = true;
        }
        else {
            for (ASTNode child : this.getChildren()) {
                if (child.hasDescendent(node)) {
                    hasDescendendent = true;
                    break;
                }
            }
        }

        return hasDescendendent;
    }

    /**
     * Utility node for printing the ASTNode root by pre-order traversal.
     * 
     * @param root -
     *            the node to be printed
     * @param output -
     *            the output string
     * @return
     */
    public String preOrderPrint(ASTNode root, String output) {
        output = root.getLabel() + "(" + root.getType() + ")";
        output = output + "[";
        if (root.getChildren().size() > 0) {
            for (ASTNode child : root.getChildren()) {
                if (child != null) {
                    output = output + child.preOrderPrint(child, output);
                }
                else {
                    output = output + "null";
                }
                output = output + ", ";
            }
        }
        output = output + "]";
        return output;
    }

    /**
     * Returns the number of node sub-nodes.
     * 
     * @param noOfNodes -
     *            the number of sub nodes
     * @return
     */
    public int countSubNodes(int noOfNodes) {
        if (this != null) {
            noOfNodes = noOfNodes + 1;
        }

        // visit next node
        if (this.getChildren().size() > 0) {
            for (ASTNode child : this.getChildren()) {
                if (child != null) {
                    noOfNodes = child.countSubNodes(noOfNodes);
                }
            }
        }

        return noOfNodes;
    }

    /**
     * Returns a deep copy of this node.
     * 
     * @return
     */
    public ASTNode deepCopy() {

        ASTNode nodeDeepCopy = null;
        if (this != null) {
            if (this instanceof ASTBinaryNode) {
            	//System.out.println("====>Printing " + this.getLabel().toString() + "  " + this.getType());
                nodeDeepCopy = new ASTBinaryNode();

                nodeDeepCopy.setType(this.getType());
                nodeDeepCopy.setLabel(this.getLabel());
                nodeDeepCopy.setObject(this.getObject());
                if (this.getChildren().size() > 0) {
                    if (this.getChildren().get(0) != null) {
                        ((ASTBinaryNode) nodeDeepCopy).setLeftChild(this.getChildren().get(0).deepCopy());
                    }
                    else {
                        ((ASTBinaryNode) nodeDeepCopy).setLeftChild(null);
                    }
                    if (this.getChildren().get(1) != null) {
                        ((ASTBinaryNode) nodeDeepCopy).setRightChild(this.getChildren().get(1).deepCopy());
                    }
                    else {
                        ((ASTBinaryNode) nodeDeepCopy).setRightChild(null);
                    }
                }
            }
            else {
                nodeDeepCopy = new ASTNode();

                nodeDeepCopy.setType(this.getType());
                nodeDeepCopy.setLabel(this.getLabel());
                nodeDeepCopy.setObject(this.getObject());

                if (this.getChildren().size() > 0) {
                    for (ASTNode child : this.getChildren()) {
                        if (child != null) {
                            nodeDeepCopy.addChild(child.deepCopy());
                        }
                        else {
                            nodeDeepCopy.addChild(null);
                        }
                    }
                }
            }
        }
        return nodeDeepCopy;
    }

    /**
     * Returns a boolean indicating whether this node is equal to the input node.
     * 
     * @param node -
     *            the input node
     * @return
     */
    public boolean isEqualTo(ASTNode node) {
        boolean equals = false;

        if (this != null && node != null) {
            if (this.getLabel() != null && node.getLabel() != null && this.getLabel().equals(node.getLabel())
                    && this.getType().equals(node.getType()) && this.getObject().equals(node.getObject())) {

                if (this.getChildren().size() == node.getChildren().size()) {
                    if (this.getChildren().size() == 0) {
                        equals = true;
                    }
                    else {
                        for (int i = 0; i < this.getChildren().size(); i++) {
                            ASTNode child = this.getChildren().get(i);
                            ASTNode nodeChild = node.getChildren().get(i);
                            if (child != null && nodeChild != null) {
                                if (child.isEqualTo(nodeChild)) {
                                    equals = true;
                                }
                                else {
                                    equals = false;
                                    break;
                                }
                            }
                            else {
                                equals = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if (this == null && node == null) {
            equals = true;
        }

        return equals;
    }

    /**
     * Returns a boolean indicating whether this node contains the input node in its sub-tree.
     * 
     * @param node -
     *            the input node
     * @return
     */
    public boolean contains(ASTNode node) {
        boolean contains = false;
        if (this != null) {
            if (this.isEqualTo(node)) {
                contains = true;
            }
            else {
                if (this.getChildren().size() > 0) {
                    for (ASTNode child : this.getChildren()) {
                        if (child != null) {
                            contains = child.contains(node);
                            if (contains) {
                                break;
                            }
                        }
                    }
                }
            }
        }

        return contains;
    }

}
