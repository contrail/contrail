/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.slasoiInteractionEvent;

/**
 * @author SLA@SOI (City)
 *
 * @date June 28, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;

/**
 * This class provides methods for managing the SlasoiInteractionEvent MySQL database table.
 * 
 */
public class SlasoiInteractionEventEntityManager extends EntityManagerCommons implements EntityManagerInterface,
        SlasoiInteractionEventEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(SlasoiInteractionEventEntityManager.class);

    /**
     * Constructor.
     * 
     * @param connection
     */
    public SlasoiInteractionEventEntityManager(Connection connection) {
        super(connection);
    }

    /**
     * @see org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManagerInterface#addEvent(java.lang.String,
     *      java.lang.String, uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent)
     */
    public void addEvent(String slasoiEventId, String slasoiInteractionEventXml, TypeOfEvent everestEventObject) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        Blob everestEventObjectBlob = null;
        try {
            everestEventObjectBlob = toBlob(everestEventObject);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        /*
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		*/
        try {
            pstmt =
                    getConnection().prepareStatement(
                            "INSERT INTO slasoi_interaction_event "
                                    + "(slasoi_event_id, slasoi_interaction_event_xml, everest_event_object) "
                                    + "VALUES (?, ?, ?)");

            pstmt.setString(1, slasoiEventId);
            pstmt.setString(2, slasoiInteractionEventXml);
            pstmt.setBlob(3, everestEventObjectBlob);

            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

    }

    /**
     * @see org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManagerInterface#getAllEverestEvents()
     */
    public ArrayList<TypeOfEvent> getAllEverestEvents() {
        ArrayList<TypeOfEvent> everestEvents = new ArrayList<TypeOfEvent>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement("SELECT * FROM slasoi_interaction_event");

            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("everest_event_object");
                    TypeOfEvent event = null;
                    try {
                        event = (TypeOfEvent) toObject(blob);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }     
                    /*
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (ClassNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
 					*/
 
                    if (event != null) {
                        everestEvents.add(event);
                    }
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return everestEvents;
    }

    
    public HashMap<String, TypeOfEvent> getAllEverestEventsWithPrimaryKey() {
    	HashMap<String, TypeOfEvent> everestEvents = new HashMap<String, TypeOfEvent>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement("SELECT * FROM slasoi_interaction_event");

            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("everest_event_object");
                    TypeOfEvent event = null;
                    String key = resultSet.getString("slasoi_event_id");
                    try {
                        event = (TypeOfEvent) toObject(blob);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }  
                    /*
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (ClassNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
 
 					*/
                    if (event != null) {
                        everestEvents.put(key, event);
                    }
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return everestEvents;
    }
    
    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
     */
    public ArrayList executeQuery(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#select()
     */
    public ArrayList select() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
     */
    public PersistentEntity selectByPrimaryKey(String entityPrimaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public int delete(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
     */
    public int deleteByPrimaryKey(String arg0) {
    	PreparedStatement pstmt = null;
    	ResultSet resultSet = null;
        try {
        	
            pstmt = getConnection().prepareStatement("DELETE FROM slasoi_interaction_event " + "WHERE slasoi_event_id = ?");
            pstmt.setString(1, arg0);                 
            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public int insert(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
     */
    public int update(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}
