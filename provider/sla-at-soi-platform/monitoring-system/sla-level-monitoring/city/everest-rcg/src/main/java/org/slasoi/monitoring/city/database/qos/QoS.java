/**
 * 
 */
package org.slasoi.monitoring.city.database.qos;

import uk.ac.city.soi.database.PersistentEntity;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 3, 2011
 */
public class QoS implements PersistentEntity {
	private Object value;
	private Class type;
	
	/**
	 * @param value
	 * @param type
	 */
	public QoS(Object value, Class type) {
		this.value = value;
		this.type = type;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * @return the type
	 */
	public Class getType() {
		return type;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
