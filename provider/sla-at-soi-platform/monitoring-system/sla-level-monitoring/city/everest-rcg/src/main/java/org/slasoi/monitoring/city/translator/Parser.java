/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.translator;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.database.Operator.OperatorEntityManager;
import org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface;
import org.slasoi.monitoring.city.utils.ASTBinaryNode;
import org.slasoi.monitoring.city.utils.ASTNode;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;

import uk.ac.city.soi.database.DatabaseManagerFactory;

/**
 * This class is used to translate SLA objects to AST trees.
 * 
 */
public class Parser {
    // initialise logging
    Logger logger = Logger.getLogger(Parser.class.getName());

    private SLA inputSLA;
    private LinkedHashMap<ASTNode, Object[]> qosTermsPaths;
    private LinkedHashMap<ASTNode, String> qosTermsCases;
    private LinkedHashMap<String, ArrayList<ASTNode>> nodeOperatorsSignatures;

    /**
     * Manager for inserting, selecting, deleting or updating QoSTerm cases.
     */
    private OperatorEntityManagerInterface functionExprOperatorCaseStorer;

    /**
     * Constructor.
     * 
     * @param inputSLA -
     *            the input SLA
     */
    public Parser(SLA inputSLA) {
        this.inputSLA = inputSLA;
        this.qosTermsPaths = new LinkedHashMap<ASTNode, Object[]>();
        this.qosTermsCases = new LinkedHashMap<ASTNode, String>();
        functionExprOperatorCaseStorer =
                (OperatorEntityManagerInterface) DatabaseManagerFactory.getDatabaseManager().getEntityManagerFactory()
                        .getEntityManager(OperatorEntityManager.class);
        nodeOperatorsSignatures = new LinkedHashMap<String, ArrayList<ASTNode>>();

    }

    /**
     * Retrieves the operators (and qos terms) paths.
     * 
     * @return
     */
    public LinkedHashMap<ASTNode, Object[]> getQosTermsPaths() {
        return qosTermsPaths;
    }

    /**
     * Retrieves the operators (and qos terms) cases.
     * 
     * @return
     */
    public LinkedHashMap<ASTNode, String> getQosTermsCases() {
        return qosTermsCases;
    }

    /**
     * Retrieves the nodes for each operator (and qos terms) signature.
     * 
     * @return
     */
    public LinkedHashMap<String, ArrayList<ASTNode>> getNodeOperatorsSignatures() {
        return nodeOperatorsSignatures;
    }

    /**
     * Transforms an AgreementTerm object to an ASTNode.
     * 
     * @param aTNode
     * @param inputObject
     * @param qosTermPath
     * @return
     */
    private ASTNode parseAgreementTerm(ASTNode aTNode, AgreementTerm inputObject, Object[] qosTermPath) {
        // update qos term path
        qosTermPath[1] = inputObject;

        // instantiate node as AST Binary Node
        aTNode = new ASTBinaryNode();

        // initialise node
        ((ASTBinaryNode) aTNode).setObject(inputObject);
        ((ASTBinaryNode) aTNode).setType(RCGConstants.IMPLICATION);
        ((ASTBinaryNode) aTNode).setLabel(RCGConstants.IMPLIES);

        // Create the LHS of node
        // check whether there is a precondition for the given agreement
        // term
        // if yes, generate a head node and add it in the LHS of node
        if (!(((AgreementTerm) aTNode.getObject()).getPrecondition() == null)) {

            // update qos term path
            qosTermPath[2] = inputObject;

            ASTNode bodyNode = parse(((AgreementTerm) aTNode.getObject()).getPrecondition(), qosTermPath);
            ((ASTBinaryNode) aTNode).setLeftChild(bodyNode);
        }
        // else add null in the LHS of node
        else {
            ((ASTBinaryNode) aTNode).setLeftChild(null);
        }

        // Create the RHS of node
        ASTNode headNode = new ASTNode();
        headNode.setType(RCGConstants.LOGICAL_OPERATOR);
        headNode.setLabel(RCGConstants.AND);
        headNode.setObject(core.and);

        for (Guaranteed guaranteedState : ((AgreementTerm) aTNode.getObject()).getGuarantees()) {
            // implementation considers only guaranteed states
            if (guaranteedState instanceof State) {
                ASTNode guaranteedStateNode = parse((State) guaranteedState, qosTermPath);
                headNode.addChild(guaranteedStateNode);
            }
        }

        // add the RHS to node
        ((ASTBinaryNode) aTNode).setRightChild(headNode);

        return aTNode;
    }

    private ASTNode parseGuaranteedState(ASTNode gSNode, State inputObject, Object[] qosTermPath) {
        // update qos term path
        qosTermPath[3] = inputObject;

        // instantiate node as AST Binary Node
        gSNode = new ASTBinaryNode();

        // initialise node
        ((ASTBinaryNode) gSNode).setObject(inputObject);
        ((ASTBinaryNode) gSNode).setType(RCGConstants.IMPLICATION);
        ((ASTBinaryNode) gSNode).setLabel(RCGConstants.IMPLIES);

        // Create the LHS of node
        // check whether there is a precondition for the given state
        // if yes, generate a head node and add it in the LHS of node
        if (!(((State) gSNode.getObject()).getPrecondition() == null)) {

            // update qos term path
            qosTermPath[2] = inputObject;

            ASTNode bodyNode = parse(((State) gSNode.getObject()).getPrecondition(), qosTermPath);
            ((ASTBinaryNode) gSNode).setLeftChild(bodyNode);
        }

        // else add null in the LHS of node
        else {
            ((ASTBinaryNode) gSNode).setLeftChild(null);
        }

        // Create the RHS of node
        ASTNode headNode = parse(((State) gSNode.getObject()).getState(), qosTermPath);
        // add the RHS to node
        ((ASTBinaryNode) gSNode).setRightChild(headNode);

        return gSNode;
    }

    private ASTNode parseCompoundConstraintExpr(ASTNode cCENode, CompoundConstraintExpr inputObject,
            Object[] qosTermPath) {
        // instantiate node as AST Node
        cCENode = new ASTNode();

        // initialise node
        ((ASTNode) cCENode).setObject(((CompoundConstraintExpr) inputObject).getLogicalOp());
        ((ASTNode) cCENode).setType(RCGConstants.LOGICAL_OPERATOR);
        ((ASTNode) cCENode).setLabel(((CompoundConstraintExpr) inputObject).getLogicalOp().getValue());

        // Create the node children, if any
        if (((CompoundConstraintExpr) inputObject).getSubExpressions().length != 0) {
            for (ConstraintExpr subExpression : ((CompoundConstraintExpr) inputObject).getSubExpressions()) {
                ASTNode subExpressionNode = parse((ConstraintExpr) subExpression, qosTermPath);
                ((ASTNode) cCENode).addChild(subExpressionNode);
            }
        }
        // if no subExpressions are defined, create and add a null node
        // child
        else {
            ASTNode child = null;
            ((ASTNode) cCENode).addChild(child);
        }

        return cCENode;
    }

    private ASTNode parseConst(ASTNode cNode, CONST inputObject) {
        // instantiate node as AST Node
        cNode = new ASTNode();

        // initialise LHS
        cNode.setObject(inputObject);
        cNode.setLabel(inputObject.toString());
        cNode.setType(RCGConstants.CONSTANT);

        return cNode;
    }

    private ASTNode parseFunctionalExpr(ASTNode fENode, FunctionalExpr inputObject, Object[] visitedQosTermPath) {

        Object[] qosTermPath = new Object[11];
        if (visitedQosTermPath == null) {
            qosTermPath[0] = inputSLA.getUuid();
        }
        else {
            for (int i = 0; i < 11; i++) {
                if (visitedQosTermPath[i] != null) {
                    qosTermPath[i] = visitedQosTermPath[i];
                }
            }
        }

        // instantiate node as AST Node
        fENode = new ASTNode();

        // initialise LHS
        fENode.setObject(inputObject);

        STND funcExprOperator = inputObject.getOperator();

        fENode.setLabel(funcExprOperator.toString());

        // case of supported EVEREST QOS_TERMS
        if (funcExprOperator.equals(common.throughput) || funcExprOperator.equals(common.mttr)
                || funcExprOperator.equals(common.mttf) || funcExprOperator.equals(common.reliability)
                || funcExprOperator.equals(common.availability) || funcExprOperator.equals(common.completion_time)
                || funcExprOperator.equals(common.arrival_rate)|| funcExprOperator.equals(common.accessibility)
                || funcExprOperator.equals("http://www.slaatsoi.org/extension#completion_time")
                || funcExprOperator.equals("http://www.slaatsoi.org/extension#idle_time")
                || funcExprOperator.equals("http://www.slaatsoi.org/extension#ttf")) {
            // set SUPPORTED_FUNCTIONAL_OPERATOR_BY_PARAMETRIC_TEMPLATE type
            fENode.setType(RCGConstants.SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE);
        }
        else if (funcExprOperator.equals(core.mean) || funcExprOperator.equals(core.add)
                || funcExprOperator.equals(core.multiply) || funcExprOperator.equals(core.modulo)
                || funcExprOperator.equals(core.subtract)|| funcExprOperator.equals(core.divide) 
                || funcExprOperator.equals(core.mode) || funcExprOperator.equals(core.series)
                || funcExprOperator.equals(core.std) || funcExprOperator.equals(core.median)
                || funcExprOperator.equals(core.day_is)|| funcExprOperator.equals(core.month_is) 
                || funcExprOperator.equals(core.year_is)|| funcExprOperator.equals(core.time_is)
                || funcExprOperator.equals(core.count) || funcExprOperator.equals(core.sum)
                || funcExprOperator.equals(core.max) || funcExprOperator.equals(core.min)
                || funcExprOperator.equals(core.round) || funcExprOperator.equals(core.value) 
                || funcExprOperator.equals("http://www.slaatsoi.org/extension#series")) {
            // set SUPPORTED_OPERATOR_BY_EVEREST_INTERNAL_FUNCTION type
            fENode.setType(RCGConstants.SUPPORTED_OPERATOR_BY_EVEREST_INTERNAL_FUNCTION);
        }
        else {
            logger.debug("NOTIFICATION: The given input object is a currenlty UNSUPPORTED_FUNCTIONAL_OPERATOR");

            // set unsupported QOS_TERM type
            fENode.setType(RCGConstants.UNSUPPORTED_OPERATOR);
        }

        // update qos term path
        qosTermPath[9] = funcExprOperator;
        qosTermPath[10] = RCGConstants.FUNC_EXPR_OPERATOR;
        String operatorSignature = inputObject.toString();

        // update functionalExpressionOperatorsNodes
        if (this.nodeOperatorsSignatures.containsKey(operatorSignature)) {
            this.nodeOperatorsSignatures.get(operatorSignature).add(fENode);
        }
        else {
            ArrayList operatorNodes = new ArrayList<ASTNode>();
            operatorNodes.add(fENode);
            this.nodeOperatorsSignatures.put(operatorSignature, operatorNodes);
        }

        // update qos term path
        qosTermPath[8] = inputObject.getParameters();

        // update qos term path
        qosTermPath[7] = inputObject.toString();

        // parse functional expression parameters
        for (Object parameter : inputObject.getParameters()) {

            if (parameter instanceof CONST) {
                fENode.addChild(this.parseConst(null, (CONST) parameter));
            }
            else if (parameter instanceof FunctionalExpr) {
                // update qos term path
                qosTermPath[6] = inputObject;

                fENode.addChild(this.parseFunctionalExpr(null, (FunctionalExpr) parameter, qosTermPath));

                qosTermPath[6] = null;
            }
            else if (parameter instanceof ID) {
                fENode.addChild(this.parseId(null, (ID) parameter, qosTermPath));
            }
            else if (parameter instanceof EventExpr) {

                fENode.addChild(this.parseEventExpr(null, (EventExpr) parameter, qosTermPath));
            }
        }

        // update qosTerms Position map
        this.qosTermsPaths.put(fENode, qosTermPath);

        return fENode;
    }

    private ASTNode parseEventExpr(ASTNode evENode, EventExpr inputObject, Object[] visitedQosTermPath) {

        Object[] qosTermPath = new Object[11];
        if (visitedQosTermPath == null) {
            qosTermPath[0] = inputSLA.getUuid();
        }
        else {
            for (int i = 0; i < 11; i++) {
                if (visitedQosTermPath[i] != null) {
                    qosTermPath[i] = visitedQosTermPath[i];
                }
            }
        }

        // instantiate node as AST Node
        evENode = new ASTNode();

        // initialise LHS
        evENode.setObject(inputObject);

        STND eventExprOperator = inputObject.getOperator();

        evENode.setLabel(eventExprOperator.toString());

        // case of supported EVEREST QOS_TERMS
        if (eventExprOperator.equals(core.periodic)) {
            // set supported QOS_TERM type
            evENode.setType(RCGConstants.PERIODIC_EVENT_OPERATOR);
        }
        else {
            logger.debug("NOTIFICATION: The given input object is a currenlty UNSUPPORTED_OPERATOR");

            // set unsupported QOS_TERM type
            evENode.setType(RCGConstants.UNSUPPORTED_OPERATOR);
        }

        // update qos term path
        qosTermPath[9] = eventExprOperator;
        qosTermPath[10] = RCGConstants.EVENT_EXPR_OPERATOR;
        String operatorSignature = inputObject.toString();

        // update functionalExpressionOperatorsNodes
        if (this.nodeOperatorsSignatures.containsKey(operatorSignature)) {
            this.nodeOperatorsSignatures.get(operatorSignature).add(evENode);
        }
        else {
            ArrayList operatorNodes = new ArrayList<ASTNode>();
            operatorNodes.add(evENode);
            this.nodeOperatorsSignatures.put(operatorSignature, operatorNodes);
        }

        // update qos term path
        qosTermPath[8] = inputObject.getParameters();

        // update qos term path
        qosTermPath[7] = inputObject.toString();

        // parse functional expression parameters
        for (Object parameter : inputObject.getParameters()) {

            // update qos term path
            qosTermPath[6] = inputObject;

            if (parameter instanceof CONST) {
                evENode.addChild(this.parseConst(null, (CONST) parameter));
            }
            else if (parameter instanceof FunctionalExpr) {
                // update qos term path
                qosTermPath[6] = inputObject;

                evENode.addChild(this.parseFunctionalExpr(null, (FunctionalExpr) parameter, qosTermPath));

                qosTermPath[6] = null;
            }
            else if (parameter instanceof ID) {
                evENode.addChild(this.parseId(null, (ID) parameter, qosTermPath));
            }
        }

        // update qosTerms Position map
        this.qosTermsPaths.put(evENode, qosTermPath);

        return evENode;
    }

    private ASTNode parseCompoundDomainExpr(ASTNode cDENode, CompoundDomainExpr inputObject,
    		ASTNode firstNode, Object[] qosTermPath) {
        // instantiate node as AST Node
        cDENode = new ASTNode();

        // initialise node
        ((ASTNode) cDENode).setObject(((CompoundDomainExpr) inputObject).getLogicalOp());
        ((ASTNode) cDENode).setType(RCGConstants.LOGICAL_OPERATOR);
        ((ASTNode) cDENode).setLabel(((CompoundDomainExpr) inputObject).getLogicalOp().getValue());

        // Create the node children, if any
        if (((CompoundDomainExpr) inputObject).getSubExpressions().length != 0) {
            for (DomainExpr subExpression : ((CompoundDomainExpr) inputObject).getSubExpressions()) {
                // SimpleDomainExpr sub-case
                if (subExpression instanceof SimpleDomainExpr) {

                    SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) subExpression;

                    // parseSimpleDomainExpr
                    cDENode.addChild(this.parseSimpleDomainExpr(cDENode, simpleDomainExpr, firstNode, qosTermPath));
                }
            }
        }

        return cDENode;
    }

    private ASTNode parseExpr(ASTNode eNode, Expr inputObject, Object[] qosTermPath) {
/*    	
        if (inputObject instanceof FunctionalExpr) {
            FunctionalExpr funcExpr = (FunctionalExpr) inputObject;

            // parseExprFunctionalExpr
            eNode = this.parseFunctionalExpr(eNode, funcExpr, qosTermPath);
        }
        else if (inputObject instanceof CompoundDomainExpr) {
            CompoundDomainExpr compoundDomainExpr = (CompoundDomainExpr) inputObject;

            // parseCompoundDomainExpr
            eNode = this.parseCompoundDomainExpr(eNode, compoundDomainExpr, typeConstraintExprObject, qosTermPath);
        }
        else if (inputObject instanceof SimpleDomainExpr) {
            SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) inputObject;

            // parseSimpleDomainExpr
            eNode = this.parseSimpleDomainExpr(eNode, simpleDomainExpr, typeConstraintExprObject, qosTermPath);
        }
        */
        if (inputObject instanceof ValueExpr) {
        	ValueExpr valExpr = (ValueExpr) inputObject;
        	eNode = this.parseValueExpr(eNode, valExpr, qosTermPath);
            // parseExprFunctionalExpr
            //eNode = this.parseFunctionalExpr(eNode, funcExpr, qosTermPath);
        }
        else if (inputObject instanceof ConstraintExpr) {
        	ConstraintExpr consExpr = (ConstraintExpr) inputObject;
        	eNode = this.parseConstraintExpr(eNode, consExpr, qosTermPath);
            // parseCompoundDomainExpr
            //eNode = this.parseCompoundDomainExpr(eNode, compoundDomainExpr, typeConstraintExprObject, qosTermPath);
        }    	
        else {
            logger.debug("NOTIFICATION: The given input object is currenlty not supported");
        }

        return eNode;
    }

    private ASTNode parseId(ASTNode iDNode, ID inputObject, Object[] qosTermPath) {
        // instantiate node as AST Node
        iDNode = new ASTNode();

        VariableDeclr varDecl = null;
        LinkedHashMap<String, Object> ifcDecl = null;

        // check whether the ID identifies a variable
        // declaration of the input SLA
        //System.out.println("DDDDDDD Parsing ID " + inputObject.toString());
        //System.out.println("DDDDDDD input SLA " + inputSLA);
        //System.out.println("DDDDDDD input SLA variables" + inputSLA.getVariableDeclrs());
        boolean indicatesSLAVarDecl = false;
        if(inputSLA.getVariableDeclrs() != null){
            for (VariableDeclr slaVarDecl : inputSLA.getVariableDeclrs()) {
                if (slaVarDecl.getVar().equals((ID) inputObject)) {
                    varDecl = inputSLA.getVariableDeclr(((ID) inputObject).getValue());                
                    indicatesSLAVarDecl = true;
                    break;
                }
            }        	
        }

        // if the ID DOES NOT identify a variable declaration of
        // the input SLA
        // check whether the ID identifies a variable
        // declaration of an SLA agreement term
        if (!indicatesSLAVarDecl) {
        	//System.out.println("DDDDDDD SLA variable not found ");
            outerLoop: for (AgreementTerm slaAt : inputSLA.getAgreementTerms()) {
                for (VariableDeclr slaAtVarDecl : slaAt.getVariableDeclrs()) {
                    if (slaAtVarDecl.getVar().toString().contains(((ID) inputObject).toString())) {
                        varDecl = slaAt.getVariableDeclr(((ID) inputObject).getValue());
                        break outerLoop;
                    }
                    if (((ID) inputObject).toString().contains(slaAtVarDecl.getVar().toString())) {
                        varDecl = slaAt.getVariableDeclr(slaAtVarDecl.getVar().toString());
                        break outerLoop;
                    }
                }
            }
        	        
        }

        // if the ID DOES NOT identify a variable declaration of
        // of an SLA agreement term, check whether the ID identifies
        // an input/output of an operation of an interface declaration
        boolean indicatesSLAInterfaceDecl = false;
        if (!indicatesSLAVarDecl && !indicatesSLAInterfaceDecl) {
            outerloop: for (InterfaceDeclr interfaceDecl : inputSLA.getInterfaceDeclrs()) {
                if (!((ID) inputObject).toString().equals(interfaceDecl.getId().toString())) {
                    if (((ID) inputObject).toString().contains(interfaceDecl.getId().toString())
                            || interfaceDecl.getId().toString().contains(((ID) inputObject).toString())) {

                        Object interfaceObject = interfaceDecl.getInterface();
                        if (interfaceObject instanceof Specification) {
                            Specification interfaceSpec = (Specification) interfaceObject;
                            for (Operation operation : interfaceSpec.getOperations()) {
                            	if(!((ID) inputObject).toString().equals(interfaceDecl.getId().toString() + "/" + operation.getName().toString())){
                                //if (((ID) inputObject).toString().contains(operation.getName().toString())) {
                                    for (Operation.Property output : operation.getOutputs()) {
                                        if (((ID) inputObject).toString().contains(output.getName().toString())) {
                                        	//System.out.println("*&&&****&&&***Matched**** " + ((ID) inputObject).toString() + " and " + output.getName());
                                            ifcDecl = new LinkedHashMap<String, Object>();                                            
                                            ifcDecl.put(RCGConstants.INTERFACE_DECLARATION, interfaceDecl);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION, operation);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_STATUS,
                                                    RCGConstants.STATUS_RESPONSE_AFTER);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT, output);
                                            break outerloop;
                                        }
                                    }
                                    for (Operation.Property input : operation.getInputs()) {
                                        if (((ID) inputObject).toString().contains(input.getName().toString())) {
                                            ifcDecl = new LinkedHashMap<String, Object>();
                                            ifcDecl.put(RCGConstants.INTERFACE_DECLARATION, interfaceDecl);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION, operation);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_STATUS,
                                                    RCGConstants.STATUS_REQUEST_BEFORE);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT, input);
                                            break outerloop;
                                        }
                                    }
                                    for (Operation.Property related : operation.getRelated()) {
                                        if (((ID) inputObject).toString().contains(related.getName().toString())) {
                                            ifcDecl = new LinkedHashMap<String, Object>();
                                            ifcDecl.put(RCGConstants.INTERFACE_DECLARATION, interfaceDecl);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION, operation);
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_STATUS,
                                                    RCGConstants.STATUS_RESPONSE_AFTER);  //check it out later
                                            ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT, related);
                                            break outerloop;
                                        }
                                    }                                    
                                }
                            	else{
                                    ifcDecl = new LinkedHashMap<String, Object>();
                                    ifcDecl.put(RCGConstants.INTERFACE_DECLARATION, interfaceDecl);
                                    ifcDecl.put(RCGConstants.INTERFACE_SPECIFICATION_OPERATION, operation);
                                    break outerloop;                            		
                            	}
                            }
                        }
                        /*
                         * if (interfaceObject instanceof InterfaceResourceTypeTypeImpl) { // TO DO!!! }
                         */
                    }
                }
                else {
                    ifcDecl = new LinkedHashMap<String, Object>();
                    ifcDecl.put(RCGConstants.INTERFACE_DECLARATION, interfaceDecl);
                    break outerloop;
                }
            }
        }

        // sub-case that ID identifies a variable declaration of
        // the input SLA or the SLA agreement terms
        if (varDecl != null) {

            // update qos term path
            qosTermPath[4] = varDecl;
            
//            Expr varDeclExpr = varDecl.getExpr();
//            
//             // parseExpr
//            iDNode = this.parseExpr(iDNode, varDeclExpr, qosTermPath);
            iDNode = this.parseVariableDeclr(iDNode, varDecl, qosTermPath);
            
        }
        // sub-case that ID identifies
        // an input/output of an operation of an interface declaration
        if (ifcDecl != null) {

        	//ifcDecl.put(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE, RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE);
            // update qos term path
            //qosTermPath[5] = ifcDecl;
        	ifcDecl.put(((ID) inputObject).toString(), "");
        	
        	if(qosTermPath[5] != null){
        		ArrayList<LinkedHashMap<String, Object>> ifcDecls = (ArrayList<LinkedHashMap<String, Object>>)qosTermPath[5];
        		ifcDecls.add(ifcDecl);
        		qosTermPath[5] = ifcDecls;
        		//qosTermPath[9] = new STND(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE);
        	}
        	else{
        		ArrayList<LinkedHashMap<String, Object>> ifcDecls = new ArrayList<LinkedHashMap<String, Object>>();
        		ifcDecls.add(ifcDecl);
        		qosTermPath[5] = ifcDecls;    
        		//qosTermPath[9] = new STND(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE);
        	}

            iDNode.setObject(ifcDecl);
            
            iDNode.setType(RCGConstants.INTERFACE_DECLARATION);
            iDNode.setLabel(inputObject.toString());
            
            //InterfaceDeclr interfaceDecl = (InterfaceDeclr)ifcDecl.get(RCGConstants.INTERFACE_DECLARATION);
            // update functionalExpressionOperatorsNodes
            if (this.nodeOperatorsSignatures.containsKey(((ID) inputObject).toString())) {
                this.nodeOperatorsSignatures.get(((ID) inputObject).toString()).add(iDNode);
            }
            else {
                ArrayList<ASTNode> operatorNodes = new ArrayList<ASTNode>();
                operatorNodes.add(iDNode);
                this.nodeOperatorsSignatures.put(((ID) inputObject).toString(), operatorNodes);
            }
            
            // update qosTerms Position map
            this.qosTermsPaths.put(iDNode, qosTermPath);
            
        }


        
        return iDNode;
    }

    private ASTNode parseVariableDeclr(ASTNode vDNode, VariableDeclr inputObject, Object[] qosTermPath) {
    	
        // case that variable is customisable
        if (inputObject instanceof Customisable) {
            ID var = inputObject.getVar();

            // Create the LHS child
            ASTNode lhs = new ASTNode();
            CONST val = ((Customisable)inputObject).getValue();
                         
            // initialise LHS
            lhs.setObject(val);
            lhs.setType(RCGConstants.VARIABLE);
            lhs.setLabel(var.toString()); 
            
            Expr varDeclExpr = inputObject.getExpr();
            
            if(varDeclExpr instanceof SimpleDomainExpr){
            	SimpleDomainExpr sde = (SimpleDomainExpr)varDeclExpr;
            	vDNode = this.parseSimpleDomainExpr(vDNode, sde, lhs, qosTermPath);
            }
            else if(varDeclExpr instanceof CompoundDomainExpr){
            	CompoundDomainExpr cde = (CompoundDomainExpr)varDeclExpr;
            	vDNode = this.parseCompoundDomainExpr(vDNode, cde, lhs, qosTermPath);
            }
        }
        else{//case that variable is not customisable
        	Expr varDeclExpr = inputObject.getExpr();
        	vDNode = parseExpr(vDNode, varDeclExpr, qosTermPath);
        }


        return vDNode;
    }
    
    private ASTNode parseValueExpr(ASTNode vENode, ValueExpr inputObject, Object[] qosTermPath) {
        // case that value expr is CONST
        if (inputObject instanceof CONST) {
            CONST constant = (CONST) inputObject;

            // parseConst
            vENode = this.parseConst(vENode, constant);
        }
        // case that value expr is FunctionalExpr
        else if (inputObject instanceof FunctionalExpr) {
            FunctionalExpr functionalExpr = (FunctionalExpr) inputObject;

            // parseFunctionalExpr
            vENode = this.parseFunctionalExpr(vENode, functionalExpr, qosTermPath);
        }
        // case that value expr is SimpleDomainExpr
        else if (inputObject instanceof SimpleDomainExpr) {
            SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) inputObject;

            // parseFunctionalExpr
            vENode = this.parseSimpleDomainExpr(vENode, simpleDomainExpr, null, qosTermPath);
        }
        // case that value expr is ID
        else if (inputObject instanceof ID) {
            ID id = (ID) inputObject;

            // parseId
            vENode = this.parseId(vENode, id, qosTermPath);
            //vENode = this.parseId(vENode, id, null, qosTermPath);
        }

        return vENode;
    }

    private ASTNode parseSimpleDomainExpr(ASTNode sDENode, SimpleDomainExpr inputObject,
    		ASTNode lhsNode, Object[] qosTermPath) {
        // instantiate node as AST Binary Node
        sDENode = new ASTBinaryNode();

        // initialise node
        ((ASTBinaryNode) sDENode).setObject(((SimpleDomainExpr) inputObject).getComparisonOp());
        ((ASTBinaryNode) sDENode).setType(RCGConstants.COMPARISON_OPERATOR);
        ((ASTBinaryNode) sDENode).setLabel(((SimpleDomainExpr) inputObject).getComparisonOp().toString());
        
        
        /*
         * parse left hand side operand
         */

//        ValueExpr lhsValueExpr = typeConstraintExprObject.getValue();
//        // Create the LHS child
//        ASTNode lhs = new ASTNode();
//
//        // initialise LHS
//        lhs.setObject(lhsValueExpr);
//        lhs.setLabel(lhsValueExpr.toString());      
//        
//        lhs = this.parseValueExpr(lhs, lhsValueExpr, typeConstraintExprObject, qosTermPath);
//
//        sDENode.addChild(lhs);
        
        //if(lhsNode != null) 
        sDENode.addChild(lhsNode);
        
        /*
         * parse right hand side operand
         */

        ValueExpr rhsValueExpr = inputObject.getValue();

        // Create the RHS child
        ASTNode rhs = new ASTNode();

        // initialise RHS
        rhs.setObject(rhsValueExpr);
        rhs.setLabel(rhsValueExpr.toString());

        rhs = this.parseValueExpr(rhs, rhsValueExpr, qosTermPath);
        
        // update qosTerms Position map
        //this.qosTermsPaths.put(lhsNode, qosTermPath);

//        sDENode.addChild(rhs);
//        return sDENode;
        
        // Dark area
        
        if (rhs.getType() != null && rhs.getType().equals(RCGConstants.LOGICAL_OPERATOR)) {        	        	
        		//rhs = this.replaceAllNodes(rhs, lhsNode, RCGConstants.VARIABLE);        	
        	rhs = this.replaceAllNodes(rhs, lhsNode, RCGConstants.VARIABLE);
        		//this.qosTermsPaths.put(rhs, qosTermPath);
                return rhs;
        }
        else {
            sDENode.addChild(rhs);
            return sDENode;
        }
        
        
    }

    private ASTNode parseTypeConstraintExpr(ASTNode tCENode, TypeConstraintExpr inputObject, Object[] qosTermPath) {

      ValueExpr lhsValueExpr = inputObject.getValue();
      // Create the LHS child
      ASTNode lhs = new ASTNode();

      // initialise LHS
      lhs.setObject(lhsValueExpr);
      lhs.setLabel(lhsValueExpr.toString());      
      
      lhs = this.parseValueExpr(lhs, lhsValueExpr, qosTermPath);
    	
        DomainExpr domainExpr = ((TypeConstraintExpr) inputObject).getDomain();

        if (domainExpr instanceof SimpleDomainExpr) {
            SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) domainExpr;

            // parseSimpleDomainExpr
            tCENode = this.parseSimpleDomainExpr(tCENode, simpleDomainExpr, lhs, qosTermPath);
        }
        else if (domainExpr instanceof CompoundConstraintExpr) {
            CompoundConstraintExpr compoundConstraintExpr = (CompoundConstraintExpr) domainExpr;

            // parseCompoundConstraintExpr
            tCENode = this.parseCompoundConstraintExpr(tCENode, compoundConstraintExpr, qosTermPath);
        }
        else if (domainExpr instanceof CompoundDomainExpr) {
            CompoundDomainExpr compoundDomainExpr = (CompoundDomainExpr) domainExpr;

            // parseCompoundDomainExpr
            tCENode = this.parseCompoundDomainExpr(tCENode, compoundDomainExpr, lhs, qosTermPath);
        }
        else {
            logger.debug("NOTIFICATION: The given input object is currenlty not supported");
        }

        return tCENode;
    }

    private ASTNode parseConstraintExpr(ASTNode cENode, ConstraintExpr inputObject, Object[] qosTermPath) {
        // CompoundConstraintExpr sub-case
        if (inputObject instanceof CompoundConstraintExpr) {

            CompoundConstraintExpr compoundConstraintExpr = (CompoundConstraintExpr) inputObject;

            // parseCompoundConstraintExpr
            cENode = this.parseCompoundConstraintExpr(cENode, compoundConstraintExpr, qosTermPath);

        }
        // TypeConstraintExpr sub-case
        else if (inputObject instanceof TypeConstraintExpr) {

            TypeConstraintExpr typeConstraintExpr = (TypeConstraintExpr) inputObject;

            // parseTypeConstraintExpr
            cENode = this.parseTypeConstraintExpr(cENode, typeConstraintExpr, qosTermPath);

        }
        else {
            logger.debug("NOTIFICATION: The given input object is currenlty not supported");
        }

        return cENode;
    }

    /**
     * Transforms the input object to an ASTNode.
     * 
     * @param inputObject
     * @param visitedQosTermPath[]
     *            visitedQosTermPath[0]: inputSLA.Uuid visitedQosTermPath[1]: agreementeTerm.Id visitedQosTermPath[2]:
     *            preconditionOf visitedQosTermPath[3]: guaranteed.State.Id visitedQosTermPath[4]: variableDecl
     *            visitedQosTermPath[5]: interfaceDecl visitedQosTermPath[6]: parameterOf visitedQosTermPath[7]:
     *            funcExprOperator.String visitedQosTermPath[8]: funcExprOperatorParameters visitedQosTermPath[9]:
     *            funcExprOperator visitedQosTermPath[10]: operatorType
     * @return ASTNode
     */
    public ASTNode parse(Object inputObject, Object[] visitedQosTermPath) {

        Object[] qosTermPath = new Object[11];
        if (visitedQosTermPath == null) {
            qosTermPath[0] = inputSLA.getUuid();
        }
        else {
            for (int i = 0; i < 11; i++) {
                if (visitedQosTermPath[i] != null) {
                    qosTermPath[i] = visitedQosTermPath[i];
                }
            }
        }

        // generate a new null Node
        ASTNode node = null;

        // AgreementTerm case
        if (inputObject instanceof AgreementTerm) {

            AgreementTerm agreementTerm = ((AgreementTerm) inputObject);

            // parseAgreementTerm
            node = this.parseAgreementTerm(node, agreementTerm, qosTermPath);

        }
        // State case
        else if (inputObject instanceof State) {

            State guaranteedState = ((State) inputObject);

            // parseGuaranteedState
            node = this.parseGuaranteedState(node, guaranteedState, qosTermPath);
        }
        // ConstraintExpr case
        else if (inputObject instanceof ConstraintExpr) {

            ConstraintExpr constraintExpr = (ConstraintExpr) inputObject;

            // parseConstraintExpr
            node = this.parseConstraintExpr(node, constraintExpr, qosTermPath);

        }
        else {
            logger.debug("NOTIFICATION: The given input object is currenlty not supported " + inputObject.toString());
        }

        return node;
    }

    /**
     * Generates the caseId and updates the qosTerm case DB table for each operator (and qos term) included in assigned
     * SLA expression.
     */
    public void generateQoSTermsCases() {

        ArrayList<String> visitedFuncExprOperators = new ArrayList<String>();

        // for a given SLA, the hash(SLAUUID) is common for every included qos
        // terms
        String slaUUID = this.inputSLA.getUuid().toString();
        int slaUUIDHash = this.hashCode();

        // process the paths of the assigned expression qos terms
        for (ASTNode key : this.qosTermsPaths.keySet()) {

            String operatorString = "";
            if (key.getObject() instanceof FunctionalExpr) {
                operatorString = ((FunctionalExpr) key.getObject()).toString();
            }
            if (key.getObject() instanceof EventExpr) {
                operatorString = ((EventExpr) key.getObject()).toString();
            }

            // initialise qos term case id

            Object[] qosTermPath = this.qosTermsPaths.get(key);

            String caseId = "";

            // hash(agreement term id)
            int atHash = qosTermPath[1].hashCode();

            // hash(preconditionOf)
            Object timestamp0 = System.currentTimeMillis();
            int preconditionOfHash = timestamp0.hashCode();
            if (qosTermPath[2] != null) {
                preconditionOfHash = qosTermPath[2].hashCode();
            }

            // hash(guaranteed state id)
            Object timestamp1 = System.currentTimeMillis();
            int gsHash = timestamp1.hashCode();
            if (qosTermPath[3] != null) {
                gsHash = qosTermPath[3].hashCode();
            }

            // hash(variable declaration)
            Object timestamp2 = System.currentTimeMillis();
            int vdHash = timestamp2.hashCode();
            if (qosTermPath[4] != null) {
                vdHash = qosTermPath[4].hashCode();
            }
            
            // hash(interface declaration)
            Object timestamp3 = System.currentTimeMillis();
            int ifcDeclHash = timestamp3.hashCode();
            if (qosTermPath[5] != null) {
                ifcDeclHash = qosTermPath[5].hashCode();
            }
			
            // hash(funcExprOperatorSignature)
            Object timestamp5 = System.currentTimeMillis();
            int funcExprOperatorSignatureHash = timestamp5.hashCode();
            if (qosTermPath[7] != null) {
                funcExprOperatorSignatureHash = qosTermPath[7].hashCode();
            }

            // hash(parameters)
            Object timestamp6 = System.currentTimeMillis();
            int parametersHash = timestamp6.hashCode();
            if (qosTermPath[8] != null) {
                parametersHash = qosTermPath[8].hashCode();
            }


            // case id = hash(SLAUUID):hash(agreement term):hash(preconditionOf):
            // hash(guaranteed.State):hash(varDeclaration):hash(interfacDeclaration):
            // hash(funcExprOperatorSignature):hash(parameters):qosTerm
            caseId =
                slaUUIDHash + ":" + atHash + ":" + preconditionOfHash + ":" + gsHash + ":" + vdHash + ":"
                        + ifcDeclHash + ":" + funcExprOperatorSignatureHash + ":" + parametersHash + ":"
                        + qosTermPath[9];
            
            String preconditionOf = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[2] != null) {
                if (qosTermPath[2] instanceof AgreementTerm) {
                    preconditionOf = ((AgreementTerm) qosTermPath[2]).getId().toString();
                }
                if (qosTermPath[2] instanceof State) {
                    preconditionOf = ((State) qosTermPath[2]).getId().toString();
                }
            }

            String grtdState = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[3] != null) {
                if (qosTermPath[3] instanceof State) {
                    grtdState = ((State) qosTermPath[3]).getId().toString();
                }
            }

            String varDecl = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[4] != null) {
                if (qosTermPath[4] instanceof VariableDeclr) {
                    varDecl = ((VariableDeclr) qosTermPath[4]).getVar().toString();
                }
            }

            
            String interfaceDecl = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[5] != null) {
            	ArrayList<LinkedHashMap<String, Object>> ifcDecls = (ArrayList<LinkedHashMap<String, Object>>)qosTermPath[5];
            	for(LinkedHashMap<String, Object> ifcDecl : ifcDecls){
                    if (ifcDecl.containsKey(RCGConstants.INTERFACE_DECLARATION)) {
                        interfaceDecl +=
                                ((InterfaceDeclr)ifcDecl.get(RCGConstants.INTERFACE_DECLARATION)).getId().toString();
                    }
            	}
            }
			
            String parameterOf = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[6] != null) {
                if (qosTermPath[6] instanceof FunctionalExpr) {
                    parameterOf = ((FunctionalExpr) qosTermPath[6]).toString();
                }
            }

            String operatorSignature = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[7] != null) {
                if (qosTermPath[7] instanceof String) {
                    operatorSignature = ((String) qosTermPath[7]);
                }
            }

            String funcExprOperator = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[9] != null) {                
                	funcExprOperator = ((STND) qosTermPath[9]).toString();                
            }
            
            String operatorType = RCGConstants.NOT_APPLICABLE;
            if (qosTermPath[10] != null) {
                if (qosTermPath[10] instanceof String) {
                    operatorType = ((String) qosTermPath[10]);
                }
            }
            
            if (visitedFuncExprOperators.size() == 0 || !visitedFuncExprOperators.contains(operatorString)) {
                // update qosTerm case DB table
                if (this.functionExprOperatorCaseStorer.existsRecord(caseId)) {
                    this.functionExprOperatorCaseStorer.update(caseId, qosTermPath);
                }
                else {
                    this.functionExprOperatorCaseStorer.insert(slaUUID, ((AgreementTerm) qosTermPath[1]).getId()
                            .toString(), caseId, preconditionOf, grtdState, varDecl, interfaceDecl, parameterOf,
                            operatorSignature, operatorType, funcExprOperator, qosTermPath);
                }
            }

            visitedFuncExprOperators.add(operatorString);            	
      
            // update qosTerm cases
            this.qosTermsCases.put(key, caseId);                        
        }
    }

    /**
     * Receives a frontier(list) of ASTNodes and resolves the OR ASTNodes to AND ASTNodes.
     * 
     * @param frontier
     * @param treesToBeResolved
     * @param resolvedTrees
     * @return
     */
    public ArrayList<ASTNode> resolveOrIncrementally(ArrayList<ASTNode> frontier, ArrayList<ASTNode> treesToBeResolved,
            ArrayList<ASTNode> resolvedTrees) {

        if (!this.areOrResolved(resolvedTrees)) {

            ArrayList<ASTNode> newTreesToBeResolved = new ArrayList<ASTNode>();
            ArrayList<ASTNode> newFrontier = new ArrayList<ASTNode>();

            // compute resolved trees per frontier/level
            ArrayList<ASTNode> frontierORNodes = this.getORSubList(frontier);
            if (treesToBeResolved.size() > 0 && frontierORNodes.size() > 0) {
                for (ASTNode tree : treesToBeResolved) {
                    if (resolvedTrees.contains(tree)) {
                        resolvedTrees.remove(tree);
                    }

                    ArrayList<ASTNode> qualifiedOrNodes = new ArrayList<ASTNode>();

                    for (ASTNode orNode : frontierORNodes) {
                        if (tree.contains(orNode)) {
                            qualifiedOrNodes.add(orNode);
                        }
                    }

                    ArrayList<ASTNode> levelResolvedTrees = this.resolveOrNodes(tree, qualifiedOrNodes);
                    for (ASTNode levelTree : levelResolvedTrees) {
                        resolvedTrees.add(levelTree);
                        newTreesToBeResolved.add(levelTree);
                    }
                }
            }

            // get next frontier/level
            if (frontier.size() > 0) {
                for (ASTNode node : frontier) {
                    if (node != null) {
                        if (node.getChildren().size() > 0) {
                            newFrontier.addAll(node.getChildren());
                        }
                    }
                }
            }

            // resolve new trees for next frontier/level
            if (newTreesToBeResolved.size() == 0) {
                for (ASTNode tree : treesToBeResolved) {
                    newTreesToBeResolved.add(tree);
                }
            }
            this.resolveOrIncrementally(newFrontier, newTreesToBeResolved, resolvedTrees);

        }

        return resolvedTrees;
    }

    private int computeOrChildrenCombinationsNumber(ArrayList<ASTNode> orNodes) {
        int orChildrenCombinationsNumber = 1;

        for (ASTNode orNode : orNodes) {
            orChildrenCombinationsNumber = orChildrenCombinationsNumber * orNode.getChildren().size();
        }

        return orChildrenCombinationsNumber;
    }

    private void findAndReplaceChild(ASTNode root, ASTNode childToBeReplaced, ASTNode replacementChild) {
        if (root.getChildren().size() > 0) {
            for (int i = 0; i < root.getChildren().size(); i++) {
                ASTNode child = root.getChildren().get(i);
                if (child != null) {
                    if (child.isEqualTo(childToBeReplaced)) {
                        root.getChildren().remove(i);
                        root.getChildren().add(i, replacementChild);
                        break;
                    }
                    else {
                        findAndReplaceChild(child, childToBeReplaced, replacementChild);
                    }
                }
            }
        }
    }

    private ArrayList<ASTNode> getORSubList(ArrayList<ASTNode> inputList) {
        ArrayList<ASTNode> orSubList = new ArrayList<ASTNode>();

        for (ASTNode node : inputList) {
            if (node != null) {
                if (node.getLabel().equals(core.or.getValue())) {
                    orSubList.add(node.deepCopy());
                }
            }
        }

        return orSubList;
    }
   
    private ArrayList<ASTNode> resolveOrNodes(ASTNode root, ArrayList<ASTNode> orNodes) {

        ArrayList<ASTNode> resolvedTrees = new ArrayList<ASTNode>();

        int orChildrenCombinationsNumber = this.computeOrChildrenCombinationsNumber(orNodes);

        for (int i = 1; i <= orChildrenCombinationsNumber; i++) {
            resolvedTrees.add(root.deepCopy());
        }

        for (ASTNode orNode : orNodes) {
            int initialNumberOfOrNodeChildren = orNode.getChildren().size();
            for (int i = 0; i < initialNumberOfOrNodeChildren; i++) {
                ASTNode child = orNode.getChildren().get(i);
                for (int j = 1 + i; j <= orChildrenCombinationsNumber; j = j + initialNumberOfOrNodeChildren) {
                    ASTNode treeToBeResolved = resolvedTrees.get(j - 1);
                    findAndReplaceChild(treeToBeResolved, orNode, child);
                }
            }
        }

        // prune duplicates
        LinkedHashMap<String, ASTNode> resolvedTreeSignatures = new LinkedHashMap<String, ASTNode>();
        for (ASTNode resolvedTree : resolvedTrees) {
            String resolvedTreeSignature = resolvedTree.preOrderPrint(resolvedTree, "");
            if (!resolvedTreeSignatures.containsKey(resolvedTreeSignature)) {
                resolvedTreeSignatures.put(resolvedTreeSignature, resolvedTree);
            }
        }

        resolvedTrees.clear();
        for (ASTNode tree : resolvedTreeSignatures.values()) {
            resolvedTrees.add(tree);
        }

        return resolvedTrees;
    }

    private boolean hasOrDescendent(ASTNode node) {
        boolean hasOrChild = false;

        if (node.getChildren().size() > 0) {
            for (ASTNode child : node.getChildren()) {
                if (child != null) {
                    if (child.getLabel() != null && child.getLabel().equals(core.or.getValue())) {
                        hasOrChild = true;
                        break;
                    }
                    else {
                        hasOrChild = this.hasOrDescendent(child);
                        if (hasOrChild) {
                            break;
                        }
                    }
                }
            }
        }

        return hasOrChild;
    }

    private boolean areOrResolved(ArrayList<ASTNode> inputList) {
        boolean resolved = true;

        for (ASTNode tree : inputList) {
            if (this.hasOrDescendent(tree)) {
                resolved = false;
                break;
            }
        }

        return resolved;
    }

    private ASTNode replaceAllNodes(ASTNode root, ASTNode sub, String nodeType){
    	/*
        if (root.getChildren().size() > 0) {
            for (ASTNode child : root.getChildren()) {
                if (child != null) {
                    this.replaceAllNodes(child, sub, nodeType);
                }
            }
        }    	
        else{
        	if(root.getType().equals(nodeType)) root = sub;
        }
        return root;
    	*/
    	ASTNode newRoot = null;
    	if(root != null){
    		if(root instanceof ASTBinaryNode){
    			newRoot = new ASTBinaryNode();
                ASTNode leftChild = ((ASTBinaryNode) root).getLeftChild();
                if(leftChild != null) ((ASTBinaryNode)newRoot).setLeftChild(this.replaceAllNodes(leftChild, sub, nodeType));
                ASTNode rightChild = ((ASTBinaryNode) root).getRightChild();
                if(rightChild != null) ((ASTBinaryNode)newRoot).setRightChild(this.replaceAllNodes(rightChild, sub, nodeType));
    		}
            else newRoot = new ASTNode();

            if (root.getChildren().size() > 0) {
                for (ASTNode child : root.getChildren()) {
                    if (child != null) {
                        newRoot.addChild(this.replaceAllNodes(child, sub, nodeType));
                    }
                    else {
                        newRoot.addChild(null);
                    }
                }
            }
            
            if(root.getType().equals(nodeType)){
            	//newRoot = sub;
                newRoot.setType(sub.getType());
                newRoot.setLabel(sub.getLabel());
                newRoot.setObject(sub.getObject());
            	Object[] qtp = this.qosTermsPaths.get(sub);
            	if(qtp != null) this.qosTermsPaths.put(newRoot, qtp);                
            }
            else{
            	//newRoot = root;
                newRoot.setType(root.getType());
                newRoot.setLabel(root.getLabel());
                newRoot.setObject(root.getObject());                	
            	Object[] qtp = this.qosTermsPaths.get(root);
            	if(qtp != null) this.qosTermsPaths.put(newRoot, qtp);                                
            }                		                        
    	}

        return newRoot;    
        
    }
    
    /*
    public ArrayList<ASTNode> splitGuaranteedStates(ArrayList<ASTNode> inputList){
    	ArrayList<ASTNode> guaranteedStates = new ArrayList<ASTNode>();
    	
    	for(ASTNode astNode : inputList){
    		if(astNode.getObject() instanceof AgreementTerm){
    			ASTNode globalPreCond = ((ASTBinaryNode)astNode).getLeftChild();
    			ArrayList<ASTNode> gsNodes = this.getGuaranteedSubList(astNode);
    			for(ASTNode gsNode : gsNodes){
    				if(globalPreCond != null){
    					if(((ASTBinaryNode)gsNode).getLeftChild() != null){
    						ASTNode oldPreCond = ((ASTBinaryNode)gsNode).getLeftChild();
    						ASTNode newPreCond = new ASTNode();
    						newPreCond.setType(RCGConstants.LOGICAL_OPERATOR);
    						newPreCond.setLabel(RCGConstants.AND);
    						newPreCond.setObject(core.and);
    						newPreCond.addChild(oldPreCond);
    						newPreCond.addChild(globalPreCond);
    						if(gsNode.hasChild(oldPreCond)){
    							gsNode.getChildren().remove(oldPreCond);
    						}
    						((ASTBinaryNode)gsNode).setLeftChild(newPreCond);
    						
    					}
    					else{
    						((ASTBinaryNode)gsNode).setLeftChild(globalPreCond);
    					}
    				}
    				ASTNode newAgrT = ((ASTBinaryNode)astNode).deepCopy();
    				if(globalPreCond != null && newAgrT.getChildren().contains(globalPreCond)){
    					newAgrT.getChildren().remove(globalPreCond);
    					((ASTBinaryNode)newAgrT).setLeftChild(null);
    				}
    				((ASTBinaryNode)newAgrT).getRightChild().getChildren().clear();
    				((ASTBinaryNode)newAgrT).getRightChild().addChild(gsNode);
    				
    				guaranteedStates.add(newAgrT);
    			}
    			
    		}
    	}
    	
    	return guaranteedStates;
    }
    */
    
    public ArrayList<ASTNode> splitGuaranteedStates(ArrayList<ASTNode> inputList){
    	ArrayList<ASTNode> guaranteedStates = new ArrayList<ASTNode>();
    	
    	for(ASTNode astNode : inputList){
    		if(astNode.getObject() instanceof AgreementTerm){
    			//ASTNode globalPreCond = ((ASTBinaryNode)astNode).getLeftChild();
    			ArrayList<ASTNode> gsNodes = this.getGuaranteedSubList(astNode);
    			for(ASTNode gsNode : gsNodes){
    				/*
    				if(globalPreCond != null){
    					if(((ASTBinaryNode)gsNode).getLeftChild() != null){
    						ASTNode oldPreCond = ((ASTBinaryNode)gsNode).getLeftChild();
    						ASTNode newPreCond = new ASTNode();
    						newPreCond.setType(RCGConstants.LOGICAL_OPERATOR);
    						newPreCond.setLabel(RCGConstants.AND);
    						newPreCond.setObject(core.and);
    						newPreCond.addChild(oldPreCond);
    						newPreCond.addChild(globalPreCond);
    						if(gsNode.hasChild(oldPreCond)){
    							gsNode.getChildren().remove(oldPreCond);
    						}
    						((ASTBinaryNode)gsNode).setLeftChild(newPreCond);
    						
    					}
    					else{
    						((ASTBinaryNode)gsNode).setLeftChild(globalPreCond);
    					}
    				}
    				*/
    				ASTNode newAgrT = ((ASTBinaryNode)astNode).deepCopy();
    				/*
    				if(globalPreCond != null && newAgrT.getChildren().contains(globalPreCond)){
    					newAgrT.getChildren().remove(globalPreCond);
    					((ASTBinaryNode)newAgrT).setLeftChild(null);
    				}
    				*/
    				((ASTBinaryNode)newAgrT).getRightChild().getChildren().clear();
    				((ASTBinaryNode)newAgrT).getRightChild().addChild(gsNode);
    				
    				guaranteedStates.add(newAgrT);
    			}
    			
    		}
    	}
    	
    	return guaranteedStates;
    }
    
    private ArrayList<ASTNode> getGuaranteedSubList(ASTNode node) {
        ArrayList<ASTNode> guaranteedSubList = new ArrayList<ASTNode>();

        if (node.getChildren().size() > 0) {
            for (ASTNode child : node.getChildren()) {
                if (child != null) {
                    if (child.getObject() instanceof State) {
                        guaranteedSubList.add(child.deepCopy());
                    }
                    else {
                    	guaranteedSubList.addAll(this.getGuaranteedSubList(child));
                    }
                }
            }
        }        
        return guaranteedSubList;
    }    
}
