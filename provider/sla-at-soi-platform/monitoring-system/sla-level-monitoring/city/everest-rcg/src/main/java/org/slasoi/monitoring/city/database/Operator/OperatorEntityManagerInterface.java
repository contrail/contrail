/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.Operator;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.util.ArrayList;
import java.util.HashMap;

import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * This interface provides methods for managing the Operator entity.
 * 
 */
public interface OperatorEntityManagerInterface extends EntityManagerInterface {

    /**
     * Inserts an operator and its relevant information into table
     * 
     * @param slaUUId -
     *            the id of the sla that operator is specified within
     * @param atId -
     *            the id of the agreement term that operator is specified within
     * @param operatorCaseId -
     *            the operator case id
     * @param preconditionOf -
     *            shows the operator which current operator is precondition of
     * @param gsId -
     *            the guaranteed state id that operator is specified within
     * @param vdId -
     *            the variable declaration id that operator is related to
     * @param interfaceId -
     *            the interface id that operator is related to
     * @param parameterOf -
     *            shows the operator which current operator is parameter of
     * @param operatorSignature -
     *            the operator signature
     * @param operatorType -
     *            the operator type
     * @param operator -
     *            the operator itself
     * @param operatorPath -
     *            the operator's path
     */
    public void insert(String slaUUId, String atId, String operatorCaseId, String preconditionOf, String gsId,
            String vdId, String interfaceId, String parameterOf, String operatorSignature, String operatorType,
            String operator, Object[] operatorPath);

    /**
     * Retrieves the guaranteed states ids for a given sla and agreement term.
     * 
     * @param slaUuid
     * @param agreementTermId
     * @return
     */
    public ArrayList<String> getGuaranteedStatesIds(String slaUuid, String agreementTermId);

    /**
     * Retrieves a map containing the operators' paths per operator hash code for a given sla, agreement term and
     * guaranteed state.
     * 
     * @param slaUuid
     * @param agreementTermId
     * @param guaranteedStateId
     * @return
     */
    public HashMap<String, Object[]> getFunctExprOperatorHashCodesAndPaths(String slaUuid, String agreementTermId,
            String guaranteedStateId);

    /**
     * Checks whether there is a tuple for the given case id.
     * 
     * @param caseId
     * @return
     */
    public boolean existsRecord(String caseId);

    /**
     * Updates the tuple which caseId is equal to the given one with the given path.
     * 
     * @param caseId
     * @param qosTermPath
     */
    public void update(String caseId, Object[] qosTermPath);
}
