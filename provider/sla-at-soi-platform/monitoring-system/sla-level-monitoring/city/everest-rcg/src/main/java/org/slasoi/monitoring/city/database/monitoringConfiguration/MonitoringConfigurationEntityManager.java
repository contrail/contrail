/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.monitoringConfiguration;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.database.PersistentEntity;

/**
 * This class provides methods for managing the MonitoringConfiguration MySQL database table.
 * 
 */
public class MonitoringConfigurationEntityManager extends EntityManagerCommons implements EntityManagerInterface,
        MonitoringConfigurationEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(MonitoringConfigurationEntityManager.class);

    /**
     * Constructor.
     * 
     * @param connection
     */
    public MonitoringConfigurationEntityManager(Connection connection) {
        super(connection);
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
     */
    public ArrayList executeQuery(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#select()
     */
    public ArrayList select() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
     */
    public PersistentEntity selectByPrimaryKey(String entityPrimaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public int delete(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
     */
    public int deleteByPrimaryKey(String arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public int insert(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
     */
    public int update(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see org.slasoi.monitoring.city.database.monitoringConfiguration.MonitoringConfigurationEntityManagerInterface#addConfiguration(java.lang.String,
     *      java.lang.String, java.lang.Object)
     */
    public void addConfiguration(String slaUuid, String configurationId, Object configurationObject) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        Blob configurationBlob = null;
        try {
            configurationBlob = toBlob(configurationObject);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        
        try {
            pstmt =
                    getConnection().prepareStatement(
                            "INSERT INTO monitoring_configuration "
                                    + "(sla_uuid, configuration_id, configuration_object) " + "VALUES (?, ?, ?)");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, configurationId);
            pstmt.setBlob(3, configurationBlob);

            logger.debug(pstmt.toString());
            
            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
    }

}
