/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.PersistentEntity;

/**
 * This class manages the QoSTermParametricTemplate MySQL database table.
 * 
 */
public class FunctionalExprOperatorParametricTemplateEntityManager extends EntityManagerCommons implements
        FunctionalExprOperatorParametricTemplateEntityManagerInterface {

    // logger
    private static Logger logger = Logger.getLogger(FunctionalExprOperatorParametricTemplateEntityManager.class);

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#count()
     */
    @Override
    public int count() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#execute(java.lang.String)
     */
    @Override
    public boolean execute(String sql) {
        // TODO Auto-generated method stub
        return true;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#executeUpdate(java.lang.String)
     */
    @Override
    public int executeUpdate(String query) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#existsDatabase(java.lang.String)
     */
    @Override
    public boolean existsDatabase(String databaseName) {
        // TODO Auto-generated method stub
        return true;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#existsTable(java.lang.String, java.lang.String)
     */
    @Override
    public boolean existsTable(String databaseName, String tableName) {
        // TODO Auto-generated method stub
        return super.existsTable(databaseName, tableName);
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#getConnection()
     */
    @Override
    public Connection getConnection() {
        // TODO Auto-generated method stub
        return super.getConnection();
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#getDatabaseTable()
     */
    @Override
    public String getDatabaseTable() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#releaseResources(java.sql.PreparedStatement,
     *      java.sql.ResultSet)
     */
    @Override
    public void releaseResources(PreparedStatement pstmt, ResultSet resultSet) {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#setConnection(java.sql.Connection)
     */
    @Override
    public void setConnection(Connection connection) {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#setDatabaseTable(java.lang.String)
     */
    @Override
    public void setDatabaseTable(String databaseTable) {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#toBlob(java.lang.Object)
     */
    @Override
    public Blob toBlob(Object object) throws IOException, SQLException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#toObject(java.sql.Blob)
     */
    @Override
    public Object toObject(Blob blob) throws IOException, SQLException, ClassNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#truncate()
     */
    @Override
    public int truncate() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Constructor
     * 
     * @param connection
     */
    public FunctionalExprOperatorParametricTemplateEntityManager(Connection connection) {
        super(connection);
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
     */
    public ArrayList executeQuery(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#select()
     */
    public ArrayList select() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
     */
    public PersistentEntity selectByPrimaryKey(String entityPrimaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
     */
    public int deleteByPrimaryKey(String arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManagerInterface#getConditionsPointer(java.lang.String)
     */
    public String getConditionsPointer(String functionalExprOperator) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        String conditionsPointer = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT conditions_holdsAt_formula_pointer "
                                    + "FROM functional_expr_operator_parametric_template "
                                    + "WHERE functional_expr_operator = ?");
            pstmt.setString(1, functionalExprOperator);
            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                conditionsPointer = resultSet.getString("conditions_holdsAt_formula_pointer");
            }
        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        return conditionsPointer;
    }

    /**
     * @see org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManagerInterface#getTemplate(java.lang.String)
     */
    public String getTemplate(String functionalExprOperator) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        String template = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT parametric_theory_template " + "FROM functional_expr_operator_parametric_template "
                                    + "WHERE functional_expr_operator = ?");
            pstmt.setString(1, functionalExprOperator);
            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                template = resultSet.getString("parametric_theory_template");
            }
        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        return template;
    }

    /**
     * @see org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManagerInterface#insert(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public void insert(String functionalExprOperator, String parametricTemplate, String conditionsPointer) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        //logger.debug(getConnection());
        //System.out.println(getConnection());

        try {
            pstmt =
                    getConnection()
                            .prepareStatement(
                                    "INSERT INTO functional_expr_operator_parametric_template "
                                            + "(functional_expr_operator, parametric_theory_template, conditions_holdsAt_formula_pointer) "
                                            + "VALUES (?, ?, ?)");
            pstmt.setString(1, functionalExprOperator);
            pstmt.setString(2, parametricTemplate);
            pstmt.setString(3, conditionsPointer);
            logger.debug(pstmt.toString());
            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public int delete(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public int insert(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
     */
    public int update(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

}
