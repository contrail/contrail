/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.agreementTerm;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.everest.bse.Formula;

/**
 * This interface provides methods for managing the AgreementTerm entity.
 * 
 */
public interface AgreementTermEntityManagerInterface extends EntityManagerInterface {

    /**
     * Adds a agreement term and its relevant information.
     * 
     * @param slaUuid -
     *            the id of the sla that the agreement term is taken from
     * @param agreementTermId -
     *            the agreement term id
     * @param agreementTermHashCode -
     *            the agreement term hash code
     * @param agreementTermTheoryObject -
     *            the agreement term operable monitoring specifications
     */
    public void addAgreementTerm(String slaUuid, String agreementTermId, String agreementTermHashCode,
            LinkedHashMap<String, Formula> agreementTermTheoryObject);

    /**
     * Retrieves the agreement term hash code given the sla_uuid and agreementTermId.
     * 
     * @param slaUuid
     * @param agreementTermId
     * @return
     */
    public String getAgreementTermHashCode(String slaUuid, String agreementTermId);

    /**
     * Retrieves the agreement term ids of an SLA given the sla_uuid.
     * 
     * @param slaUuid
     * @param agreementTermId
     * @return
     */
    public ArrayList<String> getAgreementsTermIds(String slaUuid);

    /**
     * Retrieves the operable monitoring specifications for an sla agreement term given the sla_uuid and
     * agreementTermId.
     * 
     * @param slaUuid
     * @param agreementTermId
     * @return
     */
    public LinkedHashMap<String, Formula> getAgreementTermTheory(String slaUuid, String agreementTermId);

    /**
     * Retrieves a map of agreement term hash codes and ids for a given sla.
     * 
     * @param slaUuid
     * @return
     */
    public HashMap<String, String> getAgreementTermsHashCodesAndIds(String slaUuid);

}
