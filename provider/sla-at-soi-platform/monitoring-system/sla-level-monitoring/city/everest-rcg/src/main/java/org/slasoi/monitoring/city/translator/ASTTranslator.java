/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.translator;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManager;
import org.slasoi.monitoring.city.utils.ASTBinaryNode;
import org.slasoi.monitoring.city.utils.ASTNode;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.units;

import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.bse.FormulaWriter;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.monitor.Predicate;
//import uk.ac.city.soi.everest.util.PeriodicEventGenerationTask;
//import uk.ac.city.soi.everest.util.PeriodicEventManager;

/**
 * ASTTranslator is a class for translating Abstract Syntax Trees into EC-Assertion monitoring specifications.
 * 
 */
public class ASTTranslator {
    // initialise logging
    Logger logger = Logger.getLogger(ASTTranslator.class.getName());

    /**
     * Manager for inserting, selecting, deleting or updating QoSTermParametricTemplates.
     */
    private FunctionalExprOperatorParametricTemplateEntityManager functionalExprOperatorParametricTemplateStorer;

    private LinkedHashMap<String, String> parametricTemplates;
    private LinkedHashMap<ASTNode, Object> monitoringRuleBody;
    private LinkedHashMap<ASTNode, Object> monitoringRuleHead;
    private LinkedHashMap<ASTNode, Object[]> qosTermsPaths;
    private LinkedHashMap<ASTNode, String> qosTermsCases;
    private LinkedHashMap<ASTNode, ArrayList<LinkedHashMap<ASTNode, Object>>> monitoringRules;
    private ArrayList<String> qosTermsHoldsAtPredicatesInBody;
    private SLA inputSLA;
    //private PeriodicEventManager periodicEventManager;
    private LinkedHashMap<String, Predicate> monitoringRulePeriodicEvents;
    
    
    private LinkedHashMap<String, Variable> auxiliaryVariablesFromParametricTemplates = new LinkedHashMap<String, Variable>(); // quick fix to handle series as internal function
    private String notOperator = "";  // quick fix to handle NOT logical operator in the compileMonitoringRule method
    
    private ArrayList<ASTNode> nonParametricTemplateNodes = new ArrayList<ASTNode>(); 

    private LinkedHashMap<String, String> guaranteeTermFormulaIdMap;
    
    // EVEREST outer layer propagated through out EVEREST RCG for supporting periodic events
    private NewDataAnalyzerEC ndaec;
    


    /**
     * Constructor with values for class fields.
     * 
     * @param qosTermsPaths
     * @param qosTermsCases
     * @param inputSLA
     * @param ndaec
     */
    public ASTTranslator(LinkedHashMap<ASTNode, Object[]> qosTermsPaths, LinkedHashMap<ASTNode, String> qosTermsCases,
            SLA inputSLA, NewDataAnalyzerEC ndaec) {
        this.qosTermsPaths = qosTermsPaths;
        functionalExprOperatorParametricTemplateStorer =
                (FunctionalExprOperatorParametricTemplateEntityManager) RCGProperties
                        .getProperty(RCGConstants.FUNCTIONAL_EXPR_OPERATOR_PARAMETRIC_TEMPLATE_STORER_KEY);
        monitoringRuleBody = new LinkedHashMap<ASTNode, Object>();
        monitoringRuleHead = new LinkedHashMap<ASTNode, Object>();
        parametricTemplates = new LinkedHashMap<String, String>();
        this.qosTermsCases = qosTermsCases;
        monitoringRules = new LinkedHashMap<ASTNode, ArrayList<LinkedHashMap<ASTNode, Object>>>();
        qosTermsHoldsAtPredicatesInBody = new ArrayList<String>();
        this.inputSLA = inputSLA;
        //periodicEventManager = new PeriodicEventManager();
        monitoringRulePeriodicEvents = new LinkedHashMap<String, Predicate>();
        guaranteeTermFormulaIdMap = (LinkedHashMap<String, String>)RCGProperties.getProperty(RCGConstants.GUARANTEED_TERM_FORMULA_ID_MAP_KEY);
        
        this.ndaec = ndaec;
    }

    /**
     * Retrieves the parametric templates retrieved for the input AST
     * 
     * @return
     */
    public LinkedHashMap<String, String> getParametricTemplates() {
        return parametricTemplates;
    }

    /**
     * Retrieves the map for the monitoring rule body predicates that has been generated for the input AST.
     * 
     * @return
     */
    public LinkedHashMap<ASTNode, Object> getMonitoringRuleBody() {
        return monitoringRuleBody;
    }

    /**
     * Retrieves the map for the monitoring rule head predicates that has been generated for the input AST.
     * 
     * @return
     */
    public LinkedHashMap<ASTNode, Object> getMonitoringRuleHead() {
        return monitoringRuleHead;
    }

    /**
     * Retrieves the monitoring rule generated for the input AST.
     * 
     * @return
     */
    public ArrayList<LinkedHashMap<ASTNode, Object>> getMonitoringRule() {
        ArrayList<LinkedHashMap<ASTNode, Object>> monitoringRule = new ArrayList<LinkedHashMap<ASTNode, Object>>();
        monitoringRule.add(monitoringRuleBody);
        monitoringRule.add(monitoringRuleHead);
        return monitoringRule;
    }

    /**
     * Retrieves the monitoring rules generated for the input AST.
     * 
     * @return
     */
    public LinkedHashMap<ASTNode, ArrayList<LinkedHashMap<ASTNode, Object>>> getMonitoringRules() {
        return monitoringRules;
    }

    /**
     * Updates the monitoring rules generated for the input AST.
     * 
     */
    public void updateMonitoringRules(ASTNode ast) {
        ArrayList<LinkedHashMap<ASTNode, Object>> bodyAndHead = new ArrayList<LinkedHashMap<ASTNode, Object>>();
        bodyAndHead.add((LinkedHashMap<ASTNode, Object>) monitoringRuleBody.clone());
        bodyAndHead.add((LinkedHashMap<ASTNode, Object>) monitoringRuleHead.clone());
        monitoringRules.put(ast, bodyAndHead);
    }

    /**
     * Clears the monitoring rule body predicates map.
     */
    public void clearMonitoringrRuleBody() {
        this.monitoringRuleBody.clear();
    }

    /**
     * Clears the monitoring rule head predicates map.
     */
    public void clearMonitoringrRuleHead() {
        this.monitoringRuleHead.clear();
    }

    /**
     * Clears the operators (and qos terms) holds at body predicates map.
     */
    public void clearQosTermsHoldsAtPredicatesInBody() {
        this.qosTermsHoldsAtPredicatesInBody.clear();
    }

    /**
     * Clears the monitoring rule periodic events map.
     */
    public void clearMonitoringRulePeriodicEvents() {
        this.monitoringRulePeriodicEvents.clear();
    }

    /**
     * Retrieves the parametric templates (in XML format) for the input AST node (root) from the parametric template db
     * table.
     * 
     * @param root
     * @param initialNode
     */
    public void retrieveParametricTemplates(ASTNode root, ASTNode initialNode) {

        if (root != null && root.getType() != null) {

            // check the type of root
            // case that root type is IMPLICATION
            if (root.getType().equals(RCGConstants.IMPLICATION)) {
                // process the root LHS
                ASTNode body = ((ASTBinaryNode) root).getLeftChild();
                if (body != null) {
                    retrieveParametricTemplates(body, initialNode);
                }
                // process the root RHS
                ASTNode head = ((ASTBinaryNode) root).getRightChild();
                if (head != null) {
                    retrieveParametricTemplates(head, initialNode);
                }
            }
            // case that root type is LOGICAL_OPERATOR
            else if (root.getType().equals(RCGConstants.LOGICAL_OPERATOR)) {
                if (root.getChildren().size() > 0) {
                	int parametricTemplateSize = this.parametricTemplates.size();
                    for (ASTNode child : root.getChildren()) {
                        retrieveParametricTemplates(child, initialNode);
                        if(root.getLabel().equals(RCGConstants.AND) && parametricTemplates.size() == parametricTemplateSize)
                        	this.nonParametricTemplateNodes.add(child);                        
                    }
                }
            }
            // case that root type is COMPARISON_OPERATOR
            else if (root.getType().equals(RCGConstants.COMPARISON_OPERATOR)) {

                // process the root children
                if (root.getChildren().size() > 0) {
                    for (ASTNode child : root.getChildren()) {
                        retrieveParametricTemplates(child, initialNode);
                    }
                }
            }
            // case that child type is QOS_TERM supported by EVEREST
            else if (root.getType().contains(RCGConstants.SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE)) {
                // retrieve the child node QoS term
                Object qosTermPath[] = this.qosTermsPaths.get(root);
                String operatorSignature = (String) qosTermPath[7];
                STND operator = (STND) qosTermPath[9];
                String rootTerm = operator.toString();
                // String childQoSTerm = child.getType().substring(child.getType().indexOf(": "));

                // retrieve the parametric template for the child node QoS term
                String childParametricTemplateXML =
                        functionalExprOperatorParametricTemplateStorer.getTemplate(rootTerm);
                if (childParametricTemplateXML != null) {
                    if (!parametricTemplates.containsKey(operatorSignature)) {
                        // add the parametric template for the child node QoS term to parametric EC constructs
                        // collection
                        parametricTemplates.put(operatorSignature, childParametricTemplateXML);
                    }
                }
            }            
            else if (root.getType().contains(RCGConstants.INTERFACE_DECLARATION)) {
            	LinkedHashMap<String, Object> ifcDecl = (LinkedHashMap<String, Object>)root.getObject();
            	if(ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT) || ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT)){
                    //Object qosTermPath[] = this.qosTermsPaths.get(root);                	
                	//InterfaceDeclr interfaceDecl = (InterfaceDeclr)ifcDecl.get(RCGConstants.INTERFACE_DECLARATION);
                    String operatorSignature = root.getLabel();
            		//String operatorSignature = root.toString();
                    // retrieve the default parametric template for the child node QoS term
                    String childParametricTemplateXML =
                            functionalExprOperatorParametricTemplateStorer.getTemplate(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE);
                    if (childParametricTemplateXML != null) {
                        if (!parametricTemplates.containsKey(operatorSignature)) {
                            // add the parametric template for the child node QoS term to parametric EC constructs
                            // collection
                            parametricTemplates.put(operatorSignature, childParametricTemplateXML);
                        }
                    }                    
            	}            	
            }
        	
            // otherwise
            else {
                // visit root children
                if (root.getChildren().size() > 0) {
                    for (ASTNode child : root.getChildren()) {
                        retrieveParametricTemplates(child, initialNode);
                    }
                }
            }
        }
    }

    /**
     * Constructs on the fly the monitoring rule for the given AST node root.
     * 
     * @param root
     * @param initialNode
     * @param qosTermsFormulas
     */
    public void compileMonitoringRule(ASTNode root, ASTNode initialNode,
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas) {

        // check the type of root
        // case that root type is IMPLICATION
        if (root.getType().equals(RCGConstants.IMPLICATION)) {
            // process the root LHS
            ASTNode body = ((ASTNode) root).getChildren().get(0);
            if (body != null) {            	
                compileMonitoringRule(body, root, qosTermsFormulas);
            }
            // process the root RHS
            ASTNode head = ((ASTNode) root).getChildren().get(1);
            if (head != null) {            	
                compileMonitoringRule(head, root, qosTermsFormulas);
            }
        }
        // case that root type is LOGICAL_OPERATOR
        else if (root.getType().equals(RCGConstants.LOGICAL_OPERATOR)) {
        	if(root.getLabel().contains(core.not.toString())) notOperator = "true";
            if (root.getChildren().size() > 0) {
                for (ASTNode child : root.getChildren()) {
                    compileMonitoringRule(child, initialNode, qosTermsFormulas);
                }
            }
            notOperator = "";
        }
        // case that root type is COMPARISON_OPERATOR
        else if (root.getType().equals(RCGConstants.COMPARISON_OPERATOR)) {
            this.translateComparisonOperatorNode(root, initialNode, qosTermsFormulas);
        }
        
    }

    /**
     * Translates AST nodes containing operators(and qos terms) that are supported by parametric templates.
     * 
     * @param node
     * @param qosTermsFormulas
     * @return
     */
    private Variable translateOperatorNodeSupportedByParametricTemplate(ASTNode node,
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas) {    	
        Variable operatorVariable = null;
        this.auxiliaryVariablesFromParametricTemplates.clear();

        // retrieve the child node QoS term
        Object qosTermPath[] = null;
        ASTNode initNode = null;
        for (ASTNode key : qosTermsPaths.keySet()) {
            if (key.isEqualTo(node)) {
                qosTermPath = this.qosTermsPaths.get(key);
                initNode = key;
                break;
            }
        }

        if (qosTermPath != null) {
        	String operatorSignature = "";
        	String childQoSTermString = "";
            // retrieve the conditions pointer for the child node QoS term
            // String childQoSTermString = ((STND)qosTermPath[4]).toString();
        	if(node.getType().equals(RCGConstants.INTERFACE_DECLARATION)){
        		//LinkedHashMap<String, Object> ifcDecl = (LinkedHashMap<String, Object>)qosTermPath[5];
                operatorSignature = node.getLabel();
                //childQoSTermString = (String)ifcDecl.get(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE);
                childQoSTermString = RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE;
        	}
        	else{
                STND operator = ((STND) qosTermPath[9]);
                operatorSignature = ((String) qosTermPath[7]);
                childQoSTermString = operator.toString();        		
        	}

            String childQosTermHashCode = this.qosTermsCases.get(initNode);
            String conditionsPointer =
                    functionalExprOperatorParametricTemplateStorer.getConditionsPointer(childQoSTermString);

            // retrieve the pointed formula from qosTerms formulas
            Formula pointedFormula = null;
            
            LinkedHashMap<String, Formula> fs = qosTermsFormulas.get(operatorSignature);
            //System.out.println("+++++++++++> " + operatorSignature + "::" + fs);
            
            Set set = fs.entrySet();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                Map.Entry me = (Map.Entry) it.next();
                String formulaId = (String) me.getKey();

                if (formulaId.contains(conditionsPointer)) {
                    pointedFormula = new Formula((Formula) me.getValue());
                    break;
                }
            }

            ArrayList<Predicate> qosTermPredicateConditions = null;
            Predicate qosTermHoldsAtPredicate = null;
            Variable qosTermFluentArgument = null;
            /*
             * ATTENTION: The following code is based strictly on the parametric templates stored in
             * ./docs/QoSTermParametricTemplates/
             */
            if (pointedFormula != null) {
                // retrieve the qos term predicate conditions from the pointed formula
                qosTermPredicateConditions = (ArrayList<Predicate>) pointedFormula.getBody();
                // exclude the last HoldsAt predicate and
                // retrieve the qos term holds at predicate from the pointed formula
                qosTermHoldsAtPredicate =
                        qosTermPredicateConditions.remove(((ArrayList<Predicate>) pointedFormula.getBody()).size() - 1);

                /*******************************************************************************************************
                 * Manage qosTermPredicateConditions
                 ******************************************************************************************************/

                // in any case
                // check whether the monitoring rule body contains monitoring triggering conditions
                // (i.e. unconstrained Happens predicates) and if not, add the qos term preconditions
                boolean containsTriggeringConditions = false;
                Set set1 = this.monitoringRuleBody.entrySet();
                Iterator it1 = set1.iterator();
                outerLoop: while (it1.hasNext()) {
                    Map.Entry me = (Map.Entry) it1.next();
                    Object object = (Object) me.getValue();

                    if (object instanceof ArrayList) {
                        ArrayList<Predicate> conditions = (ArrayList<Predicate>) object;
                        for (Predicate predicate : conditions) {
                            if (predicate.isUnconstrained()
                                    && predicate.getEcName().equalsIgnoreCase(Constants.PRED_HAPPENS)) {
                                containsTriggeringConditions = true;
                                break outerLoop;
                            }
                        }
                    }
                }

                if (!containsTriggeringConditions) {
                    this.monitoringRuleBody.put(node, (Object) qosTermPredicateConditions);
                    // check whether periodic events are specified within the conditions
                    if (checkPredicateConditionsForPeriodicEvents(qosTermPredicateConditions)) {
                        // if yes keep track of the used periodic events
                        LinkedHashMap<String, Predicate> conditionsPeriodicEventsSpecs =
                                getPeriodicEventsFromConditions(qosTermPredicateConditions);
                        for (String key : conditionsPeriodicEventsSpecs.keySet()) {
                            monitoringRulePeriodicEvents.put(key, conditionsPeriodicEventsSpecs.get(key));
                        }
                    }
                }
                else {
                    // check whether periodic events are specified within the conditions
                    if (checkPredicateConditionsForPeriodicEvents(qosTermPredicateConditions)) {
                        LinkedHashMap<String, Predicate> conditionsPeriodicEventsSpecs =
                                getPeriodicEventsFromConditions(qosTermPredicateConditions);
                        ArrayList<Predicate> periodicEventsToBeAddedToMonitoringRuleBody = new ArrayList<Predicate>();
                        for (String key : conditionsPeriodicEventsSpecs.keySet()) {
                            // check whether the found periodic events are specified within the conditions are already
                            // used
                            if (!monitoringRulePeriodicEvents.containsKey(key)) {
                                // if no add them to the monitoring rule body and to used periodic events list
                                monitoringRulePeriodicEvents.put(key, conditionsPeriodicEventsSpecs.get(key));
                                periodicEventsToBeAddedToMonitoringRuleBody.add(conditionsPeriodicEventsSpecs.get(key));
                            }
                        }
                        this.monitoringRuleBody.put(node, (Object) periodicEventsToBeAddedToMonitoringRuleBody);
                    }
                }

                logger.debug("qosTermPredicateConditions :" + qosTermPredicateConditions);
                logger.debug("qosTermHoldsAtPredicate :" + qosTermHoldsAtPredicate);
                logger.debug("qosTermFluent :" + qosTermHoldsAtPredicate.getFluent());

                /*******************************************************************************************************
                 * Manage qosTermHoldsAtPredicate
                 ******************************************************************************************************/

                /*
                 * ATTENTION: The following code is based on the assumption that a qos term fluent contains ONLY ONE
                 * argument
                 */
                // retrieve the qos term argument (impl. as EVEREST variable) and
                // change make its name equal to qos Term
                qosTermFluentArgument = (Variable) qosTermHoldsAtPredicate.getFluent().getArguments().get(0);

                // add qosTermHoldsAtPredicate to monitoring rule body
                // case that monitoring rule body already contains the child qos term predicate conditions
                if (this.monitoringRuleBody.get(node) != null
                        && !qosTermsHoldsAtPredicatesInBody.contains(childQosTermHashCode)) {
                    // add the qosTermHoldsAtPredicate to the already existed predicate array list
                    ((ArrayList<Predicate>) this.monitoringRuleBody.get(node)).add(qosTermHoldsAtPredicate);
                    qosTermsHoldsAtPredicatesInBody.add(childQosTermHashCode);
                }
                else {
                    if (!qosTermsHoldsAtPredicatesInBody.contains(childQosTermHashCode)) {
                        this.monitoringRuleBody.put(node, qosTermHoldsAtPredicate);
                        qosTermsHoldsAtPredicatesInBody.add(childQosTermHashCode);
                    }
                }

                logger.debug("qosTermFluentArgument :" + qosTermFluentArgument);
                logger.debug("updated qosTermFluent :" + qosTermHoldsAtPredicate.getFluent());
                
                this.auxiliaryVariablesFromParametricTemplates.put(Constants.VAR_NAME_FLUENT_NAME, new Variable(Constants.VAR_NAME_FLUENT_NAME, Constants.DATA_TYPE_CONST, qosTermHoldsAtPredicate.getFluent().getFluentName()));

                /*******************************************************************************************************
                 * Manage compOperatorRelationalPredicate
                 ******************************************************************************************************/

                operatorVariable = qosTermFluentArgument;

            }
            else {
                logger.debug("NOTIFICATION: No pointed formula found");
            }
        }

        // check parameters
        if (node.getChildren().size() > 0) {
            for (ASTNode child : node.getChildren()) {
                if (child != null) {
                    // case that child type is periodic event operator
                    if (child.getType().contains(RCGConstants.PERIODIC_EVENT_OPERATOR)) {
                        long period = this.translatePeriodicEventOperatorNode(child);
                        if(period > 0)
                        	this.auxiliaryVariablesFromParametricTemplates.put(Constants.VAR_NAME_PERIOD, new Variable(Constants.VAR_NAME_PERIOD, Constants.DATA_TYPE_CONST, period));
                    }
                }
            }
        }

        return operatorVariable;
    }
    


    /**
     * Retrieves periodic events specifications (i.e. period and period start time) from body predicates (i.e.
     * conditions) of the monitoring rule.
     * 
     * @param conditions
     * @return
     */
    private LinkedHashMap<String, Predicate> getPeriodicEventsFromConditions(ArrayList<Predicate> conditions) {
        LinkedHashMap<String, Predicate> periodicEventsSpecs = new LinkedHashMap<String, Predicate>();

        for (Predicate condition : conditions) {
            if (condition.getOperationName().equals(Constants.PERIODIC_EVENT_OPERATION_NAME)) {
                String periodicEventKey = "";
                String periodicEventPeriod = "";
                String periodicEventPeriodStartTime = "";
                for (Variable periodicEventArgument : condition.getAllVariablesOnly()) {
                    if (periodicEventArgument.getName().contains(Constants.PERIODIC_EVENT_PERIOD)) {
                        periodicEventPeriod = periodicEventArgument.getValue().toString();
                    }
                    if (periodicEventArgument.getName().contains(Constants.PERIODIC_EVENT_PERIOD_START_TIME)) {
                        periodicEventPeriodStartTime = periodicEventArgument.getValue().toString();
                    }
                }
                periodicEventKey = periodicEventPeriod + ":" + periodicEventPeriodStartTime;
                periodicEventsSpecs.put(periodicEventKey, condition);
            }
        }

        return periodicEventsSpecs;
    }

    /**
     * Utility method that checks whether a list of predicate instances (i.e. conditions) contains a predicate that
     * represents a periodic event.
     * 
     * @param conditions
     * @return
     */
    private boolean checkPredicateConditionsForPeriodicEvents(ArrayList<Predicate> conditions) {
        boolean foundPeriodicEvent = false;

        for (Predicate condition : conditions) {
            if (condition.getOperationName().equals(Constants.PERIODIC_EVENT_OPERATION_NAME)) {
                foundPeriodicEvent = true;
                break;
            }
        }

        return foundPeriodicEvent;
    }

    /**
     * Generates and returns an EVEREST constant by processing the input node.
     * 
     * @param node
     * @return
     */
    private Variable translateConstantNode(ASTNode node) {

        // generate an EVEREST constant
        Variable everestConstant = this.generateEverestConstant(node);
        // add the everestConstant to the root relational predicate

        logger.debug("everestConstant: " + everestConstant);

        return everestConstant;
    }
    

    /**
     * Create a new periodic event generation task by accessing the node children and by using the periodic event
     * generation uk.ac.city.soi.everest utilities.
     * 
     * @param node
     */
    private long translatePeriodicEventOperatorNode(ASTNode node) {

        // initialise period and peridoStartTime for the periodic operator node
        long period = 0l;
        long periodStartTime = 0l;

        // retrieve the child node QoS term
        Object qosTermPath[] = null;
        for (ASTNode key : qosTermsPaths.keySet()) {
            if (key.isEqualTo(node)) {
                qosTermPath = this.qosTermsPaths.get(key);
                break;
            }
        }

        // retrieve period
        if (qosTermPath != null) {

            // check parameters to retrieve the period
            if (node.getChildren().size() > 0) {
                for (ASTNode child : node.getChildren()) {
                    if (child != null) {
                        // case that parameter is a constant
                        if (child.getType().equals(RCGConstants.CONSTANT)) {
                            // SLA constant value
                            String constantValue = ((CONST) child.getObject()).getValue();
                            // SLA constant type
                            STND constantSLADataType = ((CONST) child.getObject()).getDatatype();

                            if (constantSLADataType != null) {
                                Double doubleConstantValue = Double.parseDouble(constantValue);
                                // case that SLA constant type is measured in ms units
                                if (constantSLADataType.equals(units.ms)) {
                                    period = Long.parseLong(constantValue);
                                }
                                // case that SLA constant type is measured in second units
                                else if (constantSLADataType.equals(units.s)) {
                                    // convert seconds into msecs
                                    period = ((Double) (doubleConstantValue * 1000)).longValue();
                                }
                                // case that SLA constant type is measured in minute units
                                if (constantSLADataType.equals(units.min)) {
                                    // convert minutes into msecs
                                    period = ((Double) (doubleConstantValue * 60 * 1000)).longValue();
                                }
                                // case that SLA constant type is measured in hour units
                                else if (constantSLADataType.equals(units.hrs)) {
                                    // convert hours into msecs
                                    period = ((Double) (doubleConstantValue * 60 * 60 * 1000)).longValue();
                                }
                                // case that SLA constant type is measured in day units
                                else if (constantSLADataType.equals(units.day)) {
                                    // convert days into msecs
                                    period = ((Double) (doubleConstantValue * 24 * 60 * 60 * 1000)).longValue();
                                }
                                // case that SLA constant type is measured in week units
                                else if (constantSLADataType.equals(units.week)) {
                                    // convert days into msecs
                                    period = ((Double) (doubleConstantValue * 7 * 24 * 60 * 60 * 1000)).longValue();
                                }                                
                                // case that SLA constant type is measured in month units
                                else if (constantSLADataType.equals(units.month)) {
                                    // convert months into msecs
                                    period = ((Double) (doubleConstantValue * 30 * 24 * 60 * 60 * 1000)).longValue();
                                }
                                // case that SLA constant type is measured in year units
                                else if (constantSLADataType.equals(units.year)) {
                                    // convert years into msecs
                                    period =
                                            ((Double) (doubleConstantValue * 12 * 30 * 24 * 60 * 60 * 1000))
                                                    .longValue();
                                }
                                else if (constantSLADataType.equals(units.tx_per_s)) {
                                    // convert tx_per_s into tx_per_msecs
                                    period =
                                            ((Double) (doubleConstantValue / 1000))
                                                    .longValue();
                                }
                            }
                        }
                    }
                }
            }

            // retrieve periodStartTime
            // periodStartTime : SLAEffectiveFromTime in ms
            periodStartTime = ((TIME) this.inputSLA.getEffectiveFrom()).getValue().getTimeInMillis();
        }

        // instantiate a new periodicEventGenerationTask
        /*
        PeriodicEventGenerationTask task = new PeriodicEventGenerationTask(period, periodStartTime, this.ndaec);
        periodicEventManager.addPeriodicEventsGenerationTasks(period, periodStartTime, task);
        logger.debug("periodicEventManager.getPeriodicEventsGenerationTasks(): "
                + periodicEventManager.getPeriodicEventsGenerationTasks());
        */
        return period;

    }

    /**
     * Translates AST nodes containing comparison operators.
     * 
     * @param node
     * @param initialNode
     * @param qosTermsFormulas
     */
    private void translateComparisonOperatorNode(ASTNode node, ASTNode initialNode,
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas) {
        // generate the corresponding EVEREST relational predicate
        Predicate compOperatorRelationalPredicate = this.generateRelationalPredicate(node);
        
        if(notOperator.equals("true")) compOperatorRelationalPredicate.setNegated(true);
        
        String compType = ""; 
        //boolean foundVar = false;
        //boolean sizeZero = true;
        String type = "";
        // process the root children
        if (node.getChildren().size() > 0) {
        	//sizeZero = false;
            for (ASTNode child : node.getChildren()) {
                if (child != null) {                	
                    // case that child type is functional operator supported by parametric templates
                    if (child.getType().contains(RCGConstants.SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE)) {
                        compOperatorRelationalPredicate.addVariable(this
                                .translateOperatorNodeSupportedByParametricTemplate(child, qosTermsFormulas));
                        compType += "v";
                        //foundVar = true;
                    }
                    // case that child node is constant
                    else if (child.getType().equals(RCGConstants.CONSTANT)) {
                        compOperatorRelationalPredicate.addVariable(this.translateConstantNode(child));
                        compType += "c";
                        //foundVar = true;
                    }
                    // case that child node is variable
                    else if (child.getType().equals(RCGConstants.VARIABLE)) {
                        //compOperatorRelationalPredicate.addVariable(this.translateVariableNode(child));
                        compType += "v";
                        //foundVar = true;
                    }
                    // case that child type is functional operator supported by EVEREST internal function
                    else if (child.getType().contains(RCGConstants.SUPPORTED_OPERATOR_BY_EVEREST_INTERNAL_FUNCTION)) {
                        compOperatorRelationalPredicate.addFunction(this.generateEverestFunction(child, initialNode,
                                qosTermsFormulas));
                        compType += "f";
                        //compOperatorRelationalPredicate.setPrefix(Constants.RELATION_FC);
                        //foundVar = true;
                    }
                    else if (child.getType().equals(RCGConstants.INTERFACE_DECLARATION)) {
                    	LinkedHashMap<String, Object> ifcDecl = (LinkedHashMap<String, Object>)child.getObject();
                    	if(ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT) || ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT)){
                            Variable var =
                                this.translateOperatorNodeSupportedByParametricTemplate(child, qosTermsFormulas);
                            compType += "v";
                            compOperatorRelationalPredicate.addVariable(var);
                            
                    	}
                    }                    
                    else{
                    	type += child.getType();
                    }
                }
            }
        }
        compOperatorRelationalPredicate.setPrefix(compType);

        // add compOperatorRelationalPredicate to monitoring rule
        // by checking the relevant position of root with respect to initial node
        // case that initial Node is ASTBinaryNode
        if (initialNode instanceof ASTBinaryNode) {
            // check whether root is in left or right subtree of initial node
            if (((ASTBinaryNode) initialNode).hasInLeftSubtree(node)) {
                // if yes, add root relational predicate to the monitoring rule body
                this.monitoringRuleBody.put(node, compOperatorRelationalPredicate);
            }
            else {
                // add root relational predicate to the monitoring rule head
                this.monitoringRuleHead.put(node, compOperatorRelationalPredicate);
            }
        }
        else {
            // add root relational predicate to the monitoring rule head
            this.monitoringRuleHead.put(node, compOperatorRelationalPredicate);
        }
    }

    /**
     * Generates a relational predicate by processing the input node.
     * 
     * @param node
     * @return
     */
    private Predicate generateRelationalPredicate(ASTNode node) {
        // construct a new EVEREST relational predicate
        Predicate relationalPred = new Predicate(Constants.PRED_RELATION, Constants.RELATION_VC, "", "", "");

        // check the node comparison operator
        // and set the relation function in the EVEREST relational predicate
        STND operator = (STND) node.getObject();
        // case that operator is >
        if (operator.equals(core.$greater_than)) {
            relationalPred.setOperationName(Constants.GREATER_THAN);
        }
        // case that operator's >=
        else if (operator.equals(core.$greater_than_or_equals)) {
            relationalPred.setOperationName(Constants.GREATER_THAN_EQUAL);
        }
        // case that operator's <
        else if (operator.equals(core.$less_than)) {
            relationalPred.setOperationName(Constants.LESS_THAN);
        }
        // case that operator's <=
        else if (operator.equals(core.$less_than_or_equals)) {
            relationalPred.setOperationName(Constants.LESS_THAN_EQUAL);
        }
        // case that operator's =
        else if (operator.equals(core.$equals)) {
            relationalPred.setOperationName(Constants.EQUAL);
        }
        // case that operator's !=
        else if (operator.equals(core.$not_equals)) {
            relationalPred.setOperationName(Constants.NOT_EQUAL);
        }
        // any other comparison operator that is not supported by EVEREST
        else {
            logger.debug("NOTIFICATION: The comparison operator " + operator + " is not supported by EVEREST");
            //System.out.println("OOOOOOOOOOOOOOOOOOOOOOPS " + relationalPred.toString());
        }

        return relationalPred;
    }

    /**
     * Generates an EVEREST constant by processing the input node.
     * 
     * @param node
     * @return
     */
    private Variable generateEverestConstant(ASTNode node) {
        // EVEREST constant name
        String constantName = node.hashCode() + ":" + RCGConstants.CONSTANT;
        // EVEREST constant constname
        String constantConstName = "v" + constantName;
        // SLA constant value
        String constantValue = ((CONST) node.getObject()).getValue();
        // SLA constant type
        STND constantSLADataType = ((CONST) node.getObject()).getDatatype();

        if (constantSLADataType != null) {
            // case that SLA constant type is measured in percentage units
            if (constantSLADataType.equals(units.percentage)) {
                double doubleConstantValue = Double.parseDouble(constantValue);
                doubleConstantValue = doubleConstantValue / 100;
                constantValue = ((Object) doubleConstantValue).toString();
            }
            // case that SLA constant type is measured in day units
            if (constantSLADataType.equals(units.day)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert days into msecs
                long doubleConstantValueInDays = doubleConstantValue * 24 * 60 * 60 * 1000;
                constantValue = ((Object) doubleConstantValueInDays).toString();
            }
            // case that SLA constant type is measured in months units
            if (constantSLADataType.equals(units.month)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert months into msecs
                long doubleConstantValueInMonths = doubleConstantValue * 30 * 24 * 60 * 60 * 1000;
                constantValue = ((Object) doubleConstantValueInMonths).toString();
            }
            // case that SLA constant type is measured in tx_per_s units
            if (constantSLADataType.equals(units.tx_per_s)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert months into msecs
                double doubleConstantValueInMS = (double)(doubleConstantValue)/1000;
                constantValue = ((Object) doubleConstantValueInMS).toString();
            }                 
            if (constantSLADataType.equals(units.hrs)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert hrs into msecs
                long doubleConstantValueInMS = doubleConstantValue * 60 * 60 * 1000;
                constantValue = ((Object) doubleConstantValueInMS).toString();
            }
            if (constantSLADataType.equals(units.min)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert min into msecs
                long doubleConstantValueInMS = doubleConstantValue * 60 * 1000;
                constantValue = ((Object) doubleConstantValueInMS).toString();
            }
            if (constantSLADataType.equals(units.s)) {
                long doubleConstantValue = Long.parseLong(constantValue);
                // convert s into msecs
                long doubleConstantValueInMS = doubleConstantValue * 1000;
                constantValue = ((Object) doubleConstantValueInMS).toString();
            }                                    
        }

        /*
         * ATTENTION: More checks and modifications with regards to SLA constant type might be necessary
         */

        // construct a new EVEREST constant
        Variable everestConstant =
                new Variable(constantConstName, constantName, Constants.DATA_TYPE_CONST, constantValue);

        return everestConstant;
    }


    /**
     * Generates an EVEREST function by processing the input node.
     * 
     * @param node
     * @param initialNode
     * @param qosTermsFormulas
     * @return
     */
    private Function generateEverestFunction(ASTNode node, ASTNode initialNode,
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas) {
        Function function = new Function();

        // retrieve the node path
        Object qosTermPath[] = null;
        for (ASTNode key : qosTermsPaths.keySet()) {
            if (key.isEqualTo(node)) {
                qosTermPath = this.qosTermsPaths.get(key);
                break;
            }
        }

        // set function fields
        if (qosTermPath != null) {

            STND operator = ((STND) qosTermPath[9]);

            // set operation name
            if (operator.equals(core.add)) {
                function.setOperationName(Constants.FUNCTION_ADD);
            }
            else if (operator.equals(core.subtract)) {
                function.setOperationName(Constants.FUNCTION_SUB);
            }
            else if (operator.equals(core.multiply)) {
                function.setOperationName(Constants.FUNCTION_MUL);
            }
            else if (operator.equals(core.divide)) {
                function.setOperationName(Constants.FUNCTION_DIV);
            }
            else if (operator.equals(core.sum)) {
                function.setOperationName(Constants.FUNCTION_SUM);
            }
            else if (operator.equals(core.mean)) {
                function.setOperationName(Constants.FUNCTION_AVG);
            }
            else if (operator.equals(core.modulo)) {
                function.setOperationName(Constants.FUNCTION_MDL);
            }
            else if (operator.equals(core.round)) {
                function.setOperationName(Constants.FUNCTION_RND);
            }
            else if (operator.equals(core.std)) {
                function.setOperationName(Constants.FUNCTION_STD);
            }
            else if (operator.equals(core.median)) {
                function.setOperationName(Constants.FUNCTION_MED);
            }
            else if (operator.equals(core.max)) {
                function.setOperationName(Constants.FUNCTION_MAX);
            }
            else if (operator.equals(core.min)) {
                function.setOperationName(Constants.FUNCTION_MIN);
            }
            else if (operator.equals(core.mode)) {
                function.setOperationName(Constants.FUNCTION_MOD);
            }            
            else if (operator.equals(core.series) || operator.equals("http://www.slaatsoi.org/extension#series")) {
                function.setOperationName(Constants.FUNCTION_SER);
            }
            else if (operator.equals(core.value)) {
                function.setOperationName(Constants.FUNCTION_VAL);
            }            
            else if (operator.equals(core.count)) {
                function.setOperationName(Constants.FUNCTION_SIZ);
            }
            else if(operator.equals(core.day_is)){
            	function.setOperationName(Constants.FUNCTION_GET_DAY);
            }
            else if(operator.equals(core.time_is)){
            	function.setOperationName(Constants.FUNCTION_GET_TIME);
            }
            else if(operator.equals(core.month_is)){
            	function.setOperationName(Constants.FUNCTION_GET_MONTH);
            }            
            else if(operator.equals(core.year_is)){
            	function.setOperationName(Constants.FUNCTION_GET_YEAR);
            }            
            else {
                logger.debug("NOTIFICATION: Unsupported operator - " + operator);
            }

            // set parameters
            if (node.getChildren().size() > 0) {
                for (ASTNode child : node.getChildren()) {
                    if (child != null) {
                        // case that parameter is a SUPPORTED_OPERATOR_BY_EVEREST_INTERNAL_FUNCTION
                        if (child.getType().contains(RCGConstants.SUPPORTED_OPERATOR_BY_EVEREST_INTERNAL_FUNCTION)) {
                            Function innerFunctionParameter =
                                    this.generateEverestFunction(child, initialNode, qosTermsFormulas);
                            function.addParameter(innerFunctionParameter);
                        }
                        // case that child node is constant
                        else if (child.getType().equals(RCGConstants.CONSTANT)) {
                            Variable variableParameter = this.generateEverestConstant(child);
                            function.addParameter(variableParameter);
                        }
                        // case that child node is SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE
                        else if (child.getType().equals(RCGConstants.SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE)) {
                            Variable variableParameter =
                                    this.translateOperatorNodeSupportedByParametricTemplate(child, qosTermsFormulas);
                            function.addParameter(variableParameter);
                            if(function.getOperationName().equals(Constants.FUNCTION_SER) && this.auxiliaryVariablesFromParametricTemplates.size() > 0){
                            	
                            	
                            	Iterator<String> avi = this.auxiliaryVariablesFromParametricTemplates.keySet().iterator();
                            	while(avi.hasNext()){
                            		String key = avi.next();
                            		Variable var =  this.auxiliaryVariablesFromParametricTemplates.get(key);
                            		function.addParameter(var);
                            	}
                            	
                            }
                            else if(function.getOperationName().equals(Constants.FUNCTION_VAL) && this.auxiliaryVariablesFromParametricTemplates.size() > 0){                            	
                            	Iterator<String> avi = this.auxiliaryVariablesFromParametricTemplates.keySet().iterator();
                            	while(avi.hasNext()){                            		
                            		String key = avi.next();
                            		Variable var =  this.auxiliaryVariablesFromParametricTemplates.get(key);
                            		function.addParameter(var);
                            	}                            	
                            }
                        }
                        else if (child.getType().equals(RCGConstants.INTERFACE_DECLARATION)) {
                        	LinkedHashMap<String, Object> ifcDecl = (LinkedHashMap<String, Object>)child.getObject();
                        	if(ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT) || ifcDecl.containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT)){
                                Variable variableParameter =
                                    this.translateOperatorNodeSupportedByParametricTemplate(child, qosTermsFormulas);
                                function.addParameter(variableParameter);
                                if(function.getOperationName().equals(Constants.FUNCTION_SER) && this.auxiliaryVariablesFromParametricTemplates.size() > 0){
                                	
                                	Iterator<String> avi = this.auxiliaryVariablesFromParametricTemplates.keySet().iterator();
                                	while(avi.hasNext()){
                                		String key = avi.next();
                                		Variable var =  this.auxiliaryVariablesFromParametricTemplates.get(key);
                                		function.addParameter(var);
                                	}
                                	
                                }                                
                        	}
                        }
                        // case that child type is periodic event operator
                        else if (child.getType().contains(RCGConstants.PERIODIC_EVENT_OPERATOR)) {
                            long period = this.translatePeriodicEventOperatorNode(child);
                            if(period > 0 && function.getOperationName().equals(Constants.FUNCTION_SER)){
                            	function.addParameter(new Variable(Constants.VAR_NAME_PERIOD, Constants.DATA_TYPE_CONST, period));
                            }
                            else if(period > 0 && function.getOperationName().equals(Constants.FUNCTION_VAL)){
                            	function.addParameter(new Variable(Constants.VAR_NAME_PERIOD, Constants.DATA_TYPE_CONST, period));
                            } 
                        }
                    }
                }
            }

        }

        return function;
    }

    /**
     * Generates the operational monitoring theory for a given assigned expression.
     * 
     * @param assignedExpressionCaseId
     * @param qosTermsFormulas
     * @return
     */
    public LinkedHashMap<String, Formula> compileOperationalMonitoringTheory(String assignedExpressionCaseId,
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas) {
        // initialise OperationalMonitoringTheory
        LinkedHashMap<String, Formula> operationalMonitoringTheory = new LinkedHashMap<String, Formula>();

        LinkedHashMap<String, Formula> formulasTobeAddedAtTheEnd = new LinkedHashMap<String, Formula>();
        /***************************************************************************************************************
         * Add assumptions formulas to OperationalMonitoringTheory
         **************************************************************************************************************/

        Set set = qosTermsFormulas.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            String operatorNodeSignature = (String) me.getKey();

            Set set0 = qosTermsFormulas.get(operatorNodeSignature).entrySet();
            Iterator it0 = set0.iterator();
            String assumptionFormulaId = "";
            while (it0.hasNext()) {
                Map.Entry me0 = (Map.Entry) it0.next();
                assumptionFormulaId = (String) me0.getKey();
                Formula assumption = (Formula) me0.getValue();
                logger.debug("assumption id: " + assumption.getFormulaId());
                allignPredicatePrefixWithStatus(assumption);

                // add assumption to OperationalMonitoringTheory
                operationalMonitoringTheory.put(assumptionFormulaId, assumption);
            }
            
            
            if(!assumptionFormulaId.equals("")){
            	String qosTerm = assumptionFormulaId.substring(assumptionFormulaId.indexOf(".") + 1, assumptionFormulaId.lastIndexOf(".")).toLowerCase();
            	//System.out.println("555555555 QoS Term " + qosTerm);
            	String pointedFormulaId = functionalExprOperatorParametricTemplateStorer.getConditionsPointer(qosTerm);
            	
            	String[] keys = operationalMonitoringTheory.keySet().toArray(new String[operationalMonitoringTheory.size()]);
            	String lastKey = keys[keys.length - 1];
            	if(!lastKey.contains(pointedFormulaId)){
            		Formula f = operationalMonitoringTheory.get(lastKey);
            		formulasTobeAddedAtTheEnd.put(lastKey, f);
            		operationalMonitoringTheory.remove(lastKey);
            	}
            }            
        }

        /***************************************************************************************************************
         * Add monitoring rules formula to OperationalMonitoringTheory
         **************************************************************************************************************/

        for (ASTNode resolvedAST : monitoringRules.keySet()) {

            // initialise the monitoring rule formula object for root
            Formula monitoringRuleFormula = new Formula();

            // construct and set monitoringRuleFormula id
            int resolvedASTHashCode = resolvedAST.hashCode();
            String qosTermName = this.getQoSTermName(resolvedAST);
            String monitoringRuleFormulaId =
                    RCGConstants.MONITORING_RULE_INITIALS + resolvedASTHashCode + "." + assignedExpressionCaseId + ":" + qosTermName;
            monitoringRuleFormula.setFormulaId(monitoringRuleFormulaId);
            String gsName = getGuaranteedStateName(resolvedAST);
            
            this.guaranteeTermFormulaIdMap.put(monitoringRuleFormulaId, gsName);
            //System.out.println("SSSSSSSSSSSSSSMap " + gsName + "==" + monitoringRuleFormulaId);            
            // get monitoring rule body and head
            LinkedHashMap<ASTNode, Object> resolvedASTMonitoringRuleBody = monitoringRules.get(resolvedAST).get(0);
            LinkedHashMap<ASTNode, Object> resolvedASTMonitoringRuleHead = monitoringRules.get(resolvedAST).get(1);

            // construct the monitoringRuleFormula body
            Set set1 = resolvedASTMonitoringRuleBody.entrySet();
            Iterator it1 = set1.iterator();
            outerLoop: while (it1.hasNext()) {
                Map.Entry me1 = (Map.Entry) it1.next();
                Object object1 = (Object) me1.getValue();

                if (object1 instanceof ArrayList) {
                    ArrayList<Predicate> predicates = (ArrayList<Predicate>) object1;
                    monitoringRuleFormula.addAllToBody(predicates);
                }
                else if (object1 instanceof Predicate) {
                    monitoringRuleFormula.addPredicateToBody((Predicate) object1);
                }
            }

            // construct the monitoringRuleFormula head
            Set set2 = resolvedASTMonitoringRuleHead.entrySet();
            Iterator it2 = set2.iterator();
            outerLoop: while (it2.hasNext()) {
                Map.Entry me2 = (Map.Entry) it2.next();
                Object object2 = (Object) me2.getValue();

                if (object2 instanceof Predicate) {
                    Predicate predicate = (Predicate) object2;
                    monitoringRuleFormula.addPredicateToHead(predicate);
                }
            }

            logger.debug("monitoringRuleFormula: " + monitoringRuleFormula.toString());

            logger.debug("monitoringRuleFormula id: " + monitoringRuleFormula.getFormulaId());
            allignPredicatePrefixWithStatus(monitoringRuleFormula);

            // add monitoringRuleFormula to operationalMonitoringTheory
            operationalMonitoringTheory.put(monitoringRuleFormulaId, monitoringRuleFormula);
        }

        if(formulasTobeAddedAtTheEnd.size() > 0){
        	Iterator<String> i = formulasTobeAddedAtTheEnd.keySet().iterator();
        	while(i.hasNext()){
        		String key = i.next();
        		Formula f = formulasTobeAddedAtTheEnd.get(key);
        		operationalMonitoringTheory.put(key, f);
        	}
        }
        //logger.debug("operationalMonitoringTheory: " + operationalMonitoringTheory.toString());

        return operationalMonitoringTheory;
    }

    /**
     * Changes the predicates prefixes included in a formula according to the value of the corresponding Status
     * variables.
     * 
     * @param formula
     */
    private void allignPredicatePrefixWithStatus(Formula formula) {
        ArrayList<Predicate> formulaPredicates = new ArrayList<Predicate>();
        formulaPredicates.addAll(formula.getBody());
        formulaPredicates.addAll(formula.getHead());

        for (Predicate predicate : formulaPredicates) {
            if (!predicate.getEcName().equals(Constants.PRED_HOLDSAT)
                    && !predicate.getEcName().equals(Constants.PRED_INITIALLY)
                    && !predicate.getEcName().equals(Constants.PRED_RELATION)) {
                if (predicate.getPrefix() != null && predicate.getPrefix() != "") {
                    for (Variable variable : predicate.getAllVariablesOnly()) {
                        if (variable.getConstName().contains("status")) {
                            if ((variable.getValue().equals(Constants.STATUS_REQUEST_AFTER) || variable.getValue()
                                    .equals(Constants.STATUS_REQUEST_BEFORE))
                                    && !predicate.getPrefix().equals(Constants.PREFIX_IC)) {
                                predicate.setPrefix(Constants.PREFIX_IC);
                                logger.debug("set prefix to: " + Constants.PREFIX_IC);
                            }
                            else if ((variable.getValue().equals(Constants.STATUS_RESPONSE_AFTER) || variable
                                    .getValue().equals(Constants.STATUS_RESPONSE_BEFORE))
                                    && !predicate.getPrefix().equals(Constants.PREFIX_IR)) {
                                predicate.setPrefix(Constants.PREFIX_IR);
                                logger.debug("set prefix to: " + Constants.PREFIX_IR);
                            }
                        }
                    }
                }
            }
        }
    }
    //The following method assume that only one guarantee term may appear in a agreement term 
    private String getGuaranteedStateName(ASTNode node) {
        String name = "";

        if (node.getChildren().size() > 0) {
            for (ASTNode child : node.getChildren()) {
                if (child != null) {
                    if (child.getObject() instanceof State) {
                    	name = ((State)child.getObject()).getId().toString();
                    	break;
                    }
                    else {
                    	name = getGuaranteedStateName(child);                    	
                    }
                }
            }
        }        
        return name;
    }
    
    
    //The following method assume that only one QoS term may appear in a guarantee term
    private String getQoSTermName(ASTNode node) {
        String name = "";

        if (node.getChildren().size() > 0) {
            for (ASTNode child : node.getChildren()) {
                if (child != null) {
                    if (child.getType().equals(RCGConstants.SUPPORTED_OPERATOR_BY_PARAMETRIC_TEMPLATE)) {
                    	name = child.getLabel();
                    	break;
                    }
                    else {
                    	name = getQoSTermName(child);                    	
                    }
                }
            }
        }        
        return name;
    }       
}
