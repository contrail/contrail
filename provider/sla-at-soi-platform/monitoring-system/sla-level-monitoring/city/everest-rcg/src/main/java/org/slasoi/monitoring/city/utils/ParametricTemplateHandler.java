/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.utils;

/**
 * @author SLA@SOI (City)
 *
 * @date 29 June 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */
import java.io.FileNotFoundException;
import java.io.IOException;

import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManager;
import org.slasoi.slamodel.vocab.core;
import org.w3c.dom.Document;

import uk.ac.city.soi.everest.bse.XMLConverter;
import uk.ac.city.soi.everest.bse.XMLReader;

/**
 * ParametricTemplateHandler is a utility class for inserting parametric templates for supported operators in RCG db.
 * 
 */
public class ParametricTemplateHandler {

    /**
     * Manager for inserting, selecting, deleting or updating QoSTermParametricTemplates.
     */
    private FunctionalExprOperatorParametricTemplateEntityManager fExOpParTemplateStorer;

    /**
     * Constructor.
     */
    public ParametricTemplateHandler() {

        fExOpParTemplateStorer =
                (FunctionalExprOperatorParametricTemplateEntityManager) RCGProperties
                        .getProperty(RCGConstants.FUNCTIONAL_EXPR_OPERATOR_PARAMETRIC_TEMPLATE_STORER_KEY);
    }

    /**
     * Inserts given parametric template in parametric template db table.
     * 
     * @param operatorName -
     *            name of the operator to be inserted
     * @param parametricTemplateXMLfile -
     *            string representations of the XML parametric template to be inserted
     * @param conditions -
     *            the id of the conditions that are necessary for the monitoring of the operator
     */
    public void insertQoSTerm(String operatorName, String parametricTemplateXMLfile, String conditions) {

        String parametricTemplate = "";

        if (!parametricTemplateXMLfile.equals("")) {
            // read the xml file and parse it into a Document object
            Document templateDocument = new XMLReader().read(parametricTemplateXMLfile);
            // transform the document object into a String object
            parametricTemplate = XMLConverter.toString(templateDocument.getDocumentElement());
        }

        fExOpParTemplateStorer.insert(operatorName, parametricTemplate, conditions);
    }

    /**
     * A main for populating the parametric template db table.
     * 
     * @param args
     * @throws FileNotFoundException
     * @throws IOException
     */
    /*
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ParametricTemplateHandler handler = new ParametricTemplateHandler();
        // handler.insertQoSTerm(common.mttr.toString(), "./docs/QoSTermParametricTemplates/parametricMTTRTheory.xml",
        // "A5.MTTR");
        // handler.insertQoSTerm(core.time_is.toString(),
        // "./docs/QoSTermParametricTemplates/parametricTimeIsTheory.xml", "A1.TimeIs");
        // handler.insertQoSTerm(core.day_is.toString(), "./docs/QoSTermParametricTemplates/parametricDayIsTheory.xml",
        // "A1.DayIs");
        handler.insertQoSTerm(core.series.toString(), "./docs/QoSTermParametricTemplates/parametricSeriesTheory.xml",
                "A2.Series");
    }
    */
}
