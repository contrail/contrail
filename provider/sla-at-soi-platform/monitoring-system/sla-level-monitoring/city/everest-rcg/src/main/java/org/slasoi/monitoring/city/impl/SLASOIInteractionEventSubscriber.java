/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.impl;

/**
 * @author SLA@SOI (City)
 * 
 * @date 29 July 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;

/**
 * This class is used to subscribe to the interaction event bus.
 */
public class SLASOIInteractionEventSubscriber {

    PubSubManager pubSubManager = null;
    //private String channelName = "A4MonTest";
    String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
    private String slasoiInteractionEventBusProperties = slasoiOrcHome+ "/monitoring-system/sla-level-monitoring/city/slasoiInteractionEventBus.properties";

    private void instantiatePubSubManager() {
        try {
            try {
                pubSubManager = PubSubFactory.createPubSubManager(slasoiInteractionEventBusProperties);
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }                    
//            catch (FileNotFoundException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
//        catch (MessagingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
    }

    /**
     * Subscribes to the channel where interaction events are exchanged.
     */
    public void subscribe(String chName, MessageListener listener) {
        try {
            if (pubSubManager == null) {
                instantiatePubSubManager();
                pubSubManager.subscribe(chName);
                /*
                 * Add the listener to handle the interaction message arriving on the channel
                 */
                pubSubManager.addMessageListener(listener);
            }

        }
        catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
