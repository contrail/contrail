/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.translator;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.utils.ASTNode;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.bse.FormulaWriter;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * This class is used to translate SLAs to EVEREST operable monitoring specifications.
 */
public class Translator {

    // initialise logging
    Logger logger = Logger.getLogger(Parser.class.getName());

    // the given SLA containing the expression assigned to EVEREST
    private SLA inputSLA;

    // SLA object parser
    private Parser parser;

    // AST translator
    private ASTTranslator astTranslator;

    // parametric templates instantiator
    private Instantiator instantiator;

    // agreement terms theories
    private LinkedHashMap<String, LinkedHashMap<String, Formula>> agreementTermsTheories;

    // Manager for inserting, selecting, deleting or updating
    // QoSTermParametricTemplates
    private AgreementTermEntityManager agreementTermStorer;

    // EVEREST outer layer propagated through out EVEREST RCG for supporting periodic events
    private NewDataAnalyzerEC ndaec;

    /**
     * Constructor using inputSLA.
     * 
     * @param inputSLA
     * @param ndaec
     */
    public Translator(SLA inputSLA, NewDataAnalyzerEC ndaec) {
        this.inputSLA = inputSLA;
        this.agreementTermsTheories = new LinkedHashMap<String, LinkedHashMap<String, Formula>>();
        agreementTermStorer =
                (AgreementTermEntityManager) RCGProperties.getProperty(RCGConstants.AGREEMENT_TERM_STORER_KEY);
        this.ndaec = ndaec;

    }

    /**
     * Translates an agreement term object to EVEREST monitoring specifications.
     * 
     * @param aT -
     *            the input agreement term
     * @param aTHashCode
     * @return
     */
    public LinkedHashMap<String, Formula> translate(AgreementTerm aT, String aTHashCode) {
        // initialise OperationalMonitoringTheory
        LinkedHashMap<String, Formula> operationalMonitoringTheory = new LinkedHashMap<String, Formula>();

        // initialise RCG translator parser
        this.parser = new Parser(this.inputSLA);

        logger.debug("assigned SLA expression: " + aT.getId());

        // parse assigned SLA expression to AST
        ASTNode ast = this.parser.parse(((Object) aT), null);

        logger.debug("output ast no of nodes: " + ast.countSubNodes(0));

        logger.debug("output ast: " + ast.preOrderPrint(ast, ""));

        // generate unique case ids for each qos term included in given SLA
        // expression
        this.parser.generateQoSTermsCases();
        LinkedHashMap<ASTNode, String> qosTermsCases = this.parser.getQosTermsCases();

        // retrieve qos terms paths that is necessary for AST translator
        LinkedHashMap<ASTNode, Object[]> qosTermsPaths = this.parser.getQosTermsPaths();

        // retrieve operators signatures that are necessary for Instantiator
        LinkedHashMap<String, ArrayList<ASTNode>> nodeOperatorsSignatures = this.parser.getNodeOperatorsSignatures();

        // initialise AST translator
        this.astTranslator = new ASTTranslator(qosTermsPaths, qosTermsCases, this.inputSLA, this.ndaec);

        // get parametric templates for the output ast
        this.astTranslator.retrieveParametricTemplates(ast, ast);
        LinkedHashMap<String, String> parametricTemplates = this.astTranslator.getParametricTemplates();

        //logger.debug("ast parametricTemplates: " + parametricTemplates);

        if (parametricTemplates.size() > 0) {

            // initialise instantiator
            this.instantiator =
                    new Instantiator(this.inputSLA, parametricTemplates, qosTermsCases, qosTermsPaths,
                            nodeOperatorsSignatures, aTHashCode);

            // instantiate the parametric templates
            this.instantiator.generateInstantiatedFormulasFromParametricTemplates();

            // get formulas
            LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas =
                    this.instantiator.getQosTermsFormulas();
            logger.debug("Operators & Formulas: " + qosTermsFormulas.toString());

            // for each guarantee term there would be a monitoring formula, so split the guarantee terms
            ArrayList<ASTNode> resolvedASTs = new ArrayList<ASTNode>();
            resolvedASTs.add(ast);
            ArrayList<ASTNode> gTs = parser.splitGuaranteedStates(resolvedASTs);
            
            // resolve ast or nodes
            resolvedASTs.clear();            
            for(ASTNode gsT : gTs){
                ArrayList<ASTNode> inputAST = new ArrayList<ASTNode>();
                inputAST.add(gsT);
                resolvedASTs.addAll(parser.resolveOrIncrementally(inputAST, inputAST, inputAST));            	
            }                   

            //resolvedASTs = parser.splitGuaranteedStates(resolvedASTs);
            
            // compile monitoring rules from resolvedASTS
            for (ASTNode resolvedAST : resolvedASTs) {

                logger.debug("====== Monitoring rule info ======");
                this.astTranslator.compileMonitoringRule(resolvedAST, resolvedAST, qosTermsFormulas);

                logger.debug("Monitoring rule body: " + astTranslator.getMonitoringRuleBody().toString());

                logger.debug("Monitoring rule head: " + astTranslator.getMonitoringRuleHead().toString());

                //logger.debug("Monitoring rule: " + astTranslator.getMonitoringRule().toString());

                logger.debug("Update monitoring rules map");
                astTranslator.updateMonitoringRules(resolvedAST);

                logger.debug("Clear astTranslator fields to proceed");
                astTranslator.clearMonitoringrRuleBody();
                astTranslator.clearMonitoringrRuleHead();
                astTranslator.clearQosTermsHoldsAtPredicatesInBody();
                astTranslator.clearMonitoringRulePeriodicEvents();

            }

            // compile operationalMonitoringTheory
            operationalMonitoringTheory =
                    astTranslator.compileOperationalMonitoringTheory(aTHashCode, qosTermsFormulas);

            //logger.debug("operationalMonitoringTheory: " + operationalMonitoringTheory);
        }

        //operationalMonitoringTheory.putAll(astTranslator.generateNonParametricTemplateFormulas());
        
        FormulaWriter writer = new FormulaWriter();                
        logger.debug("operationalMonitoringTheory: \n\n" + writer.generateXML(operationalMonitoringTheory));                
        
        return operationalMonitoringTheory;
    }

    /**
     * Translates SLA to EVEREST operable monitoring specifications.
     */
    /*
    public void translate(ArrayList<String> sensorIds) {

        // visit each agreement term of the input sla
        for (int a = 0; a < this.inputSLA.getAgreementTerms().length; a++) {
            AgreementTerm aT = this.inputSLA.getAgreementTerms()[a];

            // get the hash code for the given agreement term
            String aTHashCode = this.generateAgreementTermHashCode(aT);

            // translate the agreement term
            LinkedHashMap<String, Formula> aTTheory = this.translate(aT, aTHashCode);
            for(String sensorId : sensorIds){
            	String sensorHashCode = "" + sensorId.hashCode();
            	LinkedHashMap<String, Formula> aTTheory1 = replacePartnerIds(aTTheory, sensorId, sensorHashCode);
            	
                // store the agreement term theory to local collection
                this.agreementTermsTheories.put(aTHashCode + ":" + sensorHashCode, aTTheory1);

                // store the agreement term info and theory to db
                this.agreementTermStorer.addAgreementTerm(this.inputSLA.getUuid().toString(), aT.getId().toString(),
                        aTHashCode + ":" + sensorHashCode, aTTheory1);            	
            }


        }
    }
    */

    /**
     * Translates SLA to EVEREST operable monitoring specifications.
     */
    public void translate(LinkedHashMap<String, String> sensorMap) {

    	/*
        // visit each agreement term of the input sla
        for (int a = 0; a < this.inputSLA.getAgreementTerms().length; a++) {
            AgreementTerm aT = this.inputSLA.getAgreementTerms()[a];

            // get the hash code for the given agreement term
            String aTHashCode = this.generateAgreementTermHashCode(aT);

            // translate the agreement term
            LinkedHashMap<String, Formula> aTTheory = this.translate(aT, aTHashCode);
            for(String sensorId : sensorIds){
            	String sensorHashCode = "" + sensorId.hashCode();
            	LinkedHashMap<String, Formula> aTTheory1 = replacePartnerIds(aTTheory, sensorId, sensorHashCode);
            	
                // store the agreement term theory to local collection
                this.agreementTermsTheories.put(aTHashCode + ":" + sensorHashCode, aTTheory1);

                // store the agreement term info and theory to db
                this.agreementTermStorer.addAgreementTerm(this.inputSLA.getUuid().toString(), aT.getId().toString(),
                        aTHashCode + ":" + sensorHashCode, aTTheory1);            	
            }


        }
        */
    }    
    /**
     * Translates SLA to EVEREST operable monitoring specifications.
     */
    public void translate() {

        // visit each agreement term of the input sla
        for (int a = 0; a < this.inputSLA.getAgreementTerms().length; a++) {
            AgreementTerm aT = this.inputSLA.getAgreementTerms()[a];

            // get the hash code for the given agreement term
            String aTHashCode = this.generateAgreementTermHashCode(aT);

            // translate the agreement term
            LinkedHashMap<String, Formula> aTTheory = this.translate(aT, aTHashCode);

            // store the agreement term theory to local collection
            this.agreementTermsTheories.put(aTHashCode, aTTheory);

            // store the agreement term info and theory to db
            this.agreementTermStorer.addAgreementTerm(this.inputSLA.getUuid().toString(), aT.getId().toString(),
                    aTHashCode, aTTheory);

        }
    }
    /**
     * Produces an agreement Term hash code.
     * 
     * @param aT -
     *            the input agreement term
     * @return
     */
    public String generateAgreementTermHashCode(AgreementTerm aT) {
        String aTHashCode = null;

        int slaUUIDHash = this.inputSLA.getUuid().toString().hashCode();
        int aTHash = aT.hashCode();

        aTHashCode = slaUUIDHash + ":" + aTHash;

        return aTHashCode;
    }
    
    
    /*
    private LinkedHashMap<String, Formula> replacePartnerIds(LinkedHashMap<String, Formula> aTTheory, String sensorId, String sensorHashCode){
    	LinkedHashMap<String, Formula> returnFormulas = new LinkedHashMap<String, Formula>();
    	
    	Iterator<Formula> formulas = aTTheory.values().iterator();
    	while(formulas.hasNext()){
    		Formula formula = formulas.next();
    		ArrayList<Predicate> predicates = formula.getBody();
    		predicates.addAll(formula.getHead());
    		for(Predicate pred : predicates)
    			if(pred.getEcName().equals(Constants.PRED_HAPPENS)) pred.setPartnerId(sensorId);
    		returnFormulas.put(formula.getFormulaId() + ":" + sensorHashCode, formula);
    	}
    	return returnFormulas;    	
    }
    */

}
