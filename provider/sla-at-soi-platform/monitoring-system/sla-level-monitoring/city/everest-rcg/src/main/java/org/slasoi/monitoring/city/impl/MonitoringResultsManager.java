/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.impl;

/**
 * @author SLA@SOI (City)
 * 
 * @date 29 July 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.database.slasoiMonitoringResultEvent.SlasoiMonitoringResultEventEntityManager;
import org.slasoi.monitoring.city.utils.RCGConstants;

import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.monitor.Template;

/**
 * This class acts as a manager of the monitoring results upon their generation by EVEREST. It is responsible for
 * retrieving the results from EVEREST db, translating them to SLA@SOI monitoring result events and forward them to
 * SLA@SOI bus.
 */
public class MonitoringResultsManager extends TimerTask {

    // initialise logging
    private static final Logger logger = Logger.getLogger(MonitoringResultsManager.class.getName());

    // time stamps of the latest retrieved monitoring results per agreement term
    private LinkedHashMap<String, Long> agreementTermsLatestMonResultTimestamps;

    // rcg db manager for agreement term table
    private AgreementTermEntityManager agreementTermManager;

    // the given sla id
    //private String slaId;

    private HashMap<String, String> formulaIdSLAIdMap = new HashMap<String, String>();
    

	// last report timestamp
    long lastReportTimestamp = -1;

    // agreement term hash codes list
    private HashMap<String, String> agreementTermHashCodesAndIdsList = new HashMap<String, String>();

    // monitoring results generator
    private MonitoringResultGenerator monResultGenerator;

    // rcg db manager for slasoi_mon_result_event table
    private SlasoiMonitoringResultEventEntityManager slasoiMonResultEventManager;

    // EVEREST instance
    private NewDataAnalyzerEC ndaec;

    //Result forwarder
    private MonitoringResultEventForwarder vf;
    /**
     * Constructor setting values to fields.
     * 
     * @param slaId -
     *            the input sla
     * @param ndaec -
     *            the EVEREST instance
     */
    public MonitoringResultsManager(String slaId, NewDataAnalyzerEC ndaec) {
        //this.slaId = slaId;
        this.agreementTermManager =
                (AgreementTermEntityManager) RCGProperties.getProperty(RCGConstants.AGREEMENT_TERM_STORER_KEY);
        agreementTermsLatestMonResultTimestamps = new LinkedHashMap<String, Long>();
        this.monResultGenerator = new MonitoringResultGenerator();
        this.slasoiMonResultEventManager =
                (SlasoiMonitoringResultEventEntityManager) RCGProperties
                        .getProperty(RCGConstants.SLASOI_MONITORING_RESULT_EVENT_STORER_KEY);
        this.ndaec = ndaec;
        
        vf = new MonitoringResultEventForwarder(); 
                      
    }

    public void addSLAId(String slaId){
    	this.agreementTermHashCodesAndIdsList.putAll(agreementTermManager.getAgreementTermsHashCodesAndIds(slaId));
    	this.initialiaseAgreementTermsLatestMonResultsTimestamps();
    }
    
    private void initialiaseAgreementTermsLatestMonResultsTimestamps() {

        

        // for each agreement term, set the latest monitoring result times tamp
        // equal to -1
        for (String agreementTermHashCode : agreementTermHashCodesAndIdsList.keySet()) {
            if(!agreementTermsLatestMonResultTimestamps.containsKey(agreementTermHashCode)) this.agreementTermsLatestMonResultTimestamps.put(agreementTermHashCode, -1l);
        }
    }

    /**
     * Periodically checks for new monitoring results of the input sla.
     * 
     * @see java.util.TimerTask#run()
     */
    public void run() {

    	 
        //logger.info("Periodically checking for new monitoring reluts of " + this.slaId);
    	logger.info("Periodically checking for new monitoring reluts");

    	if(agreementTermHashCodesAndIdsList.size() > 0){
            // loop over the agreement Term Hash Codes List
            for (String agreementTermHashCode : agreementTermHashCodesAndIdsList.keySet()) {
                // agreement term id
                String agreementTermId = agreementTermHashCodesAndIdsList.get(agreementTermHashCode);

                // get the latest monitoring result time stamp for current
                // agreement term
                long latestMonResultTimestamp = this.agreementTermsLatestMonResultTimestamps.get(agreementTermHashCode);

                //System.out.println("*&*&*&*& Retrieving resutls after " + latestMonResultTimestamp);
                // get monitoring results from templateDefined serenity db table
                // for current agreement term
                ArrayList<Template> resultTemplates =
                        this.ndaec.getMonitoringResults(Constants.STATUS_IRB, latestMonResultTimestamp,
                                agreementTermHashCode);
                resultTemplates.addAll(this.ndaec.getMonitoringResults(Constants.STATUS_SAT, latestMonResultTimestamp,
                        agreementTermHashCode));
                // sort violation templates according to their decision time
                resultTemplates = this.bubbleSortViolationTemplates(resultTemplates);
                
                //logger.info("5555 Size of violation templates  " + resultTemplates.size());
                
                if (resultTemplates.size() > 0) {

                    // update the latest violation time stamp for the current
                    // agreement term
                    this.agreementTermsLatestMonResultTimestamps.put(agreementTermHashCode, resultTemplates.get(
                            resultTemplates.size() - 1).getDET());

                    // loop over sorted violation templates to generate
                    // sla soi monitoring events
                    for (Template violationTemplate : resultTemplates) {
                    	String slaId = "SLA_ID_NOT_FOUND";
                    	if(formulaIdSLAIdMap.containsKey(violationTemplate.getFormulaId()))
                    		slaId = formulaIdSLAIdMap.get(violationTemplate.getFormulaId());
                        // generate a violation event instance
                        EventInstance violationEventInstance =
                                this.monResultGenerator.generateAgreementTermMonResultEventInstance(slaId, agreementTermId,
                                        violationTemplate);

                        // de-serialise the violationEventInstance
                        MonitoringEventServiceImpl monitoringEventServiceImpl = new MonitoringEventServiceImpl();
                        String violationEventInstanceString = null;
                        try {
                            violationEventInstanceString = monitoringEventServiceImpl.marshall(violationEventInstance);
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        // forward the deserialised violation event instance string
                        
                        violationEventInstanceString = violationEventInstanceString.replace("eventInstance", "Event");
                        
                        vf.forward(violationEventInstanceString);

                        // store monitoring results to db
                        String slasoiMonResultId = ((Long) violationEventInstance.getEventID().getID()).toString();
                        long timestamp = violationEventInstance.getEventContext().getTime().getTimestamp();
                        String everestMonResultId = violationTemplate.getTemplateId();
                        logger.info("slaId  " + slaId + " monresultId " + slasoiMonResultId);
                        this.slasoiMonResultEventManager.addMonResultEvent(slaId, slasoiMonResultId, timestamp,
                                violationEventInstanceString, everestMonResultId);
                    }
                }
            }    		
    	}

    }

    private ArrayList<Template> bubbleSortViolationTemplates(ArrayList<Template> violationTemplates) {
        Template violationTemplatesArray[] = violationTemplates.toArray(new Template[0]);

        for (int i = 0; i < violationTemplatesArray.length - 1; i++) {
            for (int j = i + 1; j < violationTemplatesArray.length; j++) {
                // if true swap
                if (violationTemplatesArray[i].getDET() > violationTemplatesArray[j].getDET()) {
                    Template tmp = violationTemplatesArray[i];
                    violationTemplatesArray[i] = violationTemplatesArray[j];
                    violationTemplatesArray[j] = tmp;
                }
            }
        }

        ArrayList<Template> result = new ArrayList<Template>();

        for (int i = 0; i < violationTemplatesArray.length; i++) {
            result.add(violationTemplatesArray[i]);
        }

        return result;
    }
    
    public HashMap<String, String> getFormulaIdSLAIdMap() {
		return formulaIdSLAIdMap;
	}

	public void addFormulaIdSLAIdMap(HashMap<String, String> sLAIdFormulaIdMap) {
		this.formulaIdSLAIdMap.putAll(sLAIdFormulaIdMap);
	}    
}
