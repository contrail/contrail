/**
 * 
 */
package uk.ac.city.soi.everestplus.core;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 29, 2010
 */
public class QoS {
	private String predictionPolicyId;
	private String qosId;
	private double value;
	private long timestamp;
	private long startTime;
	private long stopTime;
	
	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @param value
	 * @param timestamp
	 * @param startTime
	 * @param stopTime
	 */
	public QoS(String specificationId, String qosId, double value, long timestamp) {
		this.predictionPolicyId = specificationId;
		this.qosId = qosId;
		this.value = value;
		this.timestamp = timestamp;
		this.startTime = 0;
		this.stopTime = 0;
	}

	/**
	 * @return the predictionPolicyId
	 */
	public String getPredictionPolicyId() {
		return predictionPolicyId;
	}

	/**
	 * @param predictionPolicyId the predictionPolicyId to set
	 */
	public void setPredictionPolicyId(String predictionPolicyId) {
		this.predictionPolicyId = predictionPolicyId;
	}

	/**
	 * @return the qosId
	 */
	public String getQosId() {
		return qosId;
	}

	/**
	 * @param qosId the qosId to set
	 */
	public void setQosId(String qosId) {
		this.qosId = qosId;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the stopTime
	 */
	public long getStopTime() {
		return stopTime;
	}

	/**
	 * @param stopTime the stopTime to set
	 */
	public void setStopTime(long stopTime) {
		this.stopTime = stopTime;
	}
}
