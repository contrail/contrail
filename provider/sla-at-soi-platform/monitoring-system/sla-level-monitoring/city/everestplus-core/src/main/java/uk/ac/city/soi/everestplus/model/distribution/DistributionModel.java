/**
 * 
 */
package uk.ac.city.soi.everestplus.model.distribution;

import uk.ac.city.soi.everestplus.core.Model;

/**
 * @author Davide Lorenzoli
 * 
 * @date 25 Feb 2010
 */
public abstract class DistributionModel implements Model {
	private double parameters[];
	private String distributionId;
	
	public DistributionModel(String distributionId, double parameters[]) {
		this.distributionId = distributionId;
		this.parameters = parameters;
	}
	
	/* ------------------------------------------------------------------------
	 * 								PUBLIC METHODS
	 * --------------------------------------------------------------------- */
	
	/**
	 * @return the parameters
	 */
	public double[] getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(double[] parameters) {
		this.parameters = parameters;
	}

	/**
	 * @param distributionId the distributionId to set
	 */
	public void setDistributionId(String distributionId) {
		this.distributionId = distributionId;
	}

	/**
	 * @return the distributionId
	 */
	public String getDistributionId() {
		return distributionId;
	}
	
	/* ------------------------------------------------------------------------
	 * 							PUBLIC ABSTRACT METHODS
	 * --------------------------------------------------------------------- */

	/**
	 * In probability theory and statistics, a probability distribution
	 * identifies either the probability of each value of a random variable
	 * (when the variable is discrete), or the probability of the value falling
	 * within a particular interval (when the variable is continuous)
	 * 
	 * @param x
	 * 
	 * @return the probability distribution value
	 */
	public abstract double getPDF(double x);

	/**
	 * In probability theory and statistics, the cumulative distribution function
	 * (CDF), or just distribution function, describes the probability that a
	 * real-valued random variable X with a given probability distribution will be
	 * found at a value less than or equal to x
	 * 
	 * @param x
	 * 
	 * @return the cumulative distribution function result
	 */
	public abstract double getCDF(double x);
		
	@Override
	public String toString() {
		// construct the parameter string
		String parametersString = new String();
		for (int i=0; i<parameters.length; i++) {
			parametersString += parameters[i];
			
			// if the current parameter isn't the last one add a comma
			if (i<(parameters.length-1)) {
				parametersString += ", ";
			}
		}
		
		return distributionId + "(" + parametersString + ")";
	}
}
