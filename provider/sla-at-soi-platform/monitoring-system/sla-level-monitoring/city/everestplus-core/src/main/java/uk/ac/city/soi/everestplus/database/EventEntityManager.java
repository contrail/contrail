/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Signature;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.Range;

/**
 * @author Davide Lorenzoli
 * 
 * @date 3 Feb 2010
 */
public class EventEntityManager extends EntityManagerCommons implements EntityManagerInterface<LogEvent> {
	private static final String DATABASE_TABLE_NAME = "event";
	
	/**
	 * @param connection
	 */
	public EventEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public EventEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param sender
	 * @param receiver
	 * @param service
	 * @param operation
	 * @param timestamp
	 * @param type it can be "request", "response", or "dropped"
	 * @return
	 */
	public int insert(String sender, String receiver, String service, String operation, long timestamp, String type) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;

		try {			
			String query = "INSERT INTO " + getDatabaseTable() + " " +
            			   "SET sender=?, receiver=?, service=?, operation=?, timestamp=?, type=?";
			
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, sender);
		    pstmt.setString(2, receiver);
		    pstmt.setString(3, service);
		    pstmt.setString(4, operation);
		    pstmt.setLong(5, timestamp);
		    pstmt.setString(6, type);
        
            insertedRecords = pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}
	
	/**
	 * @param startTime
	 * @param endTime
	 * @param eventType from the class <code>EventType</code>
	 * @return
	 */
	public int countEventWithinTimeRange(long startTime, long endTime, String eventType) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int events = 0;
		
		try {
			String query = "SELECT COUNT(timestamp) AS events FROM " + getDatabaseTable() + " " +
						   "WHERE timestamp > ? AND timestamp < ? AND type = ?";
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setLong(1, startTime);
			pstmt.setLong(2, endTime);
			pstmt.setString(3, eventType);
			
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.first()) {
				events = rs.getInt("events");
			}
		} 
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return events;
	}
	
	/**
	 * @return The timestamp of the first event inserted, 0 if no event was found
	 */
	public long getFirstEventTimestamp() {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		long timestamp = 0;
		
		try {
			String query = "SELECT timestamp FROM " + getDatabaseTable() + " ORDER BY timestamp ASC LIMIT 1";
			pstmt = getConnection().prepareStatement(query);
			
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.first()) {
				timestamp = rs.getLong("timestamp");
			}
		} 
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return timestamp;
	}
	
	/**
	 * @return The timestamp of the last event inserted, 0 if no event was found
	 */
	public long getLastEventTimestamp() {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		long timestamp = 0;
		
		try {
			String query = "SELECT timestamp FROM " + getDatabaseTable() + " ORDER BY timestamp DESC LIMIT 1";
			pstmt = getConnection().prepareStatement(query);
			
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.first()) {
				timestamp = rs.getLong("timestamp");
			}
		} 
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return timestamp;
	}

	
	/**
	 * @see everest.database.EntityManagerInterface#selectAll()
	 */
	
	public ArrayList<LogEvent> selectAll() {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
		    pstmt = getConnection().prepareStatement("SELECT eventObject FROM " + getDatabaseTable());
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}
	
	@Deprecated
	public void add(LogEvent event) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		
		try {
			// delete the existing graph
			/*
			if (get(event.getID()) != null) {
				delete(event.getID());
			}
			*/
		    
			Blob blob = toBlob(event);
			
		    pstmt = getConnection().prepareStatement("INSERT INTO " + getDatabaseTable() + " " +
			                                    "(eventId, timestamp, ecName, prefix, operationName, partnerId, negated, abducible, recordable, eventObject, eventString) " +
												"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		    pstmt.setString(1, event.getID());
            pstmt.setLong(2, event.getTimestamp());
            pstmt.setString(3, event.getEcName());
            pstmt.setString(4, event.getPrefix());
            pstmt.setString(5, event.getOperationName());
            pstmt.setString(6, event.getPartnerId());
            pstmt.setBoolean(7, event.isNegated());
            pstmt.setBoolean(8, event.isAbducible());
            pstmt.setBoolean(9, event.isRecordable());
            pstmt.setBlob(10, blob);
            pstmt.setString(11, event.toString());

            pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}	
	}


	/**
	 * @see everest.database.event.EventEntityManagerInterface#get(java.lang.String)
	 */
	
	public LogEvent get(String eventId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		LogEvent event = null;

		try {
		    pstmt = getConnection().prepareStatement("SELECT eventObject " +
					    					    "FROM " + getDatabaseTable() + " "+
					    					    "WHERE eventId = ?");
		    pstmt.setString(1, eventId);
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	Blob blob = resultSet.getBlob("eventObject");		    	
			    event = (LogEvent) toObject(blob);				
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}		
		
		return event;
	}

	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvent(int, int)
	 * 
	 * @param offset the offset of the first row to return. The offset of the
	 * initial row is 0 (not 1).
	 * @param numberRows the maximum number of rows to return.
	 * @return all row between from offset to numberRows. For instance, if 
	 * offset=5 and numberRows=10 it returns rows [6-15]
	 */
	public ArrayList<LogEvent> getEvent(int offset, int numberRows) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
			String query = "SELECT eventObject FROM " + getDatabaseTable() + " " +
						   "LIMIT ?, ?";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setInt(1, offset);
			pstmt.setInt(2, numberRows);

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}

	
	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(everest.core.Signature)
	 */
	
	public ArrayList<LogEvent> getEvents(Signature eventSignature) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
			String query = "SELECT eventObject FROM " + getDatabaseTable() + " " +
						   "WHERE ecName = ? AND prefix = ? AND operationName = ? AND partnerId = ?";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setString(1, eventSignature.getEcName());
			pstmt.setString(2, eventSignature.getPrefix());
			pstmt.setString(3, eventSignature.getOperationName());
			pstmt.setString(4, eventSignature.getPartnerId());

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}


	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvent(everest.core.Signature, long)
	 */
	
	public ArrayList<LogEvent> getEvent(Signature eventSignature, long timestamp) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
			String query = "SELECT eventObject FROM " + getDatabaseTable() + " " + 
						   "WHERE ecName = ? AND prefix = ? AND operationName = ? AND partnerId = ? AND " +
						   "timestamp = ?";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setString(1, eventSignature.getEcName());
			pstmt.setString(2, eventSignature.getPrefix());
			pstmt.setString(3, eventSignature.getOperationName());
			pstmt.setString(4, eventSignature.getPartnerId());
			pstmt.setLong(5, timestamp);			

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}
	
	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(everest.core.Signature, everest.monitor.Range)
	 */
	
	public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
			String query = "SELECT eventObject FROM " + getDatabaseTable() + " " +
						   "WHERE ecName = ? AND prefix = ? AND operationName = ? AND partnerId = ? AND " +
						   "timestamp >= ? AND timestamp <= ?";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setString(1, eventSignature.getEcName());
			pstmt.setString(2, eventSignature.getPrefix());
			pstmt.setString(3, eventSignature.getOperationName());
			pstmt.setString(4, eventSignature.getPartnerId());
			pstmt.setLong(5, timeRange.getLowerBound());
			pstmt.setLong(6, timeRange.getUpperBound());			

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}

	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(everest.core.Signature, everest.monitor.Range, java.lang.String)
	 */
	
	public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange, String eventSourceId) {
		return new ArrayList<LogEvent>();
	}

	
	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(everest.monitor.Range)
	 */
	
	public ArrayList<LogEvent> getEvents(Range timeRange) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();		
			
		try {
			String query = "SELECT eventObject FROM " + getDatabaseTable() + " " +
						   "WHERE timestamp >= ? AND timestamp <= ?";						   
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setLong(1, timeRange.getLowerBound());
			pstmt.setLong(2, timeRange.getUpperBound());			

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}


	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(java.lang.String)
	 */
	
	public ArrayList<LogEvent> getEvents(String eventSourceId) {
		return new ArrayList<LogEvent>();
	}


	/**
	 * @see everest.database.event.EventEntityManagerInterface#getEvents(java.lang.String, everest.monitor.Range)
	 */
	
	public ArrayList<LogEvent> getEvents(String eventSourceId, Range timeRange) {
		return new ArrayList<LogEvent>();
	}

	/**
	 * @see everest.database.event.EventEntityManagerInterface#getCaptorTimestamp(java.lang.String)
	 */
	
	public long getCaptorTimestamp(String partnerId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		long timestamp = Constants.TIME_UD;		
			
		try {
			String query = "SELECT timestamp " +
						   "FROM " + getDatabaseTable() + " WHERE partnerId = ? " +
						   "ORDER BY timestamp DESC LIMIT 0,1;";						   
			   
		    pstmt = getConnection().prepareStatement(query);			
		    pstmt.setString(1, partnerId);
		    
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	timestamp = resultSet.getLong(1);
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return timestamp;
	}
	
	/**
	 * @param operationName The current implementation 
	 * @param timestamp
	 * @return An <code>LogEvent</code> object, <code>null</code> if nothing
	 * was found.
	 */
	public LogEvent selectLastEventByOperationName(String operationName, long timestamp) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		LogEvent result = null;
		
		try {
			String query = "SELECT * FROM " +  getDatabaseTable() + " " +
			   "WHERE operationName = ? AND timestamp < ? " +
			   "ORDER BY timestamp DESC " +
			   "LIMIT 1";
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, operationName);
			pstmt.setLong(2, timestamp);
			
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	// add the operation name		
				result = (LogEvent) toObject(resultSet.getBlob("eventObject"));
		    }
		}
		catch (SQLException ex){
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return result;

	}
	
	/**
	 * @param query
	 * @return
	 */
	public ArrayList<Long> queryForTimestamps(String query) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<Long> result = new ArrayList<Long>();
		
		try {
			pstmt = getConnection().prepareStatement(query);
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
				    // add the operation name		
				    result.add(resultSet.getLong("timestamp"));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return result;
	}
	
	/**
	 * @param query
	 * @return
	 */
	public ArrayList<String> queryForOperationNames(String query) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<String> result = new ArrayList<String>();
		
		try {
			pstmt = getConnection().prepareStatement(query);
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
				    // add the operation name		
				    result.add(resultSet.getString("operationName"));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return result;
	}
	
	/**
	 * @param query
	 * @return
	 */
	public ArrayList<LogEvent> queryForEvents(String query) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<LogEvent> events = new ArrayList<LogEvent>();
		
		try {
			pstmt = getConnection().prepareStatement(query);
		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    Blob blob = resultSet.getBlob("eventObject");
				    // add the event		
		    		events.add((LogEvent) toObject(blob));
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return events;
	}
	
	/**
	 * 
	 */
	public void deleteAll() {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		
		try {
			pstmt = getConnection().prepareStatement("TRUNCATE TABLE " + getDatabaseTable());
			
			pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(LogEvent entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	public ArrayList<LogEvent> executeQuery(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(LogEvent entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<LogEvent> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public LogEvent selectByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(LogEvent entity) {
		throw new UnsupportedOperationException("TO DO");
	}
}
