/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everestplus.core.Statistics;

/**
 * @author Davide Lorenzoli
 * 
 * @date Aug 18, 2010
 */
public class StatisticsEntity extends Statistics implements PersistentEntity {
	private long entityId;
		
	/**
	 * Default constructor
	 */
	public StatisticsEntity(Statistics statistics) {
		super();
		super.setExecutionId(statistics.getExecutionId());
		super.setK(statistics.getK());
		super.setkType(statistics.getkType());
		super.setModelGoodness(statistics.getModelGoodness());
		super.setModelHistorySize(statistics.getModelHistorySize());
		super.setMttrc(statistics.getMttrc());
		super.setMttrf(statistics.getMttrf());
		super.setMttry(statistics.getMttry());
		super.setPrediction(statistics.getPrediction());
		super.setPredictionTimestamp(statistics.getPredictionTimestamp());
		super.setPredictionWindow(statistics.getPredictionWindow());
		super.setPrMttrc(statistics.getMttrc());
		super.setPrMttry(statistics.getMttry());
		super.setPrY(statistics.getPrY());
		super.setStatisticsId(statistics.getStatisticsId());
		super.setY(statistics.getY());
		super.setPredictionCorrect(statistics.isPredictionCorrect());
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return the entityId
	 */
	public long getEntityId() {
		return entityId;
	}
	
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
