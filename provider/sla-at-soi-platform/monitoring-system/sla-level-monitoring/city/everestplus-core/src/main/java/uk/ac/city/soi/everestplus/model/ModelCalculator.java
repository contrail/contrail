package uk.ac.city.soi.everestplus.model;

import java.util.ArrayList;

import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slaatsoi.prediction.schema.VariableType;

import uk.ac.city.soi.everestplus.core.Model;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 22, 2011
 */
public abstract class ModelCalculator {
	protected String modelCalculatorId;
	
	/**
	 * @param connection
	 */
	public ModelCalculator(String modelCalculatorId) {
		this.modelCalculatorId = modelCalculatorId;
	}

	/* ------------------------------------------------------------------------
	 * 								PUBLIC METHODS
	 * --------------------------------------------------------------------- */

	/**
	 * @return the modelCalculatorId
	 */
	public String getModelCalculatorId() {
		return modelCalculatorId;
	}

	/**
	 * @param modelCalculatorId the modelCalculatorId to set
	 */
	public void setModelCalculatorId(String modelCalculatorId) {
		this.modelCalculatorId = modelCalculatorId;
	}

	/* ------------------------------------------------------------------------
	 * 								ABSTRACT METHODS
	 * --------------------------------------------------------------------- */
	
	/**
	 * @param predictionPolicy
	 */
	public abstract void updateModels(PredictionPolicyType predictionPolicy);
	
	/**
	 * @param predictionSpecificationId
	 * @param qosId
	 * @param samples
	 */
	public abstract void updateModels(PredictionPolicyType predictionPolicy, VariableType variable, double[] samples);

	/**
	 * @param predictionPolicy
	 * @param qosId
	 * @return
	 */
	public abstract Model getBestFitModel(PredictionPolicyType predictionPolicy);
	
	/**
	 * @param predictionSpecificationId
	 * @param qosId
	 * @return
	 */
	public abstract Model getBestFitModel(String predictionPolicyId);
	
	/**
	 * @param predictionSpecificationId
	 * @param qosId
	 * @param samples
	 * @return
	 */
	public abstract ArrayList<Model> getModels(PredictionPolicyType predictionPolicy, double[] samples);
	
	/**
	 * @param predictionPolicyId
	 * @param samples
	 * @return
	 */
	public abstract ArrayList<Model> getModels(String predictionPolicyId, double[] samples);
}