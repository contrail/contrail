/**
 * 
 */
package uk.ac.city.soi.everestplus.core;

/**
 * This interface must be implemented by all classes that represent an inferred
 * model by the prediction framework. For instance, an inferred statistical
 * distribution is model and its object representation must implement this interface
 * 
 * @author Davide Lorenzoli
 * 
 * @date Aug 9, 2010
 */
public interface Model {

}
