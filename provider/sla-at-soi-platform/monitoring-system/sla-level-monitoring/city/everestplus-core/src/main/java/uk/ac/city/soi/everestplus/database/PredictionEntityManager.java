/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slaatsoi.prediction.schema.PredictionPolicyType;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.everestplus.core.Prediction;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 8, 2010
 */
public class PredictionEntityManager extends EntityManagerCommons implements EntityManagerInterface<PredictionEntity> {
	private static final String DATABASE_TABLE_NAME = "prediction";
	private PredictionPolicyEntityManager predictionPolicyEM;
	
	/**
	 * @param connection
	 */
	public PredictionEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public PredictionEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
		predictionPolicyEM = new PredictionPolicyEntityManager(connection);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param timestamp
	 * @return the Prediction if exists, <code>null</code> otherwise
	 */
	public PredictionEntity selectBySpecificationIdAndTimestamp(String predictionPolicyId, long timestamp) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		PredictionEntity prediction = null;
		
		try {
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "WHERE prediction_policy_id=? AND timestamp<=? " +
						   "ORDER BY timestamp DESC " +
						   "LIMIT 1";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setString(1, predictionPolicyId);
			pstmt.setLong(2, timestamp);

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
			    double value = resultSet.getDouble("value");
			    long time = resultSet.getLong("timestamp");
			   
			    PredictionPolicyType predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicyId);
			    
			    prediction = new PredictionEntity(new Prediction(predictionPolicy, value, time));
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return prediction;
	}
	
	/**
	 * @param offset
	 * @param numberRows
	 * @return
	 */
	public ArrayList<PredictionEntity> selectPrediction(int offset, int numberRows) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<PredictionEntity> predictions = new ArrayList<PredictionEntity>();		
			
		try {
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "LIMIT ?, ?";
						   
		    pstmt = getConnection().prepareStatement(query);
		    
			pstmt.setInt(1, offset);
			pstmt.setInt(2, numberRows);

		    resultSet = pstmt.executeQuery();
		    
		    // if any blob has been returned
		    if (resultSet.first()) {
		    	do {
		    		// get the blob
				    String predictionPolicyId = resultSet.getString("prediction_policy_id");
				    double value = resultSet.getDouble("value");
				    long timestamp = resultSet.getLong("timestamp");
				    
				    PredictionPolicyType predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicyId);
				    
				    PredictionEntity prediction = new PredictionEntity(new Prediction(predictionPolicy, value, timestamp));
				    
				    // add the event		
		    		predictions.add(prediction);
		    	} while (resultSet.next());
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return predictions;
	}

	/**
	 *
	 * @return either (1) the row count for SQL Data Manipulation Language
	 * (DML) statements or (2) 0 for SQL statements that return nothing
	 *
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(PredictionEntity entity) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;

		try {			
			String query = "INSERT INTO " + getDatabaseTable() + " " +
           			   "SET prediction_policy_id=?, value=?, timestamp=?, window=?";
			
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, entity.getPredictionSpecification().getPredictionPolicyId());
		    pstmt.setDouble(2, entity.getValue());
		    pstmt.setLong(3, entity.getTimestamp());
		    pstmt.setLong(4, entity.getPredictionSpecification().getPredictionSettings().getPredictionHorizon());
       
           insertedRecords = pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}
	
	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(PredictionEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery()
	 */
	public ArrayList<PredictionEntity> executeQuery(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	public int executeUpdate(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<PredictionEntity> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public PredictionEntity selectByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(PredictionEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}
}
