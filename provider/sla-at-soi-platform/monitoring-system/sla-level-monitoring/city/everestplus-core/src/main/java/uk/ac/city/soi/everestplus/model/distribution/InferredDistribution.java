/**
 * 
 */
package uk.ac.city.soi.everestplus.model.distribution;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jan 4, 2011
 */
public abstract class InferredDistribution extends DistributionModel {
	private double dataPoints[];
	private double goodnessOfFit;
	
	/**
	 * @param inferredDistribution
	 */
	public InferredDistribution(InferredDistribution inferredDistribution) {
		super(inferredDistribution.getDistributionId(), inferredDistribution.getParameters());
		this.dataPoints = inferredDistribution.getDataPoints();
		this.goodnessOfFit = inferredDistribution.getGoodnessOfFit();
	}
	
	/**
	 * @param distributionId
	 * @param parameters
	 * @param dataPoints
	 * @param goodnessOfFit
	 */
	public InferredDistribution(String distributionId, double[] parameters, double[] dataPoints, double goodnessOfFit) {
		super(distributionId, parameters);
		this.dataPoints = dataPoints;
		this.goodnessOfFit = goodnessOfFit;
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * The goodness of fit of a statistical model describes how well it fits a
	 * set of observations. Measures of goodness of fit typically summarise the
	 * discrepancy between observed values and the values expected under the
	 * model in question
	 *  
	 * @param goodnessOfFit
	 */
	public void setGoodnessOfFit(double goodnessOfFit) {
		this.goodnessOfFit = goodnessOfFit;
	}

	/**
	 * The goodness of fit of a statistical model describes how well it fits a
	 * set of observations. Measures of goodness of fit typically summarise the
	 * discrepancy between observed values and the values expected under the
	 * model in question
	 *  
	 * @return the goodnessOfFit
	 */
	public double getGoodnessOfFit() {
		return goodnessOfFit;
	}
	
	/**
	 * @return the dataPoints
	 */
	public double[] getDataPoints() {
		return dataPoints;
	}
	
	/**
	 * @param dataPoints the dataPoints to set
	 */
	public void setDataPoints(double[] dataPoints) {
		this.dataPoints = dataPoints;
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
