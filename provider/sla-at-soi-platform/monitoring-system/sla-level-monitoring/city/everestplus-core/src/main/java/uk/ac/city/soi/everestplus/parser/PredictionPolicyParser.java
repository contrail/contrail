/**
 * 
 */
package uk.ac.city.soi.everestplus.parser;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import org.apache.log4j.Logger;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slaatsoi.prediction.schema.ThresholdType;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class PredictionPolicyParser {
	// Logger
	private static Logger logger = Logger.getLogger(PredictionPolicyParser.class);
	
	private final static String PREDICTION_POLICY_PROPERTY_NAME = "PredictionPolicy";
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * Parse an SLA object to an array of prediction policy objects
	 * 
	 * @param sla
	 * @param predictionPolicyXmlSchema
	 * @return An array of prediction policy objects
	 * @throws SAXException 
	 */
	public static PredictionPolicyType[] parseSLA(SLA sla, String predictionPolicyXmlSchema) throws SAXException {
		ArrayList<PredictionPolicyType> predictionPolicies = new ArrayList<PredictionPolicyType>();
		
		String slaId = "";
		String agreementTermId = "";
		String guaranteedTermId = "";
		
		// set the slaId value
		slaId = sla.getUuid().getValue();
		
		logger.debug("Parsing SLA id: " + slaId);
		
		// loop over the agreement terms
		for (AgreementTerm agreementTerm : sla.getAgreementTerms()) {
			// set the agreementTermId value
			agreementTermId = agreementTerm.getId().getValue();
		
			logger.debug("Parsing agreement term id: " + agreementTermId);
			
			// loop over the guaranteed terms
			for (Guaranteed guaranteed : agreementTerm.getGuarantees()) {
				
				// filter by Guaranteed.Action
				if (guaranteed instanceof Guaranteed.Action) {
					Guaranteed.Action action = (Guaranteed.Action) guaranteed;
					
					// set the guaranteedTermId value
					guaranteedTermId = action.getId().getValue();
					
					logger.debug("Parsing guaranteed Id: " + guaranteedTermId);
					
					if (action.getPostcondition() instanceof CustomAction) {
					
						// get custom action
						CustomAction customAction = (CustomAction) action.getPostcondition();
						
						// get the business term reporting
						String xmlPredictionPolicy = customAction.getPropertyValue(new STND(PREDICTION_POLICY_PROPERTY_NAME));
						
						logger.debug("Prediction policy found:\n" + xmlPredictionPolicy);
						
						// parse the xml to a BusinessTerm object
						PredictionPolicyType predictionPolicy = parsePredictionPolicy(xmlPredictionPolicy, predictionPolicyXmlSchema);
						
						if (predictionPolicy != null) {
							logger.debug("Prediction policy id: " + predictionPolicy.getPredictionPolicyId());
							logger.debug("History size: " + predictionPolicy.getPredictionSettings().getHistorySize());
							logger.debug("Prediction horizon: " + predictionPolicy.getPredictionSettings().getPredictionHorizon());
							logger.debug("Theresholds:");
							for (ThresholdType thereshold : predictionPolicy.getPredictionSettings().getNotificationThresholds().getNotificationThreshold()) {
								logger.debug("\t" + thereshold.getOperator() + " " + thereshold.getValue());
							}
							
							logger.debug("Prediction target: " + predictionPolicy.getPredictionTarget().getVariable().getQoSId() + " " + predictionPolicy.getPredictionTarget().getComparisonOperator() + " " + predictionPolicy.getPredictionTarget().getValue());
							logger.debug("Predictor: " + predictionPolicy.getPredictor().getPredictorId());
							
							
							// add the business term to the list
							predictionPolicies.add(predictionPolicy);
						}
					}
				}
			}
		}
		
		return predictionPolicies.toArray(new PredictionPolicyType[0]);
	}
	
	
	/**
	 * Parse an XML containing a prediction policy to an instance of
	 * PredictionPolicyType class.
	 * 
	 * @param xmlPredictionPolicy
	 * @return An instance of PredictionPolicy class, <code>null</code> if
	 *         the given XML in invalid, e.g., doesn't comply to the expected schema
	 */
	public static PredictionPolicyType parsePredictionPolicy(String xmlPredictionPolicy) {
		PredictionPolicyType predictionPolicy = null;
		
		try {
			predictionPolicy = parsePredictionPolicy(xmlPredictionPolicy, null);
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		}
		
		return predictionPolicy;
	}
	
	/**
	 * Parse an XML containing a prediction policy to an instance of
	 * PredictionPolicyType class.
	 *  
	 * @param xmlPredictionPolicy
	 * @param xmlPredictionPolicySchema
	 * @return An instance of PredictionPolicy class, <code>null</code> if
	 *         the given XML in invalid, e.g., doesn't comply to the expected schema
	 * @throws SAXException 
	 */
	public static PredictionPolicyType parsePredictionPolicy(String xmlPredictionPolicy, String xmlPredictionPolicySchema) throws SAXException {
		PredictionPolicyType predictionPolicy = null;
		Schema schema = null;
		
		// check for null input
		if (xmlPredictionPolicy == null) return predictionPolicy;
		
		if (xmlPredictionPolicySchema != null) {
			//SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.XML_NS_PREFIX);
			//schema = schemaFactory.newSchema(new StreamSource(new StringReader(xmlBusinessTermSchema)));
		}
		
		//logger.debug("Parsing business term:\n" + xmlBusinessTerm);
		
		ByteArrayInputStream xmlContentBytes = new ByteArrayInputStream(xmlPredictionPolicy.getBytes());
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(PredictionPolicyType.class);
            // create an Unmarshaller
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            //note: setting schema to null will turn validator off
            unmarshaller.setSchema(schema);
            
            predictionPolicy = (PredictionPolicyType) unmarshaller.unmarshal(xmlContentBytes);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
		return predictionPolicy;
	}
	
	/**
	 * @param predictionPolicy
	 * @return
	 * @throws SAXException
	 */
	public static String parsePredictionPolicy(PredictionPolicyType predictionPolicy) throws SAXException {
		StringWriter businessTermXML = new StringWriter();
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(PredictionPolicyType.class);
            // create an marshaller
            Marshaller marshaller = jc.createMarshaller();
            
            marshaller.marshal(predictionPolicy, businessTermXML);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
        return businessTermXML.toString();
	}
}
