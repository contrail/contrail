/**
 * 
 */
package uk.ac.city.soi.everestplus.predictor;

import org.slaatsoi.prediction.schema.PredictionPolicyType;

import uk.ac.city.soi.everestplus.core.Prediction;


/**
 * @author Davide Lorenzoli
 * 
 * @date 25 Feb 2010
 */
public abstract class Predictor {
	private String predictorId;

	/**
	 * @param predictorId
	 * @param predictionSpecification
	 */
	public Predictor(String predictorId) {
		this.predictorId = predictorId;
	}
	
	/* ------------------------------------------------------------------------ 
	 * 							Abstract Methods
	 * ---------------------------------------------------------------------- */
	
	/**
	 * Compute a prediction given a prediction specification and a prediction timestamp
	 * 
	 * @param predictionPolicy
	 * @param timestamp
	 * @return A Prediction object
	 */
	public abstract Prediction getPrediction(PredictionPolicyType predictionPolicy, long timestamp);
	
	/* ------------------------------------------------------------------------ 
	 * 							Public Methods
	 * ---------------------------------------------------------------------- */
	
	/**
	 * @return the predictorId
	 */
	public String getPredictorId() {
		return predictorId;
	}
}
