/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 29, 2010
 */
public class ModelFactoryEntityManager extends EntityManagerCommons implements EntityManagerInterface<ModelFactoryEntity> {
	private static final String DATABASE_TABLE_NAME = "model_factory";
	
	/**
	 * @param connection
	 */
	public ModelFactoryEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public ModelFactoryEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param modelFactotyId
	 * @return
	 */
	public ModelFactoryEntity selectById(String modelFactotyId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ModelFactoryEntity result = null;

		try {
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "WHERE model_factory_id=?";
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, modelFactotyId);
		    		           
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = new ModelFactoryEntity(
						rs.getString("model_factory_id"),
						rs.getString("class_name")
						);
			}
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return result;			
	}
	
	/**
	 * @param modelFactoryId
	 * @param className
	 * @return
	 */
	public int insert(String modelFactoryId, String className) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;
		
		// if it already exists do not add it
		if (selectById(modelFactoryId) != null) {
			return 0;
		}
		
		try {
			
			String query = "INSERT INTO " + getDatabaseTable() + " " +
			   			   "SET model_factory_id=?, class_name=?";
			
			pstmt = getConnection().prepareStatement(query);
				
			pstmt.setString(1, modelFactoryId);
			pstmt.setString(2, className);
			
			insertedRecords = pstmt.executeUpdate();
		}
		catch (SQLException e){
		    // handle any errors
		    e.printStackTrace();
		} 
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}
	
	/**
	 * @param modelFactoryId
	 * @return
	 */
	public int delete(String modelFactoryId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int deletedRecords = 0;

		try {
			String query = "DELETE FROM " + getDatabaseTable() + " " +
			   			   "WHERE model_factory_id = ?";
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, modelFactoryId);
			
			deletedRecords = pstmt.executeUpdate();

		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return deletedRecords;		
	}

	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(ModelFactoryEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	public ArrayList<ModelFactoryEntity> executeQuery(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(ModelFactoryEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<ModelFactoryEntity> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public ModelFactoryEntity selectByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(ModelFactoryEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}
}
