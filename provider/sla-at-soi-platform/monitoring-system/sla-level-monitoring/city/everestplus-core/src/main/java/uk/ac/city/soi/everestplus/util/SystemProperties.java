/**
 * 
 */
package uk.ac.city.soi.everestplus.util;

import java.util.Properties;

/**
 * @author Davide Lorenzoli
 * 
 * @date 1 Apr 2010
 */
public class SystemProperties {
	public static void println() {
		Properties properties = System.getProperties();
		
		for (Object key : properties.keySet()) {
			System.out.println(key + ": " + properties.get(key));
		}
	}
}
