package uk.ac.city.soi.everestplus.util;

import java.util.Random;

public class BubbleSort {

	// Sorts an integer array in ascending order.

	// Parameters:
	// data - reference to the integer array to sort, must not be null

	// Postcondition:
	// The array is sorted in ascending order.

	public static void bubbleSort(int[] data) {
		for (int k = 0; k < data.length - 1; k++) {
			boolean isSorted = true;

			for (int i = 1; i < data.length - k; i++) {
				if (data[i] < data[i - 1]) {
					int tempVariable = data[i];
					data[i] = data[i - 1];
					data[i - 1] = tempVariable;

					isSorted = false;

				}
			}

			if (isSorted)
				break;
		}
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int samples = 150;
		double sizes[] = new double[samples];
		double times[] = new double[samples];
		double gaussian[] = new double[samples];
		
		for (int i = 0; i < samples; i++) {
			
			//gaussian[i] = new Random().nextGaussian()*10000;			
			//sizes[i] = (int) Math.abs(gaussian[i]);
			
			sizes[i] = new Random().nextInt(30000);
			
			int arrayValues[] = new int[(int) sizes[i]];
			
			
			for (int j = 0; j < arrayValues.length; j++) {
				arrayValues[j] = new Random().nextInt(1000);
			}

			long startTime = System.currentTimeMillis();
			bubbleSort(arrayValues);
			long stopTime = System.currentTimeMillis();

			times[i] = stopTime-startTime;								
		}
		
		System.out.println("Data:");
		for (int i = 0; i < samples; i++) {
			System.out.println(sizes[i] + "\t" + times[i]);
		}
		
		System.out.println("Gaussian:");
		for (int i = 0; i < samples; i++) {
			System.out.println(gaussian[i]);
		}
		
		System.out.println("Sizes:");
		for (int i = 0; i < samples; i++) {
			System.out.println(sizes[i]);
		}
		
		System.out.println("Times:");
		for (int i = 0; i < samples; i++) {
			System.out.println(times[i]);
		}
		System.out.println();
		
		System.out.println("Array size / Bubble sort computational time");
		System.out.println("Pearson correlation: " + PearsonCorrelation.getPearsonCorrelation(sizes, times));
		
		sizes = new double[samples];
		times = new double[samples];
		
		for (int i = 0; i < samples; i++) {
			sizes[i] = new Random().nextInt(10);
			times[i] = new Random().nextInt(10000);
		}
		
		System.out.println("Random array size / Random computational time");
		System.out.println("Pearson correlation: " + PearsonCorrelation.getPearsonCorrelation(sizes, times));
	}
}
