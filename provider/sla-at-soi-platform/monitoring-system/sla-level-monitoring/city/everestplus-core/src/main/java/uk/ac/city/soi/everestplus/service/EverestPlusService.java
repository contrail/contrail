/**
 * 
 */
package uk.ac.city.soi.everestplus.service;

import java.util.ArrayList;

import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.everestplus.core.Prediction;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 1, 2011
 */
public interface EverestPlusService {
	
	/**
	 * Add an SLA containing one or more prediction policies. The method invocation
	 * triggers the computation of a prediction result per each prediction policy.
	 * Prediction results are returned to the caller. Also, prediction result events
	 * are published to the communication infrastructure.
	 * 
	 * @param sla The SLA containing one or more prediction policies
	 * @return The prediction results
	 */
	public ArrayList<Prediction> getPrediction(SLA sla);
	
	/**
	 * Add a prediction policy. The method invocation triggers the computation of a
	 * prediction result. The prediction result are returned to the caller. Also,
	 * prediction result event is published to the communication infrastructure. 
	 * 
	 * @param predictionPolicy The prediction policy
	 * @return The prediction result
	 */
	public Prediction getPrediction(PredictionPolicyType predictionPolicy);
}
