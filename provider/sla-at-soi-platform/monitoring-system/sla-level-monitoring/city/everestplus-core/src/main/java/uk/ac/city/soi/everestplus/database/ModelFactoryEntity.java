/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import uk.ac.city.soi.database.PersistentEntity;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 29, 2010
 */
public class ModelFactoryEntity implements PersistentEntity {
	private long entityId;
	
	private String modelFactoryId;
	private String className;
	
	/**
	 * @param modelFactoryId
	 * @param className
	 */
	public ModelFactoryEntity(String modelFactoryId, String className) {
		this.modelFactoryId = modelFactoryId;
		this.className = className;
	}

	/**
	 * @return the entityId
	 */
	public long getEntityId() {
		return entityId;
	}
	
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
	
	/**
	 * @return the modelFactoryId
	 */
	public String getModelFactoryId() {
		return modelFactoryId;
	}

	/**
	 * @param modelFactoryId the modelFactoryId to set
	 */
	public void setModelFactoryId(String modelFactoryId) {
		this.modelFactoryId = modelFactoryId;
	}
	
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
}
