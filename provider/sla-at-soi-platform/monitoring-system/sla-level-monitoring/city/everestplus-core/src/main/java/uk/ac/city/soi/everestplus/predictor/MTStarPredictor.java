/**
 * 
 */
package uk.ac.city.soi.everestplus.predictor;

import org.apache.log4j.Logger;
import org.slaatsoi.prediction.schema.ComparisonOperatorType;
import org.slaatsoi.prediction.schema.PredictionPolicyType;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Prediction;
import uk.ac.city.soi.everestplus.core.Statistics;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntity;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface;
import uk.ac.city.soi.everestplus.database.StatisticsEntity;
import uk.ac.city.soi.everestplus.database.StatisticsEntityManager;
import uk.ac.city.soi.everestplus.model.ModelFactoryBuilder;
import uk.ac.city.soi.everestplus.model.distribution.DistributionFactory;
import uk.ac.city.soi.everestplus.model.distribution.DistributionModel;

/**
 * This class implements the MT* prediction model described in the paper
 * SERP 2011, Runtime Prediction of Software Service Availability.
 * 
 * The prediction model applies to QoS of the type Mean Time "of something"
 * and therefore the use of the wild char start "*". For instance: mean time
 * to repair (MTTR), mean time to failure (MTTF), mean time to completion
 * (MTTC).  
 * 
 * @author Davide Lorenzoli
 * 
 * @date Jun 22, 2011
 */
public class MTStarPredictor extends Predictor {
	// logger
	private static Logger logger = Logger.getLogger(MTStarPredictor.class);
	
	// the threshold parameter epsilon, that is the precision of a computed
	// prediction. It is used as exiting condition from the  loop for
	// computing MTTRy
	public static final double DEFAULT_EPSILON = 0.0001;
	
	// the threshold parameter epsilon, that is the precision of a computed
	// prediction. It is used as exiting condition from the  loop for
	// computing MTTRy
	private double epsilon;
	
	// entity managers
	private QoSEntityManagerInterface qosEM;
	private StatisticsEntityManager statisticsEM;
	private ModelFactoryEntityManager modelFactoryEM;
	private DistributionEntityManager distributionEM;
	private InferredDistributionEntityManager inferredDistributionEM;
	
	/**
	 * Class constructor. By default the predictorId is equal to the class
	 * name including its package, i.e., equal to MTTRPredictor.class.getName().
	 * The threshold parameter epsilon, that is the precision of a computed
	 * prediction is, by default, set to 0.0001.
	 */
	public MTStarPredictor() {	
		this(DEFAULT_EPSILON);
	}
	
	/**
	 * Class constructor. By default the predictorId is equal to the class
	 * name including its package, i.e., equal to MTTRPredictor.class.getName().
	 *  
	 * @param epsilon The threshold parameter epsilon, that is the precision
	 *                of a computed prediction
	 */
	public MTStarPredictor(double epsilon) {
		super(MTStarPredictor.class.getName());
		
		// the threshold parameter epsilon, that is the precision of a computed
		// prediction. It is used as exiting condition from the  loop for
		// computing MTTRy
		this.epsilon = epsilon;
		
		// gets entity managers
		qosEM = (QoSEntityManagerInterface) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.QOSS);
		statisticsEM = (StatisticsEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.STATISTICS);
		modelFactoryEM = (ModelFactoryEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MODEL_FACTORIES);
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
		inferredDistributionEM = (InferredDistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.INFERRED_DISTRIBUTIONS);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.everestplus.predictor.Predictor#getPrediction(org.slaatsoi.prediction.schema.PredictionPolicyType, long)
	 */
	@Override
	public Prediction getPrediction(PredictionPolicyType predictionPolicy, long timestamp) {
		// the prediction target variable is the variable (QoS) for which to predict
		// its violation
		String predictionTargetVariable = predictionPolicy.getPredictionTarget().getVariable().getQoSId();
		// the prediction auxiliary variable is a variable which is needed in order
		// to predict about the target variable
		String predictionAuxiliaryVariable = predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getQoSId();
		
		// computes updated best-fit models
		InferredDistributionEntity targetVariableBestFitClass = inferredDistributionEM.selectBestFit(predictionPolicy.getPredictionPolicyId(), predictionTargetVariable);
		InferredDistributionEntity auxiliaryVariableBestFitClass = inferredDistributionEM.selectBestFit(predictionPolicy.getPredictionPolicyId(), predictionAuxiliaryVariable);

		// If there are not best fit distributions the prediction computation
		// cannot be performed, therefore a Prediction object is returned
		// the prediction default value of -1
		if (targetVariableBestFitClass == null || auxiliaryVariableBestFitClass == null) {
			return new Prediction(predictionPolicy, Prediction.DEFAULT_VALUE, timestamp);
		}
		
		// creates target variable (e.g. MTTR) distribution instance
		logger.debug("Getting best distribution for target QoS: " + predictionTargetVariable);
		DistributionModel targetVariableDistribution = getDistributionInstance(targetVariableBestFitClass);
		
		// creates auxiliary variable (e.g. TTF) distribution instance
		logger.debug("Getting best distribution for auxiliary QoS: " + predictionAuxiliaryVariable);
		DistributionModel auxiliaryVariableDistribution = getDistributionInstance(auxiliaryVariableBestFitClass);
		
		// executes the prediction algorithm 
		double probabilityOfViolation = executePredictionAlgorithm(
				predictionPolicy,				// prediction policy
				targetVariableDistribution,		// MTTR distribution instance
				auxiliaryVariableDistribution,	// TTF distribution instance
				timestamp						// prediction timestamp
				);

		// updated last inserted statistic entity
		StatisticsEntity statisticsEntity = statisticsEM.selectLast();
		statisticsEntity.setModelGoodness(targetVariableBestFitClass.getGoodnessOfFit());
		statisticsEntity.setModelHistorySize(targetVariableBestFitClass.getDataPoints().length);
		statisticsEM.update(statisticsEntity);
		
		// creates a prediction object
		Prediction prediction = new Prediction(
				predictionPolicy,		// prediction policy
				probabilityOfViolation,	// prediction value
				timestamp 				// prediction timestamp
			);
		prediction.setDataPointSize(targetVariableBestFitClass.getDataPoints().length);
		
		// return the prediction
		return prediction;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param inferredDistribution
	 * @return An instance of the inferred distribution
	 */
	private DistributionModel getDistributionInstance(InferredDistributionEntity inferredDistribution) {
		DistributionModel distributionInstance = null;
		
		//logger.debug("inferredDistribution: " + inferredDistribution);
		//logger.debug("inferredDistribution.getModelFactoryId(): " + inferredDistribution.getModelFactoryId());
		//logger.debug("modelFactoryEM: " + modelFactoryEM);
		//logger.debug("modelFactoryEM.selectById(inferredDistribution.getModelFactoryId()): " + modelFactoryEM.selectById(inferredDistribution.getModelFactoryId()));
		
		// gets the model factory to create a distribution model instance
		String modelFactoryClass = modelFactoryEM.selectById(inferredDistribution.getModelFactoryId()).getClassName();
		DistributionFactory distributionFactory = (DistributionFactory) ModelFactoryBuilder.createModelFactory(modelFactoryClass);
		
		// gets the inferred distribution model class name
		String distributionClassName = distributionEM.selectDistributionById(inferredDistribution.getDistributionId()).getClassName();

		// creates an instance of the inferred distribution model
		distributionInstance = (DistributionModel) distributionFactory.createDistribution(distributionClassName, inferredDistribution.getParameters());
		
		return distributionInstance;
	}
	
	/**
	 * @param predictionSpecification
	 * @param mttrModel
	 * @param ttfModel
	 * @param timestamp
	 * @return
	 */
	private double executePredictionAlgorithm(PredictionPolicyType predictionPolicy, DistributionModel targetVariableDistribution, DistributionModel auxiliaryVariableDistribution, long timestamp) {
		double probability = 0;
		double currentSum = 0;
		double previousSum = 0;
		// the critical value beyond which the constraint TargetVariable < K is violated
		double targetVariableCriticalValue = 0;
		// probability Pr(MT*y {<= | >} MT*crit) 
		double probabilityTargetVariableViolatesCriticalValue = 0;
		double probabilityOfYFailures = 0;
		Double targetVariableCurrent;
		double countQoSTerm;
		
		// k is the constant term of a constraint expression, e.g., QoS < k
		String qosId  = predictionPolicy.getPredictionTarget().getVariable().getQoSId();
		ComparisonOperatorType comparisonOperator = predictionPolicy.getPredictionTarget().getComparisonOperator();
		double k = predictionPolicy.getPredictionTarget().getValue();
		
		logger.info("Prediction target: " + qosId + " " + comparisonOperator + " " + k);
		logger.info("Prediction horizon: " + predictionPolicy.getPredictionSettings().getPredictionHorizon());
		logger.info("Prediction timestamp: " + timestamp);
		logger.info("Prediction future timestamp: " + (timestamp+predictionPolicy.getPredictionSettings().getPredictionHorizon()));
				
		targetVariableCurrent = qosEM.selectLastByTimestamp(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),		// SLA id
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),	// Guaranteed id
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),		// QoS id
				timestamp	// timestamp
				);
		
		// if qosEM.selectLastByTimestamp query does not return a valid QoS value
		// than the prediction process stops.
		if (targetVariableCurrent == null) {
			logger.info("The prediction couldn't be computed because no QoS values were found.");
			return Prediction.DEFAULT_VALUE;
		}
		
		countQoSTerm = qosEM.count(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),		// SLA id
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),	// Guaranteed id
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),		// QoS id
				timestamp	// timestamp
				);
		
		logger.debug("Current target variable value: " + targetVariableCurrent);
		logger.debug("Current target variable occurrences: " + countQoSTerm);
		
		int y = 1;
		do {
			previousSum = currentSum;
			
			// ----------------------------------------------------------------
			// Computing Pr(y): probability to observe y occurrences of the
			// auxiliary variable given a value of y
			// ----------------------------------------------------------------
			double x1 = (double) predictionPolicy.getPredictionSettings().getPredictionHorizon()/(y-0.5);
			double x2 = (double) predictionPolicy.getPredictionSettings().getPredictionHorizon()/(y+0.5);
			double prX1 = 1-auxiliaryVariableDistribution.getCDF(x1);
			double prX2 = 1-auxiliaryVariableDistribution.getCDF(x2);
			
			// computing Pr(y)
			probabilityOfYFailures = Math.abs(prX1-prX2);
			
			logger.debug("x1: " + x1 + ", Pr(x1): " + targetVariableDistribution.getCDF(x1));
			logger.debug("x2: " + x2 + ", Pr(x2): " + targetVariableDistribution.getCDF(x2));
			logger.debug("Probability Pr(y=" + y + "): " + probabilityOfYFailures);

			// ----------------------------------------------------------------
			// Computing Pr(MT*y OP MT*crit): probability that
			//  MT*y OP MT*crit, where OP={<,<=,>=,>}
			// ----------------------------------------------------------------
			
			// MTTRCalculator future [k*(N+x)-N*MTTRc)]/x
			targetVariableCriticalValue = ((k*(countQoSTerm+y)-countQoSTerm*targetVariableCurrent))/y;
			
			logger.debug("MT*crit (y=" + y + "): " + targetVariableCriticalValue);
			
			// case of MT* {<,<=} K
			if (predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.LESS_THAN) ||
				predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.LESS_EQUAL_THAN))
			{
				// Pr(MT*y {<|<=} MT*crit)
				if (targetVariableCurrent <= k)
				{
					// probability of seen MT*y > MT*crit
					probabilityTargetVariableViolatesCriticalValue = 1.0 - targetVariableDistribution.getCDF(targetVariableCriticalValue);
					
					logger.debug("Case MT* <= k, MT*c <= k, Pr(MT*y > MT*crit) (y=" + y + "): " + probabilityTargetVariableViolatesCriticalValue);
				}
				// Pr(MT*y {>=|>} MT*crit)
				else if (targetVariableCurrent > k)
				{
					// probability of seen MT*y <= than MT*crit
					probabilityTargetVariableViolatesCriticalValue = targetVariableDistribution.getCDF(targetVariableCriticalValue);
					
					logger.debug("Case MT* <= k, MT*c > k, Pr(MT*y <= MT*crit) (y=" + y + "): " + probabilityTargetVariableViolatesCriticalValue);
				}
			}
			// case of MT* {>=,>} K
			else if (predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.GREATER_EQUAL_THAN) ||
				predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.GREATER_THAN))
			{
				// Pr(MT*y {<|<=} MT*crit)
				if (targetVariableCurrent <= k)
				{
					// probability of seen MT*y > MT*crit
					probabilityTargetVariableViolatesCriticalValue = targetVariableDistribution.getCDF(targetVariableCriticalValue);
					
					logger.debug("Case MT* >= k, MT*c > k, Pr(MT*y > MT*crit) (y=" + y + "): " + probabilityTargetVariableViolatesCriticalValue);
				}
				// Pr(MT*y {>=|>} MT*crit)
				else if (targetVariableCurrent > k)
				{
					// probability of seen MT*y <= MT*crit
					probabilityTargetVariableViolatesCriticalValue = 1.0 - targetVariableDistribution.getCDF(targetVariableCriticalValue);
					
					logger.debug("Case MT* >= k, MT*c <= k, Pr(MT*y <= MT*crit) (y=" + y + "): " + probabilityTargetVariableViolatesCriticalValue);
				}				
			}
			
			// SUM [i=0..x] Pr(Y=x)*Pr(MTTRCalculator future > W)
			currentSum += probabilityOfYFailures * probabilityTargetVariableViolatesCriticalValue;
			
			// increment counter
			y++;
		} while ((currentSum-previousSum)>epsilon);
		
		
		// ----------------------------------------------------------------
		// Computing Pr(MT* OP K): probability to violate the constraint
		// MT* OP K, where OP={<,<=,>=,>}
		// ----------------------------------------------------------------
		
		// case of MT* {<,<=} K
		if (predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.LESS_THAN) ||
			predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.LESS_EQUAL_THAN))
		{
			// Case: MT*c {<,<=} K 
			if (targetVariableCurrent <= k)
			{
				probability = previousSum;
			}		
			// Case: MT*c {>=,>} K
			else if (targetVariableCurrent > k)
			{	
				probability = 1-previousSum;
			}
		}
		// case of MT* {>=,>} K
		else if (predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.GREATER_EQUAL_THAN) ||
				predictionPolicy.getPredictionTarget().getComparisonOperator().equals(ComparisonOperatorType.GREATER_THAN))
		{
			// Case: MT*c {<,<=} K 
			if (targetVariableCurrent <= k)
			{
				probability = 1-previousSum;
			}		
			// Case: MT*c {>=,>} K
			else if (targetVariableCurrent > k)
			{	
				probability = previousSum;
			}
		}

		// START STATISTICS
		Double futureQoS = qosEM.selectLastByTimestamp(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),		// SLA id
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),	// Guaranteed id
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),		// QoS id
				timestamp + predictionPolicy.getPredictionSettings().getPredictionHorizon());
		
		Statistics statistics = new Statistics();
		statistics.setK(k);
		statistics.setMttrc(targetVariableCurrent);
		statistics.setMttry(targetVariableCriticalValue);
		statistics.setPredictionWindow(predictionPolicy.getPredictionSettings().getPredictionHorizon());
		statistics.setPrediction(probability);
		statistics.setPrMttrc(targetVariableDistribution.getCDF(targetVariableCurrent));
		statistics.setPrMttry(probabilityTargetVariableViolatesCriticalValue);
		statistics.setPrY(probabilityOfYFailures);
		statistics.setY(y);
		if (futureQoS != null) {
			statistics.setMttrf(futureQoS);
		}
		statistics.setModelHistorySize(-1);
		statistics.setModelGoodness(-1);
		statistics.setPredictionTimestamp(timestamp);
		
		statisticsEM.insert(new StatisticsEntity(statistics));
		// END STATISTICS
		
		return probability;
	}

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
