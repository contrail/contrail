/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everestplus.model.distribution.DistributionModel;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 24, 2010
 */
public class DistributionEntity extends DistributionModel implements PersistentEntity {
	private long entityId;
	
	//private String distributionId;
	private String className;
	private String description;
	
	/**
	 * @param distributionModel
	 */
	public DistributionEntity(DistributionModel distributionModel) {
		super(distributionModel.getDistributionId(), distributionModel.getParameters());
		this.className = new String(); //distributionId.getClass().getName();
		this.description = new String();
	}
	
	/**
	 * @param name
	 * @param parameters
	 */
	public DistributionEntity(String name, double[] parameters, String distributionId, String className) {
		super(distributionId, parameters);
		//this.distributionId = distributionId;
		this.className = className;
		this.description = new String();
	}
	
	/**
	 * @return the entityId
	 */
	public long getEntityId() {
		return entityId;
	}
	
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.everestplus.model.Distribution#getCDF(double)
	 */
	@Override
	public double getCDF(double x) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.everestplus.model.Distribution#getPDF(double)
	 */
	@Override
	public double getPDF(double x) {
		// TODO Auto-generated method stub
		return 0;
	}
}
