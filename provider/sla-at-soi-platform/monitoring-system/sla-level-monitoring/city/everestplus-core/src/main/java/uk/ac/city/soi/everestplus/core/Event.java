package uk.ac.city.soi.everestplus.core;

public class Event {
	private String sender;
	private String receiver;
	private String service;
	private String operation;
	private long timestamp;
	private String type;
	
	/**
	 * @param sender
	 * @param receiver
	 * @param service
	 * @param operation
	 * @param timestamp
	 * @param type
	 */
	public Event(String sender, String receiver, String service, String operation, long timestamp, String type) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.service = service;
		this.operation = operation;
		this.timestamp = timestamp;
		this.type = type;
	}

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the receiver
	 */
	public String getReceiver() {
		return receiver;
	}

	/**
	 * @param receiver the receiver to set
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
