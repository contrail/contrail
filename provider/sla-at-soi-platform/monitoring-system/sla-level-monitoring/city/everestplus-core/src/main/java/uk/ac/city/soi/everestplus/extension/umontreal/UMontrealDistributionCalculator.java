/**
 * 
 */
package uk.ac.city.soi.everestplus.extension.umontreal;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slaatsoi.prediction.schema.VariableType;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Model;
import uk.ac.city.soi.everestplus.database.DistributionEntity;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntity;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface;
import uk.ac.city.soi.everestplus.model.ModelCalculator;
import umontreal.iro.lecuyer.gof.GofFormat;
import umontreal.iro.lecuyer.probdist.ContinuousDistribution;
import umontreal.iro.lecuyer.probdist.DistributionFactory;
import cern.colt.list.DoubleArrayList;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 23, 2010
 */
public class UMontrealDistributionCalculator extends ModelCalculator {
	// logger
	private static Logger logger = Logger.getLogger(UMontrealDistributionCalculator.class);
	
	protected DistributionEntityManager distributionEM;
	protected InferredDistributionEntityManager inferredDistributionEM;
	protected QoSEntityManagerInterface qosEM;
	protected PredictionPolicyEntityManager predictionPolicyEM;
	private ModelFactoryEntityManager modelFactoryEM;
	
	private UMontrealDistributionFactory distributionFactory;
	
	
	/**
	 * @param modelCalculatorId
	 * @param connection
	 */
	public UMontrealDistributionCalculator(String modelCalculatorId) {
		super(modelCalculatorId);
		
		modelFactoryEM = (ModelFactoryEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MODEL_FACTORIES);
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
		inferredDistributionEM = (InferredDistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.INFERRED_DISTRIBUTIONS);
		qosEM = (QoSEntityManagerInterface) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.QOSS);
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
		
		distributionFactory = new UMontrealDistributionFactory();
		
		modelFactoryEM.insert(distributionFactory.getClass().getName(), distributionFactory.getClass().getName());
	}

	/* ------------------------------------------------------------------------
	 * 								PUBLIC METHODS
	 * --------------------------------------------------------------------- */

	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#updateModels(uk.ac.city.soi.everestplus.core.PredictionSpecification)
	 */
	@Override
	public void updateModels(PredictionPolicyType predictionPolicy) {
		ArrayList<VariableType> variables = new ArrayList<VariableType>();
		
		// Main model to be computed
		variables.add(predictionPolicy.getPredictionTarget().getVariable());
		
		// An auxiliary variable represents some data needed by a predictor
		// to operate and therefore a model about that data must be computed
		variables.addAll(predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable());
		
		for (VariableType variable : variables)
		{
			logger.debug("Computing distribution for QoS: " + variable.getQoSId());
			
			// retrieves QoS entities
			ArrayList<Double> qosEntities = qosEM.selectLastN(
					variable.getSlaId(),
					variable.getGuaranteedId(),
					variable.getQoSId(),
					predictionPolicy.getPredictionSettings().getHistorySize());
			
			logger.debug("Retrieved QoS: " + qosEntities);
			
			// converts the ArrayList to double[]
			double[] samples = new double[qosEntities.size()];
			for (int i=0; i<samples.length; i++)
			{
				samples[i] = qosEntities.get(i);
			}
			
			// update the models
			updateModels(predictionPolicy, variable, samples);	
		}
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#updateModels(uk.ac.city.soi.everestplus.core.PredictionSpecification, double[])
	 */
	@Override
	public void updateModels(PredictionPolicyType predictionPolicy, VariableType variable, double[] dataPoints) {
		// load distribution from database
		ArrayList<DistributionEntity> availableDistributions = distributionEM.selectAllDistributions();
		
		// loop over loaded distributions to create their instances
		for (DistributionEntity distributionEntity : availableDistributions) {
			boolean exception = false;
			
			try {
				// retrieve distribution class
				Class<ContinuousDistribution> distributionClass = (Class<ContinuousDistribution>) this.getClass().getClassLoader().loadClass(distributionEntity.getClassName());
				
				// create distribution instance
				ContinuousDistribution distributionInstance = DistributionFactory.getDistributionMLE(distributionClass, dataPoints, dataPoints.length);
				
				// insert the distribution into the model database
				if (distributionInstance != null) {
					// compute goodness of fit
					double goodnessOfFit = getKolmogorovSmirnovStatistic(dataPoints, distributionInstance);
					UMontrealDistribution distribution = new UMontrealDistribution(
							distributionInstance, 						// distribution instance
							distributionInstance.getClass().getName(), 	// distribution id
							dataPoints, 								// data points
							goodnessOfFit 								// goodness of fit
							);
					
					// insert model	
					insertInferredDistribution(predictionPolicy, variable, distribution, dataPoints.length);
				} else {
					exception = true;
					new Exception();
				}
			} catch (ClassNotFoundException e) {
				exception = true;
				//System.err.println("Not found distribution class: " + distributionEntity.getClassName());
			} catch (Exception e) {
				exception = true;
				//System.err.println("Unable to instance distribution class: " + distributionEntity.getClassName());
			}
			
			// if exception then remove the model corresponding to the one who
			// generated the exception
			if (exception) {
				String distributionId = distributionEM.selectDistributionByClassName(distributionEntity.getClassName()).getDistributionId();
				inferredDistributionEM.delete(predictionPolicy.getPredictionPolicyId(), variable.getQoSId(), distributionId);
			}
		}
		System.out.println();
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#getBestFitModel(org.slaatsoi.prediction.schema.PredictionPolicyType)
	 */
	@Override
	public Model getBestFitModel(PredictionPolicyType predictionPolicy) {	
		return getBestFitModel(predictionPolicy.getPredictionPolicyId(), predictionPolicy.getPredictionTarget().getVariable().getQoSId());
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#getBestFitModel(java.lang.String)
	 */
	@Override
	public Model getBestFitModel(String predictionPolicyId) {
		PredictionPolicyType predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicyId);
		return predictionPolicy == null ? null : getBestFitModel(predictionPolicy);
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#getModels(java.lang.String, double[])
	 */
	@Override
	public ArrayList<Model> getModels(String predictionPolicyId, double[] dataPoints) {
		PredictionPolicyType predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicyId);
		
		ArrayList<Model> result = new ArrayList<Model>();
		
		if (predictionPolicy != null) {
			result = getModels(predictionPolicy.getPredictionPolicyId(), predictionPolicy.getPredictionTarget().getVariable().getQoSId(), dataPoints);
		}
		
		return result; 
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelCalculator#getModels(org.slaatsoi.prediction.schema.PredictionPolicyType, double[])
	 */
	@Override
	public ArrayList<Model> getModels(PredictionPolicyType predictionPolicy, double[] dataPoints) {
		return getModels(predictionPolicy.getPredictionPolicyId(), predictionPolicy.getPredictionTarget().getVariable().getQoSId(), dataPoints);
	}	
	
	/* ------------------------------------------------------------------------
	 * 								PRIVATE METHODS
	 * --------------------------------------------------------------------- */

	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @param dataPoints
	 * @return
	 */
	private ArrayList<Model> getModels(String predictionPolicyId, String qosId, double[] dataPoints) {
		// load distribution from database
		ArrayList<DistributionEntity> distributions = distributionEM.selectAllDistributions();
		ArrayList<Model> models = new ArrayList<Model>();
		
		// loop over loaded distributions to create their instances
		for (DistributionEntity distributionEntity : distributions) {
			boolean exception = false;
			
			try {
				// retrieve distribution class
				Class<ContinuousDistribution> distributionClass = (Class<ContinuousDistribution>) this.getClass().getClassLoader().loadClass(distributionEntity.getClassName());
				
				// create distribution instance
				ContinuousDistribution distributionInstance = DistributionFactory.getDistributionMLE(distributionClass, dataPoints, dataPoints.length);
				
				// insert the distribution into the model database
				if (distributionInstance != null) {
					// compute goodness of fit
					double goodnessOfFit = getKolmogorovSmirnovStatistic(dataPoints, distributionInstance);
					UMontrealDistribution distribution = new UMontrealDistribution(
							distributionInstance, // distribution instance
							distributionInstance.getClass().getName(), // distribution id
							dataPoints, // data points
							goodnessOfFit // goodness of fit
							);
					// add model	
					models.add(distribution);
				} else {
					exception = true;
					new Exception();
				}
			} catch (ClassNotFoundException e) {
				exception = true;
				//System.err.println("Not found distribution class: " + distributionEntity.getClassName());
			} catch (Exception e) {
				exception = true;
				//System.err.println("Unable to instance distribution class: " + distributionEntity.getClassName());
			}
		}
		
		return models;
	}
	
	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @return
	 */
	public UMontrealDistribution getBestFitModel(String predictionPolicyId, String qosId) {
		// get the best fit distribution corresponding to a prediction specification
		InferredDistributionEntity inferredDistributionEntity = inferredDistributionEM.selectBestFit(predictionPolicyId, qosId);
		
		// construct the parameter string
		String parameters = new String();
		for (int i=0; i<inferredDistributionEntity.getParameters().length; i++) {
			parameters += inferredDistributionEntity.getParameters()[i];
			
			// if the current parameter isn't the last one add a comma
			if (i<(inferredDistributionEntity.getParameters().length-1)) {
				parameters += ", ";
			}
		}
		
		String className = distributionEM.selectDistributionById(inferredDistributionEntity.getDistributionId()).getClassName();
		
		UMontrealDistribution distribution = (UMontrealDistribution) new UMontrealDistributionFactory().createDistribution(className, inferredDistributionEntity.getParameters()); 
		
		if (distribution != null) {
			distribution.setGoodnessOfFit(inferredDistributionEntity.getGoodnessOfFit());
		}
		
		return distribution; 
	}
	
	/**
	 * @param qoss
	 * @param distributions
	 */
	private double getKolmogorovSmirnovStatistic(double qoss[], ContinuousDistribution distribution) {
		DoubleArrayList qosArrayList = new DoubleArrayList(qoss);
		
		// get the best fit distribution

		String ksOutput = GofFormat.formatKS(qosArrayList, distribution);
		
		//System.out.println(distribution.getClass().getName());
		//System.out.println(ksOutput);
		
		// parsing formatKS output
		String startToken = "Kolmogorov-Smirnov statistic = D      :    ";
		String endToken = "Significance level of test            :";

		// removing information about D+ and D-
		ksOutput = ksOutput.substring(ksOutput.indexOf(startToken));

		// extracting information about D
		ksOutput = ksOutput.substring(
				ksOutput.indexOf(startToken)+startToken.length(), // start substring
				ksOutput.indexOf(endToken) // end substring
				);
		
		return Double.parseDouble(ksOutput);
	}
	
	/**
	 * @param distribution
	 */
	private void insertInferredDistribution(PredictionPolicyType predictionPolicy, VariableType variable, UMontrealDistribution distribution, int dataPoints) {
		
		InferredDistributionEntity inferredDistributionEntity = new InferredDistributionEntity(
				predictionPolicy.getPredictionPolicyId(),
				variable.getQoSId(),
				distributionFactory.getClass().getName(),
				distribution
				);
		
		// insert the object in the database
		inferredDistributionEM.insert(inferredDistributionEntity);
	}
}