/**
 * 
 */
package uk.ac.city.soi.everestplus.model;

/**
 * @author Davide Lorenzoli
 * 
 * @date Nov 22, 2010
 */
public interface ModelFactory<E> {
	
	/**
	 * Checks whether a factory supports the creation of the model passed
	 * as argument.
	 * 
	 * @param modelClass
	 * @return <code>true</code> if the model factory supports the creation of
	 *         the model passed as argument, <code>false</code> otherwise.
	 */
	public boolean isSupported(Class modelClass); 
}
