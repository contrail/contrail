/**
 * 
 */
package uk.ac.city.soi.everestplus.extension.umontreal;

import uk.ac.city.soi.everestplus.model.distribution.InferredDistribution;
import umontreal.iro.lecuyer.gof.GofFormat;
import umontreal.iro.lecuyer.probdist.ContinuousDistribution;
import cern.colt.list.DoubleArrayList;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 23, 2010
 */
public class UMontrealDistribution extends InferredDistribution {
	private ContinuousDistribution distribution;

	/**
	 * @param distribution
	 * @param distributionId
	 * @param parameters
	 * @param dataPoints
	 * @param goodnessOfFit
	 */
	public UMontrealDistribution(ContinuousDistribution distribution, String distributionId, double[] dataPoints, double goodnessOfFit) {
		super(distributionId, distribution.getParams(), dataPoints, goodnessOfFit);
		this.distribution = distribution;		
	}
	
	/**
	 * @param distribution
	 * @param distributionId
	 * @param dataPoints
	 */
	public UMontrealDistribution(ContinuousDistribution distribution, String distributionId, double[] dataPoints) {
		super(distributionId, distribution.getParams(), dataPoints, Double.NaN);
		this.distribution = distribution;
	}

	/**
	 * @param distribution
	 * @param distributionId
	 */
	public UMontrealDistribution(ContinuousDistribution distribution, String distributionId) {
		super(distributionId, distribution.getParams(), new double[0], Double.NaN);
		this.distribution = distribution;
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionModel#getCDF(double)
	 */
	public double getCDF(double x) {
		return distribution.cdf(x);
	}


	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionModel#getPDF(double)
	 */
	public double getPDF(double x) {		
		return distribution.density(x);
	}

	/**
	 * @return the distribution
	 */
	public ContinuousDistribution getDistribution() {
		return distribution;
	}

	/**
	 * @param distribution the distribution to set
	 */
	public void setDistribution(ContinuousDistribution distribution) {
		this.distribution = distribution;
	}

	/**
	 * @param samples
	 * @return
	 */
	public double getGoodnessOfFit(double[] samples) {
		DoubleArrayList qosArrayList = new DoubleArrayList(samples);
		
		// get the best fit distribution

		String ksOutput = GofFormat.formatKS(qosArrayList, distribution);
		
		//System.out.println(distribution.getClass().getName());
		//System.out.println(ksOutput);
		
		// parsing formatKS output
		String startToken = "Kolmogorov-Smirnov statistic = D      :    ";
		String endToken = "Significance level of test            :";

		// removing information about D+ and D-
		ksOutput = ksOutput.substring(ksOutput.indexOf(startToken));

		// extracting information about D
		ksOutput = ksOutput.substring(
				ksOutput.indexOf(startToken)+startToken.length(), // start substring
				ksOutput.indexOf(endToken) // end substring
				);

		return Double.parseDouble(ksOutput);
	}
}
