/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 26, 2011
 */
public class QoSEntityManager extends EntityManagerCommons implements QoSEntityManagerInterface {
	// logger
	private static Logger logger = Logger.getLogger(QoSEntityManager.class);

	private static final String DATABASE_TABLE_NAME = "qos"; 
	
	/**
	 * @param connection
	 */
	public QoSEntityManager(Connection connection) {
		super(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public QoSEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	public int insert(String slaId, String agreementTermId, String guaranteedId, String qosId, double value, long timestamp) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;
		
		try {			
			String query = "INSERT INTO " + getDatabaseTable() + " " +
            			   "SET sla_id = ?, agreement_term_id = ?, guaranteed_id = ?, qos_id = ?, value = ?, timestamp = ?";
			
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, slaId);
		    pstmt.setString(2, agreementTermId);
		    pstmt.setString(3, guaranteedId);
		    pstmt.setString(4, qosId);
		    pstmt.setDouble(5, value);
		    pstmt.setLong(6, timestamp);
		   
            insertedRecords = pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	@Override
	public ArrayList<Double> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLastByTimestamp(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	@Override
	public Double selectLastByTimestamp(String slaId, String guaranteedId, String qosId, long timestamp) {
		Double result = Double.NaN;
		
		PreparedStatement pstmt;
		
		String query = "SELECT value " +
	       			   "FROM " + getDatabaseTable() + " " +
		               "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ? AND timestamp > 0 AND timestamp <= ? " +
		               "ORDER BY timestamp DESC LIMIT 1";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, qosId);
            pstmt.setLong(4, timestamp);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				result = resultSet.getDouble("value");
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLastN(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	@Override
	public ArrayList<Double> selectLastN(String slaId, String guaranteedId, String qosId, long numberOfQoS) {
		ArrayList<Double> result = new ArrayList<Double>();
		
		PreparedStatement pstmt;
		
		String query = "SELECT value " +
	       			   "FROM " + getDatabaseTable() + " " +
		               "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ? AND timestamp > 0 " +
		               "ORDER BY timestamp DESC LIMIT ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, qosId);
            pstmt.setLong(4, numberOfQoS);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				result.add(resultSet.getDouble("value"));
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLast(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Double selectLast(String slaId, String guaranteedId, String qosId) {
		Double result = Double.NaN;
		
		PreparedStatement pstmt;
		
		String query = "SELECT value " +
	       			   "FROM " + getDatabaseTable() + " " +
		               "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ? " +
		               "ORDER BY timestamp DESC LIMIT 1";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, qosId);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				result = resultSet.getDouble("value");
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectByName(java.lang.String, java.lang.String, java.lang.String, long, int)
	 */
	@Override
	public ArrayList<Double> selectByName(String slaId, String guaranteedId, String qosId, long timestamp, int limit) {
		ArrayList<Double> result = new ArrayList<Double>();
		
		PreparedStatement pstmt;
		
		String query = "SELECT value " +
	       			   "FROM " + getDatabaseTable() + " " +
		               "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ? AND timestamp > 0 AND timestamp <= ? " +
		               "ORDER BY timestamp DESC LIMIT ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, qosId);
            pstmt.setLong(4, timestamp);
            pstmt.setInt(5, limit);

            logger.debug(pstmt);
            
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				result.add(resultSet.getDouble("value"));
			}

			resultSet.close();

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#count(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	@Override
	public int count(String slaId, String guaranteedId, String qosId, long timestamp) {
		int result = 0;
		
		PreparedStatement pstmt;
		
		String query = "SELECT COUNT(qos_id) " +
				       "FROM " + getDatabaseTable() + " " +
					   "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ? AND timestamp > 0 AND timestamp <= ?";
		
		try {
	        pstmt = getConnection().prepareStatement(query);

	        pstmt.setString(1, slaId);
	        pstmt.setString(2, guaranteedId);
	        pstmt.setString(3, qosId);
	        pstmt.setLong(4, timestamp);

	        logger.debug(pstmt);
	        
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = rs.getInt("COUNT(qos_id)");
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#count(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int count(String slaId, String guaranteedId, String qosId) {
		int result = 0;
		
		PreparedStatement pstmt;
		
		String query = "SELECT COUNT(qos_id) " +
				       "FROM " + getDatabaseTable() + " " +
					   "WHERE sla_id = ? AND guaranteed_id = ? AND qos_id = ?";
		
		try {
            pstmt = getConnection().prepareStatement(query);

            pstmt.setString(1, slaId);
            pstmt.setString(2, guaranteedId);
            pstmt.setString(3, qosId);

            logger.debug(pstmt);
            
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = rs.getInt("COUNT(qos_id)");
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}

	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	@Override
	public int deleteByPrimaryKey(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	@Override
	public Double selectByPrimaryKey(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	@Override
	public ArrayList<Double> executeQuery(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	@Override
	public int executeUpdate(String arg0) {
		throw new UnsupportedOperationException("TO DO");
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
