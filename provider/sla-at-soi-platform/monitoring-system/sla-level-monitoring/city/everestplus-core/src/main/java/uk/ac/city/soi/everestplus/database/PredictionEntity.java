/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everestplus.core.Prediction;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 15, 2010
 */
public class PredictionEntity extends Prediction implements PersistentEntity {
	private long entityId;
	
	/**
	 * @param prediction
	 */
	public PredictionEntity(Prediction prediction) {
		super(prediction.getPredictionSpecification(), prediction.getValue(), prediction.getTimestamp());
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the entityId
	 */
	public long getEntityId() {
		return entityId;
	}
	
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
