/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 22, 2010
 */
public class DistributionEntityManager extends EntityManagerCommons implements EntityManagerInterface<DistributionEntity> {
	private static final String DATABASE_TABLE_NAME = "distribution";
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public DistributionEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}

	/**
	 * @param connection
	 */
	public DistributionEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param distributionId
	 * @param name
	 * @param description
	 * @param className
	 * @return
	 */
	public int insert(String distributionId, String name, String description, String className) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;
		
		try {
			
			String query = "INSERT INTO " + getDatabaseTable() + " " +
			   			   "SET distribution_id=?, name=?, description=?, class_name=?";	
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, distributionId);
			pstmt.setString(2, name);
			pstmt.setString(3, description);
			pstmt.setString(4, className);
			
			insertedRecords = pstmt.executeUpdate();
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}	
	
	/**
	 * @param className
	 * @return
	 */
	public DistributionEntity selectDistributionByClassName(String className) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		DistributionEntity result = null;

		try {			
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "WHERE class_name = ?";
			
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, className);
		    
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = new DistributionEntity(
						rs.getString("name"),			// name
						new double[0],					// array of parameters
						rs.getString("distribution_id"),	// distribution id	
						rs.getString("class_name")		// class name
						);
				result.setDescription(rs.getString("description"));
			}
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return result;		
	}
	
	/**
	 * @param distributionId
	 * @return A DistributionEntity object, <code>null</code> if nothing is found
	 */
	public DistributionEntity selectDistributionById(String distributionId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		DistributionEntity result = null;

		try {			
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "WHERE distribution_id = ?";
			
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, distributionId);
		    
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				result = new DistributionEntity(
						rs.getString("name"),			// name
						new double[0],					// array of parameters
						rs.getString("distribution_id"),	// distribution id	
						rs.getString("class_name")		// class name
						);
				result.setDescription(rs.getString("description"));
			}
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return result;		
	}
		
	/**
	 * @return The list of available class names implementing statistical distributions
	 */
	public ArrayList<DistributionEntity> selectAllDistributions() {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<DistributionEntity> results = new ArrayList<DistributionEntity>();

		try {			
			String query = "SELECT * FROM " + getDatabaseTable();
			
		    pstmt = getConnection().prepareStatement(query);
		    		           
		    ResultSet rs = pstmt.executeQuery();
		    
			if (rs.first()) {
				do {
					DistributionEntity distribution = new DistributionEntity(
							rs.getString("name"),			// name
							new double[0],					// array of parameters
							rs.getString("distribution_id"),	// distribution id	
							rs.getString("class_name")		// class name
							);
					distribution.setDescription(rs.getString("description"));
					
					results.add(distribution);
				} while (rs.next());
			}
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return results;		
	}

	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(DistributionEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery()
	 */
	public ArrayList<DistributionEntity> executeQuery(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	public int executeUpdate(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(DistributionEntity entity) {
		throw new UnsupportedOperationException("TO DO");	
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<DistributionEntity> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public DistributionEntity selectByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(DistributionEntity entity) {
		throw new UnsupportedOperationException("TO DO");
	}
}
