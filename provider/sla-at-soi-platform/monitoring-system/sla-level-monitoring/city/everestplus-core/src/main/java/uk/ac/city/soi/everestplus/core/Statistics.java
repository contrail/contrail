/**
 * 
 */
package uk.ac.city.soi.everestplus.core;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jan 3, 2011
 */
public class Statistics {
	private long statisticsId;
	private double k;
	private double predictionWindow;
	private double y;
	private double mttrc;
	private double mttry;
	private double mttrf;
	private double prY;
	private double prMttrc;
	private double prMttry;
	private double prediction;
	private int modelHistorySize;
	private double modelGoodness;
	private long predictionTimestamp;
	private String executionId;
	private int kType;
	private String isPredictionCorrect;
	
	/**
	 * 
	 */
	public Statistics() {
		predictionWindow = -1;
		k = -1;
		y = -1;
		mttrc = -1;
		mttry = -1;
		mttrf = -1;
		prY = -1;
		prMttrc = -1;
		prMttry = -1;
		prediction = -1;
		modelHistorySize = -1;
		modelGoodness = -1;
		predictionTimestamp = -1;
		executionId = new String();
		kType = -1;
		isPredictionCorrect = new String();
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return the statisticsId
	 */
	public long getStatisticsId() {
		return statisticsId;
	}

	/**
	 * @param statisticsId the statisticsId to set
	 */
	public void setStatisticsId(long statisticsId) {
		this.statisticsId = statisticsId;
	}

	/**
	 * @return the k
	 */
	public double getK() {
		return k;
	}

	/**
	 * @param k the k to set
	 */
	public void setK(double k) {
		this.k = k;
	}

	/**
	 * @return the predictionWindow
	 */
	public double getPredictionWindow() {
		return predictionWindow;
	}

	/**
	 * @param predictionWindow the predictionWindow to set
	 */
	public void setPredictionWindow(double predictionWindow) {
		this.predictionWindow = predictionWindow;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * @return the mttrc
	 */
	public double getMttrc() {
		return mttrc;
	}

	/**
	 * @param mttrc the mttrc to set
	 */
	public void setMttrc(double mttrc) {
		this.mttrc = mttrc;
	}

	/**
	 * @return the mttry
	 */
	public double getMttry() {
		return mttry;
	}

	/**
	 * @param mttry the mttry to set
	 */
	public void setMttry(double mttry) {
		this.mttry = mttry;
	}

	/**
	 * @return the mttrf
	 */
	public double getMttrf() {
		return mttrf;
	}

	/**
	 * @param mttrf the mttrf to set
	 */
	public void setMttrf(double mttrf) {
		this.mttrf = mttrf;
	}

	/**
	 * @return the prY
	 */
	public double getPrY() {
		return prY;
	}

	/**
	 * @param prY the prY to set
	 */
	public void setPrY(double prY) {
		this.prY = prY;
	}

	/**
	 * @return the prMttrc
	 */
	public double getPrMttrc() {
		return prMttrc;
	}

	/**
	 * @param prMttrc the prMttrc to set
	 */
	public void setPrMttrc(double prMttrc) {
		this.prMttrc = prMttrc;
	}

	/**
	 * @return the prMttry
	 */
	public double getPrMttry() {
		return prMttry;
	}

	/**
	 * @param prMttry the prMttry to set
	 */
	public void setPrMttry(double prMttry) {
		this.prMttry = prMttry;
	}

	/**
	 * @return the prediction
	 */
	public double getPrediction() {
		return prediction;
	}

	/**
	 * @param prediction the prediction to set
	 */
	public void setPrediction(double prediction) {
		this.prediction = prediction;
	}

	/**
	 * @return the modelHistorySize
	 */
	public int getModelHistorySize() {
		return modelHistorySize;
	}

	/**
	 * @param modelHistorySize the modelHistorySize to set
	 */
	public void setModelHistorySize(int modelHistorySize) {
		this.modelHistorySize = modelHistorySize;
	}

	/**
	 * @return the modelGoodness
	 */
	public double getModelGoodness() {
		return modelGoodness;
	}

	/**
	 * @param modelGoodness the modelGoodness to set
	 */
	public void setModelGoodness(double modelGoodness) {
		this.modelGoodness = modelGoodness;
	}

	/**
	 * @return the predictionTimestamp
	 */
	public long getPredictionTimestamp() {
		return predictionTimestamp;
	}

	/**
	 * @param predictionTimestamp the predictionTimestamp to set
	 */
	public void setPredictionTimestamp(long predictionTimestamp) {
		this.predictionTimestamp = predictionTimestamp;
	}

	/**
	 * @return the executionId
	 */
	public String getExecutionId() {
		return executionId;
	}

	/**
	 * @param executionId the executionId to set
	 */
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	/**
	 * @return the kType
	 */
	public int getkType() {
		return kType;
	}

	/**
	 * @param kType the kType to set
	 */
	public void setkType(int kType) {
		this.kType = kType;
	}
	
	/**
	 * @param isPredictionCorrect the isPredictionCorrect to set
	 */
	public void setPredictionCorrect(String isPredictionCorrect) {
		this.isPredictionCorrect = isPredictionCorrect;
	}
	
	/**
	 * @return the isPredictionCorrect
	 */
	public String isPredictionCorrect() {
		return isPredictionCorrect;
	}
		
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
