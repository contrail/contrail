/**
 * 
 */
package uk.ac.city.soi.everestplus.communication;

import java.util.Properties;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.common.eventschema.service.MonitoringEventService;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;

import uk.ac.city.soi.everestplus.core.FrameworkContext.PropertiesFileNames;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Prediction;
import uk.ac.city.soi.everestplus.parser.PredictionEventParser;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 30, 2011
 */
public class PredictionResultPublisher {
	// logger
	private final static Logger logger = Logger.getLogger(PredictionResultPublisher.class);
	
	private final static String CHANNEL_NAME_PROPERTY = "xmpp_channel";
	
	private MonitoringEventService monitoringEventService = new MonitoringEventServiceImpl();

    /** pub sub manager. */
    private PubSubManager pubSubManager;

    private Properties xmppProperties;
	
	/**
	 * @param channelName
	 * @param monitoringResultEntityManager
	 */
	public PredictionResultPublisher() {
		this.xmppProperties = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.XMPP);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Subscribe to the result channel.
	 */
	public void subscribe() {
        try {
        	// create pub/sub manager by passing a properties object
			pubSubManager = PubSubFactory.createPubSubManager(xmppProperties);
			// subscribe to channel
			pubSubManager.createAndSubscribe(new Channel(xmppProperties.getProperty(CHANNEL_NAME_PROPERTY)));
			
			logger.debug("Subscription parameters:");
			logger.debug("\tProperties: " + xmppProperties);
			logger.debug("\tChannel: " + xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}		
	}

    /**
     * Unsubscribe from the result channel.
     */
    public void unsubscribe() {
        try {
        	pubSubManager.unsubscribe(xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
            pubSubManager.close();
        } catch (MessagingException e) {
        	logger.error(e.getMessage(), e);
        }
    }
    
    /**
     * @param prediction
     * @return <code>true</code> if publishing succeed, <code>false</code> otherwise 
     */
    public boolean publish(Prediction prediction) {
    	boolean result = true;
    	
		try {
			String eventPayload = monitoringEventService.marshall(PredictionEventParser.getPredictionEvent(prediction));
			//EventInstance ev = PredictionEventParser.getPredictionEvent(prediction);
			//ev.getEventPayload().
			pubSubManager.publish(new PubSubMessage(xmppProperties.getProperty(CHANNEL_NAME_PROPERTY), eventPayload));
			
			logger.info("Prediction Result " + eventPayload);
			
		} catch (DatatypeConfigurationException e) {
			result = false;
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			result = false;
			logger.error(e.getMessage(), e);
		}
    	
    	return result;
    }
    
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
        
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
