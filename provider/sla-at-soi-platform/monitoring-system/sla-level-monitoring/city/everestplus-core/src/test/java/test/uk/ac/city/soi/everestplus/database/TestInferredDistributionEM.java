/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;


import java.io.IOException;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntity;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistribution;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;
import uk.ac.city.soi.everestplus.model.ModelFactoryBuilder;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;
import umontreal.iro.lecuyer.probdist.CramerVonMisesDist;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.UniformDist;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 16, 2010
 */
public class TestInferredDistributionEM extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestInferredDistributionEM.class);
	
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	
	private PredictionPolicyEntityManager predictionPolicyEM;
	private DistributionEntityManager distributionEM;
	private ModelFactoryEntityManager modelFactoryEM;
	private InferredDistributionEntityManager inferredDistributionEM;
	private PredictionEntityManager predictionEM;
	
	private UMontrealDistribution[] distributions;
	
	/**
	 * 
	 */
	public TestInferredDistributionEM() {
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
		modelFactoryEM = (ModelFactoryEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MODEL_FACTORIES);
		inferredDistributionEM = (InferredDistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.INFERRED_DISTRIBUTIONS);
		predictionEM = (PredictionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTIONS);
		
		distributions = getDistributions();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		inferredDistributionEM.truncate();
		predictionEM.truncate();
		predictionPolicyEM.truncate();
		distributionEM.truncate();
		modelFactoryEM.truncate();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	protected void tearDown() throws Exception {
		inferredDistributionEM.truncate();
		predictionEM.truncate();
		predictionPolicyEM.truncate();
		distributionEM.truncate();
		modelFactoryEM.truncate();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws SAXException 
	 * @throws IOException 
	 * 
	 */
	@Test
	public void testInsertDistribution() throws IOException, SAXException {
		insertDistributions();
		insertModelFactory();
		
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		predictionPolicyEM.insert(predictionPolicy);
		
		insertInferredDistributions(predictionPolicy);
		
		int entries = distributions[0].getParameters().length +  distributions[1].getParameters().length +  distributions[2].getParameters().length;
			
		Assert.assertEquals(entries, inferredDistributionEM.count());
	}
	
	/**
	 * @throws SAXException 
	 * @throws IOException 
	 * 
	 */
	@Test
	public void testSelectBestFit() throws IOException, SAXException {
		insertDistributions();
		insertModelFactory();
		
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		predictionPolicyEM.insert(predictionPolicy);
		
		insertInferredDistributions(predictionPolicy);
		
		InferredDistributionEntity inferredDistribution = inferredDistributionEM.selectBestFit(
				predictionPolicy.getPredictionPolicyId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId());
		
		Assert.assertEquals(distributions[0].getDistributionId(), inferredDistribution.getDistributionId());
		Assert.assertEquals(distributions[0].getParameters().length, inferredDistribution.getParameters().length);
		Assert.assertEquals(0.3, inferredDistribution.getGoodnessOfFit());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param predictionPolicy
	 */
	private void insertInferredDistributions(PredictionPolicyType predictionPolicy) {
		InferredDistributionEntity entity = new InferredDistributionEntity(
				predictionPolicy.getPredictionPolicyId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
				UMontrealDistributionFactory.class.getName(),
				distributions[0].getDistributionId(),
				distributions[0].getParameters(),
				new double[300],
				0.3d
				);
		
		inferredDistributionEM.insert(entity);
		
		entity = new InferredDistributionEntity(
				predictionPolicy.getPredictionPolicyId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
				UMontrealDistributionFactory.class.getName(),
				distributions[1].getDistributionId(),
				distributions[1].getParameters(),
				new double[500],
				0.5d
				);
		
		inferredDistributionEM.insert(entity);
		
		entity = new InferredDistributionEntity(
				predictionPolicy.getPredictionPolicyId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
				UMontrealDistributionFactory.class.getName(),
				distributions[2].getDistributionId(),
				distributions[2].getParameters(),
				new double[250],
				0.5d
				);
		
		inferredDistributionEM.insert(entity);
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	/**
	 * 
	 */
	private void insertDistributions() {
		Class[] distributions = UMontrealDistributionFactory.getSupportedDistributions();
		
		for (int i=0; i<distributions.length; i++) {
			String className = distributions[i].getConstructors()[0].getName();
			
			distributionEM.insert(
					className, // distribution id
					className.substring(className.lastIndexOf(".")+1, className.lastIndexOf("Dist")), // distribution name
					"University of Montreal distribution implementation", // descritpion
					className); // class name
		}
		
		Assert.assertEquals(distributions.length, distributionEM.count());
	}
	
	/**
	 * 
	 */
	private void insertModelFactory() {
		modelFactoryEM.insert(UMontrealDistributionFactory.class.getName(), UMontrealDistributionFactory.class.getSimpleName());
	}
	
	/**
	 * @return
	 */
	private UMontrealDistribution[] getDistributions() {
		UMontrealDistribution[] distributions = new UMontrealDistribution[3];
		
		UMontrealDistributionFactory modelFactory = (UMontrealDistributionFactory) ModelFactoryBuilder.createModelFactory(UMontrealDistributionFactory.class.getName());
		
		distributions[0] = modelFactory.createDistribution(
				NormalDist.class.getName(), 		// distribution id
				new double[] {12.3d, 23.4d});	// parameters
		
		distributions[1] = modelFactory.createDistribution(
				CramerVonMisesDist.class.getName(), // distribution id
				new double[] {12.3d});				// parameters
		
		distributions[2] = modelFactory.createDistribution(
				UniformDist.class.getName(), 	// distribution id
				new double[] {23.4d, 54.3d});		// parameters
		
		return distributions;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
