/**
 * 
 */
package test.uk.ac.city.soi.everestplus.util;

import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;

import eu.slaatsoi.slamodel.SLADocument;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 3, 2011
 */
public class SLAFactory {

	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	public static SLA loadSLA(String fileName) throws Exception {
		SLADocument slaXml = SLADocument.Factory.parse(ClassLoader.getSystemResourceAsStream(fileName));
		
		SLASOIParser slasoiParser = new SLASOIParser();
		SLA sla = slasoiParser.parseSLA(slaXml.xmlText());

		return sla;
	}

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// MAIN METHOD
	// ------------------------------------------------------------------------
}
