/**
 * 
 */
package test.uk.ac.city.soi.everestplus.parser;


import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;

import test.uk.ac.city.soi.everestplus.util.B6SLACreatorSyntaxConverter;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 11, 2011
 */
public class TestSLAParser extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	// logger
	private static Logger logger = Logger.getLogger(TestSLAParser.class);

	private final static String PREDICTION_SCHEMA_NAME = "model/prediction-policy.xsd";
	private final static String B6_SLA = "B6-SLA-PredictionPolicy-MeanBookTime.xml";
	
	private final static String MEAN_BOOK_TIME_REPORTING_POLICY = "PredictionPolicy-MeanBookTime.xml";
	
	private String predictionPolicyXmlSchema;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		predictionPolicyXmlSchema = XMLUtils.fileToString(getClass().getClassLoader().getResource(PREDICTION_SCHEMA_NAME).getPath());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	@Test
	public void testParsePredictionPolicy() throws Exception {
		String predictionPolicyXml = XMLUtils.fileToString(getClass().getClassLoader().getResource(MEAN_BOOK_TIME_REPORTING_POLICY).getPath());
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXml);
		
		assertNotNull(predictionPolicy);
		
		assertEquals("mean_book_time", predictionPolicy.getPredictionPolicyId());
	}
	
	@Test
	public void testParseSLA() throws Exception {
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(B6_SLA);
		
		assertNotNull(sla);
		
		CustomAction customAction = (CustomAction)  ((Guaranteed.Action) sla.getAgreementTerm("GuarActions").getGuaranteed("meanBookTimeKpiReporting")).getPostcondition();
		
		assertEquals(1, customAction.getPropertyKeys().length);
		
		PredictionPolicyType[] predictionPolicies = PredictionPolicyParser.parseSLA(sla, predictionPolicyXmlSchema);
		
		assertEquals(1, predictionPolicies.length);
		
		assertEquals("mean_book_time", predictionPolicies[0].getPredictionPolicyId());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
