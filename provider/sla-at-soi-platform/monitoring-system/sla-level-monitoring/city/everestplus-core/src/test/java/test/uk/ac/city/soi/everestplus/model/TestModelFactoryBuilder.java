/**
 * 
 */
package test.uk.ac.city.soi.everestplus.model;


import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;

import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistribution;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;
import uk.ac.city.soi.everestplus.model.ModelFactory;
import uk.ac.city.soi.everestplus.model.ModelFactoryBuilder;
import umontreal.iro.lecuyer.probdist.NormalDist;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 3, 2011
 */
public class TestModelFactoryBuilder extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	// logger
	private static Logger logger = Logger.getLogger(TestModelFactoryBuilder.class);
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	@Test
	public void testModelFactoryCreation() throws Exception {
		ModelFactoryBuilder.addModelFactory(new UMontrealDistributionFactory());
		ModelFactory modelFactory = ModelFactoryBuilder.createModelFactoryNew(NormalDist.class);
		
		assertNotNull(modelFactory);
		assertEquals(UMontrealDistributionFactory.class, modelFactory.getClass());

		UMontrealDistributionFactory distributionFactory = (UMontrealDistributionFactory) modelFactory;
		
		double mean = 1;
		double variance = 3;
		double[] parameters = new double[] {mean, variance};
		UMontrealDistribution distributionModel = distributionFactory.createDistribution(NormalDist.class, parameters);
		
		assertNotNull(distributionModel);
		
		modelFactory = ModelFactoryBuilder.createModelFactoryNew(String.class);
		assertNull(modelFactory);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
