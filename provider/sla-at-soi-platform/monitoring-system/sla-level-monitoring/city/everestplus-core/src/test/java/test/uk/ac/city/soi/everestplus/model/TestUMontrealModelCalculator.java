/**
 * 
 */
package test.uk.ac.city.soi.everestplus.model;


import java.io.IOException;
import java.util.ArrayList;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Model;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionCalculator;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 16, 2010
 */
public class TestUMontrealModelCalculator extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestUMontrealModelCalculator.class);
	
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();

	private PredictionPolicyEntityManager predictionPolicyEM;
	private DistributionEntityManager distributionEM;
	private ModelFactoryEntityManager modelFactoryEM;
	private InferredDistributionEntityManager inferredDistributionEM;
	private PredictionEntityManager predictionEM;
	
	/**
	 * 
	 */
	public TestUMontrealModelCalculator() {
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
		inferredDistributionEM = (InferredDistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.INFERRED_DISTRIBUTIONS);
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
		modelFactoryEM = (ModelFactoryEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MODEL_FACTORIES);
		predictionEM = (PredictionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTIONS);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		inferredDistributionEM.truncate();
		predictionEM.truncate();
		predictionPolicyEM.truncate();
		distributionEM.truncate();
		modelFactoryEM.truncate();
		
		insertDistributions();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	protected void tearDown() throws Exception {
		inferredDistributionEM.truncate();
		predictionEM.truncate();
		predictionPolicyEM.truncate();
		distributionEM.truncate();
		modelFactoryEM.truncate();
	}
	
	@Test
	public void testUpdateModels() throws IOException, SAXException {
		UMontrealDistributionCalculator modelCalculator = new UMontrealDistributionCalculator(UMontrealDistributionCalculator.class.getName());
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		double[] dataPoints = getSamples(50000);
		
		predictionPolicyEM.insert(predictionPolicy);
		
		ArrayList<Model> models = modelCalculator.getModels(predictionPolicy, dataPoints);
		
		modelCalculator.updateModels(predictionPolicy, predictionPolicy.getPredictionTarget().getVariable(), dataPoints);
		
		Assert.assertEquals(models.size(), inferredDistributionEM.count(predictionPolicy.getPredictionPolicyId(), predictionPolicy.getPredictionTarget().getVariable().getQoSId()));
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	public void insertDistributions() {
		Class[] distributions = UMontrealDistributionFactory.getSupportedDistributions();
		
		for (int i=0; i<distributions.length; i++) {
			String className = distributions[i].getConstructors()[0].getName();
			
			distributionEM.insert(
					className, // distribution id
					className.substring(className.lastIndexOf(".")+1, className.lastIndexOf("Dist")), // distribution name
					"University of Montreal distribution implementation", // descritpion
					className); // class name
		}
		
		Assert.assertEquals(distributions.length, distributionEM.count());
	}
	
	/**
	 * @param size
	 * @return
	 */
	private double[] getSamples(int size) {
		double[] samples = new double[size];
		
		for (int i=0; i<size; i++) {
			samples[i] = Math.random();
		}
		
		return samples;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
