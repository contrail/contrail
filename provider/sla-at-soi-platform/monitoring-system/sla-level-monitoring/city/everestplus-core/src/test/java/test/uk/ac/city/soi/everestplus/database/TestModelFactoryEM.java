/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;


import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntity;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 16, 2010
 */
public class TestModelFactoryEM {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestModelFactoryEM.class);
	
	private ModelFactoryEntityManager modelFactoryEM;
	
	/**
	 * 
	 */
	public TestModelFactoryEM() {
		modelFactoryEM = (ModelFactoryEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MODEL_FACTORIES);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		modelFactoryEM.truncate();
	}
	
	/**
	 * 
	 */
	@After
	public void tearDown() {
		modelFactoryEM.truncate();
	}

	/**
	 * 
	 */
	@Test
	public void testInsert() {
		modelFactoryEM.insert(UMontrealDistributionFactory.class.getName(), UMontrealDistributionFactory.class.getName());
		
		Assert.assertEquals(1, modelFactoryEM.count());
	}
	
	/**
	 * 
	 */
	@Test
	public void testSelectById() {
		modelFactoryEM.insert(UMontrealDistributionFactory.class.getName(), UMontrealDistributionFactory.class.getName());
		
		ModelFactoryEntity modelFactory = modelFactoryEM.selectById(UMontrealDistributionFactory.class.getName());
		
		Assert.assertEquals(UMontrealDistributionFactory.class.getName(), modelFactory.getModelFactoryId());
		Assert.assertEquals(UMontrealDistributionFactory.class.getName(), modelFactory.getClassName());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
