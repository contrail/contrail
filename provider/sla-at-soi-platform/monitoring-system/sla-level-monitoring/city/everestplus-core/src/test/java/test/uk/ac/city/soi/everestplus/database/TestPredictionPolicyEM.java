/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;


import java.io.FileInputStream;
import java.io.IOException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.ComparisonOperatorType;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.predictor.MTStarPredictor;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 13, 2011
 */
public class TestPredictionPolicyEM extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("everest-plus-core");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	private final Logger logger = Logger.getLogger(getClass());
	
	private final static String PREDICTION_POLICY_FILE_NAME = "PredictionPolicy-MeanBookTime.xml";
	private final static String PREDICTION_POLICY_SCHEMA = "model/prediction-policy.xsd";
	
	private PredictionPolicyEntityManager predictionPolicyEM;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		FrameworkContextManager.initialiseFrameworkContext();
		
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
		
		predictionPolicyEM.truncate();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	@Test
	public void testInsertPredictionPolicy() throws IOException, SAXException {	
		String xmlPredictionPolicy = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_FILE_NAME).getPath());
		String xmlPredictionPolicySchema = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_SCHEMA).getPath());
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(xmlPredictionPolicy, xmlPredictionPolicySchema);
		
		assertEquals(0, predictionPolicyEM.count());
		
		predictionPolicyEM.insert(predictionPolicy);
		
		assertEquals(1, predictionPolicyEM.count());
	}
	
	@Test
	public void testDeletePredictionPolicy() throws IOException, SAXException {
		String xmlPredictionPolicy = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_FILE_NAME).getPath());
		String xmlPredictionPolicySchema = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_SCHEMA).getPath());
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(xmlPredictionPolicy, xmlPredictionPolicySchema);
	
		predictionPolicyEM.insert(predictionPolicy);
		
		assertEquals(1, predictionPolicyEM.count());
		
		predictionPolicyEM.delete(predictionPolicy);
		
		assertEquals(0, predictionPolicyEM.count());
	}
	
	@Test
	public void testSelectPredictionPolicy() throws IOException, SAXException {
		String xmlPredictionPolicy = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_FILE_NAME).getPath());
		String xmlPredictionPolicySchema = loadFile(ClassLoader.getSystemResource(PREDICTION_POLICY_SCHEMA).getPath());
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(xmlPredictionPolicy, xmlPredictionPolicySchema);
	
		predictionPolicyEM.insert(predictionPolicy);
		
		assertEquals(1, predictionPolicyEM.count());
		
		predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicy.getPredictionPolicyId());
		
		assertEquals("mean_book_time", predictionPolicy.getPredictionPolicyId());
		// prediction target
		assertEquals("98989893010", predictionPolicy.getPredictionTarget().getVariable().getSlaId());
		assertEquals("GuarStates", predictionPolicy.getPredictionTarget().getVariable().getAgreementTermId());
		assertEquals("G6", predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId());
		assertEquals("mean_book_time", predictionPolicy.getPredictionTarget().getVariable().getQoSId());
		assertEquals(ComparisonOperatorType.LESS_THAN, predictionPolicy.getPredictionTarget().getComparisonOperator());
		assertEquals(260d, predictionPolicy.getPredictionTarget().getValue());
		// predictor
		assertEquals(MTStarPredictor.class.getName(), predictionPolicy.getPredictor().getPredictorId());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private String loadFile(String fileName) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(fileName);
		byte[] buffer = new byte[fileInputStream.available()];
		fileInputStream.read(buffer);
		
		return new String(buffer);
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
