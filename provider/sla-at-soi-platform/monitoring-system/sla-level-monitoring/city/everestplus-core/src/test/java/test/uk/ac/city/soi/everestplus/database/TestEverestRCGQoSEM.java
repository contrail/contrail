/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;

import java.io.IOException;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 15, 2010
 */
public class TestEverestRCGQoSEM extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	// logger
	private static Logger logger = Logger.getLogger(TestEverestRCGQoSEM.class);
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	
	private QoSEntityManagerInterface qosEM;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		FrameworkContextManager.initialiseFrameworkContext();
		qosEM = (QoSEntityManagerInterface) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.QOSS);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void testSelectLast() throws IOException, SAXException {
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		Double qos = qosEM.selectLast(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId());
		
		logger.info("Last QoS (" + predictionPolicy.getPredictionTarget().getVariable().getQoSId() + ") value : " + qos);
		assertNotNull(qos);
		
		qos = qosEM.selectLast(
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getSlaId(),
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getGuaranteedId(),
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getQoSId());

		logger.info("Last QoS (" + predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getQoSId() + ") value : " + qos);
		assertNotNull(qos);
	}
	
	/**
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void testSelectLastN() throws IOException, SAXException {
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		ArrayList<Double> qoss = qosEM.selectLastN(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
				10);
		
		logger.info("Last QoS (" + predictionPolicy.getPredictionTarget().getVariable().getQoSId() + ") value : " + qoss);
		assertNotNull(qoss);
		
		qoss = qosEM.selectLastN(
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getSlaId(),
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getGuaranteedId(),
				predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getQoSId(),
				10);

		logger.info("Last QoS (" + predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable().get(0).getQoSId() + ") value : " + qoss);
		assertNotNull(qoss);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
