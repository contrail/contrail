/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.fluent;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.database.EntityManagerUtil;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * This class implements the FluentEntityManagerInterface for MySQL fluent db table.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 10 Mar 2009
 */
public class FluentMySQLDatabase extends EntityManagerUtil implements FluentEntityManagerInterface {


    // logger
    private static Logger logger = Logger.getLogger(FluentMySQLDatabase.class);

    private final String INITIATE = "INITIATE";
    private final String TERMINATE = "TERMINATE";

    /**
     * Constructor.
     */
    public FluentMySQLDatabase(Connection connection) {
        super(connection);
    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM EntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#count()
     */
    public int count() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public void delete(Object object) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.String)
     */
    public void delete(String entityId) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#deleteAll()
     */
    public void deleteAll() {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public void insert(Object object) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#select(java.lang.String)
     */
    public Object select(String entityId) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#selectAll()
     */
    public ArrayList<Object> selectAll() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#update(java.lang.Object)
     */
    public void update(Object object) {
        // TODO Auto-generated method stub

    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM FluentEntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#addInitiate(uk.ac.city.soi.everest.monitor.Predicate)
     */
    public void addInitiate(Predicate initiate) {
        insert(initiate, INITIATE);
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#addTerminate(uk.ac.city.soi.everest.monitor.Predicate)
     */
    public void addTerminate(Predicate terminate) {
        insert(terminate, TERMINATE);

    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getAllInitiatedFluents(long,
     *      uk.ac.city.soi.everest.core.Fluent)
     */
    public ArrayList<Predicate> getAllInitiatedFluents(long time, Fluent fluent) {
        ArrayList<Predicate> predicates =
                selectByBeforeTimeAndStatusAndTypeAndName(time, INITIATE, fluent.getFluentName());

        ArrayList<Predicate> returnPredicates = new ArrayList<Predicate>();

        for (Predicate predicate : predicates) {
            if (predicate.getFluent().getArguments().size() == fluent.getArguments().size()) {

                Predicate terminate = getTerminateAfter(predicate.getTime(), predicate.getFluent());

                if (terminate == null) {
                    ArrayList<Variable> fVars = getFluentVariables(predicate.getFluent());
                    ArrayList<Variable> fluentVars = fluent.getArguments();
                    if (containsVariables(fVars, fluentVars)) {
                        returnPredicates.add(predicate);
                    }
                }
            }
        }
        return returnPredicates;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getInitiateBefore(long,
     *      uk.ac.city.soi.everest.core.Fluent)
     */

    public Predicate getInitiateBefore(long time, Fluent fluent) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getInitiates()
     */

    public ArrayList<Predicate> getInitiates() {
        return selectByStatus(INITIATE);
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getInitiatesBefore(long,
     *      uk.ac.city.soi.everest.core.Fluent)
     */

    public ArrayList<Predicate> getInitiatesBefore(long time, Fluent fluent) {
        ArrayList<Predicate> predicates =
                selectByBeforeTimeAndStatusAndTypeAndName(time, INITIATE, fluent.getFluentName());
        ArrayList<Predicate> returnPredicates = new ArrayList<Predicate>();

        for (Predicate predicate : predicates) {
            if (predicate.getFluent().getArguments().size() == fluent.getArguments().size()) {
                returnPredicates.add(predicate);
            }
        }

        return returnPredicates;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#checkForInitiatedFluent(long,
     *      java.lang.String)
     */
    public ArrayList<Predicate> checkForInitiatedFluents(long time, String fluentName) {
        ArrayList<Predicate> predicates =
            selectByAfterTimeAndStatusAndTypeAndName(time, INITIATE, fluentName);    	
        return predicates;
    }
    
    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getTerminateAfter(long,
     *      uk.ac.city.soi.everest.core.Fluent)
     */

    public Predicate getTerminateAfter(long time, Fluent fluent) {
        ArrayList<Predicate> predicates = selectByAfterTimeAndStatus(time, TERMINATE);
        //ArrayList<Predicate> predicates = selectByAfterTimeAndStatusAndTypeAndName(time, TERMINATE, fluent.getFluentName());

        for (Predicate predicate : predicates) {
            //if (predicate.getFluent().getHashValue().equals(fluent.getHashValue())) {
        	if (predicate.getFluent().getFluentName().equals(fluent.getFluentName()) && 
        			(predicate.getFluent().getArguments().size() == fluent.getArguments().size())){
                return predicate;
            }
        }

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getTerminatesAfter(long,
     *      uk.ac.city.soi.everest.core.Fluent)
     */

    public ArrayList<Predicate> getTerminatesAfter(long time, Fluent fluent) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getByName(java.lang.String)
     *      CLARIFICATION: Assuming that there are i initiations for the given fluent in DB and the given fluent is
     *      specified having j arguments, the method returns an array of objects of i length. Each element of the
     *      returned array contains an array of objects of j length. Each element of the latter array contains the
     *      fluent arguments values for the given fluent for a retrieved initiation.
     * 
     * @return A fluent represents a state which contains several variables: F={var1, ..., varn}. A fluent state is
     *         represented as an array of type Object, that is Object[] state = new Object[]{var1, ..., varn}. The
     *         method returns an array of fluent states, that is Object[] states = new Object[]{state1, ..., staten}.
     */
    public Object[] getByName(String fluentName) {
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface#getByQuery(java.lang.String)
     */
    public Object[] getByQuery(String query) {

        return null;
    }

    // ------------------------------------------------------------------------
    // PRIVATE METHODS
    // ------------------------------------------------------------------------

    /**
     * @param predicate
     * @param status
     *            can be INITIATE or TERMINATE
     */
    private void insert(Predicate predicate, String status) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            Blob blob = toBlob(predicate);

            String query =
                    "INSERT INTO fluent " + "(fluentId, name, timestamp, status, predicateObject) "
                            + "VALUES (?, ?, ?, ?, ?)";

            pstmt = connection.prepareStatement(query);

            pstmt.setString(1, predicate.getFluent().getHashCode());
            pstmt.setString(2, predicate.getFluent().getFluentName());
            pstmt.setLong(3, predicate.getTime());
            pstmt.setString(4, status);
            pstmt.setBlob(5, blob);

            pstmt.executeUpdate();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
    }

    /**
     * @param time
     * @param status
     * @return
     */
    private ArrayList<Predicate> selectByAfterTimeAndStatus(long time, String status) {
        ArrayList<Predicate> predicates = new ArrayList<Predicate>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    connection.prepareStatement("SELECT predicateObject " + "FROM fluent "
                            + "WHERE timestamp > ? AND status = ?"
                            + " ORDER BY timestamp ASC");
            pstmt.setLong(1, time);
            pstmt.setString(2, status);
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("predicateObject");
                    Predicate template = (Predicate) toObject(blob);
                    predicates.add(template);
                }
                while (resultSet.next());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return predicates;
    }

    /**
     * @param time
     * @param status
     * @param type
     * @param name
     * @return
     */
    private ArrayList<Predicate> selectByBeforeTimeAndStatusAndTypeAndName(long time, String status, String name) {
        ArrayList<Predicate> predicates = new ArrayList<Predicate>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    connection.prepareStatement("SELECT predicateObject " + "FROM fluent "
                            + "WHERE timestamp <= ? AND status = ? AND name = ?"
                            + " ORDER BY timestamp ASC");
            pstmt.setLong(1, time);
            pstmt.setString(2, status);
            pstmt.setString(3, name);
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("predicateObject");
                    Predicate template = (Predicate) toObject(blob);
                    predicates.add(template);
                }
                while (resultSet.next());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return predicates;
    }

    
    /**
     * @param time
     * @param status
     * @param type
     * @param name
     * @return
     */
    private ArrayList<Predicate> selectByAfterTimeAndStatusAndTypeAndName(long time, String status, String name) {
        ArrayList<Predicate> predicates = new ArrayList<Predicate>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    connection.prepareStatement("SELECT predicateObject " + "FROM fluent "
                            + "WHERE timestamp >= ? AND status = ? AND name = ?"
                            + " ORDER BY timestamp ASC");
            pstmt.setLong(1, time);
            pstmt.setString(2, status);
            pstmt.setString(3, name);
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("predicateObject");
                    Predicate template = (Predicate) toObject(blob);
                    predicates.add(template);
                }
                while (resultSet.next());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return predicates;
    }
    
    /**
     * @param status
     * @return
     */
    private ArrayList<Predicate> selectByStatus(String status) {
        ArrayList<Predicate> predicates = new ArrayList<Predicate>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = connection.prepareStatement("SELECT predicateObject " + "FROM fluent " + "WHERE status = ?");
            pstmt.setString(1, status);
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("predicateObject");
                    Predicate template = (Predicate) toObject(blob);
                    predicates.add(template);
                }
                while (resultSet.next());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return predicates;
    }

    /**
     * Auxiliary method used in the search for stored fluents. Gets the variables of a given fluent.
     * 
     * @author K.Mahbub
     */
    private ArrayList<Variable> getFluentVariables(Fluent fluent) {
        ArrayList<Variable> vars = new ArrayList<Variable>();

        for (int i = 0; i < fluent.getArguments().size(); i++) {
            Object obj = fluent.getArguments().get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                vars.add(var);
            }
            else if (obj instanceof Function) {
                Function func = (Function) obj;
                vars.addAll(func.getAllVariables());
            }
        }

        return vars;
    }

    /**
     * Auxiliary method used in the search for stored fluents. Compares whether a list of variable vars are contained in
     * a list of variables fvars. What is checked is the type of each variable and its objValue.
     * 
     * @author t7t
     */
    private boolean containsVariables(ArrayList<Variable> fVars, ArrayList<Variable> vars) {
        boolean allFound = true;

        for (int i = 0; i < vars.size() && allFound; i++) {
            Variable var = (Variable) vars.get(i);
            boolean found = false;
            for (int j = 0; j < fVars.size() && !found; j++) {
                Variable fVar = (Variable) fVars.get(j);
                if (var.getType().equals(fVar.getType())) {
                    if (!var.getType().contains(Constants.ARRAY)) {
                        if (!var.getObjValue().equals("")) {
                            if (var.getObjValue().equals(fVar.getObjValue())) {
                                found = true;
                            }
                            else
                                found = false;
                        }
                        else {
                            found = true;
                        }
                    }
                    else {
                        if (var.getValues().size() > 0) {
                            if (var.getValues().size() == fVar.getValues().size()) {
                                boolean allValuesFound = true;
                                for (int k = 0; k < var.getValues().size(); k++) {
                                    Object objVar = var.getValues().get(k);
                                    Object objFVar = fVar.getValues().get(k);
                                    if (!objVar.equals(objFVar)) {
                                        allValuesFound = false;
                                        break;
                                    }
                                }
                                if (allValuesFound) {
                                    found = true;
                                }
                                else {
                                    found = false;
                                }
                            }
                            else {
                                found = false;
                            }
                        }
                        else {
                            found = true;
                        }
                    }
                }

            }
            allFound = allFound && found;
        }
        return allFound;
    }
}
