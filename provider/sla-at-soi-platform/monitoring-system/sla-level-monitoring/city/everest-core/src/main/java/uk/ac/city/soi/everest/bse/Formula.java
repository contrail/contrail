/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Formula.java
 *
 * Created on 03 January 2005, 13:28
 */

package uk.ac.city.soi.everest.bse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.monitor.ConstrainedPredicatesSubformula;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * Java object representation of a monitoring formula.
 * 
 * @author K.Mahbub
 * 
 */
public class Formula implements Serializable {

    private String formulaId;
    private String type;
    private ArrayList body;
    private ArrayList head;
    private boolean forChecking;
    private boolean diagnosisRequired;
    private boolean threatDetectionRequired;

    /**
     * List of constrained predicated subformulas.
     */
    private ArrayList<ConstrainedPredicatesSubformula> constrainedPredicatesSubformulasList =
            new ArrayList<ConstrainedPredicatesSubformula>();

    /**
     * Constructor. This creates an empty formula with only formula ID. Predicates should be added to head and body.
     * 
     * @param formulaId -
     *            ID of the formula
     */
    /*
    public Formula(String formulaId) {
        this.formulaId = new String(formulaId);
        this.type = new String(Constants.TYPE_FUTURE);
        body = new ArrayList();
        head = new ArrayList();
        forChecking = true;
        diagnosisRequired = false;
        threatDetectionRequired = false;
        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
    }
	*/
    /**
     * Constructor. This creates an empty formula. Predicates should be added to te head and body. Also ID should be
     * assigned.
     */
    public Formula() {
        this.formulaId = "";
        this.type = new String(Constants.TYPE_FUTURE);
        body = new ArrayList();
        head = new ArrayList();
        forChecking = true;
        diagnosisRequired = false;
        threatDetectionRequired = false;
        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
    }

    /**
     * Constructor. Creates a formula with a given ID, a list of predicates as body and a list of predicates as head.
     * 
     * @param id -
     *            ID of the formula
     * @param body -
     *            List of predicates as body
     * @param head -
     *            list of predicates as head
     * @param subformulasList -
     *            list of constrained predicates sub-formulas
     */
    public Formula(String id, ArrayList body, ArrayList head, ArrayList<ConstrainedPredicatesSubformula> subformulasList) {
        this.formulaId = new String(id);
        this.type = new String(Constants.TYPE_FUTURE);
        this.body = new ArrayList();
        this.body.addAll(body);
        this.head = new ArrayList();
        this.head.addAll(head);
        forChecking = true;
        diagnosisRequired = false;
        threatDetectionRequired = false;
        this.setConstrainedPredicatesSubformulasList(subformulasList);
    }

    /**
     * Copy constructor.
     * 
     * @param formula -
     *            Formula that should be copied
     */
    public Formula(Formula formula) {
        this.formulaId = new String(formula.getFormulaId());
        this.type = new String(formula.getFormulaType());
        this.forChecking = formula.isForChecking();
        this.diagnosisRequired = formula.isDiagnosisRequired();
        this.threatDetectionRequired = formula.isThreatDetectionRequired();
        body = new ArrayList();
        for (int i = 0; i < formula.getBody().size(); i++) {
            Object obj = (Object) formula.getBody().get(i);
            if (obj instanceof Predicate)
                body.add(new Predicate((Predicate) obj));
            else if (obj instanceof TimePredicate)
                body.add((TimePredicate) obj);
        }
        head = new ArrayList();
        for (int i = 0; i < formula.getHead().size(); i++) {
            Object obj = formula.getHead().get(i);
            if (obj instanceof Predicate)
                head.add(new Predicate((Predicate) obj));
            else if (obj instanceof TimePredicate)
                head.add((TimePredicate) obj);
        }
        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
        for (ConstrainedPredicatesSubformula subformula : formula.getConstrainedPredicatesSubformulasList()) {
            constrainedPredicatesSubformulasList.add(subformula);
        }
    }

    /**
     * Sets the formula ID.
     * 
     * @param formulaId
     */
    public void setFormulaId(String formulaId) {
        this.formulaId = new String(formulaId);
    }

    /**
     * Sets formula type, e.g. Constants.TYPE_FUTURE or Constants.TYPE_PAST.
     * 
     * @param type
     */
    public void setFormulaType(String type) {
        this.type = new String(type);
    }

    /**
     * Sets the diagnosisNeeded flag for this formula.
     * 
     * @param val
     */
    public void setDiagnosisRequired(boolean val) {
        diagnosisRequired = val;
    }

    /**
     * Sets if the the threatDetectionNeeded flag for this formula.
     * 
     * @param val
     */
    public void setThreatDetectionRequired(boolean val) {
        threatDetectionRequired = val;
    }

    /**
     * Sets if the formula for checking or not.
     * 
     * @param val
     */
    public void setForChecking(boolean val) {
        forChecking = val;
    }

    /**
     * Adds a list of predicates to the body of the formula.
     * 
     * @param al -
     *            list of predicates
     */
    public void addAllToBody(ArrayList al) {
        body.addAll(al);
    }

    /**
     * Adds a list of predicates to the head of the formula.
     * 
     * @param al -
     *            list of predicates
     */
    public void addAllToHead(ArrayList al) {
        head.addAll(al);
    }

    /**
     * Adds a predicates to the body of the formula.
     * 
     * @param pred -
     *            a predicate
     */
    public void addPredicateToBody(Predicate pred) {
        body.add(pred);
    }

    /**
     * Adds a predicates to the head of the formula.
     * 
     * @param pred -
     *            a predicate
     */
    public void addPredicateToHead(Predicate pred) {
        head.add(pred);
    }

    /**
     * Returns formula ID.
     * 
     * @return
     */
    public String getFormulaId() {
        return formulaId;
    }

    /**
     * Returns formula type.
     * 
     * @return
     */
    public String getFormulaType() {
        return type;
    }

    /**
     * Returns all the predicates in the body as a list.
     * 
     * @return
     */
    public ArrayList getBody() {
        return body;
    }

    /**
     * Returns all the predicates in the body as a list.
     * 
     * @return
     */
    public ArrayList getHead() {
        return head;
    }

    /**
     * Returns true if teh formula is for checking other wise returns false.
     * 
     * @return
     */
    public boolean isForChecking() {
        return forChecking;
    }

    /**
     * Returns the value of the diagnosisNeeded flag for this formula.
     * 
     * @return
     */
    public boolean isDiagnosisRequired() {
        return diagnosisRequired;
    }

    /**
     * Returns the value of the threatDetectionNeeded flag for this formula.
     * 
     * @return
     */
    public boolean isThreatDetectionRequired() {
        return threatDetectionRequired;
    }

    /**
     * It returns the list of constrained Predicate sub-formulas.
     * 
     * @return the constrainedPredicatesSubformulasList
     */
    public ArrayList<ConstrainedPredicatesSubformula> getConstrainedPredicatesSubformulasList() {
        return constrainedPredicatesSubformulasList;
    }

    /**
     * It sets the list of constrained Predicate sub-formulas.
     * 
     * @param constrainedPredicatesSubformulasList
     *            the constrainedPredicatesSubformulasList to set
     */
    public void setConstrainedPredicatesSubformulasList(
            ArrayList<ConstrainedPredicatesSubformula> constrainedPredicatesSubformulasList) {
        this.constrainedPredicatesSubformulasList = constrainedPredicatesSubformulasList;
    }

    /**
     * Adds a constrained Predicate sub-formula to the list of constrained Predicate sub-formulas.
     * 
     * @param constrainedPredicatesSubformula
     */
    public void addConstrainedPredicatesSubformula(ConstrainedPredicatesSubformula subformula) {
        this.constrainedPredicatesSubformulasList.add(subformula);
    }

    /**
     * Returns string representation of the formula.
     * 
     * @param withPID -
     *            if true partner ID is added to the string representation
     * @param withQuant -
     *            if true quantifiers are added to the string representation
     */
    
    /*
    public String toString(boolean withPID, boolean withQuant) {
        String quants = "";
        String val = "";
        String existQuants = "";
        String forallQuants = "";
        HashSet timeVars = new HashSet();
        ArrayList tempList = body;
        for (int k = 0; k < 2; k++) {
            if (k == 1) {
                tempList = head;
                if (body.size() > 0 && head.size() > 0)
                    val += "\n==>\n";
            }
            for (int i = 0; i < tempList.size(); i++) {
                Object obj = tempList.get(i);
                if (obj instanceof Predicate) {
                    Predicate pred = (Predicate) obj;
                    if (i > 0)
                        val += "\n";
                    if (withQuant) {
                        if (!timeVars.contains(pred.getTimeVarName())) {
                            if (pred.getQuantifier().equals(Constants.QUANT_EXIST))
                                existQuants += ", " + pred.getTimeVarName();
                            else if (pred.getQuantifier().equals(Constants.QUANT_FORALL))
                                forallQuants += ", " + pred.getTimeVarName();
                            timeVars.add(pred.getTimeVarName());
                        }
                    }
                    val += ((Predicate) obj).toString(withPID);
                }
                else if (obj instanceof TimePredicate)
                    val += ((TimePredicate) obj).toString();
                if (i < tempList.size() - 1)
                    val += " ^ ";
            }
        }
        if (withQuant) {
            if (!forallQuants.equals(""))
                quants += "forall " + forallQuants.substring(1) + " : time \n";
            if (!existQuants.equals(""))
                quants += "exists " + existQuants.substring(1) + " : time \n";
        }
        return quants + val;
    }
    */

    /**
     * Returns the string representation of the body of the formula.
     * 
     * @param withPID -
     *            if true partner ID is added to the string representation
     * @return
     */
    
    /*
    public String toStringBody(boolean withPID) {
        // boolean quant = false;
        HashSet timeVars = new HashSet();
        String val = "";
        for (int i = 0; i < body.size(); i++) {
            Object obj = body.get(i);
            if (obj instanceof Predicate) {
                Predicate pred = (Predicate) obj;
                if (i > 0)
                    val += "\n";
                val += ((Predicate) obj).toString(withPID);
            }
            else if (obj instanceof TimePredicate)
                val += ((TimePredicate) obj).toString();
            if (i < body.size() - 1)
                val += " ^ ";
        }
        return val;
    }
	*/
    /**
     * Returns the string representation of the head of the formula.
     * 
     * @param withPID -
     *            if true partner ID is added to the string representation
     * @return
     */
    
    /*
    public String toStringHead(boolean withPID) {
        String val = "";
        for (int i = 0; i < head.size(); i++) {
            Object obj = head.get(i);
            if (obj instanceof Predicate) {
                if (i > 0)
                    val += "\n";
                val += ((Predicate) obj).toString(withPID);
            }
            else if (obj instanceof TimePredicate)
                val += ((TimePredicate) obj).toString();
            if (i < head.size() - 1)
                val += " ^ ";
        }
        return val;
    }
    */
}
