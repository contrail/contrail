/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.monitor;

import java.util.HashMap;

/**
 * This class is used to hold a list of event source (channel) name and their clock values. This was used for the clock
 * synchronisation problem in Serenity, when events may arrive from multiple event sources (channels).
 */
public class ClockMap {

    /**
     * The instance of this field will be shared by any instance of this class.
     */
    private static HashMap<String, String> sourceClockMap = null;

    /**
     * Create a singleton instance of this class.
     */
    public ClockMap() {
        if (sourceClockMap == null) {
            sourceClockMap = new HashMap<String, String>();
        }
    }

    /**
     * Adds the clock time for a given event source (channel).
     * 
     * @param src -
     *            identifier of the event source (channel)
     * @param time -
     *            time of the source
     */
    public void addClockTime(String src, long time) {
        sourceClockMap.put(src, String.valueOf(time));
    }

    /**
     * Returns the latest time of an event source (channel).
     * 
     * @param src -
     *            identifier of the event source (channel)
     * @return
     */
    public long getLatestTime(String src) {
        return Long.parseLong(sourceClockMap.get(src));
    }

    /**
     * Returns true if an entry for the given event source identifier is already stored.
     * 
     * @param src -
     *            identifier of the event source (channel)
     * @return
     */
    public boolean containsSource(String src) {
        return sourceClockMap.containsKey(src);
    }
}
