/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * SeCSEConChecker.java
 *
 * Created on 23 September 2005, 15:22
 */
package uk.ac.city.soi.everest.monitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Variable;

/**
 * This class sets the monitoring decision in the templates.
 * 
 * @author am697
 */
public class SeCSEConChecker extends Observable implements Observer {
    // logger
    private static Logger logger = Logger.getLogger(SeCSEConChecker.class);

    private boolean goOn;
    private ArrayList<TemplateInfo> tInfos;

    /**
     * Creates a new instance of SimpleConChecker.
     */
    public SeCSEConChecker() {
        tInfos = new ArrayList<TemplateInfo>();
    }

    /**
     * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
     */
    public void update(Observable o, Object arg) {
        //tInfos.add((TemplateInfo) arg);
        //mainLoop();
    }

    /**
     * Receives a template instance and the current monitoring time wrapped in TemplateInfo object, then call the main
     * loop to make a decision.
     * 
     * @param tInfo
     */
    public void update(TemplateInfo tInfo) {
        tInfos.add(tInfo);
        mainLoop();
    }

    /**
     * This is the method that implements the algorithm consistency checker.
     */
    private void mainLoop() {
        logger.debug("$$$$$$$$$$$$ ConChecker");
        // while(goOn){
        if (tInfos.size() > 0) {
            TemplateInfo tInfo = tInfos.get(0);
            Template template = tInfo.getTemplate();
            logger.debug("CC template id: " + template.getFormulaId());
            logger.debug("CC body preds:" + template.toStringBody(true));
            logger.debug("CC head preds:" + template.toStringHead(true));
            //for (int hp = template.bodySize(); hp < template.totalPredicates(); hp++)
                //logger.debug(template.getPredicate(hp).toString(true));
            logger.debug("body is true: " + template.isBodyTrue());
            logger.debug("head is true: " + template.isHeadTrue());
            logger.debug("body is false: " + template.isBodyFalse());
            logger.debug("head is false: " + template.isHeadFalse());
//            Set set = template.getUnifiers().entrySet();
//            Iterator i = set.iterator();
//            while (i.hasNext()) {
//                Map.Entry me = (Map.Entry) i.next();
//                logger.debug(me.getKey() + " : " + ((Variable) me.getValue()).toString(true));
//            }
            tInfos.remove(0);
            template.setDST(System.currentTimeMillis());
            logger.debug("$$$ temp noUnknowTruthValue: " + template.noUnKnownTruthValue());
            if (template.isForChecking() && template.noUnKnownTruthValue()) {
                if (!template.isBodyFalse() && !template.isHeadFalse()) {
                    template.setStatus(Constants.STATUS_SAT);
                    logger.debug("$$$$$$$$$$$$ test S_SAT");
                }
                else if (template.isHeadFalse() && !template.isBodyFalse() && template.noSourceDE()) {
                    template.setStatus(Constants.STATUS_IRB);
                    logger.debug("$$$$$$$$$$$$ test S_IRB");

                }
                /*
                else if (template.isHeadFalse() && !template.isBodyFalse() && !template.noSourceDE()) {
                    template.setStatus(Constants.STATUS_IEB);
                    logger.debug("$$$$$$$$$$$$ test S_IEB");
                }
               
                else if (!template.isHeadFalse() && template.bodyFalseByDerived()) {
                    template.setStatus(Constants.STATUS_UB);
                    logger.debug("$$$$$$$$$$$$ test S_UB");
                }
                
                else if (template.isHeadFalse() && !template.isBodyFalse() && template.hasPHappens()) {
                    template.setStatus(Constants.STATUS_PIEB);
                    logger.debug("$$$$$$$$$$$$ test S_PIEB");
                }
                else if (!template.isHeadFalse() && template.bodyFalseByDerived() && template.hasPHappens()) {
                    template.setStatus(Constants.STATUS_PUB);
                    logger.debug("$$$$$$$$$$$$ test S_PIEB");
                }
                */

                template.setDET(System.currentTimeMillis());
                template.setMCT(System.currentTimeMillis());

            }
        }
    }

}
