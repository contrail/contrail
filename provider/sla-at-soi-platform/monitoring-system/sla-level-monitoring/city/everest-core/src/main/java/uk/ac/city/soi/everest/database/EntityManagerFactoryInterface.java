/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database;

import uk.ac.city.soi.everest.database.event.EventEntityManagerInterface;
import uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface;
import uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface;

/**
 * Interface that provides methods for retrieving entity manager interfaces.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 13 May 2009
 */
public interface EntityManagerFactoryInterface {

    /**
     * Returns an instance of the given entity manager class.
     * 
     * @param entityManagerClass
     * @return
     */
    public EntityManagerInterface getEntityManager(Class entityManagerClass);

    /**
     * Returns an instance of the EventEntityManagerInterface.
     * 
     * @return
     */
    public EventEntityManagerInterface getEventEntityManager();

    /**
     * Returns an instance of the FluentEntityManagerInterface.
     * 
     * @return
     */
    public FluentEntityManagerInterface getFluentEntityManager();

    /**
     * Returns an instance of the TemplateEntityManagerInterface.
     * 
     * @return
     */
    public TemplateEntityManagerInterface getTemplateEntityManager();

}
