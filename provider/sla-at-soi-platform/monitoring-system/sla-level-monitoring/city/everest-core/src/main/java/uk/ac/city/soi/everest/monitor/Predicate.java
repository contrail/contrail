/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Predicate.java
 *
 * Created on 30 May 2004, 17:59
 */
package uk.ac.city.soi.everest.monitor;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Signature;
import uk.ac.city.soi.everest.core.TimeExpression;
import uk.ac.city.soi.everest.core.TimeVar;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.er.LogEvent;

/**
 * This class models a predicate in a formula (Template). This class extends the Signature class.
 * 
 * @author Khaled Mahbub
 */
public class Predicate extends Signature implements Cloneable, Serializable {
    // logger
    private static Logger logger = Logger.getLogger(Predicate.class);

    /**
     * a time variable that will store the time point when this predicate is unified with a run time event.
     */
    private TimeVar timeStamp;

    /**
     * it is used only in abduction and holds the lower bound of the predicate.
     */
    private long time1;

    /**
     * In deduction,this is to hold time for pHappens predicate. In abduction, it holds the upper bound of the
     * predicate.
     */
    private long time2;

    /**
     * used only for abduction; it holds the formula Ids that used for abduction to abduce the event that the predicate
     * has been unified with.
     */
    private ArrayList<String> predAbdPath = new ArrayList<String>();

    /**
     * used only for abduction; it holds the formula Ids that were used for deducing the predicate.
     */
    private ArrayList<String> predDedPath = new ArrayList<String>();

    /**
     * Stores the ID of the event that has been unified with this predicate.
     */
    private String ID;
    // private TimeRange range;
    /**
     * Lower bound time expression.
     */
    private TimeExpression lB;
    /**
     * Upper bound time expression.
     */
    private TimeExpression uB;
    /**
     * Stores the truth value of the predicate.
     */
    private String truthVal;
    /**
     * Stores the quantifier of the time variable of this predicate.
     */
    private String quantifier;
    /**
     * Stores the source of the event (e.g. recorded event or derived event) once truth value of the predicate is set.
     */
    private String source;
    /**
     * set to true if teh predicate is negated.
     */
    private boolean negated;
    /**
     * set to true if pHappens condition is true for this predicate.
     */
    private boolean pHappens;

    private boolean recordable;

    private boolean abducible;

    private boolean confirmed;

    private double minLikelihood = Double.NaN;

    private double maxLikelihood = Double.NaN;

    /**
     * set to true if the time variable of this predicate is unconstrained.
     */
    private boolean unConstrained;

    private HashMap tempMgu = new HashMap();

    private long EPUT;

    /**
     * Creates a new instance of Predicate. The same constructor will be used to create relational predicate. In that
     * case ecName will be equal to "RELATION_PRED", opnName will hold the relation type (e.g. lessThan, GreaterThan
     * etc.) and prefix will hold the type of operands (e.g. VV for Var vs Var, VC for Var vs Cons).
     */
    public Predicate(String ecName, String prefix, String partnerId, String opnName, String tVarName) {
        super(ecName, prefix, partnerId, opnName);
        timeStamp = new TimeVar(tVarName);
        time1 = Constants.TIME_UD;
        time2 = Constants.TIME_UD;
        uB = new TimeExpression();
        uB.setTimeValue(Constants.RANGE_UB);
        lB = new TimeExpression();
        lB.setTimeValue(Constants.RANGE_LB);
        negated = false;
        pHappens = false;
        unConstrained = false;
        predAbdPath = null;
        predDedPath = null;
        truthVal = Constants.TRUTH_VAL_UK;
        quantifier = Constants.QUANT_EXIST;
        source = Constants.SOURCE_UD;
        ID = "";
        EPUT = Constants.TIME_UD;
        confirmed = true;
        minLikelihood = 0;
        maxLikelihood = 0;
    }

    /**
     * Copy constructor.
     * 
     * @param pred
     */
    public Predicate(Predicate pred) {
        super(pred.getEcName(), pred.getPrefix(), pred.getPartnerId(), pred.getOperationName());
        this.timeStamp = new TimeVar(pred.getTimeVar());
        this.time1 = pred.getTime1();
        this.time2 = pred.getTime2();
        this.negated = pred.isNegated();
        this.pHappens = pred.getPHappens();
        this.unConstrained = pred.isUnconstrained();
        this.ID = new String(pred.getID());
        this.truthVal = new String(pred.getTruthVal());
        this.quantifier = new String(pred.getQuantifier());
        this.source = new String(pred.getSource());
        this.uB = new TimeExpression(pred.getUpBound());
        this.lB = new TimeExpression(pred.getLowBound());
        for (int i = 0; i < pred.getVariables().size(); i++) {
            Object obj = pred.getVariables().get(i);
            if (obj instanceof Function)
                this.variables.add(new Function((Function) obj));
            else
                this.variables.add(((Variable) obj).makeCopy());
        }
        if (pred.getFluent() != null)
            setFluent(pred.getFluent());
        EPUT = pred.getEPUT();
        this.predAbdPath = pred.getPredAbdPath();
        this.predDedPath = pred.getPredDedPath();
        this.recordable = pred.isRecordable();
        this.abducible = pred.isAbducible();
        this.confirmed = pred.isConfirmed();
        this.minLikelihood = pred.getMinLikelihood();
        this.maxLikelihood = pred.getMaxLikelihood();

    }

    /**
     * Sets the upper bound of the time variable.
     * 
     * @param tex
     */
    public void setUpBound(TimeExpression tex) {
        uB = new TimeExpression(tex);
    }

    /**
     * Sets the lower bound of the time variable.
     * 
     * @param tex
     */
    public void setLowBound(TimeExpression tex) {
        lB = new TimeExpression(tex);
    }

    /**
     * Sets the truth value of the predicate.
     * 
     * @param truthVal
     */
    public void setTruthVal(String truthVal) {
        this.truthVal = new String(truthVal);
    }

    /**
     * sets the source of the event.
     * 
     * @param src
     */
    public void setSource(String src) {
        this.source = new String(src);
    }

    /**
     * sets the value of the pHappens condition.
     * 
     * @param val
     */
    public void setPHappens(boolean val) {
        this.pHappens = val;
    }

    /**
     * sets the ID of the event that unifies with this predicate.
     * 
     * @param ID -
     */
    public void setID(String ID) {
        this.ID = new String(ID);
    }

    /**
     * sets time1.
     * 
     * @param time1
     */
    /*
    public void setTime1(long time1) {
        this.time1 = time1;
    }
	*/
    /**
     * sets time2.
     * 
     * @param time2
     */
    /*
    public void setTime2(long time2) {
        this.time2 = time2;
    }
	*/
    /**
     * Sets EPUT.
     * 
     * @param EPUT
     */
    /*
    public void setEPUT(long EPUT) {
        this.EPUT = EPUT;
    }
	*/
    /**
     * Sets quantifier of the time variable of this predicate.
     * 
     * @param quantifier
     */
    public void setQuantifier(String quantifier) {
        this.quantifier = new String(quantifier);
    }

    /**
     * Sets the name of the time variable involved in the upper bound expression.
     * 
     * @param name
     */
    public void setUBName(String name) {
        uB.setTimeVarName(name);
    }

    /**
     * Sets the name of the time variable involved in the lower bound expression.
     * 
     * @param name
     */
    public void setLBName(String name) {
        lB.setTimeVarName(name);
    }

    /**
     * Sets the number involved in the upper bound expression.
     * 
     * @param name
     */
    public void setUBNumber(long num) {
        uB.setNumber(num);
    }

    /**
     * Sets the number involved in the lower bound expression.
     * 
     * @param name
     */
    public void setLBNumber(long num) {
        lB.setNumber(num);
    }

    /**
     * sets the value of the time variable.
     * 
     * @param tVal
     */
    public void setTime(long tVal) {
        timeStamp.setValue(tVal);
    }

    /**
     * sets the name of the time variable.
     * 
     * @param tVal
     */
    public void setTimeVarName(String name) {
        timeStamp.setName(name);
    }

    /**
     * sets the value of the negated field.
     * 
     * @param val
     */
    public void setNegated(boolean val) {
        negated = val;
    }

    /**
     * sets the value of the abducible field.
     * 
     * @param val
     */
    public void setAbducible(boolean val) {
        abducible = val;
    }

    /**
     * sets the value of the confirmed field.
     * 
     * @param val
     */
    /*
    public void setConfirmed(boolean val) {
        // confirmed = val;
        if (minLikelihood >= 1 - maxLikelihood)
            confirmed = true;
        else
            confirmed = false;
    }
	*/
    /**
     * sets the value of the minLikelihood field.
     * 
     * @param val
     */
    /*
    public void setMinLikelihood(double val) {
        minLikelihood = val;
    }
	*/
    /**
     * sets the value of the maxLikelihood field.
     * 
     * @param val
     */
    /*
    public void setMaxLikelihood(double val) {
        maxLikelihood = val;
    }
	*/
    /**
     * sets the value of the recordable field.
     * 
     * @param val
     */
    public void setRecordable(boolean val) {
        recordable = val;
    }

    /**
     * sets the value of the unconstrained field.
     * 
     * @param val
     */
    public void setUnconstrained(boolean val) {
        unConstrained = val;
    }

    /**
     * Returns the quantifier of the time variable.
     * 
     * @return
     */
    public String getQuantifier() {
        return quantifier;
    }

    /**
     * Returns the truth value of the predicate.
     * 
     * @return
     */
    public String getTruthVal() {
        return truthVal;
    }

    /**
     * Returns the ID of event that was unified with this predicate.
     * 
     * @return
     */
    public String getID() {
        return ID;
    }

    /**
     * Returns the value of the time variable.
     * 
     * @return
     */
    public long getTime() {
        return timeStamp.getValue();
    }

    /**
     * Returns EPUT.
     * 
     * @return
     */
    public long getEPUT() {
        return EPUT;
    }

    /**
     * Returns the time variable.
     * 
     * @return
     */
    public TimeVar getTimeVar() {
        return timeStamp;
    }

    /**
     * Returns the name of the time variable.
     * 
     * @return
     */
    public String getTimeVarName() {
        return timeStamp.getName();
    }

    /**
     * Returns the name of the time variable involved in the upper bound expression.
     * 
     * @return
     */
    public String getUBName() {
        return uB.getTimeVarName();
    }

    /**
     * Returns the name of the time variable involved in the lower bound expression.
     * 
     * @return
     */
    public String getLBName() {
        return lB.getTimeVarName();
    }

    /**
     * Returns the number involved in the lower bound expression.
     * 
     * @return
     */
    public long getLBNumber() {
        return lB.getNumber();
    }

    /**
     * Checks whether the predicate is negated.
     * 
     * @return
     */
    public boolean isNegated() {
        return negated;
    }

    /**
     * Checks whether the predicate is abducible.
     * 
     * @return
     */
    
    public boolean isAbducible() {
        return abducible;
    }

    /**
     * Checks whether the predicate is confirmed.
     * 
     * @return
     */
    public boolean isConfirmed() {
        if (minLikelihood > 1 - maxLikelihood)
            return true;
        else
            return false;
    }

    /**
     * Checks whether the predicate is recordable.
     * 
     * @return
     */
    public boolean isRecordable() {
        return recordable;
    }

    /**
     * Returns the predicate source.
     * 
     * @return
     */
    public String getSource() {
        return source;
    }

    /**
     * Returns the minimum predicate likelihood.
     * 
     * @return
     */
    public double getMinLikelihood() {
        return minLikelihood;
    }

    /**
     * Returns the maximum predicate likelihood.
     * 
     * @return
     */
    public double getMaxLikelihood() {
        return maxLikelihood;
    }

    /**
     * Returns the upper bound time expression.
     * 
     * @return
     */
    public TimeExpression getUpBound() {
        return uB;
    }

    /**
     * Returns the lower bound time expression.
     * 
     * @return
     */
    public TimeExpression getLowBound() {
        return lB;
    }

    /**
     * Returns true if pHappens condition is set for this predicate.
     * 
     * @return
     */
    public boolean getPHappens() {
        return pHappens;
    }

    /**
     * Returns true if a given value is within the time range of this predicate. If the lower bound of this predicate is
     * still not defined then it will return false.
     * 
     * @param tVal
     * @return
     */
    public boolean withinRange(long tVal) {
        boolean val = false;
        // if(isUnconstrained()) val = true; else
        if (lB.getValue() == Constants.RANGE_LB)
            val = false; // lower bound must be set, it is assumed range can be
        // opened in the upper bound but not in the lower
        // bound
        else if (lB.getValue() <= tVal && uB.getValue() >= tVal)
            val = true;
        return val;
    }

    /**
     * Returns true if the predicate is partially instantiated. A predicate is partially instantiated if not all the
     * variables in the predicate are initialized to some value.
     * 
     * @param ufp -
     *            list of variables in the template that have been initialized to some value.
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value.
     * @return
     */
    public boolean isPartial(HashMap ufp, HashMap timeVarMap) {
        boolean val = false;
        // if(prefix.equals("rc") || prefix.equals("as") || prefix.equals("ir"))
        // val = true;
        if (!ecName.equals(Constants.PRED_RELATION)) {
            for (int i = 0; i < variables.size(); i++) {
                Variable var = (Variable) variables.get(i);
                // if(!ufp.containsKey(var.getName() + var.getType())) {val =
                // true; break;}
                if (var.getType().equals(Constants.DATA_TYPE_CONST)/*
                                                                     * || var.getName ( ).startsWith ( "vsource" )
                                                                     */)
                    continue;
                else {
                    //logger.debug("test partial1");
                    if (!var.getType().equals("OpStatus")) {
                        if (!ufp.containsKey(var.getName() + var.getType())) {
                            val = true;
                            break;
                        }
                    }
                    //logger.debug("test partial2");
                }
            }
            if (fluent != null && !val) {
                //logger.debug("test partial3");
                val = fluent.isPartial(ufp, timeVarMap);
                //logger.debug("test partial4");
                /*
                 * //Variable var1 = fluent.getVariable(); //if(var1.getType().endsWith("Array") &&
                 * !var1.getArrayIndexName().equals("") && !ufp.containsKey(var1.getArrayIndexName() +
                 * var1.getArrayIndexType())) val = true; if(!val){
                 * if(fluent.getFluentType().equals(Constants.RELATION_VF)) val = fluent.getFunction().isPartial(ufp,
                 * timeVarMap); else{ Variable var2 = fluent.getVariable(); if(!ufp.containsKey(var2.getName() +
                 * var2.getType())) val = true; } }
                 */
                // else if(!var.getValue().equals("")) val = true;
            }
        }
        else {// checking for relational predicate
            if (variables.size() == 2) {
                if (prefix.equals(Constants.RELATION_VV)) {
                    Variable var1 = (Variable) variables.get(0);
                    Variable var2 = (Variable) variables.get(1);
                    val |=
                            !ufp.containsKey(var1.getName() + var1.getType())
                                    || !ufp.containsKey(var2.getName() + var2.getType());
                }
                else if (prefix.equals(Constants.RELATION_VC)) {
                    Variable var = (Variable) variables.get(0);
                    val |= !ufp.containsKey(var.getName() + var.getType());
                }
                else if (prefix.equals(Constants.RELATION_VF)) {
                    Variable var = (Variable) variables.get(0);
                    Function func = (Function) variables.get(1);
                    val |= !ufp.containsKey(var.getName() + var.getType()) || func.isPartial(ufp, timeVarMap);
                }
                else if (prefix.equals(Constants.RELATION_CV)) {
                    Variable var = (Variable) variables.get(1);
                    val |= !ufp.containsKey(var.getName() + var.getType());
                }
                else if (prefix.equals(Constants.RELATION_CF)) {
                    Function func = (Function) variables.get(1);
                    val |= func.isPartial(ufp, timeVarMap);
                }
                else if (prefix.equals(Constants.RELATION_FV)) {
                    Variable var = (Variable) variables.get(1);
                    Function func = (Function) variables.get(0);
                    val |= !ufp.containsKey(var.getName() + var.getType()) || func.isPartial(ufp, timeVarMap);
                }
                else if (prefix.equals(Constants.RELATION_FC)) {
                    Function func = (Function) variables.get(0);
                    val |= func.isPartial(ufp, timeVarMap);
                }
                else if (prefix.equals(Constants.RELATION_FF)) {
                    Function func1 = (Function) variables.get(0);
                    Function func2 = (Function) variables.get(1);
                    val |= func1.isPartial(ufp, timeVarMap) || func2.isPartial(ufp, timeVarMap);
                }
            }
        }
        return val;
    }

    /**
     * Compares two variables and returns the result of comparison.
     * 
     * @param operator -
     *            comparison operator (e.g. less than, equal to etc.)
     * @param var1 -
     *            first variable
     * @param var2 -
     *            second variable
     * @return
     */
    private String compare(String operator, Variable var1, Variable var2) {
        String val = "";
        String type1 = var1.getRealType();
        String type2 = var2.getRealType();
        String type = Constants.DATA_TYPE_DOUBLE;
        
        if (type2.equals(Constants.DATA_TYPE_CONST)){
            type = new String(type1);
            type2 = new String(type1); 
        }
        else if (type1.equals(Constants.DATA_TYPE_CONST)){
            type = new String(type2);
            type1 = new String(type2);
        }
        else if (var1.getRealType().equals(Constants.DATA_TYPE_STRING)
                && var2.getRealType().equals(Constants.DATA_TYPE_STRING))
            type = Constants.DATA_TYPE_STRING;
        else if (var1.getRealType().equals(Constants.DATA_TYPE_BOOL)
                && var2.getRealType().equals(Constants.DATA_TYPE_BOOL))
            type = Constants.DATA_TYPE_BOOL;
        if ((type1.equals(Constants.DATA_TYPE_INT)
                || type1.equals(Constants.DATA_TYPE_DOUBLE) || type1.equals(Constants.DATA_TYPE_LONG))
                && (type2.equals(Constants.DATA_TYPE_INT)
                        || type2.equals(Constants.DATA_TYPE_DOUBLE) || type2.equals(Constants.DATA_TYPE_LONG)))
            type = Constants.DATA_TYPE_DOUBLE;

        if (type.equals(Constants.DATA_TYPE_STRING) || type.equals(Constants.DATA_TYPE_BOOL)) {
            if (operator.equals(Constants.EQUAL)) {
                if (var1.getRealValue() == null || var2.getRealValue() == null) {
                    val = "false";
                }
                else {
                    // val =
                    // String.valueOf(var1.getRealValue().equals(var2.getRealValue
                    // ()));
                    // val = String.valueOf(var1.equals(var2));
                    val =
                            String.valueOf(Predicate.toString(var1.getRealValue()).equals(
                                    Predicate.toString(var2.getRealValue())));
                }
            }
            else if (operator.equals(Constants.NOT_EQUAL)) {
                if (var1.getRealValue() == null || var2.getRealValue() == null) {
                    val = "false";
                }
                else {
                    // val = String.valueOf(!var1.getRealValue().equals(var2.
                    // getRealValue()));
                    // val = String.valueOf(!var1.equals(var2));
                    val =
                            String.valueOf(!Predicate.toString(var1.getRealValue()).equals(
                                    Predicate.toString(var2.getRealValue())));
                }
            }
        }
        else if (type.equals(Constants.DATA_TYPE_DOUBLE)) {
            try {
                if (operator.equals(Constants.EQUAL))
                    val =
                            String.valueOf(Double.parseDouble(var1.getRealValue().toString()) == Double
                                    .parseDouble(var2.getRealValue().toString()) ? true : false); // parseDouble
                // (
                // var1
                // .
                // getValue
                // (
                // )
                // )
                else if (operator.equals(Constants.NOT_EQUAL))
                    val =
                            String.valueOf(Double.parseDouble(var1.getObject().toString()) != Double.parseDouble(var2
                                    .getObject().toString()) ? true : false);
                else if (operator.equals(Constants.LESS_THAN))
                    val =
                            String.valueOf(Double.parseDouble(var1.getObject().toString()) < Double.parseDouble(var2
                                    .getObject().toString()) ? true : false);
                else if (operator.equals(Constants.LESS_THAN_EQUAL))
                    val =
                            String.valueOf(Double.parseDouble(var1.getObject().toString()) <= Double.parseDouble(var2
                                    .getObject().toString()) ? true : false);
                else if (operator.equals(Constants.GREATER_THAN))
                    val =
                            String.valueOf(Double.parseDouble(var1.getObject().toString()) > Double.parseDouble(var2
                                    .getObject().toString()) ? true : false);
                else if (operator.equals(Constants.GREATER_THAN_EQUAL))
                    val =
                            String.valueOf(Double.parseDouble(var1.getObject().toString()) >= Double.parseDouble(var2
                                    .getObject().toString()) ? true : false);
            }
            catch (Exception e) {
                val = "false";
            }
        }
        else System.out.println("Incompatible data type in comparison operator");
        return val;
    }

    /**
     * Used (i) to evaluate a relational predicate and set the truth value (ii) to execute a function inside a fluent,
     * but doesn't set the truth value of the corresponding Initiates predicate. It updates the value of the variable
     * for which the function is responsible for.
     * 
     * @param values -
     *            list of variables in the template that have been initialized to some value.
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value.
     */
    public void evaluateRelationalPredicates(HashMap values, HashMap timeVarMap) {
        if (ecName.equals(Constants.PRED_RELATION)) {
            String val = "";
            if (prefix.equals(Constants.RELATION_VV)) {
                Variable var1 = ((Variable) variables.get(0)).makeCopy();
                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                if (values.containsKey(var1.getName() + var1.getType())) {
                    var1.setValue(((Variable) values.get(var1.getName() + var1.getType())).getValue());
                }
                if (values.containsKey(var2.getName() + var2.getType())) {
                    var2.setValue(((Variable) values.get(var2.getName() + var2.getType())).getValue());
                }
                if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                    val = compare(opnName, var1, var2);
                }
            }
            else if (prefix.equals(Constants.RELATION_VC)) {            	
                Variable var1 = ((Variable) variables.get(0)).makeCopy();

                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                if (values.containsKey(var1.getName() + var1.getType())) {
                    var1.setValue(((Variable) values.get(var1.getName() + var1.getType())).getValue());
                }
                var2.setType(var1.getRealType());
                if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                    val = compare(opnName, var1, var2);
                }
            }
            else if (prefix.equals(Constants.RELATION_CV)) {
                Variable var1 = ((Variable) variables.get(0)).makeCopy();
                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                if (values.containsKey(var2.getName() + var2.getType())) {
                    var2.setValue(((Variable) values.get(var2.getName() + var2.getType())).getValue());
                }
                var1.setType(var2.getRealType());
                if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                    val = compare(opnName, var1, var2);
                }
            }
            else if (prefix.equals(Constants.RELATION_VF)) {
                Variable var1 = ((Variable) variables.get(0)).makeCopy();
                Function func = (Function) variables.get(1);
                Variable var2 = func.execute(values, timeVarMap);
                if (var2 != null) {
                    if (values.containsKey(var1.getName() + var1.getType())) {
                        var1.setValue(((Variable) values.get(var1.getName() + var1.getType())).getValue());
                    }
                    if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                        val = compare(opnName, var1, var2);
                    }
                }
            }
            else if (prefix.equals(Constants.RELATION_FV)) {
                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                Function func = (Function) variables.get(0);
                Variable var1 = func.execute(values, timeVarMap);
                if (var1 != null) {
                    if (values.containsKey(var2.getName() + var2.getType())) {
                        var2.setValue(((Variable) values.get(var2.getName() + var2.getType())).getValue());
                    }
                    if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                        val = compare(opnName, var1, var2);
                    }
                }
            }
            else if (prefix.equals(Constants.RELATION_FC)) {
                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                Function func = (Function) variables.get(0);
                Variable var1 = func.execute(values, timeVarMap);
                if (var1 != null) {
                    if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                        val = compare(opnName, var1, var2);
                    }
                }
            }
            else if (prefix.equals(Constants.RELATION_CF)) {
                Variable var1 = ((Variable) variables.get(0)).makeCopy();
                Function func = (Function) variables.get(1);
                Variable var2 = func.execute(values, timeVarMap);
                if (var2 != null) {
                    if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                        val = compare(opnName, var1, var2);
                    }
                }
            }
            else if (prefix.equals(Constants.RELATION_FF)) {
                Function func1 = (Function) variables.get(0);
                Function func2 = (Function) variables.get(1);
                Variable var1 = func1.execute(values, timeVarMap);
                Variable var2 = func2.execute(values, timeVarMap);
                if (var1 != null && var2 != null) {
                    if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                        val = compare(opnName, var1, var2);
                    }
                }
            }
            /*
            else if (prefix.equals(Constants.RELATION_CC)) {
                Variable var1 = ((Variable) variables.get(0)).makeCopy();
                Variable var2 = ((Variable) variables.get(1)).makeCopy();
                if (!var1.getValue().equals("NaN") && !var2.getValue().equals("NaN")) {
                    val = compare(opnName, var1, var2);
                }
            }
            */
            if (val.startsWith("t")) {
                if(this.isNegated()) setTruthVal(Constants.TRUTH_VAL_FALSE);
                else setTruthVal(Constants.TRUTH_VAL_TRUE);
                setSource(Constants.SOURCE_EV);
                setID("EV_ID");
            }
            else if (val.startsWith("f")) {
            	if(this.isNegated()) setTruthVal(Constants.TRUTH_VAL_TRUE);
            	else setTruthVal(Constants.TRUTH_VAL_FALSE);
                setSource(Constants.SOURCE_EV);
                setID("EV_ID");
            }
        }
    }

    /**
     * Used (i) to execute a function inside a fluent, but doesn't set the truth value of the corresponding Initiates
     * predicate. It updates the value of the variable in the template for which the function is responsible for.
     * 
     * @param values -
     *            list of variables in the template that have been initialized to some value.
     * @param unifiers -
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value.
     * 
     * ATTENTION: NOT COMPLETELY IMPLEMENTED NOT ACTUALLY USED FROMT THE FEED METHOD OF SERENITY ANALYZER
     */
    public void evaluateInitiatesAndTerminatesPredicates(HashMap values, HashMap unifiers, HashMap timeVarMap) {
    	/*
        boolean evaluationResult = false;

        Function func = fluent.getFunction();
        Variable res = func.execute(values, timeVarMap);
        Variable var2 = fluent.getVariable();
        if (unifiers.containsKey(var2.getName() + var2.getType())) {
            Variable var = (Variable) unifiers.get(var2.getName() + var2.getType());
            if (var2.getType().endsWith("Array")) {
                 //*
                 //* if(values.containsKey(var2.getArrayIndexName() + var2.getArrayIndexType())){ Variable index =
                 //* (Variable)values.get(var2.getArrayIndexName() + var2.getArrayIndexType());
                 //* var.addValueAt(res.getValue(), Double.parseDouble(index.getValue())); }
                 //*
                if (res.getType().endsWith("Array")) {
                    var.getValues().clear();
                    var.addValues(res.getValues());
                }
                else
                    var.addValue(res.getValue().toString());
            }
            else
                var.setValue(res.getValue());
        }
        else {
            Variable var = var2.makeCopy();
            if (var.getType().endsWith("Array")) {
                 //*
                 //* if(values.containsKey(var.getArrayIndexName() + var.getArrayIndexType())){ Variable index =
                 //* (Variable)values.get(var.getArrayIndexName() + var.getArrayIndexType());
                 //* var.addValueAt(res.getValue(), Double.parseDouble(index.getValue())); }
                 //*
                if (res.getType().endsWith("Array")) {
                    var.getValues().clear();
                    var.addValues(res.getValues());
                }
                else
                    var.addValue(res.getValue().toString());
            }
            else
                var.setValue(res.getValue());
            unifiers.put(var.getName() + var.getType(), var);
        }
        */
    }

    /**
     * Returns the value of the lower bound expression.
     */
    public long getRangeLB() {
        return lB.getValue();
    }

    /**
     * Returns the value of the upper bound expression.
     */
    public long getRangeUB() {
        return uB.getValue();
    }

    /**
     * Returns time1.
     * 
     * @return
     */
    public long getTime1() {
        return time1;
    }

    /**
     * Returns time2.
     * 
     * @return
     */
    public long getTime2() {
        return time2;
    }

    /**
     * Returns the abductive path of the predicate that has been unified with an abduced event.
     * 
     * @return
     */
    public ArrayList<String> getPredAbdPath() {
        return predAbdPath;
    }

    /**
     * Returns the deductive path of the predicate that has been unified with an abduced event.
     * 
     * @return
     */
    public ArrayList<String> getPredDedPath() {
        return predDedPath;
    }

    /**
     * Returns true if time variables in both lower bound and upper bound expressions are initialized.
     * 
     * @return
     */
    public boolean isRangeSet() {
        if (lB.getTimeValue() != Constants.RANGE_LB && uB.getTimeValue() != Constants.RANGE_UB) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns true if a given partner ID matches with teh partner ID of this predicate.
     * 
     * @return
     */
    public boolean matchesPartnerID(String partnerID) {
        if (partnerID.equals("") || this.partnerId.equals(partnerID))
            return true;
        else
            return false;
    }

    /**
     * Returns true if the EC predicate name, operation name,partner ID and preifx of a given Link matches with the
     * corresponding fields of thsi predicate.
     * 
     * @param link
     * @return
     */
    /*
    public boolean matchesNames(Link link) {
        if (link.getEcName().equals(ecName) && link.getPrefix().equals(prefix)
                && link.getOperationName().equals(opnName) && matchesPartnerID(link.getPartnerId()))
            return true;
        else
            return false;
    }
	*/
    /**
     * Returns true if the EC predicate name, operation name,partner ID and prefix of a given LogEvent matches with the
     * corresponding fields of this predicate.
     * 
     * @param event
     * @return
     */
    public boolean matchesNames(LogEvent event) {
        if (!event.getEcName().equals("HoldsAt")) {
            if (event.getEcName().equals(ecName) && event.getPrefix().equals(prefix)
                    && event.getOperationName().equals(opnName) && matchesPartnerID(event.getPartnerId())) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (event.getEcName().equals(ecName)) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    /**
     * Returns true if the fluent of this predicate matches with a given fluent. This method is only for fluent
     * signature match, i.e. var name and type (not value).
     * 
     * @param flnt
     * @return
     */
    public boolean matchesFluent(Fluent flnt) {
        boolean val = false;
        if (fluent == null && flnt == null)
            val = true;
        else if (fluent != null && flnt != null && fluent.matches(flnt)) {
            Fluent eFluent = new Fluent(flnt);
            eFluent.getVariable().setName(this.getFluent().getVariable().getName());
            eFluent.getVariable().setConstName(this.getFluent().getVariable().getConstName());
            tempMgu.put(eFluent.getVariable().getName() + eFluent.getVariable().getType(), eFluent.getVariable());
            val = true;
        }
        return val;
    }

    /**
     * Returns true if a list of variables matches with the variables of this predicate. This matching is done based on
     * the types of the variables at the corresponding positions of the two list of variables.
     * 
     * 
     * @param vars
     * @return
     */
    public boolean matchesVars(ArrayList vars) {
        int matchCount = 0;
        // logger.debug("Trying to match event variables...");
        if (vars.size() == variables.size()) {
//            logger.debug("size of the predicate variable list: " + variables.size());
//            logger.debug("size of the event variable list: " + vars.size());
            for (int i = 0; i < variables.size(); i++) {
                Variable var1 = (Variable) variables.get(i);
                Variable var2 = (Variable) vars.get(i);
//                logger.debug("var1: " + var1);
//                logger.debug("var2: " + var2);
                if (var1.matchesType(var2)) {
//                    logger.debug("matchesVars test!!!");
//                    logger.debug("predicate var: " + var1.toString(true));
//                    logger.debug("event var: " + var2.toString(true));
                    matchCount++;
                    var2.setName(var1.getName());
                    var2.setConstName(var1.getConstName());
                    tempMgu.put(var2.getName() + var2.getType(), var2);
                }

            }
        }
        logger.debug("matchCount = " + matchCount);
        if (matchCount == vars.size())
            return true;
        else
            return false;
    }

    /**
     * Returns true if a given ID is same the ID of this predicate.
     * 
     * @param ID
     * @return
     */
    public boolean matchesID(String ID) {
        if (this.ID.equals("") || this.ID.equals("NF_ID") || this.ID.equals("I_ID") || this.ID.equals(ID))
            return true;
        else
            return false;
    }

    /**
     * Returns true if a LogEvent can be matched with this predicate. The matching is done using matchesNames,
     * matchesVars and matchesFluent methods.
     * 
     * @param event
     * @return
     */
    public boolean canBeUnified(LogEvent event) {
        boolean val = false;
        tempMgu.clear();
        if (!event.getEcName().equals("HoldsAt")) {
            if (matchesNames(event) && matchesVars(event.getVariables()) && matchesFluent(event.getFluent())) {
                val = true;
            }
        }
        else {
            if (matchesNames(event) && matchesFluent(event.getFluent())) {
                val = true;
            }
        }
        // logger.debug("Can this event be unified? " +val);

        return val;
    }

    /**
     * Returns true if a mismatch is found during unification. A mismatch is defined as follows if an event E and
     * predicate P can be unified, but one variable in E have different value than the value of the corresponding
     * variable in P. The value of this variable in P might have been assigned due to the unification of other predicate
     * P1.
     * 
     * @param ufp -
     *            list of variables in the template that have been initialized to some value.
     * @param ucurr -
     *            list of variables from the most general unifier of this predicate and an event.
     * @return
     */
    private boolean misMatch(HashMap ufp, HashMap ucurr) {
        boolean val = false;
        Iterator it = ucurr.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (ufp.containsKey(key)) {
                Variable vCur = (Variable) ucurr.get(key);
                Variable vUfp = (Variable) ufp.get(key);
                if (!vCur.equals(vUfp)) {
                    val = true;
                    break;
                }
            }
        }
        return val;
    }

    /**
     * Returns the most general unifier of this predicate and a given event.
     * 
     * @param event -
     *            event that should be unified
     * @param ufp -
     *            list of variables in the template that have been initialized to some value.
     * @return
     */
    public HashMap<String, Variable> getCurrentMGU(LogEvent event, HashMap ufp) {
        HashMap<String, Variable> mgu = new HashMap<String, Variable>();
        logger.debug("Can the current pred be unified with the current event? " + canBeUnified(event));
        if (canBeUnified(event)) {
            mgu.putAll(tempMgu);
            tempMgu.clear();
            if (matchesID(event.getID()))
                mgu.put("ID", new Variable("ID", "string", event.getID()));
            else
                mgu.clear();
            
            if (misMatch(ufp, mgu))
                mgu.clear();            
        }
        return mgu;
    }

    /**
     * Checks whether the predicate has been specified as unconstrained.
     * 
     * @return
     */
    public boolean isUnconstrained() {
        return unConstrained;
    }

    /**
     * Updates the ranges of this predicate.
     * 
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value.
     */
    public void updateRange(HashMap<String, TimeVar> timeVarMap) {
        if (timeVarMap.containsKey(lB.getTimeVarName())) {
            lB.setTimeValue(((TimeVar) timeVarMap.get(lB.getTimeVarName())).getValue());
        }
        if (timeVarMap.containsKey(uB.getTimeVarName())) {
            uB.setTimeValue(((TimeVar) timeVarMap.get(uB.getTimeVarName())).getValue());
        }
        if (this.getTime() == Constants.TIME_UD && this.getRangeLB() != Constants.RANGE_LB) {
            this.setTime(this.getRangeLB());
        }
    }

    /**
     * Returns a String representation of this predicate.
     * 
     * @param withPID
     * @return
     */
    public String toString(boolean withPID) {
        String val = "";
        if (!ecName.equals(Constants.PRED_RELATION)) {
            // String sig = toStringSignature(withPID);
            if (isNegated())
                val += "! ";
            String sig = toString(ID, withPID);
            val += sig.substring(0, sig.length() - 1) + "," + timeStamp.getName() + "=" + timeStamp.getValue();
            if (ecName.equals(Constants.PRED_HAPPENS))
                val += ",R(" + lB.toString() + "," + uB.toString() + ")";
            if (source.equals(Constants.SOURCE_ABD) || (time1 != Constants.TIME_UD && time2 != Constants.TIME_UD))
                val += ",R'(" + this.getTime1() + "," + this.getTime2() + ")";
            val += ")";
        }

        else {// relational predicate
            val = Constants.PRED_RELATION + "(";
            for (int i = 0; i < variables.size(); i++) {
                Object obj = variables.get(i);
                if (obj instanceof Variable) {
                    val += ((Variable) obj).toString(true);
                }
                else {
                    val += ((Function) obj).toString();
                }

                if (i < 1) {
                    val += " " + partnerId + " ";
                }
            }
            val += ")";
        }
        return val;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String s = new String();

        s += isNegated() ? "!" : "";
        
        if(ecName.equals(Constants.PRED_HOLDSAT))
        	s += 
        		ecName + "(" + "prefix=" + prefix + "," + "partnerId=" + partnerId + "," + "opnName=" + opnName + ","
            + "timestamp=" + timeStamp + "," + "eventId=" + ID + "," + "fluent=" + fluent.getFluentName() + "," + "(" + lB + "," + uB + "))";
        else
        	s +=
                ecName + "(" + "prefix=" + prefix + "," + "partnerId=" + partnerId + "," + "opnName=" + opnName + ","
                        + "timestamp=" + timeStamp + "," + "eventId=" + ID + "," + "(" + lB + "," + uB + "))";

        return s;
    }

    private static String toString(Object o) {
        ObjectOutputStream oos1 = null;
        ByteArrayOutputStream bos1 = null;

        try {
            bos1 = new ByteArrayOutputStream();

            oos1 = new ObjectOutputStream(bos1);

            // serialize and pass the object
            oos1.writeObject(o);
            oos1.flush();

            return bos1.toString();
        }
        catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }

}
