/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * LogEvent.java
 *
 * Created on 19 May 2004, 15:37
 */

/**
 * This class extends Signature.java class to model runtime EC event.
 * @author  am697
 */
package uk.ac.city.soi.everest.er;

import java.io.Serializable;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Signature;
import uk.ac.city.soi.everest.core.Variable;

public class LogEvent extends Signature implements Serializable {

    private long timestamp;
    private long time1; // used only for abduction as the lower bound
    private long time2; // used only for abduction as the upper bound
    private String ID;
    private boolean negated;
    private boolean abducible;
    private boolean recordable;

    /**
     * Creates a new instance of LogEvent.
     * 
     * @param ecName
     * @param prefix
     * @param partnerId
     * @param opnName
     * @param timeStamp
     */
    public LogEvent(String ecName, String prefix, String partnerId, String opnName, long timeStamp) {
        super(ecName, prefix, partnerId, opnName);
        this.timestamp = timeStamp;
        this.time1 = Constants.TIME_UD;
        this.time2 = Constants.TIME_UD;
        // this.abdPath = new ArrayList<String>();
        // this.dedPath = new ArrayList<String>();
        negated = false;
        ID = "";
    }

    /**
     * Copy constructor.
     * 
     * @param logEvent
     */
    /*
    public LogEvent(LogEvent logEvent) {
        super(logEvent.getEcName(), logEvent.getPrefix(), logEvent.getPartnerId(), logEvent.getOperationName());
        this.timestamp = logEvent.getTimestamp();
        this.ID = logEvent.getID();
        this.negated = logEvent.isNegated();
        for (int i = 0; i < logEvent.getVariables().size(); i++) {
            Variable var = (Variable) logEvent.getVariables().get(i);
            this.variables.add(new Variable(var));
        }
        if (logEvent.getFluent() != null)
            setFluent(logEvent.getFluent());
        this.time1 = logEvent.getTime1();
        this.time2 = logEvent.getTime2();
    }
	*/
    /**
     * Sets the ID of the event.
     * 
     * @param ID
     */
    public void setID(String ID) {
        this.ID = new String(ID);
    }

    /**
     * Sets if the event is negated or not.
     * 
     * @param val
     */
    /*
    public void setNegated(boolean val) {
        negated = val;
    }
	*/
    /**
     * Returns the time stamp of this event.
     * 
     * @return
     * @deprecated Use {@link #getTimestamp()} instead
     */
    public long getTime() {
        return getTimestamp();
    }

    /**
     * Returns the time stamp of this event.
     * 
     * @return
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the time1 of this event.
     * 
     * @return
     */
    public long getTime1() {
        return time1;
    }

    /**
     * Returns the time2 of this event.
     * 
     * @return
     */
    public long getTime2() {
        return time2;
    }

    /**
     * Returns the ID of the event.
     * 
     * @return
     */
    public String getID() {
        return ID;
    }

    /**
     * Returns true if the event is negated, otherwise returns false.
     * 
     * @return
     */
    public boolean isNegated() {
        return negated;
    }

    /**
     * Returns true if the event is abducible, otherwise returns false.
     * 
     * @return
     */
    public boolean isAbducible() {
        return abducible;
    }

    /**
     * Returns true if the event is recordable, otherwise returns false.
     * 
     * @return
     */
    public boolean isRecordable() {
        return recordable;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getClass().getName() + "{" + "ecName=" + this.ecName + "," + "ID=" + this.ID + "," + "prefix="
                + this.prefix + "," + "partnerId=" + this.partnerId + "," + "operationName=" + this.opnName + ","
                + "parameters=" + variables + "," + "timeStamp=" + this.timestamp + "}";
    }

    /**
     * Returns a string representation of the event.
     * 
     * @param showPID -
     *            if true includes the partner ID in the string representation
     * @return
     */
    public String toString(boolean showPID) {
        String val = "";
//        if (isNegated())
//            val += "! ";
        val += getEcName() + "(";
        if (!ecName.equals("HoldsAt")) {
            val += prefix + ":";
        }
        if (showPID && !prefix.equals("as") && !ecName.equals("HoldsAt")) {
            val += partnerId + ":";
        }
        if (!ecName.equals("HoldsAt")) {
            val += opnName + "(" + ID + ",";
            for (int i = 0; i < variables.size(); i++) {
                Variable var = (Variable) variables.get(i);
                val += var.getName() + "=" + var.getValue() + ",";
            }
            val += ")";

//            if (fluent != null)
//                val += ",";
        }
//        if (fluent != null) {
//            // if(ecName.equals("HoldsAt")) val += fluent.getFluentType() + "(" + fluent.getFirstVariable().getName() +
//            // "," + fluent.getSecondVariable().getName() + ")";
//            // else
//            val += "valueOf(" + fluent.getVariable().getName() + "," + fluent.getVariable().getObjValue() + ")";
//        }
        val += ")";

        val += "at " + timestamp;

        val += " with extra time constraints R'(" + this.getTime1() + "," + this.getTime2() + ")";

        return val;
    }

}
