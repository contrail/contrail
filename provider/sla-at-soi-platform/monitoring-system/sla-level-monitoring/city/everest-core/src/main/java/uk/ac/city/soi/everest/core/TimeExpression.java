/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * TimeExpression.java
 *
 * Created on 30 May 2004, 18:39
 */
package uk.ac.city.soi.everest.core;

import java.io.Serializable;

/**
 * This class models time expressions. it is assumed here that only the following expression are permitted T1 + number
 * or T1 - number. Although in the schema expressions like T1 + T2 or T1 - T2 + number is permitted. But it is not
 * implemented This class has the following fields :
 * 
 * timeVariable - Time variable involved in the espression operator - operator of the expression (plus or minus) number -
 * a numerical value involved in the expression
 * 
 * @author Khaled Mahbub
 */
public class TimeExpression implements Serializable {

    private TimeVar timeVariable;
    private String operator;
    private long number;

    /**
     * Creates a new instance of TimeExpression.
     */
    public TimeExpression() {
        timeVariable = new TimeVar("");
        operator = Constants.PLUS;
        number = 0;
    }

    /**
     * Creates a new instance of TimeExpression.
     */
    public TimeExpression(String tName, String operator, long number) {
        timeVariable = new TimeVar(tName);
        this.operator = new String(operator.getBytes());
        this.number = number;
    }

    /**
     * Creates a new Time Expression with a given time variable name. Operator is plus and number is 0.
     * 
     * @param tName
     */
    public TimeExpression(String tName) {
        timeVariable = new TimeVar(tName);
        operator = Constants.PLUS;
        number = 0;
    }

    /**
     * Creates a new Time Expression with a given time variable. Operator is plus and number is 0.
     * 
     * @param var
     */
    public TimeExpression(TimeVar var) {
        timeVariable = new TimeVar(var);
        operator = Constants.PLUS;
        number = 0;
    }

    /**
     * Copy constructor.
     * 
     * @param tex
     */
    public TimeExpression(TimeExpression tex) {
        timeVariable = new TimeVar(tex.getTimeVar());
        operator = new String(tex.getOperator());
        number = tex.getNumber();
    }

    /**
     * Returns the time variable of this expression.
     * 
     * @return
     */
    public TimeVar getTimeVar() {
        return timeVariable;
    }

    /**
     * Returns the operator of this expression.
     * 
     * @return
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Returns the number of this expression.
     * 
     * @return
     */
    public long getNumber() {
        return number;
    }

    /**
     * Sets the number of this expression.
     * 
     * @param num
     */
    public void setNumber(long num) {
        number = num;
    }

    /**
     * Sets the operator of this expression.
     * 
     * @param operator
     */
    public void setOperator(String operator) {
        this.operator = new String(operator);
    }

    /**
     * Sets the name of the time variable of this expression.
     * 
     * @param name
     */
    public void setTimeVarName(String name) {
        timeVariable.setName(name);
    }

    /**
     * Sets the value of the time variable of this expression.
     * 
     * @param val
     */
    public void setTimeValue(long val) {
        timeVariable.setValue(val);
    }

    /**
     * Returns the value of the time variable of this expression.
     * 
     * @return
     */
    public long getTimeValue() {
        return timeVariable.getValue();
    }

    /**
     * Returns the name of the time variable of this expression.
     * 
     * @return
     */
    public String getTimeVarName() {
        return timeVariable.getName();
    }

    /**
     * Returns the value of this time expression, i.e. expression is evaluated and returned.
     * 
     * @return
     */
    public long getValue() {
        long val = timeVariable.getValue();
        if (val != Constants.TIME_UD && val != Constants.RANGE_UB) {
            if (operator.equals(Constants.PLUS))
                val += number;
            else if (operator.equals(Constants.MINUS))
                val -= number;
        }
        return val;
    }

    /**
     * Returns a string representation of this time expression.
     */
    @Override
    public String toString() {
        String val = "";

        if (timeVariable.getName().equals("")) {
            val += number;
        }
        else {
            if (number == 0) {
                val += timeVariable.getName() + "(" + timeVariable.getValue() + ")";
            }
            else {
                val += timeVariable.getName() + "(" + timeVariable.getValue() + ")" + operator + Math.abs(number);
            }
        }

        return val;
    }
}
