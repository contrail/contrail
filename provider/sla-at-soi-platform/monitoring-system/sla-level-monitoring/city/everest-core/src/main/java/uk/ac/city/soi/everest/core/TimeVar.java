/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * TimeVar.java
 *
 * Created on 30 May 2004, 18:34
 */
package uk.ac.city.soi.everest.core;

import java.io.Serializable;

/**
 * This class models a time variable. This has following fields: name - name of the time variable value - value of the
 * time variable, (default value is undefined, i.e. -1)
 * 
 * @author Khaled Mahbub
 */
public class TimeVar implements Serializable {

    private String name;
    private long value = Constants.TIME_UD;

    /**
     * Creates a new instance of TimeVar.
     * 
     */
    public TimeVar(String name) {
        this.name = new String(name);
        value = Constants.TIME_UD;
    }

    /**
     * Copy constructor.
     * 
     * @param tVar
     */
    public TimeVar(TimeVar tVar) {
        name = new String(tVar.getName());
        value = tVar.getValue();
    }

    /**
     * Sets the name of the time variable.
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = new String(name);
    }

    /**
     * Sets the value of the time variable.
     * 
     * @param value
     */
    public void setValue(long val) {
        this.value = val;
    }

    /**
     * Returns the name of the time variable.
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the value of the time variable.
     * 
     * @return
     */
    public long getValue() {
        return value;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.name + "(" + this.value + ")";
    }
}
