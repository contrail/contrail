/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * @author Theocharis Tsigkritis
 * 
 * 14 Apr 2010
 */
package uk.ac.city.soi.everest.monitor;

import java.io.Serializable;
import java.util.ArrayList;

import uk.ac.city.soi.everest.core.Constants;


/**
 * This class is used for modelling  constrained Event calculus predicate sub formulas. 
 */
public class ConstrainedPredicatesSubformula implements Serializable{

	private ArrayList<Predicate> constrainedPredicates;
	private ArrayList<Predicate> constraints;
	private boolean negated = false;
	private String truthValue = Constants.TRUTH_VAL_UK;
	private String positioningInFormula = null;
	
	/**
	 * Empty Constructor.
	 */
	public ConstrainedPredicatesSubformula() {
		constrainedPredicates = new ArrayList<Predicate>();
		constraints = new ArrayList<Predicate>();
		negated = false;
		truthValue = Constants.TRUTH_VAL_UK;
		positioningInFormula = null;
	}

	/**
	 * Copy Constructor.
	 */
	public ConstrainedPredicatesSubformula(ConstrainedPredicatesSubformula subformula) {
		this.constrainedPredicates = new ArrayList<Predicate>();
		for(Predicate constraintedPredicate : subformula.getConstrainedPredicates()){
			this.addConstrainedPredicate(new Predicate(constraintedPredicate));
		}
		this.constraints = new ArrayList<Predicate>();
		for(Predicate constraint : subformula.getConstraints()){
			this.addConstraint(new Predicate(constraint));
		}
		this.negated = subformula.isNegated();
		this.setTruthValue(subformula.getTruthValue());
		this.setPositioningInFormula(subformula.getPositioningInFormula());
	}


	/**
	 * Retrieves the list of constrained predicates.
	 * 
	 * @return the constrainedPredicates
	 */
	public ArrayList<Predicate> getConstrainedPredicates() {
		return constrainedPredicates;
	}

	/**
	 * Sets the list of constrained predicates.
	 * 
	 * @param constrainedPredicates the constrainedPredicates to set
	 */
	public void setConstrainedPredicates(ArrayList<Predicate> constrainedPredicates) {
		this.constrainedPredicates = constrainedPredicates;
	}

	/**
	 * Retrieves the list of constrains.
	 * 
	 * @return the constraints
	 */
	public ArrayList<Predicate> getConstraints() {
		return constraints;
	}

	/**
	 * Sets the list of constrains.
	 * 
	 * @param constraints the constraints to set
	 */
	public void setConstraints(ArrayList<Predicate> constraints) {
		this.constraints = constraints;
	}

	/**
	 * Adds a constrained predicate to the list of constrained predicates.
	 * 
	 * @param constraintedPredicate
	 */
	public void addConstrainedPredicate(Predicate constraintedPredicate){
		this.constrainedPredicates.add(constraintedPredicate);
	}
	
	/**
	 * Adds a constraint.
	 * 
	 * @param constraint
	 */
	public void addConstraint(Predicate constraint){
		this.constraints.add(constraint);
	}

	/**
	 * Checks whether the sub formula is negated.
	 * 
	 * @return the negated
	 */
	public boolean isNegated() {
		return negated;
	}

	/**
	 * Sets the sub formula negated.
	 * 
	 * @param negated the negated to set
	 */
	public void setNegated(boolean negated) {
		this.negated = negated;
	}

	/**
	 * Retrieves the truth value of the sub formula.
	 * 
	 * @return the truthValue
	 */
	public String getTruthValue() {
		return truthValue;
	}

	/**
	 * Sets the truth value of the sub formula.
	 * 
	 * @param truthValue the truthValue to set
	 */
	public void setTruthValue(String truthValue) {
		this.truthValue = truthValue;
	}

	/**
	 * Returns the actual positioning of the sub formula within a formula. 
	 * 
	 * @return the positioningInFormula
	 */
	public String getPositioningInFormula() {
		return positioningInFormula;
	}

	/**
	 * Sets the actual positioning of the sub formula within a formula. 
	 * 
	 * @param positioningInFormula the positioningInFormula to set
	 */
	public void setPositioningInFormula(String positioningInFormula) {
		this.positioningInFormula = positioningInFormula;
	}
	
	
	
	
}
