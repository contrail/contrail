This version of BSE has got the following limitations

1) Not all patterns of <to>,<from> in <assign> activity been considered. Only one pattern (variable/part) has been considered.
2) In case of <onAlarm> (in <pick>) "dealine-expr" (i.e. value of 'until' attribute) has not been considered.
3) In parsing WSDL file to get message/part type, <types> element has not been considered, i.e. this implementation doesn't 
   support complexType defined in the WSDL file. Also in case of <part> inside the <message> the 'element' attribute 
   is ignored. If a WSDL file imports message from another WSDL file, that is ignored by the variable tracker.
4) In parsing BPEL file to find variable type from WSDL file, <scope> activity has been ignored. i.e. only global variables 
   have been considered.