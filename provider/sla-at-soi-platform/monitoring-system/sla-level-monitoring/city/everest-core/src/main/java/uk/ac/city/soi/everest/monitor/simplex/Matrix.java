/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.monitor.simplex;

/**
 * This Matrix class is used by the Simplex class for internal coefficient matrices.
 * 
 * @author Khaled Mahbub
 * 
 */
public class Matrix {
    public float A[][];
    private int size;
    private int k;

    private int[] order;
    private float[] temp;
    private int swap;
    private float max;
    private float scale;
    private int sizeMinus1;

    /**
     * Constructor for given size.
     * 
     * @param size
     */
    public Matrix(int size) {
        A = new float[size][size];
        order = new int[size];
        temp = new float[size];
        this.size = size;
        sizeMinus1 = size - 1;
    } /* end Matrix procedure */

    /**
     * The solve function does Gaussian Elimination with partial pivoting. In the process it destroys the matrix.
     */

    public boolean solve(float[] x, float[] b) {
        /* This sets the order originally */
        for (int i = 0; i < size; i++)
            order[i] = i;

        /* This is the forward elimination part of the uk.ac.city.soi.everest */

        for (int i = 0; i < sizeMinus1; i++)
            for (int j = i + 1; j < size; j++) {
                max = A[order[i]][i];

                /* take absolute value into account */

                if (max < 0)
                    max *= -1;

                for (int t = j; t < size; t++)
                    if (max < A[order[t]][i] || max == 0) {
                        max = A[order[t]][i];
                        swap = order[i];
                        order[i] = t;
                        order[t] = swap;
                    }

                scale = A[order[j]][i] / A[order[i]][i];

                if (A[order[j]][i] != 0) {
                    for (k = i; k < size; k++) {
                        A[order[j]][k] -= (scale * A[order[i]][k]);
                    }
                    b[order[j]] -= (scale * b[order[i]]);
                }
            }

        /* make sure x vector starts at zero! */
        for (int i = 0; i < size; i++)
            x[i] = 0;

        /* This is the backward substitution part of the uk.ac.city.soi.everest */

        for (int i = sizeMinus1; i >= 0; i--) {
            for (int j = sizeMinus1; j > i; j--)
                x[order[i]] -= (x[order[j]] * A[order[i]][j]);
            x[order[i]] += b[order[i]];
            if (A[order[i]][i] != 0)
                x[order[i]] /= A[order[i]][i];
        }

        /* put the answer back in order or the rest of the uk.ac.city.soi.everest won't work. */

        for (int i = 0; i < size; i++)
            temp[i] = x[order[i]];

        /* copy it back to x */
        for (int i = 0; i < size; i++)
            x[i] = temp[i];

        return true;
    } /* end solve procedure */

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = new String();

        // loop on rows
        for (int r = 0; r < A.length; r++) {
            // loop on columns
            for (int c = 0; c < A[r].length; c++) {
                result += A[r][c] + "\t";
            }
            result += "\n";
        }
        return super.toString();
    }

} /* end Matrix class */
