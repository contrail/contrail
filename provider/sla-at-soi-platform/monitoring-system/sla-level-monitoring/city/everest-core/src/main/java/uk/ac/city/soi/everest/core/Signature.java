/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Signature.java
 *
 * Created on 01 June 2004, 16:44
 */

package uk.ac.city.soi.everest.core;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This is the core class that has been used to model Event Calculus predicates. It has the following fields ecName -
 * signifies EC predicate name, e.g. Happens, Initiates etc. prefix - signifies the BPEL activity, e.g. ic for invoke,
 * rc for receive etc. opnName - name of the operation (action) in the EC predicate partnerId - name of teh partner
 * provides the operation variables - list of variables in the operation fluent - holds the fluent of EC predicate (in
 * case of initiates and holdsAt).
 * 
 * This class has been further extended to implement LogEvent and Predicate.
 * 
 * @author am697
 */
public class Signature implements Serializable {

    protected String ecName;
    protected String prefix;
    protected String opnName;
    protected String partnerId;

    protected ArrayList variables;

    protected Fluent fluent;

    /**
     * Creates a new instance of Signature.
     */
    public Signature(String ecName, String prefix, String partnerId, String opnName) {
        this.ecName = ecName;
        this.prefix = prefix;
        this.partnerId = partnerId;
        this.opnName = opnName;
        variables = new ArrayList<Object>(); // allocate the minimum
        fluent = null;
    }

    /**
     * Sets a fluent for this predicate.
     * 
     * @param var
     */
    public void setFluent(Variable var) {
        this.fluent = new Fluent(var);
    }

    /**
     * Sets a fluent for this predicate.
     * 
     * @param fluent
     */
    public void setFluent(Fluent fluent) {
        this.fluent = new Fluent(fluent);
    }

    /**
     * Sets event calculus predicate name (e.g. happens, initiates etc.).
     * 
     * @param ecName
     */
    public void setEcName(String ecName) {
        this.ecName = new String(ecName.getBytes());
    }

    /**
     * Sets prefix that signifies a BPEL activity, e.g. rc for receive activity, re for reply activity etc.
     * 
     * @param prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = new String(prefix.getBytes());
    }

    /**
     * Sets operation name for this predicate.
     * 
     * @param opnName
     */
    public void setOperationName(String opnName) {
        this.opnName = new String(opnName.getBytes());
    }

    /**
     * Sets partner name for this predicate.
     * 
     * @param partnerId
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = new String(partnerId.getBytes());
    }

    /**
     * Returns the EC predicate name.
     * 
     * @return
     */
    public String getEcName() {
        return ecName;
    }

    /**
     * Returns the prefix of this predicate.
     * 
     * @return
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Returns the operation name of this predicate.
     * 
     * @return
     */
    public String getOperationName() {
        return opnName;
    }

    /**
     * Returns the partner name of this predicate.
     * 
     * @return
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * Adds a variable to this predicate.
     * 
     * @param var
     */
    public void addVariable(Variable var) {
        variables.add(var);
    }

    /**
     * Adds a function to the predicate. Functions can be added if the predicate is a relational predicate, or the
     * predicate is EC initiates.
     * 
     * @param func
     */
    public void addFunction(Function func) {
        variables.add(func);
    }

    /**
     * Returns the fluent of this predicate.
     * 
     * @return
     */
    public Fluent getFluent() {
        return fluent;
    }

    /**
     * Returns a specific variable of the predicate.
     * 
     * @param index -
     *            location of the variable to be returned
     * @return
     */
    public Variable getVariable(int index) {
        if (index < variables.size())
            return (Variable) variables.get(index);
        else
            return null;
    }

    /**
     * Returns all the variables of the predicate. NOTE: this array may contain BOTH Variable and Function objects!
     * 
     * @return Returns all the variables of the predicate
     */
    public ArrayList getVariables() {
        return variables;
    }

    /**
     * Returns all and only the Variable objects of the predicate, but not the variables contained into the predicate's
     * Fluent object, if any. This method is different from the method getVariables that return BOTH Variable and
     * Function objects.
     * 
     * @return Returns all and only the Variable objects of the predicate
     */
    public ArrayList<Variable> getAllVariablesOnly() {
        ArrayList<Variable> variables = new ArrayList<Variable>();

        for (int i = 0; i < getVariables().size(); i++) {
            Object obj = getVariables().get(i);

            // instanceof Variable
            if (obj instanceof Variable) {
                variables.add((Variable) obj);

            }
            // relational predicate may have functions
            else if (obj instanceof Function) {
                Function function = (Function) obj;
                variables.addAll(function.getAllVariables());
            }
        }

        return variables;
    }

    /**
     * Returns a string representation of the predicate.
     * 
     * @param id -
     *            id of teh operation to be displayed in the string representation
     * @param withPID -
     *            if true then partner ID is displayed in the string representation
     * @return
     */
    public String toString(String id, boolean withPID) {
        String val = "";
        val += ecName + "(";
        if (!ecName.equals("HoldsAt"))
            val += prefix + ":";
        if (withPID && !prefix.equals("as") && !ecName.equals("HoldsAt"))
            val += partnerId + ":";
        if (!ecName.equals("HoldsAt")) {
            val += opnName + "(" + (id.equals("") ? "ID" : id);
            for (int i = 0; i < variables.size(); i++) {
                Object obj = variables.get(i);
                if (obj instanceof Variable)
                    val += "," + ((Variable) obj).toString(false);
                // {
                // Variable var = (Variable)obj;
                // val += "," + var.getName();
                // }
                else
                    val += ((Function) obj).toString();
            }
            val += ")";
            if (fluent != null)
                val += ",";

            for (int i = 0; i < variables.size(); i++) {
                Variable var = (Variable) variables.get(i);
                val += var.getName() /* + "=" + (String)var.getObjValue() */
                        + ",";
            }
            val += ")";

            if (fluent != null)
                val += ",";
        }
        if (fluent != null)
            val += fluent.toString();
        val += ")";
        return val;
    }

    /**
     * Returns a hash uk.ac.city.soi.everest for this predicate Hash uk.ac.city.soi.everest is the
     * uk.ac.city.soi.everest used for unification purpose in case of past event handling. It considers only the name
     * and type of the variables, and does not conisder the value. It is used as key in a map to store and match
     * event/predicate.
     */
    public String getHashCode() {
        String val = ecName + prefix + opnName;
        for (int i = 0; i < variables.size(); i++) {
            Object obj = variables.get(i);
            if (obj instanceof Variable)
                val += ((Variable) obj).getHashCode();
            else
                val += ((Function) obj).getHashCode();
        }
        if (fluent != null)
            val += fluent.getHashCode();
        return val;
    }

    /**
     * The method does not compare Fluent objects and Variable objects.
     * 
     * @param signature
     *            the signature to be compared with
     * @param consideringValues
     *            if true also the signature variables value are checked
     * @return true is the input signature is equal, regarding the selected criteria, to the target signature, false
     *         otherwise
     */
    public boolean equals(Signature signature, boolean consideringValues) {
        boolean result = false;

        // logger.debug("this: " + this);
        // logger.debug("signature: " + signature);

        // checking not collection objects
        if (signature != null && this.ecName.equals(signature.getEcName())
                && this.opnName.equals(signature.getOperationName()) && this.partnerId.equals(signature.getPartnerId())
                && this.prefix.equals(signature.getPrefix())) {

            // logger.debug("this.getVariables().size(): " + this.getVariables().size());
            // logger.debug("signature.getVariables().size(): " + signature.getVariables().size());

            result = matchOperationParameters(signature.getVariables());
        }

        // logger.debug("Signature.equal: return: " + result);

        return result;
    }

    /**
     * Check the array of Variable objects of with the current array of Variable objects of this Signature object.
     * 
     * @param parameters
     *            the array of Variable objects
     * @return true if the size of the arrays are equal, and each element at a position i, in both the array, has the
     *         same type
     */
    private boolean matchOperationParameters(ArrayList<Object> parameters) {
        boolean result = true;

        // check for the array size
        if (this.variables.size() == parameters.size()) {

            // loop over the variable array
            for (int i = 0; i < variables.size(); i++) {

                // check for the object type
                if (variables.get(i) instanceof Variable && parameters.get(i) instanceof Variable) {

                    Variable var1 = (Variable) variables.get(i);
                    Variable var2 = (Variable) parameters.get(i);

                    // the match is performed on the variable type only
                    if (!var1.getType().equals(var2.getType())) {
                        // only one false value is needed to make the whole matching false
                        return false;
                    }
                }
                else {
                    // only one false value is needed to make the whole matching false
                    return false;
                }
            }
        }
        else {
            result = false;
        }

        // logger.debug("Result: " + result);

        return result;
    }

}
