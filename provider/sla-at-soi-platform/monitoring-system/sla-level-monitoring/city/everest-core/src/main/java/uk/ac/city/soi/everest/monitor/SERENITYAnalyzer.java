/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * SERENITYAnalyzer.java
 *
 * Created on 23 September 2005, 15:03
 */

package uk.ac.city.soi.everest.monitor;

/**
 * 
 * @author am697
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.database.DatabaseManagerFactory;
import uk.ac.city.soi.everest.database.event.EventEntityManagerInterface;
import uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface;
import uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.util.GlobalCounter;
import uk.ac.city.soi.everest.util.InternalFunctionUtility;;

/**
 * This is the main class that implements the monitor.
 * 
 * @author am697
 */

public class SERENITYAnalyzer extends Observable {
    // init logger
    private static Logger logger = Logger.getLogger(SERENITYAnalyzer.class);

    HashMap staticVars = new HashMap();

    /**
     * serves as in memory event storage. All the events are stored in it and processed sequentially.
     */
    private ArrayList<LogEvent> eventQ = new ArrayList<LogEvent>();
    /**
     * This boolean field is used to control the monitoring process. As long as it is true monitoring goes on. This is
     * set to false if there is no more event in the event Q.
     */
    private boolean goOn;
    private ArrayList<Template> newTemplates;
    /**
     * SeCSE consistency checker, used to check in consistencies.
     */
    private SeCSEConChecker secseConsistencyChecker;
    /**
     * SeCSE event generator used to generate and handle derived events.
     */
    private SeCSEEventGen secseEventGenerator;
    /**
     * An instance of ClockMap class.
     */
    private ClockMap clockMap = new ClockMap();
    /**
     * An instance of ConstraintMatrixStorer.
     */
    private ConstraintMatrixStorer cms = new ConstraintMatrixStorer();
    /**
     * in memory storage for past events.
     */
    private PastEventStorer pastEventStorer = new PastEventStorer(cms);
    /**
     * Garbage collector to past event storer.
     */
    private PastEventCleaner pec;

    /**
     * List that contains IDs of the formulas (templates) to be monitored.
     */
    private ArrayList<String> templateIds = new ArrayList<String>();

    /**
     * Parameters that control the monitoring mode. Monitoring mode can be (1) wrt Recorded events only (2) wrt Recorded
     * and Derived events.
     */
    private int modeStart = 0, modeStop = 1;

    /**
     * Holds the status of the monitor. Set to true if the monitor is running.
     */
    private boolean running = false;
    /**
     * Holds the status of the monitor. Set to true if the monitor is busy.
     */
    private boolean busy = false;

    private ArrayList<Template> emptyTemplates;

    private HashSet<String> eventFilter = new HashSet<String>();
    /*
     * --------------------------------------------------------------------- STORERS
     * ---------------------------------------------------------------------
     */

    /**
     * Manager for inserting, selecting, deleting or updating events.
     */
    private EventEntityManagerInterface eventStorer;
    /**
     * Manager for inserting, selecting, deleting or updating templates.
     */
    private TemplateEntityManagerInterface templateStorer;
    /**
     * Manager for inserting, selecting, deleting or updating fluents.
     */
    private FluentEntityManagerInterface fluentStorer;

    /*
     * --------------------------------------------------------------------- Global variables
     * ---------------------------------------------------------------------
     */

    /*
     * Global variable: The ids of the templates that have been sent to DOVT
     */
    private ArrayList<String> templatesToDOVT = new ArrayList<String>();

    /*
     * Global variable: The timestamp of the first event
     */
    private long firstEventTimestamp = -1;

    /*
     * Global variable: The start time of monitoring session (i.e., the time when the monitor started to process the
     * given formulas)
     */
    private long monitoringSessionStartTime = -1;

    /**
     * Creates a new instance of SeCSEAnalyzer
     */
    public SERENITYAnalyzer() {
        // this.templateIds = templateIds;
        newTemplates = new ArrayList<Template>();
        emptyTemplates = new ArrayList<Template>();

        // initialise storers
        eventStorer = DatabaseManagerFactory.getDatabaseManager().getEntityManagerFactory().getEventEntityManager();
        templateStorer =
                DatabaseManagerFactory.getDatabaseManager().getEntityManagerFactory().getTemplateEntityManager();
        fluentStorer = DatabaseManagerFactory.getDatabaseManager().getEntityManagerFactory().getFluentEntityManager();
        // fluentStorer = new FluentRAMDatabase();

        initializeInternalComponents();

    }

    /**
     * Initialises internal components of the monitor. E.g. consistency checker and event generator.
     * 
     */
    private void initializeInternalComponents() {
        secseConsistencyChecker = new SeCSEConChecker();
        secseEventGenerator = new SeCSEEventGen(templateStorer, fluentStorer);
        addObserver(secseConsistencyChecker);
        addObserver(secseEventGenerator);
        secseEventGenerator.addObserver(secseConsistencyChecker);
        InternalFunctionUtility.setFluentStorer(fluentStorer);
    }

    /**
     * Receives a collection of formulas (Formula object) to be monitored. This method converts the formulas to
     * templates using TemplateMaker class and then stores the initial sets of templates to the template storer.
     * 
     * @param properties
     */
    public boolean monitorRule(LinkedHashMap<String, Formula> properties) {
    	
    	//System.err.println("55555Size of received formulas " + properties.size());
        // = new LinkedHashMap();
        running = true;

        logger.debug("properties: " + properties);

        // Storing the start time of the monitoring session
        if (monitoringSessionStartTime == -1) {
            monitoringSessionStartTime = System.currentTimeMillis();

            logger.debug("Generates the variable that keeps the start time of the monitoring session that is: "
                    + monitoringSessionStartTime);
            Variable monitoringSessionStartTimeVariable =
                    new Variable("monitoringSessionStartTime", "int", monitoringSessionStartTime);
            monitoringSessionStartTimeVariable.setValue(monitoringSessionStartTime);

            if (staticVars.containsKey(monitoringSessionStartTimeVariable.getName()
                    + monitoringSessionStartTimeVariable.getType())) {
                Variable var =
                        (Variable) staticVars.get(monitoringSessionStartTimeVariable.getName()
                                + monitoringSessionStartTimeVariable.getType());
                var.setValue(monitoringSessionStartTime);
            }

            logger.debug("monitoringSessionStartTimeVariable: " + monitoringSessionStartTimeVariable.toString(true));

            logger.debug("Generates the fluent that keeps the start time of the monitoring session");
            Fluent monitoringSessionStartTimeFluent = new Fluent("monitoringSessionStartTimeFluent");
            monitoringSessionStartTimeFluent.addArgument(monitoringSessionStartTimeVariable);

            logger.debug("Generates an initiates predicate for the start time of the monitoring session");
            Predicate monitoringSessionStartTimeInitiatesPred = new Predicate("", "", "", "", "t0");
            monitoringSessionStartTimeInitiatesPred.setEcName(Constants.PRED_INITIATES);
            monitoringSessionStartTimeInitiatesPred.setTime(monitoringSessionStartTime);
            monitoringSessionStartTimeInitiatesPred.setFluent(monitoringSessionStartTimeFluent);
            monitoringSessionStartTimeInitiatesPred.addVariable(monitoringSessionStartTimeVariable);
            monitoringSessionStartTimeInitiatesPred.setOperationName("self");

            logger.debug("Stores the start time of the monitoring session initiates predicate in the fluent storer");
            fluentStorer.addInitiate(monitoringSessionStartTimeInitiatesPred);
            
            InternalFunctionUtility.setMonitorSessionStartTime(monitoringSessionStartTime);
        }

        ArrayList<String> tIds = new ArrayList<String>();
        Iterator<String> it = properties.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            tIds.add(key);
        }
                 
        ArrayList<Template>  rTemps = TemplateMaker.getTemplates(tIds, properties, true, true, staticVars);

        //System.err.println("777777777Size of rTemps " + rTemps.size());
        // manage the initially predicates of the given formulas
        for (int i = 0; i < TemplateMaker.getInitPred().size(); i++) {
            fluentStorer.addInitiate((Predicate) TemplateMaker.getInitPred().get(i));
        }

        templateStorer.storeTemplates(rTemps, Constants.EVENTS_RECORDED);

        cms.computeConstraintMatrices(rTemps);
        pec = new PastEventCleaner(pastEventStorer, clockMap);
        pec.start();

        for (Template templateI : rTemps) {
            emptyTemplates.add(templateI);            
        }
        
        //System.err.println("999999Size of empty " + emptyTemplates.size());
        templateIds.addAll(tIds);
        
        goOn = true;
        mainLoop();

        pec.stop();
        
        return true;

    }

    /**
     * Receives a list of runtime events (LogEvent objects) and adds to the eventQ If the monitor is not running it
     * starts the monitor.
     * 
     * @param events
     */
    public void notify(ArrayList<LogEvent> events) {
        ArrayList<LogEvent> log = new ArrayList<LogEvent>();
        ArrayList<LogEvent> temp = new ArrayList<LogEvent>();
        if (running) {
            eventQ.addAll(events);
            //System.out.println("SSSSSS EventQ size now " + eventQ.size());
            log.addAll(events);
            /*
             * if(diagnoser.getEventLog().isEmpty()) diagnoser.setEventLog(log); else diagnoser.addEvents(log);
             */
            if (!busy) {
                goOn = true;
                mainLoop();
            }
        }
    }

    public void notify(LogEvent event) {
    	
    	// The lines below are only for testing 
    	/*
    	if(!eventFilter.contains(event.getID() + event.getPrefix())){
    		eventStorer.add(event);
    		eventFilter.add(event.getID() + event.getPrefix());
    	}
    	if(eventFilter.size() > 500) eventFilter.clear();
    	*/
    	// The lines above are only for testing
    	
    	
        if (running) {
        	//if(!eventFilter.contains(event.getID() + event.getPrefix())){
        		eventQ.add(event);
        		eventFilter.add(event.getID() + event.getPrefix());
        	//}
            //System.out.println("SSSSSS EventQ size now " + eventQ.size());
            if(eventFilter.size() > 500) eventFilter.clear();
            //log.addAll(events);
            
             //if(diagnoser.getEventLog().isEmpty()) diagnoser.setEventLog(log); else diagnoser.addEvents(log);
             
            if (!busy) {
                goOn = true;
                mainLoop();
            }
        }
    	
    }
    
    /**
     * Get the projection of a predicate considering the static(persistent) variables of the current monitoring session.
     * 
     * @param pred
     * @return
     */
    private HashMap getProjection(Predicate pred) {
        HashMap projection = new HashMap();
        ArrayList predVars = pred.getVariables();
        for (int i = 0; i < predVars.size(); i++) {
            Object obj = predVars.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                if (var.isStatic()) {
                    if (staticVars.containsKey(var.getName() + var.getType()))
                        projection.put(var.getName() + var.getType(), (Variable) staticVars.get(var.getName()
                                + var.getType()));
                }
            }
            else {// relational predicate may have functions
                Function func = (Function) obj;
                ArrayList funcVars = func.getAllVariables();
                for (int j = 0; j < funcVars.size(); j++) {
                    Variable var = (Variable) funcVars.get(j);
                    if (staticVars.containsKey(var.getName() + var.getType()))
                        projection.put(var.getName() + var.getType(), (Variable) staticVars.get(var.getName()
                                + var.getType()));
                }
            }
        }
        Fluent fluent = pred.getFluent();
        if (fluent != null) {
            for (int i = 0; i < fluent.getArguments().size(); i++) {
                Object obj = fluent.getArguments().get(i);
                if (obj instanceof Variable) {
                    Variable var = (Variable) obj;
                    if (staticVars.containsKey(var.getName() + var.getType())) {
                        projection.put(var.getName() + var.getType(), (Variable) staticVars.get(var.getName()
                                + var.getType()));
                    }
                }
                else if (obj instanceof Function) {
                    Function func = (Function) obj;
                    ArrayList<Variable> funcVars = func.getAllVariables();
                    for (Variable funcVar : funcVars) {
                        if (staticVars.containsKey(funcVar.getName() + funcVar.getType())) {
                            projection.put(funcVar.getName() + funcVar.getType(), (Variable) staticVars.get(funcVar
                                    .getName()
                                    + funcVar.getType()));
                        }
                    }
                }
            }
        }
        return projection;
    }

    /**
     * Stops the monitoring process.
     * 
     */
    public void stop() {
        running = false;
    }

    /**
     * Given a formula Id, configures the analyzer in order to get ready to delete the corresponding formula.
     * 
     * @author t7t
     */
    /*
    public void stopMonitoringFormula(String formulaId) {
        ControlFlag.formulaDeletionFlag = true;
        ControlFlag.dataAffectedByFDF = formulaId;

        logger.info("stopMonitoringFormula method has been called for formula: " + ControlFlag.dataAffectedByFDF);
    }
	*/
    /**
     * Stops monitoring the formula that is affected by the ControlFlag.dataAffectedByFDF.
     * 
     * @author t7t
     */
    /*
    public void deleteFormula() {
        // TO BE IMPLEMENTED USING DATABASE
    }
	*/
    /**
     * Returns all the instances of a formula so far in the monitoring process.
     * 
     * @param formulaId -
     *            The ID of the formula sought for
     * @return
     */
    /*
    public ArrayList<Template> getRules(String formulaId) {
        ArrayList<Template> formulaList = new ArrayList<Template>();

        formulaList = templateStorer.getMonitoringResults(Constants.STATUS_IRB, 0, formulaId);
        formulaList.addAll(templateStorer.getMonitoringResults(Constants.STATUS_SAT, 0, formulaId));

        return formulaList;
    }
	*/
    /**
     * Returns a collection of a monitoring result type selected by time stamp and template id.
     * 
     * @param withStatus -
     *            the monitoring result type
     * @param afterTimepoint -
     *            the specific time stamp
     * @param includingInTemplateId -
     *            the string to be included in the result id
     * @return
     */
    public ArrayList<Template> getMonitoringResults(String withStatus, long afterTimepoint, String includingInTemplateId) {

        ArrayList<Template> monitoringResults = new ArrayList<Template>();

        monitoringResults = templateStorer.getMonitoringResults(withStatus, afterTimepoint, includingInTemplateId);

        return monitoringResults;

    }

    /**
     * This is the main driving loop. It picks up events one by one from the event Q and process the events. It
     * continues as long as event Q has a single event.
     * 
     */
    public void mainLoop() { // this has been made public only for test
        // purpose.
        busy = true;
        while (goOn) {
            if (eventQ.size() > 0) {
                if (ControlFlag.formulaDeletionFlag) {
                    logger.debug("deleting formula");
                    //deleteFormula();
                    ControlFlag.formulaDeletionFlag = false;
                    ControlFlag.dataAffectedByFDF = "";
                }

                LogEvent event = eventQ.get(0);
                eventQ.remove(0);
                
                InternalFunctionUtility.setCurrentEventTime(event.getTimestamp());
                /*
                 * STATISTIC: monitorStartTime
                 */
                long monitorStartTime = System.currentTimeMillis();

                // *****************************************************
                // A new really unique ID is needed, so we decided to
                // create it as eventId+eventPredix. Of course this
                // must be somehow changed in the future, e.g., creating
                // this new id inside the LogEvent class.
                // *****************************************************
                event.setID(event.getID());

                // Insert event to the event database
                eventStorer.add(event);
                
                 
                 
                // Storing the timestamp of the first received event in a
                // special fluent
                if (firstEventTimestamp == -1) {
                	
                    firstEventTimestamp = event.getTimestamp();
                    
                    InternalFunctionUtility.setFirstEventTime(firstEventTimestamp);
                    
                    logger.debug("Generates the variable that keeps the first event timestamp that is: "
                            + firstEventTimestamp);
                    Variable firstEventTimestampVariable =
                            new Variable("firstEventTimestamp", "long", firstEventTimestamp);
                    firstEventTimestampVariable.setValue(firstEventTimestamp - 1); //this is a quick fix, to make sure that (currentEventTime - firstEventTimestamp) never gets zero 

                    if (staticVars.containsKey(firstEventTimestampVariable.getName()
                            + firstEventTimestampVariable.getType())) {
                        Variable var =
                                (Variable) staticVars.get(firstEventTimestampVariable.getName()
                                        + firstEventTimestampVariable.getType());
                        var.setValue(firstEventTimestamp);
                    }

                    logger.debug("firstEventTimestampVariable: " + firstEventTimestampVariable.toString(true));

                    logger.debug("Generates the fluent that keeps the first event timestamp");
                    Fluent firstEventTimestampFluent = new Fluent("firstEventTimestampFluent");
                    firstEventTimestampFluent.addArgument(firstEventTimestampVariable);

                    logger.debug("Generates an initiates predicate for first event timestamp");
                    Predicate firstEventTimestampInitiatesPred = new Predicate("", "", "", "", "t0");
                    firstEventTimestampInitiatesPred.setEcName(Constants.PRED_INITIATES);
                    firstEventTimestampInitiatesPred.setTime(firstEventTimestamp);
                    firstEventTimestampInitiatesPred.setFluent(firstEventTimestampFluent);
                    firstEventTimestampInitiatesPred.addVariable(firstEventTimestampVariable);
                    firstEventTimestampInitiatesPred.setOperationName("self");

                    logger.debug("Stores the first Event Timestamp initiates predicate in the fluent storer");
                    fluentStorer.addInitiate(firstEventTimestampInitiatesPred);
                }

                // store latest time of this channel
                clockMap.addClockTime(event.getPartnerId(), event.getTime());
                
                // logger.debug("Event no. : " + ++eventCounter);
                long time = event.getTimestamp();
                for (int mode = modeStart; mode < modeStop; mode++) {
                    for (Template initialEmptyTemplate : emptyTemplates) {
                        // String formulaId = formulaIds.get(tc);
                        String formulaId = initialEmptyTemplate.getFormulaId();
                        logger.debug("Templates of the formula " + formulaId + " to be processed");

                        ArrayList<Template> formulaList = templateStorer.loadTemplatesF(formulaId, mode);

                        boolean next = true;
                        // int pos = 1;
                        for (int j = formulaList.size() - 1; j >= 0 && next; j--) {

                            Template template = formulaList.get(j);

                            if ((mode == Constants.EVENTS_RECORDED && template.isForChecking() && template.getStatus()
                                    .equals(Constants.STATUS_UD))
                                    || (template.getStatus().equals(Constants.STATUS_UD) || (mode == Constants.EVENTS_MIXED && template
                                            .linkSize() != 0))) {

                                // feed method
                                feed(template, event);

                                /*
                                 * REMARK: A template is removed when the feed algorithm decides that this template is
                                 * removable or when it body is FALSE, as a template with FALSE body can lead only to
                                 * satisfaction templates of the form F==>T that we are not interested in or when is an
                                 * assumption template and both its head and body are True
                                 */
                                if (template.isRemovable()
                                        || template.isBodyFalse()
                                        || (!template.isForChecking() && template.isBodyTrue() && template.isHeadTrue())) {
                                    logger.debug(getClass().getName() + ".mainLoop: removing template " + template);
                                    formulaList.remove(template);
                                    templateStorer.delete(template, mode);
                                }
                                else {
                                    updateStaticVars(template);
                                    // In case there is no other event to be
                                    // processed, the uk.ac.city.soi.everest checks all the
                                    // templates and evaluates
                                    // any relational predicates that have not
                                    // been evaluated till that time
                                    if(template.isActive()){
                                        evaluateRelationalPredicates(template);

                                        evaluateConstrainedPredicatesSubformulas(template);                                    	
                                    }


                                    if (template.isUpdated()) {
                                        secseEventGenerator.update(new TemplateInfo(template, time, mode));
                                        secseConsistencyChecker.update(new TemplateInfo(template, time, mode));
                                    }
                                }
                            }
                        }

                        logger.debug("the following new templates have been added:");
                        while (newTemplates.size() > 0) {
                            Template nTemplate = newTemplates.get(0);

                            nTemplate.setCC(templateStorer.getCount(formulaId, mode) + 1);

                            evaluateBodyHoldsAtPredicates(nTemplate);

                            // print unifiers of the template
                            logger.debug("nTemplate unifiers: ");
                            nTemplate.printUnifiers();

                            if (!(nTemplate.isRemovable() || nTemplate.isBodyFalse() || (!nTemplate.isForChecking()
                                    && nTemplate.isBodyTrue() && nTemplate.isHeadTrue()))) {
                                formulaList.add(nTemplate);
                            }

                            newTemplates.remove(0);
                        }

                        newTemplates.clear();

                        for (Template template : formulaList) {

                            // In case there is no other event to be processed,
                            // the uk.ac.city.soi.everest checks all the templates and
                            // evaluates
                            // any relational predicates that have not been
                            // evaluated till that time
                            if(template.isActive()){
                                evaluateRelationalPredicates(template);

                                evaluateConstrainedPredicatesSubformulas(template);                                    	
                            }

                            if (template.isUpdated() && template.getStatus().equals(Constants.SOURCE_UD)) {
                                secseEventGenerator.update(new TemplateInfo(template, time, mode));
                                secseConsistencyChecker.update(new TemplateInfo(template, System.currentTimeMillis(),
                                        mode));
                            }

                            logger.debug("templateId: " + template.getTemplateId());
                            logger.debug("template status: " + template.getStatus());

                            // print unifiers of the template
                            logger.debug("template unifiers: ");
                            template.printUnifiers();
                        }
                        //logger.debug("444444 Rearranging formulas for  " + formulaId);
                        if(formulaList.size() > 0) reArrangeFormulas(formulaList);
                        templateStorer.updatedTemplatesF(formulaList, formulaId, mode);
                    }
                }

                
                
                /*
                 * STATISTIC: increment the global counter
                 */
                GlobalCounter.next();

            }
            else {

                goOn = false;
            }
        } // END WHILE
        busy = false;
    }

    /**
     * Ensure that the empty template is always at the end of the list
     * @param formulaList
     */
    private void reArrangeFormulas(ArrayList<Template> formulaList){
    	boolean found = false;
    	int i = 0;
    	Template template = formulaList.get(formulaList.size() - 1);
    	if(!template.isActive() && template.getUnifiers().size() == 0) return;
    	else{
        	while(i < formulaList.size() && !found){
        		template = formulaList.get(i);
        		if(!template.isActive() && template.getUnifiers().size() == 0)
        			found = true;
        		else  i++;
        	}    	        	
    		formulaList.remove(i);
    		formulaList.add(template);        	    		
    	}
    }
    
    /**
     * Returns the difference between two maps. Here the concept of set difference is used to compute the difference.
     * The keys of each map is treated as a set and the set difference is computed.
     * 
     * @param ucurr
     * @param ufp
     * @return
     */
    private HashMap<String, Variable> mapDifference(HashMap ucurr, HashMap ufp) {
        HashMap<String, Variable> diff = new HashMap<String, Variable>();
        Iterator it = ucurr.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (!ufp.containsKey(key))
                diff.put(key, (Variable) ucurr.get(key));
        }
        return diff;
    }

    /**
     * This is the method that handles the recorded events.
     * 
     * @param template -
     * @param event -
     */
    private void feed(Template template, LogEvent event) {
        logger.debug("!!!!!\nfeeder test\n!!!!!");
        logger.debug("feed the template: " + template.getFormulaId() + " - " + template.getCC() + " with the event: ");
        logger.debug(event.toString(true));
        boolean templateOk = true;

        for (int predCounter = 0; predCounter < template.totalPredicates() && templateOk; predCounter++) {
            Predicate pred = template.getPredicate(predCounter);
            logger.debug("Try to unify the following pred:" + pred.toString(true));
            logger.debug("with the event" + event.toString(true));
            long channelTime = event.getTimestamp();

            // get the latest time of the channel of this predicate
            if (clockMap.containsSource(pred.getPartnerId()))
                channelTime = clockMap.getLatestTime(pred.getPartnerId());

            if (pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {

                if (!pred.isRangeSet()) {
                    // initialise the range set
                    pred.updateRange(template.getTimeVarMap());
                }

                // the projection of unifiers over the variables of a predicate
                HashMap ufp = template.getProjection(pred);

                Set set1 = template.getUnifiers().entrySet();
                Iterator ite = set1.iterator();
//                while (ite.hasNext()) {
//                    Map.Entry me = (Map.Entry) ite.next();
//                   logger.debug("\t" + me.getKey() + " : " + ((Variable) me.getValue()).toString(true));
//                }
                if (pred.getEcName().equals(Constants.PRED_HAPPENS)) {
                    logger.debug("!!!!!\nPRED_HAPPENS test\n!!!!!");
                    HashMap ucurr = pred.getCurrentMGU(event, ufp);
                    HashMap<String, Variable> diff = mapDifference(ucurr, ufp);
                    ufp.putAll(this.getProjection(pred));
                    if (pred.isUnconstrained()) {
                        logger.debug("!!!!!\nis unconstrained \n!!!!!");
                        if (ucurr.size() > 0) {
                            logger.debug("!!!!!\nuccur test\n!!!!!");
                            Template nTemplate = new Template(template);
                            newTemplates.add(nTemplate);

                            if (event.isNegated() ^ pred.isNegated()) {
                                pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                if (predCounter < template.bodySize()) {
                                    template.setRemovable(true);
                                    templateOk = false;
                                    logger.debug(".... setRemovable test1");
                                }
                            }
                            else
                                pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            pred.setID(event.getID());
                            pred.setSource(Constants.SOURCE_RE);
                            pred.setTime(event.getTimestamp());
                            template.addVarsToUnifiers(ucurr);
                            logger.debug(template.getUnifiers().size() + " has added to unifiers of the template");
                            Set set = template.getUnifiers().entrySet();
                            Iterator i = set.iterator();
//                            while (i.hasNext()) {
//                                Map.Entry me = (Map.Entry) i.next();
//                                logger.debug("\t" + me.getKey() + " : " + ((Variable) me.getValue()).toString(true));
//                            }
                            template.setActive(true);
                            template.addTimeVarToMap(pred.getTimeVar());
                            template.setUpdated(true);
                            if (event.getEcName().equals(Constants.PRED_HAPPENS)
                                    && (event.getPrefix().equals("ic") || event.getPrefix().equals("ir")
                                            || event.getPrefix().equals("rc") || event.getPrefix().equals("as")))
                                template.updateIDs(event);
                            pred.updateRange(template.getTimeVarMap());
                        }
                        else if (event.getTimestamp() > pred.getRangeUB()) {
                            if (channelTime > pred.getRangeUB()) {// check the
                                // channel time of this predicate
                                if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                                    if (pred.getEcName().equals(Constants.PRED_RELATION)) {
                                        pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                                        if (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)
                                                && predCounter < template.bodySize()) {
                                            template.setRemovable(true);
                                            templateOk = false;
                                            logger.debug("..... setRemovable test2");
                                        }
                                    }
                                    else {
                                        if (pred.getEcName().equals(Constants.PRED_INITIATES))
                                            pred.evaluateInitiatesAndTerminatesPredicates(ufp, template.getUnifiers(),
                                                    template.getTimeVarMap());
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                        else
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    }
                                    pred.setID("NF_ID");
                                    pred.setSource(Constants.SOURCE_NF);
                                    pred.setTime(pred.getRangeUB());
                                    template.addTimeVarToMap(pred.getTimeVar());
                                    template.setEITLast(event.getTimestamp());
                                    template.setActive(true);
                                    template.setUpdated(true);
                                    pred.updateRange(template.getTimeVarMap());
                                    if (event.getEcName().equals(Constants.PRED_HAPPENS)
                                            && (event.getPrefix().equals("ic") || event.getPrefix().equals("ir")
                                                    || event.getPrefix().equals("rc") || event.getPrefix().equals("as")))
                                        template.updateIDs(event);
                                    if (template.getType().equals(Constants.TYPE_PAST)
                                            && template.sizeOfUnifiers() == 0) {
                                        /*
                                         * Code for revising timing constraint of other predicates in template
                                         * considering this predicate as the unconstrained predicate
                                         */
                                    }
                                }// end of NOT partial
                            }// end of the check of the channel time of this predicate
                        }
                    }
                    else if (pred.getQuantifier().equals(Constants.QUANT_EXIST) && template.isActive()) {
                        // problematic...
                        logger.debug("is constrained and has existential quantifier");
                        logger.debug((ucurr.size() > 0) + " " + pred.withinRange(event.getTime()) + " "
                                + !event.isNegated());
                        if (ucurr.size() > 0
                                && !event.isNegated()
                                && (pred.withinRange(event.getTime()) || (template.getType()
                                        .equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0))) {
                            logger.debug("existential 1");
                            Template nTemplate = null;
                            if (pred.isPartial(ufp, template.getTimeVarMap()) && diff.size() > 0) {
                                logger.debug("existential 1.1");
                                // uk.ac.city.soi.everest for creating new template
                                nTemplate = new Template(template);
                                //newTemplates.add(nTemplate);
                            }
                            if (pred.isNegated()) {
                                logger.debug("existential 1.2");
                                pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                if (predCounter < template.bodySize()) {
                                    template.setRemovable(true);
                                    templateOk = false;
                                    logger.debug(".... setRemovable test3");
                                }
                            }
                            else {
                                pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            }
                            if(!template.isRemovable() && nTemplate != null) newTemplates.add(nTemplate);
                            logger.debug("existential 1.3");
                            pred.setID(event.getID());
                            pred.setSource(Constants.SOURCE_RE);
                            pred.setTime(event.getTimestamp());
                            if (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0) {
                                /*
                                 * Code for revising timing constraint of other predicates in template considering this
                                 * predicate as the unconstrained predicate
                                 */
                            }
                            if (ucurr.size() > 0) {
                                logger.debug("existential 1.4");
                                template.addVarsToUnifiers(ucurr);
                                logger.debug(template.getUnifiers().size() + " has added to unifiers of the template");
                                Set set = template.getUnifiers().entrySet();
                                Iterator i = set.iterator();
                                while (i.hasNext()) {
                                    Map.Entry me = (Map.Entry) i.next();
                                    logger
                                            .debug("\t" + me.getKey() + " : "
                                                    + ((Variable) me.getValue()).toString(true));
                                }
                                template.addTimeVarToMap(pred.getTimeVar());
                            }
                            if (event.getEcName().equals(Constants.PRED_HAPPENS)
                                    && (event.getPrefix().equals("ic") || event.getPrefix().equals("ir")
                                            || event.getPrefix().equals("rc") || event.getPrefix().equals("as"))) {
                                logger.debug("existential 1.5");
                                template.updateIDs(event);
                            }
                            template.setUpdated(true);
                        }
                        else { // event of the form !p(a,t) or q(_,t)
                            logger.debug("existential 2");
                            if (event.getTimestamp() < pred.getRangeUB()
                                    || (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0)) {
                                logger.debug("existential 2.1");
                                if (ucurr.size() > 0
                                        && event.isNegated()
                                        && (event.getTime() == pred.getTime() + Constants.minT || (template.getType()
                                                .equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0))) {
                                    //logger.debug("existential 2.2");
                                    if (pred.isPartial(ufp, template.getTimeVarMap()) && diff.size() > 0) {
                                        //logger.debug("existential 2.3");
                                        Template nTemplate = new Template(template);
                                        newTemplates.add(nTemplate);
                                    }
                                    pred.setTime(event.getTimestamp());
                                    template.addVarsToUnifiers(ucurr);
//                                    logger.debug(template.getUnifiers().size()
//                                            + " has added to unifiers of the template");
//                                    Set set = template.getUnifiers().entrySet();
//                                    Iterator i = set.iterator();
//                                    while (i.hasNext()) {
//                                        Map.Entry me = (Map.Entry) i.next();
//                                        logger.debug("\t" + me.getKey() + " : "
//                                                + ((Variable) me.getValue()).toString(true));
//                                    }
                                    template.setUpdated(true);
                                }
                            } // end of E.t < p.constraint.stopTime
                            else if (event.getTimestamp() >= pred.getRangeUB()) {
                                // E.t >= P.constraint.stopTime
                                logger.debug("existential 2.4");
                                if (channelTime > pred.getRangeUB()) {// check
                                    // the channel time of this predicate
                                    //logger.debug("existential 2.5");
                                    if (!pred.isPartial(ufp, template.getTimeVarMap())) {
//                                        logger
//                                                .debug("successful check of partial pred for E.t >= P.constraint.stopTime case");
                                        if (pred.getEcName().equals(Constants.PRED_RELATION)) {
                                            pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                                            if (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)
                                                    && predCounter < template.bodySize()) {
                                                template.setRemovable(true);
                                                templateOk = false;
                                                //logger.debug(".... setRemovable test4");
                                            }
                                        }
                                        else {
                                            if (pred.getEcName().equals(Constants.PRED_INITIATES)) {
                                                pred.evaluateInitiatesAndTerminatesPredicates(ufp, template
                                                        .getUnifiers(), template.getTimeVarMap());
                                            }
                                            if (pred.isNegated())
                                                pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                            else {
                                                pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                            }
                                        }
                                        if (pred.getID().equals(""))
                                            pred.setID("NF_ID");
                                        if (pred.getTime() == pred.getRangeUB()
                                                && pred.getSource().equals(Constants.SOURCE_UD))
                                            pred.setSource(Constants.SOURCE_RE);
                                        else {
                                            if (pred.getSource().equals(Constants.SOURCE_UD))
                                                pred.setSource(Constants.SOURCE_NF);
                                            pred.setTime(pred.getRangeUB());
                                        }
                                        template.addTimeVarToMap(pred.getTimeVar());
                                        template.setUpdated(true);
                                        template.setEITLast(event.getTimestamp());
                                        if (template.getType().equals(Constants.TYPE_PAST)
                                                && template.sizeOfUnifiers() == 0) {
                                            /*
                                             * Code for revising timing constraint of other predicates in template
                                             * considering this predicate as the unconstrained predicate
                                             */
                                        }
                                    }// end of NOT partial

                                    else {// predicate is partial
                                    	if(pred.isNegated()){
                                    		pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                            pred.setSource(Constants.SOURCE_NF);
                                            template.setUpdated(true);
                                            pred.setTime(pred.getRangeUB());
                                            template.addTimeVarToMap(pred.getTimeVar());
                                    	}
                                    	else if (!template.getType().equals(Constants.TYPE_PAST)) {
                                            template.setRemovable(true);
                                            templateOk = false;
                                            //logger.debug("....setRemovable test5");
                                        }
                                    }// end of partial

                                }// end of the check of the channel time of this predicate
                            }// end of E.t >= P.constraint.stopTime
                        }// end of form !p(a,t) or q(_,t)
                    }// end of existential
                    else if (pred.getQuantifier().equals(Constants.QUANT_FORALL) && template.isActive()) {
                        // Universally quantified time var
                        if (event.getTimestamp() < pred.getRangeUB()
                                || (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0)) {
                            if (event.getTimestamp() == pred.getTime() + Constants.minT
                                    || (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0)) {
                                if (ucurr.size() > 0) {
                                    if (pred.isPartial(ufp, template.getTimeVarMap()) && diff.size() > 0) {
                                        Template nTemplate = new Template(template);
                                        newTemplates.add(nTemplate);
                                    }
                                    if (!event.isNegated()) { // p(x,t)
                                        pred.setTime(event.getTimestamp());
                                    }
                                    else { // !p(x,t)
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                        else {
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                            if (predCounter < template.bodySize()) {
                                                template.setRemovable(true);
                                                templateOk = false;
                                                //logger.debug("...setRemovable test6");
                                            }
                                        }
                                        pred.setSource(Constants.SOURCE_RE);
                                        pred.setTime(event.getTimestamp());
                                        template.addTimeVarToMap(pred.getTimeVar());
                                        if (template.getType().equals(Constants.TYPE_PAST)
                                                && template.sizeOfUnifiers() == 0) {
                                            /*
                                             * Code for revising timing constraint of other predicates in template
                                             * considering this predicate as the unconstrained predicate
                                             */
                                        }
                                    }
                                    template.addVarsToUnifiers(ucurr);
//                                    logger.debug(template.getUnifiers().size()
//                                            + " has added to unifiers of the template");
//                                    Set set = template.getUnifiers().entrySet();
//                                    Iterator i = set.iterator();
//                                    while (i.hasNext()) {
//                                        Map.Entry me = (Map.Entry) i.next();
//                                        logger.debug("\t" + me.getKey() + " : "
//                                                + ((Variable) me.getValue()).toString(true));
//                                    }
                                    template.setUpdated(true);
                                }
                            }
                            else { // event at P.constraint.stopTime > t >
                                // predicate.timeStamp + minT
                                if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                                    if (pred.getEcName().equals(Constants.PRED_RELATION)) {
                                        pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                                        if (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)
                                                && predCounter < template.bodySize()) {
                                            template.setRemovable(true);
                                            templateOk = false;
                                            //logger.debug(".... setRemovable test7");
                                        }
                                    }
                                    else {
                                        if (pred.getEcName().equals(Constants.PRED_INITIATES))
                                            pred.evaluateInitiatesAndTerminatesPredicates(ufp, template.getUnifiers(),
                                                    template.getTimeVarMap());
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                        else {
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                        }
                                    }
                                    if (pred.getID().equals(""))
                                        pred.setID("NF_ID");
                                    pred.setTime(event.getTimestamp() - Constants.minT);
                                    if (pred.getSource().equals(Constants.SOURCE_UD))
                                        pred.setSource(Constants.SOURCE_NF);
                                    template.addTimeVarToMap(pred.getTimeVar());
                                    template.setUpdated(true);
                                    template.setEITLast(event.getTimestamp());
                                    if (template.getType().equals(Constants.TYPE_PAST)
                                            && template.sizeOfUnifiers() == 0) {
                                        /*
                                         * Code for revising timing constraint of other predicates in template
                                         * considering this predicate as the unconstrained predicate
                                         */
                                    }
                                    template.addVarsToUnifiers(ucurr);
//                                    logger.debug(template.getUnifiers().size()
//                                            + " has added to unifiers of the template");
//                                    Set set = template.getUnifiers().entrySet();
//                                    Iterator i = set.iterator();
//                                    while (i.hasNext()) {
//                                        Map.Entry me = (Map.Entry) i.next();
//                                        logger.debug("\t" + me.getKey() + " : "
//                                                + ((Variable) me.getValue()).toString(true));
//                                    }
                                }// end of NOT partial

                            }// end of E.t = P.timeStamp + minT
                        }// end of E.t < P.constraint.stopTime
                        else {// E.t >= P.constraint.stopTime
                            if (pred.getTime() == pred.getRangeUB() - Constants.minT
                                    || (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0)) {
                                // P.timeStamp.value = P.constraint.UB.value-mint
                                if (pred.isNegated()) {
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    if (predCounter < template.bodySize()) {
                                        template.setRemovable(true);
                                        templateOk = false;
                                        //logger.debug(".... setRemovable test8");
                                    }
                                }
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                pred.setID(event.getID());
                                pred.setSource(Constants.SOURCE_RE);
                                template.addTimeVarToMap(pred.getTimeVar());
                                if (event.getEcName().equals(Constants.PRED_HAPPENS)
                                        && (event.getPrefix().equals("ic") || event.getPrefix().equals("ir")
                                                || event.getPrefix().equals("rc") || event.getPrefix().equals("as")))
                                    template.updateIDs(event);
                                template.setUpdated(true);
                                if (template.getType().equals(Constants.TYPE_PAST) && template.sizeOfUnifiers() == 0) {
                                    /*
                                     * Code for revising timing constraint of other predicates in template considering
                                     * this predicate as the unconstrained predicate
                                     */
                                }
                            }// end of P.timeStamp.value = P.constraint.UB.value-mint
                            else {// P.timeStamp < P.constraint.stopTime - minT
                                if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                                    if (pred.getEcName().equals(Constants.PRED_RELATION)) {
                                        pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                                        if (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)
                                                && predCounter < template.bodySize()) {
                                            template.setRemovable(true);
                                            templateOk = false;
                                            //logger.debug(".... setRemovable test9");
                                        }
                                    }
                                    else {
                                        if (pred.getEcName().equals(Constants.PRED_INITIATES))
                                            pred.evaluateInitiatesAndTerminatesPredicates(ufp, template.getUnifiers(),
                                                    template.getTimeVarMap());
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                        else {
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                        }
                                    }
                                    if (pred.getID().equals(""))
                                        pred.setID("NF_ID");
                                    pred.setTime(event.getTimestamp() - Constants.minT);
                                    if (pred.getSource().equals(Constants.SOURCE_UD))
                                        pred.setSource(Constants.SOURCE_NF);
                                    template.addTimeVarToMap(pred.getTimeVar());
                                    template.setUpdated(true);
                                    template.setEITLast(event.getTimestamp());
                                    if (template.getType().equals(Constants.TYPE_PAST)
                                            && template.sizeOfUnifiers() == 0) {
                                        /*
                                         * Code for revising timing constraint of other predicates in template
                                         * considering this predicate as the unconstrained predicate
                                         */
                                    }
                                    template.addVarsToUnifiers(ucurr);
//                                    logger.debug(template.getUnifiers().size()
//                                            + " has added to unifiers of the template");
//                                    Set set = template.getUnifiers().entrySet();
//                                    Iterator i = set.iterator();
//                                    while (i.hasNext()) {
//                                        Map.Entry me = (Map.Entry) i.next();
//                                        logger.debug("\t" + me.getKey() + " : "
//                                                + ((Variable) me.getValue()).toString(true));
//                                    }
                                }// end of NOT partial

                                else {// pred is partial
                                    if (!template.getType().equals(Constants.TYPE_PAST)) {
                                        template.setRemovable(true);
                                        templateOk = false;
                                        logger.debug(".... setRemovable test10");
                                    }
                                }// end of partial

                            }// end of P.timeStamp < P.constraint.stopTime - minT
                        }// end of E.t >= P.constraint.stopTime
                    }// end of universal quantifier

                    // Considering past events if the predicate truth value is
                    // still undefined
                    if (pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                        // storing the current event as past event
                        // if the event can be unified with the predicate,
                        // but was not fed because of time constraints
                        if (ucurr.size() > 0)
                            pastEventStorer.addEvent(event, template, pred);

                        // finding past event to update the predicate
                        if (pred.isRangeSet() && event.getTimestamp() >= pred.getRangeUB()) {
                            LogEvent pastEvent = pastEventStorer.getMtachedEvent(pred);
                            if (pastEvent != null) {
                                HashMap pastMgu = pred.getCurrentMGU(pastEvent, ufp);
                                if (pastMgu.size() > 0) {
                                    /*
                                     * HashMap pastDiff = mapDifference(pastMgu, ufp); if(pastDiff.size() > 0){ Template
                                     * nTemplate = new Template(template); newTemplates.add(nTemplate); }
                                     */
                                    if (pastEvent.isNegated()) {
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                        else
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    }
                                    else {
                                        if (pred.isNegated())
                                            pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                        else
                                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                    }
                                    pred.setID(pastEvent.getID());
                                    pred.setSource(Constants.SOURCE_RE);
                                    pred.setTime(pastEvent.getTimestamp());
                                    if (pastMgu.size() > 0)
                                        template.addVarsToUnifiers(pastMgu);
                                    template.addTimeVarToMap(pred.getTimeVar());
//                                    logger.debug(template.getUnifiers().size()
//                                            + " has added to unifiers of the template");
//                                    Set set = template.getUnifiers().entrySet();
//                                    Iterator i = set.iterator();
//                                    while (i.hasNext()) {
//                                        Map.Entry me = (Map.Entry) i.next();
//                                        logger.debug("\t" + me.getKey() + " : "
//                                                + ((Variable) me.getValue()).toString(true));
//                                    }
                                    if (pastEvent.getEcName().equals(Constants.PRED_HAPPENS)
                                            && (pastEvent.getPrefix().equals("ic")
                                                    || pastEvent.getPrefix().equals("ir")
                                                    || pastEvent.getPrefix().equals("rc") || pastEvent.getPrefix()
                                                    .equals("as")))
                                        template.updateIDs(pastEvent);
                                    template.setUpdated(true);
                                }
                            }
                            else {// past event not found, set the truth value
                                // applying NF
                                if (pred.isNegated())
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                pred.setSource(Constants.SOURCE_NF);
                                template.setUpdated(true);
                                pred.setTime(event.getTimestamp());
                            }
                        }
                    } // end of past event consideration
                }// end of Happens predicate check
                else if (pred.getEcName().equals(Constants.PRED_HOLDSAT)
                        && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {// consider
                    // holdsAt
                    // predicate
                    if (pred.isRangeSet() && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                        logger.debug("HoldsAt predicate is being processed");
                        if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                            logger.debug("The holdsAt pred is fully unified");
                            Fluent fluent = new Fluent(pred.getFluent());
                            ArrayList<Variable> fVars = new ArrayList<Variable>();
                            fVars.addAll(fluent.getArguments());

                            fluent.getArguments().clear();
                            // for the time being we are presuming that time
                            // variables
                            // are not used in the fluent
                            for (int i = 0; i < fVars.size(); i++) {
                                Variable fVar = fVars.get(i);
                                logger.debug("fVar: " + fVar.toString(true));
                                if (fVar.getType().equals(Constants.DATA_TYPE_CONST))
                                    fluent.addArgument(new Variable(fVar));
                                else {
                                    Variable rVar =
                                            (Variable) template.getUnifiers().get(fVar.getName() + fVar.getType());
                                    logger.debug("rVar: " + rVar.toString(true));
                                    if (rVar != null && rVar.getName().equals(fVar.getName()))
                                        fVar.setValue(rVar.getValue());
                                    fluent.addArgument(new Variable(fVar));
                                }
                            }
                            /**
                             * ATTENTION: can the pred time be equal to -1 in some cases?
                             */
                            ArrayList<Predicate> initiates = fluentStorer.getInitiatesBefore(pred.getTime(), fluent);
                            logger.debug("find the fluent: \n" + fluent.toString());
                            if (initiates.size() > 0) {
                                logger.debug("There are " + initiates.size() + " different initiations of the fluent "
                                        + fluent.toString());
                                Predicate terminate =
                                        fluentStorer.getTerminateAfter(initiates.get(initiates.size() - 1).getTime(),
                                                fluent);
                                if (terminate == null || terminate.getTime() > pred.getTime()) {
                                    if (pred.isNegated())
                                        pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    else
                                        pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                    logger.debug("the fluent exists");
                                    pred.setSource(Constants.SOURCE_RE);
                                    template.setUpdated(true);
                                    pred.setTime(pred.getRangeLB());
                                    // ATTENTION: adding fluent variables to
                                    // unifiers
                                    for (int i = 0; i < fVars.size(); i++) {
                                        Variable fVar = (Variable) fVars.get(i);
                                        logger.debug("Analyzer: HoldsAt block: fVar: " + fVar.getValue());
                                        template.addVarToUnifiers(fVar);
                                    }
                                    logger.debug(template.getUnifiers().size()
                                            + " has added to unifiers of the template");
                                    Set set = template.getUnifiers().entrySet();
                                    Iterator i = set.iterator();
                                    while (i.hasNext()) {
                                        Map.Entry me = (Map.Entry) i.next();
                                        logger.debug("\t" + me.getKey() + " : "
                                                + ((Variable) me.getValue()).toString(true));
                                    }

                                }
                                else {// fluent was terminated
                                    if (pred.isNegated())
                                        pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                    else
                                        pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    pred.setSource(Constants.SOURCE_RE);
                                    template.setUpdated(true);
                                    pred.setTime(pred.getRangeLB());
                                }

                            }
                            else {// fluent was not initiated
                                if (pred.isNegated())
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                pred.setSource(Constants.SOURCE_RE);
                                template.setUpdated(true);
                                pred.setTime(pred.getRangeLB());
                            }
                        }// end of fully unified HoldsAt pred
                        else {
                            logger.debug("The holdsAt pred is partially unified");
                            Fluent fluent = new Fluent(pred.getFluent());
                            ArrayList<Variable> fVars = new ArrayList<Variable>();
                            fVars.addAll(fluent.getArguments());

                            fluent.getArguments().clear();
                            // for the time being we are presuming that time
                            // variables
                            // are not used in the fluent
                            for (int i = 0; i < fVars.size(); i++) {
                                Variable fVar = fVars.get(i);
                                logger.debug("fVar: " + fVar.toString(true));
                                if (fVar.getType().equals(Constants.DATA_TYPE_CONST))
                                    fluent.addArgument(new Variable(fVar));
                                else {
                                    Variable rVar =
                                            (Variable) template.getUnifiers().get(fVar.getName() + fVar.getType());
                                    if (rVar != null) {
                                        logger.debug("rVar: " + rVar.toString(true));
                                        if (rVar.getName().equals(fVar.getName()))
                                            fVar.setValue(rVar.getValue());
                                    }
                                    fluent.addArgument(new Variable(fVar));
                                }
                            }
                            /**
                             * ATTENTION: can the pred time be equal to -1 in some cases?
                             */
                            ArrayList<Predicate> inits = fluentStorer.getAllInitiatedFluents(pred.getTime(), fluent);
                            if (inits.size() > 0) {
                                logger.debug("Holds AT 2.5 - there are " + inits.size() + " initiated fluents");
                                for (int n = 0; n < inits.size(); n++) {
                                    Fluent inFluent = new Fluent(inits.get(n).getFluent());
                                    ArrayList<Variable> inVars = inFluent.getArguments();
                                    Template nTemplate = new Template(template);
                                    Fluent nFluent = nTemplate.getPredicate(predCounter).getFluent();
                                    for (int m = 0; m < inVars.size(); m++) {
                                        Variable tempVar = (Variable) nFluent.getArguments().get(m);
                                        if (!tempVar.getType().contains(Constants.ARRAY)) {
                                            tempVar.setValue((Object) inVars.get(m).getValue());
                                        }
                                        else {
                                            tempVar.addValues(inVars.get(m).getValues());
                                        }
                                        nTemplate.addVarToUnifiers(tempVar);
                                        logger.debug(tempVar.toString(true)
                                                + " is added to unifiers because of the evaluation of "
                                                + pred.toString(true));
                                    }
                                    Predicate nPred = nTemplate.getPredicate(predCounter);
                                    if (nPred.isNegated())
                                        nPred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                    else
                                        nPred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                    nPred.setSource(Constants.SOURCE_RE);
                                    nPred.setTime(nPred.getRangeLB());
                                    nTemplate.setUpdated(true);
                                    newTemplates.add(nTemplate);
                                }
                                logger.debug(inits.size() + " new templates have been added");
                                // ATTENTION: If the initial template is an
                                // empty template, it nust not be removed!!!!
                                if (template.getUnifiers().size() > 0) {
                                    template.setRemovable(true);
                                    templateOk = false;
                                }
                            }
                            else {// fluent has not been initiated or was
                                // terminated
                            	//logger.debug("4444444444 Fluent not found for " + fluent.getFluentName() + " and time " + pred.getTime());
                                if (pred.isNegated())
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                pred.setSource(Constants.SOURCE_RE);
                                template.setUpdated(true);
                                pred.setTime(pred.getRangeLB());
                            }
                        }// end of partially unified HoldsAt pred
                    }
                }// end of HoldsAt predicate
            }// end of p.status == UK
        }// end for

        // update template id
        // this.updateTemplateId(template);
    }

    /**
     * evaluator of the relational predicates of the given template.
     * 
     * @param template
     */
    private void evaluateRelationalPredicates(Template template) {
        boolean templateOk = true;
        for (int predCounter = 0; predCounter < template.bodySize() && templateOk; predCounter++) {
            Predicate pred = template.getPredicate(predCounter);
            if (pred.getEcName().equals(Constants.PRED_RELATION) && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                HashMap ufp = template.getProjection(pred);
                ufp.putAll(this.getProjection(pred));
                if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                    pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                    pred.setTime(System.currentTimeMillis());
                    template.setUpdated(true);
                }
            }
        }

        evaluateConstrainedPredicatesSubformulas(template);

        if (template.isBodyTrue()) {
            for (int p = template.bodySize(); p < template.totalPredicates(); p++) {
                Predicate pred = template.getPredicate(p);
                if (pred.getEcName().equals(Constants.PRED_RELATION)
                        && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                    HashMap ufp = template.getProjection(pred);
                    ufp.putAll(this.getProjection(pred));
                    if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                        pred.evaluateRelationalPredicates(ufp, template.getTimeVarMap());
                        pred.setTime(System.currentTimeMillis());
                        template.setUpdated(true);
                    }
                }
            }
        }
    }

    /**
     * It evaluates the truth value of: i) the subformulas constrained predicates individually and ii) the given
     * template subformulas as a whole.
     * 
     * @param template
     */
    private void evaluateConstrainedPredicatesSubformulas(Template template) {

        for (ConstrainedPredicatesSubformula subformula : template.getConstrainedPredicatesSubformulasList()) {
            if (subformula.getTruthValue().equalsIgnoreCase(Constants.TRUTH_VAL_UK)) {

                // check whether the subformula is fully unified
                // if yes, go on with its truth value establishment

                boolean fullyUnified = true;
                for (Predicate constrainedPred : subformula.getConstrainedPredicates()) {
                    HashMap ufp = template.getProjection(constrainedPred);
                    if (constrainedPred.isPartial(ufp, template.getTimeVarMap())) {
                        fullyUnified = false;
                        break;
                    }
                }
                if (fullyUnified) {
                    for (Predicate constraint : subformula.getConstraints()) {
                        HashMap ufp = template.getProjection(constraint);
                        ufp.putAll(this.getProjection(constraint));
                        if (constraint.isPartial(ufp, template.getTimeVarMap())) {
                            fullyUnified = false;
                            break;
                        }
                    }
                }

                if (fullyUnified) {
                    boolean subformulaConstraintsTruthValue = true;
                    if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.BODY)) {
                        for (Predicate bodyPredicate : template.getBody()) {
                            if (subformula.getConstraints().contains(bodyPredicate)
                                    && bodyPredicate.getTruthVal().equalsIgnoreCase(Constants.TRUTH_VAL_FALSE)) {
                                subformulaConstraintsTruthValue = false;
                                break;
                            }
                        }
                    }
                    else if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.HEAD)) {
                        for (Predicate headPredicate : template.getHead()) {
                            if (subformula.getConstraints().contains(headPredicate)
                                    && headPredicate.getTruthVal().equalsIgnoreCase(Constants.TRUTH_VAL_FALSE)) {
                                subformulaConstraintsTruthValue = false;
                                break;
                            }
                        }
                    }

                    for (Predicate constrainedPredicate : subformula.getConstrainedPredicates()) {
                        if (subformulaConstraintsTruthValue) {
                            if (!constrainedPredicate.isNegated()) {
                                constrainedPredicate.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            }
                            else {
                                constrainedPredicate.setTruthVal(Constants.TRUTH_VAL_FALSE);
                            }
                        }
                        else {
                            if (!constrainedPredicate.isNegated()) {
                                constrainedPredicate.setTruthVal(Constants.TRUTH_VAL_FALSE);
                            }
                            else {
                                constrainedPredicate.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            }
                        }
                    }
                    if (subformulaConstraintsTruthValue) {
                        if (!subformula.isNegated()) {
                            subformula.setTruthValue(Constants.TRUTH_VAL_TRUE);
                        }
                        else {
                            subformula.setTruthValue(Constants.TRUTH_VAL_FALSE);
                        }
                    }
                    else {
                        if (!subformula.isNegated()) {
                            subformula.setTruthValue(Constants.TRUTH_VAL_FALSE);
                        }
                        else {
                            subformula.setTruthValue(Constants.TRUTH_VAL_TRUE);
                        }
                    }
                }
            }
        }
    }

    private void updateStaticVars(Template template) {
        HashMap unifiers = template.getUnifiers();
        Iterator it = unifiers.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Variable ufVar = (Variable) unifiers.get(key);
            if (ufVar.isStatic()) {
                if (!staticVars.containsKey(key)) {
                    staticVars.put(key, ufVar);
                }
                else {
                    Variable staticVar = (Variable) staticVars.get(key);
                    if (!ufVar.getType().contains(Constants.ARRAY)) {
                        if (!ufVar.getObjValue().equals(staticVar.getObjValue())) {
                            staticVar.setValue((Object) ufVar.getValue());
                        }
                    }
                    else {
                        staticVar.addValues(ufVar.getValues());
                    }
                }
            }
        }
    }

    /**
     * Checks whether the body happens predicates are evaluated.
     * 
     * @param template
     * @return
     */
    private boolean happensBodyPredicatesEvaluated(Template template) {
        boolean evaluated = true;

        for (Predicate pred : template.getBody()) {
            if (pred.getEcName().equals(Constants.PRED_HAPPENS) && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                evaluated = false;
                break;
            }
        }

        return evaluated;
    }

    /**
     * Evaluates the body holdsAt predicates of the given template only if the body happens predicates are evaluated.
     * 
     * @param template
     */
    private void evaluateBodyHoldsAtPredicates(Template template) {

        if (happensBodyPredicatesEvaluated(template)) {
            for (int predCounter = 0; predCounter < template.getBody().size(); predCounter++) {
                Predicate pred = template.getPredicate(predCounter);
                if (pred.getEcName().equals(Constants.PRED_HOLDSAT)
                        && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {

                    // update the range set
                    pred.updateRange(template.getTimeVarMap());

                    // the projection of unifiers over the variables of a
                    // predicate
                    HashMap ufp = template.getProjection(pred);

                    //logger.debug("HoldsAt predicate is being processed");
                    if (!pred.isPartial(ufp, template.getTimeVarMap())) {
                        //logger.debug("The holdsAt pred is fully unified");
                        Fluent fluent = new Fluent(pred.getFluent());
                        ArrayList<Variable> fVars = new ArrayList<Variable>();
                        fVars.addAll(fluent.getArguments());

                        fluent.getArguments().clear();
                        // for the time being we are presuming that time
                        // variables are not used in the fluent
                        for (int i = 0; i < fVars.size(); i++) {
                            Variable fVar = fVars.get(i);
                            //logger.debug("fVar: " + fVar.toString(true));
                            if (fVar.getType().equals(Constants.DATA_TYPE_CONST))
                                fluent.addArgument(new Variable(fVar));
                            else {
                                Variable rVar = (Variable) template.getUnifiers().get(fVar.getName() + fVar.getType());
                                //logger.debug("rVar: " + rVar.toString(true));
                                if (rVar != null && rVar.getName().equals(fVar.getName()))
                                    fVar.setValue(rVar.getValue());
                                fluent.addArgument(new Variable(fVar));
                            }
                        }
                        /**
                         * ATTENTION: can the pred time be equal to -1 in some cases?
                         */
                        ArrayList<Predicate> initiates = fluentStorer.getInitiatesBefore(pred.getTime(), fluent);
                        //logger.debug("find the fluent: \n" + fluent.toString());
                        if (initiates.size() > 0) {
//                            logger.debug("There are " + initiates.size() + " different initiations of the fluent "
//                                    + fluent.toString());
                            Predicate terminate =
                                    fluentStorer.getTerminateAfter(initiates.get(initiates.size() - 1).getTime(),
                                            fluent);
                            if (terminate == null || terminate.getTime() > pred.getTime()) {
                                if (pred.isNegated())
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                //logger.debug("the fluent exists");
                                pred.setSource(Constants.SOURCE_RE);
                                template.setUpdated(true);
                                pred.setTime(pred.getRangeLB());
                                // ATTENTION: adding fluent variables to
                                // unifiers
                                for (int i = 0; i < fVars.size(); i++) {
                                    Variable fVar = (Variable) fVars.get(i);
                                    //logger.debug("Analyzer: HoldsAt block: fVar: " + fVar.getValue());
                                    template.addVarToUnifiers(fVar);
                                }
//                                logger.debug(template.getUnifiers().size() + " has added to unifiers of the template");
//                                Set set = template.getUnifiers().entrySet();
//                                Iterator i = set.iterator();
//                                while (i.hasNext()) {
//                                    Map.Entry me = (Map.Entry) i.next();
//                                    logger
//                                            .debug("\t" + me.getKey() + " : "
//                                                    + ((Variable) me.getValue()).toString(true));
//                                }

                            }
                            else {// fluent was terminated
                                if (pred.isNegated())
                                    pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                else
                                    pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                pred.setSource(Constants.SOURCE_RE);
                                template.setUpdated(true);
                                pred.setTime(pred.getRangeLB());
                            }

                        }
                        else {// fluent was not initiated
                            if (pred.isNegated())
                                pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            else
                                pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                            pred.setSource(Constants.SOURCE_RE);
                            template.setUpdated(true);
                            pred.setTime(pred.getRangeLB());
                        }
                    }// end of fully unified HoldsAt pred
                    else {
                        //logger.debug("The holdsAt pred is partially unified");
                        Fluent fluent = new Fluent(pred.getFluent());
                        ArrayList<Variable> fVars = new ArrayList<Variable>();
                        fVars.addAll(fluent.getArguments());

                        fluent.getArguments().clear();
                        // for the time being we are presuming that time
                        // variables are not used in the fluent
                        for (int i = 0; i < fVars.size(); i++) {
                            Variable fVar = fVars.get(i);
                            //logger.debug("fVar: " + fVar.toString(true));
                            if (fVar.getType().equals(Constants.DATA_TYPE_CONST))
                                fluent.addArgument(new Variable(fVar));
                            else {
                                Variable rVar = (Variable) template.getUnifiers().get(fVar.getName() + fVar.getType());
                                if (rVar != null) {
                                    //logger.debug("rVar: " + rVar.toString(true));
                                    if (rVar.getName().equals(fVar.getName()))
                                        fVar.setValue(rVar.getValue());
                                }
                                fluent.addArgument(new Variable(fVar));
                            }
                        }
                        /**
                         * ATTENTION: can the pred time be equal to -1 in some cases?
                         */
                        ArrayList<Predicate> inits = fluentStorer.getAllInitiatedFluents(pred.getTime(), fluent);
                        
                        if (inits.size() > 0) {
                            //logger.debug("there are " + inits.size() + " initiated fluents");
                            for (int n = 0; n < inits.size(); n++) {
                                Fluent inFluent = new Fluent(inits.get(n).getFluent());
                                ArrayList<Variable> inVars = inFluent.getArguments();
                                Template nTemplate = new Template(template);
                                Fluent nFluent = nTemplate.getPredicate(predCounter).getFluent();
                                for (int m = 0; m < inVars.size(); m++) {
                                    Variable tempVar = (Variable) nFluent.getArguments().get(m);
                                    if (!tempVar.getType().contains(Constants.ARRAY)) {
                                        tempVar.setValue((Object) inVars.get(m).getValue());
                                    }
                                    else {
                                        tempVar.addValues(inVars.get(m).getValues());
                                    }
                                    nTemplate.addVarToUnifiers(tempVar);
//                                    logger.debug(tempVar.toString(true)
//                                            + " is added to unifiers because of the evaluation of "
//                                            + pred.toString(true));
                                }
                                Predicate nPred = nTemplate.getPredicate(predCounter);
                                if (nPred.isNegated())
                                    nPred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                                else
                                    nPred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                                nPred.setSource(Constants.SOURCE_RE);
                                nPred.setTime(nPred.getRangeLB());
                                nTemplate.setUpdated(true);
                                newTemplates.add(nTemplate);
                            }
                            //logger.debug(inits.size() + " new templates have been added");
                            // ATTENTION: If the initial template is an
                            // empty template, it nust not be removed!!!!
                            if (template.getUnifiers().size() > 0) {
                                template.setRemovable(true);
                            }
                        }
                        else {// fluent has not been initiated or was
                            // terminated
                            if (pred.isNegated())
                                pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            else
                                pred.setTruthVal(Constants.TRUTH_VAL_FALSE);
                            pred.setSource(Constants.SOURCE_RE);
                            template.setUpdated(true);
                            pred.setTime(pred.getRangeLB());
                        }
                    }// end of partially unified HoldsAt pred
                }
            }
        }

    }

    public boolean isBusy(){
    	return busy;
    }
}
