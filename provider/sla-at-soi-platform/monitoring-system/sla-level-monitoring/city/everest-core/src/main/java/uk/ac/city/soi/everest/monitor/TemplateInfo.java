/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * TemplateInfo.java
 *
 * Created on 01 September 2004, 11:57
 */

package uk.ac.city.soi.everest.monitor;

import java.io.Serializable;

/**
 * This class is used as a wrapper class to share information among SeCSEAnalyzer, SeCSEEventGen and SeCSEConChecker.
 * This class wraps a Template object, current monitoring time and the monitoring mode.
 * 
 * @author am697
 */
public class TemplateInfo implements Serializable {
    /*
     * Stores the reference of a template
     */
    private Template template;
    /*
     * Stores the current time of the monitor
     */
    private long time;
    /*
     * Stores the monitoring mode
     */
    private int rOrm;

    /**
     * Creates a new instance of TemplateInfo.
     */
    public TemplateInfo(Template template, long time, int rOrm) {
        this.template = template;
        this.time = time;
        this.rOrm = rOrm;
    }

    /**
     * Returns the current monitoring time.
     * 
     * @return
     */
    public long getTime() {
        return time;
    }

    /**
     * Returns the template.
     * 
     * @return
     */
    public Template getTemplate() {
        return template;
    }

    /**
     * Returns the monitoring mode.
     * 
     * @return
     */
    /*
    public int getMode() {
        return rOrm;
    }
	*/
    /**
     * Checks whether the referenced template is mixed.
     * 
     * @return
     */
    /*
    public boolean isMixed() {
        return rOrm > 0 ? true : false;
    }
    */
}
