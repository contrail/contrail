/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.template;

import java.util.ArrayList;

import uk.ac.city.soi.everest.database.EntityManagerInterface;
import uk.ac.city.soi.everest.monitor.Template;

/**
 * Interface providing methods for managing TemplateEntity.
 * 
 */
public interface TemplateEntityManagerInterface extends EntityManagerInterface {

    /**
     * Loads templates for given formula Id and mode.
     * 
     * @param formulaId
     * @param mode
     * @return
     */
    public ArrayList<Template> loadTemplatesF(String formulaId, int mode);

    /**
     * Loads templates for given formula Id and mode.
     * 
     * @param formulaId
     * @param mode
     * @return
     */
    public ArrayList<Template> loadTemplatesG(String formulaId, int mode);

    /**
     * Stores template objects.
     * 
     * @param templates
     * @param mode
     */
    public void storeTemplates(ArrayList<Template> templates, int mode);

    /**
     * Updates the entity with given template objects, for given key and mode.
     * 
     * @param templates
     * @param key
     * @param mode
     */
    public void updatedTemplatesF(ArrayList<Template> templates, String key, int mode);

    /**
     * Updates the entity with given template objects, for given key and mode.
     * 
     * @param templates
     * @param key
     * @param mode
     */
    public void updatedTemplatesG(ArrayList<Template> templates, String key, int mode);

    /**
     * Checks whether there are more templates for given formula id and mode.
     * 
     * @param formulaId
     * @param mode
     * @return
     */
    public boolean hasMoreF(String formulaId, int mode);

    /**
     * Checks whether there are more templates for given formula id and mode.
     * 
     * @param formulaId
     * @param mode
     * @return
     */
    public boolean hasMoreG(String formulaId, int mode);

    /**
     * Resets template counter.
     */
    public void resetF();

    /**
     * Resets template counter.
     */
    public void resetG();

    /**
     * Copies file containing template objects.
     * 
     * @param newName
     */
    public void copyFile(String newName);

    /**
     * Closes file containing template objects.
     */
    public void close();

    /**
     * Returns the folder that files containing template objects are stored.
     * 
     * @return
     */
    public String getFolder();

    /**
     * Returns a list of template ids for a given mode.
     * 
     * @param mode
     * @return
     */
    public ArrayList<String> getTempIds(int mode);

    /**
     * Returns a list of templates for a given mode.
     * 
     * @param mode
     * @return
     */
    public ArrayList<Template> selectByMode(int mode);

    /**
     * Returns a list of templates for a given status.
     * 
     * @param status
     * @return
     */
    public ArrayList<Template> selectByStatus(String status);

    /**
     * Returns the number of recorded existing for given formulaId and mode.
     * 
     * @param formulaId
     * @param mode
     * @return
     */
    public int getCount(String formulaId, int mode);

    /**
     * Deletes the record of a given template and mode.
     * 
     * @param template
     */
    public void delete(Template template, int mode);

    /**
     * Returns a template for given templateId and mode.
     * 
     * @param templateId
     * @param mode
     * @return
     */
    public Template selectByTemplateIdAndMode(String templateId, int mode);

    /**
     * Returns a collection of monitoring results selected by time stamp and template id.
     * 
     * @param withStatus
     * @param afterTimepoint
     * @param includingInTemplateId
     * @return
     */
    public ArrayList<Template> getMonitoringResults(String withStatus, long afterTimepoint, String includingInTemplateId);
}
