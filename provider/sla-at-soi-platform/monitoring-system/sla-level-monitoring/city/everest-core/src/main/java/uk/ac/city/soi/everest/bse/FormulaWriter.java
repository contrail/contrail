/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * FormulaWriter.java
 *
 * Created on 28 July 2004, 16:38
 */

package uk.ac.city.soi.everest.bse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.TimeExpression;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * This class generates Serenity formula in XML from Formula object.
 * 
 * @author Khaled Mahbub
 */
public class FormulaWriter {
    // logger
    private static Logger logger = Logger.getLogger(FormulaWriter.class);

    Document document;
    private int idCounter = 1;

    /**
     * Creates a new instance of FormulaWriter.
     * 
     */
    public FormulaWriter() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument();
        }
        catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            pce.printStackTrace();
        }
    }

    /**
     * Generates TimeVariable element in XML formula.
     * 
     * @param name -
     *            name of the time variable
     * @param value -
     *            value of the time variable
     * @param tagName -
     *            tagName is used to use this method to generate TimeVariable element in different cases. For example
     *            inside Happens, tag name of TimeVariable is TimeVar, but inside TimeExpression element tag name of
     *            Timevariable is Time
     * @return
     */
    private Element getTimeVariable(String name, long value, String tagName) {
        Element var = null;
        try {
            var = document.createElement(tagName);
            Element varName = document.createElement("varName");
            varName.appendChild(document.createTextNode(name));
            var.appendChild(varName);
            Element varType = document.createElement("varType");
            varType.appendChild(document.createTextNode("TimeVariable"));
            var.appendChild(varType);
            if (value != Constants.TIME_UD && value != Constants.RANGE_UB && value != Constants.RANGE_LB) {
                Element timeValue = document.createElement("value");
                timeValue.appendChild(document.createTextNode(String.valueOf(value)));
                var.appendChild(timeValue);
            }
        }
        catch (Exception exp) {
        }
        return var;
    }

    /**
     * This method is used to generate Variable element in XML formula.
     * 
     * @param var -
     *            Variable object for which the element will be generated
     * @param tagName -
     *            tagName was used to generate Variable element in different cases (probably not used at the end, due to
     *            change in XML schema)
     */
    private Element getVariable(Variable var, String tagName) {
        Element varElem = null;
        try {
            varElem = document.createElement(tagName);
            if (var.isStatic())
                varElem.setAttribute("persistent", "true");
            else
                varElem.setAttribute("persistent", "false");
            if (!var.getConstName().equals("")) {
                varElem.setAttribute("forMatching", "true");
                Element varName = document.createElement("varName");
                varName.appendChild(document.createTextNode(var.getConstName()));
                varElem.appendChild(varName);
            }
            else {
                varElem.setAttribute("forMatching", "false");
                Element varName = document.createElement("varName");
                varName.appendChild(document.createTextNode(var.getName()));
                varElem.appendChild(varName);
            }
            if (var.getType().endsWith("Array")) {
                Element array = document.createElement("array");
                Element arrayType = document.createElement("type");
                arrayType.appendChild(document.createTextNode(var.getType()));
                array.appendChild(arrayType);
                /*
                 * if(!var.getArrayIndexName().equals("")){ Element index = document.createElement("index");
                 * index.appendChild(document.createTextNode(var.getArrayIndexName())); array.appendChild(index); }
                 */
                ArrayList values = var.getValues();
                for (int i = 0; i < values.size(); i++) {
                    String val = (String) values.get(i);
                    Element value = document.createElement("value");
                    Element indexValue = document.createElement("indexValue");
                    indexValue.appendChild(document.createTextNode(String.valueOf(i)));
                    value.appendChild(indexValue);
                    Element cellValue = document.createElement("cellValue");
                    cellValue.appendChild(document.createTextNode(val));
                    value.appendChild(cellValue);
                    array.appendChild(value);
                }
                varElem.appendChild(array);
            }
            else {
                Element varType = document.createElement("varType");
                varType.appendChild(document.createTextNode(var.getType()));
                varElem.appendChild(varType);
                if (var.getRealValue() != null) {
                    Element varValue = document.createElement("value");
                    varValue.appendChild(document.createTextNode(var.getRealValue().toString()));
                    varElem.appendChild(varValue);
                }
            }

        }
        catch (Exception exp) {
        }
        return varElem;
    }

    /**
     * This method is used to generate TimeExpression element in XML formula.
     * 
     * @param jTex -
     *            TimeExpression object for which the element will be generated
     * @param tagName -
     *            tagName was used to generate Expressionvariable element in different cases. For example inside
     *            Happens, tag name for the starting time expression in the range is fromTime, and the ending time is
     *            toTime
     * 
     */
    private Element getTimeExpression(TimeExpression jTex, String tagName) {
        Element tex = null;
        try {
            tex = document.createElement(tagName);
            Element time = getTimeVariable(jTex.getTimeVarName(), jTex.getTimeValue(), "time");
            tex.appendChild(time);
            if (jTex.getNumber() != 0) {
                Element operator = null;
                if (jTex.getOperator().equals(Constants.PLUS))
                    operator = document.createElement("plus");
                else
                    operator = document.createElement("minus");
                operator.appendChild(document.createTextNode(String.valueOf(jTex.getNumber())));
                tex.appendChild(operator);
            }
        }
        catch (Exception exp) {
        }
        return tex;
    }

    /**
     * Genreates re_term or ic_term element in XML formula
     * 
     * @param operation -
     *            name of the operation in re_term or ic_term
     * @param partner -
     *            name of the partner in re_term or ic_term
     * @param variables -
     *            variables inside re_term or ic_term
     * @param tagName -
     *            name of the tag, i.e. re_term or ic_term
     * @return
     */
    private Element getReIcTerm(String operation, String partner, ArrayList variables, String tagName) {
        Element term = null;
        try {
            term = document.createElement(tagName);
            Element opnName = document.createElement("operationName");
            opnName.appendChild(document.createTextNode(operation));
            term.appendChild(opnName);
            Element partnerName = document.createElement("partnerName");
            partnerName.appendChild(document.createTextNode(partner));
            term.appendChild(partnerName);
            Element id = document.createElement("id");
            id.appendChild(document.createTextNode("vID" + idCounter));
            // id.appendChild(document.createTextNode(predId));
            term.appendChild(id);
            idCounter++;
            for (int i = 0; i < variables.size(); i++) {
                Variable ecVar = (Variable) variables.get(i);
                // Element var = getVariable(ecVar.getName(), ecVar.getType(), ecVar.getValue(), "variable");
                Element var = getVariable(ecVar, "variable");
                term.appendChild(var);
            }
        }
        catch (Exception exp) {
        }
        return term;
    }

    /**
     * Genreates rc_term or ir_term element in XML formula
     * 
     * @param operation -
     *            name of the operation in rc_term or ir_term
     * @param partner -
     *            name of the partner in rc_term or ir_term
     * @param variables -
     *            variables inside rc_term or ir_term
     * @param tagName -
     *            name of the tag, i.e. rc_term or ir_term
     * @return
     */
    private Element getRcIrTerm(String operation, String partner, ArrayList variables, String tagName) {
        Element term = null;
        try {
            term = document.createElement(tagName);
            Element opnName = document.createElement("operationName");
            opnName.appendChild(document.createTextNode(operation));
            term.appendChild(opnName);
            Element partnerName = document.createElement("partnerName");
            partnerName.appendChild(document.createTextNode(partner));
            term.appendChild(partnerName);
            Element id = document.createElement("id");
            id.appendChild(document.createTextNode("vID" + idCounter));
            // id.appendChild(document.createTextNode(predId));
            term.appendChild(id);
            idCounter++;
            for (int i = 0; i < variables.size(); i++) {
                Variable ecVar = (Variable) variables.get(i);
                // Element var = getVariable(ecVar.getName(), ecVar.getType(), ecVar.getValue(), "variable");
                Element var = getVariable(ecVar, "variable");
                term.appendChild(var);
            }
        }
        catch (Exception exp) {
        }
        return term;
    }

    /**
     * This method is used to generate Function element in XML formula.
     * 
     * @param func -
     *            Function object for which the element will be generated
     * @param tagName -
     *            tagName was used to generate Function element in different cases (probably not used at the end, due to
     *            change in XML schema)
     */
    private Element getFunction(Function func, String tagName) {
        Element funcElem = null;
        try {
            funcElem = document.createElement(tagName);
            Element name = document.createElement("name");
            name.appendChild(document.createTextNode(func.getOperationName()));
            funcElem.appendChild(name);
            Element partner = document.createElement("partner");
            partner.appendChild(document.createTextNode(func.getPartner()));
            funcElem.appendChild(partner);
            LinkedHashMap params = func.getParameters();
            Iterator it = params.values().iterator();
            while (it.hasNext()) {
                Object obj = it.next();
                if (obj instanceof Variable) {
                    Variable var = (Variable) obj;
                    Element varElem = getVariable(var, "variable");
                    funcElem.appendChild(varElem);
                }
                else if (obj instanceof Function) {
                    Function paramFunc = (Function) obj;
                    Element paramFuncElem = getFunction(paramFunc, "operationCall");
                    funcElem.appendChild(paramFuncElem);
                }
            }

        }
        catch (Exception exp) {
        }
        return funcElem;
    }

    /**
     * Generates fluent element in XML formula.
     * 
     * @param fluent -
     *            Fluent object for which the element will be generated
     * @return
     */
    private Element getFluent(Fluent fluent) {
        Element flnt = null;
        try {
            flnt = document.createElement("fluent");
            flnt.setAttribute("name", fluent.getFluentName());

            // Element f = document.createElement(fluent.getFluentName());
            for (int i = 0; i < fluent.getArguments().size(); i++) {
                Object obj = fluent.getArguments().get(i);
                if (obj instanceof Variable) {
                    Variable tVar = ((Variable) obj).makeCopy();
                    flnt.appendChild(getVariable(tVar, Constants.VARIABLE));
                }
                else if (obj instanceof Function) {
                    Function func = (Function) obj;
                    flnt.appendChild(getFunction(func, Constants.OPERATION_CALL));
                }
            }
            // flnt.appendChild(f);

        }
        catch (Exception exp) {
        }
        return flnt;
    }

    /**
     * Generates happens element in XML formula.
     * 
     * @param pred -
     *            Predicate object for which the element will be generated
     * @return
     */
    private Element getHappens(Predicate pred) {
        Element happens = null;
        try {
            happens = document.createElement("happens");
            Element term = null;
            if (pred.getPrefix().equals("rc"))
                term = getRcIrTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "rc_term");
            else if (pred.getPrefix().equals("ir"))
                term = getRcIrTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "ir_term");
            else if (pred.getPrefix().equals("re"))
                term = getReIcTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "re_term");
            else if (pred.getPrefix().equals("ic"))
                term = getReIcTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "ic_term");
            happens.appendChild(term);
            Element timeVar = getTimeVariable(pred.getTimeVarName(), pred.getTimeVar().getValue(), "timeVar");
            happens.appendChild(timeVar);
            Element fromTime = getTimeExpression(pred.getLowBound(), "fromTime");
            happens.appendChild(fromTime);
            Element toTime = getTimeExpression(pred.getUpBound(), "toTime");
            happens.appendChild(toTime);
        }
        catch (Exception exp) {
        }
        return happens;
    }

    /**
     * Generates initiates/terminates element in XML formula.
     * 
     * @param pred -
     *            Predicate object for which the element will be generated
     * @param tagName -
     *            name of the tag, e.g. initiates or terminates
     * @return
     */
    private Element getInitiatesTerminates(Predicate pred, String tagName) {
        Element initiates = null;
        try {
            initiates = document.createElement(tagName);
            Element term = null;
            if (pred.getPrefix().equals("rc"))
                term = getRcIrTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "rc_term");
            else if (pred.getPrefix().equals("ir"))
                term = getRcIrTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "ir_term");
            else if (pred.getPrefix().equals("ic"))
                term = getReIcTerm(pred.getOperationName(), pred.getPartnerId(), pred.getVariables(), "ic_term");
            initiates.appendChild(term);
            Element fluent = getFluent(pred.getFluent());
            initiates.appendChild(fluent);
            Element timeVar = getTimeVariable(pred.getTimeVarName(), pred.getTimeVar().getValue(), "timeVar");
            initiates.appendChild(timeVar);
        }
        catch (Exception exp) {
        }
        return initiates;
    }

    /**
     * Generates holdsAt element in XML formula.
     * 
     * @param pred -
     *            Predicate object for which the element will be generated
     * @param tagName
     *            was used to generate Function element in different cases (probably not used at the end, due to change
     *            in XML schema)
     * @return
     */
    private Element getHoldsAt(Predicate pred, String tagName) {
        Element holds = null;
        try {
            holds = document.createElement(tagName);
            Element fluent = getFluent(pred.getFluent());
            holds.appendChild(fluent);
            Element timeVar = getTimeVariable(pred.getTimeVarName(), pred.getTimeVar().getValue(), "timeVar");
            holds.appendChild(timeVar);
        }
        catch (Exception exp) {
        }
        return holds;
    }

    /**
     * Generates constants element in XML formula.
     * 
     * @param var -
     *            Variable object that holds the constants and for which element will be generated
     * @return
     */
    private Element getConstant(Variable var) {
        Element constant = null;
        try {
            constant = document.createElement("constant");
            Element name = document.createElement("name");
            name.appendChild(document.createTextNode(var.getConstName()));
            Element value = document.createElement("value");
            value.appendChild(document.createTextNode(var.getRealValue().toString()));
            constant.appendChild(name);
            constant.appendChild(value);
        }
        catch (Exception exp) {
        }
        return constant;
    }

    /**
     * Generates RelationalPredicate element in XML formula.
     * 
     * @param pred -
     *            Predicate object for which the element will be generated. It should be noted that Predicate object is
     *            used to model relational predicate also (see documentation in FormulaReader class)
     * @return
     */
    private Element getVarRelation(Predicate pred) {
        Element varRel = null;
        HashMap relName = new HashMap();
        relName.put(Constants.EQUAL, "equal");
        relName.put(Constants.NOT_EQUAL, "notEqualTo");
        relName.put(Constants.LESS_THAN, "lessThan");
        relName.put(Constants.LESS_THAN_EQUAL, "lessThanEqualTo");
        relName.put(Constants.GREATER_THAN, "greaterThan");
        relName.put(Constants.GREATER_THAN_EQUAL, "greaterThanEqualTo");
        try {
            varRel = document.createElement((String) relName.get(pred.getOperationName()));
            Element oprnd1 = document.createElement("operand1");
            Element oprnd2 = document.createElement("operand2");
            varRel.appendChild(oprnd1);
            varRel.appendChild(oprnd2);
            if (pred.getPrefix().equals(Constants.RELATION_VV)) {
                Variable var1 = ((Variable) pred.getVariable(0)).makeCopy();
                Variable var2 = ((Variable) pred.getVariable(1)).makeCopy();
                oprnd1.appendChild(getVariable(var1, "variable"));
                oprnd2.appendChild(getVariable(var2, "variable"));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_VC)) {
                Variable var1 = ((Variable) pred.getVariable(0)).makeCopy();
                Variable var2 = ((Variable) pred.getVariable(1)).makeCopy();
                oprnd1.appendChild(getVariable(var1, "variable"));
                oprnd2.appendChild(getConstant(var2));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_CV)) {
                Variable var1 = ((Variable) pred.getVariable(0)).makeCopy();
                Variable var2 = ((Variable) pred.getVariable(1)).makeCopy();
                oprnd1.appendChild(getConstant(var1));
                oprnd2.appendChild(getVariable(var2, "variable"));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_VF)) {
                Variable var1 = ((Variable) pred.getVariable(0)).makeCopy();
                Function func = new Function((Function) pred.getVariables().get(1));
                oprnd1.appendChild(getVariable(var1, "variable"));
                oprnd2.appendChild(getFunction(func, "operationCall"));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_FV)) {
                Variable var = ((Variable) pred.getVariable(1)).makeCopy();
                Function func = new Function((Function) pred.getVariables().get(0));
                oprnd2.appendChild(getVariable(var, "variable"));
                oprnd1.appendChild(getFunction(func, "operationCall"));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_FC)) {
                Function func = new Function((Function) pred.getVariables().get(0));
                Variable var = ((Variable) pred.getVariable(1)).makeCopy();
                oprnd1.appendChild(getFunction(func, "operationCall"));
                oprnd2.appendChild(getConstant(var));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_CF)) {
                Function func = new Function((Function) pred.getVariables().get(1));
                Variable var = ((Variable) pred.getVariable(0)).makeCopy();
                oprnd1.appendChild(getConstant(var));
                oprnd2.appendChild(getFunction(func, "operationCall"));
            }
            else if (pred.getPrefix().equals(Constants.RELATION_FF)) {
                Function func1 = new Function((Function) pred.getVariables().get(0));
                Function func2 = new Function((Function) pred.getVariables().get(1));
                oprnd1.appendChild(getFunction(func1, "operationCall"));
                oprnd2.appendChild(getFunction(func2, "operationCall"));
            }
        }
        catch (Exception exp) {
        }
        return varRel;
    }

    /**
     * Generates predicate element in XML formula. This method uses getVarRelation method for relational predicate and
     * getHappens, getInitiates etc. methods for other predicates .
     * 
     * @param pred -
     *            Predicate object for which the element will be generated
     * @return
     */
    private Element generatePredicate(Predicate pred) {
        Element predicate = null;
        try {
            if (pred.getEcName().equals(Constants.PRED_RELATION)) {
                predicate = document.createElement("relationalPredicate");
                predicate.appendChild(getVarRelation(pred));
                predicate.appendChild(getTimeVariable(pred.getTimeVarName(), pred.getTimeVar().getValue(), "timeVar"));
            }
            else {
                predicate = document.createElement("predicate");
                if (pred.isNegated())
                    predicate.setAttribute("negated", "true");
                else
                    predicate.setAttribute("negated", "false");
                if (pred.isUnconstrained())
                    predicate.setAttribute("unconstrained", "true");
                else
                    predicate.setAttribute("unconstrained", "false");
                if (pred.isRecordable())
                    predicate.setAttribute("recordable", "true");
                else
                    predicate.setAttribute("recordable", "false");
                if (pred.isAbducible())
                    predicate.setAttribute("abducible", "true");
                else
                    predicate.setAttribute("abducible", "false");
                if (pred.getEcName().equals("Happens"))
                    predicate.appendChild(getHappens(pred));
                else if (pred.getEcName().equals("Initiates"))
                    predicate.appendChild(getInitiatesTerminates(pred, "initiates"));
                else if (pred.getEcName().equals("Terminates"))
                    predicate.appendChild(getInitiatesTerminates(pred, "terminates"));
                else if (pred.getEcName().equals("HoldsAt"))
                    predicate.appendChild(getHoldsAt(pred, "holdsAt"));
                else if (pred.getEcName().equals("Initially"))
                    predicate.appendChild(getHoldsAt(pred, "initially"));
            }
        }
        catch (Exception exp) {
        }
        return predicate;
    }

    /**
     * Generates operator element in XML formula. Mainly used in TimeExpression element.
     * 
     * @param operator -
     *            name of the operator, (plus or minus)
     * @return
     */
    private Element getOperator(String operator) {
        Element opt = null;
        try {
            opt = document.createElement("operator");
            opt.appendChild(document.createTextNode(operator));
        }
        catch (Exception exp) {
        }
        return opt;
    }

    /**
     * Generates quantifier element in XML formula.
     * 
     * @param varName -
     *            name of the variable for which quantifier is declared
     * @param varType -
     *            type of the variable
     * @param varValue -
     *            value of the variable
     * @param quantifier -
     *            name of the quantifier (e.g. forAll\existential)
     * @return
     */
    private Element getQuantifier(String varName, String varType, String varValue, String quantifier) {
        Element quan = null;
        try {
            quan = document.createElement("quantification");
            Element q = document.createElement("quantifier");
            q.appendChild(document.createTextNode(quantifier));
            quan.appendChild(q);
            Element var = null;
            if (varType.equals(""))
                var = getTimeVariable(varName, Constants.TIME_UD, "timeVariable");
            // else var = getVariable(varName, varType, varValue, "regularVariable");
            quan.appendChild(var);
        }
        catch (Exception exp) {
        }
        return quan;
    }

    /**
     * Generates TimeRelation element in XML formula.
     * 
     * @param tagName -
     *            tag name of the element, e.g. timeGreaterThanEqualTo, timeLessThan etc.
     * @param tex1 -
     *            First time expression
     * @param tex2 -
     *            second time expression
     * @return
     */
    /*
    private Element getTimeRelation(String tagName, TimeExpression tex1, TimeExpression tex2) {
        Element timeRel = null;
        HashMap relName = new HashMap();
        relName.put(">=", "timeGreaterThanEqualTo");
        relName.put(">", "timeGreaterThan");
        relName.put("<=", "timeLessThanEqualTo");
        relName.put("<", "timeLessThan");
        try {
            timeRel = document.createElement((String) relName.get(tagName));
            timeRel.appendChild(getTimeExpression(tex1, "timeVar1"));
            timeRel.appendChild(getTimeExpression(tex2, "timeVar2"));
        }
        catch (Exception exp) {
        }
        return timeRel;
    }
	*/
    /**
     * Generates timePredicate element in XML formula.
     * 
     * @param timePred -
     *            TimePredicate object for which the element will be generated
     * @return
     */
    /*
    private Element generateTimePredicate(TimePredicate timePred) {
        Element tp = null;
        try {
            tp = document.createElement("timePredicate");
            tp.appendChild(getTimeRelation(timePred.getOperator(), timePred.getLTimeExpression(), timePred
                    .getRTimeExpression()));
        }
        catch (Exception exp) {
        }
        return tp;
    }
	*/
    /**
     * Generates body or head element in XML formula. This method also creates quantifier object for each time variable
     * in the body or head and stores it in a list.
     * 
     * @param eventSequence -
     *            List of object in body/head. Object can be Predicate or a TimePredicate
     * @param quantifiers -
     *            List of quantifiers in the formula
     * @param name -
     *            tag name for the element, e.g. body or head
     * @return
     */
    private Element generateHeadBody(ArrayList eventSequence, ArrayList quantifiers, String name) {
        Element headBody = null;
        try {
            headBody = document.createElement(name);
            for (int i = 0; i < eventSequence.size(); i++) {
                Object obj = eventSequence.get(i);
                if (obj instanceof Predicate) {
                    Predicate pred = (Predicate) obj;
                    Element quantifier = getQuantifier(pred.getTimeVarName(), "", "", pred.getQuantifier());
                    quantifiers.add(quantifier);
                    Element predicate = generatePredicate(pred);
                    headBody.appendChild(predicate);
                }
                else if (obj instanceof TimePredicate) {
                    //headBody.appendChild(generateTimePredicate((TimePredicate) obj));
                }
                if (i < (eventSequence.size() - 1)) {
                    Element operator = getOperator("and");
                    headBody.appendChild(operator);
                }
                /*
                 * if((pred.getEcName().equals("Initiates") && pred.getPrefix().equals("as")) ||
                 * pred.getEcName().equals("HoldsAt")){ //Code should be added here for time predicates t1 < t2 or
                 * something like this
                 *  }
                 */
            }
        }
        catch (Exception exp) {
        }
        return headBody;
    }

    /**
     * This method appends quantifier elements to the root of the formula.
     * 
     * @param root -
     *            root of the formula
     * @param quantifiers -
     *            list of quantifiers, stored by the generateHeadBody
     */
    private void appendQuantifiers(Element root, ArrayList quantifiers) {
        try {
            for (int i = 0; i < quantifiers.size(); i++) {
                Element quantifier = (Element) quantifiers.get(i);
                root.appendChild(quantifier);
            }
        }
        catch (Exception exp) {
        }
    }

    /**
     * Generates formula element in XML formula.
     * 
     * @param formula -
     *            Formula object for which the element will be generated
     * @return
     */
    private Element generateFormula(Formula formula) {
        Element frml = null;
        try {
            frml = document.createElement("formula");
            frml.setAttribute("formulaId", formula.getFormulaId());
            frml.setAttribute("forChecking", String.valueOf(formula.isForChecking()));
            frml.setAttribute("diagnosisRequired", String.valueOf(formula.isDiagnosisRequired()));
            frml.setAttribute("threatDetectionRequired", String.valueOf(formula.isThreatDetectionRequired()));
            frml.setAttribute("type", formula.getFormulaType());
            ArrayList bodyList = formula.getBody();
            ArrayList headList = formula.getHead();
            ArrayList bodyQuantifiers = new ArrayList();
            ArrayList headQuantifiers = new ArrayList();
            Element body = generateHeadBody(bodyList, bodyQuantifiers, "body");
            Element head = generateHeadBody(headList, headQuantifiers, "head");
            appendQuantifiers(frml, bodyQuantifiers);
            appendQuantifiers(frml, headQuantifiers);
            frml.appendChild(body);
            frml.appendChild(head);
        }
        catch (Exception exp) {
        }
        return frml;
    }

    /**
     * Generates formula element in XML formula returns the String representation of the XML formula.
     * 
     * @param formulas -
     *            List of Formula objects for which the element will be generated
     * 
     */
    public String generateXML(LinkedHashMap formulas) {
        String docStr = "";
        // idCounter = 1;
        try {
            Element root = document.createElement("formulas");
            root.setAttribute("xmlns", "http://tempuri.org/ec/formula");
            document.appendChild(root);

            Iterator it = formulas.values().iterator();
            while (it.hasNext()) {
                Formula frml = (Formula) it.next();
                idCounter = 1;
                Element formula = generateFormula(frml);
                root.appendChild(formula);
            }
            docStr = XMLConverter.toString(root);
        }
        catch (Exception exp) {
        }
        return docStr;
    }
}
