/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest;

import java.io.File;
import java.io.FilenameFilter;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.SERENITYAnalyzer;
import uk.ac.city.soi.everest.monitor.Template;
//import uk.ac.city.soi.everest.monitor.TemplateWriter;



/**
 * This class works as an interface SeCSEAnalyzer class (i.e. the monitor). Actually DataAnalyzerEC was developed an
 * interface to SeCSEAnalyzer in SeCSE project.
 * 
 * @author Khaled Mahbub
 * 
 */
public class DataAnalyzerEC {
    private static Logger logger = Logger.getLogger(DataAnalyzerEC.class);

    SERENITYAnalyzer analyzer;
    private boolean isEC = true;

    public DataAnalyzerEC() {
        analyzer = new SERENITYAnalyzer();
    }

    /**
     * Receives Serenity formula, converts it into Java objects and then passes it to the analyzer.
     * 
     * @param input -
     *            String representation of Serenity XML formula
     * @return
     * @throws RemoteException
     */
    /*
    public boolean monitorRule(String input) throws RemoteException {

        try {
            if (input.indexOf("formula") != -1) {
                isEC = true;
                Document formulasForAnalyzer = readFormulas(input, Constants.PKG);
                FormulaReader fr = new FormulaReader();
                LinkedHashMap properties = fr.getFormula(formulasForAnalyzer);

                analyzer.monitorRule(properties);
            }
        }
        catch (Exception exp) {

            exp.getCause();
            exp.printStackTrace();
            return false;
        }

        return true;
    }
    */

    /**
     * Receives a linked hash map of formula objects and passes it to the analyzer.
     * 
     * @param formulas -
     *            linked hash map of formula objects
     * @return
     * @throws RemoteException
     */
    public boolean receiveFormulas(LinkedHashMap<String, Formula> formulas) throws RemoteException {

        logger.debug("formulas: " + formulas);

        try {
            analyzer.monitorRule(formulas);
        }
        catch (Exception exp) {
            //exp.getCause();
            exp.printStackTrace();
            return false;
        }
        return true;

    }

    /**
     * Given a formula ID this method checks if there is any monitoring result for that formula.
     * 
     * @param formulaID -
     *            ID of a formula
     * @return - String representation of the templates with monitoring decision
     */
    public String checkRule(String monitoringRuleId) throws RemoteException {
    	
        String returnVal = "";
        /*
        try {
            ArrayList templates = analyzer.getRules(monitoringRuleId);
            logger.debug("DAEC: no of. returned formulas: " + templates.size());
            logger.debug("DAEC: template status: " + ((Template) (templates.get(0))).getStatus());
            ArrayList templatesWithResult = new ArrayList();
            for (int i = 0; i < templates.size(); i++) {
                Template template = (Template) templates.get(i);
                if (!template.getStatus().equals(Constants.STATUS_UD))
                    templatesWithResult.add(template);
            }

            if (templatesWithResult.size() > 0) {
                if (isEC) {
                    TemplateWriter tr = new TemplateWriter();
                    returnVal += tr.generateXML(templatesWithResult);
                }
                // else returnVal += sla.toString(templatesWithResult);

            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
            exp.printStackTrace();
        }

        logger.debug("DataAnalyzerEc:The returned templates:\n" + returnVal);
        logger.debug("DataAnalyzerEc:End of the returned templates");
        */
        return returnVal;
    }

    /**
     * Given a preferred status, a timepoint and unique string, returns a collection of a monitoring result type
     * selected by time stamp and template id.
     * 
     * @param withStatus -
     *            the monitoring result type
     * @param afterTimepoint -
     *            the specific time stamp
     * @param includingInTemplateId -
     *            the string to be included in the result id
     * @return
     * @throws RemoteException
     */
    public ArrayList<Template> getMonitoringResults(String withStatus, long afterTimepoint, String includingInTemplateId)
            throws RemoteException {
        ArrayList<Template> monitoringResults = new ArrayList<Template>();
        try {
            monitoringResults = analyzer.getMonitoringResults(withStatus, afterTimepoint, includingInTemplateId);

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }

        return monitoringResults;
    }

    /**
     * Receives a LogEvent and sends it to the analyzer.
     * 
     * @param logEvent -
     *            received event
     * @return
     */
    /**
     * @param logEvent
     * @return
     */
    public String notify(LogEvent logEvent) {
        try {
            ArrayList events = new ArrayList();
            String partnerId = "Serenity";
            if (logEvent.getPrefix().equals("rc") || logEvent.getPrefix().equals("ir")) {

                events.add(logEvent);
                /*
                logger.debug("***********************logEvent*********************");
                logger.debug("|ecName: " + logEvent.getEcName());
                logger.debug("|id: " + logEvent.getID());
                logger.debug("|opName: " + logEvent.getOperationName());
                logger.debug("|partnerId: " + logEvent.getPartnerId());
                logger.debug("|prefix: " + logEvent.getPrefix());
                logger.debug("|time: " + logEvent.getTimestamp());
                logger.debug("|------var: ");
                
                for (int i = 0; i < logEvent.getVariables().size(); i++) {
                    logger.debug("\t ------------------------");
                    logger.debug("\t name: " + logEvent.getVariable(i).getName());
                    logger.debug("\t ConstName: " + logEvent.getVariable(i).getConstName());
                    logger.debug("\t type: " + logEvent.getVariable(i).getType());
                    logger.debug("\t value: " + logEvent.getVariable(i).getValue().toString());
                    logger.debug("\t Objtype: " + logEvent.getVariable(i).getRealType());
                    logger.debug("\t ObjValue: " + logEvent.getVariable(i).getObjValue());
                }
                */
                for (int i = 0; i < logEvent.getVariables().size(); i++) {
                    Variable var = (Variable) logEvent.getVariables().get(i);
                }
            }
            else if (logEvent.getPrefix().equals("re") || logEvent.getPrefix().equals("ic")) {
                events.add(logEvent);
                /*
                logger.debug("***********************logEvent*********************");
                logger.debug("|ecName: " + logEvent.getEcName());
                logger.debug("|id: " + logEvent.getID());
                logger.debug("|opName: " + logEvent.getOperationName());
                logger.debug("|partnerId: " + logEvent.getPartnerId());
                logger.debug("|prefix: " + logEvent.getPrefix());
                logger.debug("|time: " + logEvent.getTimestamp());
                logger.debug("|------var: ");
                for (int i = 0; i < logEvent.getVariables().size(); i++) {
                    logger.debug("\t ------------------------");
                    logger.debug("\t name: " + logEvent.getVariable(i).getName());
                    logger.debug("\t ConstName: " + logEvent.getVariable(i).getConstName());
                    logger.debug("\t type: " + logEvent.getVariable(i).getType());
                    logger.debug("\t value: " + logEvent.getVariable(i).getValue());
                    logger.debug("\t Objtype: " + logEvent.getVariable(i).getRealType());
                    logger.debug("\t ObjValue: " + logEvent.getVariable(i).getObjValue());
                }
                */
            }
            //System.out.println("EEEEEE Sending event ");
            //analyzer.notify(events);
            analyzer.notify(logEvent);

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return "";
    }

    /**
     * Requests the analyzer to stop monitoring.
     * 
     * @param slaName -
     *            not significant
     * @return
     */
    public boolean stopMonitoring(String slaName) {
        try {
            analyzer.stop();
        }
        catch (Exception exp) {

            exp.printStackTrace();
        }
        return true;
    }

    /**
     * Receives string representation of Serenity XML formula, then extracts the formula part that should be passed to
     * the analyzer. Extracted part is returned as W3C document.
     * 
     * @param file -
     *            String representation of serenity XML formula
     * @param pkg -
     *            location of the folder (i.e. datattypes package) where JAXB will store objects, if formula has some
     *            object definition
     * @return - extracted formula that should be sent to analyzer
     */
    /*
    public Document readFormulas(String file, String pkg) {
        // String filename=null;
        Document formulaDoc = null;
        // printit("start readFormulas with: "+file+" and "+pkg,"file.txt");
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            // printit("before parse.. ","file.txt");
            Document doc = docBuilder.parse(new InputSource(new StringReader(file)));
            // printit("after parse.. ","file.txt");
            // locate datatypes
            Element root = doc.getDocumentElement();
            NodeList datatypesList = root.getElementsByTagName("datatypes");
            // printit("datatype length: "+datatypesList.getLength(),"file.txt");
            for (int i = 0; i < datatypesList.getLength(); i++) {
                // String eleValue= objElement.getFirstChild().getNodeValue();
                // printit("before createObjFromSchema with
                // "+datatypesList.item(i).getFirstChild().getNodeValue(),"file.txt");
                createObjFromSchema(datatypesList.item(i).getFirstChild().getNodeValue(), pkg);
                // printit("after createObjFromSchema..","file.txt");
            }
            // locate formulas and create a file.xml with them
            NodeList formulas = root.getElementsByTagName("formulas");
            try {
                // Create instance of DocumentBuilderFactory
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                // Get the DocumentBuilder
                DocumentBuilder parser = factory.newDocumentBuilder();
                // Create blank DOM Document
                formulaDoc = parser.newDocument();

                Node node = formulaDoc.importNode(formulas.item(0), true);
                formulaDoc.appendChild(node);
            }
            catch (Exception e) {
                logger.debug(e.getMessage());
            }
        }
        catch (Exception e) {

            e.printStackTrace();
            e.printStackTrace();
        }
        return formulaDoc;
    }
	*/
    /**
     * Receives the data type definitions in serenity XML formula and uses JAXB to create corresponding Java objects.
     * 
     * @param file -
     *            String representation of datatype definition in serenity XML formula
     * @param pkg -
     *            location of the folder (i.e. datattypes package) where JAXB will store objects, if formula has some
     *            object definition
     * 
     */
    /*
    private void createObjFromSchema(String file, String pkg) {
        logger.debug("Creating classes from datatype description(xsd)...");
        SchemaCompiler sc = XJC.createSchemaCompiler();
        sc.forcePackageName(pkg);
        try {
            InputSource is = new InputSource(file);
            sc.parseSchema(is);
            S2JJAXBModel s2j = sc.bind();
            Plugin[] plug = new Plugin[1];
            ErrorListener el = null;
            JCodeModel jcm = s2j.generateCode(plug, el);
            jcm.build(new File(".")); // this the current directory of tomcat,wherever that is

            // compile

            String classpath =
                    Constants.CLASSPATH + "charsets.jar;" + Constants.CLASSPATH + "deploy.jar;" + Constants.CLASSPATH
                            + "javaws.jar;" + Constants.CLASSPATH + "jce.jar;" + Constants.CLASSPATH + "jsse.jar;"
                            + Constants.CLASSPATH + "plugin.jar;" + Constants.CLASSPATH + "rt.jar;"
                            + Constants.CLASSPATH + "jaxmexs-0.5.2.jar;" + Constants.CLASSPATH + "xalan.jar;"
                            + Constants.CLASSPATH + "activation.jar;" + Constants.CLASSPATH + "axis.jar;"
                            + Constants.CLASSPATH + "commons-discovery.jar;" + Constants.CLASSPATH
                            + "commons-logging.jar;" + Constants.CLASSPATH + "jaxrpc.jar;" + Constants.CLASSPATH
                            + "log4j.jar;" + Constants.CLASSPATH + "mail.jar;" + Constants.CLASSPATH + "qname.jar;"
                            + Constants.CLASSPATH + "saaj.jar;" + Constants.CLASSPATH + "wsdl4j.jar;"
                            + Constants.CLASSPATH + "bpws4j.jar;" + Constants.CLASSPATH + "jaxb1-impl.jar;"
                            + Constants.CLASSPATH + "jaxb-api.jar;" + Constants.CLASSPATH + "jaxb-impl.jar;"
                            + Constants.CLASSPATH + "jaxb-xjc.jar;" + Constants.CLASSPATH + "jsr173_1.0_api.jar;"
                            + Constants.CLASSPATH + "tools.jar;" + Constants.CLASSPATH + "dt.jar;"
                            + Constants.CLASSPATH + "htmlconverter.jar;" + Constants.CLASSPATH + "jconsole.jar;.";
            File f = new File("./" + pkg.replaceAll("\\.", "/"));
            lola fnf = new lola();
            File[] fList = f.listFiles(fnf);
            for (int i = 0; i < fList.length; i++) {
                int errorCode =
                        com.sun.tools.javac.Main.compile(new String[] { "-classpath", classpath, "-d", ".",
                                pkg.replaceAll("\\.", "/") + "/" + fList[i].getName() });
                if (errorCode != 0) {
                    logger.debug("We have failed to compile class " + fList[i].getName()
                            + ". The error uk.ac.city.soi.everest was: " + errorCode);
                }
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    */

    public boolean isBusy(){
    	return analyzer.isBusy();
    }
    /**
     * Given a formula Id, it calls the analyzer to stop monitoring the corresponding formula.
     * 
     * @param formulaId
     * @return
     */
    /*
    public boolean stopMonitoringFormula(String formulaId) {
        try {
            analyzer.stopMonitoringFormula(formulaId);
        }
        catch (Exception exp) {

        }
        return true;
    }
	*/
    /*
    private class lola implements FilenameFilter {
        public boolean accept(File dir, String name) {
            if (name.endsWith(".java")) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    */
}
