/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.conf.MonitorProperties;

/**
 * This class is the entry point to manage a database and its connection. Calling the method getDatabaseManager it
 * returns an instance of an object that implementing DatabaseManagerInterface interface. The desired class implementing
 * DatabaseManagerInterface interface must be specified in the configuration file ./conf/monitor.properties with the
 * property named "database.manager".
 * 
 * @author Davide Lorenzoli
 * 
 * @date 12 May 2009
 */
public class DatabaseManagerFactory extends ClassLoader {
    private static Logger logger = Logger.getLogger(DatabaseManagerFactory.class);

    private static String DATABASE_MANAGER = "everest.database.manager";

    /**
     * Load, from the file monitor.properties, the property database.manager. It contains the database manager class
     * name, e.g., uk.ac.city.soi.everest.database.DerbyDatabaseManager. Using the system class loader, this method,
     * attempts to load the specified class and return its instance.
     * 
     * @param propertiesFile
     *            properties file
     * @return a specific implementation of the ConnectionManagerInterface interface.
     */
    /*
    public static DatabaseManagerInterface getDatabaseManager(String propertiesFile) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(propertiesFile));
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (FileNotFoundException e) {
//            logger.error(e);
//        }
//        catch (IOException e) {
//            logger.error(e);
//        }
        String managerClass = (String) properties.get(DATABASE_MANAGER);

        return loadDatabaseManager(managerClass);
    }
	*/
    /**
     * Load, from the file monitor.properties, the property database.manager. It contains the database manager class
     * name, e.g., uk.ac.city.soi.everest.database.DerbyDatabaseManager. Using the system class loader, this method,
     * attempts to load the specified class and return its instance.
     * 
     * @return a specific implementation of the ConnectionManagerInterface interface.
     */
    public static DatabaseManagerInterface getDatabaseManager() {
        String managerClass = (String) MonitorProperties.getProperties().getProperty(DATABASE_MANAGER);

        return loadDatabaseManager(managerClass);
    }

    /**
     * @param managerClass
     * @return
     */
    private static DatabaseManagerInterface loadDatabaseManager(String managerClass) {
        DatabaseManagerInterface manager = null;
        try {
            // dynamically load the class
            ClassLoader classLoader = DatabaseManagerFactory.getSystemClassLoader();
            // create a new class instance using the empty constructor
            //manager = (DatabaseManagerInterface) classLoader.loadClass(managerClass).newInstance();
            manager = (DatabaseManagerInterface)Class.forName(managerClass).newInstance(); //(DatabaseManagerInterface) classLoader.loadClass(managerClass).newInstance();
            //manager = new MySQLDatabaseManager();

            logger.debug("Loaded database manager: " + manager);
        }
        catch (Exception e) {
            logger.error(e);
        }        
//        catch (InstantiationException e) {
//            logger.error(e);
//        }
//        catch (IllegalAccessException e) {
//            logger.error(e);
//        }
//        catch (ClassNotFoundException e) {
//            logger.error(e);
//        }

        return manager;
    }
}
