/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * @author Theocharis Tsigkritis
 * 
 * 11 Oct 2010
 */
package uk.ac.city.soi.everest.util;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Utility class for converting XML formula string to Document objects.
 * 
 */
public class FormulaConverter {
    private static Logger logger = Logger.getLogger(FormulaConverter.class);

    /**
     * Empty constructor.
     */
    public FormulaConverter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Receives string representation of Serenity XML formula, then extracts the formula part that should be passed to
     * the analyzer. Extracted part is returned as W3C document.
     * 
     * @param file -
     *            String representation of serenity XML formula
     * @param pkg -
     *            location of the folder (i.e. datattypes package) where JAXB will store objects, if formula has some
     *            object definition
     * @return - extracted formula that should be sent to analyzer
     */
    public Document readFormulas(String file, String pkg) {
        // String filename=null;
        Document formulaDoc = null;
        // printit("start readFormulas with: "+file+" and "+pkg,"file.txt");
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            // printit("before parse.. ","file.txt");
            Document doc = docBuilder.parse(new InputSource(new StringReader(file)));
            // printit("after parse.. ","file.txt");
            // locate datatypes
            Element root = doc.getDocumentElement();
            NodeList formulas = root.getElementsByTagName("formulas");
            try {
                // Create instance of DocumentBuilderFactory
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                // Get the DocumentBuilder
                DocumentBuilder parser = factory.newDocumentBuilder();
                // Create blank DOM Document
                formulaDoc = parser.newDocument();

                Node node = formulaDoc.importNode(formulas.item(0), true);
                formulaDoc.appendChild(node);
                /*
                 * //print to file OutputFormat format = new OutputFormat(formulaDoc); format.setIndenting(true);
                 * 
                 * //to generate a file output use fileoutputstream //XMLSerializer serializer = new XMLSerializer(
                 * //new FileOutputStream(new File(String.valueOf(formulas.item(0).hashCode()))+".xml"), format);
                 * 
                 * //serializer.serialize(formulaDoc); //filename= String.valueOf(formulas.item(0).hashCode())+".xml";
                 * 
                 * filename = formulaDoc.toString();
                 */
            }
            catch (Exception e) {
                //logger.debug(e.getMessage());
            }
        }
        catch (Exception e) {
            //e.printStackTrace();
        }
        return formulaDoc;
    }
}
