/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.monitor;

import org.apache.log4j.Logger;

/**
 * This class serves as a garbage collector to the past event storer. It runs in different thread. It receives a
 * reference of PastEventStorer, and ClockMap. At regular interval, it calls the deleteEvents method to delete those
 * events whose life time has expired.
 * 
 * @author Khaled Mahbub
 * 
 */
public class PastEventCleaner {
    // logger
    private static Logger logger = Logger.getLogger(PastEventCleaner.class);

    private boolean goOn = false;
    private PastEventStorer pes;
    // private ConstraintMatrixStorer cms;
    private ClockMap clockMap;

    /**
     * Defines Thread.
     * 
     * @author Khaled Mahbub
     * 
     */
    private static class ThreadVar {
        private Thread thread;

        ThreadVar(Thread t) {
            thread = t;
        }

        synchronized Thread get() {
            return thread;
        }

        synchronized void clear() {
            thread = null;
        }
    }

    private ThreadVar threadVar;

    private void finished() {
    }

    /**
     * Creates a new instance of PastEventCleaner.
     * 
     * @param pes
     * @param elsd
     * @param clockMap
     */
    public PastEventCleaner(PastEventStorer pes, ClockMap clockMap) {
        this.pes = pes;
        // this.cms = cms;
        this.clockMap = clockMap;

        final Runnable doFinished = new Runnable() {
            public void run() {
                finished();
            }
        };

        Runnable doConstruct = new Runnable() {
            public void run() {
                try {
                    mainLoop();
                }
                finally {
                    threadVar.clear();
                }
                // SwingUtilities.invokeLater(doFinished);
            }
        };
        Thread t = new Thread(doConstruct);
        threadVar = new ThreadVar(t);
    }

    /**
     * starts the thread (i.e. PastEventCleaner).
     * 
     */
    public void start() {
        if (!goOn) {
            goOn = true;
            logger.debug("Starting PE-DB cleaner");
            Thread t = threadVar.get();
            if (t != null) {
                if (t.isInterrupted()) {
                    // logger.debug("Was interrupted");
                    t.interrupted();
                    // logger.debug("now " + t.isInterrupted());
                }
                else
                    t.start();
            }
        }
        else
            logger.debug("PE-DB cleaner already running");
    }

    /**
     * Stops the past event cleaner.
     * 
     */
    public void stop() {
        if (goOn) {
            goOn = false;
            logger.debug("Stoping PE-DB cleaner");
            Thread t = threadVar.get();
            if (t != null) {
                try {
                    t.interrupt();
                }
                catch (Exception exp) {
                    logger.debug("Caugt : " + exp.getMessage());
                }
            }
        }
        else
            logger.debug("PE-DB cleaner was not running");
    }

    /**
     * This is the method where events are cleaned. The mechanism is described in the class description.
     * 
     */

    private void mainLoop() {
        try {
            while (goOn) {
                pes.deleteEvents(clockMap);
                Thread.sleep(5 * 60 * 1000); // clean every 5 minutes
            }
        }
        catch (Exception exp) {
            // logger.debug("Caught interrupted exception " + exp.getMessage());
            // exp.printStackTrace();
        }
    }
}
