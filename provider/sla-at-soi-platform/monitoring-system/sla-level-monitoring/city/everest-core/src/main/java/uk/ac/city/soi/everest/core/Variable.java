/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Variable.java
 *
 * Created on 16 May 2004, 10:49
 */

package uk.ac.city.soi.everest.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.SRNTEvent.TestObjConversion;

/**
 * This class is used to model Variables in EC formulas. The structure of the variable object is (constName, name, type,
 * obj/values). constName - is the field that is used to hold the BPEL variable name/user defined variable and used only
 * for unification. name - is used to give a name to the variable that is used by the monitor internally. type - type of
 * the variable. obj/values - holds the value(s) of the variable. persistent - is the boolean field to define if the
 * variable is persistent (static) through out the monitoring session, i.e. in different templates the same instance of
 * this variable will be used.
 * 
 * @author Khaled Mahbub
 */
public class Variable implements Serializable {
    // logger
    private static Logger logger = Logger.getLogger(Variable.class);

    /**
     * Is the field that is used to hold the BPEL variable name/user defined variable, and used only for unification. It
     * should be noted that in SeCSE monitoring was done for BPEL process only, so most of the variables came from BPEL
     * specification.
     */
    private String constName; // this is used to hold the BPEL variable name/user defined variable and used only for
                                // unification

    /**
     * Is used to give a name to the variable that is used by the monitor internally
     */
    private String name; // this is used to give a name to the variable that is used by the monitor

    /**
     * Type of the variable, check the class uk.ac.city.soi.everest.core.Constants for types
     */
    private String type;

    /**
     * Holds the value(s) of the variable
     */
    private Object obj = null;

    /**
     * Holds the value(s) of the variable
     */
    private ArrayList values = new ArrayList();

    /**
     * is the boolean field to define if the variable is persistent (static) through out the monitoring session, i.e. in
     * different templates the same instance of this variable will be used.
     */
    private boolean persistent = false;

    /**
     * Creates a new variable.
     * 
     * @param constName
     * @param name
     * @param type
     *            check the class <uk.ac.city.soi.everest>uk.ac.city.soi.everest.core.Constants</uk.ac.city.soi.everest>
     *            for allowed types
     * @param value
     */
    public Variable(String constName, String name, String type, Object value) {
        this.constName = new String(constName);
        this.name = new String(name);
        this.type = new String(type);
        // this.value = new String(value);
        // values = new ArrayList();
        try {
            this.obj = (Object) (ObjectCloner.deepCopy(value));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new variable.
     * 
     * @param constName
     * @param type
     *            check the class <uk.ac.city.soi.everest>uk.ac.city.soi.everest.core.Constants</uk.ac.city.soi.everest>
     *            for allowed types
     * @param value
     */
    public Variable(String constName, String type, Object value) {
        this.constName = new String(constName);
        this.name = new String("v" + constName);
        this.type = new String(type);

        try {
            this.obj = (Object) (ObjectCloner.deepCopy(value));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Copy constructor.
     */
    public Variable(Variable var) {
        this.constName = new String(var.getConstName());
        this.name = new String(var.getName());
        this.type = new String(var.getType());
        try {
            this.obj = (Object) (ObjectCloner.deepCopy(var.getObject()));
        }
        catch (Exception e) {
            logger.debug(e);
        }
        this.persistent = var.isStatic();
        if (var.getType().equals(Constants.DATA_TYPE_OBJECT))
            values = var.getValues();
        else if (var.getType().endsWith("Array")) {
            String[] strs = var.getStringArray();
            for (int i = 0; i < strs.length; i++)
                values.add(new String(strs[i]));
        }

    }

    /**
     * This method is used to make copy of a variable. It works like copy constructor in case of non static variable.
     * But in case of static variable it just returns a reference to the same variable.
     * 
     * @return
     */
    public Variable makeCopy() {
        Variable var1 = null;
        if (isStatic())
            var1 = this;
        else
            var1 = new Variable(this);
        return var1;
    }

    /**
     * Sets the constant name.
     * 
     * @param constName
     */
    public void setConstName(String constName) {
        this.constName = new String(constName);
    }

    /**
     * Sets the name of the variable.
     */
    public void setName(String name) {
        this.name = new String(name);
    }

    /**
     * Sets the type of the variable.
     * 
     * @param type
     */
    public void setType(String type) {

        this.type = new String(type);
    }

    /**
     * Sets the value of the variable.
     * 
     * @param value
     */
    public void setValue(Object value) {

        try {
            this.obj = (Object) (ObjectCloner.deepCopy(value));
        }
        catch (Exception e) {
            logger.debug(e);
        }

    }

    /**
     * Sets if the variable is persistent or not.
     * 
     * @param val
     */
    public void setStatic(boolean val) {
        this.persistent = val;
    }

    /**
     * Adds a value of the variable, in case the variable has more than one value (i.e. array).
     * 
     * @param s
     */
    public void addValue(String s) {
        values.add(new String(s));
    }

    /**
     * Adds a list of values of the variable, in case the variable has more than one value (i.e. array).
     * 
     * @param list
     */
    public void addValues(ArrayList list) {
        values.addAll(list);
    }

    /**
     * Removes a value from the variable, in case the variable can have multiple values (i.e. array)
     * 
     * @param s
     */
    public void delValue(String s) {
        values.remove(new String(s));
    }

    /**
     * Removes the values that are contained in list from the variable, in case the variable can have multiple values
     * (i.e. array).
     * 
     * @param list
     */
    public void delValues(ArrayList<String> list) {
        values.removeAll(new ArrayList<String>(list));
    }

    /**
     * Clears all values from the variable, in case the variable can have multiple values (i.e. array).
     * 
     * @param list
     */
    public void delAllValues() {
        values.clear();
    }

    /**
     * Adds a value of the variable at a specific index, in case the variable has more than one value (i.e. array).
     * 
     * @param s
     * @param index
     */
    public void addValueAt(String s, double index) {
        int ind = (int) index;
        if (ind < values.size())
            values.set(ind, new String(s));
        else
            values.add(ind, new String(s));
    }

    /**
     * Returns the constName (i.e. the BPEL variable name) of the variable.
     * 
     * @return
     */
    public String getConstName() {
        return constName;
    }

    /**
     * Returns the name of the variable.
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    // costas
    // methods for object
    /**
     * Sets an object value of the variable.
     * 
     * @param o -
     */
    public void setObject(Object o) {
        try {
            this.obj = (Object) (ObjectCloner.deepCopy(o));
        }
        catch (Exception e) {
            logger.debug(e);
        }
    }

    /**
     * Returns the object value of the variable.
     * 
     * @return
     */
    public Object getObject() {
        return obj;
    }

    /**
     * Returns the object value of the variable as string.
     * 
     * @return
     */
    public String getObjValue() {
        if (obj != null) {
            return obj.toString();
        }
        else {
            return "";
        }
    }

    /**
     * Returns the type of the variable.
     */
    public String getRealType() {

        return this.getType();

    }

    /**
     * Returns the type of the variable.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the value of the variable.
     * 
     * @return
     */
    public Object getValue() {
        return obj;
    }

    /**
     * Returns the value of the variable.
     * 
     * @return
     */
    public Object getRealValue() {
        return this.getValue();
    }

    /**
     * Returns the values of the variable.
     * 
     * @return
     */
    public ArrayList getValues() {
        return values;
    }

    /**
     * Returns string representation of the value(s) of the variable as a list.
     * 
     * @return
     */
    public String[] getStringArray() {
        String[] val = null;
        val = new String[values.size()];
        for (int i = 0; i < values.size(); i++)
            val[i] = new String((String) values.get(i));
        return val;
    }

    public Object getObjectValue() {
        return values.get(0);
    }

    /**
     * Return true if the variable is persistent, otherwise returns false.
     * 
     * @return
     */
    public boolean isStatic() {
        return persistent;
    }

    /**
     * Recursively compare the class type of the given variables.
     * 
     * @param varc
     * @param valc
     * @return
     */
    public static boolean checkSuperClass(Class varc, Class valc) {
        boolean ret = false;

        //logger.debug("varc: " + varc);
        //logger.debug("valc: " + valc);

        if (varc.equals(valc)) {
            ret = true;
        }
        else {
            try {
                ret = checkSuperClass(varc, valc.getSuperclass());
            }
            catch (Exception e) {
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Returns true if a variable var matches with this variable. Variable1 matches with Variable2 if they have same
     * constName, type and value.
     * 
     * @param var
     * @return
     */
    public boolean matches(Variable var) {
        boolean val = false;
        if (var != null && constName.equals(var.getConstName()) &&
        // type.equals(var.getType()) &&
                checkSuperClass(getClassFromType(type), getClassFromType(var.type))) {

            val = true;
        }
        // if(val) logger.debug("Matching variables: " + this.getConstName() + " and "+var.getConstName()+ ". Match?:
        // "+val);
        return val;
    }

    /**
     * Returns true if a variable has same type as this variable.
     * 
     * @param var
     * @return
     */
    public boolean matchesType(Variable var) {
        boolean val = false;
        if (var != null
        // && constName.equals(var.getConstName())
                // && type.equals(var.getType())
                && checkSuperClass(getClassFromType(type), getClassFromType(var.type))) {

            val = true;
        }
        // if(val) logger.debug("Matching variables: " + this.getConstName() + " and "+var.getConstName()+ ". Match?:
        // "+val);
        return val;
    }

    private Class getClassFromType(String type) {
        String finalStr = null;
        if (type.equals("int")) {
            finalStr = "java.lang.Integer";
        }
        else if (type.equals("string")) {
            finalStr = "java.lang.String";
        }
        else if (type.equals("long")) {
            finalStr = "java.lang.Long";
        }
        else if (type.equals("boolean")) {
            finalStr = "java.lang.Boolean";
        }
        else if (type.equals("double")) {
            finalStr = "java.lang.Double";
        }
        else if (type.equals("OpStatus") || type.equals("Entity")) {
            finalStr = "uk.ac.city.soi.everest.SRNTEvent." + type;
        }
        else {
            finalStr = Constants.PKG + "." + type;
        }
        try {
            File classesDir = new File(".");
            ClassLoader parentLoader = TestObjConversion.class.getClassLoader();
            URLClassLoader loader1 = new URLClassLoader(new URL[] { classesDir.toURL() }, parentLoader);
            Class c = loader1.loadClass(finalStr);
            return c;
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * compare two variables. The comparison has done by considering only type and the value of the given variables.
     * 
     * @param var
     * @return
     */

    public boolean equals(Variable var) {
        boolean val = false;
        if (var != null
        // && constName.equals(var.getConstName())
                // && type.equals(var.getType())
                && matchesType(var)
        // && name.equals(var.getName())
        // && value.equals(var.getValue())
        ) {
            ObjectOutputStream oos1 = null;
            ObjectOutputStream oos2 = null;

            try {
                ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
                ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
                oos1 = new ObjectOutputStream(bos1);
                oos2 = new ObjectOutputStream(bos2);
                // serialize and pass the object
                oos1.writeObject(obj);
                oos1.flush();
                oos2.writeObject(var.obj);
                oos2.flush();
                if (bos1.toString().equals(bos2.toString())) {
                    val = true;
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    /**
     * Returns the string representation of this variable.
     * 
     * @param varName
     * @return
     */
    public String toString(boolean varName) {
        String val = "";

        if (type.equals(Constants.DATA_TYPE_CONST)) {
            // val += obj.toString();
            val += obj;
        }
        else {
            if (varName) {
                val += name + ":";
            }
            else {
                val += constName + ":";
            }

            // val += this.getObjValue();
            // val += obj.toString();

            if (type.endsWith("Array")) {
                val += "[";
                for (int i = 0; i < this.getValues().size(); i++) {
                    val = val + this.getValues().get(i).toString();
                    if (i != (this.getValues().size() - 1)) {
                        val = val + ", ";
                    }
                }
                val = val + "]";
            }
        }

        return val;
    }

    /**
     * Returns a hash uk.ac.city.soi.everest for this variable.
     * 
     * @return
     */
    public String getHashCode() {
        String val = "";
        val += constName;
        val += "\n";
        val += type;

        return val;
    }

    /**
     * Returns a hash value for this variable.
     * 
     * @return
     */
    public String getHashValue() {
        String val = "";
        // val += constName;
        if (type.equals("int")) {
            val += "java.lang.Integer";
        }
        else if (type.equals("string")) {
            val += "java.lang.String";
        }
        else if (type.equals("OpStatus") || type.equals("Entity")) {
            val += "uk.ac.city.soi.everest.SRNTEvent." + type;
        }
        else if (type.contains(Constants.ARRAY)) {
            if (type.equals(Constants.DATA_TYPE_DOUBLE_ARRAY)) {
                val += Constants.DATA_TYPE_DOUBLE_ARRAY;
            }
            else if (type.equals(Constants.DATA_TYPE_STRING_ARRAY)) {
                val += Constants.DATA_TYPE_STRING_ARRAY;
            }
            else if (type.equals(Constants.DATA_TYPE_LONG_ARRAY)) {
                val += Constants.DATA_TYPE_LONG_ARRAY;
            }
            else if (type.equals(Constants.DATA_TYPE_INT_ARRAY)) {
                val += Constants.DATA_TYPE_INT_ARRAY;
            }
            val += "ofSize:" + this.getValues().size();
        }
        else {
            val += Constants.PKG + "." + type;
        }

        try {
            ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
            ObjectOutputStream oos1 = new ObjectOutputStream(bos1);
            // serialize and pass the object
            oos1.writeObject(obj);
            oos1.flush();
            val += bos1.toString();

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return val;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getClass().getName() + "{" + "name=" + name + "," + "type=" + type + "," + "value=" + obj + "}";
    }
}
