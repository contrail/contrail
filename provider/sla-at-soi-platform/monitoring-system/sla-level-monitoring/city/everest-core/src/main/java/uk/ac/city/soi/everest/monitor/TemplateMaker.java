/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * TemplateMaker.java
 *
 * Created on 21 March 2005, 09:35
 */

package uk.ac.city.soi.everest.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Variable;

/**
 * Given a list of formula objects, this class converts formula objects into template objects. This class also
 * determines the dependencies among templates (formulas) and initiates links between templates.
 * 
 * @author am697
 */
public class TemplateMaker {
    // logger
    private static Logger logger = Logger.getLogger(TemplateMaker.class);

    /*
     * Holds the IDs of the formulas
     */
    private static ArrayList templateIds = new ArrayList();

    /*
     * Holds the initiates predicates that were generated from the Initially sentences of the monitoring theory @author
     * t7t
     */
    private static ArrayList<Predicate> initPreds = new ArrayList<Predicate>();

    /**
     * Returns the generated initiates predicates list.
     * 
     * @author t7t
     */
    public static ArrayList<Predicate> getInitPred() {
        return initPreds;
    }

    /**
     * This method converts formulas into templates. This method also identifies the dependencies among formulas.
     * 
     * @param monitorableIds -
     *            The list of IDs of formulas to be monitored
     * @param allFormulas -
     *            A collection of formulas
     * @param linked -
     *            if true the formula dependency is detected
     * @param exhaustive -
     *            if true dependency is determined using all formulas, otherwise dependency is determined using only
     *            monitorable formulas
     * @return - a list of templates
     */
    public static ArrayList getTemplates(ArrayList monitorableIds, LinkedHashMap allFormulas, boolean linked,
            boolean exhaustive, HashMap staticVars) {
        // clearInitPred();
        // HashMap staticVars = new HashMap();
        LinkedHashMap formulas = new LinkedHashMap();
        ArrayList temps = new ArrayList();
        ArrayList additionalIds = new ArrayList();
        LinkedHashMap graph = new LinkedHashMap();
        additionalIds.addAll(monitorableIds);
        if (exhaustive)
            formulas.putAll(allFormulas);
        else {
            for (int i = 0; i < additionalIds.size(); i++) {
                String key = (String) additionalIds.get(i);
                logger.debug("TemplateMaker: key:" + key);
                Formula formula = (Formula) allFormulas.get(key);
                formulas.put(formula.getFormulaId(), formula);
            }
        }
        resolveInitially(formulas, additionalIds, staticVars);
        // while(!additionalIds.isEmpty()){
        for (int i = 0; i < additionalIds.size(); i++) {
            // String targetKey = (String)additionalIds.get(0);
            String targetKey = (String) additionalIds.get(i);
            if (!graph.containsKey(targetKey)) {
                HashSet links = new HashSet();
                graph.put(targetKey, links);
            }
            // findAndAddLinks(formulas, targetKey, graph, additionalIds);
            // additionalIds.remove(0);
        }
        monitorableIds.clear();
        Iterator it = graph.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            monitorableIds.add(key);
            HashSet links = (HashSet) graph.get(key);
            //System.err.println("%%%%%%%%%%%%%%% " + ((Formula) formulas.get(key)).getFormulaId());
            Template template = new Template((Formula) formulas.get(key));
            logger
                    .debug("Templatemaker: new template has created: " + template.getFormulaId() + "-"
                            + template.getCC());
            logger.debug("Templatemaker: static vars before new template" + staticVars);
            // template.addStaticVars(staticVars);
            template.adjustStaticVars(staticVars);
            logger.debug("Templatemaker: static vars after new template" + staticVars);
//            Iterator linkIt = links.iterator();
//            while (linkIt.hasNext()) {
//                Link link = (Link) linkIt.next();
//                template.addLink(link);
//            }
            temps.add(template);
        }
        return temps;
    }

    /**
     * This is the internal method used to determine formula dependency.
     * 
     * @param formulas -
     *            A collection of formulas
     * @param targetKey -
     *            ID of the formula for which dependency is checked
     * @param graph -
     *            resulted dependent graph
     * @param additionalIds -
     *            list of potential target IDs that dynamically changes
     */
    /*
    private static void findAndAddLinks(LinkedHashMap formulas, String targetKey, HashMap graph, ArrayList additionalIds) {
        // logger.debug("Looking for " + targetKey);
        Iterator it = formulas.keySet().iterator();
        while (it.hasNext()) {
            String srcKey = (String) it.next();
            if (!srcKey.equals(targetKey)) {
                HashSet newLinks = new HashSet();
                ArrayList srcHead = ((Formula) formulas.get(srcKey)).getHead();
                // Formula fh = ((Formula)formulas.get(srcKey));
                ArrayList targetBody = new ArrayList(((Formula) formulas.get(targetKey)).getBody());
                targetBody.addAll(((Formula) formulas.get(targetKey)).getHead());
                // Formula fb = ((Formula)formulas.get(targetKey));
                for (int j = 0; j < srcHead.size(); j++) {
                    Object srcObj = srcHead.get(j);
                    if (srcObj instanceof Predicate) {
                        Predicate srcPred = (Predicate) srcObj;
                        if (srcPred.getEcName().equals(Constants.PRED_HAPPENS)) {
                            for (int k = 0; k < targetBody.size(); k++) {
                                Object targetObj = targetBody.get(k);
                                if (targetObj instanceof Predicate) {
                                    Predicate targetPred = (Predicate) targetObj;
                                    if (targetPred.matchesNames(new Link(srcPred.getEcName(), srcPred.getPrefix(),
                                            srcPred.getPartnerId(), srcPred.getOperationName(), targetKey,
                                            Constants.DEDUCTIVE)))
                                        // logger.debug("sssss");
                                        newLinks.add(new Link(srcPred.getEcName(), srcPred.getPrefix(), srcPred
                                                .getPartnerId(), srcPred.getOperationName(), targetKey,
                                                Constants.DEDUCTIVE));
                                }
                            }
                        }
                    }// end of source object
                }
                if (!newLinks.isEmpty()) {
                    if (!contains(additionalIds, srcKey))
                        additionalIds.add(srcKey);
                    if (graph.containsKey(srcKey)) {
                        HashSet links = (HashSet) graph.get(srcKey);
                        links.addAll(newLinks);
                    }
                    else
                        graph.put(srcKey, newLinks);
                }
            }
        }
    }
	*/
    /**
     * Returns true if a list contains a specific value.
     * 
     * @param list
     * @param str
     * @return
     */
    /*
    private static boolean contains(ArrayList list, String str) {
        boolean found = false;
        for (int i = 0; i < list.size() && !found; i++) {
            String listStr = (String) list.get(i);
            if (listStr.equals(str))
                found = true;
        }
        return found;
    }
	*/
    /**
     * Given a set of formulas this method returns the formula IDs as a list.
     * 
     * @param formulas
     * @return
     */
    /*
    public static ArrayList getTemplateIds(HashMap formulas) {
        templateIds.clear();
        Iterator it = formulas.values().iterator();
        while (it.hasNext()) {
            Formula formula = (Formula) it.next();
            Template template = new Template(formula);
            templateIds.add(template.getFormulaId());
        }
        return templateIds;
    }
	*/
    /**
     * This method is used to resolve Initially formulas. In EC Initially predicate specifies that some fluent holds at
     * the beginning. If there is an Initially formula in the formula set, then the truth value of corresponding fluent
     * (i.e. HoldsAt predicate) in other formulas should be set to true. And the Initially formula should be taken. out
     * from the formula set.
     * 
     * @param properties
     * @param additionalIds
     */
    public static void resolveInitially(LinkedHashMap properties, ArrayList additionalIds, HashMap staticVars) {
        logger.debug("***TemplateMaker: resolveInitially call");
        ArrayList initiallies = new ArrayList();
        Iterator it1 = properties.keySet().iterator();
        while (it1.hasNext()) {
            String key = (String) it1.next();
            logger.debug("***TemplateMaker: key:" + key);
            Formula formula = (Formula) properties.get(key);
            if (((Predicate) (formula.getBody()).get(0)).getEcName().equals(Constants.PRED_INITIALLY)) {
                // properties.remove(key);
                initiallies.add(formula);
            }
        }
        logger.debug("***TemplateMaker: initiallies size: " + initiallies.size());
        for (int i = 0; i < initiallies.size(); i++) {
            Formula formula = (Formula) initiallies.get(i);
            properties.remove(formula.getFormulaId());
            additionalIds.remove(formula.getFormulaId());
            for (int j = 0; j < formula.getBody().size(); j++) {
                Predicate pred = (Predicate) formula.getBody().get(j);
                if (pred.getEcName().equals(Constants.PRED_INITIALLY)) {
                    // Revised Initially predicates handling implementation @author t7t
                    logger.debug("***TemplateMaker: generating artificial initiates predicate from initially predicate");
                    Predicate initiatesPred = new Predicate(pred);
                    initiatesPred.setEcName(Constants.PRED_INITIATES);
                    initiatesPred.setTime(System.currentTimeMillis());
                    initiatesPred.setFluent(pred.getFluent());
                    // print the fluent variables
                    logger.debug("variables of the " + pred.getFluent() + ": ");
                    for (int fv = 0; fv < pred.getFluent().getArguments().size(); fv++) {
                        Object obj = pred.getFluent().getArguments().get(fv);
                        if (obj instanceof Variable) {
                            Variable var = ((Variable) obj).makeCopy();
                            initiatesPred.addVariable(var);
                            if (var.isStatic()) {
                                staticVars.put(var.getName() + var.getType(), var);
                            }
                        }
                        else if (obj instanceof Function) {
                            Function func = (Function) obj;
                            Variable funcResult = func.execute(null, null);
                            initiatesPred.getFluent().getArguments().remove(fv);
                            initiatesPred.getFluent().addArgument(new Variable(funcResult));
                            initiatesPred.addVariable(new Variable(funcResult));
                        }
                        // logger.debug(((Variable)pred.getFluent().getArguments().get(fv)).toString(true));
                    }
                    initiatesPred.setOperationName("artifOp");
                    initiatesPred.setTime(0);
                    initPreds.add(initiatesPred);
                }
            }
        }
    }
}
