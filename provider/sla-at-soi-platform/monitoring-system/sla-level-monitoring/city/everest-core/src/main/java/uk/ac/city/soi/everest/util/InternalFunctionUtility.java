package uk.ac.city.soi.everest.util;

import uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface;

public class InternalFunctionUtility {
	private static long monitorSessionStartTime = -1;
	private static long firstEventTime = -1;
	private static long currentEventTime = -1;
	
	public static long getCurrentEventTime() {
		return currentEventTime;
	}


	public static void setCurrentEventTime(long currentEventTime) {
		InternalFunctionUtility.currentEventTime = currentEventTime;
	}


	public static long getFirstEventTime() {
		return firstEventTime;
	}

	
	public static void setFirstEventTime(long firstEventTime) {
		firstEventTime = firstEventTime;
	}
	
	private static FluentEntityManagerInterface fluentStorer = null;
	
	public static void setMonitorSessionStartTime(long t){
		monitorSessionStartTime = t;
	}
	
	public static long getMonitorSessionStartTime(){
		return monitorSessionStartTime;
	}
	
	
	public static void setFluentStorer(FluentEntityManagerInterface fs){
		fluentStorer = fs;
	}
	
	public static FluentEntityManagerInterface getFluentStorer(){
		return fluentStorer;
	}
}
