/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.conf.MonitorProperties;

/**
 * This class implements the ConnectionManagerInterface for MySQL db connections.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 12 Nov 2008
 */
public class MySQLConnectionManager implements ConnectionManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(MySQLConnectionManager.class);

    // default parameters
    private String propertiesFile = "./conf/mysql.properties";
    private String user;
    private String password;
    private String host;
    private String database;

    private static Connection connection;

    /**
     * If the file targeted by propertiesFile parameter does not exist the default properties file in properties will be
     * used indeed.
     * 
     * @see uk.ac.city.soi.everest.database.ConnectionManagerInterface#getConnection(java.lang.String)
     */
    
    public Connection getConnection(String propertiesFile) {
        Properties properties = new Properties();

        if (new File(propertiesFile).exists()) {
            this.propertiesFile = propertiesFile;
            try {
                properties.load(new FileInputStream(propertiesFile));

                loadProperties(properties);
                init();

            }
            catch (FileNotFoundException e) {
                logger.error(e.getMessage(), e.getCause());
            }
            catch (IOException e) {
                logger.error(e.getMessage(), e.getCause());
            }
        }

        return connection;
    }
	
    /**
     * Look for the default properties file in properties.
     * 
     * @return the connection to the database
     */
    public Connection getConnection() {
        loadProperties(MonitorProperties.getProperties());
        init();

        return connection;
    }

    /**
     * Initialise the database.
     */
    private void init() {
        String url = "jdbc:mysql://" + host + "/" + database + "?" + "user=" + user + "&password=" + password;
        boolean isClosed = true;

        try {
            if (connection != null) {
                isClosed = connection.isClosed();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        if (connection == null || isClosed) {
            try {
                logger.debug("MySQLConnectionManager: opening connection to " + url);
                // loading explicitly the driver, this is because JVM 1.5 does
                // not load it automatically the the method
                // DriverManager.getConnection is invoked.
                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                }
                catch (Exception e) {
                    logger.error(e.getMessage(), e.getCause());
                }  
                /*
                catch (InstantiationException e) {
                    logger.error(e.getMessage(), e.getCause());
                }
                catch (IllegalAccessException e) {
                    logger.error(e.getMessage(), e.getCause());
                }
                catch (ClassNotFoundException e) {
                    logger.error(e.getMessage(), e.getCause());
                }
                */
                connection = DriverManager.getConnection(url);
            }
            catch (SQLException ex) {
                // handle any errors
                logger.error("SQLException: " + ex.getMessage());
                logger.error("SQLState: " + ex.getSQLState());
                logger.error("VendorError: " + ex.getErrorCode());
            	ex.printStackTrace();
            }
        }
        else {
            logger.debug("MySQLConnectionManager: returning already open connection to " + url);
        }
    }

    /**
     * @param properties
     */
    private void loadProperties(Properties properties) {
        user = (String) properties.get("everest.database.user");
        password = (String) properties.get("everest.database.password");
        host = (String) properties.get("everest.database.host");
        database = (String) properties.get("everest.database.schema");
    }
}
