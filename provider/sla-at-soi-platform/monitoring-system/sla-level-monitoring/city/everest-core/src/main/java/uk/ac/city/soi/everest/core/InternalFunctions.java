/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * InternalFunctions.java
 *
 * Created on 31 July 2005, 14:51
 */

package uk.ac.city.soi.everest.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.monitor.Predicate;
import uk.ac.city.soi.everest.util.InternalFunctionUtility;
import uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface;

/**
 * This class implements the execution of internal functions.
 * 
 * @author Khaled Mahbub
 */
public class InternalFunctions {

    private static final Logger log = Logger.getLogger(InternalFunctions.class.getName());

    /**
     * Creates a new instance of InternalFunctions.
     * 
     */
    public InternalFunctions() {
    }

    /**
     * Executes three parameter functions and returns the result. Three parameter functions are series, 
     * For the series function the second parameter is a variable, the third parameter is a fluent and the fourth
     * parameter is period. The series function returns an array of values for the variable for all the initiated
     * fluent in the range (current time - period). 
     * 
     * @param opnName -
     *            name of the function
     * @param var1 -
     *            first parameter of the function
     * @param var2 -
     *            second parameter of the function
     * @param var3 -
     *            third parameter of the function                 
     */
    public Variable call(String opnName, Variable var1, Variable var2, Variable var3) {
        Variable fVar = null;
        if(opnName.equals(Constants.FUNCTION_SER)){
        	String varType = "";
        	String varName = var1.getName();
        	fVar = new Variable(varName, "", "");        	
        	FluentEntityManagerInterface fluentStorer = InternalFunctionUtility.getFluentStorer();
        	String fluentName = var2.getObjValue();
        	//System.out.println("33333 series function for " + fluentName);
        	long currentEventSourceTime = -1;
        	if(InternalFunctionUtility.getFirstEventTime() > InternalFunctionUtility.getMonitorSessionStartTime())
        		currentEventSourceTime = System.currentTimeMillis() + (InternalFunctionUtility.getFirstEventTime() - InternalFunctionUtility.getMonitorSessionStartTime());
        	else
        		currentEventSourceTime = System.currentTimeMillis() - (InternalFunctionUtility.getMonitorSessionStartTime() - InternalFunctionUtility.getFirstEventTime());
        	long time = currentEventSourceTime - Long.parseLong(var3.getObjValue());        	 
        	ArrayList<Predicate> preds = fluentStorer.checkForInitiatedFluents(time, fluentName);
        	for(Predicate pred : preds){
        		if(pred.getEcName().equals(Constants.PRED_INITIATES)){
        			//System.out.println("5555555 found fluents for series function");
        			Fluent fluent = pred.getFluent();
        			ArrayList<Variable> fluentVars = fluent.getArguments();
        			for(Variable var : fluentVars){
        				if(var.getName().equals(varName)){
        					//System.out.println("2222Found var " + var.getName());
        					varType = var.getType();
        					if(var.getType().endsWith("Array")){
        						if(var.getValues().size() > 0) fVar.addValues(var.getValues());
        					}
        					else{
        						
        						String val = var.getObjValue();
        						//System.out.println("1.51.51.51.51Found value " + var.getObjValue());
        						if(!val.equals("") && !val.equals(null)){
        							//System.out.println("11111Adding value " + var.getObjValue());
        							fVar.addValue(var.getObjValue());	
        						}
        						        					
        					}
        				}
        			}
        		}
        	}        	
        	if(varType.endsWith("Array")) fVar.setType(varType);
        	else fVar.setType(varType + "Array");
        }
        else if(opnName.equals(Constants.FUNCTION_VAL)){
        	String varType = "";
        	String varName = var1.getName();
        	fVar = new Variable(varName, "", "");        	
        	FluentEntityManagerInterface fluentStorer = InternalFunctionUtility.getFluentStorer();
        	String fluentName = var2.getObjValue();
        	//System.out.println("33333 series function for " + fluentName);
        	long currentEventSourceTime = -1;
        	if(InternalFunctionUtility.getFirstEventTime() > InternalFunctionUtility.getMonitorSessionStartTime())
        		currentEventSourceTime = System.currentTimeMillis() + (InternalFunctionUtility.getFirstEventTime() - InternalFunctionUtility.getMonitorSessionStartTime());
        	else
        		currentEventSourceTime = System.currentTimeMillis() - (InternalFunctionUtility.getMonitorSessionStartTime() - InternalFunctionUtility.getFirstEventTime());
        	long time = currentEventSourceTime - Long.parseLong(var3.getObjValue());        	 
        	ArrayList<Predicate> preds = fluentStorer.checkForInitiatedFluents(time, fluentName);
        	//for(Predicate pred : preds){
        	if(preds.size() > 0){
        		Predicate pred = preds.get(preds.size() - 1);
        		if(pred.getEcName().equals(Constants.PRED_INITIATES)){
        			//System.out.println("5555555 found fluents for series function");
        			Fluent fluent = pred.getFluent();
        			ArrayList<Variable> fluentVars = fluent.getArguments();
        			for(Variable var : fluentVars){
        				if(var.getName().equals(varName)){
        					//System.out.println("2222Found var " + var.getName());
        					varType = var.getType();
        					if(var.getType().endsWith("Array")){
        						if(var.getValues().size() > 0) fVar.addValues(var.getValues());
        					}
        					else{
        						
        						String val = var.getObjValue();
        						//System.out.println("1.51.51.51.51Found value " + var.getObjValue());
        						if(!val.equals("") && !val.equals(null)){
        							//System.out.println("11111Adding value " + var.getObjValue());
        							fVar.addValue(var.getObjValue());	
        						}
        						        					
        					}
        				}
        			}
        		}
        	}        	
        	if(varType.endsWith("Array")) fVar.setType(varType);
        	else fVar.setType(varType + "Array");
        } 
        return fVar;
    }
    
    /**
     * Executes two parameter functions and returns the result. Two parameter functions are add,sub,mult,div, append,
     * del, delAll. In case of append, del, delAll the return value is insignificant. For these functions the second
     * parameter is a variable of type array, and it contains the final result.
     * 
     * @param opnName -
     *            name of the function
     * @param var1 -
     *            first parameter of the function
     * @param var2 -
     *            second parameter of the function
     */
    public Variable call(String opnName, Variable var1, Variable var2) {
        Variable fVar = null;
        if (var1.getType().endsWith("Array")) {
            if (opnName.equals(Constants.FUNCTION_APP)) {
                if (var2.getValues().size() > 0)
                    var1.addValues(var2.getValues());
                else
                    var1.addValue(var2.getValue().toString());
                fVar = var1;
            }
            else if (opnName.equals(Constants.FUNCTION_DEL)) {
                if (var2.getValues().size() > 0) {
                    var1.delValues(var2.getValues());
                }
                else {
                    var1.delValue(var2.getValue().toString());
                }
                fVar = var1;
            }
            else if (opnName.equals(Constants.FUNCTION_DELA)) {
                var1.delAllValues();
                fVar = var1;
            }
        }
        else {
            if ((var1.getType().equals(Constants.DATA_TYPE_INT) || var1.getType().equals(Constants.DATA_TYPE_DOUBLE) || var1
                    .getType().equals(Constants.DATA_TYPE_LONG))
                    && (var2.getType().equals(Constants.DATA_TYPE_INT)
                            || var2.getType().equals(Constants.DATA_TYPE_DOUBLE)
                            || var2.getType().equals(Constants.DATA_TYPE_LONG) || var2.getType().equals(
                            Constants.DATA_TYPE_CONST))) {
                fVar = new Variable("", Constants.DATA_TYPE_DOUBLE, "");
                double val = 0.0;
                if (opnName.equals(Constants.FUNCTION_ADD)) {
                    val =
                            Double.parseDouble(var1.getObject().toString())
                                    + Double.parseDouble(var2.getObject().toString()); // here //
                    // Double.parseDouble(var2.getValue())
                }
                else if (opnName.equals(Constants.FUNCTION_SUB)) {
                    log.debug("[TT] about to convert longVar1...." + var1.getObject().toString());
                    double longVar1 = Double.parseDouble(var1.getObject().toString());
                    log.debug("[TT] longVar1 = " + longVar1);
                    double longVar2 = Double.parseDouble(var2.getObject().toString());
                    log.debug("[TT] longVar2 = " + longVar2);
                    val = longVar1 - longVar2; // here
                }

                else if (opnName.equals(Constants.FUNCTION_MUL)) {
                    val =
                            Double.parseDouble(var1.getObject().toString())
                                    * Double.parseDouble(var2.getObject().toString()); // here
                }
                else if (opnName.equals(Constants.FUNCTION_DIV)) {
                    log.debug("[MC] about to convert longVar1...." + var1.getObject().toString());
                    double longVar1 = Double.parseDouble(var1.getObject().toString());
                    log.debug("[MC] longVar1 = " + longVar1);
                    double longVar2 = Double.parseDouble(var2.getObject().toString());
                    log.debug("[MC] longVar2 = " + longVar2);
                    val = longVar1 / longVar2; // here
                }
                else if (opnName.equals(Constants.FUNCTION_MDL)) {
                    log.debug("[MC] about to convert longVar1...." + var1.getObject().toString());
                    double longVar1 = Double.parseDouble(var1.getObject().toString());
                    log.debug("[MC] longVar1 = " + longVar1);
                    double longVar2 = Double.parseDouble(var2.getObject().toString());
                    log.debug("[MC] longVar2 = " + longVar2);
                    val = longVar1 % longVar2; // here
                }
                else if (opnName.equals(Constants.FUNCTION_RND)) {
                    log.debug("[MC] about to convert longVar1...." + var1.getObject().toString());
                    double longVar1 = Double.parseDouble(var1.getObject().toString());
                    log.debug("[MC] longVar1 = " + longVar1);
                    int longVar2 = Integer.parseInt(var2.getObject().toString());
                    log.debug("[MC] longVar2 = " + longVar2);
                    double p = (double)Math.pow(10,longVar2);
                    double Rval = longVar1 * p;
                    double tmp = Math.round(Rval);                    
                    val = (double)tmp/p; // here
                }

                fVar.setValue(String.valueOf(val));
            }
        }
        return fVar;
    }

    /**
     * Executes single parameter functions and returns the result. Single parameter functions are sum,avg,max,min, size,
     * inc, dec.
     * 
     * @param opnName -
     *            name of the function
     * @param var -
     *            parameter of the function
     */
    public Variable call(String opnName, Variable var) {
        Variable fVar = null;
        if (var.getType().endsWith("Array")) {
            fVar = new Variable("", Constants.DATA_TYPE_DOUBLE, "");
            String[] values = var.getStringArray();
            double val = 0.0;
            if (opnName.equals(Constants.FUNCTION_SUM)) {
                val = getSum(values);
            }
            else if (opnName.equals(Constants.FUNCTION_AVG)) {
                val = getSum(values);
                val /= values.length;
            }
            else if (opnName.equals(Constants.FUNCTION_STD)) {
                double sum = getSum(values);
                double squareSum = getSqrSum(values);
                double mean = sum/values.length;                 
                val = Math.sqrt( squareSum/values.length - mean*mean );
            }
            else if (opnName.equals(Constants.FUNCTION_MED)) {                         
                val = getMedian(values);
            }
            else if (opnName.equals(Constants.FUNCTION_MOD)) {                         
                val = getMode(values);
            }
            else if (opnName.equals(Constants.FUNCTION_MAX)) {
                val = Double.parseDouble(values[0]);
                for (int i = 0; i < values.length; i++)
                    if (val < Double.parseDouble(values[i]))
                        val = Double.parseDouble(values[i]);
            }
            else if (opnName.equals(Constants.FUNCTION_MIN)) {
                val = Double.parseDouble(values[0]);
                for (int i = 0; i < values.length; i++)
                    if (val > Double.parseDouble(values[i]))
                        val = Double.parseDouble(values[i]);
            }
            else if (opnName.equals(Constants.FUNCTION_MIN)) {
                val = Double.parseDouble(values[0]);
                for (int i = 0; i < values.length; i++)
                    if (val > Double.parseDouble(values[i]))
                        val = Double.parseDouble(values[i]);
            }
            else if (opnName.equals(Constants.FUNCTION_SIZ)) {
                val = var.getValues().size();
            }
            fVar.setValue(String.valueOf(val));
            fVar.setObject(String.valueOf(val));
        }
        else { // inc or dec function on signle variable (non array)
            // [MC]: if var is static

            try {
                if (var.isStatic()) {
                    if (var.getType().equals(Constants.DATA_TYPE_INT)) {
                        int val = 0;
                        val = (Integer.parseInt((String) var.getValue())) + 1;
                        var.setValue(String.valueOf(val));
                    }
                    else if (var.getType().equals(Constants.DATA_TYPE_DOUBLE)) {
                        double val = 0;
                        val = (Double.parseDouble((String) var.getValue())) + 1;
                        var.setValue(String.valueOf(val));
                    }
                    else if (var.getType().equals(Constants.DATA_TYPE_LONG)) {
                        long val = 0;
                        val = (Long.parseLong((String) var.getValue())) + 1;
                        var.setValue(String.valueOf(val));
                    }
                    else {
                        throw new Exception();
                    }
                    return var;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                // TODO: handle exception
            }

            if ((var.getType().equals(Constants.DATA_TYPE_INT) || var.getType().equals(Constants.DATA_TYPE_DOUBLE) || var
                    .getType().equals(Constants.DATA_TYPE_LONG))) {
                fVar = new Variable("", Constants.DATA_TYPE_DOUBLE, "");
                double val = 0.0;
                if (opnName.equals(Constants.FUNCTION_INC))
                    val = Double.valueOf((String) var.getValue()) + 1.0; // parseDouble(var.getValue())
                else if (opnName.equals(Constants.FUNCTION_DEC))
                    val = Double.valueOf((String) var.getValue()) - 1.0; // here
                fVar.setValue(String.valueOf(val));
            }
        }

        if (!var.getType().contains(Constants.ARRAY)) {
            fVar.setType(var.getType());
        }
        else {
            fVar.setType(Constants.DATA_TYPE_DOUBLE);
        }
        fVar.setConstName(var.getConstName());
        fVar.setName(var.getName());

        return fVar;
    }

    /**
     * Executes no parameter functions and returns the result. No parameter functions are: systemTime, getDay, getTime.
     * 
     * @param opnName -
     *            name of the function
     */
    public Variable call(String opnName) {
        Variable fVar = new Variable("", "", "", "");

        // systemTime() function call
        if (opnName.equals(Constants.FUNCTION_GET_SYSTEM_TIME)) {
            fVar.setValue((long) System.currentTimeMillis());
            fVar.setType(Constants.DATA_TYPE_LONG);
            fVar.setConstName(Constants.SYSTEM_TIME);
            fVar.setName("v" + fVar.getConstName());
        }
        // getDay() function call
        if (opnName.equals(Constants.FUNCTION_GET_DAY)) {
            fVar.setType(Constants.DATA_TYPE_STRING);
            fVar.setConstName(Constants.DAY);
            fVar.setName("v" + fVar.getConstName());

            long timestamp = System.currentTimeMillis();
            Date date = new Date(timestamp);
            GregorianCalendar calendar = new GregorianCalendar();              
            int day = date.getDay() + 1;
            if (day == calendar.SUNDAY) {
                fVar.setValue(Constants.DAY_SUNDAY);
            }
            else if (day == calendar.MONDAY) {
                fVar.setValue(Constants.DAY_MONDAY);
            }
            else if (day == calendar.TUESDAY) {
                fVar.setValue(Constants.DAY_TUESDAY);
            }
            else if (day == calendar.WEDNESDAY) {
                fVar.setValue(Constants.DAY_WEDNESDAY);
            }
            else if (day == calendar.THURSDAY) {
                fVar.setValue(Constants.DAY_THURSDAY);
            }
            else if (day == calendar.FRIDAY) {
                fVar.setValue(Constants.DAY_FRIDAY);
            }
            else if (day == calendar.SATURDAY) {
                fVar.setValue(Constants.DAY_SATURDAY);
            }
        }
        // 	getMonth() function call
        if (opnName.equals(Constants.FUNCTION_GET_MONTH)) {
        	ArrayList<String> months = new ArrayList<String>();
        	months.add("JANUARY");months.add("FEBRUARY");months.add("MARCH");
        	months.add("APRIL");months.add("MAY");months.add("JUNE");
        	months.add("JULY");months.add("AUGUST");months.add("SEPTEMBER");
        	months.add("OCTOBER");months.add("NOVEMBER");months.add("DECEMBER");
        	Calendar cal = Calendar.getInstance();
        	int month = cal.get(Calendar.MONTH);
            fVar.setType(Constants.DATA_TYPE_STRING);
            fVar.setConstName(Constants.MONTH);
            fVar.setName("v" + fVar.getConstName());
            fVar.setValue(months.get(month));
        }
        // 	getYear() function call
        if (opnName.equals(Constants.FUNCTION_GET_YEAR)) {        	
        	Calendar cal = Calendar.getInstance();
        	int year = cal.get(Calendar.YEAR);
            fVar.setType(Constants.DATA_TYPE_INT);
            fVar.setConstName(Constants.YEAR);
            fVar.setName("v" + fVar.getConstName());
            fVar.setValue(year);
        }        
        // getTime() function call
        if (opnName.equals(Constants.FUNCTION_GET_TIME)) {
            fVar.setType(Constants.DATA_TYPE_DOUBLE);
            fVar.setConstName(Constants.TIME);
            fVar.setName("v" + fVar.getConstName());

            long timestamp = System.currentTimeMillis();
            Date date = new Date(timestamp);
            fVar.setValue(((Integer) (date.getHours())).toString());
        }

        return fVar;
    }

    /**
     * Returns the sum of a list of values. Where list contains string representation of numerical values.
     * 
     * @param values
     * @return
     */
    private double getSum(String[] values) {
        double val = 0.0;
        for (int i = 0; i < values.length; i++)
            val += Double.parseDouble(values[i]);
        return val;
    }
    
    private double getSqrSum(String[] values) {
        double val = 0.0;
        for (int i = 0; i < values.length; i++)
            val += Double.parseDouble(values[i]) * Double.parseDouble(values[i]);
        return val;
    }
    
    private double getMedian(String[] values) {
        double val = 0.0;
        double[] t = new double[values.length];
        
        for (int i = 0; i < values.length; i++)
            t[i] = Double.parseDouble(values[i]);
        Arrays.sort(t);

        if (t.length % 2 == 0)
        	val = (t[(t.length / 2) - 1] + t[t.length / 2]) / 2.0;
        else
        	val = t[t.length / 2];        
        return val;
    }

    public double getMode(String[] values) {
    	double maxValue = -1;
    	int maxCount = -1;

    	for (int i = 0; i < values.length; ++i) {
    		int count = 0;
    		for (int j = 0; j < values.length; ++j) {
    			if (Double.parseDouble(values[j]) == Double.parseDouble(values[i])) ++count;
    		}
    		if (count > maxCount) {
    			maxCount = count;
    			maxValue = Double.parseDouble(values[i]);
    		}
    	}
    	return maxValue;
    }
    	
}
