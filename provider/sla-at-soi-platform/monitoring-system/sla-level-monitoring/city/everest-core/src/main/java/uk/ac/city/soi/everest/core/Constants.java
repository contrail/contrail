/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Constants.java
 *
 * Created on 30 May 2004, 19:11 
 */

package uk.ac.city.soi.everest.core;

import java.io.Serializable;

/**
 * This class defines a set of constants that are used through out the project.
 * 
 * @author Khaled Mahbub
 */
public class Constants implements Serializable {

    public static final long TIME_UD = -1; // time undefined
    public static final long RANGE_UB = Long.MAX_VALUE - 500;
    public static final long RANGE_LB = -1;
    public static final String VAR_UD = "Undefined"; // variable un defined
    public static final String DEFAULT_FLUENT = "valueOf";
    public static final String TRUTH_VAL_UK = "Unknown";
    public static final String TRUTH_VAL_TRUE = "True";
    public static final String TRUTH_VAL_FALSE = "False";
    public static final String SOURCE_RE = "Recorded";
    public static final String SOURCE_DE = "Derived";
    public static final String SOURCE_ABD = "Abduced";
    public static final String SOURCE_NF = "Negation_As_Failure";
    public static final String SOURCE_UD = "Undefined";
    public static final String SOURCE_EV = "Evaluated";
    public static final String QUANT_EXIST = "existential";
    public static final String QUANT_FORALL = "forall";
    public static final String TYPE_FUTURE = "Future_Formula";
    public static final String TYPE_PAST = "Past_Formula";
    public static final String STATUS_SAT = "Satisfied";
    public static final String STATUS_UD = "Undefined";
    public static final String STATUS_ND = "No_Decision";
    public static final String STATUS_IRB = "Inconsistency_WRT_Recorded_Behaviour";
    public static final String STATUS_IEB = "Inconsistency_WRT_Expected_Behaviour";
    public static final String STATUS_UB = "Unjustified_Behaviour";
    public static final String STATUS_PIEB = "Possible_Inconsistency_WRT_Expected_Behaviour";
    public static final String STATUS_PUB = "Potentially_Unjustified_Behaviour";
    public static final String STATUS_THREAT = "Detected Threat";
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final long minT = 1;
    public static final int STORE_MM = 0;
    public static final int STORE_DB = 1;
    public static final int STORE_FL = 2;
    public static final int STORE_RF = 3;
    public static final int EVENTS_RECORDED = 0;
    public static final int EVENTS_MIXED = 1;
    public static final String LESS_THAN = "<";
    public static final String LESS_THAN_EQUAL = "<=";
    public static final String GREATER_THAN = ">";
    public static final String GREATER_THAN_EQUAL = ">=";
    public static final String EQUAL = "=";
    public static final String NOT_EQUAL = "!=";
    public static final String ABDUCTIVE = "abductive";
    public static final String DEDUCTIVE = "deductive";
    public static final int MON_REC = 0; // use one template set for recorded events only
    public static final int MON_RECDER = 1; // use one template set for recorded and derived events only
    public static final int MON_REC_RECDER = 2; // use two template sets, one for recorded events and one for recorded
                                                // and derived events
    public static final String DATA_TYPE_OBJECT = "object";
    public static final String DATA_TYPE_CONST = "constant";
    public static final String DATA_TYPE_INT = "int";
    public static final String DATA_TYPE_DOUBLE = "double";
    public static final String DATA_TYPE_STRING = "string";
    public static final String DATA_TYPE_BOOL = "boolean";
    public static final String DATA_TYPE_LONG = "long";
    public static final String DATA_TYPE_INT_ARRAY = "intArray";
    public static final String DATA_TYPE_DOUBLE_ARRAY = "doubleArray";
    public static final String DATA_TYPE_STRING_ARRAY = "stringArray";
    public static final String DATA_TYPE_LONG_ARRAY = "longArray";
    public static final String PARTNER_SELF = "self";
    public static final String FUNCTION_ADD = "add";
    public static final String FUNCTION_SUB = "sub";
    public static final String FUNCTION_AVG = "avg";
    public static final String FUNCTION_MUL = "mult";
    public static final String FUNCTION_DIV = "div";
    public static final String FUNCTION_MDL = "modulo";
    public static final String FUNCTION_RND = "round";
    public static final String FUNCTION_INC = "inc";
    public static final String FUNCTION_DEC = "dec";
    public static final String FUNCTION_SUM = "sum";
    public static final String FUNCTION_STD = "std";
    public static final String FUNCTION_MED = "median";
    public static final String FUNCTION_MOD = "mode";
    public static final String FUNCTION_MAX = "max";
    public static final String FUNCTION_MIN = "min";
    public static final String FUNCTION_SIZ = "size";
    public static final String FUNCTION_APP = "append";
    public static final String FUNCTION_DEL = "del";
    public static final String FUNCTION_DELA = "delAll";
    public static final String FUNCTION_SER = "series";
    public static final String FUNCTION_VAL = "value";   
    public static final String FUNCTION_GET_SYSTEM_TIME = "getSystemTime()";
    public static final String FUNCTION_GET_DAY = "getDay()";
    public static final String FUNCTION_GET_TIME = "getTime()";
    public static final String FUNCTION_GET_MONTH = "getMonth()";
    public static final String FUNCTION_GET_YEAR = "getYear()";
    public static final String RELATION_VV = "vv"; // permitted fluent type
    public static final String RELATION_VC = "vc";
    public static final String RELATION_VF = "vf"; // permitted fluent type
    public static final String RELATION_CV = "cv";
    public static final String RELATION_CC = "cc";
    public static final String RELATION_CF = "cf";
    public static final String RELATION_FV = "fv";
    public static final String RELATION_FC = "fc";
    public static final String RELATION_FF = "ff";
    public static final String NEGATED = "negated";
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String PREFIX_TERM = "_term";
    public static final String PREFIX_IR = "ir";
    public static final String PREFIX_IC = "ic";
    public static final String PREFIX_AS = "as";
    public static final String PREFIX_RC = "rc";
    public static final String PREFIX_RE = "re";
    public static final String PKG = "uk.ac.city.soi.everest.datatypes";
    public static final String CLASSPATH = "C:/Monitor/jars/";
    public static final String ATTACK_TYPE_BLOCK = "block";
    public static final String ATTACK_TYPE_DELAY = "delay";
    public static final String ATTACK_TYPE_ALTER = "alter";
    public static final String DAY_SUNDAY = "SUNDAY";
    public static final String DAY_MONDAY = "MONDAY";
    public static final String DAY_TUESDAY = "TUESDAY";
    public static final String DAY_WEDNESDAY = "WEDNESDAY";
    public static final String DAY_THURSDAY = "THURSDAY";
    public static final String DAY_FRIDAY = "FRIDAY";
    public static final String DAY_SATURDAY = "SATURDAY";
    public static final String STATUS_REQUEST_BEFORE = "REQ-B";
    public static final String STATUS_REQUEST_AFTER = "REQ-A";
    public static final String STATUS_RESPONSE_BEFORE = "RES-B";
    public static final String STATUS_RESPONSE_AFTER = "RES-A";

    // constants that are related to formulas XML specifications
    public static final String VARIABLE = "variable";
    public static final String OPERATION_CALL = "operationCall";
    public static final String NAME = "name";
    public static final String ARGUMENTS = "arguments";
    public static final String PREDICATE = "predicate";
    public static final String PRED_RELATION = "Relation";
    public static final String PRED_HAPPENS = "Happens";
    public static final String PRED_INITIATES = "Initiates";
    public static final String PRED_TERMINATES = "Terminates";
    public static final String PRED_HOLDSAT = "HoldsAt";
    public static final String PRED_INITIALLY = "Initially";
    public static final String FLUENT = "fluent";
    public static final String CONSTRAINED_PREDICATES_SUBFORMULA = "constrainedPredicatesSubformula";
    public static final String CONSTRAINED_PREDICATES = "constrainedPredicates";
    public static final String CONSTRAINTS = "constraints";
    public static final String CONSTRAINT = "constraint";
    public static final String BODY = "body";
    public static final String HEAD = "head";
    public static final String TIME_VARIABLE = "timeVar";
    public static final String TIME_EXPRESSION = "timeExpression";
    public static final String PARTNER = "partner";
    public static final String EXPRESSION = "expresion";
    public static final String VARIABLE_NAME = "varName";
    public static final String VARIABLE_VALUE = "value";
    public static final String ARTIFICIAL_TIME_VARIABLE = "artificialTimeVar";
    public static final String ARRAY = "Array";
    public static final String SYSTEM_TIME = "systemTime";
    public static final String DAY = "day";
    public static final String TIME = "time";
    public static final String MONTH = "month";
    public static final String YEAR = "year";

    // periodic events constants
    public static final String PERIODIC_EVENT_OPERATION_NAME = "periodic";
    public static final String PERIODIC_EVENT_PERIOD = "period";
    public static final String PERIODIC_EVENT_PERIOD_START_TIME = "periodStartTime";
    
    public static final String VAR_NAME_FLUENT_NAME = "FLUENT_NAME";
    public static final String VAR_NAME_PERIOD = "PERIOD";

//    /**
//     * Creates a new instance of Constants.
//     */
//    public Constants() {
//    }
}
