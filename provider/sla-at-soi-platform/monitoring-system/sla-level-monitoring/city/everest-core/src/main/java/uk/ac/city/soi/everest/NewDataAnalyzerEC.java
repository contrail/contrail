/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import uk.ac.city.soi.everest.SRNTEvent.ArgumentType;
import uk.ac.city.soi.everest.SRNTEvent.Entity;
import uk.ac.city.soi.everest.SRNTEvent.OpStatus;
import uk.ac.city.soi.everest.SRNTEvent.SRNTEvent;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.conf.MonitorProperties;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.Template;

/**
 * This class works as an interface to DataAnalyzerEC class, and DataAnalyzerEC class works as an interface to
 * SERENITYAnalyzer class (i.e. the monitor). Actually DataAnalyzerEC was developed an interface to SeCSEAnalyzer in
 * SeCSE project. This class was developed by Kostas Ballas to wrap that. To deploy the monitor as web service this
 * class should be used as interface.
 * 
 * @author Khaled Mahbub
 * 
 */
public class NewDataAnalyzerEC {
    public static String EVEREST_HOME = "EVEREST_HOME";
    public static final String EVEREST_PROPERTIES_FILE_NAME = "everest.properties";
    public static final String EVEREST_DATABASE_PROPERTIES_FILE_NAME = "everest.database.properties";

    // logger
    private static Logger logger = Logger.getLogger(NewDataAnalyzerEC.class);

    DataAnalyzerEC newDA;
    static String pkg = Constants.PKG;

    /**
     * Constructor.
     * 
     * @param everestHome
     */
    public NewDataAnalyzerEC(String everestHome) {
        setEverestHome(everestHome);

        try {
            loadProperties();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } 
        /*
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
		*/
        newDA = new DataAnalyzerEC();
    }

    /**
     * @throws IOException
     * @throws FileNotFoundException
     * 
     */
    private void loadProperties() throws FileNotFoundException, IOException {
        MonitorProperties.getProperties().setProperty("everest.properties", EVEREST_PROPERTIES_FILE_NAME);
        MonitorProperties.getProperties().setProperty("everest.database.properties",
                EVEREST_DATABASE_PROPERTIES_FILE_NAME);

        // Loads uk.ac.city.soi.everest properties
        Properties everestProperties = new Properties();
        everestProperties.load(new FileInputStream(new File(MonitorProperties.getProperties().getProperty(EVEREST_HOME)
                + "/conf/" + EVEREST_PROPERTIES_FILE_NAME)));

        for (Object key : everestProperties.keySet()) {
            MonitorProperties.getProperties().setProperty((String) key,
                    everestProperties.getProperty((String) key, "property not set"));
        }

        // Loads uk.ac.city.soi.everest database properties
        Properties everestDatabaseProperties = new Properties();
        everestDatabaseProperties.load(new FileInputStream(new File(MonitorProperties.getProperties().getProperty(
                EVEREST_HOME)
                + "/conf/" + EVEREST_DATABASE_PROPERTIES_FILE_NAME)));

        for (Object key : everestDatabaseProperties.keySet()) {
            MonitorProperties.getProperties().setProperty((String) key,
                    everestDatabaseProperties.getProperty((String) key, "property not set"));
        }
        logger.debug("MonitorProperties.getProperties(): " + MonitorProperties.getProperties());
    }

    /**
     * Sets EVEREST home.
     * 
     * @param everestHome
     */
    public void setEverestHome(String everestHome) {
        MonitorProperties.getProperties().setProperty(EVEREST_HOME, everestHome);
        logger.debug("uk.ac.city.soi.everest home set to " + everestHome);
    }

    /**
     * Set the rules to be monitored.
     * 
     * @param xmlRules -
     *            String representation of the XML rules to be monitored
     * @return
     */
    /*
    public boolean setRules(String xmlRules) {
    	
        try {
            newDA.monitorRule(xmlRules);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
	*/
    /**
     * Receives an event and converts into LogEvent object that is used for runtime unification. The transformed
     * LogEvent object is passed to DataAnalyzerEC class.
     * 
     * @param event -
     *            String representation of Serenity event in XML
     * @return
     */
    /*
    public boolean recordEvent(String event) {
        // Date now = new Date();
        // printit(event,"C:\\JAVA\\workspace\\SERENITY_full_Trento\\eventLog\\recordevent.txt");
        SRNTEvent x = new SRNTEvent(event);
        // printit("after SRNTEvent","recordevent.txt");
        // logger.debug("x is: "+x.getClass().getName());
        // logger.debug("number of args: "+ x.getOperationArgumentsLenght());
        String status = null;
        if (x.getOperationStatus().startsWith("REQ")) {
            status = "ic";
        }
        else if (x.getOperationStatus().startsWith("RES")) {
            status = "ir";
        }
        else {
            logger.debug("no status found");
        }
        // printit("before LogEvent","recordevent.txt");
        LogEvent logEvent = new LogEvent("Happens", status, "Serenity", x.getOperationName(), x.getTimestamp());
        logEvent.setID(x.getID());
        // printit("after LogEvent","recordevent.txt");
        List<ArgumentType> argsListRaw = x.getOperationArguments().getArgument();
        List<uk.ac.city.soi.everest.core.Variable> argsListCooked =
                new ArrayList<uk.ac.city.soi.everest.core.Variable>();
        // printit("1","recordevent.txt");
        // add Status to variables
        OpStatus os = new OpStatus(x.getOperationStatus());
        Variable statusVar = new Variable("status", os.getClass().getSimpleName(), os);
        argsListCooked.add(statusVar);
        logEvent.addVariable(statusVar);
        // printit("2","recordevent.txt");
        // add Sender to variables
        Entity sender = x.getSender();
        Variable senderVar = new Variable("sender", sender.getClass().getSimpleName(), sender);
        argsListCooked.add(senderVar);
        logEvent.addVariable(senderVar);
        // printit("3","recordevent.txt");
        // add Receiver to variables
        Entity receiver = x.getReceiver();
        Variable receiverVar = new Variable("receiver", receiver.getClass().getSimpleName(), receiver);
        argsListCooked.add(receiverVar);
        logEvent.addVariable(receiverVar);
        // printit("4","recordevent.txt");
        // add Source to variables
        Entity source = x.getSource();
        Variable sourceVar = new Variable("source", source.getClass().getSimpleName(), source);
        argsListCooked.add(sourceVar);
        logEvent.addVariable(sourceVar);

        logEvent.setPartnerId(source.getIpAddress()); // use partner field, to store source IP address, used for
        // logical clock synchronization

        // logger.debug("Setting partner ID " + source.getIpAddress());

        // printit("5","recordevent.txt");
        for (int i = 0; i < argsListRaw.size(); i++) {
            ArgumentType cur = argsListRaw.get(i);
            try {
                // printit(i+"before translate","recordevent.txt");
                Object tmp = Translate(cur, pkg);
                // printit(i+"after translate","recordevent.txt");
                if (tmp != null) {
                    // printit(i+"write file","recordevent.txt");
                    FileOutputStream fos = new FileOutputStream("t.tmp");
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(tmp);
                    oos.close();
                    // printit(i+"after file","recordevent.txt");
                    Variable var = new Variable(cur.getName(), cur.getType(), tmp);
                    argsListCooked.add(var);
                    logEvent.addVariable(var);
                    // printit(i+"add var to logevent","recordevent.txt");

                }
                else {
                    // printit(i+"else add var to logevent","recordevent.txt");
                    Variable var = new Variable(cur.getName(), cur.getType(), tmp);
                    argsListCooked.add(var);
                    logEvent.addVariable(var);
                    // throw (new Exception("NullObj"));
                    // printit(i+"else done add var to logevent","recordevent.txt");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                printit(e.getMessage(), "recordevent.txt");
            }
        }

        // Testing LogEvent
        
        LogEvent eventCopy = new LogEvent(logEvent);
        eventCopy.setNegated(true);
        eventCopy.toString(true);
        logger.debug("eventCopy.isNegated(): " + eventCopy.isNegated());
		
        newDA.notify(logEvent);
        return true;
    }
	*/
    /**
     * Receives a TypeOfEvent object event and converts it into LogEvent object that is used for runtime unification.
     * The converted LogEvent object is passed to DataAnalyzerEC class.
     * 
     * @param event -
     *            TypeOfEvent object
     * @return
     */
    public boolean receiveTypeOfEventObject(TypeOfEvent event) {
        // Date now = new Date();
        // printit(event,"C:\\JAVA\\workspace\\SERENITY_full_Trento\\eventLog\\recordevent.txt");
        SRNTEvent x = new SRNTEvent(event);
        // printit("after SRNTEvent","recordevent.txt");
        // logger.debug("x is: "+x.getClass().getName());
        // logger.debug("number of args: "+ x.getOperationArgumentsLenght());
        String status = null;
        if (x.getOperationStatus().startsWith("REQ")) {
            status = "ic";
        }
        else if (x.getOperationStatus().startsWith("RES")) {
            status = "ir";
        }
        else {
            logger.debug("no status found");
        }
        // printit("before LogEvent","recordevent.txt");
        LogEvent logEvent = new LogEvent("Happens", status, "Serenity", x.getOperationName(), x.getTimestamp());
        logEvent.setID(x.getID());
        // printit("after LogEvent","recordevent.txt");
        List<ArgumentType> argsListRaw = x.getOperationArguments().getArgument();
        List<uk.ac.city.soi.everest.core.Variable> argsListCooked =
                new ArrayList<uk.ac.city.soi.everest.core.Variable>();
        // printit("1","recordevent.txt");
        // add Status to variables
        OpStatus os = new OpStatus(x.getOperationStatus());
        Variable statusVar = new Variable("status", os.getClass().getSimpleName(), os);
        argsListCooked.add(statusVar);
        logEvent.addVariable(statusVar);
        // printit("2","recordevent.txt");
        // add Sender to variables
        Entity sender = x.getSender();
        Variable senderVar = new Variable("sender", sender.getClass().getSimpleName(), sender);
        argsListCooked.add(senderVar);
        logEvent.addVariable(senderVar);
        // printit("3","recordevent.txt");
        // add Receiver to variables
        Entity receiver = x.getReceiver();
        Variable receiverVar = new Variable("receiver", receiver.getClass().getSimpleName(), receiver);
        argsListCooked.add(receiverVar);
        logEvent.addVariable(receiverVar);
        // printit("4","recordevent.txt");
        // add Source to variables
        Entity source = x.getSource();
        Variable sourceVar = new Variable("source", source.getClass().getSimpleName(), source);
        argsListCooked.add(sourceVar);
        logEvent.addVariable(sourceVar);

        logEvent.setPartnerId(source.getIpAddress()); // use partner field, to store source IP address, used for
        // logical clock synchronization

        // logger.debug("Setting partner ID " + source.getIpAddress());

        // printit("5","recordevent.txt");
        for (int i = 0; i < argsListRaw.size(); i++) {
            ArgumentType cur = argsListRaw.get(i);
            try {
                // printit(i+"before translate","recordevent.txt");
                Object tmp = Translate(cur, pkg);
                // printit(i+"after translate","recordevent.txt");
                if (tmp != null) {
                    // printit(i+"write file","recordevent.txt");
                    FileOutputStream fos = new FileOutputStream("t.tmp");
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(tmp);
                    oos.close();
                    // printit(i+"after file","recordevent.txt");
                    Variable var = new Variable(cur.getName(), cur.getType(), tmp);
                    argsListCooked.add(var);
                    logEvent.addVariable(var);
                    // printit(i+"add var to logevent","recordevent.txt");

                }
                else {
                    // printit(i+"else add var to logevent","recordevent.txt");
                    Variable var = new Variable(cur.getName(), cur.getType(), cur.getValue());
                    argsListCooked.add(var);
                    logEvent.addVariable(var);
                    // throw (new Exception("NullObj"));
                    // printit(i+"else done add var to logevent","recordevent.txt");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                printit(e.getMessage(), "recordevent.txt");
            }
        }

        // logger.debug(event);
        newDA.notify(logEvent);
        return true;
    }

    /**
     * Receives a LogEvent object and passes it to DataAnalyzer.
     * 
     * @param event
     * @return
     */
    public boolean receiveLogEvent(LogEvent event) {
        try {
            newDA.notify(event);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method is used for object transformation. If an argument has object data type then corresponding Java object
     * is formed by this method. Template java objects are created by JAXB when a formula is parsed and stored in the
     * folder "datatypes".
     * 
     * @param arg -
     *            An argument in serenity event
     * @param pkg -
     *            location of the folder (package datatypes) where JAXB stored Java object Templates
     * @return
     * @throws Exception
     */
    private Object Translate(ArgumentType arg, String pkg) throws Exception {
        // logger.debug(arg.getName()+" "+arg.getType());
        Object returnObj = null;

        // printit("getStruct: "+arg.getStruct().toString(),"recordevent.txt");
        try {
            if (arg.getStruct() == null) {
                // simple type argument
                // printit("getType: "+arg.getType(),"recordevent.txt");
                if (arg.getValue() == null) {
                    return null;
                }
                else {
                    if (arg.getType().equals("string")) {
                        Element e = (Element) arg.getValue();
                        return new String(e.getFirstChild().getNodeValue());
                    }
                    else if (arg.getType().equals("int")) {
                        Element e = (Element) arg.getValue();
                        return new Integer(e.getFirstChild().getNodeValue());
                    }
                    else if (arg.getType().equals("double")) {
                        Element e = (Element) arg.getValue();
                        return new Double(e.getFirstChild().getNodeValue());
                    }
                    else if (arg.getType().equals("long")) {
                        Element e = (Element) arg.getValue();
                        return new Long(e.getFirstChild().getNodeValue());
                    }
                    else if (arg.getType().equals("boolean")) {
                        Element e = (Element) arg.getValue();
                        return new Boolean(e.getFirstChild().getNodeValue());
                    }
                    else {
                        throw (new Exception("BadType"));
                    }
                }
            }
            else {
                // printit("else....","recordevent.txt");
                try {
                    // object argument
                    // The dir contains the compiled classes.
                    File classesDir = new File(".");
                    // The parent classloader
                    ClassLoader parentLoader = NewDataAnalyzerEC.class.getClassLoader();
                    // Load class "sample.PostmanImpl" with our own classloader.
                    URLClassLoader loader1 = new URLClassLoader(new URL[] { classesDir.toURL() }, parentLoader);
                    Class c = loader1.loadClass(pkg + "." + arg.getType());
                    // Postman postman1 = (Postman) cls1.newInstance();
                    // Class c = Class.forName("uk.ac.city.soi.everest.datatypes."+arg.getType());
                    Object o = c.newInstance();
                    for (int i = 0; i < arg.getStruct().getArgument().size(); i++) {
                        Object fieldValue = Translate(arg.getStruct().getArgument().get(i), pkg);
                        String fieldName = arg.getStruct().getArgument().get(i).getName();
                        String fieldType = arg.getStruct().getArgument().get(i).getType();

                        Field f = c.getDeclaredField(fieldName);
                        Method m =
                                c.getMethod("set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1), f
                                        .getType());
                        Object[] arrayObj = new Object[1];
                        arrayObj[0] = fieldValue;
                        m.invoke(o, arrayObj);
                    }
                    returnObj = o;
                    // printit("else....DONE","recordevent.txt");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    //printit(e.getMessage(), "recordevent.txt");
                }
            }
        }
        catch (Exception ex) {
            //printit(ex.getMessage(), "recordevent.txt");
        }

        return returnObj;
    }

    /**
     * Given a formula ID this method checks if there is any monitoring result for that formula.
     * 
     * @param formulaID -
     *            ID of a formula
     * @return - String representation of the templates with monitoring decision
     */
    /*
    public String checkRule(String formulaID) {
        try {
            //logger.debug("checkRule executed..");
            String result = newDA.checkRule(formulaID);
            //logger.debug("NewDA: checkRule result: " + result);
            return result;

        }
        catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }
	*/
    /**
     * Given a preferred status, a timepoint and unique string, returns a collection of a monitoring result type
     * selected by time stamp and template id.
     * 
     * @param withStatus -
     *            the monitoring result type
     * @param afterTimepoint -
     *            the specific time stamp
     * @param includingInTemplateId -
     *            the string to be included in the result id
     * @return
     */
    public ArrayList<Template> getMonitoringResults(String withStatus, long afterTimepoint, String includingInTemplateId) {
        ArrayList<Template> monitoringResults = new ArrayList<Template>();
        try {
            monitoringResults = newDA.getMonitoringResults(withStatus, afterTimepoint, includingInTemplateId);

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }

        return monitoringResults;
    }

    /**
     * Used for debug purpose. It writes some information to some file.
     * 
     * @param what -
     *            what should be written
     * @param where -
     *            name of the file
     */
    private void printit(String what, String where) {
        try {
            // Create file
        	/*
            FileWriter fstream = new FileWriter(where, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write("\"<?xml version=\"1.0\" encoding=\"UTF-8\"?> ");
            out.write(what + "\",");
            out.newLine();
            // Close the output stream
            out.close();
            */
        }
        catch (Exception e) {// Catch exception if any
            //logger.debug("Error: " + e.getMessage());
        }
    }

    /**
     * Given a monitoring rule ID, this method informs analyzer to stop the monitoring process for the rule with the
     * given Id.
     * 
     * @param ruleID -
     *            ID of a rule
     * @author t7t
     */
    /*
    public String unsubscribeMonitoringRule(String ruleID) {
        try {
            newDA.stopMonitoringFormula(ruleID);
            return "OK. The rule with ID" + ruleID + "is not being monitored anymore";

        }
        catch (Exception e) {
            //e.printStackTrace();
            return e.getMessage();
        }

    }
	*/
    /**
     * Receives a linked hash map of formula objects and passes it to the analyzer.
     * 
     * @param formulas -
     *            linked hash map of formula objects
     * @return
     * @throws RemoteException
     */
    public boolean receiveFormulas(LinkedHashMap<String, Formula> formulas) throws RemoteException {

        logger.debug("formulas: " + formulas);

        try {
            newDA.receiveFormulas(formulas);
        }
        catch (Exception exp) {
            //exp.getCause();
            return false;
        }
        return true;

    }
    
    public boolean isBusy(){
    	return newDA.isBusy();
    }
}
