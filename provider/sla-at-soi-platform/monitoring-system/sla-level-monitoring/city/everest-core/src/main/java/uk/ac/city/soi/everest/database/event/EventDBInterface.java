/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.event;

import java.util.ArrayList;

import uk.ac.city.soi.everest.core.Signature;
import uk.ac.city.soi.everest.database.EntityManagerInterface;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.Range;

/**
 * Interface providing methods for maintaining and managing the event db.
 * 
 * 
 * @author Davide Lorenzoli
 * 
 * @date 6 Sep 2008
 */
public interface EventDBInterface extends EntityManagerInterface {
    /**
     * Add an event to the database.
     * 
     * @param event
     *            the event object
     */
    public void add(LogEvent event);

    /**
     * Get an event from the database.
     * 
     * @param eventId
     *            the event id
     * @return the found event, null if the event was not found
     */
    public LogEvent get(String eventId);

    /**
     * Get an event from the database.
     * 
     * @param offset
     *            the offset of the first row to return. The offset of the initial row is 0 (not 1).
     * @param numberRows
     *            the maximum number of rows to return.
     * @return all row between from offset to numberRows. For instance, if offset=5 and numberRows=10 it returns rows
     *         [6-15]
     */
    public ArrayList<LogEvent> getEvent(int offset, int numberRows);

    /**
     * Get all the events matching a given signature.
     * 
     * @param eventSignature
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(Signature eventSignature);

    /**
     * Get the event matching a given signature occurred at the time point that the given time stamp indicates.
     * 
     * @param eventSignature
     * @param timestamp
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvent(Signature eventSignature, long timestamp);

    /**
     * Get all the events matching a given signature and within a time range.
     * 
     * @param eventSignature
     * @param timeRange
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange);

    /**
     * Get all the events matching a given signature, within a time range, and coming from a given source.
     * 
     * @param eventSignature
     * @param timeRange
     * @param eventSourceId
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange, String eventSourceId);

    /**
     * Get all the events within a time range.
     * 
     * @param timeRange
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(Range timeRange);

    /**
     * Get all the event coming from a given source.
     * 
     * @param eventSourceId
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(String eventSourceId);

    /**
     * Get all the event coming from a given source and within a time range.
     * 
     * @param eventSourceId
     * @param timeRange
     * @return the ArrayList found events, an empty ArrayList if no events were found
     */
    public ArrayList<LogEvent> getEvents(String eventSourceId, Range timeRange);

    /**
     * Returns the time stamp of the selected captor, if not found it returns <uk.ac.city.soi.everest>Constant.TIME_UD</uk.ac.city.soi.everest>.
     * 
     * @param partnerId
     * @return the timestamp of the selected captor, if not found it returns <uk.ac.city.soi.everest>Constant.TIME_UD</uk.ac.city.soi.everest>
     */
    public long getCaptorTimestamp(String partnerId);
}
