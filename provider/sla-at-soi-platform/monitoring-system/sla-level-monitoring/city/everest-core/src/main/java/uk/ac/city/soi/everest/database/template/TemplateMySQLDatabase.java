/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.template;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.database.EntityManagerUtil;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.Predicate;
import uk.ac.city.soi.everest.monitor.Template;

/**
 * This class implements the TemplateEntityManagerInterface for MySQL db.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 11 Mar 2009
 */
public class TemplateMySQLDatabase extends EntityManagerUtil implements TemplateEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(TemplateMySQLDatabase.class);

    InnerTemplateMySQLDatabase database;

    /**
     * Constructor.
     * 
     * @param connection
     */
    public TemplateMySQLDatabase(Connection connection) {
        super(connection);
        database = new InnerTemplateMySQLDatabase(connection);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#getMonitoringResults(java.lang.String,
     *      long, java.lang.String)
     */
    public ArrayList<Template> getMonitoringResults(String withStatus, long afterTimepoint, String includingInTemplateId) {
        ArrayList<Template> templates = new ArrayList<Template>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        String tableName = "templatedefined";

        try {
            pstmt =
                    connection.prepareStatement("SELECT templateObject " + "FROM " + tableName + " "
                            + "WHERE status = ? AND templateId LIKE '%" + includingInTemplateId + "%'");
            pstmt.setString(1, withStatus);
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("templateObject");
                    Template template = (Template) toObject(blob);
                    // add to returned result only if the decision time
                    // is greater than the given time point
                    if (template.getDET() > afterTimepoint) {
                        templates.add(template);
                    }
                }
                while (resultSet.next());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return templates;
    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM EntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#close()
     */

    public void close() {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#count()
     */

    public int count() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.Object)
     */

    public void delete(Object object) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.String)
     */

    public void delete(String entityId) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#deleteAll()
     */

    public void deleteAll() {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#insert(java.lang.Object)
     */

    public void insert(Object object) {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#select(java.lang.String)
     */

    public Object select(String entityId) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#selectAll()
     */

    public ArrayList<Object> selectAll() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#update(java.lang.Object)
     */

    public void update(Object object) {
        // TODO Auto-generated method stub

    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM TemplateEntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#copyFile(java.lang.String)
     */

    public void copyFile(String newName) {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#getFolder()
     */

    public String getFolder() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#hasMoreF(java.lang.String, int)
     */

    public boolean hasMoreF(String formulaId, int mode) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#hasMoreG(java.lang.String, int)
     */

    public boolean hasMoreG(String formulaId, int mode) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#loadTemplatesF(java.lang.String,
     *      int)
     */

    public ArrayList<Template> loadTemplatesF(String formulaId, int mode) {
        return database.selectByFormulaIdAndModeAndStatus(formulaId, mode, Constants.STATUS_UD);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#loadTemplatesG(java.lang.String,
     *      int)
     */

    public ArrayList loadTemplatesG(String formulaId, int mode) {
        return database.selectByFormulaIdAndModeAndStatus(formulaId, mode, Constants.STATUS_UD);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#resetF()
     */

    public void resetF() {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#resetG()
     */

    public void resetG() {
        // TODO Auto-generated method stub

    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#storeTemplates(java.util.ArrayList,
     *      int)
     */

    public void storeTemplates(ArrayList<Template> templates, int mode) {
        database.insert(templates, mode);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#updatedTemplatesF(java.util.ArrayList,
     *      java.lang.String, int)
     */

    public void updatedTemplatesF(ArrayList<Template> templates, String formulaId, int mode) {
        for (Template template : templates) {
            // logger.debug(getClass().getName() + ".updatedTemplatesF: " + template);

            int tokenIndex = template.getTemplateId().indexOf("$NF_ID");

            if (tokenIndex != -1) {
                String templateId = template.getTemplateId().substring(0, tokenIndex);
                database.deleteByTemplateId(templateId, mode);
            }

            database.update(template, mode);
        }
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#updatedTemplatesG(java.util.ArrayList,
     *      java.lang.String, int)
     */

    public void updatedTemplatesG(ArrayList<Template> templates, String formulaId, int mode) {

    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#getTempIds(int)
     */

    public ArrayList<String> getTempIds(int mode) {
        return database.selectFormulaId(mode);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByMode(int)
     */

    public ArrayList<Template> selectByMode(int mode) {
        return database.selectByMode(mode);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByStatus(java.lang.String)
     */

    public ArrayList<Template> selectByStatus(String status) {
        return database.selectByStatus(status);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#getCount(java.lang.String, int)
     */

    public int getCount(String formulaId, int mode) {
        return database.countByFormulaIdAndMode(formulaId, mode);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#remove(uk.ac.city.soi.everest.monitor.Template)
     */

    public void delete(Template template, int mode) {
        database.delete(template, mode);
    }

    /**
     * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByTemplateIdAndMode(java.lang.String,
     *      int)
     */

    public Template selectByTemplateIdAndMode(String templateId, int mode) {
        return null;
    }

    // ------------------------------------------------------------------------
    // INNER CLASS
    // ------------------------------------------------------------------------

    /**
     * INNER CLASS
     * 
     * @author Davide Lorenzoli
     * 
     * @date 27 Feb 2009
     */
    public class InnerTemplateMySQLDatabase extends EntityManagerUtil {

        /**
         * @param connection
         */
        public InnerTemplateMySQLDatabase(Connection connection) {
            super(connection);
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#insert(java.util.ArrayList, int)
         */
        public void insert(ArrayList<Template> templates, int mode) {
            for (Template template : templates) {
                insert(template, mode);
            }
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#insert(uk.ac.city.soi.everest.monitor.Template,
         *      int)
         */
        public void insert(Template template, int mode) {
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;

            String tableName = template.getStatus().equals(Constants.STATUS_UD) ? "template" : "templatedefined";

            try {
                // delete the existing graph
                if (selectByTemplateIdAndMode(template.getTemplateId(), mode) != null) {
                    delete(template, mode);
                }

                Blob blob = toBlob(template);

                String query =
                        "INSERT INTO "
                                + tableName
                                + " "
                                + "(templateId, formulaId, mode, status, type, active, removable, updated, templateObject, templateString) "
                                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                pstmt = connection.prepareStatement(query);

                pstmt.setString(1, template.getTemplateId());
                pstmt.setString(2, template.getFormulaId());
                pstmt.setInt(3, mode);
                pstmt.setString(4, template.getStatus());
                pstmt.setString(5, template.getType());
                pstmt.setBoolean(6, template.isActive());
                pstmt.setBoolean(7, template.isRemovable());
                pstmt.setBoolean(8, template.isUpdated());
                pstmt.setBlob(9, blob);
                pstmt.setString(10, template.toString());
                
                
                int v = pstmt.executeUpdate();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }                                
//            catch (SQLException ex) {
//                // handle any errors
////                logger.debug("SQLException: " + ex.getMessage());
////                logger.debug("SQLState: " + ex.getSQLState());
////                logger.debug("VendorError: " + ex.getErrorCode());
//                ex.printStackTrace();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByTemplateId(java.lang.String,
         *      int)
         */
        public Template selectByTemplateId(String templateId, int mode) {
            return null;
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByTemplateIdAndType(java.lang.String,
         *      int)
         */
        public Template selectByTemplateIdAndMode(String templateId, int mode) {
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;
            Template template = null;

            try {
                pstmt =
                        connection.prepareStatement("SELECT templateObject " + "FROM template "
                                + "WHERE templateId = ? AND mode = ?");
                pstmt.setString(1, templateId);
                pstmt.setInt(2, mode);
                resultSet = pstmt.executeQuery();

                // if any blob has been returned
                if (resultSet.first()) {
                    Blob blob = resultSet.getBlob("templateObject");
                    template = (Template) toObject(blob);
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }                                
//            catch (SQLException ex) {
//                // handle any errors
////                logger.debug("SQLException: " + ex.getMessage());
////                logger.debug("SQLState: " + ex.getSQLState());
////                logger.debug("VendorError: " + ex.getErrorCode());
//                ex.printStackTrace();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//            catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }

            return template;
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByEventAndType(uk.ac.city.soi.everest.er.LogEvent,
         *      int)
         */
        public ArrayList<Template> selectByEventAndMode(LogEvent event, int mode) {
            return new ArrayList<Template>();
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByType(int)
         */
        public ArrayList<Template> selectByMode(int mode) {
            return new ArrayList<Template>();
        }

        /**
         * Returns a collection of templates selected by status
         */
        public ArrayList<Template> selectByStatus(String status) {
            ArrayList<Template> templates = new ArrayList<Template>();
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;

            String tableName = "";

            if (status.equals(Constants.STATUS_UD)) {
                tableName = "template";
            }
            else {
                tableName = "templatedefined";
            }

            try {
                pstmt =
                        connection.prepareStatement("SELECT templateObject " + "FROM " + tableName + " "
                                + "WHERE status = ?");
                pstmt.setString(1, status);
                resultSet = pstmt.executeQuery();

                // if any blob has been returned
                if (resultSet.first()) {
                    do {
                        // get the blob
                        Blob blob = resultSet.getBlob("templateObject");
                        Template template = (Template) toObject(blob);
                        templates.add(template);
                    }
                    while (resultSet.next());
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }                                
//            catch (SQLException ex) {
//                // handle any errors
////                logger.debug("SQLException: " + ex.getMessage());
////                logger.debug("SQLState: " + ex.getSQLState());
////                logger.debug("VendorError: " + ex.getErrorCode());
//                ex.printStackTrace();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//            catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }

            return templates;
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#selectByFormulaIdAndType(java.lang.String,
         *      int)
         */
        public ArrayList<Template> selectByFormulaIdAndModeAndStatus(String formulaId, int mode, String status) {
            ArrayList<Template> templates = new ArrayList<Template>();
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;
            
            try {
                pstmt =
                        connection.prepareStatement("SELECT templateObject " + "FROM template "
                                + "WHERE formulaId = ? AND mode = ? AND status = ?"
                                + " ORDER BY templatePK DESC");
                pstmt.setString(1, formulaId);
                pstmt.setInt(2, mode);
                pstmt.setString(3, status);
                resultSet = pstmt.executeQuery();

                // if any blob has been returned
                if (resultSet.first()) {
                    do {
                        // get the blob
                        Blob blob = resultSet.getBlob("templateObject");
                        Template template = (Template) toObject(blob);
                        templates.add(template);
                    }
                    while (resultSet.next());
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }                                
//            catch (SQLException ex) {
//                // handle any errors
////                logger.debug("SQLException: " + ex.getMessage());
////                logger.debug("SQLState: " + ex.getSQLState());
////                logger.debug("VendorError: " + ex.getErrorCode());
//                ex.printStackTrace();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//            catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }

            return templates;
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#update(uk.ac.city.soi.everest.monitor.Template,
         *      int)
         */
        public void update(Template template, int mode) {

            if (template.isRemovable() && template.getStatus().equals(Constants.STATUS_UD)) {
            	//System.out.println("\n^^^^^^^^^^^^^^^Deleting template inside db " + template.getFormulaId() + "\n");
                database.delete(template, mode);
            }
            else {
                /**
                 * ATTENTION: The following reasoning for deleting, updating and inserting templates DEPENDS STRONGLY on
                 * the way that the template id is computed by calling the updateTemplateId method of the same class
                 */

                PreparedStatement pstmt = null;
                ResultSet resultSet = null;

                String tableName = template.getStatus().equals(Constants.STATUS_UD) ? "template" : "templatedefined";

                // computed the updated template id
                String updatedTemplateId = updateTemplateId(template, mode);
                
                
                // retrieve the stored template that has the same id as the current one
                Template retrievedTemplatesWithSameId = selectByTemplateIdAndMode(template.getTemplateId(), mode);

                try {
                    // if it exists then update
                    if (retrievedTemplatesWithSameId != null) {

                        // if the updated template id equals to the current id
                        if (updatedTemplateId.equals(template.getTemplateId())) {

                            pstmt =
                                    connection
                                            .prepareStatement("UPDATE "
                                                    + tableName
                                                    + " "
                                                    + "SET mode = ?, status = ?, type = ?, active = ?, removable = ?, updated = ?, templateObject = ?, templateString = ? "
                                                    + "WHERE templateId = ? AND mode = ?");

                            Blob blob = toBlob(template);

                            // SET
                            pstmt.setInt(1, mode);
                            pstmt.setString(2, template.getStatus());
                            pstmt.setString(3, template.getType());
                            pstmt.setBoolean(4, template.isActive());
                            pstmt.setBoolean(5, template.isRemovable());
                            pstmt.setBoolean(6, template.isUpdated());
                            pstmt.setBlob(7, blob);
                            pstmt.setString(8, template.toString());
                            // WHERE
                            pstmt.setString(9, template.getTemplateId());
                            pstmt.setInt(10, mode);
                            
                            
                            
                            int v = pstmt.executeUpdate();
                        }
                        // if the updated id is not equal to the current one
                        else {
                            // delete the out of date row
                            // by being careful with the totally empty templates, i.e., the templates whose unifiers are
                            // empty
                            // totally empty templates may be empty ONLY in the case that the current examined template
                            // is totally
                            // empty as well
                            if (retrievedTemplatesWithSameId.getUnifiers().size() > 0
                                    || (retrievedTemplatesWithSameId.getUnifiers().size() == 0 && template
                                            .getUnifiers().size() == 0)) {
                                delete(template, mode);
                            }

                            // update the template Id and insert the updated template into the table;
                            template.setTemplateId(updatedTemplateId);
                            insert(template, mode);
                        }
                    }
                    // if it does not exist add
                    else {
                        template.setTemplateId(updatedTemplateId);
                        insert(template, mode);
                    }
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }                                    
//                catch (SQLException ex) {
//                    // handle any errors
////                    logger.debug("SQLException: " + ex.getMessage());
////                    logger.debug("SQLState: " + ex.getSQLState());
////                    logger.debug("VendorError: " + ex.getErrorCode());
//                    ex.printStackTrace();
//                }
//                catch (IOException e) {
//                    e.printStackTrace();
//                }
                finally {
                    // it is a good idea to release
                    // resources in a finally{} block
                    // in reverse-order of their creation
                    // if they are no-longer needed

                    releaseResources(pstmt, resultSet);
                }
            }
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#deleteByTemplateId(java.lang.String,
         *      int)
         */
        public void deleteByTemplateId(String templateId, int mode) {
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;

            try {
                pstmt = connection.prepareStatement("DELETE FROM template " + "WHERE templateId = ? AND mode = ?");
                pstmt.setString(1, templateId);
                pstmt.setInt(2, mode);      
                pstmt.executeUpdate();
            }
            catch (SQLException ex) {
                // handle any errors
//                logger.debug("SQLException: " + ex.getMessage());
//                logger.debug("SQLState: " + ex.getSQLState());
//                logger.debug("VendorError: " + ex.getErrorCode());
                ex.printStackTrace();
            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#deleteByFormulaId(java.lang.String,
         *      int)
         */
        public void deleteByFormulaId(String formulaId, int mode) {

        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#delete(uk.ac.city.soi.everest.monitor.Template,
         *      int)
         */
        public void delete(Template template, int mode) {
            deleteByTemplateId(template.getTemplateId(), mode);
        }

        /**
         * @see uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface#countByFormulaIdAndType(java.lang.String,
         *      int)
         */
        public int countByFormulaIdAndMode(String formulaId, int mode) {
            PreparedStatement pstmt = null;
            ResultSet resultSet = null;
            int templates = 0;

            try {
                String query = "SELECT COUNT(templatePK) FROM template " + "WHERE formulaId = ? AND mode = ?";

                pstmt = connection.prepareStatement(query);

                pstmt.setString(1, formulaId);
                pstmt.setInt(2, mode);
                resultSet = pstmt.executeQuery();

                // if any blob has been returned
                if (resultSet.first()) {
                    templates = resultSet.getInt(1);
                }
            }
            catch (SQLException ex) {
                // handle any errors
//                logger.debug("SQLException: " + ex.getMessage());
//                logger.debug("SQLState: " + ex.getSQLState());
//                logger.debug("VendorError: " + ex.getErrorCode());
                ex.printStackTrace();
            }
            finally {
                // it is a good idea to release
                // resources in a finally{} block
                // in reverse-order of their creation
                // if they are no-longer needed

                releaseResources(pstmt, resultSet);
            }

            return templates;
        }

        // ------------------------------------------------------------------------
        // PRIVATE METHODS
        // ------------------------------------------------------------------------

        /**
         * @param mode
         * @return
         */
        private ArrayList<String> selectFormulaId(int mode) {
            return new ArrayList<String>();
        }

        /**
         * @param template
         */
        private String updateTemplateId(Template template, int mode) {
            // *****************************************************
            // Before adding the current template to the formula
            // list a new templateId is create to replace the
            // existing one. This procedure must be fixed in the
            // next release of the monitor.
            // *****************************************************

            /*
             * the templateId is equal to: i) formulaId plus $ plus ii) mode plus $ plus iii) the ids of the unified and
             * evaluated predicates of the template
             */

            String updatedTemplateId = template.getFormulaId();
            logger.debug(getClass().getName() + ".updateTemplateId: Old tempateId: " + template.getTemplateId());

            updatedTemplateId += "$m: " + mode;

            for (int i = 0; i < template.totalPredicates(); i++) {
                Predicate predicate = template.getPredicate(i);
                updatedTemplateId += "$" + i + "p: " + predicate.getID();

            }

            return updatedTemplateId;
        }

    }
}
