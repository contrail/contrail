/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Function.java
 *
 * Created on 27 July 2005, 16:45
 */
package uk.ac.city.soi.everest.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

/**
 * This class models a Function (operationCall) used in XML formula. A function can be an internal function or an
 * external function. Function has following fields: partner - indicates if the function is internal or external. In
 * case of internal function the value of partner is "self" operationName - name of the function, e.g. add, sub etc
 * parameters - list of parameters in function functionParamIndex - this param index is used only to have multiple
 * instances of the same function, (e.g. two sub function) as the parameters to this function. This field does not have
 * any significance other than to ease the storing of parameters in the HashMap, where this index along with the
 * function name is used as the key for the map.
 * 
 * @author am697
 */
public class Function implements Serializable {
    // logger
    private static Logger logger = Logger.getLogger(Function.class);

    private String partner;
    private String operationName;
    private LinkedHashMap parameters;
    private int functionParamIndex = 0;

    /**
     * Creates a new instance of Function.
     * 
     */
    public Function() {
        partner = Constants.PARTNER_SELF;
        operationName = "";
        parameters = new LinkedHashMap();
        // paramTracker = new ArrayList();
    }

    /**
     * Copy constructor.
     * 
     * @param func
     */
    public Function(Function func) {
        this.partner = new String(func.getPartner());
        this.operationName = new String(func.getOperationName());
        this.parameters = new LinkedHashMap();
        // this.parameters.putAll(func.getParameters());
        Iterator it = func.getParameters().keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object obj = (Object) func.getParameters().get(key);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                parameters.put(new String(key), var.makeCopy());
            }
            else if (obj instanceof Function) {
                Function func1 = (Function) obj;
                parameters.put(new String(key), new Function(func1));
            }
        }
        // this.paramTracker.addAll(func.getParamTracker());
    }

    /**
     * Sets the name of the function provider.
     * 
     * @param partner
     */
    public void setPartner(String partner) {
        this.partner = new String(partner);
    }

    /**
     * Sets the name of the function.
     * 
     * @param operationName
     */
    public void setOperationName(String operationName) {
        this.operationName = new String(operationName);
    }

    /**
     * Adds a variable as a parameter of the function.
     * 
     * @param var -
     *            parameter
     */
    public void addParameter(Variable var) {
        parameters.put(var.getName() + var.getType(), var);
    }

    /**
     * Adds a function as a parameter of this function (i.e. nested function).
     * 
     * @param func
     */
    public void addParameter(Function func) {
        parameters.put(func.getOperationName() + functionParamIndex, func);
        functionParamIndex++;
    }

    /**
     * Returns the name of the function provider.
     * 
     * @return
     */
    public String getPartner() {
        return partner;
    }

    /**
     * Returns the name of the function.
     * 
     * @return
     */
    public String getOperationName() {
        return operationName;
    }

    /**
     * Returns the list of parameters in function.
     * 
     * @return
     */
    public LinkedHashMap getParameters() {
        return parameters;
    }

    /**
     * This method executes the function and returns the result as a variable. This method uses InternalFunctions.java
     * class to execute internal functions. Execution of external functions was not implemented.
     * 
     * @param values -
     *            list of variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     */
    public Variable execute(HashMap values, HashMap timeVarMap) {
        Variable fVar = new Variable("", "", "");
        if (parameters.size() > 0) {
            Variable firstParam = getParameter(parameters, 1, values, timeVarMap);
            if(parameters.size() > 2){
            	//System.out.println("111111 Invoking series function");
            	Variable secondParam = getParameter(parameters, 2, values, timeVarMap);
            	Variable thirdParam = getParameter(parameters, 3, values, timeVarMap);
                if (partner.equals(Constants.PARTNER_SELF)) {
                    InternalFunctions irf = new InternalFunctions();
                    fVar = irf.call(operationName, firstParam, secondParam, thirdParam);
                }            	
            }
            else if (parameters.size() > 1) {
                Variable secondParam = getParameter(parameters, 2, values, timeVarMap);
                if (partner.equals(Constants.PARTNER_SELF)) {
                    InternalFunctions irf = new InternalFunctions();
                    fVar = irf.call(operationName, firstParam, secondParam);
                }
            }
            else if (parameters.size() == 1) {
                if (partner.equals(Constants.PARTNER_SELF)) {
                    InternalFunctions irf = new InternalFunctions();
                    fVar = irf.call(operationName, firstParam);
                }
            }
        }
        else if (parameters.size() == 0) {
            if (partner.equals(Constants.PARTNER_SELF)) {
                InternalFunctions irf = new InternalFunctions();
                fVar = irf.call(operationName);
            }
        }
        return fVar;
    }

    /**
     * Returns the value of a specific parameter of the function as variable. If the specific parameter is variable and
     * the variable has been initialized to some value in the template, then an instance of the variable is returned. If
     * the specific parameter is a function then the function is executed and the result is returned as a variable.
     * 
     * @param params -
     *            list of parameters from which a specific parameter will be picked
     * @param index -
     *            position of the parameter in the function
     * @param values -
     *            list of variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     * @return
     */
    private Variable getParameter(LinkedHashMap params, int index, HashMap values, HashMap timeVarMap) {
        Variable retVar = new Variable("", "", "");
        Object obj = (Object) (params.values().toArray())[index - 1];
        if (obj instanceof Variable) {
            retVar = (Variable) obj;
            if (!retVar.getType().equals(Constants.DATA_TYPE_CONST)) {
                if (values.containsKey(retVar.getName() + retVar.getType())) {
                    Variable valVar = (Variable) values.get(retVar.getName() + retVar.getType());
                    retVar = valVar;
                    logger.debug("Function: reference key:" + retVar.getName() + retVar.getType() + "-" + valVar);
                    // retVar.setValue(valVar.getValue());
                    // retVar.addValues(valVar.getValues());
                }
                else if (timeVarMap.containsKey(retVar.getName())) {
                    TimeVar varVal = (TimeVar) timeVarMap.get(retVar.getName());
                    retVar.setValue(String.valueOf(varVal.getValue()));
                }
            }
        }
        else if (obj instanceof Function) {
            Function func = (Function) obj;
            retVar = func.execute(values, timeVarMap);
        }
        return retVar;
    }

    /**
     * Returns true if this function is equal to a given function. Function1 is equal to Function2 if they have same
     * name, and there parameters are equal (including parameter value).
     * 
     * @param func
     * @return
     */
    public boolean equals(Function func) {
        return this.getHashValue().equals(func.getHashValue());
    }

    /**
     * Returns list of all variables (parameters) in the function. If the function has some other function as parameter,
     * then the variables of that function is also included in the list.
     * 
     * @return
     */
    public ArrayList getAllVariables() {
        ArrayList vars = new ArrayList();
        Iterator it = parameters.values().iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Variable)
                vars.add((Variable) obj);
            else if (obj instanceof Function)
                vars.addAll(((Function) obj).getAllVariables());
        }
        return vars;
    }

    /**
     * Returns true if the function is partially instantiated. A function is partially instantiated if not all the
     * variables in the function are initialized to some value.
     * 
     * @param ufp -
     *            list of variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     * @param timeVarMap -
     *            list of time variables in the template that have been initialized to some value. Usually function is
     *            applied on the variables/time variables of different predicates in a formula.
     * @return
     */
    public boolean isPartial(HashMap ufp, HashMap timeVarMap) {
        boolean val = false;
        // Iterator it = parameters.values().iterator();
        ArrayList vars = getAllVariables();
        // while(it.hasNext() && !val){
        for (int i = 0; i < vars.size(); i++) {
            // Variable var = (Variable)it.next();
            Variable var = (Variable) vars.get(i);
            if (var.getType().equals(Constants.DATA_TYPE_CONST)) {
                continue;
            }
            else {
                val |= !(ufp.containsKey(var.getName() + var.getType()) || timeVarMap.containsKey(var.getName()));
            }
        }
        return val;
    }

    /**
     * Returns a string representation of the function.
     */
    public String toString() {
        String val = "oc:" + partner + ":" + operationName + "(";
        Iterator it = parameters.values().iterator();
        while (it.hasNext()) {
            // val += ((Variable)it.next()).toString(true);

            Object obj = it.next();
            if (obj instanceof Variable)
                val += ((Variable) obj).toString(true);
            else if (obj instanceof Function)
                val += ((Function) obj).toString();

            if (it.hasNext())
                val += ",";
        }
        val += ")";
        return val;
    }

    /**
     * Returns a hash uk.ac.city.soi.everest of the function.
     * 
     * @return
     */
    public String getHashCode() {
        String val = partner + operationName;
        Iterator it = parameters.values().iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Variable)
                val += ((Variable) obj).getHashCode();
            else if (obj instanceof Function)
                val += ((Function) obj).getHashCode();
        }
        return val;
    }

    /**
     * Returns a hash value of the function.
     * 
     * @return
     */
    public String getHashValue() {
        String val = partner + operationName;
        Iterator it = parameters.values().iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Variable)
                val += ((Variable) obj).getHashValue();
            else if (obj instanceof Function)
                val += ((Function) obj).getHashValue();
        }
        return val;
    }
}
