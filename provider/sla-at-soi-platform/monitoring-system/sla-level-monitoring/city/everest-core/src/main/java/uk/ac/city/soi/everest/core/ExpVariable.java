/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.core;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * This class extends the Variable class to allow access to the field of objects if the variable has an object value.
 * 
 * @author Costas Ballas
 */
public class ExpVariable extends Variable implements Serializable {
    // logger
    private static Logger logger = Logger.getLogger(ExpVariable.class);

    private String field;
    private String path;
    private ArrayList xPath;

    /**
     * Constructor with field values.
     * 
     * @param constName
     * @param name
     * @param type
     * @param value
     * @param field
     * @param path
     */
    
    public ExpVariable(String constName, String name, String type, Object value, String field, String path) {
        super(constName, name, type, value);
        this.field = new String(field);
        this.path = new String(path);

    }
	
    /**
     * Constructor by giving variable object as parameter.
     * 
     * @param var
     */
    /*
    public ExpVariable(Variable var) {
        super(var);
    }
	*/
    /**
     * Copy constructor.
     * 
     * @param ev
     */
    /*
    public ExpVariable(ExpVariable ev) {
        super(ev);
        this.field = new String(ev.field);
        this.path = new String(ev.path);
        this.xPath = new ArrayList(ev.xPath);
    }
	*/
    /**
     * @see uk.ac.city.soi.everest.core.Variable#getRealType()
     */
    /*
    public String getRealType() {

        try {
            // object argument
            // The dir contains the compiled classes.
            File classesDir = new File(".");
            // The parent classloader
            ClassLoader parentLoader = ExpVariable.class.getClassLoader();
            // Load class "sample.PostmanImpl" with our own classloader.
            URLClassLoader loader1 = new URLClassLoader(new URL[] { classesDir.toURL() }, parentLoader);
            String pkg = "";
            if (this.getType().equals("OpStatus") || this.getType().equals("Entity")) {
                pkg = "uk.ac.city.soi.everest.SRNTEvent.";
            }
            else {
                pkg = "uk.ac.city.soi.everest.datatypes.";
            }
            Class c = loader1.loadClass(pkg + this.getType());
            // Postman postman1 = (Postman) cls1.newInstance();
            // Class c = Class.forName("uk.ac.city.soi.everest.datatypes."+arg.getType());
            // Object o =c.newInstance();
            for (int i = 0; i < this.xPath.size(); i++) {
                Field f = c.getDeclaredField((String) this.xPath.get(i));
                c = f.getType();
            }

            String s = c.getName();
            if (s.equals("java.lang.String")) {
                s = Constants.DATA_TYPE_STRING;
            }
            else if (s.equals("java.lang.Double")) {
                s = Constants.DATA_TYPE_DOUBLE;
            }
            else if (s.equals("java.lang.Integer")) {
                s = Constants.DATA_TYPE_INT;
            }
            else if (s.equals("java.lang.Boolean")) {
                s = Constants.DATA_TYPE_BOOL;
            }
            else if (s.equals("java.lang.Long")) {
                s = Constants.DATA_TYPE_LONG;
            }
            return s;
        }
        catch (Exception e) {
            e.printStackTrace();
            // this field does not exist in the object
            return null;
        }

    }
	*/
    /**
     * @see uk.ac.city.soi.everest.core.Variable#getRealValue()
     */
    /*
    public Object getRealValue() {

        try {
            // object argument
            // The dir contains the compiled classes.
            File classesDir = new File(".");
            // The parent classloader
            ClassLoader parentLoader = ExpVariable.class.getClassLoader();
            // Load class "sample.PostmanImpl" with our own classloader.
            URLClassLoader loader1 = new URLClassLoader(new URL[] { classesDir.toURL() }, parentLoader);
            String pkg = "";
            if (this.getType().equals("OpStatus") || this.getType().equals("Entity")) {
                pkg = "uk.ac.city.soi.everest.SRNTEvent.";
            }
            else {
                pkg = "uk.ac.city.soi.everest.datatypes.";
            }
            Class c = loader1.loadClass(pkg + this.getType());
            Object o = this.getObject();
            for (int i = 0; i < this.xPath.size(); i++) {
                Field f = c.getDeclaredField((String) this.xPath.get(i));
                Method m =
                        c.getMethod("get" + ((String) this.xPath.get(i)).substring(0, 1).toUpperCase()
                                + ((String) this.xPath.get(i)).substring(1), null);

                c = f.getType();
                o = m.invoke(o, null);

            }

            return o;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	*/
    /**
     * Sets the field "field" of the ExpVariable object.
     * 
     * @param f
     */
    
    public void setField(String f) {
        this.field = new String(f);
    }
	
    /**
     * Sets the field "path" of the ExpVariable object.
     * 
     * @param p
     */
    
    public void setPath(String p) {
        this.path = new String(p);
    }
	
    /**
     * Sets the field "xPath" of the ExpVariable object.
     * 
     * @param al
     */
    
    public void setXPath(ArrayList al) {
        this.xPath = new ArrayList(al);
    }
	
    /**
     * Returns the size of the field "xPath" of the ExpVariable object.
     * 
     * @return
     */
    /*
    public int getXPathSize() {
        return this.xPath.size();
    }
	*/
    /**
     * Returns the string value of the i-th element of the field "xPath" of the ExpVariable object.
     * 
     * @param i
     * @return
     */
    /*
    public String getXPathAt(int i) {
        return this.xPath.get(i).toString();
    }
	*/
    /**
     * @see uk.ac.city.soi.everest.core.Variable#matches(uk.ac.city.soi.everest.core.Variable)
     */
    /*
    public boolean matches(Variable var) {
        boolean val = false;
        if (var != null && this.getConstName().equals(var.getConstName())
        // && type.equals(var.getType())
                && checkSuperClass(this.getObject().getClass(), var.getObject().getClass()))
            val = true;
        logger.debug("MATCHES_EXP: this constname " + this.getConstName() + " var constname " + var.getConstName()
                + " val: " + val);
        return val;
    }
	*/
    /**
     * @see uk.ac.city.soi.everest.core.Variable#makeCopy()
     */
    /*
    public ExpVariable makeCopy() {
        ExpVariable var1 = null;
        if (isStatic())
            var1 = this;
        else
            var1 = new ExpVariable(this);
        return var1;
    }
	*/
}
