@ECHO OFF

SET USER_NAME=serenity
SET DATABASE_NAME=serenity

ECHO ---------------------------------------------------------
ECHO MySQL script
ECHO ---------------------------------------------------------
ECHO Command:  CREATE TABLE
ECHO Database: %DATABASE_NAME%
ECHO User:     %USER_NAME%
ECHO Impact:   The whole set of tables is going to be created
ECHO           in the selected database
ECHO ---------------------------------------------------------

pause 

@ECHO ON
mysql -u serenity -p %DATABASE_NAME% < .\createTables.sql