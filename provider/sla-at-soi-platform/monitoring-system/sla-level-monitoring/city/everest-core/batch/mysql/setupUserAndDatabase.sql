-- DROP USER 'serenity'@'localhost';
CREATE USER 'serenity'@'localhost' IDENTIFIED BY 'serenity';

-- DROP DATABASE serenity;
CREATE DATABASE serenity;

USE serenity;

SOURCE ./createTables.sql

GRANT ALL ON serenity.* TO 'serenity'@'localhost';