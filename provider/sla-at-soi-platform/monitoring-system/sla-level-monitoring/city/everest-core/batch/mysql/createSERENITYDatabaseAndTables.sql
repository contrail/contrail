SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `slasoi-everest-core` DEFAULT CHARACTER SET latin1 ;
USE `slasoi-everest-core` ;

-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`event` (
  `eventPK` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `eventId` TEXT NOT NULL ,
  `timestamp` BIGINT(10) UNSIGNED NOT NULL ,
  `ecName` TEXT NOT NULL ,
  `prefix` TEXT NOT NULL ,
  `operationName` TEXT NOT NULL ,
  `partnerId` TEXT NOT NULL ,
  `negated` TINYINT(1) NOT NULL ,
  `abducible` TINYINT(1) NOT NULL ,
  `recordable` TINYINT(1) NOT NULL ,
  `eventObject` BLOB NOT NULL ,
  `eventString` TEXT NOT NULL ,
  PRIMARY KEY (`eventPK`) )
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`eventbuffer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`eventbuffer` (
  `eventPK` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `eventId` TEXT NOT NULL ,
  `timestamp` BIGINT(10) UNSIGNED NOT NULL ,
  `ecName` TEXT NOT NULL ,
  `prefix` TEXT NOT NULL ,
  `operationName` TEXT NOT NULL ,
  `partnerId` TEXT NOT NULL ,
  `negated` TINYINT(1) NOT NULL ,
  `abducible` TINYINT(1) NOT NULL ,
  `recordable` TINYINT(1) NOT NULL ,
  `eventObject` BLOB NOT NULL ,
  `eventString` TEXT NOT NULL ,
  PRIMARY KEY (`eventPK`) )
ENGINE = MyISAM
AUTO_INCREMENT = 1666
DEFAULT CHARACTER SET = latin1
ROW_FORMAT = DYNAMIC;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`fluent`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`fluent` (
  `fluentPK` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fluentId` TEXT NOT NULL ,
  `name` TEXT NOT NULL ,
  `timestamp` BIGINT(10) UNSIGNED NOT NULL ,
  `status` TEXT NOT NULL ,
  `predicateObject` BLOB NULL DEFAULT NULL ,
  PRIMARY KEY (`fluentPK`) )
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`predicate`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`predicate` (
  `predicatePK` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `predicateId` TEXT NULL DEFAULT NULL ,
  `templateId` TEXT NULL DEFAULT NULL ,
  `partnerId` TEXT NULL DEFAULT NULL ,
  `genuinenessBelief` DOUBLE UNSIGNED NOT NULL ,
  `genuinenessPlausibility` DOUBLE UNSIGNED NOT NULL ,
  `confirmed` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`predicatePK`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`statistic`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`statistic` (
  `statisticPK` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `componentName` TEXT NOT NULL ,
  `startTime` BIGINT(20) UNSIGNED NOT NULL ,
  `endTime` BIGINT(20) UNSIGNED NOT NULL ,
  `inputId` TEXT NOT NULL ,
  `executionId` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`statisticPK`) )
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`template`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`template` (
  `templatePK` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `templateId` TEXT NOT NULL ,
  `formulaId` TEXT NOT NULL ,
  `mode` INT(10) UNSIGNED NOT NULL COMMENT 'Type of the template, 0 is recorded, 1 is mixed' ,
  `status` TEXT NOT NULL ,
  `type` TEXT NOT NULL ,
  `active` TINYINT(1) NOT NULL DEFAULT '0' ,
  `removable` TINYINT(1) NOT NULL DEFAULT '0' ,
  `updated` TINYINT(1) NOT NULL DEFAULT '0' ,
  `templateObject` BLOB NOT NULL ,
  `templateString` TEXT NOT NULL ,
  PRIMARY KEY (`templatePK`) )
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everest-core`.`templatedefined`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slasoi-everest-core`.`templatedefined` (
  `templatePK` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `templateId` TEXT NOT NULL ,
  `formulaId` TEXT NOT NULL ,
  `mode` INT(10) UNSIGNED NOT NULL COMMENT 'Type of the template, 0 is recorded, 1 is mixed' ,
  `status` TEXT NOT NULL ,
  `type` TEXT NOT NULL ,
  `active` TINYINT(1) NULL DEFAULT '0' ,
  `removable` TINYINT(1) NOT NULL DEFAULT '0' ,
  `updated` TINYINT(1) NOT NULL DEFAULT '0' ,
  `templateObject` BLOB NOT NULL ,
  `templateString` TEXT NOT NULL ,
  PRIMARY KEY (`templatePK`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
