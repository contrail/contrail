@ECHO OFF

SET USER_NAME=serenity
SET DATABASE_NAME=serenity

ECHO -----------------------------------------------------------
ECHO MySQL script
ECHO -----------------------------------------------------------
ECHO Command:  TRUNCATE
ECHO Database: %DATABASE_NAME%
ECHO User:     %USER_NAME%
ECHO Impact:   All tables of the selected database will be empty
ECHO -----------------------------------------------------------

pause 

@ECHO ON
mysql -u serenity -p %DATABASE_NAME% < .\truncateAllTables.sql