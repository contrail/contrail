/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;

/**
 * This is useful to subscribe to the interaction event bus.
 *
 * @since 0.1
 */
public final class InteractionEventSubscriber {

    /** The pubsubmanager that listen on the interaction event channel bus. */
    private static PubSubManager pubSubManager = null;

    /** The channel name used to listen the interaction events. */
    private static String channelName = "InteractionEventChannel";

    /** Void private constructor. */
    private InteractionEventSubscriber() {
    }

    /** Instantiate a new pubsubmanager. */
    private static void instantiatePubSubManager() {
        try {
            try {
                String propfile =
                        System.getenv("SLASOI_HOME")
                                + System.getProperty("file.separator")
                                + "fbkrcg"
                                + System.getProperty("file.separator")
                                + "properties"
                                + System.getProperty("file.separator")
                                + "interactioneventbus.properties";

                pubSubManager = PubSubFactory.createPubSubManager(propfile);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Subscribe to the channel where there are the interaction event.
     *
     * @param channelNameChoosen the channel name of the interaction event bus
     */
    public static void subscribeToChannel(final String channelNameChoosen) {
        InteractionEventSubscriber.channelName = channelNameChoosen;
        try {
            if (pubSubManager == null) {
                instantiatePubSubManager();
                pubSubManager.createChannel(new Channel(channelNameChoosen));
                pubSubManager.subscribe(channelNameChoosen);

                /*
                 * Add the listener to handle the interaction message
                 * arriving on the channel
                 */
                FbkRCGMessageListener listener = new FbkRCGMessageListener();
                listener.start();
                pubSubManager.addMessageListener(listener);
            }

        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /** Unsubscribe from the channel. */
    public static void unsubscribe() {
        try {
            //pubSubManager.deleteChannel(channelName);
            pubSubManager.close();
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
