/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.service.MonitoringEventService;
import org.springframework.osgi.extensions.annotation.ServiceReference;

/**
 * Serialize the interaction event.
 *
 * @since 0.1
 */
public class InteractionEventSerializer {

    /** The service as a service. */
    private MonitoringEventService service;

    /** Empty constructor. */
    public InteractionEventSerializer() {
        /** empty block */
    }

    /**
     * Obtained automatically by Spring-DM and OSGi Container.
     * @param services the MonitoringEventService
     */
    @ServiceReference
    public final void setMonitoringEventServiceServices(
            final MonitoringEventService services) {
        this.service = services;
        System.out.println("*************** Registering Service *************");
        System.out
                .println("Service name: " + this.service.getClass().getName());
    }

    /**
     * Serialize the String in the EventInstance object.
     *
     * @param interactionEventString the interaction event payload
     * in string printable format
     * @return EventInstance the eventInstace object
     */
    public final EventInstance serialize(final String interactionEventString) {
        Reader sr = new StringReader(interactionEventString);
        return serialize(sr);
    }

    /**
     * Convert a Reader in a string printable format.
     *
     * @param reader the reader
     * @return String a printable string
     */
    private String convertInString(final Reader reader) {
        BufferedReader in = new BufferedReader(reader);
        String x = null;
        String result = null;
        try {
            x = in.readLine();
            while (x != null) {
                result += x;
                x = in.readLine();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return x;
    }

    /**
     * Serialize the interaction event inside the reader.
     *
     * @param reader the reader
     * @return EventInstance the event instance representing
     * the event inside the reader
     */
    public final EventInstance serialize(final Reader reader) {
        if (this.service != null) {
            // spring/osgi injection
            String stringToParse = convertInString(reader);
            try {
                return this.service.unmarshall(stringToParse);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        /* Normal java execution */

        try {

            JAXBContext jc =
                    JAXBContext.newInstance("org.slasoi.common.eventschema");

            Unmarshaller unmarshaller = jc.createUnmarshaller();

            JAXBElement<EventInstance> eventInstance =
                    (JAXBElement<EventInstance>) unmarshaller.unmarshal(reader);

            return eventInstance.getValue();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
