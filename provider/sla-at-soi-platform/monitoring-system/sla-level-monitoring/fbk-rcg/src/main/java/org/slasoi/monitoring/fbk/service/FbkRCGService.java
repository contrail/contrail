/**
 * SVN FILE: $Id$.
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.service;

import java.io.InputStream;
import java.text.ParseException;

import org.slasoi.monitoring.core.ReasoningComponentGateway;

/**
 * This is the interface for the FbkRCG osgi service; at the moment is the same of the ReasoningComponentGateway.
 * 
 * @since 0.1
 */
public interface FbkRCGService extends ReasoningComponentGateway {

    /**
     * Add slat to configuration of the reasoning component gateway.
     * @param is InputStream <containing the slat>
     */
    void addSLATToConfiguration(InputStream is);

    /**
     * Generate events for ORC scenario.
     */
    void generateEvents();

    /**
     * Refresh the monitoring system.
     * @param is InputStream <SLA>
     * @return boolean <returns true if the service is refreshed correctly>
     * @throws InterruptedException e
     */
    boolean replaceSLA(final InputStream is) throws InterruptedException;

    /**
     * API for getting monitored terms (KPIs/Variables/GSs) value for request SLA by name.
     * @param monitorTermName String
     * @param slaUuid String
     * @return Object Object(monitorTermValue) can be type casted to integer/boolean/string as per requirement
     * @throws ParseException e
     */
    Object reportMonitoredTermValue(final String slaUuid, final String monitorTermName) throws ParseException;

    /**
     * API to report all monitored terms (KPI/variables/GSs) values.
     * @param slaUuid String
     * @return Hashmap<String monitorTermName, Object monitorTermValue>
     */
    java.util.HashMap<String, Object> reportAllMonitoredTermsValues(final String slaUuid);

    /**
     * API for getting monitored terms (KPIs/Variables/GSs) value for previous SLA by name.
     * @param monitorTermName String
     * @param slaUuid String
     * @return Object Object(monitorTermValue) can be type casted to integer/boolean/string as per requirement
     * @throws ParseException e
     */
    Object reportLastMonitoredTermValue(final String slaUuid, final String monitorTermName) throws ParseException;

    /**
     * API to report all monitored terms (KPI/variables/GSs) values.
     * @param slaUuid String
     * @return Hashmap<String monitorTermName, Object monitorTermValue>
     */
    java.util.HashMap<String, Object> reportAllLastMonitoredTermsValues(final String slaUuid);

    /**
     * API to retrieve threshold value associated with monitor term.
     * @param slaUuid String
     * @param monitorTermName String
     * @return threshold String
     */
    String getMonitoringTermThreshold(final String slaUuid, final String monitorTermName);

    /**
    * generate report for all SLAs being monitored.
    * @param time timestamp
    */
    void generateReport(Long time);

    /**
    * generate report for single SLA.
    * @param slaUuid String
    * @param time timestamp
    */
    void generateReport(final String slaUuid, Long time);

    /**
     * utility method for cleaning deployment directory.
     */
    void cleanDeployDir();
}
