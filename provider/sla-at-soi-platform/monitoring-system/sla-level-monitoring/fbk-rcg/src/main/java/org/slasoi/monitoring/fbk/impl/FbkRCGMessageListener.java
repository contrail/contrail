/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;

/**
 * Listen the message on the bus.
 *
 * @since 0.1
 */
public class FbkRCGMessageListener extends Thread implements MessageListener {
    /** List of event container used as buffer. */
    private List<EventInstanceContainer> eventQueue =
            new LinkedList<EventInstanceContainer>();
    /** Is doing the shutdown. */
    private boolean shutdwon = false;
    /** Is receiving messages. */
    private boolean receiving = false;
    /** Receiving message counter. */
    private int count = 0;

    /**
     * Is receiving message.
     * @return boolean
     */
    public final boolean isReceiving() {
        boolean result = false;
        synchronized (this.eventQueue) {
            result = this.receiving;
        }
        return result;
    }

    /**
     * Set the receiving flag.
     *
     * @param receivingIn the listener is in receiving status or not
     */
    public final void setReceiving(final boolean receivingIn) {
        synchronized (this.eventQueue) {
            this.receiving = receivingIn;
        }
    }

    /**
     * Process the incoming message.
     *
     * @param messageEventIn the message event
     */
    public final void processMessage(final MessageEvent messageEventIn) {

        setReceiving(true);

        /* The contents of the message is in the payload */
        String payload = messageEventIn.getMessage().getPayload();
        if (payload.indexOf("MonitoringResultEvent") > 0) {
            System.err.println("WARNING!"
                    + "A monitor result event has been received;"
                    + " this should not happend!");
            return;
        }
        this.count++;
        /* payload contains the xml and it must serialize to surf the object */
        InteractionEventSerializer ies = new InteractionEventSerializer();
        EventInstance event = ies.serialize(payload);
        EventInstanceContainer iec = new EventInstanceContainer(event);
        synchronized (this.eventQueue) {
            System.err.println("received " + count + " events.\n");
            this.eventQueue.add(iec);

        }

        setReceiving(true);

    }

    /** Start to listen the message on the bus. */
    public final void run() {

        this.receiving = false;
        int size = 0;
        int sleepTime = 10000;
        while (!this.shutdwon) {

            try {
                while ((size == 0 || isReceiving()) && !this.shutdwon) {
                    size = this.eventQueue.size();

                    if (isReceiving()) {
                        setReceiving(false);
                    }

                    //System.err.println(new Date().toString()+": There is nothing to do,"
                    //        + " I'm taking a nap ...");
                    //Thread.sleep(sleepTime);
                }

                if (this.shutdwon) {
                    continue;
                }

                //System.err.println("Event queue size is "
                //        + this.eventQueue.size() + " event received " + count);

                synchronized (this.eventQueue) {
                    Collections.sort(this.eventQueue);
                    //System.err.println("START SENDING EVENT");
                    for (EventInstanceContainer eventContainer
                            : this.eventQueue) {
                        /*
                         * translate the event in the low level
                         * fbk monitor event.
                         */
                        InteractionEventToFbkEvent slaEvToFbk =
                                InteractionEventToFbkEvent.getInstance();
                        /* forward the event to the monitor runtime */
                        slaEvToFbk.forwardToFbkRuntimeMonitor(eventContainer
                                .getEventInstance());

                    }
                    //System.err.println("TERMINATE SENDING EVENT");
                    this.eventQueue.clear();
                    size = this.eventQueue.size();

                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    /**
     * Start up the notification manager.
     */
    public final void startup() {
        this.shutdwon = false;
        this.start();
    }

    /**
     * Shutdown the notification manager.
     */
    public final void shutdown() {
        this.shutdwon = true;
    }

    /** Inner class used to contain the eventInstance. */
    class EventInstanceContainer implements Comparable<EventInstanceContainer> {
        /** The event instance object. */
        private EventInstance eventInstance;

        /**
         * Get event instance.
         *
         * @return EventInstance the event instance
         */
        public EventInstance getEventInstance() {
            return this.eventInstance;
        }

        /**
         * Build the event instance container.
         *
         * @param event the event
         */
        public EventInstanceContainer(final EventInstance event) {
            this.eventInstance = event;
        }

        /**
         * Compare two event instance container;
         * The minor EventContainer has the minor timestamp.
         *
         * @param eventInstanceContainer the event instance container to compare
         * @return int return a negative number if this object has a timestamp
         * minor than eventInstanceContainer, 0 if equals,
         * greater than 0 if greater than
         */
        public int compareTo(
                final EventInstanceContainer eventInstanceContainer) {
            long value =
                    this.eventInstance.getEventContext().getTime()
                            .getTimestamp()
                            - eventInstanceContainer.getEventInstance()
                                    .getEventContext().getTime().getTimestamp();
            if (value == 0) {
                return 0;
            } else if (value > 0) {
                return 1;
            }
            return -1;
        }

        /**
         * Translate an event instance in a printable format.
         *
         * @return String
         */
        public String toString() {
            return this.eventInstance.getEventContext().getTime()
                    .getTimestamp()
                    + "_"
                    + eventInstance.getEventID()
                    + "_"
                    + this.eventInstance.getEventPayload()
                            .getInteractionEvent().getOperationName();
        }

    }

}
