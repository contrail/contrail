/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;

/**
 * Forward to the platform the monitoring result.
 *
 * @since 0.1
 */
public final class MonitoringResultEventForwarder {

    /** pub sub manager. */
    private static PubSubManager pubSubManager = null;

    /** Channel name for the result event. */
    private static String channelName = null;

    /** Private constructor. */
    private MonitoringResultEventForwarder() {
        /* empty block */
    }

    /**
     * Instantiate the channel for the result event.
     *
     * @param channel the channel name
     */
    public static void instantiatePubSubManager(final String channel) {
        channelName = channel;
        try {
            try {
                String propfile =
                        System.getenv("SLASOI_HOME")
                                + System.getProperty("file.separator")
                                + "fbkrcg"
                                + System.getProperty("file.separator")
                                + "properties"
                                + System.getProperty("file.separator")
                                + "monitorresulteventbus.properties";

                pubSubManager = PubSubFactory.createPubSubManager(propfile);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * publish passing the message payload.
     *
     * @param payload the payload that will contain the message
     */
    public static void publish(final String payload) {
        //System.err.println("Sending result event with violation info");
        // System.err.println(message);
        // commenting since isChannel is not supported for AMPQ
        //PubSubMessage message = new PubSubMessage(channelName, payload);
        PubSubMessage message = new PubSubMessage();
        try {
            pubSubManager.publish(message);
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Unsuscribe from the result channel.
     */
    public static void unsubscribe() {
        try {
            //pubSubManager.deleteChannel(channelName);
            pubSubManager.close();
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
