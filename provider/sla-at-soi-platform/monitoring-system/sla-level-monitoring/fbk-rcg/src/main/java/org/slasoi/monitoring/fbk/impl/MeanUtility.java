/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.VariableDeclr;

/**
 * Utility to interpret the mean formula.
 *
 * @since 0.1
 */
public class MeanUtility {

    /** The reference to the variable declaration. */
    private VariableDeclr varRef = null;

    /** The aspect. */
    private String aspect = null;

    /** The periodic. */
    private String periodic = null;

    /**
     * Constructor.
     *
     * @param varRefIn the variable reference
     */
    public MeanUtility(final VariableDeclr varRefIn) {
        this.varRef = varRefIn;
    }

    /**
     * Get aspect.
     *
     * @return String the aspect
     */
    public final String getAspect() {
        return aspect;
    }

    /**
     * Set aspect.
     *
     * @param aspectIn the aspect
     */
    public final void setAspect(final String aspectIn) {
        this.aspect = aspectIn;
    }

    /**
     * Get periodic.
     *
     * @return String
     */
    public final String getPeriodic() {
        return periodic;
    }

    /**
     * Set periodic.
     *
     * @param periodicIn the periodic
     */
    public final void setPeriodic(final String periodicIn) {
        this.periodic = periodicIn;
    }

    /**
     * Is the formula the mean.
     *
     * @return boolean the value of truth
     */
    public final boolean isMean() {
        FunctionalExpr fe = (FunctionalExpr) this.varRef.getExpr();
        if (!(fe.getOperator().toString().equals("mean"))) {
            return false;
        }
        if (fe.getParameters().length < 1) {
            return false;
        }
        ValueExpr par0l = fe.getParameters()[0];
        FunctionalExpr fe0l = (FunctionalExpr) par0l;

        if (!(fe0l.getOperator().toString().equals("series"))) {
            return false;
        } else {
            if (fe0l.getParameters().length < 1) {
                return false;
            }
        }
        return true;
    }

    /** Parse the formula. */
    public final void parse() {
        FunctionalExpr fe = (FunctionalExpr) this.varRef.getExpr();
        ValueExpr par0l = fe.getParameters()[0];
        FunctionalExpr fe0l = (FunctionalExpr) par0l;
        ValueExpr par1l = fe0l.getParameters()[0];

        EventExpr ev1r = (EventExpr) fe0l.getParameters()[1];

        Expr e2 = ev1r.getParameters()[0];
        CONST cons = (CONST) e2;
        String datatype = cons.getDatatype().toString();
        String consValue = cons.getValue();
        this.setAspect(par1l.toString());
        this.setPeriodic(consValue + " " + datatype);
    }

}
