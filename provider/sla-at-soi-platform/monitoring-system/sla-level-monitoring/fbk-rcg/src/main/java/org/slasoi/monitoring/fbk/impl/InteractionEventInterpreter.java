/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;

/**
 * This parse and interpret the semantic of the interaction event
 * and creates the emitter for the astro monitor.
 *
 * @since 0.1
 */
public class InteractionEventInterpreter {

    /** The log4j logger. */
    private static final Logger LOGGER =
            Logger.getLogger(InteractionEventInterpreter.class);

    /** The sla template. */
    private SLATemplate slat;

    /** The monitor class emitter. */
    private MonitorClassEmitter mce;

    /** The list of the terminal leaf symbols. */
    private List<String> leafReferenceList = new LinkedList<String>();

    /** Event reference list. */
    private List<String> eventReference = null;

    /** Event reference list. */
    private List<String> monitorReference = null;

    /** Event reference list. */
    private List<String> correlationParameters = null;

    /** current monitor name.**/
    private String monitorName = null;

    /** Private constructor. */
    private InteractionEventInterpreter() {
        /** empty block */
    }

    /**
     * Constructor using fields.
     *
     * @param slatIn the sla template
     * @param mceIn the monitor class emitter
     */
    public InteractionEventInterpreter(final SLATemplate slatIn,
            final MonitorClassEmitter mceIn) {
        super();
        this.slat = slatIn;
        this.mce = mceIn;
    }

    /**
     * Discriminate the operator.
     *
     * @param operator the operator
     * @return String the string to be used to build
     * the formula in the emitted class
     */
    public final String selectFormulaOperator(final String operator) {
        String retValue = "";
        if (operator.equals("+")) {
            retValue = "new PlusFormula";
        } else if (operator.equals("*")) {
            retValue = "new StarFormula";
        } else if (operator.equals("<")) {
            retValue = "new LesserThanFormula";
        } else if (operator.equals(">")) {
            retValue = "new GreaterThanFormula";
        } else if (operator.equals(">=")) {
            retValue = "new GreaterThanOrEqualsFormula";
        } else if (operator.equals("/")) {
            retValue = "new DivFormula";
        } else if (operator.equals("<=")) {
            retValue = "new LesserThanOrEqualsFormula";
        } else if (operator.equals("completion_time")) {
            retValue = "new TimeFormula";
        } else if (operator.equals("and")) {
            retValue = "new AndFormula";
        } else if (operator.equals("subtract")) {
            retValue = "new MinusFormula";
        } else if (operator.equals("=")) {
            retValue = "new EquFormula";
        } else if (operator.equals("!=")) {
            retValue = "new NequFormula";
        } else {
            LOGGER.error("OPERATOR " + operator + " not supported");
        }
        return retValue;
    }

    /**
     * Create monitor.
     *
     * @param op the monitor id
     */
    private void createMonitor(final ID op) {
        /* leaf variable declaration such as mtq */
        VariableDeclr varRef = slat.getVariableDeclr(op.toString());
        run(varRef);
    }

    /**
     * Analyze the interaction event.
     *
     * @param op the value expression
     * @return String the formula
     */
    private String analyze(final ValueExpr op) {

        System.err.println("analyze " + op.toString());

        String retValue = "";
        CountUtility cu = new CountUtility(op);
        boolean iscount = cu.isCount();
        if (iscount) {
            if (String.valueOf(op).contains("completion_time")) {
                //handling of completion_time
                //count(series(completion_time(Booking/selectService, Booking/bookTreatment),
                //periodic["6" month]), < "30" s)
                retValue = createCountCompletionTime(cu, op);

            } else if (String.valueOf(op).contains("subtract")) {
                retValue = createCountSubtract(cu, op);
            } else {
               retValue = createCountFormula(cu);
            }
        } else if (op instanceof FunctionalExpr) {
            FunctionalExpr fe = (FunctionalExpr) op;
            String operator = fe.getOperator().toString();
            String initformula = selectFormulaOperator(operator);
            retValue = initformula + "(";

            if (fe.getParameters().length > 0) {
                ValueExpr veOp1 = fe.getParameters()[0];
                retValue = retValue + analyze(veOp1);
                if (fe.getParameters().length > 1) {
                    retValue = retValue + ",";
                    ValueExpr veOp2 = fe.getParameters()[1];
                    retValue = retValue + analyze(veOp2);
                }
            }
            retValue = retValue + ")";
        } else {
            // ID or CONST
            if (op instanceof ID) {
                createMonitor((ID) op);
                // leaf this can contain a variableReference
                retValue =
                        retValue + "new MonitorReferenceFormula("
                                + "new MonitorReference(\"" + op + "_" + slat.getUuid()
                                + "\", null,\"\"))";
                /*
                 * store the leaf symbol;
                 * this used to add the monitor reference to the root monitor
                 */
                leafReferenceList.add(op.toString());
            } else if (op instanceof CONST) {
                CONST opToConst = (CONST) op;
                retValue =
                        retValue + "new ConstantFormula(new Float("
                                + opToConst.getValue() + "))";
            }
        }
        return retValue;
    }

    /**
     * method for creating count subtract monitors.
     * @param cu CountUtility
     * @param op ValueExpr
     * @return formula String
     */
    private String createCountSubtract(final CountUtility cu, final ValueExpr op) {
        FunctionalExpr feC1 = (FunctionalExpr) op;
        ValueExpr par1 = feC1.getParameters()[0];
        FunctionalExpr feC2 = (FunctionalExpr) par1;
        ValueExpr par11 = feC2.getParameters()[0];
        FunctionalExpr feC3 = (FunctionalExpr) par11;
        String correlator = "";
        // clear parent correlation list.
        correlationParameters.clear();
        List<String> correlationParametersLoc = new LinkedList<String>();

        cu.parse();
        if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("CitizenI/pickup/request_time")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Booking/bookMobility/pickupTime")) {
            ValueExpr[] params = new ValueExpr[2];
            params[0] = feC3.getParameters()[0];
            params[1] = new CONST("Booking/bookMobilityStop/pickupTime", null);
            feC3.setParameters(params);
            correlator = "reservationId";
        } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("CitizenI/arrival/request_time")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Booking/bookMobility/arrivalTime")) {
            //fix to append 'Stop' to bookMobility
            ValueExpr[] params = new ValueExpr[2];
            params[0] = feC3.getParameters()[0];
            params[1] = new CONST("Booking/bookMobilityStop/arrivalTime", null);
            feC3.setParameters(params);
            correlator = "reservationId";
        } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("CitizenI/arrival/request_time")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Mobility/bookTripOption/arrivalTime")) {
            correlator = "reservationId";
        } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("CitizenI/pickup/request_time")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Mobility/bookTripOption/pickupTime")) {
            correlator = "reservationId";
        }

        String subFormula = analyzeOperator(feC3, correlator, monitorName);
        correlationParametersLoc.add(correlator);
        String subMonitorName = "subtract_" + monitorName + "_" + slat.getUuid();
        // generate completion time monitor definition.
        mce.emitMonitorDefinition(subMonitorName, correlationParametersLoc, new LinkedList<String>(),
                monitorReference);
        // generate monitor classes.
        mce.emitMonitorClass(slat.getUuid().getValue(), subMonitorName, subFormula,
                correlationParametersLoc);
        String formula = "";
        // for parent.
        String monRef = "new MonitorReference(" + "\"" + subMonitorName + "\", null, \"\")";
        // clear monitorReference lit and add new entry.
        monitorReference.clear();
        monitorReference.add(monRef);
        //correlationParameters.add(correlator);
        // for simple query.
        if (!cu.getCompoundQuery()) {
            formula =
                    " new CountInTimesliceFormula( " + selectFormulaOperator(cu.getQueryOperator())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + cu.getQuery() + ")))" + " , new ConstantFormula(\"" + cu.getPeriodic() + "\"))";
        } else { // for compound query

            // query 1 parameters.
            CONST cons1 = (CONST) cu.getCompoundQueries()[0].getValue();
            String consValue1 = getValue(cons1);
            // query 2 parameters.
            CONST cons2 = (CONST) cu.getCompoundQueries()[1].getValue();
            String consValue2 = getValue(cons2);

            // query1 formula
            String formula1 =
                    selectFormulaOperator(cu.getCompoundQueries()[0].getComparisonOp().toString())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + consValue1 + ")))";
            // query2 formula
            String formula2 =
                    selectFormulaOperator(cu.getCompoundQueries()[1].getComparisonOp().toString())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + consValue2 + ")))";

            // compound query formula for e.g. (>= "30" s and < "45" s)
            formula =
                    " new CountInTimesliceFormula (" + selectFormulaOperator(cu.getQueryOperator()) + "( " + formula1
                            + ", " + formula2 + ") , new ConstantFormula(\"" + cu.getPeriodic() + "\"))";

        }
        return formula;

    }

    /**
     * getValue.
     * @param domExprToConst CONST
     * @return constantValue String.
     */
    private String getValue(final CONST domExprToConst) {
        String constantValue = "";
        // if String datatype put ""
        if (domExprToConst.getDatatype() != null
                && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                        "http://www.w3.org/2001/XMLSchema#string")) {
            constantValue = "\"" + domExprToConst.getValue() + "\"";
        } else if (domExprToConst.getDatatype() != null // if unit is seconds.
                && domExprToConst.getDatatype().getValue()
                        .equalsIgnoreCase("http://www.slaatsoi.org/coremodel/units#s")) {
            Float value = new Float(domExprToConst.getValue());
            value = value * 1000;
            constantValue = String.valueOf(value);
        } else if (domExprToConst.getDatatype() != null // if unit is hours.
                && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                        "http://www.slaatsoi.org/coremodel/units#hrs")) {
            Float value = new Float(domExprToConst.getValue());
            value = value * 60 * 60 * 1000;
            constantValue = String.valueOf(value);
        } else if (domExprToConst.getDatatype() != null // if unit is hours.
                && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                        "http://www.slaatsoi.org/coremodel/units#min")) {
            Float value = new Float(domExprToConst.getValue());
            value = value * 60 * 1000;
            constantValue = String.valueOf(value);
        }
        return constantValue;
    }

    /**
     * method for translating count/series/completion_time.
     * @param cu CountUtility.
     * @param op ValueExpr.
     * @return formula String
     */
    private String createCountCompletionTime(final CountUtility cu, final ValueExpr op) {
        FunctionalExpr feC1 = (FunctionalExpr) op;
        ValueExpr par1 = feC1.getParameters()[0];
        FunctionalExpr feC2 = (FunctionalExpr) par1;
        ValueExpr par11 = feC2.getParameters()[0];
        FunctionalExpr feC3 = (FunctionalExpr) par11;
        String correlator = "";
        List<String> correlationParametersLoc = new LinkedList<String>();
        // clear parent correlation list.
        correlationParameters.clear();

        cu.parse();
        // param0 completion_time(Booking/selectService, Booking/bookTreatment)
        // param1 periodic["6" month])
        // hardcoded fix to append an 'Start' with BookTreatment
        if (String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Booking/bookTreatment")) {
            ValueExpr[] params = new ValueExpr[2];
            params[0] = feC3.getParameters()[0];
            params[1] = new CONST(feC3.getParameters()[1] + "Start", null);
            feC3.setParameters(params);
            correlator = "callId";
        } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("Booking/phoneCall")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("CitizenI/operatorAnswer")) {
            correlator = "callId";
        } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("Treatment/treatmentCheckin")
                && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("CitizenI/treatment")) {
            correlator = "callId";
        }
        String completionTimeFormula = analyzeOperator(feC3, correlator, null);
        correlationParametersLoc.add(correlator);
        String completioTimeMonitorName = "completion_time_" + monitorName + "_" + slat.getUuid();
        // generate completion time monitor definition.
        mce.emitMonitorDefinition(completioTimeMonitorName, correlationParametersLoc, new LinkedList<String>(),
                monitorReference);
        // generate monitor classes.
        mce.emitMonitorClass(slat.getUuid().getValue(), completioTimeMonitorName, completionTimeFormula,
                correlationParametersLoc);

        String formula = "";
        // for parent.
        String monRef = "new MonitorReference(" + "\"" + completioTimeMonitorName + "\", null, \"\")";
        // clear monitorReference list and add new entry.
        monitorReference.clear();
        monitorReference.add(monRef);
        //correlationParameters.add(correlator);
        // for simple query.
        if (!cu.getCompoundQuery()) {
            // if operator is < make and AND (>0 and < constant).
            if (cu.getQueryOperator().equalsIgnoreCase("<=")
                    || cu.getQueryOperator().equalsIgnoreCase("<")) {

                String formula1 = "new GreaterThanFormula( new MonitorReferenceFormula("
                    + monRef + "), new ConstantFormula( new Float(0)))";

                String formula2 =
                    selectFormulaOperator(cu.getQueryOperator())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + cu.getQuery() + ")))";

                formula = "new CountInTimesliceFormula ( new AndFormula( " + formula1
                        + ", " + formula2 + ") , new ConstantFormula(\"" + cu.getPeriodic() + "\"))";

            } else {
                formula =
                        "new CountInTimesliceFormula( " + selectFormulaOperator(cu.getQueryOperator())
                                + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                                + cu.getQuery() + ")))" + " , new ConstantFormula(\"" + cu.getPeriodic() + "\"))";
            }


        } else { // for compound query

            // query 1 parameters.
            CONST cons1 = (CONST) cu.getCompoundQueries()[0].getValue();
            String consValue1 = getValue(cons1);
            // query 2 parameters.
            CONST cons2 = (CONST) cu.getCompoundQueries()[1].getValue();
            String consValue2 = getValue(cons2);

            // query1 formula
            String formula1 =
                    selectFormulaOperator(cu.getCompoundQueries()[0].getComparisonOp().toString())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + consValue1 + ")))";
            // query2 formula
            String formula2 =
                    selectFormulaOperator(cu.getCompoundQueries()[1].getComparisonOp().toString())
                            + "( new MonitorReferenceFormula(" + monRef + "), new ConstantFormula( new Float("
                            + consValue2 + ")))";

            // compound query formula for e.g. (>= "30" s and < "45" s)
            formula =
                    " new CountInTimesliceFormula (" + selectFormulaOperator(cu.getQueryOperator()) + "( " + formula1
                            + ", " + formula2 + ") , new ConstantFormula(\"" + cu.getPeriodic() + "\"))";

        }
        return formula;
    }

    /**
     * Create mean monitor.
     *
     * @param mu the mean utility
     * @param name the monitor name
     */
    private void createMeanMonitor(final MeanUtility mu, final String name) {
        mu.parse();
        String aspect = mu.getAspect();
        // String periodic = mu.getPeriodic().trim();
        String periodic = mu.getPeriodic().replaceAll("\\ ", "");
        correlationParameters.clear();
        /*
         * e.g. aspect contain
         * BookingEvaluation/getSatisfactionLevels/MedicalTreatmentQuality
         */
        StringTokenizer st = new StringTokenizer(aspect, "/");

        st.nextToken();
        String operationName = st.nextToken();
        String liftedAspect = st.nextToken();

        String eventReference1 =
                "new EventReference(\"ServiceOperationCallEndEvent\", null ,"
                        + " \"operation_name='" + operationName + "'\")";
        List<String> eventReferenceLoc = new LinkedList<String>();
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();

        eventReferenceLoc.add(eventReference1);

        correlationParametersLoc.add("operation_name");
        // for parent.
        correlationParameters.add("operation_name");

        // generate monitor definition
        mce.emitMonitorDefinition(name, correlationParametersLoc,
                eventReferenceLoc, monitorReferenceLoc);

        String monitorFormula =
                "new AverageFormula(new EventReferenceFormula("
                        + "new EventReference("
                        + "\"ServiceOperationCallEndEvent\", null, \"operation_name='"
                        + operationName + "'\"),\"" + liftedAspect + "\"), "
                        + "new ConstantFormula(\"" + periodic + "\"))";

        System.err.println(name + " is " + monitorFormula);

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, monitorFormula,
                correlationParametersLoc);
    }

    /**
     * Create count formula.
     *
     * @param cu the count utility
     * @return String the count formula is a printable format
     */
    private String createCountFormula(final CountUtility cu) {
        //count(series(completion_time(Booking/selectService, Booking/bookTreatment),
        //periodic["6" month]), < "30" s)
        String liftedAspect = null;
        String formula = null;
        String eventReference1 = null;
        String eventName = null;
        cu.parse();
        String aspect = cu.getAspect();
        String periodic = cu.getPeriodic().replaceAll("\\ ", "");
        String queryValue = cu.getQuery();

        /*
         * e.g. aspect contain
         * BookingEvaluation/getSatisfactionLevels/MedicalTreatmentQuality
         */
        StringTokenizer st = new StringTokenizer(aspect, "/");

        st.nextToken();
        String operationName = st.nextToken();
        if (st.hasMoreTokens()) {
            liftedAspect = st.nextToken();
        }

        if (operationName.equalsIgnoreCase("bookTripOption")
                || operationName.equalsIgnoreCase("bookMobility")
                || operationName.equalsIgnoreCase("arrival")
                || operationName.equalsIgnoreCase("pickup")
                || operationName.equalsIgnoreCase("treatment")) {

            if (operationName.equalsIgnoreCase("bookMobility")) {
                operationName = operationName + "Stop";
            }
            eventReference1 = "new EventReference(\"" + operationName + "\", null, \"\")";
            eventReference.add(eventReference1);
            eventName = operationName;
            if (cu.getQueryOperator() != null) {
                formula =
                    " new CountInTimesliceFormula( " + selectFormulaOperator(cu.getQueryOperator())
                    + "( new EventReferenceFormula( new EventReference(\""
                            + eventName + "\", null, \"\"), \"" + liftedAspect
                            + "\"), new  ConstantFormula(" + queryValue
                            + ")) , new ConstantFormula(\"" + periodic + "\"))";
            } else {
                formula =
                    " new CountInTimesliceFormula( new EventReferenceFormula( new EventReference(\""
                            + eventName + "\", null, \"\")), new ConstantFormula(\"" + periodic + "\"))";
            }
        } else {
           eventName = "ServiceOperationCallEndEvent";
           eventReference1 =  "new EventReference(\"" + eventName + "\","
                            + " null , \"operation_name='" + operationName + "'\")";
            eventReference.add(eventReference1);
            correlationParameters.add("operation_name");
            if (cu.getQueryOperator() != null) {
                formula =
                    " new CountInTimesliceFormula( " + selectFormulaOperator(cu.getQueryOperator())
                    + "( new EventReferenceFormula( new EventReference(\""
                            + eventName + "\", null, \"operation_name='"
                            + operationName + "'\"), \"" + liftedAspect
                            + "\"), new  ConstantFormula(" + queryValue
                            + ")) , new ConstantFormula(\"" + periodic + "\"))";
            } else {
                formula =
                        " new CountInTimesliceFormula( new EventReferenceFormula( new EventReference(\"" + eventName
                                + "\", null, \"operation_name='" + operationName + "'\")), new ConstantFormula(\""
                                + periodic + "\"))";
            }
        }
        return formula;
    }

    /**
     * Create root monitor.
     *
     * @param name the monitor name
     * @param formula the formula
     */
    private void createRootMonitor(final String name, final String formula) {

        //correlationParameters.add("operation_name");
        Iterator<String> ite = leafReferenceList.iterator();

        if (monitorReference.isEmpty()) { // if monitorReference is empty then only take leaf values.
            while (ite.hasNext()) {
                String varRef = ite.next();
                String monRef =
                        "new MonitorReference(\"" + varRef + "_" + slat.getUuid() + "\",null,\"\")";
                monitorReference.add(monRef);
                }
        }

        // generate monitor definition
        mce.emitMonitorDefinition(name, correlationParameters, eventReference,
                monitorReference);

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, formula,
                correlationParameters);

        // clear monitor reference.
        monitorReference.clear();
    }

    /**
     * Run the interpreter.
     *
     * @param var the variable declaration reference
     * @return String the formula
     */
    public final String run(final VariableDeclr var) {
        String retValue = "";
        monitorName = var.getVar().getValue();

        eventReference = new LinkedList<String>();
        monitorReference = new LinkedList<String>();
        correlationParameters = new LinkedList<String>();

        Expr expr = var.getExpr();

        // check/interpreted for mean.
        MeanUtility mu = new MeanUtility(var);
         // check/interpreted for arrival rate.
        ArrivalRateUtility ar = new ArrivalRateUtility(var);
        // check/interpreted for sum.
        SumUtility sumUtil = new SumUtility(var);
        if (sumUtil.isSum()) {
            retValue = createSumMonitor(sumUtil, var.getVar().toString() + "_" + slat.getUuid());
        } else if (ar.isArrivalRate()) {
           retValue = createArrivalRateMonitor(ar, var.getVar().toString() + "_" + slat.getUuid());
        } else if (mu.isMean()) {
            //handling of completion_time
            FunctionalExpr feC1 = (FunctionalExpr) expr;
            if (String.valueOf(feC1.getParameters()[0]).contains("completion_time")) {
                ValueExpr par1 = feC1.getParameters()[0];
                FunctionalExpr feC2 = (FunctionalExpr) par1;
                ValueExpr par11 = feC2.getParameters()[0];
                FunctionalExpr feC3 = (FunctionalExpr) par11;
                List<String> correlationParametersLoc = new LinkedList<String>();
                String correlator = "callId";
                // param0 completion_time(CitizenI/operatorAnswer, Booking/phoneCall)
                // param1 periodic["1" month]
                // hardcoded fix for handling single parameter in completion_time
                if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("Booking/bookTreatment")
                    || String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("Booking/bookMobility")) {
                    feC3 = (FunctionalExpr) constructExpression(feC3);
                } else if (String.valueOf(feC3.getParameters()[0]).equalsIgnoreCase("CitizenI/operatorAnswer")
                        && String.valueOf(feC3.getParameters()[1]).equalsIgnoreCase("Booking/phoneCall")) {
                    //hard coded fix to append an "End" with phoneCall.
                    ValueExpr[] params = new ValueExpr[2];
                    params[0] = feC3.getParameters()[0];
                    params[1] = new CONST(feC3.getParameters()[1] + "End", null);
                    feC3.setParameters(params);
                    correlator = "callId";
                }
                String completionTimeFormula = analyzeOperator(feC3, correlator, null);
                correlationParametersLoc.add(correlator);
                //String meanMonitorName = var.getVar().getValue();
                String completionTimeMonitorName = monitorName.substring(monitorName.indexOf("_") + 1);

                // generate completion time monitor definition.
                mce.emitMonitorDefinition(completionTimeMonitorName + "_" + slat.getUuid(),
                        correlationParametersLoc, new LinkedList<String>(), monitorReference);
                // generate monitor classes.
                mce.emitMonitorClass(slat.getUuid().getValue(),
                        completionTimeMonitorName + "_" + slat.getUuid(),
                        completionTimeFormula,
                        correlationParametersLoc);
                // generate mean_operation_time
                retValue = createCompleteionTimeMeanMonitor(mu, monitorName + "_" + slat.getUuid(),
                        completionTimeMonitorName + "_" + slat.getUuid());

                // generate max_operation_time at the same time.
                String maxMonitorName = monitorName.replaceAll("mean", "max");
                createCompleteionTimeMaxMonitor(mu, maxMonitorName + "_" + slat.getUuid(),
                        completionTimeMonitorName + "_" + slat.getUuid());

            } else {
            /* this is a termination clause for me the mean is a leaf */
            createMeanMonitor(mu, var.getVar().toString() + "_" + slat.getUuid());
            // add some information because the root monitor need the leaf
            // monitor reference
            }
        } else {
            if (expr instanceof FunctionalExpr) {
                FunctionalExpr fe = (FunctionalExpr) expr;
                retValue = analyze(fe);
                // Here is created the root monitor
                createRootMonitor(var.getVar().toString() + "_" + slat.getUuid(), retValue);
            }
        }
        return retValue;
    }

    /**
     * sum series monitor.
     * @param sumUtil SumUtility.
     * @param name String
     * @return formula String
     */
    private String createSumMonitor(final SumUtility sumUtil, final String name) {
        String liftedAspect = null;
        sumUtil.parse();
        String aspect = sumUtil.getAspect();
        String periodic = sumUtil.getPeriodic().replaceAll("\\ ", "");

        /*
         * e.g. aspect contain
         * Mobility/bookTripOption/coveredKm
         */
        StringTokenizer st = new StringTokenizer(aspect, "/");

        st.nextToken();
        String operationName = st.nextToken();
        if (st.hasMoreTokens()) {
            liftedAspect = st.nextToken();
        }
        String eventRef = "new EventReference(\"" + operationName + "\", null)";
        List<String> eventReferenceLoc = new LinkedList<String>();
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        eventReferenceLoc.add(eventRef);
        // generate monitor definition.
        mce.emitMonitorDefinition(name, correlationParametersLoc,
                eventReferenceLoc, monitorReferenceLoc);
        String monitorFormula =
            "new SumFormula( new EventReferenceFormula( "
                    + eventRef + ", \"" + liftedAspect + "\"), new ConstantFormula(\"" + periodic + "\"))";

     // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, monitorFormula,
                correlationParametersLoc);

        return monitorFormula;
    }

    /**
     * method for creating arrival rate monitor.
     * @param ar ArrivalRateUtility
     * @param name String
     * @return formula String
     */
    private String createArrivalRateMonitor(final ArrivalRateUtility ar, final String name) {
        ar.parse();
        String aspect = ar.getAspect();
        String periodic = ar.getPeriodic().replaceAll("\\ ", "");
        /*
         * e.g. aspect contain
         * Booking/phoneCall
         */
        StringTokenizer st = new StringTokenizer(aspect, "/");
        st.nextToken();
        String operationName = st.nextToken();
        String eventRef = "new EventReference(\"" + operationName + "\", null)";
        List<String> eventReferenceLoc = new LinkedList<String>();
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        eventReferenceLoc.add(eventRef);
        // generate monitor definition
        mce.emitMonitorDefinition(name, correlationParametersLoc,
                eventReferenceLoc, monitorReferenceLoc);

        String monitorFormula =
                "new CountFormula(new EventReferenceFormula("
                        + eventRef + "), new ConstantFormula(\"" + periodic + "\"))";

        System.err.println(name + " is " + monitorFormula);

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, monitorFormula,
                correlationParametersLoc);

        return monitorFormula;

    }

    /**
     * method for creating max monitor for completion time.
     * @param mu MeanUtility
     * @param name String
     * @param monRef String
     */
    private void createCompleteionTimeMaxMonitor(final MeanUtility mu, final String name, final String monRef) {
        mu.parse();
        List<String> eventReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        List<String> monitorReferenceLoc = new LinkedList<String>();
        String periodic = mu.getPeriodic().replaceAll("\\ ", "");

        String monitorReferenceString = "new MonitorReference(\"" + monRef + "\", null, \"\")";

        monitorReferenceLoc.add(monitorReferenceString);

        String monitorFormula =
            "new MaxFormula(new MonitorReferenceFormula(" + monitorReferenceString + "),"
            + " new ConstantFormula(\"" + periodic + "\"))";


        // generate monitor definition
        mce.emitMonitorDefinition(name, correlationParametersLoc, eventReferenceLoc,
                monitorReferenceLoc);

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, monitorFormula,
                correlationParametersLoc);
    }

    /**
     * method for creating mean monitor for completion time.
     * @param mu MeanUtility
     * @param name String
     * @param monRef MonitorReference
     * @return formula String
     */
    private String createCompleteionTimeMeanMonitor(final MeanUtility mu, final String name, final String monRef) {
        mu.parse();
        List<String> eventReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        List<String> monitorReferenceLoc = new LinkedList<String>();
        String periodic = mu.getPeriodic().replaceAll("\\ ", "");

        String monitorReferenceString = "new MonitorReference(\"" + monRef + "\", null, \"\")";

        monitorReferenceLoc.add(monitorReferenceString);

        String monitorFormula =
            "new AverageFormula(new MonitorReferenceFormula(" + monitorReferenceString + "),"
            + " new ConstantFormula(\"" + periodic + "\"))";


        // generate monitor definition
        mce.emitMonitorDefinition(name, correlationParametersLoc, eventReferenceLoc,
                monitorReferenceLoc);

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), name, monitorFormula,
                correlationParametersLoc);

        return monitorFormula;
    }

    /**
     * Method for completing the inputs of completion_time.
     * @param fe FunctionalExpr
     * @return fe1 FunctionalExpr
     */
    private FunctionalExpr constructExpression(final FunctionalExpr fe) {
        ValueExpr[] params = new ValueExpr[2];
        params[0] = new CONST(fe.getParameters()[0] + "Start", null);
        params[1] = new CONST(fe.getParameters()[0] + "Stop", null);
        fe.setParameters(params);
        return fe;
    }

    /**
     * analyze operator monitor.
     * @param fe FunctionalExpression
     * @param correlator String
     * @param name String
     * @return formula String
     */
    private String analyzeOperator(final FunctionalExpr fe, final String correlator,
            final String name) {
        //completion_time(CitizenI/operatorAnswer, Booking/phoneCallEnd)
        String retValue = "";
        String operator = fe.getOperator().toString();
        String initformula = selectFormulaOperator(operator);
        retValue = initformula + "(";
        retValue = retValue + createSubMonitors(fe.getParameters()[0], correlator, name);
        retValue = retValue + ",";
        retValue = retValue + createSubMonitors(fe.getParameters()[1], correlator, name);
        retValue = retValue + ")";
        return retValue;
    }

    /**
     * method for creation of sub monitors for subtract/completion_time.
     * @param veOp1 ValueExpression
     * @param correlator String.
     * @param name String
     * @return formula String
     */
    private String createSubMonitors(final ValueExpr veOp1, final String correlator,
            final String name) {
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        List<String> eventParametersLoc = new LinkedList<String>();
        String eventRef = "";
        String monitorFormula = "";
        String monName = "";
        String query = null;

        //CitizenI/operatorAnswer
        //Booking/phoneCall
        StringTokenizer st = new StringTokenizer(String.valueOf(veOp1), "/");
        String interfaceName = st.nextToken();
        interfaceName = interfaceName.replaceAll("\"", "");
        String operationName = st.nextToken();
        operationName = operationName.replaceAll("\"", "");
        if (st.hasMoreTokens()) { // get query
         query = st.nextToken();
         query = query.replaceAll("\"", "");
        }

        if (name != null) {
            monName = operationName + "_" + name;
        } else {
            monName = operationName;
        }

        //generate monitor operator Answer or phoneCall.
        correlationParametersLoc.add(correlator);
        // event reference to be put inside modi
        eventRef  = "new EventReference(\"" + operationName + "\",null, \""
            + correlator + "=$" + correlator + "\")";
        eventParametersLoc.add(eventRef);
        // monitor formula
        if (query != null) {
            monitorFormula = "new EventReferenceFormula(" + eventRef + ", \"" + query + "\")";
        } else {
            monitorFormula = "new EventReferenceFormula(" + eventRef + ")";
        }
        // generate monitor definition
        mce.emitMonitorDefinition(monName + "_" + slat.getUuid(),
                correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);
        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), monName + "_" + slat.getUuid(),
                monitorFormula, correlationParametersLoc);

        // monitor reference to be put in parent monitor definition
        String monRef = "new MonitorReference(\"" + monName + "_"
        + slat.getUuid() + "\", null, \"" + correlator + "=$" + correlator + "\")";
        monitorReference.add(monRef);
        // generate formula for parent
        String ret = "new MonitorReferenceFormula(";
        ret = ret + monRef + ")";
        return ret;
    }


}
