/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Invocation;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;


/**
 * Create the monitor runtime classes source code
 * starting from the sla template agreement.
 *
 * @since 0.1
 */
public class SlatToMonitorClasses {

    /** The sla template. */
    private SLATemplate slat;

    /** The agreement term. */
    private AgreementTerm agreementTerm;

    /** The guaranteed state. */
    private Guaranteed guaranteed;

    /** The deploy deir where put the generated classes. */
    private String deployDir;

    /**
     * Constructor.
     *
     * @param slatIn the sla template
     * @param agreementTermIn the agreement term
     * @param guaranteedIn the guaranteed state
     * @param deployDirIn the deploy directory
     */
    public SlatToMonitorClasses(final SLATemplate slatIn,
            final AgreementTerm agreementTermIn, final Guaranteed guaranteedIn,
            final String deployDirIn) {
        super();
        this.slat = slatIn;
        this.agreementTerm = agreementTermIn;
        this.guaranteed = guaranteedIn;
        this.deployDir = deployDirIn;
    }

    /**
     * Constructor.
     *
     * @param slatIn the sla template
     * @param agreementTermIn the agreement term
     * @param deployDirIn the deploy directory
     */
    public SlatToMonitorClasses(final SLATemplate slatIn,
            final AgreementTerm agreementTermIn, final String deployDirIn) {
        super();
        this.slat = slatIn;
        this.agreementTerm = agreementTermIn;
        this.deployDir = deployDirIn;
    }
    /**
     * Get the slat.
     *
     * @return SLATemplate
     */
    public final SLATemplate getSlat() {
        return this.slat;
    }

    /**
     * Set the slat.
     *
     * @param slatIn the sla template
     */
    public final void setSlat(final SLATemplate slatIn) {
        this.slat = slatIn;
    }

    /**
     * Get the agreement term.
     *
     * @return AgreementTerm the agreement term
     */
    public final AgreementTerm getAgreementTerm() {
        return this.agreementTerm;
    }

    /**
     * Set the agreement term.
     *
     * @param agreementTermIn the agreement term
     */
    public final void setAgreementTerm(final AgreementTerm agreementTermIn) {
        this.agreementTerm = agreementTermIn;
    }

    /**
     * Get the guaranteed.
     *
     * @return Guaranteed
     */
    public final Guaranteed getGuaranteed() {
        return this.guaranteed;
    }

    /**
     * Set guaranteed.
     *
     * @param guaranteedIn the guaranteed
     */
    public final void setGuaranteed(final Guaranteed guaranteedIn) {
        this.guaranteed = guaranteedIn;
    }

    /**
     * Get the deploy directory.
     *
     * @return String the deploy directory location for the monitors
     */
    public final String getDeployDir() {
        return this.deployDir;
    }

    /**
     * Set the deploy directory.
     *
     * @param deployDirIn
     *            the deploy directory location for the monitors
     */
    public final void setDeployDir(final String deployDirIn) {
        this.deployDir = deployDirIn;
    }

    /**
     * Get the guaranteed state name.
     *
     * @param  guaranteeIn the guaranteed
     * @return String the guaranteed state name
     */
    private String getGuaranteedStateName(final Guaranteed guaranteeIn) {
        return guaranteeIn.getId().getValue();
    }

    /**
     * The the guarantee term formula in the bnf format.
     *
     * @param guaranteeBnfFormat the guarantee state in bnf format
     * @return String the extracted argument
     */
    private String getArgument(final String guaranteeBnfFormat) {
        if (guaranteeBnfFormat == null) {
            return "";
        }

        // TODO this is an hack to extract the service name from the guarantee
        // state from the bnf format
        // if it will possible take it with a clear API

        String ret = new String();
        int beginIndex = guaranteeBnfFormat.indexOf("(");
        if (beginIndex < 0) {
            return ret;
        }
        int endIndex = guaranteeBnfFormat.indexOf(")");
        if (endIndex < 0) {
            return ret;
        }
        ret = guaranteeBnfFormat.substring(beginIndex + 1, endIndex);

        return ret;
    }

    /** Translate. */
    public final void translate(java.util.HashMap<String, Object> map) {
        // Here there is the guarantee in a string format
        String guaranteeBnfFormat = null;
        if (!(guaranteed instanceof Guaranteed.State)) {
            // guarantee action are handled here
            translateGA(guaranteed, map);
            return;
        }
        Guaranteed.State state = (Guaranteed.State) guaranteed;
        guaranteeBnfFormat = state.getState().toString();
        // @TODO guarantee action
        if (guaranteeBnfFormat == null) {
            return;
        }
        String preamble =
                "Translate the garanteed state " + state.getId() + " : '"
                        + guaranteeBnfFormat + "'";
        MonitorClassEmitter mce = new MonitorClassEmitter(deployDir);
        String guaranteedStateName = getGuaranteedStateName(guaranteed);
        String monitorName = guaranteedStateName + "_" + slat.getUuid();
        List<String> correlationParameters = new LinkedList<String>();
        String arguments = getArgument(guaranteeBnfFormat);
        String monitorFormula = new String();
        List<String> eventReference = new LinkedList<String>();
        List<String> monitorReference = new LinkedList<String>();

        //Handling ORC Guaranteed.
        if (guaranteedStateName.equalsIgnoreCase("ORCThroughputConstraintPayment")) {
            VariableDeclr var = agreementTerm.getVariableDeclr("Var_CustomerConstraintPayment");
            Customisable customisable = (Customisable) var;
            String threshold = customisable.getValue().getValue();
            threshold.replaceAll("\"", "");
            //arrival_rate Monitor for ORC
            List<String> monitorReferenceLoc = new LinkedList<String>();
            List<String> correlationParametersLoc = new LinkedList<String>();
            List<String> eventParametersLoc = new LinkedList<String>();

            String eventRef = "new EventReference(\"paymentValidationStart\", null, \"processId=$processId\")";

            eventParametersLoc.add(eventRef);
            correlationParametersLoc.add("processId");

            String orcMonitorFormula = "new LesserThanFormula( new CountFormula( new EventReferenceFormula("
                + eventRef + ")), new ConstantFormula(new Float(" + threshold + ")))";

            mce.emitMonitorClass(slat.getUuid().getValue(),
                    guaranteedStateName/* + "_" + String.valueOf(Math.abs(slat.getUuid().hashCode()))*/,
                    orcMonitorFormula, correlationParametersLoc);
            mce.emitMonitorDefinition(guaranteedStateName
                    /* + "_" + String.valueOf(Math.abs(slat.getUuid().hashCode()))*/,
                    correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);
            return;
        }

        if (guaranteedStateName.equalsIgnoreCase("ORCResponseTimePaymentState")) {
            VariableDeclr var = agreementTerm.getVariableDeclr("Var_ORCResponseTimePayment");
            Customisable customisable = (Customisable) var;
            String threshold = customisable.getValue().getValue();
            threshold.replaceAll("\"", "");
            // completion_time(ORCPaymentService) < "100.0" ms
            List<String> monitorReferenceLoc = new LinkedList<String>();
            List<String> correlationParametersLoc = new LinkedList<String>();
            List<String> eventParametersLoc = new LinkedList<String>();
            //ORCPaymentServiceStart
            String eventRef1 = "new EventReference(\"paymentValidationStart\", null, \"processId=$processId\")";
            eventParametersLoc.add(eventRef1);
            correlationParametersLoc.add("processId");
            String orcPaymentServiceStartFormula = "new EventReferenceFormula( "
                + eventRef1 + ")";
            mce.emitMonitorClass(slat.getUuid().getValue(),
                    "ORCPaymentServiceStart",
                    orcPaymentServiceStartFormula, correlationParametersLoc);
            mce.emitMonitorDefinition("ORCPaymentServiceStart",
                    correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);
            //ORCPaymentServiceStop
            String eventRef2 = "new EventReference(\"paymentValidationStop\", null, \"processId=$processId\")";
            eventParametersLoc.clear();
            eventParametersLoc.add(eventRef2);
            String orcPaymentServiceStopFormula = "new EventReferenceFormula( "
                + eventRef2 + ")";
            mce.emitMonitorClass(slat.getUuid().getValue(),
                    "ORCPaymentServiceStop",
                    orcPaymentServiceStopFormula, correlationParametersLoc);
            mce.emitMonitorDefinition("ORCPaymentServiceStop",
                    correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);
            //ORCThroughputConstraint
            String orcThroughputConstraintFormula =
                    "new LesserThanFormula( new TimeFormula( new MonitorReferenceFormula( "
                            + "new MonitorReference(\"ORCPaymentServiceStart\", null, \"processId=$processId\")),"
                            + "new MonitorReferenceFormula( "
                            + "new MonitorReference(\"ORCPaymentServiceStop\", null, \"processId=$processId\"))),"
                            + " new ConstantFormula(new Float(" + threshold + ")))";
            String monRefORCPaymentServiceStart =
                    "new MonitorReference(\"ORCPaymentServiceStart\", null, \"processId=$processId\")";
            String monRefORCPaymentServiceStop =
                    "new MonitorReference(\"ORCPaymentServiceStop\", null, \"processId=$processId\")";
            monitorReferenceLoc.add(monRefORCPaymentServiceStart);
            monitorReferenceLoc.add(monRefORCPaymentServiceStop);
            mce.emitMonitorClass(slat.getUuid().getValue(),
                    guaranteedStateName,
                    orcThroughputConstraintFormula, correlationParametersLoc);
            mce.emitMonitorDefinition(guaranteedStateName,
                    correlationParametersLoc, new LinkedList(), monitorReferenceLoc);
            return;
        }

        if (guaranteeBnfFormat.startsWith("availability")) {
            /* This parameter is fixed to identify the service */
            correlationParameters.add("service");
            String eventAvailableReference =
                    "new EventReference(\"Available\", null, \"service='"
                            + arguments + "'\")";
            String eventNotAvailableReference =
                    "new EventReference(\"NotAvailable\", null, \"service='"
                            + arguments + "'\")";
            eventReference.add(eventAvailableReference);
            eventReference.add(eventNotAvailableReference);
            String availableCountFormula =
                    "new CountFormula(new EventReferenceFormula("
                            + eventAvailableReference + "))";
            String notAvailableCountFormula =
                    "new CountFormula(new EventReferenceFormula("
                            + eventNotAvailableReference + "))";
            int percInit = guaranteeBnfFormat.indexOf("\"") + 1;
            int percEnd = guaranteeBnfFormat.lastIndexOf("\"");
            String percentage = guaranteeBnfFormat.substring(percInit, percEnd);
            monitorFormula =
                    "new GreaterThanFormula(new DivFormula(new StarFormula("
                            + availableCountFormula
                            + ", new ConstantFormula(new Float(\"100\"))), "
                            + "new PlusFormula(" + availableCountFormula + ","
                            + notAvailableCountFormula
                            + ")), new ConstantFormula(new Float(\""
                            + percentage + "\")))";
            mce.emitEventDefinition("Available", correlationParameters,
                    monitorName);
            mce.emitEventDefinition("NotAvailable", correlationParameters,
                    monitorName);
            System.out.println(preamble + " --> availability is "
                    + monitorFormula);

        } else if (guaranteeBnfFormat.startsWith("count")) {
            /* This parameter is fixed to identify the service */
            correlationParameters.add("service");

            String eventReference1 =
                    "new EventReference(\"Request\", null, \"service='"
                            + arguments + "'\")";
            eventReference.add(eventReference1);

            String quantity = "1";

            /* How I can take the value from the variable declaration */

            monitorFormula =
                    "new EquFormula(new CountFormula(new EventReferenceFormula("
                            + eventReference1
                            + ")),new ConstantFormula(new Float(" + quantity
                            + ")))";
            mce.emitEventDefinition("Request", correlationParameters,
                    monitorName);
            System.out.println(preamble + "--> count is " + monitorFormula);
        } else {

            TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();

            Object tceValue = tce.getValue();

            if (tceValue instanceof ID) {
                ID id = (ID) tceValue;
                /* e.g AVERAGE_SATISFACTION_LEVEL */
                String idValue = id.getValue();
                VariableDeclr var = slat.getVariableDeclr(idValue);

                // store guaranteed state inside provider-kpi map.
                String provider = "";
                if (var.getPropertyKeys().length > 0) {
                    provider = var.getPropertyValue(new STND("Accountable"));
                } else {
                    for (Party party : slat.getParties()) {
                        if (party.getAgreementRole().getValue().equalsIgnoreCase(
                                "http://www.slaatsoi.org/slamodel#provider")) {
                            provider = party.getId().getValue();
                            break;
                        }
                    }
                }

                if (!map.containsKey(provider)) {
                    List list = new java.util.ArrayList();
                    list.add(guaranteed);
                    map.put(provider, list);
                } else {
                    List list = (List) map.get(provider);
                    list.add(guaranteed);
                }

                InteractionEventInterpreter asi =
                        new InteractionEventInterpreter(slat, mce);

                System.out.println(preamble + " --> " + idValue + " is "
                        + asi.run(var));

                /* now must be translated AVERAGE_SATISFACTION_LEVEL > "3" */

                SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();

                String operator = sde.getComparisonOp().toString();

                Object value = sde.getValue();

                String formulaValue = "";

                if (value instanceof CONST) {
                    CONST constant = (CONST) value;
                    Object constantValue = constant.getValue();
                    if (constant.getDatatype() != null
                            && constant.getDatatype().getValue().indexOf(
                                    "percentage") != -1) {
                        constantValue =
                                Float.parseFloat(constant.getValue()) / 100;
                    }
                    if (constant.getDatatype() != null
                            && constant.getDatatype().getValue().indexOf(
                                    "units#s") != -1) {
                        constantValue =
                                Float.parseFloat(constant.getValue()) * 1000;
                    }

                    formulaValue = asi.selectFormulaOperator(operator);
                    formulaValue =
                            formulaValue + "(new MonitorReferenceFormula("
                                    + "new MonitorReference(\"" + idValue + "_" + slat.getUuid()
                                    + "\", null,\"\")),new ConstantFormula"
                                    + "(new Float(" + constantValue.toString()
                                    + ")))";

                    mce.emitMonitorClass(slat.getUuid().getValue(), monitorName, formulaValue,
                            correlationParameters);

                    List<String> monitorReferenceInt = new LinkedList<String>();

                    String monitorReference1 =
                            "new MonitorReference(\"" + idValue + "_" + slat.getUuid()
                                    + "\", null , \"\")";

                    monitorReferenceInt.add(monitorReference1);

                    mce.emitMonitorDefinition(monitorName,
                            correlationParameters, new LinkedList<String>(),
                            monitorReferenceInt);

                }
            }
            return;
        }

        // generate monitor classes
        mce.emitMonitorClass(slat.getUuid().getValue(), monitorName,
                monitorFormula, correlationParameters);

        // generate monitor definition
        mce.emitMonitorDefinition(monitorName, correlationParameters,
                eventReference, monitorReference);
    }

    /**
     * translation of guaranteed action.
     * @param ga Guranteed.
     */
    private void translateGA(final Guaranteed ga, java.util.HashMap<String, Object> map) {
        Guaranteed.Action actions = (Guaranteed.Action) ga;
        Guaranteed.Action.Defn postCondition = (Guaranteed.Action.Defn) actions.getPostcondition();
        if (postCondition instanceof Invocation) {
            Invocation invocation = (Invocation) postCondition;
            for (int i = 0; i < invocation.getParameterKeys().length; i++) {
                ID temp = invocation.getParameterKeys()[i];
                ValueExpr idValue = invocation.getParameterValue(temp);
                if (!isGuranteed(idValue.toString())) {
                    // check for KPI that can be translated by the system.
                    if (idValue.toString().equalsIgnoreCase("penalty")//change it to penalty later.
                            || idValue.toString().equalsIgnoreCase("numb_treatments_with_long_waiting")
                            || idValue.toString().equalsIgnoreCase("numb_late_pickup")
                            || idValue.toString().equalsIgnoreCase("numb_late_arrival")
                            || idValue.toString().equalsIgnoreCase("perc_mob_booking_satisfied")
                            || idValue.toString().equalsIgnoreCase("percent_late_arrival")
                            || idValue.toString().equalsIgnoreCase("percent_late_pickup")
                            || idValue.toString().equalsIgnoreCase("numb_prov_treatments")
                            || idValue.toString().equalsIgnoreCase("total_covered_distance")
                            || idValue.toString().equalsIgnoreCase("days_of_service")
                            || idValue.toString().equalsIgnoreCase("t4")
                            || idValue.toString().equalsIgnoreCase("t5")
                            || idValue.toString().equalsIgnoreCase("t3")
                            || idValue.toString().equalsIgnoreCase("t2")
                            || idValue.toString().equalsIgnoreCase("t1")
                            || idValue.toString().equalsIgnoreCase("ar")
                            || idValue.toString().equalsIgnoreCase("numb_transported_citizens")
                            || idValue.toString().equalsIgnoreCase("mean_mob_options")
                            || idValue.toString().equalsIgnoreCase("mean_health_book_time")
                            || idValue.toString().equalsIgnoreCase("mean_mobil_book_time")
                            || idValue.toString().equalsIgnoreCase("mean_book_time")
                            || idValue.toString().equalsIgnoreCase("max_health_book_time")
                            || idValue.toString().equalsIgnoreCase("max_mobil_book_time")
                            || idValue.toString().equalsIgnoreCase("max_book_time")) {
                        VariableDeclr var = slat.getVariableDeclr(idValue.toString());
                        // store guaranteed action inside provider-kpi map.
                        String provider = "";
                        if (var.getPropertyKeys().length > 0) {
                            provider = var.getPropertyValue(new STND("Accountable"));
                        } else {
                            for (Party party : slat.getParties()) {
                                if (party.getAgreementRole().getValue().equalsIgnoreCase(
                                        "http://www.slaatsoi.org/slamodel#provider")) {
                                    provider = party.getId().getValue();
                                    break;
                                }
                            }
                        }
                        if (!map.containsKey(provider)) {
                            List list = new java.util.ArrayList();
                            list.add(idValue);
                            map.put(provider, list);
                        } else {
                            List list = (List) map.get(provider);
                            list.add(idValue);
                        }

                        // translation of days_of_service is provided internally by rcg.
                        if (idValue.toString().equalsIgnoreCase("days_of_service")
                                || idValue.toString().equalsIgnoreCase("max_health_book_time")
                                || idValue.toString().equalsIgnoreCase("max_mobil_book_time")
                                || idValue.toString().equalsIgnoreCase("percent_late_pickup")
                                || idValue.toString().equalsIgnoreCase("t4")
                                || idValue.toString().equalsIgnoreCase("t5")
                                || idValue.toString().equalsIgnoreCase("t3")
                                || idValue.toString().equalsIgnoreCase("t2")
                                || idValue.toString().equalsIgnoreCase("t1")
                                || idValue.toString().equalsIgnoreCase("ar")
                                || idValue.toString().equalsIgnoreCase("max_book_time")) {
                            continue;
                        } else if (idValue.toString().equalsIgnoreCase("penalty")) {
                            // handling for penalty kpi.
                            var = slat.getVariableDeclr("pcc");
                        } else if (idValue.toString().equalsIgnoreCase("percent_late_arrival")) {
                            // create numb_arrived_citized for internal handling.
                            createNumArrivedCitizensMonitor(slat.getUuid().getValue());
                            continue;
                        } else if (idValue.toString().equalsIgnoreCase("mean_mob_options")) {
                            // create mean_mob_options.
                            createMeanMobOptionsMonitor(slat.getUuid().getValue());
                            continue;
                        } else if (idValue.toString().equalsIgnoreCase("mean_health_book_time")
                                || idValue.toString().equalsIgnoreCase("mean_mobil_book_time")
                                || idValue.toString().equalsIgnoreCase("mean_book_time")) {
                            // check if it is already translated
                            if (!isGuranteed(idValue.toString())) {
                               var = slat.getVariableDeclr(idValue.toString());
                            } else {
                              continue;
                            }

                        }

                        // start translation.
                        MonitorClassEmitter mce = new MonitorClassEmitter(deployDir);
                        InteractionEventInterpreter asi = new InteractionEventInterpreter(slat, mce);
                        asi.run(var);

                    }
                }
            }
        }
    }

    /**
     * mean_mob_options monitor.
     * @param slaUuid String
     */
    private void createMeanMobOptionsMonitor(final String slaUuid) {
        // create mean_mob_options Monitor
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        List<String> eventParametersLoc = new LinkedList<String>();
        String monitorName = "mean_mob_options" + "_" + slaUuid;
        String eventRef = "new EventReference(\"getTripOptions\", null, \"\")";
        eventParametersLoc.add(eventRef);
        String monitorFormula = "new AverageFormula( new EventReferenceFormula("
            + eventRef + ", \"trip_options_number\"), new ConstantFormula(\"1month\"))";
        MonitorClassEmitter mce = new MonitorClassEmitter(deployDir);
        mce.emitMonitorClass(slat.getUuid().getValue(),
                monitorName, monitorFormula, correlationParametersLoc);
        mce.emitMonitorDefinition(monitorName,
                correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);

    }

    /**
     * number of arrival citizen monitor.
     * @param slaUuid String.
     */
    private void createNumArrivedCitizensMonitor(final String slaUuid) {
        // create numb_arrived_citizen Monitor
        List<String> monitorReferenceLoc = new LinkedList<String>();
        List<String> correlationParametersLoc = new LinkedList<String>();
        List<String> eventParametersLoc = new LinkedList<String>();
        String monitorName = "numb_arrived_citizens" + "_" + slaUuid;
        String eventRef = "new EventReference(\"arrival\", null, \"\")";
        eventParametersLoc.add(eventRef);
        String monitorFormula = "new CountInTimesliceFormula( new EventReferenceFormula("
            + eventRef + "), new ConstantFormula(\"1month\"))";
        MonitorClassEmitter mce = new MonitorClassEmitter(deployDir);
        mce.emitMonitorClass(slat.getUuid().getValue(),
                monitorName, monitorFormula, correlationParametersLoc);
        mce.emitMonitorDefinition(monitorName,
                correlationParametersLoc, eventParametersLoc, monitorReferenceLoc);
    }

    /**
     * check if existing guaranteed state of same ID.
     * @param temp ID
     * @return true if present
     */
    private boolean isGuranteed(final String temp) {
        Boolean found = false;
        int agreementTermsNumber = slat.getAgreementTerms().length;
        for (int i = 0; i < agreementTermsNumber; i++) {
            AgreementTerm agreeTerm = slat.getAgreementTerms()[i];
            int guaramteesNumber = agreeTerm.getGuarantees().length;
            for (int j = 0; j < guaramteesNumber; j++) {
                Guaranteed guaranteedT = agreeTerm.getGuarantees()[j];
                if (guaranteedT instanceof Guaranteed.State) {
                    Guaranteed.State state = (Guaranteed.State) guaranteedT;
                    TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();
                    if (tce.getValue().toString().equalsIgnoreCase(temp)) {
                    found = true;
                    return found;
                }
               }
            }
        }
        return found;
    }

    /**
     * method for translating precondition.
     */
    public final void translatePrecondition() {

        TypeConstraintExpr tce = (TypeConstraintExpr) agreementTerm.getPrecondition();
        Object tceValue = tce.getValue();
        MonitorClassEmitter mce = new MonitorClassEmitter(deployDir);

        if (String.valueOf(tceValue).equals("ar")) { // arrival rate.
            VariableDeclr var = slat.getVariableDeclr(String.valueOf(tceValue));
            InteractionEventInterpreter asi = new InteractionEventInterpreter(slat, mce);
            asi.run(var);
            SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();
            String operator = sde.getComparisonOp().toString();
            Object value = sde.getValue();
            String formulaValue = "";
            if (value instanceof CONST) {
                CONST constant = (CONST) value;
                Object constantValue = constant.getValue();
                if (constant.getDatatype() != null
                        && constant.getDatatype().getValue().
                        equalsIgnoreCase("http://www.slaatsoi.org/extension#calls_per_day")) {
                    constantValue = Float.parseFloat(constant.getValue());
                }
                formulaValue = asi.selectFormulaOperator(operator);
                formulaValue =
                        formulaValue + "( new MonitorReferenceFormula( " + "new MonitorReference(\"" + tceValue + "_"
                                + slat.getUuid() + "\", null,\"\")), new ConstantFormula" + "(new Float("
                                + constantValue.toString() + ")))";

                mce.emitMonitorClass(slat.getUuid().getValue(), "precondition_" + agreementTerm.getId()
                        + "_" + slat.getUuid().getValue(), formulaValue, new LinkedList<String>());

                List<String> monitorReferenceInt = new LinkedList<String>();

                String monitorReference1 =
                        "new MonitorReference(\"" + tceValue + "_" + slat.getUuid() + "\", null , \"\")";

                monitorReferenceInt.add(monitorReference1);

                mce.emitMonitorDefinition("precondition_" + agreementTerm.getId() + "_" + slat.getUuid().getValue(),
                        new LinkedList<String>(), new LinkedList<String>(), monitorReferenceInt);

            }
        }
    }
}
