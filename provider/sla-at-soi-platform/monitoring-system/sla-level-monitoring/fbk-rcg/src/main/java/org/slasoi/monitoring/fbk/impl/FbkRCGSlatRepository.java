/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.util.Hashtable;

/**
 * This repository contains the slat monitored;
 * this is useful to have the Slat info associated to the monitor.
 *
 * @since 0.1
 */
public final class FbkRCGSlatRepository {

    /** The internal hashtable of the repository. */
    private static Hashtable<String, Object> hash = null;

    /** Private constructor; this is a singleton.*/
    private FbkRCGSlatRepository() {
        /* empty block */
    }

    /**
     * Get the instance.
     *
     * @return Hashtable
     */
    public static Hashtable<String, Object> getInstance() {
        if (hash == null) {
            hash = new Hashtable<String, Object>();
        }
        return hash;
    }

    /**
     * Add a slat to the repository.
     *
     * @param key the slat id
     * @param value a SLA or SLAT object
     */
    public static void addSlat(final String key, final Object value) {
        getInstance().put(key, value);
    }

    /**
     * Get a slat called by name; the object is a SLA or a SLAT.
     *
     * @param key the slat id
     * @return Object a SLA or SLAT object
     */
    public static Object getSlat(final String key) {
        return getInstance().get(key);
    }

}
