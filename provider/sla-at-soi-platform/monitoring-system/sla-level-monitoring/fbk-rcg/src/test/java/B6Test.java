/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.monitoring.fbk.impl.FbkRCGServiceImpl;

/** Class to test the b6 usecase */
public class B6Test {
	
	private static final String B6_HOME = System.getenv("B6_HOME");
	private String slasPath = B6_HOME + System.getProperty("file.separator")
	+ "b6sla-creation" + System.getProperty("file.separator")
	+ "test" + System.getProperty("file.separator");

    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
	 * 
	 */
    @Test
    public void testEventParsing1() {
        try {

            System.out.println("*****************************************");
            System.out.println("START TEST testB6");
            System.out.println("*****************************************");

            FbkRCGServiceImpl fbkRcg = new FbkRCGServiceImpl();
            fbkRcg.cleanDeployDir();

            String configuration_id = "gggg:ggggg:gggg:gggg";
            fbkRcg.startMonitoring(configuration_id);

            
			InputStream isB6SLA1 = new FileInputStream(slasPath + "B6SLA1.xml");
			InputStream isB6SLAHumanOperator1 = new FileInputStream(slasPath + "B6SLAHumanOperator1.xml");
			InputStream isB6SLAHumanOperator2 = new FileInputStream(slasPath + "B6SLAHumanOperator2.xml");
			InputStream isB6SLAShuttle1 = new FileInputStream(slasPath + "B6SLAShuttle1.xml");
			InputStream isB6SLAShuttle2 = new FileInputStream(slasPath + "B6SLAShuttle2.xml");
			InputStream isB6SLACab1 = new FileInputStream(slasPath + "B6SLACab1.xml");
			InputStream isB6SLACab2 = new FileInputStream(slasPath + "B6SLACab2.xml");
			fbkRcg.addSLATToConfiguration(isB6SLA1);
			System.out.println("B6SLA1 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLAHumanOperator1);
			System.out.println("B6SLAHumanOperator1 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLAHumanOperator2);
			System.out.println("B6SLAHumanOperator2 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLAShuttle1);
			System.out.println("B6SLAShuttle1 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLAShuttle2);
			System.out.println("B6SLAShuttle2 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLACab1);
			System.out.println("B6SLACab1 added to MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLACab2);
			System.out.println("B6SLACab2 added to MONITORING ENGINE");
			//Thread.sleep(10 * 60 *1000);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testEventParsing2() {
        try {

            System.out.println("*****************************************");
            System.out.println("START TEST testB6");
            System.out.println("*****************************************");

            FbkRCGServiceImpl fbkRcg = new FbkRCGServiceImpl();
            String configuration_id = "gggg:ggggg:gggg:gggg";
            fbkRcg.startMonitoring(configuration_id);
            
			InputStream isB6SLA2Agreed = new FileInputStream(slasPath + "B6SLA2.xml");
			InputStream isB6SLA22Agreed = new FileInputStream(slasPath + "B6SLA22.xml");
			fbkRcg.replaceSLA(isB6SLA2Agreed);
			System.out.println("B6SLA2Agreed replacesc B6SLA1 inside MONITORING ENGINE");
			fbkRcg.addSLATToConfiguration(isB6SLA22Agreed);
			System.out.println("B6SLA22Agreed added to MONITORING ENGINE");
			//Thread.sleep(10 * 60 *1000);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
