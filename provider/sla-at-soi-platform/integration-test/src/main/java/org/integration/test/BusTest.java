/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.integration.test;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;

/**
 * Class for generating bus messages.
 * 
 * @author khurshid
 * 
 */
public final class BusTest {
    /**
     * private constructor.
     */
    private BusTest() {
    }

    /**
     * @param args
     *            string[]
     */
    public static void main(final String[] args) {
        // while(true){
        // PointToPointHelper helper =
        // new PointToPointHelper();
        // helper.connect();
        //
        // try {
        // Thread.sleep(1000);
        // } catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // }
        Settings settings = new Settings();
        settings.setSetting(Setting.pubsub, "xmpp");
        settings.setSetting(Setting.xmpp_username, "primitive-ecf");
        settings.setSetting(Setting.xmpp_password, "primitive-ecf");
        settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
        settings.setSetting(Setting.xmpp_port, "5222");
        settings.setSetting(Setting.messaging, "xmpp");
        settings.setSetting(Setting.pubsub, "xmpp");
        settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
        settings.setSetting(Setting.xmpp_resource, "test");
        settings.setSetting(Setting.xmpp_pubsubservice,
                "pubsub.testbed.sla-at-soi.eu");
        String channelName = "test-messagging";

        try {
            PubSubManager pubSubManager1 = PubSubFactory
                    .createPubSubManager(settings);
            pubSubManager1.createChannel(new Channel(channelName));
            System.out.print("testing.......");
            // pubSubManager1.subscribe(channelName);
            // pubSubManager1.addMessageListener(new MessageListener() {
            // public void processMessage(MessageEvent messageEvent) {
            // System.out.println("*************** XMPP message:
            // " + messageEvent.getMessage().getPayload() +
            // "**************************");
            // }});

            for (int i = 0; i < 20; i++) {
                Thread.sleep(2000);
                pubSubManager1.publish(new PubSubMessage(channelName,
                        "test-------------" + Integer.toString(i)));
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Settings settings = new Settings();
        // settings.setSetting(Setting.pubsub, "xmpp");
        // settings.setSetting(Setting.xmpp_username, "blazeds-listener");
        // settings.setSetting(Setting.xmpp_password, "blazeds-listener");
        // settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
        // settings.setSetting(Setting.xmpp_port, "5222");
        // settings.setSetting(Setting.messaging, "xmpp");
        // settings.setSetting(Setting.pubsub, "xmpp");
        // settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
        // settings.setSetting(Setting.xmpp_resource, "blazeds");
        // settings.setSetting(Setting.xmpp_pubsubservice,
        // "pubsub.testbed.sla-at-soi.eu");
        //
        // Settings settings2 = new Settings();
        // settings2.setSetting(Setting.pubsub, "xmpp");
        // settings2.setSetting(Setting.xmpp_username, "primitive-ecf");
        // settings2.setSetting(Setting.xmpp_password, "primitive-ecf");
        // settings2.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
        // settings2.setSetting(Setting.xmpp_port, "5222");
        // settings2.setSetting(Setting.messaging, "xmpp");
        // settings2.setSetting(Setting.pubsub, "xmpp");
        // settings2.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
        // settings2.setSetting(Setting.xmpp_resource, "blazeds");
        // settings2.setSetting(Setting.xmpp_pubsubservice,
        // "pubsub.testbed.sla-at-soi.eu");
        //
        // System.out.println(
        // "*************** XMPP settings instances created *************");
        //
        // String channelName = "landscape";
        // /*
        // try {
        // PubSubManager pubSubManager2 = PubSubFactory.
        // createPubSubManager(settings);
        //
        //
        // pubSubManager2.subscribe(channelName);
        // } catch (MessagingException e) {
        // e.printStackTrace();
        //
        // } catch (Exception e) {
        // e.printStackTrace();
        //
        // }*/
        //
        //
        //
        // try{
        //
        // PubSubManager pubSubManager1 = PubSubFactory.
        // createPubSubManager(settings);
        // PubSubManager pubSubManager2 = PubSubFactory.
        // createPubSubManager(settings2);
        //
        // pubSubManager2.addMessageListener(new MessageListener() {
        // public void processMessage(MessageEvent messageEvent) {
        // System.out.println("*************** XMPP message: "
        // + messageEvent.getMessage().getPayload()
        // + "**************************");
        // }});
        //
        // pubSubManager1.createChannel(new Channel(channelName));
        //
        // pubSubManager2.subscribe(channelName);
        //
        // pubSubManager1.
        // publish(new PubSubMessage(channelName, "test1-------------"));
        //
        // pubSubManager2.
        // publish(new PubSubMessage(channelName, "test2----------------"));
        //
        // try {
        // Thread.sleep(200);
        // }
        // catch (InterruptedException e) {
        // e.printStackTrace();
        // }
        //
        // pubSubManager2.unsubscribe(channelName);
        //
        // pubSubManager1
        // .publish(new PubSubMessage(channelName, "test3--------------"));
        //
        // try {
        // Thread.sleep(200);
        // }
        // catch (InterruptedException e) {
        // e.printStackTrace();
        // }
        //
        // pubSubManager1.close();
        // pubSubManager2.close();
        // }
        // catch (Exception e) {
        // e.printStackTrace();
        // }

    }

}
