/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.integration.test;

//import java.io.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Read the SOAP envelop fiel passed as second parameter, pass it to SOAP
 * endpoint passed as the first paramenter, and print out the SOAP envelope as a
 * response.
 * 
 * @author khurshid
 */
public final class SOAPClient4XG {

    /**
     * private constructor.
     */
    private SOAPClient4XG() {
    }

    /**
     * Main.
     * 
     * @param args
     *            string[]
     * @throws Exception
     *             exception
     */
    public static void main(final String[] args) throws Exception {

        /*
         * if (args.length < 2) {
         * System.err.println("Usage:  java SOAPClient4XG " +
         * "http://soapURL soapEnvelopefile.xml" + " [SOAPAction]");
         * System.err.println("SOAPAction is optional."); System.exit(1); }
         */

        String soapURL = "http://localhost:8080/services/BusinessManager_QueryProductCatalog?wsdl";
        String xmlFile2Send = "properties/soap.xml"/* args[1] */;

        String soapAction = "";
        if (args.length > 2) {
            soapAction = args[2];
        }

        // Create the connection where we're going to send the file.
        URL url = new URL(soapURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection) connection;

        // Open the input file. After we copy it to a byte array, we can see
        // how big it is so that we can set the HTTP Cotent-Length
        // property. (See complete e-mail below for more on this.)

        FileInputStream fin = new FileInputStream(xmlFile2Send);

        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        // Copy the SOAP file to the open connection.
        copy(fin, bout);
        fin.close();

        byte[] b = bout.toByteArray();

        // Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type",
                "application/soap+xml; charset=UTF-8");
        httpConn.setRequestProperty("SOAPAction", soapAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);

        // Everything's set up; send the XML that was read in to b.
        OutputStream out = httpConn.getOutputStream();
        out.write(b);
        out.close();

        // Read the response and write it to standard out.

        InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory
                .newInstance();
        javax.xml.parsers.DocumentBuilder db;
        db = factory.newDocumentBuilder();

        org.xml.sax.InputSource inStream = new org.xml.sax.InputSource();
        inStream.setCharacterStream(new java.io.StringReader(response
                .toString()));

        System.out.println(response.toString());

        org.w3c.dom.Document doc = db.parse(inStream);
        doc.getDocumentElement().normalize();

        org.w3c.dom.NodeList uuidList = doc.getElementsByTagName("uuid");

        // org.w3c.dom.NodeList uuidList =
        // XPathAPI.selectNodeList(doc.getFirstChild(), "uuid");

        for (int k = 0; k < uuidList.getLength(); k++) {
            org.w3c.dom.Node key = uuidList.item(k);

            if (key.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {

                org.w3c.dom.Element valueElement = (org.w3c.dom.Element) key;
                org.w3c.dom.NodeList fstNmElmntLst = valueElement
                        .getElementsByTagName("value");
                org.w3c.dom.Element fstNmElmnt = (org.w3c.dom.Element) fstNmElmntLst
                        .item(0);
                org.w3c.dom.NodeList fstNm = fstNmElmnt.getChildNodes();
                System.out.println("SLATemplateID : "
                        + ((org.w3c.dom.Node) fstNm.item(0)).getNodeValue());
            }
        }

    }

    /**
     * copy method.
     * 
     * @param in
     *            streamInput
     * @param out
     *            streamOutput
     * @throws IOException
     *             exception
     */
    public static void copy(final InputStream in, final OutputStream out)
            throws IOException {

        // do not allow other threads to read from the
        // input or write to the output while copying is
        // taking place

        synchronized (in) {
            synchronized (out) {

                byte[] buffer = new byte[256];
                while (true) {
                    int bytesRead = in.read(buffer);
                    if (bytesRead == -1) {
                        break;
                    }
                    out.write(buffer, 0, bytesRead);
                }
            }
        }
    }
}
