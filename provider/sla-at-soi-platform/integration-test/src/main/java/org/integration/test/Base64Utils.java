/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.integration.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;

/**
 * Utility class for encode/decode.
 * 
 * @author khurshid
 * 
 */
public final class Base64Utils {
    /**
     * private constructor.
     */
    private Base64Utils() {
    }

    /**
     * encode.
     * 
     * @param obj
     *            object
     * @return result string
     */
    public static String encode(final Object obj) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(obj);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * decode.
     * 
     * @param response
     *            string
     * @return result object
     */
    public static Object decode(final String response) {
        Object result = null;
        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64
                    .decodeBase64(response.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            result = is.readObject();
            is.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
