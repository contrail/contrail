/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * BusinessManager_TrackStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */
package org.slasoi.businessmanager.track.types;

/*
 *  BusinessManager_TrackStub java implementation
 */

public class BusinessManager_TrackStub extends org.apache.axis2.client.Stub {
    protected org.apache.axis2.description.AxisOperation[] _operations;

    // hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
    private java.util.HashMap faultMessageMap = new java.util.HashMap();

    private static int counter = 0;

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }
        counter = counter + 1;
        return java.lang.Long.toString(System.currentTimeMillis()) + "_"
                + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {

        // creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService(
                "BusinessManager_Track" + getUniqueSuffix());
        addAnonymousOperations();

        // creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[2];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org", "trackEvent"));
        _service.addOperation(__operation);

        _operations[0] = __operation;

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "getCustomerPurchaseAuth"));
        _service.addOperation(__operation);

        _operations[1] = __operation;

    }

    // populates the faults
    private void populateFaults() {

        faultExceptionNameMap
                .put(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "CustomerNotFoundException"),
                        "org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException");
        faultExceptionClassNameMap
                .put(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "CustomerNotFoundException"),
                        "org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException");
        faultMessageMap
                .put(
                        new javax.xml.namespace.QName(
                                "http://types.track.businessManager.slasoi.org",
                                "CustomerNotFoundException"),
                        "org.slasoi.businessmanager.track.types.BusinessManager_TrackStub$CustomerNotFoundExceptionE");

        faultExceptionNameMap
                .put(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "ProductNotFoundException"),
                        "org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException");
        faultExceptionClassNameMap
                .put(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "ProductNotFoundException"),
                        "org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException");
        faultMessageMap
                .put(
                        new javax.xml.namespace.QName(
                                "http://types.track.businessManager.slasoi.org",
                                "ProductNotFoundException"),
                        "org.slasoi.businessmanager.track.types.BusinessManager_TrackStub$ProductNotFoundExceptionE");

    }

    /**
     *Constructor that takes in a configContext
     */

    public BusinessManager_TrackStub(
            org.apache.axis2.context.ConfigurationContext configurationContext,
            java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
     * Constructor that takes in a configContext and useseperate listner
     */
    public BusinessManager_TrackStub(
            org.apache.axis2.context.ConfigurationContext configurationContext,
            java.lang.String targetEndpoint, boolean useSeparateListener)
            throws org.apache.axis2.AxisFault {
        // To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(
                configurationContext, _service);

        _serviceClient.getOptions().setTo(
                new org.apache.axis2.addressing.EndpointReference(
                        targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

        // Set the soap version
        _serviceClient
                .getOptions()
                .setSoapVersionURI(
                        org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);

    }

    /**
     * Default Constructor
     */
    public BusinessManager_TrackStub(
            org.apache.axis2.context.ConfigurationContext configurationContext)
            throws org.apache.axis2.AxisFault {

        this(configurationContext,
                "http://localhost:8080/services/BusinessManager_Track.TrackHttpSoap12Endpoint/");

    }

    /**
     * Default Constructor
     */
    public BusinessManager_TrackStub() throws org.apache.axis2.AxisFault {

        this(
                "http://localhost:8080/services/BusinessManager_Track.TrackHttpSoap12Endpoint/");

    }

    /**
     * Constructor taking the target endpoint
     */
    public BusinessManager_TrackStub(java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    /**
     * Auto generated method signature
     * 
     * @see org.slasoi.businessmanager.track.types.BusinessManager_Track#trackEvent
     * @param trackEvent0
     */

    public org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse trackEvent(

            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent trackEvent0)

    throws java.rmi.RemoteException

    {
        org.apache.axis2.context.MessageContext _messageContext = null;
        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient
                    .createClient(_operations[0].getName());
            _operationClient.getOptions().setAction("urn:trackEvent");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(
                    true);

            addPropertyToOperationClient(
                    _operationClient,
                    org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                    "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                    .getSoapVersionURI()), trackEvent0,
                    optimizeContent(new javax.xml.namespace.QName(
                            "http://types.track.businessManager.slasoi.org",
                            "trackEvent")));

            // adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            // execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient
                    .getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext
                    .getEnvelope();

            java.lang.Object object = fromOM(
                    _returnEnv.getBody().getFirstElement(),
                    org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse) object;

        } catch (org.apache.axis2.AxisFault f) {

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(faultElt.getQName())) {
                    // make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                .get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class
                                .forName(exceptionClassName);
                        java.lang.Exception ex = (java.lang.Exception) exceptionClass
                                .newInstance();
                        // message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap
                                .get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class
                                .forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod(
                                "setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            _messageContext.getTransportOut().getSender().cleanup(
                    _messageContext);
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     * 
     * @see org.slasoi.businessmanager.track.types.BusinessManager_Track#starttrackEvent
     * @param trackEvent0
     */
    public void starttrackEvent(

            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent trackEvent0,

            final org.slasoi.businessmanager.track.types.BusinessManager_TrackCallbackHandler callback)

    throws java.rmi.RemoteException {

        org.apache.axis2.client.OperationClient _operationClient = _serviceClient
                .createClient(_operations[0].getName());
        _operationClient.getOptions().setAction("urn:trackEvent");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(
                _operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        // Style is Doc.

        env = toEnvelope(getFactory(_operationClient.getOptions()
                .getSoapVersionURI()), trackEvent0,
                optimizeContent(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "trackEvent")));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient
                .setCallback(new org.apache.axis2.client.async.AxisCallback() {
                    public void onMessage(
                            org.apache.axis2.context.MessageContext resultContext) {
                        try {
                            org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext
                                    .getEnvelope();

                            java.lang.Object object = fromOM(
                                    resultEnv.getBody().getFirstElement(),
                                    org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse.class,
                                    getEnvelopeNamespaces(resultEnv));
                            callback
                                    .receiveResulttrackEvent((org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse) object);

                        } catch (org.apache.axis2.AxisFault e) {
                            callback.receiveErrortrackEvent(e);
                        }
                    }

                    public void onError(java.lang.Exception error) {
                        if (error instanceof org.apache.axis2.AxisFault) {
                            org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                            org.apache.axiom.om.OMElement faultElt = f
                                    .getDetail();
                            if (faultElt != null) {
                                if (faultExceptionNameMap.containsKey(faultElt
                                        .getQName())) {
                                    // make the fault by reflection
                                    try {
                                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                                .get(faultElt.getQName());
                                        java.lang.Class exceptionClass = java.lang.Class
                                                .forName(exceptionClassName);
                                        java.lang.Exception ex = (java.lang.Exception) exceptionClass
                                                .newInstance();
                                        // message class
                                        java.lang.String messageClassName = (java.lang.String) faultMessageMap
                                                .get(faultElt.getQName());
                                        java.lang.Class messageClass = java.lang.Class
                                                .forName(messageClassName);
                                        java.lang.Object messageObject = fromOM(
                                                faultElt, messageClass, null);
                                        java.lang.reflect.Method m = exceptionClass
                                                .getMethod(
                                                        "setFaultMessage",
                                                        new java.lang.Class[] { messageClass });
                                        m
                                                .invoke(
                                                        ex,
                                                        new java.lang.Object[] { messageObject });

                                        callback
                                                .receiveErrortrackEvent(new java.rmi.RemoteException(
                                                        ex.getMessage(), ex));
                                    } catch (java.lang.ClassCastException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (java.lang.ClassNotFoundException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (java.lang.NoSuchMethodException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (java.lang.reflect.InvocationTargetException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (java.lang.IllegalAccessException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (java.lang.InstantiationException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    } catch (org.apache.axis2.AxisFault e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback.receiveErrortrackEvent(f);
                                    }
                                } else {
                                    callback.receiveErrortrackEvent(f);
                                }
                            } else {
                                callback.receiveErrortrackEvent(f);
                            }
                        } else {
                            callback.receiveErrortrackEvent(error);
                        }
                    }

                    public void onFault(
                            org.apache.axis2.context.MessageContext faultContext) {
                        org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils
                                .getInboundFaultFromMessageContext(faultContext);
                        onError(fault);
                    }

                    public void onComplete() {
                        try {
                            _messageContext.getTransportOut().getSender()
                                    .cleanup(_messageContext);
                        } catch (org.apache.axis2.AxisFault axisFault) {
                            callback.receiveErrortrackEvent(axisFault);
                        }
                    }
                });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if (_operations[0].getMessageReceiver() == null
                && _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[0].setMessageReceiver(_callbackReceiver);
        }

        // execute the operation client
        _operationClient.execute(false);

    }

    /**
     * Auto generated method signature
     * 
     * @see org.slasoi.businessmanager.track.types.BusinessManager_Track#getCustomerPurchaseAuth
     * @param getCustomerPurchaseAuth2
     * 
     * @throws org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException
     *             :
     * @throws org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException
     *             :
     */

    public org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse getCustomerPurchaseAuth(

            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth getCustomerPurchaseAuth2)

            throws java.rmi.RemoteException

            ,
            org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException,
            org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException {
        org.apache.axis2.context.MessageContext _messageContext = null;
        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient
                    .createClient(_operations[1].getName());
            _operationClient.getOptions().setAction(
                    "urn:getCustomerPurchaseAuth");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(
                    true);

            addPropertyToOperationClient(
                    _operationClient,
                    org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                    "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                    .getSoapVersionURI()), getCustomerPurchaseAuth2,
                    optimizeContent(new javax.xml.namespace.QName(
                            "http://types.track.businessManager.slasoi.org",
                            "getCustomerPurchaseAuth")));

            // adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            // execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient
                    .getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext
                    .getEnvelope();

            java.lang.Object object = fromOM(
                    _returnEnv.getBody().getFirstElement(),
                    org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse) object;

        } catch (org.apache.axis2.AxisFault f) {

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(faultElt.getQName())) {
                    // make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                .get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class
                                .forName(exceptionClassName);
                        java.lang.Exception ex = (java.lang.Exception) exceptionClass
                                .newInstance();
                        // message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap
                                .get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class
                                .forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod(
                                "setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        if (ex instanceof org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException) {
                            throw (org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException) ex;
                        }

                        if (ex instanceof org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException) {
                            throw (org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException) ex;
                        }

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original
                        // Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            _messageContext.getTransportOut().getSender().cleanup(
                    _messageContext);
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     * 
     * @see org.slasoi.businessmanager.track.types.BusinessManager_Track#startgetCustomerPurchaseAuth
     * @param getCustomerPurchaseAuth2
     */
    public void startgetCustomerPurchaseAuth(

            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth getCustomerPurchaseAuth2,

            final org.slasoi.businessmanager.track.types.BusinessManager_TrackCallbackHandler callback)

    throws java.rmi.RemoteException {

        org.apache.axis2.client.OperationClient _operationClient = _serviceClient
                .createClient(_operations[1].getName());
        _operationClient.getOptions().setAction("urn:getCustomerPurchaseAuth");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(
                _operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        // Style is Doc.

        env = toEnvelope(getFactory(_operationClient.getOptions()
                .getSoapVersionURI()), getCustomerPurchaseAuth2,
                optimizeContent(new javax.xml.namespace.QName(
                        "http://types.track.businessManager.slasoi.org",
                        "getCustomerPurchaseAuth")));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient
                .setCallback(new org.apache.axis2.client.async.AxisCallback() {
                    public void onMessage(
                            org.apache.axis2.context.MessageContext resultContext) {
                        try {
                            org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext
                                    .getEnvelope();

                            java.lang.Object object = fromOM(
                                    resultEnv.getBody().getFirstElement(),
                                    org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse.class,
                                    getEnvelopeNamespaces(resultEnv));
                            callback
                                    .receiveResultgetCustomerPurchaseAuth((org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse) object);

                        } catch (org.apache.axis2.AxisFault e) {
                            callback.receiveErrorgetCustomerPurchaseAuth(e);
                        }
                    }

                    public void onError(java.lang.Exception error) {
                        if (error instanceof org.apache.axis2.AxisFault) {
                            org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                            org.apache.axiom.om.OMElement faultElt = f
                                    .getDetail();
                            if (faultElt != null) {
                                if (faultExceptionNameMap.containsKey(faultElt
                                        .getQName())) {
                                    // make the fault by reflection
                                    try {
                                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                                .get(faultElt.getQName());
                                        java.lang.Class exceptionClass = java.lang.Class
                                                .forName(exceptionClassName);
                                        java.lang.Exception ex = (java.lang.Exception) exceptionClass
                                                .newInstance();
                                        // message class
                                        java.lang.String messageClassName = (java.lang.String) faultMessageMap
                                                .get(faultElt.getQName());
                                        java.lang.Class messageClass = java.lang.Class
                                                .forName(messageClassName);
                                        java.lang.Object messageObject = fromOM(
                                                faultElt, messageClass, null);
                                        java.lang.reflect.Method m = exceptionClass
                                                .getMethod(
                                                        "setFaultMessage",
                                                        new java.lang.Class[] { messageClass });
                                        m
                                                .invoke(
                                                        ex,
                                                        new java.lang.Object[] { messageObject });

                                        if (ex instanceof org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException) {
                                            callback
                                                    .receiveErrorgetCustomerPurchaseAuth((org.slasoi.businessmanager.track.types.CustomerNotFoundExceptionException) ex);
                                            return;
                                        }

                                        if (ex instanceof org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException) {
                                            callback
                                                    .receiveErrorgetCustomerPurchaseAuth((org.slasoi.businessmanager.track.types.ProductNotFoundExceptionException) ex);
                                            return;
                                        }

                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(new java.rmi.RemoteException(
                                                        ex.getMessage(), ex));
                                    } catch (java.lang.ClassCastException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (java.lang.ClassNotFoundException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (java.lang.NoSuchMethodException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (java.lang.reflect.InvocationTargetException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (java.lang.IllegalAccessException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (java.lang.InstantiationException e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    } catch (org.apache.axis2.AxisFault e) {
                                        // we cannot intantiate the class -
                                        // throw the original Axis fault
                                        callback
                                                .receiveErrorgetCustomerPurchaseAuth(f);
                                    }
                                } else {
                                    callback
                                            .receiveErrorgetCustomerPurchaseAuth(f);
                                }
                            } else {
                                callback.receiveErrorgetCustomerPurchaseAuth(f);
                            }
                        } else {
                            callback.receiveErrorgetCustomerPurchaseAuth(error);
                        }
                    }

                    public void onFault(
                            org.apache.axis2.context.MessageContext faultContext) {
                        org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils
                                .getInboundFaultFromMessageContext(faultContext);
                        onError(fault);
                    }

                    public void onComplete() {
                        try {
                            _messageContext.getTransportOut().getSender()
                                    .cleanup(_messageContext);
                        } catch (org.apache.axis2.AxisFault axisFault) {
                            callback
                                    .receiveErrorgetCustomerPurchaseAuth(axisFault);
                        }
                    }
                });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if (_operations[1].getMessageReceiver() == null
                && _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[1].setMessageReceiver(_callbackReceiver);
        }

        // execute the operation client
        _operationClient.execute(false);

    }

    /**
     * A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(
            org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator
                    .next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private javax.xml.namespace.QName[] opNameArray = null;

    private boolean optimizeContent(javax.xml.namespace.QName opName) {

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }
        return false;
    }

    // http://localhost:8080axis2/services/BusinessManager_Track.TrackHttpSoap12Endpoint/
    public static class ProductNotFoundExceptionE implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "ProductNotFoundException", "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for ProductNotFoundException
         */

        protected ProductNotFoundException localProductNotFoundException;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localProductNotFoundExceptionTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return ProductNotFoundException
         */
        public ProductNotFoundException getProductNotFoundException() {
            return localProductNotFoundException;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ProductNotFoundException
         */
        public void setProductNotFoundException(ProductNotFoundException param) {

            if (param != null) {
                // update the setting tracker
                localProductNotFoundExceptionTracker = true;
            } else {
                localProductNotFoundExceptionTracker = true;

            }

            this.localProductNotFoundException = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    ProductNotFoundExceptionE.this.serialize(MY_QNAME, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":ProductNotFoundException", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "ProductNotFoundException", xmlWriter);
                }

            }
            if (localProductNotFoundExceptionTracker) {
                if (localProductNotFoundException == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "ProductNotFoundException", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "ProductNotFoundException");
                        }

                    } else {
                        xmlWriter.writeStartElement("ProductNotFoundException");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    localProductNotFoundException.serialize(
                            new javax.xml.namespace.QName("",
                                    "ProductNotFoundException"), factory,
                            xmlWriter);
                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localProductNotFoundExceptionTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "ProductNotFoundException"));

                elementList.add(localProductNotFoundException == null ? null
                        : localProductNotFoundException);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static ProductNotFoundExceptionE parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ProductNotFoundExceptionE object = new ProductNotFoundExceptionE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"ProductNotFoundException".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (ProductNotFoundExceptionE) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "ProductNotFoundException").equals(reader
                                    .getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.setProductNotFoundException(null);
                            reader.next();

                            reader.next();

                        } else {

                            object
                                    .setProductNotFoundException(ProductNotFoundException.Factory
                                            .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class CustomerNotFoundException extends Exception implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * CustomerNotFoundException Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    CustomerNotFoundException.this.serialize(parentQName,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://types.track.businessManager.slasoi.org/xsd");
            if ((namespacePrefix != null)
                    && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":CustomerNotFoundException",
                        xmlWriter);
            } else {
                writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "CustomerNotFoundException", xmlWriter);
            }

            if (localExceptionTracker) {

                if (localException != null) {
                    if (localException instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localException)
                                .serialize(new javax.xml.namespace.QName("",
                                        "Exception"), factory, xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "Exception", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "Exception");
                            }

                        } else {
                            xmlWriter.writeStartElement("Exception");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localException, xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "Exception",
                                    namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter
                                    .writeStartElement(namespace2, "Exception");
                        }

                    } else {
                        xmlWriter.writeStartElement("Exception");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName(
                    "http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList.add(new javax.xml.namespace.QName(
                    "http://types.track.businessManager.slasoi.org/xsd",
                    "CustomerNotFoundException"));
            if (localExceptionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "Exception"));

                elementList.add(localException == null ? null : localException);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static CustomerNotFoundException parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                CustomerNotFoundException object = new CustomerNotFoundException();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"CustomerNotFoundException".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (CustomerNotFoundException) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "Exception")
                                    .equals(reader.getName())) {

                        object
                                .setException(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class GetCustomerPurchaseAuthResponse implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "getCustomerPurchaseAuthResponse", "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for _return
         */

        protected AuthorizationResultType local_return;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean local_returnTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return AuthorizationResultType
         */
        public AuthorizationResultType get_return() {
            return local_return;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            _return
         */
        public void set_return(AuthorizationResultType param) {

            if (param != null) {
                // update the setting tracker
                local_returnTracker = true;
            } else {
                local_returnTracker = true;

            }

            this.local_return = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetCustomerPurchaseAuthResponse.this.serialize(MY_QNAME,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":getCustomerPurchaseAuthResponse",
                            xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "getCustomerPurchaseAuthResponse",
                            xmlWriter);
                }

            }
            if (local_returnTracker) {
                if (local_return == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "return",
                                    namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2, "return");
                        }

                    } else {
                        xmlWriter.writeStartElement("return");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    local_return.serialize(new javax.xml.namespace.QName("",
                            "return"), factory, xmlWriter);
                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                elementList.add(local_return == null ? null : local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static GetCustomerPurchaseAuthResponse parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetCustomerPurchaseAuthResponse object = new GetCustomerPurchaseAuthResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"getCustomerPurchaseAuthResponse".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (GetCustomerPurchaseAuthResponse) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "return")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.set_return(null);
                            reader.next();

                            reader.next();

                        } else {

                            object.set_return(AuthorizationResultType.Factory
                                    .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class TrackEventResponse implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "trackEventResponse", "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for _return
         */

        protected int local_return;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean local_returnTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return int
         */
        public int get_return() {
            return local_return;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            _return
         */
        public void set_return(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                local_returnTracker = false;

            } else {
                local_returnTracker = true;
            }

            this.local_return = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    TrackEventResponse.this.serialize(MY_QNAME, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":trackEventResponse",
                            xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "trackEventResponse", xmlWriter);
                }

            }
            if (local_returnTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter
                                .writeStartElement(prefix, "return", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "return");
                    }

                } else {
                    xmlWriter.writeStartElement("return");
                }

                if (local_return == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException(
                            "return cannot be null!!");

                } else {
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(local_return));
                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                elementList
                        .add(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(local_return));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static TrackEventResponse parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                TrackEventResponse object = new TrackEventResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"trackEventResponse".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (TrackEventResponse) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "return")
                                    .equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object
                                .set_return(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.set_return(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class ViolationEventType implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * ViolationEventType Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for GuaranteeTerm
         */

        protected java.lang.String localGuaranteeTerm;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localGuaranteeTermTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getGuaranteeTerm() {
            return localGuaranteeTerm;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            GuaranteeTerm
         */
        public void setGuaranteeTerm(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localGuaranteeTermTracker = true;
            } else {
                localGuaranteeTermTracker = true;

            }

            this.localGuaranteeTerm = param;

        }

        /**
         * field for Parameters
         */

        protected java.lang.Object localParameters;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localParametersTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.Object
         */
        public java.lang.Object getParameters() {
            return localParameters;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            Parameters
         */
        public void setParameters(java.lang.Object param) {

            if (param != null) {
                // update the setting tracker
                localParametersTracker = true;
            } else {
                localParametersTracker = true;

            }

            this.localParameters = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    ViolationEventType.this.serialize(parentQName, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org/xsd");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":ViolationEventType",
                            xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "ViolationEventType", xmlWriter);
                }

            }
            if (localGuaranteeTermTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "guaranteeTerm",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "guaranteeTerm");
                    }

                } else {
                    xmlWriter.writeStartElement("guaranteeTerm");
                }

                if (localGuaranteeTerm == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localGuaranteeTerm);

                }

                xmlWriter.writeEndElement();
            }
            if (localParametersTracker) {

                if (localParameters != null) {
                    if (localParameters instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localParameters)
                                .serialize(new javax.xml.namespace.QName("",
                                        "parameters"), factory, xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "parameters", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "parameters");
                            }

                        } else {
                            xmlWriter.writeStartElement("parameters");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localParameters, xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "parameters",
                                    namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "parameters");
                        }

                    } else {
                        xmlWriter.writeStartElement("parameters");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localGuaranteeTermTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "guaranteeTerm"));

                elementList.add(localGuaranteeTerm == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localGuaranteeTerm));
            }
            if (localParametersTracker) {
                elementList
                        .add(new javax.xml.namespace.QName("", "parameters"));

                elementList.add(localParameters == null ? null
                        : localParameters);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static ViolationEventType parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ViolationEventType object = new ViolationEventType();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"ViolationEventType".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (ViolationEventType) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "guaranteeTerm").equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setGuaranteeTerm(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "parameters")
                                    .equals(reader.getName())) {

                        object
                                .setParameters(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class RecoveryEventType implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * RecoveryEventType Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for CorrectiveActions
         */

        protected java.lang.Object localCorrectiveActions;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localCorrectiveActionsTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.Object
         */
        public java.lang.Object getCorrectiveActions() {
            return localCorrectiveActions;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            CorrectiveActions
         */
        public void setCorrectiveActions(java.lang.Object param) {

            if (param != null) {
                // update the setting tracker
                localCorrectiveActionsTracker = true;
            } else {
                localCorrectiveActionsTracker = true;

            }

            this.localCorrectiveActions = param;

        }

        /**
         * field for ViolationID
         */

        protected java.lang.String localViolationID;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localViolationIDTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getViolationID() {
            return localViolationID;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ViolationID
         */
        public void setViolationID(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localViolationIDTracker = true;
            } else {
                localViolationIDTracker = true;

            }

            this.localViolationID = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    RecoveryEventType.this.serialize(parentQName, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org/xsd");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":RecoveryEventType",
                            xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "RecoveryEventType", xmlWriter);
                }

            }
            if (localCorrectiveActionsTracker) {

                if (localCorrectiveActions != null) {
                    if (localCorrectiveActions instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localCorrectiveActions)
                                .serialize(new javax.xml.namespace.QName("",
                                        "correctiveActions"), factory,
                                        xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "correctiveActions", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "correctiveActions");
                            }

                        } else {
                            xmlWriter.writeStartElement("correctiveActions");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localCorrectiveActions,
                                        xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "correctiveActions", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "correctiveActions");
                        }

                    } else {
                        xmlWriter.writeStartElement("correctiveActions");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            if (localViolationIDTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "violationID",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "violationID");
                    }

                } else {
                    xmlWriter.writeStartElement("violationID");
                }

                if (localViolationID == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localViolationID);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localCorrectiveActionsTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "correctiveActions"));

                elementList.add(localCorrectiveActions == null ? null
                        : localCorrectiveActions);
            }
            if (localViolationIDTracker) {
                elementList
                        .add(new javax.xml.namespace.QName("", "violationID"));

                elementList.add(localViolationID == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localViolationID));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static RecoveryEventType parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                RecoveryEventType object = new RecoveryEventType();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"RecoveryEventType".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (RecoveryEventType) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "correctiveActions").equals(reader
                                    .getName())) {

                        object
                                .setCorrectiveActions(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "violationID")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setViolationID(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class GetCustomerPurchaseAuth implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "getCustomerPurchaseAuth", "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for ProductId
         */

        protected long localProductId;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localProductIdTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return long
         */
        public long getProductId() {
            return localProductId;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ProductId
         */
        public void setProductId(long param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Long.MIN_VALUE) {
                localProductIdTracker = false;

            } else {
                localProductIdTracker = true;
            }

            this.localProductId = param;

        }

        /**
         * field for CustomerId
         */

        protected long localCustomerId;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localCustomerIdTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return long
         */
        public long getCustomerId() {
            return localCustomerId;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            CustomerId
         */
        public void setCustomerId(long param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Long.MIN_VALUE) {
                localCustomerIdTracker = false;

            } else {
                localCustomerIdTracker = true;
            }

            this.localCustomerId = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetCustomerPurchaseAuth.this.serialize(MY_QNAME, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":getCustomerPurchaseAuth", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "getCustomerPurchaseAuth", xmlWriter);
                }

            }
            if (localProductIdTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "productId",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "productId");
                    }

                } else {
                    xmlWriter.writeStartElement("productId");
                }

                if (localProductId == java.lang.Long.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException(
                            "productId cannot be null!!");

                } else {
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(localProductId));
                }

                xmlWriter.writeEndElement();
            }
            if (localCustomerIdTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "customerId",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "customerId");
                    }

                } else {
                    xmlWriter.writeStartElement("customerId");
                }

                if (localCustomerId == java.lang.Long.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException(
                            "customerId cannot be null!!");

                } else {
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(localCustomerId));
                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localProductIdTracker) {
                elementList.add(new javax.xml.namespace.QName("", "productId"));

                elementList
                        .add(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localProductId));
            }
            if (localCustomerIdTracker) {
                elementList
                        .add(new javax.xml.namespace.QName("", "customerId"));

                elementList
                        .add(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localCustomerId));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static GetCustomerPurchaseAuth parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetCustomerPurchaseAuth object = new GetCustomerPurchaseAuth();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"getCustomerPurchaseAuth".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (GetCustomerPurchaseAuth) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "productId")
                                    .equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object
                                .setProductId(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToLong(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setProductId(java.lang.Long.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "customerId")
                                    .equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object
                                .setCustomerId(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToLong(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setCustomerId(java.lang.Long.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class ExtensionMapper {

        public static java.lang.Object getTypeObject(
                java.lang.String namespaceURI, java.lang.String typeName,
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {

            if ("http://types.track.businessManager.slasoi.org"
                    .equals(namespaceURI)
                    && "Exception".equals(typeName)) {

                return Exception.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "CustomerNotFoundException".equals(typeName)) {

                return CustomerNotFoundException.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "AuthorizationResultType".equals(typeName)) {

                return AuthorizationResultType.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "ViolationEventType".equals(typeName)) {

                return ViolationEventType.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "AdjustmentNotificationType".equals(typeName)) {

                return AdjustmentNotificationType.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "RecoveryEventType".equals(typeName)) {

                return RecoveryEventType.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "NotificationType".equals(typeName)) {

                return NotificationType.Factory.parse(reader);

            }

            if ("http://types.track.businessManager.slasoi.org/xsd"
                    .equals(namespaceURI)
                    && "ProductNotFoundException".equals(typeName)) {

                return ProductNotFoundException.Factory.parse(reader);

            }

            throw new org.apache.axis2.databinding.ADBException(
                    "Unsupported type " + namespaceURI + " " + typeName);
        }

    }

    public static class Exception implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * Exception Namespace URI =
         * http://types.track.businessManager.slasoi.org Namespace Prefix = ns2
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for Exception
         */

        protected java.lang.Object localException;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localExceptionTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.Object
         */
        public java.lang.Object getException() {
            return localException;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            Exception
         */
        public void setException(java.lang.Object param) {

            if (param != null) {
                // update the setting tracker
                localExceptionTracker = true;
            } else {
                localExceptionTracker = true;

            }

            this.localException = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    Exception.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":Exception", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "Exception", xmlWriter);
                }

            }
            if (localExceptionTracker) {

                if (localException != null) {
                    if (localException instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localException)
                                .serialize(new javax.xml.namespace.QName("",
                                        "Exception"), factory, xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "Exception", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "Exception");
                            }

                        } else {
                            xmlWriter.writeStartElement("Exception");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localException, xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "Exception",
                                    namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter
                                    .writeStartElement(namespace2, "Exception");
                        }

                    } else {
                        xmlWriter.writeStartElement("Exception");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localExceptionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "Exception"));

                elementList.add(localException == null ? null : localException);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static Exception parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                Exception object = new Exception();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"Exception".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (Exception) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "Exception")
                                    .equals(reader.getName())) {

                        object
                                .setException(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class TrackEvent implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org", "trackEvent",
                "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for ViolationList This was an Array!
         */

        protected AdjustmentNotificationType[] localViolationList;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localViolationListTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return AdjustmentNotificationType[]
         */
        public AdjustmentNotificationType[] getViolationList() {
            return localViolationList;
        }

        /**
         * validate the array for ViolationList
         */
        protected void validateViolationList(AdjustmentNotificationType[] param) {

        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ViolationList
         */
        public void setViolationList(AdjustmentNotificationType[] param) {

            validateViolationList(param);

            if (param != null) {
                // update the setting tracker
                localViolationListTracker = true;
            } else {
                localViolationListTracker = true;

            }

            this.localViolationList = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * 
         * @param param
         *            AdjustmentNotificationType
         */
        public void addViolationList(AdjustmentNotificationType param) {
            if (localViolationList == null) {
                localViolationList = new AdjustmentNotificationType[] {};
            }

            // update the setting tracker
            localViolationListTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil
                    .toList(localViolationList);
            list.add(param);
            this.localViolationList = (AdjustmentNotificationType[]) list
                    .toArray(new AdjustmentNotificationType[list.size()]);

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    TrackEvent.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":trackEvent", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "trackEvent", xmlWriter);
                }

            }
            if (localViolationListTracker) {
                if (localViolationList != null) {
                    for (int i = 0; i < localViolationList.length; i++) {
                        if (localViolationList[i] != null) {
                            localViolationList[i].serialize(
                                    new javax.xml.namespace.QName("",
                                            "violationList"), factory,
                                    xmlWriter);
                        } else {

                            // write null attribute
                            java.lang.String namespace2 = "";
                            if (!namespace2.equals("")) {
                                java.lang.String prefix2 = xmlWriter
                                        .getPrefix(namespace2);

                                if (prefix2 == null) {
                                    prefix2 = generatePrefix(namespace2);

                                    xmlWriter.writeStartElement(prefix2,
                                            "violationList", namespace2);
                                    xmlWriter.writeNamespace(prefix2,
                                            namespace2);
                                    xmlWriter.setPrefix(prefix2, namespace2);

                                } else {
                                    xmlWriter.writeStartElement(namespace2,
                                            "violationList");
                                }

                            } else {
                                xmlWriter.writeStartElement("violationList");
                            }

                            // write the nil attribute
                            writeAttribute(
                                    "xsi",
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();

                        }

                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "violationList", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "violationList");
                        }

                    } else {
                        xmlWriter.writeStartElement("violationList");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localViolationListTracker) {
                if (localViolationList != null) {
                    for (int i = 0; i < localViolationList.length; i++) {

                        if (localViolationList[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("",
                                    "violationList"));
                            elementList.add(localViolationList[i]);
                        } else {

                            elementList.add(new javax.xml.namespace.QName("",
                                    "violationList"));
                            elementList.add(null);

                        }

                    }
                } else {

                    elementList.add(new javax.xml.namespace.QName("",
                            "violationList"));
                    elementList.add(localViolationList);

                }

            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static TrackEvent parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                TrackEvent object = new TrackEvent();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"trackEvent".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (TrackEvent) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "violationList").equals(reader.getName())) {

                        // Process the array and step past its final element's
                        // end.

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            list1.add(null);
                            reader.next();
                        } else {
                            list1.add(AdjustmentNotificationType.Factory
                                    .parse(reader));
                        }
                        // loop until we find a start element that is not part
                        // of this array
                        boolean loopDone1 = false;
                        while (!loopDone1) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement()
                                    && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement()) {
                                // two continuous end elements means we are
                                // exiting the xml structure
                                loopDone1 = true;
                            } else {
                                if (new javax.xml.namespace.QName("",
                                        "violationList").equals(reader
                                        .getName())) {

                                    nillableValue = reader
                                            .getAttributeValue(
                                                    "http://www.w3.org/2001/XMLSchema-instance",
                                                    "nil");
                                    if ("true".equals(nillableValue)
                                            || "1".equals(nillableValue)) {
                                        list1.add(null);
                                        reader.next();
                                    } else {
                                        list1
                                                .add(AdjustmentNotificationType.Factory
                                                        .parse(reader));
                                    }
                                } else {
                                    loopDone1 = true;
                                }
                            }
                        }
                        // call the converter utility to convert and set the
                        // array

                        object
                                .setViolationList((AdjustmentNotificationType[]) org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToArray(
                                                AdjustmentNotificationType.class,
                                                list1));

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class CustomerNotFoundExceptionE implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://types.track.businessManager.slasoi.org",
                "CustomerNotFoundException", "ns2");

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org")) {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for CustomerNotFoundException
         */

        protected CustomerNotFoundException localCustomerNotFoundException;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localCustomerNotFoundExceptionTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return CustomerNotFoundException
         */
        public CustomerNotFoundException getCustomerNotFoundException() {
            return localCustomerNotFoundException;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            CustomerNotFoundException
         */
        public void setCustomerNotFoundException(CustomerNotFoundException param) {

            if (param != null) {
                // update the setting tracker
                localCustomerNotFoundExceptionTracker = true;
            } else {
                localCustomerNotFoundExceptionTracker = true;

            }

            this.localCustomerNotFoundException = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    CustomerNotFoundExceptionE.this.serialize(MY_QNAME,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    MY_QNAME, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":CustomerNotFoundException", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "CustomerNotFoundException", xmlWriter);
                }

            }
            if (localCustomerNotFoundExceptionTracker) {
                if (localCustomerNotFoundException == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "CustomerNotFoundException", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "CustomerNotFoundException");
                        }

                    } else {
                        xmlWriter
                                .writeStartElement("CustomerNotFoundException");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    localCustomerNotFoundException.serialize(
                            new javax.xml.namespace.QName("",
                                    "CustomerNotFoundException"), factory,
                            xmlWriter);
                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localCustomerNotFoundExceptionTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "CustomerNotFoundException"));

                elementList.add(localCustomerNotFoundException == null ? null
                        : localCustomerNotFoundException);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static CustomerNotFoundExceptionE parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                CustomerNotFoundExceptionE object = new CustomerNotFoundExceptionE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"CustomerNotFoundException".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (CustomerNotFoundExceptionE) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "CustomerNotFoundException").equals(reader
                                    .getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.setCustomerNotFoundException(null);
                            reader.next();

                            reader.next();

                        } else {

                            object
                                    .setCustomerNotFoundException(CustomerNotFoundException.Factory
                                            .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class AuthorizationResultType implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * AuthorizationResultType Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for ResponseCode
         */

        protected int localResponseCode;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localResponseCodeTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return int
         */
        public int getResponseCode() {
            return localResponseCode;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ResponseCode
         */
        public void setResponseCode(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localResponseCodeTracker = false;

            } else {
                localResponseCodeTracker = true;
            }

            this.localResponseCode = param;

        }

        /**
         * field for ResponseMessage
         */

        protected java.lang.String localResponseMessage;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localResponseMessageTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getResponseMessage() {
            return localResponseMessage;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ResponseMessage
         */
        public void setResponseMessage(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localResponseMessageTracker = true;
            } else {
                localResponseMessageTracker = true;

            }

            this.localResponseMessage = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AuthorizationResultType.this.serialize(parentQName,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org/xsd");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":AuthorizationResultType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "AuthorizationResultType", xmlWriter);
                }

            }
            if (localResponseCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "responseCode",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "responseCode");
                    }

                } else {
                    xmlWriter.writeStartElement("responseCode");
                }

                if (localResponseCode == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException(
                            "responseCode cannot be null!!");

                } else {
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(localResponseCode));
                }

                xmlWriter.writeEndElement();
            }
            if (localResponseMessageTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "responseMessage",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace,
                                "responseMessage");
                    }

                } else {
                    xmlWriter.writeStartElement("responseMessage");
                }

                if (localResponseMessage == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localResponseMessage);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localResponseCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "responseCode"));

                elementList
                        .add(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localResponseCode));
            }
            if (localResponseMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "responseMessage"));

                elementList.add(localResponseMessage == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localResponseMessage));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static AuthorizationResultType parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AuthorizationResultType object = new AuthorizationResultType();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"AuthorizationResultType".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (AuthorizationResultType) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "responseCode")
                                    .equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object
                                .setResponseCode(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setResponseCode(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "responseMessage").equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setResponseMessage(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class AdjustmentNotificationType implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * AdjustmentNotificationType Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for Notification
         */

        protected NotificationType localNotification;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localNotificationTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return NotificationType
         */
        public NotificationType getNotification() {
            return localNotification;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            Notification
         */
        public void setNotification(NotificationType param) {

            if (param != null) {
                // update the setting tracker
                localNotificationTracker = true;
            } else {
                localNotificationTracker = true;

            }

            this.localNotification = param;

        }

        /**
         * field for NotificationId
         */

        protected java.lang.String localNotificationId;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localNotificationIdTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getNotificationId() {
            return localNotificationId;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            NotificationId
         */
        public void setNotificationId(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localNotificationIdTracker = true;
            } else {
                localNotificationIdTracker = true;

            }

            this.localNotificationId = param;

        }

        /**
         * field for Notifier
         */

        protected java.lang.String localNotifier;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localNotifierTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getNotifier() {
            return localNotifier;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            Notifier
         */
        public void setNotifier(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localNotifierTracker = true;
            } else {
                localNotifierTracker = true;

            }

            this.localNotifier = param;

        }

        /**
         * field for ReportTime
         */

        protected java.util.Date localReportTime;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localReportTimeTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.util.Date
         */
        public java.util.Date getReportTime() {
            return localReportTime;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ReportTime
         */
        public void setReportTime(java.util.Date param) {

            if (param != null) {
                // update the setting tracker
                localReportTimeTracker = true;
            } else {
                localReportTimeTracker = true;

            }

            this.localReportTime = param;

        }

        /**
         * field for SlaID
         */

        protected java.lang.String localSlaID;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localSlaIDTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getSlaID() {
            return localSlaID;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            SlaID
         */
        public void setSlaID(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localSlaIDTracker = true;
            } else {
                localSlaIDTracker = true;

            }

            this.localSlaID = param;

        }

        /**
         * field for Type
         */

        protected java.lang.String localType;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localTypeTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.String
         */
        public java.lang.String getType() {
            return localType;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            Type
         */
        public void setType(java.lang.String param) {

            if (param != null) {
                // update the setting tracker
                localTypeTracker = true;
            } else {
                localTypeTracker = true;

            }

            this.localType = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AdjustmentNotificationType.this.serialize(parentQName,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org/xsd");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix
                                    + ":AdjustmentNotificationType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "AdjustmentNotificationType", xmlWriter);
                }

            }
            if (localNotificationTracker) {
                if (localNotification == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "notification", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "notification");
                        }

                    } else {
                        xmlWriter.writeStartElement("notification");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    localNotification.serialize(new javax.xml.namespace.QName(
                            "", "notification"), factory, xmlWriter);
                }
            }
            if (localNotificationIdTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "notificationId",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter
                                .writeStartElement(namespace, "notificationId");
                    }

                } else {
                    xmlWriter.writeStartElement("notificationId");
                }

                if (localNotificationId == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localNotificationId);

                }

                xmlWriter.writeEndElement();
            }
            if (localNotifierTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "notifier",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "notifier");
                    }

                } else {
                    xmlWriter.writeStartElement("notifier");
                }

                if (localNotifier == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localNotifier);

                }

                xmlWriter.writeEndElement();
            }
            if (localReportTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "reportTime",
                                namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "reportTime");
                    }

                } else {
                    xmlWriter.writeStartElement("reportTime");
                }

                if (localReportTime == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(localReportTime));

                }

                xmlWriter.writeEndElement();
            }
            if (localSlaIDTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "slaID", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "slaID");
                    }

                } else {
                    xmlWriter.writeStartElement("slaID");
                }

                if (localSlaID == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localSlaID);

                }

                xmlWriter.writeEndElement();
            }
            if (localTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "type", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    } else {
                        xmlWriter.writeStartElement(namespace, "type");
                    }

                } else {
                    xmlWriter.writeStartElement("type");
                }

                if (localType == null) {
                    // write the nil attribute

                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);

                } else {

                    xmlWriter.writeCharacters(localType);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localNotificationTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "notification"));

                elementList.add(localNotification == null ? null
                        : localNotification);
            }
            if (localNotificationIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "notificationId"));

                elementList.add(localNotificationId == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localNotificationId));
            }
            if (localNotifierTracker) {
                elementList.add(new javax.xml.namespace.QName("", "notifier"));

                elementList.add(localNotifier == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localNotifier));
            }
            if (localReportTimeTracker) {
                elementList
                        .add(new javax.xml.namespace.QName("", "reportTime"));

                elementList.add(localReportTime == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localReportTime));
            }
            if (localSlaIDTracker) {
                elementList.add(new javax.xml.namespace.QName("", "slaID"));

                elementList.add(localSlaID == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localSlaID));
            }
            if (localTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "type"));

                elementList.add(localType == null ? null
                        : org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(localType));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static AdjustmentNotificationType parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AdjustmentNotificationType object = new AdjustmentNotificationType();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"AdjustmentNotificationType".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (AdjustmentNotificationType) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "notification")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.setNotification(null);
                            reader.next();

                            reader.next();

                        } else {

                            object.setNotification(NotificationType.Factory
                                    .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "notificationId").equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setNotificationId(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "notifier")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setNotifier(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "reportTime")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setReportTime(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToDate(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "slaID")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setSlaID(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "type")
                                    .equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if (!"true".equals(nillableValue)
                                && !"1".equals(nillableValue)) {

                            java.lang.String content = reader.getElementText();

                            object
                                    .setType(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(content));

                        } else {

                            reader.getElementText(); // throw away text nodes if
                                                     // any.
                        }

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class NotificationType implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * NotificationType Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * field for NotificationEvent
         */

        protected java.lang.Object localNotificationEvent;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localNotificationEventTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return java.lang.Object
         */
        public java.lang.Object getNotificationEvent() {
            return localNotificationEvent;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            NotificationEvent
         */
        public void setNotificationEvent(java.lang.Object param) {

            if (param != null) {
                // update the setting tracker
                localNotificationEventTracker = true;
            } else {
                localNotificationEventTracker = true;

            }

            this.localNotificationEvent = param;

        }

        /**
         * field for RecoveryEvent
         */

        protected RecoveryEventType localRecoveryEvent;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localRecoveryEventTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return RecoveryEventType
         */
        public RecoveryEventType getRecoveryEvent() {
            return localRecoveryEvent;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            RecoveryEvent
         */
        public void setRecoveryEvent(RecoveryEventType param) {

            if (param != null) {
                // update the setting tracker
                localRecoveryEventTracker = true;
            } else {
                localRecoveryEventTracker = true;

            }

            this.localRecoveryEvent = param;

        }

        /**
         * field for ViolationEvent
         */

        protected ViolationEventType localViolationEvent;

        /*
         * This tracker boolean wil be used to detect whether the user called
         * the set method for this attribute. It will be used to determine
         * whether to include this field in the serialized XML
         */
        protected boolean localViolationEventTracker = false;

        /**
         * Auto generated getter method
         * 
         * @return ViolationEventType
         */
        public ViolationEventType getViolationEvent() {
            return localViolationEvent;
        }

        /**
         * Auto generated setter method
         * 
         * @param param
         *            ViolationEvent
         */
        public void setViolationEvent(ViolationEventType param) {

            if (param != null) {
                // update the setting tracker
                localViolationEventTracker = true;
            } else {
                localViolationEventTracker = true;

            }

            this.localViolationEvent = param;

        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    NotificationType.this.serialize(parentQName, factory,
                            xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://types.track.businessManager.slasoi.org/xsd");
                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", namespacePrefix + ":NotificationType",
                            xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type", "NotificationType", xmlWriter);
                }

            }
            if (localNotificationEventTracker) {

                if (localNotificationEvent != null) {
                    if (localNotificationEvent instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localNotificationEvent)
                                .serialize(new javax.xml.namespace.QName("",
                                        "notificationEvent"), factory,
                                        xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "notificationEvent", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "notificationEvent");
                            }

                        } else {
                            xmlWriter.writeStartElement("notificationEvent");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localNotificationEvent,
                                        xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "notificationEvent", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "notificationEvent");
                        }

                    } else {
                        xmlWriter.writeStartElement("notificationEvent");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            if (localRecoveryEventTracker) {
                if (localRecoveryEvent == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "recoveryEvent", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "recoveryEvent");
                        }

                    } else {
                        xmlWriter.writeStartElement("recoveryEvent");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    localRecoveryEvent.serialize(new javax.xml.namespace.QName(
                            "", "recoveryEvent"), factory, xmlWriter);
                }
            }
            if (localViolationEventTracker) {
                if (localViolationEvent == null) {

                    java.lang.String namespace2 = "";

                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2,
                                    "violationEvent", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter.writeStartElement(namespace2,
                                    "violationEvent");
                        }

                    } else {
                        xmlWriter.writeStartElement("violationEvent");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();
                } else {
                    localViolationEvent
                            .serialize(new javax.xml.namespace.QName("",
                                    "violationEvent"), factory, xmlWriter);
                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localNotificationEventTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "notificationEvent"));

                elementList.add(localNotificationEvent == null ? null
                        : localNotificationEvent);
            }
            if (localRecoveryEventTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "recoveryEvent"));

                elementList.add(localRecoveryEvent == null ? null
                        : localRecoveryEvent);
            }
            if (localViolationEventTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "violationEvent"));

                elementList.add(localViolationEvent == null ? null
                        : localViolationEvent);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static NotificationType parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                NotificationType object = new NotificationType();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"NotificationType".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (NotificationType) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "notificationEvent").equals(reader
                                    .getName())) {

                        object
                                .setNotificationEvent(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "recoveryEvent").equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.setRecoveryEvent(null);
                            reader.next();

                            reader.next();

                        } else {

                            object.setRecoveryEvent(RecoveryEventType.Factory
                                    .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("",
                                    "violationEvent").equals(reader.getName())) {

                        nillableValue = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil");
                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            object.setViolationEvent(null);
                            reader.next();

                            reader.next();

                        } else {

                            object.setViolationEvent(ViolationEventType.Factory
                                    .parse(reader));

                            reader.next();
                        }
                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    public static class ProductNotFoundException extends Exception implements
            org.apache.axis2.databinding.ADBBean {
        /*
         * This type was generated from the piece of schema that had name =
         * ProductNotFoundException Namespace URI =
         * http://types.track.businessManager.slasoi.org/xsd Namespace Prefix =
         * ns1
         */

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace
                    .equals("http://types.track.businessManager.slasoi.org/xsd")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil
                    .getUniquePrefix();
        }

        /**
         * isReaderMTOMAware
         * 
         * @return true if the reader supports MTOM
         */
        public static boolean isReaderMTOMAware(
                javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE
                        .equals(reader
                                .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            } catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
         * 
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName) {

                public void serialize(
                        org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    ProductNotFoundException.this.serialize(parentQName,
                            factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
                    parentQName, factory, dataSource);

        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName
                            .getLocalPart());
                } else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName
                            .getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            } else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://types.track.businessManager.slasoi.org/xsd");
            if ((namespacePrefix != null)
                    && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":ProductNotFoundException",
                        xmlWriter);
            } else {
                writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "ProductNotFoundException", xmlWriter);
            }

            if (localExceptionTracker) {

                if (localException != null) {
                    if (localException instanceof org.apache.axis2.databinding.ADBBean) {
                        ((org.apache.axis2.databinding.ADBBean) localException)
                                .serialize(new javax.xml.namespace.QName("",
                                        "Exception"), factory, xmlWriter, true);
                    } else {
                        java.lang.String namespace2 = "";
                        if (!namespace2.equals("")) {
                            java.lang.String prefix2 = xmlWriter
                                    .getPrefix(namespace2);

                            if (prefix2 == null) {
                                prefix2 = generatePrefix(namespace2);

                                xmlWriter.writeStartElement(prefix2,
                                        "Exception", namespace2);
                                xmlWriter.writeNamespace(prefix2, namespace2);
                                xmlWriter.setPrefix(prefix2, namespace2);

                            } else {
                                xmlWriter.writeStartElement(namespace2,
                                        "Exception");
                            }

                        } else {
                            xmlWriter.writeStartElement("Exception");
                        }
                        org.apache.axis2.databinding.utils.ConverterUtil
                                .serializeAnyType(localException, xmlWriter);
                        xmlWriter.writeEndElement();
                    }
                } else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter
                                .getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "Exception",
                                    namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        } else {
                            xmlWriter
                                    .writeStartElement(namespace2, "Exception");
                        }

                    } else {
                        xmlWriter.writeStartElement("Exception");
                    }

                    // write the nil attribute
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "nil",
                            "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }

            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter
                    .getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix
                            + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter
                            .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qname));
                }

            } else {
                xmlWriter
                        .writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not
                // possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(
                                            org.apache.axis2.databinding.utils.ConverterUtil
                                                    .convertToString(qnames[i]));
                        } else {
                            stringToWrite
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil
                                            .convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite
                                .append(org.apache.axis2.databinding.utils.ConverterUtil
                                        .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil
                            .getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         * 
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
                javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName(
                    "http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList.add(new javax.xml.namespace.QName(
                    "http://types.track.businessManager.slasoi.org/xsd",
                    "ProductNotFoundException"));
            if (localExceptionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "Exception"));

                elementList.add(localException == null ? null : localException);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
                    qName, elementList.toArray(), attribList.toArray());

        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            public static ProductNotFoundException parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ProductNotFoundException object = new ProductNotFoundException();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader
                            .getAttributeValue(
                                    "http://www.w3.org/2001/XMLSchema-instance",
                                    "type") != null) {
                        java.lang.String fullTypeName = reader
                                .getAttributeValue(
                                        "http://www.w3.org/2001/XMLSchema-instance",
                                        "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName
                                    .substring(fullTypeName.indexOf(":") + 1);

                            if (!"ProductNotFoundException".equals(type)) {
                                // find namespace for the prefix
                                java.lang.String nsUri = reader
                                        .getNamespaceContext().getNamespaceURI(
                                                nsPrefix);
                                return (ProductNotFoundException) ExtensionMapper
                                        .getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ
                    // normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "Exception")
                                    .equals(reader.getName())) {

                        object
                                .setException(org.apache.axis2.databinding.utils.ConverterUtil
                                        .getAnyTypeObject(reader,
                                                ExtensionMapper.class));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a
                        // trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement "
                                        + reader.getLocalName());

                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }// end of factory class

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.CustomerNotFoundExceptionE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.CustomerNotFoundExceptionE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.ProductNotFoundExceptionE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.ProductNotFoundExceptionE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory
                                    .getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory,
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {

            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
                    .getDefaultEnvelope();
            emptyEnvelope
                    .getBody()
                    .addChild(
                            param
                                    .getOMElement(
                                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent.MY_QNAME,
                                            factory));
            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    /* methods to provide back word compatibility */

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory,
            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {

            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
                    .getDefaultEnvelope();
            emptyEnvelope
                    .getBody()
                    .addChild(
                            param
                                    .getOMElement(
                                            org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth.MY_QNAME,
                                            factory));
            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    /* methods to provide back word compatibility */

    /**
     * get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param,
            java.lang.Class type, java.util.Map extraNamespaces)
            throws org.apache.axis2.AxisFault {

        try {

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEvent.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.TrackEventResponse.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuth.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.GetCustomerPurchaseAuthResponse.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.CustomerNotFoundExceptionE.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.CustomerNotFoundExceptionE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.ProductNotFoundExceptionE.class
                    .equals(type)) {

                return org.slasoi.businessmanager.track.types.BusinessManager_TrackStub.ProductNotFoundExceptionE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }

}
