#######################################################################
########################## INTEGRATION ENVIRONMENT ####################
#######################################################################

 
In order to install the bundle on the local repository run the following command
	mvn -Dmaven.test.skip=true clean install

As evident this command will skip the testing workpsace launch and will 
create a bundle inside maven repo which can then be launched as bundle
inside OSGI environment (using Pax runner).

License command
mvn license:format -Drun-license=true -P license

	
 
