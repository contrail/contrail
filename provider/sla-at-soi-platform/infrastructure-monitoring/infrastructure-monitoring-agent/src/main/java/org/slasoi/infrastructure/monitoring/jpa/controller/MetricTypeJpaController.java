/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/MetricTypeJpaController.jav $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.IllegalOrphanException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.PreexistingEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class MetricTypeJpaController {

    public MetricTypeJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MetricType metricType) throws PreexistingEntityException, Exception {
        if (metricType.getMetricList() == null) {
            metricType.setMetricList(new ArrayList<Metric>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Metric> attachedMetricList = new ArrayList<Metric>();
            for (Metric metricListMetricToAttach : metricType.getMetricList()) {
                metricListMetricToAttach = em.getReference(metricListMetricToAttach.getClass(), metricListMetricToAttach.getMetricId());
                attachedMetricList.add(metricListMetricToAttach);
            }
            metricType.setMetricList(attachedMetricList);
            em.persist(metricType);
            for (Metric metricListMetric : metricType.getMetricList()) {
                MetricType oldMetricTypeOfMetricListMetric = metricListMetric.getMetricType();
                metricListMetric.setMetricType(metricType);
                metricListMetric = em.merge(metricListMetric);
                if (oldMetricTypeOfMetricListMetric != null) {
                    oldMetricTypeOfMetricListMetric.getMetricList().remove(metricListMetric);
                    oldMetricTypeOfMetricListMetric = em.merge(oldMetricTypeOfMetricListMetric);
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            if (findMetricType(metricType.getMetricTypeId()) != null) {
                throw new PreexistingEntityException("MetricType " + metricType + " already exists.", ex);
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MetricType metricType) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricType persistentMetricType = em.find(MetricType.class, metricType.getMetricTypeId());
            List<Metric> metricListOld = persistentMetricType.getMetricList();
            List<Metric> metricListNew = metricType.getMetricList();
            List<String> illegalOrphanMessages = null;
            for (Metric metricListOldMetric : metricListOld) {
                if (!metricListNew.contains(metricListOldMetric)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Metric " + metricListOldMetric + " since its metricType field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Metric> attachedMetricListNew = new ArrayList<Metric>();
            for (Metric metricListNewMetricToAttach : metricListNew) {
                metricListNewMetricToAttach = em.getReference(metricListNewMetricToAttach.getClass(), metricListNewMetricToAttach.getMetricId());
                attachedMetricListNew.add(metricListNewMetricToAttach);
            }
            metricListNew = attachedMetricListNew;
            metricType.setMetricList(metricListNew);
            metricType = em.merge(metricType);
            for (Metric metricListNewMetric : metricListNew) {
                if (!metricListOld.contains(metricListNewMetric)) {
                    MetricType oldMetricTypeOfMetricListNewMetric = metricListNewMetric.getMetricType();
                    metricListNewMetric.setMetricType(metricType);
                    metricListNewMetric = em.merge(metricListNewMetric);
                    if (oldMetricTypeOfMetricListNewMetric != null && !oldMetricTypeOfMetricListNewMetric.equals(metricType)) {
                        oldMetricTypeOfMetricListNewMetric.getMetricList().remove(metricListNewMetric);
                        oldMetricTypeOfMetricListNewMetric = em.merge(oldMetricTypeOfMetricListNewMetric);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = metricType.getMetricTypeId();
                if (findMetricType(id) == null) {
                    throw new NonexistentEntityException("The metricType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricType metricType;
            try {
                metricType = em.getReference(MetricType.class, id);
                metricType.getMetricTypeId();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The metricType with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Metric> metricListOrphanCheck = metricType.getMetricList();
            for (Metric metricListOrphanCheckMetric : metricListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MetricType (" + metricType + ") cannot be destroyed since the Metric " + metricListOrphanCheckMetric + " in its metricList field has a non-nullable metricType field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(metricType);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MetricType> findMetricTypeEntities() {
        return findMetricTypeEntities(true, -1, -1);
    }

    public List<MetricType> findMetricTypeEntities(int maxResults, int firstResult) {
        return findMetricTypeEntities(false, maxResults, firstResult);
    }

    private List<MetricType> findMetricTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MetricType.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public MetricType findMetricType(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MetricType.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getMetricTypeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MetricType> rt = cq.from(MetricType.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
