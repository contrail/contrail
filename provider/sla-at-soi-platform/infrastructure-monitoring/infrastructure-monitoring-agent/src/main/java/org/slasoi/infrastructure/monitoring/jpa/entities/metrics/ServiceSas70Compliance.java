/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2997 $
 * @lastrevision $Date: 2011-08-22 17:10:42 +0200 (pon, 22 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceSas70Complianc $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SAS70Compliance;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("SERVICE_SAS70_COMPLIANCE")
public class ServiceSas70Compliance extends Metric {
    private static final Logger log = Logger.getLogger(ServiceSas70Compliance.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric SERVICE_SAS70_COMPLIANCE of the service " + service.getServiceName());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        // service is SAS70 compliant if all virtual machines run on SAS70 compliant hosts
        SAS70Compliance sas70Compliance = SAS70Compliance.COMPLIANT;
        for (Vm vm : service.getVmList()) {
            // check if host is SAS70 compliant
            String hostFqdn = monitoringEngine.getVmHostMachine(vm.getFqdn());
            boolean isVmSas70Compliant = (Boolean) monitoringEngine.getHostMetricData(hostFqdn, RawMetric.SAS70_COMPLIANCE).getValue();
            if (!isVmSas70Compliant) {
                sas70Compliance = SAS70Compliance.NOT_COMPLIANT;
                break;
            }
        }
        log.trace(String.format("SERVICE_SAS70_COMPLIANCE(%s) = %s", service.getServiceName(), sas70Compliance));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, sas70Compliance, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, sas70Compliance, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            SAS70Compliance required = SAS70Compliance.fromString(this.getViolationThreshold());
            SAS70Compliance actual = SAS70Compliance.fromString(metricValue.getValue());
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (required == SAS70Compliance.COMPLIANT && actual == SAS70Compliance.NOT_COMPLIANT) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s: service is not SAS70 compliant.",
                        service.getServiceUrl()));
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public SAS70Compliance parseValue(String value) throws InvalidMetricValueException {
        return SAS70Compliance.fromString(value);
    }

    @Override
    public String formatValue(Object value) {
        return ((SAS70Compliance) value).toString();
    }

    @Override
    public String displayValue(String value) throws InvalidMetricValueException {
        SAS70Compliance enumValue = parseValue(value);
        return enumValue.getTitle();
    }

    @Override
    public SAS70Compliance getViolationThresholdValue() throws InvalidMetricValueException {
        return SAS70Compliance.fromString(this.getViolationThreshold());
    }

    @Override
    public String getViolationThresholdWithUnit() throws InvalidMetricValueException {
        return getViolationThresholdValue().getTitle();
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        if ((SAS70Compliance) getViolationThresholdValue() == SAS70Compliance.COMPLIANT) {
            return "If host is not SAS70 compliant.";
        }
        else {
            return "None, SAS70 compliance is not required.";
        }
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
