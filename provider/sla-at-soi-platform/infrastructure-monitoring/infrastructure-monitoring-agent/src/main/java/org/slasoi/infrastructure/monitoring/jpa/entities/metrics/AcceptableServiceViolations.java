/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2997 $
 * @lastrevision $Date: 2011-08-22 17:10:42 +0200 (pon, 22 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/AcceptableServiceViol $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.ViolationManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("ACCEPTABLE_SERVICE_VIOLATIONS")
public class AcceptableServiceViolations extends Metric {
    private static final Logger log = Logger.getLogger(AcceptableServiceViolations.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric ACCEPTABLE_SERVICE_VIOLATIONS of the service " + service.getServiceName());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        List<Integer> metricIdList = new ArrayList<Integer>();
        for (Metric metric : service.getMetricList()) {
            if (!metric.getMetricType().isCritical()) {
                metricIdList.add(metric.getMetricId());
            }
        }
        for (Vm vm : service.getVmList()) {
            for (Metric metric : vm.getMetricList()) {
                if (!metric.getMetricType().isCritical()) {
                    metricIdList.add(metric.getMetricId());
                }
            }
        }

        int violationsCounter;
        if (metricIdList.size() == 0) {
            violationsCounter = 0;
        }
        else {
            EntityManager em = ViolationManager.getInstance().getEntityManager();
            try {
                Date reportingPeriodStart = service.getReportingPeriod().getStartDate();
                Date reportingPeriodEnd = service.getReportingPeriod().getEndDate();
                Query query = em.createQuery("SELECT COUNT(v) FROM Violation v " +
                        "WHERE v.metric.metricId IN :metricIdList AND " +
                        "v.type = :type AND v.severity = :severity AND " +
                        "v.startTime > :periodStart AND v.startTime < :periodEnd ");
                query.setParameter("metricIdList", metricIdList);
                query.setParameter("type", ViolationType.VIOLATION);
                query.setParameter("severity", ViolationSeverity.PUNISHABLE);
                query.setParameter("periodStart", reportingPeriodStart);
                query.setParameter("periodEnd", reportingPeriodEnd);

                try {
                    violationsCounter = ((Long) query.getSingleResult()).intValue();
                }
                catch (NoResultException e) {
                    violationsCounter = 0;
                }
            }
            finally {
                em.close();
            }
        }

        log.trace(String.format("ACCEPTABLE_SERVICE_VIOLATIONS(%s) = %s", service.getServiceName(), violationsCounter));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(violationsCounter, metricsTimestamp);
        storeMetricHistoryValue(violationsCounter, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            int violationThreshold = Integer.parseInt(this.getViolationThreshold());
            int actual = metricValue.getIntValue();
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (actual > violationThreshold) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s: number of acceptable service violations is exceeded. Currently %d, acceptable is %d.",
                        service.getServiceUrl(), actual, violationThreshold));
            }
            else if (this.getWarningThreshold() != null) {
                int warningThreshold = Integer.parseInt(this.getWarningThreshold());
                if (actual > warningThreshold) {
                    slaCompliance = SLAComplianceEnum.WARNING;
                    log.warn(String.format("Warning at the service %s: number of acceptable service violations is nearing the violation threshold. Currently %d, acceptable is %d.",
                            service.getServiceUrl(), actual, violationThreshold));
                }
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public Integer parseValue(String value) {
        return Integer.valueOf(value);
    }

    @Override
    public String formatValue(Object value) {
        if (value == null) {
            return null;
        }
        else {
            Integer v = (Integer) value;
            return v.toString();
        }
    }

    @Override
    public Integer getViolationThresholdValue() {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            return Integer.parseInt(this.getViolationThreshold());
        }
    }

    @Override
    public String getViolationCondition() {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            return String.format("value > %d", getViolationThresholdValue());
        }
    }

    @Override
    public Integer getWarningThresholdValue() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            return Integer.parseInt(this.getWarningThreshold());
        }
    }

    @Override
    public String getWarningCondition() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            return String.format("%d < value <= %d", getWarningThresholdValue(), getViolationThresholdValue());
        }
    }

    @Override
    public Integer estimateWarningThreshold(Object violationThreshold) {
        if (violationThreshold != null) {
            int vt = Integer.parseInt(violationThreshold.toString());
            int wt = (int) (vt * 0.8);
            return wt;
        }
        else {
            return null;
        }
    }
}
