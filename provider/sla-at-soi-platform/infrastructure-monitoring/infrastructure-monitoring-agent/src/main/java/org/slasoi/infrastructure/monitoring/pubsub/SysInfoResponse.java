/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2818 $
 * @lastrevision $Date: 2011-07-26 08:32:55 +0200 (tor, 26 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/SysInfoResponse.java $
 */

package org.slasoi.infrastructure.monitoring.pubsub;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.DataType;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SysInfoResponse extends PubSubResponse {
    String entityType;
    String fqdn;
    Date metricsTimestamp;

    public SysInfoResponse() {
        super.setResponseType("SysInfoResponse");
    }

    public static SysInfoResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, SysInfoResponse.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public Date getMetricsTimestamp() {
        return metricsTimestamp;
    }

    public void setMetricsTimestamp(Date metricsTimestamp) {
        this.metricsTimestamp = metricsTimestamp;
    }

    public static class HostInfoResponse extends SysInfoResponse {
        HostInfo data;

        public HostInfo getData() {
            return data;
        }

        public void setData(HostInfo data) {
            this.data = data;
        }
    }

    public static class HostInfo {
        private static final String type = "host";
        private String fqdn;
        private String name;
        private String ip;
        private Map<RawMetric, MetricInfo> metrics;

        public HostInfo() {
            metrics = new HashMap<RawMetric, MetricInfo>();
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public Map<RawMetric, MetricInfo> getMetrics() {
            return metrics;
        }

        public void setMetrics(Map<RawMetric, MetricInfo> metrics) {
            this.metrics = metrics;
        }

        public void putMetric(MetricInfo MetricInfo) {
            metrics.put(MetricInfo.getMetric(), MetricInfo);
        }

        public void putMetric(MetricData metricData) {
            putMetric(new MetricInfo(metricData));
        }
    }

    public static class VmInfoResponse extends SysInfoResponse {
        VmInfo data;

        public VmInfo getData() {
            return data;
        }

        public void setData(VmInfo data) {
            this.data = data;
        }
    }

    public static class VmInfo {
        private static final String type = "vm";
        private String fqdn;
        private String name;
        private String ip;
        private String hostFqdn;
        private Map<RawMetric, MetricInfo> metrics;

        public VmInfo() {
            metrics = new HashMap<RawMetric, MetricInfo>();
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getHostFqdn() {
            return hostFqdn;
        }

        public void setHostFqdn(String hostFqdn) {
            this.hostFqdn = hostFqdn;
        }

        public Map<RawMetric, MetricInfo> getMetrics() {
            return metrics;
        }

        public void setMetrics(Map<RawMetric, MetricInfo> metrics) {
            this.metrics = metrics;
        }

        public void putMetric(MetricInfo MetricInfo) {
            metrics.put(MetricInfo.getMetric(), MetricInfo);
        }

        public void putMetric(MetricData metricData) {
            putMetric(new MetricInfo(metricData));
        }
    }


    public static class ClusterInfoResponse extends SysInfoResponse {
        ClusterInfo data;

        public ClusterInfo getData() {
            return data;
        }

        public void setData(ClusterInfo data) {
            this.data = data;
        }
    }

    public static class MetricInfo {
        private RawMetric metric;
        private String value;
        private DataType type;
        private String unit;
        private String title;
        private String description;

        public MetricInfo() {
        }

        public MetricInfo(MetricData metricData) {
            RawMetric rawMetric = metricData.getMetric();
            metric = rawMetric;
            value = metricData.getStringValue();
            type = metricData.getType();
            unit = metricData.getUnit();
            title = rawMetric.getMetricTitle();
            description = rawMetric.getDescription();
        }

        public RawMetric getMetric() {
            return metric;
        }

        public void setMetric(RawMetric metric) {
            this.metric = metric;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public DataType getType() {
            return type;
        }

        public void setType(DataType type) {
            this.type = type;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class ClusterInfo {
        private static final String type = "cluster";
        private String name;
        private String fqdn;
        private ClusterStats clusterStats;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public ClusterStats getClusterStats() {
            return clusterStats;
        }

        public void setClusterStats(ClusterStats clusterStats) {
            this.clusterStats = clusterStats;
        }
    }

    public static class ClusterStats {
        private int hostCount;
        private int vmCount;

        public int getHostCount() {
            return hostCount;
        }

        public void setHostCount(int hostCount) {
            this.hostCount = hostCount;
        }

        public int getVmCount() {
            return vmCount;
        }

        public void setVmCount(int vmCount) {
            this.vmCount = vmCount;
        }
    }
}
