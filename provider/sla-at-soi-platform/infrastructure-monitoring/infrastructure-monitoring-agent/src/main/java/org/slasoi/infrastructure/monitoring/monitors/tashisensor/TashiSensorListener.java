/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2599 $
 * @lastrevision $Date: 2011-07-11 14:08:02 +0200 (pon, 11 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/tashisensor/TashiSensorListener.j $
 */

package org.slasoi.infrastructure.monitoring.monitors.tashisensor;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.pointtopoint.Message;
import org.slasoi.common.messaging.pointtopoint.MessageEvent;
import org.slasoi.common.messaging.pointtopoint.MessageListener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class TashiSensorListener implements MessageListener {
    private static Logger log = Logger.getLogger(TashiSensorListener.class);
    TashiSensorHandler tashiSensorHandler;

    public TashiSensorListener(TashiSensorHandler tashiSensorHandler) {
        this.tashiSensorHandler = tashiSensorHandler;
    }

    public void processMessage(MessageEvent messageEvent) {
        Message message = messageEvent.getMessage();
        log.trace(String.format("Message received from %s:\n%s", message.getFrom(), message.getPayload()));
        Document xmlDoc;
        String messageType;
        try {
            xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(message.getPayload())));
            Element messageNode = xmlDoc.getDocumentElement();
            messageType = messageNode.getAttribute("type");
        }
        catch (Exception e) {
            log.warn(String.format("Message received from %s is invalid:\n%s", message.getFrom(), message.getPayload()));
            return;
        }

        try {
            if (messageType.equals("VmLayout")) {
                tashiSensorHandler.processVmLayoutMessage(xmlDoc);
            }
            else if (messageType.equals("auditRecord")) {
                tashiSensorHandler.processAuditRecordMessage(xmlDoc);
            }
            else if (messageType.equals("scheduler")) {
                tashiSensorHandler.processSchedulerMessage(xmlDoc);
            }
            else if (messageType.equals("ClusterConfiguration")) {
                tashiSensorHandler.processClusterConfigurationMessage(xmlDoc);
            }
            else {
                log.warn("Invalid message type: " + messageType);
            }
        }
        catch (Exception e) {
            log.error(String.format("Error encountered while processing message of type %s: %s", messageType, e.getMessage()));
        }
    }
}
