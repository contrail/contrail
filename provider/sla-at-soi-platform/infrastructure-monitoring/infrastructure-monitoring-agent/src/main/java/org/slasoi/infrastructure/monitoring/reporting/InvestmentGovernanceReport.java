/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2983 $
 * @lastrevision $Date: 2011-08-16 15:43:36 +0200 (tor, 16 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/reporting/InvestmentGovernanceReport.java $
 */

package org.slasoi.infrastructure.monitoring.reporting;

import com.lowagie.text.*;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.math.stat.regression.SimpleRegression;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistory;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class InvestmentGovernanceReport extends ReportTemplate {
    Document document;
    long MAX_PREDICTION_INTERVAL = 90 * 24 * 3600; // maximum prediction interval length in seconds

    public InvestmentGovernanceReport() {
    }

    @Override
    public ByteArrayOutputStream generate() throws Exception {
        document = new Document();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, os);
        pdfWriter.setStrictImageSequence(true);
        document.open();

        addTitlePage();
        addResourceUsageChartsPage();

        document.close();
        return os;
    }

    private void addTitlePage() throws DocumentException {
        Paragraph p1 = new Paragraph();
        p1.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(p1, 17);
        String title = "Investment Governance Report";
        p1.add(new Paragraph(title, titlePageTitleFont));

        addEmptyLine(p1, 2);
        // TODO get cluster name from monitoring engine
        String subtitle = String.format("Cluster '%s'", "tashi.xlab.tst");
        p1.add(new Paragraph(subtitle, titlePageSubtitleFont));

        document.add(p1);

        Paragraph p2 = new Paragraph();
        p2.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(p2, 10);
        p2.add(new Paragraph("Generated on: " + new Date()));
        document.add(p2);
        document.newPage();

        document.addTitle(title);
        document.addAuthor(InfrastructureMonitoringAgent.APPLICATION_NAME);
    }

    private void addResourceUsageChartsPage() throws Exception {
        Chapter chapter = new ChapterAutoNumber(new Paragraph("Resource Usage", chapterFont));

        byte[] imageBytes;
        Image image;
        Section section;

        // vCPU usage chart
        section = chapter.addSection(new Paragraph("Virtual CPUs", sectionFont));
        imageBytes = createResourceUsageChart(MetricTypeEnum.CLUSTER_VCPU_USAGE, 1, "vCPUs Usage", "vCPUs");
        image = Image.getInstance(imageBytes);
        section.add(image);

        // CPU speed usage chart
        section = chapter.addSection(new Paragraph("CPU Speed", sectionFont));
        imageBytes = createResourceUsageChart(MetricTypeEnum.CLUSTER_CPU_SPEED_USAGE, 1, "CPU Speed Usage",
                "CPU Speed [GHz]");
        image = Image.getInstance(imageBytes);
        section.add(image);

        // Provisioned memory usage chart
        section = chapter.addSection(new Paragraph("Provisioned Memory", sectionFont));
        imageBytes = createResourceUsageChart(MetricTypeEnum.CLUSTER_PROVISIONED_MEMORY_USAGE, 1.0 / 1024,
                "Provisioned Memory Usage", "Memory Size [GB]");
        image = Image.getInstance(imageBytes);
        section.add(image);

        // Actual memory usage chart
        section = chapter.addSection(new Paragraph("Actual Memory", sectionFont));
        imageBytes = createResourceUsageChart(MetricTypeEnum.CLUSTER_ACTUAL_MEMORY_USAGE, 1.0 / 1024,
                "Actual Memory Usage",
                "Memory Size [GB]");
        image = Image.getInstance(imageBytes);
        section.add(image);

        // Disk space usage
        section = chapter.addSection(new Paragraph("Disk Space", sectionFont));
        imageBytes = createResourceUsageChart(MetricTypeEnum.CLUSTER_DISK_SPACE_USAGE, 1, "Disk Space Usage",
                "Disk Space [GB]");
        image = Image.getInstance(imageBytes);
        section.add(image);

        document.add(chapter);
    }

    private byte[] createResourceUsageChart(MetricTypeEnum metricTypeEnum, double conversion, String chartTitle,
                                            String yAxisLabel) throws IOException {
        TimeSeriesCollection dataset = new TimeSeriesCollection();

        TimeSeries usageSeries = new TimeSeries("Usage");
        TimeSeries capacitySeries = new TimeSeries("Capacity");

        Metric metric = MetricManager.getInstance().findInfrastructureMetric(metricTypeEnum);
        List<MetricValueHistory> history = MetricValueHistoryManager.getInstance().getMvhValues(metric, null, null,
                false);

        SimpleRegression regression = new SimpleRegression();

        for (MetricValueHistory mvh : history) {
            if (mvh.getValue() != null) {
                String[] values = mvh.getValue().split("/");
                double used = Double.parseDouble(values[0]) * conversion;
                double capacity = Double.parseDouble(values[1]) * conversion;
                usageSeries.add(new FixedMillisecond(mvh.getStartTime()), used);
                usageSeries.add(new FixedMillisecond(new Date(mvh.getEndTime().getTime() - 1)), used);

                capacitySeries.add(new FixedMillisecond(mvh.getStartTime()), capacity);
                capacitySeries.add(new FixedMillisecond(new Date(mvh.getEndTime().getTime() - 1)), capacity);

                if (history.size() == 1) {
                    // if there is only one history value, add two points so that it is possible to calculate trend
                    regression.addData(mvh.getStartTime().getTime(), used);
                    regression.addData(mvh.getEndTime().getTime(), used);
                }
                else {
                    // there is more than one history value, use interval middle point for trend calculation
                    long periodMiddle = (mvh.getStartTime().getTime() + mvh.getEndTime().getTime()) / 2;
                    regression.addData(periodMiddle, used);
                }
            }
        }

        dataset.addSeries(usageSeries);
        dataset.addSeries(capacitySeries);

        if (!Double.isNaN(regression.getSlope())) {  // if it is possible to calculate trend
            MetricValueHistory firstMvh = history.get(0);
            MetricValueHistory lastMvh = history.get(history.size() - 1);
            Date historyStart = firstMvh.getStartTime();
            Date historyEnd = lastMvh.getEndTime();

            long historyLength = historyEnd.getTime() - historyStart.getTime();
            Date trendLineStart = historyStart;
            long predictionInterval = (long) (historyLength * 1.5);
            if (predictionInterval > MAX_PREDICTION_INTERVAL * 1000) {
                predictionInterval = MAX_PREDICTION_INTERVAL * 1000;
            }
            Date trendLineEnd = new Date(historyEnd.getTime() + predictionInterval);
            double trendLineStartValue = regression.predict(trendLineStart.getTime());
            double trendLineEndValue = regression.predict(trendLineEnd.getTime());

            TimeSeries trendSeries = new TimeSeries("Trend");
            trendSeries.add(new FixedMillisecond(trendLineStart), trendLineStartValue);
            trendSeries.add(new FixedMillisecond(trendLineEnd), trendLineEndValue);
            dataset.addSeries(trendSeries);

            // extend the capacity line
            TimeSeries extendedCapacitySeries = new TimeSeries("Extended Capacity");
            double lastCapacity = capacitySeries.getValue(capacitySeries.getItemCount() - 1).doubleValue();
            extendedCapacitySeries.add(new FixedMillisecond(historyEnd), lastCapacity);
            extendedCapacitySeries.add(new FixedMillisecond(trendLineEnd), lastCapacity);
            dataset.addSeries(extendedCapacitySeries);
        }

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                chartTitle,       // title
                "Time",           // x-axis label
                yAxisLabel,       // y-axis label
                dataset,          // data
                true,             // create legend?
                false,            // generate tooltips?
                false             // generate URLs?
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(chartBackgroundPaint);
        plot.setDomainGridlinePaint(Color.gray);
        plot.setRangeGridlinePaint(Color.gray);

        // set colors
        XYItemRenderer renderer = plot.getRenderer(0);
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesPaint(1, Color.RED);

        // set Y axis lower bound to 0
        ValueAxis rangeAxis = plot.getRangeAxis();
        rangeAxis.setLowerBound(0);

        if (!Double.isNaN(regression.getSlope())) {
            // set upper bound of domain axis (to show whole trend line)
            ValueAxis domainAxis = plot.getDomainAxis();
            double upperBound = dataset.getDomainUpperBound(true);
            domainAxis.setUpperBound(upperBound);

            // set trend line color
            renderer.setSeriesPaint(2, Color.CYAN);
            Stroke trendStroke = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f,
                    new float[]{6.0f, 6.0f}, 0.0f);
            renderer.setSeriesStroke(2, trendStroke);

            // draw extended part of capacity line dashed
            Stroke extCapacityStroke = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f,
                    new float[]{6.0f, 6.0f}, 0.0f);
            renderer.setSeriesStroke(3, extCapacityStroke);
            renderer.setSeriesVisibleInLegend(3, false);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ChartUtilities.writeChartAsPNG(baos, chart, chartWidth, chartHeight);
        return baos.toByteArray();
    }
}
