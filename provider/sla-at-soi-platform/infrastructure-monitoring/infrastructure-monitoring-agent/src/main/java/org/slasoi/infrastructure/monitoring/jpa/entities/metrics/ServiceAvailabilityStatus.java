/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceAvailabilitySt $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("SERVICE_AVAILABILITY_STATUS")
public class ServiceAvailabilityStatus extends Metric {
    private static final Logger log = Logger.getLogger(ServiceAvailabilityStatus.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric SERVICE_AVAILABILITY_STATUS of the service " + service.getServiceName());
        boolean availabilityStatus = true;
        for (Vm vm : service.getVmList()) {
            Metric metricVm = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_AVAILABILITY_STATUS, vm);
            MetricValue metricValueVm = metricVm.getMetricValue();
            if (metricValueVm.getBooleanValue() == false) {
                availabilityStatus = false;
                break;
            }
        }
        log.trace(String.format("SERVICE_AVAILABILITY_STATUS(%s) = %s", service.getServiceName(), availabilityStatus));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, availabilityStatus, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, availabilityStatus, metricsTimestamp);

        if (!availabilityStatus) {
            log.warn(String.format("The service '%s' is down.", service.getServiceName()));
        }
    }

    @Override
    public Object parseValue(String value) {
        return Boolean.valueOf(value);
    }

    @Override
    public String formatValue(Object value) {
        Boolean isAvailable = (Boolean) value;
        if (isAvailable == null) {
            return null;
        }
        else if (isAvailable) {
            return "UP";
        }
        else {
            return "DOWN";
        }
    }

    @Override
    public Object getViolationThresholdValue() {
        return null;
    }

    @Override
    public String getViolationCondition() {
        return null;
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
