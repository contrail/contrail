/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2073 $
 * @lastrevision $Date: 2011-06-07 14:38:47 +0200 (tor, 07 jun 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/AuditRecordManager.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.slasoi.infrastructure.monitoring.jpa.controller.AuditRecordJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class AuditRecordManager extends AuditRecordJpaController {
    private static AuditRecordManager instance;

    public static AuditRecordManager getInstance() {
        if (instance == null) {
            instance = new AuditRecordManager();
        }
        return instance;
    }

    public List<AuditRecord> getAuditRecords(Service service, String userId, String source, int maxNumberOfRecords) {
        List<String> fqdnList = new ArrayList<String>();
        for (Vm vm : service.getVmList()) {
            fqdnList.add(vm.getFqdn());
        }

        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT a FROM AuditRecord a WHERE a.fqdn IN :fqdnList AND a.userId = :userId AND a.source = :source ORDER BY a.timestamp DESC");
            query.setParameter("fqdnList", fqdnList);
            query.setParameter("userId", userId);
            query.setParameter("source", source);
            query.setMaxResults(maxNumberOfRecords);

            try {
                return (List<AuditRecord>) query.getResultList();
            }
            catch (NoResultException e) {
                return null;
            }
        }
        finally {
            em.close();
        }
    }

    public List<AuditRecord> getAuditRecords(String source, int maxNumberOfRecords) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT a FROM AuditRecord a WHERE a.source = :source ORDER BY a.timestamp DESC");
            query.setParameter("source", source);
            query.setMaxResults(maxNumberOfRecords);

            try {
                return (List<AuditRecord>) query.getResultList();
            }
            catch (NoResultException e) {
                return null;
            }
        }
        finally {
            em.close();
        }
    }
}
