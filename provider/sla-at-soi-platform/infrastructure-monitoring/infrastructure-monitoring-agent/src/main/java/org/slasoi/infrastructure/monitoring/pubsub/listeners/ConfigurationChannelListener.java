/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3034 $
 * @lastrevision $Date: 2011-08-29 14:30:56 +0200 (pon, 29 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/listeners/ConfigurationChannelListe $
 */

package org.slasoi.infrastructure.monitoring.pubsub.listeners;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.pubsub.ErrorResponse;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubRequest;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.handlers.ConfigureMonitoringHandler;
import org.slasoi.infrastructure.monitoring.pubsub.handlers.MonitoringFeaturesRequestHandler;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RemoveServiceRequest;
import org.slasoi.infrastructure.monitoring.utils.Utils;

import java.util.Date;

public class ConfigurationChannelListener implements org.slasoi.common.messaging.pubsub.MessageListener {
    private static Logger log = Logger.getLogger(ConfigurationChannelListener.class);

    public void processMessage(MessageEvent messageEvent) {
        String payload = null;
        PubSubRequest request = null;
        try {
            PubSubMessage message = messageEvent.getMessage();
            payload = message.getPayload();
            log.trace(String.format("New message arrived on the channel %s. Message payload: %s", message.getChannelName(), payload));
            try {
                request = PubSubRequest.fromJson(payload);
            }
            catch (Exception e) {
                throw new Exception(String.format("Invalid pubsub message received: %s", e.getMessage()));
            }

            log.trace("Request type: " + request.getRequestType());

            PubSubResponse response;
            String requestType = request.getRequestType();
            if (requestType.equals("RegisterServiceRequest")) {
                RegisterServiceRequest registerServiceRequest = RegisterServiceRequest.fromJson(payload);
                response = ConfigureMonitoringHandler.registerService(registerServiceRequest);
            }
            else if (requestType.equals("MonitoringFeaturesRequest")) {
                MonitoringFeaturesRequest monitoringFeaturesRequest = MonitoringFeaturesRequest.fromJson(payload);
                response = MonitoringFeaturesRequestHandler.processRequest(monitoringFeaturesRequest);
            }
            else if (requestType.equals("RemoveServiceRequest")) {
                RemoveServiceRequest removeSrvReq = RemoveServiceRequest.fromJson(payload);
                response = ConfigureMonitoringHandler.removeService(removeSrvReq);
            }
            else {
                return;
                //throw new Exception(String.format("Request type '%s' is not supported.", request.getRequestType()));
            }

            String responsePayload = response.toJson();
            publishMessage(responsePayload);

            log.trace(String.format("Request %s processed successfully.", request.getMessageId()));
            if (log.isTraceEnabled()) {
                log.trace("Response:\n" + responsePayload.substring(0, Math.min(responsePayload.length(), 1000)));
            }
        }
        catch (Exception e) {
            log.error(String.format("Error encountered while processing PubSub request %s: %s", Utils.shortenString(payload, 1000), e.getMessage()));
            ErrorResponse errorResponse = new ErrorResponse();
            String inReplyTo = "";
            if (request != null) {
                inReplyTo = request.getMessageId();
            }
            errorResponse.setInReplyTo(inReplyTo);
            errorResponse.setTimestamp(new Date());
            errorResponse.setExceptionMessage((e.getMessage() != null) ? e.getMessage() : e.toString());

            try {
                String responsePayload = errorResponse.toJson();
                publishMessage(responsePayload);
            }
            catch (Exception e1) {
                log.error("Error encountered while sending error response: " + e.getMessage());
            }
        }
    }

    private void publishMessage(String payload) throws MessagingException {
        PubSubManager pubSubManager = InfrastructureMonitoringAgent.getInstance().getPubSubManager();
        Channel monitoringDataRequestChannel = InfrastructureMonitoringAgent.getInstance().getConfigurationChannel();
        PubSubMessage pubSubMessage = new PubSubMessage(monitoringDataRequestChannel.getName(), payload);
        pubSubManager.publish(pubSubMessage);
    }
}
