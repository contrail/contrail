/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3049 $
 * @lastrevision $Date: 2011-09-05 10:29:54 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/ViolationManager.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.eclipse.persistence.queries.ScrollableCursor;
import org.slasoi.infrastructure.monitoring.jpa.controller.ViolationJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.*;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.*;

public class ViolationManager extends ViolationJpaController {
    private static ViolationManager instance;

    private ViolationManager() {

    }

    public static ViolationManager getInstance() {
        if (instance == null) {
            instance = new ViolationManager();
        }
        return instance;
    }

    public Violation getLastViolation(Metric metric, ViolationType type) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT v FROM Violation v WHERE v.metric = :metric AND v.type = :type AND v.startTime = (SELECT MAX(v1.startTime) FROM Violation v1 WHERE v1.metric = :metric)");
            query.setParameter("metric", metric);
            query.setParameter("type", type);
            Violation last = null;
            try {
                last = (Violation) query.getSingleResult();
            }
            catch (NoResultException e) {
                return null;
            }
            return last;
        }
        finally {
            em.close();
        }
    }

    public HashMap<Date, ViolationsCount> getServiceViolationsFrequency(Service service, Date fromDate, Date toDate) {
        List<Integer> metricIdList = new ArrayList<Integer>();

        HashMap<Date, ViolationsCount> violationsFreq = new HashMap<Date, ViolationsCount>();

        Calendar now = Calendar.getInstance();
        Calendar periodStart = Calendar.getInstance();
        periodStart.setTime(service.getStartDate());
        periodStart.set(Calendar.HOUR_OF_DAY, 0);
        periodStart.set(Calendar.MINUTE, 0);
        periodStart.set(Calendar.SECOND, 0);
        periodStart.set(Calendar.MILLISECOND, 0);

        while (periodStart.before(now)) {
            violationsFreq.put(periodStart.getTime(), new ViolationsCount());
            periodStart.add(Calendar.DAY_OF_MONTH, 1);
        }

        // get service metric ids
        for (Metric metric : service.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        // get resource metric ids
        for (Vm vm : service.getVmList()) {
            for (Metric metric : vm.getMetricList()) {
                metricIdList.add(metric.getMetricId());
            }
        }

        if (metricIdList.size() == 0) {
            return violationsFreq;
        }

        // get service and resource level violations
        EntityManager em = getEntityManager();
        try {
            Query query;
            if (fromDate != null && toDate != null) {
                query = em.createQuery("SELECT v FROM Violation v WHERE v.metric.metricId IN :metricIdList AND " +
                        "(v.endTime IS NULL OR v.endTime > :fromDate) AND v.startTime < :toDate");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
            }
            else {
                query = em.createQuery("SELECT v FROM Violation v WHERE v.metric.metricId IN :metricIdList");
            }
            query.setParameter("metricIdList", metricIdList);
            query.setHint("eclipselink.cursor.scrollable", true);

            ScrollableCursor scrollableCursor;
            try {
                scrollableCursor = (ScrollableCursor) query.getSingleResult();
            }
            catch (NoResultException e) {
                return violationsFreq;
            }

            while (scrollableCursor.hasNext()) {
                Violation violation = (Violation) scrollableCursor.next();

                Calendar violatedPeriodStart = Calendar.getInstance();
                violatedPeriodStart.setTime(violation.getStartTime());
                violatedPeriodStart.set(Calendar.HOUR_OF_DAY, 0);
                violatedPeriodStart.set(Calendar.MINUTE, 0);
                violatedPeriodStart.set(Calendar.SECOND, 0);
                violatedPeriodStart.set(Calendar.MILLISECOND, 0);

                Calendar violationEndDate = Calendar.getInstance();
                violationEndDate.setTime((violation.getEndTime() != null) ? violation.getEndTime() : new Date());

                while (violatedPeriodStart.before(violationEndDate)) {
                    ViolationsCount count = violationsFreq.get(violatedPeriodStart.getTime());
                    count.increase(violation.getType(), violation.getSeverity());
                    violatedPeriodStart.add(Calendar.DAY_OF_MONTH, 1);
                }
            }

            return violationsFreq;
        }
        finally {
            em.close();
        }
    }

    public List<Violation> getServiceOnlyViolations(Service service) {
        // get service metrics ids
        List<Integer> metricIdList = new ArrayList<Integer>();
        for (Metric metric : service.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        return getViolations(metricIdList);
    }

    public List<Violation> getVmViolations(Vm vm) {
        // get vm metrics ids
        List<Integer> metricIdList = new ArrayList<Integer>();
        for (Metric metric : vm.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        return getViolations(metricIdList);
    }

    public List<Violation> getServiceViolations(Service service) {
        return getServiceViolations(service, null, null);
    }

    public List<Violation> getServiceViolations(Service service, ViolationType type, ViolationSeverity severity) {
        List<Integer> metricIdList = new ArrayList<Integer>();

        // get service metric ids
        for (Metric metric : service.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        // get resource metric ids
        for (Vm vm : service.getVmList()) {
            for (Metric metric : vm.getMetricList()) {
                metricIdList.add(metric.getMetricId());
            }
        }

        if (type == null && severity == null) {
            return getViolations(metricIdList);
        }
        else {
            return getViolations(metricIdList, type, severity);
        }
    }

    private List<Violation> getViolations(List<Integer> metricIdList) {
        if (metricIdList.size() == 0) {
            return null;
        }

        // get service and resource level violations
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT v FROM Violation v WHERE v.metric.metricId IN :metricIdList ORDER BY v.startTime ASC");
            query.setParameter("metricIdList", metricIdList);

            try {
                return (List<Violation>) query.getResultList();
            }
            catch (NoResultException e) {
                return null;
            }
        }
        finally {
            em.close();
        }
    }

    private List<Violation> getViolations(List<Integer> metricIdList, ViolationType type, ViolationSeverity severity) {
        if (metricIdList.size() == 0) {
            return null;
        }

        // get service and resource level violations
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT v FROM Violation v WHERE v.metric.metricId IN :metricIdList AND " +
                    "v.type = :type AND v.severity = :severity " +
                    "ORDER BY v.startTime ASC");
            query.setParameter("metricIdList", metricIdList);
            query.setParameter("type", type);
            query.setParameter("severity", severity);

            try {
                return (List<Violation>) query.getResultList();
            }
            catch (NoResultException e) {
                return null;
            }
        }
        finally {
            em.close();
        }
    }

    public long getServiceTotalInViolationTime(Service service) {
        Metric metric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_SLA_COMPLIANCE, service);
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND " +
                    "mvh.value = :value");
            query.setParameter("metric", metric);
            query.setParameter("value", SLAComplianceEnum.VIOLATION.toString());
            List results = query.getResultList();

            long violationTime = 0;
            Iterator iterator = results.iterator();
            while (iterator.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) iterator.next();
                long interval = mvh.getEndTime().getTime() - mvh.getStartTime().getTime();
                violationTime += interval;
            }
            return violationTime;
        }
        finally {
            em.close();
        }
    }

    public static class ViolationsCount {
        private HashMap<ViolationType, HashMap<ViolationSeverity, Integer>> countMap;

        public ViolationsCount() {
            countMap = new HashMap<ViolationType, HashMap<ViolationSeverity, Integer>>();
            for (ViolationType type : ViolationType.values()) {
                HashMap<ViolationSeverity, Integer> typeMap = new HashMap<ViolationSeverity, Integer>();
                countMap.put(type, typeMap);

                for (ViolationSeverity severity : ViolationSeverity.values()) {
                    typeMap.put(severity, 0);
                }
            }
        }

        public int getCount(ViolationType type, ViolationSeverity severity) {
            return countMap.get(type).get(severity);
        }

        public void setCount(ViolationType type, ViolationSeverity severity, int count) {
            HashMap<ViolationSeverity, Integer> typeMap = countMap.get(type);
            typeMap.put(severity, count);
        }

        public void increase(ViolationType type, ViolationSeverity severity) {
            HashMap<ViolationSeverity, Integer> typeMap = countMap.get(type);
            int oldCount = typeMap.get(severity);
            typeMap.put(severity, oldCount + 1);
        }
    }
}
