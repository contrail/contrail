/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/qos/events/ViolationEvent.java $
 */

package org.slasoi.infrastructure.monitoring.qos.events;

import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.infrastructure.monitoring.utils.DateUtils;

import java.util.Date;

public class ViolationEvent {
    private long violationId;
    private String guaranteedTerm;
    private String serviceUri;
    private String resourceName;
    private String metricValue;
    private String metricUnit;
    private String thresholdValue;
    private Date timestamp;

    public ViolationEvent(long violationId) {
        this.violationId = violationId;
    }

    public ViolationEvent(JSONObject o) throws Exception {
        violationId = o.getLong("violationId");
        guaranteedTerm = o.getString("guaranteedTerm");
        serviceUri = o.getString("serviceUri");
        resourceName = o.getString("resourceName");
        metricValue = o.getString("metricValue");
        metricUnit = o.getString("metricUnit");
        thresholdValue = o.getString("thresholdValue");
        timestamp = DateUtils.parse(o.getString("timestamp"));
    }

    public long getViolationId() {
        return violationId;
    }

    public void setViolationId(long violationId) {
        this.violationId = violationId;
    }

    public String getGuaranteedTerm() {
        return guaranteedTerm;
    }

    public void setGuaranteedTerm(String guaranteedTerm) {
        this.guaranteedTerm = guaranteedTerm;
    }

    public String getServiceUri() {
        return serviceUri;
    }

    public void setServiceUri(String serviceUri) {
        this.serviceUri = serviceUri;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(String metricValue) {
        this.metricValue = metricValue;
    }

    public String getMetricUnit() {
        return metricUnit;
    }

    public void setMetricUnit(String metricUnit) {
        this.metricUnit = metricUnit;
    }

    public String getThresholdValue() {
        return thresholdValue;
    }

    public void setThresholdValue(String thresholdValue) {
        this.thresholdValue = thresholdValue;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("violationId", violationId);
        o.put("guaranteedTerm", guaranteedTerm);
        o.put("serviceUri", serviceUri);
        o.put("resourceName", resourceName);
        o.put("metricValue", metricValue);
        o.put("metricUnit", metricUnit);
        o.put("thresholdValue", thresholdValue);
        o.put("timestamp", DateUtils.format(timestamp));
        return o;
    }
}
