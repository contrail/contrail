/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2869 $
 * @lastrevision $Date: 2011-07-29 09:12:57 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceSlaCompliance. $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.*;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("SERVICE_SLA_COMPLIANCE")
public class ServiceSlaCompliance extends Metric {
    private static final Logger log = Logger.getLogger(ServiceSlaCompliance.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric SERVICE_SLA_COMPLIANCE of the service " + service.getServiceName());
        SLAComplianceEnum serviceSlaCompliance = SLAComplianceEnum.COMPLIANT;

        // prepare metrics list
        List<Metric> metricList = new ArrayList<Metric>();
        for (Metric metric : service.getMetricList()) {
            metricList.add(metric);
        }
        for (Vm vm : service.getVmList()) {
            for (Metric metric : vm.getMetricList()) {
                metricList.add(metric);
            }
        }

        // check if any critical QoS term is in violation. In that case SLA is violated.
        // uncritical QoS terms: if there is any acceptable violation/warning (which happened outside working hours - service restriction window) that is ok
        // number of punishable violations should be less than ACCEPTABLE_SERVICE_VIOLATIONS threshold ie ACCEPTABLE_SERVICE_VIOLATIONS term
        // should not be violated (ACCEPTABLE_SERVICE_VIOLATIONS term is critical)
        int punishableWarningsCount = 0;
        for (Metric metric : metricList) {
            MetricValue metricValue = metric.getMetricValue();
            if (metricValue == null) {
                continue;
            }

            SLAComplianceEnum slaCompliance = metricValue.getSlaCompliance();
            ViolationSeverity violationSeverity = metricValue.getViolationSeverity();
            MetricType metricType = metric.getMetricType();
            if (metricType.isCritical()) {
                // QoS term is critical (e.g. SERVICE_DATA_CLASSIFICATION)
                if (slaCompliance != null && slaCompliance.getSeverity() > serviceSlaCompliance.getSeverity()) {
                    serviceSlaCompliance = slaCompliance;
                }
            }
            else {
                // QoS term is not critical (e.g. SERVICE_AVAILABILITY)
                if (slaCompliance != null && slaCompliance == SLAComplianceEnum.WARNING &&
                        violationSeverity == ViolationSeverity.PUNISHABLE) {
                    punishableWarningsCount++;
                }
            }

            if (serviceSlaCompliance == SLAComplianceEnum.VIOLATION) {
                break; // no need to check further - service is in violation
            }
        }

        if (serviceSlaCompliance == SLAComplianceEnum.COMPLIANT && punishableWarningsCount > 0) {
            // if number of "punishable warnings" is greater than ACCEPTABLE_SERVICE_VIOLATIONS threshold than service is in warning state
            Metric asvMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, service);
            if (asvMetric != null && asvMetric.getViolationThreshold() != null) {
                int violationThreshold = Integer.parseInt(asvMetric.getViolationThreshold());
                if (punishableWarningsCount > violationThreshold) {
                    serviceSlaCompliance = SLAComplianceEnum.WARNING;
                }
            }
        }
        log.trace(String.format("SERVICE_SLA_COMPLIANCE(%s) = %s", service.getServiceName(), serviceSlaCompliance));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, serviceSlaCompliance, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, serviceSlaCompliance, metricsTimestamp);
    }

    @Override
    public SLAComplianceEnum parseValue(String value) throws InvalidMetricValueException {
        return SLAComplianceEnum.fromString(value);
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public Object getViolationThresholdValue() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getWarningCondition() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
