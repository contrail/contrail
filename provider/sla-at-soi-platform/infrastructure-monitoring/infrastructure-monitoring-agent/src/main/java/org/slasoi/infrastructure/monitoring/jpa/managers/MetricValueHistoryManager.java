/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/MetricValueHistoryManager.jav $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.computation.ReportingPeriod;
import org.slasoi.infrastructure.monitoring.jpa.controller.MetricValueHistoryJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MetricValueHistoryManager extends MetricValueHistoryJpaController {
    private static MetricValueHistoryManager instance;

    private MetricValueHistoryManager() {

    }

    public static MetricValueHistoryManager getInstance() {
        if (instance == null) {
            instance = new MetricValueHistoryManager();
        }
        return instance;
    }

    public void storeValue(Metric metric, Object valueObj, Date timestamp) throws Exception {
        String value = (valueObj != null) ? valueObj.toString() : null;
        MetricValueHistory last = getLastMVH(metric);
        long interval = InfrastructureMonitoringAgent.getInstance().getMetricsValidationInterval() * 1000;

        long now = System.currentTimeMillis();
        Date endTime = new Date(timestamp.getTime() + interval);

        if (last == null) {
            createRecord(metric, value, timestamp, endTime, null);
            return;
        }

        long lastMvhEndTime = last.getEndTime().getTime();
        if (Math.abs(last.getStartTime().getTime() - timestamp.getTime()) < 1000) {
            // record with the same start time (in seconds) already exists
            // update value if it is different from the last stored value
            if (!equalValues(value, last.getValue())) {
                last.setValue(value);
                MetricValueHistoryManager.getInstance().edit(last);
            }
        }
        else if (Math.abs(now - lastMvhEndTime) < 3.25 * interval) {
            // there was no break of monitoring after the last MetricValueHistory record
            // check if current value is equal to the last stored value
            if (equalValues(value, last.getValue())) {
                // current value is equal to the last stored value
                // extend the period of last MetricValueHistory
                last.setEndTime(endTime);
                MetricValueHistoryManager.getInstance().edit(last);
            }
            else {
                // current value is different from the last stored value
                // create a new MetricValueHistory record
                createRecord(metric, value, timestamp, endTime, last);
            }
        }
        else {
            // there was a break of monitoring after the last MetricValueHistory record
            // create a new MetricValueHistory record
            createRecord(metric, value, timestamp, endTime, null);
        }
    }

    public void storeValue(Metric metric, Object valueObj, ReportingPeriod reportingPeriod) throws Exception {
        String value = (valueObj != null) ? valueObj.toString() : null;
        Date startDate = reportingPeriod.getStartDate();
        Date endDate = reportingPeriod.getEndDate();

        MetricValueHistory mvh = getMVH(metric, startDate);
        if (mvh == null) {
            createRecord(metric, value, startDate, endDate, null);
        }
        else {
            mvh.setValue(value);
            MetricValueHistoryManager.getInstance().edit(mvh);
        }
    }

    private static boolean equalValues(String first, String second) {
        if (first != null) {
            return first.equals(second);
        }
        else if (second != null) {
            return second.equals(first);
        }
        else {
            return true; // first = second = null
        }
    }

    private void createRecord(Metric metric, String value, Date startTime, Date endTime, MetricValueHistory last) throws Exception {
        MetricValueHistory metricValueHistory = new MetricValueHistory(metric.getMetricId(), startTime);
        metricValueHistory.setMetric(metric);
        metricValueHistory.setValue(value);
        metricValueHistory.setEndTime(endTime);
        MetricValueHistoryManager.getInstance().create(metricValueHistory);

        // update the endTime of last MetricValueHistory
        if (last != null) {
            last.setEndTime(startTime);
            MetricValueHistoryManager.getInstance().edit(last);
        }
    }

    private MetricValueHistory getLastMVH(Metric metric) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND mvh.metricValueHistoryPK.startTime = (SELECT MAX(mvh1.metricValueHistoryPK.startTime) FROM MetricValueHistory mvh1 WHERE mvh1.metric = :metric)");
            query.setParameter("metric", metric);
            MetricValueHistory last = null;
            try {
                last = (MetricValueHistory) query.getSingleResult();
            }
            catch (NoResultException e) {
                return null;
            }
            return last;
        }
        finally {
            em.close();
        }
    }

    private MetricValueHistory getMVH(Metric metric, Date startDate) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND mvh.metricValueHistoryPK.startTime = :startDate");
            query.setParameter("metric", metric);
            query.setParameter("startDate", startDate);
            MetricValueHistory last = null;
            try {
                last = (MetricValueHistory) query.getSingleResult();
            }
            catch (NoResultException e) {
                return null;
            }
            return last;
        }
        finally {
            em.close();
        }
    }

    public List<MetricValue> getMetricHistory(Metric metric, Date fromDate, Date toDate, Integer maxNumberOfValues) {
        List<MetricValue> history = new ArrayList<MetricValue>();
        EntityManager em = getEntityManager();
        List<MetricValueHistory> mvhList;
        try {
            Query query;
            if (fromDate != null && toDate != null) {
                query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND " +
                        "mvh.endTime > :fromDate AND mvh.metricValueHistoryPK.startTime < :toDate " +
                        "ORDER BY mvh.metricValueHistoryPK.startTime");
                query.setParameter("metric", metric);
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
            }
            else {
                query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric " +
                        "ORDER BY mvh.metricValueHistoryPK.startTime");
                query.setParameter("metric", metric);
            }

            mvhList = (List<MetricValueHistory>) query.getResultList();
            if (mvhList.size() == 0) {
                return history; // return empty history
            }
        }
        finally {
            em.close();
        }

        long historyStartTime = mvhList.get(0).getStartTime().getTime();
        long historyEndTime = mvhList.get(mvhList.size() - 1).getEndTime().getTime();
        int historyValuesInterval = InfrastructureMonitoringAgent.getInstance().getMetricsValidationInterval() * 1000;
        long numberOfValues = (historyEndTime - historyStartTime) / historyValuesInterval;

        long interval;
        if (maxNumberOfValues == null || numberOfValues <= maxNumberOfValues) {
            interval = historyValuesInterval;
        }
        else {
            interval = (historyEndTime - historyStartTime) / maxNumberOfValues;
        }

        long time = mvhList.get(0).getStartTime().getTime();
        int mvhIndex = 0;
        while (time <= historyEndTime) {
            MetricValueHistory mvh = mvhList.get(mvhIndex);
            while (time >= mvh.getEndTime().getTime()) {
                mvhIndex++;
                if (mvhIndex >= mvhList.size()) {
                    break;
                }
                mvh = mvhList.get(mvhIndex);
            }
            if (mvhIndex >= mvhList.size()) {
                break;
            }

            String value;
            if (time < mvh.getStartTime().getTime()) {
                value = null;
            }
            else {
                value = mvh.getValue();
            }
            Date timestamp = new Date(time);
            MetricValue metricValue = new MetricValue();
            metricValue.setMetric(metric);
            metricValue.setTime(timestamp);
            metricValue.setValue(value);
            history.add(metricValue);

            time += interval;
        }

        return history;
    }

    public List<MetricValueHistory> getMvhValues(Metric metric, Date fromDate, Date toDate, boolean includeUnavailable) {
        EntityManager em = getEntityManager();
        List<MetricValueHistory> mvhList;
        try {
            Query query;
            if (fromDate != null && toDate != null) {
                query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND " +
                        "mvh.endTime > :fromDate AND mvh.metricValueHistoryPK.startTime < :toDate " +
                        "ORDER BY mvh.metricValueHistoryPK.startTime");
                query.setParameter("metric", metric);
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
            }
            else {
                query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric ORDER BY mvh.metricValueHistoryPK.startTime");
                query.setParameter("metric", metric);
            }
            mvhList = (List<MetricValueHistory>) query.getResultList();
        }
        finally {
            em.close();
        }

        if (includeUnavailable) {
            // check if there are any holes in the history
            long maxInterval = InfrastructureMonitoringAgent.getInstance().getMetricsCollectionInterval() * 1000 * 3;
            for (int i = 1; i < mvhList.size(); i++) {
                if (mvhList.get(i).getStartTime().getTime() - mvhList.get(i - 1).getEndTime().getTime() > maxInterval) {
                    MetricValueHistory emptyMvh = new MetricValueHistory(
                            mvhList.get(i).getMetric().getMetricId(),
                            mvhList.get(i - 1).getEndTime());
                    emptyMvh.setMetric(mvhList.get(i).getMetric());
                    emptyMvh.setEndTime(mvhList.get(i).getStartTime());
                    emptyMvh.setValue(null);
                    mvhList.add(i, emptyMvh);
                }
            }
        }
        return mvhList;
    }
}
