/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2869 $
 * @lastrevision $Date: 2011-07-29 09:12:57 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/VmImage.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("VM_IMAGE")
public class VmImage extends Metric {
    private static final Logger log = Logger.getLogger(VmImage.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Vm vm = this.getVm();
        log.trace("Computing metric VM_IMAGE of the VM " + vm.getFqdn());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        String vmImageTemplate = (String) monitoringEngine.getVmMetricData(vm.getFqdn(), RawMetric.VM_IMAGE_TEMPLATE).getValue();
        log.trace(String.format("VM_IMAGE(%s) = %s", vm.getFqdn(), vmImageTemplate));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, vmImageTemplate, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, vmImageTemplate, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            String required = this.getViolationThreshold();
            String actual = metricValue.getValue();
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (!actual.equals(required)) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s, virtual machine %s: VM image template is '%s' but it should be '%s'.",
                        vm.getService().getServiceName(), vm.getFqdn(), actual, required));
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public String parseValue(String value) {
        return value;
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public String getViolationThresholdValue() {
        return this.getViolationThreshold();
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        String value = getViolationThresholdValue();
        if (value == null) {
            return null;
        }
        else {
            return String.format("If VM image is not '%s'.", value);
        }
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
