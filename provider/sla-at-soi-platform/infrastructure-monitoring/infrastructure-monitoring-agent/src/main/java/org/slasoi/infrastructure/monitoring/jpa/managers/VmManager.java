/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/VmManager.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.slasoi.infrastructure.monitoring.jpa.controller.VmJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class VmManager extends VmJpaController {
    private static VmManager instance;

    private VmManager() {

    }

    public static VmManager getInstance() {
        if (instance == null) {
            instance = new VmManager();
        }
        return instance;
    }

    public Vm findVm(int resourceId, String clusterName) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT vm FROM Vm vm WHERE vm.resourceId = :resourceId AND vm.clusterName = :clusterName");
            query.setParameter("resourceId", resourceId);
            query.setParameter("clusterName", clusterName);
            return (Vm) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public Vm findVmByFqdn(String fqdn) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Vm.findByFqdn");
            query.setParameter("fqdn", fqdn);
            return (Vm) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }
}
