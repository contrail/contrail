/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2818 $
 * @lastrevision $Date: 2011-07-26 08:32:55 +0200 (tor, 26 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/qos/ViolationHandler.java $
 */

package org.slasoi.infrastructure.monitoring.qos;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Violation;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;
import org.slasoi.infrastructure.monitoring.jpa.managers.ViolationManager;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventMessage;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventType;
import org.slasoi.infrastructure.monitoring.qos.events.ViolationEvent;

import java.util.Date;

public class ViolationHandler {
    private static Logger log = Logger.getLogger(ViolationHandler.class);

    public static void handleViolation(Metric metric, MetricValue metricValue, SLAComplianceEnum slaCompliance, SLAComplianceEnum prevSLACompliance) throws Exception {
        if (prevSLACompliance == null) {
            prevSLACompliance = SLAComplianceEnum.COMPLIANT;
        }

        if (slaCompliance.getSeverity() > prevSLACompliance.getSeverity()) {
            // new warning or violation occurred
            Violation violation = createViolation(metric, metricValue, slaCompliance);
            if (metricValue.getViolationSeverity() == ViolationSeverity.PUNISHABLE) {
                publishViolation(violation, metric, metricValue);
            }
        }
        else if (slaCompliance.getSeverity() == prevSLACompliance.getSeverity() && slaCompliance.getSeverity() > SLAComplianceEnum.COMPLIANT.getSeverity()) {
            // warning or violation from previous check is still existent
            updateViolation(metric, metricValue, slaCompliance);
            // TODO: update violation severity
        }
        else if (slaCompliance.getSeverity() < prevSLACompliance.getSeverity()) {
            // close old violation in the DB
            ViolationType type = ViolationType.fromString(prevSLACompliance.name());
            Violation violation = ViolationManager.getInstance().getLastViolation(metric, type);
            if (violation != null) {
                violation.setEndTime(metricValue.getTime());
                ViolationManager.getInstance().edit(violation);
            }
        }
    }

    private static Violation createViolation(Metric metric, MetricValue metricValue, SLAComplianceEnum slaCompliance) {
        Violation violation = new Violation();
        violation.setMetric(metric);
        violation.setMetricValue(metricValue.getValue());
        violation.setStartTime(metricValue.getTime());
        violation.setEndTime(null);

        if (slaCompliance == SLAComplianceEnum.VIOLATION) {
            violation.setThreshold(metric.getViolationThreshold());
            violation.setType(ViolationType.VIOLATION);
        }
        else if (slaCompliance == SLAComplianceEnum.WARNING) {
            violation.setThreshold(metric.getWarningThreshold());
            violation.setType(ViolationType.WARNING);
        }
        else {
            assert false;
        }

        violation.setSeverity(metricValue.getViolationSeverity());
        ViolationManager.getInstance().create(violation);
        return violation;
    }

    private static Violation updateViolation(Metric metric, MetricValue metricValue, SLAComplianceEnum slaCompliance) {
        // update metric value
        return null;
    }

    public static void publishViolation(Violation violation, Metric metric, MetricValue metricValue) {
        // publish message on the event bus
        try {
            ViolationEvent violationEvent = new ViolationEvent(violation.getViolationId());
            violationEvent.setGuaranteedTerm(metric.getMetricType().getName());
            violationEvent.setMetricValue(metricValue.getValue());
            violationEvent.setThresholdValue(metric.getViolationThreshold());
            String unit = (metric.getMetricType().getMetricUnit() != null) ? metric.getMetricType().getMetricUnit() : "";
            violationEvent.setMetricUnit(unit);
            violationEvent.setTimestamp(metricValue.getTime());
            if (metric.getMetricType().getTarget() == MetricType.Target.SERVICE) {
                violationEvent.setServiceUri(metric.getService().getServiceUrl());
                violationEvent.setResourceName("");
            }
            else if (metric.getMetricType().getTarget() == MetricType.Target.VM) {
                violationEvent.setServiceUri(metric.getVm().getService().getServiceUrl());
                violationEvent.setResourceName(metric.getVm().getName());
            }

            EventMessage eventMessage = new EventMessage();
            eventMessage.setEventType(EventType.SLA_VIOLATION);
            eventMessage.setOriginator(InfrastructureMonitoringAgent.APPLICATION_NAME);
            eventMessage.setTimestamp(new Date());
            eventMessage.setEventData(violationEvent.toJson());

            JSONObject payload = eventMessage.toJson();
            InfrastructureMonitoringAgent agent = InfrastructureMonitoringAgent.getInstance();
            PubSubMessage message = new PubSubMessage(agent.getEventChannel().getName(), payload.toString());
            agent.getPubSubManager().publish(message);
            log.trace("Violation event published to the event channel: " + payload);
        }
        catch (Exception e) {
            log.error(String.format("Failed to publish violation event %d to the event channel: %s",
                    violation.getViolationId(), e.getMessage()));
        }
    }
}
