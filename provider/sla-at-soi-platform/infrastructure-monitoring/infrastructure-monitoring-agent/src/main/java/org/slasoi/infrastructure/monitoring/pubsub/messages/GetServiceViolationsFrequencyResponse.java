/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GetServiceViolationsFreque $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetServiceViolationsFrequencyResponse extends PubSubResponse {
    private String serviceUri;
    private List<ViolationsCount> violationsFrequency;
    private List<ViolationsCount> warningsFrequency;

    public GetServiceViolationsFrequencyResponse() {
        this.setResponseType("GetServiceViolationsFrequencyResponse");
        violationsFrequency = new ArrayList<ViolationsCount>();
        warningsFrequency = new ArrayList<ViolationsCount>();
    }

    public static GetServiceViolationsFrequencyResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, GetServiceViolationsFrequencyResponse.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public String getServiceUri() {
        return serviceUri;
    }

    public void setServiceUri(String serviceUri) {
        this.serviceUri = serviceUri;
    }

    public List<ViolationsCount> getViolationsFrequency() {
        return violationsFrequency;
    }

    public void setViolationsFrequency(List<ViolationsCount> violationsFrequency) {
        this.violationsFrequency = violationsFrequency;
    }

    public void putViolationsCount(ViolationsCount count) {
        this.violationsFrequency.add(count);
    }

    public List<ViolationsCount> getWarningsFrequency() {
        return warningsFrequency;
    }

    public void setWarningsFrequency(List<ViolationsCount> warningsFrequency) {
        this.warningsFrequency = warningsFrequency;
    }

    public void putWarningsCount(ViolationsCount count) {
        this.warningsFrequency.add(count);
    }

    public static class ViolationsCount {
        private Date timestamp;
        private int punishable;
        private int acceptable;
        private String periodTitle;

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public int getPunishable() {
            return punishable;
        }

        public void setPunishable(int punishable) {
            this.punishable = punishable;
        }

        public int getAcceptable() {
            return acceptable;
        }

        public void setAcceptable(int acceptable) {
            this.acceptable = acceptable;
        }

        public String getPeriodTitle() {
            return periodTitle;
        }

        public void setPeriodTitle(String periodTitle) {
            this.periodTitle = periodTitle;
        }
    }

}
