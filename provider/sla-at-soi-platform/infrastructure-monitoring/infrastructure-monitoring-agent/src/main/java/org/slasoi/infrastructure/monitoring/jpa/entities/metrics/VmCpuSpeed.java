/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3041 $
 * @lastrevision $Date: 2011-08-31 09:10:11 +0200 (sre, 31 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/VmCpuSpeed.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("VM_CPU_SPEED")
public class VmCpuSpeed extends Metric {
    private static final Logger log = Logger.getLogger(VmCpuSpeed.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Vm vm = this.getVm();
        log.trace("Computing metric VM_CPU_SPEED of the VM " + vm.getFqdn());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        double cpuSpeed; // CPU speed used by the host process running the VM
        String hostFqdn;
        try {
            double cpuLoad = (Double) monitoringEngine.getVmMetricData(vm.getFqdn(), RawMetric.CPU_LOAD).getValue();
            hostFqdn = monitoringEngine.getVmHostMachine(vm.getFqdn());
            double hostCpuSpeedOneCore = (Double) monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_SPEED).getValue();
            cpuSpeed = cpuLoad * hostCpuSpeedOneCore;
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to compute metric VM_CPU_SPEED of the vm %s: %s", vm.getFqdn(), e.getMessage()));
        }

        log.trace(String.format("VM_CPU_SPEED(%s) = %s", vm.getFqdn(), cpuSpeed));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(cpuSpeed, metricsTimestamp);
        storeMetricHistoryValue(cpuSpeed, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            // QoS term is violated if actual CPU speed is lower than guaranteed by 100% VM vCPU utilization

            double violationThreshold = this.getViolationThresholdValue();
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            double hostCpuIdle = (Double) monitoringEngine.getHostMetricData(hostFqdn,
                    RawMetric.CPU_IDLE).getValue();

            if (cpuSpeed < violationThreshold && hostCpuIdle < 0.01) { // approximation because we don't know the
                // vCPU utilization inside the VM
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s, virtual machine %s: " +
                        "VM is not getting enough CPU share. Actual CPU speed is %s, " +
                        "guaranteed by the SLA is %s.", vm.getService().getServiceName(), vm.getFqdn(),
                        formatValue(cpuSpeed), formatValue(violationThreshold)));
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public Double parseValue(String value) {
        return Double.valueOf(value);
    }

    @Override
    public String formatValue(Object value) {
        if (value == null) {
            return null;
        }
        else {
            return String.format("%.2f", (Double) value);
        }
    }

    @Override
    public Double getViolationThresholdValue() {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            return Double.parseDouble(this.getViolationThreshold());
        }
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            String unit = this.getMetricType().getMetricUnit();
            return String.format("value < %.2f %s and vCPU is fully utilized.", getViolationThresholdValue(), unit);
        }
    }

    @Override
    public Double getWarningThresholdValue() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            return Double.parseDouble(this.getWarningThreshold());
        }
    }

    @Override
    public String getWarningCondition() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            String unit = this.getMetricType().getMetricUnit();
            return String.format("%.2f %s <= value < %.2f %s and vCPU is fully utilized.", getViolationThresholdValue(),
                    unit, getWarningThresholdValue(), unit);
        }
    }

    @Override
    public Double estimateWarningThreshold(Object violationThreshold) {
        if (violationThreshold != null) {
            double v = Double.parseDouble(violationThreshold.toString());
            return v * 1.03;
        }
        else {
            return null;
        }
    }
}
