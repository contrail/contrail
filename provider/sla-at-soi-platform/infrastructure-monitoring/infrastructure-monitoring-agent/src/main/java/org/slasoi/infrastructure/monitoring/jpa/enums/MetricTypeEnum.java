/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2958 $
 * @lastrevision $Date: 2011-08-11 11:18:14 +0200 (čet, 11 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/enums/MetricTypeEnum.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.enums;

import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricTypeManager;

import java.util.HashMap;
import java.util.List;

public enum MetricTypeEnum {
    SERVICE_AUDITABILITY(1),
    SERVICE_AVAILABILITY_RESTRICTIONS(2),
    SERVICE_AVAILABILITY_STATUS(3),
    SERVICE_AVAILABILITY(4),
    ACCEPTABLE_SERVICE_VIOLATIONS(5),
    SERVICE_DATA_CLASSIFICATION(6),
    SERVICE_HARDWARE_REDUNDANCY_LEVEL(7),
    SERVICE_ISOLATION(8),
    SERVICE_MTTF(9),
    SERVICE_MTTR(10),
    SERVICE_MTTV(11),
    SERVICE_REPORTING_INTERVAL(12),
    SERVICE_SAS70_COMPLIANCE(13),
    VM_AVAILABILITY_STATUS(14),
    VM_AVAILABILITY(15),
    SERVICE_CCR(16),
    VM_CORES(17),
    VM_CPU_SPEED(18),
    VM_CPU_SPEED_AVAILABLE(19),
    VM_CPU_SPEED_USED(20),
    VM_DATA_ENCRYPTION(21),
    VM_DISK_THROUGHPUT(22),
    VM_IMAGE(23),
    VM_LOCATION(24),
    VM_MEMORY_SIZE(25),
    VM_MEMORY_SIZE_AVAILABLE(26),
    VM_MEMORY_SIZE_USED(27),
    VM_NET_THROUGHPUT(28),
    VM_PERSISTENCE(29),
    VM_QUANTITY(30),
    VM_SLA_COMPLIANCE(31),
    SERVICE_SLA_COMPLIANCE(32),
    CLUSTER_VCPU_USAGE(33),
    CLUSTER_CPU_SPEED_USAGE(34),
    CLUSTER_PROVISIONED_MEMORY_USAGE(35),
    CLUSTER_ACTUAL_MEMORY_USAGE(36),
    CLUSTER_DISK_SPACE_USAGE(37);

    private int metricTypeId;

    MetricTypeEnum(int metricTypeId) {
        this.metricTypeId = metricTypeId;
    }

    public MetricType getMetricType() {
        return MetricTypeManager.getInstance().findMetricType(metricTypeId);
    }

    public static MetricTypeEnum convert(MetricType metricType) {
        return MetricTypeEnum.valueOf(metricType.getName());
    }

    public static MetricTypeEnum fromString(String text) throws Exception {
        try {
            return MetricTypeEnum.valueOf(text.toUpperCase());
        }
        catch (Exception e) {
            throw new Exception("Invalid MetricTypeEnum value: " + text);
        }
    }
}

class MetricTypeMapping {
    private HashMap<MetricTypeEnum, MetricType> mapping;
    private static MetricTypeMapping instance;

    static MetricTypeMapping getInstance() {
        if (instance == null) {
            instance = new MetricTypeMapping();
        }
        return instance;
    }

    private MetricTypeMapping() {
        List<MetricType> metricTypes = MetricTypeManager.getInstance().findMetricTypeEntities();
        mapping = new HashMap<MetricTypeEnum, MetricType>();
        for (MetricType metricType : metricTypes) {
            MetricTypeEnum metricTypeEnum = MetricTypeEnum.valueOf(metricType.getName());
            mapping.put(metricTypeEnum, metricType);
        }
    }

    MetricType convertEnumToDBValue(MetricTypeEnum metricTypeEnum) {
        return mapping.get(metricTypeEnum);
    }
}
