/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/handlers/MonitoringFeaturesRequestH $
 */

package org.slasoi.infrastructure.monitoring.pubsub.handlers;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesResponse;
import org.slasoi.infrastructure.monitoring.qos.MonitoringFeaturesBuilder;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

import java.util.Date;

public class MonitoringFeaturesRequestHandler {
    private static Logger log = Logger.getLogger(MonitoringFeaturesRequestHandler.class);

    public static PubSubResponse processRequest(MonitoringFeaturesRequest request) throws Exception {
        log.trace("processRequest() started.");
        MonitoringFeaturesBuilder monitoringFeaturesBuilder = new MonitoringFeaturesBuilder();
        ComponentMonitoringFeatures[] cmf = monitoringFeaturesBuilder.getMonitoringFeatures();
        MonitoringFeaturesResponse response = new MonitoringFeaturesResponse();
        response.setInReplyTo(request.getMessageId());
        response.setComponentMonitoringFeatures(cmf);
        response.setTimestamp(new Date());
        log.trace("processRequest() finished successfully.");
        return response;
    }
}
