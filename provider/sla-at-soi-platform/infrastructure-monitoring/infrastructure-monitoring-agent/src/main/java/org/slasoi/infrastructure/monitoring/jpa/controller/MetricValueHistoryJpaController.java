/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/MetricValueHistoryJpaContro $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.PreexistingEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistory;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistoryPK;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class MetricValueHistoryJpaController {

    public MetricValueHistoryJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MetricValueHistory metricValueHistory) throws PreexistingEntityException, Exception {
        if (metricValueHistory.getMetricValueHistoryPK() == null) {
            metricValueHistory.setMetricValueHistoryPK(new MetricValueHistoryPK());
        }
        metricValueHistory.getMetricValueHistoryPK().setMetricId(metricValueHistory.getMetric().getMetricId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Metric metric = metricValueHistory.getMetric();
            if (metric != null) {
                metric = em.getReference(metric.getClass(), metric.getMetricId());
                metricValueHistory.setMetric(metric);
            }
            em.persist(metricValueHistory);
            if (metric != null) {
                metric.getMetricValueHistoryList().add(metricValueHistory);
                metric = em.merge(metric);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            if (findMetricValueHistory(metricValueHistory.getMetricValueHistoryPK()) != null) {
                throw new PreexistingEntityException("MetricValueHistory " + metricValueHistory + " already exists.", ex);
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MetricValueHistory metricValueHistory) throws NonexistentEntityException, Exception {
        metricValueHistory.getMetricValueHistoryPK().setMetricId(metricValueHistory.getMetric().getMetricId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricValueHistory persistentMetricValueHistory = em.find(MetricValueHistory.class, metricValueHistory.getMetricValueHistoryPK());
            Metric metricOld = persistentMetricValueHistory.getMetric();
            Metric metricNew = metricValueHistory.getMetric();
            if (metricNew != null) {
                metricNew = em.getReference(metricNew.getClass(), metricNew.getMetricId());
                metricValueHistory.setMetric(metricNew);
            }
            metricValueHistory = em.merge(metricValueHistory);
            if (metricOld != null && !metricOld.equals(metricNew)) {
                metricOld.getMetricValueHistoryList().remove(metricValueHistory);
                metricOld = em.merge(metricOld);
            }
            if (metricNew != null && !metricNew.equals(metricOld)) {
                metricNew.getMetricValueHistoryList().add(metricValueHistory);
                metricNew = em.merge(metricNew);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                MetricValueHistoryPK id = metricValueHistory.getMetricValueHistoryPK();
                if (findMetricValueHistory(id) == null) {
                    throw new NonexistentEntityException("The metricValueHistory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(MetricValueHistoryPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricValueHistory metricValueHistory;
            try {
                metricValueHistory = em.getReference(MetricValueHistory.class, id);
                metricValueHistory.getMetricValueHistoryPK();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The metricValueHistory with id " + id + " no longer exists.", enfe);
            }
            Metric metric = metricValueHistory.getMetric();
            if (metric != null) {
                metric.getMetricValueHistoryList().remove(metricValueHistory);
                metric = em.merge(metric);
            }
            em.remove(metricValueHistory);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MetricValueHistory> findMetricValueHistoryEntities() {
        return findMetricValueHistoryEntities(true, -1, -1);
    }

    public List<MetricValueHistory> findMetricValueHistoryEntities(int maxResults, int firstResult) {
        return findMetricValueHistoryEntities(false, maxResults, firstResult);
    }

    private List<MetricValueHistory> findMetricValueHistoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MetricValueHistory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public MetricValueHistory findMetricValueHistory(MetricValueHistoryPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MetricValueHistory.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getMetricValueHistoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MetricValueHistory> rt = cq.from(MetricValueHistory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
