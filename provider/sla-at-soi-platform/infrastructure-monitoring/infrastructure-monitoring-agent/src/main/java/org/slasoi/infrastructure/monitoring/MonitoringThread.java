/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/MonitoringThread.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.computation.MetricsComputationEngine;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import java.util.Date;
import java.util.List;

public class MonitoringThread implements Runnable {
    private static Logger log = Logger.getLogger(MonitoringThread.class);
    private IMonitoringEngine monitoringEngine;
    private MetricsComputationEngine metricsComputationEngine;
    PubSubManager pubSubManager;
    Channel eventChannel;

    public MonitoringThread(IMonitoringEngine monitoringEngine, PubSubManager pubSubManager, Channel eventChannel) {
        this.monitoringEngine = monitoringEngine;
        this.metricsComputationEngine = new MetricsComputationEngine(monitoringEngine);
        this.pubSubManager = pubSubManager;
        this.eventChannel = eventChannel;
    }

    public void run() {
        log.info("MonitoringThread started.");

        Date lastValidationTime = new Date();
        while (!Thread.currentThread().isInterrupted()) {
            Date startTime = new Date();
            try {
                log.trace("Collecting metrics...");
                monitoringEngine.query();
                long timeSinceLastValidation = startTime.getTime() - lastValidationTime.getTime();
                long metricsValidationInterval = InfrastructureMonitoringAgent.getInstance().getMetricsValidationInterval() * 1000;
                if (timeSinceLastValidation >= metricsValidationInterval && metricsValidationInterval > 0) {
                    log.trace("Acquiring the lock.");
                    synchronized (InfrastructureMonitoringAgent.getInstance().getLock()) {
                        log.trace("Lock acquired.");
                        log.trace("Validating QoS terms...");
                        List<Service> services = ServiceManager.getInstance().findServiceEntities();
                        metricsComputationEngine.computeValidateMetrics(services);
                        lastValidationTime = startTime;
                    }
                }
            }
            catch (Exception e) {
                log.error("Monitoring error: " + e.getMessage(), e);
            }

            Date endTime = new Date();
            long spentTime = endTime.getTime() - startTime.getTime();

            long collectInterval = InfrastructureMonitoringAgent.getInstance().getMetricsCollectionInterval() * 1000;
            long sleepTime = (spentTime < collectInterval) ? (collectInterval - spentTime) : 0;
            log.trace("Sleeping for " + sleepTime / 1000 + " seconds.");
            if (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(sleepTime);
                }
                catch (InterruptedException e) {
                    break;
                }
            }
        }
        log.trace("MonitoringThread has been stopped.");
    }

    protected void monitorManually() throws Exception {
        log.trace("monitorManually() started.");
        monitoringEngine.query();
        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        metricsComputationEngine.computeValidateMetrics(services);
        log.trace("monitorManually() finished.");
    }
}
