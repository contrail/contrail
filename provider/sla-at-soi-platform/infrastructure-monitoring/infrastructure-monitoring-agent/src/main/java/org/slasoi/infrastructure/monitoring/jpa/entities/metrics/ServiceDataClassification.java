/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2997 $
 * @lastrevision $Date: 2011-08-22 17:10:42 +0200 (pon, 22 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceDataClassifica $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.DataClassificationEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("SERVICE_DATA_CLASSIFICATION")
public class ServiceDataClassification extends Metric {
    private static final Logger log = Logger.getLogger(ServiceDataClassification.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric SERVICE_DATA_CLASSIFICATION of the service " + service.getServiceName());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        // service data classification level is the lowest level of all hosts classification levels where vms run
        DataClassificationEnum serviceDataClass = null;
        for (Vm vm : service.getVmList()) {
            String hostFqdn = monitoringEngine.getVmHostMachine(vm.getFqdn());
            DataClassificationEnum vmDataClass =
                    (DataClassificationEnum) monitoringEngine.getHostMetricData(hostFqdn, RawMetric.DATA_CLASSIFICATION).getValue();
            if (serviceDataClass == null) {
                serviceDataClass = vmDataClass;
            }
            else if (vmDataClass.getClassificationLevel() < serviceDataClass.getClassificationLevel()) {
                serviceDataClass = vmDataClass;
            }
        }
        log.trace(String.format("SERVICE_DATA_CLASSIFICATION(%s) = %s", service.getServiceName(), serviceDataClass));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, serviceDataClass, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, serviceDataClass, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            DataClassificationEnum required = DataClassificationEnum.fromString(this.getViolationThreshold());
            DataClassificationEnum actual = DataClassificationEnum.fromString(metricValue.getValue());
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (actual.getClassificationLevel() < required.getClassificationLevel()) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s: service data classification level is %s but should be %s.",
                        service.getServiceUrl(), actual, required));
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public DataClassificationEnum parseValue(String value) throws InvalidMetricValueException {
        return DataClassificationEnum.fromString(value);
    }

    @Override
    public String formatValue(Object value) {
        return ((DataClassificationEnum) value).toString();
    }

    @Override
    public String displayValue(String value) throws InvalidMetricValueException {
        DataClassificationEnum enumValue = parseValue(value);
        return enumValue.getTitle();
    }

    @Override
    public DataClassificationEnum getViolationThresholdValue() throws InvalidMetricValueException {
        return DataClassificationEnum.fromString(this.getViolationThreshold());
    }

    @Override
    public String getViolationThresholdWithUnit() throws InvalidMetricValueException {
        return getViolationThresholdValue().getTitle();
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        DataClassificationEnum classificationLevel = (DataClassificationEnum) getViolationThresholdValue();
        if (classificationLevel == DataClassificationEnum.PUBLIC) {
            return "None, classification level is PUBLIC.";
        }
        else {
            return "classification_level < " + classificationLevel;
        }
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
