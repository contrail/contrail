/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/VmCpuSpeedAvailable.j $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("VM_CPU_SPEED_AVAILABLE")
public class VmCpuSpeedAvailable extends Metric {
    private static final Logger log = Logger.getLogger(VmCpuSpeedAvailable.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Vm vm = this.getVm();
        log.trace("Computing metric VM_CPU_SPEED_AVAILABLE of the VM " + vm.getFqdn());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();
        double cpuSpeedAvailable; // approximation of cpu resources available to this vm
        try {
            String hostFqdn = monitoringEngine.getVmHostMachine(vm.getFqdn());
            double hostCpuSpeedTotal = (Double) monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_SPEED).getValue();
            int hostNumOfCores = (Integer) monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_CORES_COUNT).getValue();
            List<String> guestMachines = monitoringEngine.getGuestMachines(hostFqdn);
            double loadSum = 0;
            for (String vmFqdn : guestMachines) {
                if (vmFqdn.equals(vm.getFqdn())) {
                    continue;
                }
                double cpuLoad = (Double) monitoringEngine.getVmMetricData(vmFqdn, RawMetric.CPU_LOAD).getValue();
                loadSum += cpuLoad;
            }
            cpuSpeedAvailable = Math.round((1 - loadSum / hostNumOfCores) * hostCpuSpeedTotal);
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to compute VM_CPU_SPEED_AVAILABLE metric of the vm %s: %s", vm.getFqdn(), e.getMessage()));
        }

        log.trace(String.format("VM_CPU_SPEED_AVAILABLE(%s) = %f", vm.getFqdn(), cpuSpeedAvailable));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, cpuSpeedAvailable, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, cpuSpeedAvailable, metricsTimestamp);

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            double violationThreshold = Double.parseDouble(this.getViolationThreshold());
            double actual = metricValue.getDoubleValue();
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (actual < violationThreshold) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s, virtual machine %s: CPU speed available to the VM is %.0f GHz, guaranteed by the SLA is %.0f GHz.",
                        vm.getService().getServiceName(), vm.getFqdn(), actual, violationThreshold));
            }
            else if (this.getWarningThreshold() != null) {
                double warningThreshold = Double.parseDouble(this.getWarningThreshold());
                if (actual < warningThreshold) {
                    slaCompliance = SLAComplianceEnum.WARNING;
                    log.warn(String.format("SLA violation detected at the service %s, virtual machine %s: CPU speed available to the VM is %.0f GHz, should be more than %.0f GHz.",
                            vm.getService().getServiceName(), vm.getFqdn(), actual, warningThreshold));
                }
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public Double parseValue(String value) {
        return Double.valueOf(value);
    }

    @Override
    public String formatValue(Object value) {
        if (value == null) {
            return null;
        }
        else {
            return String.format("%.2f", (Double) value);
        }
    }

    @Override
    public Double getViolationThresholdValue() {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            return Double.parseDouble(this.getViolationThreshold());
        }
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            String unit = this.getMetricType().getMetricUnit();
            return String.format("cpu_speed < %.2f %s", getViolationThresholdValue(), unit);
        }
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        if (violationThreshold != null) {
            double v = Double.parseDouble(violationThreshold.toString());
            return v * 1.1;
        }
        else {
            return null;
        }
    }
}
