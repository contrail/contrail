/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/MetricValue.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "metric_value")
@NamedQueries({
        @NamedQuery(name = "MetricValue.findAll", query = "SELECT m FROM MetricValue m"),
        @NamedQuery(name = "MetricValue.findByMetricId", query = "SELECT m FROM MetricValue m WHERE m.metricId = :metricId"),
        @NamedQuery(name = "MetricValue.findByTime", query = "SELECT m FROM MetricValue m WHERE m.time = :time"),
        @NamedQuery(name = "MetricValue.findByValue", query = "SELECT m FROM MetricValue m WHERE m.value = :value"),
        @NamedQuery(name = "MetricValue.findBySlaCompliance", query = "SELECT m FROM MetricValue m WHERE m.slaCompliance = :slaCompliance")})
public class MetricValue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "metric_id", nullable = false)
    private Integer metricId;
    @Basic(optional = false)
    @Column(name = "time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    @Column(name = "value", length = 32)
    private String value;
    @Column(name = "sla_compliance")
    @Enumerated(EnumType.STRING)
    private SLAComplianceEnum slaCompliance;
    @Column(name = "violation_severity")
    @Enumerated(EnumType.STRING)
    private ViolationSeverity violationSeverity;
    @JoinColumn(name = "metric_id", referencedColumnName = "metric_id", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Metric metric;

    public MetricValue() {
    }

    public MetricValue(Integer metricId) {
        this.metricId = metricId;
    }

    public MetricValue(Integer metricId, Date time) {
        this.metricId = metricId;
        this.time = time;
    }

    public Integer getMetricId() {
        return metricId;
    }

    public void setMetricId(Integer metricId) {
        this.metricId = metricId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public boolean getBooleanValue() {
        return Boolean.parseBoolean(value);
    }

    public int getIntValue() {
        return Integer.parseInt(value);
    }

    public long getLongValue() {
        return Long.parseLong(value);
    }

    public double getDoubleValue() {
        return Double.parseDouble(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SLAComplianceEnum getSlaCompliance() {
        return slaCompliance;
    }

    public void setSlaCompliance(SLAComplianceEnum slaCompliance) {
        this.slaCompliance = slaCompliance;
    }

    public ViolationSeverity getViolationSeverity() {
        return violationSeverity;
    }

    public void setViolationSeverity(ViolationSeverity violationSeverity) {
        this.violationSeverity = violationSeverity;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (metricId != null ? metricId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MetricValue)) {
            return false;
        }
        MetricValue other = (MetricValue) object;
        if ((this.metricId == null && other.metricId != null) || (this.metricId != null && !this.metricId.equals(other.metricId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue[metricId=" + metricId + "]";
    }
}
