/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GetServiceViolationsRespon $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetServiceViolationsResponse extends PubSubResponse {
    private String serviceUri;
    private ServiceEvents serviceEvents;
    private List<ResourceEvents> resourceEvents;

    public GetServiceViolationsResponse() {
        this.setResponseType("GetServiceViolationsResponse");
        this.serviceEvents = new ServiceEvents();
        this.resourceEvents = new ArrayList<ResourceEvents>();
    }

    public static GetServiceViolationsResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, GetServiceViolationsResponse.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public String getServiceUri() {
        return serviceUri;
    }

    public void setServiceUri(String serviceUri) {
        this.serviceUri = serviceUri;
    }

    public ServiceEvents getServiceEvents() {
        return serviceEvents;
    }

    public void setServiceEvents(ServiceEvents serviceEvents) {
        this.serviceEvents = serviceEvents;
    }

    public List<ResourceEvents> getResourceEvents() {
        return resourceEvents;
    }

    public void setResourceEvents(List<ResourceEvents> resourceEvents) {
        this.resourceEvents = resourceEvents;
    }

    public void putResourceEvents(ResourceEvents resourceEvents) {
        this.resourceEvents.add(resourceEvents);
    }

    public static class Violation {
        private int violationId;
        private String guaranteedTerm;
        private String metricValue;
        private String thresholdValue;
        private String metricUnit;
        private Date beginDate;
        private Date endDate;
        private ViolationSeverity severity;
        private ViolationType type;

        public int getViolationId() {
            return violationId;
        }

        public void setViolationId(int violationId) {
            this.violationId = violationId;
        }

        public String getGuaranteedTerm() {
            return guaranteedTerm;
        }

        public void setGuaranteedTerm(String guaranteedTerm) {
            this.guaranteedTerm = guaranteedTerm;
        }

        public String getMetricValue() {
            return metricValue;
        }

        public void setMetricValue(String metricValue) {
            this.metricValue = metricValue;
        }

        public String getThresholdValue() {
            return thresholdValue;
        }

        public void setThresholdValue(String thresholdValue) {
            this.thresholdValue = thresholdValue;
        }

        public String getMetricUnit() {
            return metricUnit;
        }

        public void setMetricUnit(String metricUnit) {
            this.metricUnit = metricUnit;
        }

        public Date getBeginDate() {
            return beginDate;
        }

        public void setBeginDate(Date beginDate) {
            this.beginDate = beginDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public ViolationSeverity getSeverity() {
            return severity;
        }

        public void setSeverity(ViolationSeverity severity) {
            this.severity = severity;
        }

        public ViolationType getType() {
            return type;
        }

        public void setType(ViolationType type) {
            this.type = type;
        }
    }

    public static class ServiceEvents {
        private List<Violation> violations;
        private List<Violation> warnings;

        public ServiceEvents() {
            violations = new ArrayList<Violation>();
            warnings = new ArrayList<Violation>();
        }

        public List<Violation> getViolations() {
            return violations;
        }

        public void setViolations(List<Violation> violations) {
            this.violations = violations;
        }

        public void putViolation(Violation violation) {
            this.violations.add(violation);
        }

        public List<Violation> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<Violation> warnings) {
            this.warnings = warnings;
        }

        public void putWarning(Violation warning) {
            this.warnings.add(warning);
        }
    }

    public static class ResourceEvents {
        private String resourceName;
        private String resourceFqdn;
        private List<Violation> violations;
        private List<Violation> warnings;

        public ResourceEvents() {
            violations = new ArrayList<Violation>();
            warnings = new ArrayList<Violation>();
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public String getResourceFqdn() {
            return resourceFqdn;
        }

        public void setResourceFqdn(String resourceFqdn) {
            this.resourceFqdn = resourceFqdn;
        }

        public List<Violation> getViolations() {
            return violations;
        }

        public void setViolations(List<Violation> violations) {
            this.violations = violations;
        }

        public void putViolation(Violation violation) {
            this.violations.add(violation);
        }

        public List<Violation> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<Violation> warnings) {
            this.warnings = warnings;
        }

        public void putWarning(Violation warning) {
            this.warnings.add(warning);
        }
    }
}
