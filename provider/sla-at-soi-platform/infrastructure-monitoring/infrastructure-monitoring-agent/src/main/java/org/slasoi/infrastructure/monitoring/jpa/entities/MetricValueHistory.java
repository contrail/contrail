/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/MetricValueHistory.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "metric_value_history")
@NamedQueries({
        @NamedQuery(name = "MetricValueHistory.findAll", query = "SELECT m FROM MetricValueHistory m"),
        @NamedQuery(name = "MetricValueHistory.findByMetricId", query = "SELECT m FROM MetricValueHistory m WHERE m.metricValueHistoryPK.metricId = :metricId")})
public class MetricValueHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MetricValueHistoryPK metricValueHistoryPK;
    @Basic(optional = false)
    @Column(name = "end_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @Column(name = "value", length = 32)
    private String value;
    @JoinColumn(name = "metric_id", referencedColumnName = "metric_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metric metric;

    public MetricValueHistory() {
    }

    public MetricValueHistory(MetricValueHistoryPK metricValueHistoryPK) {
        this.metricValueHistoryPK = metricValueHistoryPK;
    }

    public MetricValueHistory(MetricValueHistoryPK metricValueHistoryPK, Date endTime) {
        this.metricValueHistoryPK = metricValueHistoryPK;
        this.endTime = endTime;
    }

    public MetricValueHistory(int metricId, Date startTime) {
        this.metricValueHistoryPK = new MetricValueHistoryPK(metricId, startTime);
    }

    public MetricValueHistoryPK getMetricValueHistoryPK() {
        return metricValueHistoryPK;
    }

    public void setMetricValueHistoryPK(MetricValueHistoryPK metricValueHistoryPK) {
        this.metricValueHistoryPK = metricValueHistoryPK;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Date getStartTime() {
        return metricValueHistoryPK.getStartTime();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (metricValueHistoryPK != null ? metricValueHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MetricValueHistory)) {
            return false;
        }
        MetricValueHistory other = (MetricValueHistory) object;
        if ((this.metricValueHistoryPK == null && other.metricValueHistoryPK != null) || (this.metricValueHistoryPK != null && !this.metricValueHistoryPK.equals(other.metricValueHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistory[metricValueHistoryPK=" + metricValueHistoryPK + "]";
    }

}
