/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2021 $
 * @lastrevision $Date: 2011-06-03 17:22:37 +0200 (pet, 03 jun 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GetServicesInfoResponse.ja $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.VmStateEnum;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetServicesInfoResponse extends PubSubResponse {
    private List<ServiceInfo> services;

    public GetServicesInfoResponse() {
        super.setResponseType("GetServicesInfoResponse");
        services = new ArrayList<ServiceInfo>();
    }

    public List<ServiceInfo> getServices() {
        return services;
    }

    public void setServices(List<ServiceInfo> services) {
        this.services = services;
    }

    public void addService(ServiceInfo service) {
        this.services.add(service);
    }

    public static GetServicesInfoResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, GetServicesInfoResponse.class);
    }

    public String toJsonString() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public static class ServiceInfo {
        private String name;
        private String serviceUri;
        private SLAComplianceEnum state;
        private Date createdDate;
        private int resourceCount;
        private String totalUptime;
        private String totalDowntime;
        private List<ResourceInfo> resources;

        public ServiceInfo() {
            resources = new ArrayList<ResourceInfo>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getServiceUri() {
            return serviceUri;
        }

        public void setServiceUri(String serviceUri) {
            this.serviceUri = serviceUri;
        }

        public SLAComplianceEnum getState() {
            return state;
        }

        public void setState(SLAComplianceEnum state) {
            this.state = state;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public int getResourceCount() {
            return resourceCount;
        }

        public void setResourceCount(int resourceCount) {
            this.resourceCount = resourceCount;
        }

        public String getTotalUptime() {
            return totalUptime;
        }

        public void setTotalUptime(String totalUptime) {
            this.totalUptime = totalUptime;
        }

        public String getTotalDowntime() {
            return totalDowntime;
        }

        public void setTotalDowntime(String totalDowntime) {
            this.totalDowntime = totalDowntime;
        }

        public List<ResourceInfo> getResources() {
            return resources;
        }

        public void setResources(List<ResourceInfo> resources) {
            this.resources = resources;
        }

        public void addResource(ResourceInfo resource) {
            this.resources.add(resource);
        }
    }

    public static class ResourceInfo {
        private String name;
        private String fqdn;
        private VmStateEnum state;
        private String cpuCores;
        private String cpuSpeed;
        private String memory;
        private boolean persistent;
        private String imageTemplateName;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public VmStateEnum getState() {
            return state;
        }

        public void setState(VmStateEnum state) {
            this.state = state;
        }

        public String getCpuCores() {
            return cpuCores;
        }

        public void setCpuCores(String cpuCores) {
            this.cpuCores = cpuCores;
        }

        public String getCpuSpeed() {
            return cpuSpeed;
        }

        public void setCpuSpeed(String cpuSpeed) {
            this.cpuSpeed = cpuSpeed;
        }

        public String getMemory() {
            return memory;
        }

        public void setMemory(String memory) {
            this.memory = memory;
        }

        public boolean isPersistent() {
            return persistent;
        }

        public void setPersistent(boolean persistent) {
            this.persistent = persistent;
        }

        public String getImageTemplateName() {
            return imageTemplateName;
        }

        public void setImageTemplateName(String imageTemplateName) {
            this.imageTemplateName = imageTemplateName;
        }
    }
}
