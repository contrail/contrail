/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/handlers/ConfigureMonitoringHandler $
 */

package org.slasoi.infrastructure.monitoring.pubsub.handlers;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.VmManager;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RemoveServiceRequest;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigureMonitoringHandler {
    private static Logger log = Logger.getLogger(ConfigureMonitoringHandler.class);

    public static PubSubResponse registerService(RegisterServiceRequest registerServiceRequest) throws Exception {
        log.trace("registerService() started.");
        // store service
        log.trace("Registering service " + registerServiceRequest.getServiceName());
        Service service = new Service();
        service.setServiceName(registerServiceRequest.getServiceName());
        service.setServiceUrl(registerServiceRequest.getServiceUrl());
        service.setStartDate(new Date());
        ServiceManager.getInstance().create(service);

        // store virtual machines
        List<Vm> vmList = new ArrayList<Vm>();
        for (RegisterServiceRequest.Vm vm : registerServiceRequest.getVmList()) {
            log.trace("Registering vm " + vm.getFqdn());
            if (vm.getName() == null && vm.getFqdn() != null) {
                log.trace("Vm name is not given. Trying to extract it from the fqdn.");
                Pattern pattern = Pattern.compile("^([\\w-]+)\\.");
                Matcher m = pattern.matcher(vm.getFqdn());
                if (m.find()) {
                    vm.setName(m.group(1));
                }
                else {
                    throw new Exception("Invalid FQDN of the vm: " + vm.getFqdn());
                }
            }
            Vm vmJpa = new Vm();
            vmJpa.setName(vm.name);
            vmJpa.setFqdn(vm.fqdn);
            vmJpa.setClusterName(vm.clusterName);
            vmJpa.setService(service);
            VmManager.getInstance().create(vmJpa);
            vmList.add(vmJpa);
        }
        service.setVmList(vmList);

        log.trace("Registering metrics...");
        registerMetrics(service, registerServiceRequest);
        log.trace("Metrics registered.");

        PubSubResponse response = new PubSubResponse();
        response.setInReplyTo(registerServiceRequest.getMessageId());
        response.setResponseType("RegisterServiceResponse");
        response.setStatus(PubSubResponse.Status.OK);
        response.setTimestamp(new Date());

        log.info("Monitoring request processed successfully. Service '" + service.getServiceName() + "' is now being monitored.");
        log.trace("registerService() finished successfully.");
        return response;
    }

    private static void registerMetrics(Service service, RegisterServiceRequest registerServiceRequest) throws Exception {
        Set<MetricTypeEnum> registeredAgrTerms;

        registeredAgrTerms = new HashSet<MetricTypeEnum>();
        for (RegisterServiceRequest.AgreementTerm agreementTerm : registerServiceRequest.getAgreementTerms()) {
            MetricTypeEnum agreementTermType;
            try {
                agreementTermType = MetricTypeEnum.fromString(agreementTerm.getGuaranteedTerm());
            }
            catch (Exception e) {
                log.warn(String.format("Agreement term '%s' is not supported.", agreementTerm.getGuaranteedTerm()));
                continue;
            }

            if (registeredAgrTerms.contains(agreementTermType)) {
                throw new Exception("Agreement term " + agreementTerm.getGuaranteedTerm() + " is duplicated.");
            }
            registerServiceMetric(service, agreementTerm, agreementTermType);
            registeredAgrTerms.add(agreementTermType);
        }

        for (RegisterServiceRequest.Vm vm : registerServiceRequest.getVmList()) {
            Vm vmJpa = VmManager.getInstance().findVmByFqdn(vm.getFqdn());
            registeredAgrTerms = new HashSet<MetricTypeEnum>();
            for (RegisterServiceRequest.AgreementTerm agreementTerm : vm.getAgreementTerms()) {
                MetricTypeEnum agreementTermType;
                try {
                    agreementTermType = MetricTypeEnum.fromString(agreementTerm.getGuaranteedTerm());
                }
                catch (Exception e) {
                    log.warn(String.format("Agreement term '%s' is not supported.", agreementTerm.getGuaranteedTerm()));
                    continue;
                }

                if (registeredAgrTerms.contains(agreementTermType)) {
                    throw new Exception("Agreement term " + agreementTerm.getGuaranteedTerm() + " is duplicated.");
                }
                registerVmMetric(vmJpa, agreementTerm, agreementTermType);
                registeredAgrTerms.add(agreementTermType);
            }

            MetricTypeEnum[] additionalMetrics = {MetricTypeEnum.VM_AVAILABILITY_STATUS,
                    MetricTypeEnum.VM_AVAILABILITY,
                    MetricTypeEnum.VM_MEMORY_SIZE_USED,
                    MetricTypeEnum.VM_CPU_SPEED_USED,
                    MetricTypeEnum.VM_SLA_COMPLIANCE};
            for (MetricTypeEnum additionalMetric : additionalMetrics) {
                if (!registeredAgrTerms.contains(additionalMetric)) {
                    registerVmMetric(additionalMetric, vmJpa, null);
                }
            }
        }

        registerServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_STATUS, service, null);
        registerServiceMetric(MetricTypeEnum.SERVICE_SLA_COMPLIANCE, service, null);

        // TODO: extract constraint expressions from MSC
    }

    private static void registerServiceMetric(Service service, RegisterServiceRequest.AgreementTerm agreementTerm,
                                              MetricTypeEnum metricTypeEnum) throws Exception {
        MetricType metricType = metricTypeEnum.getMetricType();
        if (metricType.getTarget() != MetricType.Target.SERVICE) {
            throw new Exception(String.format("Invalid agreement term: %s is not service level term.", metricType.getName()));
        }
        Metric metric = createMetric(agreementTerm, metricTypeEnum);
        metric.setService(service);
        MetricManager.getInstance().create(metric);
    }

    private static void registerVmMetric(Vm vm, RegisterServiceRequest.AgreementTerm agreementTerm,
                                         MetricTypeEnum metricTypeEnum) throws Exception {
        MetricType metricType = metricTypeEnum.getMetricType();
        if (metricType.getTarget() != MetricType.Target.VM) {
            throw new Exception(String.format("Invalid agreement term: %s is not vm level term.", metricType.getName()));
        }
        Metric metric = createMetric(agreementTerm, metricTypeEnum);
        metric.setVm(vm);
        MetricManager.getInstance().create(metric);
    }

    private static Metric createMetric(RegisterServiceRequest.AgreementTerm agreementTerm,
                                       MetricTypeEnum metricTypeEnum) throws Exception {
        Metric metric = Metric.create(metricTypeEnum);
        MetricType metricType = metric.getMetricType();

        if (metricType.getType() == MetricType.Type.CONFIG) {
            // config QoS term
            metric.setConfigValue(agreementTerm.getViolationThreshold());
        }
        else {
            // set violation threshold
            metric.setViolationThreshold(agreementTerm.getViolationThreshold());

            // set warning threshold
            if (agreementTerm.getWarningThreshold() != null) {
                metric.setWarningThreshold(agreementTerm.getWarningThreshold());
            }
            else if (metric.getViolationThreshold() != null) {
                Object warningThreshold = metric.estimateWarningThreshold(metric.getViolationThreshold());
                metric.setWarningThreshold(warningThreshold != null ? warningThreshold.toString() : null);
            }
        }

        return metric;
    }

    private static void registerServiceMetric(MetricTypeEnum metricTypeEnum, Service service, String constraintExpression) throws Exception {
        Metric metric = Metric.create(metricTypeEnum);
        metric.setMetricType(metricTypeEnum.getMetricType());
        metric.setService(service);
        metric.setViolationThreshold(constraintExpression);
        MetricManager.getInstance().create(metric);
    }

    private static void registerVmMetric(MetricTypeEnum metricTypeEnum, Vm vm, String constraintExpression) throws Exception {
        Metric metric = Metric.create(metricTypeEnum);
        metric.setMetricType(metricTypeEnum.getMetricType());
        metric.setVm(vm);
        metric.setViolationThreshold(constraintExpression);
        MetricManager.getInstance().create(metric);
    }

    public static PubSubResponse removeService(RemoveServiceRequest request) throws Exception {
        log.trace("removeService() started.");
        Service service;
        try {
            service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
        }
        catch (Exception e) {
            throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
        }

        if (request.isRemoveFromDB()) {
            log.trace("Acquiring the lock.");
            synchronized (InfrastructureMonitoringAgent.getInstance().getLock()) {
                log.trace("Lock acquired.");

                log.trace(String.format("Removing the service '%s' and all of its history from the DB.",
                        service.getServiceName()));
                List<Integer> metricIdList = new ArrayList<Integer>();
                for (Metric m : service.getAllMetrics()) {
                    metricIdList.add(m.getMetricId());
                }

                EntityManagerFactory emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
                EntityManager em = emf.createEntityManager();
                try {
                    em.getTransaction().begin();
                    Query query;

                    if (metricIdList.size() > 0) {
                        query = em.createQuery("DELETE FROM MetricValue mv WHERE mv.metricId IN :metricIdList");
                        query.setParameter("metricIdList", metricIdList);
                        query.executeUpdate();

                        query = em.createQuery("DELETE FROM MetricValueHistory h WHERE h.metricValueHistoryPK.metricId IN" +
                                " :metricIdList");
                        query.setParameter("metricIdList", metricIdList);
                        query.executeUpdate();

                        query = em.createQuery("SELECT v.violationId FROM Violation v WHERE v.metric.metricId IN :metricIdList");
                        query.setParameter("metricIdList", metricIdList);
                        List<Integer> violationIdList = query.getResultList();

                        if (violationIdList.size() > 0) {
                            query = em.createQuery("DELETE FROM Violation v WHERE v.violationId IN :violationIdList");
                            query.setParameter("violationIdList", violationIdList);
                            query.executeUpdate();
                        }

                        query = em.createQuery("DELETE FROM Metric m WHERE m.metricId IN :metricIdList");
                        query.setParameter("metricIdList", metricIdList);
                        query.executeUpdate();
                    }

                    if (service.getVmList().size() > 0) {
                        query = em.createQuery("DELETE FROM Vm vm WHERE vm.vmId IN :vmIdList");
                        List<Integer> vmIdList = new ArrayList<Integer>();
                        for (Vm vm : service.getVmList()) {
                            vmIdList.add(vm.getVmId());
                        }
                        query.setParameter("vmIdList", vmIdList);
                        query.executeUpdate();
                    }

                    query = em.createQuery("DELETE FROM Service s WHERE s.serviceId = :serviceId");
                    query.setParameter("serviceId", service.getServiceId());
                    query.executeUpdate();

                    em.getTransaction().commit();
                    log.info(String.format("Monitoring of service '%s' has been stopped, " +
                            "all service data has been deleted.",
                            service.getServiceName()));
                }
                finally {
                    em.close();
                }
            }
        }

        PubSubResponse response = new PubSubResponse();
        response.setInReplyTo(request.getMessageId());
        response.setResponseType("RemoveServiceResponse");
        response.setStatus(PubSubResponse.Status.OK);
        response.setTimestamp(new Date());
        log.trace("removeService() finished successfully.");
        return response;
    }
}
