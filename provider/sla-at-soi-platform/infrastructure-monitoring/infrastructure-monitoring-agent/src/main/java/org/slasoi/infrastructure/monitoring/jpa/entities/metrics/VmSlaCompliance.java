/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2869 $
 * @lastrevision $Date: 2011-07-29 09:12:57 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/VmSlaCompliance.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("VM_SLA_COMPLIANCE")
public class VmSlaCompliance extends Metric {
    private static final Logger log = Logger.getLogger(VmSlaCompliance.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Vm vm = this.getVm();
        List<Metric> metrics = vm.getMetricList();
        log.trace("Computing metric VM_SLA_COMPLIANCE of the VM " + vm.getFqdn());
        SLAComplianceEnum vmSlaCompliance = SLAComplianceEnum.COMPLIANT;
        for (Metric metric : metrics) {
            MetricValue metricValue = metric.getMetricValue();
            if (metricValue == null) {
                continue;
            }

            // SLA is violated:
            // if QoS term is critical and it is in violation (regardless of service restriction window)
            // if QoS term is uncritical and it is in violation which happened during working hours (service restriction window)
            SLAComplianceEnum slaCompliance = metricValue.getSlaCompliance();
            ViolationSeverity violationSeverity = metricValue.getViolationSeverity();
            MetricType metricType = metric.getMetricType();
            if (metricType.isCritical()) {
                // QoS term is critical (e.g. VM_DATA_ENCRYPTION)
                if (slaCompliance != null && slaCompliance.getSeverity() > vmSlaCompliance.getSeverity()) {
                    vmSlaCompliance = slaCompliance;
                }
            }
            else {
                // QoS term is not critical (e.g. VM Memory Size)
                if (slaCompliance != null && slaCompliance.getSeverity() > vmSlaCompliance.getSeverity() &&
                        violationSeverity == ViolationSeverity.PUNISHABLE) {
                    vmSlaCompliance = slaCompliance;
                }
            }

            if (vmSlaCompliance == SLAComplianceEnum.VIOLATION) {
                break; // no need to check further - vm is in violation
            }
        }
        log.trace(String.format("VM_SLA_COMPLIANCE(%s) = %s", vm.getFqdn(), vmSlaCompliance));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = MetricValueManager.getInstance().storeMetricValue(this, vmSlaCompliance, metricsTimestamp);
        MetricValueHistoryManager.getInstance().storeValue(this, vmSlaCompliance, metricsTimestamp);
    }

    @Override
    public SLAComplianceEnum parseValue(String value) throws InvalidMetricValueException {
        return SLAComplianceEnum.fromString(value);
    }

    @Override
    public String formatValue(Object value) {
        return null;
    }

    @Override
    public Object getViolationThresholdValue() {
        return null;
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
