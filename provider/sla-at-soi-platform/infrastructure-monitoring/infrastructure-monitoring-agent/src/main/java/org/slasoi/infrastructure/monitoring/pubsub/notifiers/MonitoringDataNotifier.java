/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1494 $
 * @lastrevision $Date: 2011-04-26 22:17:22 +0200 (tor, 26 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/notifiers/MonitoringDataNotifier.ja $
 */

package org.slasoi.infrastructure.monitoring.pubsub.notifiers;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.managers.VmManager;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceEventsResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.ServiceEventNotification;

public class MonitoringDataNotifier {
    private static Logger log = Logger.getLogger(MonitoringDataNotifier.class);

    public static void publishServiceEventNotification(AuditRecord auditRecord) throws Exception {
        log.trace("Publishing service event (AuditRecord) notification to pubsub...");
        ServiceEventNotification message = new ServiceEventNotification();

        GetServiceEventsResponse.Event event = new GetServiceEventsResponse.Event();
        message.setEvent(event);
        event.setSource(auditRecord.getSource());
        event.setTimestamp(auditRecord.getTimestamp());
        event.setType(GetServiceEventsResponse.EventType.AUDIT_RECORD);
        event.setUserId(auditRecord.getUserId());
        event.setDescription(auditRecord.getDescription());

        if (auditRecord.getFqdn() != null) {
            Vm vm = VmManager.getInstance().findVmByFqdn(auditRecord.getFqdn());
            if (vm != null) {
                event.setServiceUri(vm.getService().getServiceUrl());
            }
        }

        publishMessage(message.toJson());
        log.trace("AuditRecord published successfully.");
    }

    private static void publishMessage(String payload) throws MessagingException {
        log.trace("Publishing message to monitoringDataRequestChannel: " + payload);
        PubSubManager pubSubManager = InfrastructureMonitoringAgent.getInstance().getPubSubManager();
        Channel monitoringDataRequestChannel = InfrastructureMonitoringAgent.getInstance().getMonitoringDataRequestChannel();
        PubSubMessage pubSubMessage = new PubSubMessage(monitoringDataRequestChannel.getName(), payload);
        pubSubManager.publish(pubSubMessage);
    }
}
