/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2978 $
 * @lastrevision $Date: 2011-08-12 15:39:20 +0200 (pet, 12 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ClusterVcpuUsage.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("CLUSTER_VCPU_USAGE")
public class ClusterVcpuUsage extends Metric {
    private static final Logger log = Logger.getLogger(ClusterVcpuUsage.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        log.trace(String.format("Computing metric CLUSTER_VCPU_USAGE."));
        String vcpuUsage;
        try {
            int vcpuTotalCapacity = 0;
            int vcpuUsed = 0;
            for (String hostFqdn : monitoringEngine.getAllHostMachines()) {
                // compute host vCPU capacity
                MetricData hostCores = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_CORES_COUNT);
                // vCPU capacity of a host machine is number of cores multiplied by 2
                // the factor is defined in the Tashi configuration
                int hostVcpuCapacity = (Integer) hostCores.getValue() * 2;
                vcpuTotalCapacity += hostVcpuCapacity;

                // compute number of used vCPUs on the host
                for (String vmFqdn : monitoringEngine.getGuestMachines(hostFqdn)) {
                    MetricData vmCores = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.CPU_CORES_COUNT);
                    vcpuUsed += (Integer) vmCores.getValue();
                }
            }
            vcpuUsage = String.format("%d/%d", vcpuUsed, vcpuTotalCapacity);
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to compute metric CLUSTER_VCPU_USAGE: %s", e.getMessage()));
        }

        log.trace(String.format("CLUSTER_VCPU_USAGE = %s", vcpuUsage));
        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(vcpuUsage, metricsTimestamp);
        storeMetricHistoryValue(vcpuUsage, metricsTimestamp);
    }

    @Override
    public String parseValue(String value) throws InvalidMetricValueException {
        return value;
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public Object getViolationThresholdValue() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
