/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2982 $
 * @lastrevision $Date: 2011-08-16 15:12:45 +0200 (tor, 16 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/listeners/MonitoringDataRequestList $
 */

package org.slasoi.infrastructure.monitoring.pubsub.listeners;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.pubsub.*;
import org.slasoi.infrastructure.monitoring.pubsub.handlers.MonitoringDataRequestHandler;
import org.slasoi.infrastructure.monitoring.pubsub.messages.*;
import org.slasoi.infrastructure.monitoring.utils.Utils;

import java.util.Date;

public class MonitoringDataRequestListener implements org.slasoi.common.messaging.pubsub.MessageListener {
    private static Logger log = Logger.getLogger(MonitoringDataRequestListener.class);

    public void processMessage(MessageEvent messageEvent) {
        String payload = null;
        PubSubRequest request = null;
        try {
            PubSubMessage message = messageEvent.getMessage();
            payload = message.getPayload();
            log.trace(String.format("New message arrived on the channel %s. Message payload: %s", message.getChannelName(), Utils.shortenString(payload, 1000)));
            JSONObject o;
            try {
                request = PubSubRequest.fromJson(payload);
            }
            catch (Exception e) {
                throw new Exception(String.format("Invalid pubsub message received: %s", e.getMessage()));
            }

            log.trace("Request type: " + request.getRequestType());

            PubSubResponse response;
            if (request.getRequestType().equals("SysInfoRequest")) {
                SysInfoRequest sysInfoRequest = SysInfoRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(sysInfoRequest);
            }
            else if (request.getRequestType().equals("InfrastructureInfoRequest")) {
                InfrastructureInfoRequest infrastructureInfoRequest = InfrastructureInfoRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(infrastructureInfoRequest);
            }
            else if (request.getRequestType().equals("GetServicesInfoRequest")) {
                GetServicesInfoRequest servicesInfoRequest = GetServicesInfoRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(servicesInfoRequest);
            }
            else if (request.getRequestType().equals("GetMetricHistoryRequest")) {
                GetMetricHistoryRequest getMetricHistoryRequest = GetMetricHistoryRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(getMetricHistoryRequest);
            }
            else if (request.getRequestType().equals("GetServiceViolationsFrequencyRequest")) {
                GetServiceViolationsFrequencyRequest serviceViolationsFrequencyReq = GetServiceViolationsFrequencyRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(serviceViolationsFrequencyReq);
            }
            else if (request.getRequestType().equals("GetServiceViolationsRequest")) {
                GetServiceViolationsRequest serviceViolationsReq = GetServiceViolationsRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(serviceViolationsReq);
            }
            else if (request.getRequestType().equals("GetServiceSLASummaryRequest")) {
                GetServiceSLASummaryRequest slaSummaryReq = GetServiceSLASummaryRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(slaSummaryReq);
            }
            else if (request.getRequestType().equals("GetServiceEventsRequest")) {
                GetServiceEventsRequest srvEventsReq = GetServiceEventsRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(srvEventsReq);
            }
            else if (request.getRequestType().equals("GetHostsInfoRequest")) {
                response = MonitoringDataRequestHandler.processRequest(request);
            }
            else if (request.getRequestType().equals("GetSchedulerEventsRequest")) {
                GetSchedulerEventsRequest schEventsReq = GetSchedulerEventsRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(schEventsReq);
            }
            else if (request.getRequestType().equals("GenerateReportRequest")) {
                GenerateReportRequest genReportRequest = GenerateReportRequest.fromJson(payload);
                response = MonitoringDataRequestHandler.processRequest(genReportRequest);
            }
            else {
                return;
                //throw new Exception(String.format("Request type '%s' is not supported.", request.getRequestType()));
            }

            String responsePayload = response.toJson();
            publishMessage(responsePayload);

            log.trace(String.format("Request %s processed successfully.", request.getMessageId()));
            if (log.isTraceEnabled()) {
                String text = response.toJson();
                log.trace("Response:\n" + text);
            }
        }
        catch (Exception e) {
            log.error(String.format("Error encountered while processing request %s: %s", Utils.shortenString(payload, 1000), e.getMessage()));
            ErrorResponse errorResponse = new ErrorResponse();
            String inReplyTo = "";
            if (request != null) {
                inReplyTo = request.getMessageId();
            }
            errorResponse.setInReplyTo(inReplyTo);
            errorResponse.setTimestamp(new Date());
            errorResponse.setExceptionMessage((e.getMessage() != null) ? e.getMessage() : e.toString());

            try {
                String responsePayload = errorResponse.toJson().toString();
                publishMessage(responsePayload);
            }
            catch (Exception e1) {
                log.error("Error encountered while sending error response: " + e.getMessage());
            }
        }
    }

    private void publishMessage(String payload) throws MessagingException {
        PubSubManager pubSubManager = InfrastructureMonitoringAgent.getInstance().getPubSubManager();
        Channel monitoringDataRequestChannel = InfrastructureMonitoringAgent.getInstance().getMonitoringDataRequestChannel();
        PubSubMessage pubSubMessage = new PubSubMessage(monitoringDataRequestChannel.getName(), payload);
        pubSubManager.publish(pubSubMessage);
    }
}
