/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2997 $
 * @lastrevision $Date: 2011-08-22 17:10:42 +0200 (pon, 22 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceMttf.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.computation.MetricsComputationHelper;
import org.slasoi.infrastructure.monitoring.computation.ReportingPeriod;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.qos.ViolationHandler;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("SERVICE_MTTF")
public class ServiceMttf extends Metric {
    private static final Logger log = Logger.getLogger(ServiceMttf.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        Service service = this.getService();
        log.trace("Computing metric SERVICE_MTTF of the service " + service.getServiceName());
        SLAComplianceEnum prevSLACompliance = getPrevSLACompliance();

        Metric availStatusMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_STATUS, service);
        ReportingPeriod reportingPeriod = service.getReportingPeriod();
        Double mttf = MetricsComputationHelper.computeMttf(availStatusMetric, reportingPeriod);
        log.trace(String.format("SERVICE_MTTF(%s) = %s", service.getServiceName(), mttf));

        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(mttf, metricsTimestamp);
        if (mttf != null) {
            storeMetricHistoryValue(mttf, reportingPeriod);
        }
        else {
            return;
        }

        // validate metric value against SLA
        if (this.getViolationThreshold() != null) {
            double violationThreshold = Double.parseDouble(this.getViolationThreshold());
            double actual = metricValue.getDoubleValue();
            SLAComplianceEnum slaCompliance = SLAComplianceEnum.COMPLIANT;

            if (actual < violationThreshold) {
                slaCompliance = SLAComplianceEnum.VIOLATION;
                log.warn(String.format("SLA violation detected at the service %s: MTTF is %.0f, should be more than %.0f.",
                        service.getServiceUrl(), actual, violationThreshold));
            }
            else if (this.getWarningThreshold() != null) {
                double warningThreshold = Double.parseDouble(this.getWarningThreshold());
                if (actual < warningThreshold) {
                    slaCompliance = SLAComplianceEnum.WARNING;
                    log.warn(String.format("Warning of SLA violation detected at the service %s: MTTF is %.0f, should be more than %.0f.",
                            service.getServiceUrl(), actual, warningThreshold));
                }
            }

            updateMetricSLACompliance(this, metricValue, slaCompliance, metricsTimestamp);
            ViolationHandler.handleViolation(this, metricValue, slaCompliance, prevSLACompliance);
        }
    }

    @Override
    public Double parseValue(String value) throws InvalidMetricValueException {
        return Double.parseDouble(value);
    }

    @Override
    public String formatValue(Object value) {
        if (value == null) {
            return null;
        }
        else {
            return String.format("%.2f", (Double) value);
        }
    }

    @Override
    public String displayValue(String value) throws InvalidMetricValueException {
        Double v = parseValue(value);
        if (v.isInfinite()) {
            return "No failure yet.";
        }
        else {
            return String.format("%.2f %s", v, this.getMetricType().getMetricUnit());
        }
    }

    @Override
    public Object getViolationThresholdValue() {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            return Double.parseDouble(this.getViolationThreshold());
        }
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        if (this.getViolationThreshold() == null) {
            return null;
        }
        else {
            String unit = this.getMetricType().getMetricUnit();
            return String.format("value < %.2f %s", (Double) getViolationThresholdValue(), unit);
        }
    }

    @Override
    public Object getWarningThresholdValue() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            return Double.parseDouble(this.getWarningThreshold());
        }
    }

    @Override
    public String getWarningCondition() {
        if (this.getWarningThreshold() == null) {
            return null;
        }
        else {
            String unit = this.getMetricType().getMetricUnit();
            return String.format("%.2f %s <= value < %.2f %s", (Double) getViolationThresholdValue(), unit,
                    (Double) getWarningThresholdValue(), unit);
        }
    }

    @Override
    public Double estimateWarningThreshold(Object violationThreshold) {
        if (violationThreshold != null) {
            double v = Double.parseDouble(violationThreshold.toString());
            return v * 1.25;
        }
        else {
            return null;
        }
    }
}
