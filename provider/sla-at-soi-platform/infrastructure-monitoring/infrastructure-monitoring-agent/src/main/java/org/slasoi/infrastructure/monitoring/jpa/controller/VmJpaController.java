/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/VmJpaController.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class VmJpaController {

    public VmJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vm vm) {
        if (vm.getMetricList() == null) {
            vm.setMetricList(new ArrayList<Metric>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Service service = vm.getService();
            if (service != null) {
                service = em.getReference(service.getClass(), service.getServiceId());
                vm.setService(service);
            }
            List<Metric> attachedMetricList = new ArrayList<Metric>();
            for (Metric metricListMetricToAttach : vm.getMetricList()) {
                metricListMetricToAttach = em.getReference(metricListMetricToAttach.getClass(), metricListMetricToAttach.getMetricId());
                attachedMetricList.add(metricListMetricToAttach);
            }
            vm.setMetricList(attachedMetricList);
            em.persist(vm);
            if (service != null) {
                service.getVmList().add(vm);
                service = em.merge(service);
            }
            for (Metric metricListMetric : vm.getMetricList()) {
                Vm oldVmOfMetricListMetric = metricListMetric.getVm();
                metricListMetric.setVm(vm);
                metricListMetric = em.merge(metricListMetric);
                if (oldVmOfMetricListMetric != null) {
                    oldVmOfMetricListMetric.getMetricList().remove(metricListMetric);
                    oldVmOfMetricListMetric = em.merge(oldVmOfMetricListMetric);
                }
            }
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vm vm) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vm persistentVm = em.find(Vm.class, vm.getVmId());
            Service serviceOld = persistentVm.getService();
            Service serviceNew = vm.getService();
            List<Metric> metricListOld = persistentVm.getMetricList();
            List<Metric> metricListNew = vm.getMetricList();
            if (serviceNew != null) {
                serviceNew = em.getReference(serviceNew.getClass(), serviceNew.getServiceId());
                vm.setService(serviceNew);
            }
            List<Metric> attachedMetricListNew = new ArrayList<Metric>();
            for (Metric metricListNewMetricToAttach : metricListNew) {
                metricListNewMetricToAttach = em.getReference(metricListNewMetricToAttach.getClass(), metricListNewMetricToAttach.getMetricId());
                attachedMetricListNew.add(metricListNewMetricToAttach);
            }
            metricListNew = attachedMetricListNew;
            vm.setMetricList(metricListNew);
            vm = em.merge(vm);
            if (serviceOld != null && !serviceOld.equals(serviceNew)) {
                serviceOld.getVmList().remove(vm);
                serviceOld = em.merge(serviceOld);
            }
            if (serviceNew != null && !serviceNew.equals(serviceOld)) {
                serviceNew.getVmList().add(vm);
                serviceNew = em.merge(serviceNew);
            }
            for (Metric metricListOldMetric : metricListOld) {
                if (!metricListNew.contains(metricListOldMetric)) {
                    metricListOldMetric.setVm(null);
                    metricListOldMetric = em.merge(metricListOldMetric);
                }
            }
            for (Metric metricListNewMetric : metricListNew) {
                if (!metricListOld.contains(metricListNewMetric)) {
                    Vm oldVmOfMetricListNewMetric = metricListNewMetric.getVm();
                    metricListNewMetric.setVm(vm);
                    metricListNewMetric = em.merge(metricListNewMetric);
                    if (oldVmOfMetricListNewMetric != null && !oldVmOfMetricListNewMetric.equals(vm)) {
                        oldVmOfMetricListNewMetric.getMetricList().remove(metricListNewMetric);
                        oldVmOfMetricListNewMetric = em.merge(oldVmOfMetricListNewMetric);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = vm.getVmId();
                if (findVm(id) == null) {
                    throw new NonexistentEntityException("The vm with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vm vm;
            try {
                vm = em.getReference(Vm.class, id);
                vm.getVmId();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vm with id " + id + " no longer exists.", enfe);
            }
            Service service = vm.getService();
            if (service != null) {
                service.getVmList().remove(vm);
                service = em.merge(service);
            }
            List<Metric> metricList = vm.getMetricList();
            for (Metric metricListMetric : metricList) {
                metricListMetric.setVm(null);
                metricListMetric = em.merge(metricListMetric);
            }
            em.remove(vm);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vm> findVmEntities() {
        return findVmEntities(true, -1, -1);
    }

    public List<Vm> findVmEntities(int maxResults, int firstResult) {
        return findVmEntities(false, maxResults, firstResult);
    }

    private List<Vm> findVmEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vm.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public Vm findVm(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vm.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getVmCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vm> rt = cq.from(Vm.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
