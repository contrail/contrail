/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2869 $
 * @lastrevision $Date: 2011-07-29 09:12:57 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/RegisterServiceRequest.jav $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubRequest;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

public class RegisterServiceRequest extends PubSubRequest {
    private String serviceName;
    private String serviceUrl;
    private List<Vm> vmList;
    String monSystemConfSerialized;
    List<AgreementTerm> agreementTerms;

    public RegisterServiceRequest() {
        super.setRequestType("RegisterServiceRequest");
        vmList = new ArrayList<Vm>();
    }

    public static RegisterServiceRequest fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, RegisterServiceRequest.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public List<Vm> getVmList() {
        return vmList;
    }

    public void setVmList(List<Vm> vmList) {
        this.vmList = vmList;
    }

    public void putVm(Vm vm) {
        this.vmList.add(vm);
    }

    public String getMonSystemConfSerialized() {
        return monSystemConfSerialized;
    }

    public void setMonSystemConfSerialized(String monSystemConfSerialized) {
        this.monSystemConfSerialized = monSystemConfSerialized;
    }

    public List<AgreementTerm> getAgreementTerms() {
        return agreementTerms;
    }

    public void setAgreementTerms(List<AgreementTerm> agreementTerms) {
        this.agreementTerms = agreementTerms;
    }

    public void addAgreementTerm(AgreementTerm agreementTerm) {
        if (agreementTerms == null) {
            agreementTerms = new ArrayList<AgreementTerm>();
        }
        agreementTerms.add(agreementTerm);
    }

    public static class Vm {
        public String name;
        public String fqdn;
        public String clusterName;
        List<AgreementTerm> agreementTerms;

        public Vm() {
        }

        public Vm(String name, String fqdn) {
            this.name = name;
            this.fqdn = fqdn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public String getClusterName() {
            return clusterName;
        }

        public void setClusterName(String clusterName) {
            this.clusterName = clusterName;
        }

        public List<AgreementTerm> getAgreementTerms() {
            return agreementTerms;
        }

        public void setAgreementTerms(List<AgreementTerm> agreementTerms) {
            this.agreementTerms = agreementTerms;
        }

        public void addAgreementTerm(AgreementTerm agreementTerm) {
            if (agreementTerms == null) {
                agreementTerms = new ArrayList<AgreementTerm>();
            }
            agreementTerms.add(agreementTerm);
        }
    }

    public static class AgreementTerm {
        private String guaranteedTerm;
        private String violationThreshold;
        private String warningThreshold;
        private String unit;
        private String operator;

        public AgreementTerm() {
        }

        public AgreementTerm(String guaranteedTerm, String violationThreshold, String warningThreshold, String unit) {
            this.guaranteedTerm = guaranteedTerm;
            this.violationThreshold = violationThreshold;
            this.warningThreshold = warningThreshold;
            this.unit = unit;
        }

        public AgreementTerm(MetricTypeEnum guaranteedTerm, String violationThreshold, String warningThreshold, String unit) {
            this.guaranteedTerm = guaranteedTerm.toString();
            this.violationThreshold = violationThreshold;
            this.warningThreshold = warningThreshold;
            this.unit = unit;
        }

        public String getGuaranteedTerm() {
            return guaranteedTerm;
        }

        public void setGuaranteedTerm(String guaranteedTerm) {
            this.guaranteedTerm = guaranteedTerm;
        }

        public String getViolationThreshold() {
            return violationThreshold;
        }

        public void setViolationThreshold(String violationThreshold) {
            this.violationThreshold = violationThreshold;
        }

        public String getWarningThreshold() {
            return warningThreshold;
        }

        public void setWarningThreshold(String warningThreshold) {
            this.warningThreshold = warningThreshold;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }
    }
}
