/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2982 $
 * @lastrevision $Date: 2011-08-16 15:12:45 +0200 (tor, 16 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/utils/JsonUtils.java $
 */

package org.slasoi.infrastructure.monitoring.utils;

import com.google.gson.*;
import com.lowagie.text.pdf.codec.Base64;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.log4j.Logger;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;

public class JsonUtils {
    private static Logger log = Logger.getLogger(JsonUtils.class);

    private static JsonUtils instance;
    private Gson gson;

    private JsonUtils() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

        gsonBuilder.registerTypeAdapter(ComponentMonitoringFeatures[].class,
                new ComponentMonitoringFeaturesSerializer());
        gsonBuilder.registerTypeAdapter(ComponentMonitoringFeatures[].class,
                new ComponentMonitoringFeaturesDeserializer());

        gsonBuilder.registerTypeAdapter(MonitoringSystemConfiguration.class,
                new MonitoringSystemConfigurationDeserializer());
        gsonBuilder.registerTypeAdapter(MonitoringSystemConfiguration.class,
                new MonitoringSystemConfigurationSerializer());

        gsonBuilder.registerTypeAdapter(byte[].class,
                new ByteArraySerializer());
        gsonBuilder.registerTypeAdapter(byte[].class,
                new ByteArrayDeserializer());

        gson = gsonBuilder.create();
        log.info("Gson created.");

    }

    public static JsonUtils getInstance() {
        if (instance == null) {
            instance = new JsonUtils();
        }
        return instance;
    }

    public Gson getGson() {
        return gson;
    }

    private static class DateSerializer implements JsonSerializer<Date> {

        public JsonElement serialize(Date date, Type typeOfSrc,
                                     JsonSerializationContext context) {
            JsonPrimitive dateprim = null;
            try {
                String dateString = DateUtils.format(date);
                dateprim = new JsonPrimitive(dateString);
            }
            catch (Exception e) {
                e.printStackTrace();
                log.error("DateDeserializer failed for dateString ", e);

                throw new JsonParseException(e.getMessage());
            }
            return dateprim;
        }
    }

    private static class DateDeserializer implements JsonDeserializer<Date> {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            String dateString = json.getAsJsonPrimitive().getAsString();
            try {
                return DateUtils.parse(dateString);
            }
            catch (ParseException e) {

                e.printStackTrace();
                log.error("DateDeserializer failed for dateString "
                        + dateString, e);

                throw new JsonParseException(e.getMessage());
            }
        }
    }

    private static class ComponentMonitoringFeaturesSerializer implements
            JsonSerializer<ComponentMonitoringFeatures[]> {
        public JsonElement serialize(ComponentMonitoringFeatures[] cmf,
                                     Type typeOfSrc, JsonSerializationContext context) {
            String cmfXml;
            JsonPrimitive prim = null;
            try {
                XStream xstream = new XStream(new DomDriver());

                xstream.setClassLoader(ComponentMonitoringFeatures.class
                        .getClassLoader());

                xstream
                        .alias(
                                org.slasoi.monitoring.common.features.ComponentMonitoringFeatures.class
                                        .getSimpleName(),
                                org.slasoi.monitoring.common.features.ComponentMonitoringFeatures.class);

                cmfXml = xstream.toXML(cmf);
                prim = new JsonPrimitive(cmfXml);
            }
            catch (Exception e) {

                e.printStackTrace();
                log.error("ComponentMonitoringFeaturesSerializer failed ", e);

                throw new JsonParseException(e.getMessage());
            }

            return prim;
        }
    }

    private static class ComponentMonitoringFeaturesDeserializer implements
            JsonDeserializer<ComponentMonitoringFeatures[]> {
        public ComponentMonitoringFeatures[] deserialize(JsonElement json,
                                                         Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            ComponentMonitoringFeatures[] comp = null;
            try {
                String cmfXml = json.getAsJsonPrimitive().getAsString();
                XStream xstream = new XStream(new DomDriver());

                xstream.setClassLoader(ComponentMonitoringFeatures.class
                        .getClassLoader());

                xstream
                        .alias(
                                org.slasoi.monitoring.common.features.ComponentMonitoringFeatures.class
                                        .getSimpleName(),
                                org.slasoi.monitoring.common.features.ComponentMonitoringFeatures.class);

                comp = (ComponentMonitoringFeatures[]) xstream.fromXML(cmfXml);
            }
            catch (Exception e) {
                e.printStackTrace();
                log.error("ComponentMonitoringFeaturesDeserializer failed",
                        e);

                throw new JsonParseException(e.getMessage());
            }
            return comp;
        }
    }

    private static class MonitoringSystemConfigurationSerializer
            implements
            JsonSerializer<org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration> {

        public JsonElement serialize(MonitoringSystemConfiguration cmf,
                                     Type typeOfSrc, JsonSerializationContext context) {
            JsonPrimitive prim = null;
            try {
                XStream xstream = new XStream(new DomDriver());
                xstream.setClassLoader(MonitoringSystemConfiguration.class
                        .getClassLoader());

                xstream
                        .alias(
                                org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class
                                        .getSimpleName(),
                                org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class);

                String cmfXml = xstream.toXML(cmf);
                prim = new JsonPrimitive(cmfXml);
            }
            catch (Exception e) {
                e.printStackTrace();
                log.error("MonitoringSystemConfigurationSerializer failed for "
                        + cmf, e);

                throw new JsonParseException(e.getMessage());
            }
            return prim;
        }
    }

    private static class MonitoringSystemConfigurationDeserializer implements
            JsonDeserializer<MonitoringSystemConfiguration> {

        public MonitoringSystemConfiguration deserialize(JsonElement json,
                                                         Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            MonitoringSystemConfiguration mon = null;

            try {
                String cmfXml = json.getAsJsonPrimitive().getAsString();
                XStream xstream = new XStream(new DomDriver());
                xstream.setClassLoader(MonitoringSystemConfiguration.class
                        .getClassLoader());

                xstream
                        .alias(
                                org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class
                                        .getSimpleName(),
                                org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class);
                mon = (MonitoringSystemConfiguration) xstream.fromXML(cmfXml);
            }
            catch (Exception e) {
                log.error(
                        "MonitoringSystemConfigurationDeserializer failed ", e);

                e.printStackTrace();
                throw new JsonParseException(e.getMessage());
            }
            return mon;
        }
    }

    private static class ByteArraySerializer implements JsonSerializer<byte[]> {

        public JsonElement serialize(byte[] bytes, Type typeOfSrc, JsonSerializationContext context) {
            String base64 = Base64.encodeBytes(bytes);
            return new JsonPrimitive(base64);
        }
    }

    private static class ByteArrayDeserializer implements JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            String jsonString = json.getAsJsonPrimitive().getAsString();
            return Base64.decode(jsonString);
        }
    }

}
