/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3048 $
 * @lastrevision $Date: 2011-09-05 10:26:14 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/tashisensor/TashiSensorHandler.ja $
 */

package org.slasoi.infrastructure.monitoring.monitors.tashisensor;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord;
import org.slasoi.infrastructure.monitoring.jpa.enums.*;
import org.slasoi.infrastructure.monitoring.jpa.managers.AuditRecordManager;
import org.slasoi.infrastructure.monitoring.monitors.data.*;
import org.slasoi.infrastructure.monitoring.pubsub.notifiers.MonitoringDataNotifier;
import org.slasoi.infrastructure.monitoring.utils.DateUtils;
import org.slasoi.infrastructure.monitoring.utils.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.StringWriter;
import java.util.Date;

public class TashiSensorHandler {
    private static Logger log = Logger.getLogger(TashiSensorMonitor.class);
    TashiSensorMonitor tashiSensorMonitor;

    public TashiSensorHandler(TashiSensorMonitor tashiSensorMonitor) {
        this.tashiSensorMonitor = tashiSensorMonitor;
    }

    void processVmLayoutMessage(Document xmlDoc) throws Exception {
        parseVmLayoutData(xmlDoc);
        createLayoutXml(xmlDoc);
        tashiSensorMonitor.setGotVmLayoutData(true);
    }

    private void parseVmLayoutData(Document vmLayoutDoc) throws Exception {
        log.trace("Parsing VmLayout data...");
        MonitoringData monitoringData = new MonitoringData();
        ClusterData clusterData = new ClusterData();
        clusterData.setFqdn("tashi.openlab.com");
        clusterData.setName("Tashi");
        monitoringData.putClusterData(clusterData);

        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/Message/NetworkEntities/NetworkEntity";
        XPathExpression xPathExpression = xPath.compile(expression);
        NodeList hostNodes = (NodeList) xPathExpression.evaluate(vmLayoutDoc, XPathConstants.NODESET);
        for (int i = 0; i < hostNodes.getLength(); i++) {
            Element hostNode = (Element) hostNodes.item(i);
            HostData hostData = parseHostData(hostNode, clusterData);
            clusterData.putHostData(hostData);
            monitoringData.putHostData(hostData);

            expression = "NetworkEntity";
            xPathExpression = xPath.compile(expression);
            NodeList vmNodes = (NodeList) xPathExpression.evaluate(hostNode, XPathConstants.NODESET);
            for (int j = 0; j < vmNodes.getLength(); j++) {
                Element vmNode = (Element) vmNodes.item(j);
                VmData vmData = parseVmData(vmNode, hostData, clusterData);
                clusterData.putVmData(vmData);
                hostData.putVm(vmData.getFqdn());
                monitoringData.putVmData(vmData);
            }
        }

        tashiSensorMonitor.setVmLayoutData(monitoringData);
        // TODO: locking
        log.trace("VmLayout data stored successfully.");
    }

    private HostData parseHostData(Element node, ClusterData clusterData) {
        HostData hostData = new HostData();
        hostData.setFqdn(node.getAttribute("fqdn"));
        MetricData metricData;

        String hostname = node.getAttribute("name");
        metricData = new MetricData(RawMetric.HOSTNAME, hostname, DataType.STRING, "");
        hostData.putMetricData(metricData);

        Boolean availStatus = node.getAttribute("up").equals("1");
        metricData = new MetricData(RawMetric.AVAILABILITY_STATUS, availStatus, DataType.BOOLEAN, "");
        hostData.putMetricData(metricData);

        // number of cpu cores
        Integer cpuCores = Integer.parseInt(node.getAttribute("cpu_cores"));
        metricData = new MetricData(RawMetric.CPU_CORES_COUNT, cpuCores, DataType.INTEGER, "");
        hostData.putMetricData(metricData);

        // cpu speed (sum of all cores)
        Double cpuSpeed = Double.parseDouble(node.getAttribute("cpu_speed")) / 1024;
        metricData = new MetricData(RawMetric.CPU_SPEED, cpuSpeed, DataType.DOUBLE, "GHz");
        hostData.putMetricData(metricData);

        // SHARED_IMAGES_DISK_FREE
        Double sharedDiskFree = Double.parseDouble(node.getAttribute("disk_available")) / 1024 / 1024;
        metricData = new MetricData(RawMetric.SHARED_IMAGES_DISK_FREE, sharedDiskFree, DataType.DOUBLE, "GB");
        hostData.putMetricData(metricData);

        // SHARED_IMAGES_DISK_USED
        Double sharedDiskUsed = Double.parseDouble(node.getAttribute("disk_used")) / 1024 / 1024;
        metricData = new MetricData(RawMetric.SHARED_IMAGES_DISK_USED, sharedDiskUsed, DataType.DOUBLE, "GB");
        hostData.putMetricData(metricData);

        Boolean powerStatus = node.getAttribute("poweredOn").equals("1");
        metricData = new MetricData(RawMetric.POWER_STATUS, powerStatus, DataType.BOOLEAN, "");
        hostData.putMetricData(metricData);

        return hostData;
    }

    private VmData parseVmData(Element node, HostData hostData, ClusterData clusterData) throws Exception {
        VmData vmData = new VmData();
        vmData.setFqdn(node.getAttribute("fqdn"));
        vmData.setHostFqdn(hostData.getFqdn());
        MetricData metricData;

        String hostname = node.getAttribute("name");
        metricData = new MetricData(RawMetric.HOSTNAME, hostname, DataType.STRING, "");
        vmData.putMetricData(metricData);

        VmStateEnum vmState = VmStateEnum.fromString(node.getAttribute("state"));
        metricData = new MetricData(RawMetric.VM_STATE, vmState, DataType.STRING, "");
        vmData.putMetricData(metricData);

        Boolean availStatus = (vmState == VmStateEnum.RUNNING || vmState == VmStateEnum.MIGRATETRANS);
        metricData = new MetricData(RawMetric.AVAILABILITY_STATUS, availStatus, DataType.BOOLEAN, "");
        vmData.putMetricData(metricData);

        // VM_PERSISTENCE and VM_IMAGE_TEMPLATE
        String disksAttr = node.getAttribute("disks");  // format: [{'uri': 'tashi.img', 'persistent': False}]
        disksAttr = disksAttr.replace("'uri': u'", "'uri': '");
        JSONArray diskArr = new JSONArray(disksAttr);
        // assume there is only one disk
        JSONObject disk1 = diskArr.getJSONObject(0);
        metricData = new MetricData(RawMetric.VM_PERSISTENCE, disk1.getBoolean("persistent"), DataType.BOOLEAN, "");
        vmData.putMetricData(metricData);
        metricData = new MetricData(RawMetric.VM_IMAGE_TEMPLATE, disk1.getString("uri"), DataType.STRING, "");
        vmData.putMetricData(metricData);

        // VM_CORES
        Integer cpuCores = Integer.parseInt(node.getAttribute("cpu_cores"));
        metricData = new MetricData(RawMetric.CPU_CORES_COUNT, cpuCores, DataType.INTEGER, "");
        vmData.putMetricData(metricData);

        // VM_CPU_SPEED
        Double cpuSpeed = Double.parseDouble(node.getAttribute("cpu_speed")) / 1024;
        metricData = new MetricData(RawMetric.CPU_SPEED, cpuSpeed, DataType.DOUBLE, "GHz");
        vmData.putMetricData(metricData);

        // VM_MEMORY_SIZE
        Double memorySize = Double.parseDouble(node.getAttribute("memory"));
        metricData = new MetricData(RawMetric.MEM_TOTAL, memorySize, DataType.DOUBLE, "MB");
        vmData.putMetricData(metricData);

        return vmData;
    }

    private void createLayoutXml(Document vmLayoutDoc) throws Exception {
        log.trace("Creating layout xml.");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        Document infrastructureDoc = docBuilder.newDocument();
        Element root = infrastructureDoc.createElement("Infrastructure");
        infrastructureDoc.appendChild(root);
        Node networkEntityCluster = createNetworkEntity(infrastructureDoc, "Tashi", "tashi.openlab.com", "cluster");
        root.appendChild(networkEntityCluster);

        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/Message/NetworkEntities/NetworkEntity";
        XPathExpression xPathExpression = xPath.compile(expression);
        NodeList hostNodes = (NodeList) xPathExpression.evaluate(vmLayoutDoc, XPathConstants.NODESET);
        for (int i = 0; i < hostNodes.getLength(); i++) {
            Element hostNode = (Element) hostNodes.item(i);
            String hostName = hostNode.getAttribute("name");
            String hostFqdn = hostNode.getAttribute("fqdn");
            Node networkEntityHost = createNetworkEntity(infrastructureDoc, hostName, hostFqdn, "host");
            networkEntityCluster.appendChild(networkEntityHost);

            expression = "NetworkEntity";
            xPathExpression = xPath.compile(expression);
            NodeList vmNodes = (NodeList) xPathExpression.evaluate(hostNode, XPathConstants.NODESET);
            for (int j = 0; j < vmNodes.getLength(); j++) {
                Element vmNode = (Element) vmNodes.item(j);
                String vmName = vmNode.getAttribute("name");
                String vmFqdn = vmNode.getAttribute("fqdn");
                Node networkEntityVm = createNetworkEntity(infrastructureDoc, vmName, vmFqdn, "vm");
                networkEntityHost.appendChild(networkEntityVm);
            }
        }

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(infrastructureDoc);
        transformer.transform(source, result);

        tashiSensorMonitor.setInfrastructureLayoutXml(result.getWriter().toString());
        // TODO: locking
        log.trace("Layout xml created successfully.");
    }

    private Node createNetworkEntity(Document doc, String name, String fqdn, String type) {
        Element node = doc.createElement("NetworkEntity");
        node.setAttribute("name", name);
        node.setAttribute("fqdn", fqdn);
        node.setAttribute("type", type);
        return node;
    }

    public void processClusterConfigurationMessage(Document xmlDoc) throws Exception {
        log.trace("Parsing ClusterConfiguration data...");
        MonitoringData monitoringData = new MonitoringData();

        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/Message/ClusterConfiguration";
        XPathExpression xPathExpression = xPath.compile(expression);
        Element clusterNode = (Element) xPathExpression.evaluate(xmlDoc, XPathConstants.NODE);
        String clusterFqdn = clusterNode.getAttribute("fqdn");

        ClusterData clusterData = new ClusterData();
        clusterData.setFqdn(clusterFqdn);
        monitoringData.putClusterData(clusterData);

        expression = "Host";
        xPathExpression = xPath.compile(expression);
        NodeList hostNodes = (NodeList) xPathExpression.evaluate(clusterNode, XPathConstants.NODESET);
        for (int i = 0; i < hostNodes.getLength(); i++) {
            Element hostNode = (Element) hostNodes.item(i);
            String hostFqdn = hostNode.getAttribute("fqdn");
            HostData hostData = new HostData();
            hostData.setFqdn(hostFqdn);
            clusterData.putHostData(hostData);
            monitoringData.putHostData(hostData);
            MetricData metricData;
            String value;

            xPathExpression = xPath.compile("Auditability");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            Boolean auditability = Boolean.parseBoolean(value.trim());
            metricData = new MetricData(RawMetric.AUDITABILITY, auditability, DataType.BOOLEAN, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("Location/CountryCode");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            String countryCode = value.trim();
            metricData = new MetricData(RawMetric.LOCATION, countryCode, DataType.STRING, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("SAS70");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            Boolean sas70 = Boolean.parseBoolean(value.trim());
            metricData = new MetricData(RawMetric.SAS70_COMPLIANCE, sas70, DataType.BOOLEAN, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("CCR");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            Boolean ccr = Boolean.parseBoolean(value.trim());
            metricData = new MetricData(RawMetric.CCR, ccr, DataType.BOOLEAN, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("DataClassification");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            DataClassificationEnum dataClassification =
                    DataClassificationEnum.fromString(value.trim());
            metricData = new MetricData(RawMetric.DATA_CLASSIFICATION, dataClassification, DataType.STRING, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("HWRedundancyLevel");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            HwRedundancyLevelEnum hwRedundancyLevel =
                    HwRedundancyLevelEnum.fromString(value.trim());
            metricData = new MetricData(RawMetric.HW_REDUNDANCY_LEVEL, hwRedundancyLevel, DataType.STRING, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("DiskThroughput");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            DiskThroughputLevel diskThroughput =
                    DiskThroughputLevel.fromString(value.trim());
            metricData = new MetricData(RawMetric.DISK_THROUGHPUT, diskThroughput, DataType.STRING, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("NetThroughput");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            NetThroughputLevel netThroughput =
                    NetThroughputLevel.fromString(value.trim());
            metricData = new MetricData(RawMetric.NET_THROUGHPUT, netThroughput, DataType.STRING, "");
            hostData.putMetricData(metricData);

            xPathExpression = xPath.compile("DataEncryption");
            value = (String) xPathExpression.evaluate(hostNode, XPathConstants.STRING);
            Boolean dataEncryption = Boolean.parseBoolean(value.trim());
            metricData = new MetricData(RawMetric.DATA_ENCRYPTION, dataEncryption, DataType.BOOLEAN, "");
            hostData.putMetricData(metricData);
        }
        tashiSensorMonitor.setClusterConfData(monitoringData);

        tashiSensorMonitor.setGotClusterConfData(true);
        log.trace("ClusterConfiguration data stored successfully.");
    }

    public void processAuditRecordMessage(Document xmlDoc) throws Exception {
        log.trace("Parsing AuditRecord message...");
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/Message";
        XPathExpression xPathExpression = xPath.compile(expression);
        Element messageNode = (Element) xPathExpression.evaluate(xmlDoc, XPathConstants.NODE);
        String fqdn = messageNode.getAttribute("id");
        String userId = messageNode.getAttribute("userId");

        // parse date in the format 2011-04-18T14:44:03.397073+2:00
        String timestampString = messageNode.getAttribute("time");
        if (timestampString.matches(".*[+-]\\d:\\d\\d$")) {
            timestampString = timestampString.replaceAll("(\\d):(\\d\\d)$", "0$1$2");
        }
        else if (timestampString.matches(".*[+-]\\d\\d:\\d\\d$")) {
            timestampString = timestampString.replaceAll("(\\d\\d):(\\d\\d)$", "$1$2");
        }
        Date timestamp = DateUtils.parse(timestampString, "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");

        xPathExpression = xPath.compile("Action");
        String description = (String) xPathExpression.evaluate(messageNode, XPathConstants.STRING);
        description = description.trim();

        AuditRecord auditRecord = new AuditRecord();
        auditRecord.setSource("Tashi");
        auditRecord.setFqdn(fqdn);
        auditRecord.setUserId(userId);
        auditRecord.setTimestamp(timestamp);
        auditRecord.setDescription(description);

        AuditRecordManager.getInstance().create(auditRecord);

        MonitoringDataNotifier.publishServiceEventNotification(auditRecord);

        log.trace("AuditRecord stored successfully.");
    }

    public void processSchedulerMessage(Document xmlDoc) throws Exception {
        log.trace("Parsing Scheduler message...");
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/Message";
        XPathExpression xPathExpression = xPath.compile(expression);
        Element messageNode = (Element) xPathExpression.evaluate(xmlDoc, XPathConstants.NODE);

        // parse date in the format 2011-04-18T14:44:03.397073+2:00
        String timestampString = messageNode.getAttribute("time");
        if (timestampString.matches(".*[+-]\\d:\\d\\d$")) {
            timestampString = timestampString.replaceAll("(\\d):(\\d\\d)$", "0$1$2");
        }
        else if (timestampString.matches(".*[+-]\\d\\d:\\d\\d$")) {
            timestampString = timestampString.replaceAll("(\\d\\d):(\\d\\d)$", "$1$2");
        }
        Date timestamp = DateUtils.parse(timestampString, "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");

        xPathExpression = xPath.compile("Action");
        String description = (String) xPathExpression.evaluate(messageNode, XPathConstants.STRING);
        description = description.trim();

        AuditRecord auditRecord = new AuditRecord();
        auditRecord.setSource("Scheduler");
        auditRecord.setTimestamp(timestamp);
        auditRecord.setDescription(Utils.shortenString(description, 256));

        AuditRecordManager.getInstance().create(auditRecord);

        log.trace("Scheduler message stored successfully.");
    }
}
