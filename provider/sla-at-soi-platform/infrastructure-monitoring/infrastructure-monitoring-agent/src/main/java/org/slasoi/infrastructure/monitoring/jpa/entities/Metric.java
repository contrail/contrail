/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2997 $
 * @lastrevision $Date: 2011-08-22 17:10:42 +0200 (pon, 22 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/Metric.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.slasoi.infrastructure.monitoring.computation.ReportingPeriod;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "metric", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"metric_type_id", "vm_id"})})
@NamedQueries({
        @NamedQuery(name = "Metric.findAll", query = "SELECT m FROM Metric m"),
        @NamedQuery(name = "Metric.findByMetricId", query = "SELECT m FROM Metric m WHERE m.metricId = :metricId"),
        @NamedQuery(name = "Metric.findByViolationThreshold", query = "SELECT m FROM Metric m WHERE m.violationThreshold = :violationThreshold"),
        @NamedQuery(name = "Metric.findByWarningThreshold", query = "SELECT m FROM Metric m WHERE m.warningThreshold = :warningThreshold")})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "metric_type", discriminatorType = DiscriminatorType.STRING, length = 50)
@DiscriminatorValue("GENERIC")
public abstract class Metric implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "metric_id", nullable = false)
    private Integer metricId;
    @Column(name = "metric_type")
    @Enumerated(EnumType.STRING)
    private MetricTypeEnum metricTypeEnum;
    @Column(name = "violationThreshold", length = 32)
    private String violationThreshold;
    @Column(name = "warningThreshold", length = 32)
    private String warningThreshold;
    @Column(name = "config_value", length = 32)
    private String configValue;
    @JoinColumn(name = "vm_id", referencedColumnName = "vm_id")
    @ManyToOne
    private Vm vm;
    @JoinColumn(name = "service_id", referencedColumnName = "service_id")
    @ManyToOne
    private Service service;
    @JoinColumn(name = "metric_type_id", referencedColumnName = "metric_type_id", nullable = false)
    @ManyToOne(optional = false)
    private MetricType metricType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metric")
    private List<Violation> violationList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "metric")
    private MetricValue metricValue;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metric")
    private List<MetricValueHistory> metricValueHistoryList;

    public Metric() {
    }

    public Metric(Integer metricId) {
        this.metricId = metricId;
    }

    public Integer getMetricId() {
        return metricId;
    }

    public void setMetricId(Integer metricId) {
        this.metricId = metricId;
    }

    public String getViolationThreshold() {
        return violationThreshold;
    }

    public void setViolationThreshold(String violationThreshold) {
        this.violationThreshold = violationThreshold;
    }

    public String getWarningThreshold() {
        return warningThreshold;
    }

    public void setWarningThreshold(String warningThreshold) {
        this.warningThreshold = warningThreshold;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public Vm getVm() {
        return vm;
    }

    public void setVm(Vm vm) {
        this.vm = vm;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }

    public List<Violation> getViolationList() {
        return violationList;
    }

    public void setViolationList(List<Violation> violationList) {
        this.violationList = violationList;
    }

    public MetricValue getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(MetricValue metricValue) {
        this.metricValue = metricValue;
    }

    public List<MetricValueHistory> getMetricValueHistoryList() {
        return metricValueHistoryList;
    }

    public void setMetricValueHistoryList(List<MetricValueHistory> metricValueHistoryList) {
        this.metricValueHistoryList = metricValueHistoryList;
    }

    public MetricTypeEnum getMetricTypeEnum() {
        return metricTypeEnum;
    }

    public void setMetricTypeEnum(MetricTypeEnum metricTypeEnum) {
        this.metricTypeEnum = metricTypeEnum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (metricId != null ? metricId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metric)) {
            return false;
        }
        Metric other = (Metric) object;
        if ((this.metricId == null && other.metricId != null) || (this.metricId != null && !this.metricId.equals(other.metricId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.Metric[metricId=" + metricId + "]";
    }

    protected SLAComplianceEnum getPrevSLACompliance() {
        MetricValue metricValue = this.getMetricValue();
        if (metricValue == null) {
            return null;
        }
        else {
            return metricValue.getSlaCompliance();
        }
    }

    protected void updateMetricSLACompliance(Metric metric, MetricValue metricValue, SLAComplianceEnum slaCompliance, Date metricsTimestamp) throws Exception {
        // set SLA compliance of metric value
        metricValue.setSlaCompliance(slaCompliance);

        // set violation severity
        ViolationSeverity violationSeverity = null;
        if (slaCompliance == SLAComplianceEnum.COMPLIANT) {
            violationSeverity = null;
        }
        else {
            if (metric.getMetricType().isCritical()) {
                violationSeverity = ViolationSeverity.PUNISHABLE;
            }
            else {
                boolean isWorkingDate;
                if (metric.getMetricType().getTarget() == MetricType.Target.SERVICE) {
                    isWorkingDate = metric.getService().isWorkingTime(metricsTimestamp);
                }
                else if (metric.getMetricType().getTarget() == MetricType.Target.VM) {
                    isWorkingDate = metric.getVm().getService().isWorkingTime(metricsTimestamp);
                }
                else {
                    throw new RuntimeException("Invalid MetricType target.");
                }
                violationSeverity = isWorkingDate ? ViolationSeverity.PUNISHABLE : ViolationSeverity.ACCEPTABLE;
            }
        }
        metricValue.setViolationSeverity(violationSeverity);

        // update metricValue
        MetricValueManager.getInstance().edit(metricValue);
    }

    public MetricValue storeMetricValue(Object value, Date metricsTimestamp) throws Exception {
        String formattedValue = formatValue(value);
        return MetricValueManager.getInstance().storeMetricValue(this, formattedValue, metricsTimestamp);
    }

    public void storeMetricHistoryValue(Object value, Date metricsTimestamp) throws Exception {
        String formattedValue = formatValue(value);
        MetricValueHistoryManager.getInstance().storeValue(this, formattedValue, metricsTimestamp);
    }

    public void storeMetricHistoryValue(Object value, ReportingPeriod reportingPeriod) throws Exception {
        String formattedValue = formatValue(value);
        MetricValueHistoryManager.getInstance().storeValue(this, formattedValue, reportingPeriod);
    }

    public static Metric create(MetricTypeEnum metricTypeEnum) throws Exception {
        MetricType metricType = metricTypeEnum.getMetricType();
        String fqClassName = "org.slasoi.infrastructure.monitoring.jpa.entities.metrics." +
                Utils.toCamelCase(metricTypeEnum.toString());
        try {
            Class theClass = Class.forName(fqClassName);
            Metric metric = (Metric) theClass.newInstance();
            metric.setMetricTypeEnum(metricTypeEnum);
            metric.setMetricType(metricType);
            return metric;
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to create instance of class %s: %s.", fqClassName, e.getMessage()));
        }
    }

    public abstract void compute(IMonitoringEngine monitoringEngine) throws Exception;

    public abstract Object parseValue(String value) throws InvalidMetricValueException;

    public abstract String formatValue(Object value);

    public String displayValue(String value) throws InvalidMetricValueException {
        if (this.getMetricType().getMetricUnit() != null) {
            return value + " " + this.getMetricType().getMetricUnit();
        }
        else {
            return value;
        }
    }

    public abstract Object getViolationThresholdValue() throws InvalidMetricValueException;

    public String getViolationThresholdWithUnit() throws InvalidMetricValueException {
        if (this.getMetricType().getMetricUnit() != null) {
            return this.getViolationThreshold() + " " + this.getMetricType().getMetricUnit();
        }
        else {
            return this.getViolationThreshold();
        }
    }

    public abstract String getViolationCondition() throws InvalidMetricValueException;

    public abstract Object getWarningThresholdValue();

    public abstract String getWarningCondition();

    public abstract Object estimateWarningThreshold(Object violationThreshold);
}
