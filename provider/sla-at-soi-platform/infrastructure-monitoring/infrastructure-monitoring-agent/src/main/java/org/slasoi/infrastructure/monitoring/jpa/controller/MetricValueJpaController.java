/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/MetricValueJpaController.ja $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.IllegalOrphanException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.PreexistingEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class MetricValueJpaController {

    public MetricValueJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MetricValue metricValue) throws IllegalOrphanException, PreexistingEntityException, Exception {
        List<String> illegalOrphanMessages = null;
        Metric metricOrphanCheck = metricValue.getMetric();
        if (metricOrphanCheck != null) {
            MetricValue oldMetricValueOfMetric = metricOrphanCheck.getMetricValue();
            if (oldMetricValueOfMetric != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Metric " + metricOrphanCheck + " already has an item of type MetricValue whose metric column cannot be null. Please make another selection for the metric field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Metric metric = metricValue.getMetric();
            if (metric != null) {
                metric = em.getReference(metric.getClass(), metric.getMetricId());
                metricValue.setMetric(metric);
            }
            em.persist(metricValue);
            if (metric != null) {
                metric.setMetricValue(metricValue);
                metric = em.merge(metric);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            if (findMetricValue(metricValue.getMetricId()) != null) {
                throw new PreexistingEntityException("MetricValue " + metricValue + " already exists.", ex);
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MetricValue metricValue) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricValue persistentMetricValue = em.find(MetricValue.class, metricValue.getMetricId());
            Metric metricOld = persistentMetricValue.getMetric();
            Metric metricNew = metricValue.getMetric();
            List<String> illegalOrphanMessages = null;
            if (metricNew != null && !metricNew.equals(metricOld)) {
                MetricValue oldMetricValueOfMetric = metricNew.getMetricValue();
                if (oldMetricValueOfMetric != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Metric " + metricNew + " already has an item of type MetricValue whose metric column cannot be null. Please make another selection for the metric field.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (metricNew != null) {
                metricNew = em.getReference(metricNew.getClass(), metricNew.getMetricId());
                metricValue.setMetric(metricNew);
            }
            metricValue = em.merge(metricValue);
            if (metricOld != null && !metricOld.equals(metricNew)) {
                metricOld.setMetricValue(null);
                metricOld = em.merge(metricOld);
            }
            if (metricNew != null && !metricNew.equals(metricOld)) {
                metricNew.setMetricValue(metricValue);
                metricNew = em.merge(metricNew);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = metricValue.getMetricId();
                if (findMetricValue(id) == null) {
                    throw new NonexistentEntityException("The metricValue with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MetricValue metricValue;
            try {
                metricValue = em.getReference(MetricValue.class, id);
                metricValue.getMetricId();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The metricValue with id " + id + " no longer exists.", enfe);
            }
            Metric metric = metricValue.getMetric();
            if (metric != null) {
                metric.setMetricValue(null);
                metric = em.merge(metric);
            }
            em.remove(metricValue);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MetricValue> findMetricValueEntities() {
        return findMetricValueEntities(true, -1, -1);
    }

    public List<MetricValue> findMetricValueEntities(int maxResults, int firstResult) {
        return findMetricValueEntities(false, maxResults, firstResult);
    }

    private List<MetricValue> findMetricValueEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MetricValue.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public MetricValue findMetricValue(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MetricValue.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getMetricValueCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MetricValue> rt = cq.from(MetricValue.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
