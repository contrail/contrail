/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3034 $
 * @lastrevision $Date: 2011-08-29 14:30:56 +0200 (pon, 29 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/ganglia/GangliaMonitoringEngine.j $
 */

package org.slasoi.infrastructure.monitoring.monitors.ganglia;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.exceptions.InfrastructureMonitoringException;
import org.slasoi.infrastructure.monitoring.exceptions.MetricNotSupportedException;
import org.slasoi.infrastructure.monitoring.exceptions.MonitoringEngineException;
import org.slasoi.infrastructure.monitoring.jpa.enums.DataType;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses Ganglia status report.
 */
public class GangliaMonitoringEngine {
    private static Logger log = Logger.getLogger(GangliaMonitoringEngine.class);
    private static Pattern tashiMetricPattern = Pattern.compile("tashi_vm_([^_]+)_(\\w+)");
    private String gmetadHostname;
    private int gmetadPort;
    private boolean mockupMode = false;
    private File mockupReportFile;
    private Date reportDate;
    private MonitoringData monitoringData;
    private XPath xPath;
    Document gangliaXmlDoc;
    IMonitoringEngine monitoringEngine;

    private static RawMetric[] hostMetrics = {RawMetric.MEM_FREE, RawMetric.MEM_TOTAL, RawMetric.CPU_SPEED, RawMetric.CPU_SYSTEM,
            RawMetric.CPU_USER, RawMetric.CPU_IDLE, RawMetric.DISK_FREE, RawMetric.DISK_TOTAL, RawMetric.IP_ADDRESS,
            RawMetric.LOAD_ONE, RawMetric.LOAD_FIVE};
    private static final Set<RawMetric> hostMetricsSet = new HashSet<RawMetric>(Arrays.asList(hostMetrics));

    private static RawMetric[] vmMetrics = {RawMetric.MEM_USAGE, RawMetric.CPU_LOAD};
    private static final Set<RawMetric> vmMetricsSet = new HashSet<RawMetric>(Arrays.asList(vmMetrics));

    public GangliaMonitoringEngine(String gmetadHostname, int gmetadPort) throws Exception {
        this.gmetadHostname = gmetadHostname;
        this.gmetadPort = gmetadPort;
    }

    public GangliaMonitoringEngine(String mockupReportFilePath) throws Exception {
        mockupReportFile = new File(mockupReportFilePath);
        if (!mockupReportFile.canRead()) {
            throw new InfrastructureMonitoringException("Cannot open file " + mockupReportFilePath);
        }
        mockupMode = true;
    }

    public void queryGanglia() throws Exception {
        log.trace("queryGanglia() started.");
        String gangliaXmlReport;
        monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();
        if (mockupMode) {
            log.trace("Collecting monitoring data from Ganglia mockup report file " + mockupReportFile.getAbsolutePath() + ".");
            try {
                byte[] ba = new byte[(int) mockupReportFile.length()];
                FileInputStream fis = new FileInputStream(mockupReportFile);
                fis.read(ba);
                gangliaXmlReport = new String(ba);
                log.trace("Retrieved monitoring data from mockup Ganglia XML file.");
            }
            catch (Exception e) {
                throw new MonitoringEngineException("Failed to read Ganglia mockup report file: " + e);
            }
        }
        else {
            log.trace(String.format("Collecting monitoring data from Ganglia at %s:%d.", gmetadHostname, gmetadPort));
            GangliaReader gangliaReader = new GangliaReader(gmetadHostname, gmetadPort);
            try {
                gangliaXmlReport = gangliaReader.getReport();
                log.trace("Retrieved monitoring data from Ganglia. Content length=" + gangliaXmlReport.length());
            }
            catch (Exception e) {
                throw new MonitoringEngineException(String.format("Failed to query Ganglia Monitoring System at %s:%d: %s", gmetadHostname, gmetadPort, e.getMessage()));
            }
        }
        reportDate = new Date();
        parseGangliaReport(gangliaXmlReport);
        log.trace("queryGanglia() finished successfully.");
    }

    private void parseGangliaReport(String gangliaXmlReport) throws Exception {
        log.trace("parseGangliaReport() started.");
        MonitoringData monitoringDataNew = new MonitoringData();
        monitoringDataNew.setTimestamp(new Date());
        // TODO: metrics should be taken from monitoringData structure
        //Document gangliaXmlDoc;
        try {
            gangliaXmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(gangliaXmlReport)));
        }
        catch (Exception e) {
            throw new MonitoringEngineException("Failed to parse Ganglia report file: " + e);
        }
        xPath = XPathFactory.newInstance().newXPath();

        String expression = "/GANGLIA_XML/GRID/CLUSTER";
        XPathExpression xPathExpression = xPath.compile(expression);
        NodeList clusterNodes = (NodeList) xPathExpression.evaluate(gangliaXmlDoc, XPathConstants.NODESET);
        for (int i = 0; i < clusterNodes.getLength(); i++) {
            Node clusterNode = clusterNodes.item(i);
            ClusterData clusterData = new ClusterData();
            clusterData.setName(((Element) clusterNode).getAttribute("NAME"));
            clusterData.setFqdn(((Element) clusterNode).getAttribute("NAME"));

            xPathExpression = xPath.compile("HOST");
            NodeList hostNodes = (NodeList) xPathExpression.evaluate(clusterNode, XPathConstants.NODESET);
            for (int j = 0; j < hostNodes.getLength(); j++) {
                Node hostNode = hostNodes.item(j);
                HostData hostData = parseHostData(hostNode, clusterData, monitoringDataNew);
                clusterData.putHostData(hostData);
                monitoringDataNew.putHostData(hostData);
            }
            monitoringDataNew.putClusterData(clusterData);
        }

        // TODO: locking
        monitoringData = monitoringDataNew;

        log.trace("Ganglia XML parsed successfully.");
        log.trace("parseGangliaReport() finished successfully.");
    }

    private HostData parseHostData(Node hostNode, ClusterData clusterData, MonitoringData monitoringDataNew) throws Exception {
        HostData hostData = new HostData();
        hostData.setFqdn(((Element) hostNode).getAttribute("NAME"));

        MetricData ipAddress = new MetricData(RawMetric.IP_ADDRESS, ((Element) hostNode).getAttribute("IP"), DataType.STRING, "");
        hostData.putMetricData(ipAddress);

        XPathExpression xPathExpression = xPath.compile("METRIC");
        NodeList metricNodes = (NodeList) xPathExpression.evaluate(hostNode, XPathConstants.NODESET);

        for (int j = 0; j < metricNodes.getLength(); j++) {
            Node metricNode = metricNodes.item(j);
            String name = ((Element) metricNode).getAttribute("NAME");
            String value = ((Element) metricNode).getAttribute("VAL");
            if (name.startsWith("tashi_vm_")) {
                extractTashiMetric(name, value, hostData, clusterData, monitoringDataNew);
            }
            else if (name.equals("mem_free")) {
                double memFree = Double.parseDouble(value) / 1024;
                MetricData metricData = new MetricData(RawMetric.MEM_FREE, memFree, DataType.DOUBLE, "MB");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("mem_total")) {
                double memTotal = Double.parseDouble(value) / 1024;
                MetricData metricData = new MetricData(RawMetric.MEM_TOTAL, memTotal, DataType.DOUBLE, "MB");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("cpu_speed")) {
                double cpuSpeed = Double.parseDouble(value) / 1024;
                MetricData metricData = new MetricData(RawMetric.CPU_SPEED, cpuSpeed, DataType.DOUBLE, "GHz");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("cpu_user")) {
                double cpuUser = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.CPU_USER, cpuUser, DataType.DOUBLE, "%");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("cpu_system")) {
                double cpuSystem = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.CPU_SYSTEM, cpuSystem, DataType.DOUBLE, "%");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("cpu_idle")) {
                double cpuIdle = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.CPU_IDLE, cpuIdle, DataType.DOUBLE, "%");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("disk_free")) {
                double diskFree = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.DISK_FREE, diskFree, DataType.DOUBLE, "GB");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("disk_total")) {
                double diskTotal = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.DISK_TOTAL, diskTotal, DataType.DOUBLE, "GB");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("load_one")) {
                double loadOne = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.LOAD_ONE, loadOne, DataType.DOUBLE, "");
                hostData.putMetricData(metricData);
            }
            else if (name.equals("load_five")) {
                double loadFive = Double.parseDouble(value);
                MetricData metricData = new MetricData(RawMetric.LOAD_FIVE, loadFive, DataType.DOUBLE, "");
                hostData.putMetricData(metricData);
            }

        }
        return hostData;
    }

    private void extractTashiMetric(String name, String value, HostData hostData, ClusterData clusterData, MonitoringData monitoringDataNew) {
        Matcher m = tashiMetricPattern.matcher(name);
        if (m.find()) {
            String vmFqdn = m.group(1);
            String tashiMetricName = m.group(2);

            VmData vmData;

            String hostFqdn = monitoringEngine.getVmHostMachine(vmFqdn);
            if (hostFqdn == null) {
                return; // Tashi didn't report this vm. Data from Ganglia can be obsolete
            }
            else if (!hostFqdn.equals(hostData.getFqdn())) {
                log.trace(String.format("Monitoring data mismatch detected: host of vm '%s' extracted from Ganglia " +
                        "report doesn't match with host retrieved from Tashi sensor. Ganglia: %s, " +
                        "Tashi sensor: %s. Metric skipped.", vmFqdn, hostData.getFqdn(), hostFqdn));
                return;
            }

            if (monitoringDataNew.containsVmData(vmFqdn)) {
                vmData = monitoringDataNew.getVmData(vmFqdn);
            }
            else {
                vmData = new VmData();
                vmData.setFqdn(vmFqdn);
                monitoringDataNew.putVmData(vmData);
                clusterData.putVmData(vmData);
            }

            if (tashiMetricName.equals("rss")) {
                MetricData metricData = new MetricData();
                metricData.setMetric(RawMetric.MEM_USAGE);
                metricData.setType(DataType.DOUBLE);
                metricData.setUnit("MB");
                metricData.setValue(Double.valueOf(value));
                vmData.putMetricData(metricData);
            }
            else if (tashiMetricName.equals("cpuLoad")) {
                MetricData metricData = new MetricData();
                metricData.setMetric(RawMetric.CPU_LOAD);
                metricData.setType(DataType.DOUBLE);
                metricData.setUnit("");
                metricData.setValue(Double.valueOf(value));
                vmData.putMetricData(metricData);
            }
        }
    }

    public Date getReportDate() {
        return reportDate;
    }

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (vmMetricsSet.contains(rawMetric)) {
            return monitoringData.getVmData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by Ganglia.");
        }
    }

    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (hostMetricsSet.contains(rawMetric)) {
            return monitoringData.getHostData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by Ganglia.");
        }
    }

    public boolean isVmMonitoringDataAvailable(String fqdn) {
        return monitoringData.containsVmData(fqdn);
    }
}
