/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GenerateReportRequest.java $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.ReportType;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubRequest;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.Date;

public class GenerateReportRequest extends PubSubRequest {
    private ReportType reportType;

    public GenerateReportRequest(ReportType reportType) {
        this.setRequestType("GenerateReportRequest");
        this.reportType = reportType;
    }

    public static GenerateReportRequest fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        GenerateReportRequest request = gson.fromJson(jsonString, GenerateReportRequest.class);
        if (request.getReportType() == ReportType.SERVICE_SUMMARY) {
            return gson.fromJson(jsonString, ServiceSummary.class);
        }
        else if (request.getReportType() == ReportType.INVESTMENT_GOVERNANCE) {
            return gson.fromJson(jsonString, InvestmentGovernance.class);
        }
        else {
            throw new RuntimeException(String.format("Invalid report type '%s'.", request.getReportType()));
        }
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public static class ServiceSummary extends GenerateReportRequest {
        private String serviceUri;
        private Date fromDate;
        private Date toDate;

        public ServiceSummary() {
            super(ReportType.SERVICE_SUMMARY);
        }

        public String getServiceUri() {
            return serviceUri;
        }

        public void setServiceUri(String serviceUri) {
            this.serviceUri = serviceUri;
        }

        public Date getFromDate() {
            return fromDate;
        }

        public void setFromDate(Date fromDate) {
            this.fromDate = fromDate;
        }

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }
    }

    public static class InvestmentGovernance extends GenerateReportRequest {
        public InvestmentGovernance() {
            super(ReportType.INVESTMENT_GOVERNANCE);
        }
    }

}
