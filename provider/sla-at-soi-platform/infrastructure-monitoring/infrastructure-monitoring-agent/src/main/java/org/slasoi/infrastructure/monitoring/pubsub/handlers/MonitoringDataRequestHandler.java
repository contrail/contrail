/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3048 $
 * @lastrevision $Date: 2011-09-05 10:26:14 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/handlers/MonitoringDataRequestHandl $
 */

package org.slasoi.infrastructure.monitoring.pubsub.handlers;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.InfrastructureMonitoringAgent;
import org.slasoi.infrastructure.monitoring.jpa.entities.*;
import org.slasoi.infrastructure.monitoring.jpa.enums.*;
import org.slasoi.infrastructure.monitoring.jpa.managers.*;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.pubsub.*;
import org.slasoi.infrastructure.monitoring.pubsub.messages.*;
import org.slasoi.infrastructure.monitoring.reporting.InvestmentGovernanceReport;
import org.slasoi.infrastructure.monitoring.reporting.ServiceSummaryReport;
import org.slasoi.infrastructure.monitoring.utils.DateUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class MonitoringDataRequestHandler {
    private static Logger log = Logger.getLogger(MonitoringDataRequestHandler.class);

    public static SysInfoResponse processRequest(SysInfoRequest request) throws Exception {
        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();
        String fqdn = request.getFqdn();

        SysInfoResponse response;
        if (request.getEntityType().equals("host")) {
            response = new SysInfoResponse.HostInfoResponse();
            if (!monitoringEngine.checkHostExists(fqdn)) {
                throw new Exception(String.format("No data available for host %s.", fqdn));
            }

            MetricData metricData;

            SysInfoResponse.HostInfo hostInfo = new SysInfoResponse.HostInfo();
            hostInfo.setFqdn(fqdn);
            metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.HOSTNAME);
            hostInfo.setName(metricData.getStringValue());

            metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.IP_ADDRESS);
            hostInfo.setIp(metricData.getStringValue());

            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.CPU_SPEED));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.CPU_IDLE));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.CPU_USER));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.LOAD_ONE));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.LOAD_FIVE));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.MEM_TOTAL));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.MEM_FREE));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.DISK_TOTAL));
            hostInfo.putMetric(monitoringEngine.getHostMetricData(fqdn, RawMetric.DISK_FREE));
            ((SysInfoResponse.HostInfoResponse) response).setData(hostInfo);
        }
        else if (request.getEntityType().equals("vm")) {
            response = new SysInfoResponse.VmInfoResponse();
            if (!monitoringEngine.checkVmExists(fqdn)) {
                throw new Exception(String.format("No data available for vm %s.", fqdn));
            }

            MetricData metricData;

            SysInfoResponse.VmInfo vmInfo = new SysInfoResponse.VmInfo();
            vmInfo.setFqdn(fqdn);
            metricData = monitoringEngine.getVmMetricData(fqdn, RawMetric.HOSTNAME);
            vmInfo.setName(metricData.getStringValue());

            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.MEM_USAGE));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.CPU_LOAD));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.CPU_SPEED));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.MEM_TOTAL));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_STATE));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_IMAGE_TEMPLATE));
            vmInfo.putMetric(monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_PERSISTENCE));
            ((SysInfoResponse.VmInfoResponse) response).setData(vmInfo);
        }
        else if (request.getEntityType().equals("cluster")) {
            response = new SysInfoResponse.ClusterInfoResponse();
            SysInfoResponse.ClusterInfo clusterInfo = new SysInfoResponse.ClusterInfo();

            // currently only one cluster is supported
            clusterInfo.setFqdn(fqdn);
            clusterInfo.setName(fqdn.substring(0, fqdn.indexOf('.')));

            SysInfoResponse.ClusterStats clusterStats = new SysInfoResponse.ClusterStats();
            clusterStats.setHostCount(monitoringEngine.getAllHostMachines().size());
            clusterStats.setVmCount(monitoringEngine.getAllGuestMachines().size());
            clusterInfo.setClusterStats(clusterStats);

            ((SysInfoResponse.ClusterInfoResponse) response).setData(clusterInfo);
        }
        else {
            throw new Exception(String.format("Invalid entityType value: %s", request.getEntityType()));
        }

        response.setInReplyTo(request.getMessageId());
        response.setMetricsTimestamp(monitoringEngine.getMetricsCollectedDate());
        response.setEntityType(request.getEntityType());
        response.setFqdn(fqdn);
        response.setTimestamp(new Date());

        return response;
    }

    public static InfrastructureInfoResponse processRequest(InfrastructureInfoRequest request) throws Exception {
        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();
        String layoutXml = monitoringEngine.getInfrastructureLayoutXml();
        if (layoutXml != null) {
            InfrastructureInfoResponse response = new InfrastructureInfoResponse();
            response.setInReplyTo(request.getMessageId());
            response.setTimestamp(new Date());
            response.setData(layoutXml);
            return response;
        }
        else {
            throw new Exception("Infrastructure layout data is not available. Failed to query Tashi sensor.");
        }
    }

    public static GetServicesInfoResponse processRequest(GetServicesInfoRequest servicesInfoRequest) throws Exception {
        GetServicesInfoResponse response = new GetServicesInfoResponse();
        response.setInReplyTo(servicesInfoRequest.getMessageId());
        response.setTimestamp(new Date());
        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();

        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        for (Service service : services) {
            GetServicesInfoResponse.ServiceInfo serviceInfo = new GetServicesInfoResponse.ServiceInfo();
            response.addService(serviceInfo);
            serviceInfo.setName(service.getServiceName());
            serviceInfo.setServiceUri(service.getServiceUrl());
            serviceInfo.setCreatedDate(service.getStartDate());
            serviceInfo.setResourceCount((service.getVmList() != null) ? service.getVmList().size() : 0);

            HashMap<String, String> stats = computeUpDowntime(service);
            serviceInfo.setTotalUptime(stats.get("UPTIME"));
            serviceInfo.setTotalDowntime(stats.get("DOWNTIME"));

            Metric slaComplianceMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_SLA_COMPLIANCE, service);
            if (slaComplianceMetric.getMetricValue() != null) {
                SLAComplianceEnum slaCompliance = SLAComplianceEnum.fromString(slaComplianceMetric.getMetricValue().getValue());
                serviceInfo.setState(slaCompliance);
            }

            for (Vm vm : service.getVmList()) {
                String fqdn = vm.getFqdn();
                GetServicesInfoResponse.ResourceInfo resourceInfo = new GetServicesInfoResponse.ResourceInfo();
                serviceInfo.addResource(resourceInfo);
                resourceInfo.setName(vm.getName());
                resourceInfo.setFqdn(vm.getFqdn());

                if (!monitoringEngine.checkVmExists(vm.getFqdn())) {
                    resourceInfo.setState(VmStateEnum.NOT_RESPONDING);
                    continue;
                }

                // VM_STATE
                MetricData vmState = monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_STATE);
                resourceInfo.setState((VmStateEnum) vmState.getValue());

                // VM_CORES
                MetricData vmCores = monitoringEngine.getVmMetricData(fqdn, RawMetric.CPU_CORES_COUNT);
                resourceInfo.setCpuCores(vmCores.getValue().toString());

                // VM_CPU_SPEED
                MetricData cpuSpeed = monitoringEngine.getVmMetricData(fqdn, RawMetric.CPU_SPEED);
                resourceInfo.setCpuSpeed(cpuSpeed.getValue().toString() + " " + cpuSpeed.getUnit());

                // VM_MEMORY_SIZE
                MetricData memorySize = monitoringEngine.getVmMetricData(fqdn, RawMetric.MEM_TOTAL);
                resourceInfo.setMemory(memorySize.getValue().toString() + " " + memorySize.getUnit());

                // VM_PERSISTENCE
                MetricData vmPersistence = monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_PERSISTENCE);
                resourceInfo.setPersistent((Boolean) vmPersistence.getValue());

                // VM_IMAGE
                MetricData vmImage = monitoringEngine.getVmMetricData(fqdn, RawMetric.VM_IMAGE_TEMPLATE);
                resourceInfo.setImageTemplateName(vmImage.getValue().toString());
            }
        }
        return response;
    }

    public static GetMetricHistoryResponse processRequest(GetMetricHistoryRequest request) throws Exception {
        GetMetricHistoryResponse response = new GetMetricHistoryResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        Service service;
        try {
            service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
        }
        catch (Exception e) {
            throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
        }

        Metric metric;
        if (request.getResourceFqdn() == null) {
            // service metric
            try {
                metric = MetricManager.getInstance().findServiceMetric(request.getMetricType(), service);
            }
            catch (Exception e) {
                throw new Exception(String.format("Metric '%s' for service '%s' not found.", request.getMetricType(), request.getServiceUri()));
            }

            response.setServiceUri(service.getServiceUrl());
        }
        else {
            // vm metric
            Vm vm;
            try {
                vm = VmManager.getInstance().findVmByFqdn(request.getResourceFqdn());
            }
            catch (Exception e) {
                throw new Exception(String.format("Resource '%s' not found.", request.getResourceFqdn()));
            }

            try {
                metric = MetricManager.getInstance().findVmMetric(request.getMetricType(), vm);
            }
            catch (Exception e) {
                throw new Exception(String.format("Metric '%s' for resource '%s' not found.", request.getMetricType(), request.getResourceFqdn()));
            }

            response.setServiceUri(service.getServiceUrl());
            response.setResourceName(vm.getFqdn());
        }

        response.setMetricType(metric.getMetricType().getName());
        response.setViolationThreshold(metric.getViolationThreshold());
        response.setMetricUnit(metric.getMetricType().getMetricUnit());

        List<MetricValue> history = MetricValueHistoryManager.getInstance().getMetricHistory(metric, null, null, request.getMaxNumberOfValues());
        for (MetricValue metricValue : history) {
            GetMetricHistoryResponse.MetricValue mv = new GetMetricHistoryResponse.MetricValue();
            mv.setTimestamp(metricValue.getTime());
            mv.setValue(metricValue.getValue());
            response.putMetricValue(mv);
        }
        return response;
    }

    public static GetServiceViolationsFrequencyResponse processRequest(GetServiceViolationsFrequencyRequest request) throws Exception {
        GetServiceViolationsFrequencyResponse response = new GetServiceViolationsFrequencyResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        Service service;
        try {
            service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
        }
        catch (Exception e) {
            throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
        }

        HashMap<Date, ViolationManager.ViolationsCount> history =
                ViolationManager.getInstance().getServiceViolationsFrequency(service, null, null);
        List<Date> periods = new ArrayList<Date>();
        for (Date date : history.keySet()) {
            periods.add(date);
        }
        Collections.sort(periods);

        response.setServiceUri(service.getServiceUrl());
        SimpleDateFormat sdf = new SimpleDateFormat("d.MMM", Locale.ENGLISH);

        for (Date periodStart : periods) {
            ViolationManager.ViolationsCount historyViolCount = history.get(periodStart);
            Calendar periodMiddle = Calendar.getInstance();
            periodMiddle.setTime(periodStart);
            periodMiddle.add(Calendar.HOUR_OF_DAY, 12);
            String periodTitle = sdf.format(periodMiddle.getTime());

            // violations
            GetServiceViolationsFrequencyResponse.ViolationsCount violationsCount = new GetServiceViolationsFrequencyResponse.ViolationsCount();
            violationsCount.setTimestamp(periodMiddle.getTime());
            violationsCount.setPeriodTitle(periodTitle);
            violationsCount.setAcceptable(historyViolCount.getCount(ViolationType.VIOLATION, ViolationSeverity.ACCEPTABLE));
            violationsCount.setPunishable(historyViolCount.getCount(ViolationType.VIOLATION, ViolationSeverity.PUNISHABLE));
            response.putViolationsCount(violationsCount);

            // warnings
            GetServiceViolationsFrequencyResponse.ViolationsCount warningsCount = new GetServiceViolationsFrequencyResponse.ViolationsCount();
            warningsCount.setTimestamp(periodMiddle.getTime());
            warningsCount.setPeriodTitle(periodTitle);
            warningsCount.setAcceptable(historyViolCount.getCount(ViolationType.WARNING, ViolationSeverity.ACCEPTABLE));
            warningsCount.setPunishable(historyViolCount.getCount(ViolationType.WARNING, ViolationSeverity.PUNISHABLE));
            response.putWarningsCount(warningsCount);
        }

        return response;
    }

    public static GetServiceViolationsResponse processRequest(GetServiceViolationsRequest request) throws Exception {
        GetServiceViolationsResponse response = new GetServiceViolationsResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        Service service;
        try {
            service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
        }
        catch (Exception e) {
            throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
        }
        response.setServiceUri(service.getServiceUrl());

        // fill service violations
        List<Violation> violations = ViolationManager.getInstance().getServiceOnlyViolations(service);
        if (violations != null) {
            for (Violation violation : violations) {
                GetServiceViolationsResponse.Violation violationData = getViolationData(violation);
                if (violation.getType() == ViolationType.VIOLATION) {
                    response.getServiceEvents().putViolation(violationData);
                }
                else if (violation.getType() == ViolationType.WARNING) {
                    response.getServiceEvents().putWarning(violationData);
                }
            }
        }

        // fill resource violations
        for (Vm vm : service.getVmList()) {
            GetServiceViolationsResponse.ResourceEvents resourceData = new GetServiceViolationsResponse.ResourceEvents();
            resourceData.setResourceName(vm.getName());
            resourceData.setResourceFqdn(vm.getFqdn());
            response.putResourceEvents(resourceData);

            violations = ViolationManager.getInstance().getVmViolations(vm);
            if (violations != null) {
                for (Violation violation : violations) {
                    GetServiceViolationsResponse.Violation violationData = getViolationData(violation);
                    if (violation.getType() == ViolationType.VIOLATION) {
                        resourceData.putViolation(violationData);
                    }
                    else if (violation.getType() == ViolationType.WARNING) {
                        resourceData.putWarning(violationData);
                    }
                }
            }
        }

        return response;
    }

    private static GetServiceViolationsResponse.Violation getViolationData(Violation violation) {
        GetServiceViolationsResponse.Violation violationData = new GetServiceViolationsResponse.Violation();
        violationData.setViolationId(violation.getViolationId());
        violationData.setGuaranteedTerm(violation.getMetric().getMetricType().getName());
        violationData.setMetricValue(violation.getMetricValue());
        violationData.setThresholdValue(violation.getThreshold());
        String unit = violation.getMetric().getMetricType().getMetricUnit();
        violationData.setMetricUnit(unit != null ? unit : "");
        violationData.setBeginDate(violation.getStartTime());
        violationData.setEndDate(violation.getEndTime());
        violationData.setSeverity(violation.getSeverity());
        violationData.setType(violation.getType());
        return violationData;
    }

    public static GetServiceSLASummaryResponse processRequest(GetServiceSLASummaryRequest request) throws Exception {
        GetServiceSLASummaryResponse response = new GetServiceSLASummaryResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        Service service;
        try {
            service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
        }
        catch (Exception e) {
            throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
        }
        response.setServiceUri(service.getServiceUrl());

        // fill service QoS terms
        if (service.getMetricList() != null) {
            for (Metric metric : service.getMetricList()) {
                MetricType metricType = metric.getMetricType();
                if (metricType.getType() == MetricType.Type.QOS_TERM) {
                    response.putQosTerm(getQoSTermInfo(metric));
                }
            }
        }

        // fill resource QoS terms
        for (Vm vm : service.getVmList()) {
            GetServiceSLASummaryResponse.ResourceInfo resourceInfo = new GetServiceSLASummaryResponse.ResourceInfo();

            resourceInfo.setResourceName(vm.getName());
            resourceInfo.setResourceFqdn(vm.getFqdn());
            response.putResourceInfo(resourceInfo);

            if (vm.getMetricList() != null) {
                for (Metric metric : vm.getMetricList()) {
                    MetricType metricType = metric.getMetricType();
                    if (metricType.getType() == MetricType.Type.QOS_TERM) {
                        resourceInfo.putQosTerm(getQoSTermInfo(metric));
                    }
                }
            }
        }

        return response;
    }

    private static GetServiceSLASummaryResponse.QoSTermInfo getQoSTermInfo(Metric metric) throws Exception {
        GetServiceSLASummaryResponse.QoSTermInfo qosTermInfo = new GetServiceSLASummaryResponse.QoSTermInfo();
        MetricType metricType = metric.getMetricType();
        MetricValue metricValue = metric.getMetricValue();
        qosTermInfo.setGuaranteedTerm(MetricTypeEnum.convert(metricType));
        qosTermInfo.setMetricTitle(metricType.getTitle());
        qosTermInfo.setMetricDescription(metricType.getDescription());
        String value;
        if (metricType.getType() == MetricType.Type.CONFIG) {
            value = metric.getConfigValue();
        }
        else {
            value = metricValue.getValue();
        }
        qosTermInfo.setValue((value != null) ? metric.displayValue(value) : "");
        qosTermInfo.setUnit(""); // TODO: hack for the GUI
        qosTermInfo.setThreshold(metric.getViolationThresholdWithUnit());


        qosTermInfo.setCompliance(GetServiceSLASummaryResponse.QoSTermState.getQoSTermState(metricValue.getSlaCompliance(), metricValue.getViolationSeverity()));
        return qosTermInfo;
    }

    public static GetServiceEventsResponse processRequest(GetServiceEventsRequest request) throws Exception {
        GetServiceEventsResponse response = new GetServiceEventsResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());
        int maxNumberOfRecords = 50;

        List<AuditRecord> auditRecords;
        if (request.getServiceUri() != null) {
            Service service;
            try {
                service = ServiceManager.getInstance().findServiceByUri(request.getServiceUri());
            }
            catch (Exception e) {
                throw new Exception(String.format("Service '%s' not found.", request.getServiceUri()));
            }
            auditRecords = AuditRecordManager.getInstance().getAuditRecords(service, request.getUserId(), "Tashi", maxNumberOfRecords);
        }
        else {
            auditRecords = AuditRecordManager.getInstance().getAuditRecords("Tashi", maxNumberOfRecords);
        }

        for (AuditRecord auditRecord : auditRecords) {
            GetServiceEventsResponse.Event event = new GetServiceEventsResponse.Event();
            event.setSource(auditRecord.getSource());
            event.setTimestamp(auditRecord.getTimestamp());
            event.setType(GetServiceEventsResponse.EventType.AUDIT_RECORD);
            event.setUserId(auditRecord.getUserId());
            event.setDescription(auditRecord.getDescription());
            if (request.getServiceUri() != null) {
                event.setServiceUri(request.getServiceUri());
            }
            response.putEvent(event);
        }

        return response;
    }

    public static GetHostsInfoResponse processRequest(PubSubRequest request) throws Exception {
        GetHostsInfoResponse response = new GetHostsInfoResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();

        for (String hostFqdn : monitoringEngine.getAllHostMachines()) {
            GetHostsInfoResponse.Host host = new GetHostsInfoResponse.Host();
            response.putHost(host);
            host.setFqdn(hostFqdn);
            MetricData hostname = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.HOSTNAME);
            host.setName(hostname.getValue().toString());

            // CPU speed
            MetricData cpuSpeed = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_SPEED);
            if (cpuSpeed != null) {
                host.setCpuSpeed(String.format("%.1f %s", (Double) cpuSpeed.getValue(), cpuSpeed.getUnit()));
            }

            // CPU utilization
            MetricData cpuUser = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_USER);
            MetricData cpuSystem = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_SYSTEM);
            if (cpuUser != null && cpuSystem != null) {
                double cpuUtilization = (Double) cpuUser.getValue() + (Double) cpuSystem.getValue();
                host.setCpuUtilization(String.format("%.1f %%", cpuUtilization));
            }

            // disk_total
            MetricData diskTotal = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.DISK_TOTAL);
            if (diskTotal != null) {
                host.setDiskTotal(diskTotal.getValue() + " " + diskTotal.getUnit());
            }

            // disk_free
            MetricData diskFree = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.DISK_FREE);
            if (diskFree != null) {
                host.setDiskFree(diskFree.getValue() + " " + diskFree.getUnit());
            }

            // location
            MetricData location = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.LOCATION);
            if (location != null) {
                host.setLocation(location.getValue().toString());
            }

            // number of cores
            MetricData numOfCoresHost = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.CPU_CORES_COUNT);
            if (numOfCoresHost != null) {
                host.setNumOfCores(numOfCoresHost.getValue().toString());
            }

            // mem_total
            MetricData memTotal = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.MEM_TOTAL);
            if (memTotal != null) {
                host.setMemorySize(String.format("%.2f %s", (Double) memTotal.getValue(), memTotal.getUnit()));
            }

            // mem_free
            MetricData memFree = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.MEM_FREE);
            if (memFree != null) {
                host.setMemFree(String.format("%.2f %s", (Double) memFree.getValue(), memFree.getUnit()));
            }

            // poweredOn
            MetricData powerStatus = monitoringEngine.getHostMetricData(hostFqdn, RawMetric.POWER_STATUS);
            if (powerStatus != null) {
                host.setPoweredOn((Boolean) powerStatus.getValue());
            }

            // guest machines
            for (String vmFqdn : monitoringEngine.getGuestMachines(hostFqdn)) {
                GetHostsInfoResponse.Vm vm = new GetHostsInfoResponse.Vm();
                host.putGuestMachine(vm);
                vm.setFqdn(vmFqdn);
                String vmName = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.HOSTNAME).getValue().toString();
                vm.setName(vmName);

                // cpu load
                MetricData cpuLoad = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.CPU_LOAD);
                if (cpuLoad != null && numOfCoresHost != null) {
                    double cpuLoadPercentage = (Double) cpuLoad.getValue() / (Integer) numOfCoresHost.getValue() * 100;
                    vm.setCpuLoad(String.format("%.1f %%", cpuLoadPercentage));
                }

                // cpu_speed
                MetricData cpuSpeedVm = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.CPU_SPEED);
                if (cpuSpeedVm != null) {
                    vm.setCpuSpeed(String.format("%.1f %s", (Double) cpuSpeedVm.getValue(), cpuSpeedVm.getUnit()));
                }

                // image template name
                MetricData templateName = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.VM_IMAGE_TEMPLATE);
                if (templateName != null) {
                    vm.setImageTemplateName(templateName.getValue().toString());
                }

                // memory size (provisioned)
                MetricData memorySizeVm = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.MEM_TOTAL);
                if (memorySizeVm != null) {
                    vm.setMemorySize(String.format("%.2f %s", (Double) memorySizeVm.getValue(), memorySizeVm.getUnit()));
                }

                // memory usage
                MetricData memoryUsage = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.MEM_USAGE);
                if (memoryUsage != null) {
                    vm.setMemUsage(String.format("%.2f %s", (Double) memoryUsage.getValue(), memoryUsage.getUnit()));
                }

                // number of cores
                MetricData numOfCoresVm = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.CPU_CORES_COUNT);
                if (numOfCoresVm != null) {
                    vm.setNumOfCores(numOfCoresVm.getValue().toString());
                }

                // persistence
                MetricData persistence = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.VM_PERSISTENCE);
                if (persistence != null) {
                    vm.setPersistent(persistence.getValue().toString());
                }

                // service name and URI
                Vm vmJpa = VmManager.getInstance().findVmByFqdn(vmFqdn);
                if (vmJpa != null) {
                    vm.setServiceName(vmJpa.getService().getServiceName());
                    vm.setServiceUri(vmJpa.getService().getServiceUrl());
                }
                else {
                    vm.setServiceName("not defined");
                    vm.setServiceUri("not defined");
                }

                // vm state
                MetricData vmState = monitoringEngine.getVmMetricData(vmFqdn, RawMetric.VM_STATE);
                if (vmState != null) {
                    vm.setState((VmStateEnum) vmState.getValue());
                }
            }
        }

        return response;
    }

    public static GetSchedulerEventsResponse processRequest(GetSchedulerEventsRequest request) throws Exception {
        GetSchedulerEventsResponse response = new GetSchedulerEventsResponse();
        response.setInReplyTo(request.getMessageId());
        response.setTimestamp(new Date());

        List<AuditRecord> auditRecords = AuditRecordManager.getInstance().getAuditRecords("Scheduler", request.getMaxNumberOfRecords());

        for (AuditRecord auditRecord : auditRecords) {
            GetSchedulerEventsResponse.Event event = new GetSchedulerEventsResponse.Event();
            event.setSource(auditRecord.getSource());
            event.setTimestamp(auditRecord.getTimestamp());
            event.setDescription(auditRecord.getDescription());
            response.putEvent(event);
        }

        return response;
    }

    public static GenerateReportResponse processRequest(GenerateReportRequest request) throws Exception {
        if (request.getReportType() == ReportType.SERVICE_SUMMARY) {
            log.trace("Requested report type is ServiceSummaryReport.");

            GenerateReportRequest.ServiceSummary srvSumRequest = (GenerateReportRequest.ServiceSummary) request;
            log.trace(String.format("Parameters: service URI=%s, fromDate=%s, toDate=%s",
                    srvSumRequest.getServiceUri(), srvSumRequest.getFromDate(), srvSumRequest.getToDate()));

            Service service;
            try {
                service = ServiceManager.getInstance().findServiceByUri(srvSumRequest.getServiceUri());
            }
            catch (Exception e) {
                throw new Exception(String.format("Service '%s' not found.", srvSumRequest.getServiceUri()));
            }

            log.trace("Generating report...");
            ServiceSummaryReport report = new ServiceSummaryReport(service, srvSumRequest.getFromDate(),
                    srvSumRequest.getToDate());
            ByteArrayOutputStream baos = report.generate();

            GenerateReportResponse response = new GenerateReportResponse();
            response.setInReplyTo(request.getMessageId());
            response.setTimestamp(new Date());
            response.setPdfDocument(baos.toByteArray());
            response.setReportType(ReportType.SERVICE_SUMMARY);

            baos.close();
            log.trace("ServiceSummaryReport generated successfully.");
            return response;
        }
        else if (request.getReportType() == ReportType.INVESTMENT_GOVERNANCE) {
            log.trace("Requested report type is InvestmentGovernanceReport.");

            log.trace("Generating report...");
            InvestmentGovernanceReport report = new InvestmentGovernanceReport();
            ByteArrayOutputStream baos = report.generate();

            GenerateReportResponse response = new GenerateReportResponse();
            response.setInReplyTo(request.getMessageId());
            response.setTimestamp(new Date());
            response.setPdfDocument(baos.toByteArray());
            response.setReportType(ReportType.INVESTMENT_GOVERNANCE);

            baos.close();
            log.trace("InvestmentGovernanceReport generated successfully.");
            return response;
        }
        else {
            throw new Exception(String.format("Report type '%s' is not supported.", request.getReportType()));
        }
    }

    public static HashMap<String, String> computeUpDowntime(Service service) {
        Metric availMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_STATUS, service);
        HashMap<String, String> result = new HashMap<String, String>();
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric");
            query.setParameter("metric", availMetric);
            List results = query.getResultList();
            if (results.size() == 0) {
                // no availability history data available
                result.put("UPTIME", "0");
                result.put("DOWNTIME", "0");
                return result;
            }

            long upTime = 0;
            long downTime = 0;
            Iterator iterator = results.iterator();
            while (iterator.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) iterator.next();
                long interval = mvh.getEndTime().getTime() - mvh.getStartTime().getTime();
                if (Boolean.parseBoolean(mvh.getValue()) == true) {
                    upTime += interval;
                }
                else {
                    downTime += interval;
                }
            }
            long totalTime = upTime + downTime;
            if (totalTime > 0) {
                result.put("UPTIME", DateUtils.getHumanFriendlyTimeInterval(upTime));
                result.put("DOWNTIME", DateUtils.getHumanFriendlyTimeInterval(downTime));
                return result;
            }
            else {
                result.put("UPTIME", "0");
                result.put("DOWNTIME", "0");
                return result;
            }
        }
        finally {
            em.close();
        }
    }
}
