/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/Service.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.computation.AvailabilityRestrictions;
import org.slasoi.infrastructure.monitoring.computation.ReportingPeriod;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ReportingInterval;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "service", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_url"})})
@NamedQueries({
        @NamedQuery(name = "Service.findAll", query = "SELECT s FROM Service s"),
        @NamedQuery(name = "Service.findByServiceId", query = "SELECT s FROM Service s WHERE s.serviceId = :serviceId"),
        @NamedQuery(name = "Service.findByServiceName", query = "SELECT s FROM Service s WHERE s.serviceName = :serviceName"),
        @NamedQuery(name = "Service.findByServiceUrl", query = "SELECT s FROM Service s WHERE s.serviceUrl = :serviceUrl"),
        @NamedQuery(name = "Service.findByStartDate", query = "SELECT s FROM Service s WHERE s.startDate = :startDate")})
public class Service implements Serializable {
    private static Logger log = Logger.getLogger(Service.class);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "service_id", nullable = false)
    private Integer serviceId;
    @Basic(optional = false)
    @Column(name = "service_name", nullable = false, length = 64)
    private String serviceName;
    @Column(name = "service_url", length = 128)
    private String serviceUrl;
    @Basic(optional = false)
    @Column(name = "startDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "service")
    private List<Vm> vmList;
    @OneToMany(mappedBy = "service")
    private List<Metric> metricList;

    @Transient
    private Date cacheDate;
    @Transient
    private boolean isWorkingTime;

    public Service() {
    }

    public Service(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Service(Integer serviceId, String serviceName, Date startDate) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.startDate = startDate;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Vm> getVmList() {
        return vmList;
    }

    public void setVmList(List<Vm> vmList) {
        this.vmList = vmList;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceId != null ? serviceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Service)) {
            return false;
        }
        Service other = (Service) object;
        if ((this.serviceId == null && other.serviceId != null) || (this.serviceId != null && !this.serviceId.equals(other.serviceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.Service[serviceId=" + serviceId + "]";
    }

    public boolean isWorkingTime(Date metricsTimestamp) throws Exception {
        if (cacheDate != null && cacheDate.equals(metricsTimestamp)) {
            return isWorkingTime;
        }
        else {
            Metric availRestrMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_RESTRICTIONS, this);
            if (availRestrMetric != null && availRestrMetric.getConfigValue() != null) {
                AvailabilityRestrictions.Period period = AvailabilityRestrictions.Period.fromString(availRestrMetric.getConfigValue());
                log.trace("SERVICE_AVAILABILITY_RESTRICTIONS = " + period);
                AvailabilityRestrictions availRestr = new AvailabilityRestrictions(period);
                isWorkingTime = availRestr.isWorkingTime(metricsTimestamp);
                log.trace("Violations will be treated as " + (isWorkingTime ? ViolationSeverity.PUNISHABLE : ViolationSeverity.ACCEPTABLE));
            }
            else {
                log.trace("SERVICE_AVAILABILITY_RESTRICTIONS is not defined. All violations will be treated as " + ViolationSeverity.PUNISHABLE);
                isWorkingTime = true;
            }
            cacheDate = metricsTimestamp;
            return isWorkingTime;
        }
    }

    public ReportingPeriod getReportingPeriod() throws Exception {
        Metric reportingIntervalMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_REPORTING_INTERVAL, this);
        ReportingInterval reportingInterval = ReportingInterval.fromString(reportingIntervalMetric.getConfigValue());
        ReportingPeriod reportingPeriod = new ReportingPeriod(reportingInterval);
        return reportingPeriod;
    }

    public Metric getMetric(MetricTypeEnum metricTypeEnum) {
        return MetricManager.getInstance().findServiceMetric(metricTypeEnum, this);
    }

    public List<Metric> getAllMetrics() {
        List<Metric> metrics = new ArrayList<Metric>();

        // add service metrics
        for (Metric m : this.getMetricList()) {
            metrics.add(m);
        }

        // add VMs metrics
        for (Vm vm : this.getVmList()) {
            for (Metric m : vm.getMetricList()) {
                metrics.add(m);
            }
        }

        return metrics;
    }
}
