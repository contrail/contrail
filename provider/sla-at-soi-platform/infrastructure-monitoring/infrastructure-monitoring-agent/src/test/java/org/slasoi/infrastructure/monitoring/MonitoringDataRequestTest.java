/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/MonitoringDataRequestTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.SysInfoRequest;
import org.slasoi.infrastructure.monitoring.pubsub.SysInfoResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class MonitoringDataRequestTest {
    private static Logger log = Logger.getLogger(MonitoringDataRequestTest.class);
    private List<SysInfoResponse> responses = new ArrayList<SysInfoResponse>();

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    @DirtiesContext
    public void testSendSysInfoRequest() throws Exception {
        log.trace("Test testSendSysInfoRequest() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        MessageListener messageListener = registerMessagesListener(pubSubManager2);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Creating PubSubMessage-s.");
        SysInfoRequest[] requests = new SysInfoRequest[4];
        requests[0] = createSysInfoRequest("host", "iricbl011.openlab.com");
        requests[1] = createSysInfoRequest("vm", "vm1.openlab.com");
        requests[2] = createSysInfoRequest("vm", "vm2.openlab.com");
        requests[3] = createSysInfoRequest("cluster", "tashi.openlab.com");

        PubSubMessage[] messages = new PubSubMessage[requests.length];
        for (int i = 0; i < requests.length; i++) {
            messages[i] = createPubSubMessage(requests[i]);
        }

        log.trace("Sending monitoring data request to the InfrastructureMonitoringAgent.");
        for (int i = 0; i < requests.length; i++) {
            pubSubManager2.publish(messages[i]);
        }

        long startTime = System.currentTimeMillis();
        while (responses.size() < requests.length && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), requests.length);

        SysInfoResponse[] responses = new SysInfoResponse[requests.length];
        for (int i = 0; i < requests.length; i++) {
            responses[i] = findResponse(requests[i].getMessageId());
        }

        for (int i = 0; i < requests.length; i++) {
            assertEquals(responses[i].getFqdn(), requests[i].getFqdn());
            assertEquals(responses[i].getEntityType(), requests[i].getEntityType());
            assertEquals(responses[i].getResponseType(), "SysInfoResponse");
        }

        TestingUtils.unregisterMessagesListener(pubSubManager2, agent.getMonitoringDataRequestChannel().getName(),
                messageListener);
        agent.stop();
        log.trace("Test testSendSysInfoRequest() finished successfully.");
    }

    private SysInfoRequest createSysInfoRequest(String entityType, String machineName) {
        UUID uuid = UUID.randomUUID();
        SysInfoRequest request = new SysInfoRequest();
        request.setMessageId(uuid.toString());
        request.setEntityType(entityType);
        request.setFqdn(machineName);
        return request;
    }

    private PubSubMessage createPubSubMessage(SysInfoRequest request) throws JSONException {
        String payload = request.toJson();
        return new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived: " + payload);
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("SysInfoResponse")) {
                        SysInfoResponse sysInfoResponse = SysInfoResponse.fromJson(payload);
                        responses.add(sysInfoResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid SysInfoResponse response message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private SysInfoResponse findResponse(String responseId) {
        for (SysInfoResponse response : responses) {
            if (response.getInReplyTo().equals(responseId)) {
                return response;
            }
        }
        return null;
    }
}
