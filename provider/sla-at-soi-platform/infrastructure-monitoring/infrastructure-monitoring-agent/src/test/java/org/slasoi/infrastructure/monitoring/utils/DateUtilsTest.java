/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2978 $
 * @lastrevision $Date: 2011-08-12 15:39:20 +0200 (pet, 12 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/utils/DateUtilsTest.java $
 */

package org.slasoi.infrastructure.monitoring.utils;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DateUtilsTest {

    @Test
    public void testUtils() throws Exception {
        Date date = new Date(1311924206264L);
        String formattedDate = DateUtils.format(date);
        assertEquals(formattedDate, "2011-07-29T09:23:26.264+0200");

        Date date1 = DateUtils.parse("2011-07-29T09:23:26.264+0200");
        assertEquals(date1, date);

        Date date2 = DateUtils.parse("2011-07-29T09:23:26.264+0200", "yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        assertEquals(date2, date);

        String formattedDateDisp = DateUtils.formatForDisplay(date1);
        assertTrue(formattedDateDisp.equalsIgnoreCase("29 jul 2011 09:23:26"));

        String interval1 = DateUtils.getHumanFriendlyTimeInterval((2 * 60 + 30) * 1000);
        assertEquals(interval1, "3 min");

        String interval2 = DateUtils.getHumanFriendlyTimeInterval((3 * 3600 + 7 * 60 + 55) * 1000);
        assertEquals(interval2, "3 h 8 min");

        String interval3 = DateUtils.getHumanFriendlyTimeInterval((30 * 3600 + 20 * 60 + 10) * 1000);
        assertEquals(interval3, "1 day 6 h 20 min");

        String interval4 = DateUtils.getHumanFriendlyTimeInterval((3 * 24 * 3600 + 15 * 3600 + 45 * 60 + 50) * 1000);
        assertEquals(interval4, "3 days 15 h 46 min");
    }
}
