/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/MonitoringFeaturesBuilderTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.MonitoringFeaturesResponse;
import org.slasoi.infrastructure.monitoring.qos.MonitoringFeaturesBuilder;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class MonitoringFeaturesBuilderTest {
    private static Logger log = Logger.getLogger(MonitoringFeaturesBuilderTest.class);

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    MonitoringFeaturesResponse monitoringFeaturesResponse;

    @Test
    public void testGetMonitoringFeatures() throws Exception {
        log.trace("testGetMonitoringFeatures() started.");
        MonitoringFeaturesBuilder monitoringFeaturesBuilder = new MonitoringFeaturesBuilder();
        ComponentMonitoringFeatures[] cmf = monitoringFeaturesBuilder.getMonitoringFeatures();
        assertEquals(cmf.length, 3);
        assertEquals(cmf[0].getType(), "REASONER");
        assertEquals(cmf[1].getType(), "SENSOR");
        assertEquals(cmf[2].getType(), "SENSOR");

        assertEquals(cmf[0].getMonitoringFeatures().length, 45);
        assertEquals(cmf[1].getMonitoringFeatures().length, 2);
        assertEquals(cmf[2].getMonitoringFeatures().length, 15);

        XStream xstream = new XStream(new DomDriver());
        String cmfSer = xstream.toXML(cmf);

        //BufferedWriter out = new BufferedWriter(new FileWriter("E:\\temp\\tmp.xml"));
        //out.write(cmfSer);
        //out.close();

        ComponentMonitoringFeatures[] cmfDeser = (ComponentMonitoringFeatures[]) xstream.fromXML(cmfSer);
        assertEquals(cmfDeser.length, 3);
        assertEquals(cmfDeser[0].getType(), "REASONER");
        assertEquals(cmfDeser[1].getType(), "SENSOR");
        assertEquals(cmfDeser[2].getType(), "SENSOR");

        assertEquals(cmfDeser[0].getMonitoringFeaturesList().size(), 45);
        assertEquals(cmfDeser[1].getMonitoringFeaturesList().size(), 2);
        assertEquals(cmfDeser[2].getMonitoringFeaturesList().size(), 15);

        MonitoringFeature mf = cmfDeser[0].getMonitoringFeatures(0);
        assertEquals(mf.getName(), "http://www.slaatsoi.org/coremodel#series");
        assertEquals(mf.getDescription(), "Reasoner for Series of BOOLEAN");
        log.trace("testGetMonitoringFeatures() finished successfully.");
    }

    @Test
    public void testPubSubCommunication() throws Exception {
        log.trace("testPubSubCommunication() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        log.trace("Sending MonitoringFeaturesRequest to the configuration channel.");
        MessageListener messageListener = registerPubSubListener(pubSubManager2);
        MonitoringFeaturesRequest monFeaturesRequest = new MonitoringFeaturesRequest();
        monFeaturesRequest.setMessageId(UUID.randomUUID().toString());
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getConfigurationChannel().getName(), monFeaturesRequest.toJson());
        pubSubManager2.publish(pubSubMessage);

        log.trace("Waiting for MonitoringFeaturesResponse...");
        long startTime = System.currentTimeMillis();
        while (monitoringFeaturesResponse == null && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }

        log.trace("Checking MonitoringFeaturesResponse...");
        assertNotNull(monitoringFeaturesResponse);
        assertEquals(monFeaturesRequest.getMessageId(), monitoringFeaturesResponse.getInReplyTo());
        assertEquals(monitoringFeaturesResponse.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);

        ComponentMonitoringFeatures[] cmf = monitoringFeaturesResponse.getComponentMonitoringFeatures();
        assertEquals(cmf.length, 3);
        assertEquals(cmf[0].getType(), "REASONER");
        assertEquals(cmf[1].getType(), "SENSOR");
        assertEquals(cmf[2].getType(), "SENSOR");

        assertEquals(cmf[0].getMonitoringFeaturesList().size(), 45);
        assertEquals(cmf[1].getMonitoringFeaturesList().size(), 2);
        assertEquals(cmf[2].getMonitoringFeaturesList().size(), 15);

        MonitoringFeature mf = cmf[0].getMonitoringFeatures(0);
        assertEquals(mf.getName(), "http://www.slaatsoi.org/coremodel#series");
        assertEquals(mf.getDescription(), "Reasoner for Series of BOOLEAN");

        TestingUtils.unregisterMessagesListener(pubSubManager2, agent.getConfigurationChannel().getName(),
                messageListener);
        agent.stop();
        log.trace("testPubSubCommunication() finished successfully.");
    }

    private MessageListener registerPubSubListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Subscribing to the configuration channel.");
        pubSubManager.subscribe(agent.getConfigurationChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived: " + payload.substring(100) + "...");
                try {
                    PubSubResponse response = PubSubResponse.fromJson(payload);
                    String responseType = response.getResponseType();
                    if (responseType.equalsIgnoreCase("MonitoringFeaturesResponse")) {
                        monitoringFeaturesResponse = MonitoringFeaturesResponse.fromJson(payload);
                    }
                }
                catch (Exception e) {
                    fail("Invalid MonitoringFeaturesResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }
}
