/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3034 $
 * @lastrevision $Date: 2011-08-29 14:30:56 +0200 (pon, 29 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/IMATest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.computation.AvailabilityRestrictions;
import org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.*;
import org.slasoi.infrastructure.monitoring.jpa.managers.AuditRecordManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.monitors.tashisensor.TashiSensorMockup;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RemoveServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventMessage;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventType;
import org.slasoi.infrastructure.monitoring.qos.events.ViolationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class IMATest {
    private static Logger log = Logger.getLogger(IMATest.class);
    private List<ViolationEvent> violationEvents = new ArrayList<ViolationEvent>();

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    @DirtiesContext
    public void IMAFullTest() throws Exception {
        log.trace("Test IMAFullTest started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        //  subscribe to event channel and register violation messages listener
        MessageListener messageListener = registerViolationMessagesListener(pubSubManager2);
        TashiSensorMockup.getInstance().start();

        // check if the monitoring request was registered correctly
        log.trace("Checking if monitoring request was registered correctly...");
        // check service registration
        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        assert services.size() == 1;
        Service service = services.get(0);
        assert service.getServiceName().equals("TravelService");
        assert service.getServiceUrl().equals("slasoi://myManagedObject.company.com/Service/TravelService");

        // check vm registration
        List<Vm> vmList = service.getVmList();
        assertEquals(vmList.size(), 2);
        Vm vm1 = vmList.get(0);
        Vm vm2 = vmList.get(1);
        assertEquals(vm1.getService().getServiceId(), service.getServiceId());
        assertEquals(vm2.getService().getServiceId(), service.getServiceId());
        assertEquals(vm1.getName(), "vm1");
        assertEquals(vm2.getName(), "vm2");
        assertEquals(vm1.getFqdn(), "vm1.openlab.com");
        assertEquals(vm2.getFqdn(), "vm2.openlab.com");
        assertEquals(vm1.getClusterName(), "tashi.openlab.com");
        assertEquals(vm2.getClusterName(), "tashi.openlab.com");

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        // wait for violation event messages
        log.trace("Waiting for all violation messages to arrive...");
        long startTime = new Date().getTime();
        while (violationEvents.size() < 23 && new Date().getTime() - startTime < 30000) {
            Thread.sleep(200);
        }
        assertEquals(violationEvents.size(), 22);
        pubSubManager2.unsubscribe(agent.getEventChannel().getName());

        // check metrics registration and values
        log.trace("Checking metrics registration and metric values...");

        // SERVICE_REPORTING_INTERVAL
        Metric serviceRepIntervalMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_REPORTING_INTERVAL, service);
        assertNotNull(serviceRepIntervalMetric);
        ReportingInterval reportingInterval = ReportingInterval.fromString(serviceRepIntervalMetric.getConfigValue());
        assertEquals(reportingInterval, ReportingInterval.DAY);

        // SERVICE_AVAILABILITY_RESTRICTIONS
        Metric availRestrMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_RESTRICTIONS, service);
        assertNotNull(availRestrMetric);
        AvailabilityRestrictions.Period period = AvailabilityRestrictions.Period.fromString(availRestrMetric.getConfigValue());
        assertEquals(period, AvailabilityRestrictions.Period.NON_STOP);

        // ACCEPTABLE_SERVICE_VIOLATIONS
        Metric asvMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, service);
        assertNotNull(asvMetric);
        assertEquals(asvMetric.getViolationThreshold(), "0");

        // SERVICE_AVAILABILITY_STATUS
        Metric serviceAvailStatusMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_STATUS, service);
        assertNotNull(serviceAvailStatusMetric);
        assertEquals(Boolean.parseBoolean(serviceAvailStatusMetric.getMetricValue().getValue()), false);

        // SERVICE_AVAILABILITY
        Metric serviceAvailTotalMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY, service);
        assertNotNull(serviceAvailTotalMetric);
        assertEquals(Double.parseDouble(serviceAvailTotalMetric.getMetricValue().getValue()), 0, 0.0001);
        assertEquals(serviceAvailTotalMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(serviceAvailTotalMetric.getMetricValue().getViolationSeverity(), ViolationSeverity.PUNISHABLE);

        // VM_AVAILABILITY_STATUS
        Metric vmAvailStatusMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_AVAILABILITY_STATUS, vm1);
        Metric vmAvailStatusMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_AVAILABILITY_STATUS, vm2);
        assertNotNull(vmAvailStatusMetric1);
        assertNotNull(vmAvailStatusMetric2);
        assertEquals(Boolean.parseBoolean(vmAvailStatusMetric1.getMetricValue().getValue()), true);
        assertEquals(Boolean.parseBoolean(vmAvailStatusMetric2.getMetricValue().getValue()), false);

        // VM_AVAILABILITY
        Metric vmAvailTotalMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_AVAILABILITY, vm1);
        Metric vmAvailTotalMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_AVAILABILITY, vm2);
        assertNotNull(vmAvailTotalMetric1);
        assertNotNull(vmAvailTotalMetric2);
        assertEquals(Double.parseDouble(vmAvailTotalMetric1.getMetricValue().getValue()), 100, 0.0001);
        assertEquals(Double.parseDouble(vmAvailTotalMetric2.getMetricValue().getValue()), 0, 0.0001);
        assertEquals(serviceAvailTotalMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);

        // VM_CORES
        Metric vmCoresMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CORES, vm1);
        Metric vmCoresMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CORES, vm2);
        assertNotNull(vmCoresMetric1);
        assertNotNull(vmCoresMetric2);
        assertEquals(Integer.parseInt(vmCoresMetric1.getMetricValue().getValue()), 1);
        assertEquals(Integer.parseInt(vmCoresMetric2.getMetricValue().getValue()), 2);
        assertEquals(Integer.parseInt(vmCoresMetric1.getViolationThreshold()), 2);
        assertEquals(Integer.parseInt(vmCoresMetric2.getViolationThreshold()), 1);
        assertEquals(vmCoresMetric1.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(vmCoresMetric2.getMetricValue().getSlaCompliance(), SLAComplianceEnum.COMPLIANT);
        assertEquals(vmCoresMetric1.getMetricValue().getViolationSeverity(), ViolationSeverity.PUNISHABLE);

        // VM_MEMORY_SIZE
        Metric vmMemorySizeMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE, vm1);
        Metric vmMemorySizeMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE, vm2);
        assertNotNull(vmMemorySizeMetric1);
        assertNotNull(vmMemorySizeMetric2);
        assertEquals(Double.parseDouble(vmMemorySizeMetric1.getMetricValue().getValue()), 128, 0.0001);
        assertEquals(Double.parseDouble(vmMemorySizeMetric2.getMetricValue().getValue()), 256, 0.0001);
        assertEquals(Double.parseDouble(vmMemorySizeMetric1.getViolationThreshold()), 256, 0.0001);
        assertEquals(Double.parseDouble(vmMemorySizeMetric2.getViolationThreshold()), 200, 0.0001);

        // VM_MEMORY_SIZE_AVAILABLE
        Metric vmMemorySizeAvailMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, vm1);
        Metric vmMemorySizeAvailMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, vm2);
        assertNotNull(vmMemorySizeAvailMetric1);
        assertNotNull(vmMemorySizeAvailMetric2);
        assertEquals(Double.parseDouble(vmMemorySizeAvailMetric1.getMetricValue().getValue()), 197.391, 0.000001);
        assertEquals(Double.parseDouble(vmMemorySizeAvailMetric2.getMetricValue().getValue()), 216.535, 0.000001);
        assertEquals(Double.parseDouble(vmMemorySizeAvailMetric1.getViolationThreshold()), 256, 0.0001);
        assertEquals(Double.parseDouble(vmMemorySizeAvailMetric2.getViolationThreshold()), 200, 0.0001);

        // VM_MEMORY_SIZE_USED
        Metric vmMemorySizeUsedMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE_USED, vm1);
        Metric vmMemorySizeUsedMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_MEMORY_SIZE_USED, vm2);
        assertNotNull(vmMemorySizeUsedMetric1);
        assertNotNull(vmMemorySizeUsedMetric2);
        assertEquals(Double.parseDouble(vmMemorySizeUsedMetric1.getMetricValue().getValue()), 112.391, 0.0001);
        assertEquals(Double.parseDouble(vmMemorySizeUsedMetric2.getMetricValue().getValue()), 116.535, 0.0001);

        // VM_CPU_SPEED
        Metric vmCpuSpeedMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED, vm1);
        Metric vmCpuSpeedMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED, vm2);
        assertNotNull(vmCpuSpeedMetric1);
        assertNotNull(vmCpuSpeedMetric2);
        assertEquals(Double.parseDouble(vmCpuSpeedMetric1.getMetricValue().getValue()), 1.0, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedMetric2.getMetricValue().getValue()), 2.0, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedMetric1.getViolationThreshold()), 1.4, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedMetric2.getViolationThreshold()), 1.9, 0.0001);

        // VM_CPU_SPEED_AVAILABLE
        Metric vmCpuSpeedAvailMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, vm1);
        Metric vmCpuSpeedAvailMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, vm2);
        assertNotNull(vmCpuSpeedAvailMetric1);
        assertNotNull(vmCpuSpeedAvailMetric2);
        assertEquals(Double.parseDouble(vmCpuSpeedAvailMetric1.getMetricValue().getValue()), 10.0, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedAvailMetric2.getMetricValue().getValue()), 2.0, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedAvailMetric1.getViolationThreshold()), 1.4, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedAvailMetric2.getViolationThreshold()), 1.9, 0.0001);

        // VM_CPU_SPEED_USED
        Metric vmCpuSpeedUsedMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED_USED, vm1);
        Metric vmCpuSpeedUsedMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CPU_SPEED_USED, vm2);
        assertNotNull(vmCpuSpeedUsedMetric1);
        assertNotNull(vmCpuSpeedUsedMetric2);
        assertEquals(Double.parseDouble(vmCpuSpeedUsedMetric1.getMetricValue().getValue()), 0.8, 0.0001);
        assertEquals(Double.parseDouble(vmCpuSpeedUsedMetric2.getMetricValue().getValue()), 1.4, 0.0001);

        // VM_QUANTITY
        Metric vmQuantityMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.VM_QUANTITY, service);
        assertNotNull(vmQuantityMetric);
        assertEquals(Integer.parseInt(vmQuantityMetric.getMetricValue().getValue()), 2);

        // VM_PERSISTENCE
        Metric vmPersistence1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_PERSISTENCE, vm1);
        Metric vmPersistence2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_PERSISTENCE, vm2);
        assertNotNull(vmPersistence1);
        assertNotNull(vmPersistence2);
        assertEquals(Boolean.parseBoolean(vmPersistence1.getMetricValue().getValue()), false);
        assertEquals(Boolean.parseBoolean(vmPersistence2.getMetricValue().getValue()), false);

        // VM_IMAGE
        Metric vmImage1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_IMAGE, vm1);
        Metric vmImage2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_IMAGE, vm2);
        assertNotNull(vmImage1);
        assertNotNull(vmImage2);
        assertEquals(vmImage1.getMetricValue().getValue(), "tashi.img");
        assertEquals(vmImage2.getMetricValue().getValue(), "tashi.img");

        // VM_LOCATION
        Metric vmLocation1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_LOCATION, vm1);
        Metric vmLocation2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_LOCATION, vm2);
        assertNotNull(vmLocation1);
        assertNotNull(vmLocation2);
        assertEquals(vmLocation1.getMetricValue().getValue(), "IE");
        assertEquals(vmLocation2.getMetricValue().getValue(), "RU");

        // VM_SLA_COMPLIANCE
        Metric vmSlaCompliance1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_SLA_COMPLIANCE, vm1);
        Metric vmSlaCompliance2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_SLA_COMPLIANCE, vm2);
        assertNotNull(vmSlaCompliance1);
        assertNotNull(vmSlaCompliance2);
        assertEquals(SLAComplianceEnum.fromString(vmSlaCompliance1.getMetricValue().getValue()), SLAComplianceEnum.VIOLATION);
        assertEquals(SLAComplianceEnum.fromString(vmSlaCompliance2.getMetricValue().getValue()), SLAComplianceEnum.VIOLATION);

        // SERVICE_AUDITABILITY
        Metric serviceAuditabilityMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AUDITABILITY, service);
        assertNotNull(serviceAuditabilityMetric);
        assertEquals(serviceAuditabilityMetric.getMetricValue().getValue(), "false");

        // SERVICE_SAS70_COMPLIANCE
        Metric serviceSAS70ComplianceMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_SAS70_COMPLIANCE, service);
        assertNotNull(serviceSAS70ComplianceMetric);
        assertEquals(SAS70Compliance.fromString(serviceSAS70ComplianceMetric.getMetricValue().getValue()), SAS70Compliance.NOT_COMPLIANT);

        // SERVICE_CCR
        Metric serviceCcrComplianceMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_CCR, service);
        assertNotNull(serviceCcrComplianceMetric);
        assertEquals(CcrEnum.fromString(serviceCcrComplianceMetric.getMetricValue().getValue()), CcrEnum.CCR_REGION);

        // SERVICE_DATA_CLASSIFICATION
        Metric serviceDataClassMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_DATA_CLASSIFICATION, service);
        assertNotNull(serviceDataClassMetric);
        DataClassificationEnum dataClass = DataClassificationEnum.fromString(serviceDataClassMetric.getMetricValue().getValue());
        assertEquals(dataClass, DataClassificationEnum.PUBLIC);

        // SERVICE_HARDWARE_REDUNDANCY_LEVEL
        Metric serviceHWRedundLevelMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_HARDWARE_REDUNDANCY_LEVEL, service);
        assertNotNull(serviceHWRedundLevelMetric);
        HwRedundancyLevelEnum hwRedundLevel = HwRedundancyLevelEnum.fromString(serviceHWRedundLevelMetric.getMetricValue().getValue());
        assertEquals(hwRedundLevel, HwRedundancyLevelEnum.STANDARD);

        // SERVICE_SLA_COMPLIANCE
        Metric serviceSlaCompliance = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_SLA_COMPLIANCE, service);
        assertNotNull(serviceSlaCompliance);
        assertEquals(SLAComplianceEnum.fromString(serviceSlaCompliance.getMetricValue().getValue()), SLAComplianceEnum.VIOLATION);

        // SERVICE_MTTF
        Metric serviceMttfMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_MTTF, service);
        assertNotNull(serviceMttfMetric);
        assertEquals(serviceMttfMetric.getViolationThreshold(), "12");
        assertEquals(serviceMttfMetric.getWarningThreshold(), "14");
        assertEquals(serviceMttfMetric.getMetricValue().getDoubleValue(), 0, 0.0001);
        assertEquals(serviceMttfMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);

        // SERVICE_MTTR
        Metric serviceMttrMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_MTTR, service);
        assertNotNull(serviceMttrMetric);
        assertEquals(serviceMttrMetric.getViolationThreshold(), "15");
        assertEquals(serviceMttrMetric.getWarningThreshold(), "0.15");
        assertTrue(Double.isNaN(serviceMttrMetric.getMetricValue().getDoubleValue()));
        assertEquals(serviceMttrMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.COMPLIANT);

        // SERVICE_MTTV
        Metric serviceMttvMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_MTTV, service);
        assertNotNull(serviceMttvMetric);
        assertEquals(serviceMttvMetric.getViolationThreshold(), "6");
        assertEquals(serviceMttvMetric.getWarningThreshold(), "5");
        assertEquals(Double.parseDouble(serviceMttvMetric.getMetricValue().getValue()), 0, 0.0001);
        assertEquals(serviceMttvMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);

        // VM_DISK_THROUGHPUT
        Metric vmDiskThroughput1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DISK_THROUGHPUT, vm1);
        Metric vmDiskThroughput2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DISK_THROUGHPUT, vm2);
        assertNotNull(vmDiskThroughput1);
        assertNotNull(vmDiskThroughput2);
        assertEquals(DiskThroughputLevel.fromString(vmDiskThroughput1.getMetricValue().getValue()), DiskThroughputLevel.STANDARD);
        assertEquals(DiskThroughputLevel.fromString(vmDiskThroughput2.getMetricValue().getValue()), DiskThroughputLevel.BEST);

        // VM_NET_THROUGHPUT
        Metric vmNetThroughput1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_NET_THROUGHPUT, vm1);
        Metric vmNetThroughput2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_NET_THROUGHPUT, vm2);
        assertNotNull(vmNetThroughput1);
        assertNotNull(vmNetThroughput2);
        assertEquals(NetThroughputLevel.fromString(vmNetThroughput1.getMetricValue().getValue()), NetThroughputLevel.STANDARD);
        assertEquals(NetThroughputLevel.fromString(vmNetThroughput2.getMetricValue().getValue()), NetThroughputLevel.BEST);

        // VM_DATA_ENCRYPTION
        Metric vmDataEncryption1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DATA_ENCRYPTION, vm1);
        Metric vmDataEncryption2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DATA_ENCRYPTION, vm2);
        assertNotNull(vmDataEncryption1);
        assertNotNull(vmDataEncryption2);
        assertEquals(Boolean.parseBoolean(vmDataEncryption1.getMetricValue().getValue()), false);
        assertEquals(Boolean.parseBoolean(vmDataEncryption2.getMetricValue().getValue()), true);

        // SERVICE_ISOLATION
        Metric serviceIsolationMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_ISOLATION, service);
        assertNotNull(serviceIsolationMetric);
        assertEquals(ServiceIsolationLevel.fromString(serviceIsolationMetric.getViolationThreshold()), ServiceIsolationLevel.ISOLATED);
        assertEquals(ServiceIsolationLevel.fromString(serviceIsolationMetric.getMetricValue().getValue()), ServiceIsolationLevel.ISOLATED);
        assertEquals(serviceIsolationMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.COMPLIANT);

        // ACCEPTABLE_SERVICE_VIOLATIONS
        Metric acceptableSrvViolMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, service);
        assertNotNull(acceptableSrvViolMetric);
        assertEquals(Integer.parseInt(acceptableSrvViolMetric.getViolationThreshold()), 0);
        assertNull(acceptableSrvViolMetric.getWarningThreshold());
        assertEquals(Integer.parseInt(acceptableSrvViolMetric.getMetricValue().getValue()), 11);
        assertEquals(acceptableSrvViolMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);

        // CLUSTER_VCPU_USAGE
        Metric vcpuUsage = MetricManager.getInstance().findInfrastructureMetric(MetricTypeEnum.CLUSTER_VCPU_USAGE);
        assertNotNull(vcpuUsage);
        assertNull(vcpuUsage.getViolationThreshold());
        assertNull(vcpuUsage.getWarningThreshold());
        assertEquals(vcpuUsage.getMetricValue().getValue(), "9/40");

        // CLUSTER_CPU_SPEED_USAGE
        Metric cpuSpeedUsage = MetricManager.getInstance().findInfrastructureMetric(
                MetricTypeEnum.CLUSTER_CPU_SPEED_USAGE);
        assertNotNull(cpuSpeedUsage);
        assertNull(cpuSpeedUsage.getViolationThreshold());
        assertNull(cpuSpeedUsage.getWarningThreshold());
        assertEquals(cpuSpeedUsage.getMetricValue().getValue(), "9.00/145.52");

        // CLUSTER_PROVISIONED_MEMORY_USAGE
        Metric provMemoryUsage = MetricManager.getInstance().findInfrastructureMetric(
                MetricTypeEnum.CLUSTER_PROVISIONED_MEMORY_USAGE);
        assertNotNull(provMemoryUsage);
        assertNull(provMemoryUsage.getViolationThreshold());
        assertNull(provMemoryUsage.getWarningThreshold());
        assertEquals(provMemoryUsage.getMetricValue().getValue(), "896/28043");

        // CLUSTER_ACTUAL_MEMORY_USAGE
        Metric actualMemoryUsage = MetricManager.getInstance().findInfrastructureMetric(
                MetricTypeEnum.CLUSTER_ACTUAL_MEMORY_USAGE);
        assertNotNull(actualMemoryUsage);
        assertNull(actualMemoryUsage.getViolationThreshold());
        assertNull(actualMemoryUsage.getWarningThreshold());
        assertEquals(actualMemoryUsage.getMetricValue().getValue(), "622/28043");

        // CLUSTER_DISK_SPACE_USAGE
        Metric diskSpaceUsage = MetricManager.getInstance().findInfrastructureMetric(
                MetricTypeEnum.CLUSTER_DISK_SPACE_USAGE);
        assertNotNull(diskSpaceUsage);
        assertNull(diskSpaceUsage.getViolationThreshold());
        assertNull(diskSpaceUsage.getWarningThreshold());
        assertEquals(diskSpaceUsage.getMetricValue().getValue(), "19.890/137.506");


        // check if violation events arrived
        ViolationEvent violationEvent;
        log.trace("Checking violation event messages...");

        // VM_CORES
        // vm1 is in violation, vm2 is ok
        violationEvent = findVmViolationEvent(vm1, "VM_CORES");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "1");
        assertEquals(violationEvent.getMetricUnit(), "");
        assertEquals(violationEvent.getThresholdValue(), "2");
        assertEquals(violationEvent.getResourceName(), vm1.getName());
        assertEquals(violationEvent.getServiceUri(), service.getServiceUrl());

        violationEvent = findVmViolationEvent(vm2, "VM_CORES");
        assertNull(violationEvent);

        // VM_MEMORY_SIZE
        // vm1 is in violation, vm2 is ok
        violationEvent = findVmViolationEvent(vm1, "VM_MEMORY_SIZE");
        assertNotNull(violationEvent);
        assertEquals(Double.parseDouble(violationEvent.getMetricValue()), 128, 0.0001);
        assertEquals(violationEvent.getMetricUnit(), "MB");
        assertEquals(Double.parseDouble(violationEvent.getThresholdValue()), 256, 0.0001);
        assertEquals(violationEvent.getResourceName(), vm1.getName());
        assertEquals(violationEvent.getServiceUri(), service.getServiceUrl());

        violationEvent = findVmViolationEvent(vm2, "VM_MEMORY_SIZE");
        assertNull(violationEvent);

        // VM_MEMORY_SIZE_AVAILABLE
        violationEvent = findVmViolationEvent(vm1, "VM_MEMORY_SIZE_AVAILABLE");
        assertNotNull(violationEvent);
        assertEquals(Double.parseDouble(violationEvent.getMetricValue()), 197.391, 0.0001);
        assertEquals(violationEvent.getMetricUnit(), "MB");
        assertEquals(Double.parseDouble(violationEvent.getThresholdValue()), 256, 0.0001);
        assertEquals(violationEvent.getResourceName(), vm1.getName());
        assertEquals(violationEvent.getServiceUri(), service.getServiceUrl());

        violationEvent = findVmViolationEvent(vm2, "VM_MEMORY_SIZE_AVAILABLE");
        assertNull(violationEvent);

        // VM_CPU_SPEED
        // vm1 is in violation, vm2 is ok
        violationEvent = findVmViolationEvent(vm1, "VM_CPU_SPEED");
        assertNotNull(violationEvent);
        assertEquals(Double.parseDouble(violationEvent.getMetricValue()), 1.0, 0.0001);
        assertEquals(violationEvent.getMetricUnit(), "GHz");
        assertEquals(Double.parseDouble(violationEvent.getThresholdValue()), 1.4, 0.0001);
        assertEquals(violationEvent.getResourceName(), vm1.getName());
        assertEquals(violationEvent.getServiceUri(), service.getServiceUrl());

        violationEvent = findVmViolationEvent(vm2, "VM_CPU_SPEED");
        assertNull(violationEvent);

        // VM_LOCATION
        violationEvent = findVmViolationEvent(vm1, "VM_LOCATION");
        assertNull(violationEvent);

        violationEvent = findVmViolationEvent(vm2, "VM_LOCATION");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "RU");

        // SERVICE_AUDITABILITY
        violationEvent = findServiceViolationEvent(service, "SERVICE_AUDITABILITY");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "false");

        // SERVICE_CCR
        violationEvent = findServiceViolationEvent(service, "SERVICE_CCR");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "CCR_REGION");

        // SERVICE_DATA_CLASSIFICATION
        violationEvent = findServiceViolationEvent(service, "SERVICE_DATA_CLASSIFICATION");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "PUBLIC");

        // SERVICE_HARDWARE_REDUNDANCY_LEVEL
        violationEvent = findServiceViolationEvent(service, "SERVICE_HARDWARE_REDUNDANCY_LEVEL");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "STANDARD");

        // SERVICE_MTTF
        violationEvent = findServiceViolationEvent(service, "SERVICE_MTTF");
        assertNotNull(violationEvent);
        assertEquals(Double.parseDouble(violationEvent.getMetricValue()), 0, 0.0001);

        // SERVICE_MTTR
        violationEvent = findServiceViolationEvent(service, "SERVICE_MTTR");
        assertNull(violationEvent);

        // SERVICE_MTTV
        violationEvent = findServiceViolationEvent(service, "SERVICE_MTTV");
        assertNotNull(violationEvent);
        assertEquals(Double.parseDouble(violationEvent.getMetricValue()), 0, 0.0001);

        // VM_DISK_THROUGHPUT
        violationEvent = findVmViolationEvent(vm1, "VM_DISK_THROUGHPUT");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "STANDARD");
        assertEquals(violationEvent.getThresholdValue(), "BEST");

        violationEvent = findVmViolationEvent(vm2, "VM_DISK_THROUGHPUT");
        assertNull(violationEvent);

        // VM_NET_THROUGHPUT
        violationEvent = findVmViolationEvent(vm1, "VM_NET_THROUGHPUT");
        assertNotNull(violationEvent);
        assertEquals(violationEvent.getMetricValue(), "STANDARD");
        assertEquals(violationEvent.getThresholdValue(), "BEST");

        violationEvent = findVmViolationEvent(vm2, "VM_NET_THROUGHPUT");
        assertNull(violationEvent);

        // VM_DATA_ENCRYPTION
        violationEvent = findVmViolationEvent(vm1, "VM_DATA_ENCRYPTION");
        assertNotNull(violationEvent);
        assertEquals(Boolean.parseBoolean(violationEvent.getMetricValue()), false);
        assertEquals(Boolean.parseBoolean(violationEvent.getThresholdValue()), true);

        violationEvent = findVmViolationEvent(vm2, "VM_DATA_ENCRYPTION");
        assertNull(violationEvent);

        // SERVICE_ISOLATION
        violationEvent = findServiceViolationEvent(service, "SERVICE_ISOLATION");
        assertNull(violationEvent);

        // ACCEPTABLE_SERVICE_VIOLATIONS
        violationEvent = findServiceViolationEvent(service, "ACCEPTABLE_SERVICE_VIOLATIONS");
        assertNotNull(violationEvent);

        log.trace("Checking if all audit record messages arrived...");
        startTime = new Date().getTime();
        List<AuditRecord> auditRecords;
        do {
            Thread.sleep(100);
            auditRecords = AuditRecordManager.getInstance().getAuditRecords("Tashi", 10);
        } while (auditRecords.size() < 3 && new Date().getTime() - startTime < 30000);
        assertEquals(auditRecords.size(), 3);

        log.trace("Removing service...");
        RemoveServiceRequest removeRequest = new RemoveServiceRequest();
        removeRequest.setMessageId(UUID.randomUUID().toString());
        removeRequest.setRecipient(InfrastructureMonitoringAgent.APPLICATION_NAME);
        removeRequest.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        removeRequest.setRemoveFromDB(true);
        serviceRegistrationHelper.sendRemoveServiceRequest(removeRequest);
        Service removedService = ServiceManager.getInstance().findServiceByUri(
                "slasoi://myManagedObject.company.com/Service/TravelService");
        assertNull(removedService);

        log.trace("Stopping Infrastructure Monitoring Agent and Tashi Sensor mockup...");
        TestingUtils.unregisterMessagesListener(pubSubManager2, agent.getEventChannel().getName(), messageListener);
        TashiSensorMockup.getInstance().stop();
        agent.stop();
        log.trace("Infrastructure Monitoring Agent test finished successfully.");
    }

    private MessageListener registerViolationMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        pubSubManager.subscribe(agent.getEventChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New violation event message arrived: " + payload);
                try {
                    JSONObject o = new JSONObject(payload);
                    EventMessage eventMessage = new EventMessage(o);
                    assertEquals(eventMessage.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
                    if (eventMessage.getEventType() == EventType.SLA_VIOLATION) {
                        ViolationEvent violationEvent = new ViolationEvent(eventMessage.getEventData());
                        violationEvents.add(violationEvent);
                    }
                }
                catch (Exception e) {
                    fail("Invalid violation event message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private ViolationEvent findVmViolationEvent(Vm vm, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getResourceName().equals(vm.getName())) {
                return v;
            }
        }
        return null;
    }

    private ViolationEvent findServiceViolationEvent(Service service, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getServiceUri().equals(service.getServiceUrl())) {
                return v;
            }
        }
        return null;
    }
}
