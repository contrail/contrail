/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/TashiClusterConfMessageTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.jpa.enums.DataClassificationEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.DataType;
import org.slasoi.infrastructure.monitoring.jpa.enums.HwRedundancyLevelEnum;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class TashiClusterConfMessageTest {
    private static Logger log = Logger.getLogger(TashiClusterConfMessageTest.class);

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Test
    public void testTashiClusterConfMessage() throws Exception {
        log.trace("Test testTashiClusterConfMessage started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // initiate one monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Checking if ClusterConfiguration message was received, parsed and stored.");
        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();
        String fqdn = "iricbl011.openlab.com";

        MetricData metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.AUDITABILITY);
        assertEquals(metricData.getValue(), false);
        assertEquals(metricData.getType(), DataType.BOOLEAN);
        assertEquals(metricData.getUnit(), "");

        metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.LOCATION);
        assertEquals(metricData.getValue(), "IE");
        assertEquals(metricData.getType(), DataType.STRING);
        assertEquals(metricData.getUnit(), "");

        metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.SAS70_COMPLIANCE);
        assertEquals(metricData.getValue(), false);
        assertEquals(metricData.getType(), DataType.BOOLEAN);
        assertEquals(metricData.getUnit(), "");

        metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.CCR);
        assertEquals(metricData.getValue(), true);
        assertEquals(metricData.getType(), DataType.BOOLEAN);
        assertEquals(metricData.getUnit(), "");

        metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.DATA_CLASSIFICATION);
        assertEquals(metricData.getValue(), DataClassificationEnum.PUBLIC);
        assertEquals(metricData.getType(), DataType.STRING);
        assertEquals(metricData.getUnit(), "");

        metricData = monitoringEngine.getHostMetricData(fqdn, RawMetric.HW_REDUNDANCY_LEVEL);
        assertEquals(metricData.getValue(), HwRedundancyLevelEnum.STANDARD);
        assertEquals(metricData.getType(), DataType.STRING);
        assertEquals(metricData.getUnit(), "");

        log.trace("Test testTashiClusterConfMessage() finished.");
    }
}
