/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/ServiceRegistrationHelper.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.computation.AvailabilityRestrictions;
import org.slasoi.infrastructure.monitoring.jpa.enums.CcrEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SAS70Compliance;
import org.slasoi.infrastructure.monitoring.jpa.enums.ServiceIsolationLevel;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RemoveServiceRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@Ignore
public class ServiceRegistrationHelper {
    private static Logger log = Logger.getLogger(ServiceRegistrationHelper.class);
    private PubSubManager pubSubManager;
    private InfrastructureMonitoringAgent agent;
    private List<PubSubResponse> responses;
    private MessageListener messageListener;

    public ServiceRegistrationHelper(InfrastructureMonitoringAgent agent, PubSubManager pubSubManager) {
        this.agent = agent;
        this.pubSubManager = pubSubManager;
    }

    public void sendRegisterServiceRequest(RegisterServiceRequest registerServiceRequest) throws Exception {
        log.trace("Sending RegisterServiceRequest to the configuration channel.");
        responses = new ArrayList<PubSubResponse>();
        registerPubSubResponseMessagesListener();
        PubSubMessage message = new PubSubMessage(agent.getConfigurationChannel().getName(), registerServiceRequest.toJson());
        pubSubManager.publish(message);

        log.trace("Waiting for the response to the RegisterServiceRequest...");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 60000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);
        PubSubResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), registerServiceRequest.getMessageId());
        assertEquals(response.getStatus(), PubSubResponse.Status.OK);
        log.trace("Response to the RegisterServiceRequest is correct.");

        unregisterPubSubResponseMessagesListener();
    }

    public void sendRemoveServiceRequest(RemoveServiceRequest request) throws Exception {
        log.trace("Sending RemoveServiceRequest to the configuration channel.");
        responses = new ArrayList<PubSubResponse>();
        registerPubSubResponseMessagesListener();
        PubSubMessage message = new PubSubMessage(agent.getConfigurationChannel().getName(), request.toJson());
        pubSubManager.publish(message);

        log.trace("Waiting for the response to the RemoveServiceRequest...");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 60000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);
        PubSubResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), request.getMessageId());
        assertEquals(response.getStatus(), PubSubResponse.Status.OK);
        log.trace("Response to the RemoveServiceRequest is correct.");

        unregisterPubSubResponseMessagesListener();
    }

    public RegisterServiceRequest createRegisterServiceRequest() {
        return createRegisterServiceRequest(AvailabilityRestrictions.Period.NON_STOP, "0");
    }

    public RegisterServiceRequest createRegisterServiceRequest(AvailabilityRestrictions.Period workingPeriod, String acceptableSrvViolations) {
        RegisterServiceRequest request = new RegisterServiceRequest();
        request.setMessageId(UUID.randomUUID().toString());
        request.setRecipient(InfrastructureMonitoringAgent.APPLICATION_NAME);
        request.setServiceName("TravelService");
        request.setServiceUrl("slasoi://myManagedObject.company.com/Service/TravelService");

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_REPORTING_INTERVAL, "DAY", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AVAILABILITY_RESTRICTIONS, workingPeriod.name(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, acceptableSrvViolations, null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AVAILABILITY, "98", "99", "%"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_QUANTITY, "2", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AUDITABILITY, "true", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_SAS70_COMPLIANCE, SAS70Compliance.COMPLIANT.toString(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_CCR, CcrEnum.CCR_REGION_FORBIDDEN.toString(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_DATA_CLASSIFICATION, "Secret", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_HARDWARE_REDUNDANCY_LEVEL, "Best", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTF, "12", "14", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTR, "15", "0.15", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTV, "6", "5", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_ISOLATION, ServiceIsolationLevel.ISOLATED.toString(), null, ""));

        // vm1
        RegisterServiceRequest.Vm vm1 = new RegisterServiceRequest.Vm("vm1", "vm1.openlab.com");
        vm1.setClusterName("tashi.openlab.com");
        request.putVm(vm1);

        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_AVAILABILITY, "98", "98.5", "%"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CORES, "2", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED, "1.4", null, "GHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, "1.4", "1.6", "GHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE, "256", null, "MB"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, "256", null, "MHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_PERSISTENCE, "true", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_IMAGE, "tashi1.img", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_LOCATION, "IE, GB, DE, SI", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DISK_THROUGHPUT, "BEST", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_NET_THROUGHPUT, "BEST", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DATA_ENCRYPTION, "True", null, ""));

        // vm2
        RegisterServiceRequest.Vm vm2 = new RegisterServiceRequest.Vm("vm2", "vm2.openlab.com");
        vm2.setClusterName("tashi.openlab.com");
        request.putVm(vm2);

        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CORES, "1", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED, "1.9", null, "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, "1.9", "2.1", "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE, "200", null, "MB"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, "200", null, "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_PERSISTENCE, "true", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_IMAGE, "tashi1.img", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_LOCATION, "IE, GB, DE, SI", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DISK_THROUGHPUT, "STANDARD", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_NET_THROUGHPUT, "STANDARD", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DATA_ENCRYPTION, "False", null, ""));

        return request;
    }

    public RegisterServiceRequest createRegisterServiceRequestForASVTest() {
        RegisterServiceRequest request = new RegisterServiceRequest();
        request.setMessageId(UUID.randomUUID().toString());
        request.setRecipient(InfrastructureMonitoringAgent.APPLICATION_NAME);
        request.setServiceName("TravelService");
        request.setServiceUrl("slasoi://myManagedObject.company.com/Service/TravelService");

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_REPORTING_INTERVAL, "DAY", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AVAILABILITY_RESTRICTIONS, AvailabilityRestrictions.Period.NON_STOP.name(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, "5", "4", ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AVAILABILITY, "98", "99", "%"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_QUANTITY, "2", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_AUDITABILITY, "false", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_SAS70_COMPLIANCE, SAS70Compliance.NOT_IMPORTANT.toString(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_CCR, CcrEnum.CCR_REGION_ALLOWED.toString(), null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_DATA_CLASSIFICATION, "PUBLIC", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_HARDWARE_REDUNDANCY_LEVEL, "STANDARD", null, ""));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTF, "12", "14", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTR, "15", "12", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_MTTV, "6", "5", "s"));

        request.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.SERVICE_ISOLATION, ServiceIsolationLevel.ISOLATED.toString(), null, ""));

        // vm1
        RegisterServiceRequest.Vm vm1 = new RegisterServiceRequest.Vm("vm1", "vm1.openlab.com");
        vm1.setClusterName("tashi.openlab.com");
        request.putVm(vm1);

        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CORES, "1", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED, "1.0", null, "GHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, "1.0", "1.6", "GHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE, "256", null, "MB"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, "128", null, "MHz"));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_PERSISTENCE, "false", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_IMAGE, "tashi.img", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_LOCATION, "IE, GB, DE, SI", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DISK_THROUGHPUT, "STANDARD", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_NET_THROUGHPUT, "STANDARD", null, ""));
        vm1.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DATA_ENCRYPTION, "False", null, ""));

        // vm2
        RegisterServiceRequest.Vm vm2 = new RegisterServiceRequest.Vm("vm2", "vm2.openlab.com");
        vm2.setClusterName("tashi.openlab.com");
        request.putVm(vm2);

        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CORES, "1", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED, "2.0", null, "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_CPU_SPEED_AVAILABLE, "1.6", "1.5", "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE, "200", null, "MB"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_MEMORY_SIZE_AVAILABLE, "200", null, "MHz"));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_PERSISTENCE, "false", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_IMAGE, "tashi.img", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_LOCATION, "IE, GB, DE, RU", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DISK_THROUGHPUT, "STANDARD", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_NET_THROUGHPUT, "STANDARD", null, ""));
        vm2.addAgreementTerm(new RegisterServiceRequest.AgreementTerm(
                MetricTypeEnum.VM_DATA_ENCRYPTION, "False", null, ""));

        return request;
    }

    private void registerPubSubResponseMessagesListener() throws MessagingException, InterruptedException {
        pubSubManager.subscribe(agent.getConfigurationChannel().getName());

        messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived: " + payload);
                try {
                    PubSubResponse response = PubSubResponse.fromJson(payload);
                    responses.add(response);
                }
                catch (Exception e) {
                    fail("Invalid PubSubResponse message.");
                }
            }
        };

        pubSubManager.addMessageListener(messageListener);
    }

    private void unregisterPubSubResponseMessagesListener() throws Exception {
        pubSubManager.removeMessageListener(messageListener);
        messageListener = null;
        pubSubManager.unsubscribe(agent.getConfigurationChannel().getName());
    }
}
