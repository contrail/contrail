/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev$
 * @lastrevision $Date$
 * @filesource $URL$
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceSLASummaryRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceSLASummaryResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class GetServiceSLASummaryTest {
    private static Logger log = Logger.getLogger(GetServiceSLASummaryTest.class);
    private List<GetServiceSLASummaryResponse> responses = new ArrayList<GetServiceSLASummaryResponse>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("tashiMockupSensorMessaging")
    private Messaging tashiMockupSensorMessaging;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testGetServiceSLASummary() throws Exception {
        log.trace("testGetServiceSLASummary() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send RegisterService request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        MessageListener messageListener = registerMessagesListener(pubSubManager2);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Sending GetServiceSLASummaryRequest message to pubsub.");
        GetServiceSLASummaryRequest request = new GetServiceSLASummaryRequest();
        request.setMessageId(UUID.randomUUID().toString());
        request.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        String payload = request.toJson();
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
        pubSubManager2.publish(pubSubMessage);

        log.trace("Waiting for response...");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);

        log.trace("Checking GetServiceSLASummaryResponse...");
        GetServiceSLASummaryResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), request.getMessageId());
        assertEquals(response.getResponseType(), "GetServiceSLASummaryResponse");
        assertNotNull(response.getTimestamp());
        assertEquals(response.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
        assertEquals(response.getServiceUri(), request.getServiceUri());

        // check SERVICE_AVAILABILITY QoS term info
        GetServiceSLASummaryResponse.QoSTermInfo availabilityInfo = null;
        for (GetServiceSLASummaryResponse.QoSTermInfo qosTermInfo : response.getQosTerms()) {
            if (qosTermInfo.getGuaranteedTerm() == MetricTypeEnum.SERVICE_AVAILABILITY) {
                availabilityInfo = qosTermInfo;
            }
        }
        assertNotNull(availabilityInfo);
        assertEquals(availabilityInfo.getValue(), "0.00 %");
        assertEquals(availabilityInfo.getThreshold(), "98 %");
        assertEquals(availabilityInfo.getUnit(), "%");
        assertEquals(availabilityInfo.getCompliance(), GetServiceSLASummaryResponse.QoSTermState.PUNISHABLE_VIOLATION);

        assertEquals(response.getResources().size(), 2);

        // find vm1
        GetServiceSLASummaryResponse.ResourceInfo vm1ResourceInfo = null;
        for (GetServiceSLASummaryResponse.ResourceInfo resourceInfo : response.getResources()) {
            if (resourceInfo.getResourceFqdn().equals("vm1.openlab.com")) {
                vm1ResourceInfo = resourceInfo;
            }
        }
        assertNotNull(vm1ResourceInfo);

        // check VM_CORES QoS term info
        GetServiceSLASummaryResponse.QoSTermInfo vmCoresInfo = null;
        for (GetServiceSLASummaryResponse.QoSTermInfo qosTermInfo : vm1ResourceInfo.getQosTerms()) {
            if (qosTermInfo.getGuaranteedTerm() == MetricTypeEnum.VM_CORES) {
                vmCoresInfo = qosTermInfo;
            }
        }
        assertNotNull(vmCoresInfo);
        assertEquals(Integer.parseInt(vmCoresInfo.getValue()), 1);
        assertEquals(Integer.parseInt(vmCoresInfo.getThreshold()), 2);
        assertEquals(vmCoresInfo.getUnit(), null);
        assertEquals(vmCoresInfo.getCompliance(), GetServiceSLASummaryResponse.QoSTermState.PUNISHABLE_VIOLATION);

        log.trace("GetServiceSLASummaryResponse is ok.");

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("testGetServiceSLASummary() finished.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("GetServiceSLASummaryResponse")) {
                        GetServiceSLASummaryResponse slaSummaryResponse = GetServiceSLASummaryResponse.fromJson(payload);
                        responses.add(slaSummaryResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid GetServiceSLASummaryResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }
}