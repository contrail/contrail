/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/TestingUtils.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Ignore
public class TestingUtils {
    private static Logger log = Logger.getLogger(TestingUtils.class);

    public static void cleanupDatabase() {
        log.trace("cleanupDatabase() started.");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM MetricValue v");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM MetricValueHistory h");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Violation v");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Metric m");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Vm v");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Service s");
        query.executeUpdate();

        query = em.createQuery("DELETE FROM AuditRecord a");
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
        log.trace("cleanupDatabase() finished successfully.");
    }

    public static PubSubResponse findResponse(String messageId, List<? extends PubSubResponse> responses) {
        for (PubSubResponse response : responses) {
            if (response.getInReplyTo().equals(messageId)) {
                return response;
            }
        }
        return null;
    }

    public static void unregisterMessagesListener(PubSubManager pubSubManager, String channel, MessageListener
                                            messageListener) throws MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(channel);
    }
}
