/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/GetServiceEventsTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord;
import org.slasoi.infrastructure.monitoring.jpa.managers.AuditRecordManager;
import org.slasoi.infrastructure.monitoring.monitors.tashisensor.TashiSensorMockup;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceEventsRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceEventsResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.ServiceEventNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class GetServiceEventsTest {
    private static Logger log = Logger.getLogger(GetServiceEventsTest.class);
    private List<GetServiceEventsResponse> responses = new ArrayList<GetServiceEventsResponse>();
    private List<ServiceEventNotification> notifications = new ArrayList<ServiceEventNotification>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("tashiMockupSensorMessaging")
    private Messaging tashiMockupSensorMessaging;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testGetServiceEvents() throws Exception {
        log.trace("testGetServiceEvents started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send RegisterService request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        MessageListener messageListener = registerMessagesListener(pubSubManager2);
        TashiSensorMockup.getInstance().start();

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Waiting for some AuditRecord messages from Tashi...");
        Thread.sleep(3000);
        long startTime = new Date().getTime();
        List<AuditRecord> auditRecords;
        do {
            Thread.sleep(1000);
            auditRecords = AuditRecordManager.getInstance().getAuditRecords("Tashi", 10);
        } while (auditRecords.size() < 3 && new Date().getTime() - startTime < 30000);

        for (AuditRecord r : auditRecords) {
            log.trace(r.getDescription() + "; " + r.getSource() + "; " + r.getTimestamp());
        }

        assertEquals(auditRecords.size(), 3);


        log.trace("Sending GetServiceEventsRequest message to pubsub.");
        GetServiceEventsRequest request1 = new GetServiceEventsRequest();
        request1.setMessageId(UUID.randomUUID().toString());
        request1.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        request1.setUserId("1006");
        String payload = request1.toJson();
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
        pubSubManager2.publish(pubSubMessage);

        GetServiceEventsRequest request2 = new GetServiceEventsRequest();
        request2.setMessageId(UUID.randomUUID().toString());
        String payload2 = request2.toJson();
        PubSubMessage pubSubMessage2 = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload2);
        pubSubManager2.publish(pubSubMessage2);

        log.trace("Waiting for response.");
        startTime = System.currentTimeMillis();
        while (responses.size() + notifications.size() < 5 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 2);
        assertEquals(notifications.size(), 3);

        log.trace("Checking GetServiceEventsResponse responses...");
        GetServiceEventsResponse response1 = (GetServiceEventsResponse) TestingUtils.findResponse(request1.getMessageId(), responses);
        assertEquals(response1.getInReplyTo(), request1.getMessageId());
        assertEquals(response1.getResponseType(), "GetServiceEventsResponse");
        assertNotNull(response1.getTimestamp());
        assertEquals(response1.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);

        assertEquals(response1.getEvents().size(), 2);
        GetServiceEventsResponse.Event event1 = response1.getEvents().get(0);
        assertEquals(event1.getServiceUri(), request1.getServiceUri());
        assertEquals(event1.getSource(), "Tashi");
        assertEquals(event1.getType(), GetServiceEventsResponse.EventType.AUDIT_RECORD);
        assertEquals(event1.getUserId(), "1006");
        assertEquals(event1.getDescription(), "VM vm1.openlab.com on host iricbl011.openlab.com paused");
        assertNotNull(event1.getTimestamp());

        GetServiceEventsResponse response2 = (GetServiceEventsResponse) TestingUtils.findResponse(request2.getMessageId(), responses);
        assertEquals(response2.getInReplyTo(), request2.getMessageId());
        assertEquals(response2.getResponseType(), "GetServiceEventsResponse");
        assertNotNull(response2.getTimestamp());
        assertEquals(response2.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
        assertEquals(response2.getEvents().size(), 3);

        TashiSensorMockup.getInstance().stop();
        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("testGetServiceEvents() finished successfully.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse response = PubSubResponse.fromJson(payload);
                    log.trace("Message type is " + response.getResponseType());
                    if (response.getResponseType().equals("GetServiceEventsResponse")) {
                        GetServiceEventsResponse serviceEventsResponse = GetServiceEventsResponse.fromJson(payload);
                        responses.add(serviceEventsResponse);
                    }
                    else if (response.getResponseType().equals("ServiceEventNotification")) {
                        ServiceEventNotification notification = ServiceEventNotification.fromJson(payload);
                        notifications.add(notification);
                    }
                    else {
                        log.trace("Unexpected message received.");
                    }
                }
                catch (Exception e) {
                    assert false : "Invalid message received.";
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }
}
