/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/DroolsGateway.java $
 */

package org.slasoi.llms.manager;

import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseConfiguration;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.conf.EventProcessingOption;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slasoi.llms.common.Observation;
import org.slasoi.llms.common.RulePackage;

import java.io.File;

public class DroolsGateway {
    private static final Logger log = Logger.getLogger(DroolsGateway.class);

    KnowledgeBase kbase;
    StatefulKnowledgeSession ksession;

    public DroolsGateway() {
        //tells Drools to operate in STREAM mode, i.e. to be aware of time and hence allow temporal operations.
        KnowledgeBaseConfiguration conf = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
        conf.setOption(EventProcessingOption.STREAM);
//        config.setOption( ClockTypeOption.get("realtime") ); // already by default

        kbase = KnowledgeBaseFactory.newKnowledgeBase(conf);
        ksession = kbase.newStatefulKnowledgeSession();  // IT SEEM THAT NEW RULES PROPERLY APPLY TO THIS SESSION

        String packageFile = System.getenv("SLASOI_HOME") + File.separator + "LLMS" + File.separator + "core" + File.separator + "InitRulePackage.drl";
        log.debug("Loading drools package file " + packageFile);
        addPackage(RulePackage.createRulePackageFromFile(packageFile));
        // needed that Observation is defined as event at initialization
        // at provisioning time it is too late, as the observations may arrive before that

        // add several entry-points (at least one per each sender, probably one per metric) as events inside one entry-point must be ordered TODO
    }

    protected void finalize() {
        ksession.dispose();
    }

    public synchronized boolean addPackage(RulePackage rulePackage) {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(rulePackage.getResource(), rulePackage.getType());

        KnowledgeBuilderErrors errors = kbuilder.getErrors();
        if (errors.size() > 0) {
            for (KnowledgeBuilderError error : errors) {
                System.err.println(error);
            }
//			throw new IllegalArgumentException("Could not parse rule package.");
            return false;
        }

        rulePackage.setId(kbuilder.getKnowledgePackages().iterator().next().getName());
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
//		ksession = kbase.newStatefulKnowledgeSession();  // now the session is created in constructor

        return true;
    }

    public synchronized void removePackage(String rulePackageName) {
        kbase.removeKnowledgePackage(rulePackageName);
    }

    public synchronized void setManager(Manager manager) {
        ksession.setGlobal("manager", manager);
    }

    public synchronized void insertObservation(Observation o) {
        ksession.insert(o);
        // CHECK WHERE TO ADD STORING IN HISTORY DB
    }

    // return the number of fired rules
    public synchronized int evaluate() {
        return ksession.fireAllRules();
    }
}
