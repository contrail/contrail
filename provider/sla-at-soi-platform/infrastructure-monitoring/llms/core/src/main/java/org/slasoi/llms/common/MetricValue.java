/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/MetricValue.java $
 */

package org.slasoi.llms.common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * @author Miha Vuk, XLAB
 *         <p/>
 *         Metric value is an aggregate (average) of one or more observations.
 *         Time of MetricValue is the time of first observation.
 */
public class MetricValue extends Observation {

    int count;        // count of observations included in aggregate
    Date timeLast;    // time of last observation included in this aggregate

    public MetricValue(Metric metric, Date time, String value, int count, Date timeLast) {
        super(metric, time, value);
        this.count = count;
        this.timeLast = timeLast;
    }

    public MetricValue(Metric metric, Date time, String value) {
        super(metric, time, value);
        this.count = 1;
        this.timeLast = time;
    }

    public MetricValue(Observation o) {
        super(o.metric, o.time, o.value);
        this.count = 1;
        this.timeLast = o.time;
    }

    public MetricValue(JSONObject o) throws JSONException, ClassNotFoundException {
        super(new Metric(o.getJSONObject("metric")), new Date(o.getLong("time")), o.getString("value"));
        this.count = o.getInt("count");
        this.timeLast = new Date(o.getLong("timeLast"));
    }

    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("metric", metric.toJSON());
            o.put("time", time.getTime());
            o.put("value", value);
            o.put("count", count);
            o.put("timeLast", timeLast.getTime());

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getTimeLast() {
        return timeLast;
    }

    public void setTimeLast(Date timeLast) {
        this.timeLast = timeLast;
    }

    //true if the periods intercept

    public boolean matchPeriod(Period period) {
        return !((time.after(period.getTo())) || (timeLast.before(period.getFrom())));
    }
}