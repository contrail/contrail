/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/Observation.java $
 */

package org.slasoi.llms.common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class Observation {

    Metric metric;
    Date time;        // time of occurrence
    String value;         // can be string, serialized simple type, JSON or XML structure

    public Observation(Metric metric, Date time, String value) {
        super();
        this.metric = metric;
        this.time = time;
        this.value = value;
    }

    public Observation(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.metric = new Metric(o.getJSONObject("metric"));
        this.time = new Date(o.getLong("time"));
        this.value = o.getString("value");
    }

    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("metric", metric.toJSON());
            o.put("time", time.getTime());
            o.put("value", value);

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // set all fiels
    public void set(Observation o) {
        this.metric = o.metric;
        this.time = o.time;
        this.value = o.value;
    }


    // custom type specific getValue* methods
    // NOTE: Use the call appropriate for the given metric.metricType.dataType

    public boolean getBoolValue() throws MetricValueException {
        if (!metric.metricType.dataType.equals(Boolean.class)) // MIHA CHECK?
            throw new MetricValueException("Value  " + value + " is of type " + metric.metricType.dataType.toString() + " instead of Boolean.");
        return Boolean.parseBoolean(value);
    }

    public int getIntValue() throws MetricValueException {
        if (!metric.metricType.dataType.equals(Integer.class))
            throw new MetricValueException("Value  " + value + " is of type " + metric.metricType.dataType.toString() + " instead of Integer.");
        return Integer.parseInt(value);
    }

    public double getDoubleValue() throws MetricValueException {
        if (!metric.metricType.dataType.equals(Double.class))
            throw new MetricValueException("Value  " + value + " is of type " + metric.metricType.dataType.toString() + " instead of Double.");
        return Double.parseDouble(value);
    }

    public String getStringValue() throws MetricValueException {
        if (!metric.metricType.dataType.equals(String.class)) // MIHA CHECK?
            throw new MetricValueException("Value  " + value + " is of type " + metric.metricType.dataType.toString() + " instead of String.");
        return value;
    }


    // HELPER METHODS
    public String toString() {
        return String.format("%s, %s, %s %s", metric.getMetricURL(), time, value, metric.getMetricType().getUnit());
    }
}