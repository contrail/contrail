/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/stubs/ServiceInstance2MonitoringStubRemote.java $
 */

package org.slasoi.llms.stubs;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.interfaces.IServiceInstance2Monitoring;
import org.slasoi.llms.manager.MetricRegistry;
import org.slasoi.llms.manager.MetricRegistry.RegistrationStatus;
import org.slasoi.llms.rpc.PubSubRPC;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

public class ServiceInstance2MonitoringStubRemote implements IServiceInstance2Monitoring {
    private static Logger log = Logger.getLogger(ServiceInstance2MonitoringStubRemote.class);

    public static String channelName = "SENSORS_TO_LLMS";

    PubSubRPC pubSubRPC;

    public ServiceInstance2MonitoringStubRemote(PubSubRPC pubSubRPC) throws MessagingException, FileNotFoundException, IOException {
        super();
        this.pubSubRPC = pubSubRPC;
        System.out.println("---------------------------- ServiceInstance2MonitoringMonitoringStubRemote initialized -------------------------------");
    }


    // Return true if the message has been successfully sent. Does not check the success of registration.
    public MetricRegistry.RegistrationStatus registerMetric(MetricEx metric) {
        try {
            JSONObject retObj = pubSubRPC.sendCommand(registerMetric2JSON(metric), true);
            assert (retObj.has("retval"));
            return RegistrationStatus.valueOf(retObj.getString("retval"));

        }
        catch (Exception e) { // JSONException or MessagingException
            log.error(e);
            return RegistrationStatus.ERROR;
        }
        //		return Manager.getInstance().getRegistry().registerMetric(metric);
    }

    public JSONObject registerMetric2JSON(MetricEx metric) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("command", "registerMetric");
        ret.put("metric", metric.toJSON());

        return ret;
    }

    public void unregisterMetric(String metricURL) {
        try {
            pubSubRPC.sendCommand(unregisterMetric2JSON(metricURL), false);
        }
        catch (Exception e) {
            log.error(e);
        }
        //		Manager.getInstance().getRegistry().unregisterMetric(metricURL);
    }

    public JSONObject unregisterMetric2JSON(String metricURL) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("command", "unregisterMetric");
        ret.put("metricURL", metricURL);

        return ret;
    }

    public boolean isRegisteredMetric(String metricURL) {
        try {
            JSONObject retObj = pubSubRPC.sendCommand(isRegisteredMetric2JSON(metricURL), true);
            assert (retObj.has("retval"));
            return retObj.getBoolean("retval");

        }
        catch (Exception e) {
            log.error(e);
            return false;
        }
        //		return Manager.getInstance().getRegistry().isRegisteredMetric(metricURL);
    }

    public JSONObject isRegisteredMetric2JSON(String metricURL) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("command", "isRegisteredMetric");
        ret.put("metricURL", metricURL);

        return ret;
    }

    public void storeMetricObservation(String metricURL, Date time, String value) {
        try {
            pubSubRPC.sendCommand(sendObservation2JSON(metricURL, time, value), false);
        }
        catch (Exception e) {
            log.error(e);
        }
        //		Manager.getInstance().getStore().storeMetricObservation(metricURL, time, value);
    }

    public JSONObject sendObservation2JSON(String metricURL, Date time, String value) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("command", "sendObservation");
        ret.put("metricURL", metricURL);
        ret.put("time", time.getTime());
        ret.put("value", value);

        return ret;
    }

    public JSONArray sendCommandList(JSONArray commandList, boolean synchronous) throws MessagingException {
        return pubSubRPC.sendCommandList(commandList, synchronous);
    }

}
