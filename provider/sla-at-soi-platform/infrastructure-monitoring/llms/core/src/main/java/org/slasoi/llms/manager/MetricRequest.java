/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/MetricRequest.java $
 */

package org.slasoi.llms.manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.llms.common.MetricParam;
import org.slasoi.llms.common.Util;

import java.util.ArrayList;

public class MetricRequest {                          // see also Monitoring2Resources interface

    String metricURL;
    //	HistoryLevel				historyLevel;
    ArrayList<MetricParam> params;             // required parameters (also describe the history, aggregation, caching, etc. requirements)
    String updateXmppChannel;     // optional, can be null
    String newValueXmppChannel;// optional, can be null

    public MetricRequest(String metricURL, ArrayList<MetricParam> params,
                         String updateXmppChannel, String newValueXmppChannel) {
        super();
        this.metricURL = metricURL;
        this.params = params;
        this.updateXmppChannel = updateXmppChannel;
        this.newValueXmppChannel = newValueXmppChannel;
    }

    public MetricRequest(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.metricURL = o.getString("metricURL");

        if (o.has("params")) {
            this.params = new ArrayList<MetricParam>();
            JSONArray arr = o.getJSONArray("params");
            for (int i = 0; i < arr.length(); i++)
                params.add(new MetricParam(arr.getJSONObject(i)));
        }
        else
            this.params = null;

        this.updateXmppChannel = Util.getStringOrNull(o, "updateXmppChannel");
        this.newValueXmppChannel = Util.getStringOrNull(o, "newValueXmppChannel");
    }

    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("metricURL", metricURL);

            if (params != null) {
                JSONArray arr = new JSONArray();
                for (MetricParam p : params)
                    arr.put(p.toJSON());
                o.put("params", arr);
            }

            o.put("updateXmppChannel", updateXmppChannel);
            o.put("newValueXmppChannel", newValueXmppChannel);

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public String getMetricURL() {
        return metricURL;
    }

    public void setMetricURL(String metricURL) {
        this.metricURL = metricURL;
    }

    public ArrayList<MetricParam> getParams() {
        return params;
    }

    public void setParams(ArrayList<MetricParam> params) {
        this.params = params;
    }

    public String getUpdateXmppChannel() {
        return updateXmppChannel;
    }

    public void setUpdateXmppChannel(String updateXmppChannel) {
        this.updateXmppChannel = updateXmppChannel;
    }

    public String getNewValueXmppChannel() {
        return newValueXmppChannel;
    }

    public void setNewValueXmppChannel(String newValueXmppChannel) {
        this.newValueXmppChannel = newValueXmppChannel;
    }
}