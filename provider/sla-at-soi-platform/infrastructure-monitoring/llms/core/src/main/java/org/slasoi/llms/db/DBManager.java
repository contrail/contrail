/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/db/DBManager.java $
 */

package org.slasoi.llms.db;

import org.apache.log4j.Logger;
import org.slasoi.llms.common.*;
import org.slasoi.llms.common.MetricType.MetricClass;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class DBManager {
    private static final Logger log = Logger.getLogger(DBManager.class);

    //	String connStr = "jdbc:mysql://localhost/miha1?user=root&password=admin"; // TODO move to settings

    String connStr;

    Connection conn = null;
    Hashtable<String, Integer> metricIDs = new Hashtable<String, Integer>();
    Hashtable<Integer, Metric> metricByID = new Hashtable<Integer, Metric>();

    public DBManager(String connStr) throws Exception {
        this.connStr = connStr;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(connStr);
            log.info("Connection to DB established. connStr=" + connStr);
        }
        catch (Exception e) {
            log.error("Error while connectiong to SQL server. connStr=" + connStr, e);
            throw (e);
        }
    }

    protected void finalize() {
        try {
            if (conn != null)
                conn.close();
        }
        catch (SQLException e) {
            log.error("Error while closing SQL connection.", e);
        }
    }

    public String getConnStr() {
        return connStr;
    }

    // LOAD/SAVE METRIC
    public ArrayList<MetricEx> loadMetrics() {
        try {
            ArrayList<MetricEx> ret = new ArrayList<MetricEx>();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Metric");
            while (rs.next()) {
                log.trace((String.format("load Metric: %d,%s,%s,%s,%s",
                        rs.getInt("ID"), rs.getString("metricURL"),
                        rs.getString("name"), rs.getString("unit"),
                        rs.getString("metricClass"), rs.getString("dataType"))));

                MetricType metricType = new MetricType(rs.getString("name"), rs.getString("unit"),
                        MetricClass.valueOf(rs.getString("metricClass")), Class.forName(rs.getString("dataType")));
                MetricEx metric = new MetricEx(rs.getString("metricURL"), metricType);

                ret.add(metric);
                metricIDs.put(metric.getMetricURL(), rs.getInt("ID"));
                metricByID.put(rs.getInt("ID"), metric);
            }
            return ret;
        }
        catch (SQLException e) {
            log.error("SQL error", e);
        }
        catch (ClassNotFoundException e) {
            log.error("Error while parsing metricClass.", e);
        }
        // TODO close the statement to free the resources immediately (rs is closed implicitly).
        return null;
    }

    @SuppressWarnings("unchecked")
    public void saveMetric(Metric metric) {
        Statement stmt;
        try {
            stmt = conn.createStatement();
            int ret = stmt.executeUpdate(String.format(
                    "INSERT INTO Metric (metricURL, name, unit, metricClass, dataType) VALUES ('%s', '%s', '%s', '%s', '%s')",
                    metric.getMetricURL(), metric.getMetricType().getName(), metric.getMetricType().getUnit(),
                    metric.getMetricType().getMetricClass(), ((Class) metric.getMetricType().getDataType()).getName()),
                    Statement.RETURN_GENERATED_KEYS);
            if (ret != 1)
                log.error(String.format("Saving metric %s to DB failed.", metric.getMetricURL()));
            else {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                int metricID = rs.getInt(1);

                metricIDs.put(metric.getMetricURL(), metricID);
                metricByID.put(metricID, metric);
                log.trace(String.format("Metric %s ID=%d saved to DB.", metric.getMetricURL(), metricID));
            }
        }
        catch (SQLException e) {
            log.error(String.format("Saving metric %s to DB failed.", metric.getMetricURL()), e);
        }
    }


    // LOAD/SAVE METRIC OBSERVATIONS/VALUES

    public ArrayList<Observation> loadObservations() {
        try {
            ArrayList<Observation> ret = new ArrayList<Observation>();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM MetricValue");
            while (rs.next()) {
                int metricID = rs.getInt("metricID");
                Metric metric = metricByID.get(metricID);
                Date time = rs.getTimestamp("time");
                String value = rs.getString("time");
                int count = rs.getInt("count");
                Date timeLast = rs.getTimestamp("timeLast");

                log.trace((String.format("load MetricValue: %d,%d,%s,%d,%s",
                        rs.getInt("ID"), metricID, time, count, timeLast)));

                Observation o;
                if (count > 0)
                    o = new MetricValue(metric, time, value, count, timeLast);
                else
                    o = new Observation(metric, timeLast, value);

                ret.add(o);
            }
            return ret;
        }
        catch (SQLException e) {
            log.error("SQL error", e);
        }

        // TODO close the statement to free the resources immediately (rs is closed implicitly).
        return null;
    }

    public void saveObservation(Observation o) {
        Statement stmt;
        try {
            int metricID = metricIDs.get(o.getMetric().getMetricURL());
            Timestamp time = new Timestamp(o.getTime().getTime());
            String value = o.getValue();
            int count = 1;
            Timestamp timeLast = time;

            if (o instanceof MetricValue) {
                count = ((MetricValue) o).getCount();
                timeLast = new Timestamp(((MetricValue) o).getTimeLast().getTime());
            }

            stmt = conn.createStatement();
            int ret = stmt.executeUpdate(String.format(
                    "INSERT INTO MetricValue (metricID, time, value, count, timeLast) VALUES ('%s', '%s', '%s', '%s', '%s')",
                    metricID, time, value, count, timeLast));
            if (ret != 1)
                log.error(String.format("Saving MetricValue %s %s to DB failed.", o.getMetric().getMetricURL(), o.getTime()));
            else
                log.trace(String.format("MetricValue %s time=%s saved to DB.", o.getMetric().getMetricURL(), o.getTime()));

        }
        catch (SQLException e) {
            log.error(String.format("Saving MetricValue %s %s to DB failed.", o.getMetric().getMetricURL(), o.getTime()), e);
        }
    }

    public void cleanDB() {
        try {
            Statement stmt = conn.createStatement();
            int ret = stmt.executeUpdate("DELETE FROM Metric");
            log.debug(String.format("Deleted %d rows from Metric table", ret));

            ret = stmt.executeUpdate("DELETE FROM MetricValue");
            log.debug(String.format("Deleted %d rows from MetricValue table", ret));

        }
        catch (SQLException e) {
            log.error("SQL error connection.", e);
        }
    }

}
