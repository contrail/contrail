/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/MonitoringRequest.java $
 */

package org.slasoi.llms.manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.llms.common.MetricParam;
import org.slasoi.llms.common.RulePackage;
import org.slasoi.llms.common.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class MonitoringRequest {

    ArrayList<MetricRequest> metrics;
    ArrayList<RulePackage> rulePackages;
    String notificationChannel;      // generic notification channel, optional

    public MonitoringRequest(ArrayList<MetricRequest> metrics,
                             ArrayList<RulePackage> rulePackages,
                             String notificationChannel) {
        super();
        this.metrics = metrics;
        this.rulePackages = rulePackages;
        this.notificationChannel = notificationChannel;
    }

    public MonitoringRequest(MetricRequest[] metrics,
                             RulePackage[] rulePackages,
                             String notificationChannel) {
        super();
        this.metrics = new ArrayList<MetricRequest>();
        if (metrics != null)
            for (MetricRequest m : metrics)
                this.metrics.add(m);

        this.rulePackages = new ArrayList<RulePackage>();
        if (rulePackages != null)
            for (RulePackage r : rulePackages)
                this.rulePackages.add(r);

        this.notificationChannel = notificationChannel;
    }

    public MonitoringRequest(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.metrics = new ArrayList<MetricRequest>();
        JSONArray arr = o.getJSONArray("metrics");
        for (int i = 0; i < arr.length(); i++)
            metrics.add(new MetricRequest(arr.getJSONObject(i)));

        this.rulePackages = new ArrayList<RulePackage>();
        JSONArray arr2 = o.getJSONArray("rulePackages");
        for (int i = 0; i < arr2.length(); i++)
            rulePackages.add(new RulePackage(arr2.getJSONObject(i)));

        notificationChannel = Util.getStringOrNull(o, "notificationChannel");
    }

    public JSONObject toJSON() throws JSONException, IOException {
        JSONObject o = new JSONObject();

        JSONArray arr = new JSONArray();
        for (MetricRequest m : metrics)
            arr.put(m.toJSON());
        o.put("metrics", arr);

        JSONArray arr2 = new JSONArray();
        for (RulePackage r : rulePackages)
            arr2.put(r.toJSON());
        o.put("rulePackages", arr2);

        o.put("notificationChannel", notificationChannel);

        return o;
    }


    public ArrayList<MetricRequest> getMetrics() {
        return metrics;
    }

    public void setMetrics(ArrayList<MetricRequest> metrics) {
        this.metrics = metrics;
    }

    public ArrayList<RulePackage> getRulePackages() {
        return rulePackages;
    }

    public void setRulePackages(ArrayList<RulePackage> rulePackages) {
        this.rulePackages = rulePackages;
    }

    public String getNotificationChannel() {
        return notificationChannel;
    }

    public void setNotificationChannel(String notificationChannel) {
        this.notificationChannel = notificationChannel;
    }

    // create MonitoringRequest from XML (see MonitoringRequest.xml example)
    public static MonitoringRequest createFromXML(String requestXML) {
        try {
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xPath = xPathFactory.newXPath();
            Document xmlDocument = DocumentBuilderFactory.newInstance().
                    newDocumentBuilder().parse(new InputSource(new StringReader(requestXML)));
            xPath = XPathFactory.newInstance().newXPath();

            XPathExpression xPathExpression = xPath.compile("/MonitoringRequest");
            Node rootX = (Node) xPathExpression.evaluate(xmlDocument, XPathConstants.NODE);

            xPathExpression = xPath.compile("Metrics/MetricRequest");
            NodeList metricRequestsX = (NodeList) xPathExpression.evaluate(rootX, XPathConstants.NODESET);
            ArrayList<MetricRequest> metrics = new ArrayList<MetricRequest>();
            for (int i = 0; i < metricRequestsX.getLength(); i++) {
                Node metricRequestX = metricRequestsX.item(i);

                xPathExpression = xPath.compile("MetricURL");
                Node metricURLX = (Node) xPathExpression.evaluate(metricRequestX, XPathConstants.NODE);
                String metricURL = metricURLX.getTextContent();

                xPathExpression = xPath.compile("MetricParam");
                NodeList paramsX = (NodeList) xPathExpression.evaluate(metricRequestX, XPathConstants.NODESET);
                ArrayList<MetricParam> params = new ArrayList<MetricParam>();
                for (int j = 0; j < paramsX.getLength(); j++) {
                    Node paramX = paramsX.item(j);

                    String name = paramX.getAttributes().getNamedItem("name").getNodeValue();
                    String value = paramX.getTextContent();
                    params.add(new MetricParam(name, value));
                }

                metrics.add(new MetricRequest(metricURL, params, null, null));
            }

            xPathExpression = xPath.compile("RulePackages/RulePackage");
            NodeList rulePackagesX = (NodeList) xPathExpression.evaluate(rootX, XPathConstants.NODESET);
            ArrayList<RulePackage> rulePackages = new ArrayList<RulePackage>();
            for (int i = 0; i < rulePackagesX.getLength(); i++) {
                Node rulePackageX = rulePackagesX.item(i);

                //				String type = rulePackageX.getAttributes().getNamedItem("type").getNodeValue();
                String source = rulePackageX.getTextContent();
                rulePackages.add(RulePackage.createRulePackageFromString(source));     // add type support
            }

            xPathExpression = xPath.compile("NotificationChannel");
            Node notificationChannelX = (Node) xPathExpression.evaluate(rootX, XPathConstants.NODE);
            String notificationChannel = notificationChannelX.getTextContent();

            return new MonitoringRequest(metrics, rulePackages, notificationChannel);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}