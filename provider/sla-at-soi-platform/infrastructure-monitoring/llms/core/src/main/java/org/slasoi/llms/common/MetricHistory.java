/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/MetricHistory.java $
 */

package org.slasoi.llms.common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MetricHistory {

    Metric metric;
    ArrayList<MetricValue> values;

    public MetricHistory(Metric metric, ArrayList<MetricValue> values) {
        super();
        this.metric = metric;
        this.values = values;
    }

    public MetricHistory(Metric metric) {
        super();
        this.metric = metric;
        this.values = new ArrayList<MetricValue>();
    }

    public MetricHistory(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.metric = new Metric(o.getJSONObject("metric"));

        this.values = new ArrayList<MetricValue>();
        JSONArray arr = o.getJSONArray("values");
        for (int i = 0; i < arr.length(); i++)
            values.add(new MetricValue(arr.getJSONObject(i)));
    }

    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("metric", metric.toJSON());

            JSONArray arr = new JSONArray();
            for (MetricValue v : values)
                arr.put(v.toJSON());
            o.put("values", arr);

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public ArrayList<MetricValue> getValues() {
        return values;
    }

    public void setValues(ArrayList<MetricValue> values) {
        this.values = values;
    }
}