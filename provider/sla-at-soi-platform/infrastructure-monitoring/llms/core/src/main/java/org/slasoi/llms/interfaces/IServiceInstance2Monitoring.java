/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/interfaces/IServiceInstance2Monitoring.java $
 */

package org.slasoi.llms.interfaces;

import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.manager.MetricRegistry;

import java.util.Date;

/**
 * The Resource (represented by Managed Objects) to Monitoring System Interface.
 * <p/>
 * Provides an API that resource instances use to register its metrics (and events) and to sent the measured/detected data.
 * The interface is implemented by Monitoring System.
 *
 * @author Miha Vuk, XLAB
 */
public interface IServiceInstance2Monitoring {

    /**
     * Register Metric (or Event) (on already registered Resource Instance) to the Monitoring System.
     *
     * @param resourceInstance The Resource Instance that the Metric relates to.
     * @param metric           The (real) instance of virtual of physical resource. It includes the relevant metric  specific information.
     * @return URI of registered Metric or null in case of error.
     */
    public MetricRegistry.RegistrationStatus registerMetric(MetricEx metric);

    public void unregisterMetric(String metricURL);

    public boolean isRegisteredMetric(String metricURL);

    /**
     * Send the current/last value of specific Metric (or Event) on specific Resource Instance to the Monitoring System.
     * Also called the push method.
     *
     * @param metric The specific metric.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    public void storeMetricObservation(String metricURL, Date time, String value);
 
}