/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/stubs/MonitoringDataAndHistoryStubRemote.java $
 */

package org.slasoi.llms.stubs;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricHistory;
import org.slasoi.llms.common.MetricValue;
import org.slasoi.llms.common.Period;
import org.slasoi.llms.interfaces.IMonitoringDataAndHistory;
import org.slasoi.llms.rpc.PubSubRPC;

import javax.sql.RowSet;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MonitoringDataAndHistoryStubRemote implements IMonitoringDataAndHistory {
    private static Logger log = Logger.getLogger(MonitoringDataAndHistoryStubRemote.class);

    public static String channelName = "USERCMP_TO_LLMS";

    PubSubRPC pubSubRPC;

    public MonitoringDataAndHistoryStubRemote(PubSubRPC pubSubRPC) throws MessagingException, FileNotFoundException, IOException {
        super();
        this.pubSubRPC = pubSubRPC;
        System.out.println("---------------------------- MonitoringDataAndHistoryStubRemote initialized -------------------------------");
    }


    /**
     * Get metric details for the given metric URL..
     *
     * @param metricURL URL of requested metric
     * @return Metric details or null if not found.
     */
    public Metric getMetric(String metricURL) {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "getMetric"); // obligatory for each command
            args.put("metricURL", metricURL);

            JSONObject retObj = pubSubRPC.sendCommand(args, true);
            if (retObj.has("retval"))
                return new Metric(retObj.getJSONObject("retval"));
            else
                return null;

        }
        catch (Exception e) {
            log.error(e);
            return null;
        }
        //		return Manager.getInstance().getRegistry().getMetric(metricURL);
    }

    /**
     * Get list of metricsURL that match the given pattern.
     *
     * @param pattern URL pattern
     * @return List of matching metrics.
     */
    public ArrayList<Metric> getMatchingURLs(String pattern) {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "getMatchingURLs"); // obligatory for each command
            args.put("pattern", pattern);

            JSONObject retObj = pubSubRPC.sendCommand(args, true);

            assert (retObj.has("retval"));
            JSONArray retArr = retObj.getJSONArray("retval");
            ArrayList<Metric> ret = new ArrayList<Metric>();
            for (int i = 0; i < retArr.length(); i++)
                ret.add(new Metric(retArr.getJSONObject(i)));

            return ret;

        }
        catch (Exception e) {
            log.error(e);
            return null;
        }
        //		return Manager.getInstance().getRegistry().getMatchingURLs(pattern);
    }

    /**
     * Get the current/last value of specific Metric (or Event) on specific Resource Instance.
     *
     * @param metric The specific metric.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    public MetricValue getMetricValue(String metricURL) {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "getMetricValue"); // obligatory for each command
            args.put("metricURL", metricURL);

            JSONObject retObj = pubSubRPC.sendCommand(args, true);
            if (retObj.has("retval"))
                return new MetricValue(retObj.getJSONObject("retval"));
            else
                return null;

        }
        catch (Exception e) {
            log.error(e);
            return null;
        }
        //		return Manager.getInstance().getStore().getMetricValue(metricURL);
    }

    /**
     * Get the list of history values of specific Metric (or Event) for the given period.
     *
     * @param metric The specific metric.
     * @param period From - To datetimes or null for all.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    public MetricHistory getMetricValueHistory(String metricURL, Period period) {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "getMetricValueHistory"); // obligatory for each command
            args.put("metricURL", metricURL);
            args.put("period", period.toJSON());

            JSONObject retObj = pubSubRPC.sendCommand(args, true);
            if (retObj.has("retval"))
                return new MetricHistory(retObj.getJSONObject("retval"));
            else
                return null;

        }
        catch (Exception e) {
            log.error(e);
            return null;
        }
        //		return Manager.getInstance().getStore().getMetricValueHistory(metricURL, period);
    }

    /**
     * Query the Monitoring HistoryInfoStore SQL database.
     *
     * @param query SQL query
     * @return Result set.
     */
    public ArrayList<RowSet> queryHistory(String query) { // result set TODO MIHA
        return null; //TODO implement
        //		return Manager.getInstance().getStore().queryHistory(query);
    }

    public String test() {
        return "-------------------- testing ---------------------------------------------";
	}
}
