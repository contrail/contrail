/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/rpc/PubSubRPC.java $
 */

package org.slasoi.llms.rpc;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.llms.common.Util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;

public class PubSubRPC {
    private static Logger log = Logger.getLogger(PubSubRPC.class);

    public static final int MAX_RESPONSE_TIME = 5000; // in ms

    class Requester {
        Thread thread;
        JSONArray returnList = new JSONArray();
    }

    PubSubManager pubSubManager;
    String channelName;
    boolean server;        // true for server, false for client

    Hashtable<String, Requester> waitList = new Hashtable<String, Requester>();
    Hashtable<String, CommandHandler> commandHandlers = new Hashtable<String, CommandHandler>();


    public PubSubRPC(PubSubManager pubSubManager, String channelName, boolean server) throws MessagingException, FileNotFoundException, IOException {
        this.pubSubManager = pubSubManager;
        this.channelName = channelName;
        this.server = server;

        // Add message listener
        pubSubManager.addMessageListener(new MessageListener(), new String[]{channelName});

        // Create the channel if not existing and subscribe to it
        pubSubManager.createAndSubscribe(new Channel(channelName));
        log.debug("PubSubRPC: Channel " + channelName + " created and subscribed as " + (server ? "server" : "client") + ".");
    }

    protected void finalize() throws Throwable {
        try {
            pubSubManager.unsubscribe(channelName);
            pubSubManager.close();
        }
        catch (Exception e) {
            log.error("Error when closing the connection " + channelName);
        }
        super.finalize();
    }


    // WAITING REQUESTER LIST METHODS

    public synchronized String addRequest() {
        Requester req = new Requester();
        req.thread = Thread.currentThread();

        String request_id = Util.getSafeUUID();
        waitList.put(request_id, req);

        log.trace(String.format("Request %s for sent by thread %s.", request_id, req.thread.toString()));
        return request_id;
    }

    public synchronized void wakeRequester(String request_id, JSONArray returnList) {
        Requester req = waitList.get(request_id);
        req.returnList = returnList;
        if (req == null)
            log.error("Return on nonexisting request. request_id=" + request_id);
        else
            req.thread.interrupt();
    }

    // get response and dispose entry in waitList
    public synchronized JSONArray grabReturnList(String request_id) {
        Requester req = waitList.get(request_id);
        if (req == null)
            return null;

        JSONArray ret = req.returnList;
        waitList.remove(request_id);
        return ret;
    }


    // COMMAND HANDLERS LIST METHODS
    public synchronized void addCommandHandler(String command, CommandHandler handler) {
        commandHandlers.put(command, handler);
    }


    // MESSAGE AND RESPONSE SENDING FUNCTIONS

    public void sendReturnList(String request_id, JSONArray return_list) {
        String payload = null;
        try {
            JSONObject o = new JSONObject();
            o.put("request_id", request_id);
            o.put("return_list", return_list);

            payload = o.toString();
            PubSubMessage message = new PubSubMessage(channelName, payload);

            pubSubManager.publish(message);
            log.debug("Message sent: " + payload);
        }
        catch (JSONException e) {
            log.error(e);
        }
        catch (MessagingException e) {
            log.error(channelName + " Error when publishing msg: " + payload);
        }
    }

    public JSONObject sendCommand(JSONObject command, boolean synchronous) throws JSONException, MessagingException {
        // Create new message and publish it.
        JSONArray array = new JSONArray();
        array.put(command);

        JSONArray returnList = sendCommandList(array, synchronous);
        if (returnList != null) {
            assert (returnList.length() == 1);
            return returnList.getJSONObject(0);
        }
        else
            return null;
    }

    public JSONArray sendCommandList(JSONArray commandList, boolean synchronous) throws MessagingException {
        String payload = null;
        String request_id = null;
        try {
            // Create new message and publish it.
            JSONObject o = new JSONObject();
            if (synchronous) {
                request_id = addRequest();
                o.put("request_id", request_id);
            }

            o.put("command_list", commandList);
            payload = o.toString();
            PubSubMessage message = new PubSubMessage(channelName, payload);

            pubSubManager.publish(message);
            log.debug("Message sent: " + payload);

            if (!synchronous)
                return null;

            // else wait for response
            Thread.sleep(PubSubRPC.MAX_RESPONSE_TIME);

            log.debug("PubSubRPC synchronous XMPP call timeout. Check that ManagerProxy/LLMSServer/InfrastructureMonitoringAgentProxy/etc. is instantiated.");
            throw new MessagingException("PubSubRPC synchronous XMPP call timeout.");

        }
        catch (InterruptedException e) {
            // wake on response - the only proper execution path
            JSONArray returnList = grabReturnList(request_id);
            return returnList;

        }
        catch (JSONException e) {
            log.error(e);
        }
        catch (MessagingException e) {
            log.error(channelName + " Error when publishing msg: " + payload);
            throw e;
        }
        return null;
    }


    private class MessageListener implements org.slasoi.common.messaging.pubsub.MessageListener {

        public void processMessage(MessageEvent messageEvent) {
            String payload = messageEvent.getMessage().getPayload();
            log.trace("Message received: " + payload);

            JSONArray return_list = new JSONArray();     // to be prepared for response to command_list
            String request_id = null;
            try {
                JSONObject m = new JSONObject(messageEvent.getMessage().getPayload());

                if (m.has("request_id")) {
                    request_id = m.getString("request_id");

                    if (m.has("return_list")) {
                        if (!waitList.containsKey(request_id))
                            return;     // it is a return to an known request

                        log.debug("Return received: " + payload);
                        return_list = m.getJSONArray("return_list");
                        wakeRequester(request_id, return_list);
                        return;
                    }
                }

                if (!server)    // ignore command messages on clients
                    return;

                log.debug("Command received: " + payload);

                if (m.has("command_list")) {
                    JSONArray command_list = m.getJSONArray("command_list");
                    for (int i = 0; i < command_list.length(); i++) {

                        JSONObject o = command_list.getJSONObject(i);
                        String command = o.getString("command");

                        CommandHandler handler = commandHandlers.get(command);
                        if (handler == null) {
                            log.error("Unsupported command (" + command + ") in received message on " + channelName);
                            return;        // ignore the rest of the message
                        }

                        log.trace("Invoking handler for command (" + command + ") in received message on " + channelName);
                        Object retObj = handler.processCommand(command, o);
                        log.trace("Return from handler for command (" + command + ")");

                        if (request_id != null) {
                            JSONObject o2 = new JSONObject();
                            o2.put("retval", retObj);
                            return_list.put(o2);
                        }
                    }

                    if (request_id != null)
                        sendReturnList(request_id, return_list);
                }
                else
                    log.error("Missing command_list or return_list");

            }
            catch (Exception e) {
                log.error("Parsing error or exception in command handler in received message on channel " + channelName + ":" +
                        messageEvent.getMessage().getPayload(), e);
            }
        }
    }

}
