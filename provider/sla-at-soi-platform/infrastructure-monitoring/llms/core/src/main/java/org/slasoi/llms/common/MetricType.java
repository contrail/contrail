/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/MetricType.java $
 */

package org.slasoi.llms.common;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public /*abstract*/ class MetricType {

    public static enum MetricClass {
        FLUENT,
        EVENT,
        ALARM
    }

    String name;                //	just for comprehensiveness (e.g. CPU)
    String unit;                //	e.g. KB, s, requests, etc.
    MetricClass metricClass;        //  fluent, event or alarm
    Type dataType;

    /* TO ADD LATER  TODO
     String			defaultValue;
     HistorySettings historySettings;
     int 			minRefreshRate = 0; // 	in ms, 0 if not required
                                         //	if no new value arrives in expected time, the getMetricValue() is called on the registered Sensor (probe)
      */

    public MetricType(String name, String unit, MetricClass metricClass,
                      Type dataType) {
        super();
        this.name = name;
        this.unit = unit;
        this.metricClass = metricClass;
        this.dataType = dataType;
    }

    public MetricType(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.name = o.getString("name");
        this.unit = o.getString("unit");
        this.metricClass = MetricClass.valueOf(o.getString("metricClass"));
        this.dataType = Class.forName(o.getString("dataType"));
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("name", name);
            o.put("unit", unit);
            o.put("metricClass", metricClass);
            o.put("dataType", ((Class) dataType).getName());

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public MetricClass getMetricClass() {
        return metricClass;
    }

    public Type getDataType() {
        return dataType;
    }


    //	public int getMinRefreshRate() {
    //		return minRefreshRate;
    //	}

    //	public abstract String encode(Object value);
    //
    //	public abstract Object decode(String str);
}