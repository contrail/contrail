/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/MetricRegistry.java $
 */

package org.slasoi.llms.manager;

import org.apache.log4j.Logger;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.common.Util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map.Entry;

public class MetricRegistry {
    private static final Logger log = Logger.getLogger(MonitoringStore.class);

    public static enum RegistrationStatus {
        SUCCESS,
        ALREADY_REGISTERED,
        ERROR
    }

    // table of registered metrics
    Hashtable<String, MetricEx> metricTable = new Hashtable<String, MetricEx>();


    //true at successful registration and false if the same metricURL was registered before
    public synchronized RegistrationStatus registerMetric(MetricEx metric) {
        assert (metric != null);
        if (!metricTable.containsKey(metric.getMetricURL())) {
            metricTable.put(metric.getMetricURL(), metric);
            log.debug("Registered metric " + metric.getMetricURL());
            return RegistrationStatus.SUCCESS;
        }
        else {
            log.debug("Trying to register already registed metric " + metric.getMetricURL());
            return RegistrationStatus.ALREADY_REGISTERED; //TODO check if new metricType equals old one
        }
    }

    public synchronized void unregisterMetric(String metricURL) {
        assert (metricURL != null);
        Metric m = metricTable.remove(metricURL);
        assert (m != null);
    }

    public synchronized boolean containsMetric(String metricURL) {
        assert (metricURL != null);
        return metricTable.containsKey(metricURL);
    }

    // returns Metric or null if not found
    public synchronized Metric getMetric(String metricURL) {
        assert (metricURL != null);
        return metricTable.get(metricURL);
    }

    // returns MetricEx or null if not found
    public synchronized MetricEx getMetricEx(String metricURL) {
        assert (metricURL != null);
        return metricTable.get(metricURL);
    }

    // returns list of Metrics matching the URL pattern
    public synchronized ArrayList<Metric> getMatchingURLs(String pattern) {
        assert (pattern != null);
        ArrayList<Metric> ret = new ArrayList<Metric>();

        Enumeration<MetricEx> e = metricTable.elements();
        while (e.hasMoreElements()) {
            Metric m = e.nextElement();
            if (Util.wildcardMatch(m.getMetricURL(), pattern))
                ret.add(m);
        }

        return ret;
    }


    // HELPER METHODS
    public void print() {
        for (Entry<String, MetricEx> e : metricTable.entrySet())
            System.out.println(e.getKey() + " " + e.getValue().toString());
    }
}