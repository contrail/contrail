/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/MonitoringStore.java $
 */

package org.slasoi.llms.manager;

import org.apache.log4j.Logger;
import org.slasoi.llms.common.*;
import org.slasoi.llms.interfaces.MetricNotificationListener.MetricNotificationType;

import javax.sql.RowSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class MonitoringStore {
    private static final Logger log = Logger.getLogger(MonitoringStore.class);

    Manager manager;
    MetricRegistry registry;

    Hashtable<String, Observation> lastValues = new Hashtable<String, Observation>();
    Hashtable<String, MetricHistory> historyValues = new Hashtable<String, MetricHistory>();

    public MonitoringStore(Manager manager, MetricRegistry registry) {
        super();
        this.manager = manager;
        this.registry = registry;
    }


    /**
     * TODO
     * Send the current/last value of specific Metric (or Event) on specific Resource Instance to the Monitoring System.
     * Also called the push method.
     *
     * @param metric The specific metric.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    public synchronized Observation storeMetricObservation(String metricURL, Date time, String value) {
        assert (metricURL != null);
        assert (value != null);
        MetricEx metric = registry.getMetricEx(metricURL);
        if (metric == null) {
            log.error(String.format("storeMetricObservation(%s) trying to store unregistered metric.", metricURL));
            throw new RuntimeException("Storing unregistered metric " + metricURL);
        }
        Observation oNew = new Observation(metric, time, value);
        return storeMetricObservation(oNew);
    }

    public synchronized Observation storeMetricObservation(Observation o) {
        assert (o != null);
        MetricEx metric = (MetricEx) o.getMetric();
        String metricURL = metric.getMetricURL();
        Observation oNew = o;
        // store last value (replace the old one)
        Observation oPrevious = lastValues.put(metricURL, oNew);

        // if this observation is already stored, ignore it
        if ((oPrevious != null) && (oPrevious.getTime().equals(oNew.getTime()))) {
            log.trace("Trying to store already stored observation: " + oNew);
            return null;
        }

        // store the value to the history
        MetricHistory m = historyValues.get(metricURL);
        if (m == null) {
            m = new MetricHistory(metric);
            historyValues.put(metricURL, m);
        }
        m.getValues().add(new MetricValue(oNew));

        ArrayList<MetricRequest> subscribers = manager.getSubscribers(metricURL);
        if (subscribers != null) {
            // send new value notification
            if ((oPrevious == null) || (!oPrevious.getValue().equals(oNew.getValue())))
                for (MetricRequest mr : subscribers)
                    if (mr.getNewValueXmppChannel() != null)
                        manager.sendMetricNotification(mr.getNewValueXmppChannel(), metricURL, MetricNotificationType.NEW_VALUE, oNew);

            // send update value (new observation) notification
            for (MetricRequest mr : subscribers)
                if (mr.getUpdateXmppChannel() != null)
                    manager.sendMetricNotification(mr.getUpdateXmppChannel(), metricURL, MetricNotificationType.UPDATE, oNew);
        }

        //		// evaluate the rules		// disabled in Y2
        //		manager.reasoningEngine.insertObservation(oNew); 	// insert observation into "Drools" fact store
        //		manager.reasoningEngine.evaluate(); 				// immediately evaluate new fact //TODO implement batch operations

        log.trace("Stored observation: " + oNew);
        return oNew;
    }


    /**
     * Get the current/last value of specific Metric (or Event) on specific Resource Instance.
     *
     * @param metric The specific metric.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    public synchronized MetricValue getMetricValue(String metricURL) {
        assert (metricURL != null);
        Observation last = lastValues.get(metricURL);
        log.trace(String.format("getMetricValue(%s) returned %s", metricURL, last));
        if (last != null)
            return new MetricValue(last);
        else
            return null;
    }

    /**
     * Get the list of history values of specific Metric (or Event) for the given period.
     *
     * @param metric The specific metric.
     * @param period From - To datetimes or null for all.
     * @return Value of the metric or null in case of error.
     *         The value can be simple string, JSON or XML (according to definition of metric (see setMetricParams)).
     */
    @SuppressWarnings("unchecked")
    public synchronized MetricHistory getMetricValueHistory(String metricURL, Period period) {
        assert (metricURL != null);
        assert (period != null);

        MetricHistory metricHistory = historyValues.get(metricURL);
        if (metricHistory == null) {
            log.trace(String.format("getMetricValueHistory(%s) returned null", metricURL));
            return null;
        }

        ArrayList<MetricValue> values;

        if (period != null) {
            values = new ArrayList<MetricValue>();
            for (MetricValue mv : metricHistory.getValues())
                if (mv.matchPeriod(period))
                    values.add(mv);
        }
        else
            values = (ArrayList<MetricValue>) metricHistory.getValues().clone();

        log.trace(String.format("getMetricValueHistory(%s) returned  %d values.", metricURL, values.size()));
        return new MetricHistory(metricHistory.getMetric(), values);
    }

    /**
     * Query the Monitoring HistoryInfoStore SQL database.
     *
     * @param query SQL query
     * @return Result set.
     */
    public synchronized ArrayList<RowSet> queryHistory(String query) { // result set TODO MIHA
		return null; //TODO implement
	}


}