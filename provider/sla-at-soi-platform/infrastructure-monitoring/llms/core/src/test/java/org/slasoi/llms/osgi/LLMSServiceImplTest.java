/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/osgi/LLMSServiceImplTest.java $
 */

package org.slasoi.llms.osgi;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.llms.common.*;
import org.slasoi.llms.manager.MetricRequest;
import org.slasoi.llms.manager.MonitoringRequest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Semaphore;

public class LLMSServiceImplTest extends TestCase {
    private static final Logger log = Logger.getLogger(LLMSServiceImplTest.class);
    private ApplicationContext context;
    boolean error = false;
    final Semaphore semSensorFinished = new Semaphore(0);
    final Semaphore semUserCompFinished = new Semaphore(0);
    final Semaphore semMetricsRegistered = new Semaphore(0);
    final Semaphore semMonReqRegistered = new Semaphore(0);
    final Semaphore semDataSent = new Semaphore(0);
    final Semaphore semMetricsReadingFinished = new Semaphore(0);

    public void setUp() {
        context = new FileSystemXmlApplicationContext("src/main/resources/META-INF/spring/llms-context.xml");
    }

    @Test
    public void testApp() throws InterruptedException {
        log.info("\n********************* " + log.getName() + " started. ************************");
        context.getBean("llmsServerStarterService");                        //new LLMSServerStarterServiceImpl();
        Thread.sleep(100);

        // starts two "typical components", each in its own thread
        Thread sensor = new SensorThread();
        Thread userComp = new UserComponentThread();
        sensor.start();
        semMetricsRegistered.acquire(); // wait that the sensors register the metrics, before userComp invoke provisioning request (that uses these metrics)
        userComp.start();

        semSensorFinished.acquire();
        semUserCompFinished.acquire();
        assertFalse(error);

        log.info("********************* " + log.getName() + " ended. ************************");
    }

    /**
     * Sensor is a component that measures/observes/detects and send this data to Low Level Monitoring (core)
     */
    private class SensorThread extends Thread {

        public void run() {

            try {
                log.debug("SensorThread started.");
                LLMSServiceInstance2MonitoringServiceImpl stub = (LLMSServiceInstance2MonitoringServiceImpl) context.getBean("llmsServiceInstance2MonitoringService");

                // define new metric
                MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
                String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                        MetricType.MetricClass.FLUENT, cpuMetricType.getName());
                MetricEx cpu1 = new MetricEx(cpu1MetricURL, cpuMetricType);

                assertFalse(stub.isRegisteredMetric(cpu1MetricURL));
                // register metric
                stub.registerMetric(cpu1);

                assertTrue(stub.isRegisteredMetric(cpu1MetricURL));
                semMetricsRegistered.release();

                semMonReqRegistered.acquire();    // wait some time that UserComponent registers monReq

                // send observations
                Thread.sleep(100);

                stub.storeMetricObservation(cpu1MetricURL, new Date(), "75");
                Thread.sleep(200);

                stub.storeMetricObservation(cpu1MetricURL, new Date(), "80");
                Thread.sleep(10);
                stub.storeMetricObservation(cpu1MetricURL, new Date(), "85");
                semDataSent.release();

                semMetricsReadingFinished.acquire();
                log.debug("semMetricsReadingFinished acquired.");
                log.debug("Unregistering metric cpu1.");
                stub.unregisterMetric(cpu1MetricURL);
                assertFalse(stub.isRegisteredMetric(cpu1MetricURL));
            }
            catch (Exception e) {
                e.printStackTrace();
                error = true;
                fail();
            }
            semSensorFinished.release();
            log.debug("SensorThread finished.");
        }
    }

    /**
     * UserComponent can be Adjustment, Reporting or any other component that wants to access monitoring data and
     * to be notified when specific condition occurs.
     */
    private class UserComponentThread extends Thread {
        String notificationChannel = "USER_COMP_NOTIFICATIONS_1";

        public void run() {
            try {
                // the stub objects are used locally to interact with LLMS

                LLMSConfigureMonitoringServiceImpl configStub = (LLMSConfigureMonitoringServiceImpl) context.getBean("llmsConfigureMonitoringService");
                LLMSMonitoringDataAndHistoryServiceImpl dataStub = (LLMSMonitoringDataAndHistoryServiceImpl) context.getBean("llmsMonitoringDataAndHistoryService");
                LLMSServiceInstance2MonitoringServiceImpl stub = (LLMSServiceInstance2MonitoringServiceImpl) context.getBean("llmsServiceInstance2MonitoringService");

                // test connection
                dataStub.test();

                // define CPU overflow event metric
                MetricType cpu1OverflowEventMetricType = new MetricType("cpu1OverflowEvent", "%", MetricType.MetricClass.EVENT, Integer.class);
                MetricEx cpu1OverflowEvent = new MetricEx("cpu1OverflowEvent", cpu1OverflowEventMetricType);
                // register metric
                stub.registerMetric(cpu1OverflowEvent);

                // create monitoring request
                String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1", MetricType.MetricClass.FLUENT, "CPU");

                MetricRequest metricReq = new MetricRequest(cpu1MetricURL, null, notificationChannel, null);
                RulePackage rulePackage = RulePackage.createRulePackageFromFile("src/test/resources/ExampleRuleTestPackage1.drl");

                MonitoringRequest monReq = new MonitoringRequest(new MetricRequest[]{metricReq}, new RulePackage[]{rulePackage},
                        null/*TODO*/);

                // provision monitoring request
                Object requestID = configStub.provision(monReq);
                semMonReqRegistered.release();

                // do anything else
                Thread.sleep(900);
                semDataSent.acquire();

                // get last reported value of the given metric
                MetricValue lastValue = dataStub.getMetricValue(cpu1MetricURL);
                log.debug("Last reported metric value: " + lastValue);
                assertTrue(lastValue.getIntValue() == 85);
                MetricValue lastValue1 = dataStub.getMetricValue("non_existing");
                assertNull(lastValue1);

                // test method getMetric
                Metric registeredMetric = dataStub.getMetric(cpu1MetricURL);
                assertEquals(registeredMetric.getMetricURL(), cpu1MetricURL);
                assertEquals(registeredMetric.getMetricType().getName(), "CPU");
                assertEquals(registeredMetric.getMetricType().getUnit(), "%");
                assertEquals(registeredMetric.getMetricType().getMetricClass(), MetricType.MetricClass.FLUENT);
                assertEquals(registeredMetric.getMetricType().getDataType(), Integer.class);

                // test method getMetric for non existing metric
                Metric registeredMetric1 = dataStub.getMetric("non_existing");
                assertNull(registeredMetric1);

                // test method getMatchingURLs
                ArrayList<Metric> metrics = dataStub.getMatchingURLs("*CPU*");
                assertEquals(metrics.size(), 1);
                assertEquals(metrics.get(0).getMetricURL(), cpu1MetricURL);

                // get history for the given metric and period (in this case last 50ms before the last)
                Date last = lastValue.getTimeLast();
                MetricHistory history = dataStub.getMetricValueHistory(cpu1MetricURL, new Period(new Date(last.getTime() - 150), last));
                assertTrue(history.getValues().size() == 2);

                MetricHistory history1 = dataStub.getMetricValueHistory(cpu1MetricURL, new Period(new Date(last.getTime() - 2000), new Date(last.getTime() - 1000)));
                assertTrue(history1.getValues().size() == 0);
                MetricHistory history2 = dataStub.getMetricValueHistory("not_existing", new Period(new Date(last.getTime() - 150), last));
                assertNull(history2);

                // free monitoring request
                configStub.free(requestID);
                semMetricsReadingFinished.release();
            }
            catch (Exception e) {
                e.printStackTrace();
                error = true;
                fail();
            }
            semUserCompFinished.release();
            log.debug("UserComponentThread finished.");
        }
    }
}
