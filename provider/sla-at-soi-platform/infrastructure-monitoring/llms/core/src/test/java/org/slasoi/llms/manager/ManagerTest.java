/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/manager/ManagerTest.java $
 */

package org.slasoi.llms.manager;

import junit.framework.TestCase;
import org.junit.Test;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.common.MetricType;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;


public class ManagerTest extends TestCase {

    @Test
    public void testManagerMem() {
        ApplicationContext context = new FileSystemXmlApplicationContext("config/test-config.xml");
        Manager manager = (Manager) context.getBean("manager");

        testManagerImpl(manager, context);
    }

    @Test
    public void testManagerDb() {
        ApplicationContext context = new FileSystemXmlApplicationContext("config/test-config.xml");
        Manager manager = (Manager) context.getBean("managerWithDbSupport");
        testManagerImpl(manager, context);
    }

    private void testManagerImpl(Manager manager, ApplicationContext context) {
        // PropertyConfigurator.configure("log4j.properties");
        String notificationChannel = "USER_COMP_NOTIFICATIONS_1";

        try {
            // SENSOR part
            MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
            String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1", MetricType.MetricClass.FLUENT, cpuMetricType.getName());
            MetricEx cpu1 = new MetricEx(cpu1MetricURL, cpuMetricType);

            manager.registerMetric(cpu1);

            // USER COMPONENT part
            MetricRequest metricReq = new MetricRequest(cpu1MetricURL, null, notificationChannel, notificationChannel);
            MonitoringRequest monReq = new MonitoringRequest(new MetricRequest[]{metricReq}, null, null);

            // listen to notifications
            PubSubManager pubSubManager = (PubSubManager) context.getBean("userCompPubSubManager");
            pubSubManager.addMessageListener(new MessageListener(), new String[]{notificationChannel});
            // Create the channel if not existing and subscribe to it
            pubSubManager.createAndSubscribe(new Channel(notificationChannel));


            Object requestID = manager.provision(monReq);


            // SENSOR part
            manager.storeMetricObservation(cpu1MetricURL, new Date(), "75");
            Thread.sleep(100);

            manager.storeMetricObservation(cpu1MetricURL, new Date(), "80");
            Thread.sleep(10);
            manager.storeMetricObservation(cpu1MetricURL, new Date(), "85");

            // USER COMPONENT part
            manager.free(requestID);

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testManager() {

    }

    /**
     * Listener for update and new value notifications of the chosen metric
     */
    private class MessageListener implements org.slasoi.common.messaging.pubsub.MessageListener {

        public void processMessage(MessageEvent messageEvent) {
            System.out.println("Notification message received: " + messageEvent.getMessage().getPayload());
        }
    }

}
