/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/db/DBManagerTest.java $
 */

package org.slasoi.llms.db;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.common.MetricType;
import org.slasoi.llms.common.Observation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;

public class DBManagerTest extends TestCase {
    private static final Logger log = Logger.getLogger(DBManagerTest.class);

    boolean disableTest = true;        // not to bother continuous integration

    @Test
    public void testApp() {
        if (disableTest) return;
        //PropertyConfigurator.configure("log4j.properties");
        ApplicationContext context = new FileSystemXmlApplicationContext("config/test-config.xml");

        try {
            DBManager db = (DBManager) context.getBean("dbManager");

            db.cleanDB();

            MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
            String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                    MetricType.MetricClass.FLUENT, cpuMetricType.getName());
            MetricEx cpu1 = new MetricEx(cpu1MetricURL, cpuMetricType);

            MetricType mem1MetricType = new MetricType("MEM", "KB", MetricType.MetricClass.FLUENT, Double.class);
            String mem1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                    MetricType.MetricClass.FLUENT, mem1MetricType.getName());
            MetricEx mem1 = new MetricEx(mem1MetricURL, mem1MetricType);

            db.saveMetric(cpu1);
            db.saveMetric(mem1);

            db.saveObservation(new Observation(cpu1, new Date(), "75"));
            db.saveObservation(new Observation(mem1, new Date(), Integer.toString(1024 * 1024)));
            db.saveObservation(new Observation(cpu1, new Date(), "85"));

            assertTrue(db.loadMetrics().size() == 2);
            assertTrue(db.loadObservations().size() == 3);

        }
        catch (Exception e) {
            log.error(e);
            fail();
        }
    }

}
