/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/manager/MetricRequestTest.java $
 */

package org.slasoi.llms.manager;

import junit.framework.TestCase;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricParam;
import org.slasoi.llms.common.MetricType;

import java.util.ArrayList;

public class MetricRequestTest extends TestCase {
    @Test
    public void testMetricReq() throws JSONException, ClassNotFoundException {
        String updateXmppChannelName = "updateChannel";
        String newValueXmppChannelName = "newValueChannel";
        MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
        String cpu1MetricURL =
                Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                        MetricType.MetricClass.FLUENT, cpuMetricType.getName());

        ArrayList<MetricParam> params = new ArrayList<MetricParam>();
        params.add(new MetricParam("param1", "value1"));
        params.add(new MetricParam("param2", "value2"));
        MetricRequest metricReq =
                new MetricRequest(cpu1MetricURL, params, updateXmppChannelName, newValueXmppChannelName);

        assertEquals(metricReq.getMetricURL(), cpu1MetricURL);
        assertEquals(metricReq.getUpdateXmppChannel(), updateXmppChannelName);
        assertEquals(metricReq.getNewValueXmppChannel(), newValueXmppChannelName);

        ArrayList<MetricParam> params1 = metricReq.getParams();
        assertEquals(params.get(0).getName(), params1.get(0).getName());
        assertEquals(params.get(0).getValue(), params1.get(0).getValue());

        // test MetricRequest from JSON
        JSONObject json = new JSONObject();
        json.put("metricURL", cpu1MetricURL);
        JSONArray paramsArr = new JSONArray();
        for (MetricParam param : params) {
            JSONObject o = new JSONObject();
            o.put("name", param.getName());
            o.put("value", param.getValue());
            paramsArr.put(o);
        }
        json.put("params", paramsArr);
        json.put("updateXmppChannel", updateXmppChannelName);
        json.put("newValueXmppChannel", newValueXmppChannelName);
        MetricRequest metricReqJson = new MetricRequest(json);

        assertEquals(metricReqJson.getMetricURL(), cpu1MetricURL);
        assertEquals(metricReqJson.getUpdateXmppChannel(), updateXmppChannelName);
        assertEquals(metricReqJson.getNewValueXmppChannel(), newValueXmppChannelName);

        ArrayList<MetricParam> params2 = metricReqJson.getParams();
        assertEquals(params.get(0).getName(), params2.get(0).getName());
        assertEquals(params.get(0).getValue(), params2.get(0).getValue());

        JSONObject metricReqJsonRet = metricReqJson.toJSON();
    }

}
