/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/manager/MetricRegistryTest.java $
 */

package org.slasoi.llms.manager;

import junit.framework.TestCase;
import org.junit.Test;
import org.slasoi.llms.common.Metric;
import org.slasoi.llms.common.MetricEx;
import org.slasoi.llms.common.MetricType;


public class MetricRegistryTest extends TestCase {

    @Test
    public void testApp() {

        MetricRegistry registry = new MetricRegistry();

        MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
        String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1", MetricType.MetricClass.FLUENT, cpuMetricType.getName());
        String cpu2MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM2", MetricType.MetricClass.FLUENT, cpuMetricType.getName());
        MetricEx cpu1 = new MetricEx(cpu1MetricURL, cpuMetricType);
        MetricEx cpu2 = new MetricEx(cpu2MetricURL, cpuMetricType);

        registry.registerMetric(cpu1);
        registry.registerMetric(cpu2);

        assertTrue(registry.containsMetric(cpu1MetricURL));
        assertTrue(registry.containsMetric(cpu2MetricURL));

        assertEquals(registry.getMetric(cpu1MetricURL).getMetricType().getName(), "CPU");

        assertTrue(registry.getMatchingURLs("slasoi://myManagedObject.company.com/TravelService/*/CPU").size() == 2);
        assertTrue(registry.getMatchingURLs("slasoi://myManagedObject.company.com/TravelService/VMs/VM1/*").size() == 1);

        registry.unregisterMetric(cpu1MetricURL);

        assertTrue(registry.getMatchingURLs("slasoi://myManagedObject.company.com/TravelService/*/CPU").size() == 1);
    }

}