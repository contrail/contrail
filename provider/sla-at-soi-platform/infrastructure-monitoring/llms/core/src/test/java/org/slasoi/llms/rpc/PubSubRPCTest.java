/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/rpc/PubSubRPCTest.java $
 */

package org.slasoi.llms.rpc;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.Test;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class PubSubRPCTest extends TestCase {
    private static final Logger log = Logger.getLogger(PubSubRPCTest.class);

    String channelName = "RPC_TEST";

    @Test
    public void testApp() {
//		PropertyConfigurator.configure("log4j.properties");
        ApplicationContext context = new FileSystemXmlApplicationContext("config/test-config.xml");

        // NOTE: In this example server and client code are in the same thread.
        // In real life they are in separate executable and server is up and running before the client starts.

        // START OF SERVER CODE
        try {
            // Create PubSubRPC instances. Make sure properties files are set.
            PubSubManager pubSubManager = (PubSubManager) context.getBean("managerPubSubManager");
            PubSubRPC pubSubRPCServer = new PubSubRPC(pubSubManager, channelName, true);

            // Register command handlers
            // NOTE: There can be just one handler per each command on each pubsub channel
            pubSubRPCServer.addCommandHandler("increment", new CommandHandler() {
                // that code is executed in MessageListener thread
                public Object processCommand(String command, JSONObject o) throws Exception {
                    int counter = o.getInt("counter");
                    int retval = counter + 1;

                    log.debug(String.format("increment(%d)=%d", counter, retval));
                    return retval;     //send return message
                }
            });
        }
        catch (Exception e) {
            log.error(e);
            fail();
        }
        // END OF SERVER CODE

        // START OF CLIENT CODE
        try {
            PubSubManager pubSubManager = (PubSubManager) context.getBean("sensorPubSubManager");
            PubSubRPC pubSubRPCClient = new PubSubRPC(pubSubManager, channelName, false);
            // use proper properties file TODO

            int counter = 0;
            counter = increment(pubSubRPCClient, counter);
            log.info("counter=" + counter);
            assertEquals(1, counter);

            counter = increment(pubSubRPCClient, counter);
            log.info("counter=" + counter);
            assertEquals(2, counter);
        }
        catch (Exception e) {
            log.error(e);
            fail();
        }
        // END OF CLIENT CODE
    }

    // synchronous blocking function
    int increment(PubSubRPC pubSubRPC, int counter) {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "increment"); // obligatory for each command
            args.put("counter", counter);

            JSONObject retObj = pubSubRPC.sendCommand(args, true);
            return retObj.getInt("retval");

        }
        catch (Exception e) { // JSONException or MessagingException
            log.error(e);
            return -1;
        }
    }

}


