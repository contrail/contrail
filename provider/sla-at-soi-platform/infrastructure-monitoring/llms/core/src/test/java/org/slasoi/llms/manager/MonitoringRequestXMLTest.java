/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/manager/MonitoringRequestXMLTest.java $
 */

package org.slasoi.llms.manager;

import junit.framework.TestCase;
import org.drools.builder.ResourceType;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class MonitoringRequestXMLTest extends TestCase {

    @Test
    public void testApp() throws IOException {

        MonitoringRequest monReq = MonitoringRequest.createFromXML(
                MonitoringRequestXMLTest.readFileAsString("src/test/resources/MonitoringRequest.xml"));

        assertEquals(monReq.getMetrics().get(1).getMetricURL(), "slasoi://myManagedObject.company.com/DemoService/Tashi/192.168.1.26/fluent/cpu_num");
        assertEquals(monReq.getMetrics().get(0).getParams().get(1).getName(), "type");
        assertEquals(monReq.getMetrics().get(0).getParams().get(1).getValue(), "max");

        assertEquals(monReq.getRulePackages().get(0).getType(), ResourceType.DRL);
        assertEquals(monReq.getNotificationChannel(), "myCustomNotificationChannel");
    }

    private static String readFileAsString(String filePath) throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }
}
