/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/AllTests.java $
 */

package org.slasoi.llms;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.slasoi.llms.common.ObservationTest;
import org.slasoi.llms.common.PeriodTest;
import org.slasoi.llms.db.DBManagerTest;
import org.slasoi.llms.manager.*;
import org.slasoi.llms.osgi.LLMSServiceImplTest;
import org.slasoi.llms.rpc.PubSubRPCTest;
import org.slasoi.llms.stubs.ExampleStubRemoteTest;
import org.slasoi.llms.stubs.ExampleStubTest;
import org.slasoi.llms.util.UtilTest;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Tests for org.slasoi.llms.manager");
        //$JUnit-BEGIN$
        suite.addTestSuite(ObservationTest.class);
        suite.addTestSuite(PeriodTest.class);
        suite.addTestSuite(DBManagerTest.class);
        suite.addTestSuite(DroolsGatewayTest.class);
        suite.addTestSuite(ExampleMTest.class);
        suite.addTestSuite(ManagerTest.class);
        suite.addTestSuite(MetricRegistryTest.class);
        suite.addTestSuite(MetricRequestTest.class);
        suite.addTestSuite(MonitoringRequestXMLTest.class);
        suite.addTestSuite(MonitoringStoreTest.class);
        suite.addTestSuite(LLMSServiceImplTest.class);
        suite.addTestSuite(PubSubRPCTest.class);
        suite.addTestSuite(UtilTest.class);
        suite.addTestSuite(ExampleStubTest.class);
        suite.addTestSuite(ExampleStubRemoteTest.class);     // cannot run in the same testsuite as it (re)uses some singletons (PROBABLY)
        //$JUnit-END$
        return suite;
    }
}
