package org.slasoi.businessManager.common.ws.types;

public class IndividualType {
    private long languageId;
    private long countryId;
    private String address;
    private String email;
    private String fax;
    private String firstName;
    private String jobdepartment;
    private String jobtitle;
    private String lastName;
    private String phoneNumber;
    
    public long getLanguageId() {
        return languageId;
    }
    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }
    public long getCountryId() {
        return countryId;
    }
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getFax() {
        return fax;
    }
    public void setFax(String fax) {
        this.fax = fax;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getJobdepartment() {
        return jobdepartment;
    }
    public void setJobdepartment(String jobdepartment) {
        this.jobdepartment = jobdepartment;
    }
    public String getJobtitle() {
        return jobtitle;
    }
    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    

}
