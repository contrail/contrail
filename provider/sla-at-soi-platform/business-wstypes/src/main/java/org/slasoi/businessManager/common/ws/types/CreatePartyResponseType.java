package org.slasoi.businessManager.common.ws.types;

public class CreatePartyResponseType {
    private int responseCode;
    private String responseMessage;
    
    
    public CreatePartyResponseType(){}
    
    public CreatePartyResponseType(int responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}
	public int getResponseCode() {
        return responseCode;
    }
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
    public String getResponseMessage() {
        return responseMessage;
    }
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    
}
