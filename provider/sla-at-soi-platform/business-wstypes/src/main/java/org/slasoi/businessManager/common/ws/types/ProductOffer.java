package org.slasoi.businessManager.common.ws.types;

public class ProductOffer {
    
    private long id;
    private String name;
    private String description;
    private String [] services;
    private RatingType rating;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String[] getServices() {
        return services;
    }
    public void setServices(String[] services) {
        this.services = services;
    }
	public RatingType getRating() {
		return rating;
	}
	public void setRating(RatingType rating) {
		this.rating = rating;
	}
    

}
