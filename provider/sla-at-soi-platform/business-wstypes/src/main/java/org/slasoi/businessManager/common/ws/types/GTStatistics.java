/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/types/GTStatistics.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.common.ws.types;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class GTStatistics {
    int numberGTs;
    int numberGTsNoViolatedNoPenalty;
    int numberGTsViolatedNoPenalty;
    int numberGTsViolatedWithPenalty;

    public GTStatistics() {

    }

    public GTStatistics(int numberGTs, int numberGTsNoViolatedNoPenalty, int numberGTsViolatedNoPenalty,
            int numberGTsViolatedWithPenalty) {
        this.numberGTs = numberGTs;
        this.numberGTsNoViolatedNoPenalty = numberGTsNoViolatedNoPenalty;
        this.numberGTsViolatedNoPenalty = numberGTsViolatedNoPenalty;
        this.numberGTsViolatedWithPenalty = numberGTsViolatedWithPenalty;
    }

    public int getNumberGTs() {
        return numberGTs;
    }

    public void setNumberGTs(int numberGTs) {
        this.numberGTs = numberGTs;
    }

    public int getNumberGTsViolatedNoPenalty() {
        return numberGTsViolatedNoPenalty;
    }

    public void setNumberGTsViolatedNoPenalty(int numberGTsViolatedNoPenalty) {
        this.numberGTsViolatedNoPenalty = numberGTsViolatedNoPenalty;
    }

    public int getNumberGTsViolatedWithPenalty() {
        return numberGTsViolatedWithPenalty;
    }

    public void setNumberGTsViolatedWithPenalty(int numberGTsViolatedWithPenalty) {
        this.numberGTsViolatedWithPenalty = numberGTsViolatedWithPenalty;
    }

    public int getNumberGTsNoViolatedNoPenalty() {
        return numberGTsNoViolatedNoPenalty;
    }

    public void setNumberGTsNoViolatedNoPenalty(int numberGTsNoViolatedNoPenalty) {
        this.numberGTsNoViolatedNoPenalty = numberGTsNoViolatedNoPenalty;
    }

}
