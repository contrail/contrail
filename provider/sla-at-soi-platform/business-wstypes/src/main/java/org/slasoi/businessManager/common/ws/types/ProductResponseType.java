package org.slasoi.businessManager.common.ws.types;

import org.slasoi.businessManager.common.ws.types.ParameterDataType;

public class ProductResponseType {

    private int responseCode;
    private String responseMessage;
    private Product [] productOfferList;

    public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Product[] getProductOfferList() {
		return productOfferList;
	}
	public void setProductOfferList(Product[] productOfferList) {
		this.productOfferList = productOfferList;
	}
	
}
