/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/types/SLAStatistics.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.common.ws.types;

/**
 * @author Beatriz
 * 
 */
public class SlaStatistics {

    int numberSLAs;
    int numberSLAsOK;
    int numberSLAsViolatedNoPenalty;
    int numberSLAsViolatedWithPenalty;

    public SlaStatistics() {

    }

    public SlaStatistics(int numberSLAs, int numberSLAsOK, int numberSLAsViolatedNoPenalty,
            int numberSLAsViolatedWithPenalty) {
        this.numberSLAs = numberSLAs;
        this.numberSLAsOK = numberSLAsOK;
        this.numberSLAsViolatedNoPenalty = numberSLAsViolatedNoPenalty;
        this.numberSLAsViolatedWithPenalty = numberSLAsViolatedWithPenalty;
    }

    public int getNumberSLAs() {
        return numberSLAs;
    }

    public void setNumberSLAs(int numberSLAs) {
        this.numberSLAs = numberSLAs;
    }

    public int getNumberSLAsOK() {
        return numberSLAsOK;
    }

    public void setNumberSLAsOK(int numberSLAsOK) {
        this.numberSLAsOK = numberSLAsOK;
    }

    public int getNumberSLAsViolatedNoPenalty() {
        return numberSLAsViolatedNoPenalty;
    }

    public void setNumberSLAsViolatedNoPenalty(int numberSLAsViolatedNoPenalty) {
        this.numberSLAsViolatedNoPenalty = numberSLAsViolatedNoPenalty;
    }

    public int getNumberSLAsViolatedWithPenalty() {
        return numberSLAsViolatedWithPenalty;
    }

    public void setNumberSLAsViolatedWithPenalty(int numberSLAsViolatedWithPenalty) {
        this.numberSLAsViolatedWithPenalty = numberSLAsViolatedWithPenalty;
    }

    public void addNumberSLAs(int number) {
        numberSLAs = numberSLAs + number;
    }

    public void addNumberSLAsOK(int number) {
        this.numberSLAsOK = numberSLAsOK + number;
    }

    public void addNumberSLAsViolatedNoPenalty(int number) {
        this.numberSLAsViolatedNoPenalty = numberSLAsViolatedNoPenalty + number;
    }

    public void addNumberSLAsViolatedWithPenalty(int number) {
        this.numberSLAsViolatedWithPenalty = numberSLAsViolatedWithPenalty + number;
    }

    public void incrNumberSLAs() {
        numberSLAs = numberSLAs + 1;
    }

    public void incrNumberSLAsOK() {
        this.numberSLAsOK = numberSLAsOK + 1;
    }

    public void incrNumberSLAsViolatedNoPenalty() {
        this.numberSLAsViolatedNoPenalty = numberSLAsViolatedNoPenalty + 1;
    }

    public void incrNumberSLAsViolatedWithPenalty() {
        this.numberSLAsViolatedWithPenalty = numberSLAsViolatedWithPenalty + 1;
    }
}
