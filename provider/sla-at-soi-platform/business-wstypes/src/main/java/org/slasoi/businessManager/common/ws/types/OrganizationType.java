package org.slasoi.businessManager.common.ws.types;

public class OrganizationType {
    private String tradingName;
    private String description;
    private String fiscalId;
    private long countryId;
    private IndividualType [] individuals;
    
    
    public long getCountryId() {
        return countryId;
    }
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
    public String getTradingName() {
        return tradingName;
    }
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getFiscalId() {
        return fiscalId;
    }
    public void setFiscalId(String fiscalId) {
        this.fiscalId = fiscalId;
    }
    public IndividualType[] getIndividuals() {
        return individuals;
    }
    public void setIndividuals(IndividualType[] individuals) {
        this.individuals = individuals;
    }


}
