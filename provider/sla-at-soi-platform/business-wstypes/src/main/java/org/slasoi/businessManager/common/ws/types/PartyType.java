package org.slasoi.businessManager.common.ws.types;

public class PartyType {
	private long partyId;
    private long currencyId;
    private IndividualType individual;   
    private OrganizationType organization;
    private UserType[] users;
    
    public long getPartyId(){
    	return partyId;
    }
    
    public void setPartyId(long partyId){
    	this.partyId= partyId;
    }
        public long getCurrencyId() {
        return currencyId;
    }
        
        
        
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }
    public IndividualType getIndividual() {
        return individual;
    }
    public void setIndividual(IndividualType individual) {
        this.individual = individual;
    }
    public OrganizationType getOrganization() {
        return organization;
    }
    public void setOrganization(OrganizationType organization) {
        this.organization = organization;
    }
    public UserType[] getUsers() {
        return users;
    }
    public void setUsers(UserType[] users) {
        this.users = users;
    }
    
    
}
