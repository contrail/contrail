package org.slasoi.businessManager.common.ws.types;

public class ParameterDataType {
    private long parameterId;
    private String parameterName;
    
    
    public long getParameterId() {
        return parameterId;
    }
    public void setParameterId(long parameterId) {
        this.parameterId = parameterId;
    }
    public String getParameterName() {
        return parameterName;
    }
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    
}
