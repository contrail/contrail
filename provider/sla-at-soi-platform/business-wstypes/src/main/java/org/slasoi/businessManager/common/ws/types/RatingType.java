package org.slasoi.businessManager.common.ws.types;

public class RatingType {

	private long votes;
	private double average;
	
	public RatingType(long votes, double average) {
		super();
		this.votes = votes;
		this.average = average;
	}
	public long getVotes() {
		return votes;
	}
	public void setVotes(long votes) {
		this.votes = votes;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}


	
}
