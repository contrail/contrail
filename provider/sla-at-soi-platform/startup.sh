echo -e "\nStarting SLAM ..."

rootdir=`pwd`
export SLASOI_HOME=$rootdir/osgi-config
echo -e "\nSLASOI_HOME=$SLASOI_HOME"

ipaddr=`ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`
slam_conf=$SLASOI_HOME/slams/contrail.instance1.cfg
echo -e "\nConfiguring $ipaddr as SLAM address in $slam_conf..."
sed "s/localhost/$ipaddr/g" -i "$slam_conf" > /dev/null

PATH=$PATH:$rootdir/../pax-runner-1.7.1/bin
export PATH
# echo -e "\nPATH=$PATH"

echo -e "\nStarting syntax converter..."
cd $rootdir/syntax-converter
java  -jar $rootdir/syntax-converter/syc-broker.jar &
cd $rootdir/pax-runner

echo -e "\nStarting SLA@SOI platform...\n\n"
pax-run.sh #--clean

cd $rootdir
