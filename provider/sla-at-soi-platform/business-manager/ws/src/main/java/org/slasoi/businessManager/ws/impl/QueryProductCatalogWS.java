package org.slasoi.businessManager.ws.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.businessManager.productSLA.productDiscovery.ProductDiscovery;
import org.slasoi.businessManager.ws.interfaces.QueryProductCatalogIf;
import org.slasoi.businessManager.ws.internal.Activator;
import org.slasoi.businessManager.ws.internal.Base64Utils;
import org.slasoi.businessManager.common.ws.types.Product;

import org.slasoi.slamodel.sla.SLATemplate;

public class QueryProductCatalogWS implements QueryProductCatalogIf {
	private static final Logger LOGGER = Logger
			.getLogger(QueryProductCatalogWS.class);

	/**
	 * 
	 * @param customerId
	 * @param productId
	 * @return
	 */
	public String getTemplates(long customerId, long productId) {
		try {
			LOGGER.debug("Inside getTemplates method");
			ProductDiscovery discovery = getProductDiscovery();
			List<SLATemplate> templates = discovery.getTemplatesSLAModel(
					customerId, productId);
			if (templates != null && templates.size() > 0) {
				SLATemplate[] offers = new SLATemplate[templates.size()];
				offers = templates.toArray(offers);
				LOGGER.debug("Array size:" + offers.length);
				String result = Base64Utils.encode(offers);
				return result;
			} else {
				return null;
			}
		} catch (Exception ex) {
			LOGGER.error("Error:" + ex.toString());
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * @return encoded SLATemplates (all)
	 */
	public String getAllTemplates() {
		try {
			LOGGER.debug("Inside getTemplates method");
			ProductDiscovery discovery = getProductDiscovery();
			SLATemplate[] templates = discovery.getTemplates();
			LOGGER.debug("SLA template array size: " + templates.length);
			return Base64Utils.encode(templates);
		} catch (Exception ex) {
			LOGGER.error("Error:" + ex.toString());
			ex.printStackTrace();
			return null;
		}
	}

	/**
  *   
  */
	public Product[] getProducts(String customerID, String[] parametersList) {
		try {
			LOGGER.debug("Inside getProducts method");
			LOGGER.debug("customerID" + customerID);
			LOGGER.debug("Parameters : " + parametersList.toString());
			ProductDiscovery discovery = getProductDiscovery();
			Product[] productsOffer = discovery.getWsProducts(customerID,
					parametersList);
			return productsOffer;

		} catch (Exception e) {
			LOGGER.error("Error: " + e.toString());
			e.printStackTrace();
			return null;
		}
	}

	private ProductDiscovery getProductDiscovery() throws Exception {
		BundleContext bc = Activator.getContext();
		if (bc == null)
			throw new Exception("Bundle Context Null.");
		ServiceReference ref = bc.getServiceReference(ProductDiscovery.class
				.getName());
		if (ref == null)
			throw new Exception("No Service Product Discovery found.");

		return (ProductDiscovery) bc.getService(ref);

	}

}
