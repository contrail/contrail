package org.slasoi.businessManager.ws.internal;

import java.util.Dictionary;
import java.util.Properties;

import org.apache.axis2.osgi.deployment.tracker.WSTracker;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slasoi.businessManager.ws.impl.PartyWS;
import org.slasoi.businessManager.ws.impl.ProviderWS;
import org.slasoi.businessManager.ws.impl.QueryProductCatalogWS;
import org.slasoi.businessManager.ws.impl.QueryTemplatesWS;

public class Activator implements BundleActivator {

    private static BundleContext componentBServiceTracker;

    public void start(BundleContext context) throws Exception {
        
    	Dictionary prop = new Properties();
        prop.put(WSTracker.AXIS2_WS, "BusinessManager_QueryProductCatalog");
        context.registerService(QueryProductCatalogWS.class.getName(), new QueryProductCatalogWS(), prop);

        Dictionary prop2 = new Properties();
        prop2.put(WSTracker.AXIS2_WS, "BusinessManager_Party");
        context.registerService(PartyWS.class.getName(), new PartyWS(), prop2);        

        Dictionary prop3 = new Properties();
        prop3.put(WSTracker.AXIS2_WS, "BusinessManager_Provider");
        context.registerService(ProviderWS.class.getName(), new ProviderWS(), prop3);        

        Dictionary prop4 = new Properties();
        prop4.put(WSTracker.AXIS2_WS, "BusinessManager_QueryTemplates");
        context.registerService(QueryTemplatesWS.class.getName(), new QueryTemplatesWS(), prop4);        
        
        componentBServiceTracker = context;
    }

    public void stop(BundleContext context) throws Exception {

    }

    public static BundleContext getContext() {
        return componentBServiceTracker;
    }
} 
