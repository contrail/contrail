package org.slasoi.businessManager.ws.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.businessManager.productSLA.productDiscovery.ProductDiscovery;
import org.slasoi.businessManager.ws.interfaces.QueryTemplatesIf;
import org.slasoi.businessManager.ws.internal.Activator;
import org.slasoi.businessManager.ws.internal.Base64Utils;
import org.slasoi.businessManager.common.ws.types.Product;

import org.slasoi.slamodel.sla.SLATemplate;


public class QueryTemplatesWS implements QueryTemplatesIf {

	private static final Logger LOGGER = Logger.getLogger(QueryTemplatesWS.class);


    public String getTemplates() {
        try {

        	LOGGER.debug("Inside getTemplates method");

            BundleContext bc= Activator.getContext();
            if (bc == null) throw new Exception("Bundle Context Null.");
            
            ServiceReference ref = bc.getServiceReference(ProductDiscovery.class.getName());
            if (ref == null) throw new Exception("No Service Product Discovery found.");
            
            ProductDiscovery pd = (ProductDiscovery) bc.getService(ref);
            
            SLATemplate[] templates= pd.getTemplates();
            
            LOGGER.debug("SLA template array size: " + templates.length);
            
            return Base64Utils.encode(templates);
        }
        catch (Exception ex) {
            LOGGER.error("Error: " + ex.toString());
            ex.printStackTrace();
            return null;
        }
    }

    
    
    

    
    
    
}
