package org.slasoi.businessManager.ws.internal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;

public class Base64Utils {
    public Base64Utils() {
    }

    public static String encode(Object obj) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(obj);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Object decode(String response) {
        Object result = null;

        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64.decodeBase64(response.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            result = is.readObject();
            is.close();

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}
