package org.slasoi.businessManager.ws.interfaces;

import org.slasoi.businessManager.common.ws.types.Product;
import org.slasoi.slamodel.sla.SLATemplate;

public interface QueryProductCatalogIf {
    /**
     * 
     * @param customerId
     * @param productId
     * @return SLATemplate[] object parse into base64
     */
    // public SLATemplate[] getTemplates(long customerId,long productId);
    public String getTemplates(long customerId,long productId);

    
    /**
     * 
     * @return SLATemplate[] object parse into base64
     */
    public String getAllTemplates();

    
    
    /**
     * 
     * @param customerID
     * @param parameters
     * @return
     */
    public Product[] getProducts(String customerID, String[] parameters);

}
