/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.core;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.slamodel.sla.SLA;

public interface BillingEngine {
    
    /**
     * This method checks if the account of a customer is valid
     * @param partyId
     * @param amount
     * @param currency
     * @return
     */
    
    public boolean checkChargingCustomer(Long partyId,BigDecimal amount, EmCurrencies currency);
    
    /**
     * Start the charging process of a customer
     * @param bsla
     * @return
     */
    public boolean startChargingBSLA(SLA bsla);
    
    
    /**
     * Start the periodic charging for the customers
     * @param bsla
     * @return
     */
    public boolean periodicCharging(Date date);
    
    
    /**
     * Stop the the charging of  a Customer. 
     * @param bslaId
     * @param Code --> Code with termination reason, for instant, normal,malfunction, etc..
     * @return
     */
    public boolean stopChargingBSLA(SLA bsla,String Code);
    
    /**
     * Charge an event to an account 
     * @param slaid
     * @param Event
     * @TODO  Define the data needed to be received to do the charing
     * @return
     */
    public boolean eventCharging(String slaid,String Event);
    
    /**
     * Charging of penalties
     * @param slaId -->ID of SLA that suffers a penalty
     * @param partyType --> Customer,provider
     * @param Penalty --> penaltyData such as amount?
     * @param Description -->Description of the event
     * @return
     */
    public boolean penaltyCharging (String slaId, String partyType,String Penalty, String Description);
    
    /**
     * Obtain Charging information about the customer
     * @param criteriaParameters -->this data could be for instant, slaid,productid,
     *        productOfferID, partyid
     * @TODO define this information or the way to do it
     * @return
     */
    public EmPartyBank getChargingInformation(Long partyID,Date periodStart, Date periodEnd)throws Exception;
    
}
