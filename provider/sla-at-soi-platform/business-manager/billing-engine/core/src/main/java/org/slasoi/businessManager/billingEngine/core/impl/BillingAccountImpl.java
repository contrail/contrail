/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.core.impl;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.billingEngine.core.BankPayment;
import org.slasoi.businessManager.billingEngine.core.BillingAccount;
import org.slasoi.businessManager.billingEngine.dao.AccountEventDAO;
import org.slasoi.businessManager.billingEngine.dao.AccountEventTypeDAO;
import org.slasoi.businessManager.billingEngine.dao.PartyBankDAO;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.billing.EmAccountEventType;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.billingEngine.service.impl.AccountEventManagerImpl;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.billingEngine.util.InsufficientCreditException;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.osgi.extensions.annotation.ServiceReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="billingAccountService")
public class BillingAccountImpl implements BillingAccount {
    private static final Logger log = Logger.getLogger(AccountEventManagerImpl.class);
    
    @Autowired
    private PartyBankDAO partyBankDAO; 
    @Autowired
    private AccountEventDAO accountEventDAO; 
    @Autowired
    private AccountEventTypeDAO accountEventTypeDAO; 
    @Autowired
    private BankPayment bankPaymentService;
    @Autowired
    private PartyManager partyService;   
    
 /*   @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setPartyService(PartyManager partyService) {
        log.info("***************Registered Service partyService Found*************");
        this.partyService = partyService;
    }*/   
    
    /**
     * Create a new Bank Account   
     * @param account
     * @return
     * @throws Exception
     */
       @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
       public Long createAccount(EmPartyBank account)throws Exception{
           log.info("Into createAccount method");
           if(partyBankDAO.getPartyBankActiveByParty(account.getEmParty().getNuPartyid()) != null){     
               log.info("There is a previous account");
               account.setTcDefaultYn(BillingConstants.CHAR_NO);
           }else{
               log.info("First account");
               account.setTcDefaultYn(BillingConstants.CHAR_YES);
           }
           this.partyBankDAO.save(account);
           log.info("Account created correctly with id: "+account.getNuBankId());
           return account.getNuBankId();           
       }              
       /**
        * Give a summary of an account during a period
        * @param partyID
        * @param periodStart
        * @param periodEnd
        * @return EmPartyBank: contains all the info related to an account and the corresponding events.
        * @throws Exception
        */
      public EmPartyBank getAccountStatement(Long partyID,Date periodStart, Date periodEnd) throws Exception{
          log.info("Into getAccountStatement method");         
          //EmPartyBank partyBank = this.partyBankDAO.getPartyBankActiveByParty(partyID);
          EmParty emParty = partyService.getPartyById(partyID);
          EmPartyBank partyBank = new EmPartyBank();
          partyBank.setEmParty(emParty);
          if(partyBank != null){             
              List <EmAccountEvent> events = accountEventDAO.getEventsByPartyAndDates(partyID,periodStart,periodEnd);
              if(events !=null && events.size()>0){
                  partyBank.setEmAccountEvents(new HashSet<EmAccountEvent>(events));
              }else{
                  log.info("Events are null");   
                  partyBank.setEmAccountEvents(null);
              }
          }
          return partyBank;
      }
 /**
  * This method set the active account to a party     
  * @param partyID
  * @param bankAcountId
  * @throws Exception
  */
      @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
      public void  setActiveAccount(Long partyID,Long bankAcountId) throws Exception{
          log.info("Into getAccountStatement method");         
          EmPartyBank partyBank = this.partyBankDAO.getPartyBankActiveByParty(partyID); 
          if(partyBank != null){             
              partyBank.setTcDefaultYn(BillingConstants.CHAR_NO);
              this.partyBankDAO.saveOrUpdate(partyBank);
          }
          EmPartyBank partyBank2 = this.partyBankDAO.load(bankAcountId);             
          partyBank2.setTcDefaultYn(BillingConstants.CHAR_YES);
          this.partyBankDAO.saveOrUpdate(partyBank2);
      }

      /**
       * Close an account
       * @throws RemoteException If the account cannot be closed
       */
      public void closeAccount(Long PartyId) throws Exception{
          log.info("Into closeAccount method");
          /**
           * @TODO what would do this method ?
           */
          
      }
      /**
       * Set the credit Limit to a customer      
       * @param partyid
       * @param creditLimit
       * @throws Exception
       */
        @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
       public void setCreditLimit(Long partyid,BigDecimal creditLimit) throws Exception {
           log.info("Into setCreditLimit method");
           if (creditLimit.compareTo(ZERO) < 0)
              throw new Exception("Credit limit less than zero!");
           EmParty party = partyService.getPartyById(partyid);
           party.setNuCreditLimit(creditLimit);
           partyService.saveOrUpdate(party);
      }           
 /**
  * This method search into the database the parties with negative balance and make the corresponding bill      
  * @throws Exception
  */
       @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
       public void billToCustomers()throws Exception{
           log.info("Into billToCustomers method");
           List<EmParty> parties = partyService.getPartiesWithNegativeBalance();
           if(parties != null ){
               for(EmParty party:parties){
                   //obtain the active account
                  //EmPartyBank bank = this.partyBankDAO.getPartyBankActiveByParty(party.getNuPartyid());
                  //Save payment Event
                  log.info("Save payment Event");
                  BigDecimal balance = party.getNuBalance().abs();
                  EmAccountEvent paymentEvent = new EmAccountEvent();
                  paymentEvent.setTxDescription("Payment of the bill");
                  paymentEvent.setNuAmount(balance);
                  paymentEvent.setCurrency(party.getEmCurrencies());
                  //paymentEvent.setEmPartyBank(bank);                  
                  paymentEvent.setPayer(party);
                  paymentEvent.setTcType(BillingConstants.EVENT_CREDIT);
                  paymentEvent.setTsTimeStamp(new Date());
                  EmAccountEventType emAccountEventType = accountEventTypeDAO.getByName(BillingConstants.EVENT_TYPE_PAYMENT);
                  if(emAccountEventType == null){
                      //create paymentEvent
                      log.info("paymentEventType does not exist. Create it");
                      emAccountEventType = new EmAccountEventType();
                      emAccountEventType.setTxName(BillingConstants.EVENT_TYPE_PAYMENT);
                      emAccountEventType.setTxDescription("Event of payment");
                      accountEventTypeDAO.saveOrUpdate(emAccountEventType);
                  }
                  paymentEvent.setEmAccountEventType(emAccountEventType);
                  this.accountEventDAO.saveOrUpdate(paymentEvent);
                  //charging events with nu_event null
                  List <EmAccountEvent> events = this.accountEventDAO.getUnPaidByParty(party.getNuPartyid());
                  log.info("Updating events related to the current payment");
                  if(events!=null && events.size()>0){
                      for(EmAccountEvent event:events){
                          event.setPaymentEvent(paymentEvent);
                          this.accountEventDAO.saveOrUpdate(event);
                      }                      
                  }                                                   
                  this.accountEventDAO.save(paymentEvent);                 
                  //change balance to O                 
                  log.info("St balance to 0");
                  party.setNuBalance(new BigDecimal(0));
                  partyService.saveOrUpdate(party);
                  log.info("Chargue the payment to the bank");                  
                  //contact to the bank and charge the account                  
                  //this.bankPaymentService.makeTransaction(bank.getTxAccountNumber(),balance,party.getEmCurrencies());   
                  this.bankPaymentService.makeTransaction("number_account",balance,party.getEmCurrencies());
               }               
           }    
           log.info("billToCustomers end.");   
       }

   /**
    * Charge an event to the corresponding account
    * @param event
    * @param currency
    * @throws Exception
    */
       @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
      public void bill(EmAccountEvent event)throws Exception{
          log.info("Into bill method");
          if(BillingConstants.EVENT_CREDIT == event.getTcType()){
              log.info("The event is a payment");                 
              event.getPayer().setNuBalance(event.getPayer().getNuBalance().add(event.getNuAmount()));
          }else if(BillingConstants.EVENT_DEBIT == event.getTcType()){
              log.info("The event is a charge");
              this.checkCreditAvailable(event.getPayer().getNuPartyid(), event.getNuAmount(), event.getCurrency());
              //subtract because the balance is negative
              event.getPayer().setNuBalance(event.getPayer().getNuBalance().subtract(event.getNuAmount().abs()));
          }else{
              throw new Exception("Event type not found");
          }
          log.info("Saving data into the database");
          accountEventDAO.save(event);
          EmParty payer = partyService.getPartyById(event.getPayer().getNuPartyid());
          payer.setNuBalance( event.getPayer().getNuBalance());
          this.partyService.saveOrUpdate(payer);
      }


      /** Test whether we can withdraw this amount now.
       * @param amount amount to test
       * @param currency units for amount
       * @throws InsufficientCreditException if there are insufficient funds
       * @throws Exception for other errors
       */
      public void checkCreditAvailable(Long partyId,BigDecimal amount, EmCurrencies currency) throws InsufficientCreditException, Exception{
          log.info("Into checkCreditAvailable method");
          EmParty party = partyService.getPartyById(partyId);
          if (amount.compareTo(ZERO) > 0){
              log.info("Amount must be <= 0!");
              throw new Exception("Amount must be <= 0!");
          }

          if (currency.getNuCurrencyId().longValue()!= 
              party.getEmCurrencies().getNuCurrencyId().longValue()) {
              log.info("Currency is diference.Translate it.");
              throw new Exception("Can't check credit: this account's currency is " +
                              party.getEmCurrencies().getTxName() + ", not " + currency.getTxName());
              //make a translation
          }                        
          
          if((party.getNuBalance().longValue()+ amount.longValue()) >0){
              log.info("The customer has got enougth credit.");
          
          }else if ((Math.abs((party.getNuBalance().longValue()))+ amount.longValue()) > party.getNuCreditLimit().longValue()) {
              log.info("The customer hasn't got enougth credit.");
              throw new InsufficientCreditException("Not enough credit; " +
                  "current liability (" + party.getNuBalance().longValue() + currency.getTxName() + ") + amount (" + amount.longValue() + currency + ") would exceed " +
                  "credit limit (" + party.getNuCreditLimit().longValue() + currency.getTxName() + ")");
          }
          
          log.info("The customer has got enougth credit.");
      }

}
