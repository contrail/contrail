/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.core.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.billingEngine.core.BillingAccount;
import org.slasoi.businessManager.billingEngine.core.BillingEngine;
import org.slasoi.businessManager.billingEngine.dao.AccountEventTypeDAO;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.billingEngine.util.Utils;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.billing.EmAccountEventType;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.common.service.CurrencyService;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.business;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value="billingEngineService")
public class BillingEngineImpl implements BillingEngine{
    private static final Logger log = Logger.getLogger(BillingEngineImpl.class);
    
    @Autowired
    private BillingAccount billingAccountService; 
    @Autowired
    private AccountEventTypeDAO accountEventTypeDAO;
    @Autowired
    private PartyManager partyService;   
    @Autowired
    private CurrencyService currencyService; 
    @Autowired
    private CustomerProductManager customerProductService;
    @Autowired
    private ProductOfferManager productOfferService;
    
    
    
    /**@ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setPartyService(PartyManager partyService) {
        log.info("***************Registered Service partyService Found*************");
        this.partyService = partyService;
    }   
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setCurrencyService(CurrencyService currencyService) {
        log.info("***************Registered Service partyService Found*************");
        this.currencyService = currencyService;
    } 
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setCustomerProductService(CustomerProductManager customerProductService) {
        log.info("***************Registered Service partyService Found*************");
        this.customerProductService = customerProductService;
    } **/
    
    
    /**
     * This method checks if the account of a customer is valid
     * @param partyId
     * @return
     */
    public boolean checkChargingCustomer(Long partyId,BigDecimal amount, EmCurrencies currency){
        log.debug("Into checkChargingCustomer method");
        try{
            billingAccountService.checkCreditAvailable( partyId, amount, currency);
            log.debug("Credit available");
            return true;
        }catch(Exception e){
           log.debug("Error:"+e.toString());
           return false;
        }
    }
    
    /**
     * Start the charging process of a customer
     * @param bsla
     * @return
     */
    public boolean startChargingBSLA(SLA bsla){
        log.debug("Into startChargingBSLA method");
        try{         
            Utils utils = new Utils();
            CONST charge = utils.searchforSubscriptionPrice(bsla);
            if(charge !=null){
                log.debug("There is a Subscription to pay:" + charge.getValue()+ " "+charge.getDatatype());
                Long partyId = utils.getCustomerIdFromSLA(bsla);
                if(partyId !=null){
                    //EmPartyBank emPartyBank = partyBankDAO.getPartyBankActiveByParty(partyId);
                    EmParty payer = partyService.getPartyById(partyId);
                    EmCurrencies currency = currencyService.getCurrencyByName(charge.getDatatype().getValue().
                            substring(charge.getDatatype().getValue().lastIndexOf("#")+1));
                    EmAccountEvent event = new EmAccountEvent();
                    //event.setEmPartyBank(emPartyBank);
                    event.setCurrency(currency);
                    event.setNuAmount((new BigDecimal(charge.getValue())).negate());
                    EmAccountEventType emAccountEventType = accountEventTypeDAO.getByName(BillingConstants.EVENT_TYPE_SUBCRITPTION);
                    if(emAccountEventType == null){
                        //create subcriptionEvent
                        log.info("paymentEventType does not exist. Create it");
                        emAccountEventType = new EmAccountEventType();
                        emAccountEventType.setTxName(BillingConstants.EVENT_TYPE_SUBCRITPTION);
                        emAccountEventType.setTxDescription("Event of Subscription");
                        accountEventTypeDAO.saveOrUpdate(emAccountEventType);
                    }
                    event.setEmAccountEventType(emAccountEventType);
                    event.setTcType(BillingConstants.EVENT_DEBIT);
                    event.setTsTimeStamp(new Date());
                    event.setTxBslaId(bsla.getUuid().getValue());
                    event.setPayer(payer);
                    long productOffer = utils.getProductOfferIdFromSLA(bsla);
                    if(productOffer == -1) return false;
                    EmSpProductsOffer offer = productOfferService.getProductOfferById(productOffer);
                    String description =offer.getEmSpProducts().getNuProductid().toString()+";"+offer.getEmSpProducts().getTxProductname()+";";
                    description += offer.getNuIdproductoffering().toString()+";"+offer.getTxName()+";";
                    description += business.$one_time_charge.substring(business.$one_time_charge.lastIndexOf("#")+1);
                    event.setTxDescription(description);
                    this.billingAccountService.bill(event);
                    return true;
                }else return false;
            }
            return true;
        }catch(Exception e){
            log.error("Error: "+e.toString());
            e.printStackTrace();
            return false;
        }
    }

    
    /**
     * Stop the the charging of  a Customer. 
     * @param bslaId
     * @param Code --> Code with termination reason, for instant, normal,malfunction, etc..
     * @return
     */
    public boolean stopChargingBSLA(SLA bsla,String Code){
        log.debug("Into startChargingBSLA method");
        try{         
            Utils utils = new Utils();
            CONST charge = utils.calculateFinalCharge(bsla);
            if(charge !=null){
                log.debug("There is something to pay: " +charge.getValue());
                Long partyId = utils.getCustomerIdFromSLA(bsla);
                if(partyId !=null){
                    //EmPartyBank emPartyBank = partyBankDAO.getPartyBankActiveByParty(partyId);
                    EmParty payer = partyService.getPartyById(partyId);
                    EmCurrencies currency = currencyService.getCurrencyByName(charge.getDatatype().getValue().
                            substring(charge.getDatatype().getValue().lastIndexOf("#")+1));
                    EmAccountEvent event = new EmAccountEvent();
                    //event.setEmPartyBank(emPartyBank);
                    event.setCurrency(currency);
                    event.setNuAmount((new BigDecimal(charge.getValue())).negate());
                    EmAccountEventType emAccountEventType = accountEventTypeDAO.getByName(BillingConstants.EVENT_TYPE_PAYMENT);
                    if(emAccountEventType == null){
                        //create subcriptionEvent
                        log.info("paymentEventType does not exist. Create it");
                        emAccountEventType = new EmAccountEventType();
                        emAccountEventType.setTxName(BillingConstants.EVENT_TYPE_PAYMENT);
                        emAccountEventType.setTxDescription("Payment Event");
                        accountEventTypeDAO.saveOrUpdate(emAccountEventType);
                    }
                    event.setEmAccountEventType(emAccountEventType);
                    event.setTcType(BillingConstants.EVENT_DEBIT);
                    event.setTsTimeStamp(new Date());
                    event.setTxBslaId(bsla.getUuid().getValue());
                    event.setPayer(payer);
                    event.setTxDescription(Code);
                    this.billingAccountService.bill(event);
                    return true;
                }else return false;
            }
            return true;
        }catch(Exception e){
            log.error("Error: "+e.toString());
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Start the periodic charging for the customers
     * @param date
     * @return
     */
    public boolean periodicCharging(Date date){
        log.debug("Into periodicCharging method");
        try{     
            List<EmCustomersProducts> customerProducts = this.customerProductService.getCustomerProductsActiveByDate(date);
            log.debug("CustomerProduct Size:" + customerProducts.size());
            Utils utils = new Utils();
            if(customerProducts !=null && customerProducts.size()>0){
                EmAccountEventType emAccountEventType = accountEventTypeDAO.getByName(BillingConstants.EVENT_TYPE_PAYMENT);
                if(emAccountEventType == null){
                    //create subcriptionEvent
                    log.info("paymentEventType does not exist. Create it");
                    emAccountEventType = new EmAccountEventType();
                    emAccountEventType.setTxName(BillingConstants.EVENT_TYPE_PAYMENT);
                    emAccountEventType.setTxDescription("Payment Event");
                    accountEventTypeDAO.saveOrUpdate(emAccountEventType);
                }
                for(EmCustomersProducts contract:customerProducts){                    
                    CONST charge = utils.calculatePeriodicCharge(contract);                    
                    if(charge !=null){
                        log.debug("There is montly Payment: " + charge.getValue());
                        Long partyId = contract.getEmParty().getNuPartyid();
                        if(partyId != null){
                            //EmPartyBank emPartyBank = partyBankDAO.getPartyBankActiveByParty(partyId);
                            EmParty payer = partyService.getPartyById(partyId);
                            EmCurrencies currency = currencyService.getCurrencyByName(charge.getDatatype().getValue().
                                    substring(charge.getDatatype().getValue().lastIndexOf("#")+1));
                            EmAccountEvent event = new EmAccountEvent();
                            //event.setEmPartyBank(emPartyBank);
                            event.setCurrency(currency);
                            event.setNuAmount((new BigDecimal(charge.getValue())).negate());
                            event.setEmAccountEventType(emAccountEventType);
                            event.setTcType(BillingConstants.EVENT_DEBIT);
                            event.setTsTimeStamp(new Date());
                            event.setTxBslaId(contract.getTxBslaid());       
                            EmSpProductsOffer offer = productOfferService.getProductOfferById(contract.getEmSpProductsOffer().getNuIdproductoffering());
                            String description =offer.getEmSpProducts().getNuProductid().toString()+";"+offer.getEmSpProducts().getTxProductname();
                            description += offer.getNuIdproductoffering().toString()+";"+offer.getTxName();
                            description += business.$per_month.substring(business.$per_month.lastIndexOf("#")+1);
                            event.setTxDescription(description);
                            event.setPayer(payer);
                            this.billingAccountService.bill(event);
                        }
                    }
                }
            }
            log.debug("Charging to bank account afther billing the monthly Cost");
            this.billingAccountService.billToCustomers();
            log.debug("End periodicCharging method");
            return true;
        }catch(Exception e){
            log.error("Error: "+e.toString());
            e.printStackTrace();
            return false;
        }
    }    
    
    /**
     * Charge an event to an account 
     * @param slaid
     * @param Event
     * @TODO  Define the data needed to be received to do the charing
     * @return
     */
    public boolean eventCharging(String slaid,String Event){
        log.debug("Into eventCharging method");
        
        return false;
    }
    
    /**
     * Charging of penalties
     * @param slaId -->ID of SLA that suffers a penalty
     * @param partyType --> Customer,provider
     * @param Penalty --> penaltyData such as amount?
     * @param Description -->Description of the event
     * @return
     */
    public boolean penaltyCharging (String slaId, String partyType,String Penalty, String Description){
        log.debug("Into penaltyCharging method");
        return false;
    }
    
    /**
     * Obtain Charging information about the customer
     * @param criteriaParameters -->this data could be for instant, slaid,productid,
     *        productOfferID, partyid
     * @TODO define this information or the way to do it
     * @return
     */
    public EmPartyBank getChargingInformation(Long partyID,Date periodStart, Date periodEnd)throws Exception{
        log.debug("Into getChargingInformation method");
        log.debug("PArtyID: "+ partyID);
        return this.billingAccountService.getAccountStatement(partyID, periodStart, periodEnd);
    }
    
}
