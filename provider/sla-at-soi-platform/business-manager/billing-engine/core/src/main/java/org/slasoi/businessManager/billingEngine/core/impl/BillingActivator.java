/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.billingEngine.core.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slasoi.businessManager.billingEngine.core.BillingEngine;

public class BillingActivator implements BundleActivator {
    /** Bundle context variable. **/
    private BundleContext osgiContext = null;
    /** OSGI Service tracker variable. **/
    private ServiceTracker tracker = null;
    private BillingEngine billingEngine = null;

    /**
     * Start method.
     @param context bundle context
     @throws Exception exception
     */
    public final void start(final BundleContext context) throws Exception {

        CommandListener cl = new CommandListener();
        context.registerService(cl.getClass().getName(),cl, null);
        this.osgiContext = context;      
    }

    /**
     * start startPeriodicCharging method.
     * @throws Exception exception
     */
    public final void startPeriodicCharging(Date date) throws Exception {
       System.out.println("Into startPeridicCharging method: "+ date);
       try{
           tracker = new ServiceTracker(osgiContext, BillingEngine.class
                   .getName(), null);
           tracker.open();
           billingEngine = (BillingEngine) tracker.waitForService(2000);
           if (billingEngine != null) {
               System.out.println("BillingEngine injected successfully");
           }         
           boolean result = this.billingEngine.periodicCharging(date);         
           System.out.println("Reesult of the process: "+ result);
       }catch(Exception e){
           e.printStackTrace();
       }
    }    

 /**
  * 
  */
    public final void stop(final BundleContext arg0) throws Exception {
        System.out.println("****************"
                + "Billing BUNDLE STOPPED ******************");
        tracker.close();
    }

    /**
     * Billing Listener for command line
     */
    public class CommandListener {
        /**
         * run.
         *
         * @param params table
         */
        public final void run(final Hashtable<String, String> params) {
            System.out.println("Received command by command Line");
            try {
                String dateString = value(params, "periodicCharging");
                if(dateString != null){
                    SimpleDateFormat format =  new SimpleDateFormat("yyyy/MM/dd");
                    Date date = format.parse(dateString);
                    startPeriodicCharging(date);
                }else{
                    System.out.println("Command doesn't exit or bad parameters");
                    help();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Help information.
         */
        public final void help() {
            System.out.println("Usage:  osgi>invoke #bundleid run"
                    + " periodicCharging=#date [#date = yyyy/MM/dd");
        }

        /**
         * Return value from hash table.
         *
         * @param params table
         * @param key string
         * @return string date
         */
        protected final String value(final Hashtable<String, String> params,
                final String key) {
            String v = params.get(key);
            if (v != null) {
                return v;
            }

            return null;
        }
    }
}
