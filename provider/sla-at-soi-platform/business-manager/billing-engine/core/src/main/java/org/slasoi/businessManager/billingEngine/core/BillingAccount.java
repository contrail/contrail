/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.core;

import java.math.BigDecimal;
import java.util.Date;

import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.billingEngine.util.InsufficientCreditException;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.service.PartyManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;



public interface BillingAccount {
    
 /**
  * Set the party service needed.
  * @param partyService
  */
    //public void setPartyService(PartyManager partyService);
    
    
 /**
  * Create a new Bank Account   
  * @param account
  * @return
  * @throws RemoteException
  */
    @Transactional(propagation = Propagation.REQUIRED)
    public Long createAccount(EmPartyBank account)throws Exception;   
    
    
    /**
     * Give a summary of an account during a period
     * @param partyID
     * @param periodStart
     * @param periodEnd
     * @return EmPartyBank: contains all the info related to an account and the corresponding events.
     * @throws Exception
     */
   @Transactional(propagation = Propagation.REQUIRED)
   public EmPartyBank getAccountStatement(Long partyID,Date periodStart, Date periodEnd) throws Exception;
   
   /**
    * This method set the active account to a party     
    * @param partyID
    * @param bankAcountId
    * @throws Exception
    */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void  setActiveAccount(Long partyID,Long bankAcountId) throws Exception;

   /**
    * Set the credit Limit to a customer      
    * @param partyid
    * @param creditLimit
    * @throws Exception
    */
    public void setCreditLimit(Long partyid,BigDecimal creditLimit) throws Exception;
      
    
    /**
     * This method search into the database the parties with negative balance and make the corresponding bill      
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void billToCustomers()throws Exception;
   
   /**
    * Close an account
    * @throws RemoteException If the account cannot be closed
    */
   @Transactional(propagation = Propagation.REQUIRED)
   public void closeAccount(Long PartyId) throws Exception;

/**
 * Charge an event to the corresponding account
 * @param event
 * @param currency
 * @throws Exception
 */
   @Transactional(propagation = Propagation.REQUIRED)
   public void bill(EmAccountEvent event)
           throws Exception;


   /** Test whether we can withdraw this amount now.
    * @param partyId
    * @param amount amount to test
    * @param currency units for amount
    * @throws InsufficientCreditException if there are insufficient funds
    * @throws Exception for other errors
    */
   @Transactional(propagation = Propagation.REQUIRED)
   public void checkCreditAvailable(Long partyId,BigDecimal amount, EmCurrencies currency) throws InsufficientCreditException, Exception;

}
