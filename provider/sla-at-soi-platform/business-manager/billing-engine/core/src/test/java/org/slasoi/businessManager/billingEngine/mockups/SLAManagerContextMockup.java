/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/core/context/src/main/java/org/slasoi/gslam/core/context/SLAManagerContext.java $
 */

package org.slasoi.businessManager.billingEngine.mockups;

import java.util.Hashtable;

import org.slasoi.gslam.core.authorization.Authorization;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.ServiceManagerRegistry;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;

/**
 * A SLAManagerContext defines the context of any SLA-manager. This context gives the visibility to each component about
 * other entities inside the SLA-manager.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLAManagerContextMockup implements SLAManagerContext{
  

    protected SLARegistry slaRegistry;
    
    public static class SLAManagerContextException extends Exception {
        public SLAManagerContextException(String cause) {
            super(cause);
        }
    }

    public Authorization getAuthorization()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getEPR() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getGroupID() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public IMonitoringManager getMonitorManager()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public PlanningOptimization getPlanningOptimization()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProtocolEngine getProtocolEngine()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisioningAdjustment getProvisioningAdjustment()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getSLAManagerID() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLARegistry getSLARegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        
        return this.slaRegistry;
    
    }

    public SLATemplateRegistry getSLATemplateRegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        
        return null;
    }

    public ServiceAdvertisement getServiceAdvertisement()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ServiceManagerRegistry getServiceManagerRegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public GenericSLAManagerUtils getSlamUtils() {
        // TODO Auto-generated method stub
        return null;
    }

    public Hashtable<SyntaxConverterType, ISyntaxConverter> getSyntaxConverters()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getWSPrefix() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setAuthorization(Authorization arg0) {
        // TODO Auto-generated method stub
        
    }

    public void setGroupID(String arg0) {
        // TODO Auto-generated method stub
        
    }

    public void setPlanningOptimization(PlanningOptimization arg0) {
        // TODO Auto-generated method stub
        
    }

    public void setProvisioningAdjustment(ProvisioningAdjustment arg0) {
        // TODO Auto-generated method stub
        
    }

    public void setSlamUtils(GenericSLAManagerUtils arg0) {
        // TODO Auto-generated method stub
        
    }

    public SLARegistry getSlaRegistry() {
        return slaRegistry;
    }

    public void setSlaRegistry(SLARegistry slaRegistry) {
        this.slaRegistry = slaRegistry;
    }

    public Hashtable<String, String> getProperties()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setProperties(Hashtable<String, String> arg0)
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        
    }
}
