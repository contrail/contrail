/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 791 $
 * @lastrevision   $Date: 2011-02-23 11:32:58 +0100 (Wed, 23 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/billing-engine/src/test/java/org/slasoi/businessManager/billingEngine/BillingTASTest.java $
 */

package org.slasoi.businessManager.billingEngine;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.billingEngine.core.BillingAccount;
import org.slasoi.businessManager.billingEngine.dao.AccountEventTypeDAO;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.billing.EmAccountEventType;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.billingEngine.util.InsufficientCreditException;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.service.PartyManager;
import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.springframework.transaction.annotation.Transactional;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
@Transactional
public class BillingTASTest extends AbstractTransactionalSpringContextTests{ 
    private static final Logger log = Logger.getLogger(BillingTASTest.class);
    @Resource
    private PartyManager partyManager;
    @Resource
    private BillingAccount billingAccountService;
    @Resource
    private AccountEventTypeDAO  accountEventTypeDAO;
        
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-billing-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BillingTASTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BillingTASTest.class );
    }
    
    private EmPartyBank generateAccount(){
        EmPartyBank account = new EmPartyBank();
        EmParty emParty = partyManager.getPartyById(new Long(1));
        if(emParty !=null ){
            emParty.setNuBalance(new BigDecimal(0));
            emParty.setNuCreditLimit(new BigDecimal(0));
        }else{            
            emParty = new EmParty();
            emParty.setNuPartyid(new Long(1));
            emParty.setNuBalance(new BigDecimal(0));
            emParty.setNuCreditLimit(new BigDecimal(0));
            EmCurrencies emCurrencies = new EmCurrencies();
            emCurrencies.setNuCurrencyId(new Long(1));
            emParty.setEmCurrencies(emCurrencies);
        }
        account.setEmParty(emParty);
        account.setTxAccountNumber("txAccountNumber");
        account.setNuIssueNumber(new Long(1));
        account.setTxAccountName("txAccountName");
        account.setDtUpdated(new Date());
        account.setTcDefaultYn('Y');
        account.setTxSwiftCode("txSwiftCode");
        return account;
    }
    
    /**
     * Test insert an account into the database
     */
    public void testInsertAccount()
    {      System.out.println("testInsertAccount");
        try{
            EmPartyBank account = this.generateAccount();
            billingAccountService.createAccount(account);
            System.out.println("After the test id: " +account.getNuBankId());
        }catch (Exception e){
            log.error("Error: " +e.toString());
            e.printStackTrace();
        }        

    }
    
    /**
     * Test insert an account into the database and several events and retrieve them.
     */
    public void testGetAccountStatement()
    {  System.out.println("testGetAccountStatement");
        try{
            //billingAccountService.setPartyService(partyManager);
            EmAccountEventType object = new EmAccountEventType();
            object.setTxName("name");
            object.setTxDescription("txDescription");
            accountEventTypeDAO.save(object);
            EmPartyBank account = this.generateAccount();
            Set<EmAccountEvent> emAccountEvents = new HashSet <EmAccountEvent>();
            EmAccountEvent e = new EmAccountEvent();
            e.setPayer(account.getEmParty());
            e.setEmAccountEventType(object);
            e.setNuAmount(new BigDecimal(1));
            e.setTsTimeStamp(new Date());
            e.setTcType(BillingConstants.EVENT_DEBIT);
            EmCurrencies currency = new EmCurrencies();
            currency.setNuCurrencyId(new Long(1));
            currency.setTxName("Euro");
            e.setCurrency(currency);
            e.setEmPartyBank(account);
            emAccountEvents.add(e);
            EmAccountEvent e2 = new EmAccountEvent();
            e2.setEmAccountEventType(object);
            e2.setTcType(BillingConstants.EVENT_DEBIT);
            e2.setNuAmount(new BigDecimal(1));
            e2.setTsTimeStamp(new Date());
            e2.setCurrency(currency);
            e2.setPayer(account.getEmParty());
            e2.setEmPartyBank(account);
            emAccountEvents.add(e2);
            account.setEmAccountEvents(emAccountEvents);
            System.out.println("Inserting account and events");
            billingAccountService.createAccount(account);
            Date init = new Date();
            init.setHours(init.getHours()-1);
            Date end = new Date();
            end.setHours(end.getHours()+1);
            System.out.println("Retrieving events by date");
            EmPartyBank accountFinal = billingAccountService.
                getAccountStatement(account.getEmParty().getNuPartyid(), init, end);
            System.out.println("Events Retrieved: " + accountFinal.getEmAccountEvents());
            System.out.println("After the test");
        }catch (Exception e){
            System.out.println("Error: " +e.toString());
            e.printStackTrace();
        }        
    }
    
    
    /**
     * Test check to bill an event
     */
    public void testBillEvent()
    {  System.out.println("testBillEvent");
        try{
            //billingAccountService.setPartyService(partyManager);
            EmAccountEventType object = new EmAccountEventType();
            object.setTxName("name");
            object.setTxDescription("txDescription");
            accountEventTypeDAO.save(object);
            EmPartyBank account = this.generateAccount();
            System.out.println("Creating Account");
            billingAccountService.createAccount(account);
            //bill and event
            EmAccountEvent e = new EmAccountEvent();
            e.setPayer(account.getEmParty());
            e.setEmAccountEventType(object);
            e.setNuAmount(new BigDecimal(10));
            e.setTsTimeStamp(new Date());
            e.setTcType(BillingConstants.EVENT_DEBIT);
            EmCurrencies currency = new EmCurrencies();
            currency.setNuCurrencyId(new Long(1));
            currency.setTxName("Euro");
            e.setCurrency(currency);
            e.setEmPartyBank(account);
            System.out.println("Set intial limit under what we want to bill");
            billingAccountService.setCreditLimit(new Long(1), new BigDecimal(1));
            try{
                billingAccountService.bill(e);
            }catch(InsufficientCreditException ex){
                System.out.println("InsufficientCreditException");
            }            
            System.out.println("Set new Limit");
            billingAccountService.setCreditLimit(new Long(1), new BigDecimal(10000));
            billingAccountService.bill(e);           
            System.out.println("After the test");
        }catch (Exception e){
            System.out.println("Error: " +e.toString());
        }
    }    
    
    /**
     * Test insert an account into the database
     */
    public void testSetActiveAccount()
    {      System.out.println("testSetActiveAccount");
        try{
            EmPartyBank account = this.generateAccount();
            billingAccountService.createAccount(account);
            System.out.println("After the test id: " +account.getNuBankId());
            EmPartyBank account2 = this.generateAccount();
            account2.setTcDefaultYn('N');
            System.out.println("Insert second account Account");
            billingAccountService.createAccount(account2);
            System.out.println("Insert second account Account");
            System.out.println("Before setActive Account: Account A: "+ account.getTcDefaultYn()+" AccountB: " +account2.getTcDefaultYn());
            billingAccountService.setActiveAccount(account2.getEmParty().getNuPartyid(), account2.getNuBankId());
            System.out.println("Account A: "+ account.getTcDefaultYn()+" AccountB: " +account2.getTcDefaultYn());
        }catch (Exception e){
            log.error("Error: " +e.toString());
            e.printStackTrace();
        }        

    }
    /**
     * Test to probe the 
     * @throws Exception
     */
    public void testBillToCustomers()throws Exception{
        System.out.println("testBillToCustomers");
        try{
            EmAccountEventType object = new EmAccountEventType();
            object.setTxName("name");
            object.setTxDescription("txDescription");
            accountEventTypeDAO.save(object);
            EmPartyBank account = this.generateAccount();
            Set<EmAccountEvent> emAccountEvents = new HashSet <EmAccountEvent>();
            EmAccountEvent e = new EmAccountEvent();
            e.setPayer(account.getEmParty());
            e.setEmAccountEventType(object);
            e.setNuAmount(new BigDecimal(1));
            e.setTsTimeStamp(new Date());
            e.setTcType(BillingConstants.EVENT_DEBIT);
            EmCurrencies currency = new EmCurrencies();
            currency.setNuCurrencyId(new Long(1));
            currency.setTxName("Euro");
            e.setCurrency(currency);
            e.setEmPartyBank(account);
            emAccountEvents.add(e);
            EmAccountEvent e2 = new EmAccountEvent();
            e2.setEmAccountEventType(object);
            e2.setTcType(BillingConstants.EVENT_DEBIT);
            e2.setNuAmount(new BigDecimal(1));
            e2.setTsTimeStamp(new Date());
            e2.setCurrency(currency);
            e2.setPayer(account.getEmParty());
            e2.setEmPartyBank(account);
            emAccountEvents.add(e2);
            account.setEmAccountEvents(emAccountEvents);
            System.out.println("Inserting account and events");
            billingAccountService.createAccount(account);
            System.out.println("Set negative Balance");
            EmParty party = account.getEmParty();
            party.setNuBalance(new BigDecimal(-10));
            partyManager.saveOrUpdate(party);
            // charging
            System.out.println("Events before payment: " +account.getEmAccountEvents().size());
            this.billingAccountService.billToCustomers();  
            Date init = new Date();
            init.setHours(init.getHours()-1);
            Date end = new Date();
            end.setHours(end.getHours()+1);
            EmPartyBank accountFinal = billingAccountService.
                getAccountStatement(account.getEmParty().getNuPartyid(), init, end);
            System.out.println("Balance after payment: " +party.getNuBalance().longValue());
            System.out.println("Events after payment: " +account.getEmAccountEvents().size());
            System.out.println("After the test");
        }catch (Exception e){
            System.out.println("Error: " +e.toString());
            e.printStackTrace();
        }        
    }  
    
       
}
