/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.billingEngine;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.billingEngine.core.BillingAccount;
import org.slasoi.businessManager.billingEngine.core.impl.BillingEngineImpl;
import org.slasoi.businessManager.billingEngine.dao.AccountEventTypeDAO;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.billing.EmAccountEventType;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.service.CurrencyService;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.PriceModification;
import org.slasoi.slamodel.sla.business.Product;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.springframework.test.AbstractTransactionalSpringContextTests;

public class BillingEngineTest extends AbstractTransactionalSpringContextTests{ 
    private static final Logger log = Logger.getLogger(BillingEngineTest.class);
    @Resource
    private PartyManager partyManager;
    @Resource
    private BillingAccount billingAccountService;
    @Resource
    private AccountEventTypeDAO  accountEventTypeDAO;    
    @Resource
    private CurrencyService currencyService; 
    @Resource
    private CustomerProductManager customerProductService;
    @Resource
    private BillingEngineImpl billingEngineService;    
    @Resource
    private ProductOfferManager productOfferService;
        
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-billing-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BillingEngineTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BillingEngineTest.class );
    }
    
    private EmPartyBank generateAccount(){
        
        EmPartyBank account = new EmPartyBank();
        EmParty emParty = partyManager.getPartyById(new Long(1));
        if(emParty !=null ){
            emParty.setNuBalance(new BigDecimal(0));
            emParty.setNuCreditLimit(new BigDecimal(0));
        }else{            
            emParty = new EmParty();
            emParty.setNuPartyid(new Long(1));
            emParty.setNuBalance(new BigDecimal(0));
            emParty.setNuCreditLimit(new BigDecimal(0));
            EmCurrencies emCurrencies = new EmCurrencies();
            emCurrencies.setNuCurrencyId(new Long(1));
            emParty.setEmCurrencies(emCurrencies);
        }
        account.setEmParty(emParty);
        account.setTxAccountNumber("txAccountNumber");
        account.setNuIssueNumber(new Long(1));
        account.setTxAccountName("txAccountName");
        account.setDtUpdated(new Date());
        account.setTcDefaultYn('Y');
        account.setTxSwiftCode("txSwiftCode");
        return account;
    }
    
    
    /**
     * Test GetAccountStatement
     */
    public void testGetAccountStatement()
    {  System.out.println("testGetAccountStatement");
        try{
            //billingAccountService.setPartyService(partyManager);
            EmAccountEventType object = new EmAccountEventType();
            object.setTxName("name");
            object.setTxDescription("txDescription agus");
            accountEventTypeDAO.save(object);
            EmPartyBank account = this.generateAccount();
            Set<EmAccountEvent> emAccountEvents = new HashSet <EmAccountEvent>();
            EmAccountEvent e = new EmAccountEvent();
            e.setPayer(account.getEmParty());
            e.setEmAccountEventType(object);
            e.setNuAmount(new BigDecimal(1));
            e.setTsTimeStamp(new Date());
            e.setTcType(BillingConstants.EVENT_DEBIT);
            EmCurrencies currency = new EmCurrencies();
            currency.setNuCurrencyId(new Long(1));
            currency.setTxName("Euro");
            e.setCurrency(currency);
            e.setEmPartyBank(account);
            emAccountEvents.add(e);
            EmAccountEvent e2 = new EmAccountEvent();
            e2.setEmAccountEventType(object);
            e2.setTcType(BillingConstants.EVENT_DEBIT);
            e2.setNuAmount(new BigDecimal(1));
            e2.setTsTimeStamp(new Date());
            e2.setCurrency(currency);
            e2.setPayer(account.getEmParty());
            e2.setEmPartyBank(account);
            emAccountEvents.add(e2);
            account.setEmAccountEvents(emAccountEvents);
            System.out.println("Inserting account and events");
            billingAccountService.createAccount(account);
            Date init = new Date();
            init.setHours(init.getHours()-1);
            Date end = new Date();
            end.setHours(end.getHours()+1);
            System.out.println("Retrieving events by date");
            //EmPartyBank accountFinal = billingAccountService.
            //    getAccountStatement(account.getEmParty().getNuPartyid(), init, end);
            EmPartyBank accountFinal = billingEngineService
            .getChargingInformation(
                    account.getEmParty().getNuPartyid(), init, end);            
            System.out.println("Events Retrieved: " + accountFinal.getEmAccountEvents());
            System.out.println("After the test");
        }catch (Exception e){
            System.out.println("Error: " +e.toString());
            e.printStackTrace();
        }        
    }
    
    /**
     * Test if it is possible a charge to a customer
     */
    public void testcheckChargingCustomer(){
        System.out.println("testcheckChargingCustomer");
        partyManager.getPartyById(new Long(1));
        EmCurrencies currency = new EmCurrencies();
        currency.setNuCurrencyId(new Long(1));
        System.out.println("Charging -2 --> Amount must be negative in charges");
        System.out.println ("Result: "+ billingEngineService.checkChargingCustomer(new Long(1), new BigDecimal(-2),currency ));
        
    }
    /**
     * Test testStartChargingBSLA
     */
    public void  testStartChargingBSLA(){
             System.out.println("inside testStartChargingBSLA");
            SLA bsla = this.generateSLA();   ;
           /* billingEngineService.setPartyService(partyManager);
            billingEngineService.setCustomerProductService(customerProductService);
            billingEngineService.setCurrencyService(currencyService);*/
            billingEngineService.startChargingBSLA(bsla);       
            System.out.println("End Test");
    }
    /**
     * Test testStopChargingBSLA
     */
    public void  testStopChargingBSLA(){
        try{
            System.out.println("inside testStopChargingBSLA");
            SLA bsla = this.generateSLA();   
            EmCustomersProducts customersProducts =new EmCustomersProducts();
            customersProducts.setDtDateBegin(new Date());
            customersProducts.setDtDateEnd(new Date());
            customersProducts.setDtInsertDate(new Date());
            customersProducts.setTxBslaid(bsla.getUuid().toString());
            EmParty  emParty= new EmParty();
            emParty.setNuPartyid(new Long(1));
            customersProducts.setEmParty(emParty);
            EmSpProductsOffer emSpProductsOffer= new EmSpProductsOffer();
            emSpProductsOffer.setNuIdproductoffering(new Long(1));
            customersProducts.setState("OBSERVED");
            customersProducts.setEmSpProductsOffer(emSpProductsOffer);
            this.customerProductService.saveCustomer(customersProducts);
            /*billingEngineService.setPartyService(partyManager);
            billingEngineService.setCustomerProductService(customerProductService);
            billingEngineService.setCurrencyService(currencyService);*/
            billingEngineService.stopChargingBSLA(bsla, "End execution");       
            System.out.println("End Test");
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    /**
     * Test testStopChargingBSLA
     */
    public void  testPeriodicCharging(){
        try{
            System.out.println("inside testPeriodicCharging");
            EmCustomersProducts customersProducts =new EmCustomersProducts();
            customersProducts.setDtDateBegin(new Date());
            Date dateEnd = new Date();
            dateEnd.setMonth(dateEnd.getMonth()+1);
            customersProducts.setDtDateEnd(dateEnd);
            customersProducts.setDtInsertDate(new Date());
            customersProducts.setTxBslaid("TXBSLAID");
            EmParty  emParty= new EmParty();
            emParty.setNuPartyid(new Long(1));
            customersProducts.setEmParty(emParty);
            EmSpProductsOffer emSpProductsOffer= new EmSpProductsOffer();
            emSpProductsOffer.setNuIdproductoffering(new Long(1));
            customersProducts.setState("OBSERVED");
            customersProducts.setEmSpProductsOffer(emSpProductsOffer);
            this.customerProductService.saveCustomer(customersProducts);
            /*billingEngineService.setPartyService(partyManager);
            billingEngineService.setCustomerProductService(customerProductService);
            billingEngineService.setCurrencyService(currencyService);*/
            Date date = new Date();
            date.setDate(28);           
            billingEngineService.periodicCharging(date);   
            EmParty party1 = this.partyManager.getPartyById(new Long(1));
            if(party1!=null){
                System.out.println("Balance: "+party1.getNuBalance());
            }
            EmParty party2 = this.partyManager.getPartyById(new Long(39));
            if(party2!=null){
                System.out.println("Balance: "+party2.getNuBalance());
            }
            System.out.println("End Test testPeriodicCharging");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
    /**
     * Generate SLA test
     */   
    private SLA generateSLA(){
            System.out.println("inside generateSLA");
            SLA slabuild = new SLA();
            try{
                    Party [] parties = new Party[1];
                    Party party = new Party(new ID("1"), sla.customer);
                    parties[0]= party;
                    slabuild.setParties(parties);
                    EmSpProductsOffer offer = productOfferService.getProductOfferById(new Long(1));
                    ProductOfferingPrice offerSLA =this.convertToProductOfferingPrice(offer);
                    if(offerSLA==null){
                        log.error("An error happends obtaining the template in SLAModel");
                        return null;
                    }
                    AgreementTerm[]terms = new AgreementTerm[1];
                    AgreementTerm term = new AgreementTerm();
                    Guaranteed[] guarantees = new Guaranteed[1];  
                    Expr[] params = new Expr[1];
                    params [0]= new ID("PriceSpecificationAction");
                    EventExpr pre = new EventExpr(core.violated, params);
                    Action action =new Action(new ID("PriceSpecificationAction"),sla.provider,sla.mandatory, pre, offerSLA);
                    guarantees[0]= action;
                    term.setGuarantees(guarantees);
                    terms[0]= term;
                    slabuild.setAgreementTerms(terms);
                    slabuild.setUuid(new UUID("SLAID"));                   
            }catch(Exception e){
                log.equals("Error:"+e.toString());
                e.printStackTrace();
            }  
            return slabuild;
    }
    
    /**
     * This method convert from product offer business model to prodcutOfferSLAModel   
     * @param productOfferPrice
     * @return
     */
      private ProductOfferingPrice convertToProductOfferingPrice(EmSpProductsOffer productOfferPrice){
          log.info("Inside convertToProductOfferingPrice method");
          try{
              //ID
              ID id= new ID(productOfferPrice.getNuIdproductoffering().toString());
              //name
              String name = productOfferPrice.getTxName();
              //validFor
              GregorianCalendar from = new GregorianCalendar();
              from.setTime(productOfferPrice.getDtValidFrom());
              TIME validFrom =   new TIME(from);
              GregorianCalendar until = new GregorianCalendar();
              until.setTime(productOfferPrice.getDtValidTo());        
              TIME validUntil = new TIME(until);

              //billingFrecuency
              /**TODO
               * check the billingFrecuency??
               */
              STND billingFrequency = new STND(business.$base+productOfferPrice.getEmBillingFrecuency().getTxName());
              //product
              Product product = new Product(new ID(productOfferPrice.getNuIdproductoffering().toString())
                                            , productOfferPrice.getTxName());
              //Components offeringPrices
              Set<EmComponentPrice> components = productOfferPrice.getEmComponentPrices();    
              ComponentProductOfferingPrice[] cpops = null;          
              if(components!= null &&components.size()>0){
                  Iterator <EmComponentPrice> iterator = components.iterator(); 
                  cpops = new ComponentProductOfferingPrice[components.size()];               
                  ComponentProductOfferingPrice componentPrice = null;               
                  int index = 0;              
                  while(iterator.hasNext()){
                      EmComponentPrice component = new EmComponentPrice();
                      component = iterator.next();
                      //id
                      ID idCompo = new ID(component.getTxName());
                      //priceType
                      /**TODO
                       * verify priceType
                       */
                      STND priceType = new STND(business.$base+component.getEmPriceType().getTxName());
                      //Price
                      /**TODO
                       * verify Price
                       */
                      CONST price = new CONST(component.getNuPrice().toString(),
                              new STND(units.$base+component.getEmCurrencies().getTxName()));
                      //Quantity
                      CONST quantity = new CONST(component.getNuQuantity().toString(),null);
                      //Price Modifications
                      Set<EmPriceVariation>  variations= component.getEmPriceVariations();
                      if(variations!=null && variations.size()>0){
                          log.debug("component with modifications:" + component.getTxDescription());
                          Iterator<EmPriceVariation> it= variations.iterator();
                          PriceModification[] pms = new PriceModification[variations.size()];
                          PriceModification modification;                      
                          int indexModify = 0;
                          while(it.hasNext()){
                              EmPriceVariation variation = new EmPriceVariation();
                              variation = it.next();
                              //from here it is easy to obtain the type and unit of the variation
                              //increment,decrement
                              STND type = null;
                              if(Constants.MODIFICATION_INCREMENT.equals(variation.getEmPriceVariationType().getTcModificationType())){
                                  type = business.increment;
                              }else  if(Constants.MODIFICATION_DECREMENT.equals(variation.getEmPriceVariationType().getTcModificationType())){
                                  type = business.discount;
                              }
                              //amount,percentage
                              STND unit = null;
                              if(Constants.UNIT_AMOUNT.equalsIgnoreCase(variation.getEmPriceVariationType().getTcUnitType())){
                                  unit = price.getDatatype();
                              }else  if(Constants.UNIT_PERCENTAGE.equalsIgnoreCase(variation.getEmPriceVariationType().getTcUnitType())){
                                  unit = units.percentage;
                              }
                              
                              modification = new PriceModification(type, 
                                                       new CONST (variation.getNuValue().toString(),unit));
                              pms[indexModify] = modification;
                              indexModify ++;
                          } 
                          componentPrice = new ComponentProductOfferingPrice(idCompo,priceType,price,quantity,pms);
                      }else{
                          componentPrice = new ComponentProductOfferingPrice(idCompo,priceType,price,quantity,null);
                      }
                      cpops[index] = componentPrice;
                      index++;
                  }
                  
              }
              
              return new ProductOfferingPrice(id,name,validFrom,validUntil, billingFrequency,product, cpops);             
              
          }catch(Exception e){
              log.error("Error:"+e.getMessage());
              e.printStackTrace();
          }
          return null;
      }

}
