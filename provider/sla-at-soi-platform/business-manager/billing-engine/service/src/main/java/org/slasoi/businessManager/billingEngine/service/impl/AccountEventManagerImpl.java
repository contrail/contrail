/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.service.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger; 

import org.slasoi.businessManager.billingEngine.dao.AccountEventDAO;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.billingEngine.service.AccountEventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="AccountEventService")
public class AccountEventManagerImpl implements AccountEventManager{
        
    static Logger log = Logger.getLogger(AccountEventManagerImpl.class);
    
    @Autowired
    private AccountEventDAO accountEventDAO;
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.service.impl.CharacteristicManager#getBillingFrecuencies()
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public  List<EmAccountEvent> getAccountEvents(){
        log.info("getAccountEvent()");
        return accountEventDAO.getList();
    }

    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.service.impl.CharacteristicManager#getCharacteristicById(java.lang.Long)
     */
    @Transactional(propagation=Propagation.REQUIRED)
     public  EmAccountEvent getEmAccountEventById(Long id){
        log.info("getEmAccountEventById():Id:"+id);
        return accountEventDAO.load(id);
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.service.impl.CharacteristicManager#saveCharacteristic(org.slasoi.businessManager.common.model.EmBillingFrecuency)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public  void saveAccountEvent(EmAccountEvent object){
        if(object.getNuEventId()==0)
            object.setNuEventId(null);
        log.info("saveAccountEvent()");
        accountEventDAO.saveOrUpdate(object);
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.service.impl.CharacteristicManager#deleteBillingFrecuencies(java.util.List)
     */
    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
     public  void deleteAccountEvents(List<Long> ids){

        log.info("deleteAccountTypes()");
        
        //Delete all Characteristic of the list Ids
        Iterator<Long> itIds = ids.iterator();
        while(itIds.hasNext()){
            EmAccountEvent cht = accountEventDAO.load(itIds.next());
            accountEventDAO.delete(cht);
        }
    }

    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public List<EmAccountEvent> getEmAccountEventsBySlaId(String slaId, Date from, Date until) throws Exception {
       return accountEventDAO.getEmAccountEventsBySlaId(slaId,from,until);
    }

    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public List<EmAccountEvent> getEmAccountEventsByPayerId(Long payerId, Date from, Date until) throws Exception {
       return accountEventDAO.getEmAccountEventsByPayerId(payerId, from, until);
    }

    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public List<EmAccountEvent> getEmAccountEventsByVendorId(Long vendorId, Date from, Date until) throws Exception {
        return accountEventDAO.getEmAccountEventsByVendorId(vendorId, from, until);
    }

    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public List<EmAccountEvent> getEmAccountEventByViolationId(String violationId, Date from, Date until)
            throws Exception {
        return accountEventDAO.getEmAccountEventByViolationId(violationId, from, until);
    }



}
