/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.dao;


import java.util.Date;
import java.util.List;

import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.dao.AbstractHibernateDAO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface AccountEventDAO extends AbstractHibernateDAO<EmAccountEvent, Long> {
    
    /**
    * Get the events corresponding to a period
    * @param party
    * @return
    */
   public List<EmAccountEvent> getEventsByPartyAndDates(Long party,Date initDate,Date endDate)throws Exception;
   
   
   /**
    * Get Unpaid events
    * @param party
    * @return
    */
   public List<EmAccountEvent> getUnPaidByParty(Long party)throws Exception;

   //Added for Billing Reporting
   
   /**
    * @param slaId Id of the SLA to check account info
    * @return An arraylist of account events of provided slaId 
    */
   public abstract List<EmAccountEvent> getEmAccountEventsBySlaId(String slaId, Date from, Date until)throws Exception;
   
   /**
    * @param payerId Id of the payer with account events
    * @return An arraylist of account events of provided  payerId 
    */
   public abstract List<EmAccountEvent> getEmAccountEventsByPayerId(Long payerId, Date from, Date until)throws Exception;
   
   /**
    * @param vendorId Id of the vendor with account events
    * @return An arraylist of account events of provided vendorId 
    */
   public abstract List<EmAccountEvent> getEmAccountEventsByVendorId(Long vendorId, Date from, Date until)throws Exception;
   
   /**
    * @param violationId Id of the violation with account events
    * @return An arraylist of account events of provided vendorId 
    */
   public abstract List<EmAccountEvent> getEmAccountEventByViolationId(String violationId, Date from, Date until)throws Exception;
   
}
