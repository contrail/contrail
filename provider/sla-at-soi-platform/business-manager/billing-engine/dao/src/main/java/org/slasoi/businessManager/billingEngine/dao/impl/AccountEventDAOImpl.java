/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.billingEngine.dao.AccountEventDAO;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.common.dao.impl.AbstractHibernateDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="accountEventDAO")
public class AccountEventDAOImpl extends AbstractHibernateDAOImpl<EmAccountEvent, Long> implements AccountEventDAO {
    private static final Logger log = Logger.getLogger( AccountEventDAOImpl.class );
    @Autowired
    public AccountEventDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
    
    @Override
    protected Class<EmAccountEvent> getDomainClass() {
            return EmAccountEvent.class;
    }
    
    /**
     * Get the events corresponding to a period
     * @param party
     * @return
     */
    public List<EmAccountEvent> getEventsByPartyAndDates(Long party,Date initDate,Date endDate)throws Exception{
        log.debug("finding EmPartyBank By PartyID: " +party);
        try {                 
            Criteria criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
           //To filter by service
            //criteria.createAlias("payer","payer",CriteriaSpecification.LEFT_JOIN); 
            //criteria.createAlias("emPartyBank","bank",CriteriaSpecification.LEFT_JOIN);
            //criteria.createAlias("emPartyBank.emParty","party",CriteriaSpecification.LEFT_JOIN);
            //criteria.add(Restrictions.eq("party.nuPartyid", party));
            //criteria.add(Restrictions.eq("bank.tcDefaultYn", BillingConstants.CHAR_YES));
            criteria.add(Restrictions.eq("payer.nuPartyid", party));
            criteria.add(Restrictions.between("tsTimeStamp", initDate,endDate));
            List<EmAccountEvent> result = criteria.list();
            return result;             
        } catch (RuntimeException re) {
            log.error("finding getEventsByPartyAndDates", re);
            throw re;       
        }
    }
    
    /**
     * Get Unpaid events (payment event null and debit type)
     * @param party
     * @return
     */
    public List<EmAccountEvent> getUnPaidByParty(Long party)throws Exception{
        
        log.debug("finding getUnPaidByParty By PartyID: " +party);
        try {                 
            Criteria criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
           //To filter by service
            //criteria.createAlias("emPartyBank","bank",CriteriaSpecification.LEFT_JOIN);
            //criteria.createAlias("emPartyBank.emParty","party",CriteriaSpecification.LEFT_JOIN);
            //criteria.add(Restrictions.eq("bank.tcDefaultYn", BillingConstants.CHAR_YES));
            criteria.add(Restrictions.eq("payer.nuPartyid", party));
            criteria.add(Restrictions.eq("tcType", BillingConstants.EVENT_DEBIT));
            criteria.add(Restrictions.isNull("paymentEvent"));
            List<EmAccountEvent> result = criteria.list();
            return result;             
        } catch (RuntimeException re) {
            log.error("finding getEventsByPartyAndDates", re);
            throw re;       
        }
        
        
    }

    //New Billing methods for reporting
    
    public List<EmAccountEvent> getEmAccountEventsBySlaId(String slaId, Date from, Date until) throws Exception{
      
        Criteria criteria = null;
        List<EmAccountEvent> result = null;
        
        log.debug("finding getEmAccountEvents By SlaId: " +slaId);
        
        try {                 
            criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            
            criteria.add(Restrictions.ilike("txBslaId", slaId));            
            criteria.add(Restrictions.isNull("paymentEvent"));
            
            if (until!=null && from!=null){                
                criteria.add(Restrictions.between("tsTimeStamp", from, until));
            }else if(from!=null){
                criteria.add(Restrictions.between("tsTimeStamp", from, new Date()));
            }else if(until!=null){
                criteria.add(Restrictions.le("tsTimeStamp", until));
            }
            
            result = (List<EmAccountEvent>) criteria.list();
                         
        } catch (RuntimeException re) {
            log.error("finding getEmAccountEvents", re);
            throw re;       
        }
        
        return result;
    }

    public List<EmAccountEvent> getEmAccountEventsByPayerId(Long payerId, Date from, Date until) throws Exception{
        
        Criteria criteria = null;
        List<EmAccountEvent> list = null;
        
        log.debug("finding getEmAccountEventsByPayerId By payerId: " +payerId);
        
        try {                 
            criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                     
         
            criteria.add(Restrictions.eq("payer.nuPartyid", payerId));
            criteria.add(Restrictions.eq("tcType", BillingConstants.EVENT_DEBIT));
            criteria.add(Restrictions.isNull("paymentEvent"));
            
            if (until!=null && from!=null){                
                criteria.add(Restrictions.between("tsTimeStamp", from, until));
            }else if(from!=null){
                criteria.add(Restrictions.between("tsTimeStamp", from, new Date()));
            }else if(until!=null){
                criteria.add(Restrictions.le("tsTimeStamp", until));
            }
            
            list = (List<EmAccountEvent>)criteria.list();
                         
        } catch (RuntimeException re) {
            log.error("finding getEmAccountEventsByPayerId", re);
            throw re;       
        }
        
        if (list==null) {
            list = new ArrayList<EmAccountEvent>();
        }
                
        return list;
    }

    public List<EmAccountEvent> getEmAccountEventsByVendorId(Long vendorId, Date from, Date until) throws Exception{
        
        Criteria criteria = null;
        List<EmAccountEvent> list = null;
        
        log.debug("finding getEmAccountEventsByVendorId By vendorId: " +vendorId);
        
        try {                 
            criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                     
         
            criteria.add(Restrictions.eq("vendor.nuPartyid", vendorId));
            criteria.add(Restrictions.eq("tcType", BillingConstants.EVENT_DEBIT));
            criteria.add(Restrictions.isNull("paymentEvent"));
            
            if (until!=null && from!=null){                
                criteria.add(Restrictions.between("tsTimeStamp", from, until));
            }else if(from!=null){
                criteria.add(Restrictions.between("tsTimeStamp", from, new Date()));
            }else if(until!=null){
                criteria.add(Restrictions.le("tsTimeStamp", until));
            }
            
            list = (List<EmAccountEvent>)criteria.list();
                         
        } catch (RuntimeException re) {
            log.error("finding getEventsByPartyAndDates", re);
            throw re;       
        }
        
        if (list==null) {
            list = new ArrayList<EmAccountEvent>();
        }
                
        return list;
    }

    public List<EmAccountEvent> getEmAccountEventByViolationId(String violationId, Date from, Date until) throws Exception{
        
        Criteria criteria = null;
        List<EmAccountEvent> list = null;
        
        log.debug("finding getEmAccountEventByViolationId By violationId: " +violationId);
        
        try {                 
            criteria = this.getSession().createCriteria(EmAccountEvent.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                     
         
            criteria.add(Restrictions.ilike("txPenaltyId", violationId));
            criteria.add(Restrictions.ilike("tcType", BillingConstants.EVENT_CREDIT));
            criteria.add(Restrictions.isNull("paymentEvent"));
            
            if (until!=null && from!=null){                
                criteria.add(Restrictions.between("tsTimeStamp", from, until));
            }else if(from!=null){
                criteria.add(Restrictions.between("tsTimeStamp", from, new Date()));
            }else if(until!=null){
                criteria.add(Restrictions.le("tsTimeStamp", until));
            }
            
            list = (List<EmAccountEvent>)criteria.list();
                         
        } catch (RuntimeException re) {
            log.error("finding getEventsByPartyAndDates", re);
            throw re;       
        }
        
        if (list==null) {
            list = new ArrayList<EmAccountEvent>();
        }
        
        
        return list;
    }

}
