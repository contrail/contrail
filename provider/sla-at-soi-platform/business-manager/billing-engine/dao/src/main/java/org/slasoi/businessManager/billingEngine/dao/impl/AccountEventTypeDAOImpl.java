/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.billingEngine.dao.AccountEventTypeDAO;
import org.slasoi.businessManager.common.dao.impl.AbstractHibernateDAOImpl;
import org.slasoi.businessManager.common.model.billing.EmAccountEventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value = "accountEventTypeDAO")
public class AccountEventTypeDAOImpl extends AbstractHibernateDAOImpl<EmAccountEventType, Long> implements
        AccountEventTypeDAO {
    private static final Logger log = Logger.getLogger(AccountEventTypeDAOImpl.class);

    @Autowired
    public AccountEventTypeDAOImpl(SessionFactory factory) {
        setSessionFactory(factory);
    }

    @Override
    protected Class<EmAccountEventType> getDomainClass() {
        return EmAccountEventType.class;
    }

    /**
     * Get EventType by name
     * 
     * @param eventName
     * @return
     */
    public EmAccountEventType getByName(String eventTypeName) throws Exception {

        log.debug("finding EmAccountEventType By name: " + eventTypeName);
        try {
            Criteria criteria = this.getSession().createCriteria(EmAccountEventType.class);
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            criteria.add(Restrictions.eq("txName", eventTypeName));
            List<EmAccountEventType> result = criteria.list();
            if (result != null && result.size() > 0) {
                return result.get(0);
            }
            else
                return null;
        }
        catch (RuntimeException re) {
            log.error("finding finding EmAccountEventType By name", re);
            throw re;
        }

    }

}
