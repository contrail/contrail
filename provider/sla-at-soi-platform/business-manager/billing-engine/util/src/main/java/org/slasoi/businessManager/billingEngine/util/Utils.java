/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.util;


import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmPriceVariationType;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.PriceModification;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;

public class Utils {
    private static final Logger log = Logger.getLogger(Utils.class);
 /**
  * Get the partyID from the SLA   
  * @param bsla
  * @return
  */
    public long getCustomerIdFromSLA(SLA bsla){
        log.info("Inside getCustomerIdFromSLA");
        try{
            Party []parties = bsla.getParties();
            if(parties!=null && parties.length>0){
                Party party;
                for(int i=0;i<parties.length;i++){
                    party = parties[i];
                    if(sla.customer.toString().equalsIgnoreCase(party.getAgreementRole().toString())){
                        log.debug("The template has a customer Role");
                        return new Long(party.getId().getValue());
                    }
                 }      
            }
        }catch (Exception e){
            log.error("Error: " + e.toString());
            e.printStackTrace();   
            return -1;
        }
        return -1;
    }
    

 /**
  * Calculate the final charge to add after finishing an SLA
  * @param bsla
  * @return
  */
      public CONST calculateFinalCharge(SLA bsla ){
          log.info("Inside calculateFinalCharge method");       
          
          try{
              //obtain the offer price in sla model
              ComponentProductOfferingPrice compo = this.getComponentPrice(bsla, business.per_month.getValue());
              if(compo !=null){
                  CONST monthlyPrice = calculatePrice(compo);
                  GregorianCalendar cal = new GregorianCalendar();
                  int currentDay = cal.get(Calendar.DAY_OF_MONTH);
                  int maximumDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                  double finalCharge= (Double.valueOf(monthlyPrice.getValue())*currentDay)/maximumDays;
                  monthlyPrice.setValue(Double.toString(finalCharge));
                  return monthlyPrice;
              }  
              log.info("Component not found");    
              return null;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return null;
          }                  
      }
      
/**
 * Calculate periodic charge to a customer
 * @param contract
 * @return
 */
      public CONST calculatePeriodicCharge(EmCustomersProducts contract){
          log.info("Inside calculatePeriodicCharge method");                
          try{
              Set<EmComponentPrice> components = contract.getEmSpProductsOffer().getEmComponentPrices();
              Iterator<EmComponentPrice>it = components.iterator();
              
              CONST charge =null;
              boolean changed = false;
              while(it.hasNext()){
                  EmComponentPrice component = it.next();
                  if(business.per_month.getValue().substring(business.per_month.getValue().lastIndexOf("#")+1).
                          equalsIgnoreCase(component.getEmPriceType().getTxName())){
                         //price per month
                      if(charge == null) charge=new CONST(component.getNuPrice().toString(),
                              new STND(component.getEmCurrencies().getTxName()));   
                      Set<EmPriceVariation> variations = component.getEmPriceVariations();
                      if(variations != null && variations.size()>0){
                        //Apply variations
                        log.info("Component has variations");  
                        double finalPrice = calculatePrice(component,variations);
                        if(!changed){
                            charge.setValue(Double.toString(finalPrice));
                        }else{
                            double initialCharge = Double.parseDouble(charge.getValue());
                            charge.setValue(Double.toString(finalPrice+initialCharge));
                        }
                          
                      }
                      changed = true;

                  }else if(business.per_week.getValue().substring(business.per_week.getValue().lastIndexOf("#")+1).
                          equalsIgnoreCase(component.getEmPriceType().getTxName())){
                      //price per week
                      if(charge == null) charge=new CONST(component.getNuPrice().toString(),
                              new STND(component.getEmCurrencies().getTxName()));   
                      
                   Set<EmPriceVariation> variations = component.getEmPriceVariations();
                   if(variations != null && variations.size()>0){
                     //Apply variations
                       log.info("Component has variations");  
                       double finalPrice = calculatePrice(component,variations);
                       if(!changed){
                           charge.setValue(Double.toString(finalPrice*4));
                       }else{
                           double initialCharge = Double.parseDouble(charge.getValue());
                           charge.setValue(Double.toString((finalPrice*4)+initialCharge));
                       }                         
                     }
                     changed = true;            
                }                   
              }
             return charge;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return null;
          }                  
      }
      /**
       * Calculate the price of a component      
       * @param componentPrice
       * @return
       */
         private  double calculatePrice(EmComponentPrice componentPrice, Set<EmPriceVariation> variations){
                log.debug("Inside calculatePrice Method");
                try{
                    double initialPrice = componentPrice.getNuPrice().doubleValue();
                    double price = initialPrice;
                      if(variations != null&variations.size()>0){
                        log.debug("Number of variations: " + variations.size() );
                        Iterator <EmPriceVariation>iterator = variations.iterator();
                        EmPriceVariation variation;
                        EmPriceVariationType variationType;
                        while(iterator.hasNext()){
                            variation = iterator.next();
                            variationType = variation.getEmPriceVariationType();                       
                            if(Constants.UNIT_AMOUNT.equalsIgnoreCase(variationType.getTcUnitType())){
                                //it is a fixed quantity
                                if(Constants.MODIFICATION_INCREMENT.equalsIgnoreCase( variationType.getTcModificationType())){
                                  //it is an increment
                                    price = price + variation.getNuValue().doubleValue();
                                }else if(Constants.MODIFICATION_DECREMENT.equalsIgnoreCase( variationType.getTcModificationType())){
                                    //it is a discount
                                    price = price - variation.getNuValue().doubleValue();
                                }   
                              
                            }else  if(Constants.UNIT_PERCENTAGE.equalsIgnoreCase(variationType.getTcUnitType())){
                              //it is a percentage
                                if(Constants.MODIFICATION_INCREMENT.equalsIgnoreCase( variationType.getTcModificationType())){
                                    //it is an increment
                                      price = price + (initialPrice* variation.getNuValue().doubleValue()/100);
                                  }else if(Constants.MODIFICATION_DECREMENT.equalsIgnoreCase( variationType.getTcModificationType())){
                                      //it is a discount
                                      price = price - (initialPrice* variation.getNuValue().doubleValue()/100);
                                  }     
                            }
                        }
                        
                      }else{
                        log.debug("No variations");
                      }
                    return price;
                }catch(Exception e){
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                
                return 0;
            }
    
 
    /**
     * This method search for a subscription price and return the vale
     * @param template
     * @param newoffer
     * @return
     */
      public CONST searchforSubscriptionPrice(SLA bsla ){
          log.info("Inside searchforMontlyPrice method");       
          
          try{
              //obtain the offer price in sla model
              ComponentProductOfferingPrice compo = this.getComponentPrice(bsla, business.one_time_charge.getValue());
              if(compo !=null){
                  return calculatePrice(compo);
              }  
              log.info("Component not found");    
              return null;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return null;
          }                  
      }
/**
 * Get a component Price from an SLA      
 * @param bsla
 * @param type
 * @return
 */
      private ComponentProductOfferingPrice getComponentPrice(SLA bsla , String type){
          log.info("Inside getComponentPrice method");         
          try{
              //obtain the offer price in sla model
              AgreementTerm[]terms = bsla.getAgreementTerms();
              AgreementTerm term;
              Guaranteed[] guarantees;
              Guaranteed guarantee;
              Action action;
              Defn def; 
              for(int i=0;i<terms.length;i++){
                  term = terms[i];
                  guarantees = term.getGuarantees();
                  for(int j=0;j<guarantees.length;j++){
                      guarantee = guarantees[j];
                      if(guarantee instanceof Action){
                          action = (Action)guarantee;
                          def = action.getPostcondition();
                          if(def instanceof ProductOfferingPrice){
                              log.debug("ProductOfferingPrice Found");
                              ProductOfferingPrice offerPrice = (ProductOfferingPrice)def;
                              ComponentProductOfferingPrice[] components = offerPrice.getComponentProductOfferingPrices();
                              if(components !=null && components.length>0){
                                  for(ComponentProductOfferingPrice compo : components){
                                      if(compo.getPriceType().getValue().equalsIgnoreCase(type)){
                                          return compo;       
                                      }                                     
                                  }                                  
                              }
                          }
                      }
                  }                  
              }
              log.error("Product Offering Price not found");
              return null;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return null;
          }                  
          
      }
      
 /**
  * This method calculate the final price of the monthly price when there are variations     
  * @param componentPrice
  * @return
  */
      private  CONST calculatePrice(ComponentProductOfferingPrice componentPrice){
          log.debug("Inside calculatePrice Method");
          CONST finalPrice; 
          try{
              //the space in CONST is needed
              finalPrice = new CONST("0",componentPrice.getPrice().getDatatype());
              
              double initialPrice = Double.parseDouble(componentPrice.getPrice().getValue());
              double price = initialPrice;
              PriceModification[] variations = componentPrice.getPriceModifications();
                if(variations != null&variations.length>0){
                  log.debug("Number of variations: " + variations.length );
                  for(int i=0;i <variations.length;i++){                  
                      if(!units.percentage.getValue().equalsIgnoreCase(variations[i].getValue().getDatatype().getValue())){
                          //it is a fixed quantity
                          if(business.increment.getValue().equalsIgnoreCase( variations[i].getType().getValue())){
                            //it is an increment
                              price = price + Double.parseDouble(variations[i].getValue().getValue());
                          }else if(business.discount.getValue().equalsIgnoreCase( variations[i].getType().getValue())){
                              //it is a discount
                              price = price - Double.parseDouble(variations[i].getValue().getValue());
                          }   
                      //it is a percentage  
                      }else {
                        //it is a percentage
                          if(business.increment.getValue().equalsIgnoreCase( variations[i].getType().getValue())){
                              //it is an increment
                                price = price + (initialPrice* Double.parseDouble(variations[i].getValue().getValue())/100);
                            }else if(business.discount.getValue().equalsIgnoreCase( variations[i].getType().getValue())){
                                //it is a discount
                                price = price - (initialPrice*Double.parseDouble(variations[i].getValue().getValue())/100);
                            }     
                      }
                  }                                    
                  finalPrice.setValue(Double.toString(price));
                }else{
                  log.debug("No variations");
				  finalPrice.setValue(Double.toString(initialPrice));
                } 
              return finalPrice;
          }catch(Exception e){
              log.error(e.getMessage());
              e.printStackTrace();
          }          
          return null;
      }
/**
 * Get the product offer from the sla.      
 * @param sla
 * @return
 */
      public long getProductOfferIdFromSLA(SLA sla){          
          log.info("Inside getProductOfferIdFromSLA method");         
          try{
              //obtain the offer price in sla model
              AgreementTerm[]terms = sla.getAgreementTerms();
              AgreementTerm term;
              Guaranteed[] guarantees;
              Guaranteed guarantee;
              Action action;
              Defn def; 
              for(int i=0;i<terms.length;i++){
                  term = terms[i];
                  guarantees = term.getGuarantees();
                  for(int j=0;j<guarantees.length;j++){
                      guarantee = guarantees[j];
                      if(guarantee instanceof Action){
                          action = (Action)guarantee;
                          def = action.getPostcondition();
                          if(def instanceof ProductOfferingPrice){
                              log.debug("ProductOfferingPrice Found");
                              ProductOfferingPrice offerPrice = (ProductOfferingPrice)def;
                              return Long.parseLong(offerPrice.getId().toString());                              
                          }
                      }
                  }                  
              }
              log.error("Product Offering Price not found");
              return -1;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return -1;
          }                       
          
      }


}
