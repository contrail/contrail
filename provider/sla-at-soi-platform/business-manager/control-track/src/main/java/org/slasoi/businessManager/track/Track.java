/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/control-track/src/main/java/org/slasoi/businessManager/track/types/Track.java $
 */

package org.slasoi.businessManager.track;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.bslamanager.pac.IBusinessProvisioningAdjustment;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.common.ws.types.CustomerNotFoundException;
import org.slasoi.businessManager.common.ws.types.ProductNotFoundException;
import org.slasoi.businessManager.postSale.management.CustomerManagement;
import org.slasoi.businessManager.track.interfaces.ITrack;
import org.slasoi.businessManager.track.internal.Activator;

public class Track implements ITrack{
    private static final Logger log = Logger.getLogger(Track.class);
    /*protected IBusinessProvisioningAdjustment adjustment;
    
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setIBusinessProvisioningAdjustment(IBusinessProvisioningAdjustment adjustment) {
        log.info("***************Registered Service IBusinessProvisioningAdjustment Found*************");
        this.adjustment = adjustment;
    }  */      
    
    /**
     * The BM is informed about a violation Event.
     * @param violationList
     * @return
     */
       public int trackEvent (AdjustmentNotificationType[] violationList){
           log.debug("Inside trackEvent method");
           
           IBusinessProvisioningAdjustment adjustment = null;
          
           BundleContext bc= Activator.getContext();
           if (bc!=null){
              ServiceReference ref =  bc.getServiceReference(IBusinessProvisioningAdjustment.class.getName());
           if (ref!=null)
               adjustment = (IBusinessProvisioningAdjustment) bc.getService(ref);
           }else{
               log.error("**********************Service IBusinessProvisioningAdjustment not Found");
           }
           if(violationList!=null&& violationList.length>0){
             log.debug("Violation List size: "+violationList.length);
             //the Receive different violations.
             List<AdjustmentNotificationType> violations = new ArrayList <AdjustmentNotificationType>(); 
             
             //transform the violations format             
             AdjustmentNotificationType violationType=null;
             
             for(AdjustmentNotificationType type: violations){                 
                 violationType = new AdjustmentNotificationType();
                 violationType.clone(type);
                 violations.add(violationType);
             }
             log.debug("Call violationPenalty Service");
             if(adjustment!=null){
                 //send the notifications.
                adjustment.trackEvent(violations);
             }else{
                 log.error("IBusinessProvisioningAdjustment is null");
             }
           }else{
               log.error("Empty Violation List");            
           }           
           return 0;           
       }   
       
   /**
    * The BM is asked about the authorization of a purchase    
    * @param productId
    * @param customerID
    * @return
    * @throws CustomerNotFoundException
    * @throws ProductNofFoundException
    */
       public boolean getCustomerPurchaseAuth(long productId, long customerId)
       throws CustomerNotFoundException,ProductNotFoundException{
           log.debug("Inside getCustomerPurchaseAuth method");
           log.debug("ProductID:"+productId);
           log.debug("CustomerID"+customerId);
           boolean auth = false;
           //if the user has enough privileges return true.
           try{
               CustomerManagement customerManagement = null;
               BundleContext bc= Activator.getContext();
               if (bc!=null){
                   ServiceReference ref =  bc.getServiceReference(CustomerManagement.class.getName());
                   if (ref!=null)
                       customerManagement = (CustomerManagement) bc.getService(ref);
                       auth=customerManagement.customerPurchaseAuth(productId, customerId);
                       log.info("Authorization result: " +auth);
                   
               }else{
                   log.error("**********************Service CustomerManagement not Found");
               }
           }catch(Exception e){
               log.error("Error: " +e.toString());
               e.printStackTrace();               
           }

           return auth;
       }

}
