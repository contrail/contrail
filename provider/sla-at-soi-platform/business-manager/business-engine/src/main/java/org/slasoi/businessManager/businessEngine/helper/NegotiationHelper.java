/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/helper/NegotiationHelper.java $
 */

package org.slasoi.businessManager.businessEngine.helper;

import java.io.FileInputStream;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.SLAEvaluation.AgreementTermData;
import org.slasoi.businessManager.businessEngine.SLAEvaluation.GuaranteeData;
import org.slasoi.businessManager.businessEngine.SLAEvaluation.SLATData;
import org.slasoi.businessManager.businessEngine.SLAEvaluation.SLATEvaluator;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.service.GtTranslationManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.PriceModification;
import org.slasoi.slamodel.sla.business.Product;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;


public class NegotiationHelper {
    private static final Logger log = Logger.getLogger(NegotiationHelper.class);  
    public static String BUILD_TYPE_BUSINESS = "business";
    public static String BUILD_TYPE_LOWLAYERS= "lowLayers";
    private GtTranslationManager gtTranslationManager;
    
    public NegotiationHelper(GtTranslationManager gtTranslationManager){
        this.gtTranslationManager = gtTranslationManager;
    }
    
    /**
     * This method built a BSLAT from the different templates obtained from negotiation
     * @param bSlaTemplate
     * @param templates
     * @return
     */
        public SLATemplate reBuiltBslat(SLATemplate bSlaTemplate, List<SLATemplate> templates,String type){
            log.info("Inside reBuiltBslat method.Low layer Templates size: "+templates.size() );
            try{
                SLATData slatData;
                List<AgreementTermData> agreementTerms = null;
                for(SLATemplate template:templates){
                    slatData = SLATEvaluator.getSLATData(template);
                    log.debug("number of GTs "+template.getUuid().getValue()+": "+ slatData.getAgreementsTerms().size());
                    if(agreementTerms==null){
                        agreementTerms = slatData.getAgreementsTerms();
                    }else{
                        agreementTerms.addAll(slatData.getAgreementsTerms());
                    }
                }
                log.debug("Total number of slats:"+ agreementTerms.size());
                //once we have all the Terms--> modify the main SLAT
                AgreementTerm[] terms = bSlaTemplate.getAgreementTerms();
                if(terms!=null && terms.length>0){
                    for(int i=0;i<terms.length;i++){        
                        Guaranteed[] guarantees = terms[i].getGuarantees();
                        log.info("Term "+i+" ID:"+terms[i].getId());       
                        State aux;
                        for(int j=0;j<guarantees.length;j++){ 
                            log.info("Guarantee "+j+" ID:"+guarantees[j].getId().getValue());
                            //State part
                            if(guarantees[j] instanceof State ){
                                log.info("Guarantee is a Guarantee.State");
                                aux = (State) guarantees[j];                   
                                ConstraintExpr cons = aux.getState();
                                if(cons instanceof TypeConstraintExpr){  
                                    log.info("ConstraintExpr is TypeConstraintExpr");
                                    TypeConstraintExpr constraint = (TypeConstraintExpr)cons;
                                    STND Operator =((FunctionalExpr)constraint.getValue()).getOperator();
                                    log.info("Operator value:"+Operator.getValue());
                                    //ValueExpr[] parameters =((FunctionalExpr)constraint.getValue()).getParameters();
                                    /*if(parameters[0] instanceof ID){                              
                                        ID idService = (ID)parameters[0];
                                        log.info("Id Service:"+idService.getValue());
                                    }*/
                                    //Domain expression
                                    if(constraint.getDomain()instanceof SimpleDomainExpr){
                                        SimpleDomainExpr domain = (SimpleDomainExpr)constraint.getDomain();
                                        STND comparisonOp = domain.getComparisonOp();
                                        log.info("comparison Operation:"+comparisonOp.getValue());
                                        if(domain.getValue() instanceof CONST){
                                            log.info("Value is CONTS type");
                                            CONST  data = (CONST)domain.getValue();
                                            log.info("Parameter Value: "+data.getValue());
                                            if(data.getDatatype()!=null){
                                                log.info("Parameter Type: "+data.getDatatype().getValue());
                                            }
                                            //set the new Values for the guaranteeTerm
                                            SLATData localSlatData = new SLATData();
                                            localSlatData.setAgreementsTerms(agreementTerms);
                                            //search for the GT
                                            GuaranteeData dataToModify = SLATEvaluator.searchGuarantee(localSlatData.getAgreementsTerms(),terms[i].getId().getValue(), 
                                                    guarantees[j].getId().getValue());
                                            //if the gt is not found --> search into the translation
                                            if(dataToModify == null){  
                                                EmGtTranslation translation = new EmGtTranslation();
                                                EmGtTranslationId id = new EmGtTranslationId();     
                                                translation.setId(id);
                                              //build BSLAT form low layers
                                                if(BUILD_TYPE_BUSINESS.equalsIgnoreCase(type)){
                                                    log.info("Rebuilt Business Template");
                                                    id.setTxBslatId(bSlaTemplate.getUuid().getValue());
                                                    id.setTxMixAgreedId(guarantees[j].getId().getValue());
                                                    id.setTxMixAgreementTermId(terms[i].getId().getValue());
                                                    EmGtTranslation translationResult = gtTranslationManager.getGtTranslation("business", translation);
                                                    if(translationResult !=null){
                                                        log.info("Translation found");   
                                                        //Search again into the list with the translated term
                                                        dataToModify = SLATEvaluator.searchGuarantee(localSlatData.getAgreementsTerms(),translationResult.getTxAgreementTermId(), 
                                                            translationResult.getTxAgreedId());
                                                    }
                                                //build SLAT from business    
                                                }else if(BUILD_TYPE_LOWLAYERS.equalsIgnoreCase(type)){
                                                    log.info("Rebuilt Low layer Template");                                                    
                                                    id.setTxBslatId(templates.get(0).getUuid().getValue());
                                                    id.setTxSlatId(bSlaTemplate.getUuid().getValue());
                                                    translation.setTxAgreedId(guarantees[j].getId().getValue());
                                                    translation.setTxAgreementTermId(terms[i].getId().getValue());
                                                    //Search again into the list with the translated term
                                                    EmGtTranslation translationResult = gtTranslationManager.getGtTranslation("lowLayer", translation);
                                                    if(translationResult !=null){
                                                        log.info("Translation found");
                                                        //Search again into the list with the translated term
                                                        dataToModify = SLATEvaluator.searchGuarantee(localSlatData.getAgreementsTerms(),translationResult.getId().getTxMixAgreementTermId(), 
                                                            translationResult.getId().getTxMixAgreedId());
                                                    }
                                                }
                                                
                                                
                                            }
                                            
                                            //Set the value.
                                            if(dataToModify != null){
                                                if("Numerical".equalsIgnoreCase(dataToModify.getGtParameterType())){
                                                    data.setValue(Double.toString(dataToModify.getGtParameterValue()));
                                                }else{
                                                //if it is not a numerical Value.
                                                    data.setValue(dataToModify.getGtParameterValueString());
                                                }   
                                            }
                                            domain.setValue(data);
                                        }else if(domain.getValue() instanceof ID){
                                            //for infrastructure domain
                                           /*Find how modify infrastructure template
                                           * */                                            
                                        }else break;
                                        constraint.setDomain(domain);
                                    } 
                                    aux.setState(constraint);
                                }                            
                                guarantees[j]=aux;
                            }//else do nothing
                        }
                        terms[i].setGuarantees(guarantees);
                    }//end for terms
                }//                         
                return bSlaTemplate;
            }catch(Exception e){
                log.error("Error:"+e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
     
    /**
     * This method calculate the different possibilities of the templates retrieves from negotiation with other layers  
     * @param listSize
     * @return
     */
    public static int[][]  calculateIndices(int[] listSize){
           int posibilities = 1;
           for(int i =0;i<listSize.length;i++){
               posibilities = posibilities * listSize[i];
           }
           int[][] indices = new int[listSize.length][posibilities];
           int [] values = new int [listSize.length];       
           
           for(int ite=0;ite<posibilities;ite++){
               for(int ite2=0;ite2<listSize.length;ite2++){   
                   indices[ite2][ite] =values[ite2];
                   if(ite2==listSize.length-1){
                       values[ite2]=values[ite2]+1;
                       if(values[ite2]>listSize[ite2]-1){
                           values[ite2]=0;
                           int ind = ite2-1;
                           while(ind>=0){
                               values[ind]=values[ind]+1;
                               if(values[ind]>listSize[ind]-1){
                                   values[ind]=0;
                               }else{
                                   break;
                               }        
                               ind --;
                           }
                       }
                   }
               }
           }
           return indices;
    }
 
 /**
  * This method convert from product offer business model to prodcutOfferSLAModel   
  * @param productOfferPrice
  * @return
  */
   public static ProductOfferingPrice convertToProductOfferingPrice(EmSpProductsOffer productOfferPrice){
       log.info("Inside convertToProductOfferingPrice method");
       try{
           //ID
           ID id= new ID(productOfferPrice.getNuIdproductoffering().toString());
           //name
           String name = productOfferPrice.getTxName();
           //validFor
           GregorianCalendar from = new GregorianCalendar();
           from.setTime(productOfferPrice.getDtValidFrom());
           TIME validFrom =   new TIME(from);
           GregorianCalendar until = new GregorianCalendar();
           until.setTime(productOfferPrice.getDtValidTo());        
           TIME validUntil = new TIME(until);

           //billingFrecuency
           /**TODO
            * check the billingFrecuency??
            */
           STND billingFrequency = new STND(business.$base+productOfferPrice.getEmBillingFrecuency().getTxName());
           //product
           EmSpProducts storedProduct = productOfferPrice.getEmSpProducts();
           Product product = new Product(new ID(storedProduct.getNuProductid().toString())
                                         , storedProduct.getTxProductname());
           //Components offeringPrices
           Set<EmComponentPrice> components = productOfferPrice.getEmComponentPrices();    
           ComponentProductOfferingPrice[] cpops = null;          
           if(components!= null &&components.size()>0){
               Iterator <EmComponentPrice> iterator = components.iterator(); 
               cpops = new ComponentProductOfferingPrice[components.size()];               
               ComponentProductOfferingPrice componentPrice = null;               
               int index = 0;              
               while(iterator.hasNext()){
                   EmComponentPrice component = new EmComponentPrice();
                   component = iterator.next();
                   //id
                   ID idCompo = new ID(component.getTxName());
                   //priceType
                   /**TODO
                    * verify priceType
                    */
                   STND priceType = new STND(business.$base+component.getEmPriceType().getTxName());
                   //Price
                   /**TODO
                    * verify Price
                    */
                   CONST price = new CONST(component.getNuPrice().toString(),
                           new STND(units.$base+component.getEmCurrencies().getTxName()));
                   //Quantity
                   CONST quantity = new CONST(component.getNuQuantity().toString(),null);
                   //Price Modifications
                   Set<EmPriceVariation>  variations= component.getEmPriceVariations();
                   if(variations!=null && variations.size()>0){
                       log.debug("component with modifications:" + component.getTxDescription());
                       Iterator<EmPriceVariation> it= variations.iterator();
                       PriceModification[] pms = new PriceModification[variations.size()];
                       PriceModification modification;                      
                       int indexModify = 0;
                       while(it.hasNext()){
                           EmPriceVariation variation = new EmPriceVariation();
                           variation = it.next();
                           //from here it is easy to obtain the type and unit of the variation
                           //increment,decrement
                           STND type = null;
                           if(Constants.MODIFICATION_INCREMENT.equals(variation.getEmPriceVariationType().getTcModificationType())){
                               type = business.increment;
                           }else  if(Constants.MODIFICATION_DECREMENT.equals(variation.getEmPriceVariationType().getTcModificationType())){
                               type = business.discount;
                           }
                           //amount,percentage
                           STND unit = null;
                           if(Constants.UNIT_AMOUNT.equalsIgnoreCase(variation.getEmPriceVariationType().getTcUnitType())){
                               unit = price.getDatatype();
                           }else  if(Constants.UNIT_PERCENTAGE.equalsIgnoreCase(variation.getEmPriceVariationType().getTcUnitType())){
                               unit = units.percentage;
                           }
                           
                           modification = new PriceModification(type, 
                                                    new CONST (variation.getNuValue().toString(),unit));
                           pms[indexModify] = modification;
                           indexModify ++;
                       } 
                       componentPrice = new ComponentProductOfferingPrice(idCompo,priceType,price,quantity,pms);
                   }else{
                       componentPrice = new ComponentProductOfferingPrice(idCompo,priceType,price,quantity,null);
                   }
                   cpops[index] = componentPrice;
                   index++;
               }
               
           }
           
           return new ProductOfferingPrice(id,name,validFrom,validUntil, billingFrequency,product, cpops);             
           
       }catch(Exception e){
           log.error("Error:"+e.getMessage());
           e.printStackTrace();
       }
       return null;
   }
 /**
  * This method obtain the SLAM's EndPoint for its template.   
  * @param template
  * @return
  */
   public static String obtainEndPoint(SLATemplate template){
       log.debug("Inside obtainEndPoint method");
       try{
           Party[] parties = template.getParties();
           if(parties!=null&& parties.length>0){
               Party party;
               for(int i=0;i<parties.length;i++){
                   party = parties[i];
                   if(sla.$provider.equalsIgnoreCase(party.getAgreementRole().getValue())){
                      //the party is the provider reference
                       String endPoint =party.getPropertyValue(sla.gslam_epr);
                       log.info("SLAM Endpoint: " + endPoint);
                      return endPoint;
                   }                   
               }               
           }
           
       }catch(Exception e){
           log.error("Error: "+e.toString());
           e.printStackTrace();                  
       }       
       return null;       
   }
/**
 * Check that the template has a customer Party   
 * @param template
 * @return
 */
   public static boolean checkCustomerParty(SLATemplate template){
       log.info("Inside checkCustomerParty");
       boolean exist = false;
       try{
           Party []parties = template.getParties();
           if(parties!=null && parties.length>0){
               Party party;
               for(int i=0;i<parties.length;i++){
                   party = parties[i];
                   if(sla.customer.toString().equalsIgnoreCase(party.getAgreementRole().toString())){
                       log.debug("The template has a customer Role");
                       return true;
                   }
               }
           }
           
       }catch (Exception e){
           log.error("Error: " + e.toString());
           e.printStackTrace();          
       }
       return exist;       
   }
 /**
  * Check that the template has a customer Party. If not, insert it.
  * @param template
  * @return
  */
   public static SLATemplate checkAndItroduceCustomerParty(SLATemplate template){
       log.info("inside checkAndItroduceCustomerParty");
       try{
           if(!checkCustomerParty(template)){
               log.info("The template does not have a cusomter Role");
               Party [] partiesAux = template.getParties();
               Party [] parties;
               if(partiesAux!=null &&partiesAux.length>0){
                   parties = new Party[partiesAux.length+1];
                   for(int i=0;i<partiesAux.length;i++){
                       parties[i] = partiesAux[i];
                   }
               }else{
                   parties = new Party[1];
               }

                              String propertiesFile= "provider.properties";
               String CONFIG_DIR =
                System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "BusinessManager"
                        + System.getProperty("file.separator"); 
               Properties properties = new Properties();
               properties.load(new FileInputStream(CONFIG_DIR + propertiesFile));
               Party party = new Party(new ID(properties.getProperty("provider.ID")), sla.customer);
               party.setPropertyValue(business.ContactPointPhone, properties.getProperty("provider.ContactPointPhone"));
               party.setPropertyValue(business.ContactPointFax, properties.getProperty("provider.ContactPointFax"));
               party.setPropertyValue(business.ContactPointAddress, properties.getProperty("provider.ContactPointAddress"));
               party.setPropertyValue(business.ContactPointName,properties.getProperty("provider.ContactPointName"));
               party.setPropertyValue(business.ContactPointEmail,properties.getProperty("provider.ContactPointEmail"));
               party.setPropertyValue(sla.gslam_epr,properties.getProperty("provider.TrackDir"));                  
               parties[parties.length-1] = party;
               template.setParties(parties);
           }
       }catch(Exception e){
           log.equals("Error:"+e.toString());
           e.printStackTrace();
       }  
       return template;
   }
   
   /**
    * Check that the template has a customer Party   
    * @param template
    * @return
    */
      public static long getCustomerID(SLATemplate template){
          log.info("Inside getCustomerID");
          try{
              Party []parties = template.getParties();
              if(parties!=null && parties.length>0){
                  Party party;
                  for(int i=0;i<parties.length;i++){
                      party = parties[i];
                      if(sla.customer.toString().equalsIgnoreCase(party.getAgreementRole().toString())){
                          log.debug("The template has a customer Role");
                          return Long.parseLong(party.getId().getValue());
                      }
                  }
              }
              
          }catch (Exception e){
              log.error("Error: " + e.toString());
              e.printStackTrace();          
          }
          return -1;       
      }
    /**
     * This method reintroduce the new offer price inside the template.  
     * @param template
     * @param newoffer
     * @return
     */
      public static SLATemplate insertNewProductOffer(SLATemplate template,EmSpProductsOffer newOffer ){
          log.info("Inisde introduceNewProductOffer method");
          
          try{
              //obtain the offer price in sla model
              ProductOfferingPrice offerPrice = convertToProductOfferingPrice(newOffer);
              if(offerPrice==null){
                  log.error("An error happends obtaining the template in SLAModel");
                  return null;
              }
              //set a parameter in order to set the name of the terms!!!
              //Id containing "PriceSpecification"
              AgreementTerm[]terms = template.getAgreementTerms();
              AgreementTerm term;
              Guaranteed[] guarantees;
              Guaranteed guarantee;
              Action action;
              Defn def; 
              for(int i=0;i<terms.length;i++){
                  term = terms[i];
                  guarantees = term.getGuarantees();
                  for(int j=0;j<guarantees.length;j++){
                      guarantee = guarantees[j];
                      if(guarantee instanceof Action){
                          action = (Action)guarantee;
                          def = action.getPostcondition();
                          if(def instanceof ProductOfferingPrice){
                              log.debug("ProductOfferingPrice Found");
                              action.setPostcondition(offerPrice);
                              guarantees[j] = action;
                              terms[i]= term;
                              template.setAgreementTerms(terms);
                              return template;  
                          }
                      }
                  }                  
              }
              log.error("Product Offering Price not found");
              return null;
          }catch(Exception e){
              log.error("Error: "+e.getMessage());
              e.printStackTrace();
              return null;
          }                  
      }
    
      
}
