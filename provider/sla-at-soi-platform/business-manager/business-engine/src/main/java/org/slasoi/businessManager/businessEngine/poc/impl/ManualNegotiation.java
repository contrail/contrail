package org.slasoi.businessManager.businessEngine.poc.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.poc.IManualNegotiation;
import org.springframework.stereotype.Service;

@Service(value="manualNegotiation")
public class ManualNegotiation implements IManualNegotiation{
    static Logger log = Logger.getLogger(ManualNegotiation.class);
    private java.util.HashMap<String, NegotiationSession> negotiationSessions;           

     public java.util.HashMap<String, NegotiationSession> getNegotiationSessions() {
        return negotiationSessions;
    }

    public void setNegotiationSessions(java.util.HashMap<String, NegotiationSession> negotiationSessions) {
        log.debug("method setNegotiationSessions: IManual negotiation: NegotiationSessions setted");
        this.negotiationSessions = negotiationSessions;
    }
    
    /**
     * Return the Current active manual Sessions;
     * @param type
     * @return
     */
    public List<NegotiationSession> getCurrentNegotiations(String type){
       log.debug("Into  getCurrentNegotiations method. Type required:"+type);
       List<NegotiationSession> sessions = null;
       if(this.getNegotiationSessions()!=null && this.getNegotiationSessions().keySet().size()>0){
           sessions = new  ArrayList<NegotiationSession>();
           for(String  key:this.getNegotiationSessions().keySet()){
               //return all the session for the moment
               NegotiationSession  session = this.getNegotiationSessions().get(key);
               //checks that is a session that has to be monitored
               if(session.isWaitingLoop()&& type.equalsIgnoreCase(session.getNegotiationType())){
                   sessions.add(session);
               }
           }           
       }
       return sessions;
    }
    
    /**
     * Return the required NegotiationSesion
     * @param id
     * @return
     */        
     public NegotiationSession getNegotiation(String id){
       log.debug("Into  getNegotiation method. Session required:"+id);
       if(this.getNegotiationSessions()!=null && this.getNegotiationSessions().keySet().size()>0){
            return this.getNegotiationSessions().get(id);           
       }else{
            log.error("NegotiationSessions session object null");
            return null;
       }
     }

}
