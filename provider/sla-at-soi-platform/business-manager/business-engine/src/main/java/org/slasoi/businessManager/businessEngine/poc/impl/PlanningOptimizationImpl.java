/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/poc/impl/PlanningOptimizationImpl.java $
 */

package org.slasoi.businessManager.businessEngine.poc.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.assessment.impl.NegotiationManager;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmCustprodsSlas;
import org.slasoi.businessManager.common.model.EmSlamanagers;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.GtTranslationManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.productSLA.management.SLAManagement;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class PlanningOptimizationImpl implements PlanningOptimization.IAssessmentAndCustomize, SLAMContextAware{
    private static final Logger log = Logger.getLogger(PlanningOptimizationImpl.class);
	//private ProvisioningAdjustment provisioningAdjustment;
	private SLAManagerContext context;
	//this hashMap contains the data of the previous sessions
    private java.util.HashMap<String, NegotiationSession> negotiationSessionHashMap;
    private boolean test= false;
    private SLATemplate templateOrigin;
    private SLAManagement slaManagement;
    //for test purpose
    private ProductOfferManager productOfferService;  
    private CustomerProductManager customerProductManager;
    private ServiceManager serviceManager;
    private GtTranslationManager gtTranslationManager;
  
    public PlanningOptimizationImpl(){
        log.info("Initializating PlaningOptimization");
        negotiationSessionHashMap = new HashMap<String, NegotiationSession>();         
    }
    
    public PlanningOptimizationImpl(boolean test,ProductOfferManager productOfferService,
            CustomerProductManager customerProductManager, ServiceManager serviceManager,
            SLATemplate template){
        log.info("Initializating test PlaningOptimization");
        this.test=true;
        this.templateOrigin = template;
        this.productOfferService = productOfferService;
        this.customerProductManager = customerProductManager;
        this.serviceManager = serviceManager;
        negotiationSessionHashMap = new HashMap<String, NegotiationSession>();         
    }
    
    public void setSLAManagement(SLAManagement slaManagement) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.slaManagement = slaManagement;
    }    
    public void setCustomerProductManager(CustomerProductManager customerProductManager) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.customerProductManager = customerProductManager;
    }    
    public void setProductOfferservice(ProductOfferManager productOfferService) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.productOfferService = productOfferService;
    }    
    public void setServiceManager(ServiceManager serviceManager) {
        log.info("***************Registered Service serviceManager Found*************");
        this.serviceManager = serviceManager;    }
    
    public void setSLAManagerContext( SLAManagerContext context){
        log.info("***************Registered Context Found*************");
        this.context = context;
    }
    
    public void setGtTranslationManager(GtTranslationManager gtTranslationManager) {
        this.gtTranslationManager = gtTranslationManager;
    }

    public java.util.HashMap<String, NegotiationSession> getNegotiationSessionHashMap() {
        return negotiationSessionHashMap;
    }

/**
 * To save the data during the negotiation
 * @param key
 * @param negotiationManager
 */
    public void addToNegotiationHashMap(String key, NegotiationSession negotiationManager) {
        negotiationSessionHashMap.put(key, negotiationManager);
    }
    
   /**
   * To delete the data for a finished negotiation
   * @param key
   */
    public void removeFromNegotiationHashMap(String key){
        negotiationSessionHashMap.remove(key);
    }
    
/**
 * This method negotiate a template    
 */
public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate){
            /****************************************** INegotiationOutgoing Operations follow ****/
            log.info("*** INFO *** PlanningOptimizationImpl:negotiatie ***");
            log.info("negotiationID: "+negotiationID);
            try{            
                if (slaTemplate!= null){  
                    //save data of previous negotiations?
                    NegotiationSession negotiationSession= 
                        negotiationSessionHashMap.get(negotiationID);            
                        if(negotiationSession==null){
                           log.info("It is a new Negotiation. Create negotationSession");
                           negotiationSession = new NegotiationSession();
                           negotiationSession.setNegotiationID(negotiationID);  
                           //add the new session to the array of negotiations 
                           this.negotiationSessionHashMap.put(negotiationID, negotiationSession);
                           //compare with the template from the repository
                           UUID idTemplate = slaTemplate.getUuid();
                           try{
                               SLATemplate template =null;
                               if(test){
                                   log.info("For test purpose use the same template");
                                   template = this.templateOrigin;                                  
                               }else{
                                   log.info("Template given:"+slaTemplate.getUuid().getValue());
                                   if(this.context!=null&&this.context.getSLATemplateRegistry()!=null){
                                       template = this.context.getSLATemplateRegistry().getSLATemplate(idTemplate);
                                   }else{
                                       if(this.context==null){
                                           log.error("Context is null");
                                       }else{
                                           log.error("Template registry could not been retrieved.");
                                       }
                                       return new SLATemplate[0];
                                   }
                               }
                               //the first time, add to proposal sent the one obtained from the identification received
                               SLATemplate[] proposals = {template};
                               //save the identification of the first template was used to begin the negotiation
                               negotiationSession.setOriginalTemplateID(slaTemplate.getUuid());
                               //add the sent proposal to the object.
                               negotiationSession.addToSentProposals(proposals);
                               negotiationSession.setOffersSent(1);
                           }catch(Exception e){
                               log.error("Error: "+ e.getMessage());
                               e.printStackTrace();
                               return new SLATemplate[0];
                           }                           
                        }//else compare with the templates from the negotiation object                                                       
                        //add the proposal received
                        SLATemplate[] proposals = {slaTemplate};
                        negotiationSession.addToReceivedProposals(proposals); 
                        //this object is in charge of managing the negotiation                        
                        NegotiationManager manager = 
                            new NegotiationManager(context, negotiationSession,customerProductManager,
                                    productOfferService,slaManagement,gtTranslationManager,this.test);
                        log.info("Calling Negotiation");
                        SLATemplate[] templates = manager.negotiate(); 
                        if(templates!=null&&templates.length>0){
                            log.info("Proposal receviced: "+templates.length);
                        }else{
                            log.info("No templates to return");
							templates = new SLATemplate[0];
                        }
                    //save data of previous negotiations    
                    this.negotiationSessionHashMap.put(negotiationID, negotiationSession);
                    //return the templates
                    return templates;
                }else{
                    log.info("The template is null.");
                    return new SLATemplate[0];
                }
            }catch(Exception e){ //Catch Exception, log it, and then throw it back to caller.
                log.error(e.getMessage());
                e.printStackTrace();//log e.
                return new SLATemplate[0];
            }       
        }
/**
 * This method create a template.	
 */
	public SLA createAgreement(String negotiationID, SLATemplate slaTemplate){
	    log.info("*** INFO *** PlanningOptimizationImpl:createAgreement***");
	    log.info("negotiationID:"+negotiationID);
	    SLA finalSLA = null;
        try{
            if(negotiationID != null && slaTemplate!= null){
                if(this.negotiationSessionHashMap.containsKey(negotiationID)){
                   //load the corresponding session
                    NegotiationSession negotiationSession= 
                        negotiationSessionHashMap.get(negotiationID);  
                   //add the proposal received
                    SLATemplate[] proposals = {slaTemplate};
                    negotiationSession.addToReceivedProposals(proposals);                    
                    //this object is in charge of managing the negotiation
                    log.info("Calling Negotiation Manager");
                    NegotiationManager manager = 
                        new NegotiationManager(context, negotiationSession,customerProductManager,
                                productOfferService,slaManagement,gtTranslationManager,this.test);                    
                    finalSLA = manager.createAgreement();   
                    if(finalSLA != null){
                        log.info("SLA ID returned:"+finalSLA.getUuid().getValue());
                        //delete the negotiation Session
                        this.removeFromNegotiationHashMap(negotiationID);
                    }else{
                        log.error("Error During Negotiation Process or SLA non valid");
                    }
                    //finally return the SLA                                     
                    return finalSLA;
                }else{
                    log.error("The negotiationID does not exist");
                    return null;
                    //throw new Exception("Negotiation ID does not exist.");//Standardize message.
                }
            }else{
                log.error("NegotiationID or SLA is Null");
                return null;
            }
        //}catch(SLACreationException slaCE){
        }catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();//log e.
            return null;
            //throw slaCE;
        }
	}
/**
 * 
 */
	public SLA provision(UUID slaID) {
	 //This function probably it is not needed. Here it is possible to place the init of the 
	 //charging and other tasks related because we do not have resources to provision
	    log.info("*** INFO *** PlanningOptimizationImpl:provision***");
        log.info("SLAID: "+ slaID.getValue());
	    SLA provisionedSLA = null;
        try{
            if(slaID != null){
                log.info("Provisioning SLA:" + slaID.getValue());
                provisionedSLA = new SLA();
                provisionedSLA.setUuid(slaID);
                return provisionedSLA;
            }else{
                log.error("slsId is null");
                return null;
            }
        //}catch (SLANotFoundException slaNFE) {
        }catch (Exception slaNFE) {    
            log.info(slaNFE.getMessage());
            slaNFE.printStackTrace();//log e.
            //throw slaNFE;
            return null;
        }/*catch(ProvisioningException pE){
            log.info(pE.getMessage());
            pE.printStackTrace();//log e.
            //throw pE;
            return null;
        }*/
	}
/**
 * This method ends a SLA.
 */ 
	public boolean terminate(UUID slaID, List<TerminationReason> terminationReason){
	    log.info("*** INFO *** PlanningOptimizationImpl:terminate***");
        log.info("SLAID: "+ slaID.getValue());
        try{
            if(slaID != null && terminationReason != null){
                log.info("Finishing SLA: "+ slaID.getValue());
                String finalTerminationReason="";
                for(TerminationReason reason:terminationReason){
                    log.info("Termination Reason: " + reason.toString());
                    if("".equalsIgnoreCase(reason.toString())){
                        finalTerminationReason = reason.toString();
                    }else{
                        finalTerminationReason = reason + "|" + terminationReason.toString();
                    }
                }
                //check the contract exist
                UUID[] ids = {slaID};                
                if(!this.test){
                    SLA[]slas = this.context.getSLARegistry().getIQuery().getSLA(ids);                
                    if(slas == null){
                        log.error("SLAID not Found");
                        return false;
                    }
                 }
                 //get Dependencies -->for the future
                 //UUID[] dependencies = this.context.getSLARegistry().getIQuery().getDependencies(slaID);
                 EmCustomersProducts customerProduct = customerProductManager.getCustomerProductByIdLoaded(slaID.getValue());
                 if(customerProduct!=null){
                     Set<EmCustprodsSlas> slas = customerProduct.getEmCustprodsSlases();
                     if(slas!=null && slas.size()>0){
                         Iterator<EmCustprodsSlas> it = slas.iterator();
                         log.debug("Associated Slas: " +slas.size());
                         EmCustprodsSlas custProd;
                         EmSlamanagers manager;
                         while(it.hasNext()){
                                custProd = it.next();
                                manager = custProd.getEmSpServices().getEmSlamanagers();
                                EmSpServices service = serviceManager.getServiceById(custProd.getEmSpServices().getNuServiceid());
                                log.info("Low SLA ID:" + custProd.getId().getTxSlaid());
                                log.info("EPR: "+service.getEmSlamanagers().getTxAddress());
                                if(!test){
                                    if(!context.getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter).
                                        getNegotiationClient(service.getEmSlamanagers().getTxAddress()).terminate(new UUID(custProd.getId().getTxSlaid()), terminationReason)){
                                        log.error("Error ending SLA");
                                        throw new Exception("SLA "+custProd.getId().getTxSlaid()+" has not be able ended");
                                    }
                                }
                          }
                     }
                    
                }else{
                    log.error("SLAID not Found");
                    return false;
                }
                //stopSLA
                /**TODO
                * String source??
                */
                if(!test){
                    if(this.slaManagement.stopSLA(slaID.getValue(), finalTerminationReason, Constants.PARTY_ROLE_CUSTOMER_STRING)<0){
                        log.error("Error stopping SLA: " + slaID.toString());
                        throw new Exception("Error stopping SLA:"+ slaID.toString());                   
                    }                    
                }
                return true;
            }else{
                log.error("slaid or terminationReason is null");
                return false;
            }
        }//catch (SLANotFoundException slaNFE) {
        catch (Exception slaNFE) {
            log.error(slaNFE.getMessage());
            slaNFE.printStackTrace();
            return false;
            //log e.
            //throw slaNFE;
        }
       
	}

}
