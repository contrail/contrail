/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/assessment/impl/NegotiationManager.java $
 */

package org.slasoi.businessManager.businessEngine.assessment.impl;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slasoi.businessManager.businessEngine.assessment.SemiAutomaticChecker;
import org.slasoi.businessManager.businessEngine.helper.Timer;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.SLAEvaluation.SLATEvaluator;
import org.slasoi.businessManager.businessEngine.helper.NegotiationHelper;
import org.slasoi.businessManager.businessEngine.helper.RegistryHelper;
import org.slasoi.businessManager.businessEngine.poc.impl.NegotiationSession;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.GtTranslationManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.util.Cloner;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.util.GTDifference;
import org.slasoi.businessManager.common.util.LowSLAInfo;
import org.slasoi.businessManager.productSLA.management.SLAManagement;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;


public class NegotiationManager {
    private static final Logger log = Logger.getLogger(NegotiationManager.class);
    private NegotiationSession negotiationSession;
    private SLAManagerContext  context;
    private SLATemplate templateorigin;    
    //move to Session object
    //private long customerID;
    private ProductOfferManager productOfferService;  
    private CustomerProductManager customerProductManager;
    private SLAManagement slaManagement;
    private Assessment assessment;
    private GtTranslationManager gtTranslationManager;
    private boolean test = false;
    
    /**
     * Constructor
     * @param context
     * @param negotiationSession
     */
       public NegotiationManager(SLAManagerContext context,NegotiationSession negotiationSession,
               CustomerProductManager customerProductManager,ProductOfferManager productOfferService,
               SLAManagement slaManagement,GtTranslationManager gtTranslationManager,boolean test){           
           this.negotiationSession = negotiationSession;   
           this.customerProductManager = customerProductManager;
           this.productOfferService = productOfferService;
           this.slaManagement = slaManagement;
           this.gtTranslationManager = gtTranslationManager;
           assessment = new Assessment(test);
           assessment.setProductOfferservice(productOfferService);
           this.context = context;
           this.test = test;
       }       
        
/**
 * This method implements the logic of negotiation process  
 * @return
 */
       
    public SLATemplate[] negotiate() throws Exception{
        log.info("Inside negotiate method");
        List<SLATemplate> offers = new ArrayList<SLATemplate>();
        SLATemplate offer= negotiationSession.getSLAOffer();        
        //get last offers sent
        List <SLATemplate> templates = this.negotiationSession.getListOfferSent();
        boolean templateFound = false;
        for(SLATemplate template:templates){
             if(offer.getUuid().getValue().equals(template.getUuid().getValue())){
                 //use the initial template and apply the corresponding promotions
                 this.templateorigin = this.negotiationSession.getSentProposals().get(0);
                 templateFound =true;
             }           
        }
        //check the template origin has been found
        if(!templateFound){
            throw new Exception("The original template has not been found");
        }
        //check the customerID
        long customerID = NegotiationHelper.getCustomerID(offer);
        if( customerID>=0){
           this.negotiationSession.setCustomerId(customerID);
        }else{
            log.error("The template does not have a customer ID");
            return null;
        }
        //find the sla offer associated to evaluate.
        EmSpProductsOffer productOffer = null;
        try{
            //use the original template ID that was saved.
           productOffer = this.productOfferService.getOfferByBslatID(this.negotiationSession.getOriginalTemplateID().getValue());
           
        }catch(Exception ex){
            log.error("Error: "+ ex.toString());
            ex.printStackTrace();
        }
        //check that there is a product offer associated
        if(productOffer==null){
           log.error("there is no associated product offer to the template");
           return null;
        }  
        this.negotiationSession.setRound(this.negotiationSession.getRound()+1);
        this.negotiationSession.setProductOffer(productOffer);
        if(!Constants.STATUS_ACTIVE.equalsIgnoreCase(productOffer.getTxStatus())||
           !Constants.STATUS_ACTIVE.equalsIgnoreCase(productOffer.getEmSpProducts().getTxStatus())){
            
            log.error("The product or product offer asociated to the template is not active");
            return null;
        }
        
      //Get the type of negotiation from the productOffer
        this.negotiationSession.setNegotiationType(productOffer.getEmSpProducts().getTxNegotiationType());
          if(Constants.NEGOTIATION_TYPE_MANUAL.equalsIgnoreCase(this.negotiationSession.getNegotiationType())){
              this.negotiationSession.setWaitingLoop(true);         
          }else  if(Constants.NEGOTIATION_TYPE_SEMI.equalsIgnoreCase(this.negotiationSession.getNegotiationType())){
              //check if the negotiationSession is manual or automatic depends on the conditions
              SemiAutomaticChecker checker = new SemiAutomaticCheckerImpl(this.customerProductManager);
              this.negotiationSession.setWaitingLoop(checker.checkNegotiationType(offer, productOffer.getEmSpProducts()));    
          }
        //End set type of negotiation
        
        
        //first call evaluator to checks the conditions an first assess
        SLATEvaluator evaluator = new SLATEvaluator(offer,templateorigin,null);        
        List<GTDifference> differences = evaluator.evaluateSLAT();
        //evaluate the differences  and customization
        /**TODO check it works fine
         * call the method to apply the policies
         * Evaluate promotions and decide if the offer is valid?
         */          
        EmSpProductsOffer  newOffer = applyPromotions(differences,this.negotiationSession.getCustomerId(), productOffer);
        if(test){
            assessment.setProductOfferservice(productOfferService);
        }
        if(!assessment.assessOffer(this.negotiationSession.getCustomerId(), newOffer)){
            log.debug("This is a non valid offer for this customer");
            /**
             * @TODO si es manual-->pasamos del assessment y dejamos que sea el operador el que decida??
             * 
             */
            return null;
        }
        //call other levels
        //check if the first time looking for previous templates.
        List <LowLayerTemplateInfo> lowLayersOffers;
        if(this.negotiationSession.getLowNegotationOffers()== null ||
                this.negotiationSession.getLowNegotationOffers().get(offer.getUuid().toString())==null){
            log.info("First Negotiation.Obtain templates from bbdd");
            lowLayersOffers = new ArrayList<LowLayerTemplateInfo>();
            //obtain the slats associated
            //how to modify them??-->it is need the templates associated and its SLAMs.               
            Set<EmSpServices> slats= productOffer.getEmSpServiceses();
            Iterator <EmSpServices> iterator = slats.iterator();
            EmSpServices service;
            String swSLATID;
            log.info("Negotiation with los layers SLAMs");
            SLATemplate templateAux;
            LowLayerTemplateInfo templateInfo;
            if(!this.test){
                while(iterator.hasNext()){
                    service = iterator.next();
                    swSLATID = service.getTxSlatid();
                    if(swSLATID!=null&&!"".equalsIgnoreCase(swSLATID)){
                        templateAux = this.context.getSLATemplateRegistry().getSLATemplate(new UUID(swSLATID));
                        log.info("Adding to offers template:" + swSLATID);
                        if(templateAux!=null){
                            templateInfo = new LowLayerTemplateInfo();
                            templateInfo.setServiceID(service.getNuServiceid());
                            templateInfo.setTemplate(templateAux);
                            lowLayersOffers.add(templateInfo);
                        }
                    }                               
                }  
            }
         }else{ //the offers are inside of the negotiation manager object.
             log.info("Not First Negotiation.The templates are inside the negotiation session object.");
             lowLayersOffers = this.negotiationSession.getLowNegotationOffers().get(offer.getUuid().toString());        
         }
         //negotiate with low layer
         SLATemplate aux;
         //lowLayersOffers = null;
         if(lowLayersOffers!=null && lowLayersOffers.size()>0){
             SLATemplate[] templatesNegotiated;
             List<SLATemplate> templatesAux;
             negotiationSession.setLowNegotationTemplates(new java.util.HashMap<String, SLATemplate[]>());
             for(LowLayerTemplateInfo template:lowLayersOffers){
                 log.info("ID SLAT Low layer: "+template.getTemplate().getUuid().toString());
                 templatesAux = new ArrayList<SLATemplate>();
                 //add the template to rebuilt the offer
                 templatesAux.add(offer);
                 //built the low sla offer
                 /**TODO To check that this method works fine.
                  **/
                 NegotiationHelper helper = new NegotiationHelper(gtTranslationManager);
                 aux = helper.reBuiltBslat(Cloner.cloneTemplate(template.getTemplate()), templatesAux,NegotiationHelper.BUILD_TYPE_LOWLAYERS);                                        
                 templatesNegotiated = negotiateLowSLAManager(aux);
                 if(templatesNegotiated != null && templatesNegotiated.length >0){
                     log.debug("Low Templates returned: "+templatesNegotiated.length);
                     System.out.println(templatesNegotiated[0].toString());
                   //use as key serviceID (bbdd id)
                     negotiationSession.getLowNegotationTemplates().put(template.getServiceID().toString(),templatesNegotiated);
                 }else{
                     log.debug("Low Templates returned: 0");
                     return null;
                 }                                  
                 
             }        
         }                      
         //do the next assess taking into account the data received
         //evaluate the differences  and customization   
         log.info("ID SLAT Low layer size: "+ negotiationSession.getLowNegotationTemplates().size());
         //rebuild the BSLAT from low layers offers.
         Set<String> keys = negotiationSession.getLowNegotationTemplates().keySet();         
         if(keys.size()>0){
             //init the hashmap that contains the low layer offers.
             this.negotiationSession.setLowNegotationOffers(new java.util.HashMap<String, List<LowLayerTemplateInfo>>());
             Iterator<String> iteratorKeys= keys.iterator();
             int[] listSize = new int[keys.size()];
             SLATemplate[][] listTemplates = new SLATemplate[keys.size()][];
             //save the service ids to be used later
             String[]serviceKeys = new String[keys.size()];
             int ite = 0;
             int possibilities = 1;
             String auxKey;
             while(iteratorKeys.hasNext()){
                 auxKey = iteratorKeys.next();
                 log.debug("key iterator: "+ite+ " value: "+ auxKey);
                 listTemplates[ite]= negotiationSession.getLowNegotationTemplates().get(auxKey);
                 log.debug("number templates:"+negotiationSession.getLowNegotationTemplates().get(auxKey).length);
                 listSize[ite]= negotiationSession.getLowNegotationTemplates().get(auxKey).length;
                 serviceKeys [ite] = auxKey;
                 possibilities = possibilities*listSize[ite];
                 ite ++;
             }   
             int [][]offerIndex = NegotiationHelper.calculateIndices(listSize);         
             log.info("Quantity of possibilities: "+possibilities);
             //build the list of low templates to rebuild the BSLAT.
             List<List <SLATemplate>>listOffers = new ArrayList<List<SLATemplate>>();
             List<SLATemplate> lowOffer;
             for(int i=0;i<possibilities;i++){
                 lowOffer = new ArrayList<SLATemplate>();
                 for(int j=0;j<listSize.length;j++){
                     //add the corresponding template to the list
                     log.debug("template added:"+listTemplates[j][offerIndex[j][i]].getUuid().getValue());
                     lowOffer.add(listTemplates[j][offerIndex[j][i]]);
                 }
                 //add the offer to the list
                 listOffers.add(lowOffer);
             }
             //rebuilt BSLAT  
              SLATemplate finalOffer;
              List<GTDifference> diffs;
              for(List <SLATemplate> lowOffers:listOffers){
                  //first rebuilt the whole offer from low layer offers
                  //finalOffer = NegotiationHelper.reBuiltBslat(offer, lowOffers);  
                  NegotiationHelper helper = new NegotiationHelper (gtTranslationManager);
                  finalOffer = helper.reBuiltBslat(Cloner.cloneTemplate(offer), lowOffers, NegotiationHelper.BUILD_TYPE_BUSINESS);                     
                  /**TODO only check if it works fine.
                   * call the method to apply the policies
                   * Evaluate promotions
                   * if the offer is valid, add to the list of offers to return.
                   */                    
                  //first call evaluator to checks the conditions and first assess
                  SLATEvaluator evaluation = new SLATEvaluator(offer,templateorigin,null);
                  diffs = evaluation.evaluateSLAT();  
                  //calculate the new offer
                  EmSpProductsOffer productOfferOld = this.productOfferService.getOfferByBslatID(this.negotiationSession.getOriginalTemplateID().getValue());
                  EmSpProductsOffer finalPriceOffer = applyPromotions(diffs,this.negotiationSession.getCustomerId(), productOfferOld);
                  if(test){
                      assessment.setProductOfferservice(productOfferService);
                  }
                  if(!assessment.assessOffer(this.negotiationSession.getCustomerId(), finalPriceOffer)){
                      log.debug("This is a non valid offer for this customer");
                  }else{
                    //if it is a valid offer save all the data associated                    
                    log.debug("It is a valid offer. Adding new offer prices");
                    finalOffer = NegotiationHelper.insertNewProductOffer(finalOffer,finalPriceOffer);
                    if(finalOffer == null){
                        log.error("An error happends adding the new offer prices.");
                    }else{
                        //generate an unique identification
                        finalOffer.setUuid(new UUID(java.util.UUID.randomUUID().toString())); 
                        //save the  low layer offers associated
                        log.debug("Valid offer:Add to the list:" +finalOffer.toString());
                        //Generate the LowLayerTemplateInfo
                        List <LowLayerTemplateInfo> list = new ArrayList<LowLayerTemplateInfo>();
                        LowLayerTemplateInfo info;
                        int s=0;
                        for(SLATemplate offerTemplate:lowOffers){
                            info = new LowLayerTemplateInfo();
                            info.setTemplate(offerTemplate);
                            info.setServiceID(new Long(serviceKeys[s]));
                            list.add(info);
                            s++;
                        }                        
                        this.negotiationSession.getLowNegotationOffers().put(finalOffer.getUuid().toString(), list);                  
                        offers.add(finalOffer);
                    }
                  }
              }
         }else{
             log.info("No offers from low layers");
             log.debug("It is a valid offer. Adding new offer prices");
              //for the moment only negotiation without low layers.
             SLATemplate finalOffer = NegotiationHelper.insertNewProductOffer(Cloner.cloneTemplate(offer),newOffer);
             finalOffer.setUuid(new UUID(java.util.UUID.randomUUID().toString()));             
             offers.add(finalOffer);              
         }
         //finally return the new offers.
         SLATemplate[] finalOffers= null;
        //check if it is manual negotiation
         if(Constants.NEGOTIATION_TYPE_MANUAL.equalsIgnoreCase(this.negotiationSession.getNegotiationType())||
            Constants.NEGOTIATION_TYPE_SEMI.equalsIgnoreCase(this.negotiationSession.getNegotiationType()) ){
            log.debug("###############################ManualNegotiation:Init loop");
            this.negotiationSession.setOfferAsessment(offers);
            Timer.initTime();            
            while(Timer.passedTime()< getNegotiationTimeout() && this.negotiationSession.isWaitingLoop()){
                log.debug("#####################################Into waiting loop");
                Thread.sleep(2000);
            }
            //if there is a response from administrator return it else return null;
            if(!this.negotiationSession.isWaitingLoop()){
                log.debug("#####################################There is a response from the administrator");
                if(this.negotiationSession.getManualOffer()!=null){
                    finalOffers = new SLATemplate[1];
                    finalOffers [0] = this.negotiationSession.getManualOffer();
                    //add the new offerSent
                    this.negotiationSession.addToSentProposals(finalOffers);
                    //set the number of offers sent
                    this.negotiationSession.setOffersSent(finalOffers.length);
                }
                return finalOffers;
            }
            log.debug("#####################################No response. Return null.");
            //if not, clear the session and return null.
            this.negotiationSession.setManualOffer(null);  
            this.negotiationSession.setWaitingLoop(false);
            this.negotiationSession.setOfferAsessment(null);
            return null;
        }else{
            log.debug("###############################Automatic Negotiation");            
        if(offers!=null&offers.size()>0){
          log.info("Offers returned: "+offers.size());
          finalOffers = new SLATemplate[offers.size()];
          offers.toArray(finalOffers);
          //add the new offerSent
          this.negotiationSession.addToSentProposals(finalOffers);
          //set the number of offers sent
          this.negotiationSession.setOffersSent(finalOffers.length);
              this.negotiationSession.setOfferAsessment(offers);
        }        
        return finalOffers;
    }
       
    }
 /**
 * 
 * @param differents
 * @param partyId
 * @param productOfferId
 */
   private EmSpProductsOffer applyPromotions(List<GTDifference> differents,long partyId,EmSpProductsOffer initialProductOffer){
    log.debug("Inside applyPromotions method");
    log.debug("PartyID: " + partyId);
    log.debug("productOfferId: " + initialProductOffer.getNuIdproductoffering());
    try{      
        //apply policies        
        EmSpProductsOffer productOfferToCheck=null;
        if(differents != null && differents.size()>0){
            log.debug("Apply polcies");
            log.debug("Modifation type: "+differents.get(0).getModificationType());
            log.debug("Modifation comparator: "+differents.get(0).getOperationComparator());
            log.debug("Modifation parameter: "+differents.get(0).getParameter());
            log.debug("Modifation percentage: "+differents.get(0).getPercentage());
            log.debug("Modifation service: "+differents.get(0).getServiceName());
            productOfferToCheck = productOfferService.getVariationsByPolicies(initialProductOffer.getNuIdproductoffering(), differents);
        }
        //if no policies to apply.
        if(productOfferToCheck == null){
            productOfferToCheck=initialProductOffer;
        }

        //apply Promotions
        EmCustomersProducts customerProduct = customerProductManager.getVariationsByPromotion(partyId, initialProductOffer.getNuIdproductoffering());
        if(customerProduct!=null){
            log.debug("Promotion applied");
            if(customerProduct.getEmPriceVariations()!=null&& customerProduct.getEmPriceVariations().size()>0){
                log.info("Modifications applied:"+customerProduct.getEmPriceVariations().size());
                //add modifications
                Set<EmComponentPrice> components = productOfferToCheck.getEmComponentPrices();
                Set<EmComponentPrice> newComponents; 
                
                if(components!=null && components.size()>0){
                    newComponents = new HashSet<EmComponentPrice>();
                    Iterator <EmComponentPrice> itComponents = components.iterator();
                    Iterator<EmPriceVariation> it;                   
                    
                    Set<EmPriceVariation> variatations =customerProduct.getEmPriceVariations();
                    
                    while(itComponents.hasNext()){  
                        EmComponentPrice componentPrice = new EmComponentPrice();
                        componentPrice = itComponents.next();
                        log.debug("Component: " +componentPrice.getTxName());
                        it = variatations.iterator();
                        while(it.hasNext()){
                            EmPriceVariation variation = new EmPriceVariation();
                            variation = it.next();
                            log.debug("Component price type:"+componentPrice.getEmPriceType().getNuIdpriceType());
                            log.debug("Variation Desc:" + variation.getTxDescription()+"." +
                                    "Variation price type:"+ variation.getEmComponentPrice().getEmPriceType().getNuIdpriceType());   
                            
                            if(componentPrice.getEmPriceType().getNuIdpriceType().intValue()== 
                                variation.getEmComponentPrice().getEmPriceType().getNuIdpriceType().intValue()){
                                //it the same tipe of component Price
                                log.debug("Component price found:"+componentPrice.getEmPriceType().getNuIdpriceType());
                                if(componentPrice.getEmPriceVariations()==null){
                                    log.debug("no variations. Create list");
                                    Set<EmPriceVariation> var= new HashSet<EmPriceVariation>();
                                    var.add(variation);
                                    componentPrice.setEmPriceVariations(var);
                                }else{
                                    log.debug("Add to the list of variations.Variations previous: "+componentPrice.getEmPriceVariations().size());
                                    componentPrice.getEmPriceVariations().add(variation);
                                }                            
                            }                     
                        }   
                        if(componentPrice.getEmPriceVariations()!=null && componentPrice.getEmPriceVariations().size()>0){
                            log.debug("With modifications");
                        }else{
                            log.debug("WithOut modifications");
                        }
                        newComponents.add(componentPrice);
                    }
                    //set the new components.
                    productOfferToCheck.setEmComponentPrices(newComponents);
                }
            }else{
                log.debug("Without modifications");
            }        
        }
        return productOfferToCheck;
     }catch(Exception e){
        log.error("Error: "+e.toString());
        e.printStackTrace();
        return null;
     }
  }
    
 /**
  * Negotiation with other SLA Managers.   
  * @param template
  * @param BusinessSLAT
  * @return
  */
    private SLATemplate[] negotiateLowSLAManager(SLATemplate template){
        log.info("Inside negotiateLowSLAManager method");
        // negotiation with other layers
        try{
            String endpoint = NegotiationHelper.obtainEndPoint(template);
            if(endpoint!=null && !"".equalsIgnoreCase(endpoint)){
                log.info("EndPoint obtained: "+endpoint);
            }else{
              log.error("EndPoint can not be null");
              throw new Exception("Endpoint null"); 
            }
            //check that the template has customer party(BM) and if not, add it.
            template = NegotiationHelper.checkAndItroduceCustomerParty(template);
            /**TODO
             * Use a combination of parent negotiationSession, endpoint and serviceID. 
             */
            String key = this.negotiationSession.getNegotiationID()+"|"+endpoint+"|"+template.getInterfaceDeclrs()[0].getId().toString();            
            NegotiationSession negotiationSession= 
                this.negotiationSession.getLowNegotationSession().get(key);            
            if(negotiationSession==null){
                log.info("No negotiationSession.Create One");
                //Obtain the endPoint from the SLAManagerID
                log.info("It is a new Negotiation. Create negotationSession");
                //Obtain the template??
                negotiationSession = new NegotiationSession();     
                //String SWNegotiationID ="";
                String SWNegotiationID  = context.getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter).
                                         getNegotiationClient(endpoint).initiateNegotiation(template);
                if(SWNegotiationID!=null &&  !"".equalsIgnoreCase(SWNegotiationID)){
                    log.info("Low negotiationID returned:"+SWNegotiationID);
                    negotiationSession.setNegotiationID(SWNegotiationID);
                }else{
                    log.error("No negotiationID returned:");
                    return null;
                }                                
                
            }
            //call the sw SLAManager to negotiate
            SLATemplate[] templatesSW = context.getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter).
                    getNegotiationClient(endpoint).negotiate(negotiationSession.getNegotiationID(),template);            
            //save the templates
            negotiationSession.addToReceivedProposals(templatesSW);
            negotiationSession.setOffersSent(templatesSW.length);
            //save the session
            this.negotiationSession.getLowNegotationSession().put(key, negotiationSession);
            return templatesSW;
        }catch(Exception e){
            log.error("Error:"+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
 /**
  * This method implements the logic of create agreement process
  * @return SLA generated
  */
    public SLA createAgreement() throws Exception{
        log.info("Inside createAgreement method");
        try{
            SLATemplate offer= negotiationSession.getSLAOffer();        
            //get last offers sent
            List <SLATemplate> templates = this.negotiationSession.getListOfferSent();
            for(SLATemplate template:templates){
                if(offer.getUuid().getValue().equals(template.getUuid().getValue())){
                    this.templateorigin = template;
                }           
            }
            //check the template origin has been found
            if(this.templateorigin == null){
                throw new Exception("The origin template has not been found");
            }        
            //check there is not any change. Then generate the agreement
            SLATEvaluator evaluator = new SLATEvaluator(offer,templateorigin,null);
            List<GTDifference> differences = evaluator.evaluateSLAT();
            if(differences!=null&&differences.size()>0){
                throw new Exception("Changes againts the templates are not allowed");
            } 
            //look for the offer associated to the template given.
            EmSpProductsOffer productOffer = null;
            try{
                //use the original template ID that was saved.
               productOffer = this.productOfferService.getOfferByBslatID(this.negotiationSession.getOriginalTemplateID().getValue());
            }catch(Exception ex){
                log.error("Error: "+ ex.toString());
            }
            //check that there is a product offer associated
            if(productOffer==null){
               log.error("there is no associated product offer to the template");
               return null;
            }  
            if(!Constants.STATUS_ACTIVE.equalsIgnoreCase(productOffer.getTxStatus())){
              log.error("The product offer asociated to the template is not active");
              return null;
            }                            
            //call other levels
            //obtain the slats associated
            List<LowLayerTemplateInfo> offers = null;
            if(this.negotiationSession.getLowNegotationOffers()!= null &&
                    this.negotiationSession.getLowNegotationOffers().get(offer.getUuid().toString())!=null){
                offers = this.negotiationSession.getLowNegotationOffers().get(this.templateorigin.getUuid().toString());
            }
            //No modifications--> are the same negotiated in the past.
            log.info("CreateAgreement:Negotiation with los layers SLAMs");
            NegotiationSession lowNegotiationSession;
            List<SLA> lowSLAs =  new ArrayList <SLA>();
            List<LowSLAInfo> lowSLAInfo = new ArrayList<LowSLAInfo>();
            SLATEvaluator lowLEvelevaluator;
            List<GTDifference> lowDifferences;
            String endpoint;
            try{
              if(offers!=null&&offers.size()>0){
                  LowSLAInfo lowInfo;
                  for(LowLayerTemplateInfo lowOffer:offers){
                      log.info("CreateAgreement:ID SLAT Low layer: "+lowOffer.getTemplate().getUuid().toString());                      
                      //createAgreement
                      //first obtain the endpoint
                      endpoint = NegotiationHelper.obtainEndPoint(lowOffer.getTemplate());
                      if(endpoint!=null && !"".equalsIgnoreCase(endpoint)){
                          log.info("CreateAgreement:EndPoint obtained: "+endpoint);
                      }else{
                        log.error("CreateAgreement:EndPoint can not be null");
                        throw new Exception("CreateAgreement:Endpoint null"); 
                      }
                      //Obtain the key to get the low negotiation data.
                      /**
                       * @TODO check that the key wroks fine.
                       */
                      String key = this.negotiationSession.getNegotiationID()+"|"+endpoint+"|"+lowOffer.getTemplate().getInterfaceDeclrs()[0].getId().toString(); 
                      //obtain the associated negotiation session
                      lowNegotiationSession = negotiationSession.getLowNegotationSession().get(key); 
                      if(lowNegotiationSession==null){
                          log.error("CreateAgreement:Low Negotiation Session Not Found");
                          throw new Exception("CreateAgreement:Low Negotiation Session Not Found"); 
                      }
                      log.info("CreateAgreement:Low negotiation ID:"+lowNegotiationSession.getNegotiationID());
                      SLATemplate SLAtemplateOffer = NegotiationHelper.checkAndItroduceCustomerParty(lowOffer.getTemplate());
                      SLA otherLevelSLA = context.getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter).
                          getNegotiationClient(endpoint).createAgreement(lowNegotiationSession.getNegotiationID(), SLAtemplateOffer);
                      //compare with the offers sent, if there is any error, error!!
                      //cancel all previous agreements and return exception 
                    //add to provisional list
                      lowInfo = new LowSLAInfo();
                      lowInfo.setServiceID(lowOffer.getServiceID());
                      lowInfo.setSLATID(otherLevelSLA.getUuid().toString());
                      lowSLAs.add(otherLevelSLA);   
                      lowSLAInfo.add(lowInfo);
                      //Evaluate if there is any difference to cancel the SLA
                      lowLEvelevaluator = new SLATEvaluator(null,lowOffer.getTemplate(),otherLevelSLA);
                      lowDifferences = lowLEvelevaluator.evaluateSLA();
                      if(lowDifferences!=null && lowDifferences.size()>0){
                          log.error("CreateAgreement:There are differeces agaisnt the offered template and the SLA returned");
                          throw new Exception("CreateAgreement:SLA and SLAT must not be different"); 
                      }
                      //add the endpoints received from low layers to the original one
                      addEndpointsFinalSLA(offer,otherLevelSLA);
                      
                  }
              }else{
                  log.info("CreateAgreement:There are not low layer templates associated."); 
              } 
            }catch(Exception e){
                log.error("Error"+e.toString());
                 e.printStackTrace();
                 //reason to finalize the agreement.
                 List<TerminationReason> terminationReason = new ArrayList<TerminationReason>();
                 terminationReason.add(TerminationReason.BUSINESS_DECISION);
                 //terminate all the previous SLAs
                 if(lowSLAs!=null && lowSLAs.size()>0)
                     log.error("CreateAgreement:Error:"+e.toString());
                   for(SLA lowSLA:lowSLAs){
                      log.error("CreateAgreement:Ending SLA: " + lowSLA.getUuid());
                      if( ! context.getSyntaxConverters().get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter).
                       getNegotiationClient(NegotiationHelper.obtainEndPoint(lowSLA)).terminate(lowSLA.getUuid(), terminationReason)){
                          log.error("CreateAgreement:The SLA "+lowSLA.getUuid()+ "can not be terminated");
                      }                 
                 }
                 return null;
            }
              //return the SLA built from the SLAT and setting what necessary terms? 
              //set the original template UUID in order to know where the agreement come from.
              offer.setUuid(this.negotiationSession.getOriginalTemplateID());
              SLA finalSLA = RegistryHelper.buildSLAfromSLAT(offer);            
              //startSLA
             if(!test){
                 log.info("Starting and Saving SLA: " + finalSLA.getUuid().toString());
                 if(this.slaManagement.startSLA(finalSLA,lowSLAInfo)<0){
                     log.error("Error starting SLA: " + finalSLA.getUuid().toString());
                     throw new Exception("Error starting SLA:"+ finalSLA.getUuid().toString());
                 }
                  //save the SLA
                  RegistryHelper.storeSLA(finalSLA, context, lowSLAs);                 
             }
             return finalSLA;
        }catch(Exception e){
            log.equals("Error:"+e.toString());
            e.printStackTrace();
            return null;            
        }
    }
 
  /**
  * this method add the endpoints received from the new SLAs.    
  * @param offer
  * @param otherLevelSLA
  */
    private void addEndpointsFinalSLA(SLATemplate offer,SLA sla){
      log.info("Adding new Endpoints from SLA:"+sla.getUuid());       
      try{  
     // Extract the endpoint from the sla
        InterfaceDeclr[] interfaces = sla.getInterfaceDeclrs();
        if (interfaces != null && interfaces.length > 0) {
            for(InterfaceDeclr ifz:interfaces){
                Endpoint[] endpoints = ifz.getEndpoints();
                if (endpoints != null && endpoints.length > 0) {
                    log.debug("Setting enpoints from:"+ifz.getId().getValue());
                    offer.getInterfaceDeclr(ifz.getId().getValue()).setEndpoints(endpoints);
                }
            }
        }   
      }catch (Exception e){
          log.error("Error adding new Endpoints");
          e.printStackTrace();
      }
   }
 /**
  * Get timeout from property file   
  * @return
  */
   private long getNegotiationTimeout(){
       log.info("Into getNegotiationTimeout method"); 
       try{
           String propertiesFile= "provider.properties";
           String CONFIG_DIR =
               System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "BusinessManager"
                + System.getProperty("file.separator"); 
           Properties properties = new Properties();
           properties.load(new FileInputStream(CONFIG_DIR + propertiesFile));
           long value = new Long(properties.getProperty("negotiation.timeout")).longValue(); 
           log.info("Timeout value: " +value);  
           return value;
       }catch(Exception e){
           log.error("Error: " +e.toString());
           e.printStackTrace();
           return 1000;           
       }
   }
    
}
