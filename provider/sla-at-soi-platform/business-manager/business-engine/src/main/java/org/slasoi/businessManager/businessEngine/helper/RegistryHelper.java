/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/helper/RegistryHelper.java $
 */

package org.slasoi.businessManager.businessEngine.helper;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class RegistryHelper {
    private static final Logger log = Logger.getLogger(RegistryHelper.class);
  
    /**
     * This method obtain the template from the register.
     * @param templateId
     * @param context
     * @return
     * @throws Exception
     */
   public static SLATemplate getTemplate(UUID templateId, SLAManagerContext  context) throws Exception{
       return context.getSLATemplateRegistry().getSLATemplate(templateId);
   }
   
/**
 * This method store a SLA into the SLA Register.   
 * @param sla
 * @param context
 * @param lowSLAs
 * @throws Exception
 */
   public static void storeSLA(SLA sla, SLAManagerContext  context,List<SLA> lowSLAs) throws Exception{
       log.info("Saving SLA: " +sla.getUuid().toString());
       UUID[] dependencies = null;
       if(lowSLAs!=null&&lowSLAs.size()>0){
           dependencies = new UUID[lowSLAs.size()];
           int index=0;
           for(SLA lowSLA:lowSLAs){
               dependencies [index] = lowSLA.getUuid();
               index++;
           }              
       }
       context.getSLARegistry().getIRegister().register(sla, dependencies, SLARegistry.SLAState.OBSERVED);
   }
/**
 * This method generate an SLA from the negotiated template.   
 * @param template
 * @return
 */
   public static SLA buildSLAfromSLAT(SLATemplate template){
       log.info("Generating SLA from Template: "+ template.getUuid().toString());
       /**TODO how built the SLA from the SLATemplate.
        **/
       SLA finalSLA = new SLA();
       finalSLA.setUuid(new UUID(java.util.UUID.randomUUID().toString()));
       finalSLA.setTemplateId(template.getUuid());
       finalSLA.setParties(template.getParties());
       finalSLA.setAgreementTerms(template.getAgreementTerms());
       finalSLA.setInterfaceDeclrs(template.getInterfaceDeclrs());
       finalSLA.setVariableDeclrs(template.getVariableDeclrs());
       /**TODO Revise!!!! 
        * effective from and effective
       ***/
       GregorianCalendar agreedAt = new GregorianCalendar();
       GregorianCalendar effectiveFrom = new GregorianCalendar();
       GregorianCalendar effectiveUntil = new GregorianCalendar();
       //for the momment add one year and start from the instant it is created.
       effectiveUntil.add(1, 1);
       finalSLA.setAgreedAt(new TIME(agreedAt));
       finalSLA.setEffectiveFrom(new TIME(effectiveFrom));
       finalSLA.setEffectiveUntil(new TIME(effectiveUntil));
          
       return finalSLA;
   }

}
