/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/poc/impl/NegotiationSession.java $
 */

package org.slasoi.businessManager.businessEngine.poc.impl;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slasoi.businessManager.businessEngine.assessment.impl.LowLayerTemplateInfo;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.gslam.core.negotiation.INegotiation.CancellationReason;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class NegotiationSession {
    
    private String negotiationID; //This is the NegotiationSessionID
    private long customerId;
    private int round =0; //negotiation round
    private EmSpProductsOffer productOffer;
    private SessionStatus status;    
    private Timestamp creationTime;
    private List<SLATemplate> receivedProposals;
    private List<SLATemplate> sentProposals;
    private List<Party> involvedParties;
    private List<CancellationReason> cancellationReasons;
    private List<TerminationReason> terminationReasons;
    private int offersSent;
    private SLA agreedSLA;
    private UUID originalTemplateID;
    private String endpointAddress;
    private java.util.HashMap<String,SLATemplate[]> lowNegotationTemplates;
    private java.util.HashMap<String,NegotiationSession> lowNegotationSession;
    private java.util.HashMap<String,List<LowLayerTemplateInfo>> lowNegotationOffers;
    private List <SLATemplate> offerAsessment = null;
    private SLATemplate manualOffer = null;
    private boolean waitingLoop = false;
    private String negotiationType = "A";    
    
    public NegotiationSession(){
        creationTime = new Timestamp(System.currentTimeMillis());
        receivedProposals = new ArrayList<SLATemplate>();
        sentProposals = new ArrayList<SLATemplate>();
        involvedParties = new ArrayList<Party>();
        cancellationReasons = new ArrayList<CancellationReason>();
        terminationReasons = new ArrayList<TerminationReason>();
        status = SessionStatus.AVAILABLE;      
        lowNegotationTemplates = new  java.util.HashMap<String,SLATemplate[]>();
        lowNegotationSession = new java.util.HashMap<String,NegotiationSession>();
    }         
    
          
    public SLATemplate getManualOffer() {
        return manualOffer;
    }

    public void setManualOffer(SLATemplate manualOffer) {
        this.manualOffer = manualOffer;
    }

    public List<SLATemplate> getOfferAsessment() {
        return offerAsessment;
    }

    public void setOfferAsessment(List<SLATemplate> offerAsessment) {
        this.offerAsessment = offerAsessment;
    }    

    public boolean isWaitingLoop() {
        return waitingLoop;
    }

    public void setWaitingLoop(boolean waitingLoop) {
        this.waitingLoop = waitingLoop;
    }
   
    public String getNegotiationType() {
        return negotiationType;
    }

    public void setNegotiationType(String negotiationType) {
        this.negotiationType = negotiationType;
    }


    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public long getCustomerId() {
        return customerId;
    }
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }
    
    public EmSpProductsOffer getProductOffer() {
        return productOffer;
    }
    public void setProductOffer(EmSpProductsOffer productOffer) {
        this.productOffer = productOffer;
    }
    
    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    //contains the low layer offers associated to a business Offer.
    public java.util.HashMap<String, List<LowLayerTemplateInfo>> getLowNegotationOffers() {
        return lowNegotationOffers;
    }

    public void setLowNegotationOffers(java.util.HashMap<String, List<LowLayerTemplateInfo>> lowNegotationOffers) {
        this.lowNegotationOffers = lowNegotationOffers;
    }

    //Orignal UUID of the template the negotiation is based
    public UUID getOriginalTemplateID() {
        return originalTemplateID;
    }

    public void setOriginalTemplateID(UUID originalTemplateID) {
        this.originalTemplateID = originalTemplateID;
    }

    public java.util.HashMap<String, SLATemplate[]> getLowNegotationTemplates() {
        return lowNegotationTemplates;
    }

    public void setLowNegotationTemplates(java.util.HashMap<String, SLATemplate[]> lowNegotationTemplates) {
        this.lowNegotationTemplates = lowNegotationTemplates;
    }

    public String getEndpointAddress() {
        return endpointAddress;
    }


    public void setEndpointAddress(String endpointAddress) {
        this.endpointAddress = endpointAddress;
    }
/**
 * Acces to low Negotatiation Sessions
 * @return
 */
    public java.util.HashMap<String, NegotiationSession> getLowNegotationSession() {
        return lowNegotationSession;
    }

    public void setLowNegotationSession(java.util.HashMap<String, NegotiationSession> lowNegotationSession) {
        this.lowNegotationSession = lowNegotationSession;
    }

/**
 * This method return the last offers sent to the customer     
 * @return
 */
    public List<SLATemplate> getListOfferSent(){
        if (this.offersSent>0){
            return this.sentProposals.
                subList(this.sentProposals.size()-this.offersSent,this.sentProposals.size());
        }else{
            return null;
        }                 
    }
    
    public List<SLATemplate> getSentProposals() {
        return sentProposals;
    }
    public SLATemplate getSLAOffer(){
        return this.receivedProposals.get(this.receivedProposals.size()-1);
    }
    
    public int getOffersSent() {
        return offersSent;
    }

    public void setOffersSent(int offersSent) {
        this.offersSent = offersSent;
    }   
    
    public SLA getAgreedSLA() {
        return agreedSLA;
    }

    public void setAgreedSLA(SLA agreedSLA) {
        this.agreedSLA = agreedSLA;
    }

    public String getNegotiationID() {
        return negotiationID;
    }
    public void setNegotiationID(String negotiationID) {
        this.negotiationID = negotiationID;
    }
    public void generateAndSetUuid(){
        this.negotiationID = java.util.UUID.randomUUID().toString();
    }
    public void addToReceivedProposals(SLATemplate[] slaTemplates){
        for(SLATemplate slaTemplate : slaTemplates){
            receivedProposals.add(slaTemplate);
        }
    }
    public void addToSentProposals(SLATemplate[] slaTemplates){
        for(SLATemplate slaTemplate : slaTemplates){
            sentProposals.add(slaTemplate);
        }
    }   
    public SessionStatus getStatus() {
        return status;
    }
    public void setStatus(SessionStatus status) {
        this.status = status;
    }
    public List<Party> getInvolvedParties() {
        return involvedParties;
    }
    public void setInvolvedParties(List<Party> involvedParties) {
        this.involvedParties = involvedParties;
    }
    public void addToInvolvedParties(SLATemplate slaTemplate){
        for(Party party : slaTemplate.getParties()){
            this.involvedParties.add(party);
        }
        System.out.println("Inside NegotiationSession.addToInvolvedParties - Count = "+ this.involvedParties.size());
    }
    public List<CancellationReason> getCancellationReasons() {
        return cancellationReasons;
    }

    public void setCancellationReasons(List<CancellationReason> cancellationReasons) {
        this.cancellationReasons = cancellationReasons;
    }

    public List<TerminationReason> getTerminationReasons() {
        return terminationReasons;
    }

    public void setTerminationReasons(List<TerminationReason> terminationReasons) {
        this.terminationReasons = terminationReasons;
    }
    
    public SLATemplate getOriginalTemplate(){
        if(this.sentProposals !=null && this.sentProposals.size()>0){
            return this.sentProposals.get(0);
        }
        return null;        
    }
}

