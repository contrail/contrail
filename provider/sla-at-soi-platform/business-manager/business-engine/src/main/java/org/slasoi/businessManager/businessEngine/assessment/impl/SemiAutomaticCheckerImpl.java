package org.slasoi.businessManager.businessEngine.assessment.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.assessment.SemiAutomaticChecker;
import org.slasoi.businessManager.common.drools.DroolsManager;
import org.slasoi.businessManager.common.drools.Rule;
import org.slasoi.businessManager.common.drools.RuleParam;
import org.slasoi.businessManager.common.drools.Template;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.slamodel.sla.SLATemplate;




public class SemiAutomaticCheckerImpl  implements SemiAutomaticChecker{
    static Logger log = Logger.getLogger(SemiAutomaticCheckerImpl.class);
    private CustomerProductManager customerProductService;
    
    public SemiAutomaticCheckerImpl(CustomerProductManager customerProductService){
        this.customerProductService = customerProductService;
    }
 
    /**
     * This method checks if a semiautomaticNegotiation has to be manual or automatic  
     * @param template
     * @param product
     * @return true --> Manual negotiation
     */
    public boolean checkNegotiationType(SLATemplate template, EmSpProducts product){
        log.info("Into checkNegotiationType method. Product ID:"+product.getTxProductname());
        try{
          //Prepare the WorkingMemory, add the ComponentPrice at Facts
            ArrayList<Object> arrayFacts = new ArrayList<Object>();
            //load the different rules to apply.   
            HashMap<Long, Template> templates = this.loadRules();
            List <EmCustomersProducts> contracts= customerProductService.getCustomerProductsByProduct(product);
            int numberContracts =0;
            if(contracts !=null && contracts.size()>0){
                log.debug("Number of contracts:" +contracts.size());
                numberContracts  = contracts.size(); 
            }
            product.setTxProductdesc("false");
            arrayFacts.add(product); 
            arrayFacts.add(new Long(numberContracts));
            //Call the rule
            log.debug("Array Facts >>> "+arrayFacts.size());
            //Create DroolsManager, with array templates and facts
            log.debug("Generating Drools Manager");
            DroolsManager dm = new DroolsManager(templates.values(), arrayFacts);
            log.debug("Process rules");
            dm.processRules();
            log.debug("Result:"+product.getTxProductdesc()); 
            return new Boolean(product.getTxProductdesc()).booleanValue();
        }catch(Exception e){
            log.error("Error:"+e.toString());
            e.printStackTrace();
            return false;
        }
    }
   
 /**
  * This method load the corresponding rules to apply
  * @return
  */
   private HashMap<Long, Template> loadRules(){
       log.debug("Into loadRules method.");
       HashMap<Long, Template> templates = new HashMap<Long, Template>();       
       //
       //Template template = templates.get(ruleTplt.getNuRuleTemplateId());
       //Currently we have only a template.
       Template template = templates.get(new Long(1));
       if(template==null){
           //template = new Template(ruleTplt.getNuRuleTemplateId(), policyType.getTxName(), ruleTplt.getTxDescription(), ruleTplt.getTxTemplateDrt());
           template = new Template(new Long(1), "Check SemiAtuomatic Negotiation", "Check SemiAtuomatic Negotiation Description",getRule());
           templates.put(template.getId(), template);
       }
       
       ArrayList<RuleParam> ruleParams = new ArrayList<RuleParam>();            
       //RuleParam rp = new RuleParam(promoValue.getEmRuleParams().getNuParamRuleOrder(), promoValue.getTxParamValue());
       //Generate the specific parameters for this rule
       RuleParam rp = new RuleParam(new Long(1), Constants.NEGOTIATION_TYPE_SEMI);
       ruleParams.add(rp);
       //Number of contracts --> initially 0
       RuleParam rp2 = new RuleParam(new Long(2),"0");
       ruleParams.add(rp2);
       //Specific Rule and its parameters
       Rule rule = new Rule(new Long(1),"Check SemiAtuomatic", "Check SemiAtuomatic", ruleParams);
       template.getRules().add(rule);       
       return templates;          
   }

/**
 * Get the string with the template rule from a file.
 * @return
 */
    private String getRule(){
        log.info("Get Rule from drl file"); 
        String rule ="";
        String ruleFileName = "/rule.drl";;
        BufferedReader br = null;

        try {
           br = new BufferedReader(new InputStreamReader(SemiAutomaticCheckerImpl.class.getResourceAsStream(ruleFileName)));                              
           String linea;
           while((linea=br.readLine())!=null){
              rule += linea + "\n";
           }
        }
        catch(Exception e){
           e.printStackTrace();
        }
        return rule;
    
    }
    
}
