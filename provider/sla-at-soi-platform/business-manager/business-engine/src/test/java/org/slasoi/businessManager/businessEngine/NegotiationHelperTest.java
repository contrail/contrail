/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/test/java/org/slasoi/businessManager/businessEngine/NegotiationHelperTest.java $
 */

package org.slasoi.businessManager.businessEngine;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slasoi.businessManager.businessEngine.helper.NegotiationHelper;
import org.slasoi.businessManager.common.model.EmBillingFrecuency;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmPriceType;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmPriceVariationType;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Product;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;

public class NegotiationHelperTest extends TestCase {
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public NegotiationHelperTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( NegotiationHelperTest.class );
    }
    
    private EmSpProductsOffer genereateOfferPrice(){
        EmSpProductsOffer offer = new EmSpProductsOffer();
        offer.setDtValidFrom(new Date());
        offer.setDtValidTo(new Date());
        EmBillingFrecuency emBillingFrecuency = new EmBillingFrecuency();
        emBillingFrecuency.setTxName("per_month");
        offer.setEmBillingFrecuency(emBillingFrecuency);
        offer.setNuIdproductoffering(new Long(1).longValue());
        offer.setTxName("Gold");
        offer.setTxStatus("A");
        Set<EmComponentPrice> emComponentPrices = new HashSet<EmComponentPrice>();
        EmComponentPrice price = new EmComponentPrice();
        EmPriceType emPriceType= new EmPriceType();
        emPriceType.setTxName("one_time_charge");
        price.setEmPriceType(emPriceType);
        price.setNuPrice(new BigDecimal(1.5));
        EmCurrencies emCurrencies = new EmCurrencies();
        emCurrencies.setTxName("EUR");
        price.setEmCurrencies(emCurrencies);
        price.setNuQuantity(new BigDecimal(1));
        price.setTxName("one time charge");
        Set<EmPriceVariation> emPriceVariations = new HashSet<EmPriceVariation>();
        EmPriceVariation variation = new EmPriceVariation();
        variation.setNuValue(new BigDecimal(2));
        EmPriceVariationType emPriceVariationType = new EmPriceVariationType();
        emPriceVariationType.setTcModificationType("I");
        emPriceVariationType.setTcUnitType("P");
        variation.setEmPriceVariationType(emPriceVariationType);                      
        emPriceVariations.add(variation);
        //second variation
        EmPriceVariation variation2 = new EmPriceVariation();
        variation2.setNuValue(new BigDecimal(3));
        EmPriceVariationType emPriceVariationType2 = new EmPriceVariationType();
        emPriceVariationType2.setTcModificationType("D");
        emPriceVariationType2.setTcUnitType("F");
        variation2.setEmPriceVariationType(emPriceVariationType2);
        emPriceVariations.add(variation2);
        price.setEmPriceVariations(emPriceVariations);
        emComponentPrices.add(price);
        
        EmComponentPrice price2 = new EmComponentPrice();
        EmPriceType emPriceType2= new EmPriceType();
        emPriceType2.setTxName("montly");
        price2.setEmPriceType(emPriceType);
        price2.setNuPrice(new BigDecimal(10));
        EmCurrencies emCurrencies2 = new EmCurrencies();
        emCurrencies2.setTxName("US");
        price2.setEmCurrencies(emCurrencies2);
        price2.setNuQuantity(new BigDecimal(8));
        price2.setTxName("monthly");
        emComponentPrices.add(price2);
        offer.setEmComponentPrices(emComponentPrices);
        return offer;
    }
 /**
  *    
  */
    public void testObtainEndPoint(){
        System.out.println("test testObtainEndPoint");  
        SLATemplate template = new SLATemplate();
        Party[] parties = new Party[2];
        Party party = new Party(new ID("id"),sla.customer);
        parties[0] = party;
        Party party2 = new Party(new ID("id"),sla.provider);
        party2.setPropertyValue(sla.gslam_epr, "http://localhost:8080");
        parties[1] = party2;
        template.setParties(parties);
        String endpoint = NegotiationHelper.obtainEndPoint(template);
        System.out.println("EndPoint: " +endpoint);
        
    }
    /**
     *   Test conversion among differentTypes of OfferPrice     
     */
           public void testProductOfferingConversion(){
               try{
                   System.out.println("test testProductOfferingConversion");                 
                   EmSpProductsOffer offer = genereateOfferPrice();  
                   ProductOfferingPrice productOfferPrice =NegotiationHelper.convertToProductOfferingPrice(offer);
                   System.out.println(productOfferPrice.toString());                   
               }catch(Exception e){
                   e.printStackTrace();
               }
               
           }
           /**
            * Test insertPrice
            */
           public void testInsertPriceOffer(){
               System.out.println("test testInsertPriceOffer");    
               try{
               EmSpProductsOffer offer = genereateOfferPrice();
               SLATemplate template = new SLATemplate();

               AgreementTerm[] terms = new AgreementTerm[1];
               Guaranteed[] guarantees = new Guaranteed[1];
               ComponentProductOfferingPrice[] cpops = new ComponentProductOfferingPrice[1];
               ComponentProductOfferingPrice compo = 
                   new ComponentProductOfferingPrice(new ID("5"), business.one_time_charge,
                           new CONST("2",units.EUR), new CONST("1",null));
               cpops[0] = compo;
               Product prod = new Product(new ID("1"),"myProduct");
               ProductOfferingPrice productOfferPrice = new ProductOfferingPrice(new ID("id2"),"name", 
                       new TIME(new GregorianCalendar()),new TIME(new GregorianCalendar()),
                       business.per_day,prod, cpops);
               Expr[] params = new Expr[1];
               params [0]= new ID("hola");
               EventExpr pre = new EventExpr(common.accuracy, params);
               Guaranteed guarantee = new Action(new ID("id"),sla.consumer,sla.mandatory, pre, productOfferPrice);
               guarantees[0]=guarantee;
               AgreementTerm term = new  AgreementTerm(new ID("ID"), null, null,guarantees);
               terms[0]= term;
               template.setAgreementTerms(terms);
               System.out.println(template.toString());
               template = NegotiationHelper.insertNewProductOffer(template, offer);
               System.out.println(template.toString());
               }catch(Exception e){
                   e.printStackTrace();
               }
           }
}

