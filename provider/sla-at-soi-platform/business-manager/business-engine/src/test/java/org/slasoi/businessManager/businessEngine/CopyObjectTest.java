package org.slasoi.businessManager.businessEngine;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slasoi.businessManager.common.util.Cloner;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;


import eu.slaatsoi.slamodel.SLATemplateDocument;


/**
 * Unit test for simple App.
 */
public class CopyObjectTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CopyObjectTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CopyObjectTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testCloneTemplate()
    { try{
            System.out.println("###########Init testCloneTemplate  test");
            SLATemplateDocument slaTemplateXml =
            SLATemplateDocument.Factory.parse(CopyObjectTest.class
                    .getResourceAsStream("/IaaS_GOLD_new.xml"));    
            SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();
               
            SLATemplate slaTemplate = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());
            System.out.println("############dir:"+slaTemplate.getAgreementTerms());
            System.out.println(slaTemplate.toString());            
            SLATemplate slaTemplate2 = Cloner.cloneTemplate(slaTemplate);
            System.out.println("############dir:"+slaTemplate2.getAgreementTerms());
            System.out.println(slaTemplate2.toString());
            
      }catch(Exception e){
         e.printStackTrace();
      }
    }
}
