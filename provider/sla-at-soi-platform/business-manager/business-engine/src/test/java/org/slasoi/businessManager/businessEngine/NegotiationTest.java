/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/test/java/org/slasoi/businessManager/businessEngine/NegotiationTest.java $
 */

package org.slasoi.businessManager.businessEngine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slasoi.businessManager.businessEngine.poc.impl.PlanningOptimizationImpl;
import org.slasoi.businessManager.common.model.EmBillingFrecuency;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmCustprodsSlas;
import org.slasoi.businessManager.common.model.EmCustprodsSlasId;
import org.slasoi.businessManager.common.model.EmPriceType;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmPriceVariationType;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Product;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.springframework.test.AbstractTransactionalSpringContextTests;

import eu.slaatsoi.slamodel.SLATemplateDocument;


/**
 * Unit test for simple App.
 */
public class NegotiationTest  extends AbstractTransactionalSpringContextTests{
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-businessengine-context.xml"};
    }
    @Resource
    private CustomerProductManager customerProductManager;
    
    @Resource
    private ProductOfferManager productOfferManager;
    
    @Resource
    private ServiceManager serviceManager;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public NegotiationTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( NegotiationTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testNegotiation()
    { System.out.println("Init testNegotiation");
      try{        
        
        SLATemplateDocument slaTemplateXml =
            SLATemplateDocument.Factory.parse(NegotiationTest.class
                    .getResourceAsStream("/ORC_Business_SLAT.xml"));    
        SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();               
        SLATemplate slaTemplate = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());
        Party[] parties = new Party[1];
        Party party = new Party();
        party.setId(new ID("1"));
        party.setDescr("party");
        party.setAgreementRole(new STND("customer"));
        parties[0] = party;
        slaTemplate.setParties(parties);
        ///insert offerPrice
        EmSpProductsOffer offer = genereateOfferPrice();
        AgreementTerm[] termsAux = slaTemplate.getAgreementTerms();
        AgreementTerm[] terms = new AgreementTerm[termsAux.length+1];
        for(int i=0;i<termsAux.length;i++ ){
            terms[i]= termsAux[i];
        }
        Guaranteed[] guarantees = new Guaranteed[1];
        ComponentProductOfferingPrice[] cpops = new ComponentProductOfferingPrice[1];
        ComponentProductOfferingPrice compo = 
            new ComponentProductOfferingPrice(new ID("5"), business.one_time_charge,
                    new CONST("2",units.EUR), new CONST("1",null));
        cpops[0] = compo;
        Product prod = new Product(new ID("1"),"myProduct");
        ProductOfferingPrice productOfferPrice = new ProductOfferingPrice(new ID("id2"),"name", 
                new TIME(new GregorianCalendar()),new TIME(new GregorianCalendar()),
                business.per_day,prod, cpops);
        Expr[] params = new Expr[1];
        params [0]= new ID("hola");
        EventExpr pre = new EventExpr(common.accuracy, params);
        Guaranteed guarantee = new Action(new ID("id"),sla.consumer,sla.mandatory, pre, productOfferPrice);
        guarantees[0]=guarantee;
        AgreementTerm term = new  AgreementTerm(new ID("ID"), null, null,guarantees);
        terms[terms.length-1]= term;
        slaTemplate.setAgreementTerms(terms);
        //first negotiate the templates. 
        //These functions will be called by the protocol engine
        //the uuid for negotiation purpose will be given by the protocol engine.   
        SLATemplateDocument slaTemplateOriXml =
            SLATemplateDocument.Factory.parse(NegotiationTest.class
                    .getResourceAsStream("/ORC_Business_SLAT_ori.xml"));             
        SLATemplate slaTemplateOri = slasoiTemplateParser.parseTemplate(slaTemplateOriXml.xmlText());
        PlanningOptimizationImpl poc = new PlanningOptimizationImpl(true,productOfferManager,customerProductManager,serviceManager,slaTemplateOri);
        String negotiationID = java.util.UUID.randomUUID().toString();
        System.out.println("NegotiationID:"+negotiationID);
        SLATemplate[] templates = poc.negotiate(negotiationID, slaTemplate);
        assertNotNull(templates);
        System.out.println("template returned:"+templates.length);
        UUID slaid = null;
        if(templates.length>0){
            System.out.println("tamplateID: "+templates[0].getUuid().getValue());
            System.out.println("RETURNED TEMPLATE");
            System.out.println(templates[0].toString());
            //once the template have been negotiated, call the createAgreement operation
            System.out.println("Call CreateAgreement operation");
            SLA finalSLA = poc.createAgreement(negotiationID, templates[0]);
            assertNotNull(finalSLA);
            System.out.println("RETURNED SLA");
            System.out.println(finalSLA.toString());
            slaid = finalSLA.getUuid();
        }
        //finally terminate the agreement when no more will be used.
         List<TerminationReason> terminationReason = new ArrayList<TerminationReason>();
         terminationReason.add(TerminationReason.BUSINESS_DECISION);
         poc.terminate(slaid, terminationReason);
        //test error createAgreement without negotationID
         System.out.println("Return null if the negotiationID does not exist");
         SLA sla = poc.createAgreement("id", slaTemplate);
         assertNull(sla);
         System.out.println("SLA:" + sla);
      }catch(Exception e){
         e.printStackTrace();
      }
    }
    
    public void testTermination()
    { System.out.println("Init testTermination");
      try{
          EmCustomersProducts customerProduct = new EmCustomersProducts();
          customerProduct.setTxBslaid("txBslaidTest");
          customerProduct.setDtDateBegin(new Date());
          customerProduct.setDtDateEnd(new Date());
          customerProduct.setDtInsertDate(new Date());
          customerProduct.setState("ACTIVE");
          EmSpProductsOffer emSpProductsOffer = new EmSpProductsOffer();
          emSpProductsOffer.setNuIdproductoffering(new Long(1));
          customerProduct.setEmSpProductsOffer(emSpProductsOffer);
          Set<EmCustprodsSlas> emCustprodsSlases = new HashSet<EmCustprodsSlas>();
          EmCustprodsSlas sla1= new EmCustprodsSlas();
          EmSpServices emSpServices = new EmSpServices();
          emSpServices.setNuServiceid(new Long(1));
          sla1.setEmSpServices(emSpServices);
          EmCustprodsSlasId id= new EmCustprodsSlasId();
          id.setTxBslaid("txBslaidTest");
          id.setTxSlaid("txslaidTest1");
          sla1.setId(id);
          //sla2
          EmCustprodsSlas sla2= new EmCustprodsSlas();
          EmSpServices emSpServices2 = new EmSpServices();
          emSpServices2.setNuServiceid(new Long(2));
          sla2.setEmSpServices(emSpServices2);
          EmCustprodsSlasId id2= new EmCustprodsSlasId();
          id2.setTxBslaid("txBslaidTest");
          id2.setTxSlaid("txslaidTest2");
          sla2.setId(id2);
          emCustprodsSlases.add(sla1);
          emCustprodsSlases.add(sla2);
          customerProduct.setEmCustprodsSlases(emCustprodsSlases);
          customerProductManager.saveCustomer(customerProduct);
          PlanningOptimizationImpl poc = new PlanningOptimizationImpl(true,productOfferManager,customerProductManager,serviceManager,null);
          List<INegotiation.TerminationReason> teminationReasons = new ArrayList<INegotiation.TerminationReason>();
          teminationReasons.add(TerminationReason.BUSINESS_DECISION);
          boolean result = poc.terminate(new UUID("txBslaidTest"),teminationReasons);
          System.out.println("result: "+ result);
      }catch(Exception e){
          System.out.println("Error:" + e.toString());
          e.printStackTrace();
      }
    }  
    
    private EmSpProductsOffer genereateOfferPrice(){
        EmSpProductsOffer offer = new EmSpProductsOffer();
        offer.setDtValidFrom(new Date());
        offer.setDtValidTo(new Date());
        EmBillingFrecuency emBillingFrecuency = new EmBillingFrecuency();
        emBillingFrecuency.setTxName("per_month");
        offer.setEmBillingFrecuency(emBillingFrecuency);
        offer.setNuIdproductoffering(new Long(1).longValue());
        offer.setTxName("Gold");
        offer.setTxStatus("A");
        Set<EmComponentPrice> emComponentPrices = new HashSet<EmComponentPrice>();
        EmComponentPrice price = new EmComponentPrice();
        EmPriceType emPriceType= new EmPriceType();
        emPriceType.setTxName("one_time_charge");
        price.setEmPriceType(emPriceType);
        price.setNuPrice(new BigDecimal(1.5));
        EmCurrencies emCurrencies = new EmCurrencies();
        emCurrencies.setTxName("EUR");
        price.setEmCurrencies(emCurrencies);
        price.setNuQuantity(new BigDecimal(1));
        price.setTxName("one time charge");
        Set<EmPriceVariation> emPriceVariations = new HashSet<EmPriceVariation>();
        EmPriceVariation variation = new EmPriceVariation();
        variation.setNuValue(new BigDecimal(2));
        EmPriceVariationType emPriceVariationType = new EmPriceVariationType();
        emPriceVariationType.setTcModificationType("I");
        emPriceVariationType.setTcUnitType("P");
        variation.setEmPriceVariationType(emPriceVariationType);                      
        emPriceVariations.add(variation);
        //second variation
        EmPriceVariation variation2 = new EmPriceVariation();
        variation2.setNuValue(new BigDecimal(3));
        EmPriceVariationType emPriceVariationType2 = new EmPriceVariationType();
        emPriceVariationType2.setTcModificationType("D");
        emPriceVariationType2.setTcUnitType("F");
        variation2.setEmPriceVariationType(emPriceVariationType2);
        emPriceVariations.add(variation2);
        price.setEmPriceVariations(emPriceVariations);
        emComponentPrices.add(price);
        offer.setEmComponentPrices(emComponentPrices);
        return offer;
    }
}

