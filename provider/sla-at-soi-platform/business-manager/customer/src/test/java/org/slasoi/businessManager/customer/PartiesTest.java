/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 496 $
 * @lastrevision   $Date: 2011-01-25 17:05:43 +0100 (Tue, 25 Jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/customer/src/test/java/org/slasoi/businessManager/customer/PartiesTest.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.customer;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.service.CountryService;
import org.slasoi.businessManager.common.service.CurrencyService;
import org.slasoi.businessManager.common.service.LanguageService;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.PartyPartyRoleManager;
import org.slasoi.businessManager.common.service.PartyRoleManager;
import org.slasoi.businessManager.common.service.UserService;
import org.slasoi.businessManager.customer.impl.PartyWS;
import org.slasoi.businessManager.common.ws.types.AuthenticateUserResponseType;
import org.slasoi.businessManager.common.ws.types.CreatePartyResponseType;
import org.slasoi.businessManager.common.ws.types.GetParameterListResponseType;
import org.slasoi.businessManager.common.ws.types.IndividualType;
import org.slasoi.businessManager.common.ws.types.OrganizationType;
import org.slasoi.businessManager.common.ws.types.PartyType;
import org.slasoi.businessManager.common.ws.types.UserType;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * Unit test for simple App.
 */
public class PartiesTest extends AbstractTransactionalSpringContextTests {
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-customer-context.xml" };
    }

    /**
     * Create the test case
     * 
     * @param testName
     *            name of the test case
     */
    public PartiesTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(PartiesTest.class);
    }

    @Resource
    private CountryService countryService;
    @Resource
    private CurrencyService currencyService;
    @Resource
    private LanguageService languageService;
    @Resource
    private PartyManager partyService;
    @Resource
    private PartyRoleManager partyRoleService;
    @Resource
    private UserService userService;
    @Resource
    private PartyPartyRoleManager partyPartyRoleManager;

    // private static String[] files = new String[] {"spring-config.xml"};
    // final ApplicationContext context = new ClassPathXmlApplicationContext(files);
    // final CountryService countryService = (CountryService)context.getBean("countryService");
    // final CurrencyService currencyService = (CurrencyService)context.getBean("currencyService");
    // final LanguageService languageService = (LanguageService)context.getBean("languageService");
    // final PartyManager partyService = (PartyManager)context.getBean("partyService");;
    // final PartyRoleManager partyRoleService = (PartyRoleManager)context.getBean("partyRoleService");;
    // final UserService userService = (UserService)context.getBean("userService");

    /**
     * Test get parameter list countries.
     */
    public void testGetParameterListCountries() {
        try {
            System.out.println("PartyWS, Test method getParameterList, parameter:Country");
            PartyWS partyWS = new PartyWS();
            partyWS.setCountryService(countryService);
            GetParameterListResponseType respCountry = partyWS.getParameterList(Constants.PARTY_WS_COUNTRY);
            System.out.println("Response code: " + respCountry.getResponseCode() + " Response message: "
                    + respCountry.getResponseMessage());
            // for(ParameterDataType dataType:respCountry.getParameterList()){
            // System.out.println(dataType.getParameterId() + " ---" + dataType.getParameterName());
            // }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test get parameter list languages.
     */
    public void testGetParameterListLanguages() {
        try {
            System.out.println("PartyWS, Test method getParameterList, parameter:Language");
            PartyWS partyWS = new PartyWS();
            partyWS.setLanguageService(languageService);
            GetParameterListResponseType resp = partyWS.getParameterList(Constants.PARTY_WS_LANGUAGE);
            System.out.println("Response code: " + resp.getResponseCode() + " Response message: "
                    + resp.getResponseMessage());
            // for(ParameterDataType dataType:resp.getParameterList()){
            // System.out.println(dataType.getParameterId() + " ---" + dataType.getParameterName());
            // }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test get parameter list currencies.
     */
    public void testGetParameterListCurrencies() {
        try {
            System.out.println("PartyWS, Test method getParameterList, parameter:Currency");
            PartyWS partyWS = new PartyWS();
            partyWS.setCurrencyService(currencyService);
            GetParameterListResponseType resp = partyWS.getParameterList(Constants.PARTY_WS_CURRENCY);
            System.out.println("Response code: " + resp.getResponseCode() + " Response message: "
                    + resp.getResponseMessage());
            // for(ParameterDataType dataType:resp.getParameterList()){
            // System.out.println(dataType.getParameterId() + " ---" + dataType.getParameterName());
            // }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test is authenticated.
     */
    public void testIsAuthenticated() {
        try {
            System.out.println("PartyWS, Test method isAuthenticated, parameter:david/david");
            PartyWS partyWS = new PartyWS();
            partyWS.setUserService(userService);
            AuthenticateUserResponseType response = partyWS.isAuthenticated("david", "david");
            System.out.println("Response: " + response.getResponseCode() + " " + response.getResponseMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test is authenticated.
     */
    public void testIsAuthenticatedOK() {
        try {
            System.out.println("PartyWS, Test method isAuthenticated, parameter:userKO/passKO");
            PartyWS partyWS = new PartyWS();
            partyWS.setUserService(userService);
            AuthenticateUserResponseType response = partyWS.isAuthenticated("userKO", "passKO");
            System.out.println("Response: " + response.getResponseCode() + " " + response.getResponseMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test create party.
     */
    public void testCreateParty() {
        try {

            System.out.println("PartyWS, Test method createParty");
            PartyWS partyWS = new PartyWS();
            partyWS.setPartyService(partyService);
            partyWS.setPartyRoleService(partyRoleService);
            partyWS.setUserService(userService);
            PartyType party = new PartyType();
            party.setCurrencyId(1);
            IndividualType individual = new IndividualType();
            individual.setAddress("address");
            individual.setEmail("22");
            individual.setFax("fax");
            individual.setFirstName("firstName");
            individual.setLastName("lastName");
            individual.setJobdepartment("jobdepartment");
            individual.setJobtitle("jobtitle");
            individual.setPhoneNumber("phoneNumber");
            individual.setCountryId(1);
            individual.setLanguageId(1);
            party.setIndividual(individual);
            OrganizationType organization = new OrganizationType();
            organization.setDescription("description");
            organization.setFiscalId("fiscalId");
            organization.setTradingName("tradingName");
            IndividualType[] individuals = new IndividualType[1];
            individuals[0] = individual;
            UserType[] users = new UserType[1];
            UserType a = new UserType();
            a.setPasswd("passwd");
            a.setUserLogin("userLogin");
            users[0] = a;
            party.setUsers(users);
            organization.setIndividuals(individuals);
            party.setOrganization(organization);
            // CreatePartyResponseType response3 = partyWS.createParty("I",party);
            // System.out.println("Response: "+ response3.getResponseCode() + " " + response3.getResponseMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test create party.
     */
    public void testCreatePartymIO() {
        try {

            System.out.println("PartyWS, Test method createParty");
            PartyWS partyWS = new PartyWS();
            partyWS.setPartyService(partyService);
            partyWS.setPartyRoleService(partyRoleService);
            partyWS.setUserService(userService);
            partyWS.setPartyPartyRoleService(partyPartyRoleManager);
            PartyType party = new PartyType();
            party.setCurrencyId(1);
            OrganizationType organization = new OrganizationType();
            organization.setDescription("description");
            organization.setFiscalId("fiscalId");
            organization.setTradingName("SLASOI B5");
            party.setOrganization(organization);
            UserType userType = new UserType();
            userType.setUserLogin("B5User");
            userType.setPasswd("lala");
            UserType[] users = new UserType[1];
            users[0] = userType;
            party.setUsers(users);
            CreatePartyResponseType response3 = partyWS.createCustomer(Constants.PARTY_ORGANIZATION, party);
            System.out.println("Response: " + response3.getResponseCode() + " " + response3.getResponseMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
