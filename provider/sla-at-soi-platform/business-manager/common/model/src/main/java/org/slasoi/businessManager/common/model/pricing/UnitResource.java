package org.slasoi.businessManager.common.model.pricing;

// Generated May 24, 2013 4:52:11 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * UnitResource generated by hbm2java
 */
public class UnitResource implements java.io.Serializable {

	private long unitResourceId;
	private PricedItem pricedItem;
	private String unitResourceIstanceId;
	private String unitResourceType;
	private String unitResourceUnit;
	private Set<CompoundResource> compoundResources = new HashSet<CompoundResource>(0);

	public UnitResource() {
	}

	public UnitResource(String unitResourceType) {
		this.unitResourceType = unitResourceType;
	}

	public UnitResource(PricedItem pricedItem, String unitResourceIstanceId, String unitResourceType, String unitResourceUnit, Set<CompoundResource> compoundResources) {
		this.pricedItem = pricedItem;
		this.unitResourceIstanceId = unitResourceIstanceId;
		this.unitResourceType = unitResourceType;
		this.unitResourceUnit = unitResourceUnit;
		this.compoundResources = compoundResources;
	}

	public long getUnitResourceId() {
		return this.unitResourceId;
	}

	public void setUnitResourceId(long unitResourceId) {
		this.unitResourceId = unitResourceId;
	}

	public PricedItem getPricedItem() {
		return this.pricedItem;
	}

	public void setPricedItem(PricedItem pricedItem) {
		this.pricedItem = pricedItem;
	}

	public String getUnitResourceIstanceId() {
		return this.unitResourceIstanceId;
	}

	public void setUnitResourceIstanceId(String unitResourceIstanceId) {
		this.unitResourceIstanceId = unitResourceIstanceId;
	}

	public String getUnitResourceType() {
		return this.unitResourceType;
	}

	public void setUnitResourceType(String unitResourceType) {
		this.unitResourceType = unitResourceType;
	}

	public String getUnitResourceUnit() {
		return this.unitResourceUnit;
	}

	public void setUnitResourceUnit(String unitResourceUnit) {
		this.unitResourceUnit = unitResourceUnit;
	}

	public Set<CompoundResource> getCompoundResources() {
		return this.compoundResources;
	}

	public void setCompoundResources(Set<CompoundResource> compoundResources) {
		this.compoundResources = compoundResources;
	}

}
