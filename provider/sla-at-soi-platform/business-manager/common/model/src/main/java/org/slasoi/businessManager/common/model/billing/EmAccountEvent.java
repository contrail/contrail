/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.common.model.billing;

import java.math.BigDecimal;
import java.util.Date;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmCurrencies;

public class EmAccountEvent implements java.io.Serializable{
    
    private Long nuEventId;
    private EmAccountEvent paymentEvent;
    private char tcType;
    private EmPartyBank emPartyBank;
    private EmParty vendor;
    private EmParty payer;
    private EmAccountEventType emAccountEventType;
    private EmCurrencies currency;
    private Date tsTimeStamp; 
    private String txBslaId;
    private String txPenaltyId;
    private BigDecimal  nuAmount;
    private String txDescription;
    
    
    public Long getNuEventId() {
        return nuEventId;
    }
    public void setNuEventId(Long nuEventId) {
        this.nuEventId = nuEventId;
    }         
    public EmAccountEvent getPaymentEvent() {
        return paymentEvent;
    }
    public void setPaymentEvent(EmAccountEvent paymentEvent) {
        this.paymentEvent = paymentEvent;
    }
    public char getTcType() {
        return tcType;
    }
    public void setTcType(char tcType) {
        this.tcType = tcType;
    }
    public EmPartyBank getEmPartyBank() {
        return emPartyBank;
    }
    public void setEmPartyBank(EmPartyBank emPartyBank) {
        this.emPartyBank = emPartyBank;
    }
    public EmParty getVendor() {
        return vendor;
    }
    public void setVendor(EmParty vendor) {
        this.vendor = vendor;
    }
    public EmParty getPayer() {
        return payer;
    }
    public void setPayer(EmParty payer) {
        this.payer = payer;
    }
    public EmAccountEventType getEmAccountEventType() {
        return emAccountEventType;
    }
    public void setEmAccountEventType(EmAccountEventType emAccountEventType) {
        this.emAccountEventType = emAccountEventType;
    }    
    public EmCurrencies getCurrency() {
        return currency;
    }
    public void setCurrency(EmCurrencies currency) {
        this.currency = currency;
    }
    public Date getTsTimeStamp() {
        return tsTimeStamp;
    }
    public void setTsTimeStamp(Date tsTimeStamp) {
        this.tsTimeStamp = tsTimeStamp;
    }
    public String getTxBslaId() {
        return txBslaId;
    }
    public void setTxBslaId(String txBslaId) {
        this.txBslaId = txBslaId;
    }
    public String getTxPenaltyId() {
        return txPenaltyId;
    }
    public void setTxPenaltyId(String txPenaltyId) {
        this.txPenaltyId = txPenaltyId;
    }
    public BigDecimal getNuAmount() {
        return nuAmount;
    }
    public void setNuAmount(BigDecimal nuAmount) {
        this.nuAmount = nuAmount;
    }
    public String getTxDescription() {
        return txDescription;
    }
    public void setTxDescription(String txDescription) {
        this.txDescription = txDescription;
    }

    
    
    
}