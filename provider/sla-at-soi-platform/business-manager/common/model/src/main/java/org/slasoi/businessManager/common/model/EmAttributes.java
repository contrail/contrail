/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/model/EmAttributes.java $
 */

package org.slasoi.businessManager.common.model;
// Generated 22-abr-2010 16:32:31 by Hibernate Tools 3.2.0.beta8


import java.util.HashSet;
import java.util.Set;

/**
 * EmAttributes generated by hbm2java
 */
public class EmAttributes  implements java.io.Serializable {

    // Fields    

     private Long nuAttributeid;
     private String txName;
     private String txDescription;
     private Long nuMinValue;
     private Long nuMaxValue;
     private Set emSpServSatRateAgrs = new HashSet(0);
     private Set emSpServSatisfactionRates = new HashSet(0);
     private Set emSpServiceses = new HashSet(0);

     // Constructors

    /** default constructor */
    public EmAttributes() {
    }

	/** minimal constructor */
    public EmAttributes(Long nuAttributeid, String txName) {
        this.nuAttributeid = nuAttributeid;
        this.txName = txName;
    }
    /** full constructor */
    public EmAttributes(Long nuAttributeid, String txName, String txDescription, Long nuMinValue, Long nuMaxValue, Set emSpServSatRateAgrs, Set emSpServSatisfactionRates, Set emSpServiceses) {
       this.nuAttributeid = nuAttributeid;
       this.txName = txName;
       this.txDescription = txDescription;
       this.nuMinValue = nuMinValue;
       this.nuMaxValue = nuMaxValue;
       this.emSpServSatRateAgrs = emSpServSatRateAgrs;
       this.emSpServSatisfactionRates = emSpServSatisfactionRates;
       this.emSpServiceses = emSpServiceses;
    }
   
    // Property accessors
    public Long getNuAttributeid() {
        return this.nuAttributeid;
    }
    
    public void setNuAttributeid(Long nuAttributeid) {
        this.nuAttributeid = nuAttributeid;
    }
    public String getTxName() {
        return this.txName;
    }
    
    public void setTxName(String txName) {
        this.txName = txName;
    }
    public String getTxDescription() {
        return this.txDescription;
    }
    
    public void setTxDescription(String txDescription) {
        this.txDescription = txDescription;
    }
    public Long getNuMinValue() {
        return this.nuMinValue;
    }
    
    public void setNuMinValue(Long nuMinValue) {
        this.nuMinValue = nuMinValue;
    }
    public Long getNuMaxValue() {
        return this.nuMaxValue;
    }
    
    public void setNuMaxValue(Long nuMaxValue) {
        this.nuMaxValue = nuMaxValue;
    }
    public Set getEmSpServSatRateAgrs() {
        return this.emSpServSatRateAgrs;
    }
    
    public void setEmSpServSatRateAgrs(Set emSpServSatRateAgrs) {
        this.emSpServSatRateAgrs = emSpServSatRateAgrs;
    }
    public Set getEmSpServSatisfactionRates() {
        return this.emSpServSatisfactionRates;
    }
    
    public void setEmSpServSatisfactionRates(Set emSpServSatisfactionRates) {
        this.emSpServSatisfactionRates = emSpServSatisfactionRates;
    }
    public Set getEmSpServiceses() {
        return this.emSpServiceses;
    }
    
    public void setEmSpServiceses(Set emSpServiceses) {
        this.emSpServiceses = emSpServiceses;
    }




}


