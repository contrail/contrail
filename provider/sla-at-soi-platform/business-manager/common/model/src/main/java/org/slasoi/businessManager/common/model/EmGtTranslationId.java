package org.slasoi.businessManager.common.model;

public class EmGtTranslationId implements java.io.Serializable {
    
    private String txBslatId;
    private String txSlatId;
    private String txMixAgreedId;
    private String txMixAgreementTermId;    
    
    public String getTxBslatId() {
        return txBslatId;
    }
    public void setTxBslatId(String txBslatId) {
        this.txBslatId = txBslatId;
    }
    public String getTxSlatId() {
        return txSlatId;
    }
    public void setTxSlatId(String txSlatId) {
        this.txSlatId = txSlatId;
    }
    public String getTxMixAgreedId() {
        return txMixAgreedId;
    }
    public void setTxMixAgreedId(String txMixAgreedId) {
        this.txMixAgreedId = txMixAgreedId;
    }
    public String getTxMixAgreementTermId() {
        return txMixAgreementTermId;
    }
    public void setTxMixAgreementTermId(String txMixAgreementTermId) {
        this.txMixAgreementTermId = txMixAgreementTermId;
    }
 
    
}
