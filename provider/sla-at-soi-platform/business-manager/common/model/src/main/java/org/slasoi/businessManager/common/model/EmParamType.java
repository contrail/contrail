/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/model/EmParamType.java $
 */

package org.slasoi.businessManager.common.model;

// Generated 26-ene-2010 12:24:58 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * EmParamType generated by hbm2java
 */
public class EmParamType implements java.io.Serializable {

	private Long idParamType;
	private String txParamTypeName;
	private String txParamTypeViewName;
	private String txParamValues;
	//If paramTypeName = SELECT, load values of asociated Table
	private List listValues;
	
	private Set<EmParams> emParamses = new HashSet<EmParams>(0);

	public EmParamType() {
	}

	public EmParamType(Long idParamType, String txParamTypeName) {
		this.idParamType = idParamType;
		this.txParamTypeName = txParamTypeName;
	}

	public EmParamType(Long idParamType, String txParamTypeName,
			Set<EmParams> emParamses) {
		this.idParamType = idParamType;
		this.txParamTypeName = txParamTypeName;
		this.emParamses = emParamses;
	}

	public Long getIdParamType() {
		return this.idParamType;
	}

	public void setIdParamType(Long idParamType) {
		this.idParamType = idParamType;
	}

	public String getTxParamTypeName() {
		return this.txParamTypeName;
	}

	public void setTxParamTypeName(String txParamTypeName) {
		this.txParamTypeName = txParamTypeName;
	}
	
	public String getTxParamTypeViewName() {
		return txParamTypeViewName;
	}

	public void setTxParamTypeViewName(String txParamTypeViewName) {
		this.txParamTypeViewName = txParamTypeViewName;
	}

	public Set<EmParams> getEmParamses() {
		return this.emParamses;
	}

	public void setEmParamses(Set<EmParams> emParamses) {
		this.emParamses = emParamses;
	}

	public String getTxParamValues() {
		return txParamValues;
	}

	public void setTxParamValues(String txParamValues) {
		this.txParamValues = txParamValues;
	}

	public List getListValues() {
		return listValues;
	}

	public void setListValues(List listValues) {
		this.listValues = listValues;
	}

}
