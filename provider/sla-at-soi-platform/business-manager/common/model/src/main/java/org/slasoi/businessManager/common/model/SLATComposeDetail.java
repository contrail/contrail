package org.slasoi.businessManager.common.model;

import java.util.ArrayList;
import java.util.List;
import org.slasoi.businessManager.common.model.EmGtTranslation;

import org.slasoi.slamodel.sla.SLATemplate;

public class SLATComposeDetail {
        
    SLATemplate composedTemplate;
    List<EmGtTranslation> gtTranslations;
   
    public SLATComposeDetail(){
        gtTranslations = new ArrayList<EmGtTranslation>();
    }
    
    public SLATemplate getComposedTemplate() {
        return composedTemplate;
    }
    public void setComposedTemplate(SLATemplate composedTemplate) {
        this.composedTemplate = composedTemplate;
    }
    public List<EmGtTranslation> getGtTranslations() {
        return gtTranslations;
    }
    public void setGtTranslations(List<EmGtTranslation> gtTranslations) {
        this.gtTranslations = gtTranslations;
    } 
    
    

}
