package org.slasoi.businessManager.common.model;

public class EmGtTranslation implements java.io.Serializable {
    
    private EmGtTranslationId id;
    private String txAgreedId;
    private String txAgreementTermId;
    
    public EmGtTranslationId getId() {
        return id;
    }
    public void setId(EmGtTranslationId id) {
        this.id = id;
    }
    public String getTxAgreedId() {
        return txAgreedId;
    }
    public void setTxAgreedId(String txAgreedId) {
        this.txAgreedId = txAgreedId;
    }
    public String getTxAgreementTermId() {
        return txAgreementTermId;
    }
    public void setTxAgreementTermId(String txAgreementTermId) {
        this.txAgreementTermId = txAgreementTermId;
    }


    

}
