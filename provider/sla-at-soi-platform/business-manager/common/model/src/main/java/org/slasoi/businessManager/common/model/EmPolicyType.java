/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/model/EmPolicyType.java $
 */

package org.slasoi.businessManager.common.model;

// Generated 15-feb-2010 12:45:28 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

/**
 * EmPolicyType generated by hbm2java
 */
public class EmPolicyType implements java.io.Serializable {

	private Long nuPolicyTypeId;
	private EmRuleTemplates emRuleTemplates;
	private String txName;
	private String txDescription;
	private Set<EmPolicies> emPolicieses = new HashSet<EmPolicies>(0);

	public EmPolicyType() {
	}

	
	public EmPolicyType(Long nuPolicyTypeId) {
		super();
		this.nuPolicyTypeId = nuPolicyTypeId;
	}
	
	
	public EmPolicyType(Long nuPolicyTypeId, String txName) {
		super();
		this.nuPolicyTypeId = nuPolicyTypeId;
		this.txName = txName;
	}


	public EmPolicyType(EmRuleTemplates emRuleTemplates, String txName,
			String txDescription, Set<EmPolicies> emPolicieses) {
		this.emRuleTemplates = emRuleTemplates;
		this.txName = txName;
		this.txDescription = txDescription;
		this.emPolicieses = emPolicieses;
	}

	public Long getNuPolicyTypeId() {
		return this.nuPolicyTypeId;
	}

	public void setNuPolicyTypeId(Long nuPolicyTypeId) {
		this.nuPolicyTypeId = nuPolicyTypeId;
	}

	public EmRuleTemplates getEmRuleTemplates() {
		return this.emRuleTemplates;
	}

	public void setEmRuleTemplates(EmRuleTemplates emRuleTemplates) {
		this.emRuleTemplates = emRuleTemplates;
	}

	public String getTxName() {
		return this.txName;
	}

	public void setTxName(String txName) {
		this.txName = txName;
	}

	public String getTxDescription() {
		return this.txDescription;
	}

	public void setTxDescription(String txDescription) {
		this.txDescription = txDescription;
	}

	public Set<EmPolicies> getEmPolicieses() {
		return this.emPolicieses;
	}

	public void setEmPolicieses(Set<EmPolicies> emPolicieses) {
		this.emPolicieses = emPolicieses;
	}

}
