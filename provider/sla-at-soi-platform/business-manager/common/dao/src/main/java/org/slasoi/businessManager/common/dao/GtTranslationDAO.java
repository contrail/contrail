package org.slasoi.businessManager.common.dao;

import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;


public interface GtTranslationDAO extends AbstractHibernateDAO<EmGtTranslation, EmGtTranslationId>{
 
  /**
  * Get the translation for the given term 
  * @param Type
  * @param translstion
  * @return
  */
    public abstract EmGtTranslation getGtTranslation(String type, EmGtTranslation translation);
}
