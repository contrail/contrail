/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/PartyDAOImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.PenaltyDAO;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value = "penaltyDAO")
public class PenaltyDAOImpl extends AbstractHibernateDAOImpl<EmPenalty, Long> implements PenaltyDAO {

    private static final Logger logger = Logger.getLogger(PenaltyDAOImpl.class);

    @Autowired
    public PenaltyDAOImpl(SessionFactory factory) {
        setSessionFactory(factory);
    }

    @Override
    protected Class<EmPenalty> getDomainClass() {
        return EmPenalty.class;
    }

    public List<EmPenalty> getPenaltiesByViolationsSlatIdOrderedByDate(String slaId, boolean asc){
        return getPenaltiesOrderedByDate(slaId,asc);
    }
    
    public List<EmPenalty> getPenaltiesOrderedByDate(String slaId, boolean asc) {
        
        Criteria crit = null;
        
        logger.info("PenaltyDAOImpl,getPenaltiesOrderedByDate");
        
        crit = getSession(false).createCriteria(EmPenalty.class);        
        crit.createCriteria("violation").add(Restrictions.like("slaId", slaId));
        
        if (asc){
            crit.addOrder(Order.asc("penaltyDate"));
        }else{
            crit.addOrder(Order.desc("penaltyDate"));
        }
                
        return  (List<EmPenalty>)crit.list();
    }

    public List<EmPenalty> getPenaltiesByViolationsSlatIdAndDateBetween(String slaId, Date startDate, Date endDate) {

        /*
          DBManager.createCriteria(EmPenalty.class)
            .add(Restrictions.between("penaltyDate", startDate, endDate)).addOrder(
                  Order.asc("penaltyDate")).createCriteria("violation").add(
                    Restrictions.eq("slaId", id));
         */
        
        Criteria criteria=null;
        
        logger.info("PenaltyDAOImpl,getPenaltiesPeriodOrderedByDate, slaId = " + slaId + " between "
                + startDate.toString() + " and " + endDate.toString());
        
        criteria = getSession(false).createCriteria(EmPenalty.class)
                        .add(Restrictions.between("penaltyDate", startDate, endDate)).addOrder(
                                Order.asc("penaltyDate")).createCriteria("violation").add(
                                        Restrictions.like("slaId", slaId));
        
        return (List<EmPenalty>) criteria.list();
    }

    
    public List<EmPenalty> getPenaltiesByViolationSlaId(String slaId) {
        
        /*DBManager.createCriteria(EmPenalty.class).createCriteria("violation").add(
        Restrictions.eq("slaId", slaId.toString()));*/
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmPenalty.class)
        .createCriteria("violation").add(
                Restrictions.like("slaId", slaId));

        return (List<EmPenalty>)criteria.list();
    }

    public List<EmPenalty> getPenaltiesByDateAndViolationSlaIdInList(Date penaltyDate, List<String> slaIdList) {

        /*DBManager.createCriteria(EmPenalty.class).add(Restrictions.eq("penaltyDate", auxDate))
        .createCriteria("violation").add(Restrictions.in("slaId", slaIds));*/
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmPenalty.class).add(Restrictions.eq("penaltyDate", penaltyDate))
        .createCriteria("violation").add(Restrictions.in("slaId", slaIdList));
        
        return (List<EmPenalty>)criteria.list();
    }

    public List<EmPenalty> getPenaltiesByViolationSlaIdAndGTid(String slaId, String guaranteeTermId) {

        /* DBManager.createCriteria(EmPenalty.class).createCriteria("violation").add(
        Restrictions.eq("slaId", slaId.toString())).add(
        Restrictions.eq("guaranteeTermId", gt.getId()));
         */
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmPenalty.class).createCriteria("violation").add(
                Restrictions.like("slaId", slaId)).add(
                        Restrictions.like("guaranteeTermId", guaranteeTermId));
        
        return (List<EmPenalty>)criteria.list();
    }

    public List<EmPenalty> getPenaltiesByViolationSlaIdAndGTidAndInitTimeBetween(String slaId, 
            String guaranteeTermId, Date startDate, Date endDate) {
        
        /* DBManager.createCriteria(EmPenalty.class).createCriteria("violation").add(
        Restrictions.eq("slaId", slaId.toString())).add(
        Restrictions.eq("guaranteeTermId", gt.getId().toString())).add(
        Restrictions.between("initialTime", startDate, endDate));
         */
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmPenalty.class).createCriteria("violation").add(
                Restrictions.like("slaId", slaId)).add(
                        Restrictions.like("guaranteeTermId", guaranteeTermId)).add(
                        Restrictions.between("initialTime", startDate, endDate));
        
        return (List<EmPenalty>)criteria.list();
    }

}
