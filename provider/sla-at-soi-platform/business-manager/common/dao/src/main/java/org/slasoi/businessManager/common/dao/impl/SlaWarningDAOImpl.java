/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/PartyDAOImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.SlaWarningDAO;
import org.slasoi.businessManager.common.model.pac.EmSlaWarning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value = "slaWarningDAO")
public class SlaWarningDAOImpl extends AbstractHibernateDAOImpl<EmSlaWarning, Long> implements SlaWarningDAO {

    private static final Logger logger = Logger.getLogger(SlaWarningDAOImpl.class);

    @Autowired
    public SlaWarningDAOImpl(SessionFactory factory) {
        setSessionFactory(factory);
    }

    @Override
    protected Class<EmSlaWarning> getDomainClass() {
        return EmSlaWarning.class;
    }

    public List<EmSlaWarning> getWarningsByNotificationId(String notificationId) {
      
        /*
         * DBManager.createCriteria(EmSlaWarning.class).add(Restrictions.eq("notificationId", notificationId));
         */
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmSlaWarning.class).add(
                Restrictions.eq("notificationId", notificationId));
        
        return (List<EmSlaWarning>) criteria.list();
    }

    public List<EmSlaWarning> getWarningsBySlaIdAndNotificationId(String slaId, String notificationId) {
       /* DBManager.createCriteria(EmSlaWarning.class).add(Restrictions.eq("notificationId", notificationId));
        crit.add(Restrictions.eq("slaId", slaId));
        */
        
        Criteria criteria = null;
        
        criteria = getSession(false).createCriteria(EmSlaWarning.class).add(
                Restrictions.eq("notificationId", notificationId));
        criteria.add(Restrictions.eq("slaId", slaId));
        
        return (List<EmSlaWarning>) criteria.list();

    }



}
