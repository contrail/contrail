/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/PolDAO.java $
 */

package org.slasoi.businessManager.common.dao;

import java.util.Date;
import java.util.List;

import org.slasoi.businessManager.common.model.pac.EmSlaViolation;

public interface SlaViolationDAO extends AbstractHibernateDAO<EmSlaViolation, Long>{
        
    /* DBManager.createCriteria(SlaViolation.class).add(Restrictions.in("slaId", slaIds)).add(
    Restrictions.le("initialTime", auxDate)).add(Restrictions.ge("endTime", auxDate));*/
    public List<EmSlaViolation> getViolationsBySlaIdInListAndTime(List<String> slaIdList,Date penaltyDate);
 
    
    /* DBManager.createCriteria(SlaViolation.class).add(Restrictions.eq("slaId", slaId.toString())).add(
            Restrictions.eq("guaranteeTermId", guaranteeTermId)).add(Restrictions.isNull("endTime"));
    */
    public List<EmSlaViolation> getViolationsBySlaIdAndGTidAndEndTimeNull(String slaId, String gtId);
        
        
    /* DBManager.createCriteria(SlaViolation.class)
    .add(Restrictions.eq("slaId", slaId.toString())).add(
            Restrictions.eq("guaranteeTermId", gt.getId()));
    */
    public List<EmSlaViolation> getViolationsBySlaIdAndGTid(String slaId, String gtId);
    
    
    /* DBManager.createCriteria(SlaViolation.class).add(
            Restrictions.eq("slaId", slaId.toString())).add(
            Restrictions.eq("guaranteeTermId", gt.getId().toString())).add(
            Restrictions.between("initialTime", startDate, endDate));
    */
    public List<EmSlaViolation> getViolationsBySlaIdAndGTidAndDateBetween(
            String slaId, String gtId,Date startDate, Date endDate);
    
    
    /* 
       DBManager.createCriteria(SlaViolation.class).add(Restrictions.eq("violationId", violationId));
    */
    public List<EmSlaViolation> getViolationsById(String violationId);
    
}