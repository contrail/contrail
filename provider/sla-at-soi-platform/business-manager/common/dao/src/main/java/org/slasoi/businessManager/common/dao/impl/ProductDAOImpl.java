/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 747 $
 * @lastrevision   $Date: 2011-02-17 16:42:40 +0100 (Thu, 17 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/ProductDAOImpl.java $
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ProductDAO;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmProductOfferSlat;
import org.slasoi.businessManager.common.model.EmServiceCategories;
import org.slasoi.businessManager.common.model.EmServiceSpecification;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.model.EmSpServicesCharacteristic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value = "productDAO")
public class ProductDAOImpl extends
		AbstractHibernateDAOImpl<EmSpProducts, Long> implements ProductDAO {
	private static final Logger log = Logger.getLogger(ProductDAOImpl.class);

	@Autowired
	public ProductDAOImpl(SessionFactory factory) {
		setSessionFactory(factory);
	}

	@Override
	protected Class<EmSpProducts> getDomainClass() {
		return EmSpProducts.class;
	}

	/**
	 * Find by filter
	 */

	public List<EmSpProducts> findByFilter(String filter) {
		return find(filter, null, null, null, null, false);
	}

	public List<EmSpProducts> findByFilters(List<String> filters) {
		return genericFind(filters, null, null, null, null, false);
	}

	public List<EmSpProducts> findByStatusAndFilter(String status, String filter) {
		return find(filter, status, null, null, null, false);
	}

	public List<EmSpProducts> findByStatusAndFilters(String status,
			List<String> filters) {
		return genericFind(filters, status, null, null, null, false);
	}

	public List<EmSpProducts> findByPartyAndFilter(EmParty party, String filter) {
		return find(filter, null, party, null, null, false);
	}

	public List<EmSpProducts> getAll() {
		log.debug("Inisde getAllInit method");
		return find(null, null, null, null, null, false);

	}

	public List<EmSpProducts> getAll(String status) {
		log.debug("finding EmSpProducts by Status: " + status);
		return find(null, status, null, null, null, false);
	}

	public List<EmSpProducts> findProductForContract(String filter,
			String productStatus, String offerStatus, Long customerCountryId) {
		return find(filter, productStatus, null, offerStatus,
				customerCountryId, false);
	}

	public List<EmSpProducts> findProductForPublishBSLAT(String filter) {
		return find(filter, Constants.STATUS_ACTIVE, null,
				Constants.STATUS_ACTIVE, null, true);
	}

	public List<EmSpProducts> find(String filter, String productStatus,
			EmParty party, String offerStatus, Long customerCountryId,
			boolean bslat) {
		List<String> filters = new ArrayList<String>();
		filters.add(filter);
		return genericFind(filters, productStatus, party, offerStatus,
				customerCountryId, bslat);
	}

	private List<EmSpProducts> genericFind(List<String> filters,
			String productStatus, EmParty party, String offerStatus,
			Long customerCountryId, boolean bslat) {
		log.debug("Generic find()");
		try {

			Criteria criteria = this.getSession().createCriteria(
					EmSpProducts.class);

			criteria
					.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

			// To filter by provider
			criteria.setFetchMode("emParty", FetchMode.JOIN);

			criteria.createAlias("emParty.emOrganization", "org",
					CriteriaSpecification.LEFT_JOIN);
			criteria.createAlias("emParty.emIndividual", "ind",
					CriteriaSpecification.LEFT_JOIN);

			criteria.createAlias("emSpProductsOffers", "offer",
					CriteriaSpecification.LEFT_JOIN);

			criteria.createAlias("emSpProductsOffers.emSpServiceses",
					"services", CriteriaSpecification.LEFT_JOIN);
			criteria
					.createAlias(
							"emSpProductsOffers.emSpServiceses.emServiceSpecifications",
							"sp", CriteriaSpecification.LEFT_JOIN);

			if (customerCountryId != null) {
				criteria.createAlias(
						"emSpProductsOffers.emGeographicalAreases", "geo",
						CriteriaSpecification.LEFT_JOIN);
				criteria
						.createAlias(
								"emSpProductsOffers.emGeographicalAreases.emCountrieses",
								"cty", CriteriaSpecification.LEFT_JOIN);
			}

			// for search only offers without bslat...
			if (bslat == true) {
				criteria.add(Restrictions.disjunction().add(
						Restrictions.isNull("offer.txBslatid")).add(
						Restrictions.eq("offer.txBslatid", "")));
			}

			if (filters != null && !filters.isEmpty()) {
				Disjunction or = Restrictions.disjunction();
				for (String filter : filters) {
					if (filter != null) {
						if (filter.indexOf("%") < 0)
							filter = "%" + filter + "%";
						or
								.add(Restrictions.like("sp.txName", filter))
								.add(Restrictions.like("txProductname", filter))
								.add(Restrictions.like("txProductdesc", filter))
								.add(
										Restrictions.like("org.txTradingname",
												filter)).add(
										Restrictions.like("ind.txFirstName",
												filter));
					}
				}
				criteria.add(or);
			}

			if (productStatus != null)
				criteria.add(Restrictions.eq("txStatus", productStatus));

			if (party != null)
				criteria.add(Restrictions.eq("emParty", party));

			if (offerStatus != null)
				criteria.add(Restrictions.eq("offer.txStatus", offerStatus));

			if (customerCountryId != null)
				criteria.add(Restrictions
						.eq("cty.nuCountry", customerCountryId));

			List<EmSpProducts> result = criteria.addOrder(
					Order.asc("txProductname")).list();
			return result;

		} catch (RuntimeException re) {
			log.error("genericFind failed", re);
			throw re;
		}
	}

	public EmSpProducts getProductOffersLoaded(Long id) {
		log.debug("Inisde getProductPromotionsLoaded method");
		log.debug("Product id:" + id);
		Criteria criteria = this.getSession()
				.createCriteria(EmSpProducts.class);
		criteria
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.setFetchMode("emSpProductsOffers", FetchMode.JOIN);
		criteria.add(Restrictions.eq("nuProductid", id));
		List<EmSpProducts> result = criteria.addOrder(
				Order.asc("txProductname")).list();
		log.debug("Offers asociated:"
				+ result.get(0).getEmSpProductsOffers().size());
		if (result != null && result.size() > 0) {
			return result.get(0);
		} else {
			log.debug("Product not found");
			return null;
		}
	}

	/**
	 * Return list of product that contains service with this nuServiceid.
	 * 
	 * @param nuServiceid
	 * @return
	 */
	public List<EmSpProducts> getProductByService(Long nuServiceid) {
		log.debug("Inisde getProductByService method");
		log.debug("Service id:" + nuServiceid);
		Criteria criteria = this.getSession()
				.createCriteria(EmSpProducts.class);
		criteria.createAlias("emSpProductsOffers", "offer",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("emSpProductsOffers.emSpServiceses", "service",
				CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("service.nuServiceid", nuServiceid));
		return criteria.list();
	}

	public List<EmSpProducts> getProductsWithAllCategories(List<String> categories, String productStatus) {
        log.debug("Generic getProductsWithAllCategories()");  
        try {
             
            String sql ="select distinct p from "+EmSpProducts.class.getSimpleName() +" p , "+EmSpProductsOffer.class.getSimpleName() +" po , "
            	+EmProductOfferSlat.class.getSimpleName() +" pos , "+EmSpServices.class.getSimpleName()+" s ";
            String where =" where p.nuProductid=po.emSpProducts.nuProductid and po.nuIdproductoffering=pos.emSpProductsOffer.nuIdproductoffering " +
            		" and pos.emSpServices.nuServiceid=s.nuServiceid ";
            
            if (productStatus!= null)
            	where += "and p.txStatus='"+productStatus+"'";
            
            for(int i=0; i<categories.size(); i++){
            	String category = categories.get(i);
            	if(category != null && category.trim().length()>0){
	            	sql += " join s.emServiceSpecifications spc"+i;
	            	where += " and spc"+i+".txName like '%"+category+"%'";
            	}
            }

            System.out.println("SQL >>>> "+sql+where);
            return (getHibernateTemplate().find(sql+where));


        } catch (RuntimeException re) {
            log.error("getProductsWithAllCategories failed:", re);
            throw re;       
        }
    }
}
