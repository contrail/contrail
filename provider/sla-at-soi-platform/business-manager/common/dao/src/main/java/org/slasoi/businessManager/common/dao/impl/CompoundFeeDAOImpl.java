/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import org.hibernate.SessionFactory;
import org.slasoi.businessManager.common.dao.CompoundFeeDAO;
import org.slasoi.businessManager.common.model.pricing.CompoundFee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="compoundFeeDAO")
public class CompoundFeeDAOImpl extends AbstractHibernateDAOImpl<CompoundFee, Long> implements CompoundFeeDAO {

	@Autowired
	public CompoundFeeDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<CompoundFee> getDomainClass() {
            return CompoundFee.class;
    }

}
