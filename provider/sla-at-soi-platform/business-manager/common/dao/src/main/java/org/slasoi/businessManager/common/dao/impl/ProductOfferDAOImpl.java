/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 692 $
 * @lastrevision   $Date: 2011-02-11 19:33:35 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/ProductOfferDAOImpl.java $
 */

package org.slasoi.businessManager.common.dao.impl;


import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ProductOfferDAO;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository(value="productOfferDAO")
public class ProductOfferDAOImpl extends AbstractHibernateDAOImpl<EmSpProductsOffer, Long> implements ProductOfferDAO {
    private static final Logger log = Logger.getLogger( ProductOfferDAOImpl.class );
	@Autowired
	public ProductOfferDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<EmSpProductsOffer> getDomainClass() {
            return EmSpProductsOffer.class;
    }
    
    /**
     * Find by bslat   
     */
        public EmSpProductsOffer getByBslat(String bslatID) {
            log.debug("finding EmSpProductsOffer By bslatID");  
            log.debug("BslatID:"+bslatID);  
            try {
                 
                Criteria criteria = this.getSession().createCriteria(EmSpProductsOffer.class);
                
                
                criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                    
                   
                  //To filter by service
                    criteria.setFetchMode("emProductOfferSlats", FetchMode.JOIN);  
                    criteria.setFetchMode("emComponentPrices", FetchMode.JOIN);
                    criteria.setFetchMode("emComponentPrices.emPriceVariations", FetchMode.JOIN);
                    criteria.createAlias("emSpServiceses","services",CriteriaSpecification.LEFT_JOIN);
                    criteria.createAlias("services.emServiceSpecifications", "sp",CriteriaSpecification.LEFT_JOIN);                    
                    criteria.add(Restrictions.eq("txBslatid", bslatID));                  
                List<EmSpProductsOffer> result = criteria.addOrder(Order.asc("txBslatid")).list();
                EmSpProductsOffer productOffer = null;
                if(result!=null&&result.size()>=1)
                    productOffer = result.get(0);
                
                return productOffer;
            
            } catch (RuntimeException re) {
                log.error("getByBslat failed", re);
                throw re;       
            }
        }
        
        
        public List<EmSpProductsOffer> findProductOffers(Long nuProductid){
        	Criteria criteria = this.getSession().createCriteria(EmSpProductsOffer.class);
        	criteria.add(Restrictions.eq("emSpProducts.nuProductid", nuProductid));
        	return criteria.list();
        }

        public void deleteProductOffers(Long nuProductid){
        	List<EmSpProductsOffer> offers = findProductOffers(nuProductid);
        	getHibernateTemplate().deleteAll(offers);
        }
        
        
        public List<EmSpProductsOffer> searchPartyProductOffers(Long nuPartyId, String filter){
        	return searchPartyProductOffers(nuPartyId, filter, false, false);
        }
        
        public List<EmSpProductsOffer> searchActivePartyProductOffers(Long nuPartyId, String filter){
        	return searchPartyProductOffers(nuPartyId, filter, true, true);
        }
        
        public List<EmSpProductsOffer> getProductOffersContracted(Long nuPartyId, String category){
        	return searchPartyProductOffers(nuPartyId, category, false, true);
        }

        public List<EmSpProductsOffer> getProductOffersToContract(Long nuPartyId, String category){
        	return searchPartyProductOffers(nuPartyId, category, true, false);
        }
        
        public List<EmSpProductsOffer> searchPartyProductOffers(Long nuPartyId, String filter, boolean availables, boolean contracted){
        	
        	Criteria criteria = this.getSession().createCriteria(EmSpProductsOffer.class);
        	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        	
        	//criteria.setFetchMode("emSpProducts", FetchMode.JOIN);
        	criteria.createAlias("emSpProducts","product",CriteriaSpecification.LEFT_JOIN);

            criteria.createAlias("emComponentPrices","price",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("price.emPriceType","priceType",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("price.emCurrencies","currency",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("product.emParty","party",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("emSpServiceses","services",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("services.emServiceSpecifications", "sp",CriteriaSpecification.LEFT_JOIN);
            
            //Solo sacamos las que están disponibles por fecha...
            if(availables){
            	criteria.add(Restrictions.disjunction().add(Restrictions.ne("party.nuPartyid", nuPartyId)).add(Restrictions.isNull("party.nuPartyid")) );
            	Date now = new Date();
                criteria.add(Restrictions.le("dtValidFrom", now));
                criteria.add(Restrictions.ge("dtValidTo", now));
                criteria.add(Restrictions.eq("txStatus", Constants.STATUS_ACTIVE));
                criteria.add(Restrictions.eq("product.txStatus", Constants.STATUS_ACTIVE));
                
            }
            //si son los contratados sacamos solo las que pertenezcan al id indicado...
            if(contracted)
            	criteria.add(Restrictions.eq("party.nuPartyid", nuPartyId));

            if (filter != null && !filter.trim().equals("")){
                criteria.add(
                  Restrictions.disjunction()
                   .add(Restrictions.like("txName", filter))
                   .add(Restrictions.like("txDescription", filter))
                   .add(Restrictions.like("txBslatid", filter))
                   .add(Restrictions.eq("sp.txName", filter))
                );                
            }              
            
            
            return criteria.addOrder(Order.asc("product.nuProductid")).list();

        }
        
}
