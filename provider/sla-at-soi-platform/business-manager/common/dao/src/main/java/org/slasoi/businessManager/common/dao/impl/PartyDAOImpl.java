/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/PartyDAOImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.dao.impl;


import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.PartyDAO;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository(value="partyDAO")
public class PartyDAOImpl extends AbstractHibernateDAOImpl<EmParty, Long> implements PartyDAO{
    private static final Logger log = Logger.getLogger( PartyDAOImpl.class );
	@Autowired
	public PartyDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<EmParty> getDomainClass() {
            return EmParty.class;
    }
    
    
    /**
     * Find by bslat   
     */
        public EmParty getByIDLoad(Long ID) {
            log.debug("finding EmParty By IDLoaded");  
            log.debug("ID:"+ID);  
            try {                 
                Criteria criteria = this.getSession().createCriteria(EmParty.class);                                
                criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
               //To filter by service
                criteria.setFetchMode("emPartyPartyroles", FetchMode.JOIN);
                criteria.add(Restrictions.eq("nuPartyid", ID));                  
                List<EmParty> result = criteria.addOrder(Order.asc("nuPartyid")).list();
                if(result!=null&&result.size()>=1){
                    return result.get(0);
                }else{
                    throw new RuntimeException("ProductOffer not Found"); 
                }      
            
            } catch (RuntimeException re) {
                log.error("getByBslat failed", re);
                throw re;       
            }
        }    
        
        /**
         * Gets the by individual name.
         * 
         * @param firstName the first name
         * @param lastName the last name
         * 
         * @return the by individual name
         */
        public EmParty getByIndividualName(String firstName, String lastName) {
            log.debug("finding EmParty By IndividualName");  
            log.debug("First Name :"+firstName);
            log.debug("Last Name :"+lastName);
            try {                 
                Criteria criteria = this.getSession().createCriteria(EmParty.class);                                
                criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
               //To filter by service
                criteria.createAlias("emIndividual","ind",CriteriaSpecification.LEFT_JOIN); 
                criteria.add(Restrictions.eq("ind.txFirstName", firstName));
                criteria.add(Restrictions.eq("ind.txLastName", lastName));
                List<EmParty> result = criteria.list();
                if(result!=null&&result.size()>=1){
                    return result.get(0);
                }else{
                	return null; 
                }      
            
            } catch (RuntimeException re) {
                log.error("getByBslat failed", re);
                throw re;       
            }
        } 
        
        /**
         * Gets the by organization name.
         * 
         * @param tradingName the trading name
         * 
         * @return the by organization name
         */
        public EmParty getByOrganizationName(String tradingName) {
            log.debug("finding EmParty By OrganizationName");  
            log.debug("tradingName :"+tradingName);
            try {                 
                Criteria criteria = this.getSession().createCriteria(EmParty.class);                                
                criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
               //To filter by service
                criteria.createAlias("emOrganization","org",CriteriaSpecification.LEFT_JOIN);                
                criteria.add(Restrictions.eq("org.txTradingname", tradingName));
                List<EmParty> result = criteria.list();
                if(result!=null&&result.size()>=1){
                    return result.get(0);
                }else{
                    return null; 
                }      
            
            } catch (RuntimeException re) {
                log.error("getByBslat failed", re);
                throw re;       
            }
        }         
 /**
  * GetParties with negative Balance       
  * @return
  */
        public List<EmParty> getPartiesWithNegativeBalance()throws Exception {
            log.debug("into getPartiesWithegativeBalance");                
            Criteria criteria = this.getSession().createCriteria(EmParty.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                                                   
            criteria.add(Restrictions.lt("nuBalance", new BigDecimal(0)));
            List<EmParty> result = criteria.list();
            return result;    
        }         

        public List<EmParty> getPartiesByRole(Long role){
            log.debug("into getPartiesByRole");                
            Criteria criteria = this.getSession().createCriteria(EmParty.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            criteria.createAlias("emPartyPartyroles","ppr",CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("ppr.emPartyRole","pr",CriteriaSpecification.LEFT_JOIN);
            
            criteria.add(Restrictions.eq("pr.nuPartyroleid", role));
            List<EmParty> result = criteria.list();
            return result;    
        }         

}
