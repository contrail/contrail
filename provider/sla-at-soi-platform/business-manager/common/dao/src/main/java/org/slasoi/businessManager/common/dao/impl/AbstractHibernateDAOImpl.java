/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/AbstractHibernateDAOImpl.java $
 */

package org.slasoi.businessManager.common.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slasoi.businessManager.common.dao.AbstractHibernateDAO;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public abstract class AbstractHibernateDAOImpl<DomainObject extends Serializable, KeyType extends Serializable> extends HibernateDaoSupport implements AbstractHibernateDAO<DomainObject, KeyType>{
        
	protected Class<DomainObject> domainClass = getDomainClass();
        
	protected abstract Class<DomainObject> getDomainClass();

	/* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#load(KeyType)
     */
	public DomainObject load(KeyType id) {
		return (DomainObject) getHibernateTemplate().get(getDomainClass(), id);
	}
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#update(DomainObject)
     */
    public void update(DomainObject object){
        getHibernateTemplate().update(object);
    }

    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#save(DomainObject)
     */
    public void save(DomainObject object){
        getHibernateTemplate().save(object);
    }

    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#saveOrUpdate(DomainObject)
     */
    public void saveOrUpdate(DomainObject object){
    	//System.out.println("a");
        getHibernateTemplate().saveOrUpdate(object);
    }

    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#delete(DomainObject)
     */
    public void delete(DomainObject object){
        getHibernateTemplate().delete(object);
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#deleteById(KeyType)
     */
    public void deleteById(KeyType id){
        Object obj = load(id);
        getHibernateTemplate().delete(obj);
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#getList()
     */
    public List<DomainObject> getList(){
        return (getHibernateTemplate().find("from " + domainClass.getName() + " o"));
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#deleteAll()
     */
    public void deleteAll(){
         getHibernateTemplate().execute(new HibernateCallback() {
             public Object doInHibernate(Session session) throws HibernateException {
                 String hqlDelete = "delete " + domainClass.getName();
                 int deletedEntities = session.createQuery(hqlDelete).executeUpdate();
                 return null;
             }
         });
    }
    
    /* (non-Javadoc)
     * @see org.slasoi.businessManager.common.dao.AbstractHibernateDAO#count()
     */
    public int count(){
        List list = getHibernateTemplate().find(
                "select count(*) from " + domainClass.getName() + " o");
        Integer count = (Integer) list.get(0);
        return count.intValue();
    }
}