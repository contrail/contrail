/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.PriceDAO;
import org.slasoi.businessManager.common.model.pricing.CompoundFee;
import org.slasoi.businessManager.common.model.pricing.FixedFee;
import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.VariableFee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="priceDAO")
public class PriceDAOImpl extends AbstractHibernateDAOImpl<Price, Long> implements PriceDAO{
	
	private static final Logger log = Logger.getLogger(PriceDAOImpl.class);
	
	@Autowired
	public PriceDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<Price> getDomainClass() {
            return Price.class;
    }

    
    public boolean isCompound(Long priceId){
    	log.debug("Inside isCompoundFee");
    	boolean isCompound=false;
    	if(priceId!=null){
    		Criteria criteria=this.getSession().createCriteria(CompoundFee.class);
        	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        	criteria.add(Restrictions.eq("compoundFeeId", priceId));
        	List<CompoundFee> results=(List<CompoundFee>)criteria.list();
        	if(!results.isEmpty()){
        		isCompound=true;
        	}
    	}
    	return isCompound;
    }
    
    public boolean isVariableFee(Long priceId){
    	log.debug("Inside isVariableFee");
    	boolean isVariableFee=false;
    	if(priceId!=null){
    		Criteria criteria=this.getSession().createCriteria(VariableFee.class);
        	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        	criteria.add(Restrictions.eq("variableFeeId", priceId));
        	List<VariableFee> results=(List<VariableFee>)criteria.list();
        	if(!results.isEmpty()){
        		isVariableFee=true;
        	}
    	}
    	return isVariableFee;
    }
    
    public boolean isFixedFee(Long priceId){
    	log.debug("Inside isFixedFee");
    	boolean isFixedFee=false;
    	if(priceId!=null){
    		Criteria criteria=this.getSession().createCriteria(FixedFee.class);
        	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        	criteria.add(Restrictions.eq("fixedFeeId", priceId));
        	List<FixedFee> results=(List<FixedFee>)criteria.list();
        	if(!results.isEmpty()){
        		isFixedFee=true;
        	}
    	}
    	return isFixedFee;
    }
}
