/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.GuaranteeDAO;
import org.slasoi.businessManager.common.model.pricing.FixedFee;
import org.slasoi.businessManager.common.model.pricing.Guarantee;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="guaranteeDAO")
public class GuaranteeDAOImpl extends AbstractHibernateDAOImpl<Guarantee, Long> implements GuaranteeDAO{
	
	private static final Logger log = Logger.getLogger(GuaranteeDAOImpl.class);
	
	@Autowired
	public GuaranteeDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<Guarantee> getDomainClass() {
            return Guarantee.class;
    }
	
    
    /* Finds
     * 
     */
    public Guarantee findGuaranteeByName(String name){
    	log.debug("Inside findGuaranteeByName");
    	if(name!=null){
    	Criteria criteria=this.getSession().createCriteria(Guarantee.class);
    	criteria.setFetchMode("guaranteeId", FetchMode.JOIN);
    	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    	criteria.add(Restrictions.eq("guaranteeName", name));
    	List<Guarantee> results=(List<Guarantee>)criteria.list();
    	return results.get(0);
    	}
    	return null;
    }
    
    public Guarantee findGuaranteeByType(String type){
    	log.debug("Inside findGuaranteeByType");
    	if(type!=null){
    	Criteria criteria=this.getSession().createCriteria(Guarantee.class);
    	criteria.setFetchMode("guaranteeId", FetchMode.JOIN);
    	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    	criteria.add(Restrictions.eq("guaranteeType", type));
    	List<Guarantee> results=(List<Guarantee>)criteria.list();
    	return results.get(0);
    	}
    	return null;
    }

}
