/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 790 $
 * @lastrevision   $Date: 2011-02-23 11:12:03 +0100 (Wed, 23 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/CustomerProductDAOImpl.java $
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.CustomerProductDAO;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "customerProductDAO")
public class CustomerProductDAOImpl extends AbstractHibernateDAOImpl<EmCustomersProducts, String> implements
        CustomerProductDAO {

    @Autowired
    public CustomerProductDAOImpl(SessionFactory factory) {
        setSessionFactory(factory);
    }

    @Override
    protected Class<EmCustomersProducts> getDomainClass() {
        return EmCustomersProducts.class;
    }

    public List<EmCustomersProducts> getCustomerProductOfParty(EmParty party) {
        DetachedCriteria criteria = DetachedCriteria.forClass(EmCustomersProducts.class);
        if (party != null)
            criteria.add(Restrictions.eq("emParty", party));
        criteria.addOrder(Order.desc("emParty.nuPartyid")).addOrder(Order.asc("dtInsertDate"));
        return getHibernateTemplate().findByCriteria(criteria);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByDateAndState(Date date, String typeDate){
        Criteria criteria = this.getSession().createCriteria(EmCustomersProducts.class);
        
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        //not started SLA stare
        if("initDate".equalsIgnoreCase(typeDate)){
            criteria.add(Restrictions.eq("state", ""));
            criteria.add(Restrictions.le("dtDateBegin", date));
        }else if("endDate".equalsIgnoreCase(typeDate)){
            criteria.add(Restrictions.eq("state", "OBSERVED"));
            criteria.add(Restrictions.le("dtDateEnd", date));
        }
        List<EmCustomersProducts> result = criteria.addOrder(Order.desc("dtInsertDate")).list();
        return result;

    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsActiveByDate(Date date){
        Criteria criteria = this.getSession().createCriteria(EmCustomersProducts.class);        
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        criteria.setFetchMode("emSpProductsOffer", FetchMode.JOIN);
        criteria.setFetchMode("emSpProductsOffer.emComponentPrices", FetchMode.JOIN);
        criteria.setFetchMode("emSpProductsOffer.emComponentPrices.emPriceType", FetchMode.JOIN);
        criteria.setFetchMode("emSpProductsOffer.emPriceVariations", FetchMode.JOIN);   
        criteria.setFetchMode("emSpProductsOffer.emPriceVariations.emPriceVariationType", FetchMode.JOIN);
        criteria.add(Restrictions.eq("state", "OBSERVED"));
        criteria.add(Restrictions.le("dtDateBegin", date));        
        criteria.add(Restrictions.ge("dtDateEnd", date)); 
        List<EmCustomersProducts> result = criteria.addOrder(Order.desc("dtInsertDate")).list();
        return result;

    }

    public List<EmCustomersProducts> getCustomerProductsByPublisher(EmParty party) {
        DetachedCriteria criteria = DetachedCriteria.forClass(EmCustomersProducts.class);
        DetachedCriteria offerCriteria = criteria.createCriteria("emSpProductsOffer");
        DetachedCriteria productCriteria = offerCriteria.createCriteria("emSpProducts");
        productCriteria.add(Restrictions.eq("emParty", party));
        criteria.addOrder(Order.desc("emParty.nuPartyid")).addOrder(Order.asc("dtInsertDate"));
        return getHibernateTemplate().findByCriteria(criteria);
    }

    public List<EmCustomersProducts> getCustomerProductsAndServicesAttributes(EmParty party) {
        return getCustomerProductsAndServicesAttributes(party, null);
    }

    public List<EmCustomersProducts> getCustomerProductsAndServicesAttributes(EmParty party, String category) {
        Criteria criteria = this.getSession().createCriteria(EmCustomersProducts.class);

        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        criteria.setFetchMode("emSpProductsOffer", FetchMode.JOIN);
        criteria.setFetchMode("emSpProductsOffer.emSpServiceses", FetchMode.JOIN);
        criteria.createAlias("emSpProductsOffer.emSpServiceses.emServiceSpecifications","sp", CriteriaSpecification.LEFT_JOIN);
        criteria.setFetchMode("emSpProductsOffer.emSpServiceses.emAttributeses", FetchMode.JOIN);
        criteria.setFetchMode("emSpServSatisfactionRates", FetchMode.JOIN);
        criteria.setFetchMode("emSpProductsOffer.emComponentPrices", FetchMode.JOIN);
        criteria.setFetchMode("emPriceVariations", FetchMode.JOIN);

        criteria.add(Restrictions.eq("emParty", party));
        
        if(category!=null){
            criteria.add(Restrictions.like("sp.txName", category));
        }

        List<EmCustomersProducts> result = criteria.addOrder(Order.desc("dtInsertDate")).list();
        return result;
    }

    public EmCustomersProducts getCustomerProductByIdLoaded(String id) {
        Criteria criteria = this.getSession().createCriteria(EmCustomersProducts.class);
        criteria.add(Restrictions.eq("txBslaid", id));
        criteria.setFetchMode("emCustprodsSlases", FetchMode.JOIN);
        criteria.setFetchMode("emCustprodsSlases.emSpServices", FetchMode.JOIN);
        criteria.setFetchMode("emCustprodsSlases.emSpServices.emSlamanagers", FetchMode.JOIN);
        List<EmCustomersProducts> result = criteria.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public List<EmCustomersProducts> getCustomerProductsByProductOfferAndParty(EmSpProductsOffer productOffer,
            EmParty party) {
        DetachedCriteria criteria = DetachedCriteria.forClass(EmCustomersProducts.class);

        if (productOffer != null)
            criteria.add(Restrictions.eq("emSpProductsOffer", productOffer));

        if (party != null)
            criteria.add(Restrictions.eq("emParty", party));

        criteria.addOrder(Order.asc("dtInsertDate"));
        return getHibernateTemplate().findByCriteria(criteria);
    }
    
    public  List<EmCustomersProducts> getCustomerProductsByProduct(EmSpProducts product){
        Criteria criteria = this.getSession().createCriteria(EmCustomersProducts.class);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        criteria.setFetchMode("emSpProductsOffer", FetchMode.JOIN);
        criteria.createAlias("emSpProductsOffer.emSpProducts","product", CriteriaSpecification.LEFT_JOIN);
        criteria.add(Restrictions.eq("product.nuProductid", product.getNuProductid()));     
        List<EmCustomersProducts> result = criteria.list();
        return result;        
    }

}
