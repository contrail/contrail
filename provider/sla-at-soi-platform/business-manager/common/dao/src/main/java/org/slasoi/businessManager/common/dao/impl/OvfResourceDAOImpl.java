/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.OvfResourceDAO;
import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="ovfResourceDAO")
public class OvfResourceDAOImpl extends AbstractHibernateDAOImpl<OvfResource, Long> implements OvfResourceDAO{
	
	private static final Logger log = Logger.getLogger(OvfResourceDAOImpl.class);
	
	@Autowired
	public OvfResourceDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<OvfResource> getDomainClass() {
            return OvfResource.class;
    }
     
	public OvfResource findOvfResourceByOvfId(Long ovfDescrId, String ovfId) {
		log.debug("Inside findOvfResourceByOvfId");
		if (ovfId != null) {
			Criteria criteria = this.getSession().createCriteria(OvfResource.class);
			criteria.setFetchMode("ovfDescriptor", FetchMode.JOIN);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			Criteria ovfDesCrit = criteria.createCriteria("ovfDescriptor");
			ovfDesCrit.setFetchMode("ovfResources", FetchMode.JOIN);
			ovfDesCrit.add(Restrictions.eq("ovfDescriptorId", ovfDescrId));
			criteria.add(Restrictions.eq("ovfId", ovfId));
			List<OvfResource> results = (List<OvfResource>) criteria.list();
			if (results != null && results.size() != 0)
				return results.get(0);
		}
		return null;
	}
}