package org.slasoi.businessManager.common.util;

import org.apache.log4j.Logger;
import org.slasoi.slamodel.sla.SLATemplate;

public class Cloner {
    private static final Logger log = Logger.getLogger(Cloner.class);  
    
    /**
     * Return a clone of a template.
     * @param template
     * @return
     */
      public static SLATemplate cloneTemplate(SLATemplate template){
        log.debug("Into clone template method:"+ template.getUuid().getValue());
        com.rits.cloning.Cloner cloner=new com.rits.cloning.Cloner();
        return cloner.deepClone(template);          
      } 

}
