/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/util/ProductOffersAttr.java $
 */

package org.slasoi.businessManager.common.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProductOffersAttr implements Serializable{

	//Attributes of Offers
	private List<String> offerName;
	private List<String> offerDescription;
	private List<String> offerRelease;
	private List<String> dateValidFrom;
	private List<String> dateValidTo;
	private List<String> offerStatus;
	private List<Long> billingCode;
	private List<String[]> offerServices;
	
	
	//Attributes of ComponentPrices
	private List<String>componentPriceOffer;
	private List<String>componentName;
	private List<String>componentDescription;
	private List<String> offerDateValidFrom;
	private List<String> offerDateValidTo;
	private List<Long> pryceType;
	private List<Long> currency;
	private List<BigDecimal> quantity;
	private List<BigDecimal> pryce;
	
	//List of Geographical Areas, and offer reference
	private List<Long> nuAreasIdsReference;
	private List<String> nuAreaIds;
	
	
	
	public List<String> getOfferName() {
		return offerName;
	}
	public void setOfferName(List<String> offerName) {
		this.offerName = offerName;
	}
	public List<String> getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(List<String> offerDescription) {
		this.offerDescription = offerDescription;
	}
	public List<String> getOfferRelease() {
		return offerRelease;
	}
	public void setOfferRelease(List<String> offerRelease) {
		this.offerRelease = offerRelease;
	}
	public List<String> getDateValidFrom() {
		return dateValidFrom;
	}
	public void setDateValidFrom(List<String> dateValidFrom) {
		this.dateValidFrom = dateValidFrom;
	}
	public List<String> getDateValidTo() {
		return dateValidTo;
	}
	public void setDateValidTo(List<String> dateValidTo) {
		this.dateValidTo = dateValidTo;
	}
	public List<String> getOfferStatus() {
		return offerStatus;
	}
	public void setOfferStatus(List<String> offerStatus) {
		this.offerStatus = offerStatus;
	}
	public List<Long> getBillingCode() {
		return billingCode;
	}
	public void setBillingCode(List<Long> billingCode) {
		this.billingCode = billingCode;
	}
	public List<String[]> getOfferServices() {
		return offerServices;
	}
	public void setOfferServices(List<String[]> offerServices) {
		this.offerServices = offerServices;
	}
	public List<String> getComponentPriceOffer() {
		return componentPriceOffer;
	}
	public void setComponentPriceOffer(List<String> componentPriceOffer) {
		this.componentPriceOffer = componentPriceOffer;
	}
	public List<String> getComponentName() {
		return componentName;
	}
	public void setComponentName(List<String> componentName) {
		this.componentName = componentName;
	}
	public List<String> getComponentDescription() {
		return componentDescription;
	}
	public void setComponentDescription(List<String> componentDescription) {
		this.componentDescription = componentDescription;
	}
	public List<String> getOfferDateValidFrom() {
		return offerDateValidFrom;
	}
	public void setOfferDateValidFrom(List<String> offerDateValidFrom) {
		this.offerDateValidFrom = offerDateValidFrom;
	}
	public List<String> getOfferDateValidTo() {
		return offerDateValidTo;
	}
	public void setOfferDateValidTo(List<String> offerDateValidTo) {
		this.offerDateValidTo = offerDateValidTo;
	}
	public List<Long> getPryceType() {
		return pryceType;
	}
	public void setPryceType(List<Long> pryceType) {
		this.pryceType = pryceType;
	}
	public List<Long> getCurrency() {
		return currency;
	}
	public void setCurrency(List<Long> currency) {
		this.currency = currency;
	}
	public List<BigDecimal> getQuantity() {
		return quantity;
	}
	public void setQuantity(List<BigDecimal> quantity) {
		this.quantity = quantity;
	}
	public List<BigDecimal> getPryce() {
		return pryce;
	}
	public void setPryce(List<BigDecimal> pryce) {
		this.pryce = pryce;
	}
	public List<Long> getNuAreasIdsReference() {
		return nuAreasIdsReference;
	}
	public void setNuAreasIdsReference(List<Long> nuAreasIdsReference) {
		this.nuAreasIdsReference = nuAreasIdsReference;
	}
	public List<String> getNuAreaIds() {
		return nuAreaIds;
	}
	public void setNuAreaIds(List<String> nuAreaIds) {
		this.nuAreaIds = nuAreaIds;
	}
	
	
}
