/**
 * Copyright (c) 2008-2010, Telef�nica Investigaci�n y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telef�nica Investigaci�n y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telef�nica Investigaci�n y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 736 $
 * @lastrevision   $Date: 2011-02-17 12:08:48 +0100 (Thu, 17 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/Constants.java $
 */

package org.slasoi.businessManager.common.util;

public class Constants {
	public static String PATH;
	public static final String EMP_NS = "http://com.telefonica.com/xmlns/emp";

	public static final String EMP_PROVISIONING_WSDL_NAME = "ECI_Provision.wsdl";

	public static final String EMP_VERSION_NO = "0.1";

	public static String simpleServiceURI = "http://tid.es/ServiceType/Simple";

	public static String composedServiceURI = "http://tid.es/ServiceType/Composed";

	public static String typeSP = "SP";

	public static String typeService = "Service";

	public static String typeProduct = "http://tid.es/Product";
	
	public static String SUBIP = "SUBIP";
	    
    public static String SLA_START = "SLA_START";
	public static String SLA_END = "SLA_END";
	    
    public static String SERVICE_CHARACTERISTIC_NAME = "SERVICE NAME"; 

	
	/**
	 * NU_USERID nombre de la variable de identificaci�n de usuario que se ha
	 * logado en la plataforma y se guarda de la clase Integer.
	 */
	public static final String NU_USERID = "NU_USERID";

	/**
	 * TX_ROLE nombre de la variable de rol del usuario que se ha logado en la
	 * plataforma y se guarda de la clase Character.
	 */
	public static final String TX_ROLE = "TX_ROLE";
	
	
	/**
	 * USER_SESSION, ROLE_SESSION: Nombres de las variables de identificaci�n 
	 * para el usuario y rol validados en la aplicaci�n
	 */
	
	public static final String USER_SESSION = "USER_SESSION";
	public static final String PARTY_PARTYROLE_SESSION = "PARTY_PARTYROLE_SESSION";

	
	/**
	 * To generate the ID of the RFS
	 */
	public static String RFS_SEQUENCE = "SQ_RFSID";	

	public static String ERROR_REMOTE_EXCEPTION = "Configuration & Activation Module:The server module (SAC) for requesting provision//health//usage interfaces is not loaded.";
	
	public static final String STATUS_DEFINED = "D";
	public static final String STATUS_DEFINED_NAME = "DEFINED";
	public static final String STATUS_PROMOTIONED = "P";
	public static final String STATUS_PROMOTIONED_NAME = "PROMOTIONED";
	public static final String STATUS_REJECTED = "R";
	public static final String STATUS_REJECTED_NAME = "REJECTED";
	public static final String STATUS_ACTIVE = "A";
	public static final String STATUS_ACTIVE_NAME = "ACTIVE";
	public static final String STATUS_INACTIVE = "I";
	public static final String STATUS_INACTIVE_NAME = "INACTIVE";
	

	public static final String STATE_ACTIVE = "ACTIVE";
    public static final String STATE_INACTIVE = "INACTIVE";
    public static final String STATE_PENDING = "PENDING";
    public static final String STATE_CANCEL= "CANCEL";	
    
    //Rule Type
    public static final String PARAM_RULETYPE_WHEN= "WHEN";
    public static final String PARAM_RULETYPE_THEN= "THEN";

    //Rule Parameter Type 
    public static final String PARAM_TYPE_NUMBER= "NUMBER";
    public static final String PARAM_TYPE_STRING= "STRING";
    public static final String PARAM_TYPE_DATE= "DATE";
    public static final String PARAM_TYPE_LIST= "LIST";
    public static final String PARAM_TYPE_SELECT= "SELECT";
    
    //Party Type
    public static final String PARTY_ORGANIZATION= "O";
    public static final String PARTY_INDIVIDUAL= "I";
    
    //Party Roles
    public static final long PARTY_ROLE_OWNER = 1;
    public static final long PARTY_ROLE_PROVIDER = 2;
    public static final long PARTY_ROLE_CUSTOMER = 3;
    public static final String PARTY_ROLE_CUSTOMER_STRING ="CUSTOMER";

    //User Roles
    public static final String USER_ROLE_CUSTOMER= "C";
    public static final String USER_ROLE_ADMIN= "A";
    public static final String USER_ROLE_SERVICE_PROVIDER= "T"; //Third part
    
    public static final String USER_ROLE_BUSINESS_MANAGER= "B";
    public static final String USER_ROLE_SALES_MANAGER= "S";
    public static final String USER_ROLE_PRODUCT_MANAGER= "P";
    public static final String USER_ROLE_CUSTOMER_MANAGER= "R"; //Customer/Relationship
    
    
    //States parties
	public static final String PARTY_STATE_APPROVAL = "APPROVED";
    public static final String PARTY_STATE_PENDING = "PENDING";
    public static final String PARTY_STATE_REJECTED= "REJECTED";   
    public static final String PARTY_STATE_BAN = "BANNED";    
    
    //Parameter list types. WS.
    public static final String PARTY_WS_COUNTRY = "COUNTRY";
    public static final String PARTY_WS_LANGUAGE = "LANGUAGE";
    public static final String PARTY_WS_CURRENCY = "CURRENCY";
    
    //Component prices variations constants
    public static final String UNIT_PERCENTAGE= "P";
    public static final String UNIT_AMOUNT = "F";
    public static final String MODIFICATION_INCREMENT = "I";
    public static final String MODIFICATION_DECREMENT= "D";
    
    //SLA States
    public static final String SLA_STATE_OBS= "OBSERVED";
    public static final String SLA_STATE_VIO= "VIOLATED";
    public static final String SLA_STATE_WARN= "WARN";
    public static final String SLA_STATE_EXPI= "EXPIRED";
    public static final String SLA_STATE_SUSPEND= "SUSPENDED";
    
    //Termination clauses
    
    public static final String TERMINATION_DEM_INC= "DEMAND_INCREASED";
    public static final String TERMINATION_DEM_DEC= "DEMAND_DECREASED";
    public static final String TERMINATION_BETTER_PROV= "FOUND_BETTER_PROVIDER";
    public static final String TERMINATION_UNS_QUA= "UNSATISFACTORY_QUALITY";
    public static final String TERMINATION_COST= "COST";
    public static final String TERMINATION_STA_COMP= "STATUS_COMPROMISED";
    public static final String TERMINATION_BETTER_CUST= "FOUND_BETTER_CUSTOMER";
    public static final String TERMINATION_BUS_DEC= "BUSINESS_DECISION";
    public static final String TERMINATION_CUST_INI= "CUSTOMER_INITIATED";
    public static final String TERMINATION_PROV_INI= "PROVIDER_INITIATED";
    
    //Negotiation Types    
    public static final String NEGOTIATION_TYPE_AUTO= "A";
    public static final String NEGOTIATION_TYPE_SEMI= "S";
    public static final String NEGOTIATION_TYPE_MANUAL= "M";
 

    
}
