/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/promotion/Promotion.java $
 */

package org.slasoi.businessManager.common.service.promotion;

import java.util.ArrayList;
import java.util.List;


public class Promotion {

	private Long nuPromotionId;
	private Long nuPromotionTypeId;
	private String txDescription;
	private String txName;
	private List<PromotionParameter> promotionParameters = new ArrayList<PromotionParameter>(0);
	private Long nuProductId;
	
	public Long getNuPromotionId() {
		return nuPromotionId;
	}
	public void setNuPromotionId(Long nuPromotionId) {
		this.nuPromotionId = nuPromotionId;
	}
	public Long getNuPromotionTypeId() {
		return nuPromotionTypeId;
	}
	public void setNuPromotionTypeId(Long nuPromotionTypeId) {
		this.nuPromotionTypeId = nuPromotionTypeId;
	}
	public String getTxDescription() {
		return txDescription;
	}
	public void setTxDescription(String txDescription) {
		this.txDescription = txDescription;
	}
	public String getTxName() {
		return txName;
	}
	public void setTxName(String txName) {
		this.txName = txName;
	}
	public List<PromotionParameter> getPromotionParameters() {
		return promotionParameters;
	}
	public void setPromotionParameters(List<PromotionParameter> promotionParameters) {
		this.promotionParameters = promotionParameters;
	}
	public Long getNuProductId() {
		return nuProductId;
	}
	public void setNuProductId(Long nuProductId) {
		this.nuProductId = nuProductId;
	}
	
}
