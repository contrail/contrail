/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PartyPartyRoleManagerImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.PartyPartyRoleDAO;
import org.slasoi.businessManager.common.model.EmPartyPartyrole;
import org.slasoi.businessManager.common.model.EmPartyPartyroleId;
import org.slasoi.businessManager.common.service.PartyPartyRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="partyPartyRoleService")
public class PartyPartyRoleManagerImpl implements PartyPartyRoleManager {

		static Logger log = Logger.getLogger(PartyPartyRoleManagerImpl.class);
	
		@Autowired
		private PartyPartyRoleDAO partyPartyRoleDAO;
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#getPartyById(java.lang.Long)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmPartyPartyrole getPartyPartyRoleById(EmPartyPartyroleId id){
	    	log.info("getPartyById():Id:"+id);
	    	EmPartyPartyrole result = partyPartyRoleDAO.load(id);
	    	return result;
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#getParties()
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmPartyPartyrole> getPartiesPartiesRoles(){
	    	log.info("getParties()");
	    	return partyPartyRoleDAO.getList();
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#saveOrUpdate(org.slasoi.businessManager.common.model.EmParty)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public void saveOrUpdate(EmPartyPartyrole obj){
	    	log.info("saveOrUpdate()");
	    	partyPartyRoleDAO.saveOrUpdate(obj);
	    }

		
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deletePartiesPartiesRoles(List<EmPartyPartyroleId> ids){
	    	log.info("deleteParties()");
	    	
	    	Iterator<EmPartyPartyroleId> itIds = ids.iterator();
	    	while(itIds.hasNext()){
	    		EmPartyPartyrole party = partyPartyRoleDAO.load(itIds.next());
	    		partyPartyRoleDAO.delete(party);
	    	}
	    }		
	
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deletePartyPartyRole(EmPartyPartyroleId id){
	    	log.info("deleteParty()");
	    	EmPartyPartyrole party = partyPartyRoleDAO.load(id);
    		partyPartyRoleDAO.delete(party);
	    }
		

		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public List<EmPartyPartyrole> getRolesByParty(Long partyId){
	    	log.info("deleteParty()");
	    	return this.partyPartyRoleDAO.getRolesByParty(partyId);
	    }




}
