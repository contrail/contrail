package org.slasoi.businessManager.common.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.GtTranslationDAO;
import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;
import org.slasoi.businessManager.common.service.GtTranslationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="gtTranslationManager")
public class GtTranslationManagerImpl implements GtTranslationManager{
    
    static Logger log = Logger.getLogger(GtTranslationManagerImpl.class);
    
    @Autowired
    private GtTranslationDAO gtTranslationDAO;
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  List<EmGtTranslation> getGtTranslations(){
        log.debug("Into getGtTranslations method");
        return gtTranslationDAO.getList();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public  EmGtTranslation getGtTranslationById(EmGtTranslationId id){
        log.debug("Into getGtTranslationById method");
        return gtTranslationDAO.load(id);
    }
    

    @Transactional(propagation = Propagation.REQUIRED)
    public  void saveGtTranslation(EmGtTranslation object){
        log.debug("Into saveGtTranslation method");
        gtTranslationDAO.save(object);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  void saveGtTranslationsList(List <EmGtTranslation> object){
        log.debug("Into saveGtTranslationsList method");
        if(object != null && object.size()>0){
            for(EmGtTranslation gtTraslation : object){
                
                this.gtTranslationDAO.save(gtTraslation);
            }
            
        }
        
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public  void deleteGtTranslation(EmGtTranslationId id){
        gtTranslationDAO.deleteById(id);
    }
    
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
     public  EmGtTranslation getGtTranslation(String type, EmGtTranslation translation){
        return gtTranslationDAO.getGtTranslation( type, translation);
    }

}
