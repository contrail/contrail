/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/promotion/PromotionManagementImpl.java $
 */

package org.slasoi.businessManager.common.service.promotion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ParamDAO;
import org.slasoi.businessManager.common.dao.ParamTypeDAO;
import org.slasoi.businessManager.common.dao.ProductDAO;
import org.slasoi.businessManager.common.dao.ProductPromotionDAO;
import org.slasoi.businessManager.common.dao.PromotionDAO;
import org.slasoi.businessManager.common.dao.PromotionTypeDAO;
import org.slasoi.businessManager.common.dao.PromotionValueDAO;
import org.slasoi.businessManager.common.model.EmParams;
import org.slasoi.businessManager.common.model.EmProductPromotion;
import org.slasoi.businessManager.common.model.EmProductPromotionId;
import org.slasoi.businessManager.common.model.EmPromotionType;
import org.slasoi.businessManager.common.model.EmPromotionValues;
import org.slasoi.businessManager.common.model.EmPromotionValuesId;
import org.slasoi.businessManager.common.model.EmPromotions;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.util.SimpleDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "promotionManagement")
public class PromotionManagementImpl implements PromotionManagement {

    static Logger log = Logger.getLogger(PromotionManagementImpl.class);

    @Resource
    private PromotionTypeDAO promotioTypeDAO;

    @Resource
    private PromotionDAO promotionDAO;

    @Resource
    private ProductPromotionDAO productPromotionDAO;

    @Resource
    private PromotionValueDAO promotionValueDAO;

    @Resource
    private ProductDAO productDAO;

    @Resource
    private ParamDAO paramDAO;

    @Resource
    private ParamTypeDAO paramTypeDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.businessManager.common.service.promotion.PromotionManagement#getPromotionTypes()
     */
    public List<PromotionType> getPromotionTypes() {
        try {
            List<EmPromotionType> emTypes = promotioTypeDAO.getList();
            List<PromotionType> types = new ArrayList<PromotionType>();

            for (EmPromotionType emType : emTypes) {
                PromotionType type = new PromotionType();
                BeanUtils.copyProperties(type, emType);
                type.setTxTemplateDrt(emType.getEmRuleTemplates().getTxTemplateDrt());
                types.add(type);
            }
            return types;
        }
        catch (Exception e) {
            log.error("Error load Promotion Types: " + e.getMessage());
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.businessManager.common.service.promotion.PromotionManagement#getPromotionByType(java.lang.Long)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Promotion getPromotionByType(Long promoTypeId) throws Exception {

        Promotion promotion = new Promotion();

        // Load Parameter of template...
        EmPromotionType promoType = promotioTypeDAO.load(promoTypeId);
        promotion.setNuPromotionTypeId(promoTypeId);

        EmRuleTemplates template = promoType.getEmRuleTemplates();

        // Load Template parameters, and asociate with Promotion
        Iterator<EmRuleParams> itRuleParams = template.getEmRuleParamses().iterator();
        while (itRuleParams.hasNext()) {
            EmRuleParams ruleParam = itRuleParams.next();
            PromotionParameter promoParam = new PromotionParameter();
            BeanUtils.copyProperties(promoParam, ruleParam.getEmParams());
            promoParam.setTxParamType(ruleParam.getEmParams().getEmParamType().getTxParamTypeName());

            // Set list of valid values, if type = SELECT
            paramTypeDAO.loadParamValues(ruleParam.getEmParams().getEmParamType());
            promoParam.setListValues(ruleParam.getEmParams().getEmParamType().getListValues());

            promotion.getPromotionParameters().add(promoParam);
        }

        return promotion;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.businessManager.common.service.promotion.PromotionManagement#savePromotion(org.slasoi.businessManager
     * .common.service.promotion.Promotion)
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void savePromotion(Promotion promotion) throws Exception {

        // Check valid ProductId
        EmSpProducts emProduct = productDAO.load(promotion.getNuProductId());
        if (emProduct == null)
            throw new Exception("Product Id " + promotion.getNuProductId() + " does not exist in database.");

        EmPromotionType ept = promotioTypeDAO.load(promotion.getNuPromotionTypeId());
        if (ept == null)
            throw new Exception("Must enter a valid type of promotion, the right does not exist in database.");

        // Check not empty fields
        if (promotion.getTxName() == null || promotion.getTxName().trim().length() == 0
                || promotion.getTxDescription() == null || promotion.getTxDescription().trim().length() == 0)
            throw new Exception("Name and Description are Mandatory.");

        // Check the number of parameters is correct
        if (promotion.getPromotionParameters().size() != ept.getEmRuleTemplates().getEmRuleParamses().size())
            throw new Exception("Wrong number of parameters for the selected type of promotion. Must have "
                    + ept.getEmRuleTemplates().getEmRuleParamses().size() + 1);

        EmPromotions emp = new EmPromotions();
        BeanUtils.copyProperties(emp, promotion);
        emp.setEmPromotionType(ept);

        // Save the promotions
        promotionDAO.save(emp);

        // Create Product Promotion Relation
        EmProductPromotionId productPromotionId =
                new EmProductPromotionId(promotion.getNuProductId(), emp.getNuPromotionId());
        EmProductPromotion productPromotion = new EmProductPromotion(productPromotionId, Constants.STATE_PENDING);
        productPromotionDAO.saveOrUpdate(productPromotion);

        // Save the promotion values...
        for (PromotionParameter promoParam : promotion.getPromotionParameters()) {
            // Check if valid paramId for select template
            checkPromotionParameter(promoParam, ept.getEmRuleTemplates());
            EmPromotionValuesId id =
                    new EmPromotionValuesId(promoParam.getNuParamId(), emp.getNuPromotionId(), ept.getEmRuleTemplates()
                            .getNuRuleTemplateId());
            EmPromotionValues epv = new EmPromotionValues(id, promoParam.getTxParamValue());
            promotionValueDAO.saveOrUpdate(epv);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private boolean checkPromotionParameter(PromotionParameter promoParam, EmRuleTemplates template) throws Exception {

        boolean valid = false;
        try {
            EmParams emParam = paramDAO.load(promoParam.getNuParamId());
            if (emParam == null)
                throw new Exception("Must enter valid parameter. ParamId: " + promoParam.getNuParamId()
                        + " not exist in database.");

            if (promoParam.getTxParamValue() == null || promoParam.getTxParamValue().trim().length() == 0)
                throw new Exception("Must enter all parameters value");
            // Validate if parameterId is valid for template.
            for (EmRuleParams param : template.getEmRuleParamses()) {
                if (param.getId().getNuParamId() == promoParam.getNuParamId()) {
                    valid = true;
                    break;
                }
            }

            // check value if ParamType = DATE.
            if (emParam.getEmParamType().getTxParamTypeName().equals(Constants.PARAM_TYPE_DATE)) {
                try {
                    new SimpleDate(promoParam.getTxParamValue());
                }
                catch (Exception e) {
                    throw new Exception("Value of " + promoParam.getTxParamViewname() + ": "
                            + promoParam.getTxParamValue() + " no is a valid Date.");
                }
            }
            // check value if ParamType = NUMBER.
            if (emParam.getEmParamType().getTxParamTypeName().equals(Constants.PARAM_TYPE_NUMBER)) {
                try {
                    Double.parseDouble(promoParam.getTxParamValue());
                }
                catch (Exception e) {
                    throw new Exception("Value of " + promoParam.getTxParamViewname() + ": "
                            + promoParam.getTxParamValue() + " no is a valid Number.");
                }
            }

        }
        catch (Exception e) {
            log.error("checkPromotionParameter Error: " + e.getMessage());
            throw e;
        }
        return valid;
    }

}
