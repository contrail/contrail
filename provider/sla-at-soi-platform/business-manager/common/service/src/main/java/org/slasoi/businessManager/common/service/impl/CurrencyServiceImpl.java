/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 728 $
 * @lastrevision   $Date: 2011-02-16 16:43:52 +0100 (Wed, 16 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/CurrencyServiceImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.CurrencyDAO;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="currencyService")
public class CurrencyServiceImpl implements CurrencyService{
    static Logger log = Logger.getLogger(CurrencyServiceImpl.class);
    
    @Autowired
    private CurrencyDAO currencyDAO;
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  EmCurrencies getCurrencyById(Long id){
        return this.currencyDAO.load(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public  void saveOrUpdate(EmCurrencies obj){
        this.currencyDAO.saveOrUpdate(obj);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  List<EmCurrencies> getAllCurrencies(){
        
        return this.currencyDAO.getList();
        
    }
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public void deleteCurrencies(List<Long> ids){
    	log.info("deleteCurrencies()");
    	
    	//Delete all Characteristic of the list Ids
    	Iterator<Long> itIds = ids.iterator();
    	while(itIds.hasNext()){
    		EmCurrencies cht = currencyDAO.load(itIds.next());
    		currencyDAO.delete(cht);
    	}
    }
	
	 @Transactional(propagation = Propagation.REQUIRED)
	    public  EmCurrencies getCurrencyByName(String name){
	        return this.currencyDAO.getByName(name);
	    }
}
