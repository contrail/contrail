/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/RuleTemplateManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.RuleParamDAO;
import org.slasoi.businessManager.common.dao.RuleTemplateDAO;
import org.slasoi.businessManager.common.drools.DroolsManager;
import org.slasoi.businessManager.common.drools.Rule;
import org.slasoi.businessManager.common.drools.RuleParam;
import org.slasoi.businessManager.common.drools.Template;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleParamsId;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.service.RuleTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="ruleTemplateService")
public class RuleTemplateManagerImpl implements RuleTemplateManager {

		static Logger log = Logger.getLogger(RuleTemplateManagerImpl.class);
	
		@Autowired
		private RuleTemplateDAO ruleTemplateDAO;
		
		@Autowired
		private RuleParamDAO ruleParamDAO;
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.RuleTemplateManager#getRuleTemplateById(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmRuleTemplates getRuleTemplateById(Long id){
	    	log.info("getRuleTemplateById():Id:"+id);
	    	EmRuleTemplates result = ruleTemplateDAO.load(id);
	    	ruleTemplateDAO.getHibernateTemplate().initialize(result.getEmRuleParamses());
	    	return result;
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.RuleTemplateManager#getRuleTemplates()
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmRuleTemplates> getRuleTemplates(){
	    	log.info("getRuleTemplates()");
	    	return ruleTemplateDAO.getList();
	    }
		

		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.RuleTemplateManager#saveOrUpdateTemplate(org.slasoi.businessManager.model.EmRuleTemplates, java.util.List, java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void saveOrUpdateTemplate(EmRuleTemplates template, List<String> params, List<String> deleteParams) throws Exception{

			//Validate rule template with drools
			validateTemplate(template, params);
			
			if(template.getNuRuleTemplateId()==0)
				ruleTemplateDAO.save(template);
			
			else{
				ruleTemplateDAO.update(template);
				
				//Delete rule params of delete Params List
				Iterator<String> itDeleteParams = deleteParams.iterator();
				while(itDeleteParams.hasNext()){
					EmRuleParamsId id = new EmRuleParamsId(new Long(itDeleteParams.next()), template.getNuRuleTemplateId());
					log.info("Deleting RuleParamId: "+id.getNuParamId()+" - Template Id: "+template.getNuRuleTemplateId());
					EmRuleParams rp = new EmRuleParams(id);
					ruleParamDAO.delete(rp);
				}
			}
			
			Iterator<String> itParams = params.iterator();
			long order = 1;
			while(itParams.hasNext()){
				String stId = itParams.next();
				log.info("Insert RuleParamId: "+stId+" - Template Id: "+template.getNuRuleTemplateId());
				EmRuleParamsId rpId = new EmRuleParamsId(new Long(stId), template.getNuRuleTemplateId());
				EmRuleParams rp = new EmRuleParams(rpId);
				rp.setNuParamRuleOrder(order);
				order++;
				template.getEmRuleParamses().add(rp);
			}
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.RuleTemplateManager#deleteTemplates(java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void deleteTemplates(List<String> ids ){
			log.info("deleteTemplates():Ids:"+ids);
			Iterator<String> itlPos = ids.iterator();
			while (itlPos.hasNext()) {
				ruleTemplateDAO.deleteById(Long.parseLong(itlPos.next()));
			}
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.RuleTemplateManager#validateTemplate(org.slasoi.businessManager.model.EmRuleTemplates, java.util.List)
		 */
		public void validateTemplate(EmRuleTemplates em_template, List<String> ruleParams) throws Exception{

			// Create new drools template
			Template template = new Template(em_template.getTxName(), em_template.getTxDescription(), em_template.getTxTemplateDrt());
		
			// Get rule_params of new em_template for compile test...
			ArrayList<RuleParam> aRuleParams = new ArrayList<RuleParam>();
			Iterator<String> itRuleParams = ruleParams.iterator();
			while (itRuleParams.hasNext()) {
				String ruleParamId = itRuleParams.next();
				// Fix RuleParam default value(0)
				RuleParam rp = new RuleParam(new Long(ruleParamId), "0");
				aRuleParams.add(rp);
			}
		
			// Add Test Rule to Template
			Rule rule = new Rule(new Long(0), "Rule Test Compile", aRuleParams);
			template.getRules().add(rule);
		
			DroolsManager dm = new DroolsManager();
			dm.compileTemplate(template);
		
		}
	
}
