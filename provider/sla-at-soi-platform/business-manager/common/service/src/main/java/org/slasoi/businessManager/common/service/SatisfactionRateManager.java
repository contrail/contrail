/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/SatisfactionRateManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slasoi.businessManager.common.model.EmAttributes;
import org.slasoi.businessManager.common.model.EmSpServSatisfactionRate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface SatisfactionRateManager {

	/**
	 * Gets the attribute.
	 * 
	 * @param attributeName the attribute name
	 * 
	 * @return the attribute
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract Long getAttribute(String attributeName);
	
	/**
	 * Sets the service attributes.
	 * 
	 * @param serviceId the service id
	 * @param attributeIds the attribute ids
	 */
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public abstract void setServiceAttributes(Long serviceId, List<Long> attributeIds);	
	
	/**
	 * Gets the service attributes.
	 * 
	 * @param serviceId the service id
	 * 
	 * @return the service attributes
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract List<EmAttributes> getServiceAttributes(Long serviceId);
	

	/**
	 * Gets the service satisfaction rate.
	 * 
	 * @param partyId the party id
	 * @param serviceId the service id
	 * 
	 * @return the service satisfaction rate
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract HashMap<String, Long> getServiceSatisfactionRate(Long partyId, Long serviceId );
	

	
	/**
	 * Sets the service satisfaction rate.
	 * 
	 * @param partyId the party id
	 * @param serviceId the service id
	 * @param attributeId the attribute id
	 * @param valueAttribute the value attribute
	 */
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public abstract void setServiceSatisfactionRate(String txBslaid, Long serviceId, Long attributeId, Long valueAttribute, String comment);
	
	/**
	 * Gets the service satisfaction rate average.
	 * 
	 * @param date the date
	 * @param serviceId the service id
	 * @param attributeId the attribute id
	 * 
	 * @return the service satisfaction rate
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract double getServiceSatisfactionRateAvg(Date date, Long serviceId, Long attributeId);
	
	
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public abstract void saveSatisfactionRates(List<EmSpServSatisfactionRate> rates) throws Exception;
	
}
