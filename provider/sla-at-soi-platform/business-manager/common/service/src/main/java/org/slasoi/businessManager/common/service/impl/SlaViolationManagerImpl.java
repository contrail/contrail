/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PartyManagerImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.SlaViolationDAO;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.service.SlaViolationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="slaViolationService")
public class SlaViolationManagerImpl implements SlaViolationManager {

		private Logger logger = Logger.getLogger(SlaViolationManagerImpl.class);
	
		@Autowired
		private SlaViolationDAO slaViolationDAO;
		
		
		@Transactional(propagation=Propagation.REQUIRED)
	    public void saveOrUpdate(EmSlaViolation EmSlaViolation){
	    	logger.debug("saveOrUpdate()");
	    	slaViolationDAO.saveOrUpdate(EmSlaViolation);
	    }        
        
        @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
        public void delete(EmSlaViolation EmSlaViolation){
            logger.debug("delete(EmSlaViolation EmSlaViolation)");           
            slaViolationDAO.delete(EmSlaViolation);
        }   
		
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void delete(List<EmSlaViolation> slaViolationList){
	    	
		    logger.debug("delete(List<EmSlaViolation> slaViolationList)");
	    	
	    	for (EmSlaViolation EmSlaViolation: slaViolationList){
	    		delete(EmSlaViolation);
	    	}
	    }

		@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
        public List<EmSlaViolation> getViolationsBySlaIdInListAndTime(List<String> slaIdList, Date penaltyDate) {
           return slaViolationDAO.getViolationsBySlaIdInListAndTime(slaIdList,penaltyDate);
        }

		@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
        public List<EmSlaViolation> getViolationsBySlaIdAndGTidAndEndTimeNull(String slaId, String gtId) {
           return slaViolationDAO.getViolationsBySlaIdAndGTidAndEndTimeNull(slaId, gtId);
        }

		@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
        public List<EmSlaViolation> getViolationsBySlaIdAndGTid(String slaId, String gtId) {
           return slaViolationDAO.getViolationsBySlaIdAndGTid(slaId, gtId);
        }

		@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
        public List<EmSlaViolation> getViolationsBySlaIdAndGTidAndDateBetween(String slaId, String gtId, Date startDate,
                Date endDate) {
           return slaViolationDAO.getViolationsBySlaIdAndGTidAndDateBetween(slaId, gtId, startDate,endDate);
        }

		@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
        public List<EmSlaViolation> getViolationsById(String violationId) {
           return slaViolationDAO.getViolationsById(violationId);
        }
		
}
