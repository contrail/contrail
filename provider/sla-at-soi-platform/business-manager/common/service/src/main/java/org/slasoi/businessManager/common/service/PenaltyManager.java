/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 835 $
 * @lastrevision   $Date: 2011-02-25 12:52:13 +0100 (Fri, 25 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/PolicyManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.Date;
import java.util.List;

import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PenaltyManager {


     @Transactional(propagation = Propagation.REQUIRED)
	 public void saveOrUpdate(EmPenalty EmPenalty);
	 
     @Transactional(propagation = Propagation.REQUIRED)
	 public void delete(EmPenalty event);
	 
     @Transactional(propagation = Propagation.REQUIRED)
     public void delete(List<EmPenalty> penaltyList);
     
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
     public List<EmPenalty> getPenaltiesByViolationsSlatIdOrderedByDate(String slaId, boolean asc);
     
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	 public List<EmPenalty> getPenaltiesOrderedByDate(String slaId,boolean asc);
	 
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	 public List<EmPenalty> getPenaltiesPeriodOrderedByDate(String slaId, Date startDate, Date endDate);
	 	 
	 /*
	     * DBManager.createCriteria(Penalty.class).add(
	     * Restrictions.between("penaltyDate", startDate, endDate)).addOrder(
	     * Order.asc("penaltyDate")).createCriteria("violation").add( Restrictions.eq("slaId", id));
	     */
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	  public List<EmPenalty> getPenaltiesByViolationsSlatIdAndDateBetween(String slaId, Date startDate, Date endDate);
	    
	  /*DBManager.createCriteria(Penalty.class).createCriteria("violation").add(
	          Restrictions.eq("slaId", slaId.toString()));*/
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	  public List<EmPenalty> getPenaltiesByViolationSlaId(String slaId);
	  
	  /*DBManager.createCriteria(Penalty.class).add(Restrictions.eq("penaltyDate", auxDate))
	  .createCriteria("violation").add(Restrictions.in("slaId", slaIds));*/
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	  public List<EmPenalty> getPenaltiesByDateAndViolationSlaIdInList(Date penaltyDate,List<String> slaIdList);
	  
	  
	 /* DBManager.createCriteria(Penalty.class).createCriteria("violation").add(
	          Restrictions.eq("slaId", slaId.toString())).add(
	          Restrictions.eq("guaranteeTermId", gt.getId()));
	  */
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	  public List<EmPenalty> getPenaltiesByViolationSlaIdAndGTid(String slaId, String guaranteeTermId);
	  
	  
	  /* DBManager.createCriteria(Penalty.class).createCriteria("violation").add(
	          Restrictions.eq("slaId", slaId.toString())).add(
	          Restrictions.eq("guaranteeTermId", gt.getId().toString())).add(
	          Restrictions.between("initialTime", startDate, endDate));
	  */
     @Transactional(propagation = Propagation.REQUIRED,readOnly=true)
	  public List<EmPenalty> getPenaltiesByViolationSlaIdAndGTidAndInitTimeBetween(
	          String slaId, String guaranteeTermId,Date startDate, Date endDate);
	  
	 
}