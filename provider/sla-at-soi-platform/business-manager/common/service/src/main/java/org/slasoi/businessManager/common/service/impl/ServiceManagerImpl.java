/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 493 $
 * @lastrevision   $Date: 2011-01-25 17:03:20 +0100 (Tue, 25 Jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/ServiceManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.slasoi.businessManager.common.dao.ProductDAO;
import org.slasoi.businessManager.common.dao.ServiceCharacteristicDAO;
import org.slasoi.businessManager.common.dao.ServiceDAO;
import org.slasoi.businessManager.common.dao.ServiceSpecificationDAO;
import org.slasoi.businessManager.common.dao.SlspDAO;
import org.slasoi.businessManager.common.model.EmServiceSpecification;
import org.slasoi.businessManager.common.model.EmSlsp;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.model.EmSpServicesCharacteristic;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="serviceService")
public class ServiceManagerImpl implements ServiceManager {

	static Logger log = Logger.getLogger(ServiceManagerImpl.class);

	@Autowired
	private ServiceDAO serviceDAO;

	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private ServiceCharacteristicDAO serviceCharacteristicDAO;
	
	@Autowired
	private ServiceSpecificationDAO serviceSpecificationDAO;
	
	@Autowired
	private SlspDAO slspDAO;
	

	/* (non-Javadoc)
	 * @see org.slasoi.businesManager.common.service.impl.ServiceManager#getServiceById(java.lang.Long)
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public EmSpServices getServiceById(Long id){
		EmSpServices service = serviceDAO.load(id);
		Hibernate.initialize(service.getEmParty());
		Hibernate.initialize(service.getEmServiceType());
		Hibernate.initialize(service.getEmServiceSpecifications());
		Hibernate.initialize(service.getEmSpServicesCharacteristics());
		Hibernate.initialize(service.getEmAttributeses());
		Hibernate.initialize(service.getEmSlsps());
		return service;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<EmSpServices> getServicesByParty(Long partyId){
		log.info("getServicesByParty()");
		return serviceDAO.getServicesByParty(partyId);
	}
	
	/* (non-Javadoc)
	 * @see org.slasoi.businesManager.common.service.impl.ServiceManager#getAllServices()
	 */
	@Transactional(propagation=Propagation.REQUIRED)
    public List<EmSpServices> getAllServices(){
    	log.info("getAllServices()");
    	return serviceDAO.getList();
    }	

	/* (non-Javadoc)
	 * @see org.slasoi.businesManager.common.service.impl.ServiceManager#findServices(java.lang.String)
	 */
	@Transactional(propagation=Propagation.REQUIRED)
    public List<EmSpServices> findServices(String filter){
		if(filter!=null) filter="%"+filter+"%";
    	log.info("findServices()");
    	return serviceDAO.findByFilter(filter);
    }	
	
	@Transactional(propagation=Propagation.REQUIRED)
    public List<EmSpServices> findPartyServices(Long partyId, String filter){
		if(filter!=null) filter="%"+filter+"%";
    	log.info("findPartyServices()");
    	return serviceDAO.findByParty(partyId, filter);
    }	

	/* (non-Javadoc)
	 * @see org.slasoi.businesManager.common.service.impl.ServiceManager#saveOrUpdate(org.slasoi.businessManager.common.model.EmSpServices)
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdate(EmSpServices service) {
		this.serviceDAO.saveOrUpdate(service);
	}
	
	 @Transactional(propagation = Propagation.REQUIRED)
	 public  EmSpServices getServiceBySlatID(String slatID){		     
	     log.info("getServiceBySlatID()");
            return serviceDAO.getServiceBySlatId(slatID);
	 }
	
	 
	 @Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	 public void saveService(EmSpServices service, List<Long> categories, ArrayList<EmSpServicesCharacteristic> characteristics, List<String> slsps){
		 log.info("saveService()");
		 
		 //if ID!=0 then update process, remove Service Characteristic of the service
		 if(service.getNuServiceid()!=null && service.getNuServiceid()>0)
			 serviceCharacteristicDAO.deleteServiceCharacteristic(service.getNuServiceid());
		 
		 for(Long nuCategory : categories){
			 EmServiceSpecification category = serviceSpecificationDAO.load(nuCategory);
			 service.getEmServiceSpecifications().add(category);
		 }
		 
		 //Save the Slsps
		 if(slsps!=null){
			 for(String slspName : slsps){
				 EmSlsp slsp = slspDAO.getByName(slspName);
				 if(slsp==null){
					 slsp = new EmSlsp(slspName); 
					 slspDAO.save(slsp);
				 }
				 //Add slsp to the Service
				 service.getEmSlsps().add(slsp);
			 }
		 }
		 
		 serviceDAO.saveOrUpdate(service);
		 
		 //Save the characteristics
		 for(EmSpServicesCharacteristic characteristic : characteristics ){
			 characteristic.getId().setNuServiceid(service.getNuServiceid());
			 characteristic.setEmSpServices(service);
			 serviceCharacteristicDAO.save(characteristic);
		 }
		 
	 }
	 
	 @Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	 public void deleteService(Long nuServiceid){
		 EmSpServices obj = serviceDAO.load(nuServiceid);
		 serviceDAO.delete(obj);
	 }
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	 public boolean isModifiable(Long nuServiceid){
		 boolean modifiable = true;
		 List<EmSpProducts> products = productDAO.getProductByService(nuServiceid);
		 if(products!=null && !products.isEmpty())
			modifiable = false;
		 
		 return modifiable;
	 }
	
}
