/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/SatisfactionRateManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.AttributeDAO;
import org.slasoi.businessManager.common.dao.SatisfactionRateAverageDAO;
import org.slasoi.businessManager.common.dao.SatisfactionRateDAO;
import org.slasoi.businessManager.common.dao.ServiceDAO;
import org.slasoi.businessManager.common.model.EmAttributes;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpServSatRateAgr;
import org.slasoi.businessManager.common.model.EmSpServSatRateAgrId;
import org.slasoi.businessManager.common.model.EmSpServSatisfactionRate;
import org.slasoi.businessManager.common.model.EmSpServSatisfactionRateId;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.SatisfactionRateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service (value="satisfactionRateService")
public class SatisfactionRateManagerImpl implements SatisfactionRateManager {

		static Logger log = Logger.getLogger(SatisfactionRateManagerImpl.class);
	
		@Autowired
		private AttributeDAO attributeDAO;
		@Autowired
		private ServiceDAO serviceDAO;
		@Autowired
		private SatisfactionRateDAO satisfactionRateDAO;
		@Autowired
		private SatisfactionRateAverageDAO satisfactionRateAverageDAO;
		

		@Transactional(propagation=Propagation.REQUIRED)
		public Long getAttribute(String attributeName) {
			// TODO Auto-generated method stub
			return attributeDAO.getAttributeByName(attributeName);
		}
		
		@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
		public void setServiceAttributes(Long serviceId, List<Long> attributeIds) {
			// TODO Auto-generated method stub
			//Insert service with attribute
			EmSpServices emSpServices = serviceDAO.load(serviceId);
		
			for(Long attributeId:attributeIds){
				EmAttributes attributes = new EmAttributes();
				attributes.setNuAttributeid(attributeId);
				emSpServices.getEmAttributeses().add(attributes);
			}
			serviceDAO.save(emSpServices);			
			
		}		
		@Transactional(propagation=Propagation.REQUIRED)
		public List<EmAttributes> getServiceAttributes(Long serviceId) {
			// TODO Auto-generated method stub
			EmSpServices emSpServices = serviceDAO.load(serviceId);
			return new ArrayList(emSpServices.getEmAttributeses());
		}


		@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
		public void setServiceSatisfactionRate(String txBslaid, Long serviceId,
				Long attributeId, Long valueAttribute, String comment) {
			// TODO Auto-generated method stub
			EmSpServSatisfactionRateId emSpServSatisfactionRateId = new EmSpServSatisfactionRateId();
			emSpServSatisfactionRateId.setNuAttributeid(attributeId);
			emSpServSatisfactionRateId.setTxBslaid(txBslaid);
			emSpServSatisfactionRateId.setNuServiceid(serviceId);
			EmSpServSatisfactionRate emSpServSatisfactionRate = new EmSpServSatisfactionRate();
			emSpServSatisfactionRate.setId(emSpServSatisfactionRateId);
			emSpServSatisfactionRate.setNuSatisfactionRate(valueAttribute);
			emSpServSatisfactionRate.setTxReviewComment(comment);
			satisfactionRateDAO.save(emSpServSatisfactionRate);			
			
		}
		@Transactional(propagation=Propagation.REQUIRED)
		public double getServiceSatisfactionRateAvg(Date date, Long serviceId,
				Long attributeId) {
			// TODO Auto-generated method stub
			EmSpServSatRateAgrId id = new EmSpServSatRateAgrId();
			id.setDtDate(date);
			id.setNuAttributeid(attributeId);
			id.setNuServiceid(serviceId);
			EmSpServSatRateAgr agr = satisfactionRateAverageDAO.load(id);
			return satisfactionRateAverageDAO.load(id).getNuAvgRate().doubleValue();
		}

		public HashMap<String, Long> getServiceSatisfactionRate(Long partyId, Long serviceId) {
			// TODO Auto-generated method stub
			HashMap<String, Long> mapReturn = new HashMap<String, Long>();
			EmParty idParty = new EmParty();
			EmSpServices idService = new EmSpServices();
			idParty.setNuPartyid(partyId);
			idService.setNuServiceid(serviceId);
			List<EmSpServSatisfactionRate> services = satisfactionRateDAO.getAttributeByPartyService(idParty, idService);
			for(EmSpServSatisfactionRate emSpServSatisfactionRate:services){
				mapReturn.put(emSpServSatisfactionRate.getEmAttributes().getTxName(), emSpServSatisfactionRate.getNuSatisfactionRate());
			}
			return mapReturn;
		}

		
		@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
		public void saveSatisfactionRates(List<EmSpServSatisfactionRate> rates) throws Exception{
			for(EmSpServSatisfactionRate rate : rates)
				satisfactionRateDAO.saveOrUpdate(rate);
		}
	
	
}
