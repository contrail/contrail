/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service;

import java.util.Set;

import org.slasoi.businessManager.common.model.pricing.CompoundResource;
import org.slasoi.businessManager.common.model.pricing.OvfDescriptor;
import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PricedItemManager {
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract PricedItem getPricedItemByName(String name);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract PricedItem getPricedItemById(Long id);
	
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract Long createPricedItem(String name, String description, boolean reservation);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdatePricedItem(PricedItem pricedItem);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void deletePricedItem(Long productId);
    
    @Transactional(propagation = Propagation.REQUIRED)
	public abstract UnitResource getUnitResourceByName(String name);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract UnitResource getUnitResourceByType(String type);
    
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract UnitResource getUnitResourceById(Long id);
	
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract Long createUnitResource(String name, String description, boolean reservation, String instanceId, String type, String unit);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdateUnitResource(UnitResource unitResource);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void deleteUnitResource(Long unitResourceId);
    
    @Transactional(propagation = Propagation.REQUIRED)
	public abstract CompoundResource getCompoundResourceByName(String name);
    
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract CompoundResource getCompoundResourceById(Long id);
	
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract Long createCompoundResource(String name, String description, boolean reservation, Set<UnitResource> unitResources, Set<CompoundResource> compoundResourcesOwn);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdateCompoundResource(CompoundResource compoundResource);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void deleteCompoundResource(Long compoundResourceId);
    
    @Transactional(propagation = Propagation.REQUIRED)
	public abstract OvfResource getOvfResourceByName(String name);
    
/*    @Transactional(propagation = Propagation.REQUIRED)
    public abstract OvfResource getOvfResourceByOvfId(String ovfId);*/
    
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract OvfResource getOvfResourceById(Long id);
	
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract Long createOvfResource( String name, String description, boolean reservation, String id, String instanceId, String type, String info, Set<UnitResource> unitResources, Set<CompoundResource> compoundResourcesOwn);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdateOvfResource(OvfResource ovfResource);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void deleteOvfResource(Long ovfResourceId);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract OvfDescriptor getOvfDescriptorById(Long id);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract Long createOvfDescriptor(String name, String slaTemplateId, Set<OvfResource> ovfResources);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdateOvfDescriptor(OvfDescriptor ovfDescriptor);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void deleteOvfDescriptor(Long ovfDescriptorId);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract OvfResource findOvfResource(String ovfDescrName, String ovfId);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract OvfResource findOvfResourceByOvfId(Long ovfDId, String ovfId);
}
