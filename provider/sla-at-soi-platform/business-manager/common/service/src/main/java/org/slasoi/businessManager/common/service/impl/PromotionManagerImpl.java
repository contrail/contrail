/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 835 $
 * @lastrevision   $Date: 2011-02-25 12:52:13 +0100 (Fri, 25 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PromotionManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ParamTypeDAO;
import org.slasoi.businessManager.common.dao.ProductPromotionDAO;
import org.slasoi.businessManager.common.dao.PromotionDAO;
import org.slasoi.businessManager.common.dao.PromotionTypeDAO;
import org.slasoi.businessManager.common.model.EmParamType;
import org.slasoi.businessManager.common.model.EmProductPromotion;
import org.slasoi.businessManager.common.model.EmProductPromotionId;
import org.slasoi.businessManager.common.model.EmPromotionType;
import org.slasoi.businessManager.common.model.EmPromotionValues;
import org.slasoi.businessManager.common.model.EmPromotionValuesId;
import org.slasoi.businessManager.common.model.EmPromotions;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.service.PromotionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="promotionService")
public class PromotionManagerImpl implements PromotionManager {

		static Logger log = Logger.getLogger(PromotionManagerImpl.class);
	
		@Autowired
		private PromotionDAO promotionDAO;

		@Autowired
		private PromotionTypeDAO promotionTypeDAO;
		
		@Autowired
		private ProductPromotionDAO productPromotionDAO;

		@Autowired
		private ParamTypeDAO paramTypeDAO;
		
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#getPromotions()
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmPromotions> getPromotions(){
	    	log.info("getPromotions()");
	    	return promotionDAO.getList();
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#getPromotionById(java.lang.Long)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmPromotions getPromotionById(Long id){
	    	log.info("getRuleTemplateById():Id:"+id);
	    	EmPromotions result = promotionDAO.load(id);
	    	promotionDAO.getHibernateTemplate().initialize(result.getEmPromotionValueses());
	    	
	    	//Change the value if ruleParamType = SELECT
	    	loadPromotionRefValues(result.getEmPromotionValueses());
	    	
	    	return result;
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#getTemplateParams(java.lang.Long)
         */
		@Transactional(propagation=Propagation.REQUIRED)
		public List<EmPromotionValues> getTemplateParams(Long promotionTypeId) throws Exception{
			
			log.info("getTemplateParams(): TypeId >> "+promotionTypeId);
			
			EmPromotionType promoType = promotionTypeDAO.load(promotionTypeId);
			EmRuleTemplates template = promoType.getEmRuleTemplates();

			//Load Template parameters, and asociate with PromotionValues
			Vector<EmPromotionValues> params = new Vector<EmPromotionValues>();
			Iterator<EmRuleParams> itRuleParams = template.getEmRuleParamses().iterator();
			while(itRuleParams.hasNext()){
				EmRuleParams ruleParam = itRuleParams.next();
				EmPromotionValues promoValue = new EmPromotionValues();
				promoValue.setEmRuleParams(ruleParam);
				
				//Load Param Type Values, in function of txParamTypeName
				EmParamType paramType = ruleParam.getEmParams().getEmParamType();
				paramTypeDAO.loadParamValues(paramType);
				params.add(promoValue);
			}
			
			return params;
			
		}
		

		@Transactional(propagation=Propagation.REQUIRED)
		public List<EmPromotionValues> getPromotionValues(Long id) throws Exception{
			
			log.info("getPromotionValues(): promotionId >> "+id);

			EmPromotions promo = promotionDAO.load(id);
			promotionDAO.getHibernateTemplate().initialize(promo.getEmPromotionValueses());
			
			Vector<EmPromotionValues> params = new Vector<EmPromotionValues>();
			Iterator<EmPromotionValues> itPromoValues = promo.getEmPromotionValueses().iterator();
			while(itPromoValues.hasNext()){
				EmPromotionValues promoValue = itPromoValues.next();
				EmRuleParams ruleParam = promoValue.getEmRuleParams();
				
				//Load Param Type Values, in function of txParamTypeName
				EmParamType paramType = ruleParam.getEmParams().getEmParamType();
				paramTypeDAO.loadParamValues(paramType);
				params.add(promoValue);
			}
			
			Collections.sort(params);
			return params;

		}
		
		
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#loadPromotionRefValues(java.util.Set)
         */
		@Transactional(propagation=Propagation.REQUIRED)
		public void loadPromotionRefValues(Set<EmPromotionValues> promoValues){
			
			//if param Type = SELECT, load value of reference TABLE
			Iterator<EmPromotionValues> itValues = promoValues.iterator();
			while(itValues.hasNext()){
				EmPromotionValues value = itValues.next();
				EmParamType paramType = value.getEmRuleParams().getEmParams().getEmParamType(); 
				if(paramType.getTxParamTypeName().equals(Constants.PARAM_TYPE_SELECT)){
					String refValue = paramTypeDAO.getRefValue(paramType, value.getTxParamValue());
					if(refValue!=null && refValue.length()>0)
						value.setTxRefParamValue(refValue);
				}
			}
		}
		
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#savePromotion(org.slasoi.businessManager.common.model.EmPromotions, java.lang.Long, java.lang.Long, java.util.List, java.util.List)
         */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void savePromotion(EmPromotions promotion, Long nuProductId, Long nuPromotionTypeId, 
				List<Long> paramIds, List<String> paramValues) throws Exception{
			
			log.info("savePromotion()");
			
			EmPromotionType promotionType = promotionTypeDAO.load(nuPromotionTypeId);
			Long ruleTemplateId = promotionType.getEmRuleTemplates().getNuRuleTemplateId();
			
			log.info("Promotion ID >>>>>>>> "+promotion.getNuPromotionId());
			promotion.setEmPromotionType(promotionType);
			
			if(promotion.getNuPromotionId() == null || promotion.getNuPromotionId() == 0){
				promotionDAO.save(promotion);
				//Create Product Promotion Relation
				EmProductPromotionId productPromotionId = new EmProductPromotionId(nuProductId, promotion.getNuPromotionId());
				EmProductPromotion productPromotion = new EmProductPromotion(productPromotionId, Constants.STATE_PENDING);
				productPromotionDAO.save(productPromotion);
			}				
			else promotionDAO.update(promotion);
			
			log.info("Promotion ID After Save >>>>>>>> "+promotion.getNuPromotionId());
			
			log.info("Param Size >>>>>>> "+paramIds.size());
			//After save promotion Adds the parameter values... 
			for(int i=0; i<paramIds.size(); i++){
				EmPromotionValuesId id = new EmPromotionValuesId(paramIds.get(i), promotion.getNuPromotionId(), ruleTemplateId);
				EmPromotionValues promoValue = new EmPromotionValues(id, paramValues.get(i));
				promotion.getEmPromotionValueses().add(promoValue);
				log.info("New Promo Value Added: "+paramValues.get(i));
			}
		}
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PromotionManager#deletePromotion(org.slasoi.businessManager.common.model.EmPromotions)
         */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void deletePromotion(EmPromotions promotion){
			log.info("deletePromotion()");
			promotionDAO.delete(promotion);
		}
		
}
