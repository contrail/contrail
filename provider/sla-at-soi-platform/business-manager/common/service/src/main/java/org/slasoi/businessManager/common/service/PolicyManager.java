/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 835 $
 * @lastrevision   $Date: 2011-02-25 12:52:13 +0100 (Fri, 25 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/PolicyManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.List;
import java.util.Set;

import org.slasoi.businessManager.common.model.EmPolicies;
import org.slasoi.businessManager.common.model.EmPolicyValues;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PolicyManager {

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<EmPolicies> getPolicies();

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract EmPolicies getPolicyById(Long id);

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<EmPolicyValues> getTemplateParams(Long policyTypeId)
			throws Exception;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<EmPolicyValues> loadPolicyParams(Long id) throws Exception;

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void loadPolicyRefValues(Set<EmPolicyValues> policyValues);

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public abstract void deletePolicy(EmPolicies policy);

	/**
	 * Get the policies under product id
	 * @param productId
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract EmSpProducts getProductOffersPolicies(Long productId);

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public abstract void savePolicy(EmPolicies policy, Long nuPolicyTypeId,
			Long nuProductOfferId, Long nuServiceId, List<Long> paramIds,
			List<String> paramValues) throws Exception;

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public abstract void savePoliciesStatus(List<Long> policiesIds,
			List<String> policiesStatus);

}