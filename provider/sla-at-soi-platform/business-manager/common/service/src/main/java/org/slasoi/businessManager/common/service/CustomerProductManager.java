/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 736 $
 * @lastrevision   $Date: 2011-02-17 12:08:48 +0100 (Thu, 17 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/CustomerProductManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.Date;
import java.util.List;

import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface CustomerProductManager {

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProducts();

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract EmCustomersProducts getCustomerProductById(String id);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsOfParty(EmParty party);

    /**
     * Load the customer product, based on the owner of the product
     * 
     * @param party
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsByPublisher(EmParty party);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsForRating(EmParty party);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsByCategory(EmParty party, String category);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsByDateAndState(Date date, String typeDate);
    
    /**
     * Get Customer products active that have not ended before the given Date
     * @param date
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsActiveByDate(Date date);
      

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void contractProductsOffer(EmParty party, List<Long> productsOffer) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void applyPromotions(EmCustomersProducts customer_product) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract EmCustomersProducts getVariationsByPromotion(Long party, Long productOfferId) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void saveCustomer(EmCustomersProducts customersProducts) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void updateCustomer(EmCustomersProducts customersProducts) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract EmCustomersProducts getCustomerProductByIdLoaded(String id);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsByProductOfferAndParty(EmSpProductsOffer productOffer,
            EmParty party);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract String getServiceProvisioningName(EmCustomersProducts customerProduct);   
    
    public abstract List<String> getBslatIds(EmParty party);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmCustomersProducts> getCustomerProductsByProduct(EmSpProducts product);

}
