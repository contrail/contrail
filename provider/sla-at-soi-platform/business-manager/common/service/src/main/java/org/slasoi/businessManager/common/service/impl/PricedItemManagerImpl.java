/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.CompoundResourceDAO;
import org.slasoi.businessManager.common.dao.GuaranteeDAO;
import org.slasoi.businessManager.common.dao.OvfDescriptorDAO;
import org.slasoi.businessManager.common.dao.OvfResourceDAO;
import org.slasoi.businessManager.common.dao.PricedItemDAO;
import org.slasoi.businessManager.common.dao.UnitResourceDAO;
import org.slasoi.businessManager.common.model.pricing.CompoundResource;
import org.slasoi.businessManager.common.model.pricing.OvfDescriptor;
import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.slasoi.businessManager.common.service.PriceManager;
import org.slasoi.businessManager.common.service.PricedItemManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="pricedItemService")
public class PricedItemManagerImpl implements PricedItemManager{

	static Logger log = Logger.getLogger(PricedItemManagerImpl.class);
	
	@Autowired
	private PricedItemDAO pricedItemDAO;
	
	@Autowired
	private UnitResourceDAO unitResourceDAO;
	
	@Autowired
	private GuaranteeDAO guaranteeDAO;
	
	@Autowired
	private CompoundResourceDAO compoundResourceDAO;
	
	@Autowired
	private OvfResourceDAO ovfResourceDAO;
	
	@Autowired
	private OvfDescriptorDAO ovfDescriptorDAO;
	
	@Autowired
	private PriceManager priceService;
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	public PricedItem getPricedItemByName(String name) {
		log.info("getPricedItemByName():Name:"+name);
		return pricedItemDAO.findPricedItemByName(name);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public PricedItem getPricedItemById(Long id) {
		log.info("getPricedItemById():Id:"+id);
		PricedItem pI=pricedItemDAO.load(id);
		return pI;
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public Long createPricedItem(String name, String description, boolean reservation){
    	log.info("saveOrUpdatePricedItem()");
    	PricedItem pi=new PricedItem();
		pi.setProductName(name);
		pi.setProductDescr(description);
		pi.setReservation(reservation);
    	pricedItemDAO.saveOrUpdate(pi);
    	return pi.getProductId();
    }
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdatePricedItem(PricedItem pricedItem) {
		log.info("deletePricedItem");
		pricedItemDAO.saveOrUpdate(pricedItem);
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deletePricedItem(Long productId) {
		log.info("deletePricedItemByName():Id:" + productId);
		PricedItem obj = pricedItemDAO.load(productId);
		if (obj != null) {
			Set<Price> prices = obj.getPrices();
			for (Price p : prices) {
				priceService.deletePrice(p.getPriceId());
			}
			/*Set<Guarantee> guarantees = obj.getGuarantees();
			for(Guarantee g : guarantees){
				g.getPricedItems().remove(obj);
				guaranteeDAO.saveOrUpdate(g);
			}
			guarantees.clear();
			obj.setGuarantees(guarantees);
			System.out.println("clear guarantees associated to pricedItem: "+obj.getProductId());
			pricedItemDAO.saveOrUpdate(obj);*/
			System.out.println("delete Priced Item id: " + obj.getProductId());
			pricedItemDAO.delete(obj);
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public UnitResource getUnitResourceByName(String name) {
		log.info("getUnitResourceByName():Name:"+name);
		PricedItem pi=pricedItemDAO.findPricedItemByName(name);
		return unitResourceDAO.load(pi.getProductId());
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public UnitResource getUnitResourceByType(String type) {
		log.info("getUnitResourceByType():Type:"+type);
		UnitResource ur=unitResourceDAO.findUnitResourceByType(type);
		return ur;
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public UnitResource getUnitResourceById(Long id) {
		log.info("getUnitResourceByID():Id:"+id);
		return unitResourceDAO.load(id);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createUnitResource(String name, String description, boolean reservation, String instanceId, String type, String unit) {
		log.info("createUnitResource");
		Long productId=createPricedItem(name, description, reservation);
		PricedItem pI=pricedItemDAO.load(productId);
		UnitResource ur=new UnitResource();
		ur.setPricedItem(pI);
		ur.setUnitResourceIstanceId(instanceId);
		ur.setUnitResourceType(type);
		ur.setUnitResourceUnit(unit);
		pI.setUnitResource(ur);
		saveOrUpdatePricedItem(pI);
		saveOrUpdateUnitResource(ur);
		return ur.getUnitResourceId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdateUnitResource(UnitResource unitResource) {
		log.info("saveOrUpdateUnitResource()");
		unitResourceDAO.saveOrUpdate(unitResource);
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteUnitResource(Long unitResourceId) {
		log.info("deleteUnitResource()");
		UnitResource obj = unitResourceDAO.load(unitResourceId);
		Set<CompoundResource> cr= obj.getCompoundResources();
		for(CompoundResource c : cr){
			deleteCompoundResource(c.getCompoundResourceId());
		}
		unitResourceDAO.delete(obj);
		pricedItemDAO.deleteById(unitResourceId);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public CompoundResource getCompoundResourceByName(String name) {
		log.info("getCompoundResourceByName():Name:"+name);
		PricedItem pi=pricedItemDAO.findPricedItemByName(name);
		return compoundResourceDAO.load(pi.getProductId());
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public CompoundResource getCompoundResourceById(Long id) {
		log.info("getCompoundResourceByID():Id:"+id);
		return compoundResourceDAO.load(id);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Long createCompoundResource(String name, String description, boolean reservation, Set<UnitResource> unitResources, Set<CompoundResource> compoundResourcesOwned) {
		log.info("createCompoundResource");
		Long productId=createPricedItem(name, description, reservation);
		PricedItem pI=pricedItemDAO.load(productId);
		CompoundResource cr=new CompoundResource();
		cr.setPricedItem(pI);
		if(compoundResourcesOwned!=null && !(compoundResourcesOwned.isEmpty())){
			cr.setCompoundResourcesOwned(compoundResourcesOwned);
			/*for(CompoundResource c : compoundResourcesOwn){
				Set<CompoundResource> cRs=c.getCompoundResourcesOwned();
				cRs.add(c);
				c.set
				saveOrUpdateCompoundResource(c);
			}*/
		}
		if(unitResources!=null && !(unitResources.isEmpty())){
			cr.setUnitResources(unitResources);
		}
		pI.setCompoundResource(cr);
		saveOrUpdatePricedItem(pI);
		saveOrUpdateCompoundResource(cr);
		return cr.getCompoundResourceId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdateCompoundResource(CompoundResource compoundResource) {
		log.info("saveOrUpdateCompoundResource()");
		compoundResourceDAO.saveOrUpdate(compoundResource);
		
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteCompoundResource(Long compoundResourceId) {
		log.info("deleteCompoundResource()");
		CompoundResource obj = compoundResourceDAO.load(compoundResourceId);
		// System.out.println("Id of CR Own: " + obj.getCompoundResourceId());
		Set<CompoundResource> cr = obj.getCompoundResourcesOwned();
		for (CompoundResource c : cr) {
			// System.out.println("id of cr owned by " +
			// obj.getCompoundResourceId() + ": " + c.getCompoundResourceId());
			deleteCompoundResource(c.getCompoundResourceId());
		}
		obj = compoundResourceDAO.load(compoundResourceId);
		OvfResource o = ovfResourceDAO.load(obj.getOvfResource().getOvfResourceId());
		if (o != null) {
			// System.out.println("delete ovfResource id: " +
			// obj.getOvfResource().getOvfResourceId() + " related to CR id: " +
			// obj.getCompoundResourceId());
			deleteOvfResource(obj.getOvfResource().getOvfResourceId());
		}
		CompoundResource c = compoundResourceDAO.load(obj.getCompoundResourceId());
		if (c != null) {
			System.out.println("delete CR id: " + obj.getCompoundResourceId());
			compoundResourceDAO.delete(obj);
		}
		deletePricedItem(obj.getPricedItem().getProductId());
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public OvfResource getOvfResourceByName(String name) {
		log.info("getOvfResourceByName():Name:"+name);
		PricedItem pi=pricedItemDAO.findPricedItemByName(name);
		return ovfResourceDAO.load(pi.getProductId());
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public OvfResource getOvfResourceById(Long id) {
		log.info("getOvfResourceByID():Id:"+id);
		return ovfResourceDAO.load(id);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Long createOvfResource( String name, String description, boolean reservation, String id, String instanceId, String type, String info, Set<UnitResource> unitResources, Set<CompoundResource> compoundResourcesOwned) {
		log.info("createOvfResource");
		Long crId=createCompoundResource(name, description, reservation, unitResources, compoundResourcesOwned);
		CompoundResource cr=compoundResourceDAO.load(crId);
		OvfResource oR=new OvfResource();
		oR.setCompoundResource(cr);
		oR.setOvfId(id);
		if(!instanceId.equals("-1"))
			oR.setInstanceId(instanceId);
		oR.setOvfResourceType(type);
		oR.setOvfResourceInfo(info);
		cr.setOvfResource(oR);
		saveOrUpdateCompoundResource(cr);
		saveOrUpdateOvfResource(oR);
		return oR.getOvfResourceId();
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void saveOrUpdateOvfResource(OvfResource ovfResource) {
		log.info("saveOrUpdateOvfResource()");
		ovfResourceDAO.saveOrUpdate(ovfResource);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteOvfResource(Long ovfResourceId) {
		log.info("deleteOvfResource()");
		OvfResource oR = ovfResourceDAO.load(ovfResourceId);
		if (oR != null) {
			System.out.println("delete ovfResource id: " + oR.getOvfResourceId());
			ovfResourceDAO.delete(oR);
			if (oR.getCompoundResource() != null)
				deleteCompoundResource(oR.getCompoundResource().getCompoundResourceId());
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public OvfDescriptor getOvfDescriptorById(Long id) {
		log.info("getOvfDescriptorByID():Id:"+id);
		return ovfDescriptorDAO.load(id);
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Long createOvfDescriptor(String name, String slaTemplateId, Set<OvfResource> ovfResources){
		log.info("createOvfDescriptor()");
		OvfDescriptor ovfDescriptor=new OvfDescriptor();
		ovfDescriptor.setOvfDescriptorName(name);
		ovfDescriptor.setSlaTemplateId(slaTemplateId);
		ovfDescriptor.setOvfResources(ovfResources);
		for(OvfResource or: ovfResources){
			or.setOvfDescriptor(ovfDescriptor);
			for(CompoundResource c : or.getCompoundResource().getCompoundResourcesOwned()){
				OvfResource o=c.getOvfResource();
				o.setOvfDescriptor(ovfDescriptor);
				saveOrUpdateOvfResource(o);
			}
			saveOrUpdateOvfResource(or);
		}
		saveOrUpdateOvfDescriptor(ovfDescriptor);
		return ovfDescriptor.getOvfDescriptorId();
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void saveOrUpdateOvfDescriptor(OvfDescriptor ovfDescriptor) {
		log.info("saveOrUpdateOvfDescriptor()");
		ovfDescriptorDAO.saveOrUpdate(ovfDescriptor);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteOvfDescriptor(Long ovfDescriptorId) {
		log.info("deleteOvfDescriptor()");
		OvfDescriptor od=ovfDescriptorDAO.load(ovfDescriptorId);
		Set<OvfResource> ovfResources=od.getOvfResources();
		for(OvfResource o : ovfResources){
			//System.out.println("delete OvfResource id: "+o.getOvfResourceId()+" by OvfD id: "+od.getOvfDescriptorId());
			deleteOvfResource(o.getOvfResourceId());
		}
		System.out.println("delete OvfDescriptor id: "+ od.getOvfDescriptorId());
		ovfDescriptorDAO.delete(od);
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public OvfResource findOvfResource(String ovfDescrName, String ovfId){
		log.info("findOvfResource()");
		OvfResource ovfRes = null;
		OvfDescriptor ovfD=ovfDescriptorDAO.findOvfDescriptorByOvfName(ovfDescrName);
		if(ovfD!=null){
			ovfRes=findOvfResourceByOvfId(ovfD.getOvfDescriptorId(), ovfId);
		}
		return ovfRes;
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public OvfResource findOvfResourceByOvfId(Long ovfDId, String ovfId){
		OvfResource o= ovfResourceDAO.findOvfResourceByOvfId(ovfDId, ovfId);
		if(o!=null)
			return ovfResourceDAO.load(o.getOvfResourceId());
		return o;
	}
	

}
