/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import org.slasoi.businessManager.common.model.pricing.CompoundFee;
import org.slasoi.businessManager.common.model.pricing.FixedFee;
import org.slasoi.businessManager.common.model.pricing.VariableFee;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PriceManager {
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Price getPriceById(Long id);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Long createPrice(BigDecimal value, String currencyType, String period, String type, int quantity, String dataType, Date validFrom, Date validUntil);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void saveOrUpdatePrice(Price price);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void deletePrice(Long priceId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract FixedFee getFixedFeeById(Long id);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Long createFixedFee(BigDecimal value, String currencyType, String period, String type, Date validFrom, Date validUntil, int quantity, String dataType);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void saveOrUpdateFixedFee(FixedFee fee);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void deleteFixedFee(Long fixedFeeId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract VariableFee getVariableFeeById(Long id);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Long createVariableFee(BigDecimal value, String currencyType, String period, String type, Date validFrom, Date validUntil, int quantity, String dataType, String cond, String condValue,
			String condDataType);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void saveOrUpdateVariableFee(VariableFee fee);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void deleteVariableFee(Long variableFeeId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract CompoundFee getCompoundFeeById(Long id);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Long createCompoundFee(String currencyType, String period, Date validFrom, Date validUntil, Set<VariableFee> variableFees, Set<FixedFee> fixedFees);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdateCompoundFee(CompoundFee fee);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void deleteCompoundFee(Long compoundFeeId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void associatePrice(Long productId, Long priceId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void associatePriceToGuarantee(Long guaranteeId, Long priceId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Price findCurrentPrice(Long productId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Price findPriceOfGuarantee(Long guaranteeId);
/*	@Transactional(propagation = Propagation.REQUIRED)
	public abstract double retrieveTotalPrice(CompoundFee compoundFee);	*/
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract boolean isCompound(Long priceId);	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract boolean isVariableFee(Long priceId);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract boolean isFixedFee(Long priceId);
	
	
}
