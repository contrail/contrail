/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/PromotionManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.List;
import java.util.Set;

import org.slasoi.businessManager.common.model.EmPromotionValues;
import org.slasoi.businessManager.common.model.EmPromotions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PromotionManager {

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmPromotions> getPromotions();

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract EmPromotions getPromotionById(Long id);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmPromotionValues> getTemplateParams(Long promotionTypeId) throws Exception;
    
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<EmPromotionValues> getPromotionValues(Long id)
			throws Exception;

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void loadPromotionRefValues(Set<EmPromotionValues> promoValues);

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void savePromotion(EmPromotions promotion, Long nuProductId, Long nuPromotionTypeId,
            List<Long> paramIds, List<String> paramValues) throws Exception;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void deletePromotion(EmPromotions promotion);

}
