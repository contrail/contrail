/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/PartyManager.java $
 */

package org.slasoi.businessManager.common.service;

import java.util.List;

import org.slasoi.businessManager.common.model.EmParty;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PartyManager {

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract EmParty getPartyById(Long id);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmParty> getParties();

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveOrUpdate(EmParty obj);

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract EmParty getPartyByIdLoaded(Long id);

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void deleteParties(List<Long> ids);   

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void deleteParty(Long id);
    
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract EmParty getByIndividualName(String firstName, String lastName);  
    
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract EmParty getByOrganizationName(String tradingName);     
    
    public abstract List<EmParty> getPartiesWithNegativeBalance()throws Exception;
    
    public abstract List<EmParty> getCustomers();

}
