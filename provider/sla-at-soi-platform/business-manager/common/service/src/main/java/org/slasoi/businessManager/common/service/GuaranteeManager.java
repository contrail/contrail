/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service;

import org.slasoi.businessManager.common.model.pricing.Guarantee;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface GuaranteeManager {

	@Transactional(propagation=Propagation.REQUIRED)
	public abstract Guarantee getGuaranteeById(Long id);
	
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract Guarantee getGuaranteeByName(String name);
	
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract Guarantee getGuaranteeByType(String type);
	
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract Long createGuarantee(String name, String slaTemplateId, String agreementTerm, String description, String type, String operator, String value, String valueType, double per_variation);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void deleteGuarantee(Long guaranteeId);
	
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract void saveOrUpdateGuarantee(Guarantee guarantee);
	
	@Transactional(propagation=Propagation.REQUIRED)
	public abstract void associateProduct(Long productId, Long guaranteeId);
}
