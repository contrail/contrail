/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/UserServiceImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.UserDAO;
import org.slasoi.businessManager.common.model.EmUsers;
import org.slasoi.businessManager.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="userService")
public class UserServiceImpl implements UserService {
    static Logger log = Logger.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserDAO userDAO;

    @Transactional(propagation = Propagation.REQUIRED)
    public EmUsers getUserId(Long id) {
        return this.userDAO.load(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdate(EmUsers obj) {
        this.userDAO.saveOrUpdate(obj);

    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  void save(EmUsers obj){
        
        this.userDAO.save(obj);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmUsers> getUsersByLoginPass(String user, String pass){
        return this.userDAO.getUsersByLoginPass(user, pass);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
	public List<EmUsers> getUsers() {
    	return this.userDAO.getList();
	}
    
    @Transactional(propagation = Propagation.REQUIRED)
	public void deleteUser(Long id) {
		EmUsers emUsers = this.userDAO.load(id);
		this.userDAO.delete(emUsers);
	}
    
    @Transactional(propagation = Propagation.REQUIRED)
    public EmUsers getUser(String login, String pass){
    	return userDAO.getUser(login, pass);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public EmUsers getUserByParty(Long partyId, String userLogin) {
    	return userDAO.getUserByParty(partyId, userLogin);
    }
        

}
