/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PartyManagerImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.slasoi.businessManager.common.dao.PartyDAO;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="partyService")
public class PartyManagerImpl implements PartyManager {

		static Logger log = Logger.getLogger(PartyManagerImpl.class);
	
		@Autowired
		private PartyDAO partyDAO;
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#getPartyById(java.lang.Long)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmParty getPartyById(Long id){
	    	log.info("getPartyById():Id:"+id);
	    	EmParty result = partyDAO.load(id);
	    	Hibernate.initialize(result.getEmPartyPartyroles());
	    	return result;
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#getParties()
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmParty> getParties(){
	    	log.info("getParties()");
	    	return partyDAO.getList();
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.PartyManager#saveOrUpdate(org.slasoi.businessManager.common.model.EmParty)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public void saveOrUpdate(EmParty obj){
	    	log.info("saveOrUpdate()");
	    	partyDAO.saveOrUpdate(obj);
	    }
/**
 * 		
 */
		@Transactional(propagation=Propagation.REQUIRED)
        public EmParty getPartyByIdLoaded(Long id){
            log.info("getPartyById():Id:"+id);
            EmParty result = partyDAO.getByIDLoad(id);
            return result;
        }
		
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deleteParties(List<Long> ids){
	    	log.info("deleteParties()");
	    	
	    	Iterator<Long> itIds = ids.iterator();
	    	while(itIds.hasNext()){
	    		EmParty party = partyDAO.load(itIds.next());
	    		partyDAO.delete(party);
	    	}
	    }		
	
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deleteParty(Long id){
	    	log.info("deleteParty()");
	    	EmParty party = partyDAO.load(id);
    		partyDAO.delete(party);
	    }	
		
	    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	    public EmParty getByIndividualName(String firstName, String lastName){
	    	return partyDAO.getByIndividualName(firstName, lastName);
	    }
	    
	    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	    public EmParty getByOrganizationName(String tradingName){
	    	return partyDAO.getByOrganizationName (tradingName);
	    }
	    
	    public  List<EmParty> getPartiesWithNegativeBalance()throws Exception{
	        return partyDAO.getPartiesWithNegativeBalance();
	    }
	    
	    public List<EmParty> getCustomers(){
	    	return partyDAO.getPartiesByRole(Constants.PARTY_ROLE_CUSTOMER);
	    }
}
