package org.slasoi.businessManager.common.service;

import java.util.List;

import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface GtTranslationManager {

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract List<EmGtTranslation> getGtTranslations();

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract EmGtTranslation getGtTranslationById(EmGtTranslationId id);
    

    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveGtTranslation(EmGtTranslation object);
    
    @Transactional(propagation = Propagation.REQUIRED)
    public abstract void saveGtTranslationsList(List <EmGtTranslation> object);

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract void deleteGtTranslation(EmGtTranslationId id);
    
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public abstract EmGtTranslation getGtTranslation(String type, EmGtTranslation translation);
    
   

}
