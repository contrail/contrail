/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 656 $
 * @lastrevision   $Date: 2011-02-08 16:50:02 +0100 (Tue, 08 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/ProductOfferManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.PriceVariationTypeDAO;
import org.slasoi.businessManager.common.dao.ProductOfferDAO;
import org.slasoi.businessManager.common.drools.DroolsManager;
import org.slasoi.businessManager.common.drools.Rule;
import org.slasoi.businessManager.common.drools.RuleParam;
import org.slasoi.businessManager.common.drools.Template;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmPolicies;
import org.slasoi.businessManager.common.model.EmPolicyType;
import org.slasoi.businessManager.common.model.EmPolicyValues;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.util.GTDifference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="productOfferService")
public class ProductOfferManagerImpl implements ProductOfferManager {

		static Logger log = Logger.getLogger(ProductOfferManagerImpl.class);
	
		@Autowired
		private ProductOfferDAO productOfferDAO;
		
		@Autowired
        private PriceVariationTypeDAO priceVariationTypeDAO;
		
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmSpProductsOffer getProductOfferById(Long id){
	    	log.info("getProductOfferById():Id:"+id);
	    	EmSpProductsOffer result = productOfferDAO.load(id);
	    	
	    	//Load component Prices, Geographical Areas and Services with Partys
	    	productOfferDAO.getHibernateTemplate().initialize(result.getEmComponentPrices());
	    	productOfferDAO.getHibernateTemplate().initialize(result.getEmGeographicalAreases());
	    	for(EmSpServices service : result.getEmSpServiceses())
	    		productOfferDAO.getHibernateTemplate().initialize(service.getEmParty());
	    	
	    	return result;
	    }
		
		/* (non-Javadoc)
         * @see org.slasoi.businessManager.common.service.impl.ProductOfferManager#saveOrUpdate(org.slasoi.businessManager.common.model.EmSpProductsOffer)
         */
		@Transactional(propagation=Propagation.REQUIRED)
	    public void saveOrUpdate(EmSpProductsOffer obj){
	    	log.info("saveOrUpdate()");
	    	productOfferDAO.saveOrUpdate(obj);
	    }
		
		 @Transactional(propagation = Propagation.REQUIRED)
		 public  EmSpProductsOffer getOfferByBslatID(String bslatID){		     
		     log.info("getOfferByBslatID()");
	            return productOfferDAO.getByBslat(bslatID);
		 }
		 
		 
		 @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	        public EmSpProductsOffer getVariationsByPolicies(Long productOfferId, List<GTDifference> gtDiffs) throws Exception{
	            EmSpProductsOffer productOffer = productOfferDAO.load(productOfferId);
	            
	            //Prepare the WorkingMemory, add the ComponentPrice at Facts    
	            ArrayList<Object> arrayFacts = new ArrayList<Object>(productOffer.getEmComponentPrices());
	            //Add the product offer Services
	            arrayFacts.addAll(productOffer.getEmSpServiceses());
	            //Add the gtDifferences
	            arrayFacts.addAll(gtDiffs);
	            
	            HashMap<Long, Template> templates = new HashMap<Long, Template>();
	            log.info(">>>> Num Policies: "+productOffer.getEmPolicieses().size());
	            Iterator<EmPolicies> policies = productOffer.getEmPolicieses().iterator();
	            while(policies.hasNext()){
	                EmPolicies policy = policies.next();
	                //If policy is active add as Facts.
	                if(policy.getTxStatus().equals(Constants.STATE_ACTIVE)){
	                    
	                    EmPolicyType policyType = policy.getEmPolicyType();
	                    log.info(">>> Policy Type : "+policyType.getTxName());
	                    EmRuleTemplates ruleTplt = policyType.getEmRuleTemplates();
	                    log.info(">>> Policy Template : "+ruleTplt.getTxName());
	                    
	                    //Created and insert the template if not exist
	                    Template template = templates.get(ruleTplt.getNuRuleTemplateId());
	                    if(template==null){
	                        template = new Template(ruleTplt.getNuRuleTemplateId(), policyType.getTxName(), ruleTplt.getTxDescription(), ruleTplt.getTxTemplateDrt());
	                        templates.put(template.getId(), template);
	                    }

	                    //Get the rule params. Rule == Policy (in this case) 
	                    ArrayList<RuleParam> ruleParams = new ArrayList<RuleParam>();
	                    Iterator<EmPolicyValues> policyValues = policy.getEmPolicyValueses().iterator();
	                    while(policyValues.hasNext()){
	                        EmPolicyValues promoValue = policyValues.next();
	                        log.info(promoValue.getEmRuleParams().getNuParamRuleOrder()+" - "+promoValue.getEmRuleParams().getEmParams().getTxParamName()+": "+promoValue.getTxParamValue());
	                        //New RuleParam(order,value)
	                        RuleParam rp = new RuleParam(promoValue.getEmRuleParams().getNuParamRuleOrder(), promoValue.getTxParamValue());
	                        ruleParams.add(rp);
	                    }
	                    
	                    //Add the rule at specific template...
	                    Rule rule = new Rule(policy.getNuPolicyId(), policyType.getTxName(), policyType.getTxDescription(), ruleParams);
	                    template.getRules().add(rule);
	                }
	            }
	            
	            if(!templates.isEmpty()){
	                log.debug("Array Facts >>> "+arrayFacts.size());
	                //Create DroolsManager, with array templates and facts
	                DroolsManager dm = new DroolsManager(templates.values(), arrayFacts);
	                dm.processRules();
	                log.debug("<<<<<<<< Rules Executed >>>>>>>>>");
	                log.debug("Array Facts After >>> "+arrayFacts.size());
	                
	                for(EmComponentPrice cp : productOffer.getEmComponentPrices()){
	                    if(cp.getEmPriceVariations()!=null&& cp.getEmPriceVariations().size()>0){
	                        Set<EmPriceVariation> newVariations = new HashSet();
                            Iterator <EmPriceVariation> ite = cp.getEmPriceVariations().iterator();
                            EmPriceVariation variation;
                            while(ite.hasNext()){
                                variation= ite.next();
                                variation.setEmPriceVariationType(priceVariationTypeDAO.load(variation.getEmPriceVariationType().getNuIdVariationType()));
                                newVariations.add(variation);
                            }
                            cp.setEmPriceVariations(newVariations);
	                    }	                	
	                
	                }
	                	
	                
	            }
	            else log.info(">>>> Not Exist Active Poicies. ");
	            
	            return productOffer;
	            
	        }
		 
		 
		 @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		 public List<EmSpProductsOffer> getPartyProductOffers(Long nuPartyId, String filter){
			 return productOfferDAO.searchActivePartyProductOffers(nuPartyId, filter);
		 }
		 
		 @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		 public List<EmSpProductsOffer> getProductOffersToContract(Long nuPartyId, String category){
			 return productOfferDAO.getProductOffersToContract(nuPartyId, category);
		 }

		 @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		 public List<EmSpProductsOffer> getProductOffersContracted(Long nuPartyId, String category){
			 return productOfferDAO.getProductOffersContracted(nuPartyId, category);
		 }
		 
}
