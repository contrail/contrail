/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 612 $
 * @lastrevision   $Date: 2011-02-04 08:39:30 +0100 (Fri, 04 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/test/java/org/slasoi/businessManager/common/ProductManagementTest.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.commons.beanutils.BeanUtils;
import org.slasoi.businessManager.common.dao.ParamTypeDAO;
import org.slasoi.businessManager.common.dao.PromotionTypeDAO;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPromotionType;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServSatisfactionRate;
import org.slasoi.businessManager.common.model.EmSpServSatisfactionRateId;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.promotion.Promotion;
import org.slasoi.businessManager.common.service.promotion.PromotionManagement;
import org.slasoi.businessManager.common.service.promotion.PromotionParameter;
import org.slasoi.businessManager.common.service.promotion.PromotionType;
import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ProductManagementTest extends AbstractTransactionalSpringContextTests{ 
//TestCase{
    //private ApplicationContext ac;
	
    @Resource
    private ServiceManager serviceManager;
    @Resource
    private ProductManager productManager;
	@Resource
	private CustomerProductManager customerProductManager;
	@Resource 
	private SatisfactionRateManager satisfactionRateManager;
	@Resource
	private PromotionManagement pm ;	
	@Resource
	private PromotionTypeDAO promotioTypeDAO;	
	@Resource
	private ParamTypeDAO paramTypeDAO;


   /* public void setUp(){
        ac = new FileSystemXmlApplicationContext("src/main/resources/META-INF/spring/fooservice.xml");
        
    }*/
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-common-context.xml"};
    }

        public ProductManagementTest( String testName )
        {
            super( testName );
        }

        /**
         * @return the suite of tests being tested
         */
        public static Test suite()
        {
            return new TestSuite( ProductManagementTest.class );
        }

        /**
         * Test create service.
         */
        public void testCreateService(){
        	try{
        		System.out.println("Creating service...");
        		EmSpServices emSpServices = new EmSpServices();
        		emSpServices.setDtInsertdate(new Date());
        		emSpServices.setTxServicename("service test");
        		emSpServices.setTxServicedesc("service test desc");
        		EmParty emParty = new EmParty();
        		emParty.setNuPartyid(new Long(1));
        		emSpServices.setEmParty(emParty);
        		emSpServices.setTcServicetype("S");
        		emSpServices.setTxRelease("1");
        		serviceManager.saveOrUpdate(emSpServices);
        		EmSpServices sp = serviceManager.getServiceById(emSpServices.getNuServiceid());
        		System.out.println("Service created " + emSpServices.getNuServiceid());
        		
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
        
        /**
         * Test find service.
         */
        public void testFindService(){
        	try{
        		System.out.println("Finding services... ");
        		List<EmSpServices> lServices = serviceManager.getAllServices(); 
        		for(EmSpServices serv:lServices){
        			System.out.println("Service: " + serv.getTxServicename());
        		}
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
        
        
        /**
         * Test create product.
         */
        public void testCreateProduct_ProductOffer(){
        	try{
	        	System.out.println("Creating product...");
	        	EmSpProducts product = new EmSpProducts();
	        	product.setTxProductname("Product test");
	        	
	        	//added for txNegotiationType not-null requirenemtn
	        	product.setTxNegotiationType("A");
	        	
	        	EmSpProductsOffer emSpProductsOffer = new EmSpProductsOffer();
	        	emSpProductsOffer.setDtValidFrom(new Date());
	        	Calendar cal = new GregorianCalendar();
	        	cal.setTime(new Date());
	        	cal.add(Calendar.YEAR, 2);
	        	emSpProductsOffer.setDtValidTo(cal.getTime());
	        	emSpProductsOffer.setTxStatus("A");
	        	emSpProductsOffer.setTxRevisionid("1");
	        	emSpProductsOffer.setTxName("OFFER TEST- PRODUCT TEST");
	        	product.getEmSpProductsOffers().add(emSpProductsOffer);
	        	productManager.saveOrUpdate(product);
	        	System.out.println("Product created " + product.getNuProductid());
	        	
        	}catch(Exception e){
        		e.printStackTrace();
        	}
	      }
        
        /**
         * Find product.
         */
        public void testFindProduct(){
        	try{
	        	System.out.println("Finding product...");
	        	List<EmSpProducts> LProds = productManager.getAllInit();
	        	System.out.println("Product found: " + LProds.size());
	        	for(EmSpProducts prod:LProds){
	        		System.out.println("Product name: " + prod.getTxProductname());
	        	}
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
        
        /**
         * Find product active.
         */
        public void testFindProductActive(){
        	try{
	        	System.out.println("Finding active products ...");
	        	List<EmSpProducts> LProds = productManager.getAllInitActive();
	        	System.out.println("Product found: " + LProds.size());
	        	for(EmSpProducts prod:LProds){
	        		System.out.println("Product name: " + prod.getTxProductname());
	        	}
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }     

        /**
         * Find products of Filters.
         */
        public void testFindProductActiveByFilters(){
        	try{
	        	System.out.println("Finding active products ...");
	        	String[] filters = {"Mobile","Messaging"};
	        	List<EmSpProducts> LProds = productManager.findProductsActive(Arrays.asList(filters));
	        	System.out.println("Product found by filters: " + LProds.size());
	        	for(EmSpProducts prod:LProds){
	        		System.out.println("Product name: " + prod.getTxProductname());
	        	}
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }     

        /**
         * View products.
         */
        public void testViewProducts(){
        	try{
	        	System.out.println("View publishing products by Service Provider. Party id:5...");
	        	EmParty  emParty = new EmParty();
	        	emParty.setNuPartyid(new Long(5));
	        	List<EmCustomersProducts> customerProducts = customerProductManager.getCustomerProductsByPublisher(emParty);
	        	for(EmCustomersProducts cusProd:customerProducts){
	        		System.out.println("Party: " + cusProd.getEmParty().getPartyName());
	        		System.out.println("Product: " + cusProd.getEmSpProductsOffer().getEmSpProducts().getTxProductname());
	        	}
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }        
        
        /**
         * Sets the rating services.
         */
        public void setRatingServices(){
        		System.out.println("Test rating services....");
        	    ArrayList<EmSpServSatisfactionRate> rates = new ArrayList<EmSpServSatisfactionRate>();
				EmSpServSatisfactionRateId rateId = new EmSpServSatisfactionRateId(new Long(6), new Long(3), "1271164602515");
				EmSpServSatisfactionRate rate = new EmSpServSatisfactionRate();
				rate.setId(rateId);
				String rating = "6";
				if(rating!=null){
					rate.setNuSatisfactionRate(new Long(rating));
					rate.setTxReviewComment("Rating test junit...");
					rates.add(rate);
				}
				try {
					satisfactionRateManager.saveSatisfactionRates(rates);
					System.out.println("Service 6, Attributte 3, bslaId 1271164602515 has been rate with value 6.");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
        }
        
        /**
         * Test get rating services.
         */
        public void testGetRatingServices(){
        	System.out.println("Get rating services of Party ID 5");
        	EmParty emParty = new EmParty();
        	emParty.setNuPartyid(new Long(5));
        	List<EmCustomersProducts> customerProducts = customerProductManager.getCustomerProductsForRating(emParty);
        	for(EmCustomersProducts obj:customerProducts){
        		//System.out.println(obj.getEmSpServSatisfactionRates());
        		for(EmSpServSatisfactionRate objAux:obj.getEmSpServSatisfactionRates()){
        			System.out.println(objAux.getNuSatisfactionRate());
        		}	
        	}
        }
        
        /**
         * Test save promotion.
         */
        public void testSavePromotion(){
        	try{
			System.out.println(">>>>>>>>> PROMOTION TYPES <<<<<<<<<<<");
			List<PromotionType> promotionTypes = pm.getPromotionTypes();
			for(PromotionType promo : promotionTypes)
				System.out.println(promo);
			
			Promotion promotion = pm.getPromotionByType(promotionTypes.get(0).getNuPromotionTypeId());
			System.out.println(">>>>>>>>> PROMOTION PARAMETERS <<<<<<<<<<<");
			for(PromotionParameter param : promotion.getPromotionParameters())
				System.out.println(param);
			
			promotion.setTxName("New Promotion API");
			promotion.setTxDescription("Description Promotion API");

			promotion.getPromotionParameters().get(0).setTxParamValue("17/05/2010");
			promotion.getPromotionParameters().get(1).setTxParamValue("27/05/2010");
			promotion.getPromotionParameters().get(2).setTxParamValue("1");
			promotion.getPromotionParameters().get(3).setTxParamValue("15");
			promotion.getPromotionParameters().get(4).setTxParamValue("4");

			promotion.setNuProductId(new Long(1));
			pm.savePromotion(promotion);  
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
        
        /**
         * Load promotions.
         */
        public void loadPromotions(){
        	try{
    			Promotion promotion = new Promotion();
    			
    			//Load Parameter of template...
    			EmPromotionType promoType = promotioTypeDAO.load(new Long(1));
    			promotion.setNuPromotionTypeId(new Long(1));

    			EmRuleTemplates template = promoType.getEmRuleTemplates();
    			
    			//Load Template parameters, and asociate with Promotion
    			Iterator<EmRuleParams> itRuleParams = template.getEmRuleParamses().iterator();
    			while(itRuleParams.hasNext()){
    				EmRuleParams ruleParam = itRuleParams.next();
    				PromotionParameter promoParam = new PromotionParameter();
    				BeanUtils.copyProperties(promoParam, ruleParam.getEmParams());
    				promoParam.setTxParamType(ruleParam.getEmParams().getEmParamType().getTxParamTypeName());
    				
    				//Set list of valid values, if type = SELECT
    				paramTypeDAO.loadParamValues(ruleParam.getEmParams().getEmParamType());
    				promoParam.setListValues(ruleParam.getEmParams().getEmParamType().getListValues());
    				
    				promotion.getPromotionParameters().add(promoParam); 
    			}
    			        		
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
}
