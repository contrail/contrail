/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/drools/DroolsManager.java $
 */

package org.slasoi.businessManager.common.drools;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.drools.RuleBase;
import org.drools.RuleBaseFactory;
import org.drools.WorkingMemory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.compiler.PackageBuilder;
import org.drools.io.ResourceFactory;
import org.drools.rule.Package;
import org.drools.template.DataProviderCompiler;


public class DroolsManager {

	static Logger log = Logger.getLogger(DroolsManager.class);
	
	private Collection<Template> templates;
	private ArrayList<Object> facts;
	private ArrayList<Global> globals;	
	
	public DroolsManager(){
		facts = new ArrayList<Object>();
		globals = new ArrayList<Global>();
	}
	
	public DroolsManager(Collection<Template> templates, ArrayList<Object> facts){
		this();
		this.templates=templates;
		this.facts=facts;
	}

	public DroolsManager(Collection<Template> templates, ArrayList<Object> facts, ArrayList<Global> globals){
		this();
		this.templates=templates;
		this.facts=facts;
		this.globals=globals;
	}
	
	
    public ArrayList<Object> getFacts() {
		return facts;
	}

	public void setFacts(ArrayList<Object> facts) {
		this.facts = facts;
	}
	
	public ArrayList<Global> getGlobals() {
		return globals;
	}

	public void setGlobals(ArrayList<Global> globals) {
		this.globals = globals;
	}

	private RuleBase generateRules() throws Exception {
    	
    	//Creamos ruleBase al que iremos añadiendo paquetes de reglas, uno por cada template que tengamos...
    	RuleBase ruleBase = RuleBaseFactory.newRuleBase();
    	for(Template template : templates){
            RuleDataProvider tdp = new RuleDataProvider( template.getRules() );
            DataProviderCompiler converter = new DataProviderCompiler();
            String drl = converter.compile( tdp,  inputStreamFromString(template.getDrt()));
            log.info("Rules: "+ drl );

            //Agregamos el paquete a la base de reglas (desplegamos el paquete de reglas).
            ruleBase.addPackage(createPackage(drl));
    	}
        
        return ruleBase;
    }
    
    public void processRules() throws Exception{
    	try{
    	
	    	RuleBase ruleBase =  generateRules();
	        //Tenemos las reglas cargadas, creamos memoria de trabajo y ejecutamos 
	        WorkingMemory workingMemory = ruleBase.newStatefulSession();
	        //Cargamos la memoria de trabajo con los facts
	        for(Object fact : facts){
	        	workingMemory.insert(fact);
	        }
	        //Set the globals Variables
	        for(Global global : globals){
	        	workingMemory.setGlobal(global.getName(), global.getValue());	
	        }
			
	        //Disparamos las reglas de Drools
	        workingMemory.fireAllRules();	        
	        
    	}catch (Exception e) {
    		log.error("processRules() - Error : "+e.getMessage());
    		e.printStackTrace();
    		throw e;
		}
    	
    }
    
    private InputStream inputStreamFromString(String str){
	    byte[] bytes = str.getBytes();
	    return new ByteArrayInputStream(bytes);
    }
    
    private Package createPackage(String drl) throws Exception{
    	try{
	        PackageBuilder builder = new PackageBuilder();
	
	        //Read drl generate by the template
	        builder.addPackageFromDrl(new StringReader(drl));
	
	        //Check for Errors
	        if (builder.hasErrors()) {
	        	//throw new RuntimeException(builder.getErrors().get(0).toString());
	            throw new RuntimeException(builder.getErrors().toString());
	        }
	
	        //Get compiled rule package
	        Package pkg = builder.getPackage();
	        return pkg;
    	}catch (Exception e) {
    		log.error("createPackage() - Error : "+e.getMessage());
    		throw e;
		} 
    }
    
    public void compileTemplate(Template template) throws DroolsException{
    	try{
			RuleBase ruleBase = RuleBaseFactory.newRuleBase();
			RuleDataProvider tdp = new RuleDataProvider(template.getRules());
			DataProviderCompiler converter = new DataProviderCompiler();
			String drl = converter.compile(tdp, inputStreamFromString(template.getDrt()));
			log.info("Rules: " + drl);
	
			// Add package at the Rule Base
			ruleBase.addPackage(createPackage(drl));
    	}catch (Exception e) {
    		throw new DroolsException("Error compiling Template.", e);
		}
	}

    public void compileRuleDrl(String rule) throws DroolsException{
    	try{
    		final KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
	
			// this will parse and compile in one step
			kbuilder.add(ResourceFactory.newByteArrayResource(rule.getBytes()), ResourceType.DRL);
		
			// Check the builder for errors
			if (kbuilder.hasErrors()) {
				throw new RuntimeException(kbuilder.getErrors().toString());
			}
			
    	}catch (Exception e) {
    		log.error("Error compile drl Rule: "+e.getMessage());
    		throw new DroolsException("Unable to compile Rule.", e);
		}
	}
    
	
}
