/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/reportingws/src/main/java/org/slasoi/businessManager/reportingWS/Reporting.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.reportingWS;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.businessManager.postSale.reporting.ICustomerRelations;
import org.slasoi.businessManager.postSale.reporting.impl.CustomerRelationsImpl;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.syntaxconverter.SyntaxConverterDelegator;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class Reporting {

	private static final Logger logger = Logger.getLogger(Reporting.class);

	public String getSLA(String slaId) throws Exception {
		SLA sla = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				sla = cr.getSLA(slaId);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		if (sla == null) {
			throw new Exception("SLA ID not found");
		}

		SyntaxConverterDelegator delegator = new SyntaxConverterDelegator(SyntaxConverterType.SLASOISyntaxConverter);
		String xmlSla = delegator.renderSLA(sla);

		if (xmlSla == null) {
			throw new Exception("Syntax converter not connected");
		}

		return xmlSla;
	}


	public String getSLAbyUserID(String userId) throws InvalidUUIDException, Exception  {
		logger.info("Reporting, getSLA with userId = " + userId);
		SLA[] slas = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				UUID[] id = new UUID[1];
				id[0] = new UUID(userId);
				cr = (CustomerRelationsImpl) bc.getService(ref);
				slas = cr.getSLAbyUserID(id[0]);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		StringBuffer strBuf = new StringBuffer();
		SyntaxConverterDelegator delegator = new SyntaxConverterDelegator(SyntaxConverterType.SLASOISyntaxConverter);

		for (SLA sla : slas) {
			String xmlSla = delegator.renderSLA(sla);
			if (xmlSla == null) {
				throw new Exception("Syntax converter not connected");
			}
			strBuf.append("\n");
			strBuf.append(xmlSla);
		}

		return strBuf.toString();
	}


	public String getPenaltiesReportByProductOfferIdPartyIdYearMonthDay(long productOfferId, long partyId, int year,
			int month, int day) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename =
						cr.getPenaltiesReportByProductOfferIdPartyIdYearMonthDay(productOfferId, partyId, year, month,
								day);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getPenaltiesReportByProductOfferIdPartyIdYearMonth(long productOfferId, long partyId, int year,
			int month) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getPenaltiesReportByProductOfferIdPartyIdYearMonth(productOfferId, partyId, year, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getViolationsReportByProductOfferIdYearMonthDay(long productOfferId, int year, int month, int day) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getViolationsReportByProductOfferIdYearMonthDay(productOfferId, year, month, day);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getViolationsReportByProductOfferIdYearMonth(long productOfferId, int year, int month) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getViolationsReportByProductOfferIdYearMonth(productOfferId, year, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getViolationsReportBySLATIdYearMonthDay(String businessSLATId, int year, int month, int day) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getViolationsReportBySLATIdYearMonthDay(businessSLATId, year, month, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getViolationsReportBySLATIdYearMonth(String businessSLATId, int year, int month) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getViolationsReportBySLATIdYearMonth(businessSLATId, year, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getProductReportByProductOfferIdYearMonth(long productOfferId, int year, int month) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getProductReportByProductOfferIdYearMonth(productOfferId, year, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getProductReportBySLATIdYearMonth(String businessSLATId, int year, int month) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getProductReportBySLATIdYearMonth(businessSLATId, year, month);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getGTStatusBySLAIdGT(String businessSLAId, String GuaranteeTerm) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getGTStatusBySLAIdGT(businessSLAId, GuaranteeTerm);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}


	public String getGTStatusBySLAIdGTKPI(String businessSLAId, String GuaranteeTerm, String KPI) {
		String filename = null;
		CustomerRelationsImpl cr = null;
		BundleContext bc = Activator.getContext();
		if (bc != null) {
			ServiceReference ref = bc.getServiceReference(ICustomerRelations.class.getName());
			if (ref != null) {
				cr = (CustomerRelationsImpl) bc.getService(ref);
				filename = cr.getGTStatusBySLAIdGTKPI(businessSLAId, GuaranteeTerm, KPI);
			}
			else {
				logger.info("CustomerRelationsImpl NOT FOUND");
			}
		}
		else {
			logger.info("BundleContext NOT FOUND!!");
		}

		return filename;
	}

}
