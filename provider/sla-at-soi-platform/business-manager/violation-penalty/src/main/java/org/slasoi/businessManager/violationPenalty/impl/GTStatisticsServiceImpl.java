/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/services/GTStatisticsDBManager.java $
 */

package org.slasoi.businessManager.violationPenalty.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.service.PenaltyManager;
import org.slasoi.businessManager.common.service.SlaViolationManager;
import org.slasoi.businessManager.common.ws.types.GTStatistics;
import org.slasoi.businessManager.violationPenalty.GTStatisticsService;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value="gtStatisticsService")
public class GTStatisticsServiceImpl implements GTStatisticsService{

    private static Logger logger = Logger.getLogger(GTStatisticsServiceImpl.class.getName());
    
    @Autowired
    private BusinessContextService businessContextService;
    
    @Autowired
    private SlaViolationManager slaViolationService;

    @Autowired
    private PenaltyManager penaltyService;
    
    public GTStatistics getGTStatistics(String slatId) {

        int numberGTs = 0;
        int numberGTsNoViolatedNoPenalty = 0;
        int numberGTsViolatedNoPenalty = 0;
        int numberGTsViolatedWithPenalty = 0;
        int numberGTsViolated = 0;
        GTStatistics stat = null;

        // First, retrieve the SLAs with this slatId from sla registry
        SLA[] slas = null;
        try {
            slas = businessContextService.getBusinessContext().getSLARegistry()
                .getIQuery().getSLAsByTemplateId(new UUID(slatId));

            for (SLA sla : slas) {
                // get the current status of the SLA
                UUID slaId = sla.getUuid();
                AgreementTerm[] agreementTerms = sla.getAgreementTerms();
                numberGTs = numberGTs + agreementTerms.length;
                
                List<EmSlaViolation> violationList; 
                List<EmPenalty> penaltyList; 
                for (AgreementTerm gt : agreementTerms) {
                    // Find if there are violations for this SLA and GT
                    violationList = slaViolationService.getViolationsBySlaIdAndGTid(slaId.toString(), gt.getId().toString());
                    
                    if (violationList.size() > 0){
                        numberGTsViolated = numberGTsViolated + 1;
                    }
                    
                    penaltyList = penaltyService.getPenaltiesByViolationSlaIdAndGTid(slatId.toString(), gt.getId().toString());
                    
                    if (penaltyList.size() > 0)
                        numberGTsViolatedWithPenalty = numberGTsViolatedWithPenalty + 1;
                    else
                        numberGTsViolatedNoPenalty = numberGTsViolatedNoPenalty + 1;
                }

            }
            numberGTsNoViolatedNoPenalty = numberGTs - numberGTsViolated;
            stat = new GTStatistics(numberGTs, numberGTsNoViolatedNoPenalty, numberGTsViolatedNoPenalty,
                            numberGTsViolatedWithPenalty);
        }
        catch (InvalidUUIDException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (SLAManagerContextException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return stat;
    }

    public GTStatistics getGTStatistics(String slatId, Date startDate, Date endDate) {

        logger.info("getGTStatistics, slatId = " + slatId + " between " + startDate.toString() + " and "
                + endDate.toString());
        int numberGTs = 0;
        int numberGTsNoViolatedNoPenalty = 0;
        int numberGTsViolatedNoPenalty = 0;
        int numberGTsViolatedWithPenalty = 0;
        int numberGTsViolated = 0;
        GTStatistics stat = null;

        // First, retrieve the SLAs with this slatId from sla registry
        SLA[] slas = null;
        try {
            logger.info("Getting SLAs for template " + slatId);
            slas = businessContextService.getBusinessContext()
                .getSLARegistry().getIQuery().getSLAsByTemplateId(new UUID(slatId));

            for (SLA sla : slas) {
                // get the current status of the SLA
                UUID slaId = sla.getUuid();
                Date effectiveFrom = sla.getEffectiveFrom().getValue().getTime();
                Date effectiveTo = sla.getEffectiveUntil().getValue().getTime();
                logger.info("Got sla " + slaId.toString());
                logger.info("effective from = " + effectiveFrom.toString());
                logger.info("effective to = " + effectiveTo.toString());

                // Check dates
                List<EmSlaViolation> violationList=null;
                List<EmPenalty> penaltyList=null;
                if (effectiveFrom.compareTo(endDate) <= 0 && effectiveTo.compareTo(startDate) >= 0) {
                    logger.info("This sla was active during the requested dates");

                    AgreementTerm[] agreementTerms = sla.getAgreementTerms();
                    numberGTs = numberGTs + agreementTerms.length;

                    for (AgreementTerm gt : agreementTerms) {
                        // Find if there are violations for this SLA and GT
                        logger.info("GuaranteeTerm = " + gt.getId().toString());
                        
                        violationList = slaViolationService.getViolationsBySlaIdAndGTidAndDateBetween(
                                slaId.toString(), gt.getId().toString(), startDate, endDate);
                        
                        if (violationList.size()>0) {
                            logger.info("Found " + violationList.size() + " violations");
                            numberGTsViolated = numberGTsViolated + violationList.size();
                        }

                        // Find penalties for this SLA and GT                        
                        penaltyList = penaltyService.getPenaltiesByViolationSlaIdAndGTidAndInitTimeBetween(
                                slaId.toString(), gt.getId().toString(), startDate, endDate);
                        
                        int penalties = penaltyList.size();
                        if (penalties>0) {                            
                            logger.info("Found " + penalties + " penalties");
                                numberGTsViolatedWithPenalty = numberGTsViolatedWithPenalty + penalties;
                        }
                    }

                    logger.info("Partial result of GTStatistics:");
                    logger.info("numberGTs = " + numberGTs);
                    logger.info("numberGTsNoViolatedNoPenalty = " + numberGTsNoViolatedNoPenalty);
                    logger.info("numberGTsViolatedNoPenalty = " + numberGTsViolatedNoPenalty);

                }
                numberGTsNoViolatedNoPenalty = numberGTs - numberGTsViolated;
                numberGTsViolatedNoPenalty = numberGTsViolated - numberGTsViolatedWithPenalty;

                stat =
                        new GTStatistics(numberGTs, numberGTsNoViolatedNoPenalty, numberGTsViolatedNoPenalty,
                                numberGTsViolatedWithPenalty);

                logger.info("Result of GTStatistics:");
                logger.info("numberGTs = " + numberGTs);
                logger.info("numberGTsNoViolatedNoPenalty = " + numberGTsNoViolatedNoPenalty);
                logger.info("numberGTsViolatedNoPenalty = " + numberGTsViolatedNoPenalty);
                logger.info("numberGTsViolatedWithPenalty  = " + numberGTsViolatedWithPenalty);
            }
        }
        catch (InvalidUUIDException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return new GTStatistics(0, 0, 0, 0);
        }
        catch (SLAManagerContextException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return new GTStatistics(0, 0, 0, 0);
        }

        return stat;
    }

    public GTStatistics getGTStatistics(String slatId, String guaranteeTerm, Date startDate, Date endDate) {

        int numberGTs = 0;
        int numberGTsNoViolatedNoPenalty = 0;
        int numberGTsViolatedNoPenalty = 0;
        int numberGTsViolatedWithPenalty = 0;
        int numberGTsViolated = 0;
        GTStatistics stat = null;

        // First, retrieve the SLAs with this slatId from sla registry
        SLA[] slas = null;
        try {
            logger.info("Getting SLAs for template " + slatId);
            slas = businessContextService.getBusinessContext()
                .getSLARegistry().getIQuery().getSLAsByTemplateId(new UUID(slatId));

            List<EmSlaViolation> violationsList=null;
            List<EmPenalty> penaltyList=null;
            for (SLA sla : slas) {
                // get the current status of the SLA
                UUID slaId = sla.getUuid();
                logger.info("Got sla " + slaId.toString());

                AgreementTerm[] agreementTerms = sla.getAgreementTerms();
                numberGTs = numberGTs + agreementTerms.length;

                // Find if there are violations for this SLA and GT

                violationsList = slaViolationService.getViolationsBySlaIdAndGTidAndDateBetween(
                        slaId.toString(), guaranteeTerm, startDate, endDate);
                
                
                if (violationsList.size()>0) {
                    logger.info("Found " + violationsList.size() + " violations");
                    numberGTsViolated = numberGTsViolated + violationsList.size();
                }

                // Find penalties for this SLA and GT
                
                penaltyList = penaltyService.getPenaltiesByViolationSlaIdAndGTidAndInitTimeBetween(
                        slaId.toString(), guaranteeTerm, startDate, endDate);

                int penalties = penaltyList.size();
                if (penalties>0) {
                    logger.info("Found " + penalties + " penalties");                  
                    numberGTsViolatedWithPenalty = numberGTsViolatedWithPenalty + penalties;
                }

            }
            numberGTsNoViolatedNoPenalty = numberGTs - numberGTsViolated;
            numberGTsViolatedNoPenalty = numberGTsViolated - numberGTsViolatedWithPenalty;

            stat =  new GTStatistics(numberGTs, numberGTsNoViolatedNoPenalty, numberGTsViolatedNoPenalty,
                            numberGTsViolatedWithPenalty);

            logger.info("Result of GTStatistics:");
            logger.info("numberGTs = " + numberGTs);
            logger.info("numberGTsNoViolatedNoPenalty = " + numberGTsNoViolatedNoPenalty);
            logger.info("numberGTsViolatedNoPenalty = " + numberGTsViolatedNoPenalty);
            logger.info("numberGTsViolatedWithPenalty  = " + numberGTsViolatedWithPenalty);
        }
        catch (InvalidUUIDException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (SLAManagerContextException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return stat;
    }
}
