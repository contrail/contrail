/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/services/AdjustmentNotificationTypeDBManager.java $
 */

package org.slasoi.businessManager.violationPenalty.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.model.pac.EmSlaWarning;
import org.slasoi.businessManager.common.service.SlaViolationManager;
import org.slasoi.businessManager.common.service.SlaWarningManager;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.violationPenalty.AdjustmentNotificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "adjustmentNotificationTypeService")
public class AdjustmentNotificationTypeServiceImpl implements AdjustmentNotificationTypeService {

    private static Logger logger = Logger.getLogger(AdjustmentNotificationTypeServiceImpl.class.getName());

    @Autowired
    private SlaViolationManager slaViolationService;

    @Autowired
    private SlaWarningManager slaWarningService;

    public void saveOrUpdate(AdjustmentNotificationType event) {
        logger.info("AdjustmentNotificationTypeDBManager::saveOrUpdate ");

        switch (event.getType()) {
        case VIOLATION: {
            slaViolationService.saveOrUpdate(newSlaViolation(event));
            break;
        }
        case NOTIFICATION: {
            slaWarningService.saveOrUpdate(newSlaWarning(event));
            break;
        }
        case RECOVERY: {
            List<EmSlaViolation> list =
                    getSLAViolationById(event.getNotification().getRecoveryEvent().getViolationID());
            logger.debug("Got " + list.size() + " violations from DB");

            if (!list.isEmpty()) {
                EmSlaViolation recovery = list.get(0);
                recovery.setEndTime(event.getReportTime());

                slaViolationService.saveOrUpdate(recovery);
            }
            break;
        }
        }
    }

    public void delete(AdjustmentNotificationType event) {

        switch (event.getType()) {
        case VIOLATION: {
            slaViolationService.delete(newSlaViolation(event));
            break;
        }
        case NOTIFICATION: {
            List<EmSlaWarning> list = getSLAWarningById(event.getNotificationId(), event.getSlaID());

            if (!list.isEmpty()) {
                slaWarningService.delete(list.get(0));
            }
            break;
        }
        case RECOVERY: {
            List<EmSlaViolation> list =
                    getSLAViolationById(event.getNotification().getRecoveryEvent().getViolationID());
            if (!list.isEmpty()) {
                slaViolationService.delete(list.get(0));
            }
            break;
        }
        }
    }

    public List<EmSlaViolation> getSLAViolationById(String violationId) {
        return slaViolationService.getViolationsById(violationId);
    }

    public List<EmSlaWarning> getSLAWarningById(String notificationId, String slaId) {

        logger.info("Querying SLAWarning with notificationId = " + notificationId + " and slaId =" + slaId);

        List<EmSlaWarning> results = slaWarningService.getWarningsBySlaIdAndNotificationId(slaId, notificationId);
        logger.info("Got " + results.size() + " warnings");

        return results;
    }

    private EmSlaViolation newSlaViolation(AdjustmentNotificationType type) {

        EmSlaViolation violation = new EmSlaViolation();

        violation.setViolationId(type.getNotificationId());
        violation.setSlaId(type.getSlaID());
        violation.setGuaranteeTermId(type.getNotification().getViolationEvent().getGuaranteeTerm());
        violation.setInitialTime(type.getReportTime());

        return violation;
    }

    private EmSlaWarning newSlaWarning(AdjustmentNotificationType type) {

        EmSlaWarning warning = new EmSlaWarning();

        warning.setNotificationId(type.getNotificationId());
        warning.setSlaId(type.getSlaID());
        warning.setWarningTime(type.getReportTime());

        return warning;
    }

}
