/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/test/java/org/slasoi/businessManager/postSale/reporting/mockups/SLATemplateRegistryMockup.java $
 */

package org.slasoi.businessManager.postSale.reporting.mockups;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator.Warning;
import org.slasoi.slamodel.vocab.bnf;
import org.slasoi.slamodel.vocab.ext.Extensions;

public class SLATemplateRegistryMockup implements SLATemplateRegistry {
    
    private static final Logger logger = Logger.getLogger(SLATemplateRegistryMockup.class.getName());

    private static final String BUSINESS_SLA_FILE = "ORC_Business_SLA.xml";
    
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    
    // registry to store SLATss for testing purposes:)
    private HashMap<UUID, SLATemplate> slats = new HashMap<UUID, SLATemplate>();

    public SLATemplateRegistryMockup() {
        logger.info("SLATemplateRegistryMockup constructor");
    }

    public Warning[] addSLATemplate(SLATemplate slat, Metadata arg1) throws Exception {
        logger.info("Storing sla template with id = " + slat.getUuid());
        slats.put(slat.getUuid(), slat);
        return null;
    }

    public SLATemplate getSLATemplate(UUID id) throws Exception {
        
        logger.info("Retrieving SLAT with id = " + id);
        
        SLA[] slas = new SLA[1];
        try {
            slas[0] = createSLAT();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return slas[0];        
    }

    private SLA createSLAT() throws IOException {
        // Read SLA from xml, and convert to Java object (using Syntax Converter)
        
        String slaXml = readFile("src"+FILE_SEPARATOR+"test"+FILE_SEPARATOR+"resources"+FILE_SEPARATOR+BUSINESS_SLA_FILE);
        
        SLA sla = null;
        try {
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(slaXml);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (java.lang.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(bnf.render(sla, true));

        return sla;
    }

    
    public void addListener(Listener arg0) {
        // TODO Auto-generated method stub

    }

    public Specification getInterfaceSpecification(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public Metadata getMetadata(UUID arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public String getMetadataProperty(UUID arg0, STND arg1) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] query(ConstraintExpr arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public ResultSet query(SLATemplate arg0, ConstraintExpr arg1) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public void removeExtensions(Extensions arg0) throws Exception {
        // TODO Auto-generated method stub

    }

    public void removeListener(Listener arg0) {
        // TODO Auto-generated method stub

    }

    public void removeSLATemplate(UUID arg0) throws Exception {
        // TODO Auto-generated method stub

    }

    public void setMetadataProperty(UUID arg0, STND arg1, String arg2) throws Exception {
        // TODO Auto-generated method stub

    }

    public void setReferenceResolver(ReferenceResolver arg0) throws Exception {
        // TODO Auto-generated method stub

    }

    public Warning[] validateSLATemplate(SLATemplate arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public void addExtensions(Extensions arg0) throws Exception {
        // TODO Auto-generated method stub

    }


    private String readFile(String filename) throws IOException {
        String content = null;

        FileInputStream file = new FileInputStream(filename);
        byte[] b = new byte[file.available()];
        file.read(b);
        file.close();
        content = new String(b);

        return content;
    }
    
}
