/**
 * Copyright (c) 2008-2010, Telef�nica Investigaci�n y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telef�nica Investigaci�n y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telef�nica Investigaci�n y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Agustin Escamez - escamez@tid.es
 * @lastrevision   $Date: 2011-06-21 09:47:00 +0100 (Tu, 20 Jun 2011) $ * 
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.billingEngine.dao.AccountEventDAO;
import org.slasoi.businessManager.billingEngine.service.AccountEventManager;
import org.slasoi.businessManager.billingEngine.service.PartyBankManager;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.PenaltyManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.postSale.reporting.impl.CustomerRelationsImpl;
import org.slasoi.businessManager.violationPenalty.GTStatisticsService;
import org.slasoi.businessManager.violationPenalty.GTStatusService;
import org.slasoi.businessManager.violationPenalty.SlaStatisticsService;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * @author Agustin Escamez (TID)
 * 
 */

public class BillingReportTest extends AbstractTransactionalSpringContextTests {

    private static final Logger logger = Logger.getLogger(BillingReportTest.class.getName());

    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String BUSINESS_SLA_FILE = "src" + FILE_SEPARATOR + "test" + FILE_SEPARATOR + "resources"
            + FILE_SEPARATOR + "ORC_Business_SLA.xml";

    @Autowired
    protected BusinessContextService businessContextService;

    @Autowired
    protected ProductOfferManager productOfferManager;

    @Autowired
    protected CustomerProductManager customerProductManager;

    @Autowired
    protected GTStatisticsService gtStatisticsService = null;

    @Autowired
    protected GTStatusService gtStatusService = null;

    @Autowired
    protected SlaStatisticsService slaStatisticsService = null;

    @Autowired
    protected PenaltyManager penaltyService;

    @Autowired
    protected AccountEventManager accountEventService = null;

    @Autowired
    protected AccountEventDAO accountEventDAO = null;

    @Autowired
    protected CustomerRelationsImpl reporting;
    
    @Autowired
    protected PartyBankManager partyBankService=null;
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-postsale-context.xml" };
    }

    /**
     * Create the test case
     * 
     * @param testName
     *            name of the test case
     */
    public BillingReportTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(BillingReportTest.class);
    }

    private SLA createSLA() throws IOException, RegistrationFailureException {
        // Read SLA from xml, convert to Java object (using Syntax Converter) and store it in the registry
        String slaXml = readFile(BUSINESS_SLA_FILE);

        SLA sla = null;
        try {
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(slaXml);

            logger.info("BizzSlaParserTest# After syntax converter...");
            logger.info(sla.toString());
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return sla;
    }

    public void testBillingReport() throws IOException {

        logger.info("Context initialized correctly");

        reporting.getBillingReportBySlaId("ORCSLA", new Date(111,0,1),new Date());
    
    }

    private String readFile(String file) throws IOException {

        InputStream is = new FileInputStream(file);

        byte[] b = new byte[is.available()];
        is.read(b);
        is.close();

        return new String(b);
    }

}
