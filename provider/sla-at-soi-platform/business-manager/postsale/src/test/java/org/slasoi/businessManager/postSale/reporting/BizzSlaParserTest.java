/**
 * Copyright (c) 2008-2010, Telef�nica Investigaci�n y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telef�nica Investigaci�n y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telef�nica Investigaci�n y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Agustin Escamez - escamez@tid.es
 * @lastrevision   $Date: 2011-06-21 09:47:00 +0100 (Tu, 20 Jun 2011) $ * 
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.postSale.reporting.impl.BusinessSLAParser;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParametersList;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.xml.sax.SAXException;

/**
 * @author Agustin Escamez (TID)
 * 
 */

public class BizzSlaParserTest extends AbstractTransactionalSpringContextTests {
    
    private static final Logger logger = Logger.getLogger(BizzSlaParserTest.class.getName());

    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String BUSINESS_SLA_FILE = "src"+FILE_SEPARATOR+"test"+FILE_SEPARATOR+"resources"+FILE_SEPARATOR+"ORC_Business_SLA.xml";
    
    /*
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-postsale-report-context.xml"};
    }
    */
   
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BizzSlaParserTest( String testName )
    {
        super( testName );
    }

    
    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BizzSlaParserTest.class );
    }
    
    private SLA createSLA() throws IOException, RegistrationFailureException {
        // Read SLA from xml, convert to Java object (using Syntax Converter) and store it in the registry
        String slaXml = readFile(BUSINESS_SLA_FILE);

        SLA sla = null;
        try {
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(slaXml);

            logger.info("BizzSlaParserTest# After syntax converter...");
            logger.info(sla.toString());
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return sla;
    }
    
    public void testParseSLA() throws IOException {
        
        ParametersList parameters = new ParametersList();
       
        try {
            BusinessSLAParser.loadBusinessSLA(parameters, createSLA());
        }
        catch (NullPointerException e) {
            logger.info("BizzSlaParserTest# parseSLA# NullPointerException...");
        }
        catch (RegistrationFailureException e) {
            logger.info("BizzSlaParserTest# parseSLA# RegistrationFailureException...");
        }
        catch (SAXException e) {
            logger.info("BizzSlaParserTest# parseSLA# SAXException...");
        }
       
        for (String key: parameters.keySet()){
            logger.info(key+" :="+parameters.get(key));
        }
        
    }

    private String readFile(String file) throws IOException {

        InputStream is = new FileInputStream(file);
        
        byte[] b = new byte[is.available()];
        is.read(b);
        is.close();

        return new String(b);
    }
    
}
