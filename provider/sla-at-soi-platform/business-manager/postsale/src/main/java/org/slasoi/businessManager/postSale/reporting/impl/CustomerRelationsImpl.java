/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/CustomerRelationsImpl.java $
 */

package org.slasoi.businessManager.postSale.reporting.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.billingEngine.service.AccountEventManager;
import org.slasoi.businessManager.billingEngine.service.PartyBankManager;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.billing.EmAccountEvent;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.PenaltyManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.util.CalendarDate;
import org.slasoi.businessManager.common.ws.types.GTStatistics;
import org.slasoi.businessManager.common.ws.types.GTStatus;
import org.slasoi.businessManager.common.ws.types.SlaStatistics;
import org.slasoi.businessManager.postSale.reporting.ICustomerRelations;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParamNames;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParametersList;
import org.slasoi.businessManager.postSale.reporting.impl.types.ReportFileName;
import org.slasoi.businessManager.postSale.reporting.impl.types.ReportType;
import org.slasoi.businessManager.postSale.reporting.impl.util.DateConverter;
import org.slasoi.businessManager.postSale.reporting.impl.util.PathUtil;
import org.slasoi.businessManager.violationPenalty.GTStatisticsService;
import org.slasoi.businessManager.violationPenalty.GTStatusService;
import org.slasoi.businessManager.violationPenalty.SlaStatisticsService;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.lowagie.text.DocumentException;

@Service(value = "reporting")
public class CustomerRelationsImpl implements ICustomerRelations {
    
    private static final Logger logger = Logger.getLogger(CustomerRelationsImpl.class);

    private static final String OUTPUT_DIR = "output_directory";
    private static final String URL_OUTPUT_DIR = "URL_output_directory";
    protected static String MAIL_SERVER = "mailserver";
    protected static String MAIL_FROM = "mailfrom";
    protected static String MAIL_SUBJECT = "SLA@SOI Automatic generated report";
    protected static String MAIL_BODY =
            "Dear customer,\n, Please find attached a report about the performance of the purchased product";
   
    private static final Properties properties = new Properties();
           
    //@Autowired
    //private SLAManagerContext slaContext;

    @Autowired
    protected BusinessContextService businessContextService;

    @Autowired
    protected ProductOfferManager productOfferManager;
    
    @Autowired
    protected CustomerProductManager customerProductManager;
    
    @Autowired
    protected GTStatisticsService gtStatisticsService=null;
    
    @Autowired
    protected GTStatusService gtStatusService=null;
    
    @Autowired
    protected SlaStatisticsService slaStatisticsService=null;

    @Autowired
    protected PenaltyManager penaltyService;
    
    @Autowired
    protected AccountEventManager accountEventService=null;
    
    @Autowired
    protected PartyBankManager partyBankService=null;
    
    private String configurationFile = null;

    public CustomerRelationsImpl() {
       
    }

    public CustomerRelationsImpl(String configurationFile) {
        this.configurationFile = configurationFile;       
        init();
    }

    public void setConfigurationFile(String configurationFile) {
        logger.info("****Setting CustomerRelations configuration file to " + configurationFile + "****");
        this.configurationFile = configurationFile;
    }

    public String getConfigurationFile() {
        return configurationFile;
    }

    /*
    public void setSLAManagerContext(SLAManagerContext context){
        logger.info("***************Registered Context Found by CustomerRelationsImpl *************");
        slaContext = context;       
    }
    */
    
    /*
    public void setSlaContext(SLAManagerContext slaContext) {
        setSLAManagerContext(slaContext);
    }
    */
    
    public void setBusinessContextService(BusinessContextService businessContextService) {
        this.businessContextService = businessContextService;
    }

    public void init() {
        logger.info("CustomerRelations, init() method");
        try {
            logger.info("Loading properties file " +  configurationFile);
           
            configurationFile = PathUtil.REPORTING_HOME +PathUtil.FILE_SEPARATOR + configurationFile;
            
            //properties.load(CustomerRelationsImpl.class.getClassLoader().getResourceAsStream(configurationFile));
            properties.load(new FileInputStream(new File(configurationFile)));
            
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Properties file loaded...");
    }

    public String getBillingReportBySlaId(String businessSLAId, Date startDate,Date endDate) {
        
        ReportFileName filename = null;
       
        logger.info("CustomerRelationsImpl, getBillingReportBySlaId, [bslaId:= "
                + businessSLAId + "], [startDate:= " + startDate+ "], [endDate:= " + endDate+ "]");
        
     // First, get the information about the Product-Offers
        if (businessSLAId == null) {
            logger.info("businessSLAId is NULL!!!!");
            return null;
        }

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        readSLA(businessSLAId,parameters);//read sla parameters
        readBillingData(parameters, businessSLAId,startDate, endDate);
        
        Long offersId=null;
        if (productOfferManager.getOfferByBslatID(businessSLAId)!=null){
            offersId = productOfferManager.getOfferByBslatID(businessSLAId).getNuIdproductoffering();
            
            //Iterate over sla product offers        
            readProductOffer(parameters,offersId );
        }
              
        
        //Read violations data
        readViolationsReport(parameters, businessSLAId, startDate, endDate);

        filename = generateFilename(ReportType.BILLING_REPORT, businessSLAId);
       
        ReportGenerator.generateReport(ReportType.BILLING_REPORT, parameters, filename.getLocalFileName());
       
        return filename.getUrlFileName();
    }

    
    private ParametersList readBillingData(ParametersList list, String businessSLAId, Date from, Date until) {
        
        logger.info("CustomerRelationsImpl# readBillingData for sla = " + businessSLAId);
        
        List<EmAccountEvent> accountEvents = null;

        try {
            accountEvents = accountEventService.getEmAccountEventsBySlaId(businessSLAId, from, until);
        }
        catch (Exception e) {
            logger.error("readBillingData Exception ",e);
        }

        if (from!=null){
            list.put(ParamNames.ACCOUNTEVENT_INITIAL_DATE.name(), String.valueOf(CalendarDate.Y4M2D2.getDateAsString(from)));
        }else{
            list.put(ParamNames.ACCOUNTEVENT_INITIAL_DATE.name(), "-");
        }
        
        if (until!=null){
            list.put(ParamNames.ACCOUNTEVENT_FINAL_DATE.name(), String.valueOf(CalendarDate.Y4M2D2.getDateAsString(until)));
        }else{
            list.put(ParamNames.ACCOUNTEVENT_FINAL_DATE.name(), "-");
        }

        double totalAmount=0.0;
        
        for (EmAccountEvent acc : accountEvents) {            
            
//            if (acc.getPayer()!=null){
//                list.put(ParamNames.ACCOUNTEVENT_PARTYID_PAYER.name(), String.valueOf(acc.getPayer().getNuPartyid()));    
//            }
            
            if (acc.getVendor()!=null){
                list.put(ParamNames.ACCOUNTEVENT_PARTYID_VENDOR.name(), String.valueOf(acc.getVendor().getNuPartyid()));
            }
            
            list.put(ParamNames.ACCOUNTEVENT_TIMESTAMP.name(), CalendarDate.M3_D2_Y4_HHMMSS.getDateAsString(acc.getTsTimeStamp()));
            list.put(ParamNames.ACCOUNTEVENT_AMOUNT.name(), String.valueOf(acc.getNuAmount()));
            
            if (acc.getCurrency()!=null){
                list.put(ParamNames.ACCOUNTEVENT_CURRENCY_NAME.name(), acc.getCurrency().getTxName());
            }else{
                list.put(ParamNames.ACCOUNTEVENT_CURRENCY_NAME.name(), "EUR");
            }
            
            list.put(ParamNames.ACCOUNTEVENT_DESCRIPTION.name(), String.valueOf(acc.getTxDescription()));
//            list.put(ParamNames.ACCOUNTEVENT_TC_TYPE.name(), String.valueOf(acc.getTcType()));
            
            
//            list.put(ParamNames.ACCOUNTEVENT_EVENT_TYPEID.name(), String.valueOf(acc.getEmAccountEventType().getNuEventTypeId()));
            
            if (acc.getPaymentEvent()!=null){
                list.put(ParamNames.ACCOUNTEVENT_PAYMENT_DONE.name(), String.valueOf("true"));
            }else{
                list.put(ParamNames.ACCOUNTEVENT_PAYMENT_DONE.name(), "false");
            }
            
            if (acc.getEmPartyBank()!=null){
                list.put(ParamNames.ACCOUNTEVENT_BANK_OFFICE_ADDRESS.name(), String.valueOf(acc.getEmPartyBank().getTxBankOfficeAddress()));
                list.put(ParamNames.ACCOUNTEVENT_BANK_ACCOUNT_NUMBER.name(), String.valueOf(acc.getEmPartyBank().getTxAccountNumber()));
            }else{
                list.put(ParamNames.ACCOUNTEVENT_BANK_OFFICE_ADDRESS.name(), String.valueOf("Unknown"));
                list.put(ParamNames.ACCOUNTEVENT_BANK_ACCOUNT_NUMBER.name(), String.valueOf("Unknown"));
            }
            
            //Sum of Total 
            if (acc.getTcType() == BillingConstants.EVENT_DEBIT){
                totalAmount = totalAmount + acc.getNuAmount().doubleValue();
            }else{
                totalAmount = totalAmount - acc.getNuAmount().doubleValue();
            }
            
        }
        
        list.put(ParamNames.ACCOUNTEVENT_TOTAL_AMOUNT.name(), String.valueOf(totalAmount));
        
        return list;
    }

    /**
     * 
     * @param slaId
     *            : identifier of the SLA
     * @return the sla
     */
    public SLA getSLA(String slaId) {
        logger.info("CustomerRelations, getSLA with id = " + slaId);
        SLA sla = null;

        // First, check we have the context and we can access the SLA Registry
        if (businessContextService != null) {

            try {
                SLARegistry registry = businessContextService.getBusinessContext().getSLARegistry();

                if (registry != null) {
                    UUID[] id = new UUID[1];
                    id[0] = new UUID(slaId);

                    logger.info("Retrieving SLA " + slaId + " from registry");

                    SLA[] slas = registry.getIQuery().getSLA(id);

                    int nSlas = slas.length;

                    logger.info("Got " + nSlas + " slas");

                    // Should exist only one
                    if (nSlas > 0)
                        sla = slas[0];
                }
                else {
                    logger.error("ERROR. Can't retrieve SLA from registry. Registry is null");
                }
            }
            catch (InvalidUUIDException e) {
                logger.info("SLA with id  = " + slaId + " does not exist in the registry");
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            logger.error("ERROR: Can't retrieve SLA. Context is null");
        }

        return sla;
    }
    
    /**
     * 
     * @param slaId
     *            : identifier of the SLA
     * @return the sla
     */
    public SLA[] getSLAbyUserID(UUID userId) {
        logger.info("CustomerRelations, getSLA with id = " + userId.getValue());
        SLA[] sla = null;

        // First, check we have the context and we can access the SLA Registry
        if (businessContextService != null) {

            try {
                SLARegistry registry = businessContextService.getBusinessContext().getSLARegistry();

                if (registry != null) {
                   sla = registry.getIQuery().getSLAsByParty(userId);
             
                		 
                	
                    int nSlas = sla.length;

                    logger.info("Got " + nSlas + " slas");

                   
                }
                else {
                    logger.error("ERROR. Can't retrieve SLA from registry. Registry is null");
                }
            }
            catch (InvalidUUIDException e) {
                logger.info("Cannot retrive slas user for" + userId+ " user");
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            logger.error("ERROR: Can't retrieve SLA. Context is null");
        }

        return sla;
    }

    /**
     * Builds a report with: - Information about the Product-Offer - Prices - SLAs linked to this offer - Penalties
     * 
     * @param productOfferId
     *            : id of the Product-Offer
     * @param partyId
     *            : identifier of the client
     * @param year
     * @param month
     * @param day
     * @return
     */
    public String getPenaltiesReportByProductOfferIdPartyIdYearMonthDay(long productOfferId, long partyId, int year,
            int month, int day) {
        logger.info("CustomerRelations, getPenaltiesReportByProductOfferIdPartyIdYearMonthDay, productOfferId = "
                + productOfferId + ", partyId = " + partyId + ", year = " + year + ", month = " + month + ", day = "
                + day);
        /*
         * ReportFileName filename = generateFilename(ReportType.PRODUCT_OFFER_REPORT, new
         * Long(productOfferId).toString());
         * 
         * try { PDFConversor.generateEmptyReport(filename.getLocalFileName()); } catch (IOException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (DocumentException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); }
         */
        // First, get the information about the Product-Offer
        if (productOfferManager == null) {
            logger.info("ProductOfferManager is NULL!!!!");
            return null;
        }

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);
        parameters = readBusinessSLAId(parameters, partyId);
        parameters = readSLAs(parameters);

        List<String> bslasId = parameters.get(ParamNames.CUSTOMER_PRODUCT_BSLA_ID.toString());
        if (bslasId != null) {
            Date startDate = DateConverter.firstHourOfDay(year, month, day);
            Date endDate = DateConverter.lastHourOfDay(year, month, day);

            for (String id : bslasId) {
                // parameters = readPenaltiesBySLAId(parameters, bslasId.get(0), startDate, endDate);
                parameters = readPenaltiesBySLAId(parameters, id, startDate, endDate);
            }
        }

        ReportFileName filename =
                generateFilename(ReportType.PRODUCT_OFFER_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.PRODUCT_OFFER_REPORT, parameters, filename.getLocalFileName());

        return filename.getUrlFileName();
    }

    public String getPenaltiesReportByProductOfferIdPartyIdYearMonth(long productOfferId, long partyId, int year,
            int month) {
        logger.info("CustomerRelations, getPenaltiesReportByProductOfferIdPartyIdYearMonthDay, productOfferId = "
                + productOfferId + ", partyId = " + partyId + ", year = " + year + ", month = " + month);
        /*
         * ReportFileName filename = generateFilename(ReportType.PRODUCT_OFFER_REPORT, new
         * Long(productOfferId).toString());
         * 
         * try { PDFConversor.generateEmptyReport(filename.getLocalFileName()); } catch (IOException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (DocumentException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); }
         */
        // First, get the information about the Product-Offer
        if (productOfferManager == null) {
            logger.info("ProductOfferManager is NULL!!!!");
            return null;
        }

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);
        parameters = readBusinessSLAId(parameters, partyId);
        parameters = readSLAs(parameters);

        List<String> bslasId = parameters.get(ParamNames.CUSTOMER_PRODUCT_BSLA_ID.toString());
        if (bslasId != null && bslasId.size() > 0) {
            Date startDate = DateConverter.firstDayOfMonth(year, month);
            Date endDate = DateConverter.lastDayOfMonth(year, month);

            // Take the most recent one
            // Calendar recentTime = Calendar.getInstance();
            // recentTime.setTimeInMillis(1);
            // String recentSLAId = bslasId.get(0);
            // logger.debug("Recent time in millis = " + recentTime.getTimeInMillis());

            for (String id : bslasId) {
                // parameters = readPenaltiesBySLAId(parameters, bslasId.get(0), startDate, endDate);
                parameters = readPenaltiesBySLAId(parameters, id, startDate, endDate);
                // Calendar time = readAgreementDate(id);

                // if (time != null && time.compareTo(recentTime) > 0) {
                // logger.debug("Setting " + id + " as the most recent one");
                // logger.debug("Recent time in millis = " + recentTime.getTimeInMillis());
                // recentTime = time;
                // recentSLAId = id;
                // }
            }

            // parameters = readPenaltiesBySLAId(parameters, recentSLAId, startDate, endDate);
        }

        ReportFileName filename =
                generateFilename(ReportType.PRODUCT_OFFER_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.PRODUCT_OFFER_REPORT, parameters, filename.getLocalFileName());

        return filename.getUrlFileName();
    }

    public String getViolationsReportByProductOfferIdYearMonthDay(long productOfferId, int year, int month, int day) {
        logger.info("CustomerRelations, getViolationsReportByProductOfferIdYearMonthDay, productOfferId = "
                + productOfferId + ", year = " + year + ", month = " + month + ", day = " + day);
        // ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, new
        // Long(productOfferId).toString());

        // try {
        // PDFConversor.generateEmptyReport(filename.getLocalFileName());
        // }
        // catch (IOException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // catch (DocumentException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);
        Date date = DateConverter.toDate(year, month, day);

        List<String> bslasId = parameters.get(ParamNames.PRODUCT_OFFER_BSLAT_ID.toString());
        if (bslasId != null) {
            Date startDate = DateConverter.firstHourOfDay(year, month, day);
            Date endDate = DateConverter.lastHourOfDay(year, month, day);
            parameters = readViolationsReport(parameters, bslasId.get(0), startDate, endDate);
        }
        ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.VIOLATIONS_REPORT, parameters, filename.getLocalFileName());

        return filename.getUrlFileName();
    }

    public String getViolationsReportByProductOfferIdYearMonth(long productOfferId, int year, int month) {
        logger.info("CustomerRelations, getViolationsReportByProductOfferIdYearMonthDay, productOfferId = "
                + productOfferId + ", year = " + year + ", month = " + month);
        // ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, new
        // Long(productOfferId).toString());

        // try {
        // PDFConversor.generateEmptyReport(filename.getLocalFileName());
        // }
        // catch (IOException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // catch (DocumentException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);

        List<String> bslasId = parameters.get(ParamNames.PRODUCT_OFFER_BSLAT_ID.toString());
        Date startDate = DateConverter.firstDayOfMonth(year, month);
        Date endDate = DateConverter.lastDayOfMonth(year, month);

        if (bslasId != null && bslasId.size() > 0) {
            String bslatId = bslasId.get(0);
            if (bslatId != null)
                parameters = readViolationsReport(parameters, bslatId, startDate, endDate);
        }

        ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.VIOLATIONS_REPORT, parameters, filename.getLocalFileName());

        return filename.getUrlFileName();
    }

    public String getViolationsReportBySLATIdYearMonthDay(String businessSLATId, int year, int month, int day) {
        logger.info("CustomerRelations, getViolationsReportBySLATIdYearMonthDay, businessSLATId = " + businessSLATId
                + ", year = " + year + ", month = " + month + ", day = " + day);
        /*
         * ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, businessSLATId);
         * 
         * try { PDFConversor.generateEmptyReport(filename.getLocalFileName()); } catch (IOException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (DocumentException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); }
         */
        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        Date startDate = DateConverter.firstHourOfDay(year, month, day);
        Date endDate = DateConverter.lastHourOfDay(year, month, day);
        parameters = readViolationsReport(parameters, businessSLATId, startDate, endDate);
        ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, businessSLATId);
        ReportGenerator.generateReport(ReportType.VIOLATIONS_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public String getViolationsReportBySLATIdYearMonth(String businessSLATId, int year, int month) {
        logger.info("CustomerRelations, getViolationsReportBySLATIdYearMonth, businessSLATId = " + businessSLATId
                + ", year = " + year + ", month = " + month);
        /*
         * ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, businessSLATId);
         * 
         * try { PDFConversor.generateEmptyReport(filename.getLocalFileName()); } catch (IOException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (DocumentException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); }
         */
        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        Date startDate = DateConverter.firstDayOfMonth(year, month);
        Date endDate = DateConverter.lastDayOfMonth(year, month);
        parameters = readViolationsReport(parameters, businessSLATId, startDate, endDate);
        ReportFileName filename = generateFilename(ReportType.VIOLATIONS_REPORT, businessSLATId);
        ReportGenerator.generateReport(ReportType.VIOLATIONS_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public String getProductReportByProductOfferIdYearMonth(long productOfferId, int year, int month) {
        logger.info("CustomerRelations, getProductReportByProductOfferIdYearMonth, productOfferId = " + productOfferId
                + ", year = " + year + ", month = " + month);
        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);

        List<String> bslatsId = parameters.get(ParamNames.PRODUCT_OFFER_BSLAT_ID.toString());
        if (bslatsId != null) {
            Date startDate = DateConverter.firstDayOfMonth(year, month);
            Date endDate = DateConverter.lastDayOfMonth(year, month);
            parameters = readSLAStatistics(parameters, bslatsId.get(0), startDate, endDate);
        }
        ReportFileName filename = generateFilename(ReportType.PRODUCT_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.PRODUCT_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public String getProductReportBySLATIdYearMonth(String businessSLATId, int year, int month) {
        logger.info("CustomerRelations, getProductReportBySLATIdYearMonth, businessSLATId = " + businessSLATId
                + ", year = " + year + ", month = " + month);
        /*
         * ReportFileName filename = generateFilename(ReportType.PRODUCT_REPORT, businessSLATId);
         * 
         * try { PDFConversor.generateEmptyReport(filename.getLocalFileName()); } catch (IOException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (DocumentException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); }
         */
        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        Date startDate = DateConverter.firstDayOfMonth(year, month);
        Date endDate = DateConverter.lastDayOfMonth(year, month);
        parameters = readSLAStatistics(parameters, businessSLATId, startDate, endDate);
        ReportFileName filename = generateFilename(ReportType.PRODUCT_REPORT, businessSLATId);
        ReportGenerator.generateReport(ReportType.PRODUCT_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public String getGTStatusBySLAIdGT(String businessSLAId, String guaranteeTerm) {
        logger.info("CustomerRelations, getGTStatusBySLAIdGT, businessSLAId = " + businessSLAId + ", guaranteeTerm = "
                + guaranteeTerm);

        ReportFileName filename = generateFilename(ReportType.GT_STATUS_REPORT, businessSLAId);

        try {
            PDFConversor.generateEmptyReport(filename.getLocalFileName());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

        // reads data from database and store them in list of parameters
        // ParametersList parameters = new ParametersList();
        // parameters = readGTStatus(parameters, businessSLAId, guaranteeTerm);
        // ReportFileName filename = generateFilename(ReportType.GT_STATUS_REPORT, businessSLAId);
        // ReportGenerator.generateReport(ReportType.GT_STATUS_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public String getGTStatusBySLAIdGTKPI(String businessSLAId, String guaranteeTerm, String KPI) {
        logger.info("CustomerRelations, getGTStatusBySLAIdGTKPI, businessSLAId = " + businessSLAId
                + ", guaranteeTerm = " + guaranteeTerm + ", KPI = " + KPI);

        ReportFileName filename = generateFilename(ReportType.GT_STATUS_REPORT, businessSLAId);

        try {
            PDFConversor.generateEmptyReport(filename.getLocalFileName());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

        // reads data from database and store them in list of parameters
        // ParametersList parameters = new ParametersList();
        // parameters = readGTStatus(parameters, businessSLAId, GuaranteeTerm, KPI);
        // ReportFileName filename = generateFilename(ReportType.GT_STATUS_REPORT, businessSLAId);
        // ReportGenerator.generateReport(ReportType.GT_STATUS_REPORT, parameters, filename.getLocalFileName());
        return filename.getUrlFileName();
    }

    public void sendPenaltiesReportByProductOfferIdPartyIdYearMonthDay(long productOfferId, long partyId, int year,
            int month, int day) {

        logger.info("CustomerRelations, sendPenaltiesReportByProductOfferIdPartyIdYearMonthDay, productOfferId = "
                + productOfferId + ", partyId = " + partyId + ", year = " + year + ", month = " + month + ", day = "
                + day);

        // reads data from database and store them in list of parameters
        ParametersList parameters = new ParametersList();
        parameters = readProductOffer(parameters, productOfferId);
        parameters = readBusinessSLAId(parameters, partyId);
        parameters = readSLAs(parameters);

        List<String> bslasId = parameters.get(ParamNames.CUSTOMER_PRODUCT_BSLA_ID.toString());
        if (bslasId != null) {
            for (String id : bslasId) {
                // parameters = readPenaltiesBySLAId(parameters, bslasId.get(0));
                parameters = readPenaltiesBySLAId(parameters, id);
            }
        }

        ReportFileName filename =
                generateFilename(ReportType.PRODUCT_OFFER_REPORT, new Long(productOfferId).toString());
        ReportGenerator.generateReport(ReportType.PRODUCT_OFFER_REPORT, parameters, filename.getLocalFileName());

        String localFilename = filename.getLocalFileName();

        String customer = ""; // Retrieve customer email address from db/sla

        try {
            Mail mail =
                    new Mail(properties.getProperty(MAIL_SERVER), properties.getProperty(MAIL_FROM), customer,
                            MAIL_SUBJECT, MAIL_BODY, new String[] { localFilename });
            mail.send();
        }
        catch (AddressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Generates a filename to store the report. The filename is generated using the OUTPUT_DIR specified in the
     * properties file + the report type + prefix + actual date + a random number (to avoid clashes)
     * 
     * @param type
     *            : report type, will be used as part of the filename
     * @param prefix
     *            : any of the input data of the request (i.e. slaID), will be used as part of the filename
     * @return
     * 
     */
    private ReportFileName generateFilename(ReportType type, String prefix) {
        Format formatter = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH);
        ReportFileName filenames = new ReportFileName();
        String localOutputFile = "", urlOutputFile = "";

        String localOutputDir = properties.getProperty(OUTPUT_DIR);
        String urlOutputDir = properties.getProperty(URL_OUTPUT_DIR);

        logger.info("localOutputDir in properties = " + localOutputDir);
        logger.info("urlOutputDir = in properties " + urlOutputDir);

        if (localOutputDir != null) {
            File testFile = new File(localOutputDir);
            if (!testFile.exists()) {
                logger.info("localOutputDir does no exist, creating it...");
                boolean result = testFile.mkdirs();
                if (result)
                    localOutputFile = localOutputDir + System.getProperty("file.separator");

                else
                    localOutputFile = "";
            }
            else {
                localOutputFile = localOutputDir + System.getProperty("file.separator");
            }
        }

        if (urlOutputDir != null)
            urlOutputFile = urlOutputDir + "/";
        else
            urlOutputFile = "";

        String filename =
                type.name() + "_" + prefix + "_" + formatter.format(new Date()) + "_"
                        + java.util.UUID.randomUUID().toString() + ".pdf";

        filenames.setLocalFileName(localOutputFile + filename);
        filenames.setUrlFileName(urlOutputFile + filename);
        return filenames;
    }

    //escamez 
    //old DataBaseHandler content
    
    public ParametersList readProductOffer(ParametersList list, long productOfferId) {
        EmSpProductsOffer productOffer = productOfferManager.getProductOfferById(productOfferId);

        if (productOffer == null) {
            logger.info("There is any Product-Offer with id = " + productOfferId);
            return list;
        }
        Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);

        list.put(ParamNames.PRODUCT_OFFER_ID.toString(), new Long(productOfferId).toString());
        list.put(ParamNames.PRODUCT_OFFER_NAME.toString(), productOffer.getTxName());
        list.put(ParamNames.PRODUCT_OFFER_DESCRIPTION.toString(), productOffer.getTxDescription());
        list.put(ParamNames.PRODUCT_OFFER_REVISION_ID.toString(), productOffer.getTxRevisionid());
        list.put(ParamNames.PRODUCT_OFFER_VALID_FROM.toString(), formatter.format(productOffer.getDtValidFrom()));
        list.put(ParamNames.PRODUCT_OFFER_VALID_TO.toString(), formatter.format(productOffer.getDtValidTo()));
        list.put(ParamNames.PRODUCT_OFFER_STATUS.toString(), productOffer.getTxStatus());
        list.put(ParamNames.PRODUCT_OFFER_BSLAT_ID.toString(), productOffer.getTxBslatid());

        EmSpProducts product = productOffer.getEmSpProducts();
        list.put(ParamNames.PRODUCT_ID.toString(), new Long(product.getNuProductid()).toString());
        list.put(ParamNames.PRODUCT_NAME.toString(), product.getTxProductname());
        list.put(ParamNames.PRODUCT_DESCRIPTION.toString(), product.getTxProductdesc());
        list.put(ParamNames.PRODUCT_BRAND.toString(), product.getTxBrand());
        list.put(ParamNames.PRODUCT_RELEASE.toString(), product.getTxRelease());
        list.put(ParamNames.PRODUCT_STATUS.toString(), product.getTxStatusName());
        list.put(ParamNames.PRODUCT_VALID_FROM.toString(), formatter.format(product.getDtValidfrom()));
        list.put(ParamNames.PRODUCT_VALID_TO.toString(), formatter.format(product.getDtValidto()));
        list.put(ParamNames.PRODUCT_INSERT_DATE.toString(), formatter.format(product.getDtInsertdate()));

        if (product.getNuMaxPenaltiesCust() != null)
            list.put(ParamNames.PRODUCT_MAX_PENALTIES_CUSTOMER.toString(), product.getNuMaxPenaltiesCust().toString());

        if (product.getNuMaxPenaltyCancel() != null)
            list.put(ParamNames.PRODUCT_MAX_PENALTIES_CANCEL.toString(), product.getNuMaxPenaltyCancel().toString());

        if (product.getNuMaxPenaltyRemove() != null)
            list.put(ParamNames.PRODUCT_MAX_PENALTIES_REMOVE.toString(), product.getNuMaxPenaltyRemove().toString());

        if (product.getRating() != null)
            list.put(ParamNames.PRODUCT_RATING.toString(), product.getRating().getFormatRating());

        Set<EmComponentPrice> prices = productOffer.getEmComponentPrices();
        Iterator<EmComponentPrice> iter = prices.iterator();
        while (iter.hasNext()) {
            EmComponentPrice price = iter.next();
            list.put(ParamNames.PRICE.toString(), price.getNuPrice().toString() + " "
                    + price.getEmCurrencies().getTxName());
            list.put(ParamNames.PRICE_QUANTITY.toString(), price.getNuQuantity().toString());
            list.put(ParamNames.PRICE_NAME.toString(), price.getTxName());
            list.put(ParamNames.PRICE_CURRENCY.toString(), price.getEmCurrencies().getTxName());
            list.put(ParamNames.PRICE_TYPE.toString(), price.getEmPriceType().getTxName());

            if (price.getDtValidFrom() != null)
                list.put(ParamNames.PRICE_VALID_FROM.toString(), formatter.format(price.getDtValidFrom()));

            if (price.getDtValidTo() != null)
                list.put(ParamNames.PRICE_VALID_TO.toString(), formatter.format(price.getDtValidTo()));

            if (price.getTxDescription() != null)
                list.put(ParamNames.PRICE_DESCRIPTION.toString(), price.getTxDescription());

        }
        return list;
    }

    public ParametersList readBusinessSLAId(ParametersList list, long partyId) {
        ArrayList<String> productOfferId = list.get(ParamNames.PRODUCT_OFFER_ID.toString());
        EmParty party = new EmParty();
        party.setNuPartyid(new Long(partyId));
 
        for (String id : productOfferId) {
            logger.info("Reading CustomerProduct information with ProductOfferId = " + id + " and party " + partyId);

            List<EmCustomersProducts> customerProducts =
                    customerProductManager.getCustomerProductsByProductOfferAndParty(
                            new EmSpProductsOffer(new Long(id)), party);
            for (EmCustomersProducts customerProduct : customerProducts) {
                list.put(ParamNames.CUSTOMER_PRODUCT_BSLA_ID.toString(), customerProduct.getTxBslaid());
            }
        }

        return list;
    }

    public ParametersList readSLA(String bslaId, ParametersList list) {
        // Take the business SLA ID from the parameter list, use it to retrieve the sla from the registry as well as its
        // dependencies
        logger.info("Reading SLA:= " + bslaId);

        if (bslaId == null) {
            logger.error("\t slaId CAN NOT be null");
            return list;
        }

        try {

            logger.info("Accessing SLARegistry to retrieve SLA = " + bslaId);
            SLA[] bsla =
                    businessContextService.getBusinessContext().getSLARegistry().getIQuery()
                            .getSLA(new UUID[] { new UUID(bslaId) });

            if (bsla != null && bsla.length > 0) {
                try {
                    BusinessSLAParser.loadBusinessSLA(list, bsla[0]);

                    // escamez - AgreedAt
                    list.put(ParamNames.AGREEMENT_DATE.name(),
                            CalendarDate.D2BM2BY4.getDateAsString(bsla[0].getAgreedAt().getValue().getTime()));

                }
                catch (NullPointerException e) {
                    logger.error("CustomerRelationsImpl# readSLA# NullPointerException# " + e);
                    e.printStackTrace();
                }
                catch (SAXException e) {
                    logger.error("CustomerRelationsImpl# readSLA# SAXException# " + e);
                    e.printStackTrace();
                }

            }

        }
        catch (InvalidUUIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLAManagerContextException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    
    public ParametersList readSLAs(ParametersList list) {
        // Take the business SLA ID from the parameter list, use it to retrieve the sla from the registry as well as its
        // dependencies
        logger.info("Reading SLAs...");
        List<String> bslasId = list.get(ParamNames.CUSTOMER_PRODUCT_BSLA_ID.toString());

        // Should be only one
        String bslaId = null;

        if (bslasId != null) {
            bslaId = bslasId.get(0);
            try {
                                
                logger.info("Accessing SLARegistry to retrieve SLA = " + bslaId);
                SLA[] bsla = businessContextService.getBusinessContext().getSLARegistry().getIQuery().getSLA(new UUID[] { new UUID(bslaId) });

                if (bsla != null && bsla.length > 0) {
                    try {
                        BusinessSLAParser.loadBusinessSLA(list, bsla[0]);

                        //escamez - AgreedAt
                        list.put(ParamNames.AGREEMENT_DATE.name(), CalendarDate.D2BM2BY4.getDateAsString(bsla[0].getAgreedAt().getValue().getTime()));
                        
                    }
                    catch (NullPointerException e) {
                        logger.error("CustomerRelationsImpl# readSLAs# NullPointerException# "+e);
                        e.printStackTrace();
                    }
                    catch (SAXException e) {
                        logger.error("CustomerRelationsImpl# readSLAs# SAXException# "+e);
                        e.printStackTrace();
                    }

                    // UUID[] dependencies = slaRegistry.getIQuery().getDependencies(new UUID(bslaId));
                }

            }
            catch (InvalidUUIDException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            logger.info("WARNING: No Business SLA Id found");
        }

        return list;
    }

    public SLATemplate readSLAT(String slatid) {
        // retrieve the slat from the registry

        logger.info("Reading SLAT " + slatid);

        SLATemplate slat = null;
        try {
            slat = businessContextService.getBusinessContext().getSLATemplateRegistry().getSLATemplate(new UUID(slatid));
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return slat;
    }
    
    public Calendar readAgreementDate(String slaId) {
        // retrieve the sla info from the registry

        logger.info("Reading SLA " + slaId);

        SLAMinimumInfo[] info = null;
        Calendar agreedAt = null;
        try {
            info = businessContextService.getBusinessContext().getSLARegistry().getIQuery().getMinimumSLAInfo(new UUID[] { new UUID(slaId) });
            if (info != null && info.length > 0) {
                agreedAt = info[0].agreedAt.getValue();
                logger.info("Agreed at " + agreedAt.toString());
            }
        }

        catch (InvalidUUIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLAManagerContextException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return agreedAt;
    }

    public ParametersList readPenaltiesBySLAId(ParametersList list, String slaId) {
        logger.info("DatabaseHandler, readPenaltiesBySLAId for sla = " + slaId);
        
        List<EmPenalty> penalties = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        penalties = penaltyService.getPenaltiesOrderedByDate(slaId,true);

        Format formatter = new SimpleDateFormat("MMM dd yyyy HH:mm:ss", Locale.ENGLISH);

        for (EmPenalty penalty : penalties) {
            list.put(ParamNames.PENALTY_VALUE.toString(), new Float(penalty.getValue()).toString());
            list.put(ParamNames.PENALTY_DESCRIPTION.toString(), penalty.getDescription());
            list.put(ParamNames.PENALTY_OBLIGATED_PARTY.toString(), penalty.getObligatedParty());
            list.put(ParamNames.PENALTY_GUARANTETERM.toString(), penalty.getViolation().getGuaranteeTermId());
            list.put(ParamNames.PENALTY_VIOLATION_INITIAL_TIME.toString(), formatter.format(penalty.getViolation()
                    .getInitialTime()));
            list.put(ParamNames.PENALTY_VIOLATION_END_TIME.toString(), formatter.format(penalty.getViolation()
                    .getEndTime()));

            if (penalty.getPenaltyDate() != null)
                list.put(ParamNames.PENALTY_DATE.toString(), formatter.format(penalty.getPenaltyDate()));

        }
        return list;
    }

    public ParametersList readPenaltiesBySLAId(ParametersList list, String slaId, Date startDate, Date endDate) {
        logger.info("DatabaseHandler, readPenaltiesBySLAId for sla = " + slaId);
        List<EmPenalty> penalties = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        penalties =penaltyService.getPenaltiesPeriodOrderedByDate(slaId, startDate, endDate);

        Format formatter = new SimpleDateFormat("MMM dd yyyy HH:mm:ss", Locale.ENGLISH);

        for (EmPenalty penalty : penalties) {
            list.put(ParamNames.PENALTY_VALUE.toString(), new Float(penalty.getValue()).toString());
            list.put(ParamNames.PENALTY_DESCRIPTION.toString(), penalty.getDescription());
            list.put(ParamNames.PENALTY_OBLIGATED_PARTY.toString(), penalty.getObligatedParty());
            list.put(ParamNames.PENALTY_GUARANTETERM.toString(), penalty.getViolation().getGuaranteeTermId());
            list.put(ParamNames.PENALTY_VIOLATION_INITIAL_TIME.toString(), formatter.format(penalty.getViolation()
                    .getInitialTime()));

            Date violationEnd = penalty.getViolation().getEndTime();
            if (violationEnd != null)
                list.put(ParamNames.PENALTY_VIOLATION_END_TIME.toString(), formatter.format(violationEnd));

            if (penalty.getPenaltyDate() != null)
                list.put(ParamNames.PENALTY_DATE.toString(), formatter.format(penalty.getPenaltyDate()));

        }
        return list;
    }

    public ParametersList readViolationsReport(ParametersList list, String slatId) {
        GTStatistics stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        stat = gtStatisticsService.getGTStatistics(slatId);

        list.put(ParamNames.TOTAL_NUMBER_GT_NO_VIOLATION_NO_PENALTY.toString(), new Integer(stat
                .getNumberGTsNoViolatedNoPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_TOTAL.toString(), new Integer(stat.getNumberGTs()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_VIOLATION_NO_PENALTY.toString(), new Integer(stat
                .getNumberGTsViolatedNoPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_VIOLATION_PENALTY.toString(), new Integer(stat
                .getNumberGTsViolatedWithPenalty()).toString());

        return list;
    }

    public ParametersList readViolationsReport(ParametersList list, String slatId, Date startDate, Date endDate) {
        GTStatistics stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        //escamez
        //SLATemplate slat = readSLAT(slatId);

        // if (slat != null) {
        // List<String> gts = BusinessSLAParser.getGuaranteeTermIds(slat);

        // for (String gt : gts) {
        // stat = ((BusinessProvisioningAdjustmentImpl) bpac).getGTStatistics(slatId, gt, startDate, endDate);
        // list.put(ParamNames.GT_NAME.toString(), gt);
        // list.put(ParamNames.NUMBER_GT_NO_VIOLATION_NO_PENALTY.toString(), new Integer(stat
        // .getNumberGTsNoViolatedNoPenalty()).toString());
        // list.put(ParamNames.NUMBER_GT_TOTAL.toString(), new Integer(stat.getNumberGTs()).toString());
        // list.put(ParamNames.NUMBER_GT_VIOLATION_NO_PENALTY.toString(), new Integer(stat
        // .getNumberGTsViolatedNoPenalty()).toString());
        // list.put(ParamNames.NUMBER_GT_VIOLATION_PENALTY.toString(), new Integer(stat
        // .getNumberGTsViolatedWithPenalty()).toString());
        // }

        // }
        stat = gtStatisticsService.getGTStatistics(slatId, startDate, endDate);

        list.put(ParamNames.TOTAL_NUMBER_GT_NO_VIOLATION_NO_PENALTY.toString(), new Integer(stat
                .getNumberGTsNoViolatedNoPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_TOTAL.toString(), new Integer(stat.getNumberGTs()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_VIOLATION_NO_PENALTY.toString(), new Integer(stat
                .getNumberGTsViolatedNoPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_GT_VIOLATION_PENALTY.toString(), new Integer(stat
                .getNumberGTsViolatedWithPenalty()).toString());

        return list;
    }

    public ParametersList readSLAStatistics(ParametersList list, String slatId) {
       
        SlaStatistics stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        stat = slaStatisticsService.getSLAStatistics(slatId);

        Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);
        list.put(ParamNames.TOTAL_NUMBER_SLAS_NO_VIOLATION_NO_PENALTY.toString(), new Integer(stat.getNumberSLAsOK())
                .toString());
        list.put(ParamNames.TOTAL_NUMBER_SLAS_VIOLATION_NO_PENALTY.toString(), new Integer(stat
                .getNumberSLAsViolatedNoPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_SLAS_VIOLATION_PENALTY.toString(), new Integer(stat
                .getNumberSLAsViolatedWithPenalty()).toString());
        list.put(ParamNames.TOTAL_NUMBER_SLAS_TOTAL.toString(), new Integer(stat.getNumberSLAs()).toString());
        return list;
    }

    public ParametersList readSLAStatistics(ParametersList list, String slatId, Date startDate, Date endDate) {
        LinkedHashMap<Date, SlaStatistics> stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        stat = slaStatisticsService.getDailySLAStatistics(slatId, startDate, endDate);
       
        GraphsMaker graph = new GraphsMaker(stat);
            
        try {
            graph.createXYChart("graph.jpg");
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ParametersList readGTStatus(ParametersList list, String slaId, String guaranteeTerm) {
        GTStatus stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        stat = gtStatusService.getGTStatus(slaId, guaranteeTerm);

        Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);

        return list;
    }

    public ParametersList readGTStatus(ParametersList list, String slaId, String guaranteeTerm, String KPI) {
        GTStatus stat = null;

//        if (bpac == null) {
//            logger.info("ERROR: BPac not loaded");
//            return list;
//        }

        stat = gtStatusService.getGTStatus(slaId, guaranteeTerm, KPI);

        Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);

        return list;
    }
    
    public void setCustomerProductManager(CustomerProductManager customerProductManager) {
        this.customerProductManager = customerProductManager;
    }

    public void setProductOfferManager(ProductOfferManager productOfferManager) {
        this.productOfferManager = productOfferManager;
    }

    public void setGtStatisticsService(GTStatisticsService gtStatisticsService) {
        this.gtStatisticsService = gtStatisticsService;
    }

    public void setGtStatusService(GTStatusService gtStatusService) {
        this.gtStatusService = gtStatusService;
    }

    public void setSlaStatisticsService(SlaStatisticsService slaStatisticsService) {
        this.slaStatisticsService = slaStatisticsService;
    }

    public void setAccountEventManager(AccountEventManager accountEventService) {
        this.accountEventService = accountEventService;
    }
    
}
