/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * @author Agustin Escamez
 * @email escamez@tid.es
 * @date June 10, 2011
 */
public final class JAXBUtils<T> {
	
    private static Logger logger = Logger.getLogger(JAXBUtils.class);
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static final String BIZZTERMS_PACKAGE="org.slaatsoi.business.schema";
	
	private static  JAXBContext context=null;
	private static  Unmarshaller unmarshaller=null;
	
	static{
	    try {
            context = JAXBContext.newInstance(BIZZTERMS_PACKAGE);
            unmarshaller = context.createUnmarshaller() ;
        }
        catch (JAXBException e) {
            logger.error("JAXBUtils# static block# JAXBException# Unmarshalled NOT initialized");
        }
	}
	
	/**
	 * @param fileName fully qualified file name (including path)
	 * @return string representation of the file content
	 * @throws IOException
	 */
	public static String fileToString(String fileName) throws IOException {
		File file = new File(fileName); 
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		
		
		String line = null;
		
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(LINE_SEPARATOR);
		}
		
		return stringBuilder.toString();
	}
	
	/**
	 * @param object to save to file
	 * @param fileName to save the content of the object
	 * @return
	 */
	public static boolean saveToFile(Object object, String fileName) {
		boolean result = true;

		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			
			objectOutputStream.writeObject(object);			
			objectOutputStream.flush();
			byteArrayOutputStream.flush();
			
			FileOutputStream fileOutputStream = new FileOutputStream(fileName);
			fileOutputStream.write(byteArrayOutputStream.toByteArray());
			fileOutputStream.flush();
			
			fileOutputStream.close();
			byteArrayOutputStream.close();
			objectOutputStream.close();
			
		} catch (FileNotFoundException e) {
			result = false;
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			result = false;
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * @param object to marshall
	 * @return the XML string representation of the given object
	 * @throws JAXBException
	 */
	public static String marshall(Object object) throws JAXBException {

	    String marshalling=null;
	    StringWriter stringWriter =new StringWriter();
	    JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
	    Marshaller m = jaxbContext.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    	    
		try {
		    m.marshal(object, stringWriter);
		    marshalling = stringWriter.toString();
	        stringWriter.flush();
            stringWriter.close();
            
        }
        catch (IOException e) {
            logger.error("bwt-postsale# JAXBUtils# IOException# "+e);
        }
		
		return marshalling;
	}
	
	/**
	 * @param xml Code that contais the marshalled tree
	 * @param xmlSchema it's optional, can be used to turn validation on/off
	 * @return The unmarshalled object 
	 * @throws SAXException
	 */
	public static Object unmarshall(String xml, String xmlSchema, Class<?> objectClass) throws SAXException,NullPointerException {
	    
		Object object = null;
		Schema schema = null;
		
		// check for null input
		if (xml == null ) {
		    logger.error("postsale# JAXBUtils# NullPointerException# supplied xml or xmlSchema IS NULL");
		    throw new NullPointerException();		    
		}
		
		 SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		 if (xmlSchema!=null){		   
		    schema = schemaFactory.newSchema(new StreamSource(new StringReader(xmlSchema)));
		}
		
		//logger.debug("Parsing xml to " + objectClass + ":\n" + xml);
		
		ByteArrayInputStream xmlContentBytes = new ByteArrayInputStream(xml.getBytes());
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(objectClass);
            // create an Unmarshaller
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            //note: setting schema to null will turn validator off
            unmarshaller.setSchema(schema);
            
            object = unmarshaller.unmarshal(xmlContentBytes);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
		return object;
	}

	
	public static<T> T unmarshall(InputStream xmlIS, Class<T> clazz) throws JAXBException{
	       
       JAXBElement<T> element = unmarshaller.unmarshal(new StreamSource(xmlIS),clazz);
       
       return element.getValue();
	}
	
	public static<T> T unmarshall(String xmlChunk, Class<T> clazz) throws JAXBException{
        
	    ByteArrayInputStream bais = new ByteArrayInputStream(xmlChunk.getBytes());
	    JAXBElement<T> element = unmarshaller.unmarshal(new StreamSource(bais),clazz);
	    
	    try {
            bais.close();
        }
        catch (IOException e) {
            logger.error("JAXBUtils# unmarshall# IOException closing ByteArrayInputStream");
        }
	       
	    return element.getValue();
	}
	
}
