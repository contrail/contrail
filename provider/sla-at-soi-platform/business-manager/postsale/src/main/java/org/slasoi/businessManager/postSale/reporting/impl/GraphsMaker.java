/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/GraphsMaker.java $
 */

package org.slasoi.businessManager.postSale.reporting.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.PeriodAxis;
import org.jfree.chart.axis.PeriodAxisLabelInfo;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.Hour;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;
import org.slasoi.businessManager.common.ws.types.SlaStatistics;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class GraphsMaker {
    LinkedHashMap<Date, SlaStatistics> stat = null;

    public GraphsMaker(LinkedHashMap<Date, SlaStatistics> stat) {
        this.stat = stat;
    }

    /*
     * Creates a XYChart, saves it as png and returns the path to the file.
     */
    public void createXYChart(String filename) throws FileNotFoundException, IOException {

        XYDataset xydatasetTotalSLAs = createDatasetTotalSLAs(stat);
        XYDataset xydatasetSLAsOK = createDatasetSLAsOK(stat);
        XYDataset xydatasetSLAsViolated = createDatasetSLAsViolated(stat);
        XYDataset xydatasetSLAsPenalty = createDatasetSLAsPenalty(stat);

        JFreeChart jfreechart = createChart(xydatasetTotalSLAs);
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        chartpanel.setPreferredSize(new Dimension(500, 270));
        chartpanel.setDomainZoomable(true);
        chartpanel.setRangeZoomable(true);

        saveToFile(jfreechart, filename, 500, 300, 1);

    }

    private XYDataset createDatasetTotalSLAs(LinkedHashMap<Date, SlaStatistics> stat) {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        TimeSeries timeseries = new TimeSeries("SLA Status");

        for (Date date : stat.keySet()) {
            timeseries.add(new Day(date), stat.get(date).getNumberSLAs());
        }

        timeseriescollection.addSeries(timeseries);
        return timeseriescollection;
    }

    private XYDataset createDatasetSLAsOK(LinkedHashMap<Date, SlaStatistics> stat) {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        TimeSeries timeseries = new TimeSeries("SLA Status");

        for (Date date : stat.keySet()) {
            timeseries.add(new Day(date), stat.get(date).getNumberSLAsOK());
        }

        timeseriescollection.addSeries(timeseries);
        return timeseriescollection;
    }

    private XYDataset createDatasetSLAsViolated(LinkedHashMap<Date, SlaStatistics> stat) {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        TimeSeries timeseries = new TimeSeries("SLA Status");

        for (Date date : stat.keySet()) {
            timeseries.add(new Day(date), stat.get(date).getNumberSLAsViolatedNoPenalty());
        }

        timeseriescollection.addSeries(timeseries);
        return timeseriescollection;
    }

    private XYDataset createDatasetSLAsPenalty(LinkedHashMap<Date, SlaStatistics> stat) {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        TimeSeries timeseries = new TimeSeries("SLA Status");

        for (Date date : stat.keySet()) {
            timeseries.add(new Day(date), stat.get(date).getNumberSLAsViolatedWithPenalty());
        }
        timeseriescollection.addSeries(timeseries);
        return timeseriescollection;
    }

    private JFreeChart createChart(XYDataset xydataset) {
        JFreeChart jfreechart =
                ChartFactory.createXYLineChart("SLA Status", "Days", "Number of SLAs", xydataset,
                        PlotOrientation.VERTICAL, false, true, false);

        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setDomainGridlinePaint(Color.lightGray);
        xyplot.setDomainGridlineStroke(new BasicStroke(1.0F));
        xyplot.setRangeGridlinePaint(Color.lightGray);
        xyplot.setRangeGridlineStroke(new BasicStroke(1.0F));
        xyplot.setRangeTickBandPaint(new Color(240, 240, 240));

        PeriodAxis periodaxis = new PeriodAxis("Days");
        // new PeriodAxis(null, new Day(stat.keySet()), new Day(stat.get(stat.size() - 1).getDate()));
        PeriodAxisLabelInfo aperiodaxislabelinfo[] = new PeriodAxisLabelInfo[1];
        aperiodaxislabelinfo[0] = new PeriodAxisLabelInfo(org.jfree.data.time.Day.class, new SimpleDateFormat("dd"));
        periodaxis.setLabelInfo(aperiodaxislabelinfo);
        xyplot.setDomainAxis(periodaxis);

        ValueAxis valueaxis = xyplot.getRangeAxis();
        valueaxis.setRange(0.0D, 100D);
        XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        xyitemrenderer.setSeriesPaint(0, Color.green);
        xyitemrenderer.setSeriesStroke(0, new BasicStroke(2.0F));
        /*
         * ValueMarker valuemarker = new ValueMarker(80D); valuemarker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
         * valuemarker.setPaint(Color.red); valuemarker.setStroke(new BasicStroke(2.0F));
         * valuemarker.setLabel("Temperature Threshold"); valuemarker.setLabelFont(new Font("SansSerif", 0, 11));
         * valuemarker.setLabelPaint(Color.red); valuemarker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
         * valuemarker.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT); xyplot.addRangeMarker(valuemarker);
         */
        Hour hour = new Hour(18, 30, 6, 2005);
        Hour hour1 = new Hour(20, 30, 6, 2005);
        double d = hour.getFirstMillisecond();
        double d1 = hour1.getFirstMillisecond();
        IntervalMarker intervalmarker = new IntervalMarker(d, d1);
        intervalmarker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
        intervalmarker.setPaint(new Color(150, 150, 255));
        intervalmarker.setLabel("Automatic Cooling");
        intervalmarker.setLabelFont(new Font("SansSerif", 0, 11));
        intervalmarker.setLabelPaint(Color.blue);
        intervalmarker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
        intervalmarker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        xyplot.addDomainMarker(intervalmarker, Layer.BACKGROUND);

        ValueMarker valuemarker1 = new ValueMarker(d, Color.blue, new BasicStroke(2.0F));
        ValueMarker valuemarker2 = new ValueMarker(d1, Color.blue, new BasicStroke(2.0F));
        xyplot.addDomainMarker(valuemarker1, Layer.BACKGROUND);
        xyplot.addDomainMarker(valuemarker2, Layer.BACKGROUND);
        return jfreechart;
    }

    private void saveToFile(JFreeChart chart, String aFileName, int width, int height, float quality)
            throws FileNotFoundException, IOException {
        BufferedImage img = draw(chart, width, height);
        FileOutputStream fos = new FileOutputStream(aFileName);
        
        JPEGImageEncoder encoder2 = JPEGCodec.createJPEGEncoder(fos);
        JPEGEncodeParam param2 = encoder2.getDefaultJPEGEncodeParam(img);
        param2.setQuality((float) quality, true);
        encoder2.encode(img, param2);
        
        fos.flush();
        fos.close();
            
        
        /*
        
        IIOImage outputImage = new IIOImage(img, null, null);
        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
        ImageWriteParam writeParam = writer.getDefaultWriteParam();
        
        writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        writeParam.setCompressionQuality(quality);
        
        writer.setOutput(fos);
        writer.write( null, outputImage, writeParam);        
        writer.dispose();
        */
        
        
    }

    private BufferedImage draw(JFreeChart chart, int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Graphics2D g2 = img.createGraphics();
        chart.draw(g2, new Rectangle2D.Double(0, 0, width, height));
        g2.dispose();
        return img;
    }

}
