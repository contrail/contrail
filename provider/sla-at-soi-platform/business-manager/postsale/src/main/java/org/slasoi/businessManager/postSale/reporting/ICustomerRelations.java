/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/ICustomerRelations.java $
 */

package org.slasoi.businessManager.postSale.reporting;

import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.slamodel.sla.SLA;

public interface ICustomerRelations {

    /**
     * 
     * @param slaId
     *            : identifier of the SLA
     * @return the sla
     */
    public SLA getSLA(String slaId);

    /**
     * Generates a report with:
     * 
     * - Product-offer information (name, dates, description, etc)
     * 
     * - Prices
     * 
     * - SLA
     * 
     * - Penalties per SLA (date, violation, penalty)
     * 
     * @param productOfferId
     *            : identifier of the Product-Offer
     * @param partyId
     *            : identifier of the party (customer)
     * @param year
     * @param month
     * @param day
     * @return full path where the pdf file will be stored
     */
    public String getPenaltiesReportByProductOfferIdPartyIdYearMonthDay(long productOfferId, long partyId, int year,
            int month, int day);

    /**
     * Same as the method above except that the day is not specified
     * 
     */
    public String getPenaltiesReportByProductOfferIdPartyIdYearMonth(long productOfferId, long partyId, int year,
            int month);

    /**
     * Generates a report with the following information:
     * 
     * + number of GTs without violation nor penalty
     * 
     * + number of GTs with violation but without penalty
     * 
     * + number of GTs with violations and penalties
     * 
     * + total number of GTs
     * 
     * @param productOfferId
     *            : identifier of the Product-Offer
     * @param year
     * @param month
     * @param day
     * @return full path where the pdf file will be stored
     */
    public String getViolationsReportByProductOfferIdYearMonthDay(long productOfferId, int year, int month, int day);

    /**
     * Same as the method above except that the day is not specified
     * 
     */
    public String getViolationsReportByProductOfferIdYearMonth(long productOfferId, int year, int month);

    /**
     * Generates a report with the following information:
     * 
     * + number of GTs without violation nor penalty
     * 
     * + number of GTs with violation but without penalty
     * 
     * + number of GTs with violations and penalties
     * 
     * + total number of GTs
     * 
     * @param businessSLATId
     *            : identifier of the Business SLAT
     * @param year
     * @param month
     * @param day
     * @return full path where the pdf file will be stored
     */
    public String getViolationsReportBySLATIdYearMonthDay(String businessSLATId, int year, int month, int day);

    /**
     * Same as the method above except that the day is not specified
     * 
     */
    public String getViolationsReportBySLATIdYearMonth(String businessSLATId, int year, int month);

    /**
     * Generates a report with the following information:
     * 
     * + number of SLAs without violation nor penalty
     * 
     * + number of SLAs with violation but without penalty
     * 
     * + number of SLAs with violations and penalties
     * 
     * + total number of SLAs
     * 
     * @param productOfferId
     *            : identifier of the Product-Offer
     * @param year
     * @param month
     * @return full path where the pdf file will be stored
     */
    public String getProductReportByProductOfferIdYearMonth(long productOfferId, int year, int month);

    /**
     * Same as the method above except but filtering by business SLAT identifier
     * 
     */
    public String getProductReportBySLATIdYearMonth(String businessSLATId, int year, int month);

    /**
     * Generates a report with the following information:
     * 
     * + current status of the given GuaranteeTerm (ok, violated)
     * 
     * + current values of KPIs attached to the specified GT (if available)
     * 
     * @param businessSLAId
     *            : identifier of the Business SLA
     * @param GuaranteeTerm
     *            : identifier of the Guarantee Term
     * @return full path where the pdf file will be stored
     */
    public String getGTStatusBySLAIdGT(String businessSLAId, String GuaranteeTerm);

    /**
     * Generates a report with the following information:
     * 
     * + current status of the given GuaranteeTerm (ok, violated)
     * 
     * + current value of the specified KPI
     * 
     * @param businessSLAId
     *            : identifier of the Business SLA
     * @param GuaranteeTerm
     *            : identifier of the Guarantee Term
     * @param KPI
     *            : identifier of the KPI
     * @return full path where the pdf file will be stored
     */
    public String getGTStatusBySLAIdGTKPI(String businessSLAId, String GuaranteeTerm, String KPI);
}
