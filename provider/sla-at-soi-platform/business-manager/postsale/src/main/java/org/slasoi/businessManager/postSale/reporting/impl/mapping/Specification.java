/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/mapping/Specification.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl.mapping;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author Beatriz Fuentes (TID) This class encapsulates the specification of the report: the database queries needed,
 *         and the format of the report
 */
public class Specification {

    private static Logger logger = Logger.getLogger(Specification.class.getName());

    List<Query> queries = new ArrayList<Query>();

    public List<Query> getQueries() {
        return queries;
    }

    public void setQueries(List<Query> queries) {
        this.queries = queries;
    }

    public void addQuery(Query query) {
        queries.add(query);
    }

    public static Specification fromXML(String specXml) {
        logger.info("Specification::fromXML");
        logger.info(specXml);
        XStream xstream = new XStream(new DomDriver());
        xstream.setClassLoader(Specification.class.getClassLoader());

        xstream.alias(org.slasoi.businessManager.postSale.reporting.impl.mapping.Specification.class.getSimpleName(),
                org.slasoi.businessManager.postSale.reporting.impl.mapping.Specification.class);
        xstream.alias(org.slasoi.businessManager.postSale.reporting.impl.mapping.Query.class.getSimpleName(),
                org.slasoi.businessManager.postSale.reporting.impl.mapping.Query.class);

        Specification spec = new Specification();

        try {
            // String fileContent = readFile(specificationFile);
            spec = (Specification) xstream.fromXML(specXml);
            logger.info(spec.toString());
        }
        catch (Exception e) {
            logger.info("Unable to deseserialised Specification. \n" + e.getMessage());
        }

        return spec;
    }

    public static Specification fromXMLFile(String filename) {
        return (fromXML(readFile(filename)));
    }

    public String toXML() {
        logger.info("Specification to XML");
        logger.info(this);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.businessManager.postSale.reporting.impl.mapping.Specification.class.getSimpleName(),
                org.slasoi.businessManager.postSale.reporting.impl.mapping.Specification.class);
        xstream.alias(org.slasoi.businessManager.postSale.reporting.impl.mapping.Query.class.getSimpleName(),
                org.slasoi.businessManager.postSale.reporting.impl.mapping.Query.class);

        String specStr = xstream.toXML(this);
        logger.info(specStr);

        return (specStr);
    }

    public static String readFile(String fileName) {
        String content = null;
        try {
            FileInputStream file = new FileInputStream(fileName);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            content = new String(b);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }

    public String toString() {
        String aux = "Specification\n";
        aux = aux + "List of queries:\n";
        for (Query q : queries)
            aux = aux + q.toString() + "\n";

        return aux;
    }
}
