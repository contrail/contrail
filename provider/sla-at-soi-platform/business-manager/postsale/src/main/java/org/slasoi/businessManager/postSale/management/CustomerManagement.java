/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 671 $
 * @lastrevision   $Date: 2011-02-10 13:45:00 +0100 (Thu, 10 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/management/CustomerManagement.java $
 */

package org.slasoi.businessManager.postSale.management;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPartyPartyrole;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.gslam.core.authorization.Authorization;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.sla;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomerManagement implements Authorization{
    
    Logger log = Logger.getLogger(CustomerManagement.class);
    
    @Autowired
    protected PartyManager partyService;
    
//    @ServiceReference
//    // obtained automatically by Spring-DM and OSGi Container.
//    public void setPartyService(PartyManager partyService) {
//        log.info("***************Registered Service partyService Found*************");
//        this.partyService = partyService;
//    }
 
/**
 * This method check is the customer is trying to negotiate is a valid customer.
 */
    public boolean checkAccess( SLATemplate template ){
        log.info("CheckAccess: check access of the customer to the negotiation");
        /**TODO
         * Take into account the product to purchase.
         */
        boolean auth = false;
        Party[] parties = template.getParties();
        if(parties!=null&&parties.length>0){
            try{
                for(Party party:parties){
                    //we suppose the customer_role="CUSTOMER"
                    if(sla.customer.getValue().equalsIgnoreCase(party.getAgreementRole().getValue())){
                        //the role is a customer
                        //we suppose that the customer identification will be a number
                        long customerID = new Long(party.getId().getValue());
                        EmParty customer= partyService.getPartyByIdLoaded(customerID);
                        if(customer!=null){
                            //balance is negative as normal
                            if((customer.getNuCreditLimit().add(customer.getNuBalance())).
                                    compareTo(BigDecimal.ZERO)<1){
                                log.error("The customer has no balance to buy products");
                                return false;
                            }else{
                                //check if it is a authorized customer.
                                Set<EmPartyPartyrole> roles = customer.getEmPartyPartyroles();
                                if(roles!=null&&roles.size()>0){
                                    Iterator <EmPartyPartyrole> it = roles.iterator();
                                    EmPartyPartyrole partyRole;
                                    while(it.hasNext()){
                                        partyRole = it.next();
                                        if(Constants.PARTY_ROLE_CUSTOMER==
                                            partyRole.getEmPartyRole().getNuPartyroleid()){
                                            log.debug("Role of Customer");
                                                if(Constants.PARTY_STATE_APPROVAL.equalsIgnoreCase(
                                                        partyRole.getTxStatus())){
                                                    log.info("It is an authorized customer");
                                                    return true;
                                                }else{
                                                    log.info("It is an unAuthorized customer");
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch(Exception e){
            log.error("Error:"+e.toString());
            }
        }       
        return auth;
    }

/**
 * This method check is the customer is trying to negotiate is a valid customer.    
 * @param productId
 * @param customerId
 * @return
 */
    public boolean customerPurchaseAuth(long productId, long customerId){
        log.info("CustomerPurchaseAuth method: check access of the customer to the negotiation");
        /**TODO
         * Take into account the product to purchase.
         */
        boolean auth = false;
            try{  
              EmParty customer= partyService.getPartyByIdLoaded(customerId);
              if(customer!=null){
                  //balance is negative as normal
                   if((customer.getNuCreditLimit().add(customer.getNuBalance())).
                           compareTo(BigDecimal.ZERO)<1){
                        log.error("The customer has no balance to buy products");
                        return false;
                   }else{
                     //check if it is a authorized customer.
                     Set<EmPartyPartyrole> roles = customer.getEmPartyPartyroles();
                     if(roles!=null&&roles.size()>0){
                        Iterator <EmPartyPartyrole> it = roles.iterator();
                        EmPartyPartyrole partyRole;
                        while(it.hasNext()){
                          partyRole = it.next();
                          if(Constants.PARTY_ROLE_CUSTOMER==
                             partyRole.getEmPartyRole().getNuPartyroleid()){
                             log.debug("Role of Customer");
                             if(Constants.PARTY_STATE_APPROVAL.equalsIgnoreCase(
                                 partyRole.getTxStatus())){
                                   log.info("It is an authorized customer");
                                  return true;
                              }else{
                                 log.info("It is an unAuthorized customer");
                              }
                           }
                        }
                     }
                 }
              }

            }catch(Exception e){
            log.error("Error:"+e.toString());
            }             
        return auth;        
    }
    
}
