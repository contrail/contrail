package org.slasoi.businessManager.postSale.reporting.impl;

import java.awt.Color;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;

public enum FontType {

    TITLE("Helvetica", "Cp1252", 15, Font.NORMAL, ColorType.TITLE.getColor()),
    HEADER_DATE("Helvetica", "Cp1252", 10, Font.ITALIC),
    BILLING_INFO("Helvetica", "Cp1252", 9, Font.NORMAL),
    PRICE("Helvetica", "Cp1252", 18, Font.BOLD, ColorType.PRICE.getColor()),
    CONTACTINFO("Helvetica", "Cp1252", 8, Font.ITALIC),
    TABLE_HEADER("Helvetica", "Cp1252", 8, Font.NORMAL, ColorType.TABLE_HEADER.getColor()),
    CELL("Helvetica", "Cp1252", 8, Font.NORMAL, ColorType.TABLE_CELL.getColor()),
    HEADER2("Helvetica", "Cp1252", 12, Font.ITALIC, ColorType.HEADER2.getColor()),
    HEADER3("Helvetica", "Cp1252", 10, Font.ITALIC),
    HEADER4("Helvetica", "Cp1252", 8, Font.ITALIC,ColorType.PRICE.getColor()),
    PARAGRAPH("Helvetica", "Cp1252", 10, Font.NORMAL,ColorType.PARAGRAPH.getColor());
    
    private BaseFont baseFont;
    private Font font;

    private FontType(String fontFamily, String charset, float fontSize, int fontType, Color color) {
        try {
            baseFont = BaseFont.createFont(fontFamily, charset, false);
        }
        catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        font = new Font(baseFont, fontSize, fontType, color);
    }

    private FontType(String fontFamily, String charset, float fontSize, int fontType) {
        try {
            baseFont = BaseFont.createFont(fontFamily, charset, false);
        }
        catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        font = new Font(baseFont, fontSize, fontType);
    }

    public BaseFont getBaseFont() {
        return baseFont;
    }

    public Font getFont() {
        return font;
    }

    public Font getFont(int fontSize, int fontType, Color color) {
        return new Font(baseFont, fontSize, fontType, color);
    }

}
