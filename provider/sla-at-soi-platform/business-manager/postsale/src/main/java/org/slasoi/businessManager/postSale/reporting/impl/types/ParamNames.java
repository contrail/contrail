/**
 * Copyright (c) 2008-2010, Telef�nica Investigaci�n y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telef�nica Investigaci�n y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telef�nica Investigaci�n y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/types/ParamNames.java $
 */

package org.slasoi.businessManager.postSale.reporting.impl.types;


public enum ParamNames {

    CUSTOMER_PRODUCT_BSLA_ID,
    SLAT_DESCRIPTION,

    PRODUCT_OFFER_ID,
    PRODUCT_OFFER_NAME,
    PRODUCT_OFFER_DESCRIPTION,
    PRODUCT_OFFER_REVISION_ID,
    PRODUCT_OFFER_VALID_FROM,
    PRODUCT_OFFER_VALID_TO,
    PRODUCT_OFFER_STATUS,
    PRODUCT_OFFER_BSLAT_ID,

    BILLING_FREQUENCY,

    PRODUCT_ID,
    PRODUCT_NAME,
    PRODUCT_DESCRIPTION,
    PRODUCT_BRAND,
    PRODUCT_RELEASE,
    PRODUCT_STATUS,
    PRODUCT_RATING,
    PRODUCT_VALID_FROM,
    PRODUCT_VALID_TO,
    PRODUCT_INSERT_DATE,
    PRODUCT_MAX_PENALTIES_CUSTOMER,
    PRODUCT_MAX_PENALTIES_CANCEL,
    PRODUCT_MAX_PENALTIES_REMOVE,

    PRICE,
    PRICE_QUANTITY,
    PRICE_VALID_FROM,
    PRICE_VALID_TO,
    PRICE_NAME,
    PRICE_DESCRIPTION,
    PRICE_CURRENCY,
    PRICE_TYPE,

    PENALTY_DATE,
    PENALTY_VALUE,
    PENALTY_DESCRIPTION,
    PENALTY_OBLIGATED_PARTY,
    PENALTY_VIOLATION_INITIAL_TIME,
    PENALTY_VIOLATION_END_TIME,
    PENALTY_GUARANTETERM,

    CONTACTPOINTNAME,
    CONTACTPOINTPHONE,
    CONTACTPOINTFAX,
    CONTACTPOINTADDRESS,
    CONTACTPOINTEMAIL,

    AGREEMENT_TERM_ID,
    GUARANTEED_STATE_SERVICE_NAME,
    GUARANTEED_STATE_PARAMETER,
    GUARANTEED_STATE_OPERATOR,
    GUARANTEED_STATE_VALUE,
    GUARANTEED_STATE_UNIT,
    GUARANTEED_STATE_EXPRESSION,

    GUARANTEED_ACTION_ID,
    GUARANTEED_ACTION_ACTOR,
    GUARANTEED_ACTION_POLICY,
    GUARANTEED_ACTION_PARAMETER,
    GUARANTEED_ACTION_OPERATOR,
    GUARANTEED_ACTION_CONDITION,
    GUARANTEED_ACTION_PENALTY,
    GUARANTEED_ACTION_PENALTY_COUNT,
    GUARANTEED_ACTION_TERMINATION,

    TERMINATION_CLAUSE_ID,
    TERMINATION_CLAUSE_TYPE,
    TERMINATION_CLAUSE_DESCRIPTION,
    TERMINATION_CLAUSE_INITIATOR,
    TERMINATION_CLAUSE_NOTIFICATION_PERIOD,
    TERMINATION_CLAUSE_NOTIFICATION_METHOD,
    TERMINATION_CLAUSE_FEES,

    //new params for terms 1.3.0
    REPORT_DESCRIPTION,
    REPORT_TYPE,
    REPORT_FORMAT,
    REPORT_TARGETS_TARGET_FUNCTIONAL_AGGREGATOR,
    REPORT_TARGETS_TARGET_SLAID,
    REPORT_TARGETS_TARGET_AGREEEMENTTERMID,
    REPORT_TARGETS_TARGET_GUARANTEEDID,
    REPORT_RECEIVER_FORMATTYPE,
    REPORT_RECEIVER_DELIVERYMETHOD,
    REPORT_RECEIVER_DELIVERYENDPOINT,
    
    REPORT_CREATIONSCHEDULE_AUTOMATIC_APERIODIC_DATETIME,    
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY,
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY,
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON,
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES,
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER,
    REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME,    
    
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_APERIODIC_DATETIME,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER,
    REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME,

    
    SUPPORT_PROCEDURES_NOTIF_MEDIA,
    SUPPORT_PROCEDURES_SEVERITY_LEVEL_NAME,
    SUPPORT_PROCEDURES_SEVERITY_LEVEL_DESCRIPTION,
    SUPPORT_PROCEDURES_RESPONSE_TIME_UNIT,
    SUPPORT_PROCEDURES_RESPONSE_TIME_VALUE,
    SUPPORT_PROCEDURES_RESOLUTION_TIME_UNIT,
    SUPPORT_PROCEDURES_RESOLUTION_TIME_VALUE,

    MONITORING_PARAMETER_NAME,
    MONITORING_PARAMETER_UNIT,
    MONITORING_DATA_COLLECTION_INTERVAL_VALUE,
    MONITORING_DATA_COLLECTION_INTERVAL_UNIT,
    MONITORING_AGGREGATION_INTERVAL_VALUE,
    MONITORING_AGGREGATION_INTERVAL_UNIT,
    
    SUPPORT_PHONE_NUMBER,
    SUPPORT_DAY_WEEK,
    SUPPORT_INIT_TIME,
    SUPPORT_FINISH_TIME,

    UPDATE_FREQUENCY,
    UPDATE_INSTALLATION_DESCRIPTION,
    UPDATE_DISTRIBUTION_METHOD,

    BACKUP_DESCRIPTION,
    BACKUP_TYPE,
    BACKUP_PERIODICITY,
    BACKUP_ESTIMATED_TIME,
    BACKUP_PERIODICITY_PRECISION,
    BACKUP_ESTIMATED_TIME_PRECISION,

    TOTAL_NUMBER_GT_NO_VIOLATION_NO_PENALTY,
    TOTAL_NUMBER_GT_VIOLATION_NO_PENALTY,
    TOTAL_NUMBER_GT_VIOLATION_PENALTY,
    TOTAL_NUMBER_GT_TOTAL,

    GT_NAME,
    NUMBER_GT_NO_VIOLATION_NO_PENALTY,
    NUMBER_GT_VIOLATION_NO_PENALTY,
    NUMBER_GT_VIOLATION_PENALTY,
    NUMBER_GT_TOTAL,

    TOTAL_NUMBER_SLAS_NO_VIOLATION_NO_PENALTY,
    TOTAL_NUMBER_SLAS_VIOLATION_NO_PENALTY,
    TOTAL_NUMBER_SLAS_VIOLATION_PENALTY,
    TOTAL_NUMBER_SLAS_TOTAL,

    /*
     * Tabla del Vendor
     * 
     *           `NU_EVENTID` : Clave del evento
�            NU_PARTYID_VENDOR` : party id del que carga el apunte, de momento esta a null
�           `NU_PARTYID_PAYER` : Party id  del que paga �Este es el campo que nos interesa  para sacar en el informe
�           `TC_TYPE` char(1) : es un character que indica  (Debit (D)) cuando es un cargo o (Credit (C)) cuando es un pago o devoluci�n

                debito cuando compras
                credito cuando 

�           `NU_EVENT_TYPEID : Identificador del tipo de evento que lo relaciona con la tabla EM_ACCOUNT_EVENT
�           `NU_CURRENCY_ID` : Identificador del tipo de moneda que lo relaciona con la tabla EM_CURRENCY
�           `TS_TIMESTAMP` datetime : Cuando se insert� el evento
�           `TX_SLAID` varchar(255) : Identificador del contrato al que est� asociado el cargo (con esto puedes sacar la tabla em_customer_product la relaci�n con la ofertata, producto.. bla   bla)
�           `TX_VIOLATIONID` varchar(255) : Si la inclusi�n de la entrada est� relacionada con una penalti ponemos su identificador
�           `NU_AMOUNT` decimal(19,2) : Montante del cargo. Si es negativo quiere decir que es un cobro, mientras que si es positivo es que es un pago/descuento.
�           `TX_DESCRIPTION` varchar(255): Descripci�n del evento. La descripci�n es de la forma: un_produtid,productname,un_productOfferid,offerName, tipo de precio (monthly,suubscription)
�           `NU_EVENTID_PAYMENT`: Cuando se produce un pago, en esta columna se dice cual fue evento con el cual se pag� este cargo
�           `NU_BANK_ID` bigint(20): Inicialmente los cargos estaban asociados a una cuenta bancaria(esta de aqu�), pero al final por problemas de los ws, se deshech� esta forma y se asoci� al party.
     */
    ACCOUNTEVENT_ID,
    ACCOUNTEVENT_PARTYID_VENDOR,
    ACCOUNTEVENT_PARTYID_PAYER,
    
    ACCOUNTEVENT_TC_TYPE,
    ACCOUNTEVENT_EVENT_TYPEID,
    
    ACCOUNTEVENT_CURRENCY_NAME,
    ACCOUNTEVENT_TIMESTAMP,
    ACCOUNTEVENT_SLAID,
    ACCOUNTEVENT_VIOLATIONID,
    ACCOUNTEVENT_AMOUNT,
    ACCOUNTEVENT_TOTAL_AMOUNT,
    ACCOUNTEVENT_DESCRIPTION,
    ACCOUNTEVENT_PAYMENT_DONE,   
    ACCOUNTEVENT_BANK_OFFICE_ADDRESS,
    ACCOUNTEVENT_BANK_ACCOUNT_NUMBER,        
    
    AGREEMENT_DATE,
    
    ACCOUNTEVENT_INITIAL_DATE,
    ACCOUNTEVENT_FINAL_DATE;
    
    
}
