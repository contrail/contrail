/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/ReportGenerator.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.postSale.reporting.exception.ImageException;
import org.slasoi.businessManager.postSale.reporting.impl.mapping.Group;
import org.slasoi.businessManager.postSale.reporting.impl.mapping.Report;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParametersList;
import org.slasoi.businessManager.postSale.reporting.impl.types.ReportType;
import org.slasoi.businessManager.postSale.reporting.impl.util.PathUtil;

import com.lowagie.text.DocumentException;

/**
 * @author Beatriz Fuentes(TID)
 * 
 */
public class ReportGenerator {
    
    private static final Logger logger = Logger.getLogger(ReportGenerator.class);
    
    private final static String PRODUCT_OFFER_REPORT_FILENAME = "PenaltiesReportFormat.xml";
    private final static String PRODUCT_REPORT_FILENAME = "ProductReportFormat.xml";
    private final static String VIOLATIONS_REPORT_FILENAME = "ViolationsReportFormat.xml";
    private final static String STATUS_REPORT_FILENAME = "StatusReportFormat.xml";
    private final static String BILLING_REPORT_FILENAME = "BillingReportFormat.xml";
    
    public static void generateReport(ReportType type, ParametersList parameters, String filename) {

        // Get the file with the specification for this kind of report
        Report report = getReportSpecification(type);
        report.setType(type);
        
        try {
            PDFConversor.toPDFFile(report, parameters, filename);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ImageException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static Report getReportSpecification(ReportType type) {
       
        String filename = PathUtil.REPORTING_HOME + PathUtil.FILE_SEPARATOR;

        if (type.equals(ReportType.PRODUCT_OFFER_REPORT)){
            filename = filename + PRODUCT_OFFER_REPORT_FILENAME;
        }
        else if (type.equals(ReportType.PRODUCT_REPORT)){
            filename = filename + PRODUCT_REPORT_FILENAME;
        }
        else if (type.equals(ReportType.VIOLATIONS_REPORT)){
            filename = filename + VIOLATIONS_REPORT_FILENAME;
        }
        else if (type.equals(ReportType.BILLING_REPORT)){
            filename = filename + BILLING_REPORT_FILENAME;
        }
        else{
            filename = filename + STATUS_REPORT_FILENAME;
        }

        return Report.fromXMLFile(filename);
    }

    private static Report insertParameters(Report report, HashMap<String, String> parameters) {

        List<Group> groups = report.getGroups();
        for (Group group : groups) {
            Map<String, String> params = group.getParameters();
            Map<String, String> newParams = new LinkedHashMap<String, String>();
            Iterator<String> iter = params.keySet().iterator();
            while (iter.hasNext()) {
                String name = iter.next();
                newParams.put(name, parameters.get(params.get(name)));
            }
            group.setParameters(newParams);
        }
        return report;
    }

}
