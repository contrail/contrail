/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/BusinessSLAParser.java $
 */

package org.slasoi.businessManager.postSale.reporting.impl;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.AperiodicScheduleType;
import org.slaatsoi.business.schema.AutomaticScheduleType;
import org.slaatsoi.business.schema.BackupRecoveryMechanism;
import org.slaatsoi.business.schema.BackupRecoveryMechanism.BackupMechanism;
import org.slaatsoi.business.schema.DurationType;
import org.slaatsoi.business.schema.Monitoring;
import org.slaatsoi.business.schema.Monitoring.Parameter;
import org.slaatsoi.business.schema.PeriodicScheduleType;
import org.slaatsoi.business.schema.ReceiverType;
import org.slaatsoi.business.schema.ReceiversType;
import org.slaatsoi.business.schema.RepeatDailyType;
import org.slaatsoi.business.schema.RepeatHourlyType;
import org.slaatsoi.business.schema.RepeatMinutelyType;
import org.slaatsoi.business.schema.RepeatMonthlyType;
import org.slaatsoi.business.schema.RepeatSecondlyType;
import org.slaatsoi.business.schema.RepeatWeekdayType;
import org.slaatsoi.business.schema.RepeatWeekendType;
import org.slaatsoi.business.schema.RepeatWeeklyType;
import org.slaatsoi.business.schema.RepeatYearlyType;
import org.slaatsoi.business.schema.ReportCreationScheduleType;
import org.slaatsoi.business.schema.ReportDeliveryScheduleType;
import org.slaatsoi.business.schema.Reporting;
import org.slaatsoi.business.schema.ReportingTargetType;
import org.slaatsoi.business.schema.ReportingTargetsType;
import org.slaatsoi.business.schema.Support;
import org.slaatsoi.business.schema.Support.AvailablePeriod;
import org.slaatsoi.business.schema.SupportProcedures;
import org.slaatsoi.business.schema.SupportProcedures.SeverityLevels.SeverityLevel;
import org.slaatsoi.business.schema.SupportTimePeriodType;
import org.slaatsoi.business.schema.UpdateProcess;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParamNames;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParametersList;
import org.slasoi.businessManager.postSale.reporting.impl.util.JAXBUtils;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Product;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Termination;
import org.slasoi.slamodel.sla.business.TerminationClause;
import org.xml.sax.SAXException;

public class BusinessSLAParser {
    private static final Logger logger = Logger.getLogger(BusinessSLAParser.class);

    private static final char SEPARATOR = '#';
    private static final String PENALTY = "Penalty";
    private static final String TERMINATION = "Termination";
    private static final String PRICE = "PriceSpecificationAction";
    private static final String BUSINESS_TERM1 = "businessTerm";
    private static final String BUSINESS_TERM2 = "BusinessTerm";
    private static final String REPORTING_ACTION = "Reporting";
    private static final String REPORTING_TAG = "<Reporting>";
    private static final String REPORTING_TAG_END = "</Reporting>";
    private static final String SUPPORT_PROCEDURES_ACTION = "SupportProcedures";
    private static final String SUPPORT_PROCEDURES_TAG = "<SupportProcedures>";
    private static final String SUPPORT_PROCEDURES_TAG_END = "</SupportProcedures>";
    private static final String MONITORING_ACTION = "Monitoring";
    private static final String MONITORING_TAG = "<Monitoring>";
    private static final String MONITORING_TAG_END = "</Monitoring>";
    private static final String SUPPORT_ACTION = "Support";
    private static final String SUPPORT_TAG = "<Support>";
    private static final String SUPPORT_TAG_END = "</Support>";
    private static final String UPDATE_ACTION = "Update";
    private static final String UPDATE_TAG = "<UpdateProcess>";
    private static final String UPDATE_TAG_END = "</UpdateProcess>";
    private static final String BACKUP_ACTION = "BackupRecoveryMechanism";
    private static final String BACKUP_TAG = "<BackupRecoveryMechanism>";
    private static final String BACKUP_TAG_END = "</BackupRecoveryMechanism>";
    
    public static ParametersList loadBusinessSLA(ParametersList list, SLATemplate bsla) throws NullPointerException, SAXException {
        list = loadDescription(list, bsla);
        list = loadContactInfo(list, bsla);
        list = loadAgreementTerms(list, bsla);
        list = loadBusinessTerms(list, bsla);
        
        return list;
    }
    
    public static ParametersList loadDescription(ParametersList list, SLATemplate bsla) {
        System.out.println("loadDescription");

        if (bsla != null) {
            list.put(ParamNames.SLAT_DESCRIPTION.toString(), bsla.getDescr());
            System.out.println("Got description " + bsla.getDescr());
        }
        return list;
    }

    public static ParametersList loadContactInfo(ParametersList list, SLATemplate bsla) {
        logger.info("loadBusinessSLA");

        if (bsla != null) {
            logger.info(bsla);
            Party[] parties = bsla.getParties();
            for (Party party : parties) {
                STND[] properties = party.getPropertyKeys();
                for (STND property : properties) {
                    String aux = property.toString().toUpperCase();
                    int index = aux.lastIndexOf(SEPARATOR);
                    String key = aux.substring(index + 1);
                    list.put(key, party.getPropertyValue(property));
                }
            }
        }
        else {
            logger.info("Business SLA is null!!");
        }
        return list;
    }

    public static ParametersList loadAgreementTerms(ParametersList list, SLATemplate sla) {
        AgreementTerm[] terms = sla.getAgreementTerms();

        for (AgreementTerm term : terms) {
            // skip business terms
            if (!term.getId().toString().equals(BUSINESS_TERM1) && !term.getId().toString().equals(BUSINESS_TERM2)) {

                Guaranteed[] guaranteed = term.getGuarantees();

                boolean penaltyCheck = false;
                boolean terminationCheck = false;
                boolean penaltyCountCheck = false;
                boolean agreementTermCheck = false;

                for (Guaranteed g : guaranteed) {

                    String agreementTermId = term.getId().toString();
                    String guaranteedServiceName = "";
                    String parameter = "";
                    String operator = "";

                    if (g instanceof Guaranteed.State) {
                        ConstraintExpr ce = ((State) g).getState();
                        if (ce instanceof TypeConstraintExpr) {
                            // TypeConstraintExpr
                            TypeConstraintExpr tce = (TypeConstraintExpr) ce;
                            ValueExpr valu = tce.getValue();
                            if (valu instanceof FunctionalExpr) {
                                parameter = ((FunctionalExpr) valu).getOperator().toString();
                                logger.info("The parameter is : " + parameter);
                                ValueExpr[] ve = ((FunctionalExpr) valu).getParameters();
                                for (ValueExpr v : ve) {
                                    if (v instanceof ID)
                                        guaranteedServiceName = ((ID) v).getValue();
                                }
                            }

                            DomainExpr de = tce.getDomain();
                            if (de instanceof SimpleDomainExpr) {
                                SimpleDomainExpr sde = (SimpleDomainExpr) de;
                                operator = sde.getComparisonOp().toString();
                                ValueExpr ve = sde.getValue();
                                if (ve instanceof CONST) {
                                    String value = ((CONST) ve).getValue();
                                    STND unitStd = ((CONST) ve).getDatatype();
                                    String unit = "";
                                    if (unitStd != null) {
                                        unit = unitStd.toString();
                                        if (unit.equals("percentage"))
                                            unit = "%";
                                        else if (unit.contains("anyURI"))
                                            unit = "";
                                    }
                                    String expression = parameter + " " + operator + " " + value + " " + unit;
                                    list.put(ParamNames.AGREEMENT_TERM_ID.toString(), agreementTermId);
                                    list
                                            .put(ParamNames.GUARANTEED_STATE_SERVICE_NAME.toString(),
                                                    guaranteedServiceName);
                                    list.put(ParamNames.GUARANTEED_STATE_EXPRESSION.toString(), expression);
                                    agreementTermCheck = true;
                                }
                            } // if SimpleDomainExpr
                        } // if TypeConstraintExpr
                    } // if Guaranteed.State
                    else if (g instanceof Guaranteed.Action) {
                        Guaranteed.Action ga = (Guaranteed.Action) g;
                        String gaId = ga.getId().toString();
                        if (gaId.contains(PENALTY)) {
                            String actor = ga.getActorRef().toString();
                            int index = actor.lastIndexOf(SEPARATOR);
                            actor = actor.substring(index + 1);

                            String policy = ga.getPolicy().toString();
                            String operatorGa = ga.getPrecondition().getOperator().toString();
                            String nameGa = "";
                            String condition = "";
                            Expr expr = ga.getPrecondition().getParameters()[0];
                            if (expr instanceof ValueExpr) {
                                ValueExpr ve = (ValueExpr) expr;
                                if (ve instanceof ID) {
                                    nameGa = ((ID) ve).getValue();
                                    // condition = nameGa + " " + operatorGa;
                                    condition = operatorGa;
                                }
                            }
                            Defn action = ga.getPostcondition();
                            if (action instanceof org.slasoi.slamodel.sla.business.Penalty) {
                                org.slasoi.slamodel.sla.business.Penalty penalty =
                                        (org.slasoi.slamodel.sla.business.Penalty) action;
                                String value = penalty.getPrice().getValue().toString();
                                String currency = penalty.getPrice().getDatatype().toString();
                                // list.put(ParamNames.AGREEMENT_TERM_ID.toString(), agreementTermId);
                                list.put(ParamNames.GUARANTEED_ACTION_ACTOR.toString(), actor);
                                list.put(ParamNames.GUARANTEED_ACTION_POLICY.toString(), policy);
                                list.put(ParamNames.GUARANTEED_ACTION_CONDITION.toString(), condition);
                                list.put(ParamNames.GUARANTEED_ACTION_PENALTY.toString(), value + " " + currency);
                                penaltyCheck = true;
                            }
                        }

                        else if (gaId.contains(TERMINATION)) {
                            Expr expr = ga.getPrecondition().getParameters()[0];
                            if (expr instanceof ConstraintExpr) {
                                ConstraintExpr ce = (ConstraintExpr) expr;
                                if (ce instanceof TypeConstraintExpr) {
                                    // TypeConstraintExpr
                                    TypeConstraintExpr tce = (TypeConstraintExpr) ce;

                                    DomainExpr de = tce.getDomain();
                                    if (de instanceof SimpleDomainExpr) {
                                        SimpleDomainExpr sde = (SimpleDomainExpr) de;
                                        operator = sde.getComparisonOp().toString();
                                        ValueExpr ve = sde.getValue();
                                        if (ve instanceof CONST) {
                                            String value = ((CONST) ve).getValue();
                                            list.put(ParamNames.GUARANTEED_ACTION_PENALTY_COUNT.toString(), value);
                                            penaltyCountCheck = true;
                                        }
                                    } // if SimpleDomainExpr
                                } // if TypeConstraintExpr
                            } // if ConstraintExpr
                            Defn action = ga.getPostcondition();
                            if (action instanceof Termination) {
                                Termination termination = (Termination) action;
                                String value = termination.getTerminationClauseId().toString();
                                list.put(ParamNames.GUARANTEED_ACTION_TERMINATION.toString(), value);
                                terminationCheck = true;
                            }
                        }

                    } // guaranteed
                } // loop in guaranteed

                if (agreementTermCheck && !penaltyCheck) {
                    list.put(ParamNames.GUARANTEED_ACTION_ACTOR.toString(), "-");
                    list.put(ParamNames.GUARANTEED_ACTION_POLICY.toString(), "-");
                    list.put(ParamNames.GUARANTEED_ACTION_CONDITION.toString(), "-");
                    list.put(ParamNames.GUARANTEED_ACTION_PENALTY.toString(), "-");
                }

                if (agreementTermCheck && !terminationCheck)
                    list.put(ParamNames.GUARANTEED_ACTION_TERMINATION.toString(), "-");

                if (agreementTermCheck && !penaltyCountCheck)
                    list.put(ParamNames.GUARANTEED_ACTION_PENALTY_COUNT.toString(), "-");

            } // !businessTerms
        }// loop in AgreementTerms

        return list;
    }

    public static List<String> getGuaranteeTermIds(SLATemplate sla) {
        List<String> gts = new ArrayList<String>();

        AgreementTerm[] terms = sla.getAgreementTerms();

        for (AgreementTerm term : terms) {
            // skip business terms
            if (!term.getId().toString().equals(BUSINESS_TERM1) && !term.getId().toString().equals(BUSINESS_TERM2)) {
                Guaranteed[] guaranteed = term.getGuarantees();

                for (Guaranteed g : guaranteed) {
                    if (g instanceof Guaranteed.State) {
                        String name = g.getId().toString();

                        if (!name.startsWith("VM_"))
                            gts.add(g.getId().toString());
                    }
                }
            }
        }

        return gts;
    }

    public static ParametersList loadBusinessTerms(ParametersList list, SLATemplate sla) throws NullPointerException, SAXException {
        // I've seen templates with businessTerms and others with BusinessTerms, so just try
        AgreementTerm bTerms = sla.getAgreementTerm(BUSINESS_TERM1);

        if (bTerms == null){
            bTerms = sla.getAgreementTerm(BUSINESS_TERM2);
        }
        
        Guaranteed[] guaranteed = bTerms.getGuarantees();

        for (Guaranteed g : guaranteed) {
            if (g instanceof Guaranteed.Action) {
                Guaranteed.Action ga = (Guaranteed.Action) g;
                String gaId = ga.getId().toString();
                if (gaId.startsWith(TERMINATION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof TerminationClause) {
                        TerminationClause termination = (TerminationClause) action;
                        list = getTerminationTerms(list, termination);
                    }
                }
                else if (gaId.startsWith(PRICE)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof ProductOfferingPrice) {
                        ProductOfferingPrice price = (ProductOfferingPrice) action;
                        // list = getPriceTerms(list, price); //we have taken this information from business database
                    }
                }
                else if (gaId.startsWith(REPORTING_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getReportingTerms(list, ca);
                    }
                }
                else if (gaId.startsWith(SUPPORT_PROCEDURES_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getSupportProceduresTerms(list, ca);
                    }
                }
                else if (gaId.startsWith(MONITORING_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getMonitoringTerms(list, ca);
                    }
                }
                else if (gaId.startsWith(SUPPORT_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getSupportTerms(list, ca);
                    }
                }
                else if (gaId.startsWith(UPDATE_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getUpdateTerms(list, ca);
                    }
                }
                else if (gaId.startsWith(BACKUP_ACTION)) {
                    Defn action = ga.getPostcondition();
                    if (action instanceof CustomAction) {
                        CustomAction ca = (CustomAction) action;
                        list = getBackupTerms(list, ca);
                    }
                }
            } // if Guaranteed.Action
        } // loop in Guarantees

        return list;

    }

    private static ParametersList getTerminationTerms(ParametersList list, TerminationClause termination) {
        String id = termination.getId().toString();
        STND initiator = termination.getInitiator();
        String type = termination.getType();
        String description = termination.getDescr();
        STND method = termination.getNotificationMethod();
        CONST notificationPeriod = termination.getNotificationPeriod();
        String notificationPeriodStr = null;

        CONST fees = termination.getFees();

        list.put(ParamNames.TERMINATION_CLAUSE_ID.toString(), id);
        list.put(ParamNames.TERMINATION_CLAUSE_DESCRIPTION.toString(), description);

        if (initiator != null)
            list.put(ParamNames.TERMINATION_CLAUSE_INITIATOR.toString(), initiator.toString());
        list.put(ParamNames.TERMINATION_CLAUSE_TYPE.toString(), type);

        if (notificationPeriod != null) {
            notificationPeriodStr = notificationPeriod.getValue() + " " + notificationPeriod.getDatatype().toString();
            list.put(ParamNames.TERMINATION_CLAUSE_NOTIFICATION_PERIOD.toString(), notificationPeriodStr);
        }

        if (method != null)
            list.put(ParamNames.TERMINATION_CLAUSE_NOTIFICATION_METHOD.toString(), method.toString());

        if (fees != null) {
            String feesStr = fees.getValue() + " " + fees.getDatatype().toString();
            list.put(ParamNames.TERMINATION_CLAUSE_FEES.toString(), feesStr);
        }
        return list;
    }

    private static ParametersList getPriceTerms(ParametersList list, ProductOfferingPrice price) {

        Product product = price.getProduct();

        if (product != null) {
            list.put(ParamNames.PRODUCT_NAME.toString(), product.getName());
            list.put(ParamNames.PRODUCT_DESCRIPTION.toString(), product.getDescr());
        }
        list.put(ParamNames.PRODUCT_OFFER_NAME.toString(), price.getName());
        list.put(ParamNames.PRODUCT_OFFER_DESCRIPTION.toString(), price.getDescr());

        STND billingFreq = price.getBillingFrequency();
        if (billingFreq != null)
            list.put(ParamNames.BILLING_FREQUENCY.toString(), billingFreq.toString());

        Format formatter = new SimpleDateFormat("yyyy.MM.dd");

        TIME validFrom = price.getValidFrom();
        if (validFrom != null)
            list.put(ParamNames.PRODUCT_OFFER_VALID_FROM.toString(), formatter.format(validFrom.getValue().getTime()));

        TIME validTo = price.getValidUntil();
        if (validTo != null)
            list.put(ParamNames.PRODUCT_OFFER_VALID_TO.toString(), formatter.format(validTo.getValue().getTime()));

        ComponentProductOfferingPrice[] components = price.getComponentProductOfferingPrices();
        for (ComponentProductOfferingPrice component : components) {
            list.put(ParamNames.PRICE_NAME.toString(), component.getId().toString());

            STND priceType = component.getPriceType();

            if (priceType != null)
                list.put(ParamNames.PRICE_TYPE.toString(), priceType.toString());
            list.put(ParamNames.PRICE.toString(), component.getPrice().getValue());
            list.put(ParamNames.PRICE_CURRENCY.toString(), component.getPrice().getDatatype().toString());

            CONST quantity = component.getQuantity();
            if (quantity != null)
                list.put(ParamNames.PRICE_QUANTITY.toString(), quantity.getValue());
        }

        return list;
    }

    private static ParametersList getReportingTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        String aux=null;
        List<ReportingTargetType> targets=null;
        List<ReceiverType> receivers=null;
        
        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            // skip the BusinessSchema part
            int beginIndex = value.indexOf(REPORTING_TAG);
            int endIndex = value.indexOf(REPORTING_TAG_END) + REPORTING_TAG_END.length();
            String reportingStr = value.substring(beginIndex, endIndex);

            //JAXB
            Reporting reporting=null;
            try {
                reporting = JAXBUtils.unmarshall(reportingStr, Reporting.class);
            }
            catch (JAXBException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            list.put(ParamNames.REPORT_DESCRIPTION.toString(), reporting.getDescription());
            list.put(ParamNames.REPORT_FORMAT.toString(), reporting.getFormat().name());
            list.put(ParamNames.REPORT_TYPE.toString(), reporting.getType().name());
            
            //Reporting Targets
            ReportingTargetsType targetType = reporting.getReportingTargets();
            
            if (targetType!=null && (targets=targetType.getReportingTarget())!=null){  
                for (ReportingTargetType target: targets){
                    if ((aux=target.getFunctionalAggregator())!=null) {
                        list.put(ParamNames.REPORT_TARGETS_TARGET_FUNCTIONAL_AGGREGATOR.name(), aux);
                    }
                    if ((aux=target.getSlaId())!=null) {
                        list.put(ParamNames.REPORT_TARGETS_TARGET_SLAID.name(), aux);    
                    }
                    if ((aux=target.getAgreementTermId())!=null) {
                        list.put(ParamNames.REPORT_TARGETS_TARGET_AGREEEMENTTERMID.name(), aux);    
                    }
                    if ((aux=target.getGuaranteedId())!=null) {
                        list.put(ParamNames.REPORT_TARGETS_TARGET_GUARANTEEDID.name(), aux);    
                    }                
                }
            }
            
            //Reporting Receivers
            ReceiversType receiverType = reporting.getReceivers();            
            if (receiverType!=null && (receivers=receiverType.getReceiver())!=null){  
                for (ReceiverType receiver: receivers){
                    if ((aux=receiver.getDeliveryEndPoint())!=null) {
                        list.put(ParamNames.REPORT_RECEIVER_DELIVERYENDPOINT.name(), aux);
                    }
                    if (receiver.getDeliveryMethod()!=null) {
                        list.put(ParamNames.REPORT_RECEIVER_DELIVERYMETHOD.name(), receiver.getDeliveryMethod().name());    
                    }
                    if (receiver.getReportFormat()!=null) {
                        list.put(ParamNames.REPORT_RECEIVER_FORMATTYPE.name(), receiver.getReportFormat().name());    
                    }                 
                }
            }
            
           //Reporting Creation Schedule
            
            ReportCreationScheduleType rcsType = reporting.getReportCreationSchedule();
            if (rcsType!=null){
                
                AutomaticScheduleType atsType = null;
                if ((atsType=rcsType.getAutomaticSchedule())!=null){
                    AperiodicScheduleType apsType=null;
                    if ((apsType=atsType.getAperiodicSchedule())!=null){
                        list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_APERIODIC_DATETIME.name(), apsType.getDateTime().toString());        
                    }else{
                        
                         PeriodicScheduleType psType = atsType.getPeriodicSchedule();
                         
                         switch(psType.getPeriodicity()){
                         case SECONDLY :{
                             RepeatSecondlyType type = psType.getRepeatSecondly();
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case MINUTELY :{
                             RepeatMinutelyType type = psType.getRepeatMinutely(); 

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case HOURLY :{
                             RepeatHourlyType type = psType.getRepeatHourly(); 

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case DAYLY :{
                             RepeatDailyType type = psType.getRepeatDaily(); 

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case WEEKDAY :{
                             RepeatWeekdayType type = psType.getRepeatWeekday();

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                                                          
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case WEEKEND :{
                             RepeatWeekendType type = psType.getRepeatWeekend();

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case WEEKLY :{
                             RepeatWeeklyType type = psType.getRepeatWeekly();

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case MONTHLY :{
                             RepeatMonthlyType type = psType.getRepeatMonthly();

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }                      
                         case YEARLY :{
                             RepeatYearlyType type = psType.getRepeatYearly(); 

                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }                        

                         }      
                    }
                }else{
                    //OnDemandSchedule is not Being used
                    rcsType.getOnDemandSchedule();
                }
            }
            
            //Reporting Delivery Schedule
            
            ReportDeliveryScheduleType rdsType = reporting.getReportDeliverySchedule();
            if (rdsType!=null){
                
                AutomaticScheduleType atsType = null;
                if ((atsType=rdsType.getAutomaticSchedule())!=null){
                    AperiodicScheduleType apsType=null;
                    if ((apsType=atsType.getAperiodicSchedule())!=null){
                        list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_APERIODIC_DATETIME.name(), apsType.getDateTime().toString());        
                    }else{
                        
                         PeriodicScheduleType psType = atsType.getPeriodicSchedule();
                         
                         switch(psType.getPeriodicity()){
                         case SECONDLY :{
                             RepeatSecondlyType type = psType.getRepeatSecondly();
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case MINUTELY :{
                             RepeatMinutelyType type = psType.getRepeatMinutely(); 

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case HOURLY :{
                             RepeatHourlyType type = psType.getRepeatHourly(); 

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case DAYLY :{
                             RepeatDailyType type = psType.getRepeatDaily(); 

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case WEEKDAY :{
                             RepeatWeekdayType type = psType.getRepeatWeekday();

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                                                          
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case WEEKEND :{
                             RepeatWeekendType type = psType.getRepeatWeekend();

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }
                         case WEEKLY :{
                             RepeatWeeklyType type = psType.getRepeatWeekly();

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             
                             break;
                         }
                         case MONTHLY :{
                             RepeatMonthlyType type = psType.getRepeatMonthly();

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }                      
                         case YEARLY :{
                             RepeatYearlyType type = psType.getRepeatYearly(); 

                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name(),psType.getPeriodicity().name());                              
                             list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_REPEATEVERY.name(),String.valueOf(type.getRepeatEvery()));
                             
                             if (type.getTimePeriod().getStartsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name(),
                                         String.valueOf(type.getTimePeriod().getStartsOn().toGregorianCalendar().getTime()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsAfterOccurrences()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_AFTEROCCURRENCES.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences()));
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsNever()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_NEVER.name(),
                                         type.getTimePeriod().getEnds().getEndsNever().toString());
                             }
                             
                             if (type.getTimePeriod().getEnds().getEndsOn()!=null){
                                 list.put(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name(),
                                         String.valueOf(type.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime()));
                             }
                             break;
                         }                        

                         }      
                    }
                }else{
                    //OnDemandSchedule is not Being used
                    rdsType.getOnDemandSchedule();
                }
            }
            
        }
        return list;
    }

    private static ParametersList getSupportProceduresTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            // skip the BusinessSchema part
            int beginIndex = value.indexOf(SUPPORT_PROCEDURES_TAG);
            int endIndex = value.indexOf(SUPPORT_PROCEDURES_TAG_END) + SUPPORT_PROCEDURES_TAG_END.length();
            String supportProceduresStr = value.substring(beginIndex, endIndex);

            //JAXB 
            SupportProcedures supportProcedures=null;
            try {
                supportProcedures = JAXBUtils.unmarshall(supportProceduresStr, SupportProcedures.class);
            }
            catch (JAXBException e) {
                logger.error("BusinessSLAParser# getSupportProceduresTerms# JAXBException# "+e);
            }
          
            list.put(ParamNames.SUPPORT_PROCEDURES_NOTIF_MEDIA.toString(), supportProcedures.getNotificationMedia());

            List<SeverityLevel> levels = supportProcedures.getSeverityLevels().getSeverityLevel();
            
            for (SeverityLevel level : levels) {
                list.put(ParamNames.SUPPORT_PROCEDURES_SEVERITY_LEVEL_NAME.toString(), level.getSeverityLevelName());
                list.put(ParamNames.SUPPORT_PROCEDURES_SEVERITY_LEVEL_DESCRIPTION.toString(), level
                        .getSeverityLevelDescription());

                DurationType response = level.getResponseTime();
                if (response != null){
                    list.put(ParamNames.SUPPORT_PROCEDURES_RESPONSE_TIME_UNIT.toString(), response.getUnit().value());
                    list.put(ParamNames.SUPPORT_PROCEDURES_RESPONSE_TIME_VALUE.toString(), String.valueOf(response.getValue()));
                }

                DurationType resol = level.getResolutionTime();
                if (resol != null){
                    list.put(ParamNames.SUPPORT_PROCEDURES_RESOLUTION_TIME_UNIT.name(), resol.getUnit().value());
                    list.put(ParamNames.SUPPORT_PROCEDURES_RESOLUTION_TIME_VALUE.name(), String.valueOf(resol.getValue()));
                }
            }
        }
        return list;
    }

    private static ParametersList getMonitoringTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            // skip the BusinessSchema part
            int beginIndex = value.indexOf(MONITORING_TAG);
            int endIndex = value.indexOf(MONITORING_TAG_END) + MONITORING_TAG_END.length();
            String monitoringStr = value.substring(beginIndex, endIndex);

            //JAXB 
            Monitoring monitoring=null;
            try {
                monitoring = JAXBUtils.unmarshall(monitoringStr, Monitoring.class);
            }
            catch (JAXBException e) {
                logger.error("BusinessSLAParser# getMonitoringTerms# JAXBException# "+e);
            }
            
            List<Parameter> parameters = monitoring.getParameter();
            for (Parameter param : parameters) {
                list.put(ParamNames.MONITORING_PARAMETER_NAME.toString(), param.getParameterName());
                list.put(ParamNames.MONITORING_PARAMETER_UNIT.toString(), param.getParameterUnit());

                //TODO check with JUnit
                DurationType collect = param.getDataCollectionInterval();
                if (collect != null){
                    list.put(ParamNames.MONITORING_DATA_COLLECTION_INTERVAL_UNIT.name(), collect.getUnit().value());
                    list.put(ParamNames.MONITORING_DATA_COLLECTION_INTERVAL_VALUE.name(), String.valueOf(collect.getValue()));
                }

                DurationType agg = param.getAggregationInterval();
                if (agg != null){
                    list.put(ParamNames.MONITORING_AGGREGATION_INTERVAL_UNIT.name(), agg.getUnit().value());
                    list.put(ParamNames.MONITORING_AGGREGATION_INTERVAL_VALUE.name(), String.valueOf(agg.getValue()));
                }
            }
        }
        return list;
    }

    private static ParametersList getSupportTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            // skip the BusinessSchema part
            int beginIndex = value.indexOf(SUPPORT_TAG);
            int endIndex = value.indexOf(SUPPORT_TAG_END) + SUPPORT_TAG_END.length();
            String supportStr = value.substring(beginIndex, endIndex);

            //JAXB 
            Support support =null;
            try {
                support = JAXBUtils.unmarshall(supportStr, Support.class);
            }
            catch (JAXBException e) {
                logger.error("BusinessSLAParser# getSupportTerms# JAXBException# "+e);
            }
            
                
            list.put(ParamNames.SUPPORT_PHONE_NUMBER.toString(), support.getPhoneNumber());

            List<AvailablePeriod> avalPeriods = support.getAvailablePeriod();
            
            for (AvailablePeriod aPeriod : avalPeriods) {
                if (aPeriod == null){
                    continue;
                }
                list.put(ParamNames.SUPPORT_DAY_WEEK.toString(), aPeriod.getDayOfWeek().name());
                List<SupportTimePeriodType> timePeriods = aPeriod.getTimePeriod();

                if (timePeriods == null){
                    continue;
                }
                //TODO check with Junit
                for (SupportTimePeriodType spTime : timePeriods) {
                    list.put(ParamNames.SUPPORT_INIT_TIME.toString(), spTime.getInitTime().toGregorianCalendar().getTime().toString());
                    list.put(ParamNames.SUPPORT_FINISH_TIME.toString(), spTime.getFinishTime().toGregorianCalendar().getTime().toString());
                }
            }            

        }
        return list;
    }

    private static ParametersList getUpdateTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            System.out.println("Update terms in slat:");
            System.out.println(value);

            // skip the BusinessSchema part
            int beginIndex = value.indexOf(UPDATE_TAG);
            int endIndex = value.indexOf(UPDATE_TAG_END) + UPDATE_TAG_END.length();
            String updateStr = value.substring(beginIndex, endIndex);
            
            //JAXB 
            UpdateProcess update=null;
            try {
                update = JAXBUtils.unmarshall(updateStr, UpdateProcess.class);
            }
            catch (JAXBException e) {
                logger.error("BusinessSLAParser# getUpdateTerms# JAXBException# "+e);
            }
            
            DurationType freq = update.getFrecuency();
            if (freq != null)
                list.put(ParamNames.UPDATE_FREQUENCY.toString(), freq.toString());
            list.put(ParamNames.UPDATE_INSTALLATION_DESCRIPTION.toString(), update.getInstalationDescription());
            list.put(ParamNames.UPDATE_DISTRIBUTION_METHOD.toString(), update.getDistributionMethod());

        }
        return list;
    }

    private static ParametersList getBackupTerms(ParametersList list, CustomAction ca) throws NullPointerException, SAXException {
        STND[] props = ca.getPropertyKeys();

        if (props != null && props.length > 0) {
            String value = ca.getPropertyValue(props[0]);
            // skip the BusinessSchema part
            int beginIndex = value.indexOf(BACKUP_TAG);
            int endIndex = value.indexOf(BACKUP_TAG_END) + BACKUP_TAG_END.length();
            String backupStr = value.substring(beginIndex, endIndex);

            //BackupRecoveryMechanism backup = BackupRecoveryMechanism.fromXML(backupStr);

            //JAXB 
            BackupRecoveryMechanism backup=null;
            try {
                backup = JAXBUtils.unmarshall(backupStr, BackupRecoveryMechanism.class);
            }
            catch (JAXBException e) {
                logger.error("BusinessSLAParser# getBackupTerms# JAXBException# "+e);
            }
            
            List<BackupMechanism> listBackup = backup.getBackupMechanism();

            for (BackupMechanism mechanism : listBackup) {
                list.put(ParamNames.BACKUP_DESCRIPTION.toString(), mechanism.getDescription());
                list.put(ParamNames.BACKUP_TYPE.toString(), mechanism.getType().name());

                DurationType periodicity = mechanism.getPeriodicity();
                if (periodicity != null) {
                    list.put(ParamNames.BACKUP_PERIODICITY.toString(), periodicity.toString());
                    list.put(ParamNames.BACKUP_PERIODICITY_PRECISION.toString(), new Float(periodicity.getPrecision())
                            .toString());

                }

                DurationType estimated = mechanism.getEstimatedTime();
                if (estimated != null) {
                    list.put(ParamNames.BACKUP_ESTIMATED_TIME.toString(), estimated.toString());
                    list.put(ParamNames.BACKUP_ESTIMATED_TIME_PRECISION.toString(), new Float(estimated.getPrecision())
                            .toString());
                }

            }

        }
        return list;
    }
}
