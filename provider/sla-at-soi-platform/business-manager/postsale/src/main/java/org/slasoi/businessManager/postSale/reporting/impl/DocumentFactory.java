package org.slasoi.businessManager.postSale.reporting.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

public final class DocumentFactory {
    
    private static Logger logger = Logger.getLogger(DocumentFactory.class);
    
    private DocumentFactory(){
        
    }

    public static Document newDocument(String fileName) throws FileNotFoundException, DocumentException{
        
        logger.info("DocumentFactory# Creating document...");
        
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fileName));
        document.open();
        
        return document;        
    }
    
    public static Document newDocument(String fileName,String tittle, String author) throws FileNotFoundException, DocumentException{
        
        logger.info("DocumentFactory# Creating document with title " + tittle);
        logger.info("\t Author = " + author);
        
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fileName));
        document.open();
        
        document.addTitle(tittle);
        document.addAuthor(author);
        
        return document;        
    }
    
    
}
