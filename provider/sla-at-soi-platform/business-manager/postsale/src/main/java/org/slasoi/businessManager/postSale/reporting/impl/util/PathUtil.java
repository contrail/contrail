package org.slasoi.businessManager.postSale.reporting.impl.util;

public final class PathUtil {

    public static final String FILE_SEPARATOR =  System.getProperty("file.separator");
    
    public static final String SLASOI_HOME = System.getenv("SLASOI_HOME");
    
    public static final String REPORTING_HOME = SLASOI_HOME + FILE_SEPARATOR +
            "BusinessManager"+FILE_SEPARATOR + 
                "postSale" +FILE_SEPARATOR + 
                    "reporting";
    
}
