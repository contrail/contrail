/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/product-sla/slat-composition/src/main/java/org/slasoi/businessManager/productSLA/SLATComposition/impl/SLATComposition.java $
 */

package org.slasoi.businessManager.productSLA.SLATComposition.impl;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;
import org.slasoi.businessManager.common.model.SLATComposeDetail;
import org.slasoi.businessManager.common.util.GTDifference;
import org.slasoi.businessManager.productSLA.SLATComposition.ISLATComposition;
import org.slasoi.sa.pss4slam.core.IPublishable;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.business.Penalty;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Termination;
import org.slasoi.slamodel.sla.business.TerminationClause;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.sla;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value="slatComposition")
public class SLATComposition  implements ISLATComposition {
    
    protected Logger log = Logger.getLogger(SLATComposition.class);    

    @Autowired
    private BusinessContextService businessContextService;
    
    
/**
   * This method mixes several templates into one.   
   * @param templates
   * @return
   */
    public SLATemplate composeSLAT(List<SLATemplate> templates){
        log.debug("Inside composeSLAT method");        
        SLATComposeDetail result = composeSLATDetails(templates);
        return result.getComposedTemplate();        
    }        
    
    /**
     * This method mixes several templates into one.   
     * @param templates
     * @return
     */
    public SLATComposeDetail composeSLATDetails(List<SLATemplate> templates){
        log.debug("Inside composeSLAT method");
        SLATComposeDetail result = new SLATComposeDetail();
        /**
         * The template is compose of:
         * ID getUuid()
         * String getModelVersion()
         * Party[] getParties()
         * Interface.Declaration[] getInterfaceDeclarations()
         * VariableDeclr[] getVariableDeclrs()
         * AgreementTerm[] getAgreementTerms()
         */
        
        List <GTDifference> termsToMerge = new ArrayList <GTDifference> ();
        //Get merging method.
        boolean mergeTerms = getMergingGT();
        
        if(templates!=null && templates.size()>0){
            log.debug("Templates to compose: "+templates.size());
            try{
                SLATemplate finalTemplate = new SLATemplate();
                InterfaceDeclr[] interfaceDeclr;
                InterfaceDeclr[] interfaceDeclr2;
                InterfaceDeclr[] interfaceDeclr3;
                VariableDeclr[] variableDeclr;
                VariableDeclr[] variableDeclr2;
                VariableDeclr[] variableDeclr3;
                ArrayList<AgreementTerm> terms = null;
                for(SLATemplate template:templates){
                    //set Agreement terms
                    log.debug("Compose Template: "+template.getUuid().getValue());
                    if(finalTemplate.getAgreementTerms()!= null && finalTemplate.getAgreementTerms().length>0 ){
                        terms = new ArrayList<AgreementTerm>( Arrays.asList(finalTemplate.getAgreementTerms()) );
                    }else{
                        terms = new ArrayList<AgreementTerm>();
                    }
                    ArrayList<AgreementTerm> agreementTerms =
                            new ArrayList<AgreementTerm>( Arrays.asList(template.getAgreementTerms()) );
                   if(agreementTerms !=null &&agreementTerms.size()>0){
                       //add the new terms
                       for(AgreementTerm term:agreementTerms){
                           if(!term.getId().getValue().trim().equalsIgnoreCase("businessTerm")){
                               ArrayList<Guaranteed> guarantees =
                                   new ArrayList<Guaranteed>( Arrays.asList(term.getGuarantees()) );
                               ArrayList<Guaranteed> finalGuarantees =
                                   new ArrayList<Guaranteed>();
                               //check the guarantees
                               for(Guaranteed guarantee:guarantees){
                                   if(guarantee instanceof State){
                                       ///check merge of guarantee
                                       if(mergeTerms){
                                           GTDifference diference = Utils.getStateInfo((State)guarantee);
                                           diference.setAgreementTermId(term.getId().getValue());
                                           diference.setTemplateId(template.getUuid().toString());
                                           if(diference!=null && diference.getParameter().equalsIgnoreCase(common.$availability)){
                                               //if it is a term to merge --> it will be add at the end
                                               log.debug("Found term to merge");
                                               termsToMerge.add(diference);
                                           }else{
                                              finalGuarantees.add(guarantee);  
                                           }
                                       }else{
                                           finalGuarantees.add(guarantee); 
                                       }
                                       
                                   }else{
                                       //check the action
                                       Action action = (Action) guarantee;
                                       if(!(action.getPostcondition() instanceof Termination)&&
                                          !(action.getPostcondition() instanceof ProductOfferingPrice)&&
                                          !(action.getPostcondition() instanceof TerminationClause)&&
                                          !(action.getPostcondition() instanceof Penalty)){
                                           //add the action if it is not a business Action
                                           finalGuarantees.add(guarantee); 
                                       }
                                       
                                   }                                   
                               }
                               if(finalGuarantees !=null && finalGuarantees.size()>0){
                                   term.setGuarantees((Guaranteed[])finalGuarantees.toArray(new Guaranteed[0]));
                                   terms.add(term);  
                               }                                                            
                           }
                       }                       
                   }
                   if(terms!= null && terms.size()>0 ){                    
                       finalTemplate.setAgreementTerms((AgreementTerm[])terms.toArray(new AgreementTerm[0]));
                   }
                    // //set getInterfaceDeclrs
                    if(finalTemplate.getInterfaceDeclrs()!= null && finalTemplate.getInterfaceDeclrs().length>0 ){                 
                        interfaceDeclr = template.getInterfaceDeclrs();
                        if(interfaceDeclr!=null&& interfaceDeclr.length>0){
                            interfaceDeclr2 = finalTemplate.getInterfaceDeclrs();
                            interfaceDeclr3 = new InterfaceDeclr[interfaceDeclr.length+interfaceDeclr2.length];
                            for(int i=0;i<interfaceDeclr2.length;i++){
                                interfaceDeclr3[i]=interfaceDeclr2[i];
                            }
                            int length = interfaceDeclr2.length;
                            for(int i=0;i<interfaceDeclr.length;i++){
                                interfaceDeclr3[length+i]=interfaceDeclr[i];
                            }
                            finalTemplate.setInterfaceDeclrs(interfaceDeclr3); 
                        }             
                    }else{
                        finalTemplate.setInterfaceDeclrs(template.getInterfaceDeclrs());                        
                    }                    
                    //set getVariableDeclrs
                    if(finalTemplate.getVariableDeclrs()!= null && finalTemplate.getVariableDeclrs().length>0 ){                    
                        variableDeclr = template.getVariableDeclrs();
                        if(variableDeclr!=null&& variableDeclr.length>0){
                            variableDeclr2 = finalTemplate.getVariableDeclrs();
                            variableDeclr3 = new  VariableDeclr[variableDeclr.length+variableDeclr2.length];
                            for(int i=0;i<variableDeclr2.length;i++){
                                variableDeclr3[i]=variableDeclr2[i];
                            }
                            int length = variableDeclr2.length;
                            for(int i=0;i<variableDeclr.length;i++){
                                variableDeclr3[length+i]=variableDeclr[i];
                            }
                            finalTemplate.setVariableDeclrs(variableDeclr3); 
                        }            
                    }else{
                        finalTemplate.setVariableDeclrs(template.getVariableDeclrs());      
                    }
                    //template.getParties();                    
                }                
                //if merge --> check and add the new terms
                if(mergeTerms){                           
                    if(termsToMerge != null && termsToMerge.size()>0){
                       log.debug("Found availability");
                       GTDifference dif = null;
                       if(termsToMerge.size()>1){
                           log.debug("There are term to merge");
                           SLATMerging merge = new SLATMerging();
                           dif = merge.mergeSLATs(termsToMerge);                           
                       }else{
                           log.debug("Only one term to merge");
                           dif = termsToMerge.get(0);
                       }  
                       AgreementTerm agreementTerm = Utils.generateAgreementTermFromGtDifference(dif);
                       //set the translated terms
                        for(GTDifference transTerm:termsToMerge){                            
                            EmGtTranslation translation = new EmGtTranslation();
                            translation.setTxAgreedId(transTerm.getGuaranteeId());
                            translation.setTxAgreementTermId(transTerm.getAgreementTermId());
                            EmGtTranslationId id = new EmGtTranslationId();
                            id.setTxMixAgreedId(dif.getGuaranteeId());
                            id.setTxSlatId(transTerm.getTemplateId());
                            id.setTxMixAgreementTermId(agreementTerm.getId().getValue());
                            translation.setId(id);
                            result.getGtTranslations().add(translation);
                        }
                       ////
                       terms.add(agreementTerm);
                       /////generate interface declaration
                       ResourceType inter = new ResourceType(dif.getServiceName());
                       Endpoint endPoint = new Endpoint(new ID(dif.getServiceName() + "URL" ), new UUID("http://"),sla.SOAP);
                       Endpoint[] endpoints = {endPoint};
                       InterfaceDeclr mergedService = new InterfaceDeclr(new ID(dif.getServiceName()), sla.provider, endpoints, inter);
                       
                       List <InterfaceDeclr> interfacesDecl = new ArrayList<InterfaceDeclr>( Arrays.asList(finalTemplate.getInterfaceDeclrs()) );
                       interfacesDecl.add(mergedService);
                       finalTemplate.setInterfaceDeclrs((InterfaceDeclr[])interfacesDecl.toArray(new InterfaceDeclr[0]));
                       //add terms to the list.
                       finalTemplate.setAgreementTerms((AgreementTerm[])terms.toArray(new AgreementTerm[0]));                    
                       
                    }
                }    
                result.setComposedTemplate(finalTemplate);                             
                return result;
            }catch (Exception e){
                log.error("Error: " +e.toString());
                e.printStackTrace();
                return null;
            }            
        }                
        return null;
    }
    /**
     * This method publish a template into the bus
     * @param template
     */
    public void publishSLAT(SLATemplate template){
        log.info("Inside publishSLAT method");  
        log.info("Publising Template:"+template.getUuid().getValue());
        try{           
            ServiceAdvertisement sa = businessContextService.getBusinessContext().getServiceAdvertisement();
            IPublishable iPublishable = sa.getIPublishable();
            Template<?>[] arrayAux = new Template<?>[] { new Template<SLATemplate>( template)};
            iPublishable.publish(arrayAux, ServiceAdvertisementChannel.SLAMODEL );
            log.info("After publish the template");
        }catch ( Exception e ){
            log.error("Error:"+e.toString());
            e.printStackTrace();
        }
        
    }
    
    /**
     * Get Merging method from property file   
     * @return
     */
      private boolean getMergingGT(){
          log.info("Into getMergingGT method"); 
          try{
              String propertiesFile= "provider.properties";
              String CONFIG_DIR =
                  System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "BusinessManager"
                   + System.getProperty("file.separator"); 
              Properties properties = new Properties();
              properties.load(new FileInputStream(CONFIG_DIR + propertiesFile));
              boolean value = new Boolean(properties.getProperty("mergingGT")).booleanValue(); 
              log.info("getMergingGT value: " +value);  
              return value;
          }catch(Exception e){
              log.error("Error: " +e.toString());
              e.printStackTrace();
              return false;           
          }
      }

}
