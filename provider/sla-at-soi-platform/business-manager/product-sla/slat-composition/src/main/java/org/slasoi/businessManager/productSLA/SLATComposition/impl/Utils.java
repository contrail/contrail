package org.slasoi.businessManager.productSLA.SLATComposition.impl;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.GTDifference;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.State;

public class Utils {
    private static final Logger log = Logger.getLogger(Utils.class); 
    
    
/**
 * Get State information
 * @param state
 * @return
 * @throws Exception
 */
  public  static GTDifference getStateInfo(State state ) throws Exception{      
        log.info("Inside getStateInfo method"); 
        GTDifference result = new GTDifference();
        try{   
            result.setGuaranteeId(state.getId().getValue());
            ConstraintExpr cons = state.getState();
            if(cons instanceof TypeConstraintExpr){   
               log.info("ConstraintExpr is TypeConstraintExpr");
               TypeConstraintExpr constraint = (TypeConstraintExpr)cons;
               STND Operator =((FunctionalExpr)constraint.getValue()).getOperator();
               log.info("Operator value:"+Operator.getValue());
               result.setParameter(Operator.getValue());
               ValueExpr[] parameters =((FunctionalExpr)constraint.getValue()).getParameters();
               if(parameters[0] instanceof ID){                              
                  ID idService = (ID)parameters[0];
                  log.info("Id Service:"+idService.getValue());
                  result.setServiceName(idService.getValue());
               }
               //Domain expression
               if(constraint.getDomain()instanceof SimpleDomainExpr){
                  SimpleDomainExpr domain = (SimpleDomainExpr)constraint.getDomain();
                  STND comparisonOp = domain.getComparisonOp();
                  log.info("comparison Operation:"+comparisonOp.getValue());
                  //index = comparisonOp.getValue().lastIndexOf("#")+1;
                  result.setOperationComparator(comparisonOp.getValue());//.substring(index));
                  if(domain.getValue() instanceof CONST){
                     log.info("Value is CONTS type");
                     CONST  data = (CONST)domain.getValue();
                     log.info("Parameter Value: "+data.getValue());
                     if(data.getDatatype()!=null){
                           log.info("Parameter Type: "+data.getDatatype().getValue());
                           //int index2 = data.getDatatype().getValue().lastIndexOf("#")+1;
                           //result.setGtParameterUnit(data.getDatatype().getValue().substring(index2));
                     }
                     try{
                         result.setValue(Double.parseDouble(data.getValue())); 
                     }catch(Exception e){
                        log.error(e.toString());
                     }                                                                            
                  }else if(domain.getValue() instanceof ID){
                                          /**/
                  }
                } 
             }                               
            }catch(Exception e){
                log.error(e.getMessage());
                e.printStackTrace();
                throw e;
            } 

            return result;
        }
 
  /**
  * Generate a new GTerm from the information merged from the other slat.  
  * @param gtInfo
  * @return
  */
  public static AgreementTerm generateAgreementTermFromGtDifference(GTDifference gtInfo){
      log.info("Inside generateAgreementTermFromGtDifference method");
      Guaranteed[] guarantees = new Guaranteed[1];
      ValueExpr[] params = new ValueExpr[1];
      params [0] = new ID (gtInfo.getServiceName());
      ValueExpr value = new FunctionalExpr(new STND(gtInfo.getParameter()),params);
      //Domain
      ValueExpr value2 = new CONST(""+gtInfo.getValue(), new STND("http://www.slaatsoi.org/coremodel/units#percentage"));
      DomainExpr domain = new  SimpleDomainExpr( value2, new STND(gtInfo.getOperationComparator()));
      ConstraintExpr state = new TypeConstraintExpr(value, domain);
      State state1 = new State(new ID(gtInfo.getGuaranteeId()),  state);
      guarantees[0] = state1; 
      //Generate the new Agreement term to Add to the list
      AgreementTerm term = new AgreementTerm(new ID(gtInfo.getGuaranteeId()),null,null,guarantees);           
      return term;
  }


}
