/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 494 $
 * @lastrevision   $Date: 2011-01-25 17:04:21 +0100 (Tue, 25 Jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/product-sla/product-discovery/src/main/java/org/slasoi/businessManager/productSLA/productDiscovery/ProductDiscovery.java $
 */

package org.slasoi.businessManager.productSLA.productDiscovery;
import java.util.List;

import org.ggf.schemas.graap.x2007.x03.wsAgreement.AgreementTemplateType;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.ws.types.Product;
import org.slasoi.businessManager.common.ws.types.ProductOffer;
import org.slasoi.businessManager.common.ws.types.Service;
import org.slasoi.slamodel.sla.SLATemplate;

public interface ProductDiscovery {

    /**
     *    
     * @param customerId
     * @param productId
     * @return
     */
       public List<AgreementTemplateType> getTemplates(long customerId,long productId);
       
       /**
        *    
        * @param customerId
        * @param productId
        * @return
        */
          public List<SLATemplate> getTemplatesSLAModel(long customerId,long productId);
          
          
          /**
           *    
           * @return
           */
          public SLATemplate[] getTemplates();
          
       /**
        * 
        * @param customerID
        * @param parameters
        * @return
        */
       public List<EmSpProducts> getProducts(String customerID,String [] parameters);

       /**
        * 
        * @param customerID
        * @param parameters
        * @return
        */
       public Product[] getWsProducts(String customerID,String [] parameters);
       
       
       /**
        * 
        * @param partyID
        * @return
        */
       public Service[] getWsPartyServices(Long partyID, String filter);
       
       /**
        * 
        * @param partyId
        * @param filter
        * @return
        */
       public Product[] getWsPartyProducts(Long partyId, String filter);
       /**
        * 
        * @param partyId
        * @param filter
        * @return
        */
       public ProductOffer[] getWsPartyProductOffers(Long partyId, String filter);       
}
