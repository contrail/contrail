/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/product-sla/product-discovery/src/test/java/org/slasoi/businessManager/productSLA/productDiscovery/_system_out_logger.java $
 */

package org.slasoi.businessManager.productSLA.productDiscovery;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Listener;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.units;

class SystemLoggerListener implements Listener{

    public void message(String msg, Calendar timestamp){
        System.out.println(x_CAL_TO_STR(timestamp)+msg);
    }
    
    public void templateRegistered(UUID uuid, Calendar timestamp){
        System.out.println(x_CAL_TO_STR(timestamp)+"registered SLAT : "+uuid.getValue());
    }
    
    public void registerTemplateFailed(SLATemplate slat, java.lang.Exception e, Calendar timestamp){
        System.err.println(x_CAL_TO_STR(timestamp)+"register SLAT failed : "+e.getMessage());
    }
    
    private String x_CAL_TO_STR(Calendar timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat(units.$date_format);
        return "REG @"+sdf.format(timestamp.getTime())+"\n    >> ";
    }

}