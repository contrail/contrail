/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 614 $
 * @lastrevision   $Date: 2011-02-04 09:12:18 +0100 (Fri, 04 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/product-sla/product-discovery/src/test/java/org/slasoi/businessManager/productSLA/productDiscovery/ProductsCatalogTest.java $
 */

package org.slasoi.businessManager.productSLA.productDiscovery;


import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slasoi.businessManager.common.model.EmServiceSpecification;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.ProductManager;
import org.slasoi.businessManager.productSLA.productDiscovery.impl.ProductDiscoveryImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for simple App.
 */
public class ProductsCatalogTest 
    extends TestCase
{


    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ProductsCatalogTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ProductsCatalogTest.class );
    }
    
	private static String[] files = new String[] {"classpath:META-INF/spring/bm-productdiscovery-context.xml"}; 
	final ApplicationContext context = new ClassPathXmlApplicationContext(files);
	final ProductManager productService = (ProductManager)context.getBean("productService"); 
   

    /**
     * Test get parameter list countries.
     */
    public void testGetProducts(){
    	try{ 
	    	System.out.println("ProductDiscovery, Test method getProducts");
	    	
	    	ProductDiscoveryImpl discoveryImpl = new ProductDiscoveryImpl();
	    		    	
	    	//Busca por cada uno de los filtros con or
	    	String[] filters = {"Mobile", "Audio", "Multimedia"};
	    	List<EmSpProducts> lProducts = discoveryImpl.getProducts("1", filters);
	    	for(EmSpProducts product : lProducts){
	    		System.out.println("Product: "+product.getTxProductname()+" - Product Offers: "+product.getEmSpProductsOffers().size());
	    		for(EmSpProductsOffer offer : product.getEmSpProductsOffers()){
	    			System.out.println("  - Offer: "+offer.getTxName()+" Num Services: "+offer.getEmSpServiceses().size());
	    			for(EmSpServices service : offer.getEmSpServiceses()){
	    				System.out.println("    - Service : "+service.getTxServicename()+" Num Categories: "+service.getEmServiceSpecifications().size());
	    				for(EmServiceSpecification sp : service.getEmServiceSpecifications())
	    					System.out.println("       - Category: "+sp.getTxName());
	    			}
	    		}
	    		System.out.println("****************************************************");
	    	}
	    		
	    	System.out.println("Response: " + lProducts.size());
	    	for(EmSpProducts product:lProducts){
	    		System.out.println(product.getTxProductname());
	    	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
 
}
