/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/product-sla/product-discovery/src/test/java/org/slasoi/businessManager/productSLA/productDiscovery/InsertData.java $
 */

package org.slasoi.businessManager.productSLA.productDiscovery;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.slasoi.businessManager.productSLA.productDiscovery.mockup.SLAManagerContextMockup;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet.Result;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.InterfaceRef;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;

import eu.slaatsoi.slamodel.SLATemplateDocument;

public class InsertData extends TestCase{

    static boolean trace = false;
    
    static String SLAM_ID = null;
    public void testLOAD_SLATS(){
        try{

            if (trace) System.out.println("\n------testLOAD_SLATS------------------------\n");
            
            //SLATemplate simple1 = Examples.p_SLAT_simple1();
            SLATemplateDocument slaTemplateXml =
                SLATemplateDocument.Factory.parse(InsertData.class
                        .getResourceAsStream("/ORC_Business_SLAT.xml"));  
            
                SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();                   
                SLATemplate simple1 = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());
                
            //SLATemplate simple2 = Examples.p_SLAT_simple2();
            //SLATemplate composite1 = Examples.p_SLAT_composite1();
             
            Metadata slam_provider_meta = Examples.p_METADATA_1();
            Metadata third_provider_meta = Examples.p_METADATA_2();
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl();
            reg.setSLAManagerContext(new SLAManagerContextMockup());            
            reg.addListener(new SystemLoggerListener());
            reg.addSLATemplate(simple1, third_provider_meta);
            
            //reg.addSLATemplate(simple2, third_provider_meta);
            //reg.addSLATemplate(composite1, slam_provider_meta);

            if (trace) System.out.println("------------------------------");
            // everything null -> all available templates
            ResultSet resultset = reg.query(null, null);
            if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
                    Examples.template_composite1_id,
                    Examples.template_simple1_id,
                    Examples.template_simple2_id
                })){ fail("Query 1"); }

            if (trace) System.out.println("------------------------------");
            // only third party providers
            SimpleDomainExpr SDE1 = new SimpleDomainExpr(new ID(Examples.$another_provider_id), core.equals);
            TypeConstraintExpr TCE1 = new TypeConstraintExpr(Metadata.provider_uuid, SDE1, null);
            resultset = reg.query(null, TCE1);
            if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
                    Examples.template_simple1_id,
                    Examples.template_simple2_id
                })){ fail("Query 2"); }
         
            reg.shutdown();
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    public void testQUERY_SLATS(){
        try{

            if (trace) System.out.println("\n------testQUERY_SLATS------------------------\n");
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl();
            reg.setSLAManagerContext(new SLAManagerContextMockup());
            reg.addListener(new SystemLoggerListener());

            if (trace) System.out.println("------------------------------");
            // everything null -> all available templates
            ResultSet resultset = reg.query(null, null);
            if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
                    Examples.template_composite1_id,
                    Examples.template_simple1_id,
                    Examples.template_simple2_id
                })){ fail("Query 1"); }

            if (trace) System.out.println("------------------------------");
            // only third party providers
            SimpleDomainExpr SDE1 = new SimpleDomainExpr(new ID(Examples.$another_provider_id), core.equals);
            TypeConstraintExpr TCE1 = new TypeConstraintExpr(Metadata.provider_uuid, SDE1, null);
            resultset = reg.query(null, TCE1);
            if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
                    Examples.template_simple1_id,
                    Examples.template_simple2_id
                })){ fail("Query 2"); }

            if (trace) System.out.println("------------------------------");
            // only third parties providing simple2 ..
            SLATemplate query = new SLATemplate();
            query.setInterfaceDeclrs(new InterfaceDeclr[]{
                new InterfaceDeclr(
                    new ID("x"), 
                    sla.provider, 
                    new InterfaceRef(new UUID(Examples.$i_simple2))
                )
            });
            resultset = reg.query(query, TCE1);
            if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
                    Examples.template_simple2_id
                })){ fail("Query 2"); }

            reg.shutdown();
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    private boolean x_TEST_Q_RESULTS(ResultSet resultset, UUID[] expected){
        List<UUID> uuids = new ArrayList<UUID>();
        if (trace && resultset.results.isEmpty()) System.out.println("NO RESULTS");
        for (Result r : resultset.results){
            uuids.add(r.getUuid());
            if (trace) System.out.println(r.toString());
        }
        if (uuids.size() != expected.length) return false;
        List<UUID> list = new ArrayList<UUID>();
        for (UUID uuid : uuids) list.add(uuid);
        for (UUID x : expected){
            if (list.contains(x)) list.remove(x);
            else return false;
        }
        return true;
    }

}