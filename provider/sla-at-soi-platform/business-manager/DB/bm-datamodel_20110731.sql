-- MySQL dump 10.13  Distrib 5.1.34, for apple-darwin9.5.0 (i386)
--
-- Host: slasoi.fbk.eu    Database: slasoi
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Current Database: `slasoi`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `slasoi` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `slasoi`;

--
-- Table structure for table `EM_ACCOUNT_EVENT`
--

DROP TABLE IF EXISTS `EM_ACCOUNT_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ACCOUNT_EVENT` (
  `NU_EVENTID` bigint(20) NOT NULL,
  `NU_EVENTID_PAYMENT` bigint(20) default NULL,
  `TC_TYPE` char(1) NOT NULL,
  `NU_PARTYID_VENDOR` bigint(20) default NULL,
  `NU_PARTYID_PAYER` bigint(20) NOT NULL default '0',
  `NU_BANK_ID` bigint(20) default NULL,
  `NU_EVENT_TYPEID` bigint(20) NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `TS_TIMESTAMP` datetime default NULL,
  `TX_SLAID` varchar(255) default NULL,
  `TX_VIOLATIONID` varchar(255) default NULL,
  `NU_AMOUNT` decimal(19,2) NOT NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  `NU_BANKID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_EVENTID`),
  KEY `FK74D60A711631AA4F` (`NU_EVENTID_PAYMENT`),
  KEY `FK74D60A711B473B2A` (`NU_BANK_ID`),
  KEY `FK74D60A71871FFC37` (`NU_EVENT_TYPEID`),
  KEY `FK74D60A71657FF173` (`NU_BANKID`),
  KEY `FK74D60A71E2C44514` (`NU_PARTYID_VENDOR`),
  KEY `FK74D60A717D154275` (`NU_PARTYID_PAYER`),
  KEY `FK74D60A71905E3410` (`NU_CURRENCY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ACCOUNT_EVENT`
--

LOCK TABLES `EM_ACCOUNT_EVENT` WRITE;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT` DISABLE KEYS */;
INSERT INTO `EM_ACCOUNT_EVENT` VALUES (96,98,'D',NULL,1,6,25,1,'2011-07-30 00:51:33',NULL,NULL,'1.00',NULL,6),(95,98,'D',NULL,1,1,23,1,'2011-07-30 00:51:29',NULL,NULL,'1.00',NULL,1),(94,98,'D',NULL,1,1,23,1,'2011-07-30 00:51:29',NULL,NULL,'1.00',NULL,1),(93,98,'D',NULL,1,7,22,1,'2011-07-30 00:51:15',NULL,NULL,'1.00',NULL,7),(92,98,'D',NULL,1,7,22,1,'2011-07-30 00:51:15',NULL,NULL,'1.00',NULL,7),(91,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 00:51:13',NULL,NULL,'10.00','Payment of the bill',NULL),(90,91,'D',NULL,1,6,21,1,'2011-07-30 00:51:13',NULL,NULL,'1.00',NULL,6),(55,61,'D',NULL,1,NULL,1,1,'2011-06-14 12:50:21','ORCSLA',NULL,'15.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(56,61,'D',NULL,1,NULL,1,1,'2011-06-20 12:50:21','ORCSLA',NULL,'15.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(57,61,'D',NULL,1,1,3,1,'2011-07-25 17:56:24',NULL,NULL,'1.00',NULL,1),(58,61,'D',NULL,1,1,3,1,'2011-07-25 17:56:24',NULL,NULL,'1.00',NULL,1),(59,61,'D',NULL,1,6,5,1,'2011-07-25 17:56:28',NULL,NULL,'1.00',NULL,6),(60,61,'D',NULL,1,6,5,1,'2011-07-25 17:56:28',NULL,NULL,'1.00',NULL,6),(61,NULL,'C',NULL,1,NULL,1,1,'2011-07-25 17:56:29',NULL,NULL,'10.00','Payment of the bill',NULL),(62,68,'D',NULL,1,7,6,1,'2011-07-25 17:56:32',NULL,NULL,'1.00',NULL,7),(63,68,'D',NULL,1,7,6,1,'2011-07-25 17:56:32',NULL,NULL,'1.00',NULL,7),(64,68,'D',NULL,1,1,7,1,'2011-07-25 18:26:26',NULL,NULL,'1.00',NULL,1),(65,68,'D',NULL,1,1,7,1,'2011-07-25 18:26:26',NULL,NULL,'1.00',NULL,1),(66,68,'D',NULL,1,6,9,1,'2011-07-25 18:26:32',NULL,NULL,'1.00',NULL,6),(67,68,'D',NULL,1,6,9,1,'2011-07-25 18:26:32',NULL,NULL,'1.00',NULL,6),(68,NULL,'C',NULL,1,NULL,1,1,'2011-07-25 18:26:33',NULL,NULL,'10.00','Payment of the bill',NULL),(69,78,'D',NULL,1,7,10,1,'2011-07-25 18:26:35',NULL,NULL,'1.00',NULL,7),(70,78,'D',NULL,1,7,10,1,'2011-07-25 18:26:35',NULL,NULL,'1.00',NULL,7),(71,78,'D',NULL,1,1,11,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,1),(72,78,'D',NULL,1,1,11,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,1),(74,78,'D',NULL,1,2,12,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,2),(75,78,'D',NULL,1,2,12,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,2),(76,78,'D',NULL,1,7,14,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,7),(77,78,'D',NULL,1,7,14,1,'2011-07-26 13:21:23',NULL,NULL,'1.00',NULL,7),(78,NULL,'C',NULL,1,NULL,1,1,'2011-07-26 13:21:23',NULL,NULL,'10.00','Payment of the bill',NULL),(79,86,'D',NULL,1,1,15,1,'2011-07-26 13:28:10',NULL,NULL,'1.00',NULL,1),(80,86,'D',NULL,1,1,15,1,'2011-07-26 13:28:10',NULL,NULL,'1.00',NULL,1),(82,86,'D',NULL,1,2,16,1,'2011-07-26 13:28:11',NULL,NULL,'1.00',NULL,2),(83,86,'D',NULL,1,2,16,1,'2011-07-26 13:28:11',NULL,NULL,'1.00',NULL,2),(84,86,'D',NULL,1,7,18,1,'2011-07-26 13:28:11',NULL,NULL,'1.00',NULL,7),(85,86,'D',NULL,1,7,18,1,'2011-07-26 13:28:11',NULL,NULL,'1.00',NULL,7),(86,NULL,'C',NULL,1,NULL,1,1,'2011-07-26 13:28:11',NULL,NULL,'10.00','Payment of the bill',NULL),(87,91,'D',NULL,1,1,19,1,'2011-07-30 00:51:09',NULL,NULL,'1.00',NULL,1),(88,91,'D',NULL,1,1,19,1,'2011-07-30 00:51:09',NULL,NULL,'1.00',NULL,1),(89,91,'D',NULL,1,6,21,1,'2011-07-30 00:51:13',NULL,NULL,'1.00',NULL,6),(45,61,'D',NULL,1,NULL,2,1,'2011-06-06 11:31:58','7b2fab1f-fc5c-4c3e-a2d0-b00ab9c3c0d6',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(46,61,'D',NULL,1,NULL,2,1,'2011-06-09 13:07:56','de18cd6b-8f8a-488b-a5db-2126e26cdcf2',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(47,61,'D',NULL,1,NULL,2,1,'2011-06-09 22:30:47','71c3b6d5-a012-445d-bfa8-c79791fff60d',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(48,61,'D',NULL,1,NULL,2,1,'2011-06-09 23:54:43','faad3af5-07d6-4900-b2eb-21ebc36eb4d9',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(49,61,'D',NULL,1,NULL,2,1,'2011-06-11 03:13:11','ab1ffcb9-aff3-429f-8c6a-395fc65b914e',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(50,61,'D',NULL,1,NULL,2,1,'2011-06-11 23:39:21','9cd80080-ec04-4163-85c6-f9330417d18b',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(51,61,'D',NULL,1,NULL,2,1,'2011-06-13 13:21:36','efa23851-c7fd-4c3d-8f55-a37f10b77c8e',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(52,61,'D',NULL,1,NULL,2,1,'2011-06-14 12:33:56','4aeac0ee-7f6c-4c11-a1fa-0153fb841249',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(53,61,'D',NULL,1,NULL,2,1,'2011-06-14 12:50:21','1e38c739-7a60-4340-a55a-3a44241487ec',NULL,'0.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(54,61,'D',NULL,1,NULL,2,1,'2011-06-14 12:33:56','ORCSLA',NULL,'10.00','1;Product Test 1;1;Product Offer Test 1;one_time_charge',NULL),(97,98,'D',NULL,1,6,25,1,'2011-07-30 00:51:33',NULL,NULL,'1.00',NULL,6),(98,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 00:51:34',NULL,NULL,'10.00','Payment of the bill',NULL),(99,105,'D',NULL,1,7,26,1,'2011-07-30 00:51:35',NULL,NULL,'1.00',NULL,7),(100,105,'D',NULL,1,7,26,1,'2011-07-30 00:51:35',NULL,NULL,'1.00',NULL,7),(101,105,'D',NULL,1,1,27,1,'2011-07-30 01:30:19',NULL,NULL,'1.00',NULL,1),(102,105,'D',NULL,1,1,27,1,'2011-07-30 01:30:19',NULL,NULL,'1.00',NULL,1),(103,105,'D',NULL,1,6,29,1,'2011-07-30 01:30:23',NULL,NULL,'1.00',NULL,6),(104,105,'D',NULL,1,6,29,1,'2011-07-30 01:30:23',NULL,NULL,'1.00',NULL,6),(105,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 01:30:23',NULL,NULL,'10.00','Payment of the bill',NULL),(106,112,'D',NULL,1,7,30,1,'2011-07-30 01:30:25',NULL,NULL,'1.00',NULL,7),(107,112,'D',NULL,1,7,30,1,'2011-07-30 01:30:25',NULL,NULL,'1.00',NULL,7),(108,112,'D',NULL,1,1,31,1,'2011-07-30 01:30:40',NULL,NULL,'1.00',NULL,1),(109,112,'D',NULL,1,1,31,1,'2011-07-30 01:30:40',NULL,NULL,'1.00',NULL,1),(110,112,'D',NULL,1,6,33,1,'2011-07-30 01:30:44',NULL,NULL,'1.00',NULL,6),(111,112,'D',NULL,1,6,33,1,'2011-07-30 01:30:44',NULL,NULL,'1.00',NULL,6),(112,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 01:30:45',NULL,NULL,'10.00','Payment of the bill',NULL),(113,119,'D',NULL,1,7,34,1,'2011-07-30 01:30:47',NULL,NULL,'1.00',NULL,7),(114,119,'D',NULL,1,7,34,1,'2011-07-30 01:30:47',NULL,NULL,'1.00',NULL,7),(115,119,'D',NULL,1,1,35,1,'2011-07-30 11:56:32',NULL,NULL,'1.00',NULL,1),(116,119,'D',NULL,1,1,35,1,'2011-07-30 11:56:32',NULL,NULL,'1.00',NULL,1),(117,119,'D',NULL,1,6,37,1,'2011-07-30 11:56:36',NULL,NULL,'1.00',NULL,6),(118,119,'D',NULL,1,6,37,1,'2011-07-30 11:56:36',NULL,NULL,'1.00',NULL,6),(119,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 11:56:37',NULL,NULL,'10.00','Payment of the bill',NULL),(120,126,'D',NULL,1,7,38,1,'2011-07-30 11:56:38',NULL,NULL,'1.00',NULL,7),(121,126,'D',NULL,1,7,38,1,'2011-07-30 11:56:38',NULL,NULL,'1.00',NULL,7),(122,126,'D',NULL,1,1,39,1,'2011-07-30 11:56:54',NULL,NULL,'1.00',NULL,1),(123,126,'D',NULL,1,1,39,1,'2011-07-30 11:56:54',NULL,NULL,'1.00',NULL,1),(124,126,'D',NULL,1,6,41,1,'2011-07-30 11:56:58',NULL,NULL,'1.00',NULL,6),(125,126,'D',NULL,1,6,41,1,'2011-07-30 11:56:58',NULL,NULL,'1.00',NULL,6),(126,NULL,'C',NULL,1,NULL,1,1,'2011-07-30 11:56:58',NULL,NULL,'10.00','Payment of the bill',NULL),(127,NULL,'D',NULL,1,7,42,1,'2011-07-30 11:57:00',NULL,NULL,'1.00',NULL,7),(128,NULL,'D',NULL,1,7,42,1,'2011-07-30 11:57:00',NULL,NULL,'1.00',NULL,7);
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_ACCOUNT_EVENT_TYPE`
--

DROP TABLE IF EXISTS `EM_ACCOUNT_EVENT_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ACCOUNT_EVENT_TYPE` (
  `NU_EVENT_TYPEID` bigint(20) NOT NULL,
  `TX_NAME` varchar(255) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  PRIMARY KEY  (`NU_EVENT_TYPEID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ACCOUNT_EVENT_TYPE`
--

LOCK TABLES `EM_ACCOUNT_EVENT_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT_TYPE` DISABLE KEYS */;
INSERT INTO `EM_ACCOUNT_EVENT_TYPE` VALUES (1,'PAYMENT','Payment Event'),(2,'SUBSCRIPTION','Event of Subscription'),(3,'name','txDescription'),(4,'name','txDescription'),(5,'name','txDescription'),(6,'name','txDescription agus'),(7,'name','txDescription'),(8,'name','txDescription'),(9,'name','txDescription'),(10,'name','txDescription agus'),(11,'name','txDescription agus'),(12,'name','txDescription'),(13,'name','txDescription'),(14,'name','txDescription'),(15,'name','txDescription agus'),(16,'name','txDescription'),(17,'name','txDescription'),(18,'name','txDescription'),(19,'name','txDescription'),(20,'name','txDescription'),(21,'name','txDescription'),(22,'name','txDescription agus'),(23,'name','txDescription'),(24,'name','txDescription'),(25,'name','txDescription'),(26,'name','txDescription agus'),(27,'name','txDescription'),(28,'name','txDescription'),(29,'name','txDescription'),(30,'name','txDescription agus'),(31,'name','txDescription'),(32,'name','txDescription'),(33,'name','txDescription'),(34,'name','txDescription agus'),(35,'name','txDescription'),(36,'name','txDescription'),(37,'name','txDescription'),(38,'name','txDescription agus'),(39,'name','txDescription'),(40,'name','txDescription'),(41,'name','txDescription'),(42,'name','txDescription agus');
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_AREAS_COUNTRIES`
--

DROP TABLE IF EXISTS `EM_AREAS_COUNTRIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_AREAS_COUNTRIES` (
  `NU_AREA_ID` bigint(20) NOT NULL default '0',
  `NU_COUNTRY` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_AREA_ID`,`NU_COUNTRY`),
  KEY `FK3ED5F564F3037D69` (`NU_COUNTRY`),
  KEY `FK3ED5F5641A968BF0` (`NU_AREA_ID`),
  CONSTRAINT `EM_AREAS_COUNTRIES_ibfk_1` FOREIGN KEY (`NU_AREA_ID`) REFERENCES `EM_GEOGRAPHICAL_AREAS` (`NU_AREA_ID`),
  CONSTRAINT `FK3ED5F564F3037D69` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_AREAS_COUNTRIES`
--

LOCK TABLES `EM_AREAS_COUNTRIES` WRITE;
/*!40000 ALTER TABLE `EM_AREAS_COUNTRIES` DISABLE KEYS */;
INSERT INTO `EM_AREAS_COUNTRIES` VALUES (2,1),(3,1),(1,2),(3,2),(3,3),(3,6),(3,7);
/*!40000 ALTER TABLE `EM_AREAS_COUNTRIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_ATTRIBUTES`
--

DROP TABLE IF EXISTS `EM_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ATTRIBUTES` (
  `NU_ATTRIBUTEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `NU_MIN_VALUE` bigint(20) default NULL,
  `NU_MAX_VALUE` bigint(20) default NULL,
  PRIMARY KEY  (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ATTRIBUTES`
--

LOCK TABLES `EM_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `EM_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `EM_ATTRIBUTES` VALUES (1,'Response Time',NULL,1,5),(2,'Fiability','',1,10),(3,'Quality','',1,5),(4,'SERVICE_NAME','',1,1);
/*!40000 ALTER TABLE `EM_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_BILLING_FRECUENCY`
--

DROP TABLE IF EXISTS `EM_BILLING_FRECUENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_BILLING_FRECUENCY` (
  `NU_BILLING_FRECUENCY_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_NAME` varchar(15) default NULL,
  PRIMARY KEY  (`NU_BILLING_FRECUENCY_ID`),
  UNIQUE KEY `NU_BILLING_FRECUENCY_ID` (`NU_BILLING_FRECUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_BILLING_FRECUENCY`
--

LOCK TABLES `EM_BILLING_FRECUENCY` WRITE;
/*!40000 ALTER TABLE `EM_BILLING_FRECUENCY` DISABLE KEYS */;
INSERT INTO `EM_BILLING_FRECUENCY` VALUES (1,'Month Billing','per_month'),(3,'Once every request','per_request'),(4,'Once a year','per_year'),(6,'Daily','per_day');
/*!40000 ALTER TABLE `EM_BILLING_FRECUENCY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CHARACTERISTIC`
--

DROP TABLE IF EXISTS `EM_CHARACTERISTIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CHARACTERISTIC` (
  `NU_CHARACTERISTIC_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `NU_CHARACT_TYPEID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_CHARACTERISTIC_ID`),
  KEY `NU_CHARACT_TYPEID` (`NU_CHARACT_TYPEID`),
  CONSTRAINT `EM_CHARACTERISTIC_ibfk_1` FOREIGN KEY (`NU_CHARACT_TYPEID`) REFERENCES `EM_CHARACTERISTIC_TYPE` (`NU_CHARACT_TYPEID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CHARACTERISTIC`
--

LOCK TABLES `EM_CHARACTERISTIC` WRITE;
/*!40000 ALTER TABLE `EM_CHARACTERISTIC` DISABLE KEYS */;
INSERT INTO `EM_CHARACTERISTIC` VALUES (1,'ENDPOINT',NULL,1),(2,'RESPONSE TIME','RESPONSE TIME DESCRIPTION',2),(3,'SERVICE NAME QP','',2),(4,'SERVICE NAME UP','',3),(5,'SERVICE NAME','',1);
/*!40000 ALTER TABLE `EM_CHARACTERISTIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CHARACTERISTIC_TYPE`
--

DROP TABLE IF EXISTS `EM_CHARACTERISTIC_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CHARACTERISTIC_TYPE` (
  `NU_CHARACT_TYPEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  PRIMARY KEY  (`NU_CHARACT_TYPEID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CHARACTERISTIC_TYPE`
--

LOCK TABLES `EM_CHARACTERISTIC_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_CHARACTERISTIC_TYPE` DISABLE KEYS */;
INSERT INTO `EM_CHARACTERISTIC_TYPE` VALUES (1,'URL'),(2,'QUALITY PARAMETER'),(3,'USAGE PARAMETER');
/*!40000 ALTER TABLE `EM_CHARACTERISTIC_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_COMPONENT_PRICE`
--

DROP TABLE IF EXISTS `EM_COMPONENT_PRICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_COMPONENT_PRICE` (
  `NU_IDCOMPONENTPRICE` bigint(20) NOT NULL auto_increment,
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_PRICE` decimal(12,4) NOT NULL default '0.0000',
  `NU_QUANTITY` bigint(20) NOT NULL,
  `DT_VALID_FROM` date default NULL,
  `DT_VALID_TO` date default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `NU_IDPRICE_TYPE` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDCOMPONENTPRICE`),
  KEY `FK508C7DB0F67450FD` (`NU_IDPRICE_TYPE`),
  KEY `FK508C7DB0E72C5B49` (`NU_CURRENCY_ID`),
  KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `EM_COMPONENT_PRICE_ibfk_1` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK508C7DB0E72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `FK508C7DB0F67450FD` FOREIGN KEY (`NU_IDPRICE_TYPE`) REFERENCES `EM_PRICE_TYPE` (`NU_IDPRICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_COMPONENT_PRICE`
--

LOCK TABLES `EM_COMPONENT_PRICE` WRITE;
/*!40000 ALTER TABLE `EM_COMPONENT_PRICE` DISABLE KEYS */;
INSERT INTO `EM_COMPONENT_PRICE` VALUES (103,1,'5.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1),(104,2,'3.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1),(105,3,'8.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1);
/*!40000 ALTER TABLE `EM_COMPONENT_PRICE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_COUNTRIES`
--

DROP TABLE IF EXISTS `EM_COUNTRIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_COUNTRIES` (
  `NU_COUNTRY` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) default NULL,
  `NU_CURRENCY_ID` bigint(20) default NULL,
  `NU_LANGUAGE` bigint(20) default NULL,
  PRIMARY KEY  (`NU_COUNTRY`),
  UNIQUE KEY `NU_COUNTRY` (`NU_COUNTRY`),
  KEY `FK79A3FBBDC174AC82` (`NU_LANGUAGE`),
  KEY `FK79A3FBBDE72C5B49` (`NU_CURRENCY_ID`),
  CONSTRAINT `FK79A3FBBDC174AC82` FOREIGN KEY (`NU_LANGUAGE`) REFERENCES `EM_LANGUAGES` (`NU_LANGUAGE`),
  CONSTRAINT `FK79A3FBBDE72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_COUNTRIES`
--

LOCK TABLES `EM_COUNTRIES` WRITE;
/*!40000 ALTER TABLE `EM_COUNTRIES` DISABLE KEYS */;
INSERT INTO `EM_COUNTRIES` VALUES (1,'Spain',1,1),(2,'United Kingdom',2,2),(3,'France',1,3),(4,'Japan',5,6),(6,'Italy',1,5),(7,'Belgium',1,9),(8,'Austria',1,16);
/*!40000 ALTER TABLE `EM_COUNTRIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CURRENCIES`
--

DROP TABLE IF EXISTS `EM_CURRENCIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CURRENCIES` (
  `NU_CURRENCY_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_CURRENCY_ID`),
  UNIQUE KEY `NU_CURRENCY_ID` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CURRENCIES`
--

LOCK TABLES `EM_CURRENCIES` WRITE;
/*!40000 ALTER TABLE `EM_CURRENCIES` DISABLE KEYS */;
INSERT INTO `EM_CURRENCIES` VALUES (1,'Euro','EUR'),(2,'United Kingdom, Pounds','GBP'),(3,'United States of America, Dollars','USD'),(5,'Japan, Yen','JPY'),(11,'United Arab Emirates, Dirhams','AED'),(12,'Afghanistan, Afghanis','AFN'),(13,'Albania, Leke','ALL'),(14,'Armenia, Drams','AMD'),(15,'Netherlands Antilles, Guilders (also called Florins)','ANG'),(16,'Angola, Kwanza','AOA'),(17,'Argentina, Pesos','ARS'),(18,'Australia, Dollars','AUD'),(19,'Aruba, Guilders (also called Florins)','AWG'),(20,'Azerbaijan, New Manats','AZN'),(21,'Bosnia and Herzegovina, Convertible Marka','BAM'),(22,'Barbados, Dollars','BBD'),(23,'Bangladesh, Taka','BDT'),(24,'Bulgaria, Leva','BGN'),(25,'Bahrain, Dinars','BHD'),(26,'Burundi, Francs','BIF'),(27,'Bermuda, Dollars','BMD'),(28,'Brunei Darussalam, Dollars','BND'),(29,'Bolivia, Bolivianos','BOB'),(30,'Brazil, Brazil Real','BRL'),(31,'Bahamas, Dollars','BSD'),(32,'Bhutan, Ngultrum','BTN'),(33,'Botswana, Pulas','BWP'),(34,'Belarus, Rubles','BYR'),(35,'Belize, Dollars','BZD'),(36,'Canada, Dollars','CAD'),(37,'Congo/Kinshasa, Congolese Francs','CDF'),(38,'Switzerland, Francs','CHF'),(39,'Chile, Pesos','CLP'),(40,'China, Yuan Renminbi','CNY'),(41,'Colombia, Pesos','COP'),(42,'Costa Rica, Colones','CRC'),(43,'Cuba, Pesos','CUP'),(44,'Cape Verde, Escudos','CVE'),(45,'Cyprus, Pounds (expires 2008-Jan-31)','CYP'),(46,'Czech Republic, Koruny','CZK'),(47,'Djibouti, Francs','DJF'),(48,'Denmark, Kroner','DKK'),(49,'Dominican Republic, Pesos','DOP'),(50,'Algeria, Algeria Dinars','DZD'),(51,'Estonia, Krooni','EEK'),(52,'Egypt, Pounds','EGP'),(53,'Eritrea, Nakfa','ERN'),(54,'Ethiopia, Birr','ETB'),(55,'Fiji, Dollars','FJD'),(56,'Falkland Islands (Malvinas), Pounds','FKP'),(57,'Georgia, Lari','GEL'),(58,'Guernsey, Pounds','GGP'),(59,'Ghana, Cedis','GHS'),(60,'Gibraltar, Pounds','GIP'),(61,'Gambia, Dalasi','GMD'),(62,'Guinea, Francs','GNF'),(63,'Guatemala, Quetzales','GTQ'),(64,'Guyana, Dollars','GYD'),(65,'Hong Kong, Dollars','HKD'),(66,'Honduras, Lempiras','HNL'),(67,'Croatia, Kuna','HRK'),(68,'Haiti, Gourdes','HTG'),(69,'Hungary, Forint','HUF'),(70,'Indonesia, Rupiahs','IDR'),(71,'Israel, New Shekels','ILS'),(72,'Isle of Man, Pounds','IMP'),(73,'India, Rupees','INR'),(74,'Iraq, Dinars','IQD'),(75,'Iran, Rials','IRR'),(76,'Iceland, Kronur','ISK'),(77,'Jersey, Pounds','JEP'),(78,'Jamaica, Dollars','JMD'),(79,'Jordan, Dinars','JOD'),(80,'Kenya, Shillings','KES'),(81,'Kyrgyzstan, Soms','KGS'),(82,'Cambodia, Riels','KHR'),(83,'Comoros, Francs','KMF'),(84,'Korea (North), Won','KPW'),(85,'Korea (South), Won','KRW'),(86,'Kuwait, Dinars','KWD'),(87,'Cayman Islands, Dollars','KYD'),(88,'Kazakhstan, Tenge','KZT'),(89,'Laos, Kips','LAK'),(90,'Lebanon, Pounds','LBP'),(91,'Sri Lanka, Rupees','LKR'),(92,'Liberia, Dollars','LRD'),(93,'Lesotho, Maloti','LSL'),(94,'Lithuania, Litai','LTL'),(95,'Latvia, Lati','LVL'),(96,'Libya, Dinars','LYD'),(97,'Morocco, Dirhams','MAD'),(98,'Moldova, Lei','MDL'),(99,'Madagascar, Ariary','MGA'),(100,'Macedonia, Denars','MKD'),(101,'Myanmar (Burma), Kyats','MMK'),(102,'Mongolia, Tugriks','MNT'),(103,'Macau, Patacas','MOP'),(104,'Mauritania, Ouguiyas','MRO'),(105,'Malta, Liri (expires 2008-Jan-31)','MTL'),(106,'Mauritius, Rupees','MUR'),(107,'Maldives (Maldive Islands), Rufiyaa','MVR'),(108,'Malawi, Kwachas','MWK'),(109,'Mexico, Pesos','MXN'),(110,'Malaysia, Ringgits','MYR'),(111,'Mozambique, Meticais','MZN'),(112,'Namibia, Dollars','NAD'),(113,'Nigeria, Nairas','NGN'),(114,'Nicaragua, Cordobas','NIO'),(115,'Norway, Krone','NOK'),(116,'Nepal, Nepal Rupees','NPR'),(117,'New Zealand, Dollars','NZD'),(118,'Oman, Rials','OMR'),(119,'Panama, Balboa','PAB'),(120,'Peru, Nuevos Soles','PEN'),(121,'Papua New Guinea, Kina','PGK'),(122,'Philippines, Pesos','PHP'),(123,'Pakistan, Rupees','PKR'),(124,'Poland, Zlotych','PLN'),(125,'Paraguay, Guarani','PYG'),(126,'Qatar, Rials','QAR'),(127,'Romania, New Lei','RON'),(128,'Serbia, Dinars','RSD'),(129,'Russia, Rubles','RUB'),(130,'Rwanda, Rwanda Francs','RWF'),(131,'Saudi Arabia, Riyals','SAR'),(132,'Solomon Islands, Dollars','SBD'),(133,'Seychelles, Rupees','SCR'),(134,'Sudan, Pounds','SDG'),(135,'Sweden, Kronor','SEK'),(136,'Singapore, Dollars','SGD'),(137,'Saint Helena, Pounds','SHP'),(138,'Sierra Leone, Leones','SLL'),(139,'Somalia, Shillings','SOS'),(140,'Seborga, Luigini','SPL'),(141,'Suriname, Dollars','SRD'),(142,'São Tome and Principe, Dobras','STD'),(143,'El Salvador, Colones','SVC'),(144,'Syria, Pounds','SYP'),(145,'Swaziland, Emalangeni','SZL'),(146,'Thailand, Baht','THB'),(147,'Tajikistan, Somoni','TJS'),(148,'Turkmenistan, Manats','TMM'),(149,'Tunisia, Dinars','TND'),(150,'Tonga, Pa\'anga','TOP'),(151,'Turkey, New Lira','TRY'),(152,'Trinidad and Tobago, Dollars','TTD'),(153,'Tuvalu, Tuvalu Dollars','TVD'),(154,'Taiwan, New Dollars','TWD'),(155,'Tanzania, Shillings','TZS'),(156,'Ukraine, Hryvnia','UAH'),(157,'Uganda, Shillings','UGX'),(158,'Uruguay, Pesos','UYU'),(159,'Uzbekistan, Sums','UZS'),(160,'Venezuela, Bolivares (expires 2008-Jun-30)','VEB'),(161,'Venezuela, Bolivares Fuertes','VEF'),(162,'Viet Nam, Dong','VND'),(163,'Vanuatu, Vatu','VUV'),(164,'Samoa, Tala','WST'),(165,'Communauté Financière Africaine BEAC, Francs','XAF'),(166,'Silver, Ounces','XAG'),(167,'Gold, Ounces','XAU'),(168,'East Caribbean Dollars','XCD'),(169,'International Monetary Fund (IMF) Special Drawing Rights','XDR'),(170,'Communauté Financière Africaine BCEAO, Francs','XOF'),(171,'Palladium Ounces','XPD'),(172,'Comptoirs Français du Pacifique Francs','XPF'),(173,'Platinum, Ounces','XPT'),(174,'Yemen, Rials','YER'),(175,'South Africa, Rand','ZAR'),(176,'Zambia, Kwacha','ZMK'),(177,'Zimbabwe, Zimbabwe Dollars','ZWD');
/*!40000 ALTER TABLE `EM_CURRENCIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CURRENCY_CHANGES`
--

DROP TABLE IF EXISTS `EM_CURRENCY_CHANGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CURRENCY_CHANGES` (
  `DT_FROM` datetime NOT NULL,
  `DT_TO` datetime NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `NU_CURRENCY_ID_DEST` bigint(20) NOT NULL,
  `NU_CONVERSION_FACTOR` bigint(20) NOT NULL,
  PRIMARY KEY  (`DT_FROM`,`DT_TO`,`NU_CURRENCY_ID`,`NU_CURRENCY_ID_DEST`),
  KEY `FKEADA932C95F97A48` (`NU_CURRENCY_ID_DEST`),
  KEY `FKEADA932CE72C5B49` (`NU_CURRENCY_ID`),
  CONSTRAINT `FKEADA932C95F97A48` FOREIGN KEY (`NU_CURRENCY_ID_DEST`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `FKEADA932CE72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CURRENCY_CHANGES`
--

LOCK TABLES `EM_CURRENCY_CHANGES` WRITE;
/*!40000 ALTER TABLE `EM_CURRENCY_CHANGES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CURRENCY_CHANGES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CUSTOMERS_PRODUCTS`
--

DROP TABLE IF EXISTS `EM_CUSTOMERS_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CUSTOMERS_PRODUCTS` (
  `TX_BSLAID` varchar(100) NOT NULL default '',
  `DT_DATE_BEGIN` datetime NOT NULL default '0000-00-00 00:00:00',
  `DT_DATE_END` datetime default NULL,
  `DT_INSERT_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `NU_PARTYID` bigint(20) default NULL,
  `NU_IDPRODUCTOFFERING` bigint(20) default NULL,
  `STATE` varchar(10) NOT NULL default '',
  `TERMINATION_REASON` varchar(40) default NULL,
  PRIMARY KEY  USING BTREE (`TX_BSLAID`),
  UNIQUE KEY `NU_PROVISIONID` USING BTREE (`TX_BSLAID`),
  KEY `FK9D043F25B47E1006` (`NU_PARTYID`),
  KEY `FK9D043F252E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK9D043F252E546354` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK9D043F25B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CUSTOMERS_PRODUCTS`
--

LOCK TABLES `EM_CUSTOMERS_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `EM_CUSTOMERS_PRODUCTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CUSTOMERS_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CUSTPRODS_SLAS`
--

DROP TABLE IF EXISTS `EM_CUSTPRODS_SLAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CUSTPRODS_SLAS` (
  `TX_BSLAID` varchar(100) NOT NULL default '',
  `TX_SLAID` varchar(100) NOT NULL default '',
  `NU_SERVICEID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`TX_BSLAID`,`TX_SLAID`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_CUSTPRODS_SLAS_ibfk_1` FOREIGN KEY (`TX_BSLAID`) REFERENCES `EM_CUSTOMERS_PRODUCTS` (`TX_BSLAID`),
  CONSTRAINT `EM_CUSTPRODS_SLAS_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CUSTPRODS_SLAS`
--

LOCK TABLES `EM_CUSTPRODS_SLAS` WRITE;
/*!40000 ALTER TABLE `EM_CUSTPRODS_SLAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CUSTPRODS_SLAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_GEOGRAPHICAL_AREAS`
--

DROP TABLE IF EXISTS `EM_GEOGRAPHICAL_AREAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_GEOGRAPHICAL_AREAS` (
  `NU_AREA_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_AREA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_GEOGRAPHICAL_AREAS`
--

LOCK TABLES `EM_GEOGRAPHICAL_AREAS` WRITE;
/*!40000 ALTER TABLE `EM_GEOGRAPHICAL_AREAS` DISABLE KEYS */;
INSERT INTO `EM_GEOGRAPHICAL_AREAS` VALUES (1,'UK'),(2,'SP'),(3,'Euro Zone');
/*!40000 ALTER TABLE `EM_GEOGRAPHICAL_AREAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_GT_TRANSLATION`
--

DROP TABLE IF EXISTS `EM_GT_TRANSLATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_GT_TRANSLATION` (
  `TX_BSLATID` varchar(255) NOT NULL default '',
  `TX_SLATID` varchar(255) NOT NULL default '',
  `TX_AGREEDID` varchar(255) NOT NULL default '',
  `TX_MIX_AGREEDID` varchar(255) NOT NULL default '',
  `TX_AGREEMENT_TERMID` varchar(255) NOT NULL default '',
  `TX_MIX_AGREEMENT_TERMID` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`TX_BSLATID`,`TX_SLATID`,`TX_MIX_AGREEDID`,`TX_MIX_AGREEMENT_TERMID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_GT_TRANSLATION`
--

LOCK TABLES `EM_GT_TRANSLATION` WRITE;
/*!40000 ALTER TABLE `EM_GT_TRANSLATION` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_GT_TRANSLATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_INDIVIDUAL`
--

DROP TABLE IF EXISTS `EM_INDIVIDUAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_INDIVIDUAL` (
  `NU_INDIVIDUALID` bigint(20) NOT NULL auto_increment,
  `DT_REGISTRATION` date NOT NULL,
  `TX_ADDRESS` varchar(200) default NULL,
  `TX_EMAIL` varchar(100) NOT NULL,
  `TX_FAX` varchar(40) default NULL,
  `TX_FIRST_NAME` varchar(40) NOT NULL,
  `TX_JOBDEPARTMENT` varchar(100) default NULL,
  `TX_JOBTITLE` varchar(100) default NULL,
  `TX_LAST_NAME` varchar(60) default NULL,
  `TX_PHONE_NUMBER` varchar(15) default NULL,
  `NU_COUNTRY` bigint(20) NOT NULL,
  `NU_LANGUAGE` bigint(20) NOT NULL,
  `NU_ORGANIZATION_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_INDIVIDUALID`),
  UNIQUE KEY `NU_IND` (`NU_INDIVIDUALID`),
  KEY `FKF4C56290F3037D69` (`NU_COUNTRY`),
  KEY `FKF4C56290C174AC82` (`NU_LANGUAGE`),
  KEY `NU_ORGANIZATION_ID` (`NU_ORGANIZATION_ID`),
  CONSTRAINT `EM_INDIVIDUAL_ibfk_1` FOREIGN KEY (`NU_ORGANIZATION_ID`) REFERENCES `EM_ORGANIZATION` (`NU_ORGANIZATIONID`),
  CONSTRAINT `FKF4C56290C174AC82` FOREIGN KEY (`NU_LANGUAGE`) REFERENCES `EM_LANGUAGES` (`NU_LANGUAGE`),
  CONSTRAINT `FKF4C56290F3037D69` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_INDIVIDUAL`
--

LOCK TABLES `EM_INDIVIDUAL` WRITE;
/*!40000 ALTER TABLE `EM_INDIVIDUAL` DISABLE KEYS */;
INSERT INTO `EM_INDIVIDUAL` VALUES (38,'2011-02-09','C/Canarias, 32','raul.gonzalez@mail.es','','Raúl','','','Gonzalez','917575756',1,1,1),(46,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(47,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(48,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(49,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(50,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(51,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(52,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(53,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(54,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(55,'2011-04-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(56,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(57,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(58,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(59,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(60,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(61,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(62,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(63,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(64,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(65,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(66,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(67,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(68,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(69,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(70,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(71,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(72,'2011-04-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(73,'2011-04-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(74,'2011-04-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(75,'2011-05-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(76,'2011-05-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(77,'2011-05-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(78,'2011-05-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(79,'2011-05-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(80,'2011-05-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(81,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(82,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(83,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(84,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(85,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(86,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(87,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(88,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(89,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(90,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(91,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(92,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(93,'2011-05-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(94,'2011-05-06','Lj','p@x.si','1234','Primoz','r','prog.','Hadalin','1234',2,2,NULL),(95,'2011-05-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(96,'2011-05-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(97,'2011-05-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(98,'2011-05-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(99,'2011-05-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(100,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(101,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(102,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(103,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(104,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(105,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(106,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(107,'2011-05-10','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(108,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(109,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(110,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(111,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(112,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(113,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(114,'2011-05-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(115,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(116,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(117,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(118,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(119,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(120,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(121,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(122,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(123,'2011-05-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(124,'2011-05-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(125,'2011-05-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(126,'2011-05-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(127,'2011-05-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(128,'2011-05-16','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(129,'2011-05-16','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(130,'2011-05-16','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(131,'2011-05-17','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(132,'2011-05-17','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(133,'2011-05-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(134,'2011-05-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(135,'2011-05-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(136,'2011-05-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(137,'2011-05-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(138,'2011-05-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(139,'2011-05-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(140,'2011-05-24','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(141,'2011-05-24','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(142,'2011-05-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(143,'2011-05-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(144,'2011-05-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(145,'2011-05-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(146,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(147,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(148,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(149,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(150,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(151,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(152,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(153,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(154,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(155,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(156,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(157,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(158,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(159,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(160,'2011-05-31','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(161,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(162,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(163,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(164,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(165,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(166,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(167,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(168,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(169,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(170,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(171,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(172,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(173,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(174,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(175,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(176,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(177,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(178,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(179,'2011-06-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(180,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(181,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(182,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(183,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(184,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(185,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(186,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(187,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(188,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(189,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(190,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(191,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(192,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(193,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(194,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(195,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(196,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(197,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(198,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(199,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(200,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(201,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(202,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(203,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(204,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(205,'2011-06-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(206,'2011-06-06','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(207,'2011-06-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(208,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(209,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(210,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(211,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(212,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(213,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(214,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(215,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(216,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(217,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(218,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(219,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(220,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(221,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(222,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(223,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(224,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(225,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(226,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(227,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(228,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(229,'2011-06-09','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(230,'2011-06-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(231,'2011-06-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(232,'2011-06-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(233,'2011-06-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(234,'2011-06-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(235,'2011-06-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(236,'2011-06-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(237,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(238,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(239,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(240,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(241,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(242,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(243,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(244,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(245,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(246,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(247,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(248,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(249,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(250,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(251,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(252,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(253,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(254,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(255,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(256,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(257,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(258,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(259,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(260,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(261,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(262,'2011-06-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(263,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(264,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(265,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(266,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(267,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(268,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(269,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(270,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(271,'2011-06-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(272,'2011-06-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(273,'2011-06-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(274,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(275,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(276,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(277,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(278,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(279,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(280,'2011-06-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(281,'2011-06-23','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(282,'2011-06-23','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(283,'2011-06-23','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(284,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(285,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(286,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(287,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(288,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(289,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(290,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(291,'2011-06-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(292,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(293,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(294,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(295,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(296,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(297,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(298,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(299,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(300,'2011-06-28','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(301,'2011-06-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(302,'2011-06-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(303,'2011-06-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(304,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(305,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(306,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(307,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(308,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(309,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(310,'2011-06-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(311,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(312,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(313,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(314,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(315,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(316,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(317,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(318,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(319,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(320,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(321,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(322,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(323,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(324,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(325,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(326,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(327,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(328,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(329,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(330,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(331,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(332,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(333,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(334,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(335,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(336,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(337,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(338,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(339,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(340,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(341,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(342,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(343,'2011-07-01','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(344,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(345,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(346,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(347,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(348,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(349,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(350,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(351,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(352,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(353,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(354,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(355,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(356,'2011-07-02','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(357,'2011-07-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(358,'2011-07-03','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(359,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(360,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(361,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(362,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(363,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(364,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(365,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(366,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(367,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(368,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(369,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(370,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(371,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(372,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(373,'2011-07-04','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(374,'2011-07-05','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(375,'2011-07-05','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(376,'2011-07-05','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(377,'2011-07-05','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(378,'2011-07-05','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(379,'2011-07-06','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(380,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(381,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(382,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(383,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(384,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(385,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(386,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(387,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(388,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(389,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(390,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(391,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(392,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(393,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(394,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(395,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(396,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(397,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(398,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(399,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(400,'2011-07-07','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(401,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(402,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(403,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(404,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(405,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(406,'2011-07-08','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(407,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(408,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(409,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(410,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(411,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(412,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(413,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(414,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(415,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(416,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(417,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(418,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(419,'2011-07-11','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(420,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(421,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(422,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(423,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(424,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(425,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(426,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(427,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(428,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(429,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(430,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(431,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(432,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(433,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(434,'2011-07-12','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(435,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(436,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(437,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(438,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(439,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(440,'2011-07-13','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(441,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(442,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(443,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(444,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(445,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(446,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(447,'2011-07-14','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(448,'2011-07-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(449,'2011-07-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(450,'2011-07-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(451,'2011-07-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(452,'2011-07-15','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(453,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(454,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(455,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(456,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(457,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(458,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(459,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(460,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(461,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(462,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(463,'2011-07-18','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(464,'2011-07-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(465,'2011-07-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(466,'2011-07-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(467,'2011-07-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(468,'2011-07-19','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(469,'2011-07-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(470,'2011-07-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(471,'2011-07-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(472,'2011-07-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(473,'2011-07-20','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(474,'2011-07-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(475,'2011-07-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(476,'2011-07-21','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(477,'2011-07-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(478,'2011-07-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(479,'2011-07-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(480,'2011-07-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(481,'2011-07-22','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(482,'2011-07-25','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(483,'2011-07-25','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(484,'2011-07-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(485,'2011-07-26','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(486,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(487,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(488,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(489,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(490,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(491,'2011-07-27','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(492,'2011-07-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(493,'2011-07-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(494,'2011-07-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(495,'2011-07-29','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL),(496,'2011-07-30','3','3','0800','3First','test dept','tester','wLast','0900',3,2,NULL);
/*!40000 ALTER TABLE `EM_INDIVIDUAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_LANGUAGES`
--

DROP TABLE IF EXISTS `EM_LANGUAGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_LANGUAGES` (
  `NU_LANGUAGE` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) NOT NULL,
  PRIMARY KEY  (`NU_LANGUAGE`),
  UNIQUE KEY `NU_LANGUAGE` (`NU_LANGUAGE`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_LANGUAGES`
--

LOCK TABLES `EM_LANGUAGES` WRITE;
/*!40000 ALTER TABLE `EM_LANGUAGES` DISABLE KEYS */;
INSERT INTO `EM_LANGUAGES` VALUES (1,'Spanish'),(2,'English'),(3,'French'),(5,'Italian'),(6,'Japanese'),(7,'Portuguese'),(8,'Flemish'),(9,'German'),(10,'Slovenian'),(16,'Austrian');
/*!40000 ALTER TABLE `EM_LANGUAGES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_OFFER_RATING`
--

DROP TABLE IF EXISTS `EM_OFFER_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_OFFER_RATING`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `EM_OFFER_RATING` (
  `NU_IDPRODUCTOFFERING` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `EM_ORGANIZATION`
--

DROP TABLE IF EXISTS `EM_ORGANIZATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ORGANIZATION` (
  `NU_ORGANIZATIONID` bigint(20) NOT NULL auto_increment,
  `TX_TRADINGNAME` varchar(20) NOT NULL default '',
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_FISCALID` varchar(40) default NULL,
  `NU_COUNTRY` bigint(20) default NULL,
  PRIMARY KEY  (`NU_ORGANIZATIONID`),
  UNIQUE KEY `NU_ORGANIZATIONID` (`NU_ORGANIZATIONID`),
  KEY `NU_COUNTRY` (`NU_COUNTRY`),
  CONSTRAINT `EM_ORGANIZATION_ibfk_1` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ORGANIZATION`
--

LOCK TABLES `EM_ORGANIZATION` WRITE;
/*!40000 ALTER TABLE `EM_ORGANIZATION` DISABLE KEYS */;
INSERT INTO `EM_ORGANIZATION` VALUES (1,'TELEFONICA','Telefonica description','',7),(2,'IaaS Provider','IaaS Provider company','',1),(3,'Yellow Pages','Yellow PAges company','',1),(4,'BABYLON','BABYLON Translator','',1);
/*!40000 ALTER TABLE `EM_ORGANIZATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARAMS`
--

DROP TABLE IF EXISTS `EM_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARAMS` (
  `NU_PARAM_ID` bigint(20) NOT NULL auto_increment,
  `TX_PARAM_NAME` varchar(100) NOT NULL,
  `TX_PARAM_RULETYPE` varchar(4) default NULL,
  `ID_PARAM_TYPE` bigint(20) default NULL,
  `TX_PARAM_DESCRIPTION` varchar(100) default NULL,
  `TX_PARAM_VIEWNAME` varchar(100) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`),
  UNIQUE KEY `NU_PARAM_ID` (`NU_PARAM_ID`),
  KEY `FK62DA54FDC762B44E` (`ID_PARAM_TYPE`),
  CONSTRAINT `FK62DA54FDC762B44E` FOREIGN KEY (`ID_PARAM_TYPE`) REFERENCES `EM_PARAM_TYPE` (`ID_PARAM_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARAMS`
--

LOCK TABLES `EM_PARAMS` WRITE;
/*!40000 ALTER TABLE `EM_PARAMS` DISABLE KEYS */;
INSERT INTO `EM_PARAMS` VALUES (1,'DATE_BEGIN','WHEN',3,'Date of init of the promotion','Init Date'),(2,'DATE_END','WHEN',3,'Date of the end of the promotion','End Date'),(3,'NU_COUNTRY','WHEN',4,'Identification of the country the promotion applies to','Country'),(4,'CATEGORY','WHEN',2,'The category of the product applies to','Category'),(5,'AGE','WHEN',1,'The age of the person the promotion applied to','Age'),(6,'DISCOUNT','THEN',1,'Discount offer by the promotion','Discount'),(7,'PRICE_TYPE','WHEN',5,'Type of discount associated (subcription,monthly)','Price Type'),(8,'MODIF_TYPE','WHEN',6,'Modification Type (Increment, Decrement)','Modification Type'),(9,'PARAM_COMPONENT','WHEN',7,'Policy Parameter Component ','Parameter Component'),(10,'PARAM_VARIATION','WHEN',1,'Percentage Paremeter Variation','Parameter Variation'),(11,'VARIATION_TYPE','THEN',8,'Percentage Price Variation','Price Variation Type'),(12,'SOCIOECONOMIC LEVEL','WHEN',9,'Socioeconomic Level Customer','Socioeconomic Level'),(13,'EDUCATIONAL LEVEL','WHEN',9,'Educational Level Customer','Educational Level'),(14,'ESTIMATED AGE','WHEN',10,'Estimated Customer Age','Estimated Age'),(15,'SLSP','WHEN',11,'Service Level Specification Parameter','SLSP'),(16,'VARIATION_MIN','WHEN',1,'Minimum variation of SLSP','Minor Variation'),(17,'VARIATION_MAX','WHEN',1,'Maximum variation of SLSP','Maximum Variation'),(18,'PRICE_VARIATION','THEN',1,'Price Variation','Price Variation'),(20,'NEW_PRICE','THEN',1,'Adjustment price for changes in duration type of contract ','New Price'),(21,'DURATION_TYPE','WHEN',12,'Duration type of contract','Duration Type'),(22,'VALUE_MIN','WHEN',1,'Minimum value of parameter','Min Value'),(23,'VALUE_MAX','WHEN',1,'Maximum value of parameter','Max Value'),(25,'GUARANTEE_ID','WHEN',2,'Guarantee Term Id','Guarantee Id');
/*!40000 ALTER TABLE `EM_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARAM_TYPE`
--

DROP TABLE IF EXISTS `EM_PARAM_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARAM_TYPE` (
  `ID_PARAM_TYPE` bigint(20) NOT NULL auto_increment,
  `TX_PARAM_TYPE_NAME` varchar(30) NOT NULL,
  `TX_PARAM_TYPE_VIEW_NAME` varchar(250) default NULL,
  `TX_PARAM_VALUES` varchar(1000) default NULL,
  PRIMARY KEY  (`ID_PARAM_TYPE`),
  UNIQUE KEY `ID_PARAM_TYPE` (`ID_PARAM_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARAM_TYPE`
--

LOCK TABLES `EM_PARAM_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PARAM_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PARAM_TYPE` VALUES (1,'NUMBER','NUMBER',NULL),(2,'STRING','STRING',NULL),(3,'DATE','DATE',NULL),(4,'SELECT','COUNTRY','EM_COUNTRIES,NU_COUNTRY,TX_NAME'),(5,'SELECT','PRICE TYPE','EM_PRICE_TYPE,NU_IDPRICE_TYPE,TX_NAME'),(6,'LIST','MODIFICATION TYPE','Increment,Decrement'),(7,'LIST','PARAMETER COMPONENT','QualifayingCondition,CustomLevelPercentage,CustomLevelValue '),(8,'SELECT','PRICE VARIATION','EM_PRICE_VARIATION_TYPE,NU_ID_VARIATION_TYPE,TX_DESCRIPTION'),(9,'LIST','LEVEL','Hight,Medium,Low'),(10,'LIST','AGE CATEGORY','Adolescent,Young Adult,Adult,Elderly'),(11,'SELECT','SLSP TYPE','EM_SLSP,TX_SLSP_NAME,TX_SLSP_NAME'),(12,'LIST','DURATION_TYPE','month,weekend,day,hrs');
/*!40000 ALTER TABLE `EM_PARAM_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY`
--

DROP TABLE IF EXISTS `EM_PARTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY` (
  `NU_PARTYID` bigint(20) NOT NULL auto_increment,
  `TX_PARTY_IDENTITY_TYPE` varchar(1) NOT NULL default '',
  `NU_BALANCE` decimal(12,2) NOT NULL default '0.00',
  `NU_CREDITLIMIT` decimal(12,2) NOT NULL default '0.00',
  `NU_CURRENCY_ID` bigint(10) NOT NULL default '0',
  `NU_INDIVIDUALID` bigint(20) default NULL,
  `NU_ORGANIZATIONID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PARTYID`),
  UNIQUE KEY `NU_PARTYID` (`NU_PARTYID`),
  KEY `NU_CURRENCY_ID` (`NU_CURRENCY_ID`),
  KEY `NU_INDIVIDUALID` (`NU_INDIVIDUALID`),
  KEY `NU_ORGANIZATIONID` (`NU_ORGANIZATIONID`),
  CONSTRAINT `EM_PARTY_ibfk_1` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `EM_PARTY_ibfk_2` FOREIGN KEY (`NU_INDIVIDUALID`) REFERENCES `EM_INDIVIDUAL` (`NU_INDIVIDUALID`),
  CONSTRAINT `EM_PARTY_ibfk_3` FOREIGN KEY (`NU_ORGANIZATIONID`) REFERENCES `EM_ORGANIZATION` (`NU_ORGANIZATIONID`)
) ENGINE=InnoDB AUTO_INCREMENT=463 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY`
--

LOCK TABLES `EM_PARTY` WRITE;
/*!40000 ALTER TABLE `EM_PARTY` DISABLE KEYS */;
INSERT INTO `EM_PARTY` VALUES (1,'O','100.00','100.00',1,NULL,1),(2,'O','0.00','100.00',1,NULL,2),(3,'O','0.00','100.00',1,NULL,3),(4,'O','0.00','100.00',1,NULL,4),(12,'I','0.00','2500.00',1,46,NULL),(13,'I','0.00','2500.00',1,47,NULL),(14,'I','0.00','2500.00',1,48,NULL),(15,'I','0.00','0.00',1,49,NULL),(16,'I','0.00','0.00',1,50,NULL),(17,'I','0.00','2500.00',1,51,NULL),(18,'I','0.00','0.00',1,52,NULL),(19,'I','0.00','2500.00',1,53,NULL),(20,'I','0.00','0.00',1,54,NULL),(21,'I','0.00','2500.00',1,55,NULL),(22,'I','0.00','0.00',1,56,NULL),(23,'I','0.00','2500.00',1,57,NULL),(24,'I','0.00','2500.00',1,58,NULL),(25,'I','0.00','2500.00',1,59,NULL),(26,'I','0.00','2500.00',1,60,NULL),(27,'I','0.00','2500.00',1,61,NULL),(28,'I','0.00','2500.00',1,62,NULL),(29,'I','0.00','2500.00',1,63,NULL),(30,'I','0.00','2500.00',1,64,NULL),(31,'I','0.00','2500.00',1,65,NULL),(32,'I','0.00','2500.00',1,66,NULL),(33,'I','0.00','2500.00',1,67,NULL),(34,'I','0.00','2500.00',1,68,NULL),(35,'I','0.00','2500.00',1,69,NULL),(36,'I','0.00','2500.00',1,70,NULL),(37,'I','0.00','0.00',1,71,NULL),(38,'I','0.00','2500.00',1,72,NULL),(39,'I','0.00','2500.00',1,73,NULL),(40,'I','0.00','0.00',1,74,NULL),(41,'I','0.00','2500.00',1,75,NULL),(42,'I','0.00','2500.00',1,76,NULL),(43,'I','0.00','2500.00',1,77,NULL),(44,'I','0.00','2500.00',1,78,NULL),(45,'I','0.00','2500.00',1,79,NULL),(46,'I','0.00','2500.00',1,80,NULL),(47,'I','0.00','2500.00',1,81,NULL),(48,'I','0.00','2500.00',1,82,NULL),(49,'I','0.00','0.00',1,83,NULL),(50,'I','0.00','2500.00',1,84,NULL),(51,'I','0.00','2500.00',1,85,NULL),(52,'I','0.00','2500.00',1,86,NULL),(53,'I','0.00','2500.00',1,87,NULL),(54,'I','0.00','2500.00',1,88,NULL),(55,'I','0.00','2500.00',1,89,NULL),(56,'I','0.00','2500.00',1,90,NULL),(57,'I','0.00','2500.00',1,91,NULL),(58,'I','0.00','2500.00',1,92,NULL),(59,'I','0.00','2500.00',1,93,NULL),(60,'I','0.00','0.00',1,94,NULL),(61,'I','0.00','0.00',1,95,NULL),(62,'I','0.00','2500.00',1,96,NULL),(63,'I','0.00','2500.00',1,97,NULL),(64,'I','0.00','2500.00',1,98,NULL),(65,'I','0.00','2500.00',1,99,NULL),(66,'I','0.00','2500.00',1,100,NULL),(67,'I','0.00','2500.00',1,101,NULL),(68,'I','0.00','2500.00',1,102,NULL),(69,'I','0.00','2500.00',1,103,NULL),(70,'I','0.00','0.00',1,104,NULL),(71,'I','0.00','0.00',1,105,NULL),(72,'I','0.00','0.00',1,106,NULL),(73,'I','0.00','2500.00',1,107,NULL),(74,'I','0.00','0.00',1,108,NULL),(75,'I','0.00','2500.00',1,109,NULL),(76,'I','0.00','2500.00',1,110,NULL),(77,'I','0.00','2500.00',1,111,NULL),(78,'I','0.00','2500.00',1,112,NULL),(79,'I','0.00','2500.00',1,113,NULL),(80,'I','0.00','2500.00',1,114,NULL),(81,'I','0.00','2500.00',1,115,NULL),(82,'I','0.00','2500.00',1,116,NULL),(83,'I','0.00','2500.00',1,117,NULL),(84,'I','0.00','2500.00',1,118,NULL),(85,'I','0.00','2500.00',1,119,NULL),(86,'I','0.00','0.00',1,120,NULL),(87,'I','0.00','2500.00',1,121,NULL),(88,'I','0.00','2500.00',1,122,NULL),(89,'I','0.00','2500.00',1,123,NULL),(90,'I','0.00','0.00',1,124,NULL),(91,'I','0.00','0.00',1,125,NULL),(92,'I','0.00','0.00',1,126,NULL),(93,'I','0.00','2500.00',1,127,NULL),(94,'I','0.00','0.00',1,128,NULL),(95,'I','0.00','2500.00',1,129,NULL),(96,'I','0.00','2500.00',1,130,NULL),(97,'I','0.00','0.00',1,131,NULL),(98,'I','0.00','2500.00',1,132,NULL),(99,'I','0.00','2500.00',1,133,NULL),(100,'I','0.00','2500.00',1,134,NULL),(101,'I','0.00','2500.00',1,135,NULL),(102,'I','0.00','2500.00',1,136,NULL),(103,'I','0.00','0.00',1,137,NULL),(104,'I','0.00','2500.00',1,138,NULL),(105,'I','0.00','2500.00',1,139,NULL),(106,'I','0.00','2500.00',1,140,NULL),(107,'I','0.00','2500.00',1,141,NULL),(108,'I','0.00','2500.00',1,142,NULL),(109,'I','0.00','2500.00',1,143,NULL),(110,'I','0.00','0.00',1,144,NULL),(111,'I','0.00','2500.00',1,145,NULL),(112,'I','0.00','0.00',1,146,NULL),(113,'I','0.00','0.00',1,147,NULL),(114,'I','0.00','2500.00',1,148,NULL),(115,'I','0.00','2500.00',1,149,NULL),(116,'I','0.00','2500.00',1,150,NULL),(117,'I','0.00','2500.00',1,151,NULL),(118,'I','0.00','2500.00',1,152,NULL),(119,'I','0.00','2500.00',1,153,NULL),(120,'I','0.00','2500.00',1,154,NULL),(121,'I','0.00','2500.00',1,155,NULL),(122,'I','0.00','2500.00',1,156,NULL),(123,'I','0.00','2500.00',1,157,NULL),(124,'I','0.00','2500.00',1,158,NULL),(125,'I','0.00','0.00',1,159,NULL),(126,'I','0.00','2500.00',1,160,NULL),(127,'I','0.00','2500.00',1,161,NULL),(128,'I','0.00','0.00',1,162,NULL),(129,'I','0.00','2500.00',1,163,NULL),(130,'I','0.00','2500.00',1,164,NULL),(131,'I','0.00','2500.00',1,165,NULL),(132,'I','0.00','2500.00',1,166,NULL),(133,'I','0.00','2500.00',1,167,NULL),(134,'I','0.00','2500.00',1,168,NULL),(135,'I','0.00','0.00',1,169,NULL),(136,'I','0.00','2500.00',1,170,NULL),(137,'I','0.00','2500.00',1,171,NULL),(138,'I','0.00','2500.00',1,172,NULL),(139,'I','0.00','0.00',1,173,NULL),(140,'I','0.00','2500.00',1,174,NULL),(141,'I','0.00','2500.00',1,175,NULL),(142,'I','0.00','2500.00',1,176,NULL),(143,'I','0.00','2500.00',1,177,NULL),(144,'I','0.00','2500.00',1,178,NULL),(145,'I','0.00','2500.00',1,179,NULL),(146,'I','0.00','2500.00',1,180,NULL),(147,'I','0.00','2500.00',1,181,NULL),(148,'I','0.00','2500.00',1,182,NULL),(149,'I','0.00','2500.00',1,183,NULL),(150,'I','0.00','2500.00',1,184,NULL),(151,'I','0.00','2500.00',1,185,NULL),(152,'I','0.00','2500.00',1,186,NULL),(153,'I','0.00','2500.00',1,187,NULL),(154,'I','0.00','0.00',1,188,NULL),(155,'I','0.00','2500.00',1,189,NULL),(156,'I','0.00','2500.00',1,190,NULL),(157,'I','0.00','2500.00',1,191,NULL),(158,'I','0.00','2500.00',1,192,NULL),(159,'I','0.00','2500.00',1,193,NULL),(160,'I','0.00','2500.00',1,194,NULL),(161,'I','0.00','2500.00',1,195,NULL),(162,'I','0.00','2500.00',1,196,NULL),(163,'I','0.00','2500.00',1,197,NULL),(164,'I','0.00','2500.00',1,198,NULL),(165,'I','0.00','2500.00',1,199,NULL),(166,'I','0.00','2500.00',1,200,NULL),(167,'I','0.00','2500.00',1,201,NULL),(168,'I','0.00','0.00',1,202,NULL),(169,'I','0.00','2500.00',1,203,NULL),(170,'I','0.00','2500.00',1,204,NULL),(171,'I','0.00','2500.00',1,205,NULL),(172,'I','0.00','2500.00',1,206,NULL),(173,'I','0.00','2500.00',1,207,NULL),(174,'I','0.00','0.00',1,208,NULL),(175,'I','0.00','2500.00',1,209,NULL),(176,'I','0.00','2500.00',1,210,NULL),(177,'I','0.00','0.00',1,211,NULL),(178,'I','0.00','0.00',1,212,NULL),(179,'I','0.00','2500.00',1,213,NULL),(180,'I','0.00','0.00',1,214,NULL),(181,'I','0.00','2500.00',1,215,NULL),(182,'I','0.00','2500.00',1,216,NULL),(183,'I','0.00','2500.00',1,217,NULL),(184,'I','0.00','2500.00',1,218,NULL),(185,'I','0.00','2500.00',1,219,NULL),(186,'I','0.00','2500.00',1,220,NULL),(187,'I','0.00','2500.00',1,221,NULL),(188,'I','0.00','2500.00',1,222,NULL),(189,'I','0.00','2500.00',1,223,NULL),(190,'I','0.00','2500.00',1,224,NULL),(191,'I','0.00','2500.00',1,225,NULL),(192,'I','0.00','2500.00',1,226,NULL),(193,'I','0.00','2500.00',1,227,NULL),(194,'I','0.00','2500.00',1,228,NULL),(195,'I','0.00','2500.00',1,229,NULL),(196,'I','0.00','2500.00',1,230,NULL),(197,'I','0.00','2500.00',1,231,NULL),(198,'I','0.00','2500.00',1,232,NULL),(199,'I','0.00','0.00',1,233,NULL),(200,'I','0.00','2500.00',1,234,NULL),(201,'I','0.00','2500.00',1,235,NULL),(202,'I','0.00','2500.00',1,236,NULL),(203,'I','0.00','2500.00',1,237,NULL),(204,'I','0.00','2500.00',1,238,NULL),(205,'I','0.00','2500.00',1,239,NULL),(206,'I','0.00','2500.00',1,240,NULL),(207,'I','0.00','2500.00',1,241,NULL),(208,'I','0.00','2500.00',1,242,NULL),(209,'I','0.00','2500.00',1,243,NULL),(210,'I','0.00','2500.00',1,244,NULL),(211,'I','0.00','2500.00',1,245,NULL),(212,'I','0.00','2500.00',1,246,NULL),(213,'I','0.00','2500.00',1,247,NULL),(214,'I','0.00','2500.00',1,248,NULL),(215,'I','0.00','2500.00',1,249,NULL),(216,'I','0.00','2500.00',1,250,NULL),(217,'I','0.00','2500.00',1,251,NULL),(218,'I','0.00','2500.00',1,252,NULL),(219,'I','0.00','2500.00',1,253,NULL),(220,'I','0.00','2500.00',1,254,NULL),(221,'I','0.00','2500.00',1,255,NULL),(222,'I','0.00','2500.00',1,256,NULL),(223,'I','0.00','2500.00',1,257,NULL),(224,'I','0.00','2500.00',1,258,NULL),(225,'I','0.00','2500.00',1,259,NULL),(226,'I','0.00','2500.00',1,260,NULL),(227,'I','0.00','2500.00',1,261,NULL),(228,'I','0.00','2500.00',1,262,NULL),(229,'I','0.00','2500.00',1,263,NULL),(230,'I','0.00','2500.00',1,264,NULL),(231,'I','0.00','2500.00',1,265,NULL),(232,'I','0.00','2500.00',1,266,NULL),(233,'I','0.00','2500.00',1,267,NULL),(234,'I','0.00','2500.00',1,268,NULL),(235,'I','0.00','2500.00',1,269,NULL),(236,'I','0.00','2500.00',1,270,NULL),(237,'I','0.00','2500.00',1,271,NULL),(238,'I','0.00','0.00',1,272,NULL),(239,'I','0.00','2500.00',1,273,NULL),(240,'I','0.00','2500.00',1,274,NULL),(241,'I','0.00','2500.00',1,275,NULL),(242,'I','0.00','2500.00',1,276,NULL),(243,'I','0.00','0.00',1,277,NULL),(244,'I','0.00','2500.00',1,278,NULL),(245,'I','0.00','2500.00',1,279,NULL),(246,'I','0.00','2500.00',1,280,NULL),(247,'I','0.00','2500.00',1,281,NULL),(248,'I','0.00','2500.00',1,282,NULL),(249,'I','0.00','2500.00',1,283,NULL),(250,'I','0.00','2500.00',1,284,NULL),(251,'I','0.00','0.00',1,285,NULL),(252,'I','0.00','2500.00',1,286,NULL),(253,'I','0.00','2500.00',1,287,NULL),(254,'I','0.00','2500.00',1,288,NULL),(255,'I','0.00','2500.00',1,289,NULL),(256,'I','0.00','2500.00',1,290,NULL),(257,'I','0.00','2500.00',1,291,NULL),(258,'I','0.00','2500.00',1,292,NULL),(259,'I','0.00','0.00',1,293,NULL),(260,'I','0.00','0.00',1,294,NULL),(261,'I','0.00','2500.00',1,295,NULL),(262,'I','0.00','2500.00',1,296,NULL),(263,'I','0.00','0.00',1,297,NULL),(264,'I','0.00','0.00',1,298,NULL),(265,'I','0.00','2500.00',1,299,NULL),(266,'I','0.00','0.00',1,300,NULL),(267,'I','0.00','0.00',1,301,NULL),(268,'I','0.00','2500.00',1,302,NULL),(269,'I','0.00','2500.00',1,303,NULL),(270,'I','0.00','0.00',1,304,NULL),(271,'I','0.00','0.00',1,305,NULL),(272,'I','0.00','0.00',1,306,NULL),(273,'I','0.00','0.00',1,307,NULL),(274,'I','0.00','2500.00',1,308,NULL),(275,'I','0.00','0.00',1,309,NULL),(276,'I','0.00','2500.00',1,310,NULL),(277,'I','0.00','0.00',1,311,NULL),(278,'I','0.00','2500.00',1,312,NULL),(279,'I','0.00','2500.00',1,313,NULL),(280,'I','0.00','2500.00',1,314,NULL),(281,'I','0.00','2500.00',1,315,NULL),(282,'I','0.00','0.00',1,316,NULL),(283,'I','0.00','2500.00',1,317,NULL),(284,'I','0.00','2500.00',1,318,NULL),(285,'I','0.00','2500.00',1,319,NULL),(286,'I','0.00','2500.00',1,320,NULL),(287,'I','0.00','2500.00',1,321,NULL),(288,'I','0.00','0.00',1,322,NULL),(289,'I','0.00','2500.00',1,323,NULL),(290,'I','0.00','2500.00',1,324,NULL),(291,'I','0.00','2500.00',1,325,NULL),(292,'I','0.00','2500.00',1,326,NULL),(293,'I','0.00','2500.00',1,327,NULL),(294,'I','0.00','2500.00',1,328,NULL),(295,'I','0.00','0.00',1,329,NULL),(296,'I','0.00','2500.00',1,330,NULL),(297,'I','0.00','0.00',1,331,NULL),(298,'I','0.00','2500.00',1,332,NULL),(299,'I','0.00','0.00',1,333,NULL),(300,'I','0.00','2500.00',1,334,NULL),(301,'I','0.00','2500.00',1,335,NULL),(302,'I','0.00','2500.00',1,336,NULL),(303,'I','0.00','2500.00',1,337,NULL),(304,'I','0.00','2500.00',1,338,NULL),(305,'I','0.00','2500.00',1,339,NULL),(306,'I','0.00','2500.00',1,340,NULL),(307,'I','0.00','2500.00',1,341,NULL),(308,'I','0.00','2500.00',1,342,NULL),(309,'I','0.00','2500.00',1,343,NULL),(310,'I','0.00','2500.00',1,344,NULL),(311,'I','0.00','2500.00',1,345,NULL),(312,'I','0.00','2500.00',1,346,NULL),(313,'I','0.00','2500.00',1,347,NULL),(314,'I','0.00','2500.00',1,348,NULL),(315,'I','0.00','2500.00',1,349,NULL),(316,'I','0.00','2500.00',1,350,NULL),(317,'I','0.00','2500.00',1,351,NULL),(318,'I','0.00','2500.00',1,352,NULL),(319,'I','0.00','2500.00',1,353,NULL),(320,'I','0.00','2500.00',1,354,NULL),(321,'I','0.00','2500.00',1,355,NULL),(322,'I','0.00','2500.00',1,356,NULL),(323,'I','0.00','2500.00',1,357,NULL),(324,'I','0.00','2500.00',1,358,NULL),(325,'I','0.00','2500.00',1,359,NULL),(326,'I','0.00','0.00',1,360,NULL),(327,'I','0.00','2500.00',1,361,NULL),(328,'I','0.00','2500.00',1,362,NULL),(329,'I','0.00','0.00',1,363,NULL),(330,'I','0.00','2500.00',1,364,NULL),(331,'I','0.00','2500.00',1,365,NULL),(332,'I','0.00','2500.00',1,366,NULL),(333,'I','0.00','2500.00',1,367,NULL),(334,'I','0.00','2500.00',1,368,NULL),(335,'I','0.00','2500.00',1,369,NULL),(336,'I','0.00','2500.00',1,370,NULL),(337,'I','0.00','2500.00',1,371,NULL),(338,'I','0.00','2500.00',1,372,NULL),(339,'I','0.00','2500.00',1,373,NULL),(340,'I','0.00','2500.00',1,374,NULL),(341,'I','0.00','2500.00',1,375,NULL),(342,'I','0.00','2500.00',1,376,NULL),(343,'I','0.00','2500.00',1,377,NULL),(344,'I','0.00','2500.00',1,378,NULL),(345,'I','0.00','2500.00',1,379,NULL),(346,'I','0.00','2500.00',1,380,NULL),(347,'I','0.00','2500.00',1,381,NULL),(348,'I','0.00','2500.00',1,382,NULL),(349,'I','0.00','0.00',1,383,NULL),(350,'I','0.00','2500.00',1,384,NULL),(351,'I','0.00','2500.00',1,385,NULL),(352,'I','0.00','2500.00',1,386,NULL),(353,'I','0.00','2500.00',1,387,NULL),(354,'I','0.00','2500.00',1,388,NULL),(355,'I','0.00','2500.00',1,389,NULL),(356,'I','0.00','2500.00',1,390,NULL),(357,'I','0.00','2500.00',1,391,NULL),(358,'I','0.00','2500.00',1,392,NULL),(359,'I','0.00','2500.00',1,393,NULL),(360,'I','0.00','2500.00',1,394,NULL),(361,'I','0.00','2500.00',1,395,NULL),(362,'I','0.00','2500.00',1,396,NULL),(363,'I','0.00','0.00',1,397,NULL),(364,'I','0.00','2500.00',1,398,NULL),(365,'I','0.00','2500.00',1,399,NULL),(366,'I','0.00','2500.00',1,400,NULL),(367,'I','0.00','2500.00',1,401,NULL),(368,'I','0.00','2500.00',1,402,NULL),(369,'I','0.00','2500.00',1,403,NULL),(370,'I','0.00','2500.00',1,404,NULL),(371,'I','0.00','2500.00',1,405,NULL),(372,'I','0.00','0.00',1,406,NULL),(373,'I','0.00','0.00',1,407,NULL),(374,'I','0.00','2500.00',1,408,NULL),(375,'I','0.00','2500.00',1,409,NULL),(376,'I','0.00','2500.00',1,410,NULL),(377,'I','0.00','2500.00',1,411,NULL),(378,'I','0.00','2500.00',1,412,NULL),(379,'I','0.00','2500.00',1,413,NULL),(380,'I','0.00','2500.00',1,414,NULL),(381,'I','0.00','2500.00',1,415,NULL),(382,'I','0.00','2500.00',1,416,NULL),(383,'I','0.00','2500.00',1,417,NULL),(384,'I','0.00','2500.00',1,418,NULL),(385,'I','0.00','2500.00',1,419,NULL),(386,'I','0.00','2500.00',1,420,NULL),(387,'I','0.00','2500.00',1,421,NULL),(388,'I','0.00','2500.00',1,422,NULL),(389,'I','0.00','2500.00',1,423,NULL),(390,'I','0.00','0.00',1,424,NULL),(391,'I','0.00','0.00',1,425,NULL),(392,'I','0.00','0.00',1,426,NULL),(393,'I','0.00','2500.00',1,427,NULL),(394,'I','0.00','2500.00',1,428,NULL),(395,'I','0.00','2500.00',1,429,NULL),(396,'I','0.00','2500.00',1,430,NULL),(397,'I','0.00','2500.00',1,431,NULL),(398,'I','0.00','2500.00',1,432,NULL),(399,'I','0.00','2500.00',1,433,NULL),(400,'I','0.00','0.00',1,434,NULL),(401,'I','0.00','2500.00',1,435,NULL),(402,'I','0.00','2500.00',1,436,NULL),(403,'I','0.00','2500.00',1,437,NULL),(404,'I','0.00','2500.00',1,438,NULL),(405,'I','0.00','2500.00',1,439,NULL),(406,'I','0.00','2500.00',1,440,NULL),(407,'I','0.00','2500.00',1,441,NULL),(408,'I','0.00','2500.00',1,442,NULL),(409,'I','0.00','0.00',1,443,NULL),(410,'I','0.00','2500.00',1,444,NULL),(411,'I','0.00','2500.00',1,445,NULL),(412,'I','0.00','2500.00',1,446,NULL),(413,'I','0.00','2500.00',1,447,NULL),(414,'I','0.00','2500.00',1,448,NULL),(415,'I','0.00','2500.00',1,449,NULL),(416,'I','0.00','2500.00',1,450,NULL),(417,'I','0.00','2500.00',1,451,NULL),(418,'I','0.00','2500.00',1,452,NULL),(419,'I','0.00','2500.00',1,453,NULL),(420,'I','0.00','2500.00',1,454,NULL),(421,'I','0.00','2500.00',1,455,NULL),(422,'I','0.00','2500.00',1,456,NULL),(423,'I','0.00','2500.00',1,457,NULL),(424,'I','0.00','2500.00',1,458,NULL),(425,'I','0.00','2500.00',1,459,NULL),(426,'I','0.00','2500.00',1,460,NULL),(427,'I','0.00','2500.00',1,461,NULL),(428,'I','0.00','2500.00',1,462,NULL),(429,'I','0.00','2500.00',1,463,NULL),(430,'I','0.00','0.00',1,464,NULL),(431,'I','0.00','0.00',1,465,NULL),(432,'I','0.00','0.00',1,466,NULL),(433,'I','0.00','2500.00',1,467,NULL),(434,'I','0.00','2500.00',1,468,NULL),(435,'I','0.00','2500.00',1,469,NULL),(436,'I','0.00','2500.00',1,470,NULL),(437,'I','0.00','2500.00',1,471,NULL),(438,'I','0.00','2500.00',1,472,NULL),(439,'I','0.00','2500.00',1,473,NULL),(440,'I','0.00','2500.00',1,474,NULL),(441,'I','0.00','2500.00',1,475,NULL),(442,'I','0.00','2500.00',1,476,NULL),(443,'I','0.00','2500.00',1,477,NULL),(444,'I','0.00','2500.00',1,478,NULL),(445,'I','0.00','2500.00',1,479,NULL),(446,'I','0.00','2500.00',1,480,NULL),(447,'I','0.00','2500.00',1,481,NULL),(448,'I','0.00','2500.00',1,482,NULL),(449,'I','0.00','2500.00',1,483,NULL),(450,'I','0.00','2500.00',1,484,NULL),(451,'I','0.00','2500.00',1,485,NULL),(452,'I','0.00','2500.00',1,486,NULL),(453,'I','0.00','2500.00',1,487,NULL),(454,'I','0.00','2500.00',1,488,NULL),(455,'I','0.00','2500.00',1,489,NULL),(456,'I','0.00','2500.00',1,490,NULL),(457,'I','0.00','2500.00',1,491,NULL),(458,'I','0.00','2500.00',1,492,NULL),(459,'I','0.00','2500.00',1,493,NULL),(460,'I','0.00','2500.00',1,494,NULL),(461,'I','0.00','2500.00',1,495,NULL),(462,'I','0.00','2500.00',1,496,NULL);
/*!40000 ALTER TABLE `EM_PARTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_BANK`
--

DROP TABLE IF EXISTS `EM_PARTY_BANK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_BANK` (
  `NU_BANK_ID` bigint(20) NOT NULL,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_ACCOUNT_NUMBER` varchar(20) NOT NULL default '',
  `NU_ISSUE_NUMBER` bigint(20) NOT NULL default '0',
  `TX_ACCOUNT_NAME` varchar(80) NOT NULL default '',
  `TX_DESCRIPTION` varchar(100) default NULL,
  `DT_UPDATED` date NOT NULL,
  `TC_DEFAULT_YN` char(1) NOT NULL,
  `TX_SWIFT_CODE` varchar(25) NOT NULL default '',
  `TX_IBAN_CODE` varchar(25) default NULL,
  `TX_BANK_OFFICE_ADDRESS` varchar(100) default NULL,
  PRIMARY KEY  (`NU_BANK_ID`),
  UNIQUE KEY `NU_BANK_ID` (`NU_BANK_ID`),
  KEY `FK15D6340CB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FK15D6340CB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_BANK`
--

LOCK TABLES `EM_PARTY_BANK` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_BANK` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PARTY_BANK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_CARD`
--

DROP TABLE IF EXISTS `EM_PARTY_CARD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_CARD` (
  `NU_CARD_ID` bigint(20) NOT NULL,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_CARD_NUMBER` varchar(20) NOT NULL default '',
  `NU_ISSUE_NUMBER` bigint(20) NOT NULL default '0',
  `TX_NAME_ON_CARD` varchar(80) NOT NULL default '',
  `TX_DESCRIPTION` varchar(100) default NULL,
  `DT_EXPIRY_DATE` date NOT NULL,
  `DT_UPDATED` date NOT NULL,
  `TC_DEFAULT_YN` char(1) NOT NULL,
  `TX_CARD_PROVIDER` varchar(10) NOT NULL,
  PRIMARY KEY  (`NU_CARD_ID`),
  UNIQUE KEY `NU_CARD_ID` (`NU_CARD_ID`),
  KEY `FK15D6A8E0B47E1006` (`NU_PARTYID`),
  CONSTRAINT `FK15D6A8E0B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_CARD`
--

LOCK TABLES `EM_PARTY_CARD` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_CARD` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PARTY_CARD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_PARTYROLE`
--

DROP TABLE IF EXISTS `EM_PARTY_PARTYROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_PARTYROLE` (
  `NU_PARTYID` bigint(20) NOT NULL,
  `NU_PARTYROLEID` bigint(20) NOT NULL,
  `TX_CUSTOMERRANK` varchar(18) default NULL,
  `TX_STATUS` varchar(10) default NULL,
  `TX_REJECTION_CAUSE` varchar(255) default NULL,
  PRIMARY KEY  (`NU_PARTYID`,`NU_PARTYROLEID`),
  KEY `FKF423B46C379AD212` (`NU_PARTYROLEID`),
  KEY `FKF423B46CB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FKF423B46C379AD212` FOREIGN KEY (`NU_PARTYROLEID`) REFERENCES `EM_PARTY_ROLE` (`NU_PARTYROLEID`),
  CONSTRAINT `FKF423B46CB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_PARTYROLE`
--

LOCK TABLES `EM_PARTY_PARTYROLE` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_PARTYROLE` DISABLE KEYS */;
INSERT INTO `EM_PARTY_PARTYROLE` VALUES (1,1,NULL,'APPROVED',''),(1,2,NULL,'APPROVED',''),(1,3,NULL,'APPROVED',''),(2,2,NULL,'APPROVED',''),(3,2,NULL,'APPROVED',''),(12,3,NULL,'APPROVED',NULL),(13,3,NULL,'PENDING',NULL),(14,3,NULL,'PENDING',NULL),(15,3,NULL,'PENDING',NULL),(16,3,NULL,'PENDING',NULL),(17,3,NULL,'APPROVED',NULL),(18,3,NULL,'PENDING',NULL),(19,3,NULL,'APPROVED',NULL),(20,3,NULL,'PENDING',NULL),(21,3,NULL,'APPROVED',NULL),(22,3,NULL,'PENDING',NULL),(23,3,NULL,'APPROVED',NULL),(24,3,NULL,'PENDING',NULL),(25,3,NULL,'APPROVED',NULL),(26,3,NULL,'APPROVED',NULL),(27,3,NULL,'APPROVED',NULL),(28,3,NULL,'APPROVED',NULL),(29,3,NULL,'APPROVED',NULL),(30,3,NULL,'APPROVED',NULL),(31,3,NULL,'APPROVED',NULL),(32,3,NULL,'APPROVED',NULL),(33,3,NULL,'APPROVED',NULL),(34,3,NULL,'APPROVED',NULL),(35,3,NULL,'APPROVED',NULL),(36,3,NULL,'APPROVED',NULL),(37,3,NULL,'PENDING',NULL),(38,3,NULL,'APPROVED',NULL),(39,3,NULL,'APPROVED',NULL),(40,3,NULL,'PENDING',NULL),(41,3,NULL,'APPROVED',NULL),(42,3,NULL,'APPROVED',NULL),(43,3,NULL,'APPROVED',NULL),(44,3,NULL,'APPROVED',NULL),(45,3,NULL,'APPROVED',NULL),(46,3,NULL,'APPROVED',NULL),(47,3,NULL,'APPROVED',NULL),(48,3,NULL,'APPROVED',NULL),(49,3,NULL,'PENDING',NULL),(50,3,NULL,'APPROVED',NULL),(51,3,NULL,'APPROVED',NULL),(52,3,NULL,'APPROVED',NULL),(53,3,NULL,'APPROVED',NULL),(54,3,NULL,'APPROVED',NULL),(55,3,NULL,'APPROVED',NULL),(56,3,NULL,'APPROVED',NULL),(57,3,NULL,'APPROVED',NULL),(58,3,NULL,'APPROVED',NULL),(59,3,NULL,'APPROVED',NULL),(60,3,NULL,'PENDING',NULL),(61,3,NULL,'PENDING',NULL),(62,3,NULL,'APPROVED',NULL),(63,3,NULL,'APPROVED',NULL),(64,3,NULL,'APPROVED',NULL),(65,3,NULL,'APPROVED',NULL),(66,3,NULL,'APPROVED',NULL),(67,3,NULL,'APPROVED',NULL),(68,3,NULL,'APPROVED',NULL),(69,3,NULL,'APPROVED',NULL),(70,3,NULL,'PENDING',NULL),(71,3,NULL,'PENDING',NULL),(72,3,NULL,'PENDING',NULL),(73,3,NULL,'APPROVED',NULL),(74,3,NULL,'PENDING',NULL),(75,3,NULL,'APPROVED',NULL),(76,3,NULL,'APPROVED',NULL),(77,3,NULL,'APPROVED',NULL),(78,3,NULL,'APPROVED',NULL),(79,3,NULL,'APPROVED',NULL),(80,3,NULL,'APPROVED',NULL),(81,3,NULL,'APPROVED',NULL),(82,3,NULL,'APPROVED',NULL),(83,3,NULL,'PENDING',NULL),(84,3,NULL,'PENDING',NULL),(85,3,NULL,'APPROVED',NULL),(86,3,NULL,'PENDING',NULL),(87,3,NULL,'APPROVED',NULL),(88,3,NULL,'APPROVED',NULL),(89,3,NULL,'APPROVED',NULL),(90,3,NULL,'PENDING',NULL),(91,3,NULL,'PENDING',NULL),(92,3,NULL,'PENDING',NULL),(93,3,NULL,'APPROVED',NULL),(94,3,NULL,'PENDING',NULL),(95,3,NULL,'APPROVED',NULL),(96,3,NULL,'APPROVED',NULL),(97,3,NULL,'PENDING',NULL),(98,3,NULL,'APPROVED',NULL),(99,3,NULL,'APPROVED',NULL),(100,3,NULL,'APPROVED',NULL),(101,3,NULL,'APPROVED',NULL),(102,3,NULL,'APPROVED',NULL),(103,3,NULL,'PENDING',NULL),(104,3,NULL,'APPROVED',NULL),(105,3,NULL,'APPROVED',NULL),(106,3,NULL,'APPROVED',NULL),(107,3,NULL,'APPROVED',NULL),(108,3,NULL,'APPROVED',NULL),(109,3,NULL,'APPROVED',NULL),(110,3,NULL,'PENDING',NULL),(111,3,NULL,'APPROVED',NULL),(112,3,NULL,'PENDING',NULL),(113,3,NULL,'PENDING',NULL),(114,3,NULL,'APPROVED',NULL),(115,3,NULL,'APPROVED',NULL),(116,3,NULL,'APPROVED',NULL),(117,3,NULL,'APPROVED',NULL),(118,3,NULL,'APPROVED',NULL),(119,3,NULL,'APPROVED',NULL),(120,3,NULL,'APPROVED',NULL),(121,3,NULL,'APPROVED',NULL),(122,3,NULL,'APPROVED',NULL),(123,3,NULL,'APPROVED',NULL),(124,3,NULL,'APPROVED',NULL),(125,3,NULL,'PENDING',NULL),(126,3,NULL,'APPROVED',NULL),(127,3,NULL,'APPROVED',NULL),(128,3,NULL,'PENDING',NULL),(129,3,NULL,'APPROVED',NULL),(130,3,NULL,'APPROVED',NULL),(131,3,NULL,'APPROVED',NULL),(132,3,NULL,'APPROVED',NULL),(133,3,NULL,'APPROVED',NULL),(134,3,NULL,'APPROVED',NULL),(135,3,NULL,'PENDING',NULL),(136,3,NULL,'APPROVED',NULL),(137,3,NULL,'APPROVED',NULL),(138,3,NULL,'APPROVED',NULL),(139,3,NULL,'PENDING',NULL),(140,3,NULL,'APPROVED',NULL),(141,3,NULL,'APPROVED',NULL),(142,3,NULL,'APPROVED',NULL),(143,3,NULL,'APPROVED',NULL),(144,3,NULL,'APPROVED',NULL),(145,3,NULL,'APPROVED',NULL),(146,3,NULL,'APPROVED',NULL),(147,3,NULL,'APPROVED',NULL),(148,3,NULL,'APPROVED',NULL),(149,3,NULL,'APPROVED',NULL),(150,3,NULL,'APPROVED',NULL),(151,3,NULL,'APPROVED',NULL),(152,3,NULL,'APPROVED',NULL),(153,3,NULL,'APPROVED',NULL),(154,3,NULL,'PENDING',NULL),(155,3,NULL,'APPROVED',NULL),(156,3,NULL,'APPROVED',NULL),(157,3,NULL,'APPROVED',NULL),(158,3,NULL,'APPROVED',NULL),(159,3,NULL,'APPROVED',NULL),(160,3,NULL,'APPROVED',NULL),(161,3,NULL,'APPROVED',NULL),(162,3,NULL,'APPROVED',NULL),(163,3,NULL,'APPROVED',NULL),(164,3,NULL,'APPROVED',NULL),(165,3,NULL,'APPROVED',NULL),(166,3,NULL,'APPROVED',NULL),(167,3,NULL,'APPROVED',NULL),(168,3,NULL,'PENDING',NULL),(169,3,NULL,'APPROVED',NULL),(170,3,NULL,'APPROVED',NULL),(171,3,NULL,'APPROVED',NULL),(172,3,NULL,'APPROVED',NULL),(173,3,NULL,'APPROVED',NULL),(174,3,NULL,'PENDING',NULL),(175,3,NULL,'APPROVED',NULL),(176,3,NULL,'APPROVED',NULL),(177,3,NULL,'PENDING',NULL),(178,3,NULL,'PENDING',NULL),(179,3,NULL,'APPROVED',NULL),(180,3,NULL,'PENDING',NULL),(181,3,NULL,'APPROVED',NULL),(182,3,NULL,'APPROVED',NULL),(183,3,NULL,'APPROVED',NULL),(184,3,NULL,'APPROVED',NULL),(185,3,NULL,'APPROVED',NULL),(186,3,NULL,'APPROVED',NULL),(187,3,NULL,'APPROVED',NULL),(188,3,NULL,'APPROVED',NULL),(189,3,NULL,'APPROVED',NULL),(190,3,NULL,'APPROVED',NULL),(191,3,NULL,'APPROVED',NULL),(192,3,NULL,'APPROVED',NULL),(193,3,NULL,'APPROVED',NULL),(194,3,NULL,'APPROVED',NULL),(195,3,NULL,'APPROVED',NULL),(196,3,NULL,'APPROVED',NULL),(197,3,NULL,'APPROVED',NULL),(198,3,NULL,'APPROVED',NULL),(199,3,NULL,'PENDING',NULL),(200,3,NULL,'APPROVED',NULL),(201,3,NULL,'APPROVED',NULL),(202,3,NULL,'APPROVED',NULL),(203,3,NULL,'APPROVED',NULL),(204,3,NULL,'APPROVED',NULL),(205,3,NULL,'APPROVED',NULL),(206,3,NULL,'APPROVED',NULL),(207,3,NULL,'APPROVED',NULL),(208,3,NULL,'APPROVED',NULL),(209,3,NULL,'APPROVED',NULL),(210,3,NULL,'APPROVED',NULL),(211,3,NULL,'APPROVED',NULL),(212,3,NULL,'APPROVED',NULL),(213,3,NULL,'APPROVED',NULL),(214,3,NULL,'APPROVED',NULL),(215,3,NULL,'APPROVED',NULL),(216,3,NULL,'APPROVED',NULL),(217,3,NULL,'APPROVED',NULL),(218,3,NULL,'APPROVED',NULL),(219,3,NULL,'APPROVED',NULL),(220,3,NULL,'APPROVED',NULL),(221,3,NULL,'APPROVED',NULL),(222,3,NULL,'APPROVED',NULL),(223,3,NULL,'APPROVED',NULL),(224,3,NULL,'APPROVED',NULL),(225,3,NULL,'APPROVED',NULL),(226,3,NULL,'APPROVED',NULL),(227,3,NULL,'APPROVED',NULL),(228,3,NULL,'APPROVED',NULL),(229,3,NULL,'APPROVED',NULL),(230,3,NULL,'APPROVED',NULL),(231,3,NULL,'APPROVED',NULL),(232,3,NULL,'APPROVED',NULL),(233,3,NULL,'APPROVED',NULL),(234,3,NULL,'APPROVED',NULL),(235,3,NULL,'APPROVED',NULL),(236,3,NULL,'APPROVED',NULL),(237,3,NULL,'APPROVED',NULL),(238,3,NULL,'PENDING',NULL),(239,3,NULL,'APPROVED',NULL),(240,3,NULL,'APPROVED',NULL),(241,3,NULL,'APPROVED',NULL),(242,3,NULL,'APPROVED',NULL),(243,3,NULL,'PENDING',NULL),(244,3,NULL,'APPROVED',NULL),(245,3,NULL,'APPROVED',NULL),(246,3,NULL,'APPROVED',NULL),(247,3,NULL,'APPROVED',NULL),(248,3,NULL,'APPROVED',NULL),(249,3,NULL,'APPROVED',NULL),(250,3,NULL,'APPROVED',NULL),(251,3,NULL,'PENDING',NULL),(252,3,NULL,'APPROVED',NULL),(253,3,NULL,'APPROVED',NULL),(254,3,NULL,'APPROVED',NULL),(255,3,NULL,'APPROVED',NULL),(256,3,NULL,'APPROVED',NULL),(257,3,NULL,'APPROVED',NULL),(258,3,NULL,'APPROVED',NULL),(259,3,NULL,'PENDING',NULL),(260,3,NULL,'PENDING',NULL),(261,3,NULL,'APPROVED',NULL),(262,3,NULL,'APPROVED',NULL),(263,3,NULL,'PENDING',NULL),(264,3,NULL,'PENDING',NULL),(265,3,NULL,'APPROVED',NULL),(266,3,NULL,'PENDING',NULL),(267,3,NULL,'PENDING',NULL),(268,3,NULL,'APPROVED',NULL),(269,3,NULL,'APPROVED',NULL),(270,3,NULL,'PENDING',NULL),(271,3,NULL,'PENDING',NULL),(272,3,NULL,'PENDING',NULL),(273,3,NULL,'PENDING',NULL),(274,3,NULL,'APPROVED',NULL),(275,3,NULL,'PENDING',NULL),(276,3,NULL,'APPROVED',NULL),(277,3,NULL,'PENDING',NULL),(278,3,NULL,'APPROVED',NULL),(279,3,NULL,'APPROVED',NULL),(280,3,NULL,'APPROVED',NULL),(281,3,NULL,'APPROVED',NULL),(282,3,NULL,'PENDING',NULL),(283,3,NULL,'APPROVED',NULL),(284,3,NULL,'APPROVED',NULL),(285,3,NULL,'APPROVED',NULL),(286,3,NULL,'APPROVED',NULL),(287,3,NULL,'APPROVED',NULL),(288,3,NULL,'PENDING',NULL),(289,3,NULL,'APPROVED',NULL),(290,3,NULL,'APPROVED',NULL),(291,3,NULL,'APPROVED',NULL),(292,3,NULL,'APPROVED',NULL),(293,3,NULL,'APPROVED',NULL),(294,3,NULL,'APPROVED',NULL),(295,3,NULL,'PENDING',NULL),(296,3,NULL,'APPROVED',NULL),(297,3,NULL,'PENDING',NULL),(298,3,NULL,'APPROVED',NULL),(299,3,NULL,'PENDING',NULL),(300,3,NULL,'APPROVED',NULL),(301,3,NULL,'APPROVED',NULL),(302,3,NULL,'APPROVED',NULL),(303,3,NULL,'APPROVED',NULL),(304,3,NULL,'APPROVED',NULL),(305,3,NULL,'APPROVED',NULL),(306,3,NULL,'APPROVED',NULL),(307,3,NULL,'APPROVED',NULL),(308,3,NULL,'APPROVED',NULL),(309,3,NULL,'APPROVED',NULL),(310,3,NULL,'APPROVED',NULL),(311,3,NULL,'APPROVED',NULL),(312,3,NULL,'APPROVED',NULL),(313,3,NULL,'APPROVED',NULL),(314,3,NULL,'APPROVED',NULL),(315,3,NULL,'APPROVED',NULL),(316,3,NULL,'APPROVED',NULL),(317,3,NULL,'APPROVED',NULL),(318,3,NULL,'APPROVED',NULL),(319,3,NULL,'APPROVED',NULL),(320,3,NULL,'APPROVED',NULL),(321,3,NULL,'APPROVED',NULL),(322,3,NULL,'APPROVED',NULL),(323,3,NULL,'APPROVED',NULL),(324,3,NULL,'APPROVED',NULL),(325,3,NULL,'APPROVED',NULL),(326,3,NULL,'PENDING',NULL),(327,3,NULL,'APPROVED',NULL),(328,3,NULL,'APPROVED',NULL),(329,3,NULL,'PENDING',NULL),(330,3,NULL,'APPROVED',NULL),(331,3,NULL,'APPROVED',NULL),(332,3,NULL,'APPROVED',NULL),(333,3,NULL,'APPROVED',NULL),(334,3,NULL,'APPROVED',NULL),(335,3,NULL,'APPROVED',NULL),(336,3,NULL,'APPROVED',NULL),(337,3,NULL,'APPROVED',NULL),(338,3,NULL,'APPROVED',NULL),(339,3,NULL,'APPROVED',NULL),(340,3,NULL,'APPROVED',NULL),(341,3,NULL,'APPROVED',NULL),(342,3,NULL,'APPROVED',NULL),(343,3,NULL,'APPROVED',NULL),(344,3,NULL,'APPROVED',NULL),(345,3,NULL,'APPROVED',NULL),(346,3,NULL,'APPROVED',NULL),(347,3,NULL,'APPROVED',NULL),(348,3,NULL,'APPROVED',NULL),(349,3,NULL,'PENDING',NULL),(350,3,NULL,'APPROVED',NULL),(351,3,NULL,'APPROVED',NULL),(352,3,NULL,'APPROVED',NULL),(353,3,NULL,'APPROVED',NULL),(354,3,NULL,'APPROVED',NULL),(355,3,NULL,'PENDING',NULL),(356,3,NULL,'APPROVED',NULL),(357,3,NULL,'APPROVED',NULL),(358,3,NULL,'APPROVED',NULL),(359,3,NULL,'APPROVED',NULL),(360,3,NULL,'APPROVED',NULL),(361,3,NULL,'APPROVED',NULL),(362,3,NULL,'APPROVED',NULL),(363,3,NULL,'PENDING',NULL),(364,3,NULL,'APPROVED',NULL),(365,3,NULL,'APPROVED',NULL),(366,3,NULL,'APPROVED',NULL),(367,3,NULL,'APPROVED',NULL),(368,3,NULL,'APPROVED',NULL),(369,3,NULL,'APPROVED',NULL),(370,3,NULL,'APPROVED',NULL),(371,3,NULL,'APPROVED',NULL),(372,3,NULL,'PENDING',NULL),(373,3,NULL,'PENDING',NULL),(374,3,NULL,'APPROVED',NULL),(375,3,NULL,'APPROVED',NULL),(376,3,NULL,'APPROVED',NULL),(377,3,NULL,'APPROVED',NULL),(378,3,NULL,'APPROVED',NULL),(379,3,NULL,'APPROVED',NULL),(380,3,NULL,'APPROVED',NULL),(381,3,NULL,'APPROVED',NULL),(382,3,NULL,'APPROVED',NULL),(383,3,NULL,'APPROVED',NULL),(384,3,NULL,'APPROVED',NULL),(385,3,NULL,'APPROVED',NULL),(386,3,NULL,'APPROVED',NULL),(387,3,NULL,'APPROVED',NULL),(388,3,NULL,'APPROVED',NULL),(389,3,NULL,'APPROVED',NULL),(390,3,NULL,'PENDING',NULL),(391,3,NULL,'PENDING',NULL),(392,3,NULL,'PENDING',NULL),(393,3,NULL,'APPROVED',NULL),(394,3,NULL,'APPROVED',NULL),(395,3,NULL,'APPROVED',NULL),(396,3,NULL,'APPROVED',NULL),(397,3,NULL,'APPROVED',NULL),(398,3,NULL,'APPROVED',NULL),(399,3,NULL,'APPROVED',NULL),(400,3,NULL,'PENDING',NULL),(401,3,NULL,'APPROVED',NULL),(402,3,NULL,'APPROVED',NULL),(403,3,NULL,'APPROVED',NULL),(404,3,NULL,'APPROVED',NULL),(405,3,NULL,'APPROVED',NULL),(406,3,NULL,'APPROVED',NULL),(407,3,NULL,'APPROVED',NULL),(408,3,NULL,'APPROVED',NULL),(409,3,NULL,'PENDING',NULL),(410,3,NULL,'APPROVED',NULL),(411,3,NULL,'APPROVED',NULL),(412,3,NULL,'APPROVED',NULL),(413,3,NULL,'APPROVED',NULL),(414,3,NULL,'APPROVED',NULL),(415,3,NULL,'APPROVED',NULL),(416,3,NULL,'APPROVED',NULL),(417,3,NULL,'APPROVED',NULL),(418,3,NULL,'APPROVED',NULL),(419,3,NULL,'APPROVED',NULL),(420,3,NULL,'APPROVED',NULL),(421,3,NULL,'APPROVED',NULL),(422,3,NULL,'APPROVED',NULL),(423,3,NULL,'APPROVED',NULL),(424,3,NULL,'APPROVED',NULL),(425,3,NULL,'APPROVED',NULL),(426,3,NULL,'APPROVED',NULL),(427,3,NULL,'APPROVED',NULL),(428,3,NULL,'APPROVED',NULL),(429,3,NULL,'APPROVED',NULL),(430,3,NULL,'PENDING',NULL),(431,3,NULL,'PENDING',NULL),(432,3,NULL,'PENDING',NULL),(433,3,NULL,'APPROVED',NULL),(434,3,NULL,'APPROVED',NULL),(435,3,NULL,'APPROVED',NULL),(436,3,NULL,'APPROVED',NULL),(437,3,NULL,'APPROVED',NULL),(438,3,NULL,'APPROVED',NULL),(439,3,NULL,'APPROVED',NULL),(440,3,NULL,'APPROVED',NULL),(441,3,NULL,'APPROVED',NULL),(442,3,NULL,'APPROVED',NULL),(443,3,NULL,'APPROVED',NULL),(444,3,NULL,'APPROVED',NULL),(445,3,NULL,'APPROVED',NULL),(446,3,NULL,'APPROVED',NULL),(447,3,NULL,'APPROVED',NULL),(448,3,NULL,'APPROVED',NULL),(449,3,NULL,'APPROVED',NULL),(450,3,NULL,'APPROVED',NULL),(451,3,NULL,'APPROVED',NULL),(452,3,NULL,'APPROVED',NULL),(453,3,NULL,'APPROVED',NULL),(454,3,NULL,'APPROVED',NULL),(455,3,NULL,'APPROVED',NULL),(456,3,NULL,'APPROVED',NULL),(457,3,NULL,'APPROVED',NULL),(458,3,NULL,'APPROVED',NULL),(459,3,NULL,'APPROVED',NULL),(460,3,NULL,'APPROVED',NULL),(461,3,NULL,'APPROVED',NULL),(462,3,NULL,'APPROVED',NULL);
/*!40000 ALTER TABLE `EM_PARTY_PARTYROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_ROLE`
--

DROP TABLE IF EXISTS `EM_PARTY_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_ROLE` (
  `NU_PARTYROLEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_PARTYROLEID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_ROLE`
--

LOCK TABLES `EM_PARTY_ROLE` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_ROLE` DISABLE KEYS */;
INSERT INTO `EM_PARTY_ROLE` VALUES (1,'ADMIN','ADMINISTRATOR EMARKETPLACE'),(2,'SERVICEPROVIDER','PUBLISH SERVICES'),(3,'CUSTOMER','CONTRACT PRODUCTS');
/*!40000 ALTER TABLE `EM_PARTY_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTIES`
--

DROP TABLE IF EXISTS `EM_PENALTIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTIES` (
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(100) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `NU_PENALTY_TYPE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PENALTY_ID`),
  UNIQUE KEY `NU_PENALTY_ID` (`NU_PENALTY_ID`),
  KEY `FK69676BF0C7406F4C` (`NU_PENALTY_TYPE_ID`),
  CONSTRAINT `FK69676BF0C7406F4C` FOREIGN KEY (`NU_PENALTY_TYPE_ID`) REFERENCES `EM_PENALTY_TYPE` (`NU_PENALTY_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTIES`
--

LOCK TABLES `EM_PENALTIES` WRITE;
/*!40000 ALTER TABLE `EM_PENALTIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTY_TYPE`
--

DROP TABLE IF EXISTS `EM_PENALTY_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTY_TYPE` (
  `NU_PENALTY_TYPE_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(60) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PENALTY_TYPE_ID`),
  UNIQUE KEY `NU_PENALTY_TYPE_ID` (`NU_PENALTY_TYPE_ID`),
  KEY `FK17078787CC0A0369` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK17078787CC0A0369` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTY_TYPE`
--

LOCK TABLES `EM_PENALTY_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PENALTY_TYPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTY_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTY_VALUES`
--

DROP TABLE IF EXISTS `EM_PENALTY_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTY_VALUES` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `TX_PARAM_VALUE` varchar(40) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_PENALTY_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK7559B22F22C84C25` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK7559B22F6FABCE87` (`NU_PENALTY_ID`),
  CONSTRAINT `FK7559B22F22C84C25` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK7559B22F6FABCE87` FOREIGN KEY (`NU_PENALTY_ID`) REFERENCES `EM_PENALTIES` (`NU_PENALTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTY_VALUES`
--

LOCK TABLES `EM_PENALTY_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_PENALTY_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTY_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL`
--

DROP TABLE IF EXISTS `EM_POL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL` (
  `NU_POL_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_STATE` varchar(20) default NULL,
  `NU_POL_TYPE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_POL_ID`),
  KEY `NU_POL_TYPE_ID` (`NU_POL_TYPE_ID`),
  CONSTRAINT `EM_POL_ibfk_1` FOREIGN KEY (`NU_POL_TYPE_ID`) REFERENCES `EM_POL_TYPE` (`NU_POL_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL`
--

LOCK TABLES `EM_POL` WRITE;
/*!40000 ALTER TABLE `EM_POL` DISABLE KEYS */;
INSERT INTO `EM_POL` VALUES (1,'Policy Test 1','APPROVED',2),(3,'Policy Test 2','PENDING',2),(4,'Policy Test 3','REJECTED',2);
/*!40000 ALTER TABLE `EM_POL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICIES`
--

DROP TABLE IF EXISTS `EM_POLICIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICIES` (
  `NU_POLICY_ID` bigint(20) NOT NULL auto_increment,
  `NU_IDPRODUCTOFFERING` bigint(20) default NULL,
  `NU_POLICY_TYPE_ID` bigint(20) default NULL,
  `NU_SERVICEID` bigint(20) default NULL,
  `TX_STATUS` varchar(10) default NULL,
  PRIMARY KEY  (`NU_POLICY_ID`),
  KEY `NU_POLICY_TYPE_ID` (`NU_POLICY_TYPE_ID`),
  KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_POLICIES_ibfk_1` FOREIGN KEY (`NU_POLICY_TYPE_ID`) REFERENCES `EM_POLICY_TYPE` (`NU_POLICY_TYPE_ID`),
  CONSTRAINT `EM_POLICIES_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_POLICIES_ibfk_4` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICIES`
--

LOCK TABLES `EM_POLICIES` WRITE;
/*!40000 ALTER TABLE `EM_POLICIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_POLICIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICY_TYPE`
--

DROP TABLE IF EXISTS `EM_POLICY_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICY_TYPE` (
  `NU_POLICY_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_POLICY_TYPE_ID`),
  KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `EM_POLICY_TYPE_ibfk_1` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICY_TYPE`
--

LOCK TABLES `EM_POLICY_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_POLICY_TYPE` DISABLE KEYS */;
INSERT INTO `EM_POLICY_TYPE` VALUES (1,'Policy Percentage','Policy Percentage changes in GTs',9);
/*!40000 ALTER TABLE `EM_POLICY_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICY_VALUES`
--

DROP TABLE IF EXISTS `EM_POLICY_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICY_VALUES` (
  `NU_POLICY_ID` bigint(20) NOT NULL auto_increment,
  `NU_PARAM_ID` bigint(20) NOT NULL default '0',
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL default '0',
  `TX_PARAM_VALUE` varchar(255) default NULL,
  PRIMARY KEY  (`NU_POLICY_ID`,`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  KEY `NU_PARAM_ID` (`NU_PARAM_ID`),
  KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  KEY `NU_PARAM_ID_2` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK8D522D78CBFA24EC` (`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  CONSTRAINT `EM_POLICY_VALUES_ibfk_2` FOREIGN KEY (`NU_POLICY_ID`) REFERENCES `EM_POLICIES` (`NU_POLICY_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_POLICY_VALUES_ibfk_3` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK8D522D78CBFA24EC` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`, `NU_PARAM_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICY_VALUES`
--

LOCK TABLES `EM_POLICY_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_POLICY_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_POLICY_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL_RULES`
--

DROP TABLE IF EXISTS `EM_POL_RULES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL_RULES` (
  `EM_POL_ID` bigint(20) NOT NULL default '0',
  `NU_RULE_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`EM_POL_ID`,`NU_RULE_ID`),
  KEY `NU_RULE_ID` (`NU_RULE_ID`),
  CONSTRAINT `EM_POL_RULES_ibfk_1` FOREIGN KEY (`EM_POL_ID`) REFERENCES `EM_POL` (`NU_POL_ID`),
  CONSTRAINT `EM_POL_RULES_ibfk_2` FOREIGN KEY (`NU_RULE_ID`) REFERENCES `EM_RULE` (`NU_RULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL_RULES`
--

LOCK TABLES `EM_POL_RULES` WRITE;
/*!40000 ALTER TABLE `EM_POL_RULES` DISABLE KEYS */;
INSERT INTO `EM_POL_RULES` VALUES (1,1);
/*!40000 ALTER TABLE `EM_POL_RULES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL_TYPE`
--

DROP TABLE IF EXISTS `EM_POL_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL_TYPE` (
  `NU_POL_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  PRIMARY KEY  (`NU_POL_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL_TYPE`
--

LOCK TABLES `EM_POL_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_POL_TYPE` DISABLE KEYS */;
INSERT INTO `EM_POL_TYPE` VALUES (1,'Adjustment','Adjusment Policy'),(2,'Negotiation','Negotiation Policy'),(3,'Provider','ProviderControl Policy');
/*!40000 ALTER TABLE `EM_POL_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_TYPE`
--

DROP TABLE IF EXISTS `EM_PRICE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_TYPE` (
  `NU_IDPRICE_TYPE` bigint(20) NOT NULL auto_increment,
  `TC_PERIOD` varchar(12) NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_IDPRICE_TYPE`),
  UNIQUE KEY `NU_IDPRICE_TYPE` (`NU_IDPRICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_TYPE`
--

LOCK TABLES `EM_PRICE_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PRICE_TYPE` VALUES (1,'ONE TIME','one_time_charge','one_time_charge'),(2,'MONTH','Monthly payment','per_month'),(3,'EVENT','Per request','per_request'),(4,'EVENT','Tariff usage','tariff_usage'),(5,'WEEK','Pay for week','per_week'),(11,'MONTH','Flat Rate','flat_rate');
/*!40000 ALTER TABLE `EM_PRICE_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_VARIATION`
--

DROP TABLE IF EXISTS `EM_PRICE_VARIATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_VARIATION` (
  `NU_ID_PRICE_VARIATION` bigint(20) NOT NULL,
  `NU_POLICY_ID` bigint(20) default NULL,
  `NU_IDCOMPONENTPRICE` bigint(20) NOT NULL,
  `NU_ID_VARIATION_TYPE` bigint(20) default NULL,
  `TX_BSLAID` varchar(100) default NULL,
  `NU_PROMOTION_ID` bigint(20) default NULL,
  `NU_QUANTITY` decimal(22,0) default NULL,
  `NU_VALUE` decimal(22,0) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_ID_PRICE_VARIATION`),
  KEY `FKD21CD4C6D9855630` (`NU_POLICY_ID`),
  KEY `FKD21CD4C6CDBFDBEF` (`NU_PROMOTION_ID`),
  KEY `FKD21CD4C61A2EC446` (`NU_ID_VARIATION_TYPE`),
  KEY `FKD21CD4C6C64FFC05` (`TX_BSLAID`),
  KEY `FKD21CD4C691D9181B` (`NU_IDCOMPONENTPRICE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_VARIATION`
--

LOCK TABLES `EM_PRICE_VARIATION` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_VARIATION_TYPE`
--

DROP TABLE IF EXISTS `EM_PRICE_VARIATION_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_VARIATION_TYPE` (
  `NU_ID_VARIATION_TYPE` bigint(20) NOT NULL auto_increment,
  `TC_MODIFICATION_TYPE` char(1) default NULL,
  `TC_UNIT_TYPE` char(1) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_ID_VARIATION_TYPE`),
  UNIQUE KEY `NU_ID_VARIATION_TYPE` (`NU_ID_VARIATION_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_VARIATION_TYPE`
--

LOCK TABLES `EM_PRICE_VARIATION_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PRICE_VARIATION_TYPE` VALUES (1,'I','P','Percent Increment'),(2,'I','F','Fixed Increment'),(3,'D','P','Percent Decrement'),(4,'D','F','Decrement fixed price');
/*!40000 ALTER TABLE `EM_PRICE_VARIATION_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCTS_AREAS`
--

DROP TABLE IF EXISTS `EM_PRODUCTS_AREAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCTS_AREAS` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_AREA_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_AREA_ID`),
  KEY `FKA397D3421A968BF0` (`NU_AREA_ID`),
  KEY `FKA397D3422E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `EM_PRODUCTS_AREAS_ibfk_1` FOREIGN KEY (`NU_AREA_ID`) REFERENCES `EM_GEOGRAPHICAL_AREAS` (`NU_AREA_ID`),
  CONSTRAINT `EM_PRODUCTS_AREAS_ibfk_2` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCTS_AREAS`
--

LOCK TABLES `EM_PRODUCTS_AREAS` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCTS_AREAS` DISABLE KEYS */;
INSERT INTO `EM_PRODUCTS_AREAS` VALUES (1,3),(2,3),(3,3);
/*!40000 ALTER TABLE `EM_PRODUCTS_AREAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_OFFER_SLAT`
--

DROP TABLE IF EXISTS `EM_PRODUCT_OFFER_SLAT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_OFFER_SLAT` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_SERVICEID`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_PRODUCT_OFFER_SLAT_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_PRODUCT_OFFER_SLAT_ibfk_3` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_OFFER_SLAT`
--

LOCK TABLES `EM_PRODUCT_OFFER_SLAT` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_OFFER_SLAT` DISABLE KEYS */;
INSERT INTO `EM_PRODUCT_OFFER_SLAT` VALUES (1,25),(2,26),(3,27);
/*!40000 ALTER TABLE `EM_PRODUCT_OFFER_SLAT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_PENALTY`
--

DROP TABLE IF EXISTS `EM_PRODUCT_PENALTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_PENALTY` (
  `NU_PRODUCTID` bigint(20) NOT NULL,
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_PRODUCTID`,`NU_PENALTY_ID`),
  KEY `FK9B0C93626FABCE87` (`NU_PENALTY_ID`),
  KEY `FK9B0C93624168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `FK9B0C93624168D1FC` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`),
  CONSTRAINT `FK9B0C93626FABCE87` FOREIGN KEY (`NU_PENALTY_ID`) REFERENCES `EM_PENALTIES` (`NU_PENALTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_PENALTY`
--

LOCK TABLES `EM_PRODUCT_PENALTY` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_PENALTY` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRODUCT_PENALTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_PROMOTION`
--

DROP TABLE IF EXISTS `EM_PRODUCT_PROMOTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_PROMOTION` (
  `NU_PRODUCTID` bigint(20) NOT NULL,
  `NU_PROMOTION_ID` bigint(20) NOT NULL,
  `TX_STATUS` varchar(10) default NULL,
  PRIMARY KEY  (`NU_PRODUCTID`,`NU_PROMOTION_ID`),
  KEY `FK9A2F013C248E0328` (`NU_PROMOTION_ID`),
  KEY `FK9A2F013C4168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `EM_PRODUCT_PROMOTION_ibfk_1` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK9A2F013C248E0328` FOREIGN KEY (`NU_PROMOTION_ID`) REFERENCES `EM_PROMOTIONS` (`NU_PROMOTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_PROMOTION`
--

LOCK TABLES `EM_PRODUCT_PROMOTION` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_PROMOTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRODUCT_PROMOTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_PRODUCT_RATING`
--

DROP TABLE IF EXISTS `EM_PRODUCT_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_PRODUCT_RATING`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `EM_PRODUCT_RATING` (
  `NU_PRODUCTID` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `EM_PROMOTIONS`
--

DROP TABLE IF EXISTS `EM_PROMOTIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTIONS` (
  `NU_PROMOTION_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(100) default NULL,
  `NU_PROMOTION_TYPE_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_PROMOTION_ID`),
  UNIQUE KEY `NU_PROMOTION_ID` (`NU_PROMOTION_ID`),
  KEY `FK32CA582717BEC4C0` (`NU_PROMOTION_TYPE_ID`),
  CONSTRAINT `FK32CA582717BEC4C0` FOREIGN KEY (`NU_PROMOTION_TYPE_ID`) REFERENCES `EM_PROMOTION_TYPE` (`NU_PROMOTION_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTIONS`
--

LOCK TABLES `EM_PROMOTIONS` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PROMOTIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PROMOTION_TYPE`
--

DROP TABLE IF EXISTS `EM_PROMOTION_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTION_TYPE` (
  `NU_PROMOTION_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(60) default NULL,
  `TX_NAME` varchar(20) default NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PROMOTION_TYPE_ID`),
  UNIQUE KEY `NU_PROMOTION_TYPE_ID` (`NU_PROMOTION_TYPE_ID`),
  KEY `FK98D48DEDCC0A0369` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK98D48DEDCC0A0369` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTION_TYPE`
--

LOCK TABLES `EM_PROMOTION_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTION_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PROMOTION_TYPE` VALUES (1,'Promo discount range data Contract','Contract Dates',8),(2,'Promo discount range Data Price period','Month Price',8),(3,'Promo discount acording to Customer contract Country','Country Contract',4),(4,'Promo discount acording to Customer events Country','Country Event',4),(6,'Socioeconomic Customer Level','Socioeconomic Level',11);
/*!40000 ALTER TABLE `EM_PROMOTION_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PROMOTION_VALUES`
--

DROP TABLE IF EXISTS `EM_PROMOTION_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTION_VALUES` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_PROMOTION_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `TX_PARAM_VALUE` varchar(40) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_PROMOTION_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FKB7FEB71522C84C25` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FKB7FEB715248E0328` (`NU_PROMOTION_ID`),
  CONSTRAINT `FKB7FEB71522C84C25` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FKB7FEB715248E0328` FOREIGN KEY (`NU_PROMOTION_ID`) REFERENCES `EM_PROMOTIONS` (`NU_PROMOTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTION_VALUES`
--

LOCK TABLES `EM_PROMOTION_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTION_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PROMOTION_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RESTRICTIONS`
--

DROP TABLE IF EXISTS `EM_RESTRICTIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RESTRICTIONS` (
  `NU_RESTRICTION_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(200) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  PRIMARY KEY  (`NU_RESTRICTION_ID`),
  UNIQUE KEY `NU_RESTRICTION_ID` (`NU_RESTRICTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RESTRICTIONS`
--

LOCK TABLES `EM_RESTRICTIONS` WRITE;
/*!40000 ALTER TABLE `EM_RESTRICTIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_RESTRICTIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE`
--

DROP TABLE IF EXISTS `EM_RULE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE` (
  `NU_RULE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(255) default NULL,
  `TX_TEMPLATE` text,
  `NU_RULE_CONTEXT_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_RULE_ID`),
  UNIQUE KEY `TX_NAME` (`TX_NAME`),
  KEY `NU_RULE_CONTEXT_ID` (`NU_RULE_CONTEXT_ID`),
  CONSTRAINT `EM_RULE_ibfk_1` FOREIGN KEY (`NU_RULE_CONTEXT_ID`) REFERENCES `EM_RULE_CONTEXT` (`NU_RULE_CONTEXT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE`
--

LOCK TABLES `EM_RULE` WRITE;
/*!40000 ALTER TABLE `EM_RULE` DISABLE KEYS */;
INSERT INTO `EM_RULE` VALUES (1,'BootstrapStateMachine_Rule','rule \"BootstrapStateMachine_Rule\"\n    when\n    	//An empty would also run the then part.\n    	event : Event(eventName == EventName.StartNegotiationEvent);\n    then\n    	insert(new State(StateName.START));\n    	event.setProcessedSuccessfully(true);\n    	retract(event);\n        System.out.println(\"BootstrapStateMachine_Rule fired\" );\nend',1),(2,'CreateState_Rule','rule \"CreateState_Rule\"\r\n    when\r\n    	startState : State(name == StateName.START, status == StateStatus.NOT_STARTED);\r\n    then\r\n        System.out.println(\"CreateState_Rule fired\" );\r\n        System.out.println(\"-State Name :\" +startState.getName()+ \" has Status :\" +startState.getStatus());\r\n        startState.setStatus( StateStatus.RUNNING );\r\n        update(startState);\r\n        System.out.println(\"-State changed its status to :\" +startState.getStatus());\r\nend',1),(3,'StartState_OnEntry_Rule','rule \"StartState_OnEntry_Rule\" \r\n	salience 3\r\n	when \r\n		startState : State(name == StateName.START, status == StateStatus.RUNNING);		\r\n	then \r\n		startState.onEntry();\r\n        System.out.println(\"StartState_OnEntry_Rule fired\");\r\nend',1),(4,'Start_To_Negotiate_Transition_Rule','rule \"Start_To_Negotiate_Transition_Rule\"\n	salience 2\n	when \n		startState : State(name == StateName.START, status == StateStatus.RUNNING);\n	then \n		startState.setStatus(StateStatus.READY_TO_TRANSIT); //we dont need to modify this fact due to PropertyChangeListener.\n		System.out.println(\"State Name :\" +startState.getName()+ \" has Status :\" +startState.getStatus());		\n		insert(new State(StateName.NEGOTIATE)); //instantiating new State, without any hardcoding inside Platform components!\n        System.out.println(\"Start_To_Negotiate_Transition_Rule fired\");\nend',1),(5,'NegotiateState_ProposalArrived_Rule','rule \"NegotiateState_ProposalArrived_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.NOT_STARTED);\r\n		event01 : Event(eventName == EventName.ProposalArrivedEvent);\r\n	then \r\n		negotiateState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.\r\n		negotiateState.onEntry();\r\n		negotiateState.setNumberOfHopsAllowed(3);\r\n		negotiateState.setCurrentHop(1);\r\n		event01.setProcessedSuccessfully(true);\r\n		retract(event01);\r\n		update(negotiateState);\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n        System.out.println(\"NegotiateState_ProposalArrived_Rule fired\");\r\nend',1),(6,'NegotiateState_Evaluate_ProposalArrivedEvent_Rule','rule \"NegotiateState_Evaluate_ProposalArrivedEvent_Rule\" \r\n	when\r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop < numberOfHopsAllowed );\r\n		event1 : Event(eventName == EventName.ProposalArrivedEvent);\r\n	then \r\n		negotiateState.setCurrentHop(negotiateState.getCurrentHop()+1);\r\n		update(negotiateState);//This update is essential coz without it, the Working Memory wont execute last statement.\r\n		event1.setProcessedSuccessfully(true);\r\n		retract(event1);//Remove event as soon as it is processed.\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus()+\" HopCount :\"+negotiateState.getCurrentHop());		\r\n        System.out.println(\"NegotiateState_Evaluate_ProposalArrivedEvent_Rule fired\");\r\nend',1),(7,'NegotiateState_Evaluate_Event_Rule2','rule \"NegotiateState_Evaluate_Event_Rule2\" \r\n	when		\r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop >= numberOfHopsAllowed );\r\n	then \r\n		negotiateState.setStatus(StateStatus.READY_TO_TRANSIT);\r\n		update(negotiateState);\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus()+\" HopCount :\"+negotiateState.getCurrentHop());		\r\n        System.out.println(\"NegotiateState_Evaluate_Event_Rule2 fired\");\r\nend',1),(8,'Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule','rule \"Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );\r\n		event : Event(eventName == EventName.RequestAgreementEvent, eval(offer.getParties() != null) );\r\n	then \r\n		negotiateState.onExit();\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n		event.setProcessedSuccessfully(true);\r\n		insert(new State(StateName.DECIDE));\r\n		System.out.println(\"event.offer.getParties().length = \"+event.getOffer().getParties().length);\r\n		retract(event);		\r\n        System.out.println(\"Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule fired\");\r\nend',1),(9,'Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule','rule \"Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );\r\n		event : Event(eventName == EventName.AgreementRequestedEvent);\r\n	then \r\n		negotiateState.onExit();\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n		event.setProcessedSuccessfully(true);\r\n		insert(new State(StateName.DECIDE));\r\n		retract(event);\r\n        System.out.println(\"Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule fired\");\r\nend',1),(10,'DecideState_OnEntry_Rule','rule \"DecideState_OnEntry_Rule\" \r\n	when \r\n		decideState : State(name == StateName.DECIDE, status == StateStatus.NOT_STARTED);\r\n	then \r\n		decideState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.\r\n		decideState.onEntry();\r\n		update(decideState);\r\n        System.out.println(\"DecideState_OnEntry_Rule fired\" + decideState.getStatus());\r\nend',1),(11,'Hello World','rule \"Hello World\"\n	when\n		m : Message( status == Message.HELLO, message : message )\n	then\n		System.out.println( \"Im the stupid Hello-World rule: \"+message ); \nend',1);
/*!40000 ALTER TABLE `EM_RULE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_CONTEXT`
--

DROP TABLE IF EXISTS `EM_RULE_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_CONTEXT` (
  `NU_RULE_CONTEXT_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(255) default NULL,
  `TX_CONTENT` text,
  PRIMARY KEY  (`NU_RULE_CONTEXT_ID`),
  UNIQUE KEY `TX_NAME` (`TX_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_CONTEXT`
--

LOCK TABLES `EM_RULE_CONTEXT` WRITE;
/*!40000 ALTER TABLE `EM_RULE_CONTEXT` DISABLE KEYS */;
INSERT INTO `EM_RULE_CONTEXT` VALUES (1,'protocol','package protocol;\n\nimport org.slasoi.gslam.protocolengine.impl.StateEngine.Message;\nimport org.slasoi.gslam.protocolengine.impl.State;\nimport org.slasoi.gslam.protocolengine.impl.StateName;\nimport org.slasoi.gslam.protocolengine.impl.StateStatus;\nimport org.slasoi.gslam.protocolengine.impl.Event;\nimport org.slasoi.gslam.protocolengine.impl.EventName;\nimport org.slasoi.slamodel.sla.SLATemplate;\nimport java.util.List;');
/*!40000 ALTER TABLE `EM_RULE_CONTEXT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_PARAMS`
--

DROP TABLE IF EXISTS `EM_RULE_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_PARAMS` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `NU_PARAM_RULE_ORDER` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK78707312CC0A0369` (`NU_RULE_TEMPLATE_ID`),
  KEY `FK78707312DA3250D4` (`NU_PARAM_ID`),
  CONSTRAINT `EM_RULE_PARAMS_ibfk_2` FOREIGN KEY (`NU_PARAM_ID`) REFERENCES `EM_PARAMS` (`NU_PARAM_ID`),
  CONSTRAINT `EM_RULE_PARAMS_ibfk_3` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_PARAMS`
--

LOCK TABLES `EM_RULE_PARAMS` WRITE;
/*!40000 ALTER TABLE `EM_RULE_PARAMS` DISABLE KEYS */;
INSERT INTO `EM_RULE_PARAMS` VALUES (1,8,1),(2,8,2),(3,4,1),(6,4,4),(6,8,5),(6,11,4),(7,4,2),(7,8,3),(7,9,2),(7,11,2),(7,12,2),(8,9,1),(8,12,1),(11,4,3),(11,8,4),(11,9,6),(11,11,3),(11,12,6),(12,11,1),(15,9,3),(15,12,3),(16,9,4),(17,9,5),(18,9,7),(18,12,7),(22,12,4),(23,12,5);
/*!40000 ALTER TABLE `EM_RULE_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_TEMPLATES`
--

DROP TABLE IF EXISTS `EM_RULE_TEMPLATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_TEMPLATES` (
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `TX_TEMPLATE_DRT` text NOT NULL,
  PRIMARY KEY  (`NU_RULE_TEMPLATE_ID`),
  UNIQUE KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_TEMPLATES`
--

LOCK TABLES `EM_RULE_TEMPLATES` WRITE;
/*!40000 ALTER TABLE `EM_RULE_TEMPLATES` DISABLE KEYS */;
INSERT INTO `EM_RULE_TEMPLATES` VALUES (4,'Discount by Country and Price Type','Country','template header\r\nRULE_ID\r\nRULE_NAME\r\nNU_COUNTRY\r\nPRICE_TYPE\r\nVARIATION_TYPE\r\nDISCOUNT\r\n\r\npackage org.slasoi.businessManager.common.drools;\r\n\r\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\r\nimport org.slasoi.businessManager.common.model.EmPromotions;\r\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\r\nimport java.math.BigDecimal;\r\n\r\ntemplate \"Date Range Promotion Template\"\r\n\r\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\r\n	when\r\n		cp : EmComponentPrice()\r\n		customer_product : EmCustomersProducts()\r\n		eval (customer_product.getEmParty().getNuCountry()==@{NU_COUNTRY})\r\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\r\n	then\r\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\r\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\r\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{DISCOUNT}) );\r\n		customer_product.getEmPriceVariations().add(pv);\r\nend\r\nend template\r\n'),(8,'Discount by Date Range and Price Type','Date Range','template header\nRULE_ID\nRULE_NAME\nDATE_BEGIN\nDATE_END\nPRICE_TYPE\nVARIATION_TYPE\nDISCOUNT\n\npackage org.slasoi.businessManager.common.drools;\n\nimport org.slasoi.businessManager.common.util.SimpleDate;\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPromotions;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport java.math.BigDecimal;\n\ntemplate \"Date Range Promotion Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		cp : EmComponentPrice()\n		customer_product : EmCustomersProducts()\n		eval (new SimpleDate(cp.getDtValidFrom()).isBetwen(\"@{DATE_BEGIN}\",\"@{DATE_END}\"))\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\n	then\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1), new BigDecimal(@{DISCOUNT}));\n		customer_product.getEmPriceVariations().add(pv);\n\nend\nend template\n			'),(9,'Percentage change in GT and Price','Policy percentage','template header\nRULE_ID\nRULE_NAME\nMODIF_TYPE\nPRICE_TYPE\nSLSP\nVARIATION_MIN\nVARIATION_MAX\nVARIATION_TYPE\nPRICE_VARIATION\n\n\npackage es.tid.emarketplace.drools;\n\nimport org.slasoi.businessManager.common.model.EmPolicies;\nimport org.slasoi.businessManager.common.model.EmSpServices;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport org.slasoi.businessManager.common.util.GTDifference;\nimport java.math.BigDecimal;\n\n\nimport java.lang.Long;\n\ntemplate \"Policy Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		service : EmSpServices()\n		cp : EmComponentPrice()\n		gtDif : GTDifference()\n		\n		eval(cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})	\n		eval(service.getTxTemplateServiceid().equals(gtDif.getServiceName().trim()))\n		eval(gtDif.getParameter().equals(\"@{SLSP}\"))\n		eval(gtDif.getModificationType().equals(\"@{MODIF_TYPE}\"))\n		eval(gtDif.getPercentage()>=@{VARIATION_MIN})\n		eval(gtDif.getPercentage()<=@{VARIATION_MAX})\n		\n	then\n		EmPolicies policy = new EmPolicies(new Long(@{RULE_ID}));\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPriceVariation pv = new EmPriceVariation(policy, pvt, cp, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{PRICE_VARIATION}) );\n		cp.getEmPriceVariations().add(pv);\n		System.out.println(\"Se aplica @{RULE_NAME}\");\n\nend\nend template'),(11,'Template Socioeconomic Customer Level','Socioeconomic','template header\nRULE_ID\nRULE_NAME\nSOCIOECONOMIC_LEVEL\nPRICE_TYPE\nVARIATION_TYPE\nDISCOUNT\n\npackage org.slasoi.businessManager.common.drools;\n\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\nimport org.slasoi.businessManager.common.model.EmPromotions;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport org.slasoi.businessManager.common.bssCustomerProfile.beans.BssCustomerProfile;\n\nimport java.math.BigDecimal;\n\ntemplate \"Date Range Promotion Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		cp : EmComponentPrice()\n		customer_product : EmCustomersProducts()\n		customer_profile: BssCustomerProfile()\n		eval (customer_profile.getFeature(\"v1:segment\").equals(\"@{SOCIOECONOMIC_LEVEL}\"))\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\n	then\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{DISCOUNT}) );\n		customer_product.getEmPriceVariations().add(pv);\n		System.out.println(\"Se lanza la promocion Socioeconomic\");\n\nend\nend template\n'),(12,'Template for range values policies','Policy Range Values','template header\r\nRULE_ID\r\nRULE_NAME\r\nMODIF_TYPE\r\nPRICE_TYPE\r\nSLSP\r\nVALUE_MIN\r\nVALUE_MAX\r\nVARIATION_TYPE\r\nPRICE_VARIATION\r\n\r\n\r\npackage es.tid.emarketplace.drools;\r\n\r\nimport org.slasoi.businessManager.common.model.EmPolicies;\r\nimport org.slasoi.businessManager.common.model.EmSpServices;\r\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\r\nimport org.slasoi.businessManager.common.util.GTDifference;\r\nimport java.math.BigDecimal;\r\n\r\n\r\nimport java.lang.Long;\r\n\r\ntemplate \"Policy Template\"\r\n\r\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\r\n	when\r\n		service : EmSpServices()\r\n		cp : EmComponentPrice()\r\n		gtDif : GTDifference()\r\n\r\n		eval(cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})	\r\n		eval(service.getTxTemplateServiceid().equals(gtDif.getServiceName().trim()))\r\n		eval(gtDif.getParameter().equals(\"@{SLSP}\"))\r\n		eval(gtDif.getModificationType().equals(\"@{MODIF_TYPE}\"))\r\n		eval(gtDif.getValue()>=@{VALUE_MIN})\r\n		eval(gtDif.getValue()<=@{VALUE_MAX})\r\n\r\n		\r\n	then\r\n		EmPolicies policy = new EmPolicies(new Long(@{RULE_ID}));\r\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\r\n		EmPriceVariation pv = new EmPriceVariation(policy, pvt, cp, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{PRICE_VARIATION}) );\r\n		cp.getEmPriceVariations().add(pv);\r\n		System.out.println(\"Se aplica @{RULE_NAME}\");\r\n\r\nend\r\nend template');
/*!40000 ALTER TABLE `EM_RULE_TEMPLATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVICE_CATEGORIES`
--

DROP TABLE IF EXISTS `EM_SERVICE_CATEGORIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_CATEGORIES` (
  `NU_IDCATEGORY` bigint(20) NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDCATEGORY`,`NU_SERVICEID`),
  KEY `FKDF2022FD2B70AE8` (`NU_IDCATEGORY`),
  KEY `FKDF2022FD290B13C` (`NU_SERVICEID`),
  CONSTRAINT `EM_SERVICE_CATEGORIES_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKDF2022FD2B70AE8` FOREIGN KEY (`NU_IDCATEGORY`) REFERENCES `EM_SERVICE_SPECIFICATION` (`NU_IDCATEGORY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_CATEGORIES`
--

LOCK TABLES `EM_SERVICE_CATEGORIES` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_CATEGORIES` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_CATEGORIES` VALUES (1,25),(1,26),(1,27),(14,25),(14,26),(14,27);
/*!40000 ALTER TABLE `EM_SERVICE_CATEGORIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_SERVICE_RATING`
--

DROP TABLE IF EXISTS `EM_SERVICE_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_SERVICE_RATING`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `EM_SERVICE_RATING` (
  `NU_SERVICEID` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `EM_SERVICE_SPECIFICATION`
--

DROP TABLE IF EXISTS `EM_SERVICE_SPECIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_SPECIFICATION` (
  `NU_IDCATEGORY` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_IDCATEGORY`),
  UNIQUE KEY `NU_IDCATEGORY` (`NU_IDCATEGORY`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_SPECIFICATION`
--

LOCK TABLES `EM_SERVICE_SPECIFICATION` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_SPECIFICATION` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_SPECIFICATION` VALUES (1,'Mobile Service','Mobile'),(2,'Text Messaging Service','Messaging'),(4,'Contents: Audio, video, text, Enriched text Service','Multimedia'),(5,'Audio Service','Audio'),(6,'Video Service','Video'),(7,'Voice Service','Voice'),(8,'IP Voice','IP Voice'),(12,'Infrastructure Service','Infrastructure'),(13,'Fixed Line Service','Fixed'),(14,'Internet Service','Internet'),(15,'Leisure Time Service','Leisure Time'),(16,'IP Television Service','IPTV'),(21,'Gaming Service','Gaming'),(22,'USB connection','USB'),(26,'Generic connection','Generic');
/*!40000 ALTER TABLE `EM_SERVICE_SPECIFICATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVICE_TYPE`
--

DROP TABLE IF EXISTS `EM_SERVICE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_TYPE` (
  `NU_SERVICE_TYPE` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_SERVICE_TYPE`),
  UNIQUE KEY `NU_SERVICE_TYPE` (`NU_SERVICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_TYPE`
--

LOCK TABLES `EM_SERVICE_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_TYPE` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_TYPE` VALUES (1,'Service'),(2,'Foreing Service'),(3,'Application'),(4,'Foreign Application'),(6,'Gadget'),(7,'Back-end Service'),(9,'BPEL service');
/*!40000 ALTER TABLE `EM_SERVICE_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVS_SERVS`
--

DROP TABLE IF EXISTS `EM_SERVS_SERVS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVS_SERVS` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_SERVICEID_MAIN` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_SERVICEID`,`NU_SERVICEID_MAIN`),
  KEY `NU_SERVICEID_MAIN` (`NU_SERVICEID_MAIN`),
  CONSTRAINT `EM_SERVS_SERVS_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SERVS_SERVS_ibfk_2` FOREIGN KEY (`NU_SERVICEID_MAIN`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVS_SERVS`
--

LOCK TABLES `EM_SERVS_SERVS` WRITE;
/*!40000 ALTER TABLE `EM_SERVS_SERVS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SERVS_SERVS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVS_SLSP`
--

DROP TABLE IF EXISTS `EM_SERVS_SLSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVS_SLSP` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_SLSP_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_SERVICEID`,`NU_SLSP_ID`),
  KEY `NU_SLSP_ID` (`NU_SLSP_ID`),
  CONSTRAINT `EM_SERVS_SLSP_ibfk_2` FOREIGN KEY (`NU_SLSP_ID`) REFERENCES `EM_SLSP` (`NU_SLSP_ID`),
  CONSTRAINT `EM_SERVS_SLSP_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVS_SLSP`
--

LOCK TABLES `EM_SERVS_SLSP` WRITE;
/*!40000 ALTER TABLE `EM_SERVS_SLSP` DISABLE KEYS */;
INSERT INTO `EM_SERVS_SLSP` VALUES (25,1),(26,1),(27,1),(25,22),(26,22),(27,22),(25,23),(26,23),(27,23);
/*!40000 ALTER TABLE `EM_SERVS_SLSP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SLAMANAGERS`
--

DROP TABLE IF EXISTS `EM_SLAMANAGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SLAMANAGERS` (
  `NU_SLAMANAGERID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) NOT NULL,
  `TX_LAYER_TYPE` varchar(30) NOT NULL,
  `TX_ADDRESS` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`NU_SLAMANAGERID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SLAMANAGERS`
--

LOCK TABLES `EM_SLAMANAGERS` WRITE;
/*!40000 ALTER TABLE `EM_SLAMANAGERS` DISABLE KEYS */;
INSERT INTO `EM_SLAMANAGERS` VALUES (1,'Business SLAM','SW','http://localhost:8080/services/SWNegotiation?wsdl'),(2,'Software SLAM','BUSINESS','http://localhost:8080/services/BZNegotiation?wsdl'),(3,'Infrastructure SLAM','INFRA','http://localhost:8080/services/BZNegotiation?wsdl');
/*!40000 ALTER TABLE `EM_SLAMANAGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SLSP`
--

DROP TABLE IF EXISTS `EM_SLSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SLSP` (
  `NU_SLSP_ID` bigint(20) NOT NULL auto_increment,
  `TX_SLSP_NAME` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SLSP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SLSP`
--

LOCK TABLES `EM_SLSP` WRITE;
/*!40000 ALTER TABLE `EM_SLSP` DISABLE KEYS */;
INSERT INTO `EM_SLSP` VALUES (1,'availability'),(2,'accessibility'),(3,'arrival_rate'),(4,'data_volume'),(5,'throughput'),(6,'completion_time'),(7,'mttr'),(8,'mttf'),(9,'mttv'),(10,'reliability'),(11,'isolation'),(12,'accuracy'),(13,'non_repudiation'),(14,'supported_standards'),(15,'regulatory'),(16,'integrity'),(17,'authentication'),(18,'auditability'),(19,'authorisation'),(20,'data_encryption'),(21,'bandwidth'),(22,'duration'),(23,'latency'),(24,'count'),(25,'vm_cores'),(26,'cpu_speed'),(27,'memory'),(28,'vm_image');
/*!40000 ALTER TABLE `EM_SLSP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_OFFER_RESTRIC`
--

DROP TABLE IF EXISTS `EM_SP_OFFER_RESTRIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_OFFER_RESTRIC` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_RESTRICTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_RESTRICTION_ID`),
  KEY `FK4008E9EA6745D116` (`NU_RESTRICTION_ID`),
  KEY `FK4008E9EA2E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK4008E9EA2E546354` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK4008E9EA6745D116` FOREIGN KEY (`NU_RESTRICTION_ID`) REFERENCES `EM_RESTRICTIONS` (`NU_RESTRICTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_OFFER_RESTRIC`
--

LOCK TABLES `EM_SP_OFFER_RESTRIC` WRITE;
/*!40000 ALTER TABLE `EM_SP_OFFER_RESTRIC` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_OFFER_RESTRIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_PRODUCTS`
--

DROP TABLE IF EXISTS `EM_SP_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_PRODUCTS` (
  `NU_PRODUCTID` bigint(20) NOT NULL auto_increment,
  `DT_INSERTDATE` datetime default NULL,
  `DT_VALIDFROM` date default NULL,
  `DT_VALIDTO` date default NULL,
  `TX_BRAND` varchar(40) default NULL,
  `TX_PRODUCTDESC` varchar(100) default NULL,
  `TX_PRODUCTNAME` varchar(40) default NULL,
  `TX_RELEASE` varchar(20) default NULL,
  `TX_STATUS` varchar(1) default NULL,
  `NU_PARTYID` bigint(20) default NULL,
  `NU_MAX_PENALTIES_CUST` bigint(20) default NULL,
  `NU_MAX_PENALTY_CANCEL` bigint(20) default NULL,
  `NU_MAX_PENALTY_REMOVE` bigint(20) default NULL,
  `NU_UNSUCCESS_ROUNDS_NEG` bigint(20) default NULL,
  `NU_MAX_PRICE_DECR` bigint(20) default NULL,
  `TX_NEGOTIATION_TYPE` varchar(1) NOT NULL default 'A',
  PRIMARY KEY  (`NU_PRODUCTID`),
  UNIQUE KEY `NU_PRODUCTID` (`NU_PRODUCTID`),
  KEY `FKE6CEBEFB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FKE6CEBEFB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_PRODUCTS`
--

LOCK TABLES `EM_SP_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `EM_SP_PRODUCTS` DISABLE KEYS */;
INSERT INTO `EM_SP_PRODUCTS` VALUES (1,'2010-11-12 09:59:03','2010-11-12','2012-12-31','MOVISTAR','Product Test 1','Product Test 1','1','A',1,10,10,10,10,40,'A');
/*!40000 ALTER TABLE `EM_SP_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_PRODUCTS_OFFER`
--

DROP TABLE IF EXISTS `EM_SP_PRODUCTS_OFFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_PRODUCTS_OFFER` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL auto_increment,
  `DT_VALID_FROM` date NOT NULL,
  `DT_VALID_TO` date NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  `TX_REVISIONID` varchar(20) NOT NULL,
  `TX_STATUS` varchar(1) NOT NULL,
  `NU_BILLING_FRECUENCY_ID` bigint(20) default NULL,
  `NU_PRODUCTID` bigint(20) default NULL,
  `TX_BSLATID` varchar(256) default NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`),
  UNIQUE KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  KEY `FKEF5881EC25E1C284` (`NU_BILLING_FRECUENCY_ID`),
  KEY `FKEF5881EC4168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `EM_SP_PRODUCTS_OFFER_ibfk_1` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKEF5881EC25E1C284` FOREIGN KEY (`NU_BILLING_FRECUENCY_ID`) REFERENCES `EM_BILLING_FRECUENCY` (`NU_BILLING_FRECUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_PRODUCTS_OFFER`
--

LOCK TABLES `EM_SP_PRODUCTS_OFFER` WRITE;
/*!40000 ALTER TABLE `EM_SP_PRODUCTS_OFFER` DISABLE KEYS */;
INSERT INTO `EM_SP_PRODUCTS_OFFER` VALUES (1,'2010-11-12','2012-12-31','Product Offer Test 1','Product Offer Test 1','1','A',1,1,'ORCBUSINESSSLAT1'),(2,'2010-11-12','2012-12-31','Product Offer Test 2','Product Offer Test 2','1','A',1,1,'ORCBUSINESSSLAT1'),(3,'2010-11-12','2012-12-31','Product Offer Test 3','Product Offer Test 3','1','A',1,1,'ORCBUSINESSSLAT1');
/*!40000 ALTER TABLE `EM_SP_PRODUCTS_OFFER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_SERVICENAME` varchar(40) NOT NULL default '',
  `TX_SERVICEDESC` varchar(100) default NULL,
  `TC_SERVICETYPE` char(1) NOT NULL default '',
  `TX_RELEASE` varchar(4) NOT NULL default '',
  `DT_INSERTDATE` datetime default NULL,
  `NU_SERVICE_TYPE` bigint(20) default NULL,
  `TX_SLATID` varchar(40) default NULL,
  `NU_SLAMANAGERID` bigint(20) default NULL,
  `TX_TEMPLATE_SERVICEID` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`),
  UNIQUE KEY `NU_SERVICEID` (`NU_SERVICEID`),
  KEY `FK9C762AE942048D62` (`NU_SERVICE_TYPE`),
  KEY `FK9C762AE9B47E1006` (`NU_PARTYID`),
  KEY `NU_SLAMANAGERID` (`NU_SLAMANAGERID`),
  CONSTRAINT `EM_SP_SERVICES_ibfk_1` FOREIGN KEY (`NU_SLAMANAGERID`) REFERENCES `EM_SLAMANAGERS` (`NU_SLAMANAGERID`),
  CONSTRAINT `FK9C762AE942048D62` FOREIGN KEY (`NU_SERVICE_TYPE`) REFERENCES `EM_SERVICE_TYPE` (`NU_SERVICE_TYPE`),
  CONSTRAINT `FK9C762AE9B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES`
--

LOCK TABLES `EM_SP_SERVICES` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES` DISABLE KEYS */;
INSERT INTO `EM_SP_SERVICES` VALUES (25,1,'ORC Service 1','ORC Service 1','S','1','2010-11-12 09:49:55',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService'),(26,1,'ORC Service 2','ORC Service 2','S','1','2010-11-12 09:50:36',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService'),(27,1,'ORC Service 3','ORC Service 3','S','1','2010-11-12 09:52:15',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService');
/*!40000 ALTER TABLE `EM_SP_SERVICES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_ATTRIBUTES`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_ATTRIBUTES` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `NU_ATTRIBUTEID` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERVICES_ATTRIBUTES_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERVICES_ATTRIBUTES_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 11264 kB; (`NU_SERVICEID`) REFER `SLASOI_AUX/EM';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_ATTRIBUTES`
--

LOCK TABLES `EM_SP_SERVICES_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `EM_SP_SERVICES_ATTRIBUTES` VALUES (25,1),(26,1),(27,1),(25,2),(26,2),(27,2),(25,3),(26,3),(27,3);
/*!40000 ALTER TABLE `EM_SP_SERVICES_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_CHARACTERISTIC`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_CHARACTERISTIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_CHARACTERISTIC` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_CHARACTERISTIC_ID` bigint(20) NOT NULL default '0',
  `TX_VALUE` varchar(100) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_CHARACTERISTIC_ID`),
  KEY `NU_CHARACTERISTIC_ID` (`NU_CHARACTERISTIC_ID`),
  CONSTRAINT `EM_SP_SERVICES_CHARACTERISTIC_ibfk_2` FOREIGN KEY (`NU_CHARACTERISTIC_ID`) REFERENCES `EM_CHARACTERISTIC` (`NU_CHARACTERISTIC_ID`),
  CONSTRAINT `EM_SP_SERVICES_CHARACTERISTIC_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_CHARACTERISTIC`
--

LOCK TABLES `EM_SP_SERVICES_CHARACTERISTIC` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CHARACTERISTIC` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CHARACTERISTIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_CONTRACT`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_CONTRACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_CONTRACT` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_SPSLAID` varchar(40) NOT NULL default '',
  `DT_CONTRACT_DATE` date default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_PARTYID`,`TX_SPSLAID`),
  KEY `NU_PARTYID` (`NU_PARTYID`),
  CONSTRAINT `EM_SP_SERVICES_CONTRACT_ibfk_1` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`),
  CONSTRAINT `EM_SP_SERVICES_CONTRACT_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_CONTRACT`
--

LOCK TABLES `EM_SP_SERVICES_CONTRACT` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CONTRACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CONTRACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERV_SATISFACTION_RATE`
--

DROP TABLE IF EXISTS `EM_SP_SERV_SATISFACTION_RATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERV_SATISFACTION_RATE` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  `TX_BSLAID` varchar(40) NOT NULL default '0',
  `NU_SATISFACTION_RATE` bigint(20) NOT NULL,
  `TX_REVIEW_COMMENT` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_ATTRIBUTEID`,`TX_BSLAID`),
  KEY `FK_EM_SP_SERV_SATISFACTION_RATE_2` (`TX_BSLAID`),
  KEY `FK_EM_SP_SERV_SATISFACTION_RATE_3` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_3` FOREIGN KEY (`TX_BSLAID`) REFERENCES `EM_CUSTOMERS_PRODUCTS` (`TX_BSLAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERV_SATISFACTION_RATE`
--

LOCK TABLES `EM_SP_SERV_SATISFACTION_RATE` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERV_SATISFACTION_RATE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERV_SATISFACTION_RATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERV_SAT_RATE_AGR`
--

DROP TABLE IF EXISTS `EM_SP_SERV_SAT_RATE_AGR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERV_SAT_RATE_AGR` (
  `DT_DATE` date NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  `NU_SURVEYS` bigint(20) default NULL,
  `NU_AVG_RATE` decimal(10,2) default NULL,
  PRIMARY KEY  (`DT_DATE`,`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `FK_EM_SP_SERV_SAT_RATE_AGR_1` (`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `FK_EM_SP_SERV_SAT_RATE_AGR_2` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERV_SAT_RATE_AGR_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SP_SERV_SAT_RATE_AGR_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERV_SAT_RATE_AGR`
--

LOCK TABLES `EM_SP_SERV_SAT_RATE_AGR` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERV_SAT_RATE_AGR` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERV_SAT_RATE_AGR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_USERS`
--

DROP TABLE IF EXISTS `EM_USERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_USERS` (
  `NU_USERID` bigint(20) NOT NULL auto_increment,
  `TX_USER_LOGIN` varchar(10) NOT NULL default '',
  `TX_PASSWD` varchar(10) NOT NULL default '',
  `TC_DEPARTMENT_ROLE` varchar(1) default NULL,
  `NU_PARTYID` bigint(20) default NULL,
  `NU_PARTYROLEID` bigint(20) default NULL,
  `NU_INDIVIDUALID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_USERID`),
  UNIQUE KEY `NU_USERID` (`NU_USERID`),
  KEY `NU_PARTYROLEID` (`NU_PARTYROLEID`),
  KEY `NU_INDIVIDUALID` (`NU_INDIVIDUALID`),
  KEY `NU_PARTYID` (`NU_PARTYID`,`NU_PARTYROLEID`),
  CONSTRAINT `EM_USERS_ibfk_3` FOREIGN KEY (`NU_INDIVIDUALID`) REFERENCES `EM_INDIVIDUAL` (`NU_INDIVIDUALID`),
  CONSTRAINT `EM_USERS_ibfk_4` FOREIGN KEY (`NU_PARTYID`, `NU_PARTYROLEID`) REFERENCES `EM_PARTY_PARTYROLE` (`NU_PARTYID`, `NU_PARTYROLEID`)
) ENGINE=InnoDB AUTO_INCREMENT=474 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_USERS`
--

LOCK TABLES `EM_USERS` WRITE;
/*!40000 ALTER TABLE `EM_USERS` DISABLE KEYS */;
INSERT INTO `EM_USERS` VALUES (1,'admin','admin','A',1,1,NULL),(20,'bm','bm','B',1,1,NULL),(21,'cm','cm','R',1,1,NULL),(22,'sm','sm','S',1,1,NULL),(23,'Integrator','Integrator',NULL,12,3,46),(24,'Integrator','Integrator',NULL,13,3,47),(25,'Integrator','Integrator',NULL,14,3,48),(26,'Integrator','Integrator',NULL,15,3,49),(27,'Integrator','Integrator',NULL,16,3,50),(28,'Integrator','Integrator',NULL,17,3,51),(29,'Integrator','Integrator',NULL,18,3,52),(30,'Integrator','Integrator',NULL,19,3,53),(31,'test','test',NULL,20,3,54),(32,'Integrator','Integrator',NULL,21,3,55),(33,'Integrator','Integrator',NULL,22,3,56),(34,'Integrator','Integrator',NULL,23,3,57),(35,'Integrator','Integrator',NULL,24,3,58),(36,'Integrator','Integrator',NULL,25,3,59),(37,'Integrator','Integrator',NULL,26,3,60),(38,'Integrator','Integrator',NULL,27,3,61),(39,'Integrator','Integrator',NULL,28,3,62),(40,'Integrator','Integrator',NULL,29,3,63),(41,'Integrator','Integrator',NULL,30,3,64),(42,'Integrator','Integrator',NULL,31,3,65),(43,'Integrator','Integrator',NULL,32,3,66),(44,'Integrator','Integrator',NULL,33,3,67),(45,'Integrator','Integrator',NULL,34,3,68),(46,'Integrator','Integrator',NULL,35,3,69),(47,'Integrator','Integrator',NULL,36,3,70),(48,'Integrator','Integrator',NULL,37,3,71),(49,'Integrator','Integrator',NULL,38,3,72),(50,'Integrator','Integrator',NULL,39,3,73),(51,'Integrator','Integrator',NULL,40,3,74),(52,'Integrator','Integrator',NULL,41,3,75),(53,'Integrator','Integrator',NULL,42,3,76),(54,'Integrator','Integrator',NULL,43,3,77),(55,'Integrator','Integrator',NULL,44,3,78),(56,'Integrator','Integrator',NULL,45,3,79),(57,'Integrator','Integrator',NULL,46,3,80),(58,'Integrator','Integrator',NULL,47,3,81),(59,'Integrator','Integrator',NULL,48,3,82),(60,'test','test',NULL,49,3,83),(61,'Integrator','Integrator',NULL,50,3,84),(62,'Integrator','Integrator',NULL,51,3,85),(63,'Integrator','Integrator',NULL,52,3,86),(64,'Integrator','Integrator',NULL,53,3,87),(65,'Integrator','Integrator',NULL,54,3,88),(66,'Integrator','Integrator',NULL,55,3,89),(67,'Integrator','Integrator',NULL,56,3,90),(68,'Integrator','Integrator',NULL,57,3,91),(69,'Integrator','Integrator',NULL,58,3,92),(70,'Integrator','Integrator',NULL,59,3,93),(71,'primoz','primoz',NULL,60,3,94),(72,'Integrator','Integrator',NULL,61,3,95),(73,'Integrator','Integrator',NULL,62,3,96),(74,'Integrator','Integrator',NULL,63,3,97),(75,'Integrator','Integrator',NULL,64,3,98),(76,'Integrator','Integrator',NULL,65,3,99),(77,'Integrator','Integrator',NULL,66,3,100),(78,'Integrator','Integrator',NULL,67,3,101),(79,'Integrator','Integrator',NULL,68,3,102),(80,'Integrator','Integrator',NULL,69,3,103),(81,'Integrator','Integrator',NULL,70,3,104),(82,'Integrator','Integrator',NULL,71,3,105),(83,'Integrator','Integrator',NULL,72,3,106),(84,'Integrator','Integrator',NULL,73,3,107),(85,'Integrator','Integrator',NULL,74,3,108),(86,'Integrator','Integrator',NULL,75,3,109),(87,'Integrator','Integrator',NULL,76,3,110),(88,'Integrator','Integrator',NULL,77,3,111),(89,'Integrator','Integrator',NULL,78,3,112),(90,'Integrator','Integrator',NULL,79,3,113),(91,'Integrator','Integrator',NULL,80,3,114),(92,'Integrator','Integrator',NULL,81,3,115),(93,'Integrator','Integrator',NULL,82,3,116),(94,'Integrator','Integrator',NULL,83,3,117),(95,'Integrator','Integrator',NULL,84,3,118),(96,'Integrator','Integrator',NULL,85,3,119),(97,'Integrator','Integrator',NULL,86,3,120),(98,'Integrator','Integrator',NULL,87,3,121),(99,'Integrator','Integrator',NULL,88,3,122),(100,'Integrator','Integrator',NULL,89,3,123),(101,'Integrator','Integrator',NULL,90,3,124),(102,'Integrator','Integrator',NULL,91,3,125),(103,'Integrator','Integrator',NULL,92,3,126),(104,'Integrator','Integrator',NULL,93,3,127),(105,'Integrator','Integrator',NULL,94,3,128),(106,'Integrator','Integrator',NULL,95,3,129),(107,'Integrator','Integrator',NULL,96,3,130),(108,'Integrator','Integrator',NULL,97,3,131),(109,'Integrator','Integrator',NULL,98,3,132),(110,'Integrator','Integrator',NULL,99,3,133),(111,'Integrator','Integrator',NULL,100,3,134),(112,'Integrator','Integrator',NULL,101,3,135),(113,'Integrator','Integrator',NULL,102,3,136),(114,'test','test',NULL,103,3,137),(115,'Integrator','Integrator',NULL,104,3,138),(116,'Integrator','Integrator',NULL,105,3,139),(117,'Integrator','Integrator',NULL,106,3,140),(118,'Integrator','Integrator',NULL,107,3,141),(119,'Integrator','Integrator',NULL,108,3,142),(120,'Integrator','Integrator',NULL,109,3,143),(121,'Integrator','Integrator',NULL,110,3,144),(122,'Integrator','Integrator',NULL,111,3,145),(123,'Integrator','Integrator',NULL,112,3,146),(124,'Integrator','Integrator',NULL,113,3,147),(125,'Integrator','Integrator',NULL,114,3,148),(126,'Integrator','Integrator',NULL,115,3,149),(127,'Integrator','Integrator',NULL,116,3,150),(128,'Integrator','Integrator',NULL,117,3,151),(129,'Integrator','Integrator',NULL,118,3,152),(130,'Integrator','Integrator',NULL,119,3,153),(131,'Integrator','Integrator',NULL,120,3,154),(132,'Integrator','Integrator',NULL,121,3,155),(133,'Integrator','Integrator',NULL,122,3,156),(134,'Integrator','Integrator',NULL,123,3,157),(135,'Integrator','Integrator',NULL,124,3,158),(136,'Integrator','Integrator',NULL,125,3,159),(137,'Integrator','Integrator',NULL,126,3,160),(138,'Integrator','Integrator',NULL,127,3,161),(139,'Integrator','Integrator',NULL,128,3,162),(140,'Integrator','Integrator',NULL,129,3,163),(141,'Integrator','Integrator',NULL,130,3,164),(142,'Integrator','Integrator',NULL,131,3,165),(143,'Integrator','Integrator',NULL,132,3,166),(144,'Integrator','Integrator',NULL,133,3,167),(145,'Integrator','Integrator',NULL,134,3,168),(146,'Integrator','Integrator',NULL,135,3,169),(147,'Integrator','Integrator',NULL,136,3,170),(148,'Integrator','Integrator',NULL,137,3,171),(149,'Integrator','Integrator',NULL,138,3,172),(150,'Integrator','Integrator',NULL,139,3,173),(151,'Integrator','Integrator',NULL,140,3,174),(152,'Integrator','Integrator',NULL,141,3,175),(153,'Integrator','Integrator',NULL,142,3,176),(154,'Integrator','Integrator',NULL,143,3,177),(155,'Integrator','Integrator',NULL,144,3,178),(156,'Integrator','Integrator',NULL,145,3,179),(157,'Integrator','Integrator',NULL,146,3,180),(158,'Integrator','Integrator',NULL,147,3,181),(159,'Integrator','Integrator',NULL,148,3,182),(160,'Integrator','Integrator',NULL,149,3,183),(161,'Integrator','Integrator',NULL,150,3,184),(162,'Integrator','Integrator',NULL,151,3,185),(163,'Integrator','Integrator',NULL,152,3,186),(164,'Integrator','Integrator',NULL,153,3,187),(165,'Integrator','Integrator',NULL,154,3,188),(166,'Integrator','Integrator',NULL,155,3,189),(167,'Integrator','Integrator',NULL,156,3,190),(168,'Integrator','Integrator',NULL,157,3,191),(169,'Integrator','Integrator',NULL,158,3,192),(170,'Integrator','Integrator',NULL,159,3,193),(171,'Integrator','Integrator',NULL,160,3,194),(172,'Integrator','Integrator',NULL,161,3,195),(173,'Integrator','Integrator',NULL,162,3,196),(174,'Integrator','Integrator',NULL,163,3,197),(175,'Integrator','Integrator',NULL,164,3,198),(176,'Integrator','Integrator',NULL,165,3,199),(177,'Integrator','Integrator',NULL,166,3,200),(178,'Integrator','Integrator',NULL,167,3,201),(179,'Integrator','Integrator',NULL,168,3,202),(180,'Integrator','Integrator',NULL,169,3,203),(181,'Integrator','Integrator',NULL,170,3,204),(182,'Integrator','Integrator',NULL,171,3,205),(183,'Integrator','Integrator',NULL,172,3,206),(184,'Integrator','Integrator',NULL,173,3,207),(185,'Integrator','Integrator',NULL,174,3,208),(186,'Integrator','Integrator',NULL,175,3,209),(187,'Integrator','Integrator',NULL,176,3,210),(188,'Integrator','Integrator',NULL,177,3,211),(189,'Integrator','Integrator',NULL,178,3,212),(190,'Integrator','Integrator',NULL,179,3,213),(191,'Integrator','Integrator',NULL,180,3,214),(192,'Integrator','Integrator',NULL,181,3,215),(193,'Integrator','Integrator',NULL,182,3,216),(194,'Integrator','Integrator',NULL,183,3,217),(195,'Integrator','Integrator',NULL,184,3,218),(196,'Integrator','Integrator',NULL,185,3,219),(197,'Integrator','Integrator',NULL,186,3,220),(198,'Integrator','Integrator',NULL,187,3,221),(199,'Integrator','Integrator',NULL,188,3,222),(200,'Integrator','Integrator',NULL,189,3,223),(201,'Integrator','Integrator',NULL,190,3,224),(202,'Integrator','Integrator',NULL,191,3,225),(203,'Integrator','Integrator',NULL,192,3,226),(204,'Integrator','Integrator',NULL,193,3,227),(205,'Integrator','Integrator',NULL,194,3,228),(206,'Integrator','Integrator',NULL,195,3,229),(207,'Integrator','Integrator',NULL,196,3,230),(208,'Integrator','Integrator',NULL,197,3,231),(209,'Integrator','Integrator',NULL,198,3,232),(210,'Integrator','Integrator',NULL,199,3,233),(211,'Integrator','Integrator',NULL,200,3,234),(212,'Integrator','Integrator',NULL,201,3,235),(213,'Integrator','Integrator',NULL,202,3,236),(214,'Integrator','Integrator',NULL,203,3,237),(215,'Integrator','Integrator',NULL,204,3,238),(216,'Integrator','Integrator',NULL,205,3,239),(217,'Integrator','Integrator',NULL,206,3,240),(218,'Integrator','Integrator',NULL,207,3,241),(219,'Integrator','Integrator',NULL,208,3,242),(220,'Integrator','Integrator',NULL,209,3,243),(221,'Integrator','Integrator',NULL,210,3,244),(222,'Integrator','Integrator',NULL,211,3,245),(223,'Integrator','Integrator',NULL,212,3,246),(224,'Integrator','Integrator',NULL,213,3,247),(225,'Integrator','Integrator',NULL,214,3,248),(226,'Integrator','Integrator',NULL,215,3,249),(227,'Integrator','Integrator',NULL,216,3,250),(228,'Integrator','Integrator',NULL,217,3,251),(229,'Integrator','Integrator',NULL,218,3,252),(230,'Integrator','Integrator',NULL,219,3,253),(231,'Integrator','Integrator',NULL,220,3,254),(232,'Integrator','Integrator',NULL,221,3,255),(233,'Integrator','Integrator',NULL,222,3,256),(234,'Integrator','Integrator',NULL,223,3,257),(235,'Integrator','Integrator',NULL,224,3,258),(236,'Integrator','Integrator',NULL,225,3,259),(237,'Integrator','Integrator',NULL,226,3,260),(238,'Integrator','Integrator',NULL,227,3,261),(239,'Integrator','Integrator',NULL,228,3,262),(240,'Integrator','Integrator',NULL,229,3,263),(241,'Integrator','Integrator',NULL,230,3,264),(242,'Integrator','Integrator',NULL,231,3,265),(243,'Integrator','Integrator',NULL,232,3,266),(244,'Integrator','Integrator',NULL,233,3,267),(245,'Integrator','Integrator',NULL,234,3,268),(246,'Integrator','Integrator',NULL,235,3,269),(247,'Integrator','Integrator',NULL,236,3,270),(248,'Integrator','Integrator',NULL,237,3,271),(249,'Integrator','Integrator',NULL,238,3,272),(250,'Integrator','Integrator',NULL,239,3,273),(251,'Integrator','Integrator',NULL,240,3,274),(252,'Integrator','Integrator',NULL,241,3,275),(253,'Integrator','Integrator',NULL,242,3,276),(254,'Integrator','Integrator',NULL,243,3,277),(255,'Integrator','Integrator',NULL,244,3,278),(256,'Integrator','Integrator',NULL,245,3,279),(257,'Integrator','Integrator',NULL,246,3,280),(258,'Integrator','Integrator',NULL,247,3,281),(259,'Integrator','Integrator',NULL,248,3,282),(260,'Integrator','Integrator',NULL,249,3,283),(261,'Integrator','Integrator',NULL,250,3,284),(262,'Integrator','Integrator',NULL,251,3,285),(263,'Integrator','Integrator',NULL,252,3,286),(264,'Integrator','Integrator',NULL,253,3,287),(265,'Integrator','Integrator',NULL,254,3,288),(266,'Integrator','Integrator',NULL,255,3,289),(267,'Integrator','Integrator',NULL,256,3,290),(268,'Integrator','Integrator',NULL,257,3,291),(269,'Integrator','Integrator',NULL,258,3,292),(270,'Integrator','Integrator',NULL,259,3,293),(271,'Integrator','Integrator',NULL,260,3,294),(272,'Integrator','Integrator',NULL,261,3,295),(273,'Integrator','Integrator',NULL,262,3,296),(274,'Integrator','Integrator',NULL,263,3,297),(275,'Integrator','Integrator',NULL,264,3,298),(276,'Integrator','Integrator',NULL,265,3,299),(277,'Integrator','Integrator',NULL,266,3,300),(278,'Integrator','Integrator',NULL,267,3,301),(279,'Integrator','Integrator',NULL,268,3,302),(280,'Integrator','Integrator',NULL,269,3,303),(281,'Integrator','Integrator',NULL,270,3,304),(282,'Integrator','Integrator',NULL,271,3,305),(283,'Integrator','Integrator',NULL,272,3,306),(284,'Integrator','Integrator',NULL,273,3,307),(285,'Integrator','Integrator',NULL,274,3,308),(286,'Integrator','Integrator',NULL,275,3,309),(287,'Integrator','Integrator',NULL,276,3,310),(288,'Integrator','Integrator',NULL,277,3,311),(289,'Integrator','Integrator',NULL,278,3,312),(290,'Integrator','Integrator',NULL,279,3,313),(291,'Integrator','Integrator',NULL,280,3,314),(292,'Integrator','Integrator',NULL,281,3,315),(293,'Integrator','Integrator',NULL,282,3,316),(294,'Integrator','Integrator',NULL,283,3,317),(295,'Integrator','Integrator',NULL,284,3,318),(296,'Integrator','Integrator',NULL,285,3,319),(297,'Integrator','Integrator',NULL,286,3,320),(298,'Integrator','Integrator',NULL,287,3,321),(299,'Integrator','Integrator',NULL,288,3,322),(300,'Integrator','Integrator',NULL,289,3,323),(301,'Integrator','Integrator',NULL,290,3,324),(302,'Integrator','Integrator',NULL,291,3,325),(303,'Integrator','Integrator',NULL,292,3,326),(304,'Integrator','Integrator',NULL,293,3,327),(305,'Integrator','Integrator',NULL,294,3,328),(306,'Integrator','Integrator',NULL,295,3,329),(307,'Integrator','Integrator',NULL,296,3,330),(308,'Integrator','Integrator',NULL,297,3,331),(309,'Integrator','Integrator',NULL,298,3,332),(310,'Integrator','Integrator',NULL,299,3,333),(311,'Integrator','Integrator',NULL,300,3,334),(312,'Integrator','Integrator',NULL,301,3,335),(313,'Integrator','Integrator',NULL,302,3,336),(314,'Integrator','Integrator',NULL,303,3,337),(315,'Integrator','Integrator',NULL,304,3,338),(316,'Integrator','Integrator',NULL,305,3,339),(317,'Integrator','Integrator',NULL,306,3,340),(318,'Integrator','Integrator',NULL,307,3,341),(319,'Integrator','Integrator',NULL,308,3,342),(320,'Integrator','Integrator',NULL,309,3,343),(321,'Integrator','Integrator',NULL,310,3,344),(322,'Integrator','Integrator',NULL,311,3,345),(323,'Integrator','Integrator',NULL,312,3,346),(324,'Integrator','Integrator',NULL,313,3,347),(325,'Integrator','Integrator',NULL,314,3,348),(326,'Integrator','Integrator',NULL,315,3,349),(327,'Integrator','Integrator',NULL,316,3,350),(328,'Integrator','Integrator',NULL,317,3,351),(329,'Integrator','Integrator',NULL,318,3,352),(330,'Integrator','Integrator',NULL,319,3,353),(331,'Integrator','Integrator',NULL,320,3,354),(332,'Integrator','Integrator',NULL,321,3,355),(333,'Integrator','Integrator',NULL,322,3,356),(334,'Integrator','Integrator',NULL,323,3,357),(335,'Integrator','Integrator',NULL,324,3,358),(336,'Integrator','Integrator',NULL,325,3,359),(337,'Integrator','Integrator',NULL,326,3,360),(338,'Integrator','Integrator',NULL,327,3,361),(339,'Integrator','Integrator',NULL,328,3,362),(340,'Integrator','Integrator',NULL,329,3,363),(341,'Integrator','Integrator',NULL,330,3,364),(342,'Integrator','Integrator',NULL,331,3,365),(343,'Integrator','Integrator',NULL,332,3,366),(344,'Integrator','Integrator',NULL,333,3,367),(345,'Integrator','Integrator',NULL,334,3,368),(346,'Integrator','Integrator',NULL,335,3,369),(347,'Integrator','Integrator',NULL,336,3,370),(348,'Integrator','Integrator',NULL,337,3,371),(349,'Integrator','Integrator',NULL,338,3,372),(350,'Integrator','Integrator',NULL,339,3,373),(351,'Integrator','Integrator',NULL,340,3,374),(352,'Integrator','Integrator',NULL,341,3,375),(353,'Integrator','Integrator',NULL,342,3,376),(354,'Integrator','Integrator',NULL,343,3,377),(355,'Integrator','Integrator',NULL,344,3,378),(356,'Integrator','Integrator',NULL,345,3,379),(357,'Integrator','Integrator',NULL,346,3,380),(358,'Integrator','Integrator',NULL,347,3,381),(359,'Integrator','Integrator',NULL,348,3,382),(360,'Integrator','Integrator',NULL,349,3,383),(361,'Integrator','Integrator',NULL,350,3,384),(362,'Integrator','Integrator',NULL,351,3,385),(363,'Integrator','Integrator',NULL,352,3,386),(364,'Integrator','Integrator',NULL,353,3,387),(365,'Integrator','Integrator',NULL,354,3,388),(366,'Integrator','Integrator',NULL,355,3,389),(367,'Integrator','Integrator',NULL,356,3,390),(368,'Integrator','Integrator',NULL,357,3,391),(369,'Integrator','Integrator',NULL,358,3,392),(370,'Integrator','Integrator',NULL,359,3,393),(371,'Integrator','Integrator',NULL,360,3,394),(372,'Integrator','Integrator',NULL,361,3,395),(373,'Integrator','Integrator',NULL,362,3,396),(374,'test','test',NULL,363,3,397),(375,'Integrator','Integrator',NULL,364,3,398),(376,'Integrator','Integrator',NULL,365,3,399),(377,'Integrator','Integrator',NULL,366,3,400),(378,'Integrator','Integrator',NULL,367,3,401),(379,'Integrator','Integrator',NULL,368,3,402),(380,'Integrator','Integrator',NULL,369,3,403),(381,'Integrator','Integrator',NULL,370,3,404),(382,'Integrator','Integrator',NULL,371,3,405),(383,'Integrator','Integrator',NULL,372,3,406),(384,'Integrator','Integrator',NULL,373,3,407),(385,'Integrator','Integrator',NULL,374,3,408),(386,'Integrator','Integrator',NULL,375,3,409),(387,'Integrator','Integrator',NULL,376,3,410),(388,'Integrator','Integrator',NULL,377,3,411),(389,'Integrator','Integrator',NULL,378,3,412),(390,'Integrator','Integrator',NULL,379,3,413),(391,'Integrator','Integrator',NULL,380,3,414),(392,'Integrator','Integrator',NULL,381,3,415),(393,'Integrator','Integrator',NULL,382,3,416),(394,'Integrator','Integrator',NULL,383,3,417),(395,'Integrator','Integrator',NULL,384,3,418),(396,'Integrator','Integrator',NULL,385,3,419),(397,'Integrator','Integrator',NULL,386,3,420),(398,'Integrator','Integrator',NULL,387,3,421),(399,'Integrator','Integrator',NULL,388,3,422),(400,'Integrator','Integrator',NULL,389,3,423),(401,'Integrator','Integrator',NULL,390,3,424),(402,'Integrator','Integrator',NULL,391,3,425),(403,'Integrator','Integrator',NULL,392,3,426),(404,'Integrator','Integrator',NULL,393,3,427),(405,'Integrator','Integrator',NULL,394,3,428),(406,'Integrator','Integrator',NULL,395,3,429),(407,'Integrator','Integrator',NULL,396,3,430),(408,'Integrator','Integrator',NULL,397,3,431),(409,'Integrator','Integrator',NULL,398,3,432),(410,'Integrator','Integrator',NULL,399,3,433),(411,'Integrator','Integrator',NULL,400,3,434),(412,'Integrator','Integrator',NULL,401,3,435),(413,'Integrator','Integrator',NULL,402,3,436),(414,'Integrator','Integrator',NULL,403,3,437),(415,'Integrator','Integrator',NULL,404,3,438),(416,'Integrator','Integrator',NULL,405,3,439),(417,'Integrator','Integrator',NULL,406,3,440),(418,'Integrator','Integrator',NULL,407,3,441),(419,'Integrator','Integrator',NULL,408,3,442),(420,'Integrator','Integrator',NULL,409,3,443),(421,'Integrator','Integrator',NULL,410,3,444),(422,'Integrator','Integrator',NULL,411,3,445),(423,'Integrator','Integrator',NULL,412,3,446),(424,'Integrator','Integrator',NULL,413,3,447),(425,'Integrator','Integrator',NULL,414,3,448),(426,'Integrator','Integrator',NULL,415,3,449),(427,'Integrator','Integrator',NULL,416,3,450),(428,'Integrator','Integrator',NULL,417,3,451),(429,'Integrator','Integrator',NULL,418,3,452),(430,'Integrator','Integrator',NULL,419,3,453),(431,'Integrator','Integrator',NULL,420,3,454),(432,'Integrator','Integrator',NULL,421,3,455),(433,'Integrator','Integrator',NULL,422,3,456),(434,'Integrator','Integrator',NULL,423,3,457),(435,'Integrator','Integrator',NULL,424,3,458),(436,'Integrator','Integrator',NULL,425,3,459),(437,'Integrator','Integrator',NULL,426,3,460),(438,'Integrator','Integrator',NULL,427,3,461),(439,'Integrator','Integrator',NULL,428,3,462),(440,'Integrator','Integrator',NULL,429,3,463),(441,'Integrator','Integrator',NULL,430,3,464),(442,'Integrator','Integrator',NULL,431,3,465),(443,'Integrator','Integrator',NULL,432,3,466),(444,'Integrator','Integrator',NULL,433,3,467),(445,'Integrator','Integrator',NULL,434,3,468),(446,'Integrator','Integrator',NULL,435,3,469),(447,'Integrator','Integrator',NULL,436,3,470),(448,'Integrator','Integrator',NULL,437,3,471),(449,'Integrator','Integrator',NULL,438,3,472),(450,'Integrator','Integrator',NULL,439,3,473),(451,'Integrator','Integrator',NULL,440,3,474),(452,'Integrator','Integrator',NULL,441,3,475),(453,'Integrator','Integrator',NULL,442,3,476),(454,'Integrator','Integrator',NULL,443,3,477),(455,'Integrator','Integrator',NULL,444,3,478),(456,'Integrator','Integrator',NULL,445,3,479),(457,'Integrator','Integrator',NULL,446,3,480),(458,'Integrator','Integrator',NULL,447,3,481),(459,'Integrator','Integrator',NULL,448,3,482),(460,'Integrator','Integrator',NULL,449,3,483),(461,'Integrator','Integrator',NULL,450,3,484),(462,'Integrator','Integrator',NULL,451,3,485),(463,'Integrator','Integrator',NULL,452,3,486),(464,'Integrator','Integrator',NULL,453,3,487),(465,'Integrator','Integrator',NULL,454,3,488),(466,'Integrator','Integrator',NULL,455,3,489),(467,'Integrator','Integrator',NULL,456,3,490),(468,'Integrator','Integrator',NULL,457,3,491),(469,'Integrator','Integrator',NULL,458,3,492),(470,'Integrator','Integrator',NULL,459,3,493),(471,'Integrator','Integrator',NULL,460,3,494),(472,'Integrator','Integrator',NULL,461,3,495),(473,'Integrator','Integrator',NULL,462,3,496);
/*!40000 ALTER TABLE `EM_USERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pac_penalty`
--

DROP TABLE IF EXISTS `pac_penalty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pac_penalty` (
  `id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `violation_id` varchar(45) NOT NULL,
  `value` float NOT NULL,
  `obligated_party` varchar(45) NOT NULL,
  `description` varchar(45) default NULL,
  PRIMARY KEY  (`id`),
  KEY `violation_id` (`violation_id`),
  CONSTRAINT `violation_id` FOREIGN KEY (`violation_id`) REFERENCES `pac_slaviolation` (`violation_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pac_penalty`
--

LOCK TABLES `pac_penalty` WRITE;
/*!40000 ALTER TABLE `pac_penalty` DISABLE KEYS */;
/*!40000 ALTER TABLE `pac_penalty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pac_slaviolation`
--

DROP TABLE IF EXISTS `pac_slaviolation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pac_slaviolation` (
  `violation_id` varchar(45) NOT NULL,
  `sla_id` varchar(45) character set utf8 NOT NULL,
  `guaranteeterm_id` varchar(45) NOT NULL,
  `date_begin` datetime NOT NULL,
  `date_end` datetime default NULL,
  PRIMARY KEY  (`violation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pac_slaviolation`
--

LOCK TABLES `pac_slaviolation` WRITE;
/*!40000 ALTER TABLE `pac_slaviolation` DISABLE KEYS */;
/*!40000 ALTER TABLE `pac_slaviolation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pac_slawarning`
--

DROP TABLE IF EXISTS `pac_slawarning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pac_slawarning` (
  `warning_id` int(11) NOT NULL auto_increment,
  `sla_id` varchar(45) default NULL,
  `date_warning` datetime NOT NULL,
  `notification_id` varchar(10) NOT NULL,
  PRIMARY KEY  (`warning_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pac_slawarning`
--

LOCK TABLES `pac_slawarning` WRITE;
/*!40000 ALTER TABLE `pac_slawarning` DISABLE KEYS */;
/*!40000 ALTER TABLE `pac_slawarning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'slasoi'
--

--
-- Current Database: `slasoi`
--

USE `slasoi`;

--
-- Final view structure for view `EM_OFFER_RATING`
--

/*!50001 DROP TABLE `EM_OFFER_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_OFFER_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `EM_OFFER_RATING` AS (select `cp`.`NU_IDPRODUCTOFFERING` AS `NU_IDPRODUCTOFFERING`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from (((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `cp`.`NU_IDPRODUCTOFFERING`) */;

--
-- Final view structure for view `EM_PRODUCT_RATING`
--

/*!50001 DROP TABLE `EM_PRODUCT_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_PRODUCT_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `EM_PRODUCT_RATING` AS (select `offer`.`NU_PRODUCTID` AS `NU_PRODUCTID`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from ((((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer_slat`) join `EM_SP_PRODUCTS_OFFER` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer_slat`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`offer`.`NU_IDPRODUCTOFFERING` = `offer_slat`.`NU_IDPRODUCTOFFERING`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer_slat`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `offer`.`NU_PRODUCTID`) */;

--
-- Final view structure for view `EM_SERVICE_RATING`
--

/*!50001 DROP TABLE `EM_SERVICE_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_SERVICE_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `EM_SERVICE_RATING` AS (select `offer`.`NU_SERVICEID` AS `NU_SERVICEID`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from (((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `offer`.`NU_SERVICEID`) */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-07-31 10:17:14
