/**
 * 
 */
package org.slasoi.businessManager.reporting.parser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import org.slaatsoi.business.schema.BusinessTerm;
import org.slaatsoi.business.schema.ReportingTargetType;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.database.PersistentEntity;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class ReportingPolicy implements PersistentEntity, Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3519199864878011989L;
	
	private String reportingPolicyId;
	private String slaId;
	private ArrayList<ReportingTarget> reportingTargets;
	private Date creationDate;
	private SLA sla;
	private BusinessTerm businessTerm;

	/**
	 * @param slaId
	 * @param agreementTermId
	 * @param guaranteedTermId
	 * @param businessTerm
	 */
	public ReportingPolicy(SLA sla, BusinessTerm businessTerm) {
		this.slaId = sla.getUuid().getValue();
		this.reportingPolicyId = businessTerm.getReporting().getReportId();
		
		reportingTargets = new ArrayList<ReportingTarget>();
		for (ReportingTargetType reportingTargetType : businessTerm.getReporting().getReportingTargets().getReportingTarget()) {
			ReportingTarget reportingTarget = new ReportingTarget(
					reportingTargetType.getFunctionalAggregator(),
					reportingTargetType.getSlaId(),
					reportingTargetType.getAgreementTermId(),
					reportingTargetType.getGuaranteedId()
					);
			reportingTargets.add(reportingTarget);
		}
		
		this.creationDate = new Date();
		this.sla = sla;
		this.businessTerm = businessTerm;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the reportingPolicyId
	 */
	public String getReportingPolicyId() {
		return reportingPolicyId;
	}
	
	/**
	 * @return the slaId
	 */
	public String getSlaId() {
		return slaId;
	}

	/**
	 * @return the reportingTargets
	 */
	public ArrayList<ReportingTarget> getReportingTargets() {
		return reportingTargets;
	}
	
	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * @return the test.org.slasoi.businessManager.reporting.utils
	 */
	public SLA getSla() {
		return sla;
	}
	
	/**
	 * @return the businessTerm
	 */
	public BusinessTerm getBusinessTerm() {
		return businessTerm;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
