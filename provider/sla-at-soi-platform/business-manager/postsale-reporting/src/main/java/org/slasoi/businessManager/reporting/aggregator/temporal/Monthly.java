/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class Monthly extends TemporalAggregator {
	private int repeatEvery;
	private RepeatBy repeatBy;

	/**
	 * @param startsOn
	 * @param ends
	 * @param repeatEvery
	 * @param repeatBy
	 */
	public Monthly(Date startsOn, Ends ends, int repeatEvery, RepeatBy repeatBy) {
		super(startsOn, ends);
		this.repeatEvery = repeatEvery;
		this.repeatBy = repeatBy;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the repeatEvery
	 */
	public int getRepeatEvery() {
		return repeatEvery;
	}
	
	/**
	 * @return the repeatBy
	 */
	public RepeatBy getRepeatBy() {
		return repeatBy;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"startsOn=" + getStartsOn() + "," +
				"ends=" + getEnds() + "," +
				"repeatEvery=" + repeatEvery + "," +
				"repeatBy=" + repeatBy + "}"; 
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
