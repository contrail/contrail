/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.functional;

import java.util.ArrayList;

import org.slasoi.businessManager.reporting.store.MonitoringResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public interface FunctionalAggregator {
	
	/**
	 * @param monitoringResults
	 * @return
	 */
	public double execute(ArrayList<MonitoringResult> monitoringResults);
}
