/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.DateIntervalTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.slasoi.businessManager.reporting.aggregator.temporal.Daily;
import org.slasoi.businessManager.reporting.aggregator.temporal.Ends;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsAfterOccurrences;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsNever;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsOn;
import org.slasoi.businessManager.reporting.aggregator.temporal.Hourly;
import org.slasoi.businessManager.reporting.aggregator.temporal.Minutely;
import org.slasoi.businessManager.reporting.aggregator.temporal.Monthly;
import org.slasoi.businessManager.reporting.aggregator.temporal.RepeatBy;
import org.slasoi.businessManager.reporting.aggregator.temporal.Secondly;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregator;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekday;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekdays;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekend;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekly;
import org.slasoi.businessManager.reporting.aggregator.temporal.Yearly;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class TriggerFactory {
	private static Logger logger = Logger.getLogger(TriggerFactory.class);
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Given a temporal aggregator it creates the necessary Trigger object.
	 * 
	 * Note: the current implementation supports only the EndsOn object as value
	 *       for the filed ends of the TemporalAggregator object
	 *        
	 * @param triggerId
	 * @param triggerGroup
	 * @param temporalAggregator
	 * @return
	 */
	public static ArrayList<Trigger> createTrigger(String triggerId, String triggerGroup, TemporalAggregator temporalAggregator) {
		ArrayList<Trigger> triggers = new ArrayList<Trigger>();
		
		Calendar startsOn = Calendar.getInstance();
		startsOn.setTime(temporalAggregator.getStartsOn());
		
		// set default values
		String seconds = String.valueOf(startsOn.get(Calendar.SECOND));
		String minutes = String.valueOf(startsOn.get(Calendar.MINUTE));
		String hours = String.valueOf(startsOn.get(Calendar.HOUR));
		String dayOfMonth = "*";
		String month = "*";
		String dayOfWeek = "?";
		String year = "*";
		
		// Secondly
		if (temporalAggregator instanceof Secondly) {
			int repeatEvery = ((Secondly) temporalAggregator).getRepeatEvery();
			triggers.add(new DateIntervalTrigger(triggerId, triggerGroup, DateIntervalTrigger.IntervalUnit.SECOND, repeatEvery));
		}
		// Minutely
		else if (temporalAggregator instanceof Minutely) {
			int repeatEvery = ((Minutely) temporalAggregator).getRepeatEvery();
			triggers.add(new DateIntervalTrigger(triggerId, triggerGroup, DateIntervalTrigger.IntervalUnit.MINUTE, repeatEvery));
		}
		// Hourly
		else if (temporalAggregator instanceof Hourly) {
			int repeatEvery = ((Hourly) temporalAggregator).getRepeatEvery();
			triggers.add(new DateIntervalTrigger(triggerId, triggerGroup, DateIntervalTrigger.IntervalUnit.HOUR, repeatEvery));
		}
		// Daily
		else if (temporalAggregator instanceof Daily) {
			int repeatEvery = ((Daily) temporalAggregator).getRepeatEvery();
			triggers.add(new DateIntervalTrigger(triggerId, triggerGroup, DateIntervalTrigger.IntervalUnit.DAY, repeatEvery));
		}
		// Weekday
		else if (temporalAggregator instanceof Weekday) {
			dayOfMonth = "?";
			dayOfWeek = "MON-FRI";
		}
		// Weekend
		else if (temporalAggregator instanceof Weekend) {
			dayOfMonth = "?";
			dayOfWeek = "SAT-SUN";
		}
		// Weekly
		else if (temporalAggregator instanceof Weekly) {
			// Note: at the moment it doesn't support the "repeat every" option
			// //int repeatEvery = ((Weekly) temporalAggregator).getRepeatEvery();
			dayOfMonth = "?";
			dayOfWeek =  getWeekdays(((Weekly) temporalAggregator).getRepeatOn());
			
		}
		// Monthly
		else if (temporalAggregator instanceof Monthly) {
			// Note: at the moment it doesn't support the "repeat every" option
			//int repeatEvery = ((Monthly) temporalAggregator).getRepeatEvery();
			
			RepeatBy repeatBy = ((Monthly) temporalAggregator).getRepeatBy();
			
			if (repeatBy.equals(RepeatBy.day_of_the_month)) {
				dayOfWeek = "?";
				dayOfMonth = String.valueOf(startsOn.get(Calendar.DAY_OF_MONTH));
			}
			else if (repeatBy.equals(RepeatBy.day_of_the_week)) {
				dayOfWeek = String.valueOf(startsOn.get(Calendar.DAY_OF_WEEK));
				dayOfMonth = "?";
			}
			
		}
		// Yearly
		else if (temporalAggregator instanceof Yearly) {
			int repeatEvery = ((Yearly) temporalAggregator).getRepeatEvery();
			triggers.add(new DateIntervalTrigger(triggerId, triggerGroup, DateIntervalTrigger.IntervalUnit.YEAR, repeatEvery));
		}
		
		// initiate the cron. expression
		String cronExpression = MessageFormat.format("{0} {1} {2} {3} {4} {5} {6}",
				seconds,	// seconds
				minutes,	// minutes
				hours,		// hours
				dayOfMonth,	// day-of-month
				month,		// month
				dayOfWeek,	// day-of-week
				year);		// year
		
		try {
			// if a specific trigger hasn't been created, it creates a CronTrigger
			if (triggers.isEmpty()) {
				//logger.debug("Cron expression: " + cronExpression);
				
				CronTrigger cronTrigger = new CronTrigger(triggerId, triggerGroup, cronExpression);
				triggers.add(cronTrigger);
			}
			
			// set starting and ending dates
			for (Trigger trigger : triggers) {
				trigger.setStartTime(temporalAggregator.getStartsOn());
				Ends ends = temporalAggregator.getEnds();
				
				if (ends instanceof EndsOn) {
					trigger.setEndTime(((EndsOn) ends).getDate());
				}
				else if (ends instanceof EndsNever) {
					// TO DO
				}
				else if (ends instanceof EndsAfterOccurrences) {
					int afterOccurrences = ((EndsAfterOccurrences) ends).getOccurrences();
					List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, afterOccurrences);
					Date endsOn = dates.get(dates.size()-1);
					trigger.setEndTime(endsOn);
				}

			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return triggers;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param weekdays
	 * @return
	 */
	private static String getWeekdays(Weekdays[] weekdays) {
		StringBuilder result = new StringBuilder();
		
		for (int i=0; i<weekdays.length; i++) {
			if (i != (weekdays.length-1)) {
				result.append(weekdays[i].name() + ",");
			} else {
				result.append(weekdays[i].name());
			}
		}
		
		return result.toString();
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
