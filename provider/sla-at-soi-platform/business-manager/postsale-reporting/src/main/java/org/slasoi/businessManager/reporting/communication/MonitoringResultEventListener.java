/**
 * 
 */
package org.slasoi.businessManager.reporting.communication;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 30, 2011
 */
public class MonitoringResultEventListener implements MessageListener {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	MonitoringResultEntityManager monitoringResultEntityManager;
	MonitoringEventServiceStub monitoringEventServiceStub;
	
	/**
	 * @param monitoringResultBuffer
	 */
	public MonitoringResultEventListener(MonitoringResultEntityManager monitoringResultEntityManager) {
		this.monitoringEventServiceStub = new MonitoringEventServiceStub();
		this.monitoringResultEntityManager = monitoringResultEntityManager;
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.common.messaging.pubsub.MessageListener#processMessage(org.slasoi.common.messaging.pubsub.MessageEvent)
	 */
	
	public void processMessage(MessageEvent messageEvent) {
		try {
			EventInstance eventInstance = monitoringEventServiceStub.unmarshall(messageEvent.getMessage().getPayload());
			monitoringResultEntityManager.insert(MonitoringResult.createInstance(eventInstance));
			
			logger.debug("Inserted event: " + eventInstance.getEventID().getID());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
