/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.AssessmentResultType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.GuaranteedType;
import org.slasoi.common.eventschema.GuatanteedStateType;
import org.slasoi.common.eventschema.SLAType;

import uk.ac.city.soi.database.PersistentEntity;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public class MonitoringResult implements PersistentEntity, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7476798237755109651L;

	// logger
	private transient Logger logger = Logger.getLogger(getClass());
	
	private EventInstance eventInstance;
	private Date timestamp;
	private String slaId;
	private String agreementTermId;
	private String guaranteedId;
	
	/**
	 * @param timestamp
	 * @param qosTerm
	 */
	private MonitoringResult(EventInstance eventInstance) {		
		this.eventInstance = eventInstance;
		
		SLAType slaInfo = eventInstance.getEventPayload().getMonitoringResultEvent().getSLAInfo();
		
		// set variables
		this.timestamp = new Date(eventInstance.getEventContext().getTime().getTimestamp());
		this.slaId = slaInfo.getSlaUUID();
		this.agreementTermId = slaInfo.getAgreementTerm().get(0).getAgreementTermID();
		this.guaranteedId = slaInfo.getAgreementTerm().get(0).getGuaranteedStateOrGuaranteedAction().get(0).getGuaranteedID();
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	public static MonitoringResult createInstance(EventInstance eventInstance) throws Exception {
		return new MonitoringResult(eventInstance);
	}
	
	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return new Date(eventInstance.getEventContext().getTime().getTimestamp());
	}

	/**
	 * @return the slaId
	 */
	public String getSlaId() {
		return slaId;
	}
	
	/**
	 * @return the agreementTermId
	 */
	public String getAgreementTermId() {
		return agreementTermId;
	}
	
	/**
	 * @return the guaranteedId
	 */
	public String getGuaranteedId() {
		return guaranteedId;
	}
	
	/**
	 * @return the eventInstance
	 */
	public EventInstance getEventInstance() {
		return eventInstance;
	}
	
	/**
	 * @return
	 */
	public GuatanteedStateType getGuaranteedState() {
		GuaranteedType guaranteed = eventInstance.getEventPayload().getMonitoringResultEvent().getSLAInfo().getAgreementTerm().get(0).getGuaranteedStateOrGuaranteedAction().get(0);
		
		if (guaranteed instanceof GuatanteedStateType) {
			return (GuatanteedStateType) guaranteed;
		} else {
			return null;
		}
	}
	
	/**
	 * @return Double.NaN value if the guaranteed state does not exist or the
	 *         value is contains is not a number
	 */
	public double getGuaranteedStateQoSValue() {
		GuaranteedType guaranteed = eventInstance.getEventPayload().getMonitoringResultEvent().getSLAInfo().getAgreementTerm().get(0).getGuaranteedStateOrGuaranteedAction().get(0);
		double result = Double.NaN;
		
		if (guaranteed instanceof GuatanteedStateType) {
			try {
				result = Double.parseDouble(((GuatanteedStateType) guaranteed).getQoSValue());
			} catch (NumberFormatException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public AssessmentResultType getSLAAssessmentResult() {
		return eventInstance.getEventPayload().getMonitoringResultEvent().getSLAInfo().getAssessmentResult();
	}
	
	/**
	 * @param monitoringResult
	 * @return True if SLA id, agreement term id, and guaranteed id are equal
	 */
	public boolean equals(MonitoringResult monitoringResult) {
		if (!this.getSlaId().equals(monitoringResult.getSlaId())) return false;
		if (!this.getAgreementTermId().equals(monitoringResult.getAgreementTermId())) return false;
		if (!this.getGuaranteedId().equals(monitoringResult.getGuaranteedId())) return false;
		
		return true;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"timestamp=" + timestamp + "," +
				"slaId=" + timestamp + "," +
				"agreementTermId=" + agreementTermId + "" +
				"guaranteedId=" + guaranteedId + "" +
				"eventInstance=" + eventInstance + "}";
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
