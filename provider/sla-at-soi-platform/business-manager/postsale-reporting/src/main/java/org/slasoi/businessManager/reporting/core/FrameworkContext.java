/**
 * 
 */
package org.slasoi.businessManager.reporting.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.store.MonitoringResultMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 31, 2011
 */
public class FrameworkContext {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	/** enumeration containing names for retrieving framework properties **/
	public enum PropertiesFileNames {
		XMPP {
			
			public String toString() {
				return "xmpp.properties";
			}
		},
		QUARTZ {
			
			public String toString() {
				return "quartz.properties";
			}
		},
		FRAMEWORK {
			
			public String toString() {
				return "framework.properties";
			}
		},
		MYSQL {
			
			public String toString() {
				return "DB" + FILE_SEPARATOR + "db.properties";
			}
		},
		JAVAMAIL {
			
			public String toString() {
				return "javamail.properties";
			}
		};
		
		public abstract String toString();
	}
	
	/** enumeration containing names of the database manager classes **/
	public enum DatabaseManagers {
		MONITORING_RESULT_EVENTS {
			
			public String toString() {
				return MonitoringResultMySQLEntityManager.class.getName();
			}
		},
		REPORTS {
			
			public String toString() {
				return ReportMySQLEntityManager.class.getName();
			}
		},
		REPORTING_POLICIES {
			
			public String toString() {
				return ReportingPolicyMySQLEntityManager.class.getName();
			}
		}
	};
	
	private Hashtable<PropertiesFileNames, Properties> frameworkProperties;
	private Hashtable<DatabaseManagers, EntityManagerInterface> databaseManagers;
	private String frameworkConfigurationfolder;
	
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
	// ------------------------------------------------------------------------
	// 								CONSTRUCTORS
	// ------------------------------------------------------------------------
	
	/**
	 * @param frameworkConfigurationfolder The path to the folder DOES NOT terminates
	 * 									   with the 'file.separator' symbol, e.g., '/'
	 */
	public FrameworkContext(String frameworkConfigurationfolder) {
		this.frameworkConfigurationfolder = frameworkConfigurationfolder;
		
		if (frameworkProperties == null) {
			this.frameworkProperties = loadProperties(frameworkConfigurationfolder);
		}
		if (databaseManagers == null) {
			this.databaseManagers = loadDatabaseManagers(frameworkConfigurationfolder);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return the frameworkConfigurationfolder
	 */
	public String getFrameworkConfigurationfolder() {
		return frameworkConfigurationfolder;
	}
	
	/**
	 * @param propertiesFileName
	 * @return <code>null</code> if the property doesn't exist or the
	 *         FrameworkContext was not initialised
	 */
	public Properties getFrameworkProperties(PropertiesFileNames propertiesFileName) {
		return frameworkProperties == null ? null: frameworkProperties.get(propertiesFileName);
	}
	
	/**
	 * @param databaseManagerName
	 * @return <code>null</code> if the database manager doesn't exist or the
	 *         FrameworkContext was not initialised
	 */
	public EntityManagerInterface getDatabaseManager(DatabaseManagers databaseManagerName) {
		return databaseManagers == null ? null : databaseManagers.get(databaseManagerName);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param frameworkConfigurationFolder
	 * @return
	 */
	private Hashtable<PropertiesFileNames, Properties> loadProperties(String frameworkConfigurationFolder) {
		Hashtable<PropertiesFileNames, Properties> properties = new Hashtable<PropertiesFileNames, Properties>();
		
		
		for (PropertiesFileNames propertiesFileName : PropertiesFileNames.values()) {
			String propertiesFile = frameworkConfigurationFolder + FILE_SEPARATOR + propertiesFileName.toString();
			Properties currentProperties = new Properties();
			try {
				// load properties
				currentProperties.load(new FileInputStream(propertiesFile));
				// add properties to properties hashtable
				properties.put(propertiesFileName, currentProperties);
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(), e);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return properties;
	}
	
	/**
	 * @param frameworkConfigurationFolder
	 * @return
	 */
	private Hashtable<DatabaseManagers, EntityManagerInterface> loadDatabaseManagers(String frameworkConfigurationFolder) {
		Hashtable<DatabaseManagers, EntityManagerInterface> databaseManagers = new Hashtable<FrameworkContext.DatabaseManagers, EntityManagerInterface>();
		
		// get mysql.properties file
		String propertiesFile = frameworkConfigurationFolder + FILE_SEPARATOR + PropertiesFileNames.MYSQL.toString();
		// initiate entity manager factory
		EntityManagerFactoryInterface entityManagerFactory = DatabaseManagerFactory.getDatabaseManager(propertiesFile).getEntityManagerFactory();
		
		for (DatabaseManagers databaseManager : DatabaseManagers.values()) {
			try {
				// load class
				//Class databaseManagerClass = getClass().getClassLoader().loadClass(databaseManager.toString());
				
				Class databaseManagerClass = Class.forName(databaseManager.toString());
					
				// create instance
				EntityManagerInterface entityManager = entityManagerFactory.getEntityManager(databaseManagerClass);
				// save instance
				logger.debug("Loaded Database Entity Manager: " + entityManager.getClass().getName());
				databaseManagers.put(databaseManager, entityManager);
			} catch (ClassNotFoundException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return databaseManagers;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
