/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 17, 2011
 */
public class FunctionalAggregatorResult {
	private String slaId;
	private String agreementTermId;
	private String guaranteedId;
	private String functionalAggregatorId;
	private double value;
	
	/**
	 * @param slaId
	 * @param agreementTermId
	 * @param guaranteedId
	 * @param functionalAggregatorId
	 * @param value
	 */
	public FunctionalAggregatorResult(String slaId, String agreementTermId, String guaranteedId, String functionalAggregatorId, double value) {
		this.slaId = slaId;
		this.agreementTermId = agreementTermId;
		this.guaranteedId = guaranteedId;
		this.functionalAggregatorId = functionalAggregatorId;
		this.value = value;
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return the slaId
	 */
	protected String getSlaId() {
		return slaId;
	}

	/**
	 * @return the agreementTermId
	 */
	protected String getAgreementTermId() {
		return agreementTermId;
	}

	/**
	 * @return the guaranteedId
	 */
	protected String getGuaranteedId() {
		return guaranteedId;
	}

	/**
	 * @return the functionalAggregatorId
	 */
	protected String getFunctionalAggregatorId() {
		return functionalAggregatorId;
	}

	/**
	 * @return the value
	 */
	protected double getValue() {
		return value;
	}	
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
