/**
 * 
 */
package org.slasoi.businessManager.reporting.report;

import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.common.reportschema.PostSaleReportType;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 18, 2011
 */
public class XMLReportMaker extends ReportMaker {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.businessManager.reporting.report.ReportMaker#crerateReport(java.lang.String)
	 */
	public Report createReport(JasperReport reportTemplate, String xmlPostSaleReport) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		Report report = null;
		
		try {
			// create a DOM document
			Document document = JRXmlUtils.parse(new InputSource(new StringReader(xmlPostSaleReport)));
			//Document document = JRXmlUtils.parse(JRLoader.getLocationInputStream(xmlDataSource));
			
			// set parameters
			params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
			params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
			params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
			params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
			params.put(JRParameter.REPORT_LOCALE, Locale.UK);
			
			// fill the report template with actual data
			JasperPrint jasperPrint = JasperFillManager.fillReport(reportTemplate, params);
			
			// unmarshall report XML and creates a report object
			PostSaleReportType postSaleReportType = (PostSaleReportType) XMLUtils.unmarshall(xmlPostSaleReport, null, PostSaleReportType.class);
			String reportId = postSaleReportType.getReportInfo().getReportId();
			String slaId = postSaleReportType.getMonitoringResult().getSLAInfo().getSlaUUID();
			Date creationDate = new Date(postSaleReportType.getReportInfo().getTimestamp().getMillisecond());
			ReportFormatType format = ReportFormatType.XML;
			Object reportContent = JasperExportManager.exportReportToXml(jasperPrint);
			
			// create the report
			report = new Report(reportId, slaId, creationDate, format, reportContent);
		} catch (JRException e) {
			logger.error(e.getMessage(), e);
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		}
		
		return report;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
