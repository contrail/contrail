/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 18, 2011
 */
public class MonitoringResultMySQLEntityManager extends EntityManagerCommons implements MonitoringResultEntityManager {
    // logger
    private static Logger logger = Logger.getLogger(MonitoringResultMySQLEntityManager.class);
    
	private static final String DATABASE_TABLE_NAME = "monitoring_result_events";
	
	// ------------------------------------------------------------------------
	// 								CONSTRUCTORS
	// ------------------------------------------------------------------------
	
	/**
	 * @param connection
	 */
	public MonitoringResultMySQLEntityManager(Connection connection) {
		super(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public MonitoringResultMySQLEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/** (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	
	public int update(MonitoringResult paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	
	public int delete(MonitoringResult paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	
	public int deleteByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	
	public MonitoringResult selectByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	
	public ArrayList<MonitoringResult> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> executeQuery(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#insert(org.slasoi.businessManager.reporting.store.MonitoringResult)
	 */
	
	public int insert(MonitoringResult monitoringResult) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;

        try {
            Blob blob = toBlob(monitoringResult);

            pstmt = getConnection().prepareStatement(
            		"INSERT INTO " + getDatabaseTable() + " " +
                    "(sla_id, agreement_term_id, guaranteed_id, timestamp, event_object) " +
                    "VALUES (?, ?, ?, ?, ?)");
            
            pstmt.setString(1, monitoringResult.getSlaId());
            pstmt.setString(2, monitoringResult.getAgreementTermId());
            pstmt.setString(3, monitoringResult.getGuaranteedId());
            pstmt.setLong(4, monitoringResult.getTimestamp().getTime());
            pstmt.setBlob(5, blob);

            result = pstmt.executeUpdate();
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        
        return result;
	}

	/* (non-Javadoc)
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND agreement_term_id = ? AND guaranteed_id = ? AND timestamp >= ? AND timestamp <= ?");
            
            pstmt.setString(1, slaId);
            pstmt.setString(2, agreementTermId);
            pstmt.setString(3, guaranteedId);
            pstmt.setLong(4, fromDate.getTime());
            pstmt.setLong(5, toDate.getTime());
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND agreement_term_id = ? AND guaranteed_id = ?");
            
            pstmt.setString(1, slaId);
            pstmt.setString(2, agreementTermId);
            pstmt.setString(3, guaranteedId);
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND guaranteed_id = ? AND timestamp >= ? AND timestamp <= ?");
            
            pstmt.setString(1, slaId);
            pstmt.setString(2, agreementTermId);
            pstmt.setLong(3, fromDate.getTime());
            pstmt.setLong(4, toDate.getTime());
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND agreement_term_id = ?");
            
            pstmt.setString(1, slaId);
            pstmt.setString(2, agreementTermId);
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND timestamp >= ? AND timestamp <= ?");
            
            pstmt.setString(1, slaId);
            pstmt.setLong(2, fromDate.getTime());
            pstmt.setLong(3, toDate.getTime());
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId) {
		ArrayList<MonitoringResult> monitoringResults = new ArrayList<MonitoringResult>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT event_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ?");
            
            pstmt.setString(1, slaId);
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("event_object");
                    MonitoringResult monitoringResult;
					try {
						monitoringResult = (MonitoringResult) toObject(blob);
						monitoringResults.add(monitoringResult);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return monitoringResults;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate) {
		return select(slaId, agreementTermId, guaranteedId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.lang.String)
	 */
	
	public int count(String slaId, String agreementTermId, String guaranteedId) {
		return select(slaId, agreementTermId, guaranteedId).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, String agreementTermId, Date fromDate, Date toDate) {
		return select(slaId, agreementTermId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String)
	 */
	
	public int count(String slaId, String agreementTermId) {
		return select(slaId, agreementTermId).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, Date fromDate, Date toDate) {
		return select(slaId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String)
	 */
	
	public int count(String slaId) {
		return select(slaId).size();
	}	

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
