/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

import org.slasoi.businessManager.reporting.aggregator.Aggregator;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class TemporalAggregator implements Aggregator {
	private Date startsOn;
	private Ends ends;
	
	/**
	 * @param startsOn
	 * @param ends
	 */
	public TemporalAggregator(Date startsOn, Ends ends) {
		this.startsOn = startsOn;
		this.ends = ends;
		
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the startsOn
	 */
	public Date getStartsOn() {
		return startsOn;
	}
	
	/**
	 * @return the ends
	 */
	public Ends getEnds() {
		return ends;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
