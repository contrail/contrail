/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 3, 2011
 */
public class Actor {
	private String actorId;
	private String[] phones;
	private String[] faxes;
	private String[] emails;

	/**
	 * It creates a new actor by initialising phones, faxes, and email
	 * to empty arrays. 
	 * 
	 * @param actorId
	 */
	public Actor(String actorId) {
		this.actorId = actorId;
		this.phones = new String[0];
		this.faxes = new String[0];
		this.emails = new String[0];
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the actorId
	 */
	public String getActorId() {
		return actorId;
	}

	/**
	 * @param actorId the actorId to set
	 */
	public void setActorId(String actorId) {
		this.actorId = actorId;
	}
	
	/**
	 * @return the phones
	 */
	public String[] getPhones() {
		return phones;
	}

	/**
	 * @param phones the phones to set
	 */
	public void setPhones(String[] phones) {
		this.phones = phones;
	}

	/**
	 * @return the faxes
	 */
	public String[] getFaxes() {
		return faxes;
	}

	/**
	 * @param faxes the faxes to set
	 */
	public void setFaxes(String[] faxes) {
		this.faxes = faxes;
	}

	/**
	 * @return the emails
	 */
	public String[] getEmails() {
		return emails;
	}

	/**
	 * @param emails the emails to set
	 */
	public void setEmails(String[] emails) {
		this.emails = emails;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
