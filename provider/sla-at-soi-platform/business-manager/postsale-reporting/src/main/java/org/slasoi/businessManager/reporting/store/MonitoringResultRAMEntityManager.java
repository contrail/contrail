/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;

import org.apache.log4j.Logger;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public class MonitoringResultRAMEntityManager implements MonitoringResultEntityManager {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	// This linked list defines the iteration ordering, which is the order
	// in which elements were inserted into the set (insertion-order).
	private static LinkedHashSet<MonitoringResult> monitoringResults;

	/**
	 * Initiate the data structure holding monitoring results. The data
	 * structure is declared static so it gets initialised once only
	 * even with multiple instances of MonitoringResultRAMEntityManager
	 */
	public MonitoringResultRAMEntityManager() {
		if (monitoringResults == null) {
			monitoringResults = new LinkedHashSet<MonitoringResult>();
		}
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId) && 
				monitoringResult.getAgreementTermId().equals(agreementTermId) &&
				monitoringResult.getGuaranteedId().equals(guaranteedId)) {
				
				Date timestamp = monitoringResult.getTimestamp();
				
				// check fromDate <= timestamp <= toDate
				if (timestamp.getTime() >= fromDate.getTime() && timestamp.getTime() <= toDate.getTime()) {
					result.add(monitoringResult);
				}
			}
		}
		
		return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId) && 
				monitoringResult.getAgreementTermId().equals(agreementTermId) &&
				monitoringResult.getGuaranteedId().equals(guaranteedId)) {
				
				result.add(monitoringResult);
			}
		}
		
		return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId) && 
				monitoringResult.getAgreementTermId().equals(agreementTermId)) {
				
				Date timestamp = monitoringResult.getTimestamp();
				
				// check fromDate <= timestamp <= toDate
				if (timestamp.getTime() >= fromDate.getTime() && timestamp.getTime() <= toDate.getTime()) {
					result.add(monitoringResult);
				}
			}
		}
		
		return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId) && 
				monitoringResult.getAgreementTermId().equals(agreementTermId)) {
				
				result.add(monitoringResult);
			}
		}
		
		return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId, Date fromDate, Date toDate) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId)) {
				
				Date timestamp = monitoringResult.getTimestamp();
				
				// check fromDate <= timestamp <= toDate
				if (timestamp.getTime() >= fromDate.getTime() && timestamp.getTime() <= toDate.getTime()) {
					result.add(monitoringResult);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#select(java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> select(String slaId) {
		ArrayList<MonitoringResult> result = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			// check ids
			if (monitoringResult.getSlaId().equals(slaId)) {
				
				result.add(monitoringResult);
			}
		}
		
		return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate) {
		return select(slaId, agreementTermId, guaranteedId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.lang.String)
	 */
	
	public int count(String slaId, String agreementTermId, String guaranteedId) {
		return select(slaId, agreementTermId, guaranteedId).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, String agreementTermId, Date fromDate, Date toDate) {
		return select(slaId, agreementTermId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.lang.String)
	 */
	
	public int count(String slaId, String agreementTermId) {
		return select(slaId, agreementTermId).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int count(String slaId, Date fromDate, Date toDate) {
		return select(slaId, fromDate, toDate).size();
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#count(java.lang.String)
	 */
	
	public int count(String slaId) {
		return select(slaId).size();
	}
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#insert(org.slasoi.businessManager.reporting.store.MonitoringResult)
	 */
	
	public int insert(MonitoringResult monitoringResult) {
		return monitoringResults.add(monitoringResult) ? 1 : 0;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager#truncate()
	 */
	
	public int truncate() {
		int elements = monitoringResults.size();
		monitoringResults = new LinkedHashSet<MonitoringResult>();
		return elements;
	}

	// ------------------------------------------------------------------------
	// 								UNIMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	
	public int update(MonitoringResult paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	
	public int delete(MonitoringResult paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	
	public int deleteByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	
	public MonitoringResult selectByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	
	public ArrayList<MonitoringResult> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	
	public ArrayList<MonitoringResult> executeQuery(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	
	public int executeUpdate(String paramString) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#setDatabaseTable(java.lang.String)
	 */
	
	public void setDatabaseTable(String paramString) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#getDatabaseTable()
	 */
	
	public String getDatabaseTable() {
		// TODO Auto-generated method stub
		return null;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
