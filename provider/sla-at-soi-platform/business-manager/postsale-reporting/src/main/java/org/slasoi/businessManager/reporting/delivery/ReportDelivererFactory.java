/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.DeliveryMethodType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 8, 2011
 */
public class ReportDelivererFactory {
	// logger
	private Logger logger = Logger.getLogger(getClass());

	Hashtable<DeliveryMethodType, ReportDeliverer> reportDeliverers;
	
	/**
	 * 
	 */
	public ReportDelivererFactory() {
		reportDeliverers = new Hashtable<DeliveryMethodType, ReportDeliverer>();
		
		// init report makers
		reportDeliverers.put(DeliveryMethodType.EMAIL, new EmailReportDeliverer());
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param deliveryMethod
	 * @return the created report deliverer, <code>null</code> if errors occurred 
	 */
	public ReportDeliverer createReportDeliverer(DeliveryMethodType deliveryMethod) {
		ReportDeliverer reportDeliverer = null;
		
		// check is suitable report maker exists
		if (reportDeliverers.containsKey(deliveryMethod)) {
			
			// retrieve the appropriate report maker
			reportDeliverer = reportDeliverers.get(deliveryMethod);
		}
		
		return reportDeliverer;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
