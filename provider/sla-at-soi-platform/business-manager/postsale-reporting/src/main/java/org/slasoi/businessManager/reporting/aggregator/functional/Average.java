/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.functional;

import java.util.ArrayList;

import org.slasoi.businessManager.reporting.store.MonitoringResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 22, 2011
 */
public class Average implements FunctionalAggregator {

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator#execute(java.util.ArrayList)
	 */	
	public double execute(ArrayList<MonitoringResult> monitoringResults) {
		double sum = 0.0;
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			sum += monitoringResult.getGuaranteedStateQoSValue();
		}
		
		return sum/monitoringResults.size();
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
