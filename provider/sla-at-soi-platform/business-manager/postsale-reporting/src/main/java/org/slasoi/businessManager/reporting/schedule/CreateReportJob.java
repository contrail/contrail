/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slaatsoi.business.schema.ReceiverType;
import org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyUtils;
import org.slasoi.businessManager.reporting.parser.ReportingTarget;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.report.ReportMaker;
import org.slasoi.businessManager.reporting.report.ReportMakerFactory;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.common.eventschema.AgreementTermType;
import org.slasoi.common.eventschema.GuaranteedType;
import org.slasoi.common.eventschema.SLAType;
import org.slasoi.common.reportschema.AssessmentInfoType;
import org.slasoi.common.reportschema.AssessmentResultSummaryType;
import org.slasoi.common.reportschema.FunctionalAggregatorResultSummaryType;
import org.slasoi.common.reportschema.FunctionalAggregatorResultType;
import org.slasoi.common.reportschema.PostSaleReportType;
import org.slasoi.common.reportschema.ReportInfoType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 25, 2011
 */
public class CreateReportJob implements Job {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private ReportingPolicy reportingPolicy;
	private MonitoringResultEntityManager monitoringResultEntityManager;
	private ReportEntityManager reportEntityManager;
	private String reportTemplateFile;
	
	AssessmentInfoType slaAssessment;
	AssessmentInfoType agrementTermAssessment;
	AssessmentInfoType guaranteedAssessment;
	
	public static enum DataMapProperties { 
		REPORTING_CONFIGURATION,
		MONITORING_RESULT_DB_MANAGER,
		REPORT_DB_MANAGER,
		REPORT_TEMPLATE_FILE
	};
	
	/**
	 * 
	 */
	public CreateReportJob() {
		slaAssessment = new AssessmentInfoType();
		agrementTermAssessment = new AssessmentInfoType();
		guaranteedAssessment = new AssessmentInfoType();
		
		slaAssessment.setNotAssessted(0);
		slaAssessment.setSatisfactions(0);
		slaAssessment.setViolations(0);
		
		agrementTermAssessment.setNotAssessted(0);
		agrementTermAssessment.setSatisfactions(0);
		agrementTermAssessment.setViolations(0);
		
		guaranteedAssessment.setNotAssessted(0);
		guaranteedAssessment.setSatisfactions(0);
		guaranteedAssessment.setViolations(0);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
		
		// get job properties from job data map
		reportingPolicy = (ReportingPolicy) jobDataMap.get(DataMapProperties.REPORTING_CONFIGURATION.toString());
		monitoringResultEntityManager = (MonitoringResultEntityManager) jobDataMap.get(DataMapProperties.MONITORING_RESULT_DB_MANAGER.toString());
		reportEntityManager = (ReportEntityManager) jobDataMap.get(DataMapProperties.REPORT_DB_MANAGER.toString());
		reportTemplateFile = (String) jobDataMap.get(DataMapProperties.REPORT_TEMPLATE_FILE.toString());
		
		// init parameters to retrieve monitoring results
		String slaId = reportingPolicy.getSlaId();
		Date fromDate = jobExecutionContext.getTrigger().getPreviousFireTime();
		Date toDate = jobExecutionContext.getTrigger().getNextFireTime();
		
		// check time interval. If it's invalid than return immediately
		if (fromDate == null || toDate == null) {
			logger.debug("Invalid monitoring result fetching time interval from " + fromDate + " to " + toDate);
			return;
		}
		
		// FD = FD - (TD-FD), the from date (FD) value is equal to the FD vale
		// minus the time interval between to date (TD) and FD.
		fromDate = new Date(fromDate.getTime()-(toDate.getTime()-fromDate.getTime()));
		toDate = jobExecutionContext.getTrigger().getPreviousFireTime();
		
		logger.debug("Fetching monitoring results from " + fromDate + " to " + toDate);
		
		// fetch monitoring results
		ArrayList<MonitoringResult> monitoringResults = monitoringResultEntityManager.select(slaId, fromDate, toDate);

		// check if some job results have been retrieved
		if (!monitoringResults.isEmpty()) {
			
			logger.debug("Fetched monitoring results: " + monitoringResults.size());
			for (MonitoringResult monitoringResult : monitoringResults) logger.debug("\t" + monitoringResult + "\t" + monitoringResult.getTimestamp());
			
			// compute job result
			JobResult jobResult = createJobResult(reportingPolicy, monitoringResults);
	
			// loop over different receiver (each receiver might have different
			// report format)
			for (ReceiverType receiver : jobResult.getReceivers().getReceiver()) {
			
				// get a suitable report maker
				ReportMaker reportMaker = new ReportMakerFactory().createReportMaker(receiver.getReportFormat());
		
				// job result
				PostSaleReportType postSaleReport = parseToPostSaleReport(jobResult);
				
				try {
					logger.debug(XMLUtils.marshall(postSaleReport));
				} catch (JAXBException e) {
					logger.error(e.getMessage(), e);
				}
				
				// create report
				Report report = reportMaker.createReport(reportTemplateFile, postSaleReport);
				
				// store report
				reportEntityManager.insert(report);
			}
		} else {
			logger.debug("No monitoring results found");
		}
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param reportingPolicy
	 * @param monitoringResults
	 * @return
	 */
	private JobResult createJobResult(ReportingPolicy reportingPolicy, ArrayList<MonitoringResult> monitoringResults) {
		// create job result
		JobResult jobResult = new JobResult(reportingPolicy, monitoringResults);
			
		for (ReportingTarget reportingTarget : reportingPolicy.getReportingTargets()) {
			logger.debug("Reporting target: " + reportingTarget);
			
			FunctionalAggregator functionalAggregator = ReportingPolicyUtils.getFunctionalAggregator(reportingTarget.getFunctionalAggregatorClassName());
			
			// if the functional aggregator exists
			if (functionalAggregator != null) {
				
				// filter the monitoring results with respect to the reporting targets
				ArrayList<MonitoringResult> filteredMonitoringResults = filterMonitoringResult(monitoringResults, reportingTarget.getSlaId(), reportingTarget.getAgreementTermId(), reportingTarget.getGuaranteedId());
				
				logger.debug("Filtered monitoring results: " + filteredMonitoringResults);
				
				// if the filtered set is empty than do not compute the functional aggregator
				if (!filteredMonitoringResults.isEmpty()) {
					
					// execute the functional aggregator
					double value = functionalAggregator.execute(filteredMonitoringResults);
					
					logger.debug("Functional aggregator result: " + value); 
					
					// create tje functional aggregator result object
					FunctionalAggregatorResult functionalAggregatorResult = new FunctionalAggregatorResult(
							reportingTarget.getSlaId(),
							reportingTarget.getAgreementTermId(),
							reportingTarget.getGuaranteedId(),
							functionalAggregator.getClass().getSimpleName(),
							value);
					
					// add the functional aggregator result
					jobResult.getFunctionalAggregatorResults().add(functionalAggregatorResult);
				}
			}
		}
		
		return jobResult;
	}
	
	/**
	 * @param monitoringResults
	 * @param sldId
	 * @param agreementTermId
	 * @param guaranteedId
	 * @return
	 */
	private ArrayList<MonitoringResult> filterMonitoringResult(ArrayList<MonitoringResult> monitoringResults, String sldId, String agreementTermId, String guaranteedId) {
		ArrayList<MonitoringResult> results = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			if (monitoringResult.getSlaId().equals(sldId) && monitoringResult.getAgreementTermId().equals(agreementTermId) && monitoringResult.getGuaranteedId().equals(guaranteedId)) {
				results.add(monitoringResult);
			}
		}
		
		return results;
	}
	
	/**
	 * @param jobResult
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private PostSaleReportType parseToPostSaleReport(JobResult jobResult) {
		PostSaleReportType report = new PostSaleReportType();
		
		ReportingPolicy reportingPolicy = jobResult.getReportingConfiguration();
		ArrayList<MonitoringResult> monitoringResults = jobResult.getMonitoringResults();
		
		ReportInfoType reportInfo = new ReportInfoType();
		reportInfo.setReportCreatorId("SLA@SOI Business Reporting");
		reportInfo.setReportId(reportingPolicy.getReportingPolicyId());
		try {
			reportInfo.setTimestamp(getXMLGregorianCalendar(System.currentTimeMillis()));
		} catch (DatatypeConfigurationException e) {
			logger.error(e.getMessage(), e);
		}
		
		// report info
		report.setReportInfo(reportInfo);
		
		// report details
		for (int i=0; i<monitoringResults.size(); i++) {
			if (i == 0) {
				report.setMonitoringResult(monitoringResults.get(i).getEventInstance().getEventPayload().getMonitoringResultEvent());
			} else {
				report.getMonitoringResult().getSLAInfo().getAgreementTerm().addAll(monitoringResults.get(i).getEventInstance().getEventPayload().getMonitoringResultEvent().getSLAInfo().getAgreementTerm());
			}
			
			// Assessment result summary
			report.setAssessmentResultSummary(getAssessmentResultSummaryType(monitoringResults.get(i).getEventInstance().getEventPayload().getMonitoringResultEvent().getSLAInfo()));
		}

		// Aggregate function summary
		FunctionalAggregatorResultSummaryType functionalAggregatorResultSummaryType = new FunctionalAggregatorResultSummaryType();
		
		for (FunctionalAggregatorResult functionalAggregatorResult : jobResult.getFunctionalAggregatorResults()) {
			FunctionalAggregatorResultType functionalAggregatorResultType = new FunctionalAggregatorResultType();
			functionalAggregatorResultType.setSlaId(functionalAggregatorResult.getSlaId());
			functionalAggregatorResultType.setAgreementTermId(functionalAggregatorResult.getAgreementTermId());
			functionalAggregatorResultType.setGuaranteedTermId(functionalAggregatorResult.getGuaranteedId());
			functionalAggregatorResultType.setFunctionalAggregatorId(functionalAggregatorResult.getFunctionalAggregatorId());
			functionalAggregatorResultType.setAggregateValue(functionalAggregatorResult.getValue());
			
			functionalAggregatorResultSummaryType.getFunctionalAggregatorResult().add(functionalAggregatorResultType);
		}
		
		report.setFunctionalAggregatorResultSummary(functionalAggregatorResultSummaryType);
		
		return report;
	}
	
	/**
	 * @param monitoringResult
	 * @return
	 */
	private AssessmentResultSummaryType getAssessmentResultSummaryType(SLAType sla) {
		AssessmentResultSummaryType summary = new AssessmentResultSummaryType();
		
		// update SLA assessment
		slaAssessment = updateAssessmentResult(sla.getAssessmentResult(), slaAssessment);
		
		// loop on the agreement terms
		for (AgreementTermType agreementTerm : sla.getAgreementTerm()) {
			
			// update agreement terms assessment
			agrementTermAssessment = updateAssessmentResult(agreementTerm.getAssessmentResult(), agrementTermAssessment);
			
			// loop in the guaranteed terms
			for (GuaranteedType guaranteed : agreementTerm.getGuaranteedStateOrGuaranteedAction()) {
				
				// update guaranteed terms assessment
				guaranteedAssessment = updateAssessmentResult(guaranteed.getAssessmentResult(), guaranteedAssessment);
			}
		}
		
		summary.setSLA(slaAssessment);
		summary.setAgreementTerm(agrementTermAssessment);
		summary.setGuaranteed(guaranteedAssessment);
		
		return summary;
	}
	
	/**
	 * @param test.org.slasoi.businessManager.reporting.utils
	 * @param assessmentInfoType
	 * @return
	 */
	private AssessmentInfoType updateAssessmentResult(org.slasoi.common.eventschema.AssessmentResultType assessmentResult, AssessmentInfoType assessmentInfoType) {
		if (assessmentResult.compareTo(org.slasoi.common.eventschema.AssessmentResultType.NOT_ASSESSED) == 0) {
			assessmentInfoType.setNotAssessted(assessmentInfoType.getNotAssessted()+1);
		}
		else if (assessmentResult.compareTo(org.slasoi.common.eventschema.AssessmentResultType.VIOLATION) == 0) {
			assessmentInfoType.setViolations(assessmentInfoType.getViolations()+1);
		}
		else if (assessmentResult.compareTo(org.slasoi.common.eventschema.AssessmentResultType.SATISFACTION) == 0) {
			assessmentInfoType.setSatisfactions(assessmentInfoType.getViolations()+1);
		}
		
		return assessmentInfoType;
	}
	
	/**
	 * @param milliseconds
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(long milliseconds) throws DatatypeConfigurationException {
		// current date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(milliseconds);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		return xmlCalendar;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
