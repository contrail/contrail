/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import org.slasoi.businessManager.reporting.report.Report;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class ReportRAMEntityManager implements ReportEntityManager {
	private Hashtable<String, Report> reports;
	
	/**
	 * 
	 */
	public ReportRAMEntityManager() {
		this.reports = new Hashtable<String, Report>();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#insert(test.org.slasoi.businessManager.reporting.report.Report)
	 */
	
	public synchronized int insert(Report report)  {
		reports.put(String.valueOf(System.currentTimeMillis()), report);
		return 1;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#selectBySlaId(java.lang.String, java.lang.String, java.util.Date, java.util.Date, java.lang.String)
	 */
	public ArrayList<Report> selectBySlaId(String slaId, String reportingPilicyId, Date fromDate, Date toDate, String format) {
		return null;
	}
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#countBySlaId(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int countBySlaId(String slaId, String reportingPolicyId, Date fromDate, Date toDate) {
		return 0;
	}

	public int count() {
		return 0;
	}

	// ------------------------------------------------------------------------
	// 						PUBLIC METHODS NOT IMPLEMENTED
	// ------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	
	public int delete(Report arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	
	public int deleteByPrimaryKey(String arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	
	public ArrayList<Report> executeQuery(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	
	public int executeUpdate(String arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#getDatabaseTable()
	 */
	
	public String getDatabaseTable() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	
	public ArrayList<Report> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	
	public Report selectByPrimaryKey(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#setDatabaseTable(java.lang.String)
	 */
	
	public void setDatabaseTable(String arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	
	public int update(Report arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
