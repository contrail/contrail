/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.functional;

import java.util.ArrayList;

import org.slasoi.businessManager.reporting.store.MonitoringResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 22, 2011
 */
public class Min implements FunctionalAggregator {

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator#execute(java.util.ArrayList)
	 */
	
	public double execute(ArrayList<MonitoringResult> monitoringResults) {
		double min = Double.MAX_VALUE;
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			if (Double.compare(monitoringResult.getGuaranteedStateQoSValue(), min) < 0) {
				min = monitoringResult.getGuaranteedStateQoSValue();
			}
		}
		return min;
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
