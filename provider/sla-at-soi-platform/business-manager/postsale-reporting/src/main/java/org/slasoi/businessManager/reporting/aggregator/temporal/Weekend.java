/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class Weekend extends TemporalAggregator {

	/**
	 * @param startsOn
	 * @param ends
	 */
	public Weekend(Date startsOn, Ends ends) {
		super(startsOn, ends);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"startsOn=" + getStartsOn() + "," +
				"ends=" + getEnds() + "}"; 
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
