/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class EndsAfterOccurrences implements Ends {
	private int occurrences;
	
	/**
	 * @param occurrences
	 */
	public EndsAfterOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the occurrences
	 */
	public int getOccurrences() {
		return occurrences;
	}
	
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{occurrences=" + occurrences + "}";
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
