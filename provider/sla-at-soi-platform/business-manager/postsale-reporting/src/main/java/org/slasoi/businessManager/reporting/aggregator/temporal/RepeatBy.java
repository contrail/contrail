/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public enum RepeatBy {
	day_of_the_month, day_of_the_week
}
