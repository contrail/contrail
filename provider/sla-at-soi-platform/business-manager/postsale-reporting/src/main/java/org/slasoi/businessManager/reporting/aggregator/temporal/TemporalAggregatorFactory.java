/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;
import java.util.List;

import org.slaatsoi.business.schema.PeriodicFrequencyType;
import org.slaatsoi.business.schema.RepeatByType;
import org.slaatsoi.business.schema.RepeatDailyType;
import org.slaatsoi.business.schema.RepeatHourlyType;
import org.slaatsoi.business.schema.RepeatMinutelyType;
import org.slaatsoi.business.schema.RepeatMonthlyType;
import org.slaatsoi.business.schema.RepeatSecondlyType;
import org.slaatsoi.business.schema.RepeatWeekdayType;
import org.slaatsoi.business.schema.RepeatWeekendType;
import org.slaatsoi.business.schema.RepeatWeeklyType;
import org.slaatsoi.business.schema.RepeatYearlyType;
import org.slaatsoi.business.schema.WeekdaysType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 24, 2011
 */
public class TemporalAggregatorFactory {
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Parse a PeriodicFrequencyType instance to TemporalAggregator.
	 * @param periodicFrequency
	 * @return
	 */
	public static TemporalAggregator create(PeriodicFrequencyType periodicFrequency) {
		TemporalAggregator temporalAggregator = null;
		
		Date startsOn = new Date();
		Ends ends  = new EndsNever();
		

		
		// creates appropriate TemporalAggregator instance
		// Secondly
		if (periodicFrequency instanceof RepeatSecondlyType) {
			RepeatSecondlyType repeatFrequency = (RepeatSecondlyType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Secondly(startsOn, ends, repeatFrequency.getRepeatEvery());
		}
		// Minutely
		else if (periodicFrequency instanceof RepeatMinutelyType) {
			RepeatMinutelyType repeatFrequency = (RepeatMinutelyType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Minutely(startsOn, ends, repeatFrequency.getRepeatEvery());
		}
		// Hourly
		else if (periodicFrequency instanceof RepeatHourlyType) {
			RepeatHourlyType repeatFrequency = (RepeatHourlyType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Hourly(startsOn, ends, repeatFrequency.getRepeatEvery());
		}
		// Daily
		else if (periodicFrequency instanceof RepeatDailyType) {
			RepeatDailyType repeatFrequency = (RepeatDailyType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Daily(startsOn, ends, repeatFrequency.getRepeatEvery());
		}
		// Weekday
		else if (periodicFrequency instanceof RepeatWeekdayType) {
			RepeatWeekdayType repeatFrequency = (RepeatWeekdayType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Weekday(startsOn, ends);
		}
		// Weekend
		else if (periodicFrequency instanceof RepeatWeekendType) {
			RepeatWeekendType repeatFrequency = (RepeatWeekendType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Weekend(startsOn, ends);
		}
		// Weekly
		else if (periodicFrequency instanceof RepeatWeeklyType) {
			RepeatWeeklyType repeatWeeklyType = (RepeatWeeklyType) periodicFrequency;
			
			startsOn = repeatWeeklyType.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatWeeklyType);
			Weekdays[] repeatOn = parseWeekdays(repeatWeeklyType.getRepeatOn());
			
			temporalAggregator = new Weekly(startsOn, ends, repeatWeeklyType.getRepeatEvery(), repeatOn);
		}
		// Monthly
		else if (periodicFrequency instanceof RepeatMonthlyType) {
			RepeatMonthlyType repeatMonthlyType = (RepeatMonthlyType) periodicFrequency;
			
			startsOn = repeatMonthlyType.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatMonthlyType);
			RepeatBy repeatBy = parseRepeatBy(repeatMonthlyType.getRepeatBy());
			
			temporalAggregator = new Monthly(startsOn, ends, repeatMonthlyType.getRepeatEvery(), repeatBy);
		}
		// Yearly
		else if (periodicFrequency instanceof RepeatYearlyType) {
			RepeatYearlyType repeatFrequency = (RepeatYearlyType) periodicFrequency;
			
			startsOn = repeatFrequency.getTimePeriod().getStartsOn().toGregorianCalendar().getTime();
			ends = parseEnds(repeatFrequency);
			
			temporalAggregator = new Yearly(startsOn, ends, repeatFrequency.getRepeatEvery());
		}
		
		return temporalAggregator;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param periodicFrequencyType
	 * @return
	 */
	private static Ends parseEnds(PeriodicFrequencyType periodicFrequencyType) {
		Ends ends = new EndsNever();
		
		// converts ends element
		if (periodicFrequencyType.getTimePeriod().getEnds().getEndsAfterOccurrences() != null) {
			ends = new EndsAfterOccurrences(periodicFrequencyType.getTimePeriod().getEnds().getEndsAfterOccurrences().getAfterOccurrences());
		}
		else if (periodicFrequencyType.getTimePeriod().getEnds().getEndsNever() != null) {
			ends = new EndsNever();
		}
		else if (periodicFrequencyType.getTimePeriod().getEnds().getEndsOn() != null) {
			ends = new EndsOn(periodicFrequencyType.getTimePeriod().getEnds().getEndsOn().getDateTime().toGregorianCalendar().getTime());
		}
		
		return ends;
	}
	
	/**
	 * @param repeatByType
	 * @return
	 */
	private static RepeatBy parseRepeatBy(RepeatByType repeatByType) {
		RepeatBy result = null;
		
		if (repeatByType.equals(RepeatByType.DAY_OF_MONTH)) {
			result = RepeatBy.day_of_the_month;
		} else if (repeatByType.equals(RepeatByType.DAY_OF_WEEK)) {
			result = RepeatBy.day_of_the_week;
		}
		
		return result;
	}
	
	/**
	 * @param weekdays
	 * @return
	 */
	private static Weekdays[] parseWeekdays(List<WeekdaysType> weekdays) {
		Weekdays[] result = new Weekdays[weekdays.size()];
		
		for (int i=0; i<weekdays.size(); i++) {
			result[i] = parseWeekday(weekdays.get(i));
		}
		
		return result;
	}
	
	/**
	 * @param weekday
	 * @return
	 */
	private static Weekdays parseWeekday(WeekdaysType weekday) {
		Weekdays result = null;
		
		if (weekday.equals(WeekdaysType.MONDAY)) {
			result = Weekdays.MONDAY;
		} else if (weekday.equals(WeekdaysType.TUESDAY)) {
			result = Weekdays.TUESDAY;
		}  else if (weekday.equals(WeekdaysType.WEDNESDAY)) {
			result = Weekdays.WEDNESDAY;
		}  else if (weekday.equals(WeekdaysType.THURSDAY)) {
			result = Weekdays.THURSDAY;
		}  else if (weekday.equals(WeekdaysType.FRIDAY)) {
			result = Weekdays.FRIDAY;
		}  else if (weekday.equals(WeekdaysType.SATURDAY)) {
			result = Weekdays.SATURDAY;
		}  else if (weekday.equals(WeekdaysType.SUNDAY)) {
			result = Weekdays.SUNDAY;
		} 
		
		return result;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
