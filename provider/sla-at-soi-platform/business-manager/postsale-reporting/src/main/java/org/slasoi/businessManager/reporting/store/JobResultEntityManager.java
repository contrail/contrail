/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import org.slasoi.businessManager.reporting.schedule.JobResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public interface JobResultEntityManager {
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * Inserts the specified element into this queue if it is possible to do so
	 * immediately without violating capacity restrictions, returning true upon
	 * success and throwing an IllegalStateException if no space is currently available.
	 * 
	 * @param jobResult the element to add
	 * @return true if this collection changed as a result of the call
	 */
	public boolean put(JobResult jobResult);
	
	/**
	 * Retrieves and removes the head of this queue, or returns null if this
	 * queue is empty.
	 * 
	 * @return the head of this queue, or null if this queue is empty
	 */
	public JobResult get();
}
