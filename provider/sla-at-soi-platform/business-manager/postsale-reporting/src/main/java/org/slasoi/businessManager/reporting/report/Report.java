/**
 * 
 */
package org.slasoi.businessManager.reporting.report;

import java.io.Serializable;
import java.util.Date;

import org.slaatsoi.business.schema.ReportFormatType;

import uk.ac.city.soi.database.PersistentEntity;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class Report implements PersistentEntity, Serializable {
	private String reportId;
	private String slaId;
	private Date creationDate;
	private ReportFormatType format;
	private Object content;
	
	/**
	 * @param content
	 * @param format
	 */
	public Report(String reportId, String slaId, Date creationDate, ReportFormatType format, Object content) {
		this.reportId = reportId;
		this.slaId = slaId;
		this.creationDate = creationDate;
		this.format = format;
		this.content = content;
	}

	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}
	
	/**
	 * @return the slaId
	 */
	public String getSlaId() {
		return slaId;
	}
	
	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * @return the format
	 */
	public ReportFormatType getFormat() {
		return format;
	}
	
	/**
	 * @return the content
	 */
	public Object getContent() {
		return content;
	}
}
