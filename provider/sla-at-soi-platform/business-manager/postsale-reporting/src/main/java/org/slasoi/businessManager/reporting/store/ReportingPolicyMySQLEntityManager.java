/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.BusinessTerm;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.slamodel.sla.SLA;

import uk.ac.city.soi.database.EntityManagerCommons;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 19, 2011
 */
public class ReportingPolicyMySQLEntityManager extends EntityManagerCommons implements ReportingPolicyEntityManager {
    // logger
    private static Logger logger = Logger.getLogger(ReportingPolicyMySQLEntityManager.class);
    
	public static final String DATABASE_TABLE_NAME = "reporting_policies";
	
	// ------------------------------------------------------------------------
	// 								CONSTRUCTORS
	// ------------------------------------------------------------------------
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public ReportingPolicyMySQLEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}

	/**
	 * @param connection
	 */
	public ReportingPolicyMySQLEntityManager(Connection connection) {
		super(connection, DATABASE_TABLE_NAME);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	
	public int insert(ReportingPolicy reportingPolicy) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;

        try {
            Blob slaBlob = toBlob(reportingPolicy.getSla());
            Blob businessTerm = toBlob(reportingPolicy.getBusinessTerm());

            pstmt = getConnection().prepareStatement(
            		"INSERT INTO " + getDatabaseTable() + " " +
                    "(sla_id, reporting_policy_id, reporting_target_type, reporting_target_id, creation_date, sla_object, business_term_object) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)");
            
            pstmt.setString(1, reportingPolicy.getSlaId());
            pstmt.setString(2, reportingPolicy.getReportingPolicyId());
            pstmt.setString(3, "Unused field");
            pstmt.setString(4, "Unused filed");
            pstmt.setLong(5, reportingPolicy.getCreationDate().getTime());
            pstmt.setBlob(6, slaBlob);
            pstmt.setBlob(7, businessTerm);

            result = pstmt.executeUpdate();
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        
        return result;
	}
	
	
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportingPolicyEntityManager#selectBySlaIdAndReportingPolicyId(java.lang.String, java.lang.String)
	 */
	
	public ReportingPolicy selectBySlaIdAndReportingPolicyId(String slaId, String reportingPolicyId) {
		ReportingPolicy reportingPolicy = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT sla_object, business_term_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE sla_id = ? AND reporting_policy_id = ?");
            
            pstmt.setString(1, slaId);
            pstmt.setString(2, reportingPolicyId);

            logger.debug("Executing query: " + pstmt.toString());
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob slaObject = resultSet.getBlob("sla_object");
                    Blob businessTermObject = resultSet.getBlob("business_term_object");

					try {
						SLA sla = (SLA) toObject(slaObject);
						BusinessTerm businessTerm = (BusinessTerm) toObject(businessTermObject);
						reportingPolicy = new ReportingPolicy(sla, businessTerm);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return reportingPolicy;
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	
	public int update(ReportingPolicy paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	
	public int delete(ReportingPolicy paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	
	public int deleteByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	
	public ReportingPolicy selectByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	
	public ArrayList<ReportingPolicy> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	
	public ArrayList<ReportingPolicy> executeQuery(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
