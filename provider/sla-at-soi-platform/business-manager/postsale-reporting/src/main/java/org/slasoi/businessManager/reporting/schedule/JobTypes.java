/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 12, 2011
 */
public enum JobTypes {
	CREATE_REPORT_JOB,
	DELIVER_REPORT_JOB
}
