/**
 * 
 */
package org.slasoi.businessManager.reporting.communication;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.common.eventschema.service.MonitoringEventService;
import org.springframework.osgi.extensions.annotation.ServiceReference;

/**
 * @author Davide Lorenzoli
 * 
 * @date Sep 13, 2010
 */
public class MonitoringEventServiceStub {
	private Logger logger = Logger.getLogger(getClass());
	
	protected MonitoringEventService service;
	
	/**
	 * 
	 */
	public MonitoringEventServiceStub() {
		this.service = new MonitoringEventServiceImpl();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	@ServiceReference  // obtained automatically by Spring-DM and OSGi Container.
	public void setMonitoringEventServiceServices(MonitoringEventService services) {
		this.service = services;
		
		logger.info("Registed service: " + this.service.getClass().getName());
	}
	
	/**
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start() {
		logger.info("Bundle started");
	}

	/**
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop() {
		logger.info("Bundle stopped");
	}
	
	/**
	 * @param eventInstanceXML
	 * @return
	 * @throws Exception
	 */
	public EventInstance unmarshall(String eventInstanceXML) throws Exception {
		//System.out.println("DDDDDDDDDDDD" + eventInstanceXML);
		logger.debug("Unmarshalling event instance:\n" + eventInstanceXML);
		return service.unmarshall(eventInstanceXML);
	}
	
	public String marshall(EventInstance eventInstance) throws Exception {
		//eventInstanceXML.replaceAll("eventInstance", "Event");
		logger.debug("Marshalling event instance:\n" + eventInstance.getEventID());
		return service.marshall(eventInstance);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
