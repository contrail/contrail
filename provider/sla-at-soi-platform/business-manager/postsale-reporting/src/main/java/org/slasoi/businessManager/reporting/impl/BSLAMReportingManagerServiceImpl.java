/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Davide Lorenzoli - Davide.Lorenzoli.1@soi.city.ac.uk, George Spanoudakis - G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.reporting.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.core.ReportingCoordinator;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.service.BSLAMReportingManagerService;
import org.slasoi.slamodel.sla.SLA;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 24, 2011
 */
public class BSLAMReportingManagerServiceImpl implements BSLAMReportingManagerService {
	// logger
	Logger logger = Logger.getLogger(getClass());
	
	private ReportingCoordinator reportingCoordinator;
	
	/**
	 * 
	 */
	public BSLAMReportingManagerServiceImpl() {
		try {
			//System.out.println("MMMMMMMMMMMManager service impl");
			reportingCoordinator = new ReportingCoordinator();
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		} catch (InstantiationException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param slaId
	 * @param reportingPolicyId
	 * @param fromDate
	 * @param toDate
	 * @param format
	 * @return
	 */
	public ArrayList<Report> select(String slaId, String reportingPolicyId, Date fromDate, Date toDate, ReportFormatType format) {
		return reportingCoordinator.select(slaId, reportingPolicyId, fromDate, toDate, format);
	}

	/**
	 * @see org.slasoi.businessManager.reporting.service.ReportingPolicyManageable#addSLA(org.slasoi.slamodel.sla.SLA)
	 */
	
	public boolean addSLA(SLA sla) {
		return reportingCoordinator.addReportingPolicy(sla);
	}

	/**
	 * @see org.slasoi.businessManager.reporting.service.ReportingPolicyManageable#removeSLA(java.lang.String)
	 */
	
	public boolean removeSLA(String slaId) {
		return reportingCoordinator.removeSLA(slaId);
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
