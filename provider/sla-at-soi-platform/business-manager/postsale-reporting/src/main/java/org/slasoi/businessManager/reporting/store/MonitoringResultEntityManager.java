/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.util.ArrayList;
import java.util.Date;

import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public interface MonitoringResultEntityManager extends EntityManagerInterface<MonitoringResult> {
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(MonitoringResult monitoringResult);
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate);
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, String guaranteedId);
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId, Date fromDate, Date toDate);
	
	public ArrayList<MonitoringResult> select(String slaId, String agreementTermId);
	
	public ArrayList<MonitoringResult> select(String slaId, Date fromDate, Date toDate);
	
	public ArrayList<MonitoringResult> select(String slaId);
	
	public int count(String slaId, String agreementTermId, String guaranteedId, Date fromDate, Date toDate);
	
	public int count(String slaId, String agreementTermId, String guaranteedId);
	
	public int count(String slaId, String agreementTermId, Date fromDate, Date toDate);
	
	public int count(String slaId, String agreementTermId);
	
	public int count(String slaId, Date fromDate, Date toDate);
	
	public int count(String slaId);
	
	public int truncate();
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
