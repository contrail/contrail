/**
 * 
 */
package org.slasoi.businessManager.reporting.communication;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 30, 2011
 */
public class MonitoringResultEventSubscriber {
	// logger
	private Logger logger = Logger.getLogger(getClass());

	private final static String CHANNEL_NAME_PROPERTY = "xmpp_channel";
	
    /** pub sub manager. */
    private PubSubManager pubSubManager;

    private Properties xmppProperties;

	private MonitoringResultEventListener monitoringResultEventListener;
	
	/**
	 * @param channelName
	 * @param monitoringResultEntityManager
	 */
	public MonitoringResultEventSubscriber(MonitoringResultEntityManager monitoringResultEntityManager) {
		this.xmppProperties = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.XMPP);
		this.monitoringResultEventListener = new MonitoringResultEventListener(monitoringResultEntityManager);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Subscribe to the result channel.
	 */
	public void subscribe() {
        try {
        	// create pub/sub manager by passing a properties object
			pubSubManager = PubSubFactory.createPubSubManager(xmppProperties);
			//System.out.println("Pubsub name " + pubSubManager);
			// subscribe to channel

			pubSubManager.subscribe(xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
			
			//System.out.println("KKKKKKKKKKKK Channel name " + xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));			
			// add listener to a channel
			pubSubManager.addMessageListener(monitoringResultEventListener);
			
			logger.debug("Subscription parameters:");
			logger.debug("\tProperties: " + xmppProperties);
			logger.debug("\tChannel: " + xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
			logger.debug("\tListener: " + monitoringResultEventListener.getClass().getSimpleName());
		} catch (MessagingException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}		
	}

    /**
     * Unsubscribe from the result channel.
     */
    public void unsubscribe() {
        try {
        	pubSubManager.unsubscribe(xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
        	pubSubManager.removeMessageListener(monitoringResultEventListener);
            pubSubManager.close();
        } catch (MessagingException e) {
        	logger.error(e.getMessage(), e);
        }
    }
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
    
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
