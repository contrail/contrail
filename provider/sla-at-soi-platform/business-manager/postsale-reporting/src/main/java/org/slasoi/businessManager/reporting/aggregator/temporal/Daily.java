/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class Daily extends TemporalAggregator {
	private int repeatEvery;
	
	/**
	 * @param startsOn
	 * @param ends
	 * @param repeatEvery
	 */
	public Daily(Date startsOn, Ends ends, int repeatEvery) {
		super(startsOn, ends);
		this.repeatEvery = repeatEvery;
	}
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the repeatEvery
	 */
	public int getRepeatEvery() {
		return repeatEvery;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"startsOn=" + getStartsOn() + "," +
				"ends=" + getEnds() + "," +
				"repeatEvery=" + repeatEvery + "}"; 
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
