/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.functional;

import java.util.ArrayList;

import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.common.eventschema.AssessmentResultType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 22, 2011
 */
public class ViolationRatio implements FunctionalAggregator {

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator#execute(java.util.ArrayList)
	 */
	
	public double execute(ArrayList<MonitoringResult> monitoringResults) {
		int violations = 0;
		int notViolations = 0;
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			if (monitoringResult.getSLAAssessmentResult().equals(AssessmentResultType.VIOLATION)) {
				violations++;
			} else {
				notViolations++;
			}
		}
		
		return ((double) violations)/notViolations;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
