/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.schedule.JobResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class JobResultRAMEntityManager implements JobResultEntityManager {
	// logger
	private Logger logger = Logger.getLogger(JobResultRAMEntityManager.class);
	
	private static BlockingQueue<JobResult> database;
	
	/**
	 * Initiate the data structure holding job results. The data
	 * structure is declared static so it gets initialised once only
	 * even with multiple instances of JobResultRAMEntityManager
	 */
	public JobResultRAMEntityManager() {
		if (database == null) {
			database = new LinkedBlockingQueue<JobResult>();
		}
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.slasoi.businessManager.reporting.store.JobResultEntityManager#put(org.slasoi.businessManager.reporting.schedule.JobResult)
	 */
	
	public synchronized boolean put(JobResult jobResult) {
		logger.debug("Adding job result: " + jobResult);
		boolean result = database.offer(jobResult);
		notify();
		return result;
	}

	/**
	 * Retrieves and removes the head of this queue, or returns null if this
	 * queue is empty. Also, if the queue is empty the caller will wait until
	 * an element in inserted in the queue.
	 * 
	 * @see org.slasoi.businessManager.reporting.store.JobResultEntityManager#get()
	 */
	
	public synchronized JobResult get() {
		JobResult jobResult = null;
		
		while (database.isEmpty()) {
			try {
				logger.debug("Waiting for job results");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		try {
			jobResult = database.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		notify();
		
		return jobResult; 
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
