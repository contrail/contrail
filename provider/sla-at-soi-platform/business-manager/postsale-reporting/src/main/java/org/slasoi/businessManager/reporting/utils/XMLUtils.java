/**
 * 
 */
package org.slasoi.businessManager.reporting.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 22, 2011
 */
public class XMLUtils {
	private static Logger logger = Logger.getLogger(XMLUtils.class);
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param fileName fully qualified file name (including path)
	 * @return string representation of the file content
	 * @throws IOException
	 */
	public static String fileToString(String fileName) throws IOException {
		File file = new File(fileName); 
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		String lineSeparator = System.getProperty("line.separator");
		
		String line = bufferedReader.readLine();
		
		while (line != null) {
			stringBuilder.append(line);
			stringBuilder.append(lineSeparator);
			
			line = bufferedReader.readLine();
		}
		
		return stringBuilder.toString();
	}
	
	/**
	 * @param object
	 * @param fileName
	 * @return
	 */
	public static boolean saveToFile(Object object, String fileName) {
		boolean result = true;

		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			
			FileOutputStream fileOutputStream = new FileOutputStream(fileName);
			fileOutputStream.write(byteArrayOutputStream.toByteArray());
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (FileNotFoundException e) {
			result = false;
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			result = false;
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * @param object
	 * @return the XML string representation of the given object
	 * @throws JAXBException
	 */
	public static String marshall(Object object) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
		
		Marshaller m = jaxbContext.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		StringWriter stringWriter = new StringWriter();
		
		m.marshal(object, stringWriter);
		
		return stringWriter.toString();
	}
	
	/**
	 * @param xml
	 * @param xmlSchema optional
	 * @return
	 * @throws SAXException
	 */
	public static Object unmarshall(String xml, String xmlSchema, Class objectClass) throws SAXException {
		Object object = null;
		Schema schema = null;
		
		// check for null input
		if (xml == null) return object;
		
		if (xmlSchema != null) {
			SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			schema = schemaFactory.newSchema(new StreamSource(new StringReader(xmlSchema)));
		}
		
		//logger.debug("Parsing xml to " + objectClass + ":\n" + xml);
		
		ByteArrayInputStream xmlContentBytes = new ByteArrayInputStream(xml.getBytes());
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(objectClass);
            // create an Unmarshaller
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            //note: setting schema to null will turn validator off
            unmarshaller.setSchema(schema);
            
            object = unmarshaller.unmarshal(xmlContentBytes);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
		return object;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
