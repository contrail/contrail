/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class Weekly extends TemporalAggregator {
	private int repeatEvery;
	private Weekdays[] repeatOn;
	
	/**
	 * @param startsOn
	 * @param ends
	 * @param repeatEvery
	 * @param repeatOn if null or empty it means every day
	 */
	public Weekly(Date startsOn, Ends ends, int repeatEvery, Weekdays[] repeatOn) {
		super(startsOn, ends);
		this.repeatEvery = repeatEvery;
		
		if (repeatOn == null || repeatOn.length == 0) {
			this.repeatOn = new Weekdays[] {
					Weekdays.MONDAY,
					Weekdays.TUESDAY,
					Weekdays.WEDNESDAY,
					Weekdays.THURSDAY,
					Weekdays.FRIDAY,
					Weekdays.SATURDAY,
					Weekdays.SUNDAY};
		} else {
			this.repeatOn = repeatOn;
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the repeatEvery
	 */
	public int getRepeatEvery() {
		return repeatEvery;
	}
	
	/**
	 * @return the repeatOn
	 */
	public Weekdays[] getRepeatOn() {
		return repeatOn;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"startsOn=" + getStartsOn() + "," +
				"ends=" + getEnds() + "," +
				"repeatEvery=" + repeatEvery + "," +
				"repeatOn=" + repeatOn + "}"; 
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
