/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

import java.util.ArrayList;

import org.slaatsoi.business.schema.ReceiversType;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.store.MonitoringResult;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class JobResult {
	private ReportingPolicy reportingPolicy;
	private ArrayList<MonitoringResult> monitoringResults;
	private ArrayList<FunctionalAggregatorResult> functionalAggregatorResults;
	private ReceiversType receivers;
	
	/**
	 * @param reportingPolicy
	 * @param monitoringResults
	 * @return
	 */
	public  JobResult(ReportingPolicy reportingPolicy, ArrayList<MonitoringResult> monitoringResults) {
		this.reportingPolicy = reportingPolicy;
		this.functionalAggregatorResults = new ArrayList<FunctionalAggregatorResult>();
		this.monitoringResults = monitoringResults;
		this.receivers = reportingPolicy.getBusinessTerm().getReporting().getReceivers();
	}
		
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return
	 */
	public ReceiversType getReceivers() {
		return receivers;
	}
	
	/**
	 * @return the functionalAggregatorResults
	 */
	public ArrayList<FunctionalAggregatorResult> getFunctionalAggregatorResults() {
		return functionalAggregatorResults;
	}
	
	/**
	 * @return the reportingPolicy
	 */
	public ReportingPolicy getReportingConfiguration() {
		return reportingPolicy;
	}
	
	/**
	 * @return the monitoringResults
	 */
	public ArrayList<MonitoringResult> getMonitoringResults() {
		return monitoringResults;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"receivers=" + receivers + "," +
				"functionalAggregatorResults=" + functionalAggregatorResults + "}";
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * Converts a string into a Report.ReportFormat object
	 * 
	 * @param stringReportFormat
	 * @return the Report.ReportFormat object representation of a given string
	 */
	/*
	private ReportFormat convertReportFormat(ReportFormat receivers) {
		ReportFormat receivers = null;
		
		if (stringReportFormat.equals(ReportFormat.TEXT.toString())) {
			receivers = ReportFormat.TEXT;
		} else if (stringReportFormat.equals(ReportFormat.PDF.toString())) {
			receivers = ReportFormat.PDF;
		}
		else if (stringReportFormat.equals("XML")) {
			receivers = ReportFormat.XML;
		}
		else if (stringReportFormat.equals("HTML")) {
			receivers = ReportFormat.HTML;
		}
		
		return receivers;
	}
	*/
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
