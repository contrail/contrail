/**
 * 
 */
package test.org.slasoi.businessManager.reporting.store;

import java.io.IOException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.xmlbeans.XmlException;
import org.junit.Test;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 19, 2011
 */
public class TestReportingPolicyMySQLEntityManager extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private final static String BUSINESS_SCHEMA_NAME = "BusinessSchema-1.3.0.xsd";
	private final static String B6_SLA = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	
	private final static String SECONDLY_REPORTING_POLICY = "ReportingPolicy-Secondly.xml";
	private final static String DAILY_REPORTING_POLICY = "ReportingPolicy-Daily.xml";
	private final static String WEEKLY_REPORTING_POLICY = "ReportingPolicy-Weekly.xml";
	private final static String APERIODIC_REPORTING_POLICY = "ReportingPolicy-Aperiodic.xml";
	
	private SLA sla;
	private String businessTermXmlSchema;
	
	private ReportingPolicyMySQLEntityManager reportingPolicyEntityManager;
	private ReportingPolicy[] reportingPolicies;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	
	protected void setUp() throws Exception {
		sla = loadSLA();
		businessTermXmlSchema = loadBusinessTermsSchema();
		reportingPolicyEntityManager =  (ReportingPolicyMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTING_POLICIES);
	}

	/**
	 * 
	 */
	@Test
	public void testTruncate() {
		reportingPolicyEntityManager.truncate();
		
		assertEquals(0, reportingPolicyEntityManager.count());
	}
	
	/**
	 * @throws SAXException
	 */
	@Test
	public void testInsert() throws SAXException {
		reportingPolicyEntityManager.truncate();
		
		// add secondly policy
		addReportingPolicy(SECONDLY_REPORTING_POLICY, sla);
		reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		
		assertNotNull(sla);
		assertEquals(1, reportingPolicies.length);
		
		reportingPolicyEntityManager.insert(reportingPolicies[0]);
		
		assertEquals(1, reportingPolicyEntityManager.count());
		
		// add daily policy
		addReportingPolicy(DAILY_REPORTING_POLICY, sla);
		reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		reportingPolicyEntityManager.insert(reportingPolicies[0]);
		
		assertEquals(2, reportingPolicyEntityManager.count());
		
		// add weekly policy
		addReportingPolicy(WEEKLY_REPORTING_POLICY, sla);
		reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		reportingPolicyEntityManager.insert(reportingPolicies[0]);
		
		assertEquals(3, reportingPolicyEntityManager.count());
		
		// add aperiodic policy
		addReportingPolicy(APERIODIC_REPORTING_POLICY, sla);
		reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		reportingPolicyEntityManager.insert(reportingPolicies[0]);
		
		assertEquals(4, reportingPolicyEntityManager.count());
	}
	
	/**
	 * @throws SAXException
	 */
	@Test
	public void testSelectBySlaIdAndReportingPolicyId() throws SAXException {
		reportingPolicyEntityManager.truncate();
		
		addReportingPolicy(SECONDLY_REPORTING_POLICY, sla);
		reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		
		assertNotNull(sla);
		assertEquals(1, reportingPolicies.length);
		
		reportingPolicyEntityManager.insert(reportingPolicies[0]);
		
		ReportingPolicy reportingPolicy = reportingPolicyEntityManager.selectBySlaIdAndReportingPolicyId(sla.getUuid().getValue(), reportingPolicies[0].getReportingPolicyId());
		
		assertNotNull(reportingPolicy);
		
		assertEquals(reportingPolicy.getReportingPolicyId(), reportingPolicies[0].getReportingPolicyId());
		assertEquals(reportingPolicy.getBusinessTerm().getReporting().getReportId(), reportingPolicies[0].getReportingPolicyId());
	}
		
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	private SLA loadSLA() {
		SLA sla = null;
		try {
			sla = B6SLACreatorSyntaxConverter.loadSLA(B6_SLA);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		} catch (XmlException e) {
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return sla;
	}
	
	/**
	 * @return
	 */
	private String loadBusinessTermsSchema() {
		String businessTermXmlSchema = null;
		String fileName = ClassLoader.getSystemResource(BUSINESS_SCHEMA_NAME).getPath();
		try {
			businessTermXmlSchema = XMLUtils.fileToString(fileName);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		
		return businessTermXmlSchema;
	}
	
	/**
	 * @param reportingPolicy
	 * @param test.org.slasoi.businessManager.reporting.utils
	 */
	private void addReportingPolicy(String reportingPolicy, SLA sla) {
		try {
			String reportingPolicyXML = XMLUtils.fileToString(ClassLoader.getSystemResource(reportingPolicy).getPath());
			((Guaranteed.Action) sla.getAgreementTerm("GuarActions").getGuaranteed("kpiReporting")).getPostcondition().setPropertyValue(new STND("BusinessTermReporting"), reportingPolicyXML);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
