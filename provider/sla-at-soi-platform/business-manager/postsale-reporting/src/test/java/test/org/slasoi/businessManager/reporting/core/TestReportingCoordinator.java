/**
 * 
 */
package test.org.slasoi.businessManager.reporting.core;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.impl.BSLAMReportingManagerServiceImpl;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.service.BSLAMReportingManagerService;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.MonitoringResultRAMEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.MonitoringEventUtils;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 22, 2011
 */
public class TestReportingCoordinator extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private final static String B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	private final static String BUSINESS_SCHEMA_NAME = "BusinessSchema-1.3.0.xsd";
	private static final String MONITORING_RESULT_EVENTS = "monitoringResultEvents.xml";
	
	MonitoringResultEntityManager monitoringResultEntityManager;
	ReportEntityManager reportEntityManager;
	ReportingPolicyEntityManager reportingPolicyEntityManager;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		monitoringResultEntityManager = (MonitoringResultEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS);
		reportEntityManager = (ReportEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTS);
		reportingPolicyEntityManager = (ReportingPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTING_POLICIES);
		
		// cleaning tables
		monitoringResultEntityManager.truncate();
		((ReportMySQLEntityManager) reportEntityManager).truncate();
		((ReportingPolicyMySQLEntityManager) reportingPolicyEntityManager).truncate();
		
		// populating monitoring result table
		populateMonitoringResults();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		emptyMonitoringResults();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Test method for {@link test.org.slasoi.businessManager.reporting.core.ReportingCoordinator#addSLA(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddReportingConfiguration() throws Exception {
		BSLAMReportingManagerService reportingCoordinator = new BSLAMReportingManagerServiceImpl();
		
		// load the SLA
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY);
		String reportinPolicySchema = ClassLoader.getSystemResource(BUSINESS_SCHEMA_NAME).getPath();
		
		ReportingPolicy[] reportingPolicies = ReportingPolicyParser.parseSLA(sla, reportinPolicySchema);
		
		// set time period to [current time, current time + 60000]
		long currentTime = System.currentTimeMillis();
		long nextTime = currentTime + 60000;
		int createRepeatEvery = 5;
		int deliveryRepeatEvery = 5;
		
		for (ReportingPolicy reportinPolicy : reportingPolicies) {
			// changing creation schedule
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().setStartsOn(getXMLGregorianCalendar(currentTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().setDateTime(getXMLGregorianCalendar(nextTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setRepeatEvery(createRepeatEvery);
			
			// changing delivery schedule
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().setStartsOn(getXMLGregorianCalendar(currentTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().setDateTime(getXMLGregorianCalendar(nextTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setRepeatEvery(deliveryRepeatEvery);
		}
		
		((Guaranteed.Action) sla.getAgreementTerm("GuarActions").getGuaranteed("mailKpiReporting")).getPostcondition().setPropertyValue(new STND("BusinessTermReporting"), ReportingPolicyParser.parseBusinessTerm(reportingPolicies[0].getBusinessTerm()));
		
		reportingCoordinator.addSLA(sla);

		try {
			while (System.currentTimeMillis() < (currentTime + 60000L)) {
				Thread.sleep(1000);
			}
			assertEquals(0, ((ReportMySQLEntityManager) reportEntityManager).count());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link test.org.slasoi.businessManager.reporting.core.ReportingCoordinator#getReportingResult(java.lang.String)}.
	 */
	@Test
	public void testGetReportingResult() {
		//fail("Not yet implemented");
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param currentTime
	 * @param businessTermXML
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private String updateCreationScheduler(long currentTime, String businessTermXML) throws DatatypeConfigurationException {
		businessTermXML = businessTermXML.replaceAll(
				"<StartsOn>2011-02-08T12:00:00.000Z</StartsOn>",
				"<StartsOn>" + getXMLGregorianCalendar(currentTime) + "</StartsOn>");
		businessTermXML = businessTermXML.replaceAll(
				"<DateTime>2012-02-07T12:00:00.000Z</DateTime>",
				"<DateTime>" + getXMLGregorianCalendar(currentTime + 60000) + "</DateTime>");
		
		// set repeat every to 5 seconds
		businessTermXML = businessTermXML.replaceAll(
				"<RepeatEvery>5</RepeatEvery>",
				"<RepeatEvery>" + 5 + "</RepeatEvery>");
		
		return businessTermXML;
	}
	
	/**
	 * @param currentTime
	 * @param businessTermXML
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private String updateDeliveryScheduler(long currentTime, String businessTermXML) throws DatatypeConfigurationException {
		businessTermXML = businessTermXML.replaceAll(
				"<StartsOn>2013-02-08T12:00:00.000Z</StartsOn>",
				"<StartsOn>" + getXMLGregorianCalendar(currentTime) + "</StartsOn>");
		businessTermXML = businessTermXML.replaceAll(
				"<DateTime>2014-02-07T12:00:00.000Z</DateTime>",
				"<DateTime>" + getXMLGregorianCalendar(currentTime + 60000) + "</DateTime>");
		
		// set repeat every to 5 seconds
		businessTermXML = businessTermXML.replaceAll(
				"<RepeatEvery>10</RepeatEvery>",
				"<RepeatEvery>" + 15 + "</RepeatEvery>");
		
		return businessTermXML;
	}
	
	/**
	 * 
	 */
	private void populateMonitoringResults() {
		long currentTime = System.currentTimeMillis();
		
		String monitoringResultEventsFileName = getClass().getClassLoader().getResource(MONITORING_RESULT_EVENTS).getPath();
		
		ArrayList<EventInstance> events = MonitoringEventUtils.loadMonitoringEvents(monitoringResultEventsFileName);
		
		for (EventInstance event : events) {
			try {
				event.getEventContext().getTime().setTimestamp(currentTime);
				
				currentTime += 2000;
				
				MonitoringResult monitoringResult = MonitoringResult.createInstance(event);
				
				//logger.debug("monitoringResult.getSlaId(): " + monitoringResult.getSlaId());
				//logger.debug("monitoringResult.getAgreementTermId(): " + monitoringResult.getAgreementTermId());
				//logger.debug("monitoringResult.getGuaranteedId(): " + monitoringResult.getGuaranteedId());
				//logger.debug("monitoringResult.getTimestamp(): " + monitoringResult.getTimestamp() + "\n");
				
				monitoringResultEntityManager.insert(monitoringResult);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * 
	 */
	private void emptyMonitoringResults() {
		MonitoringResultEntityManager monitoringResultDatabase = new MonitoringResultRAMEntityManager();
		monitoringResultDatabase.truncate();
	}
	
	/**
	 * @param milliseconds
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(long milliseconds) throws DatatypeConfigurationException {
		// current date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(milliseconds);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		return xmlCalendar;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
