/**
 * 
 */
package test.org.slasoi.businessManager.reporting.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 13, 2011
 */
public class ReportReader {
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								MAIN
	// ------------------------------------------------------------------------
	
	public static void main(String[] args) throws IOException {
		ReportEntityManager reportEntityManager = (ReportEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTS);
		FileOutputStream fileOutputStream;
		String fileName;
		
		ArrayList<Report> reports = reportEntityManager.selectBySlaId("R1-Secondly", "98989893010", new Date(1305556961816L), new Date(1305556961816L), ReportFormatType.PDF.toString());
		
		for (Report report : reports) {
			fileName = "./" + report.getCreationDate().getTime() + ".pdf";
			fileOutputStream = new FileOutputStream(fileName);
			System.out.println("Writing bytes: " + ((byte[]) report.getContent()).length);
			fileOutputStream.write((byte[])report.getContent());
			fileOutputStream.flush();
			fileOutputStream.close();
		}
	}
}
