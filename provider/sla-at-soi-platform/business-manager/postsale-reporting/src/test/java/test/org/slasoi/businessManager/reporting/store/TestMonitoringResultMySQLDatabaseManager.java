/**
 * 
 */
package test.org.slasoi.businessManager.reporting.store;

import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slasoi.businessManager.reporting.core.FrameworkContext;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.MonitoringEventUtils;
import org.slasoi.common.eventschema.EventInstance;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 18, 2011
 */
public class TestMonitoringResultMySQLDatabaseManager extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	private Logger logger = Logger.getLogger(getClass());
	
	private String MONITORING_RESULTS_EVENT = "monitoringResultEvents.xml";
	
	private MonitoringResultMySQLEntityManager monitoringResultDatabase;
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	
	protected void setUp() throws Exception {
		monitoringResultDatabase = (MonitoringResultMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(FrameworkContext.DatabaseManagers.MONITORING_RESULT_EVENTS);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	@Test
	public void testInsert() {
		monitoringResultDatabase.truncate();
		assertEquals(0, monitoringResultDatabase.count());
		
		populate();
		
		assertEquals(53, monitoringResultDatabase.count());
	}
	
	@Test
	public void testSelect() {
		monitoringResultDatabase.truncate();
		assertEquals(0, monitoringResultDatabase.count());
		
		populate();
		
		// selects the event #39
		ArrayList<MonitoringResult> events = monitoringResultDatabase.select("98989893010", "GuarStates", "GT1.1.1", new Date(1297166400255L), new Date(1297166400255L));
		
		assertEquals(1, events.size());
		
		assertEquals("98989893010", events.get(0).getSlaId());
		assertEquals("GuarStates", events.get(0).getAgreementTermId());
		assertEquals("GT1.1.1", events.get(0).getGuaranteedId());
		assertEquals(1297166400255L, events.get(0).getTimestamp().getTime());
		assertEquals(101, events.get(0).getEventInstance().getEventID().getID());
	}
	
	@Test
	public void testSelectBySlaIdAndAgreementTermIdAndGuaranteedId() {
		monitoringResultDatabase.truncate();
		assertEquals(0, monitoringResultDatabase.count());
		
		populate();
		
		// selects the event #39
		ArrayList<MonitoringResult> events = monitoringResultDatabase.select("98989893010", "GuarStates", "GT1.1.1");
		
		assertEquals(44, events.size());
	}
	
	@Test
	public void testSelectNoResult() {
		monitoringResultDatabase.truncate();
		assertEquals(0, monitoringResultDatabase.count());
		
		populate();
		
		// selects the event #39
		ArrayList<MonitoringResult> events = monitoringResultDatabase.select("NotExistingSLA2", "GuarStates", "GT1.1.1", new Date(1318939200255L), new Date(1320753600255L));
		
		assertEquals(0, events.size());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	private void populate() {
		String monitoringResultEventFile = getClass().getClassLoader().getResource(MONITORING_RESULTS_EVENT).getPath();
		ArrayList<EventInstance> events = MonitoringEventUtils.loadMonitoringEvents(monitoringResultEventFile);
		
		for (EventInstance event : events) {
			try {
				
				MonitoringResult monitoringResult = MonitoringResult.createInstance(event);

				monitoringResultDatabase.insert(monitoringResult);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
		}
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
