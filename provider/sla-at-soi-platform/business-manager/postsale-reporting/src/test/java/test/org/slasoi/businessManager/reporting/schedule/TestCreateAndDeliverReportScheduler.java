/**
 * 
 */
package test.org.slasoi.businessManager.reporting.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.quartz.SchedulerException;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.schedule.JobScheduler;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.MonitoringEventUtils;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 17, 2011
 */
public class TestCreateAndDeliverReportScheduler extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
		
	private final static String BUSINESS_SCHEMA_NAME = "BusinessSchema-1.3.0.xsd";
	
	private final static String B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	
	private String REPORT_TEMPLATE_PROPERTY = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.FRAMEWORK).getProperty("reporting.report.template.file");
	private String MONITORING_RESULT_EVENTS = "monitoringResultEvents.xml";
	
	MonitoringResultEntityManager monitoringResultEntityManager;
	ReportEntityManager reportEntityManager;
	ReportingPolicyEntityManager reportingPolicyEntityManager;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		monitoringResultEntityManager = (MonitoringResultEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS);
		reportEntityManager = (ReportEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTS);
		reportingPolicyEntityManager = (ReportingPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTING_POLICIES);
		
		// cleaning tables
		monitoringResultEntityManager.truncate();
		((ReportMySQLEntityManager) reportEntityManager).truncate();
		((ReportingPolicyMySQLEntityManager) reportingPolicyEntityManager).truncate();
		
		// populating monitoring result table
		populateMonitoringResults();
	}
	
	/**
	 * Test method for {@link org.slasoi.businessManager.reporting.schedule.JobScheduler#addCreateReportJob(org.slasoi.businessManager.reporting.parser.ReportingPolicy)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAndDeliverJobSecondly() throws Exception {
		try {
			JobScheduler jobScheduler = new JobScheduler(monitoringResultEntityManager, reportEntityManager);
			Thread jobSchedulerThread = new Thread(jobScheduler);
			// it starts the scheduler engine
			jobScheduler.start();
			// it starts the thread, but it doesn't start the scheduler engine
			jobSchedulerThread.start();
			
			String reportTemplateFileName = ClassLoader.getSystemResource(REPORT_TEMPLATE_PROPERTY).getPath();
			
			logger.debug("Report temaplte file name: " + reportTemplateFileName);
			
			long currentTime = System.currentTimeMillis();
			
			for (ReportingPolicy reportingPolicy : getReportingConfigurations(B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY)) {
					
				// prints the XML
				logger.debug("Business term:\n" + XMLUtils.marshall(reportingPolicy.getBusinessTerm()));
				
				jobScheduler.addCreateReportJob(reportingPolicy, reportTemplateFileName);
				jobScheduler.addDeliverReportJob(reportingPolicy);
			}
				
			try {
				while (System.currentTimeMillis() < (currentTime + 60000L)) {
					Thread.sleep(1000);
				}
				
				assertEquals(11, ((ReportMySQLEntityManager) reportEntityManager).count());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (DatatypeConfigurationException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (JAXBException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return
	 * @throws Exception 
	 */
	private ReportingPolicy[] getReportingConfigurations(String slaFineName) throws Exception {
		ReportingPolicy[] reportingConfigurations = null;
		
		String reportinPolicySchema = ClassLoader.getSystemResource(BUSINESS_SCHEMA_NAME).getPath();
		
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(slaFineName);
		
		reportingConfigurations = ReportingPolicyParser.parseSLA(sla, reportinPolicySchema);
		
		// set time period to [current time, current time + 60000]
		long currentTime = System.currentTimeMillis();
		long nextTime = currentTime + 60000;
		int createRepeatEvery = 5;
		int deliveryRepeatEvery = 15;
		
		for (ReportingPolicy reportinPolicy : reportingConfigurations) {
			// changing creation schedule
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().setStartsOn(getXMLGregorianCalendar(currentTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().setDateTime(getXMLGregorianCalendar(nextTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setRepeatEvery(createRepeatEvery);
			
			// changing delivery schedule
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().setStartsOn(getXMLGregorianCalendar(currentTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().setDateTime(getXMLGregorianCalendar(nextTime));
			reportinPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setRepeatEvery(deliveryRepeatEvery);
		}
		
		return reportingConfigurations;
	}
	
	/**
	 * 
	 */
	private void populateMonitoringResults() {
		long currentTime = System.currentTimeMillis();
		
		String monitoringResultEventsFileName = getClass().getClassLoader().getResource(MONITORING_RESULT_EVENTS).getPath();
		
		ArrayList<EventInstance> events = MonitoringEventUtils.loadMonitoringEvents(monitoringResultEventsFileName);
		
		for (EventInstance event : events) {
			try {
				event.getEventContext().getTime().setTimestamp(currentTime);
				
				currentTime += 2000;
				
				MonitoringResult monitoringResult = MonitoringResult.createInstance(event);
				
				//logger.debug("monitoringResult.getSlaId(): " + monitoringResult.getSlaId());
				//logger.debug("monitoringResult.getAgreementTermId(): " + monitoringResult.getAgreementTermId());
				//logger.debug("monitoringResult.getGuaranteedId(): " + monitoringResult.getGuaranteedId());
				//logger.debug("monitoringResult.getTimestamp(): " + monitoringResult.getTimestamp() + "\n");
				
				monitoringResultEntityManager.insert(monitoringResult);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * @param milliseconds
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(long milliseconds) throws DatatypeConfigurationException {
		// current date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(milliseconds);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		return xmlCalendar;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
