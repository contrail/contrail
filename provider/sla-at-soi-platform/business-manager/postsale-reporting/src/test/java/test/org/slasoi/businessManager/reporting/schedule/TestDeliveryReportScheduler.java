/**
 * 
 */
package test.org.slasoi.businessManager.reporting.schedule;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.quartz.SchedulerException;
import org.slaatsoi.business.schema.EndsOnType;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slaatsoi.business.schema.TimePeriodType;
import org.slaatsoi.business.schema.TimePeriodType.Ends;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.schedule.JobScheduler;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 24, 2011
 */
public class TestDeliveryReportScheduler extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
		
	private final static String BUSINESS_SCHEMA_NAME = "BusinessSchema-1.3.0.xsd";
	
	private final static String B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	
	private String REPORT_PDF_FILE_NAME = "business-report-2.0.pdf";
	
	MonitoringResultEntityManager monitoringResultEntityManager;
	ReportEntityManager reportEntityManager;
	ReportingPolicyEntityManager reportingPolicyEntityManager;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		monitoringResultEntityManager = (MonitoringResultEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS);
		reportEntityManager = (ReportEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTS);
		reportingPolicyEntityManager = (ReportingPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTING_POLICIES);
		
		// cleaning tables
		monitoringResultEntityManager.truncate();
		((ReportMySQLEntityManager) reportEntityManager).truncate();
		((ReportingPolicyMySQLEntityManager) reportingPolicyEntityManager).truncate();
		
		// populating monitoring result table
		populateReports();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * Test method for {@link org.slasoi.businessManager.reporting.schedule.JobScheduler#addCreateReportJob(org.slasoi.businessManager.reporting.parser.ReportingPolicy)}.
	 * @throws Exception 
	 */
	@Test
	public void testDeliveryJobSecondly() throws Exception {
		try {
			JobScheduler jobScheduler = new JobScheduler(monitoringResultEntityManager, reportEntityManager);
			Thread jobSchedulerThread = new Thread(jobScheduler);
			// it starts the scheduler engine
			jobScheduler.start();
			// it starts the thread, but it doesn't start the scheduler engine
			jobSchedulerThread.start();
			
			long currentTime = 0;
			
			for (ReportingPolicy reportingPolicy : getReportingConfigurations(B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY)) {
				
				// set time period to [current time, current time + 60000]
				currentTime = System.currentTimeMillis();
				TimePeriodType timePeriod = new TimePeriodType();
				timePeriod.setStartsOn(getXMLGregorianCalendar(currentTime));
				EndsOnType endsOn = new EndsOnType();
				endsOn.setDateTime(getXMLGregorianCalendar(currentTime + 60000));
				Ends ends = new Ends();
				ends.setEndsOn(endsOn);
				timePeriod.setEnds(ends);		
				reportingPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setTimePeriod(timePeriod);
				// set repeat every to 10 seconds
				reportingPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().setRepeatEvery(10);
				
				// prints the XML
				logger.debug("Business term:\n" + XMLUtils.marshall(reportingPolicy.getBusinessTerm()));
				
				jobScheduler.addDeliverReportJob(reportingPolicy);
			}
				
			try {
				while (System.currentTimeMillis() < (currentTime + 60000L)) {
					Thread.sleep(1000);
				}
				
				assertEquals(20, ((ReportMySQLEntityManager) reportEntityManager).count());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (DatatypeConfigurationException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		} catch (JAXBException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return
	 * @throws Exception 
	 */
	private ReportingPolicy[] getReportingConfigurations(String slaFineName) throws Exception {
		ReportingPolicy[] reportingConfigurations = null;
		
		String reportinPolicySchema = ClassLoader.getSystemResource(BUSINESS_SCHEMA_NAME).getPath();
		String businessTermXmlSchema = XMLUtils.fileToString(reportinPolicySchema);
		
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(slaFineName);
		
		reportingConfigurations = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		
		return reportingConfigurations;
	}
	
	/**
	 * @throws IOException 
	 * 
	 */
	private void populateReports() throws IOException {
		long currentTime = System.currentTimeMillis();
		
		InputStream inputStream = (InputStream) getClass().getClassLoader().getResourceAsStream(REPORT_PDF_FILE_NAME);
		byte[] buffer = new byte[inputStream.available()];
		inputStream.read(buffer); 
		
		for (int i=0; i<20; i++) {
			Report report = new Report("R1-Secondly", "98989893010", new Date(currentTime), ReportFormatType.PDF, buffer);
			reportEntityManager.insert(report);
			currentTime += 10000;
		}
	}
	
	/**
	 * @param milliseconds
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(long milliseconds) throws DatatypeConfigurationException {
		// current date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(milliseconds);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		return xmlCalendar;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
