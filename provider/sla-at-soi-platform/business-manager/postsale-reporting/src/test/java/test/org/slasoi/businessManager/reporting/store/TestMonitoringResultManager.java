/**
 * 
 */
package test.org.slasoi.businessManager.reporting.store;

import java.util.ArrayList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.utils.MonitoringEventUtils;
import org.slasoi.common.eventschema.EventInstance;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 17, 2011
 */
public class TestMonitoringResultManager extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	private Logger logger = Logger.getLogger(getClass());
	
	private String MONITORING_RESULTS_EVENT = "monitoringResultEvents.xml";
	
	private MonitoringResultEntityManager monitoringResultEntityManager;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		monitoringResultEntityManager = (MonitoringResultEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS);
		
		monitoringResultEntityManager.truncate();
		
		populate();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		monitoringResultEntityManager.truncate();
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * 
	 */
	@Test
	public void testSelectBySlaIdAndAgreementIdAndGuaranteedId() {
		String slaId = "98989893010";
		String agreementTermId = "GuarStates";
		String guaranteedId = "GT1.1.1"; 
		
		assertEquals(44, monitoringResultEntityManager.count(slaId, agreementTermId, guaranteedId));
	}
	
	/**
	 * 
	 */
	@Test
	public void testSelectByWorngSlaId() {
		String slaId = "Not existing sla id";
		
		assertEquals(0, monitoringResultEntityManager.count(slaId));
	}
	
	/**
	 * 
	 */
	@Test
	public void testInsert() {
		String slaId = "98989893010";
		monitoringResultEntityManager.truncate();
		
		assertEquals(0, monitoringResultEntityManager.count(slaId));
		
		populate();
		
		assertEquals(53, monitoringResultEntityManager.count(slaId));
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * 
	 */
	private void populate() {
		String monitoringResultEventFile = ClassLoader.getSystemResource(MONITORING_RESULTS_EVENT).getPath();
		ArrayList<EventInstance> events = MonitoringEventUtils.loadMonitoringEvents(monitoringResultEventFile);
		
		for (EventInstance event : events) {
			try {
				
				MonitoringResult monitoringResult = MonitoringResult.createInstance(event);

				monitoringResultEntityManager.insert(monitoringResult);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
		}
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
