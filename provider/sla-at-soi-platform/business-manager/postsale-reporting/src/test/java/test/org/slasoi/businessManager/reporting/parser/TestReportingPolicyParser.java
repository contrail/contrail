/**
 * 
 */
package test.org.slasoi.businessManager.reporting.parser;

import java.io.IOException;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slaatsoi.business.schema.BusinessTerm;
import org.slaatsoi.business.schema.DeliveryMethodType;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slaatsoi.business.schema.Reporting;
import org.slaatsoi.business.schema.ReportingTargetType;
import org.slaatsoi.business.schema.WeekdaysType;
import org.slasoi.businessManager.reporting.aggregator.functional.Average;
import org.slasoi.businessManager.reporting.aggregator.functional.Max;
import org.slasoi.businessManager.reporting.aggregator.functional.Min;
import org.slasoi.businessManager.reporting.aggregator.functional.ViolationRatio;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 20, 2011
 */
public class TestReportingPolicyParser extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private final static String BUSINESS_SCHEMA_NAME = "BusinessSchema-1.3.0.xsd";
	private final static String B6_SLA = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	
	private final static String SECONDLY_REPORTING_POLICY = "ReportingPolicy-Secondly.xml";
	private final static String DAILY_REPORTING_POLICY = "ReportingPolicy-Daily.xml";
	private final static String WEEKLY_REPORTING_POLICY = "ReportingPolicy-Weekly.xml";
	private final static String APERIODIC_REPORTING_POLICY = "ReportingPolicy-Aperiodic.xml";
	
	private String businessTermXmlSchema;
	
	private ArrayList<String> functionalAggregators;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	
	protected void setUp() throws Exception {
		functionalAggregators = new ArrayList<String>();
		functionalAggregators.add(Average.class.getName());
		functionalAggregators.add(Min.class.getName());
		functionalAggregators.add(Max.class.getName());
		functionalAggregators.add(ViolationRatio.class.getName());
		
		businessTermXmlSchema = XMLUtils.fileToString(getClass().getClassLoader().getResource(BUSINESS_SCHEMA_NAME).getPath());
	}
	
	@Test
	public void testSLA() throws Exception {
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(B6_SLA);
		
		assertNotNull(sla);
		
		CustomAction customAction = (CustomAction)  ((Guaranteed.Action) sla.getAgreementTerm("GuarActions").getGuaranteed("mailKpiReporting")).getPostcondition();
		
		assertEquals(1, customAction.getPropertyKeys().length);
		
		ReportingPolicy[] reportingPolicies = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
		
		assertEquals(1, reportingPolicies.length);
		
		assertEquals("98989893010", reportingPolicies[0].getSlaId());
	}
	
	@Test
	public void testSecondlyReportingPolicy() {
		try {
			BusinessTerm businessTerm = getBusinessTerm(SECONDLY_REPORTING_POLICY);
			
			assertNotNull(businessTerm);
			
			Reporting reportingPolicy = businessTerm.getReporting();
			
			assertNotNull(reportingPolicy);
			
			assertEquals("R1-Secondly", reportingPolicy.getReportId());
			assertEquals("98989893010", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getSlaId());
			assertEquals("GuarStates", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getAgreementTermId());
			assertEquals("GT1.1.1", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getGuaranteedId());
			
			for (ReportingTargetType reportingTarget : reportingPolicy.getReportingTargets().getReportingTarget()) {				
				assertTrue(functionalAggregators.contains(reportingTarget.getFunctionalAggregator()));
			}
			
			assertEquals(ReportFormatType.PDF, reportingPolicy.getReceivers().getReceiver().get(0).getReportFormat());
			assertEquals(DeliveryMethodType.EMAIL, reportingPolicy.getReceivers().getReceiver().get(0).getDeliveryMethod());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getStartsOn().toString());
			assertEquals("2011-02-28T17:44:00.036Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(3, reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getRepeatEvery());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getStartsOn().toString());
			assertEquals("2011-02-28T17:44:00.036Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(3, reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly().getRepeatEvery());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testDailyReportingPolicy() {
		try {
			BusinessTerm businessTerm = getBusinessTerm(DAILY_REPORTING_POLICY);
			
			assertNotNull(businessTerm);
			
			Reporting reportingPolicy = businessTerm.getReporting();
			
			assertNotNull(reportingPolicy);
			
			assertEquals("R1-Daily", reportingPolicy.getReportId());
			assertEquals("GT1.1.1", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getGuaranteedId());
			
			for (ReportingTargetType reportingTarget : reportingPolicy.getReportingTargets().getReportingTarget()) {				
				assertTrue(functionalAggregators.contains(reportingTarget.getFunctionalAggregator()));
			}
			
			assertEquals(ReportFormatType.PDF, reportingPolicy.getReceivers().getReceiver().get(0).getReportFormat());
			assertEquals(DeliveryMethodType.EMAIL, reportingPolicy.getReceivers().getReceiver().get(0).getDeliveryMethod());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getTimePeriod().getStartsOn().toString());
			assertEquals("2011-02-28T17:44:00.036Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(1, reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getRepeatEvery());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getTimePeriod().getStartsOn().toString());
			assertEquals("2011-02-28T17:44:00.036Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(1, reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily().getRepeatEvery());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testWeeklyReportingPolicy() {
		try {
			BusinessTerm businessTerm = getBusinessTerm(WEEKLY_REPORTING_POLICY);
			
			assertNotNull(businessTerm);
			
			Reporting reportingPolicy = businessTerm.getReporting();
			
			assertNotNull(reportingPolicy);
			
			assertEquals("R1-Weekly", reportingPolicy.getReportId());
			assertEquals("GT1.1.1", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getGuaranteedId());
			
			for (ReportingTargetType reportingTarget : reportingPolicy.getReportingTargets().getReportingTarget()) {				
				assertTrue(functionalAggregators.contains(reportingTarget.getFunctionalAggregator()));
			}
			
			assertEquals(ReportFormatType.PDF, reportingPolicy.getReceivers().getReceiver().get(0).getReportFormat());
			assertEquals(DeliveryMethodType.EMAIL, reportingPolicy.getReceivers().getReceiver().get(0).getDeliveryMethod());
			
			assertEquals("2011-01-01T12:00:00.000Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getTimePeriod().getStartsOn().toString());
			assertEquals("2012-01-01T12:00:00.000Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(1, reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getRepeatEvery());
			assertEquals(WeekdaysType.MONDAY, reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getRepeatOn().get(0));
			
			assertEquals("2011-01-01T12:00:00.000Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getTimePeriod().getStartsOn().toString());
			assertEquals("2012-01-01T12:00:00.000Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getTimePeriod().getEnds().getEndsOn().getDateTime().toString());
			assertEquals(1, reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getRepeatEvery());
			assertEquals(WeekdaysType.MONDAY, reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly().getRepeatOn().get(0));
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}	
	
	@Test
	public void testAperiodicReportingPolicy() {
		try {
			BusinessTerm businessTerm = getBusinessTerm(APERIODIC_REPORTING_POLICY);
			
			assertNotNull(businessTerm);
			
			Reporting reportingPolicy = businessTerm.getReporting();
			
			assertNotNull(reportingPolicy);
			
			assertEquals("R1-Aperiodic", reportingPolicy.getReportId());
			assertEquals("GT1.1.1", reportingPolicy.getReportingTargets().getReportingTarget().get(0).getGuaranteedId());
			
			for (ReportingTargetType reportingTarget : reportingPolicy.getReportingTargets().getReportingTarget()) {				
				assertTrue(functionalAggregators.contains(reportingTarget.getFunctionalAggregator()));
			}
			
			assertEquals(ReportFormatType.PDF, reportingPolicy.getReceivers().getReceiver().get(0).getReportFormat());
			assertEquals(DeliveryMethodType.EMAIL, reportingPolicy.getReceivers().getReceiver().get(0).getDeliveryMethod());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportCreationSchedule().getAutomaticSchedule().getAperiodicSchedule().getDateTime().toString());
			
			assertEquals("2011-02-22T17:44:00.035Z", reportingPolicy.getReportDeliverySchedule().getAutomaticSchedule().getAperiodicSchedule().getDateTime().toString());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private BusinessTerm getBusinessTerm(String fileName) throws IOException {
		String reportinPolicyFileName = getClass().getClassLoader().getResource(fileName).getPath();
		String xmlReportingPolicy = XMLUtils.fileToString(reportinPolicyFileName);
		
		return ReportingPolicyParser.parseBusinessTerm(xmlReportingPolicy);
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
