/**
 * 
 */
package test.org.slasoi.businessManager.reporting.communication;

import java.util.ArrayList;
import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slasoi.businessManager.reporting.core.FrameworkContext.DatabaseManagers;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.impl.BSLAMReportingManagerServiceImpl;
import org.slasoi.businessManager.reporting.service.BSLAMReportingManagerService;
import org.slasoi.businessManager.reporting.store.MonitoringResultMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyMySQLEntityManager;
import org.slasoi.businessManager.reporting.utils.MonitoringEventUtils;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.slamodel.sla.SLA;

import test.org.slasoi.businessManager.reporting.utils.B6SLACreatorSyntaxConverter;

/**
 * @author Davide Lorenzoli
 * 
 * @date May 18, 2011
 */
public class TestMonitoringResultEventSubscriber extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());

	private final static String CHANNEL_NAME_PROPERTY = "xmpp_channel";
	private final static String MONITORING_RESULTS_EVENT = "monitoringResultEvents_k.xml";
	private final static String B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY = "B6-SLA-ReportinPolicy-Secondly-EndsOn.xml";
	
    /** pub sub manager. */
    private PubSubManager senderPubSubManager;

    /** Channel name for the result event. */
    private String channelName;
    private Properties xmppProperties;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() {
		((MonitoringResultMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS)).truncate();
		((ReportingPolicyMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTING_POLICIES)).truncate();
		((ReportMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.REPORTS)).truncate();
		
		xmppProperties = (Properties) FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.XMPP).clone();
		
		this.channelName = xmppProperties.getProperty(CHANNEL_NAME_PROPERTY);
		logger.debug(this.xmppProperties);
	}
	
	@Test
	public void testSubscriber() throws Exception {
		BSLAMReportingManagerService reportingService = new BSLAMReportingManagerServiceImpl();
		
		SLA sla = B6SLACreatorSyntaxConverter.loadSLA(B6_SLA_SECONDLY_ENDS_ON_REPORTING_POLICY);
		
		reportingService.addSLA(sla);
		
		publishMonitoringResultEvents();
		
		Thread.sleep(1000);
		
		assertEquals(4, ((MonitoringResultMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.MONITORING_RESULT_EVENTS)).count());
   	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @throws Exception 
	 */
	private void publishMonitoringResultEvents() throws Exception {
		xmppProperties.setProperty("xmpp_resource", "sender");
		// create pub/sub manager by passing a properties object
		senderPubSubManager = PubSubFactory.createPubSubManager(xmppProperties);
		// create  the monitoring result events channel
		senderPubSubManager.createChannel(new Channel(channelName));
		// subscribe to channel
		senderPubSubManager.subscribe(channelName);
		
		logger.debug("Subscription parameters:");
		logger.debug("\tProperties: " + xmppProperties);
		logger.debug("\tChannel: " + xmppProperties.getProperty(CHANNEL_NAME_PROPERTY));
		
		String monitoringResultEventsFileName = getClass().getClassLoader().getResource(MONITORING_RESULTS_EVENT).getPath();
		
		// load file containing all events
		String eventsXML =  XMLUtils.fileToString(monitoringResultEventsFileName);
		// parse it in an array of events in XML string form
		ArrayList<String> events = MonitoringEventUtils.parseEventsXML2StringList(eventsXML);
		
		// publish events on bus
		for (int i=0; i<events.size(); i++) {
			logger.debug(events.get(i));
			senderPubSubManager.publish(new PubSubMessage(channelName, events.get(i)));
			Thread.sleep(1000);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								INNER CLASS
	// ------------------------------------------------------------------------
	/*
	public static void main(String[] args){
		try{
			TestMonitoringResultEventSubscriber tmres = new TestMonitoringResultEventSubscriber();
			tmres.setUp();
			tmres.testSubscriber();			
		}catch(Exception exp){
			exp.printStackTrace();
		}

	}
	*/
}
