/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 313 $
 * @lastrevision   $Date: 2010-12-06 14:31:14 +0200 (Mon, 06 Dec 2010) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/reporting/MonitorabilityReport.java $
 */

package org.slasoi.gslam.monitoring.reporting;

import java.util.HashMap;
import java.util.LinkedList;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Result;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Step;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Type;

/**
 * The MonitorabilityReport implements reporting functions for SLA configurations.
 **/
public class MonitorabilityReport {
    /**
     * Log Entries.
     */
    private HashMap<String, LinkedList<ReportEntry>> logEntries = new HashMap<String, LinkedList<ReportEntry>>();

    /**
     * Adds a report entry to the Monitorability Report.
     * 
     * @param slaID
     *            the SLA for the Monitorability Report
     * @param reportEntry
     *            the entry for the Monitorability Report
     **/
    public final void addReport(final String slaID, final ReportEntry reportEntry) {
        LinkedList<ReportEntry> loglist = null;
        if (logEntries.containsKey(slaID)) {
            loglist = logEntries.get(slaID);
        } else {
            loglist = new LinkedList<ReportEntry>();
        }
        loglist.add(reportEntry);
        logEntries.put(slaID, loglist);
    }

    /**
     * Retrieve the log entries for this report.
     * 
     * @return HashMap<String, LinkedList<ReportEntry>>
     * 
     **/
    public final HashMap<String, LinkedList<ReportEntry>> getLogEntries() {
        return logEntries;
    }

    /**
     * Consolidate Monitorability Reports.
     * 
     * @param slaID
     *            the SLA for the Monitorability Report
     * @param report
     *            another MonitorabilityReport to consolidate with this one.
     **/
    public final void consolidate(final String slaID, final MonitorabilityReport report) {
        LinkedList<ReportEntry> loglist = null;
        if (logEntries.containsKey(slaID)) {
            loglist = logEntries.get(slaID);
        } else {
            loglist = new LinkedList<ReportEntry>();
        }
        if (report.getLogEntries().get(slaID) != null) {
            loglist.addAll(report.getLogEntries().get(slaID));
        }
        logEntries.put(slaID, loglist);
    }

    /**
     * Build a report entry.
     * 
     * @param path
     *            the path to an SLA element
     * @param id
     *            the id of an SLA element
     * @param type
     *            the type of report entry
     * @param step
     *            the step of reporting
     * @param desc
     *            the step of reporting
     * @param value
     *            the value obtained from the step
     * @param result
     *            the result of this step
     * @return ReportEntry
     * 
     **/
    public final ReportEntry buildEntry(final String path, final String id, final Type type, final Step step,
            final String desc, final String value, final Result result) {
        ReportEntry entry = new ReportEntry(path, id, type, step, desc, value, result);
        return entry;
    }
}
