/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/slasoi/FeatureUtils.java $
 */

package org.slasoi.gslam.monitoring.spec.slasoi;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.monitoring.common.features.Basic;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.Primitive;
import org.slasoi.monitoring.common.features.impl.FunctionImpl;

/**
 * Supports SLA@SOI Feature Models access and reasoning.
 **/
public class FeatureUtils extends MonitoringLogger {

    /**
     * Constructor.
     * 
     * <p>
     * Initialises logging features
     **/
    public FeatureUtils() {
        initLogging(this.getClass().getName());
    }

    /**
     * Returns number of input parameters with a type specified.
     * 
     * 
     @param mf
     *            A Monitoring Feature
     * 
     @return int
     * 
     @see org.slasoi.slamodel.sla.SLA org.slasoi.slamodel.primitives.ID
     * 
     **/
    public final int getNumberofInputsWithTypes(final MonitoringFeature mf) {
        int nowithtypes = 0;
        if (mf != null && mf instanceof FunctionImpl) {
            FunctionImpl fmf = (FunctionImpl) mf;
            Iterator<Basic> bfmf = fmf.getInputList().iterator();
            while (bfmf.hasNext()) {
                Basic aBasic = bfmf.next();
                if (aBasic.getType() != null) {
                    nowithtypes++;
                }
            }
        }
        return nowithtypes;
    }

    /**
     * Returns the number of input features with Units specified.
     * 
     * @param mf
     *            A Monitoring Feature
     * @return int
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    public final int getNumberofInputsWithUnits(final MonitoringFeature mf) {
        int nowithunits = 0;
        if (mf != null && mf instanceof FunctionImpl) {
            FunctionImpl fmf = (FunctionImpl) mf;
            Iterator<Basic> bfmf = fmf.getInputList().iterator();
            while (bfmf.hasNext()) {
                Basic aBasic = bfmf.next();
                if (aBasic instanceof Primitive) {
                    Primitive aPrimitive = (Primitive) aBasic;
                    if (aPrimitive.getUnit() != null) {
                        nowithunits++;
                    }
                }
            }
        }
        return nowithunits;
    }

    /**
     * Returns whether the monitoring feature has Input types.
     * 
     * @param mf
     *            A Monitoring Feature
     * @param type
     *            the type of Input
     * @return boolean
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    public final boolean hasInputWithType(final MonitoringFeature mf, final String type) {
        boolean result = false;

        if (mf != null && type != null && mf instanceof FunctionImpl) {
            FunctionImpl fmf = (FunctionImpl) mf;
            Iterator<Basic> bfmf = fmf.getInputList().iterator();
            while (bfmf.hasNext()) {
                Basic aBasic = bfmf.next();
                if (aBasic.getType().equals(type)) {
                    result = true;
                }
            }
        }

        return result;
    }

    /**
     * Returns whether the monitoring feature has Input units.
     * 
     * @param mf
     *            A Monitoring Feature
     * @param unit
     *            the type of unit
     * @return boolean
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/

    public final boolean hasInputWithUnit(final MonitoringFeature mf, final String unit) {
        boolean result = false;

        if (mf != null && unit != null && mf instanceof FunctionImpl) {
            FunctionImpl fmf = (FunctionImpl) mf;
            Iterator<Basic> bfmf = fmf.getInputList().iterator();
            while (bfmf.hasNext()) {
                Basic aBasic = bfmf.next();
                if (aBasic instanceof Primitive) {
                    Primitive aPrimitive = (Primitive) aBasic;
                    if (aPrimitive.getUnit() != null && aPrimitive.getUnit().equals(unit)) {
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    /**
     * Outputs (logs) all Features contained in a ComponentMonitoringFeatures set.
     * 
     * @param features
     *            The features
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    public final void debugFeatures(final ComponentMonitoringFeatures[] features) {
        if (features != null) {
            // loop through all the ComponentMonitoringFeatures and select those
            // with the term
            // convert array to list
            List<ComponentMonitoringFeatures> listOfCMF = Arrays.asList(features);
            Iterator<ComponentMonitoringFeatures> listofCMFIT = listOfCMF.iterator();
            while (listofCMFIT.hasNext()) {
                ComponentMonitoringFeatures cmf = listofCMFIT.next();
                if (cmf != null && cmf.getMonitoringFeatures() != null) {
                    List<MonitoringFeature> mfslist = Arrays.asList(cmf.getMonitoringFeatures());
                    Iterator<MonitoringFeature> mfslistit = mfslist.iterator();
                    while (mfslistit.hasNext()) {
                        MonitoringFeature mf = mfslistit.next();
                        if (mf != null) {
                            getLogger().debug("Feature: " + mf.getName());
                        }
                    }
                }
            }
        }
    }

}
