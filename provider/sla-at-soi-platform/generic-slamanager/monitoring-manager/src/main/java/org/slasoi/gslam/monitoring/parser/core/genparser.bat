rem this batch file requires the env variable SLASOI@HOME setup
rem also change the location of javacc-5.0\bin
rem 
cd %SLASOI_HOME%\..\..\generic-slamanager\monitoring-manager\src\main\java\org\slasoi\gslam\monitoring\parser\core
rem cd C:\Dev\eclipseslasoi\workspace\SLASVN\generic-slamanager\monitoring-manager\src\main\java\org\slasoi\gslam\monitoring\parser\core
del TokenMgrError.java
del Token.java
del SimpleCharStream.java
del ParseException.java
call C:\dev\javacc-5.0\bin\jjtree UQLParser.jjt
call C:\dev\javacc-5.0\bin\javacc UQLParser.jj
call javac *.java
del *.class
pause
