/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/selection/ComponentSelector.java $
 */

package org.slasoi.gslam.monitoring.selection;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.Arrays;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.spec.slasoi.FeatureUtils;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;

/**
 * Component Selector for Monitoring Feature matching.
 **/
public class ComponentSelector extends MonitoringLogger {

    /***
     * A set of monitoring features.
     **/
    private ComponentMonitoringFeatures[] cmfs = null;
    /***
     * Feature Utilities.
     **/
    private FeatureUtils oFeatureUtils = new FeatureUtils();
    /***
     * Count the number of components selected in last selection.
     **/
    private int lastMmatchedCount = 0;

    /**
     * Constructor.
     * 
     * @param features
     *            A set of Monitoring Component Features
     **/
    public ComponentSelector(final ComponentMonitoringFeatures[] features) {
        // initialise logging
        initLogging(this.getClass().getName());

        cmfs = features;
    }

    /**
     * Returns the number of components selected.
     * 
     * 
     @return int
     **/
    public final int getMatchedCount() {
        return lastMmatchedCount;
    }

    /**
     * Select candidate components for a given term.
     * 
     * <p>
     * Returns a list of candidate monitoring features that match the term requirements
     * 
     * @param term
     *            the SLA term to match with a monitoring feature
     * 
     @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures selectComponentTerm(final String term) {
        ComponentMonitoringFeatures component = null;

        List<ComponentMonitoringFeatures> selectedComponentsList = new LinkedList<ComponentMonitoringFeatures>();
        selectedComponentsList = matchComponentFeaturesTerm(term);
        // LOGGER.debug("Selected " + selectedComponentsList.size()
        // + " CMF for term: " + term);
        lastMmatchedCount = selectedComponentsList.size();
        component = selectAppropriateComponent(selectedComponentsList);
        return component;
    }

    /**
     * Select candidate reasoners for a given operation and parameter types.
     * 
     * <p>
     * Returns a list of candidate reasoner features that match the term requirements
     * 
     * @param op
     *            the operation
     * @param parametertypes
     *            an array of parameter types
     * 
     @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures selectComponentReasoners(final String op, final String[] parametertypes) {
        ComponentMonitoringFeatures component = null;

        List<ComponentMonitoringFeatures> selectedComponentsList = new LinkedList<ComponentMonitoringFeatures>();
        selectedComponentsList = matchComponentFeaturesTerm(op, parametertypes);
        lastMmatchedCount = selectedComponentsList.size();
        // LOGGER.debug("Matched " + selectedComponentsList.size()
        // + " CMF for op: " + op);
        component = selectAppropriateComponent(selectedComponentsList);

        return component;
    }

    /**
     * Select candidate reasoners for a given operation.
     * 
     * <p>
     * Returns a list of candidate reasoner features that match the term requirements
     * 
     * @param op
     *            the operation
     * 
     @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures selectComponentReasoners(final String op) {
        ComponentMonitoringFeatures component = null;

        List<ComponentMonitoringFeatures> selectedComponentsList = new LinkedList<ComponentMonitoringFeatures>();
        selectedComponentsList = matchComponentFeaturesTerm(op);
        lastMmatchedCount = selectedComponentsList.size();
        getLogger().debug("Selected " + selectedComponentsList.size() + " CMF for op: " + op);
        component = selectAppropriateComponent(selectedComponentsList);

        return component;
    }

    /**
     * Select candidate sensors for a given event type.
     * 
     * <p>
     * Returns a list of candidate sensor features that match the term requirements
     * 
     * @param eventtype
     *            the event type
     * 
     @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures selectEventComponents(final String eventtype) {
        ComponentMonitoringFeatures component = null;

        List<ComponentMonitoringFeatures> selectedComponentsList = new LinkedList<ComponentMonitoringFeatures>();
        selectedComponentsList = matchComponentFeaturesTerm(eventtype);
        lastMmatchedCount = selectedComponentsList.size();
        getLogger().debug("Selected " + selectedComponentsList.size() + " CMFs for event type: " + eventtype);
        component = selectAppropriateComponent(selectedComponentsList);

        return component;
    }

    /**
     * Checks if a feature has the parameter types required.
     * 
     * @param mf
     *            a MonitoringFeature
     * @param units
     *            an array of types
     * 
     @return boolean
     * 
     **/
    private boolean hasMFInputUnits(final MonitoringFeature mf, final String[] units) {
        boolean result = false;
        HashMap<String, String> unitcheckmap = new HashMap<String, String>();

        // first check to see if feature has any inputs with type defined
        if (oFeatureUtils.getNumberofInputsWithUnits(mf) > 0 && units != null) {
            // for each parameter type specified in types check if input found
            int paramtypes = units.length;
            if (paramtypes > 0) {
                for (int param = 0; param < paramtypes; param++) {
                    String type = units[param];
                    if (oFeatureUtils.hasInputWithUnit(mf, type)) {
                        unitcheckmap.put(type, "true");
                    } else {
                        unitcheckmap.put(type, "false");
                    }
                }
            }
        }

        // check all types are supported
        Iterator<String> typecheckit = unitcheckmap.keySet().iterator();
        while (typecheckit.hasNext()) {
            result = true;
            String type = typecheckit.next();
            String typeres = unitcheckmap.get(type);
            if (typeres.equals("false")) {
                result = false;
            }
        }

        return result;

        /*
         * // assume true before actual check boolean result = true;
         * 
         * // check for required inputs int paramcntr = 0; if (mf instanceof FunctionImpl) { paramcntr = 0; FunctionImpl
         * fmf = (FunctionImpl) mf; Iterator<Basic> bfmf = fmf.getInputList().iterator(); while (bfmf.hasNext()) { Basic
         * aBasic = bfmf.next(); if (aBasic instanceof Primitive) { Primitive aPrimitive = (Primitive)aBasic; if
         * (aPrimitive.getUnit()!=null) { if (!(aPrimitive.getUnit().equals(units[paramcntr]))) { result = false; } else
         * { paramcntr++; } } } } } else { result = false; }
         * 
         * return result;
         */
    }

    /**
     * Checks if a feature has the parameter types required.
     * 
     * @param mf
     *            a MonitoringFeature
     * @param types
     *            an array of types
     * 
     @return boolean
     * 
     **/
    private boolean hasMFInputTypes(final MonitoringFeature mf, final String[] types) {
        // assume true before actual check
        boolean result = false;
        HashMap<String, String> typecheckmap = new HashMap<String, String>();

        // first check to see if feature has any inputs with type defined
        if (oFeatureUtils.getNumberofInputsWithTypes(mf) > 0 && types != null) {
            // for each parameter type specified in types check if input found
            int paramtypes = types.length;
            if (paramtypes > 0) {
                for (int param = 0; param < paramtypes; param++) {
                    String type = types[param];
                    if (oFeatureUtils.hasInputWithType(mf, type)) {
                        typecheckmap.put(type, "true");
                    } else {
                        typecheckmap.put(type, "false");
                    }
                }
            }
        }

        // check all types are supported
        Iterator<String> typecheckit = typecheckmap.keySet().iterator();
        while (typecheckit.hasNext()) {
            result = true;
            String type = typecheckit.next();
            String typeres = typecheckmap.get(type);
            if (typeres.equals("false")) {
                result = false;
            }
        }

        return result;
    }

    /**
     * Select candidate components for a given term and parameter types.
     * 
     * <p>
     * Returns a list of candidate features that match the term requirements
     * 
     * @param term
     *            the term
     * @param parametertypes
     *            an array of parameter types
     * 
     @return List<org.slasoi.monitoring.common.features.ComponentMonitoringFeatures >
     * 
     **/
    private List<ComponentMonitoringFeatures> matchComponentFeaturesTerm(final String term,
            final String[] parametertypes) {
        List<ComponentMonitoringFeatures> matchedComponentsList = new LinkedList<ComponentMonitoringFeatures>();

        String strParamtypes = "";
        // build parameter types string
        for (String type : parametertypes) {
            strParamtypes = strParamtypes + " : " + type;
        }

        getLogger().debug("Matching Term: " + term + " with Types" + strParamtypes);
        if (cmfs != null) {
            // loop through all the ComponentMonitoringFeatures and select those
            // with the term
            // convert array to list
            List<ComponentMonitoringFeatures> listOfCMF = Arrays.asList(cmfs);
            Iterator<ComponentMonitoringFeatures> listofCMFIT = listOfCMF.iterator();
            while (listofCMFIT.hasNext()) {
                ComponentMonitoringFeatures cmf = listofCMFIT.next();
                if (cmf != null && cmf.getMonitoringFeatures() != null) {
                    List<MonitoringFeature> mfslist = Arrays.asList(cmf.getMonitoringFeatures());
                    Iterator<MonitoringFeature> mfslistit = mfslist.iterator();
                    while (mfslistit.hasNext()) {
                        MonitoringFeature mf = mfslistit.next();
                        if (mf != null && mf.getName().equals(term)) {
                            // check for required inputs
                            if (hasMFInputTypes(mf, parametertypes)) {
                                matchedComponentsList.add(cmf);
                            } else if (hasMFInputUnits(mf, parametertypes)) {
                                matchedComponentsList.add(cmf);
                            }
                        }
                    }
                }
            }
        }

        return matchedComponentsList;
    }

    /**
     * Select candidate components for a given term.
     * 
     * <p>
     * Returns a list of candidate features that match the term requirements
     * 
     * @param term
     *            the term
     * 
     @return List<org.slasoi.monitoring.common.features.ComponentMonitoringFeatures >
     * 
     **/
    private List<ComponentMonitoringFeatures> matchComponentFeaturesTerm(final String term) {
        List<ComponentMonitoringFeatures> matchedComponentsList = new LinkedList<ComponentMonitoringFeatures>();

        if (cmfs != null) {
            // loop through all the ComponentMonitoringFeatures and select those
            // with the term
            // convert array to list
            List<ComponentMonitoringFeatures> listOfCMF = Arrays.asList(cmfs);
            Iterator<ComponentMonitoringFeatures> listofCMFIT = listOfCMF.iterator();
            while (listofCMFIT.hasNext()) {
                ComponentMonitoringFeatures cmf = listofCMFIT.next();
                if (cmf != null && cmf.getMonitoringFeatures() != null) {
                    List<MonitoringFeature> mfslist = Arrays.asList(cmf.getMonitoringFeatures());
                    Iterator<MonitoringFeature> mfslistit = mfslist.iterator();
                    while (mfslistit.hasNext()) {
                        MonitoringFeature mf = mfslistit.next();
                        if (mf != null && mf.getName().equals(term)) {
                            matchedComponentsList.add(cmf);
                        }
                    }
                }
            }
        }

        return matchedComponentsList;
    }

    /**
     * Selects an appropriate component based upon some criteria.
     * 
     * <p>
     * Note: current implementation simply selects the first in list of candidate components
     * 
     * @param candidatecomponents
     *            a list of monitoring features as candidate components
     * 
     @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    private ComponentMonitoringFeatures selectAppropriateComponent(
            final List<ComponentMonitoringFeatures> candidatecomponents) {
        ComponentMonitoringFeatures component = null;

        if (candidatecomponents != null && candidatecomponents.size() > 0) {
            // select the first candidate component (basic selection)
            component = candidatecomponents.get(0);
        }

        return component;
    }

}
