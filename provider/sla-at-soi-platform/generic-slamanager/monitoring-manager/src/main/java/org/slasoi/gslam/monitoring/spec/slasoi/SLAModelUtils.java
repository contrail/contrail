/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2061 $
 * @lastrevision   $Date: 2011-06-06 22:54:35 +0200 (pon, 06 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/slasoi/SLAModelUtils.java $
 */

package org.slasoi.gslam.monitoring.spec.slasoi;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.ext.Nominal;

/**
 * Supports SLA Model access and parsing.
 **/
public class SLAModelUtils extends MonitoringLogger {

    /**
     * Constructor.
     * 
     * <p>
     * Initialises logging features
     **/
    public SLAModelUtils() {
        initLogging(this.getClass().getName());
    }

    /**
     * Duplicates an SLA model instance and returns a new SLA.
     * 
     * 
     @param model
     *            source model
     * 
     @return org.slasoi.slamodel.sla.SLA
     * 
     @see org.slasoi.slamodel.sla.SLA
     * 
     **/
    public final SLA cloneSLA(final SLA model) {
        // removes all other Agreement Terms from the SLA model except that
        // specified by the AgreementTerm ID
        SLA newmodel = new SLA();

        // clone the main parts of the original SLA model
        newmodel.setEffectiveFrom(model.getEffectiveFrom());
        newmodel.setEffectiveUntil(model.getEffectiveUntil());
        newmodel.setTemplateId(model.getTemplateId());
        newmodel.setParties(model.getParties());
        newmodel.setAgreedAt(model.getAgreedAt());
        newmodel.setDescr(model.getDescr());
        newmodel.setInterfaceDeclrs(model.getInterfaceDeclrs());
        newmodel.setUuid(model.getUuid());
        newmodel.setVariableDeclrs(model.getVariableDeclrs());

        // create a copy of the existing AgreementTerms
        AgreementTerm[] oldterms = model.getAgreementTerms();
        AgreementTerm[] newterms = new AgreementTerm[model.getAgreementTerms().length];
        for (int termno = 0; termno < oldterms.length; termno++) {
            newterms[termno] = oldterms[termno];
        }
        newmodel.setAgreementTerms(newterms);

        return newmodel;
    }

    /**
     * Removes all other Guarantee States from an Agreement Term than the one specified.
     * 
     * 
     @param model
     *            source model
     * @param agreementTermID
     *            the ID of the Agreement Term
     * @param guaranteeStateID
     *            the ID of the Guarantee State to retain
     * 
     @return org.slasoi.slamodel.sla.SLA
     * 
     @see org.slasoi.slamodel.sla.SLA org.slasoi.slamodel.primitives.ID
     * 
     **/
    @SuppressWarnings("deprecation")
    public final SLA removeAllButGuaranteedState(final SLA model, final ID agreementTermID, final ID guaranteeStateID) {
        // removes all other Guarantee States from the AgreementTerm specified
        // by ID, in the SLA model

        // removes all other Agreement Terms from the SLA model except that
        // specified by the AgreementTerm ID
        SLA newmodel = removeAllButAgreementTerm(model, agreementTermID);

        // get the GTerm to exclude others
        Guaranteed selgterm = null;
        AgreementTerm selterm = null;
        AgreementTerm[] filterms = newmodel.getAgreementTerms();
        for (AgreementTerm term : filterms) {
            if (term != null) {
                for (Guaranteed gterm : term.getGuarantees()) {
                    if (gterm.getId().toString().equals(guaranteeStateID.toString())) {
                        selterm = term;
                        selgterm = gterm;
                    }
                }
            }
        }

        if (selterm != null && selgterm != null) {
            // clone the AgreementTerm
            AgreementTerm newterm = new AgreementTerm();
            newterm.setDescr(selterm.getDescr());
            newterm.setId(selterm.getId());
            newterm.setPrecondition(selterm.getPrecondition());
            newterm.setVariableDeclrs(selterm.getVariableDeclrs());
            Guaranteed[] newguarantees = new Guaranteed[1];
            newguarantees[0] = selgterm;
            newterm.setGuarantees(newguarantees);
            AgreementTerm[] newterms = new AgreementTerm[1];
            newterms[0] = newterm;
            newmodel.setAgreementTerms(newterms);
        }

        return newmodel;
    }

    /**
     * Removes all other Agreement Terms from an SLA than the one specified.
     * 
     * 
     @param model
     *            source model
     * @param agreementTermID
     *            the ID of the Agreement Term
     * 
     @return org.slasoi.slamodel.sla.SLA
     * 
     @see org.slasoi.slamodel.sla.SLA org.slasoi.slamodel.primitives.ID
     * 
     **/
    public final SLA removeAllButAgreementTerm(final SLA model, final ID agreementTermID) {
        // removes all other Agreement Terms from the SLA model except that
        // specified by the AgreementTerm ID
        SLA newmodel = cloneSLA(model);

        AgreementTerm[] newterms = newmodel.getAgreementTerms();

        // get the agreement terms
        for (int termno = 0; termno < newterms.length; termno++) {
            AgreementTerm term = newterms[termno];
            if (term != null && !(term.getId() == agreementTermID)) {
                newterms[termno] = null;
            }
        }

        return newmodel;
    }

    /**
     * Indicates whether a unit is a core model Unit.
     * 
     * @param unitname
     *            String
     * 
     @return boolean
     * 
     @see org.slasoi.slamodel.sla.core
     * 
     **/
    public final boolean isCoreUnit(final String unitname) {
        boolean found = false;
        units slaunits = new units();
        Nominal[] noms = slaunits.getNominals();
        List<Nominal> list = Arrays.asList(noms);
        Iterator<Nominal> nomit = list.iterator();
        while (nomit.hasNext() && !found) {
            Nominal nom = nomit.next();
            if (nom.term().toString().equals(unitname)) {
                found = true;
            }
        }
        return found;
    }
}
