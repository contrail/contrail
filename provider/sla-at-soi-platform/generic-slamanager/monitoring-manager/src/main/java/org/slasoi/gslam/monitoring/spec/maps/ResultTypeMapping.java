/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/maps/ResultTypeMapping.java $
 */

package org.slasoi.gslam.monitoring.spec.maps;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.selection.IsNumber;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.meta;

/**
 * Manages Result Types mapping between SLA Terms and types required.
 **/
public class ResultTypeMapping extends MonitoringLogger {
    /** Configuration file path. **/
    private String cfgfilepath =
            System.getenv("SLASOI_HOME") + File.separator
                    + "generic-slamanager\\monitoring-manager\\term-resulttype-map.cfg";
    /** Type mapping. **/
    private HashMap<String, String> typemap = new HashMap<String, String>();

    /**
     * Constructor
     * 
     * <p>
     * Initialises logging and builds result type from configuration file.
     * 
     **/
    public ResultTypeMapping() {
        // initialise logging
        initLogging(this.getClass().getName());
        buildTermResultMappings();
    }

    /**
     * Construct the Result Types Mappings from configuration file
     * 
     * <p>
     * The configuration file root path is specified in the "SLASOI_HOME" env. variable The local path is specified as
     * "SLASOI_HOME"\generic-slamanager\monitoring -manager\term-resulttype-map.cfg
     * 
     **/
    private void buildTermResultMappings() {
        getLogger().debug("Reading Term Result Type mapping configuaration file: " + cfgfilepath);

        FileReader read;
        try {
            read = new FileReader(cfgfilepath);

            BufferedReader br = new BufferedReader(read);
            String row;
            while ((row = br.readLine()) != null) {
                // parse the line
                String[] params = row.split(",");
                if ((params != null) && (params.length == 2)) {
                    String term = params[0];
                    String type = params[1];
                    typemap.put(term, type);
                } else {
                    getLogger().error("Could not parse type mapping entry: " + row);
                }
            }
            // getLogger().debug("Finished processing type mapping - no of terms: " + typemap.size());
        } catch (Exception fileNotFoundExeception) {
            getLogger().error("Term Result Type map configuration file not found.");
        }
    }

    /**
     * Retrieves a Result Type for a Term reference string.
     * 
     * <p>
     * The terms and result types should be specified in the term-results configuration file.
     * 
     * @param term
     *            String reference
     * 
     @return String
     * 
     **/
    public final String getResultTypeFromTerm(final String term) {
        String localTerm = term;
        getLogger().debug("Checking result type for term: " + localTerm);
        // now check if there is an operation mapping for this term
        OperationMap opmap = new OperationMap();
        String opmapstr = opmap.getSLASOIFromCond(localTerm);
        if (opmapstr.length() > 0) {
            getLogger().debug("Result type for term: " + localTerm + " was replaced with: " + opmapstr);
            localTerm = opmapstr;
        }
        if (typemap.containsKey(localTerm)) {
            getLogger().debug("Found result type for term: " + localTerm + " as " + typemap.get(localTerm));
            return typemap.get(localTerm);
        } else {
            if (localTerm.equals("http://www.slaatsoi.org/commonTerms#true")
                    || localTerm.equals("http://www.slaatsoi.org/commonTerms#false")) {
                return meta.$BOOLEAN;
            } else {
                getLogger().debug("Result type for term: " + localTerm + " was not found - assuming it is a constant.");
            }
        }
        // check if term is for a constant number, else assume text
        String newterm = localTerm.replace(common.$base, "");
        newterm = newterm.replace("\"", "");
        IsNumber testtoken = new IsNumber(newterm);
        if (testtoken.getNumber()) {
            return meta.$NUMBER;
        } else {
            return meta.$TEXT;
        }
    }

}
