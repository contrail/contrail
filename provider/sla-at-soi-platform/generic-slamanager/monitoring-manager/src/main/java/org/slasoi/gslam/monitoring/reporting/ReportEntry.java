/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 313 $
 * @lastrevision   $Date: 2010-12-06 14:31:14 +0200 (Mon, 06 Dec 2010) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/reporting/ReportEntry.java $
 */
package org.slasoi.gslam.monitoring.reporting;

import java.util.LinkedList;

/**
 * Represents a Report Entry for Monitorability Reporting.
 **/
public class ReportEntry {
    /**
     * Types of report entry.
     **/
    public enum Type {
        /** DEFAULT TYPE. **/
        DEFAULT,
        /** TERM TYPE. **/
        TERM,
        /** MATCHING TYPE. **/
        MATCHING,
        /** SELECTION TYPE. **/
        SELECTION,
        /** CONFIG TYPE. **/
        CONFIG,
        /** AGREEMENT TYPE. **/
        AGREEMENT,
        /** STATE TYPE. **/
        STATE
    }

    /** Steps in reporting. **/
    public enum Step {
        /** DEFAULT TYPE. **/
        DEFAULT,
        /** DEFAULT TYPE. **/
        TERM_BUILD,
        /** DEFAULT TYPE. **/
        TERM_BUILD_VAR,
        /** DEFAULT TYPE. **/
        TERM_BUILD_FUNCTION,
        /** DEFAULT TYPE. **/
        TERM_PRECONDITION,
        /** Steps in reporting. **/
        TERM_PROCESS_EXP,
        /** DEFAULT TYPE. **/
        PARSE_EXPRESSION,
        /** DEFAULT TYPE. **/
        MATCHING_TERMS,
        /** DEFAULT TYPE. **/
        MATCHING_EVENTS,
        /** DEFAULT TYPE. **/
        SELECTED_REASONER,
        /** DEFAULT TYPE. **/
        SELECTED_SENSOR,
        /** DEFAULT TYPE. **/
        CONFIG_REASONER,
        /** DEFAULT TYPE. **/
        CONFIG_SENSOR,
    }

    /**
     * Results in report step.
     **/
    public enum Result {
        /** DEFAULT TYPE. **/
        DEFAULT,
        /** DEFAULT TYPE. **/
        ERROR,
        /** DEFAULT TYPE. **/
        DEBUG,
        /** DEFAULT TYPE. **/
        WARN,
        /** DEFAULT TYPE. **/
        MONITORABLE,
        /** DEFAULT TYPE. **/
        NOTMONITORABLE
    }

    /** Path. **/
    private String entryPath = "";
    /** Description. **/
    private String entryDesc = "";
    /** Value. **/
    private String entryValue = "";
    /** Term ID. **/
    private String entryTermID = "";
    /** Entry Step. **/
    private Step entryStep = Step.DEFAULT;
    /** Entry Type. **/
    private Type entryType = Type.DEFAULT;
    /** Result. **/
    private Result entryResult = Result.DEFAULT;
    /** Generic Entry Data (any associated object). **/
    private Object entryData = null;


    /**
     * Construct a new report entry.
     * 
     * @param path
     *            the path to an SLA element
     * @param id
     *            the id of an SLA element
     * @param type
     *            the type of report entry
     * @param step
     *            the step of reporting
     * @param desc
     *            the step of reporting
     * @param value
     *            the value obtained from the step
     * @param result
     *            the result of this step
     **/
    public ReportEntry(final String path, final String id, final Type type, final Step step, final String desc,
            final String value, final Result result) {
        setPath(path);
        setID(id);
        setType(type);
        setStep(step);
        setDescription(desc);
        setResult(result);
        setValue(value);
    }

    /**
     * Construct a new report entry.
     * 
     * @param path
     *            the path to an SLA element
     * @param id
     *            the id of an SLA element
     * @param type
     *            the type of report entry
     * @param step
     *            the step of reporting
     * @param desc
     *            the step of reporting
     * @param value
     *            the value obtained from the step
     * @param result
     *            the result of this step
     * @param data
     *            the data entry of this step
     **/
    public ReportEntry(final String path, final String id, final Type type, final Step step, final String desc,
            final String value, final Result result, final Object data) {
        setPath(path);
        setID(id);
        setType(type);
        setStep(step);
        setDescription(desc);
        setResult(result);
        setValue(value);
        setData(data);
    }

    /**
     * toArray - returns a report entry as a set of Strings.
     * 
     * @return String array
     * 
     **/
    public final String[] toArray() {
        LinkedList<String> colentry = new LinkedList<String>();
        colentry.add(getPath());
        colentry.add(getID().toString());
        colentry.add(getType().toString());
        colentry.add(getStep().toString());
        colentry.add(getDescription());
        colentry.add(getValue());
        colentry.add(getResult().toString());
        String[] arraystr = colentry.toArray(new String[colentry.size()]);
        return arraystr;
    }

    /**
     * Retrieve a string array of report column names.
     * 
     * @return String array
     * 
     **/
    public final String[] getReportCols() {
        LinkedList<String> colentry = new LinkedList<String>();
        colentry.add("Path");
        colentry.add("ID");
        colentry.add("Type");
        colentry.add("Step");
        colentry.add("Description");
        colentry.add("Value");
        colentry.add("Result");
        String[] arraystr = colentry.toArray(new String[colentry.size()]);
        return arraystr;
    }

    /**
     * Set the Path of the report entry.
     * 
     * @param path
     *            the path to an SLA element
     **/
    public final void setPath(final String path) {
        entryPath = path;
    }

    /**
     * Retrieve the report entry element path.
     * 
     * @return String
     * 
     **/
    public final String getPath() {
        return entryPath;
    }

    /**
     * Set the ID of the report entry.
     * 
     * @param id
     *            the id to an SLA element
     **/
    public final void setID(final String id) {
        entryTermID = id;
    }

    /**
     * Retrieve the report entry element ID.
     * 
     * @return String
     * 
     **/
    public final String getID() {
        return entryTermID;
    }

    /**
     * Set the Type of the report entry.
     * 
     * @param type
     *            the type to an SLA element
     **/
    public final void setType(final Type type) {
        entryType = type;
    }

    /**
     * Retrieve the report entry element Type.
     * 
     * @return Type
     * 
     **/
    public final Type getType() {
        return entryType;
    }

    /**
     * Set the Step of the report entry.
     * 
     * @param step
     *            the step of the report entry
     **/
    public final void setStep(final Step step) {
        entryStep = step;
    }

    /**
     * Retrieve the report entry element Step.
     * 
     * @return Step
     * 
     **/
    public final Step getStep() {
        return entryStep;
    }

    /**
     * Set the Result of the report entry.
     * 
     * @param result
     *            the result of a report entry
     **/
    public final void setResult(final Result result) {
        entryResult = result;
    }

    /**
     * Retrieve the report entry element Result.
     * 
     * @return Result
     * 
     **/
    public final Result getResult() {
        return entryResult;
    }

    /**
     * Set the Description of the report entry.
     * 
     * @param desc
     *            the path to an SLA element
     **/
    public final void setDescription(final String desc) {
        entryDesc = desc;
    }

    /**
     * Retrieve the report entry element Description.
     * 
     * @return String
     * 
     **/
    public final String getDescription() {
        return entryDesc;
    }

    /**
     * Set the Value of the report entry.
     * 
     * @param value
     *            the value of a report entry
     **/
    public final void setValue(final String value) {
        entryValue = value;
    }

    /**
     * Retrieve the report entry element Value.
     * 
     * @return String
     * 
     **/
    public final String getValue() {
        return entryValue;
    }

    /**
     * Set the Data of the report entry.
     * 
     * @param data
     *            the data of the report entry
     **/
    public final void setData(final Object data) {
        entryData = data;
    }

    /**
     * Retrieve the report entry element Data.
     * 
     * @return Data
     * 
     **/
    public final Object getData() {
        return entryData;
    }
}
