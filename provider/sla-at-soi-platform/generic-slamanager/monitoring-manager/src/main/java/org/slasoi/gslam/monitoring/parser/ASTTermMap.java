/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2845 $
 * @lastrevision   $Date: 2011-07-26 23:13:37 +0200 (tor, 26 jul 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/parser/ASTTermMap.java $
 */

package org.slasoi.gslam.monitoring.parser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.parser.core.Node;
import org.slasoi.slamodel.primitives.ID;

/**
 * The ASTTermMap class provides a helper class for managing term mappings between SLA and MM vocabs.
 **/
public class ASTTermMap extends MonitoringLogger {

    /***
     * Map of Agreement Term ASTs.
     * 
     **/
    @SuppressWarnings("unchecked")
    private HashMap<ID, LinkedList> termASTs = new HashMap<ID, LinkedList>();
    /** AgreementTerm preconditions map. **/
    private HashMap<ID, Node> atPreconditions = new HashMap<ID, Node>();
    /** GuaranteedState preconditions map. **/
    private HashMap<ID, Node> gtPreconditions = new HashMap<ID, Node>();

    /***
     * ASTTermMap constructor.
     * 
     * <p>
     * Creates a new instance of an ASTTermMap class.
     * 
     **/
    public ASTTermMap() {
        initLogging(this.getClass().getName());
    }

    /**
     * Retrieve the keySet of IDs in the TermMap.
     * 
     * @return java.util.Set<org.slasoi.slamodel.primitives.ID>
     * 
     @see org.slasoi.slamodel.primitives.ID
     * 
     **/
    public final Set<ID> keySet() {
        return termASTs.keySet();
    }

    /**
     * Retrieve the number of terms for an Agreement Term ID.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * 
     @return int the number of terms in the map
     * 
     **/
    public final int size(final ID agreementTermID) {
        int asize = 0;
        if (termASTs.containsKey(agreementTermID)) {
            asize = termASTs.get(agreementTermID).size();
        }
        return asize;
    }

    /**
     * Clear all term entries for an Agreement Term ID.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * 
     **/
    public final void clear(final ID agreementTermID) {
        if (termASTs.containsKey(agreementTermID)) {
            termASTs.put(agreementTermID, new LinkedList<Node>());
        }
    }

    /**
     * Add a Guarantee Term entry for an Agreement Term ID.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * @param gTermNode
     *            a Guarantee Term AST node
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    @SuppressWarnings("unchecked")
    public final void put(final ID agreementTermID, final Node gTermNode) {
        if (termASTs.containsKey(agreementTermID)) {
            LinkedList gtermlist = (LinkedList) termASTs.get(agreementTermID);
            gtermlist.add(gTermNode);
        } else {
            LinkedList<Node> gtermlist = new LinkedList<Node>();
            gtermlist.add(gTermNode);
            termASTs.put(agreementTermID, gtermlist);
        }
    }

    /**
     * Add a precondition entry for an Agreement Term ID.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * @param condNode
     *            a precondition AST node
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    public final void putATCondition(final ID agreementTermID, final Node condNode) {
        atPreconditions.put(agreementTermID, condNode);
    }

    /**
     * Add a precondition entry for a Guaranteed State ID.
     * 
     * @param gTermID
     *            an Guarantee Term ID
     * @param condNode
     *            a precondition AST node
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    public final void putGTCondition(final ID gTermID, final Node condNode) {
        gtPreconditions.put(gTermID, condNode);
    }

    /**
     * Retrieves a list of GTerm AST node roots based upon AgreementTerm ID.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * 
     @return java.util.List<org.slasoi.gslam.monitoring.parser.core.Node>
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    // retrieves a list of GTerm AST node roots based upon AgreementTerm ID
    @SuppressWarnings("unchecked")
    public final List<Node> get(final ID agreementTermID) {
        List<Node> list = null;
        if (termASTs.containsKey(agreementTermID)) {
            list = (List) termASTs.get(agreementTermID);
        }
        return list;
    }

    /**
     * Retrieves a precondition AST for an AgreementTerm.
     * 
     * @param agreementTermID
     *            an Agreement Term ID
     * 
     @return java.util.List<org.slasoi.gslam.monitoring.parser.core.Node>
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    // retrieves a list of GTerm AST node roots based upon AgreementTerm ID
    public final Node getATPrecondition(final ID agreementTermID) {
        Node ast = null;
        if (atPreconditions.containsKey(agreementTermID)) {
            ast = (Node) atPreconditions.get(agreementTermID);
        }
        return ast;
    }

    /**
     * Retrieves a precondition AST for a Guarantee Term.
     * 
     * @param gTermID
     *            an Guarantee Term ID
     * 
     @return java.util.List<org.slasoi.gslam.monitoring.parser.core.Node>
     * 
     @see org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    // retrieves a list of GTerm AST node roots based upon AgreementTerm ID
    public final Node getGTPrecondition(final ID gTermID) {
        Node ast = null;
        if (gtPreconditions.containsKey(gTermID)) {
            ast = (Node) gtPreconditions.get(gTermID);
        }
        return ast;
    }
}
