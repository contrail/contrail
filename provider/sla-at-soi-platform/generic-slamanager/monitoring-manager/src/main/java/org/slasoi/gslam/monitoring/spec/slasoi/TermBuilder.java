/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/slasoi/TermBuilder.java $
 */

package org.slasoi.gslam.monitoring.spec.slasoi;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.parser.ASTTermMap;
import org.slasoi.gslam.monitoring.parser.AgreementParser;
import org.slasoi.gslam.monitoring.parser.core.Node;
import org.slasoi.gslam.monitoring.reporting.MonitorabilityReport;
import org.slasoi.gslam.monitoring.reporting.ReportEntry;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Result;
import org.slasoi.gslam.monitoring.spec.maps.OperationMap;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.LIST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.ServiceRef;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.ext.Category;
import org.slasoi.slamodel.vocab.ext.Functional;

/**
 * Parses an Agreement or Guarantee Term and builds AST representation.
 **/
public class TermBuilder extends MonitoringLogger {
    /** Current SLA Model. **/
    private SLA currmodel = null;
    /** SLA Model Utils. **/
    private SLAModelUtils modelutils = new SLAModelUtils();
    /** Agreement Term. **/
    private AgreementTerm currTerm = null;
    /** A Common Vocabulary. **/
    private common commonVocab = new common();
    /** Operation Mappings. **/
    private OperationMap opmap = new OperationMap();
    /** Reporting. **/
    private MonitorabilityReport termreporter = new MonitorabilityReport();
    /** The SLA id. **/
    private UUID thisSLAID;
    /** The Reporting Type. **/
    private ReportEntry.Type reportingType = ReportEntry.Type.TERM;

    /**
     * Class representing a Variable.
     **/
    private final class Variable {

        /**
         * Constructor for Variable.
         **/
        private Variable() {
            operator = "";
        }

        /** Operator. **/
        private String operator = "";
        /** Value. **/
        private String value = "";
        /** ValueType. **/
        private String valuetype = "";
    }

    /**
     * Constructor.
     **/
    public TermBuilder() {
        // initialise logging
        initLogging(this.getClass().getName());
    }

    /**
     * Set the model to parse and process.
     * 
     * 
     @param model
     *            source model
     * 
     @see org.slasoi.slamodel.sla.SLA
     * 
     **/
    public final void setModel(final SLA model) {
        currmodel = model;
    }

    /**
     * Get the report from building terms.
     * 
     * @return MonitorabilityReport
     @see org.slasoi.slamodel.sla.SLA
     * 
     **/
    public final MonitorabilityReport getReporter() {
        return termreporter;
    }

    /**
     * Build a Term Mapping from a set of Agreement Terms.
     * 
     * 
     @param slaID
     *            the ID for the SLA
     @param terms
     *            an array of Agreement Terms
     * 
     @return org.slasoi.gslam.monitoring.parser.ASTTermMap
     * 
     **/
    public final ASTTermMap buildTermSet(final UUID slaID, final AgreementTerm[] terms) {
        // HashMap<ID,Node> TermASTs = new HashMap<ID,Node>();
        thisSLAID = slaID;

        ASTTermMap termMap = new ASTTermMap();

        // agreement list iterator
        List<AgreementTerm> listOfTerms = Arrays.asList(terms);
        Iterator<AgreementTerm> termListIT = listOfTerms.iterator();
        while (termListIT.hasNext()) {
            AgreementTerm term = termListIT.next();
            currTerm = term;
            String precondition = "";
            // guarantee term list iterator
            List<Guaranteed> listofGuarantees = Arrays.asList(term.getGuarantees());
            Iterator<Guaranteed> guaranteeIT = listofGuarantees.iterator();
            logLine();
            getLogger().debug("AgreementTerm: " + term.getId() + " Guarantees: " + listofGuarantees.size());
            logLine();

            // handle any AgreementTerm preconditions
            if (term.getPrecondition() != null) {
                precondition = processExpression(slaID.toString(), slaID, term.getPrecondition(), "");
                String parsecond = "\"" + term.getId() + "\" " + precondition;
                getLogger().debug("Precondition: " + parsecond);
             // guarantee term parsing to internal AST
                Node preconds = AgreementParser.parse(parsecond);
                if (preconds != null) {
                    termMap.putATCondition(term.getId(), preconds);
                    getLogger().debug("Precondition added for AgreementTerm " + term.getId());
                    termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(term.getId() + "/"
                            + "precondition", "precondition", reportingType,
                            ReportEntry.Step.TERM_PRECONDITION, "Precondition added for AgreementTerm.",
                            parsecond, Result.DEFAULT));
                } else {
                    getLogger().warn("Precondition parser root was null.");
                    termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(term.getId() + "/"
                            + "precondition", "precondition", reportingType,
                            ReportEntry.Step.TERM_PRECONDITION, "Parser root was null.", parsecond, Result.ERROR));
                }

            }

            while (guaranteeIT.hasNext()) {
                // guarantee term evaluation
                Guaranteed gtemp = guaranteeIT.next();
                if (gtemp instanceof Guaranteed.State) {
                    Guaranteed.State g = (State) gtemp;
                    getLogger().debug("Guaranteed State: " + g.getId());

                    // process any preconditions
                    if (g.getPrecondition() != null) {
                        precondition = processExpression(slaID.toString(), slaID, g.getPrecondition(), "");
                        String parsecond = "\"" + g.getId() + "\" " + precondition;
                        getLogger().debug("Precondition: " + parsecond);
                     // guarantee term parsing to internal AST
                        Node preconds = AgreementParser.parse(parsecond);
                        if (preconds != null) {
                            termMap.putGTCondition(g.getId(), preconds);
                            getLogger().debug("Precondition added for Guranteed State: "
                               + g.getId());
                            termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(g.getId() + "/"
                                    + "precondition", "precondition", reportingType,
                                    ReportEntry.Step.TERM_PRECONDITION, "Precondition added for Guaranteed State.",
                                    parsecond, Result.DEFAULT));
                        } else {
                            getLogger().warn("Precondition parser root was null.");
                            termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(g.getId() + "/"
                                    + "precondition", "precondition", reportingType,
                                    ReportEntry.Step.TERM_PRECONDITION, "Parser root was null.",
                                    parsecond, Result.ERROR));
                        }
                    }

                    // now process the state expression itself

                    String expr =
                            "\"" + g.getId() + "\" "
                                    + processExpression(term.getId() + "/" + g.getId(), g.getId(), g.getState(), "");
                    getLogger().debug("Expression: " + expr);
                    if (expr.length() > 0) {
                        // disabled for YR2
                        // expr = expr + " " + precondition;
                        getLogger().debug("Parse String: " + expr);

                        // log entry in monitorability reporter
                        termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(term.getId() + "/"
                                + g.getId(), g.getId().toString(), reportingType, ReportEntry.Step.TERM_PROCESS_EXP,
                                "Parse String", expr, Result.DEFAULT));

                        // guarantee term parsing to internal AST
                        Node root = AgreementParser.parse(expr);
                        if (root != null) {
                            // parser.visit(root);
                            getLogger().debug(
                                    "ASTTreeNode[" + termMap.size(term.getId()) + "] added for Guarantee State: "
                                            + g.getId());
                            // TermASTs.put(term.getId(), root);
                            termMap.put(term.getId(), root);
                        } else {
                            getLogger().warn("Parser root was null.");
                            termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(term.getId() + "/"
                                    + g.getId(), g.getId().toString(), reportingType,
                                    ReportEntry.Step.TERM_PROCESS_EXP, "Parser root was null.", expr, Result.ERROR));
                        }
                    }
                } else {
                    getLogger().warn("Guarantee object not supported - class: " + gtemp.getClass().getName());
                }
            }
        }

        return termMap;
    }

    /**
     * Get a ServiceRef for a VariableDeclr.
     * 
     * @param vardec
     *            the variabledeclr
     * @return ServiceRef
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ServiceRef getServiceRefForVariableDeclr(final VariableDeclr vardec) {
        ServiceRef sref = null;

        return sref;
    }

    /**
     * Retrieve a Category for a Term.
     * 
     * @param stnd
     *            a Term
     * 
     @return org.slasoi.slamodel.vocab.ext.Category
     * 
     @see org.slasoi.slamodel.primitives.STND
     * 
     **/
    @SuppressWarnings("unused")
    private Category getCategoryforTerm(final STND stnd) {
        Category cat = null;
        boolean found = false;
        List<Functional> functionals = Arrays.asList(commonVocab.getFunctionals());
        Iterator<Functional> functionalit = functionals.iterator();
        while (functionalit.hasNext() && !found) {
            Functional func = functionalit.next();
            if (func.op() == stnd) {
                cat = func.category();
                found = true;
            }
        }
        return cat;
    }

    /**
     * Build a string representation of a ValueExpr.
     * 
     * 
     @param expr
     *            a ValueExpr
     * 
     @return String
     * 
     @see org.slasoi.slamodel.primitives.ValueExpr
     * 
     **/
    @SuppressWarnings("unused")
    private String buildValueExpr(final ValueExpr expr) {
        String varid = expr.toString();
        getLogger().debug("ValueExpr: " + varid);
        return varid;
    }

    /**
     * Builds an expression string by looping through all subexpressions.
     * 
     * 
     @param path
     *            Path to element to process
     * @param id
     *            ID of element to process
     * @param expr
     *            an Object of an expression
     * @param subject
     *            the subject of the expression
     * 
     @return String
     * 
     **/
    private String processExpression(final String path, final ID id, final Object expr, final String subject) {
        String value = "";

        if (expr instanceof TypeConstraintExpr || expr instanceof FunctionalExpr || expr instanceof SimpleDomainExpr
                || expr instanceof CompoundConstraintExpr || expr instanceof CONST || expr instanceof ID
                || expr instanceof CompoundDomainExpr) {
            getLogger().debug("Process Expression: " + expr.getClass().toString());
            termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(path, id.toString(), reportingType,
                    ReportEntry.Step.TERM_PROCESS_EXP, "Process Expression", expr.toString(), Result.DEFAULT));
            if (expr instanceof TypeConstraintExpr) {
                value = buildTypeConstraintExpr(path, id, (TypeConstraintExpr) expr);
            } else if (expr instanceof FunctionalExpr) {
                value = buildFunctionalExpr(path, id, (FunctionalExpr) expr);
            } else if (expr instanceof SimpleDomainExpr) {
                value = buildSimpleDomainExpr(path, id, (SimpleDomainExpr) expr, subject);
            } else if (expr instanceof CompoundDomainExpr) {
                value = buildCompoundDomainExpr(path, id, (CompoundDomainExpr) expr, subject);
            } else if (expr instanceof CompoundConstraintExpr) {
                value = buildCompoundConstraintExpr(path, id, (CompoundConstraintExpr) expr);
            } else if (expr instanceof CONST) {
                value = buildConst((CONST) expr);
            } else if (expr instanceof ID) {
                value = buildID((ID) expr);
            }
        } else {
            getLogger().error("The expression type: " + expr.getClass().toString() + " is not supported.");
        }

        return value;
    }

    /**
     * Build a string representation of a CONST.
     * 
     * 
     @param expr
     *            a CONST
     * 
     @return String
     * 
     @see org.slasoi.slamodel.primitives.CONST
     * 
     **/
    private String buildConst(final CONST expr) {
        String constvalue = expr.getValue();
        return constvalue;
    }

    /**
     * Build a string representation of an ID.
     * 
     * 
     @param expr
     *            an ID
     * 
     @return String
     * 
     @see org.slasoi.slamodel.primitives.ID
     * 
     **/
    private String buildID(final ID expr) {
        String value = expr.getValue();
        return value;
    }

    /**
     * Build a string representation of a FunctionalExpr.
     * 
     * 
     @param path
     *            Path to element to process
     * @param id
     *            ID of element to process
     * @param expr
     *            a FunctionalExpr
     * 
     @return String
     * 
     @see org.slasoi.slamodel.core.FunctionalExpr
     * 
     **/
    private String buildFunctionalExpr(final String path, final ID id, final FunctionalExpr expr) {
        String funcstr = "";
        String op = expr.getOperator().toString();
        String params = "";
        List<ValueExpr> valueexprs = Arrays.asList(expr.getParameters());
        Iterator<ValueExpr> valueexprsIT = valueexprs.iterator();
        boolean notfirst = false;
        while (valueexprsIT.hasNext()) {
            if (notfirst) {
                params = params + ",";
            }
            ValueExpr value = valueexprsIT.next();
            params = params + processExpression(path, id, value, "");
            notfirst = true;
        }
        funcstr = op;
        if (!op.equals("and") && !op.equals("or") && params.length() > 0) {
            funcstr = funcstr + "(" + params + ")";
        }
        getLogger().debug("Value: " + funcstr);
        termreporter
                .addReport(thisSLAID.toString(), termreporter.buildEntry(path, id.toString(), reportingType,
                        ReportEntry.Step.TERM_BUILD_FUNCTION, "Process Functional Expression", expr.toString(),
                        Result.DEFAULT));
        return funcstr;
    }

    /**
     * Build a string representation of a SimpleDomainExpr.
     * 
     * 
     @param path
     *            Path to element to process
     * @param id
     *            ID of element to process
     * @param expr
     *            an Object of an expression
     * @param subject
     *            the subject of the expression
     * 
     @return String
     * 
     @see org.slasoi.slamodel.core.SimpleDomainExpr
     * 
     **/
    private String buildSimpleDomainExpr(final String path, final  ID id, final SimpleDomainExpr expr,
            final String subject) {
        String exp = "";
        String op = expr.getComparisonOp().toString();
        String value = expr.getValue().toString();
        // split value and return only first half
        String[] values = value.split(" ");
        value = values[0];
        // check if value is a variable id in Model or current Agreement Term
        VariableDeclr var = currmodel.getVariableDeclr(value);
        if (var == null) {
            var = currTerm.getVariableDeclr(value);
        }
        if (var != null) {
            Variable varrep = buildVariable(path, id, var);
            value = varrep.value;
        }

        // strip any quotes out of string
        // value = value.replace("\"","");

        // exp = op + " " + value;
        exp = op + "(";
        if (subject.length() > 0) {
            exp = exp + subject + ",";
        }
        exp = exp + value + ")";
        exp = opmap.replaceGrammarWithModel(exp);
        getLogger().debug("Value: " + exp);

        return exp;
    }

    /**
     * Build a string representation of a CompoundDomainExpr.
     * 
     * 
     @param path
     *            Path to element to process
     * @param id
     *            ID of element to process
     * @param expr
     *            an Object of an expression
     * @param subject
     *            the subject of the expression
     * 
     @return String
     * 
     @see org.slasoi.slamodel.core.CompoundDomainExpr
     * 
     **/
    private String buildCompoundDomainExpr(final String path, final  ID id, final CompoundDomainExpr expr,
            final String subject) {

        String value = "";
        String expstr = expr.toString();
        getLogger().debug("Value: " + expr.toString());
        String op = expr.getLogicalOp().toString();
        String newexpstr = op + "(";
        DomainExpr[] exprs = expr.getSubExpressions();
        if (exprs != null && exprs.length > 0) {
            int exprno = 0;
            while (exprno < exprs.length) {
                value = processExpression(path, id, exprs[exprno], "");
                value = value.replace("(", "(" + subject + ",");
                value = value.replace("isequalto true", "");
                if (exprno > 0 && value.length() > 0) {
                   newexpstr = newexpstr + ", ";
                }
                newexpstr = newexpstr + value;
                exprno++;
            }
        }
        newexpstr = newexpstr.toString().replace("isequalto true", "");
        newexpstr = newexpstr + ") isequalto true";
        newexpstr = newexpstr.toString().replace("()", "");

        return newexpstr;
    }

    /**
     * remove any special characters from a string.
     * 
     * 
     @param s
     *            the string
     * 
     @return String
     * 
     * 
     **/
    public final String removeSpecialCharacters(final String s) {
        return s.replaceAll("\\W", "");
    }

    /**
     * Get a variabledeclr for a variable id.
     * 
     * @param label
     *            the variable id as a String
     * 
     @return VariableDeclr
     * 
     * 
     **/
    private VariableDeclr getVariableDeclr(final String label) {
        VariableDeclr var = null;
        var = currmodel.getVariableDeclr(label);
        if (var == null) {
            var = currTerm.getVariableDeclr(label);
        }
        return var;
    }

    /**
     * Build a variable representation of a VariableDeclr.
     * 
     * 
     @param path
     *            path
     @param id
     *            id
     @param var
     *            a VariableDeclr
     * 
     @return org.slasoi.gslam.monitoring.spec.slasoi.TermBuilder.Variable
     * 
     @see org.slasoi.slamodel.sla.VariableDeclr
     * 
     **/
    private Variable buildVariable(final String path, final ID id, final VariableDeclr var) {
        String stndtermstr = "*unknown*";
        STND stndterm = null;
        @SuppressWarnings("unused")
        String constraint = "";
        Variable newvar = new Variable();

        getLogger().debug("Variable ID: " + var.getVar());
        getLogger().debug("Variable Exp: " + var.toString());

        termreporter.addReport(thisSLAID.toString(), termreporter.buildEntry(path + "/" + var.getVar(), var.getVar()
                .toString(), reportingType, ReportEntry.Step.TERM_BUILD_VAR, "Replace variable with expression.", var
                .toString(), Result.DEFAULT));

        // decode if the variable is defined in an expression
        if (var.getExpr() != null) {
            getLogger().debug("Expression Type: " + var.getExpr().getClass().toString());
            getLogger().debug("Expression: " + var.getExpr().toString());

            if (var.getExpr() instanceof FunctionalExpr) {
                FunctionalExpr fexpr = (FunctionalExpr) var.getExpr();
                stndterm = fexpr.getOperator();
                // category = getCategoryforTerm(stndterm);
                // get the standard (model) term for this expression
                stndtermstr = fexpr.getOperator().toString();

                newvar.operator = stndtermstr;
                String[] values = fexpr.getParameters()[0].toString().split(" ");
                newvar.value = values[0];
                if (values.length > 1) {
                    newvar.valuetype = values[1];
                }
                newvar.value = "" + var.getExpr().toString();
                if (newvar.value.contains("[") || newvar.value.contains("]")) {
                    newvar.value = newvar.value.replace('[', '(');
                    newvar.value = newvar.value.replace(']', ')');
                }

                // for each part of the expression check if significant name and replace
                for (int y = 0; y < fexpr.getParameters().length; y++) {
                    String[] newvalues = fexpr.getParameters()[y].toString().split(" ");
                    for (int i = 0; i < newvalues.length; i++) {
                        String label = removeSpecialCharacters(newvalues[i]);
                        getLogger().debug("Checking variable substitutions for: " + label);
                        VariableDeclr declr = getVariableDeclr(label);
                        if (declr != null) {
                            getLogger().debug("Variable substitutions found for: " + label);
                            Variable varexp = buildVariable(path, id, declr);
                            String expvalue = varexp.value;
                            newvar.value = newvar.value.replace(label, expvalue);
                        }
                        if (modelutils.isCoreUnit(label)) {
                            getLogger().debug(label + " is a core unit type.");
                        }
                    }
                }

                // quickfix to remove labels of expressions
                if (newvar.value.contains(", = \"")) {
                    int startloc = 0, endloc = 0;
                    startloc = newvar.value.indexOf(", = \"");
                    if (startloc > 0) {
                        endloc = newvar.value.indexOf("\"", startloc + 1);
                    }
                    if (startloc > 0 && endloc > 0) {
                        String lstr = newvar.value.substring(0, startloc);
                        String rstr = newvar.value.substring(endloc + (endloc - startloc) + 2);
                        String newvarstr = lstr + "" + rstr;
                        newvar.value = newvarstr;
                    }
                }
                // newvar.value = newvar.value.replace("month", "");
                newvar.value = opmap.replaceGrammarWithModel(newvar.value);
                getLogger().debug("FuncExpr: " + newvar.value);
            } else if (var.getExpr() instanceof CompoundDomainExpr) {
                CompoundDomainExpr cdexpr = (CompoundDomainExpr) var.getExpr();
                stndterm = cdexpr.getLogicalOp();
                if (stndterm.toString().equals("and")) {
                    newvar.value = "true";
                } else {
                    newvar.value = "0";
                }
            } else if (var.getExpr() instanceof SimpleDomainExpr) {
                // check if it is a boolean value in variable
                if (var instanceof Customisable) {
                    Customisable customvar = (Customisable) var;
                    if (customvar.getExpr() instanceof SimpleDomainExpr) {
                        SimpleDomainExpr sdex = (SimpleDomainExpr) customvar.getExpr();
                        ValueExpr sDexValueExp = sdex.getValue();
                        String valuestr = "" + sDexValueExp;
                        // parse out a list if need be
                        if (sDexValueExp instanceof LIST) {
                            valuestr = "";
                            int count = 0;
                            LIST sDexValueExpList = (LIST) sDexValueExp;
                            Iterator<ValueExpr> sVelit = sDexValueExpList.iterator();
                            while (sVelit.hasNext()) {
                                Object sLitItem = sVelit.next();
                                if (sLitItem instanceof CONST) {
                                    CONST sVelitItemConst = (CONST) sLitItem;
                                    if (count > 0) {
                                        valuestr = valuestr + ",";
                                    }
                                    valuestr = valuestr + sVelitItemConst.toString();
                                    count++;
                                }
                            }
                        }
                        newvar.value = sdex.getComparisonOp() + "(" + customvar.getValue() + "," + valuestr + ")";
                    } else {
                        newvar.value = customvar.getValue().getValue() + " " + var.getExpr().toString();
                    }
                } else {
                    newvar.value = "0";

                    /*
                     * SimpleDomainExpr simpleexpr = (SimpleDomainExpr)var.getExpr(); if (simpleexpr.getValue()!=null) {
                     * ValueExpr simplevalueexpr = simpleexpr.getValue(); if (simplevalueexpr instanceof CONST) { CONST
                     * valueconst = (CONST)simplevalueexpr; newvar.value = valueconst.getValue(); } }
                     */
                }
            } else {
                getLogger().error(
                        "The variable expression type: " + var.getExpr().getClass().toString() + " is not supported.");
            }

        }

        newvar.value = opmap.replaceGrammarWithModel(newvar.value);

        return newvar;
    }

    /**
     * Build a string representation of a TypeConstraintExpr.
     * 
     * 
     @param path
     *            Path to element to process
     * @param id
     *            ID of element to process
     * @param expr
     *            a TypeConstraintExpr
     * 
     @return String
     * 
     @see org.slasoi.slamodel.core.TypeConstraintExpr
     * 
     **/
    private String buildTypeConstraintExpr(final String path, final ID id, final TypeConstraintExpr expr) {
        getLogger().debug("Value: " + expr.toString());
        String valuestr = "";
        String domainexpr = "";
        String exprstr = "";

        if (expr.getValue() instanceof ID) {
            ID varid = (ID) expr.getValue();
            VariableDeclr var = getVariableDeclr(varid.toString());
            if (var == null) {
                getLogger().warn("Variable " + varid.toString() + " not found.");
            } else {
                Variable varrep = buildVariable(path, id, var);
                valuestr = varrep.value;
            }
        } else {
            valuestr = processExpression(path, id, expr.getValue(), "");
        }

        domainexpr = processExpression(path, id, expr.getDomain(), valuestr);

        exprstr = domainexpr + " isequalto true";

        return exprstr;
    }

    /**
     * Build a string representation of a CompoundConstraintExpr.
     * 
     * 
     @param path
     *            the SLA path to the expression
     @param id
     *            the id of the SLA
     @param expr
     *            a CompoundConstraintExpr
     * 
     @return String
     * 
     @see org.slasoi.slamodel.core.CompoundConstraintExpr
     * 
     **/
    private String buildCompoundConstraintExpr(final String path, final ID id, final CompoundConstraintExpr expr) {
        String value = "";
        String expstr = expr.toString();
        getLogger().debug("Value: " + expr.toString());
        String op = expr.getLogicalOp().toString();
        String newexpstr = op + "(";
        ConstraintExpr[] exprs = expr.getSubExpressions();
        if (exprs != null && exprs.length > 0) {
            int exprno = 0;
            while (exprno < exprs.length) {
                value = processExpression(path, id, exprs[exprno], "");
                value = value.replace("isequalto true", "");
                if (exprno > 0 && value.length() > 0) {
                    newexpstr = newexpstr + ", ";
                }
                newexpstr = newexpstr + value;
                exprno++;
            }
        }
        newexpstr = newexpstr.toString().replace("isequalto true", "");
        newexpstr = newexpstr + ") isequalto true";
        newexpstr = newexpstr.toString().replace("()", "");

        return newexpstr;
    }
}
