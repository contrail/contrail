/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/parser/AgreementParser.java $
 */

package org.slasoi.gslam.monitoring.parser;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.parser.core.ASTAgreement;
import org.slasoi.gslam.monitoring.parser.core.ASTComparator;
import org.slasoi.gslam.monitoring.parser.core.ASTFunctionParameters;
import org.slasoi.gslam.monitoring.parser.core.ASTGuaranteeTerm;
import org.slasoi.gslam.monitoring.parser.core.ASTTerm;
import org.slasoi.gslam.monitoring.parser.core.ASTTermFunction;
import org.slasoi.gslam.monitoring.parser.core.Node;
import org.slasoi.gslam.monitoring.parser.core.ParseException;
import org.slasoi.gslam.monitoring.parser.core.UQLParser;
import org.slasoi.slamodel.vocab.common;

/**
 * The AgreementParser implements parsing an Agreement Term for an SLA Agreement.
 **/
public class AgreementParser extends MonitoringLogger {

    /**
     * parser for Agreement Term expressions.
     * 
     **/
    private static UQLParser parser = null;

    /**
     * Constructor.
     * 
     **/
    public AgreementParser() {
        super();
        // initialise logging
        initLogging(this.getClass().getName());
    }

    /**
     * Retrieves the root node of the Agreement.
     * 
     * @return org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    public final Node getRootNode() {
        Node root = null;
        if (parser != null) {
            root = parser.rootNode();
        }
        return root;
    }

    /**
     * Parses an Agreement Term (as a string) and returns an AST node.
     * 
     * 
     @param agreement
     *            a string representation of the agreement term
     * 
     @return org.slasoi.gslam.monitoring.parser.core.Node
     * 
     **/
    public static Node parse(final String agreement) {
        Node root = null;
        // parse the constraint to MM AST
        parser = new UQLParser(agreement);
        try {
            parser.parse();
            root = parser.rootNode();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            logError(e.getMessage());
            root = null;
        }
        return root;
    }

    /**
     * Visit an AST node.
     * 
     * 
     @param root
     *            the node to visit
     * 
     **/
    public static void visit(final Node root) {
        if (root.jjtGetNumChildren() > 0) {
            // check it is an agreement
            if (root.jjtGetNumChildren() > 0 && root.jjtGetChild(0) instanceof ASTAgreement) {
                ASTAgreement agreement = (ASTAgreement) root.jjtGetChild(0);
                if (agreement.jjtGetChild(0) instanceof ASTGuaranteeTerm) {
                    ASTGuaranteeTerm gterm = (ASTGuaranteeTerm) agreement.jjtGetChild(0);
                    visitGTerm(gterm);
                }
            }
        }
    }

    /**
     * Visit an Guarantee Term AST node.
     * 
     * 
     @param gterm
     *            the node to visit
     * 
     **/
    public static void visitGTerm(final ASTGuaranteeTerm gterm) {
        ASTTerm lhsTerm = null, rhsTerm = null;
        ASTComparator termComparator = null;
        // ASTTerm, ASTComparator, ASTTerm
        if (gterm.jjtGetNumChildren() > 0) {
            if (gterm.jjtGetChild(0) instanceof ASTTerm) {
                lhsTerm = (ASTTerm) gterm.jjtGetChild(0);
            }
            if (gterm.jjtGetChild(1) instanceof ASTComparator) {
                termComparator = (ASTComparator) gterm.jjtGetChild(1);
            }
            if (gterm.jjtGetChild(2) instanceof ASTTerm) {
                rhsTerm = (ASTTerm) gterm.jjtGetChild(2);
            }

            String termstr = "";
            if (lhsTerm.jjtGetNumChildren() > 0 && lhsTerm.jjtGetChild(0) instanceof ASTTermFunction) {
                visitTermFunction((ASTTermFunction) lhsTerm.jjtGetChild(0));
            } else {
                termstr = common.$base + lhsTerm.jjtGetFirstToken().toString();
                logDebug(lhsTerm.getClass().toString() + ": " + termstr);
            }

            String condstr = termComparator.jjtGetFirstToken().toString();
            String valuestr = rhsTerm.jjtGetLastToken().toString();

            logDebug(termComparator.getClass().toString() + condstr);
            logDebug(rhsTerm.getClass().toString() + valuestr);
        }
    }

    /**
     * Visit a Term Function AST node.
     * 
     * 
     @param termfunc
     *            the node to visit
     * 
     **/
    public static void visitTermFunction(final ASTTermFunction termfunc) {
        if (termfunc.jjtGetNumChildren() > 0 && termfunc.jjtGetChild(0) instanceof ASTFunctionParameters) {
            ASTFunctionParameters param = (ASTFunctionParameters) termfunc.jjtGetChild(0);
            if (param.jjtGetNumChildren() > 0) {
                logDebug("Function: " + termfunc.jjtGetFirstToken().toString());
                logDebug("Tokens: " + termfunc.jjtGetNumChildren());
            }
        }
    }

}
