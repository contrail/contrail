/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/manager/impl/MonitoringLogger.java $
 */

package org.slasoi.gslam.monitoring.manager.impl;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * The MonitoringLogger supports logging in the MonitoringManager package.
 **/
public class MonitoringLogger {
    /** LOGGER instance. **/
    private static Logger logger = null;
    /** LOGGER properties file path. **/
    private static String lOG4JPropertiesPath =
            System.getProperty("file.separator") + "generic-slamanager"
            + System.getProperty("file.separator") + "monitoring-manager"
            + System.getProperty("file.separator") + "log4j.properties";
    /** LOGGER classname. **/
    private String classname = "";

    /**
     * initialises the logging system.
     * 
     * @param name
     *            String name of the class for logging
     **/
    public final void initLogging(final String name) {
        // initialise logging
        PropertyConfigurator.configure(System.getenv("SLASOI_HOME") + MonitoringLogger.lOG4JPropertiesPath);
        classname = name;
        logger = Logger.getLogger(classname);
    }

    /*
     * public final void initLogging() { // initialise logging
     * PropertyConfigurator.configure(System.getenv("SLASOI_HOME") + MonitoringLogger.lOG4JPropertiesPath); logger =
     * Logger.getLogger(this.getClass().getName()); }
     */

    /**
     * Logs a line.
     * 
     **/
    public final void logLine() {
        logger.debug("================================================================");
    }

    /**
     * Logs an error.
     * 
     * @param error
     *            String representing a description of the error.
     **/
    public static final void logError(final String error) {
        logger.error(error);
    }

    /**
     * Logs a debug message.
     * 
     * @param debug
     *            String representing a description of the debug.
     **/
    public static final void logDebug(final String debug) {
        logger.error(debug);
    }

    /**
     * Logs an Asterisk line.
     * 
     **/
    public final void logAsteriskLine() {
        logger.debug("****************************************************************");
    }

    /**
     * Returns a logger.
     * 
     * @return logger
     * 
     **/
    public final Logger getLogger() {
        Logger alogger = Logger.getLogger(classname);
        return alogger;
    }
}
