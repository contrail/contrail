/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/manager/impl/MonitoringManager.java $
 */

package org.slasoi.gslam.monitoring.manager.impl;

import java.util.List;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.monitoring.configuration.MonitoringConfigurationBuilder;
import org.slasoi.gslam.monitoring.manager.IMonitorability;
import org.slasoi.gslam.monitoring.parser.ASTTermMap;
import org.slasoi.gslam.monitoring.reporting.MonitorabilityReport;
import org.slasoi.gslam.monitoring.selection.Selection;
import org.slasoi.gslam.monitoring.selection.SelectionManager;
import org.slasoi.gslam.monitoring.spec.slasoi.TermBuilder;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;

/**
 * The MonitoringManager implements the matching and configuration for a MonitoringSystemConfiguration.
 **/
public class MonitoringManager extends MonitoringLogger implements IMonitoringManager, IMonitorability {
    /** The current SLA model. **/
    private SLA currModel = null;
    /** Utilities for building terms. **/
    private TermBuilder slamodelutils = new TermBuilder();
    /** Monitorability Reporter. **/
    private MonitorabilityReport reporter = new MonitorabilityReport();
    /** The current SLA model. **/
    private boolean addReporting = false;
    /** AST Term Map. **/
    private ASTTermMap termMap;

    /**
     * Constructor.
     * 
     **/
    public MonitoringManager() {
        initLogging(this.getClass().getName());
    }

    /**
     * Logs information about the SLA received.
     * 
     * 
     @param slaModel
     *            an instance of an SLA
     * 
     * 
     @see org.slasoi.gslam.monitoring.manager.impl.MonitoringManager#checkMonitorability(SLA,
     *      ComponentMonitoringFeatures[]),org.slasoi.slamodel.sla.SLA
     **/
    private void reportSLA(final SLA slaModel) {
        getLogger().debug("SLA Received - UUID:" + slaModel.getUuid());
        getLogger().debug("SLA Template - ID: " + slaModel.getTemplateId());
        getLogger().debug("SLA AgreementTerms - Size: " + slaModel.getAgreementTerms().length);
    }

    /**
     * Logs the number of component features passed to the checkMonitorability function.
     * 
     * @param componentMonitoringFeatures
     *            a set of component monitoring features (Reasoners, Sensors, Effectors)
     * 
     * 
     @see org.slasoi.gslam.monitoring.manager.impl.MonitoringManager#checkMonitorability(SLA,
     *      ComponentMonitoringFeatures[]),org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     **/
    private void reportFeatures(final ComponentMonitoringFeatures[] componentMonitoringFeatures) {
        getLogger().debug("ComponentMonitoringFeatures Received. Components: " + componentMonitoringFeatures.length);
    }

    /**
     * Obtain the last report from checking monitorability.
     * 
     * @return String a MonitorabilityReport as a String.
     * 
     **/
    public final String getMonitorabilityReport() {
        String report = "";
        String reportRootName = "MonitorabilityReport";
        report = "<" + reportRootName + ">test</" + reportRootName + ">";
        return report;
    }

    /**
     * Return the AST Term Mapping from the SLA Parsing.
     * 
     * @return ASTTermMap
     * 
     **/
    public final ASTTermMap getTermMap() {
        return termMap;
    }

    /**
     * Checks the monitorability of SLA Agreement Terms and Guarantee States.
     * 
     * <p>
     * If one or more Agreement Terms are monitorable a component is generated for each Agreement Term and Guarantee
     * State with a set of monitoring component configurations. If it is not monitorable
     * 
     * @param slaModel
     *            an instance of an SLA template
     * 
     @param componentMonitoringFeatures
     *            a set of component monitoring features (Reasoners, Sensors, Effectors)
     * 
     @return MonitoringSystemConfiguration
     * 
     @see org.slasoi.gslam.core.monitoring.IMonitoringManager#checkMonitorability(org.slasoi.slamodel.sla.SLA,
     *      org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[])
     *      ,org.slasoi.slamodel.sla.SLA,org.slasoi.monitoring
     *      .common.features.ComponentMonitoringFeatures,org.slasoi.monitoring
     *      .common.configuration.MonitoringSystemConfiguration
     **/

    public final MonitoringSystemConfiguration checkMonitorability(final SLA slaModel,
            final ComponentMonitoringFeatures[] componentMonitoringFeatures) {
        logLine();
        getLogger().debug("<<<<< MONITORING MANAGER START >>>>>");
        logLine();

        if (addReporting) {
           getLogger().debug("<<<<< REPORTING ENABLED >>>>>");
        } else {
          getLogger().debug("<<<<< REPORTING NOT ENABLED >>>>>");
        }

        SelectionManager selector = null;
        currModel = slaModel;
        slamodelutils.setModel(currModel);
        String slaid = currModel.getUuid().toString();

        reportSLA(slaModel);
        reportFeatures(componentMonitoringFeatures);

        // extract gterms from SLA
        // assumes SLA is valid (i.e. checked and complete)
        AgreementTerm[] terms = slaModel.getAgreementTerms();

        // parse the gterms and initialise map of agreement terms and constraints
        termMap = slamodelutils.buildTermSet(slaModel.getUuid(), terms);
        if (addReporting) {
           reporter.consolidate(slaid, slamodelutils.getReporter());
        }

        // build component feature selection map
        List<Selection> selections = null;
        selector = new SelectionManager(termMap, componentMonitoringFeatures, currModel);
        selections = selector.matchFeatureComponents();
        if (addReporting) {
           reporter.consolidate(slaid, selector.getReporter());
        }

        // process selections as a monitoring configuration
        MonitoringConfigurationBuilder mcbuilder = new MonitoringConfigurationBuilder();
        mcbuilder.processSelections(selections, componentMonitoringFeatures, slaModel.getUuid());
        if (addReporting) {
            reporter.consolidate(slaid, mcbuilder.getReporter());
        }

        // build monitor configuration object from term feature map and return it
        MonitoringSystemConfiguration msc = mcbuilder.getConfiguration(slaModel.getUuid());
        // MonitoringSystemConfiguration msc = null;

        logLine();
        getLogger().debug("<<<<< MONITORING MANAGER END >>>>>");
        logLine();

        return msc;
    }

    /**
     * Obtain the last report from checking monitorability.
     * 
     * @return MonitorabilityReport a MonitorabilityReport as a String.
     * 
     **/
    public final MonitorabilityReport getReport() {
        return reporter;
    }

    /**
     * Switch monitorability reporting.
     * 
     * @param enabled
     *            enabled is true to enable reporting
     **/
    public final void enableReporting(final boolean enabled) {
        addReporting = enabled;
    }
}
