/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/maps/EventTypesMap.java $
 */

package org.slasoi.gslam.monitoring.spec.maps;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;

/**
 * Manages Event Types mapping between SLA Terms and Events required.
 **/
public class EventTypesMap extends MonitoringLogger {

    /***
     * Path to the term-events-map.cfg file.
     **/
    private String cfgfilepath =
            System.getenv("SLASOI_HOME") + File.separator
                    + "generic-slamanager\\monitoring-manager\\term-events-map.cfg";
    /***
     * HashMap of Term IDs with Events.
     **/
    private HashMap<String, TermEvents> eventMap = new HashMap<String, TermEvents>();

    /**
     * Constructor.
     * 
     * <p>
     * Initialises logging and builds event list from configuration file.
     * 
     **/
    public EventTypesMap() {
        // initialise logging
        initLogging(this.getClass().getName());
        buildeventMappings();
    }

    /**
     * Construct the Event Mappings from configuration file
     * 
     * <p>
     * The configuration file root path is specified in the "SLASOI_HOME" env. variable The local path is specified as
     * "SLASOI_HOME"\generic-slamanager\monitoring-manager\term-events-map.cfg
     * 
     **/
    private void buildeventMappings() {
        getLogger().debug("Reading events mapping configuaration file: " + cfgfilepath);

        FileReader read;
        try {
            read = new FileReader(cfgfilepath);

            BufferedReader br = new BufferedReader(read);
            String row;
            FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
            while ((row = br.readLine()) != null) {
                // parse the line
                String[] params = row.split(",");
                if ((params != null) && (params.length == 2)) {
                    String term = params[0];
                    String event = params[1];
                    TermEvents events = null;
                    if (eventMap.containsKey(term)) {
                        events = eventMap.get(term);
                    } else {
                        events = new TermEvents();
                    }
                    Event anevent = ffi.createEvent();
                    anevent.setType(event);
                    events.addEvent(anevent);
                    eventMap.put(term, events);
                }
            }
            getLogger().debug("Finished processing events mapping - no of terms: " + eventMap.size());

        } catch (Exception fileNotFoundExeception) {
            getLogger().error("Event map configuration file not found.");
        }
    }

    /**
     * Retrieves a set of Events for a Term reference string.
     * 
     * <p>
     * The terms and events should be specified in the term-event configuration file.
     * 
     * @param ref
     *            String reference
     * 
     @return org.slasoi.gslam.monitoring.spec.maps.TermEvents
     * 
     @see org.slasoi.gslam.monitoring.spec.maps.TermEvents
     * 
     **/
    public final TermEvents getEventsForTerm(final String ref) {
        TermEvents result = null;
        if (eventMap.containsKey(ref)) {
            result = eventMap.get(ref);
        } else {
            result = null;
        }
        return result;
    }

}
