/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/spec/maps/OperationMap.java $
 */

package org.slasoi.gslam.monitoring.spec.maps;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import org.slasoi.slamodel.vocab.core;

/**
 * Operation Mapping class between SLA and MM labels.
 **/
public class OperationMap {

    /**
     * Retrieve an MM operation from an SLA operation.
     * 
     * @param ref
     *            string of SLA Term reference
     * 
     @return String MM operation
     * 
     **/
    public final String getCondFromSLASOI(final String ref) {
        if (ref.equals(core.$less_than_or_equals)) {
            return "<=";
        } else if (ref.equals(core.$equals)) {
            return "=";
        } else if (ref.equals(core.$greater_than)) {
            return ">";
        } else if (ref.equals(core.$less_than)) {
            return "<";
        }
        // else return unknown operation
        return "";
    }

    /**
     * Retrieve an SLA operation from an MM operation.
     * 
     * @param ref
     *            string of MM operation
     * 
     @return String SLA Term reference
     * 
     */
    public final String getSLASOIFromCond(final String ref) {
        if (ref.equals("=") || ref.equals("isequalto")) {
            return core.$equals;
        } else if (ref.equals("<=")) {
            return core.$less_than_or_equals;
        } else if (ref.equals(">")) {
            return core.$greater_than;
        } else if (ref.equals("<")) {
            return core.$less_than;
        } else if (ref.equals(">=")) {
            return core.$greater_than_or_equals;
        } else if (ref.equals("http://www.slaatsoi.org/commonTerms#+")) {
            return core.$add;
        } else if (ref.equals("http://www.slaatsoi.org/commonTerms#*")) {
            return core.$multiply;
        }
        return "";
    }

    /**
     * Replace Grammar with Model function.
     * 
     * @param ref
     *            string of MM operation
     * 
     @return String SLA Term reference
     * 
     */
    public final String replaceGrammarWithModel(final String ref) {
        String newref = "";
        newref = ref;
        // replace reserved operators to model names
        if (newref.contains("-(")) {
            newref = ref.replace("-(", "subtract(");
        }
        if (newref.contains("+(")) {
            newref = newref.replace("+(", "add(");
        }
        if (ref.contains("/(")) {
            newref = newref.replace("/(", "divide(");
        }
        if (ref.contains("*(")) {
            newref = newref.replace("*(", "multiply(");
        }
        if (ref.contains("<(")) {
            newref = newref.replace("<(", "less_than(");
        }
        if (ref.contains(">(")) {
            newref = newref.replace(">(", "greater_than(");
        }
        if (ref.contains("!=(")) {
            newref = newref.replace("!=(", "not_equals(");
        }
        if (ref.contains("=(")) {
            newref = newref.replace("=(", "equals(");
        }
        if (ref.contains("<=(")) {
            newref = newref.replace("<equals(", "less_than_or_equals(");
        }
        if (ref.contains(">=(")) {
            newref = newref.replace(">equals(", "greater_than_or_equals(");
        }
        if (ref.contains("%(")) {
            newref = newref.replace("%(", "modulo(");
        }
        return newref;
    }

}
