/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/manager/MonitoringManagerBuilder.java $
 */

package org.slasoi.gslam.monitoring.manager;

import org.slasoi.gslam.core.builder.IMonitoringManagerBuilder;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringManager;

/**
 * The MonitoringManagerBuilder supports building instances of MonitoringManager.
 * <p>
 * It extends the IMonitoringManagerBuilder class to implement the interface
 **/
public class MonitoringManagerBuilder implements IMonitoringManagerBuilder {

    /**
     * MonitoringManagerBuilder constructor.
     * 
     * <p>
     * Creates a new instance of a MonitoringManagerBuilder class
     * 
     **/
    public MonitoringManagerBuilder() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Creates a new instance of a MonitoringManager class.
     * 
     * @see org.slasoi.gslam.core.builder.IMonitoringManagerBuilder#create()
     * @return IMonitoringManager
     */
    public final IMonitoringManager create() {
        return new MonitoringManager();
    }
}
