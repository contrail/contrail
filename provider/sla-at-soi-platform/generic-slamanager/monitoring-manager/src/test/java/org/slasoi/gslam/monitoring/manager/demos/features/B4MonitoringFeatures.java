/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 21:49:33 +0200 (Mon, 28 Feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/demos/features/B4MonitoringFeatures.java $
 */

package org.slasoi.gslam.monitoring.manager.demos.features;

/**
 * @author SLA@SOI (City)
 * @date May 12, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.LinkedList;

import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.units;

/**
 * SLA Component Monitoring Features Test for the B4 example.
 **/
public class B4MonitoringFeatures {
    /** everest Features. **/
    private EverestTestMonitoringFeatures everestFeatures = null;

    /**
     * Constructor.
     * 
     */
    public B4MonitoringFeatures() {
        everestFeatures = new EverestTestMonitoringFeatures();
    }

    /**
     * Constructs a set of ComponentMonitoringFeatures.
     * 
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures[] buildTest() {
        FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
        ComponentMonitoringFeatures[] cmfeatures = new ComponentMonitoringFeatures[2];

        cmfeatures = everestFeatures.buildTest();

        try {
            // ////////////////////////////////////////////////////////////////////////////////
            // Extension component for B4 Sensors
            // ////////////////////////////////////////////////////////////////////////////////
            cmfeatures[2] = ffi.createComponentMonitoringFeatures();
            cmfeatures[2].setUuid("555e8400-sss2-41d4-a716-406075043333");
            cmfeatures[2].setType("SENSOR");

            LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

            // ////////////////////////////////////////////////////////////////////////////////
            // B4 Extension Features
            // ////////////////////////////////////////////////////////////////////////////////
            String type = "";
            type = "http://www.slaatsoi.org/commonTerms#persistence";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#memory";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#cpu_speed";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#vm_cores";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            /*
             * type = common.$location; mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
             * //disabled in order to compile, because $location was removed from common
             */
            type = common.$isolation;
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#vm_image";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#VM_Access_Point";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "RESPONSE"));
            type = "autogen/VM_X";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "RESPONSE"));
            type = "REQUEST";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "RESPONSE";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));

            // greater_than reasoners and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$greater_than,
                    "greater_than xsd:integer Reasoner", "xsd:integer", "xsd:integer", meta.$BOOLEAN));
            // isa reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$isa, "isa xsd:boolean Reasoner",
                    "xsd:boolean", "xsd:boolean", meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of xsd:string Reasoner",
                    "xsd:string", "xsd:string", meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                    units.$GHz, units.$GHz, meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                    "xsd:anyURI", "xsd:anyURI", meta.$BOOLEAN));

            cmfeatures[2].setMonitoringFeatures(EverestTestMonitoringFeatures.mfListToArray(mflist));

        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        return cmfeatures;
    }

}
