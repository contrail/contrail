/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/tests/GenericRun.java $
 */

package org.slasoi.gslam.monitoring.manager.tests;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.gslam.core.builder.IMonitoringManagerBuilder;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.monitoring.configuration.MonitoringConfigurationBuilder;
import org.slasoi.gslam.monitoring.manager.MonitoringManagerBuilder;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.context.ApplicationContext;

/**
 * Generic Helper class to support testing the MonitoringManager.
 **/
public class GenericRun {

    /** The Application Context. **/
    private ApplicationContext ac;
    /** Filepath. **/
    private String filepath = "";
    /** parent class. **/
    private String parent = "";
    /** A set of monitoring features. **/
    private ComponentMonitoringFeatures[] features = null;
    /** Path to the logging file. **/
    private String logpropspath =
            System.getenv("SLASOI_HOME") + File.separator + "generic-slamanager\\monitoring-manager\\log4j.properties";
    /** Flag whether to print output of configuration. **/
    private boolean printConfig = false;

    /**
     * Set the name of the class for logging.
     * 
     * 
     @param aparent
     *            the parent name of the class for logging
     * 
     **/
    public final void setParentName(final String aparent) {
        parent = aparent;
    }

    /**
     * Set the path the SLA model XML to monitor.
     * 
     * 
     @param path
     *            the path the SLA model xml
     * 
     **/
    public final void setSLAPath(final String path) {
        filepath = path;
    }

    /**
     * Set the ComponentMonitoringFeatures for this test.
     * 
     * 
     @param afeatures
     *            an array of ComponentMonitoringFeatures
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final void setFeatures(final ComponentMonitoringFeatures[] afeatures) {
        features = afeatures;
    }

    /**
     * Sets whether to output the monitoring configuration.
     * 
     * 
     @param print
     *            flag whether to output
     * 
     **/
    public final void setPrintConfig(final boolean print) {
        printConfig = print;
    }

    /**
     * Set the logging file path for this instance.
     * 
     * @param path
     *            a string of the absolute path for the log file
     * 
     * 
     **/
    public final void setLoggerPath(final String path) {
        logpropspath = path;
    }

    /**
     * Run a test.
     * 
     * <p>
     * To run a test ensure to set the features and a path to an SLA XML model
     * 
     **/
    public final void run() {
        // initialise logging
        PropertyConfigurator.configure(logpropspath);
        Logger logger = Logger.getLogger(parent);
        logger.debug("START Monitoring Manager Test");

        if (filepath.length() > 0) {
            logger.debug("SLA path: " + filepath);
            logger.debug("SLA@HOME env var. is set to: " + System.getenv("SLASOI_HOME"));

            // use SLA example from org.slasoi.slamodel
            SLA sla = null;
            FileReader read;
            logger.debug("Reading SLA from file: " + filepath);
            try {
                read = new FileReader(filepath);

                BufferedReader br = new BufferedReader(read);
                StringBuffer sb = new StringBuffer();
                String row;
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                logger.debug("Parsing SLA with " + SLASOIParser.class.getName());
                SLASOIParser slasoiParser = new SLASOIParser();
                sla = slasoiParser.parseSLA(sb.toString());

                logger.debug("Checking MonitoringFeatures List");
                if (features != null && features.length > 0) {

                    // create a MonitoringManager instance
                    logger.debug("Creating a new MonitoringManager");
                    IMonitoringManagerBuilder mmbuilder = new MonitoringManagerBuilder();
                    IMonitoringManager manager = mmbuilder.create();

                    // check Monitorability of the SLA with the services
                    // provided and receive a configuration
                    // MonitoringSystemConfiguration config =
                    // manager.checkMonitorability(sla, serviceBuilderList);
                    logger.debug("Checking Monitorability...");
                    MonitoringSystemConfiguration config = manager.checkMonitorability(sla, features);

                    if (printConfig) {
                        MonitoringConfigurationBuilder mcb = new MonitoringConfigurationBuilder();
                        mcb.printMonitoringSystemConfiguration(config);
                    }
                } else {
                    logger.error("ComponentMonitoringFeatures is empty/null.");
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            logger.debug("No SLA path specified.");
        }

        logger.debug("END Monitoring Manager TEST");
    }
}
