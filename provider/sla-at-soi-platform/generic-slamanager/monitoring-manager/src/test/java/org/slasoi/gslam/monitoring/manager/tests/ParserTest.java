/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/tests/ParserTest.java $
 */

package org.slasoi.gslam.monitoring.manager.tests;

import org.slasoi.gslam.monitoring.parser.AgreementParser;
import org.slasoi.gslam.monitoring.parser.core.Node;

import junit.framework.TestCase;

/**
 * Test Class for Parsing Agreement Term expressions.
 **/
public class ParserTest extends TestCase {

    /**
     * Initialise the test case class.
     * 
     * @param name
     *            name for test case
     **/
    public ParserTest(final String name) {
        super(name);
    }

    /**
     * Runs the test case.
     **/
    public final void test() {
        AgreementParser parser = new AgreementParser();
        // String agreement = "\"G2\" percent(series(bookingvalue/test, periodic(\"6\"))) < \"11\"";
        String agreement = "\"ANDTEST\" and(90 > reliability(test),80 > arrivalrate(test)) == true";
        System.out.println(agreement);
        Node root = parser.parse(agreement);
        assertNotNull(root);
        AgreementParser.visit(root);
    }

    /**
     * @param args
     *            arguments
     */
    public static void main(final String[] args) {
        ParserTest test = new ParserTest("test");
        try {
            test.runTest();
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
