/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 390 $
 * @lastrevision   $Date: 2010-12-17 11:38:12 +0100 (pet, 17 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/broker/SyntaxConverterBrokerChild.java $
 */
package org.slasoi.gslam.syntaxconverter.broker;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.syntaxconverter.SyntaxConverterHelper;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterBroker.SyCBrokerContext;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.SLARegistryClientConverter;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.TemplateRegistryClientConverter;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.w3.x2006.x01.wsdl.DescriptionDocument;

import eu.slaatsoi.slamodel.SLADocument;
import eu.slaatsoi.slamodel.SLATemplateDocument;

/**
 * Helper class of SyntaxConverterBroker.  This allows multiple execution (in parallel) of requests from any SLA-manager
 * 
 * @author Miguel Rojas (UDO)
 *
 */
public class SyntaxConverterBrokerChild {
    /**
     * Constructor
     * 
     * @param peer Socket peer which receives request from the SyC (under OSGi) 
     * @param context Context where this broker gets shared-components from, to process requests
     */
    public SyntaxConverterBrokerChild(Socket peer, SyCBrokerContext context) {
        this.peer = peer;
        this.context = context;

        init();
    }

    protected void init() {
        try {
            in = new BufferedReader(new InputStreamReader(peer.getInputStream()));
            out = new PrintStream(peer.getOutputStream());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts a thread for listening of requests (from the SyC under OSGi)
     */
    public void run() {
        String command = null;
        Object result = null;

        try {
            while ((command = in.readLine()) != null) {
                if (command.equalsIgnoreCase("end")) {
                    System.out.println("SyntaxConverterBrokerChild has been close");
                    return;
                }

                Object commands = Base64Utils.decode(command);
                Vector<String> content = (Vector<String>) commands;

                try {
                    String brokerAsStr = content.elementAt(BROKER_TYPE);

                    BrokerType broker = BrokerType.valueOf(brokerAsStr);

                    if (broker == BrokerType.SYNTAX_CONVERTER) {
                        String strType = content.elementAt(SC_SYC_TYPE);
                        String strCall = content.elementAt(SC_SYC_CALL);
                        String strObj = content.elementAt(SC_SYC_OBJ);

                        ISyntaxConverter iSyntaxConverter = context.set.get(SyntaxConverterType.valueOf(strType));
                        SyntaxConverterCall call = SyntaxConverterCall.valueOf(strCall);

                        // ----- SyC -----
                        if (call == SyntaxConverterCall.SLA_PARSER_PARSE_SLA) {
                            String str = (String) Base64Utils.decode(strObj);
                            tracePRE(call, str);

                            SLADocument doc = SLADocument.Factory.parse(new ByteArrayInputStream(str.getBytes()));
                            result = iSyntaxConverter.parseSLA(doc.xmlText());

                            tracePOST(call, str, doc, doc.getClass(), result);
                        }
                        else if (call == SyntaxConverterCall.SLA_RENDERER_RENDER_SLA) {
                            SLA sla = (SLA) Base64Utils.decode(strObj);
                            tracePRE(call, sla);

                            result = iSyntaxConverter.renderSLA(sla);

                            tracePOST(call, sla, sla.getClass(), result);
                        }
                        else if (call == SyntaxConverterCall.SLA_TEMPLATE_PARSER_PARSE_TEMPLATE) {
                            String str = (String) Base64Utils.decode(strObj);
                            tracePRE(call, str);

                            SLATemplateDocument doc =
                                    SLATemplateDocument.Factory.parse(new ByteArrayInputStream(str.getBytes()));
                            result = iSyntaxConverter.parseSLATemplate(doc.xmlText());

                            tracePOST(call, str, doc, doc.getClass(), result);
                        }
                        else if (call == SyntaxConverterCall.SLA_TEMPLATE_PARSER_PARSE_TEMPLATES) {
                            String[] str = (String[]) Base64Utils.decode(strObj);
                            SLATemplate[] parsed = new SLATemplate[str.length];
                            for (int i = 0; i < str.length; i++) {
                                SLATemplateDocument doc =
                                        SLATemplateDocument.Factory.parse(new ByteArrayInputStream(str[i].getBytes()));
                                parsed[i] = (SLATemplate) iSyntaxConverter.parseSLATemplate(doc.xmlText());
                            }

                            result = parsed;
                        }
                        else if (call == SyntaxConverterCall.SLA_TEMPLATE_RENDERER_RENDER_SLATEMPLATE) {
                            SLATemplate tmpl = (SLATemplate) Base64Utils.decode(strObj);
                            tracePRE(call, tmpl.getClass(), tmpl);

                            result = iSyntaxConverter.renderSLATemplate(tmpl);

                            tracePOST(call, tmpl.getClass(), tmpl, result);
                        }
                        else if (call == SyntaxConverterCall.WSDL_PARSER_PARSE_WSDL) {
                            String wsdlString = (String) Base64Utils.decode(strObj);
                            DescriptionDocument dd =
                                    DescriptionDocument.Factory.parse(new ByteArrayInputStream(wsdlString.getBytes()));
                            result = iSyntaxConverter.parseWSDL(dd.xmlText());
                        }
                        else if (call == SyntaxConverterCall.WSDL_RENDERER_RENDER_WSDL) {
                            result = iSyntaxConverter.renderWSDL((Interface.Specification) Base64Utils.decode(strObj));
                        }
                        else if (call == SyntaxConverterCall.HELPER_PARSE_CONSTRAINT_EXPR) {
                            result = SyntaxConverterHelper.parseConstraintExpr((String) Base64Utils.decode(strObj));
                        }
                        else if (call == SyntaxConverterCall.HELPER_RENDER_MINIMUM_INFO) {
                            result =
                                    SyntaxConverterHelper
                                            .renderMinimumInfo((SLAMinimumInfo) Base64Utils.decode(strObj));
                        }
                    }
                    else if (broker == BrokerType.SLAREGISTRY_WS) {
                        String strCall = content.elementAt(SR_CALL);
                        String strObj = content.elementAt(SR_OBJ);

                        SyntaxConverterCall call = SyntaxConverterCall.valueOf(strCall);

                        // ----- SLARegistry -----
                        if (call == SyntaxConverterCall.SR_PARSE_SLA_MINIMUMINFO) {
                            String cnt = (String) Base64Utils.decode(strObj);
                            tracePRE(call, cnt);

                            result = SLARegistryClientConverter.parseSLAMinimumInfo(cnt);

                            tracePOST(call, cnt, result);
                        }
                        else if (call == SyntaxConverterCall.SR_PARSE_SLA_STATEINFO) {
                            String cnt = (String) Base64Utils.decode(strObj);
                            tracePRE(call, cnt);

                            result = SLARegistryClientConverter.parseSLAStateInfo(cnt);

                            tracePOST(call, cnt, result);
                        }
                        else if (call == SyntaxConverterCall.SR_PARSE_SLA) {
                            ISyntaxConverter iSyntaxConverter =
                                    context.set.get(SyntaxConverterType.SLASOISyntaxConverter);
                            String cnt = (String) Base64Utils.decode(strObj);
                            tracePRE(call, cnt);

                            result = iSyntaxConverter.parseSLA(cnt);

                            tracePOST(call, cnt, result);
                        }
                    }
                    else if (broker == BrokerType.TEMPLATE_REGISTRY_WS) {
                        String strCall = content.elementAt(TR_CALL);
                        String strObj = content.elementAt(TR_OBJ);

                        SyntaxConverterCall call = SyntaxConverterCall.valueOf(strCall);

                        // ----- TemplateRegistry -----
                        if (call == SyntaxConverterCall.TR_PARSE_RESULTSET) {
                            result =
                                    TemplateRegistryClientConverter.parseResultSet((String) Base64Utils.decode(strObj));
                        }
                        else if (call == SyntaxConverterCall.TR_PARSE_WARNING) {
                            result = TemplateRegistryClientConverter.parseWarning((String) Base64Utils.decode(strObj));
                        }
                        else if (call == SyntaxConverterCall.TR_RENDER_METADATA) {
                            result =
                                    TemplateRegistryClientConverter.renderMetadata((Metadata) Base64Utils
                                            .decode(strObj));
                        }
                        else if (call == SyntaxConverterCall.TR_RENDER_CONSTRAINT) {
                            result =
                                    TemplateRegistryClientConverter.renderConstraint((ConstraintExpr) Base64Utils
                                            .decode(strObj));
                        }
                    }

                    // it sends back the result to the SyntaxConverterDelegator

                    System.out.println("SyntaxConverterBrokerChild (" + brokerAsStr + "):: returning an object '"
                            + result.getClass() + "'");
                    out.println(Base64Utils.encode(result));
                }
                catch (Exception e) {
                    e.printStackTrace();

                    // sends back the exception to syntax-converter's delegator (within OSGi)
                    System.out.println("SyntaxConverterBrokerChild :: returning an exception '" + e + e.getMessage() + e.getStackTrace().toString()
                            + "'");
                    Exception log = new Exception(e.toString());
                    out.println(Base64Utils.encode(log));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void tracePOST(Object... objs) {
        try {
            String active = System.getProperty("tracePOST");
            String path = System.getProperty("tracePath");
            String separator = System.getProperty("file.separator");

            if (active == null || !active.equalsIgnoreCase("true"))
                return;
            
            for (Object o : objs) {
                String id = System.nanoTime() + "-POST.txt";
	            File file = new File(path + separator + id);
	            System.out.println("SyntaxConverterBrokerChild dumping to '" + file + "'");
	            Vector<Object> content = new Vector<Object>(5, 3);
            
                content.add(SEPARATOR);
                content.add(o);
                
                FileUtils.writeLines(file, content);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void tracePRE(Object... objs) {
        try {
            String active = System.getProperty("tracePRE");
            String path = System.getProperty("tracePath");
            String separator = System.getProperty("file.separator");
            

            if (active == null || !active.equalsIgnoreCase("true"))
                return;

            for (Object o : objs) {
            	String id = System.nanoTime() + "-PRE.txt";
            	File file = new File(path + separator + id);
            	System.out.println("SyntaxConverterBrokerChild dumping to '" + file + "'");
            	Vector<Object> content = new Vector<Object>(5, 3);
            
//                content.add(SEPARATOR);
                content.add(o);
                
                FileUtils.writeLines(file, content);
            }

            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected SyCBrokerContext context;
    protected Socket peer;
    protected PrintStream out;
    protected BufferedReader in;

    public static final String SEPARATOR =
            "## -- Param -- :: ------------------------------------------------------------------------------------ ##";

    public static final int BROKER_TYPE = 0;

    public static final int SC_SYC_TYPE = 1;
    public static final int SC_SYC_CALL = 2;
    public static final int SC_SYC_OBJ = 3;

    public static final int SR_CALL = 1;
    public static final int SR_OBJ = 2;

    public static final int TR_CALL = 1;
    public static final int TR_OBJ = 2;
}
