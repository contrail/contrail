package org.slasoi.gslam.syntaxconverter;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.axis2.Constants;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.description.AxisService;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.osgi.deployment.tracker.WSTracker;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.builder.ISyntaxConverterBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.control.IControl;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.SLARegistry.IQuery;
import org.slasoi.gslam.core.negotiation.SLARegistry.IRegister;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.syntaxconverter.webservice.ControlImpl;
import org.slasoi.gslam.syntaxconverter.webservice.INegotiationAbstraction;
import org.slasoi.gslam.syntaxconverter.webservice.NegotiationImpl;
import org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction;
import org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryImpl;
import org.slasoi.gslam.syntaxconverter.webservice.SLARegistryRegisterAbstraction;
import org.slasoi.gslam.syntaxconverter.webservice.SLARegistryRegisterImpl;
import org.slasoi.gslam.syntaxconverter.webservice.SLATemplateRegistryAbstraction;
import org.slasoi.gslam.syntaxconverter.webservice.SLATemplateRegistryImpl;
//import org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.NegotiationClient;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.SLARegistryQueryClient;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.SLARegistryRegisterClient;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.TemplateRegistryClient;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

import eu.slaatsoi.slamodel.AgreementPartyType;
import eu.slaatsoi.slamodel.ConstraintExprDocument;
import eu.slaatsoi.slamodel.PartyDocument;
import eu.soi.slaAt.gslam.slaRegistry.register.SLAMinimumInfoDocument;

/**
 * The syntax-converter unifies the capabilities for rendering and parsing of
 * SLAs and SLA templates. Further it provides clients for various web services
 * and starts up web service at construction.
 * 
 * @author Peter A. Chronz
 * 
 */
public class SyntaxConverter implements ISyntaxConverter,
		ISyntaxConverterBuilder, SLAMContextAware, BundleContextAware {
	private SLAManagerContext context;
	protected SLARenderer slaRenderer;
	protected SLATemplateRenderer slaTemplateRenderer;
	protected SLAParser slaParser;
	protected SLATemplateParser slaTemplateParser;
	protected WSDLParser wsdlParser;
	protected WSDLRenderer wsdlRenderer;
	protected INegotiation iNegotiation;
	
	//protected Map<String, NegotiationClient> negotiationClients = new HashMap<String, NegotiationClient>();

	protected SyntaxConverterDelegator delegator;

	protected BundleContext osgiContext;
	
	protected GenericSLAManagerUtils gslams;

	public SyntaxConverter() {
		super();
	}

    /**
     * Sets the generic SLA manager services.
     */
    @ServiceReference
    public void setGslamUtils( GenericSLAManagerUtils utils ){
        assert (utils != null) : "Generic SLAM services != null.";
        gslams = utils;
        
        // FIXME:  this sections registers a restful service
//        SyCRetter.getInstance().register();
    }
    
    // -----------------------------------------------------------------------
    //  HELPER METHODS 
    // Miguel:  Example for getting a local SLAM  
    protected SLAManagerContext getSLAM(String epr)
    {
        try
        {
        	if(this.gslams==null) {
        		this.gslams = context.getSlamUtils(); 
        	}
            SLAManagerContext[] contextSet = gslams.getContextSet( "GLOBAL" ); // get all SLAMs locally
            if ( contextSet == null ) return null;

            for ( SLAManagerContext ctx : contextSet )
            {
                if ( ctx.getEPR().equalsIgnoreCase( epr ) ) // The EPR could be also used to identify the SLAM
                {
                    return ctx;
                }
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }

        return null;
    }
    
	/**
	 * Initializes the syntax converter with renderers and parsers as needed by
	 * the use case.
	 * 
	 * @param slaRenderer
	 *            An SLA renderer.
	 * @param slaTemplateRenderer
	 *            An SLA template renderer.
	 * @param slaParser
	 *            An SLA parser.
	 * @param slaTemplateParser
	 *            An SLA template parser.
	 */
	public void init(SLARenderer slaRenderer,
			SLATemplateRenderer slaTemplateRenderer, SLAParser slaParser,
			SLATemplateParser slaTemplateParser) {
		this.slaRenderer = slaRenderer;
		this.slaTemplateRenderer = slaTemplateRenderer;
		this.slaParser = slaParser;
		this.slaTemplateParser = slaTemplateParser;
		this.wsdlParser = new WSDLParser();
		this.wsdlRenderer = new WSDLRenderer();
	}

	/**
	 * Sets this instance of syntax-converter to a certain type, depending on
	 * the use case.
	 * 
	 * @param type
	 *            The type identifying the application of the syntax converter.
	 */
	public void init(SyntaxConverterType type) {
		delegator = new SyntaxConverterDelegator(type);
	}

	/**
	 * Sets the bundle context for the syntax converter as OSGi component.
	 * 
	 * @param osgiContext
	 *            The OSGi context.
	 */
	public void setBundleContext(BundleContext osgiContext) {
		this.osgiContext = osgiContext;
	}

	/**
	 * Renders the provided SLA object.
	 * 
	 * @param sla
	 *            The SLA object to render to string.
	 */
	public String renderSLA(SLA sla) throws Exception {
		if (sla == null)
			throw new Exception("A null value has been passed for rendering!");
		try {
			if (delegator != null)
				return delegator.renderSLA(sla);

			return this.slaRenderer.renderSLA(sla);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Renders an SLA template to string.
	 * 
	 * @param The
	 *            SLA template to render to string.
	 */
	public String renderSLATemplate(SLATemplate slaTemplate) throws Exception {
		// if(this.context.getSLATemplateRegistry().validateSLATemplate(slaTemplate).length
		// > 0)
		// throw new
		// Exception("An invalid template has been passed to the SyntaxConverter for rendering!");
		try {
			if (slaTemplate == null)
				throw new Exception(
						"A null value has been passed for rendering!");

			if (delegator != null)
				return delegator.renderSLATemplate(slaTemplate);

			return this.slaTemplateRenderer.renderSLATemplate(slaTemplate);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Parses an SLA string to SLA object.
	 * 
	 * @param The
	 *            parsed SLA object.
	 */
	public Object parseSLA(String slaObject) throws Exception {
		if (slaObject == null)
			throw new Exception("A null value has been passed for parsing!");
		try {
			if (delegator != null)
				return delegator.parseSLA(slaObject);

			return this.slaParser.parseSLA(slaObject);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Parses the SLA template from string.
	 * 
	 * @param slaTemplateObject
	 *            The string representation of an SLA template.
	 */
	public Object parseSLATemplate(String slaTemplateObject) throws Exception {
		if (slaTemplateObject == null)
			throw new Exception("A null value has been passed for parsing!");
		try {
			if (delegator != null)
				return delegator.parseSLATemplate(slaTemplateObject);

			return this.slaTemplateParser.parseTemplate(slaTemplateObject);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Renders an inteface specification to a WSDL 2.0 string.
	 */
	public String renderWSDL(Interface.Specification interfaceSpec)
			throws Exception {
		if (delegator != null)
			return delegator.renderWSDL(interfaceSpec);

		return this.wsdlRenderer.renderWSDL(interfaceSpec);
	}

	/**
	 * Parses a WSDL string to an interface specification.
	 */
	public Interface.Specification[] parseWSDL(String wsdlString)
			throws Exception {
		if (delegator != null)
			return delegator.parseWSDL(wsdlString);

		return this.wsdlParser.parseWSDL(wsdlString);
	}

	/**
	 * Invoked by generic-slam each time a slamanager creates a new context
	 * (with generic components)
	 */
	public Hashtable<SyntaxConverterType, ISyntaxConverter> create() {
		// create a pure SLASOI SyntaxConverter

		SyntaxConverter slasoiConverter = new SyntaxConverter();
		slasoiConverter.init(SyntaxConverterType.SLASOISyntaxConverter);

		/*
		 * SyntaxConverter slasoiConverter = new SyntaxConverter();
		 * slasoiConverter.init(new SLASOIRenderer(), new
		 * SLASOITemplateRenderer(), new SLASOIParser(), new
		 * SLASOITemplateParser());
		 */

		/*
		 * // create a pure WS-Agreement SyntaxConverter SyntaxConverter
		 * wsAgConverter = new SyntaxConverter(); wsAgConverter.init(new
		 * SLASOIRenderer(), new SLASOITemplateRenderer(), new SLASOIParser(),
		 * new SLASOITemplateParser());
		 */
		// put all SyntaxConverters into one map
		Hashtable<SyntaxConverterType, ISyntaxConverter> converters = new Hashtable<SyntaxConverterType, ISyntaxConverter>();
		converters.put(SyntaxConverterType.SLASOISyntaxConverter,
				slasoiConverter);

		// converters.put(SyntaxConverterType.WSAGSyntaxConverter,
		// wsAgConverter);

		return converters;
	}

	/**
	 * Starts up all web services related to and handled by the syntax
	 * converter.
	 */
	public void createWS() {
		String wsPrefix = "";
		try {
			wsPrefix = this.context.getWSPrefix();
		} catch (SLAManagerContextException e) {
			e.printStackTrace();
		}

		ContextKeeper.getInstance().registerContext(wsPrefix, this.context);

		// try {
		// new AxisServer().deployService("");
		// }
		// catch (AxisFault e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// INegotiation
		Dictionary wsPropertiesNego = new Properties();
		wsPropertiesNego.put(WSTracker.AXIS2_WS, wsPrefix + WSType.Negotiation);
		NegotiationImpl wsNegotiation = new NegotiationImpl();
		osgiContext.registerService(INegotiationAbstraction.class.getName(),
				wsNegotiation, wsPropertiesNego);

		// IControl
		Dictionary wsPropertiesControl = new Properties();
		wsPropertiesControl.put(WSTracker.AXIS2_WS, wsPrefix + WSType.Control);
		IControl wsControl = new ControlImpl();
		osgiContext.registerService(IControl.class.getName(), wsControl,
				wsPropertiesControl);
		((ControlImpl)wsControl).setContext(this.context);
		

		// Query SLA registry register
		Dictionary wsPropertiesRegistryRegister = new Properties();
		wsPropertiesRegistryRegister.put(WSTracker.AXIS2_WS, wsPrefix
				+ WSType.SLARegistryRegister);
		SLARegistryRegisterAbstraction wsRegistryRegister = new SLARegistryRegisterImpl();
		osgiContext.registerService(SLARegistryRegisterAbstraction.class
				.getName(), wsRegistryRegister, wsPropertiesRegistryRegister);
		((SLARegistryRegisterImpl)wsRegistryRegister).setContext(this.context);

		// Query SLA registry query
		Dictionary wsPropertiesRegistryQuery = new Properties();
		wsPropertiesRegistryQuery.put(WSTracker.AXIS2_WS, wsPrefix
				+ WSType.SLARegistryQuery);
		SLARegistryQueryAbstraction wsRegistryQuery = new SLARegistryQueryImpl();
		osgiContext.registerService(
				SLARegistryQueryAbstraction.class.getName(), wsRegistryQuery,
				wsPropertiesRegistryQuery);
		((SLARegistryQueryImpl)wsRegistryQuery).setContext(this.context);

		// Query SLA template registry
		Dictionary wsPropertiesSLATemplateRegistry = new Properties();
		wsPropertiesSLATemplateRegistry.put(WSTracker.AXIS2_WS, wsPrefix
				+ WSType.SLATemplateRegistry);
		SLATemplateRegistryAbstraction wsTemplateRegistry = new SLATemplateRegistryImpl();
		osgiContext
				.registerService(
						SLATemplateRegistryAbstraction.class.getName(),
						wsTemplateRegistry, wsPropertiesSLATemplateRegistry);
		((SLATemplateRegistryImpl)wsTemplateRegistry).setContext(this.context);
	}

	/**
	 * Sets the context for this syntax converter.
	 */
	public void setSLAManagerContext(SLAManagerContext context) {
		this.context = context;
	}

	/* DO NOT REMOVE */
	protected void fixWSScope(String wsName) {
		try {
		    org.osgi.framework.ServiceReference[] refs = this.osgiContext.getServiceReferences(
					ConfigurationContext.class.getName(), null);
		    org.osgi.framework.ServiceReference scc = refs[0];
			Object obj = this.osgiContext.getService(scc);
			ConfigurationContext ccService = (ConfigurationContext) obj;

			AxisConfiguration axisConfiguration = ccService
					.getAxisConfiguration();
			AxisService service = axisConfiguration.getService(wsName);

			if (service != null) {
				service.setScope(Constants.SCOPE_APPLICATION);
				System.out.println("WS : '" + wsName + "' set to scope : "
						+ service.getScope());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Provides a negotiation client for the negotiation web services.
	 * 
	 * @param epr
	 *            The EPR of the web service to contact.
	 */
	public INegotiation getNegotiationClient(String epr) throws Exception {
//		if(!this.negotiationClients.containsKey(epr))
//			this.negotiationClients.put(epr, new NegotiationClient(epr));
//		return this.negotiationClients.get(epr);
		// Workaround for web service problems
		// communicating using the SLAMs directly
		return new NegotiationDelegator(getSLAM(epr));
	}

	/**
	 * Provides a negotiation client for direct access to this syntax converter.
	 * 
	 * @param epr
	 *            The EPR of the web service to contact.
	 */
	public INegotiation getNegotiation() throws Exception {
	    return new NegotiationDelegator( this.context );
	}
	
	// FIXME remove following method as soon as web service works reliably

	public INegotiation getJavaNegotiationClient() {
		return new NegotiationDelegator(this.context);
	}

	public class NegotiationDelegator implements INegotiation {

		private SLAManagerContext context;

		public NegotiationDelegator(SLAManagerContext context) {
			super();
			this.context = context;
		}

		public boolean cancelNegotiation(String arg0,
				List<CancellationReason> arg1)
				throws OperationInProgressException,
				OperationNotPossibleException, InvalidNegotiationIDException {
			if(arg0 == null || arg1 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.cancelNegotiation(arg0, arg1);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return false;
			}
		}

		public SLA createAgreement(String arg0, SLATemplate arg1)
				throws OperationInProgressException, SLACreationException,
				InvalidNegotiationIDException {
			if(arg0 == null || arg1 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.createAgreement(arg0, arg1);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return null;
			}
		}

		public String initiateNegotiation(SLATemplate arg0)
				throws OperationNotPossibleException {
			if(arg0 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.initiateNegotiation(arg0);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return null;
			}
		}

		public SLATemplate[] negotiate(String arg0, SLATemplate arg1)
				throws OperationInProgressException,
				OperationNotPossibleException, InvalidNegotiationIDException {
			if(arg0 == null || arg1 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.negotiate(arg0, arg1);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return null;
			}
		}

		public SLA provision(UUID arg0) throws SLANotFoundException,
				ProvisioningException {
			if(arg0 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.provision(arg0);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return null;
			}
		}

		public String renegotiate(UUID arg0) throws SLANotFoundException,
				OperationNotPossibleException {
			if(arg0 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.renegotiate(arg0);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return null;
			}
		}

		public boolean terminate(UUID arg0, List<TerminationReason> arg1)
				throws SLANotFoundException {
			if(arg0 == null || arg1 == null) System.out.println("Syntax Converter has been passed a null value");
			try {
				return this.context.getProtocolEngine().getINegotiation()
						.terminate(arg0, arg1);
			} catch (SLAManagerContextException e) {
				e.printStackTrace();
				return false;
			}
		}

        public Customization customize(String negotiationID, Customization customization) throws NegotiationException {
            if(negotiationID == null || customization == null) System.out.println("Syntax Converter has been passed a null value");
            try {
                return this.context.getProtocolEngine().getINegotiation().customize(negotiationID, customization);
            } catch (SLAManagerContextException e) {
                e.printStackTrace();
                return null;
            }
        }

	}

	/**
	 * Provides a client for the SLA registry's <<query>> web service.
	 */
	public IQuery getSLARegistryQueryClient(String epr) throws Exception {
		return new SLARegistryQueryClient(epr);
	}

	/**
	 * Provides a client for the SLA registry's <<register>> web service.
	 */
	public IRegister getSLARegistryRegisterClient(String epr) throws Exception {
		return new SLARegistryRegisterClient(epr);
	}

	/**
	 * Provides a client for the SLA template registry's web service.
	 */
	public ISLATemplateRegistryClient getTemplateRegistryClient(String epr)
			throws Exception {
		return new TemplateRegistryClient(epr);
	}

	/**
	 * Parses a constraint expression from string.
	 */
	public ConstraintExpr parseConstraintExpr(String constraintString)
			throws Exception {
		if (delegator != null)
			return delegator.parseConstraintExpr(constraintString);

		ConstraintExprDocument constraintXml = ConstraintExprDocument.Factory
				.parse(constraintString);
		XmlElementParser xmlElementParser = new XmlElementParser();
		return xmlElementParser.parseConstraintExpr(constraintXml
				.getConstraintExpr());
	}

	/**
	 * Renders SLA minimum info metadata to string.
	 */
	public String renderMinInfo(SLAMinimumInfo minInfo) throws Exception {
		if (delegator != null)
			return delegator.renderMinimumInfo(minInfo);

		SLAMinimumInfoDocument slaInfoDoc = SLAMinimumInfoDocument.Factory
				.newInstance();

		slaInfoDoc.addNewSLAMinimumInfo();

		slaInfoDoc.getSLAMinimumInfo().setAgreedAt(minInfo.agreedAt.getValue());
		slaInfoDoc.getSLAMinimumInfo().setEffectiveFrom(
				minInfo.effectiveFrom.getValue());
		slaInfoDoc.getSLAMinimumInfo().setEffectiveUntil(
				minInfo.effectiveUntil.getValue());
		slaInfoDoc.getSLAMinimumInfo().setSLAID(minInfo.SLAID.getValue());
		slaInfoDoc.getSLAMinimumInfo().setTemplateID(
				minInfo.templateID.getValue());

		// Party
		XmlElementRenderer xmlElementRenderer = new XmlElementRenderer();
		PartyDocument[] partiesXml = new PartyDocument[minInfo.parties.length];
		for (int j = 0; j < minInfo.parties.length; j++) {
			AgreementPartyType partyType = xmlElementRenderer
					.renderParty(minInfo.parties[j]);
			PartyDocument partyDoc = PartyDocument.Factory.newInstance();
			partyDoc.setParty(partyType);
			partiesXml[j] = partyDoc;
		}
		slaInfoDoc.getSLAMinimumInfo().setPartyArray(partiesXml);

		return slaInfoDoc.xmlText();
	}

	public enum WSType {
		Negotiation, Control, SLARegistryRegister, SLARegistryQuery, SLATemplateRegistry
	}
}
