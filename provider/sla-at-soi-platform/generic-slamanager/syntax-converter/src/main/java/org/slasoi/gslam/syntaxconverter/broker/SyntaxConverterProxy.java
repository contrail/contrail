/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/broker/SyntaxConverterProxy.java $
 */
package org.slasoi.gslam.syntaxconverter.broker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
*
* Helper class for initializing of a Proxy (under OSGi) between the SyntaxConverter and
* the Broker (outside of OSGi).
* 
* @see org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterBroker
* 
* @author Miguel Rojas (UDO)
*
*/
public class SyntaxConverterProxy {
    
    /**
     * Constructor
     */
    public SyntaxConverterProxy() {
        init();
    }

    protected void init() {
        try {
            // connect with SyntaxConverterProxy
            String path = System.getenv("SLASOI_HOME");
            String filename =
                    path + System.getProperty("file.separator")
                            + "generic-slamanager/syntax-converter/broker.properties";
            Properties props = new Properties();
            props.load(new FileInputStream(filename));

            String host = (String) props.get("host");
            int port = Integer.parseInt((String) props.get("port"));

            LOGGER.info("SyntaxConverterProxy connecting to " + host + ":" + port + "...");
            sk = new Socket(host, port);
            in = new BufferedReader(new InputStreamReader(sk.getInputStream()));
            out = new PrintStream(sk.getOutputStream());
            LOGGER.info("SyntaxConverterProxy connected");
        }
        catch (Exception e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Helper method which serializes a command and its parameters.  This command will be
     * sent to the Broker through a Delegator according to its BrokerType.
     * 
     * @param v
     * @return
     */
    public Object command(Vector<?> v) {
        try {
            out.println(Base64Utils.encode(v));
            String result = in.readLine();
            return Base64Utils.decode(result);
        }
        catch (Exception e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Closes the channel between SyntaxConverter and the Broker
     */
    public void close() {
        try {
            out.println("end");
        }
        catch (Exception e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }
    }

    protected void finalize() throws Throwable {
        try {
            sk.close();
        }
        catch (Exception e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Helper method for testing.
     */
    public static void main(String[] args) {
        try {
            File f = new File("d:\\miguel.template");
            String xml = FileUtils.readFileToString(f);

            SyntaxConverterProxy proxy = new SyntaxConverterProxy();
            Vector<String> sentences = new Vector<String>(3, 2);
            sentences.add("SLATemplateDocument");
            sentences.add(xml);

            Object result = proxy.command(sentences);
            System.out.println(result.getClass());
            System.out.println(result);

            proxy.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Socket sk;
    protected PrintStream out;
    protected BufferedReader in;

    private static final Logger LOGGER = Logger.getLogger(SyntaxConverterProxy.class);
}
