/**
 * RegistrationFailureExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client;

public class RegistrationFailureExceptionException extends java.lang.Exception {

    private org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.RegistrationFailureExceptionE faultMessage;

    public RegistrationFailureExceptionException() {
        super("RegistrationFailureExceptionException");
    }

    public RegistrationFailureExceptionException(java.lang.String s) {
        super(s);
    }

    public RegistrationFailureExceptionException(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public RegistrationFailureExceptionException(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
            org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.RegistrationFailureExceptionE msg) {
        faultMessage = msg;
    }

    public org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.RegistrationFailureExceptionE getFaultMessage() {
        return faultMessage;
    }
}
