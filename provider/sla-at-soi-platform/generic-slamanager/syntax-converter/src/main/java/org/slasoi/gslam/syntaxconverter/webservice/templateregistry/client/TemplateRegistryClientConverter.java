package org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client;

import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlCalendar;
import org.ggf.schemas.graap.x2007.x03.wsAgreement.ConstraintDocument;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.syntaxconverter.XmlElementRenderer;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

import eu.slaAtSoi.types.templateregistry.MetadataDocument;
import eu.slaAtSoi.types.templateregistry.ResultSetDocument;
import eu.slaAtSoi.types.templateregistry.WarningDocument;
import eu.slaAtSoi.types.templateregistry.WarningType;
import eu.slaAtSoi.types.templateregistry.ResultSetType.Result;
import eu.slaatsoi.slamodel.ConstraintExprType;

public class TemplateRegistryClientConverter {
    public static String renderMetadata(Metadata metadata) {

        // Metadata
        MetadataDocument metaDataDoc = MetadataDocument.Factory.newInstance();
        metaDataDoc.addNewMetadata();

        // Activated At
        metaDataDoc.getMetadata().setActivatedAt(
                new XmlCalendar(metadata.getPropertyValue(new STND(
                        "http://www.slaatsoi.org/slat/metadata#activated_at_datetime"))));

        // Creator ID
        metaDataDoc.getMetadata().setCreatorID(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#creator_id")));

        // Disactivated At
        metaDataDoc.getMetadata().setDisactivatedAt(
                new XmlCalendar(metadata.getPropertyValue(new STND(
                        "http://www.slaatsoi.org/slat/metadata#disactived_at_datetime"))));

        // Previous Version
        metaDataDoc.getMetadata().setPreviousVersion(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#previous_version")));

        // Provider UUID
        metaDataDoc.getMetadata().setProviderUUID(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#provider_uuid")));

        // Registered At
        metaDataDoc.getMetadata().setRegisteredAt(
                new XmlCalendar(metadata.getPropertyValue(new STND(
                        "http://www.slaatsoi.org/slat/metadata#registered_at_datetime"))));

        // Registrar ID
        metaDataDoc.getMetadata().setRegistrarID(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#registrar_id")));

        // Replaced By Version
        metaDataDoc.getMetadata().setReplacedByVersion(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#replaced_by_version")));

        // Template UUID
        metaDataDoc.getMetadata().setTemplateUUID(
                metadata.getPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#template_uuid")));

        return metaDataDoc.xmlText();
    }

    public static Warning parseWarning(String warningString) throws Exception {
        WarningDocument warningDoc = WarningDocument.Factory.parse(warningString);
        return new Warning(warningDoc.getWarning().getMessage(), warningDoc.getWarning().getSourceEntity());
    }

    public static ResultSet parseResultSet(String resultString) throws Exception {
        ResultSetDocument resultXml = ResultSetDocument.Factory.parse(resultString);

        ResultSet resultSet = new ResultSet();

        // Error
        String[] errorArray = resultXml.getResultSet().getErrorArray();
        List<Exception> errors = new ArrayList<Exception>(errorArray.length);
        for (int i = 0; i < errorArray.length; i++) {
            String message = errorArray[i];
            errors.add(new Exception(message));
        }
        resultSet.errors = errors;

        // Warning
        WarningType[] warningArray = resultXml.getResultSet().getWarningArray();
        List<Warning> warnings = new ArrayList<Warning>(warningArray.length);
        for (int i = 0; i < warningArray.length; i++) {
            String message = warningArray[i].getMessage();
            String sourceEntity = warningArray[i].getSourceEntity();
            warnings.add(new Warning(message, sourceEntity));
        }
        resultSet.warnings = warnings;

        // Result
        Result[] resultArray = resultXml.getResultSet().getResultArray();
        List<SLATemplateRegistry.ResultSet.Result> results =
                new ArrayList<SLATemplateRegistry.ResultSet.Result>(resultArray.length);
        for (int i = 0; i < resultArray.length; i++) {
            Result result = resultArray[i];
            float confidence = result.getConfidence();
            String uuid = result.getUUID();

            results.add(new SLATemplateRegistry.ResultSet.Result(new UUID(uuid), confidence));
        }
        resultSet.results = results;

        return resultSet;
    }

    public static String renderConstraint(ConstraintExpr constraint) throws Exception {
        ConstraintExprType renderConstraintExpr = new XmlElementRenderer().renderConstraintExpr(constraint);
        ConstraintDocument constraintDocument = ConstraintDocument.Factory.newInstance();
        constraintDocument.setConstraint(renderConstraintExpr);
        return constraintDocument.xmlText();
    }
}
