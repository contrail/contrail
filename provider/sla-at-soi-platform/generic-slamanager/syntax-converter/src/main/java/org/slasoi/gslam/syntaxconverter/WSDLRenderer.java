package org.slasoi.gslam.syntaxconverter;

import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.w3.x2006.x01.wsdl.BindingDocument;
import org.w3.x2006.x01.wsdl.BindingType;
import org.w3.x2006.x01.wsdl.DescriptionDocument;
import org.w3.x2006.x01.wsdl.DocumentationType;
import org.w3.x2006.x01.wsdl.ImportDocument;
import org.w3.x2006.x01.wsdl.ImportType;
import org.w3.x2006.x01.wsdl.IncludeDocument;
import org.w3.x2006.x01.wsdl.IncludeType;
import org.w3.x2006.x01.wsdl.InterfaceOperationType;
import org.w3.x2006.x01.wsdl.InterfaceType;
import org.w3.x2006.x01.wsdl.MessageRefFaultType;
import org.w3.x2006.x01.wsdl.MessageRefType;
import org.w3.x2006.x01.wsdl.ServiceDocument;
import org.w3.x2006.x01.wsdl.ServiceType;
import org.w3.x2006.x01.wsdl.TypesDocument;
import org.w3.x2006.x01.wsdl.TypesType;

import eu.slaatsoi.slamodel.DomainExprDocument;
import eu.slaatsoi.slamodel.DomainExprType;
import eu.slaatsoi.slamodel.InterfaceOperationPropertyType;
import eu.slaatsoi.slamodel.RelatedDocument;

/**
 * Renders an interface specification to WSDL 2.0.
 * @author Peter A. Chronz
 *
 */
public class WSDLRenderer {
    protected XmlElementRenderer elementRenderer = new XmlElementRenderer();

    public String renderWSDL(Interface.Specification iSpecification) throws Exception {
        DescriptionDocument wsdlXml = DescriptionDocument.Factory.newInstance();
        wsdlXml.addNewDescription();

        // Interface
        InterfaceType interfaceXml = renderInterfaceSpecification(iSpecification);
        InterfaceType[] interfacesXml = new InterfaceType[1];
        interfacesXml[0] = interfaceXml;
        wsdlXml.getDescription().setInterfaceArray(interfacesXml);

        STND[] propertyKeys = iSpecification.getPropertyKeys();

        // Binding
        List<STND> keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Binding")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        BindingType[] bindingsXml = new BindingType[keysXml.size()];
        int j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            BindingDocument bindingXml = BindingDocument.Factory.parse(stringValue);
            bindingsXml[j] = bindingXml.getBinding();
            j++;
        }
        wsdlXml.getDescription().setBindingArray(bindingsXml);

        // Documentation
        keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Documentation")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        DocumentationType[] docsXml = new DocumentationType[keysXml.size()];
        j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            org.w3.x2006.x01.wsdl.DocumentationDocument docXml =
                    org.w3.x2006.x01.wsdl.DocumentationDocument.Factory.parse(stringValue);
            docsXml[j] = docXml.getDocumentation();
            j++;
        }
        wsdlXml.getDescription().setDocumentationArray(docsXml);

        // Import
        keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Import")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        ImportType[] importsXml = new ImportType[keysXml.size()];
        j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            ImportDocument importXml = ImportDocument.Factory.parse(stringValue);
            importsXml[j] = importXml.getImport();
            j++;
        }
        wsdlXml.getDescription().setImportArray(importsXml);

        // Include
        keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Include")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        IncludeType[] includesXml = new IncludeType[keysXml.size()];
        j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            IncludeDocument includeXml = IncludeDocument.Factory.parse(stringValue);
            includesXml[j] = includeXml.getInclude();
            j++;
        }
        wsdlXml.getDescription().setIncludeArray(includesXml);

        // Service
        keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Service")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        ServiceType[] servicesXml = new ServiceType[keysXml.size()];
        j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            ServiceDocument serviceXml = ServiceDocument.Factory.parse(stringValue);
            servicesXml[j] = serviceXml.getService();
            j++;
        }
        wsdlXml.getDescription().setServiceArray(servicesXml);

        // TargetNamespace
        wsdlXml.getDescription().setTargetNamespace(iSpecification.getPropertyValue(new STND("TargetNamespace")));

        // Type
        keysXml = new LinkedList<STND>();
        for (int i = 0; i < propertyKeys.length; i++) {
            if (propertyKeys[i].getValue().startsWith("Type")) {
                keysXml.add(propertyKeys[i]);
            }
        }
        TypesType[] typesXml = new TypesType[keysXml.size()];
        j = 0;
        for (STND key : keysXml) {
            String stringValue = iSpecification.getPropertyValue(key);
            TypesDocument typeXml = TypesDocument.Factory.parse(stringValue);
            typesXml[j] = typeXml.getTypes();
            j++;
        }
        wsdlXml.getDescription().setTypesArray(typesXml);

        return wsdlXml.xmlText();
    }

    private InterfaceType renderInterfaceSpecification(Specification iSpecification) throws Exception {
        InterfaceType interfaceXml = InterfaceType.Factory.newInstance();

        // Annotation
        this.elementRenderer.setAnnotation(iSpecification, interfaceXml);

        // Operation
        Operation[] operations = iSpecification.getOperations();
        InterfaceOperationType[] operationsXml = new InterfaceOperationType[operations.length];
        for (int i = 0; i < operations.length; i++) {
            Operation operation = operations[i];
            operationsXml[i] = renderOperation(operation);
        }
        interfaceXml.setOperationArray(operationsXml);

        return interfaceXml;
    }

    private InterfaceOperationType renderOperation(Operation operation) throws Exception {
        InterfaceOperationType operationXml = InterfaceOperationType.Factory.newInstance();

        // Annotation
        this.elementRenderer.setAnnotation(operation, operationXml);

        // Input
        Property[] inputs = operation.getInputs();
        MessageRefType[] inputsXml = new MessageRefType[inputs.length];
        for (int i = 0; i < inputs.length; i++) {
            inputsXml[i] = renderMessageRef(inputs[i]);
        }
        operationXml.setInputArray(inputsXml);

        // Output
        Property[] outputs = operation.getOutputs();
        MessageRefType[] outputsXml = new MessageRefType[outputs.length];
        for (int i = 0; i < outputs.length; i++) {
            outputsXml[i] = renderMessageRef(outputs[i]);
        }
        operationXml.setOutputArray(outputsXml);

        // OutFault
        STND[] faults = operation.getFaults();
        MessageRefFaultType[] outfaultsXml = new MessageRefFaultType[faults.length];
        for (int i = 0; i < faults.length; i++) {
            outfaultsXml[i] = renderMessageRefFault(faults[i]);
        }

        // Related
        Property[] related = operation.getRelated();
        for (int i = 0; i < related.length; i++) {
            InterfaceOperationPropertyType relatedXml =
                    this.elementRenderer.renderInterfaceOperationProperty(related[i]);
            RelatedDocument relatedXmlDoc = RelatedDocument.Factory.newInstance();
            relatedXmlDoc.setRelated(relatedXml);

            // append the related property to the operation
            XmlCursor operationCursor = operationXml.newCursor();
            XmlCursor relatedCursor = relatedXmlDoc.newCursor();
            operationCursor.toLastChild();
            relatedCursor.toNextToken();
            relatedCursor.moveXml(operationCursor);
            relatedCursor.dispose();
            operationCursor.dispose();
        }

        return operationXml;
    }

    private MessageRefFaultType renderMessageRefFault(STND stnd) {
        MessageRefFaultType messageRefXml = MessageRefFaultType.Factory.newInstance();

        String value = stnd.getValue();
        int lastIndexOf = value.lastIndexOf(":");
        String localName = value.substring(++lastIndexOf);
        String uri = value.substring(0, lastIndexOf);

        messageRefXml.setRef(new QName(uri, localName));

        return messageRefXml;
    }

    private MessageRefType renderMessageRef(Property property) throws Exception {
        MessageRefType messageRefXml = MessageRefType.Factory.newInstance();

        // Annotation
        this.elementRenderer.setAnnotation(property, messageRefXml);

        // MessaeLabel
        messageRefXml.setMessageLabel(property.getName().getValue());

        // DataType
        messageRefXml.setElement(new QName("tns", property.getDatatype().getValue()));

        // Auxiliary
        XmlObject auxiliaryXml = XmlObject.Factory.newInstance();
        XmlCursor auxCursor = auxiliaryXml.newCursor();
        auxCursor.toNextToken();
        auxCursor.beginElement(new QName(WSAgreementTemplateRenderer.slasoiNamespace, "BOOL"));
        auxCursor.insertChars("false");
        auxCursor.toStartDoc();
        auxCursor.toNextToken();
        XmlCursor messageRefCursor = messageRefXml.newCursor();
        messageRefCursor.toEndToken();
        auxCursor.moveXml(messageRefCursor);
        auxCursor.dispose();
        messageRefCursor.dispose();

        // DomainExpr
        DomainExprDocument domainExprDoc = DomainExprDocument.Factory.newInstance();
        DomainExprType domainExprXml = this.elementRenderer.renderDomainExpr(property.getDomain());
        domainExprDoc.setDomainExpr(domainExprXml);
        XmlCursor domainCursor = domainExprDoc.newCursor();
        domainCursor.toNextToken();
        messageRefCursor = messageRefXml.newCursor();
        messageRefCursor.toEndToken();
        domainCursor.moveXml(messageRefCursor);
        messageRefCursor.dispose();
        domainCursor.dispose();

        return messageRefXml;
    }
}
