package org.slasoi.gslam.syntaxconverter.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;
import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation.CancellationReason;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.syntaxconverter.ContextKeeper;
import org.slasoi.gslam.syntaxconverter.SLASOIRenderer;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.gslam.syntaxconverter.SyntaxConverterDelegator;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class NegotiationImpl implements INegotiationAbstraction, Lifecycle {

    public SLAManagerContext context;

    protected static SyntaxConverterDelegator delegator;
    
    private static final Logger LOGGER = Logger.getLogger( NegotiationImpl.class );
    private static int INSTANCES = 0;

    public NegotiationImpl() {
    	if(NegotiationImpl.delegator == null)
    		NegotiationImpl.delegator = new SyntaxConverterDelegator(SyntaxConverterType.SLASOISyntaxConverter);
        
        INSTANCES++;
        LOGGER.info( "NegotiationImpl:instances:" + INSTANCES );
        LOGGER.info( "NegotiationImpl:ws created for incomming requests" );
    }
    
    public void init(ServiceContext context) throws AxisFault {
        
        String wsname = context.getAxisService().getName();
        this.context = ContextKeeper.getInstance().findContext( wsname );
        LOGGER.info( "NegotiationImpl:context of recent ws : " + this.context );
    }
    
    public void destroy( ServiceContext context ){
    }
    
    public boolean cancelNegotiation(String negotiationID, String[] cancellationReason)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException {
        ArrayList<CancellationReason> reasons = new ArrayList<CancellationReason>(cancellationReason.length);
        for (int i = 0; i < cancellationReason.length; i++) {
            String cancellationReasonString = cancellationReason[i];
            if (cancellationReasonString.equalsIgnoreCase("LOST_CONVERGENCE_HOPE")) {
                reasons.add(CancellationReason.LOST_CONVERGENCE_HOPE);
            }
            else if (cancellationReasonString.equalsIgnoreCase("PARALLEL_NEGOTIATION_SUCCEEDED")) {
                reasons.add(CancellationReason.PARALLEL_NEGOTIATION_SUCCEEDED);
            }
            else if (cancellationReasonString.equalsIgnoreCase("STATUS_COMPROMISED")) {
                reasons.add(CancellationReason.STATUS_COMPROMISED);
            }
            else if (cancellationReasonString.equalsIgnoreCase("WILL_RETURN_LATER")) {
                reasons.add(CancellationReason.WILL_RETURN_LATER);
            }
        }

        try {
            return this.context.getProtocolEngine().getINegotiation().cancelNegotiation(negotiationID, reasons);
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String createAgreement(String negotiationID, String slaTemplate) throws OperationInProgressException,
            SLACreationException, InvalidNegotiationIDException {
        try {
            Object obj;
            if (delegator != null) {
                obj = delegator.parseSLATemplate(slaTemplate);
            }
            else {
                obj = new SLASOITemplateParser().parseTemplate(slaTemplate);
            }
            SLATemplate slaTemplateObject = (SLATemplate) obj;

            SLA createdAgreement =
                    this.context.getProtocolEngine().getINegotiation()
                            .createAgreement(negotiationID, slaTemplateObject);

            if (delegator != null) {
                return delegator.renderSLA(createdAgreement);
            }
            else {
                return new SLASOIRenderer().renderSLA(createdAgreement);
            }
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String initiateNegotiation(String slaTemplate) throws OperationNotPossibleException {
        try {
            if (delegator != null) {
                return this.context.getProtocolEngine().getINegotiation().initiateNegotiation(
                        (SLATemplate) delegator.parseSLATemplate(slaTemplate));
            }
            else {
                return this.context.getProtocolEngine().getINegotiation().initiateNegotiation(
                        new SLASOITemplateParser().parseTemplate(slaTemplate));
            }
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String[] negotiate(String negotiationID, String slaTemplate) throws OperationInProgressException,
            OperationNotPossibleException, InvalidNegotiationIDException {
        try {
            SLATemplate templateObject = null;
            if (delegator != null) {
                templateObject = (SLATemplate) delegator.parseSLATemplate(slaTemplate);
            }
            else {
                templateObject = new SLASOITemplateParser().parseTemplate(slaTemplate);
            }

            SLATemplate[] templateObjects =
                    this.context.getProtocolEngine().getINegotiation().negotiate(negotiationID, templateObject);
            String[] templates = new String[templateObjects.length];
            for (int i = 0; i < templateObjects.length; i++) {
                if (delegator != null) {
                    templates[i] = delegator.renderSLATemplate(templateObjects[i]);
                }
                else {
                    templates[i] = new SLASOITemplateRenderer().renderSLATemplate(templateObjects[i]);
                }

            }
            return templates;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new OperationNotPossibleException(e.getMessage());
        }
    }

    public String provision(String slaID) throws SLANotFoundException, ProvisioningException {
        try {
            SLA slaObject = this.context.getProtocolEngine().getINegotiation().provision(new UUID(slaID));
            if (delegator != null) {
                return delegator.renderSLA(slaObject);
            }
            else {
                return this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter)
                        .renderSLA(slaObject);
            }
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String renegotiate(String slaID) throws SLANotFoundException, OperationNotPossibleException {
        try {
            return this.context.getProtocolEngine().getINegotiation().renegotiate(new UUID(slaID));
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean terminate(String slaID, String[] terminationReason) throws SLANotFoundException {
        try {
            List<TerminationReason> reasons = new ArrayList<TerminationReason>(terminationReason.length);
            for (int i = 0; i < reasons.size(); i++) {
                String terminationReasonString = terminationReason[i];
                if (terminationReasonString.equalsIgnoreCase("")) {
                    reasons.add(TerminationReason.BUSINESS_DECISION);
                }
                else if (terminationReasonString.equalsIgnoreCase("COST")) {
                    reasons.add(TerminationReason.COST);
                }
                else if (terminationReasonString.equalsIgnoreCase("CUSTOMER_INITIATED")) {
                    reasons.add(TerminationReason.CUSTOMER_INITIATED);
                }
                else if (terminationReasonString.equalsIgnoreCase("DEMAND_DECREASED")) {
                    reasons.add(TerminationReason.DEMAND_DECREASED);
                }
                else if (terminationReasonString.equalsIgnoreCase("DEMAND_INCREASED")) {
                    reasons.add(TerminationReason.DEMAND_INCREASED);
                }
                else if (terminationReasonString.equalsIgnoreCase("FOUND_BETTER_CUSTOMER")) {
                    reasons.add(TerminationReason.FOUND_BETTER_CUSTOMER);
                }
                else if (terminationReasonString.equalsIgnoreCase("FOUND_BETTER_PROVIDER")) {
                    reasons.add(TerminationReason.FOUND_BETTER_PROVIDER);
                }
                else if (terminationReasonString.equalsIgnoreCase("PROVIDER_INITIATED")) {
                    reasons.add(TerminationReason.PROVIDER_INITIATED);
                }
                else if (terminationReasonString.equalsIgnoreCase("STATUS_COMPROMISED")) {
                    reasons.add(TerminationReason.STATUS_COMPROMISED);
                }
                else if (terminationReasonString.equalsIgnoreCase("UNSATISFACTORY_QUALITY")) {
                    reasons.add(TerminationReason.UNSATISFACTORY_QUALITY);
                }
            }

            return this.context.getProtocolEngine().getINegotiation().terminate(new UUID(slaID), reasons);
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return false;
        }
    }
}
