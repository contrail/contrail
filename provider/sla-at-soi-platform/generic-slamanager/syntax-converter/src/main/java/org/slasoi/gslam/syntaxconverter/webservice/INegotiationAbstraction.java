package org.slasoi.gslam.syntaxconverter.webservice;

import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;

/**
 * Provides an abstraction for the negotiatin interface to be used as web service.
 * @author Peter A. Chronz
 *
 */
public interface INegotiationAbstraction {
    public String initiateNegotiation(String slaTemplate) throws OperationNotPossibleException;

    public String[] negotiate(String negotiationID, String slaTemplate) throws OperationInProgressException,
            OperationNotPossibleException, InvalidNegotiationIDException;

    public String createAgreement(String negotiationID, String slaTemplate) throws OperationInProgressException,
            SLACreationException, InvalidNegotiationIDException;

    public boolean cancelNegotiation(String negotiationID, String[] cancellationReason)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException;

    public String renegotiate(String slaID) throws SLANotFoundException, OperationNotPossibleException;

    public boolean terminate(String slaID, String[] terminationReason) throws SLANotFoundException;

    public String provision(String slaID) throws SLANotFoundException, ProvisioningException;
}
