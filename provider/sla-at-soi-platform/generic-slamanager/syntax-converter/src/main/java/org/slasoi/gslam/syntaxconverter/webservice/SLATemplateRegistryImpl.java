package org.slasoi.gslam.syntaxconverter.webservice;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlCalendar;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.syntaxconverter.ContextKeeper;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

import eu.slaAtSoi.types.templateregistry.MetadataDocument;
import eu.slaAtSoi.types.templateregistry.ResultSetDocument;
import eu.slaAtSoi.types.templateregistry.WarningDocument;
import eu.slaAtSoi.types.templateregistry.WarningType;
import eu.slaAtSoi.types.templateregistry.ResultSetType.Result;

/**
 * An implementation for the abstracted interface for the SLA template registry to be used as a web service.
 * @author Peter A. Chronz
 *
 */
public class SLATemplateRegistryImpl implements SLATemplateRegistryAbstraction, ServiceFactory {

    private SLAManagerContext context;
    
    private static final Logger LOGGER = Logger.getLogger( SLATemplateRegistryImpl.class );
    
    public void setContext(SLAManagerContext context) {
    	this.context = context;
    }

    public SLATemplateRegistryImpl() {
        super();
    }

    // Warning[] <-- SLATemplate, Metadata
    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLATemplateRegistryAbstraction#addSLATemplate(java.lang.String,
     * java.lang.String)
     */
    /**
     * Add a new SLA template to the registry.
     * @param slat The serialized SLA template.
     * @param metadata Serialized metadata regarding the new SLA template.
     */
    public String[] addSLATemplate(String slat, String metadata) throws Exception {
        try {
            MetadataDocument metadataDoc = MetadataDocument.Factory.parse(metadata);
            Metadata metadataObject = new Metadata();

            // Activated At
            XmlCalendar activatedAt = new XmlCalendar(metadataDoc.getMetadata().getActivatedAt().getTime());
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#activated_at_datetime"),
                    activatedAt.toString());

            // Creator ID
            String creatorID = metadataDoc.getMetadata().getCreatorID();
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#creator_id"), creatorID);

            // Disactivated At
            XmlCalendar disactivatedAt = new XmlCalendar(metadataDoc.getMetadata().getDisactivatedAt().getTime());
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#disactived_at_datetime"),
                    disactivatedAt.toString());

            // Previous Version
            String previousVersion = metadataDoc.getMetadata().getPreviousVersion();
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#previous_version"),
                    previousVersion);

            // Provider UUID
            String providerUUID = metadataDoc.getMetadata().getProviderUUID();
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#provider_uuid"),
                    providerUUID);

            // Registered At
            XmlCalendar registeredAt = new XmlCalendar(metadataDoc.getMetadata().getRegisteredAt().getTime());
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#registered_at_datetime"),
                    registeredAt.toString());

            // Registrar ID
            String registrarID = metadataDoc.getMetadata().getRegistrarID();
            metadataObject
                    .setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#registrar_id"), registrarID);

            // Replaced By Version
            String replacedByVersion = metadataDoc.getMetadata().getReplacedByVersion();
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#replaced_by_version"),
                    replacedByVersion);

            // Template UUID
            String templateUUID = metadataDoc.getMetadata().getTemplateUUID();
            metadataObject.setPropertyValue(new STND("http://www.slaatsoi.org/slat/metadata#template_uuid"),
                    templateUUID);

            ISyntaxConverter iSyntaxConverter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);

            Warning[] warnings =
                    this.context.getSLATemplateRegistry().addSLATemplate(
                            (SLATemplate) iSyntaxConverter.parseSLATemplate(slat), metadataObject);

            return convertWarningsToStrings(warnings);
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    // UUID[] <-- ConstraintExpr
    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLATemplateRegistryAbstraction#query(java.lang.String)
     */
    /**
     * A metadata query for a on the template registry.
     * @param Metadata for the query on the registry.
     */
    public String[] query(String metadataQuery) throws Exception {
        try {
            ISyntaxConverter iSyntaxConverter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            ConstraintExpr constraint = iSyntaxConverter.parseConstraintExpr(metadataQuery);
            UUID[] result = this.context.getSLATemplateRegistry().query(constraint);
            String[] resultStrings = new String[result.length];
            for (int i = 0; i < result.length; i++) {
                resultStrings[i] = result[i].getValue();
            }

            return resultStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    // ResultSet <-- SLATemplate, ConstraintExpr
    /**
     * Query the template registry using a template and additional metadata.
     * @param query Template used in this query.
     * @param metadataConstraint The metadata constraint on the query. 
     */
    public String queryWithTemplate(String query, String metadataConstraint) throws Exception {
        try {
            ISyntaxConverter iSyntaxConverter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            ConstraintExpr constraint = iSyntaxConverter.parseConstraintExpr(metadataConstraint);
            ISyntaxConverter synVerter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            ResultSet resultSet =
                    this.context.getSLATemplateRegistry().query((SLATemplate) synVerter.parseSLATemplate(query),
                            constraint);
            ResultSetDocument resultXml = ResultSetDocument.Factory.newInstance();
            resultXml.addNewResultSet();

            // Warnings
            WarningType[] warningsXml = new WarningType[resultSet.warnings.size()];
            for (int i = 0; i < warningsXml.length; i++) {
                WarningDocument warningXml = WarningDocument.Factory.newInstance();
                warningXml.addNewWarning().setMessage(resultSet.warnings.get(i).message());
                warningXml.getWarning().setSourceEntity(resultSet.warnings.get(i).sourceEntity().toString());
                warningsXml[i] = warningXml.getWarning();
            }
            resultXml.getResultSet().setWarningArray(warningsXml);

            // Errors
            String[] stringErrors = new String[resultSet.errors.size()];
            for (int i = 0; i < stringErrors.length; i++) {
                stringErrors[i] = resultSet.errors.get(i).getMessage();
            }
            resultXml.getResultSet().setErrorArray(stringErrors);

            // Results
            for (int i = 0; i < resultSet.results.size(); i++) {
                Result result = resultXml.getResultSet().addNewResult();
                result.setConfidence(resultSet.results.get(i).getConfidence());
                result.setUUID(resultSet.results.get(i).getUuid().getValue());
            }

            return resultXml.xmlText();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    // UUID
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.gslam.syntaxconverter.webservice.SLATemplateRegistryAbstraction#removeSLATemplate(java.lang.String)
     */
    /**
     * Removes an SLA template from the template registry.
     * @param templateId The id of the template to be removed.
     */
    public void removeSLATemplate(String templateId) throws Exception {
        try {
            this.context.getSLATemplateRegistry().removeSLATemplate(new UUID(templateId));
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
    }

    private String[] convertWarningsToStrings(Warning[] warnings) {
        String[] warningStrings = new String[warnings.length];
        for (int i = 0; i < warnings.length; i++) {
            WarningDocument warningXml = WarningDocument.Factory.newInstance();
            warningXml.addNewWarning().setMessage(warnings[i].message());
            warningXml.getWarning().setSourceEntity(warnings[i].sourceEntity().toString());
            warningStrings[i] = warningXml.xmlText();
        }

        return warningStrings;
    }

    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        String slamId = (String) arg1.getReference().getProperty("org.apache.axis2.osgi.ws");

        this.context = ContextKeeper.getInstance().getContext(slamId);

        return this;
    }

    public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
        // TODO Auto-generated method stub

    }
}
