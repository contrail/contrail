package org.slasoi.gslam.syntaxconverter.webservice;

import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;

/**
 * An abstraction class for the SLA registry's interface for usage in a web service.
 * 
 * @author Peter A. Chronz
 *
 */

public interface SLARegistryRegisterAbstraction {

    /**
     * Register an agreement, providing dependencies and the SLA state.
     * 
     * @param agreement The SLA serialized to a string.
     * @param dependencies A set of serialized SLA dependencies.
     * @param state The State class serialized to a string.
     * @return The registered SLA in serialized form.
     * @throws RegistrationFailureException 
     */
    public abstract String register(String agreement, String[] dependencies, String state)
            throws RegistrationFailureException;

    /**
     * Update dependencies and the state for a registered agreement.
     * 
     * @param id The SLA's id.
     * @param agreement The agreement itself, serialized to string.
     * @param dependencies The updated list of serialized dependencies.
     * @param state The updated serialized state.
     * @return
     * @throws UpdateFailureException
     */
    public abstract String update(String id, String agreement, String[] dependencies, String state)
            throws UpdateFailureException;

}
