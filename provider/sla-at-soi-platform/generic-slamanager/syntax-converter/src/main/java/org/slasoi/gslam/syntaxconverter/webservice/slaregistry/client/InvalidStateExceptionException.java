/**
 * InvalidStateExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client;

public class InvalidStateExceptionException extends java.lang.Exception {

    private org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.InvalidStateExceptionE faultMessage;

    public InvalidStateExceptionException() {
        super("InvalidStateExceptionException");
    }

    public InvalidStateExceptionException(java.lang.String s) {
        super(s);
    }

    public InvalidStateExceptionException(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public InvalidStateExceptionException(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
            org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.InvalidStateExceptionE msg) {
        faultMessage = msg;
    }

    public org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.InvalidStateExceptionE getFaultMessage() {
        return faultMessage;
    }
}
