package org.slasoi.gslam.syntaxconverter;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.impl.xb.xsdschema.DocumentationDocument;
import org.apache.xmlbeans.impl.xb.xsdschema.IncludeDocument;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.w3.x2006.x01.wsdl.BindingDocument;
import org.w3.x2006.x01.wsdl.BindingType;
import org.w3.x2006.x01.wsdl.DescriptionDocument;
import org.w3.x2006.x01.wsdl.DocumentationType;
import org.w3.x2006.x01.wsdl.ImportDocument;
import org.w3.x2006.x01.wsdl.ImportType;
import org.w3.x2006.x01.wsdl.IncludeType;
import org.w3.x2006.x01.wsdl.InterfaceOperationType;
import org.w3.x2006.x01.wsdl.InterfaceType;
import org.w3.x2006.x01.wsdl.MessageRefFaultType;
import org.w3.x2006.x01.wsdl.MessageRefType;
import org.w3.x2006.x01.wsdl.ServiceDocument;
import org.w3.x2006.x01.wsdl.ServiceType;
import org.w3.x2006.x01.wsdl.TypesDocument;
import org.w3.x2006.x01.wsdl.TypesType;
import org.w3c.dom.Node;

import eu.slaatsoi.slamodel.BoolType;
import eu.slaatsoi.slamodel.RelatedDocument;
/**
 * Parses a WSDL 2.0 string to an interface specification.
 * @author Peter A. Chronz
 *
 */
public class WSDLParser {
    protected XmlElementParser elementParser;

    public WSDLParser() {
        this.elementParser = new XmlElementParser();
    }

    public Interface.Specification[] parseWSDL(String wsdlString) throws Exception {
        DescriptionDocument wsdlXml = DescriptionDocument.Factory.parse(wsdlString);
        InterfaceType[] interfacesXml = wsdlXml.getDescription().getInterfaceArray();
        Interface.Specification[] interfaces = new Interface.Specification[interfacesXml.length];
        for (int i = 0; i < interfacesXml.length; i++) {
            interfaces[i] = parseInterface(interfacesXml[i]);
        }

        if (interfaces.length > 0) {
            Specification specification = interfaces[0];

            // Binding
            BindingType[] bindings = wsdlXml.getDescription().getBindingArray();
            for (int i = 0; i < bindings.length; i++) {
                STND key = new STND("Binding" + i);
                BindingDocument bindingXml = BindingDocument.Factory.parse(bindings[i].getDomNode());

                QName interfaceName = bindings[i].getInterface();
                XmlObject[] interfaceObjectsXml = bindings[i].selectPath("@interface");
                String prefix = "";
                // interfaceName.getPrefix() returns ""
                if (interfaceObjectsXml.length == 1) {
                    XmlCursor interfaceCursor = interfaceObjectsXml[0].newCursor();
                    String interfaceString = interfaceCursor.getTextValue();
                    int endIndex = interfaceString.indexOf(":");
                    prefix = interfaceString.substring(0, endIndex);
                }
                String namespaceURI = interfaceName.getNamespaceURI();

                XmlCursor xmlCursor = bindingXml.newCursor();
                xmlCursor.toFirstChild();
                xmlCursor.toNextToken();
                xmlCursor.insertNamespace(prefix, namespaceURI);
                xmlCursor.dispose();
                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                bindingXml.save(xmlStream);
                specification.setPropertyValue(key, xmlStream.toString());
            }

            // Documentation
            DocumentationType[] documentationArray = wsdlXml.getDescription().getDocumentationArray();
            for (int i = 0; i < documentationArray.length; i++) {
                STND key = new STND("Documentation" + 1);
                DocumentationDocument docsXml = DocumentationDocument.Factory.parse(documentationArray[i].getDomNode());
                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                docsXml.save(xmlStream);
                specification.setPropertyValue(key, xmlStream.toString());
            }

            // Import
            ImportType[] importArray = wsdlXml.getDescription().getImportArray();
            for (int i = 0; i < importArray.length; i++) {
                STND key = new STND("Import" + i);
                ImportDocument importXml = ImportDocument.Factory.parse(importArray[i].getDomNode());

                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                importXml.save(xmlStream);
                specification.setPropertyValue(key, xmlStream.toString());
            }

            // Include
            IncludeType[] includeArray = wsdlXml.getDescription().getIncludeArray();
            for (int i = 0; i < includeArray.length; i++) {
                STND key = new STND("Include" + i);
                IncludeDocument includeXml = IncludeDocument.Factory.parse(includeArray[i].getDomNode());

                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                includeXml.save(xmlStream);
                specification.setPropertyValue(key, xmlStream.toString());
            }

            // Service
            ServiceType[] services = wsdlXml.getDescription().getServiceArray();
            for (int i = 0; i < services.length; i++) {
                STND key = new STND("Serive" + i);
                ServiceDocument serviceXml = ServiceDocument.Factory.parse(services[i].getDomNode());

                QName interfaceName = services[i].getInterface();

                XmlObject[] interfaceObjectsXml = bindings[i].selectPath("@interface");
                String prefix = "";
                // interfaceName.getPrefix() returns ""
                if (interfaceObjectsXml.length == 1) {
                    XmlCursor interfaceCursor = interfaceObjectsXml[0].newCursor();
                    String interfaceString = interfaceCursor.getTextValue();
                    int endIndex = interfaceString.indexOf(":");
                    prefix = interfaceString.substring(0, endIndex);
                }

                String namespaceURI = interfaceName.getNamespaceURI();

                XmlCursor xmlCursor = serviceXml.newCursor();
                xmlCursor.toFirstChild();
                xmlCursor.toNextToken();
                xmlCursor.insertNamespace(prefix, namespaceURI);
                xmlCursor.dispose();
                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                serviceXml.save(xmlStream);
                specification.setPropertyValue(key, xmlStream.toString());
            }

            // TargetNamesace
            String targetNamespace = wsdlXml.getDescription().getTargetNamespace();
            specification.setPropertyValue(new STND("TargetNamespace"), targetNamespace);

            // Type
            TypesType[] typesArray = wsdlXml.getDescription().getTypesArray();
            for (int i = 0; i < typesArray.length; i++) {
                STND key = new STND("Type" + i);
                TypesDocument typeXml = TypesDocument.Factory.parse(typesArray[i].getDomNode());

                ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
                typeXml.save(xmlStream);

                specification.setPropertyValue(key, xmlStream.toString());
            }
        }

        return interfaces;
    }

    @SuppressWarnings("unchecked")
    private Specification parseInterface(InterfaceType interfaceType) throws Exception {
        Specification specification = new Interface.Specification();

        // Annotation
        this.elementParser.setAnnotation(specification, interfaceType);

        // Name
        String name = interfaceType.getName();
        specification.setName(name);

        // Extended
        List<Object> extendedXml = interfaceType.getExtends();
        ID[] extended = new ID[extendedXml.size()];
        int i = 0;
        for (Object idObject : extendedXml) {
            QName idQname = (QName) idObject;
            String id = idQname.getNamespaceURI() + ":" + idQname.getLocalPart();
            extended[i] = new ID(id);
            i++;
        }
        specification.setExtended(extended);

        // Operation
        InterfaceOperationType[] operationsXml = interfaceType.getOperationArray();
        Interface.Operation[] operations = new Interface.Operation[operationsXml.length];
        for (int j = 0; j < operations.length; j++) {
            operations[j] = parseOperation(operationsXml[j]);
        }
        specification.setOperations(operations);

        return specification;
    }

    private Operation parseOperation(InterfaceOperationType operationXml) throws Exception {
        // Name
        String name = operationXml.getName();
        Interface.Operation operation = new Interface.Operation(new ID(name));

        // Annotation
        this.elementParser.setAnnotation(operation, operationXml);

        // Fault
        MessageRefFaultType[] outfaults = operationXml.getOutfaultArray();
        STND[] faults = new STND[outfaults.length];
        for (int i = 0; i < outfaults.length; i++) {
            MessageRefFaultType outfault = outfaults[i];
            QName ref = outfault.getRef();
            faults[i] = new STND(ref.getNamespaceURI() + ":" + ref.getLocalPart());
        }
        operation.setFaults(faults);

        // Input
        MessageRefType[] inputsXml = operationXml.getInputArray();
        Interface.Operation.Property[] inputs = new Property[inputsXml.length];
        for (int i = 0; i < inputsXml.length; i++) {
            MessageRefType inputXml = inputsXml[i];
            inputs[i] = parseMessageRef(inputXml);
        }
        operation.setInputs(inputs);

        // Output
        MessageRefType[] outputsXml = operationXml.getOutputArray();
        Interface.Operation.Property[] outputs = new Property[outputsXml.length];
        for (int i = 0; i < outputsXml.length; i++) {
            MessageRefType outputXml = outputsXml[i];
            outputs[i] = parseMessageRef(outputXml);
        }
        operation.setOutputs(outputs);

        // Related
        String relatedPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Related']";
        XmlObject[] relatedsXml = operationXml.selectPath(relatedPath);
        Interface.Operation.Property[] relateds = new Property[relatedsXml.length];
        for (int i = 0; i < relateds.length; i++) {
            XmlObject relatedObject = relatedsXml[i];
            RelatedDocument relatedXml = RelatedDocument.Factory.parse(relatedObject.getDomNode());
            relateds[i] = this.elementParser.parseInterfaceOperationProperty(relatedXml.getRelated());
        }
        operation.setRelated(relateds);

        return operation;
    }

    private Property parseMessageRef(MessageRefType messageRefXml) throws Exception {
        // Name
        String messageLabel = messageRefXml.getMessageLabel();

        Property property = new Property(new ID(messageLabel));

        // Annotation
        this.elementParser.setAnnotation(property, messageRefXml);

        // DataType
        QName element = (QName) messageRefXml.getElement();
        property.setDatatype(new STND(element.getNamespaceURI() + ":" + element.getLocalPart()));

        // Domain
        String domainPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='DomainExpr']";
        XmlObject[] domainsXml = messageRefXml.selectPath(domainPath);
        if (domainsXml.length > 0) {
            DomainExpr domainExpr = this.elementParser.parseDomainExpr(domainsXml[0]);
            property.setDomain(domainExpr);
        }

        // Auxiliary
        String auxPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Auxiliary']";
        XmlObject[] auxXml = messageRefXml.selectPath(auxPath);
        if (auxXml.length > 0) {
            // TODO verify the following snippet
            Node auxNode = auxXml[0].getDomNode();
            BoolType boolXml = BoolType.Factory.parse(auxNode);
            Object objectValue = boolXml.getObjectValue();
            property.setAuxiliary(((Boolean) objectValue).booleanValue());
        }

        return property;
    }
}
