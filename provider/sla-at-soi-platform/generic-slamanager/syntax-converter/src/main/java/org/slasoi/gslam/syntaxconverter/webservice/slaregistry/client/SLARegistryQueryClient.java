package org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.slasoi.gslam.core.negotiation.SLARegistry.IQuery;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidStateException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.syntaxconverter.SLARegistryWSDelegator;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetDependencies;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetDependenciesResponse;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetMinimumSLAInfo;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetSLA;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetSLAsByState;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetStateHistory;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryQueryStub.GetUpwardDependencies;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

public class SLARegistryQueryClient implements IQuery {

    private GSLAMSyntaxConverterWSSLARegistryQueryStub stub;
    private SLARegistryWSDelegator delegator;

    public SLARegistryQueryClient(String epr) throws AxisFault {
        super();
        this.stub = new GSLAMSyntaxConverterWSSLARegistryQueryStub(epr);
        delegator = new SLARegistryWSDelegator();
    }

    public UUID[] getDependencies(UUID id) throws InvalidUUIDException {
        GetDependencies inParam = new GetDependencies();
        inParam.setId(id.getValue());
        try {
            GetDependenciesResponse response = this.stub.getDependencies(inParam);
            String[] depStrings = response.get_return();
            UUID[] uuids = new UUID[depStrings.length];
            for (int i = 0; i < depStrings.length; i++) {
                uuids[i] = new UUID(depStrings[i]);
            }

            return uuids;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDExceptionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] ids) throws InvalidUUIDException {
        try {
            GetMinimumSLAInfo inParam = new GetMinimumSLAInfo();

            // ids
            String[] idStrings = new String[ids.length];
            for (int i = 0; i < idStrings.length; i++) {
                idStrings[i] = ids[i].getValue();
            }
            inParam.setIds(idStrings);

            String[] minInfoStrings = this.stub.getMinimumSLAInfo(inParam).get_return();
            SLAMinimumInfo[] minInfos = new SLAMinimumInfo[minInfoStrings.length];
            for (int i = 0; i < minInfos.length; i++) {
                try {
                    if (delegator != null)
                        delegator.parseSLAMinimumInfo(minInfoStrings[i]);
                    else
                        minInfos[i] = SLARegistryClientConverter.parseSLAMinimumInfo(minInfoStrings[i]);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return minInfos;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDExceptionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public SLA[] getSLA(UUID[] ids) throws InvalidUUIDException {
        GetSLA inParam = new GetSLA();

        // ids
        String[] idStrings = new String[ids.length];
        for (int i = 0; i < idStrings.length; i++) {
            idStrings[i] = ids[i].getValue();
        }
        inParam.setIds(idStrings);

        try {
            String[] slaStrings = this.stub.getSLA(inParam).get_return();
            SLA[] slas = new SLA[slaStrings.length];
            for (int i = 0; i < slas.length; i++) {
                try {
                    if (delegator != null)
                        slas[i] = this.delegator.parseSLA(slaStrings[i]);
                    else {
                        slas[i] = new SLASOIParser().parseSLA(slaStrings[i]);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return slas;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDExceptionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public UUID[] getSLAsByState(SLAState[] states, boolean inclusive) throws InvalidStateException {
        GetSLAsByState inParam = new GetSLAsByState();
        // states
        String[] stringStates = new String[states.length];
        for (int i = 0; i < stringStates.length; i++) {
            stringStates[i] = states[i].name();
        }
        inParam.setStates(stringStates);

        // inclusive
        inParam.setInclusive(inclusive);

        try {
            String[] idStrings = this.stub.getSLAsByState(inParam).get_return();
            UUID[] uuids = new UUID[idStrings.length];
            for (int i = 0; i < uuids.length; i++) {
                uuids[i] = new UUID(idStrings[i]);
            }

            return uuids;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidStateExceptionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public SLAStateInfo[] getStateHistory(UUID id, boolean current) throws InvalidUUIDException, InvalidStateException {
        GetStateHistory inParam = new GetStateHistory();
        inParam.setId(id.getValue());
        inParam.setCurrent(current);

        try {
            String[] stateInfoStrings = this.stub.getStateHistory(inParam).get_return();
            SLAStateInfo[] stateInfos = new SLAStateInfo[stateInfoStrings.length];
            for (int i = 0; i < stateInfos.length; i++) {
                if (delegator != null)
                    delegator.parseSLAStateInfo(stateInfoStrings[i]);
                else
                    stateInfos[i] = SLARegistryClientConverter.parseSLAStateInfo(stateInfoStrings[i]);
            }
            return stateInfos;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidStateExceptionException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDExceptionException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public UUID[] getUpwardDependencies(UUID id) throws InvalidUUIDException {
        GetUpwardDependencies inParam = new GetUpwardDependencies();
        inParam.setId(id.getValue());

        String[] idStrings;
        try {
            idStrings = this.stub.getUpwardDependencies(inParam).get_return();
            UUID[] uuids = new UUID[idStrings.length];
            for (int i = 0; i < uuids.length; i++) {
                uuids[i] = new UUID(idStrings[i]);
            }

            return uuids;
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InvalidUUIDExceptionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public SLA[] getSLAsByParty(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByTemplateId(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

}
