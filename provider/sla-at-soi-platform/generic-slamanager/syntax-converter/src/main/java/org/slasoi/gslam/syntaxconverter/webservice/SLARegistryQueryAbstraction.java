package org.slasoi.gslam.syntaxconverter.webservice;

import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidStateException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;

/**
 * Provides an abstraction for the SLA registry's query operation to be used as web service.
 * @author Peter A. Chronz
 *
 */
public interface SLARegistryQueryAbstraction {

    public abstract String[] getDependencies(String id) throws InvalidUUIDException;

    public abstract String[] getMinimumSLAInfo(String[] ids) throws InvalidUUIDException, Exception;

    public abstract String[] getSLA(String[] ids) throws InvalidUUIDException;

    public abstract String[] getSLAsByState(String[] states, boolean inclusive) throws InvalidStateException;

    public abstract String[] getStateHistory(String id, boolean current) throws InvalidUUIDException,
            InvalidStateException;

    public abstract String[] getUpwardDependencies(String id) throws InvalidUUIDException;

}
