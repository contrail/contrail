package org.slasoi.gslam.syntaxconverter;

import java.util.Vector;

import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.syntaxconverter.broker.Base64Utils;
import org.slasoi.gslam.syntaxconverter.broker.BrokerType;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterCall;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterProxy;
import org.slasoi.slamodel.sla.SLA;

/**
 * Delegates calls for parsing and rendering to an non-OSGi component.
 * @author Peter A. Chronz
 *
 */
public class SLARegistryWSDelegator {
    public SLARegistryWSDelegator() {
    }

    protected Object delegate(SyntaxConverterCall call, Object content) throws Exception {
        SyntaxConverterProxy proxy = new SyntaxConverterProxy();
        Vector<String> command = new Vector<String>(3, 2);
        command.add(BrokerType.SLAREGISTRY_WS.name());
        command.add(call.name());
        command.add(Base64Utils.encode(content));

        Object result = proxy.command(command);

        if (result instanceof Exception) {
            Exception er = (Exception) result;
            throw new Exception(er.toString());
        }

        return result;
    }

    // --- SLARegistry ---
    public SLAMinimumInfo parseSLAMinimumInfo(String minInfoString) throws Exception {
        return (SLAMinimumInfo) delegate(SyntaxConverterCall.SR_PARSE_SLA_MINIMUMINFO, minInfoString);
    }

    public SLAStateInfo parseSLAStateInfo(String stateInfoString) throws Exception {
        return (SLAStateInfo) delegate(SyntaxConverterCall.SR_PARSE_SLA_STATEINFO, stateInfoString);
    }

    public SLA parseSLA(String slaString) throws Exception {
        return (SLA) delegate(SyntaxConverterCall.SR_PARSE_SLA, slaString);
    }

    protected SyntaxConverterProxy proxy;
}
