/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/SyntaxConverterDelegator.java $
 */
package org.slasoi.gslam.syntaxconverter;

import java.util.Vector;

import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.syntaxconverter.broker.Base64Utils;
import org.slasoi.gslam.syntaxconverter.broker.BrokerType;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterCall;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterProxy;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Delegator Class.  It represents a delegator acting on behalf of a SyntaxConverter through the broker. The involved
 * SyntaxConverter (outside OSGi) is able to use XMLBean.
 *  
 * @author Miguel Rojas (UDO)
 *
 */
public class SyntaxConverterDelegator {
    
    /**
     * Constructor
     */
    public SyntaxConverterDelegator(SyntaxConverterType type) {
        this.type = type;
    }

    /**
     * Helper method for commands forwarding and their parameters from the SyntaxConverter towards
     * the SyntaxConverter 
     */
    protected Object delegate(SyntaxConverterCall call, Object content) throws Exception {
        if ( content == null )
            throw new NullPointerException( "SyntaxConverterBroker cannot be invoked with null parameters" );
        
        SyntaxConverterProxy proxy = new SyntaxConverterProxy();
        Vector<String> command = new Vector<String>(3, 2);
        command.add(BrokerType.SYNTAX_CONVERTER.name());
        command.add(type.name());
        command.add(call.name());
        command.add(Base64Utils.encode(content));

        Object result = proxy.command(command);

        if (result instanceof Exception) {
            Exception er = (Exception) result;
            throw new Exception(er.toString());
        }

        return result;
    }

    // --- SyntaxConverter ---
    public String renderSLA(SLA sla) throws Exception {
        return (String) delegate(SyntaxConverterCall.SLA_RENDERER_RENDER_SLA, sla);
    }

    public String renderSLATemplate(SLATemplate slaTemplate) throws Exception {
        return (String) delegate(SyntaxConverterCall.SLA_TEMPLATE_RENDERER_RENDER_SLATEMPLATE, slaTemplate);
    }

    public Object parseSLA(String slaObject) throws Exception {
        return (SLA) delegate(SyntaxConverterCall.SLA_PARSER_PARSE_SLA, slaObject);
    }

    public Object parseSLATemplate(String slaTemplateObject) throws Exception {
        return (SLATemplate) delegate(SyntaxConverterCall.SLA_TEMPLATE_PARSER_PARSE_TEMPLATE, slaTemplateObject);
    }

    public SLATemplate[] parseSLATemplates(String[] slaTemplateObjects) throws Exception {
        return (SLATemplate[]) delegate(SyntaxConverterCall.SLA_TEMPLATE_PARSER_PARSE_TEMPLATES, slaTemplateObjects);
    }

    public String renderWSDL(Interface.Specification interfaceSpec) throws Exception {
        return (String) delegate(SyntaxConverterCall.WSDL_RENDERER_RENDER_WSDL, interfaceSpec);
    }

    public Interface.Specification[] parseWSDL(String wsdlString) throws Exception {
        return (Interface.Specification[]) delegate(SyntaxConverterCall.WSDL_PARSER_PARSE_WSDL, wsdlString);
    }

    public ConstraintExpr parseConstraintExpr(String constraintString) throws Exception {
        return (ConstraintExpr) delegate(SyntaxConverterCall.HELPER_PARSE_CONSTRAINT_EXPR, constraintString);
    }

    public String renderMinimumInfo(SLAMinimumInfo minInfo) throws Exception {
        return (String) delegate(SyntaxConverterCall.HELPER_RENDER_MINIMUM_INFO, minInfo);
    }

    protected SyntaxConverterProxy proxy;
    protected SyntaxConverterType type;
}
