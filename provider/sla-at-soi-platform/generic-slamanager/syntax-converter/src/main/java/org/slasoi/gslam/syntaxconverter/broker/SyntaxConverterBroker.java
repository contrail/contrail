/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/broker/SyntaxConverterBroker.java $
 */
package org.slasoi.gslam.syntaxconverter.broker;

import java.io.FileInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Properties;

import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOIRenderer;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.gslam.syntaxconverter.SyntaxConverter;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.SLARegistryClientConverter;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.TemplateRegistryClientConverter;

/**
 * SyntaxConverterBroker represents a broker between the SyntaxConverter of any SLA-manager
 * and its internal components which use XMLBeans.  This class is a workaround for XMLBeans under OSGi.
 * All request received by this broker are forwarded to <i>SyntaxConverterBrokerChild</i> instances, 
 * so multiple requests can be handled in parallel. 
 * 
 * @see org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterBrokerChild
 *  
 * @author Miguel Rojas (UDO)
 * 
 */
public class SyntaxConverterBroker {
    
    /**
     * Constructor
     */
    public SyntaxConverterBroker() {
        init();
    }

    /**
     * Initializes the Server socket for attending requests from the SyC
     */
    protected void init() {
        try {
            context = new SyCBrokerContext();

            String path = System.getenv("SLASOI_HOME");
            String filename =
                    path + System.getProperty("file.separator")
                            + "generic-slamanager/syntax-converter/broker.properties";
            Properties props = new Properties();
            props.load(new FileInputStream(filename));

            int port = Integer.parseInt((String) props.get("port"));
            ServerSocket ss = new ServerSocket(port);

            while (true) {
                System.out.println("SyntaxConverterBroker [port=" + port + "] waiting for job ...");
                final Socket peer = ss.accept();
                Thread t = new Thread(new Runnable() {
                    public void run() {
                        SyntaxConverterBrokerChild child = new SyntaxConverterBrokerChild(peer, context);
                        child.run();
                    }
                });
                t.start();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates and starts a SyntaxConverterBroker.
     */
    public static void main(String[] args) {
        System.out.println("Starting SyntaxConverterBroker...");
        new SyntaxConverterBroker();
    }

    protected SyCBrokerContext context;

    /**
     * Inner class for holding generic-references of TemplateRegistry, SLARegistry
     * and SyntaxConverter.  
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class SyCBrokerContext {
        public SyCBrokerContext() {
            set = new SyntaxConverterSet();
            slaRegistry = new SLARegistryClientConverter();
            templateRegistry = new TemplateRegistryClientConverter();
        }

        public SyntaxConverterSet set;
        public SLARegistryClientConverter slaRegistry;
        public TemplateRegistryClientConverter templateRegistry;
    }

    public class SyntaxConverterSet extends Hashtable<SyntaxConverterType, ISyntaxConverter> {
        public SyntaxConverterSet() {
            init();
        }

        protected void init() {
            SyntaxConverter slasoiConverter = new SyntaxConverter();
            slasoiConverter.init(new SLASOIRenderer(), new SLASOITemplateRenderer(), new SLASOIParser(),
                    new SLASOITemplateParser());

            put(SyntaxConverterType.SLASOISyntaxConverter, slasoiConverter);
        }
    }
}
