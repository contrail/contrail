package org.slasoi.gslam.syntaxconverter.webservice;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlCalendar;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidStateException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.syntaxconverter.ContextKeeper;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

import eu.soi.slaAt.gslam.slaRegistry.register.SLAStateInfoDocument;

/**
 * Implements the web service for SLA registry's query interaction.
 * @author Peter A. Chronz
 *
 */
public class SLARegistryQueryImpl implements SLARegistryQueryAbstraction, ServiceFactory {

    private SLAManagerContext context;
    
    private static final Logger LOGGER = Logger.getLogger( SLARegistryQueryImpl.class );
    
    public void setContext(SLAManagerContext context) {
    	this.context = context;
    }

    public SLARegistryQueryImpl() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getDependencies(java.lang.String)
     */
    public String[] getDependencies(String id) throws InvalidUUIDException {
        try {
            UUID[] dependencies = this.context.getSLARegistry().getIQuery().getDependencies(new UUID(id));
            String[] depStrings = new String[dependencies.length];
            for (int i = 0; i < depStrings.length; i++) {
                depStrings[i] = dependencies[i].getValue();
            }
            return depStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getMinimumSLAInfo(java.lang.String[])
     */
    public String[] getMinimumSLAInfo(String[] ids) throws Exception {
        try {
            UUID[] uuids = new UUID[ids.length];
            for (int i = 0; i < uuids.length; i++) {
                uuids[i] = new UUID(ids[i]);
            }

            SLAMinimumInfo[] minInfos = this.context.getSLARegistry().getIQuery().getMinimumSLAInfo(uuids);
            String[] minInfoStrings = new String[minInfos.length];
            ISyntaxConverter synVerter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            for (int i = 0; i < minInfoStrings.length; i++) {
                minInfoStrings[i] = synVerter.renderMinInfo(minInfos[i]);
            }
            return minInfoStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getSLA(java.lang.String[])
     */
    public String[] getSLA(String[] ids) throws InvalidUUIDException {
        try {
            UUID[] uuids = new UUID[ids.length];
            for (int i = 0; i < uuids.length; i++) {
                uuids[i] = new UUID(ids[i]);
            }

            SLA[] slas = this.context.getSLARegistry().getIQuery().getSLA(uuids);
            String[] slaStrings = new String[slas.length];
            ISyntaxConverter synVerter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            for (int i = 0; i < slaStrings.length; i++) {
                slaStrings[i] = synVerter.renderSLA(slas[i]);
            }

            return slaStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getSLAsByState(java.lang.String[],
     * boolean)
     */
    public String[] getSLAsByState(String[] states, boolean inclusive) throws InvalidStateException {
        try {
            // states
            SLAState[] slaStates = new SLAState[states.length];
            for (int i = 0; i < slaStates.length; i++) {
                slaStates[i] = SLAState.valueOf(states[i]);
            }

            UUID[] uuids = this.context.getSLARegistry().getIQuery().getSLAsByState(slaStates, inclusive);
            String[] uuidStrings = new String[uuids.length];
            for (int i = 0; i < uuidStrings.length; i++) {
                uuidStrings[i] = uuids[i].getValue();
            }

            return uuidStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getStateHistory(java.lang.String,
     * boolean)
     */
    public String[] getStateHistory(String id, boolean current) throws InvalidUUIDException, InvalidStateException {
        try {
            SLAStateInfo[] stateHistory =
                    this.context.getSLARegistry().getIQuery().getStateHistory(new UUID(id), current);
            String[] stateStrings = new String[stateHistory.length];

            for (int i = 0; i < stateStrings.length; i++) {
                SLAStateInfoDocument slaStateInfo = SLAStateInfoDocument.Factory.newInstance();
                slaStateInfo.addNewSLAStateInfo();
                slaStateInfo.getSLAStateInfo().setDate(new XmlCalendar(stateHistory[i].date));
                slaStateInfo.getSLAStateInfo().setSLAState(stateHistory[i].state.toString());
                stateStrings[i] = slaStateInfo.xmlText();
            }

            return stateStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.gslam.syntaxconverter.webservice.SLARegistryQueryAbstraction#getUpwardDependencies(java.lang.String)
     */
    public String[] getUpwardDependencies(String id) throws InvalidUUIDException {
        try {
            UUID[] uuids = this.context.getSLARegistry().getIQuery().getUpwardDependencies(new UUID(id));
            String[] uuidStrings = new String[uuids.length];
            for (int i = 0; i < uuidStrings.length; i++) {
                uuidStrings[i] = uuids[i].getValue();
            }
            return uuidStrings;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        String slamId = (String) arg1.getReference().getProperty("org.apache.axis2.osgi.ws");

        this.context = ContextKeeper.getInstance().getContext(slamId);

        return this;
    }

	public void destroy(ServiceContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}

}
