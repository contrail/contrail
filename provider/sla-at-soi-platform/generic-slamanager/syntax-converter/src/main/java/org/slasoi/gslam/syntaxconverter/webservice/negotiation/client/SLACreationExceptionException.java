
/**
 * SLACreationExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.4  Built on : Dec 19, 2010 (08:18:42 CET)
 */

package org.slasoi.gslam.syntaxconverter.webservice.negotiation.client;

public class SLACreationExceptionException extends java.lang.Exception{
    
    private org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLACreationExceptionE faultMessage;

    
        public SLACreationExceptionException() {
            super("SLACreationExceptionException");
        }

        public SLACreationExceptionException(java.lang.String s) {
           super(s);
        }

        public SLACreationExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SLACreationExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLACreationExceptionE msg){
       faultMessage = msg;
    }
    
    public org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLACreationExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    