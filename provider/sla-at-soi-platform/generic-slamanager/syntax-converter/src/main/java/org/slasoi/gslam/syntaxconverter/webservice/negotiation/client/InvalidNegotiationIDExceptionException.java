
/**
 * InvalidNegotiationIDExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.4  Built on : Dec 19, 2010 (08:18:42 CET)
 */

package org.slasoi.gslam.syntaxconverter.webservice.negotiation.client;

public class InvalidNegotiationIDExceptionException extends java.lang.Exception{
    
    private org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.InvalidNegotiationIDExceptionE faultMessage;

    
        public InvalidNegotiationIDExceptionException() {
            super("InvalidNegotiationIDExceptionException");
        }

        public InvalidNegotiationIDExceptionException(java.lang.String s) {
           super(s);
        }

        public InvalidNegotiationIDExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public InvalidNegotiationIDExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.InvalidNegotiationIDExceptionE msg){
       faultMessage = msg;
    }
    
    public org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.InvalidNegotiationIDExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    