package org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.slasoi.gslam.core.negotiation.SLARegistry.IRegister;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;
import org.slasoi.gslam.syntaxconverter.SLASOIRenderer;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.Register;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.RegisterResponse;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.Update;
import org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client.GSLAMSyntaxConverterWSSLARegistryRegisterStub.UpdateResponse;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

public class SLARegistryRegisterClient implements IRegister {
    private GSLAMSyntaxConverterWSSLARegistryRegisterStub stub;

    public SLARegistryRegisterClient(String epr) throws AxisFault {
        super();
        this.stub = new GSLAMSyntaxConverterWSSLARegistryRegisterStub(epr);
    }

    public UUID register(SLA agreement, UUID[] dependencies, SLAState state) throws RegistrationFailureException {

        try {
            Register inParam = new Register();

            // state
            inParam.setState(state.name());

            // agreement
            SLASOIRenderer slasoiRenderer = new SLASOIRenderer();
            inParam.setAgreement(slasoiRenderer.renderSLA(agreement));

            // dependencies
            String[] depStrings = new String[dependencies.length];
            for (int i = 0; i < dependencies.length; i++) {
                depStrings[i] = dependencies[i].getValue();
            }
            inParam.setDependencies(depStrings);

            RegisterResponse response = this.stub.register(inParam);
            return new UUID(response.get_return());
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
        catch (RegistrationFailureExceptionException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public UUID sign(UUID slaID)
    {
        return slaID;
    }
    
    public UUID update(UUID id, SLA agreement, UUID[] dependencies, SLAState state) throws UpdateFailureException {
        Update inParam = new Update();

        try {

            // id
            inParam.setId(id.getValue());

            // agreement
            SLASOIRenderer slasoiRenderer = new SLASOIRenderer();
            String sla = slasoiRenderer.renderSLA(agreement);
            inParam.setAgreement(sla);

            // dependencies
            String[] depStrings = new String[dependencies.length];
            for (int i = 0; i < dependencies.length; i++) {
                depStrings[i] = dependencies[i].getValue();
            }
            inParam.setDependencies(depStrings);

            // state
            inParam.setState(state.name());

            UpdateResponse response = this.stub.update(inParam);
            return new UUID(response.get_return());
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (UpdateFailureExceptionException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
