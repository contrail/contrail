package org.slasoi.gslam.syntaxconverter;

import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.slamodel.core.ConstraintExpr;

import eu.slaatsoi.slamodel.AgreementPartyType;
import eu.slaatsoi.slamodel.ConstraintExprDocument;
import eu.slaatsoi.slamodel.PartyDocument;
import eu.soi.slaAt.gslam.slaRegistry.register.SLAMinimumInfoDocument;

/**
 * A frequently used helper class for parsing and rendering elements in an SLA.
 * @author Peter A. Chronz
 *
 */
public class SyntaxConverterHelper {
    
    /**
     * Parses a constraint expression from string.
     */
    public static ConstraintExpr parseConstraintExpr(String constraintString) throws Exception {
        ConstraintExprDocument constraintXml = ConstraintExprDocument.Factory.parse(constraintString);
        XmlElementParser xmlElementParser = new XmlElementParser();
        return xmlElementParser.parseConstraintExpr(constraintXml.getConstraintExpr());
    }
    
    /**
     * Renders SLA minimum info metadata to string.
     */
    public static String renderMinimumInfo(SLAMinimumInfo minInfo) {
        SLAMinimumInfoDocument slaInfoDoc = SLAMinimumInfoDocument.Factory.newInstance();

        slaInfoDoc.addNewSLAMinimumInfo();

        slaInfoDoc.getSLAMinimumInfo().setAgreedAt(minInfo.agreedAt.getValue());
        slaInfoDoc.getSLAMinimumInfo().setEffectiveFrom(minInfo.effectiveFrom.getValue());
        slaInfoDoc.getSLAMinimumInfo().setEffectiveUntil(minInfo.effectiveUntil.getValue());
        slaInfoDoc.getSLAMinimumInfo().setSLAID(minInfo.SLAID.getValue());
        slaInfoDoc.getSLAMinimumInfo().setTemplateID(minInfo.templateID.getValue());

        // Party
        XmlElementRenderer xmlElementRenderer = new XmlElementRenderer();
        PartyDocument[] partiesXml = new PartyDocument[minInfo.parties.length];
        for (int j = 0; j < minInfo.parties.length; j++) {
            AgreementPartyType partyType = xmlElementRenderer.renderParty(minInfo.parties[j]);
            PartyDocument partyDoc = PartyDocument.Factory.newInstance();
            partyDoc.setParty(partyType);
            partiesXml[j] = partyDoc;
        }
        slaInfoDoc.getSLAMinimumInfo().setPartyArray(partiesXml);

        return slaInfoDoc.xmlText();
    }
}
