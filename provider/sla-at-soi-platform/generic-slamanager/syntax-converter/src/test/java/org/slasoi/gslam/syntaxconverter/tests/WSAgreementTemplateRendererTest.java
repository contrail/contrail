package org.slasoi.gslam.syntaxconverter.tests;

import java.io.File;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.ggf.schemas.graap.x2007.x03.wsAgreement.TemplateDocument;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementTemplateParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementTemplateRenderer;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

import eu.slaatsoi.slamodel.AgreementTermDocument;
import eu.slaatsoi.slamodel.InterfaceDeclrDocument;
import eu.slaatsoi.slamodel.ModelVersionDocument;
import eu.slaatsoi.slamodel.PartyDocument;
import eu.slaatsoi.slamodel.VariableDeclrDocument;

public class WSAgreementTemplateRendererTest extends XmlRendererTest {
    public void testRenderSLA() throws Exception {
        System.out.println("H");
//        WSAgreementTemplateRenderer wsagRenderer = new WSAgreementTemplateRenderer();
//
//        // TODO get rid of the dependency of TemplateParser; serialise and can an SLA model in Java
//        TemplateDocument wsagSLATemplate =
//                TemplateDocument.Factory.parse(SLATemplateParser.class.getResourceAsStream("/WSAGTemplate.xml"));
//        WSAgreementTemplateParser slaTemplateParser = new WSAgreementTemplateParser();
//        SLATemplate slaTemplate = slaTemplateParser.parseTemplate(wsagSLATemplate.xmlText());
//
//        TemplateDocument wsagTemplate = TemplateDocument.Factory.parse(wsagRenderer.renderSLATemplate(slaTemplate));
//
//        // Location of slasoi schema
//        XmlCursor templateDocCursor = wsagTemplate.newCursor();
//        templateDocCursor.toFirstChild();
//        templateDocCursor.setAttributeText(new QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"),
//                WSAgreementTemplateRenderer.slasoiNamespace + " slasoi.xsd "
//                        + WSAgreementTemplateRenderer.wsagNamespace + " ws-agreement.xsd");
//
//        // TemplateId
//        String templateId = wsagTemplate.getTemplate().getTemplateId();
//        assertEquals("templateid", templateId);
//
//        // ModelVersion
//        String modelPath =
//                "//*[local-name()=\"ModelVersion\" and namespace-uri()=\""
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "\"]";
//        XmlObject[] modelVersionObjectsXml = wsagTemplate.selectPath(modelPath);
//        assertEquals(modelVersionObjectsXml.length, 1);
//        ModelVersionDocument modelVersionXml =
//                ModelVersionDocument.Factory.parse(modelVersionObjectsXml[0].getDomNode());
//        assertEquals(modelVersionXml.getModelVersion(), SLA.$model_version);
//
//        // Party
//        // /wsag:Template/wsag:Context/slasoi:Party
//        String partyPath =
//                "/*[local-name()=\"Template\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']/*[local-name()=\"Context\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']/*[local-name()=\"Party\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] partyObjectsXml = wsagTemplate.selectPath(partyPath);
//        assertEquals(partyObjectsXml.length, 2);
//        for (int i = 0; i < partyObjectsXml.length; i++) {
//            XmlObject partyObjectXml = partyObjectsXml[i];
//            PartyDocument partyXml = PartyDocument.Factory.parse(partyObjectXml.getDomNode());
//            testParty(partyXml.getParty());
//        }
//
//        // InterfaceDeclr
//        // /wsag:Template/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm/slasoi:InterfaceDeclr
//        String interfaceDeclrPath =
//                "/*[local-name()=\"Template\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"Terms\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"All\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"ServiceDescriptionTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"InterfaceDeclr\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] interfaceDeclrObjectsXml = wsagTemplate.selectPath(interfaceDeclrPath);
//        assertEquals(interfaceDeclrObjectsXml.length, 2);
//        for (int i = 0; i < interfaceDeclrObjectsXml.length; i++) {
//            XmlObject iFaceDeclrObjectXml = interfaceDeclrObjectsXml[i];
//            InterfaceDeclrDocument iFaceDeclrXml =
//                    InterfaceDeclrDocument.Factory.parse(iFaceDeclrObjectXml.getDomNode());
//            testInterfaceDeclr(iFaceDeclrXml.getInterfaceDeclr());
//        }
//
//        // VariableDeclr
//        // /wsag:Template/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm/slasoi:VariableDeclr
//        String variableDeclrPath =
//                "/*[local-name()=\"Template\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"Terms\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"All\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"ServiceDescriptionTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"VariableDeclr\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] variableDeclrObjectsXml = wsagTemplate.selectPath(variableDeclrPath);
//        assertEquals(variableDeclrObjectsXml.length, 2);
//        for (int i = 0; i < variableDeclrObjectsXml.length; i++) {
//            XmlObject variableDeclrObjectXml = variableDeclrObjectsXml[i];
//            VariableDeclrDocument variableDeclrXml =
//                    VariableDeclrDocument.Factory.parse(variableDeclrObjectXml.getDomNode());
//            testVariableDeclr(variableDeclrXml.getVariableDeclr());
//        }
//
//        // AgreementTerm
//        // /wsag:Template/wsag:Terms/wsag:All/wsag:GuaranteeTerm/wsag:ServiceLevelObjective/slasoi:AgreementTerm
//        String agreementTermPath =
//                "/*[local-name()=\"Template\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"Terms\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"All\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"GuaranteeTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"ServiceLevelObjective\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"CustomServiceLevel\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"AgreementTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] agreementTermObjectsXml = wsagTemplate.selectPath(agreementTermPath);
//        assertEquals(agreementTermObjectsXml.length, 2);
//        for (int i = 0; i < agreementTermObjectsXml.length; i++) {
//            XmlObject agreementTermObjectXml = agreementTermObjectsXml[1];
//            AgreementTermDocument agreementTermXml =
//                    AgreementTermDocument.Factory.parse(agreementTermObjectXml.getDomNode());
//            testAgreementTerm(agreementTermXml.getAgreementTerm());
//        }
//
//        // XMLSchema compliance
//        // boolean valid = wsagTemplate.validate();
//        wsagTemplate.save(new File("WSAGTemplateRendering.xml"));
//        // TODO currently fails since slasoi schema is not loaded in xmlbeans and xs:any is processed
//        // as strict in WS-Agreement. The document itself is valid.
//        // assertTrue(valid);
    }
}
