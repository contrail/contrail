package org.slasoi.gslam.syntaxconverter.tests;

import java.io.File;
import java.util.Calendar;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCalendar;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.ggf.schemas.graap.x2007.x03.wsAgreement.AgreementOfferDocument;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementOfferParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementOfferRenderer;
import org.slasoi.gslam.syntaxconverter.WSAgreementTemplateRenderer;
import org.slasoi.slamodel.sla.SLA;

import eu.slaatsoi.slamodel.AgreedAtDocument;
import eu.slaatsoi.slamodel.AgreementTermDocument;
import eu.slaatsoi.slamodel.EffectiveFromDocument;
import eu.slaatsoi.slamodel.EffectiveUntilDocument;
import eu.slaatsoi.slamodel.InterfaceDeclrDocument;
import eu.slaatsoi.slamodel.ModelVersionDocument;
import eu.slaatsoi.slamodel.PartyDocument;
import eu.slaatsoi.slamodel.VariableDeclrDocument;

public class WSAgreementOfferRendererTest extends XmlRendererTest {
    public void testRenderSLA() throws Exception {
        System.out.println("Hello");
//        WSAgreementOfferRenderer wsagRenderer = new WSAgreementOfferRenderer();
//
//        // TODO get rid of the dependency of TemplateParser; serialise and can an SLA model in Java
//        AgreementOfferDocument wsagSLA =
//                AgreementOfferDocument.Factory.parse(SLATemplateParser.class.getResourceAsStream("/WSAGOffer.xml"));
//        WSAgreementOfferParser slaOfferParser = new WSAgreementOfferParser();
//        SLA sla = slaOfferParser.parseSLA(wsagSLA.xmlText());
//
//        AgreementOfferDocument wsagRendered = AgreementOfferDocument.Factory.parse(wsagRenderer.renderSLA(sla));
//
//        // Location of slasoi schema
//        XmlCursor templateDocCursor = wsagRendered.newCursor();
//        templateDocCursor.toFirstChild();
//        templateDocCursor.setAttributeText(new QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"),
//                WSAgreementTemplateRenderer.slasoiNamespace + " slasoi.xsd "
//                        + WSAgreementTemplateRenderer.wsagNamespace + " ws-agreement.xsd");
//
//        // AgreementId
//        String agreementId = wsagRendered.getAgreementOffer().getAgreementId();
//        assertEquals("id", agreementId);
//
//        // TemplateId
//        String templateId = wsagRendered.getAgreementOffer().getContext().getTemplateId();
//        assertEquals("id", templateId);
//
//        // ModelVersion
//        String modelPath =
//                "//*[local-name()=\"ModelVersion\" and namespace-uri()=\""
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "\"]";
//        XmlObject[] modelVersionObjectsXml = wsagRendered.selectPath(modelPath);
//        assertEquals(modelVersionObjectsXml.length, 1);
//        ModelVersionDocument modelVersionXml =
//                ModelVersionDocument.Factory.parse(modelVersionObjectsXml[0].getDomNode());
//        assertEquals(modelVersionXml.getModelVersion(), SLA.$model_version);
//
//        // AgreedAt
//        String agreedAtPath =
//                "//*[local-name()=\"Context\" and namespace-uri()=\"" + WSAgreementOfferRenderer.wsagNamespace + "\"]"
//                        + "/*[local-name()=\"AgreedAt\" and namespace-uri()=\""
//                        + WSAgreementOfferRenderer.slasoiNamespace + "\"]";
//        XmlObject[] agreedAtObjectsXml = wsagRendered.selectPath(agreedAtPath);
//        assertEquals(agreedAtObjectsXml.length, 1);
//        AgreedAtDocument agreedAtXml = AgreedAtDocument.Factory.parse(agreedAtObjectsXml[0].getDomNode());
//        Calendar agreedAt = agreedAtXml.getAgreedAt();
//        XmlCalendar agreedAtCal = new XmlCalendar(agreedAt.getTime());
//        assertEquals("2002-05-30T09:00:00.000+02:00", agreedAtCal.toString());
//
//        // EffectiveFrom
//        String effectiveFromPath =
//                "//*[local-name()=\"Context\" and namespace-uri()=\"" + WSAgreementOfferRenderer.wsagNamespace + "\"]"
//                        + "/*[local-name()=\"EffectiveFrom\" and namespace-uri()=\""
//                        + WSAgreementOfferRenderer.slasoiNamespace + "\"]";
//        XmlObject[] effectiveFromObjectsXml = wsagRendered.selectPath(effectiveFromPath);
//        assertEquals(effectiveFromObjectsXml.length, 1);
//        EffectiveFromDocument effectiveFromXml =
//                EffectiveFromDocument.Factory.parse(effectiveFromObjectsXml[0].getDomNode());
//        Calendar effectiveFrom = effectiveFromXml.getEffectiveFrom();
//        XmlCalendar effectiveFromCal = new XmlCalendar(effectiveFrom.getTime());
//        assertEquals("2002-05-30T09:00:00.000+02:00", effectiveFromCal.toString());
//
//        // EffectiveUntil
//        String effectiveUntilPath =
//                "//*[local-name()=\"Context\" and namespace-uri()=\"" + WSAgreementOfferRenderer.wsagNamespace + "\"]"
//                        + "/*[local-name()=\"EffectiveUntil\" and namespace-uri()=\""
//                        + WSAgreementOfferRenderer.slasoiNamespace + "\"]";
//        XmlObject[] effectiveUntilObjectsXml = wsagRendered.selectPath(effectiveUntilPath);
//        assertEquals(effectiveUntilObjectsXml.length, 1);
//        EffectiveUntilDocument effectiveUntilXml =
//                EffectiveUntilDocument.Factory.parse(effectiveUntilObjectsXml[0].getDomNode());
//        Calendar effectiveUntil = effectiveUntilXml.getEffectiveUntil();
//        XmlCalendar effectiveUntilCal = new XmlCalendar(effectiveUntil.getTime());
//        assertEquals("2002-05-30T09:00:00.000+02:00", effectiveUntilCal.toString());
//
//        // Party
//        // /wsag:AgreementOffer/wsag:Context/slasoi:Party
//        String partyPath =
//                "/*[local-name()=\"AgreementOffer\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']/*[local-name()=\"Context\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']/*[local-name()=\"Party\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] partyObjectsXml = wsagRendered.selectPath(partyPath);
//        assertEquals(partyObjectsXml.length, 2);
//        for (int i = 0; i < partyObjectsXml.length; i++) {
//            XmlObject partyObjectXml = partyObjectsXml[i];
//            PartyDocument partyXml = PartyDocument.Factory.parse(partyObjectXml.getDomNode());
//            testParty(partyXml.getParty());
//        }
//
//        // InterfaceDeclr
//        // /wsag:AgreementOffer/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm/slasoi:InterfaceDeclr
//        String interfaceDeclrPath =
//                "/*[local-name()=\"AgreementOffer\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"Terms\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"All\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"ServiceDescriptionTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"InterfaceDeclr\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] interfaceDeclrObjectsXml = wsagRendered.selectPath(interfaceDeclrPath);
//        assertEquals(interfaceDeclrObjectsXml.length, 2);
//        for (int i = 0; i < interfaceDeclrObjectsXml.length; i++) {
//            XmlObject iFaceDeclrObjectXml = interfaceDeclrObjectsXml[i];
//            InterfaceDeclrDocument iFaceDeclrXml =
//                    InterfaceDeclrDocument.Factory.parse(iFaceDeclrObjectXml.getDomNode());
//            testInterfaceDeclr(iFaceDeclrXml.getInterfaceDeclr());
//        }
//
//        // VariableDeclr
//        // /wsag:AgreementOffer/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm/slasoi:VariableDeclr
//        String variableDeclrPath =
//                "/*[local-name()=\"AgreementOffer\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"Terms\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"All\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"ServiceDescriptionTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"VariableDeclr\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] variableDeclrObjectsXml = wsagRendered.selectPath(variableDeclrPath);
//        assertEquals(variableDeclrObjectsXml.length, 2);
//        for (int i = 0; i < variableDeclrObjectsXml.length; i++) {
//            XmlObject variableDeclrObjectXml = variableDeclrObjectsXml[i];
//            VariableDeclrDocument variableDeclrXml =
//                    VariableDeclrDocument.Factory.parse(variableDeclrObjectXml.getDomNode());
//            testVariableDeclr(variableDeclrXml.getVariableDeclr());
//        }
//
//        // AgreementTerm
//        // /wsag:AgreementOffer/wsag:Terms/wsag:All/wsag:GuaranteeTerm/wsag:ServiceLevelObjective/slasoi:AgreementTerm
//        String agreementTermPath =
//                "/*[local-name()=\"AgreementOffer\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"Terms\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"All\" and namespace-uri()='" + WSAgreementTemplateRenderer.wsagNamespace
//                        + "']" + "/*[local-name()=\"GuaranteeTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"ServiceLevelObjective\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"CustomServiceLevel\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.wsagNamespace + "']"
//                        + "/*[local-name()=\"AgreementTerm\" and namespace-uri()='"
//                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
//        XmlObject[] agreementTermObjectsXml = wsagRendered.selectPath(agreementTermPath);
//        assertEquals(agreementTermObjectsXml.length, 2);
//        for (int i = 0; i < agreementTermObjectsXml.length; i++) {
//            XmlObject agreementTermObjectXml = agreementTermObjectsXml[1];
//            AgreementTermDocument agreementTermXml =
//                    AgreementTermDocument.Factory.parse(agreementTermObjectXml.getDomNode());
//            testAgreementTerm(agreementTermXml.getAgreementTerm());
//        }
//
//        // XMLSchema compliance
//        // boolean valid = wsagTemplate.validate();
//        wsagRendered.save(new File("WSAGOfferRendering.xml"));
//        // TODO currently fails since slasoi schema is not loaded in xmlbeans and xs:any is processed
//        // as strict in WS-Agreement. The document itself is valid.
//        // assertTrue(valid);
    }
}
