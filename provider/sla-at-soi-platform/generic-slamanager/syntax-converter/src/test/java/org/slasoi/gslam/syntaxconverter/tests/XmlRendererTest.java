package org.slasoi.gslam.syntaxconverter.tests;

import junit.framework.TestCase;

import org.apache.xmlbeans.XmlObject;
import org.slasoi.gslam.syntaxconverter.WSAgreementTemplateRenderer;

import eu.slaatsoi.slamodel.AgreementPartyType;
import eu.slaatsoi.slamodel.AgreementTermType;
import eu.slaatsoi.slamodel.CONSTType;
import eu.slaatsoi.slamodel.CompoundConstraintExprType;
import eu.slaatsoi.slamodel.CompoundDomainExprType;
import eu.slaatsoi.slamodel.ConstraintExprType;
import eu.slaatsoi.slamodel.DomainExprType;
import eu.slaatsoi.slamodel.EndpointType;
import eu.slaatsoi.slamodel.EventExprType;
import eu.slaatsoi.slamodel.ExprType;
import eu.slaatsoi.slamodel.GuaranteedActionDefnType;
import eu.slaatsoi.slamodel.GuaranteedActionType;
import eu.slaatsoi.slamodel.GuaranteedStateType;
import eu.slaatsoi.slamodel.GuaranteedType;
import eu.slaatsoi.slamodel.IdValueExprEntryType;
import eu.slaatsoi.slamodel.InterfaceDeclrType;
import eu.slaatsoi.slamodel.InterfaceOperationPropertyType;
import eu.slaatsoi.slamodel.InterfaceOperationType;
import eu.slaatsoi.slamodel.InterfaceRefType;
import eu.slaatsoi.slamodel.InterfaceSpecType;
import eu.slaatsoi.slamodel.InterfaceType;
import eu.slaatsoi.slamodel.InvocationType;
import eu.slaatsoi.slamodel.MapIdValueExpr;
import eu.slaatsoi.slamodel.OperativeType;
import eu.slaatsoi.slamodel.SimpleDomainExprType;
import eu.slaatsoi.slamodel.TypeConstraintExprType;
import eu.slaatsoi.slamodel.ValueExprType;
import eu.slaatsoi.slamodel.VariableDeclrType;

public abstract class XmlRendererTest extends TestCase {
    protected void testAgreementTerm(AgreementTermType agreementTermXml) {
        // Annotation
        testAnnotation(agreementTermXml);

        // ID
        String id = agreementTermXml.getID();
        assertEquals(id, "id");

        // Precondition
        ConstraintExprType precondition = agreementTermXml.getPrecondition();
        testConstraintExpr(precondition);

        // VariableDeclr
        VariableDeclrType[] variableDeclrs = agreementTermXml.getVariableDeclrArray();
        assertEquals(variableDeclrs.length, 1);
        for (int i = 0; i < variableDeclrs.length; i++) {
            testVariableDeclr(variableDeclrs[i]);
        }

        // Guaranteed
        GuaranteedType[] guaranteeds = agreementTermXml.getGuaranteedArray();
        assertEquals(guaranteeds.length, 2);
        for (int i = 0; i < guaranteeds.length; i++) {
            GuaranteedType guaranteed = guaranteeds[i];
            testGuaranteed(guaranteed);
        }
    }

    protected void testGuaranteed(GuaranteedType guaranteed) {
        // Annotation
        testAnnotation(guaranteed);

        if (guaranteed.isSetAction()) {
            testGuaranteedAction(guaranteed.getAction());
        }
        else if (guaranteed.isSetState()) {
            testGuaranteedState(guaranteed.getState());
        }
    }

    protected void testGuaranteedState(GuaranteedStateType state) {
        // Priority
        assertEquals(state.getPriority().getValue(), "priority");
        assertEquals(state.getPriority().getDatatype(), "prioritytype");

        // ID
        assertEquals(state.getID(), "id");

        // State
        ConstraintExprType constraint = state.getConstraint();
        testConstraintExpr(constraint);
    }

    protected void testConstraintExpr(ConstraintExprType constraint) {
        // identify the type
        if (constraint.isSetCompoundConstraintExpr()) {
            testCompoundConstraintExpr(constraint.getCompoundConstraintExpr());
        }
        else if (constraint.isSetTypeConstraintExpr()) {
            testTypeConstraintExpr(constraint.getTypeConstraintExpr());
        }
    }

    protected void testTypeConstraintExpr(TypeConstraintExprType typeConstraintExpr) {
        // Error
        assertEquals(typeConstraintExpr.getError().getValue(), "error");
        assertEquals(typeConstraintExpr.getError().getDatatype(), "errortype");

        // Domain
        DomainExprType domain = typeConstraintExpr.getDomain();
        testDomainExpr(domain);

        // Value
        ValueExprType value = typeConstraintExpr.getValue();
        testValueExpr(value);
    }

    protected void testValueExpr(ValueExprType value) {
        // identify the type
        if (value.isSetBOOL()) {
            assertEquals(value.getBOOL(), false);
        }
        else if (value.isSetCONST()) {
            fail();
        }
        else if (value.isSetDomainExpr()) {
            fail();
        }
        else if (value.isSetEventExpr()) {
            fail();
        }
        else if (value.isSetFuncExpr()) {
            fail();
        }
        else if (value.isSetID()) {
            fail();
        }
        else if (value.isSetTIME()) {
            fail();
        }
        else {
            fail();
        }
    }

    protected void testDomainExpr(DomainExprType domain) {
        // identify the type
        if (domain.isSetCompoundDomainExpr()) {
            testCompoundDomainExpr(domain.getCompoundDomainExpr());
        }
        else if (domain.isSetSimpleDomainExpr()) {
            testSimpleDomainExpr(domain.getSimpleDomainExpr());
        }
        else {
            fail();
        }
    }

    protected void testSimpleDomainExpr(SimpleDomainExprType simpleDomainExpr) {
        // Value
        ValueExprType value = simpleDomainExpr.getValue();
        testValueExpr(value);

        // Comparison Operator
        String comparisonOp = simpleDomainExpr.getComparisonOp();
        assertEquals(comparisonOp, "comparisonOp");
    }

    protected void testCompoundDomainExpr(CompoundDomainExprType compoundDomainExpr) {
        // Logical Opertor
        String binaryLogicalOp = compoundDomainExpr.getLogicalOp();
        assertEquals(binaryLogicalOp, "and");

        // Sub-Expressions
        DomainExprType[] binarySubexpressionArray = compoundDomainExpr.getSubexpressionArray();
        assertEquals(binarySubexpressionArray.length, 1);
        for (int i = 0; i < binarySubexpressionArray.length; i++) {
            DomainExprType domainExpr = binarySubexpressionArray[i];
            testDomainExpr(domainExpr);
        }
    }

    protected void testCompoundConstraintExpr(CompoundConstraintExprType compoundConstraint) {
        // Logical Operator
        String binaryLogicalOp = compoundConstraint.getLogicalOp();
        assertEquals(binaryLogicalOp, "and");

        // Sub-Expressions
        ConstraintExprType[] binarySubexpressionArray = compoundConstraint.getSubexpressionArray();
        assertEquals(binarySubexpressionArray.length, 1);
        for (int i = 0; i < binarySubexpressionArray.length; i++) {
            ConstraintExprType constraintExpr = binarySubexpressionArray[i];
            testConstraintExpr(constraintExpr);
        }
    }

    protected void testGuaranteedAction(GuaranteedActionType action) {
        // Policy
        String policy = action.getPolicy();
        assertEquals(policy, "policy");

        // Precondition
        EventExprType precondition = action.getPrecondition();
        testEventExpr(precondition);

        // Postcondition
        GuaranteedActionDefnType postcondition = action.getPostcondition();
        testGuaranteedActionDefn(postcondition);
    }

    protected void testGuaranteedActionDefn(GuaranteedActionDefnType actionDefn) {
        // identify the type
        if (actionDefn.isSetCompoundAction()) {
            fail();
        }
        else if (actionDefn.isSetCustomAction()) {
            fail();
        }
        else if (actionDefn.isSetInvocation()) {
            InvocationType invocation = actionDefn.getInvocation();
            testInvocation(invocation);
        }
        else {
            fail();
        }
    }

    protected void testInvocation(InvocationType invocation) {
        // Annotation
//        testAnnotation(invocation);

        // Endpoint
        String endpoint = invocation.getEndpoint();
        assertEquals(endpoint, "endpoint");

        // Operation
        String operation = invocation.getOperation();
        assertEquals(operation, "operation");

        // Parameters
        MapIdValueExpr parameters = invocation.getParameters();
        IdValueExprEntryType[] entries = parameters.getEntryArray();
        for (int i = 0; i < entries.length; i++) {
            // Key
            String key = entries[i].getKey();
            assertTrue(key.startsWith("Key"));

            // Value
            ValueExprType value = entries[i].getValue();
            testValueExpr(value);
        }
    }

    protected void testEventExpr(EventExprType eventExpr) {
        // Annotation
        testAnnotation(eventExpr);

        // Operator
        String operator = eventExpr.getOperator();
        assertEquals(operator, "operator");

        // Parameters
        ExprType[] parameters = eventExpr.getParameterArray();
        assertEquals(parameters.length, 1);
        for (int i = 0; i < parameters.length; i++) {
            assertTrue(parameters[i] instanceof ExprType);
            testExpr((ExprType) parameters[i]);
        }
    }

    protected void testAnnotation(XmlObject annotated) {
        // Description
        // slasoi:Text
        String descPath =
                "*[local-name()=\"Text\" and namespace-uri()=\"" + WSAgreementTemplateRenderer.slasoiNamespace + "\"]";
        XmlObject[] descObjectsXml = annotated.selectPath(descPath);
        assertEquals(1, descObjectsXml.length);
        String textValue = descObjectsXml[0].newCursor().getTextValue();
        assertEquals(textValue, "AnnotatedText");

        // Properties
        // slasoi:Properties/slasoi:Entry
        String propertyPath =
                "*[local-name()=\"Properties\" and namespace-uri()='" + WSAgreementTemplateRenderer.slasoiNamespace
                        + "']" + "/*[local-name()=\"Entry\" and namespace-uri()='"
                        + WSAgreementTemplateRenderer.slasoiNamespace + "']";
        XmlObject[] entries = annotated.selectPath(propertyPath);
        assertEquals(entries.length, 2);
        for (int i = 0; i < entries.length; i++) {
            XmlObject entry = entries[i];
            // slasoi:Key
            XmlObject[] keys =
                    entry.selectPath("*[local-name()=\"Key\" and namespace-uri()='"
                            + WSAgreementTemplateRenderer.slasoiNamespace + "']");
            assertEquals(keys.length, 1);
            String key = keys[0].newCursor().getTextValue();
            // cannot check number since the java implementation of the sla model reorders the sequence when adding
            // entries
            assertTrue(key.startsWith("Key"));

            // slasoi:Value/pca:AnyElement/pca:AnyElement
            XmlObject[] values =
                    entry.selectPath("*[local-name()=\"Value\" and namespace-uri()='"
                            + WSAgreementTemplateRenderer.slasoiNamespace + "']"
                            + "/*[local-name()=\"AnyElement\" and namespace-uri()='"
                            + "http://peter.chronz.net/AnyTypeYouLike" + "']"
                            + "/*[local-name()=\"AnyElement\" and namespace-uri()='"
                            + "http://peter.chronz.net/AnyTypeYouLike" + "']");
            if (values.length == 1) {
                String value = values[0].newCursor().getTextValue();
                // cannot check number since the java implementation of the sla model reorders the sequence when adding
                // entries
                assertTrue(value.startsWith("Value"));
            }
            else {
                // slasoi:Value
                values =
                        entry.selectPath("*[local-name()=\"Value\" and namespace-uri()='"
                                + WSAgreementTemplateRenderer.slasoiNamespace + "']");
                if (values.length == 1) {
                    String value = values[0].newCursor().getTextValue();
                    // cannot check number since the java implementation of the sla model reorders the sequence when
                    // adding entries
                    assertTrue(value.startsWith("Value"));
                }
                else {
                    fail();
                }
            }
        }
    }

    protected void testVariableDeclr(VariableDeclrType variableDeclr) {
        // Annotation
        testAnnotation(variableDeclr);

        // Var
        String var = variableDeclr.getVar();
        assertEquals(var, "var");

        // identify the type
        if (variableDeclr.isSetCustomisable()) {
            // Value
            CONSTType constValue = variableDeclr.getCustomisable().getValue();
            assertEquals(constValue.getValue(), "value");
            assertEquals(constValue.getDatatype(), "datatype");

            // Expr
            DomainExprType expr = variableDeclr.getCustomisable().getExpr();
            testDomainExpr(expr);
        }
        else {
            // Expr
            ExprType expr = variableDeclr.getExpr();
            testExpr(expr);
        }
    }

    protected void testExpr(ExprType expr) {
        if (expr.isSetConstraintExpr()) {
            testConstraintExpr(expr.getConstraintExpr());
        }
        else if (expr.isSetValueExpr()) {
            testValueExpr(expr.getValueExpr());
        }
    }

    protected void testInterfaceDeclr(InterfaceDeclrType iFaceDeclrXml) {
        // Annotation
        testAnnotation(iFaceDeclrXml);

        // ID
        String id = iFaceDeclrXml.getID();
        assertEquals(id, "id");

        // ProviderRef
        Object providerRef = iFaceDeclrXml.getProviderRef();
        assertEquals(providerRef.toString(), "providerref");

        // Endpoint
        EndpointType[] endpoints = iFaceDeclrXml.getEndpointArray();
        assertEquals(endpoints.length, 2);
        for (int i = 0; i < endpoints.length; i++) {
            EndpointType endpoint = endpoints[i];
            testEndpoint(endpoint);
        }

        // Interface
        InterfaceType iFace = iFaceDeclrXml.getInterface();
        testInterface(iFace);
    }

    protected void testInterface(InterfaceType iFace) {
        // identify the type
        if (iFace.isSetInterfaceRef()) {
            InterfaceRefType interfaceRef = iFace.getInterfaceRef();
            testInterfaceRef(interfaceRef);
        }
        else if (iFace.isSetInterfaceSpec()) {
            InterfaceSpecType interfaceSpec = iFace.getInterfaceSpec();
            testInterfaceSpec(interfaceSpec);
        }
        else {
            fail();
        }
    }

    protected void testInterfaceSpec(InterfaceSpecType interfaceSpec) {
        // Name
        String name = interfaceSpec.getName();
        assertEquals(name, "name");

        // Extended
        String[] extendeds = interfaceSpec.getExtendedArray();
        assertEquals(extendeds.length, 2);
        for (int i = 0; i < extendeds.length; i++) {
            String extended = extendeds[i];
            assertEquals(extended, "extended" + (i + 1));
        }

        // Operation
        InterfaceOperationType[] operations = interfaceSpec.getOperationArray();
        assertEquals(operations.length, 2);
        for (int i = 0; i < operations.length; i++) {
            testOperation(operations[i]);
        }
    }

    protected void testOperation(InterfaceOperationType interfaceOperation) {
        // Annotation
        testAnnotation(interfaceOperation);

        // Name
        String name = interfaceOperation.getName();
        assertEquals(name, "name");

        // Fault
        String[] faults = interfaceOperation.getFaultArray();
        assertEquals(faults.length, 1);
        for (int i = 0; i < faults.length; i++) {
            String fault = faults[i];
            assertEquals(fault, "fault");
        }

        // Input
        InterfaceOperationPropertyType[] inputs = interfaceOperation.getInputArray();
        assertEquals(inputs.length, 2);
        for (int i = 0; i < inputs.length; i++) {
            InterfaceOperationPropertyType input = inputs[i];
            testOperationProperty(input);
        }

        // Output
        InterfaceOperationPropertyType[] outputs = interfaceOperation.getOutputArray();
        assertEquals(outputs.length, 2);
        for (int i = 0; i < outputs.length; i++) {
            InterfaceOperationPropertyType output = outputs[i];
            testOperationProperty(output);
        }

        // Related
        InterfaceOperationPropertyType[] related = interfaceOperation.getRelatedArray();
        assertEquals(related.length, 2);
        for (int i = 0; i < related.length; i++) {
            InterfaceOperationPropertyType relatedOne = related[i];
            testOperationProperty(relatedOne);
        }
    }

    protected void testOperationProperty(InterfaceOperationPropertyType property) {
        // Annotation
        testAnnotation(property);

        // Name
        String name = property.getName();
        assertEquals(name, "name");

        // Auxiliary
        assertEquals(property.getAuxiliary(), false);

        // Domain
        DomainExprType domain = property.getDomain();
        testDomainExpr(domain);

        // Datatype/Unit
        String datatype = property.getDatatype();
        assertEquals(datatype, "joule");
    }

    protected void testInterfaceRef(InterfaceRefType interfaceRef) {
        // Location
        String interfaceLocation = interfaceRef.getInterfaceLocation();
        assertEquals(interfaceLocation, "42");
    }

    protected void testEndpoint(EndpointType endpoint) {
        // Annotation
        testAnnotation(endpoint);

        // ID
        String id = endpoint.getID();
        assertEquals(id, "id");

        // Location
        String location = endpoint.getLocation();
        assertEquals(location, "location");

        // Protocol
        Object protocol = endpoint.getProtocol();
        assertEquals(protocol, "protocol");
    }

    protected void testParty(AgreementPartyType party) {
        // Annotation
        testAnnotation(party);

        // Id
        String id = party.getID();
        assertEquals(id, "id");

        // AgreementRole
        String role = party.getRole();
        assertEquals(role, "role");

        // Operative
        OperativeType[] operatives = party.getOperativeArray();
        assertEquals(operatives.length, 1);
        for (int i = 0; i < operatives.length; i++) {
            testOperative(operatives[i]);
        }
    }

    protected void testOperative(OperativeType operative) {
        // Annotation
        testAnnotation(operative);

        // ID
        assertEquals(operative.getID(), "id");
    }
}
