package org.slasoi.gslam.syntaxconverter.tests;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import junit.framework.TestCase;

import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.bnf;

import eu.slaatsoi.slamodel.SLADocument;
import eu.slaatsoi.slamodel.SLATemplateDocument;

public class UseCasesTests extends TestCase {
	// public void testOrcSlat() throws Exception {
	// SLATemplateDocument slaXml;
	// slaXml =
	// SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class.getResourceAsStream("/ORC_SLAT.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos = new DataOutputStream(new FileOutputStream(new
	// File("ORC_SLAT.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }

	// public void testErpSla() throws Exception {
	// SLATemplateDocument slaXml;
	// slaXml =
	// SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class.getResourceAsStream("/ERP_SLAT.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos = new DataOutputStream(new FileOutputStream(new
	// File("ERP_SLAT.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }

	// public void testErpBusinessSlat() throws Exception {
	// SLATemplateDocument slaXml;
	// slaXml =
	// SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class
	// .getResourceAsStream("/ERP-Business-SLAT.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos = new DataOutputStream(new FileOutputStream(new
	// File("ERP-Business-SLAT.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }

	public void testA4Slat() throws Exception {
		SLATemplateDocument slaXml;
		slaXml = SLATemplateDocument.Factory
				.parse(SLASOITemplateParserTest.class
						.getResourceAsStream("/A4_SLATemplate.xml"));
		SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
		SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
		String htmlRendering = bnf.render(slaTemplate, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("A4_SLAT.html")));
		dos.writeChars(htmlRendering);
		dos.close();
	}

	public void testA4Sla() throws Exception {
		SLADocument slaXml;
		slaXml = SLADocument.Factory.parse(SLASOIParserTest.class
				.getResourceAsStream("/A4_SLA.xml"));
		SLASOIParser slasoiParser = new SLASOIParser();
		SLA sla = slasoiParser.parseSLA(slaXml.xmlText());
		String htmlRendering = bnf.render(sla, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("A4_SLA.html")));
		dos.writeChars(htmlRendering);
		dos.close();
	}

	public void testB6Sla() throws Exception {
		SLADocument slaXml;
		slaXml = SLADocument.Factory.parse(SLASOIParserTest.class
				.getResourceAsStream("/B6SLA.xml"));
		SLASOIParser slasoiParser = new SLASOIParser();
		SLA sla = slasoiParser.parseSLA(slaXml.xmlText());
		
		String htmlRendering = bnf.render(sla, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("B6SLA.html")));
		dos.writeChars(htmlRendering);
		dos.close();
		
		// WRONG
		dos = new DataOutputStream(new FileOutputStream(
				new File("B6SLA.xml")));
		dos.writeChars(slaXml.xmlText());
		dos.close();
		
		// RIGHT
		dos = new DataOutputStream(new FileOutputStream(
				new File("B6SLA.xml")));
		dos.write(slaXml.xmlText().getBytes());
		dos.close();
	}

	
	// public void testORCBusinessSlat() throws Exception {
	// SLATemplateDocument slaXml;
	// slaXml =
	// SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class
	// .getResourceAsStream("/ORC_BUSINESS_SLAT.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos = new DataOutputStream(new FileOutputStream(new
	// File("ORC_BUSINESS_SLAT.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }

	public void testORCGoldSla() throws Exception {
		SLADocument slaXml;
		slaXml = SLADocument.Factory.parse(SLASOIParserTest.class
				.getResourceAsStream("/ORCInfrastructure_SLAGold.xml"));
		SLASOIParser slasoiParser = new SLASOIParser();
		SLA sla = slasoiParser.parseSLA(slaXml.xmlText());
		String htmlRendering = bnf.render(sla, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("ORCInfrastructure_SLAGold.html")));
		dos.writeChars(htmlRendering);
		dos.close();
	}

	public void testB4Slat() throws Exception {
		SLATemplateDocument slaXml;
		slaXml = SLATemplateDocument.Factory
				.parse(SLASOITemplateParserTest.class
						.getResourceAsStream("/B4_SLATemplate.xml"));
		SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
		SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
		String htmlRendering = bnf.render(slaTemplate, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("B4_SLAT.html")));
		dos.writeChars(htmlRendering);
		dos.close();
	}

	// public void testORCSoftwareSlat() throws Exception {
	// SLATemplateDocument slaXml;
	// slaXml =
	// SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class
	// .getResourceAsStream("/ORCSoftware-SLAT_HighLoadSlow.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos =
	// new DataOutputStream(new FileOutputStream(new
	// File("ORCSoftware-SLAT_HighLoadSlow.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }

	public void testORCSoftwareHighLoadSlat() throws Exception {
		SLATemplateDocument slaXml;
		slaXml = SLATemplateDocument.Factory
				.parse(SLASOITemplateParserTest.class
						.getResourceAsStream("/ORCSoftware-SLAT_HighLoad.xml"));
		SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
		SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
		String htmlRendering = bnf.render(slaTemplate, true);
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				new File("ORCSoftware-SLAT_HighLoad.html")));
		dos.writeChars(htmlRendering);
		dos.close();
	}

//	public void testDummySlat() throws Exception {
//		parseRenderSLAT(SLASOITemplateParserTest.class.getResourceAsStream("/dummySLAT.xml"), new File("dummySLAT.html"));
//	}

	public void parseRenderSLAT(InputStream input, File output)
			throws Exception {
		SLATemplateDocument slaXml;
		slaXml = SLATemplateDocument.Factory.parse(input);
		SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
		SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
		String htmlRendering = bnf.render(slaTemplate, true);
		DataOutputStream dos = new DataOutputStream(
				new FileOutputStream(output));
		dos.writeChars(htmlRendering);
		dos.close();
	}

	// public void testDummySla() throws Exception {
	// SLADocument slaXml;
	// slaXml =
	// SLADocument.Factory.parse(SLASOIParserTest.class
	// .getResourceAsStream("/dummySLA.xml"));
	// SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
	// SLATemplate slaTemplate = slasoiParser.parseTemplate(slaXml.xmlText());
	// String htmlRendering = bnf.render(slaTemplate, true);
	// DataOutputStream dos = new DataOutputStream(new FileOutputStream(new
	// File("ORCSoftware-SLAT_HighLoad.html")));
	// dos.writeChars(htmlRendering);
	// dos.close();
	// }
}
