package org.slasoi.gslam.syntaxconverter.tests;

import junit.framework.TestCase;

import org.slasoi.slamodel.core.Annotated;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.BOOL;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.ServiceRef;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Invocation;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.Party.Operative;

public abstract class XmlParserTest extends TestCase {

    protected void testAgreementTerms(AgreementTerm[] agreementTerms) {
        assertTrue(agreementTerms.length == 2);
        for (int i = 0; i < agreementTerms.length; i++) {
            AgreementTerm agreementTerm = agreementTerms[i];

            // Annotation
            testAnnotation(agreementTerm);

            // Attributes
            ID id = agreementTerm.getId();
            assertEquals(id.getValue(), "id");

            // VariableDeclrs
            VariableDeclr[] variableDeclrs = agreementTerm.getVariableDeclrs();
            testVariableDeclrs(variableDeclrs);

            // Guarantees
            Guaranteed[] guarantees = agreementTerm.getGuarantees();
            testGuarantees(guarantees);

            // Precondition
            // TODO test for precondition
            // agreementTerm.getPrecondition();
        }
    }

    protected void testGuarantees(Guaranteed[] guarantees) {
        assertTrue(guarantees.length == 2);

        for (int i = 0; i < guarantees.length; i++) {
            Guaranteed guaranteed = guarantees[i];

            // Annotation
            testAnnotation(guaranteed);

            // ID
            ID id = guaranteed.getId();
            assertEquals(id.getValue(), "id");

            if (guaranteed instanceof Guaranteed.Action) {
                testGuaranteedAction((Guaranteed.Action) guaranteed);
            }
            else if (guaranteed instanceof Guaranteed.State) {
                testGuaranteedState((Guaranteed.State) guaranteed);
            }
        }
    }

    protected void testGuaranteedState(Guaranteed.State guaranteedState) {
        // Annotation
        testAnnotation(guaranteedState);

        // State
        testConstraintExpr(guaranteedState.getState());

        // Priority
        CONST priority = guaranteedState.getPriority();
        assertEquals(priority.getValue(), "priority");
        assertTrue(priority.getDatatype().getValue().endsWith("prioritytype"));
    }

    protected void testConstraintExpr(ConstraintExpr constraintExpr) {
        if (constraintExpr instanceof CompoundConstraintExpr) {
            testCompoundConstraintExpr((CompoundConstraintExpr) constraintExpr);
        }
        else if (constraintExpr instanceof TypeConstraintExpr) {
            testTypeConstraintExpr((TypeConstraintExpr) constraintExpr);
        }
    }

    protected void testCompoundConstraintExpr(CompoundConstraintExpr compoundConstraintExpr) {
        // Logical Operator
        STND logicalOp = compoundConstraintExpr.getLogicalOp();
        assertEquals(logicalOp.getValue(), "and");

        // Sub-Expressions
        ConstraintExpr[] subExpressions = compoundConstraintExpr.getSubExpressions();
        for (int i = 0; i < subExpressions.length; i++) {
            ConstraintExpr constraintExpr = subExpressions[i];
            testConstraintExpr(constraintExpr);
        }
    }

    protected void testTypeConstraintExpr(TypeConstraintExpr typeConstraintExpr) {
        // Error
        CONST error = typeConstraintExpr.getError();
        assertEquals(error.getValue(), "error");
        assertTrue(error.getDatatype().getValue().endsWith("type"));

        // Value
        ValueExpr value = typeConstraintExpr.getValue();
        testValueExpr(value);

        // DomainExpr
        DomainExpr domain = typeConstraintExpr.getDomain();
        testDomainExpr(domain);
    }

    protected void testDomainExpr(DomainExpr domain) {
        // check for type of DomainExpr
        if (domain instanceof CompoundDomainExpr) {
            testCompoundDomainExpr((CompoundDomainExpr) domain);
        }
        else if (domain instanceof SimpleDomainExpr) {
            testSimpleDomainExpr((SimpleDomainExpr) domain);
        }
    }

    protected void testCompoundDomainExpr(CompoundDomainExpr compoundDomainExpr) {
        // Logical Operator
        STND logicalOp = compoundDomainExpr.getLogicalOp();
        assertEquals(logicalOp.getValue(), "and");

        // Sub-Expressions
        DomainExpr[] subExpressions = compoundDomainExpr.getSubExpressions();
        for (int i = 0; i < subExpressions.length; i++) {
            DomainExpr domainExpr = subExpressions[i];
            testDomainExpr(domainExpr);
        }
    }

    protected void testSimpleDomainExpr(SimpleDomainExpr simpleDomainExpr) {
        // Comparison Operator
        STND comparisonOp = simpleDomainExpr.getComparisonOp();
        assertEquals(comparisonOp.getValue(), "comparisonOp");

        // Value
        ValueExpr value = simpleDomainExpr.getValue();
        testValueExpr(value);
    }

    protected void testValueExpr(ValueExpr value) {
        // Identify the type of ValueExpr
        if (value instanceof DomainExpr) {

        }
        else if (value instanceof BOOL) {
            assertEquals(((BOOL) value).getValue(), false);
        }
        else if (value instanceof CONST) {
            fail();
        }
        else if (value instanceof EventExpr) {
            fail();
        }
        else if (value instanceof FunctionalExpr) {
            fail();
        }
        else if (value instanceof ID) {
            fail();
        }
        else if (value instanceof ServiceRef) {
            fail();
        }
        else if (value instanceof TIME) {
            fail();
        }
    }

    protected void testGuaranteedAction(Guaranteed.Action guaranteedAction) {
        // Policy
        STND policy = guaranteedAction.getPolicy();
        assertEquals(policy.getValue(), "policy");

        // Precondition
        EventExpr precondition = guaranteedAction.getPrecondition();
        testEventExpression(precondition);

        // Postcondition
        Defn postcondition = guaranteedAction.getPostcondition();
        testGuaranteedActionDefn(postcondition);
    }

    protected void testGuaranteedActionDefn(Defn postcondition) {
        // Annotation
//        testAnnotation(postcondition);

        if (postcondition instanceof Invocation) {
            testInvocation((Invocation) postcondition);
        }
        else if (postcondition instanceof CustomAction) {
            fail();
        }
    }

    protected void testInvocation(Invocation invocation) {
        // Endpoint
        ID endpointId = invocation.getEndpointId();
        assertEquals(endpointId.getValue(), "endpoint");

        // Operation
        ID operationId = invocation.getOperationId();
        assertEquals(operationId.getValue(), "operation");

        // Parameters
        // TODO use getKeys to all keys and iterate over them
        ID key = new ID("Key1");
        ValueExpr parameterValue = invocation.getParameterValue(key);
        if (!(parameterValue instanceof BOOL)) {
            fail();
        }
        BOOL boolValue = (BOOL) parameterValue;
        assertEquals(boolValue.getValue(), false);

        key.setValue("Key2");
        parameterValue = invocation.getParameterValue(key);
        if (!(parameterValue instanceof BOOL)) {
            fail();
        }
        boolValue = (BOOL) parameterValue;
        assertEquals(boolValue.getValue(), false);

    }

    protected void testEventExpression(EventExpr eventExpr) {
        // Annotation
        // testAnnotation(eventExpr);

        // Operator
        STND operator = eventExpr.getOperator();
        assertEquals("operator", operator.getValue());

        // Parameter
        Expr[] parameters = eventExpr.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            ValueExpr valueExpr = (ValueExpr) parameters[i];
            testValueExpr(valueExpr);
        }
    }

    protected void testVariableDeclrs(VariableDeclr[] variableDeclrs) {
        for (int i = 0; i < variableDeclrs.length; i++) {
            VariableDeclr variableDeclr = variableDeclrs[i];
            // Annotation
            testAnnotation(variableDeclr);

            // Var
            ID var = variableDeclr.getVar();
            assertEquals(var.getValue(), "var");

            // identify right type
            if (variableDeclr instanceof Customisable) {
                // Expr
                Expr expr = variableDeclr.getExpr();
                if (!(expr instanceof DomainExpr)) {
                    fail();
                }
                testDomainExpr((DomainExpr) expr);

                // Value
                CONST value = ((Customisable) variableDeclr).getValue();
                assertEquals(value.getValue(), "value");
                assertEquals(value.getDatatype().getValue(), "datatype");
            }
            else {
                // Expr
                Expr expr = variableDeclr.getExpr();
                if (!(expr instanceof BOOL)) {
                    fail();
                }
                assertEquals(((BOOL) expr).getValue(), false);
            }
        }
    }

    protected void testInterfaceDeclrs(InterfaceDeclr[] interfaceDeclrs) {
        for (int i = 0; i < interfaceDeclrs.length; i++) {
            InterfaceDeclr interfaceDeclr = interfaceDeclrs[i];

            // Annotation
            testAnnotation(interfaceDeclr);

            // ID
            ID id = interfaceDeclr.getId();
            assertEquals(id.getValue(), "id");

            // ProviderRef
            ID providerRef = interfaceDeclr.getProviderRef();
            assertEquals(providerRef.getValue(), "providerref");

            // Endpoints
            Endpoint[] endpoints = interfaceDeclr.getEndpoints();
            for (int j = 0; j < endpoints.length; j++) {
                Endpoint endpoint = endpoints[j];
                testEndpoint(endpoint);
            }

            // Interface
            Interface interfejs = interfaceDeclr.getInterface();
            testInterface(interfejs);
        }
    }

    protected void testInterface(Interface interfejs) {
        // Annotation
        testAnnotation(interfejs);

        // identify the type
        if (interfejs instanceof Interface.Specification) {
            Interface.Specification iSpec = (Interface.Specification) interfejs;
            // TODO find getName() in sla model java
            // String name = iSpec.getName();
            // assertEquals(name, "name");

            // Extended
            ID[] extended = iSpec.getExtended();
            for (int i = 0; i < extended.length; i++) {
                // ID
                ID id = extended[i];
                assertEquals(id.getValue(), "extended" + (i + 1));
            }

            Operation[] operations = iSpec.getOperations();
            for (int i = 0; i < operations.length; i++) {
                Operation operation = operations[i];
                testInterfaceOperation(operation);
            }
        }
        else
            fail();
    }

    protected void testInterfaceOperation(Operation operation) {
        // Annotation
        testAnnotation(operation);

        // Faults
        STND[] faults = operation.getFaults();
        for (int i = 0; i < faults.length; i++) {
            STND stnd = faults[i];
            assertEquals(stnd.getValue(), "fault");
        }

        // Name
        ID name = operation.getName();
        assertEquals(name.getValue(), "name");

        // Inputs
        Property[] inputs = operation.getInputs();
        for (int i = 0; i < inputs.length; i++) {
            Property property = inputs[i];
            testInterfaceProperty(property);
        }

        // Inputs
        Property[] outputs = operation.getOutputs();
        for (int i = 0; i < outputs.length; i++) {
            Property property = outputs[i];
            testInterfaceProperty(property);
        }

        // Related
        Property[] related = operation.getRelated();
        for (int i = 0; i < related.length; i++) {
            Property property = related[i];
            testInterfaceProperty(property);
        }
    }

    protected void testInterfaceProperty(Property property) {
        // Annotation
        testAnnotation(property);

        // Name
        ID name = property.getName();
        assertEquals(name.getValue(), "name");

        // Auxiliary
        boolean auxiliary = property.isAuxiliary();
        assertFalse(auxiliary);

        // Datatype
        STND datatype = property.getDatatype();
        assertEquals(datatype.getValue(), "joule");

        // Domain
        DomainExpr domain = property.getDomain();
        testDomainExpr(domain);
    }

    protected void testEndpoint(Endpoint endpoint) {
        // Annotation
        testAnnotation(endpoint);

        // ID
        ID id = endpoint.getId();
        assertEquals(id.getValue(), "id");

        // Location
        UUID location = endpoint.getLocation();
        assertEquals(location.getValue(), "location");

        // Protocol
        STND protocol = endpoint.getProtocol();
        assertEquals(protocol.getValue(), "protocol");
    }

    protected void testParties(Party[] parties) {
        // Number parties
        assertTrue(parties.length == 2);

        for (int i = 0; i < parties.length; i++) {
            Party party = parties[i];
            testParty(party);
        }
    }

    protected void testParty(Party party) {
        // Annotation
        testAnnotation(party);

        // AgreementRole
        STND agreementRole = party.getAgreementRole();
        assertEquals(agreementRole.getValue(), "role");

        // ID
        ID id = party.getId();
        assertEquals(id.getValue(), "id");

        // Operatives
        Operative[] operatives = party.getOperatives();
        assertTrue(operatives.length == 1);
        for (int i = 0; i < operatives.length; i++) {
            Operative operative = operatives[i];
            testOperative(operative);
        }
    }

    protected void testOperative(Operative operative) {
        // Annotation
        testAnnotation(operative);

        // ID
        ID id = operative.getId();
        assertEquals(id.getValue(), "id");
    }

    protected void testAnnotation(Annotated annotated) {
        // Text
        assertEquals(annotated.getDescr(), "AnnotatedText");

        // Properties
        STND[] propertyKeys = annotated.getPropertyKeys();
        for (int i = 0; i < propertyKeys.length; i++) {
            STND key = propertyKeys[i];
            String propertyValue = (String) annotated.getPropertyValue(key);
            assertTrue(((String) propertyValue).startsWith("Value"));
        }
    }
}
