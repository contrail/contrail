package org.slasoi.gslam.syntaxconverter.tests;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import org.apache.xmlbeans.XmlCalendar;
import org.apache.xmlbeans.XmlException;
import org.slasoi.gslam.syntaxconverter.SLARenderer;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOIRenderer;
import org.slasoi.slamodel.sla.SLA;

import eu.slaatsoi.slamodel.AgreementPartyType;
import eu.slaatsoi.slamodel.AgreementTermType;
import eu.slaatsoi.slamodel.InterfaceDeclrType;
import eu.slaatsoi.slamodel.SLADocument;
import eu.slaatsoi.slamodel.VariableDeclrType;

public class SLASOIRendererTest extends XmlRendererTest {
    public void testRenderSLASOI() {
        SLASOIRenderer slasoiRenderer = new SLASOIRenderer();
        SLADocument slaXml;
        try {
            // parse the template from file
            slaXml = SLADocument.Factory.parse(SLARenderer.class.getResourceAsStream("/SLASOI.xml"));
            SLASOIParser slaParser = new SLASOIParser();
            SLA sla = slaParser.parseSLA(slaXml.xmlText());

            // render the template
            SLADocument slaRendered = SLADocument.Factory.parse(slasoiRenderer.renderSLA(sla));

            // Annotation
            testAnnotation(slaRendered.getSLA());

            // UUID
            String uuid = slaRendered.getSLA().getUUID();
            assertEquals("id", uuid);

            // TemplateId
            String templateId = slaRendered.getSLA().getTemplateId();
            assertEquals("id", templateId);

            // ModelVersion
            String modelVersion = slaRendered.getSLA().getModelVersion();
            assertEquals(SLA.$model_version, modelVersion);

            // AgreedAt
            Calendar agreedAt = slaRendered.getSLA().getAgreedAt();
            XmlCalendar agreedAtCal = new XmlCalendar(agreedAt.getTime());
//            assertEquals("2002-05-30T09:00:00.000+02:00", agreedAtCal.toString());

            // EffectiveFrom
            Calendar effectiveFrom = slaRendered.getSLA().getEffectiveFrom();
            XmlCalendar effectiveFromCal = new XmlCalendar(effectiveFrom.getTime());
  //          assertEquals("2002-05-30T09:00:00.000+02:00", effectiveFromCal.toString());

            // EffectiveUntil
            Calendar effectiveUntil = slaRendered.getSLA().getEffectiveUntil();
            XmlCalendar effectiveUntilCal = new XmlCalendar(effectiveUntil.getTime());
      //      assertEquals("2002-05-30T09:00:00.000+02:00", effectiveUntilCal.toString());

            // Party
            AgreementPartyType[] parties = slaRendered.getSLA().getPartyArray();
            for (int i = 0; i < parties.length; i++) {
                testParty(parties[i]);
            }

            // InterfaceDeclr
            InterfaceDeclrType[] interfaceDeclarations = slaRendered.getSLA().getInterfaceDeclrArray();
            for (int i = 0; i < interfaceDeclarations.length; i++) {
                testInterfaceDeclr(interfaceDeclarations[i]);
            }

            // VariableDeclr
            VariableDeclrType[] variableDeclratations = slaRendered.getSLA().getVariableDeclrArray();
            for (int i = 0; i < variableDeclratations.length; i++) {
                testVariableDeclr(variableDeclratations[i]);
            }

            // AgreementTerm
            AgreementTermType[] agreementTerms = slaRendered.getSLA().getAgreementTermArray();
            for (int i = 0; i < agreementTerms.length; i++) {
                testAgreementTerm(agreementTerms[i]);
            }

            // Save
            slaRendered.save(new File("SLASOI.xml"));

            // xmlbeans is really problematic here; saved file does validate in Oxygen
            // assertTrue(slaTemplateRendered.validate());
        }
        catch (XmlException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
