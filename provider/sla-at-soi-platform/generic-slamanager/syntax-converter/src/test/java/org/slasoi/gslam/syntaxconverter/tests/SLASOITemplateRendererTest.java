package org.slasoi.gslam.syntaxconverter.tests;

import java.io.File;
import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

import eu.slaatsoi.slamodel.AgreementPartyType;
import eu.slaatsoi.slamodel.AgreementTermType;
import eu.slaatsoi.slamodel.InterfaceDeclrType;
import eu.slaatsoi.slamodel.SLATemplateDocument;
import eu.slaatsoi.slamodel.VariableDeclrType;

public class SLASOITemplateRendererTest extends XmlRendererTest {
    public void testRenderSLATemplate() {
        SLASOITemplateRenderer slasoiTemplateRenderer = new SLASOITemplateRenderer();
        SLATemplateDocument slaTemplateXml;
        try {
            // parse the template from file
            slaTemplateXml =
                    SLATemplateDocument.Factory.parse(SLATemplateParser.class
                            .getResourceAsStream("/SLASOITemplate.xml"));
            SLASOITemplateParser slaTemplateParser = new SLASOITemplateParser();
            SLATemplate slaTemplate = slaTemplateParser.parseTemplate(slaTemplateXml.xmlText());

            // render the template
            SLATemplateDocument slaTemplateRendered =
                    SLATemplateDocument.Factory.parse(slasoiTemplateRenderer.renderSLATemplate(slaTemplate));

            // Annotation
            testAnnotation(slaTemplateRendered.getSLATemplate());

            // UUID
            String uuid = slaTemplateRendered.getSLATemplate().getUUID();
            assertEquals("id", uuid);

            // ModelVersion
            String modelVersion = slaTemplateRendered.getSLATemplate().getModelVersion();
            assertEquals(SLA.$model_version, modelVersion);

            // Party
            AgreementPartyType[] parties = slaTemplateRendered.getSLATemplate().getPartyArray();
            for (int i = 0; i < parties.length; i++) {
                testParty(parties[i]);
            }

            // InterfaceDeclr
            InterfaceDeclrType[] interfaceDeclarations = slaTemplateRendered.getSLATemplate().getInterfaceDeclrArray();
            for (int i = 0; i < interfaceDeclarations.length; i++) {
                testInterfaceDeclr(interfaceDeclarations[i]);
            }

            // VariableDeclr
            VariableDeclrType[] variableDeclratations = slaTemplateRendered.getSLATemplate().getVariableDeclrArray();
            for (int i = 0; i < variableDeclratations.length; i++) {
                testVariableDeclr(variableDeclratations[i]);
            }

            // AgreementTerm
            AgreementTermType[] agreementTerms = slaTemplateRendered.getSLATemplate().getAgreementTermArray();
            for (int i = 0; i < agreementTerms.length; i++) {
                testAgreementTerm(agreementTerms[i]);
            }

            // Save
            slaTemplateRendered.save(new File("SLATemplate.xml"));

            // xmlbeans is really problematic here; saved file does validate in Oxygen
            // assertTrue(slaTemplateRendered.validate());
        }
        catch (XmlException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
