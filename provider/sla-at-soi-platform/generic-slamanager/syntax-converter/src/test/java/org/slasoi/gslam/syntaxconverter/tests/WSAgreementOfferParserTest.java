package org.slasoi.gslam.syntaxconverter.tests;

import java.io.IOException;
import java.util.Calendar;

import org.apache.xmlbeans.XmlCalendar;
import org.apache.xmlbeans.XmlException;
import org.ggf.schemas.graap.x2007.x03.wsAgreement.AgreementOfferDocument;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementOfferParser;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;

public class WSAgreementOfferParserTest extends XmlParserTest {
    public void testParseSLA() {
        try {
            AgreementOfferDocument wsagSLATemplate =
                    AgreementOfferDocument.Factory.parse(SLATemplateParser.class.getResourceAsStream("/WSAGOffer.xml"));
            WSAgreementOfferParser slaParser = new WSAgreementOfferParser();
            SLA sla = slaParser.parseSLA(wsagSLATemplate.xmlText());

            // Agreement UUID
            UUID uuid = sla.getUuid();
            assertEquals(uuid.getValue(), "id");

            // ModelVersion
            String modelVersion = sla.getModelVersion();
            assertEquals(modelVersion, SLATemplate.$model_version);

            // AgreedAt
            Calendar agreedAt = sla.getAgreedAt().getValue();
            XmlCalendar agreedAtCal = new XmlCalendar("2002-05-30T09:00:00");
            assertEquals(agreedAtCal.getTime(), agreedAt.getTime());

            // EffectiveFrom
            Calendar effectiveFrom = sla.getEffectiveFrom().getValue();
            XmlCalendar effectiveFromCal = new XmlCalendar("2002-05-30T09:00:00");
            assertEquals(effectiveFromCal.getTime(), effectiveFrom.getTime());

            // EffetiveUntil
            Calendar effectiveUntil = sla.getEffectiveUntil().getValue();
            XmlCalendar effectiveUntilCal = new XmlCalendar("2002-05-30T09:00:00");
            assertEquals(effectiveUntilCal.getTime(), effectiveUntil.getTime());

            // TemplateId
            UUID templateId = sla.getTemplateId();
            assertEquals("id", templateId.getValue());

            // Party
            Party[] parties = sla.getParties();
            testParties(parties);

            // InterfaceDeclr
            InterfaceDeclr[] interfaceDeclrs = sla.getInterfaceDeclrs();
            testInterfaceDeclrs(interfaceDeclrs);

            // VariableDeclr
            VariableDeclr[] variableDeclrs = sla.getVariableDeclrs();
            testVariableDeclrs(variableDeclrs);

            // AgreementTerm
            AgreementTerm[] agreementTerms = sla.getAgreementTerms();
            testAgreementTerms(agreementTerms);

        }
        catch (XmlException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
