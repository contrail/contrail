/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=NO_AUTO_VALUE_ON_ZERO */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gpe`;
USE `gpe`;

DROP TABLE IF EXISTS `businesshistory`;
CREATE TABLE `businesshistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allowedPenaltiesAmount` double NOT NULL,
  `billingSatisfactionLevel` int(11) DEFAULT NULL,
  `currentPenaltiesAmount` double NOT NULL,
  `penaltiesAllowed` int(11) NOT NULL,
  `penaltySatisfactionLevel` int(11) DEFAULT NULL,
  `serviceSatisfactionLevel` int(11) DEFAULT NULL,
  `slaId` varchar(255) DEFAULT NULL,
  `slaStatus` int(11) DEFAULT NULL,
  `slaTemplateId` varchar(255) DEFAULT NULL,
  `slaWorth` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `continent` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `negotiationhistory`;
CREATE TABLE `negotiationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endTime` datetime DEFAULT NULL,
  `negotiationId` varchar(255) DEFAULT NULL,
  `negotiationRounds` int(11) DEFAULT NULL,
  `outcome` int(11) DEFAULT NULL,
  `renegotiation` bit(1) DEFAULT NULL,
  `slaId` varchar(255) DEFAULT NULL,
  `slaTemplateId` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `st_ranks_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB67DCD9339EA39E3` (`st_ranks_id`),
  CONSTRAINT `FKB67DCD9339EA39E3` FOREIGN KEY (`st_ranks_id`) REFERENCES `strategyranks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `party`;
CREATE TABLE `party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketSegment` int(11) DEFAULT NULL,
  `partyId` varchar(255) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4952AC620BA89BC` (`location_id`),
  CONSTRAINT `FK4952AC620BA89BC` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `DTYPE` varchar(31) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annotations` tinyblob,
  `creationTime` datetime DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  `profileType` int(11) DEFAULT NULL,
  `customerEvolution` int(11) DEFAULT NULL,
  `customerSegment` int(11) DEFAULT NULL,
  `education` int(11) DEFAULT NULL,
  `estimatedAge` int(11) DEFAULT NULL,
  `location` tinyblob,
  `mobility` bit(1) DEFAULT NULL,
  `socioEconomicLevel` int(11) DEFAULT NULL,
  `maxPenaltyCost` int(11) DEFAULT NULL,
  `maxPenaltyCostPerCustomer` int(11) DEFAULT NULL,
  `maxViolationCount` int(11) DEFAULT NULL,
  `maxViolationCountPerCustomer` int(11) DEFAULT NULL,
  `salesCountExcelObjective` int(11) DEFAULT NULL,
  `salesMinCountObjective` int(11) DEFAULT NULL,
  `maxPenaltyCosts` int(11) DEFAULT NULL,
  `maxViolationsCount` int(11) DEFAULT NULL,
  `salesObjective` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK50C72189F6D9E51C` (`status_id`),
  CONSTRAINT `FK50C72189F6D9E51C` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_hobbies`;
CREATE TABLE `profile_hobbies` (
  `Profile_id` int(11) NOT NULL,
  `element` int(11) DEFAULT NULL,
  KEY `FK91CA513AF7478CFA` (`Profile_id`),
  CONSTRAINT `FK91CA513AF7478CFA` FOREIGN KEY (`Profile_id`) REFERENCES `profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profilebizhistory`;
CREATE TABLE `profilebizhistory` (
  `profile_id` int(11) NOT NULL,
  `biz_hist_id` int(11) NOT NULL,
  PRIMARY KEY (`profile_id`,`biz_hist_id`),
  UNIQUE KEY `biz_hist_id` (`biz_hist_id`),
  KEY `FK543E1A6AF27EE8B8` (`profile_id`),
  KEY `FK543E1A6ABEBB527E` (`biz_hist_id`),
  CONSTRAINT `FK543E1A6ABEBB527E` FOREIGN KEY (`biz_hist_id`) REFERENCES `businesshistory` (`id`),
  CONSTRAINT `FK543E1A6AF27EE8B8` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profileneghistory`;
CREATE TABLE `profileneghistory` (
  `profile_id` int(11) NOT NULL,
  `neg_hist_id` int(11) NOT NULL,
  PRIMARY KEY (`profile_id`,`neg_hist_id`),
  UNIQUE KEY `neg_hist_id` (`neg_hist_id`),
  KEY `FKC0E7574DF27EE8B8` (`profile_id`),
  KEY `FKC0E7574D72F366BE` (`neg_hist_id`),
  CONSTRAINT `FKC0E7574D72F366BE` FOREIGN KEY (`neg_hist_id`) REFERENCES `negotiationhistory` (`id`),
  CONSTRAINT `FKC0E7574DF27EE8B8` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blackListed` bit(1) DEFAULT NULL,
  `whiteListed` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `status_blacklistingreason`;
CREATE TABLE `status_blacklistingreason` (
  `Status_id` int(11) NOT NULL,
  `element` int(11) DEFAULT NULL,
  KEY `FKFED13976F6D9E51C` (`Status_id`),
  CONSTRAINT `FKFED13976F6D9E51C` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `status_whitelistingreason`;
CREATE TABLE `status_whitelistingreason` (
  `Status_id` int(11) NOT NULL,
  `element` int(11) DEFAULT NULL,
  KEY `FK950F918CF6D9E51C` (`Status_id`),
  CONSTRAINT `FK950F918CF6D9E51C` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `strategyranks`;
CREATE TABLE `strategyranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerRank` int(11) DEFAULT NULL,
  `negotiationRank` int(11) DEFAULT NULL,
  `productRank` int(11) DEFAULT NULL,
  `providerRank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `name` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;


/*
//Following script generated by hibernate at a previous attempt

CREATE DATABASE /*!32312 IF NOT EXISTS*/ gpe;
USE gpe;

    alter table NegotiationHistory 
        drop 
        foreign key FKB67DCD931BCCBA34;

    alter table Party 
        drop 
        foreign key FK4952AC6974B5ACB;

    alter table Profile 
        drop 
        foreign key FK50C72189C639ADEB;

    alter table ProfileBizHistory 
        drop 
        foreign key FK543E1A6AF1839C9;

    alter table ProfileBizHistory 
        drop 
        foreign key FK543E1A6AB1FF028F;

    alter table ProfileNegHistory 
        drop 
        foreign key FKC0E7574DF1839C9;

    alter table ProfileNegHistory 
        drop 
        foreign key FKC0E7574D64D9710D;

    alter table Profile_hobbies 
        drop 
        foreign key FK91CA513AEA8B3D0B;

    alter table Status_blackListingReason 
        drop 
        foreign key FKFED13976C639ADEB;

    alter table Status_whiteListingReason 
        drop 
        foreign key FK950F918CC639ADEB;

    drop table if exists BusinessHistory;

    drop table if exists Location;

    drop table if exists NegotiationHistory;

    drop table if exists Party;

    drop table if exists Profile;

    drop table if exists ProfileBizHistory;

    drop table if exists ProfileNegHistory;

    drop table if exists Profile_hobbies;

    drop table if exists Status;

    drop table if exists Status_blackListingReason;

    drop table if exists Status_whiteListingReason;

    drop table if exists StrategyRanks;

    create table BusinessHistory (
        id integer not null auto_increment,
        allowedPenaltiesAmount double precision not null,
        billingSatisfactionLevel integer,
        currentPenaltiesAmount double precision not null,
        penaltiesAllowed integer not null,
        penaltySatisfactionLevel integer,
        serviceSatisfactionLevel integer,
        slaId varchar(255),
        slaStatus integer,
        slaTemplateId varchar(255),
        slaWorth integer,
        primary key (id)
    );

    create table Location (
        id integer not null auto_increment,
        address varchar(255),
        continent varchar(255),
        country varchar(255),
        emailAddress varchar(255),
        fax integer,
        telephone varchar(255),
        primary key (id)
    );

    create table NegotiationHistory (
        id integer not null auto_increment,
        endTime datetime,
        negotiationId varchar(255),
        negotiationRounds integer,
        outcome integer,
        renegotiation bit,
        slaId varchar(255),
        slaTemplateId varchar(255),
        startTime datetime,
        st_ranks_id integer,
        primary key (id)
    );

    create table Party (
        id integer not null auto_increment,
        marketSegment integer,
        partyId varchar(255),
        location_id integer,
        primary key (id)
    );

    create table Profile (
        DTYPE varchar(31) not null,
        id integer not null auto_increment,
        annotations tinyblob,
        creationTime datetime,
        lastModified datetime,
        profileId varchar(255),
        profileType integer,
        customerEvolution integer,
        customerSegment integer,
        education integer,
        estimatedAge integer,
        location tinyblob,
        mobility bit,
        socioEconomicLevel integer,
        maxPenaltyCost integer,
        maxPenaltyCostPerCustomer integer,
        maxViolationCount integer,
        maxViolationCountPerCustomer integer,
        salesCountExcelObjective integer,
        salesMinCountObjective integer,
        maxPenaltyCosts integer,
        maxViolationsCount integer,
        salesObjective integer,
        status_id integer,
        primary key (id)
    );

    create table ProfileBizHistory (
        profile_id integer not null,
        biz_hist_id integer not null,
        primary key (profile_id, biz_hist_id),
        unique (biz_hist_id)
    );

    create table ProfileNegHistory (
        profile_id integer not null,
        neg_hist_id integer not null,
        primary key (profile_id, neg_hist_id),
        unique (neg_hist_id)
    );

    create table Profile_hobbies (
        Profile_id integer not null,
        element integer
    );

    create table Status (
        id integer not null auto_increment,
        blackListed bit,
        whiteListed bit,
        primary key (id)
    );

    create table Status_blackListingReason (
        Status_id integer not null,
        element integer
    );

    create table Status_whiteListingReason (
        Status_id integer not null,
        element integer
    );

    create table StrategyRanks (
        id integer not null auto_increment,
        customerRank integer,
        negotiationRank integer,
        productRank integer,
        providerRank integer,
        primary key (id)
    );

    alter table NegotiationHistory 
        add index FKB67DCD931BCCBA34 (st_ranks_id), 
        add constraint FKB67DCD931BCCBA34 
        foreign key (st_ranks_id) 
        references StrategyRanks (id);

    alter table Party 
        add index FK4952AC6974B5ACB (location_id), 
        add constraint FK4952AC6974B5ACB 
        foreign key (location_id) 
        references Location (id);

    alter table Profile 
        add index FK50C72189C639ADEB (status_id), 
        add constraint FK50C72189C639ADEB 
        foreign key (status_id) 
        references Status (id);

    alter table ProfileBizHistory 
        add index FK543E1A6AF1839C9 (profile_id), 
        add constraint FK543E1A6AF1839C9 
        foreign key (profile_id) 
        references Profile (id);

    alter table ProfileBizHistory 
        add index FK543E1A6AB1FF028F (biz_hist_id), 
        add constraint FK543E1A6AB1FF028F 
        foreign key (biz_hist_id) 
        references BusinessHistory (id);

    alter table ProfileNegHistory 
        add index FKC0E7574DF1839C9 (profile_id), 
        add constraint FKC0E7574DF1839C9 
        foreign key (profile_id) 
        references Profile (id);

    alter table ProfileNegHistory 
        add index FKC0E7574D64D9710D (neg_hist_id), 
        add constraint FKC0E7574D64D9710D 
        foreign key (neg_hist_id) 
        references NegotiationHistory (id);

    alter table Profile_hobbies 
        add index FK91CA513AEA8B3D0B (Profile_id), 
        add constraint FK91CA513AEA8B3D0B 
        foreign key (Profile_id) 
        references Profile (id);

    alter table Status_blackListingReason 
        add index FKFED13976C639ADEB (Status_id), 
        add constraint FKFED13976C639ADEB 
        foreign key (Status_id) 
        references Status (id);

    alter table Status_whiteListingReason 
        add index FK950F918CC639ADEB (Status_id), 
        add constraint FK950F918CC639ADEB 
        foreign key (Status_id) 
        references Status (id);
*/