package org.slasoi.gslam.protocolengine.persistence;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slasoi.gslam.protocolengine.persistence.profiles.BusinessHistory;
import org.slasoi.gslam.protocolengine.persistence.profiles.CustomerProfile;
import org.slasoi.gslam.protocolengine.persistence.profiles.Education;
import org.slasoi.gslam.protocolengine.persistence.profiles.Evolution;
import org.slasoi.gslam.protocolengine.persistence.profiles.Hobbies;
import org.slasoi.gslam.protocolengine.persistence.profiles.Level;
import org.slasoi.gslam.protocolengine.persistence.profiles.Location;
import org.slasoi.gslam.protocolengine.persistence.profiles.MarketSegment;
import org.slasoi.gslam.protocolengine.persistence.profiles.NegotiationHistory;
import org.slasoi.gslam.protocolengine.persistence.profiles.Outcome;
import org.slasoi.gslam.protocolengine.persistence.profiles.Party;
import org.slasoi.gslam.protocolengine.persistence.profiles.ProductProfile;
import org.slasoi.gslam.protocolengine.persistence.profiles.Profile;
import org.slasoi.gslam.protocolengine.persistence.profiles.ProfileType;
import org.slasoi.gslam.protocolengine.persistence.profiles.ProviderProfile;
import org.slasoi.gslam.protocolengine.persistence.profiles.Rank;
import org.slasoi.gslam.protocolengine.persistence.profiles.Reason;
import org.slasoi.gslam.protocolengine.persistence.profiles.SLAStatus;
import org.slasoi.gslam.protocolengine.persistence.profiles.SocioEconomicLevel;
import org.slasoi.gslam.protocolengine.persistence.profiles.Status;
import org.slasoi.gslam.protocolengine.persistence.profiles.StrategyRanks;


public class HibernateUtil {

    public static SessionFactory sessionFactory;

    static {
        String profile = System.getProperty("gslam.protocolengine.profiling");
        if ( profile!= null && profile.equals("true")) {
            try {
                // work around for Hibernate under OSGi
                JavaAssistHelper.linkJavaAssistClassLoader();
                
                //System.out.println("buildSessionFactory");
                // Create the SessionFactory from hibernate.cfg.xml
                AnnotationConfiguration annotationConfiguration = new AnnotationConfiguration();
    //            URL resources = HibernateUtil.class.getResource("/hibernate/hibernate.cfg.xml");
    //            annotationConfiguration.configure(resources);
                String path = System.getenv("SLASOI_HOME")
                                + System.getProperty("file.separator") + "generic-slamanager"+
                                System.getProperty("file.separator") + "protocol-engine" + 
                                System.getProperty("file.separator") + "hibernate" + System.getProperty("file.separator");
    
                File f = new File(path+"hibernate.cfg.xml");
                System.out.println("opening file at "+(path+"hibernate.cfg.xml"));
                //annotationConfiguration.configure("/hibernate/hibernate.cfg.xml");
                
                annotationConfiguration.configure(f);
                annotationConfiguration.setProperty(Environment.CACHE_PROVIDER_CONFIG, (path+"ehcache.xml") );
                
                annotationConfiguration.addPackage("org.slasoi.gslam.protocolengine.persistence.profiles")
                    .addAnnotatedClass(BusinessHistory.class)
                    .addAnnotatedClass(CustomerProfile.class)
                    .addAnnotatedClass(Education.class)
                    .addAnnotatedClass(Evolution.class)
                    .addAnnotatedClass(Hobbies.class)
                    .addAnnotatedClass(Level.class)
                    .addAnnotatedClass(Location.class)
                    .addAnnotatedClass(MarketSegment.class)
                    .addAnnotatedClass(NegotiationHistory.class)
                    .addAnnotatedClass(Outcome.class)
                    .addAnnotatedClass(Party.class)
                    .addAnnotatedClass(ProductProfile.class)
                    .addAnnotatedClass(Profile.class)
                    .addAnnotatedClass(ProfileType.class)
                    .addAnnotatedClass(ProviderProfile.class)
                    .addAnnotatedClass(Rank.class)
                    .addAnnotatedClass(Reason.class)
                    .addAnnotatedClass(SLAStatus.class)
                    .addAnnotatedClass(SocioEconomicLevel.class)
                    .addAnnotatedClass(Status.class)
                    .addAnnotatedClass(StrategyRanks.class);
                    
                //new SchemaExport(annotationConfiguration).create(true, true);
                
                //annotationConfiguration.buildMappings();
                sessionFactory = annotationConfiguration.buildSessionFactory();
                
                assert (sessionFactory != null) : "Hibernate Session could not be established";
                
                System.out.println("HibernateUtil :: session factory=" + sessionFactory);
                System.out.println("HibernateUtil :: GenericSLAM :: ProtocolEngine loaded!");
            }
            catch (Throwable ex) {
                // Make sure you log the exception, as it might be swallowed
                System.err.println("Initial SessionFactory creation failed." + ex);
                ex.printStackTrace();
                throw new ExceptionInInitializerError(ex);
            }
        }
    }

    public static Session getSession() throws HibernateException {
        assert (sessionFactory != null) : "Hibernate Session has not been initialized properly";
        if(sessionFactory != null){
            return sessionFactory.openSession();            
        }
        else{
            return null;
        }
    }
}