package org.slasoi.gslam.protocolengine.persistence.profiles;


public enum Hobbies {

    ANIMALS,
    ARTS,
    BOOKS,
    CARS,
    MOVIES,
    SCIENCE,
    SPORTS,
    TECHNOLOGY
}
