package org.slasoi.gslam.protocolengine.persistence.profiles;
import javax.persistence.Entity;

@Entity
public class ProductProfile extends Profile{
    
    private static final long serialVersionUID = 5938310100319077891L;
    private int salesMinCountObjective;
    private int salesCountExcelObjective;
    private int maxPenaltyCostPerCustomer;
    private int maxViolationCountPerCustomer;
    private int maxPenaltyCost;
    private int maxViolationCount;

    public ProductProfile(){
        
    }

    public int getSalesMinCountObjective() {
        return salesMinCountObjective;
    }

    public void setSalesMinCountObjective(int salesMinCountObjective) {
        this.salesMinCountObjective = salesMinCountObjective;
    }

    public int getSalesCountExcelObjective() {
        return salesCountExcelObjective;
    }

    public void setSalesCountExcelObjective(int salesCountExcelObjective) {
        this.salesCountExcelObjective = salesCountExcelObjective;
    }

    public int getMaxPenaltyCostPerCustomer() {
        return maxPenaltyCostPerCustomer;
    }

    public void setMaxPenaltyCostPerCustomer(int maxPenaltyCostPerCustomer) {
        this.maxPenaltyCostPerCustomer = maxPenaltyCostPerCustomer;
    }

    public int getMaxViolationCountPerCustomer() {
        return maxViolationCountPerCustomer;
    }

    public void setMaxViolationCountPerCustomer(int maxViolationCountPerCustomer) {
        this.maxViolationCountPerCustomer = maxViolationCountPerCustomer;
    }

    public int getMaxPenaltyCost() {
        return maxPenaltyCost;
    }

    public void setMaxPenaltyCost(int maxPenaltyCost) {
        this.maxPenaltyCost = maxPenaltyCost;
    }

    public int getMaxViolationCount() {
        return maxViolationCount;
    }

    public void setMaxViolationCount(int maxViolationCount) {
        this.maxViolationCount = maxViolationCount;
    }
    
}
