package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum Rank {
    
    HIGHLY_PREFERRED,
    PREFERRED,
    NORMAL,
    LOW,
    NOT_DESIRABLE

}
