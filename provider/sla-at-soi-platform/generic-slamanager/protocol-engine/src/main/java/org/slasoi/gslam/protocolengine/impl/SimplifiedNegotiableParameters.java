package org.slasoi.gslam.protocolengine.impl;

public class SimplifiedNegotiableParameters {

    private int numberOfCustomizationRounds;
    private int currentCustomizationRound;
    private int numberOfNegotiationRounds;
    private int currentNegotiationRound;
    private int processTimeout;
    private int maxCounterOffersAllowed;
    private boolean optionalCritiqueOnQoS;
    private Ownership belongsTo;
    private long startTime;
    private boolean isSealed;
    private String credentials;
    
    public SimplifiedNegotiableParameters(){
        
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public boolean isSealed() {
        return isSealed;
    }

    public void setSealed(boolean isSealed) {
        this.isSealed = isSealed;
    }

    public SimplifiedNegotiableParameters(Ownership ownership){
        this.belongsTo = ownership;
    }
    
    public int getNumberOfCustomizationRounds() {
        return numberOfCustomizationRounds;
    }

    public void setNumberOfCustomizationRounds(int numberOfCustomizationRounds) {
        this.numberOfCustomizationRounds = numberOfCustomizationRounds;
    }

    public int getCurrentCustomizationRound() {
        return currentCustomizationRound;
    }

    public void setCurrentCustomizationRound(int currentCustomizationRound) {
        this.currentCustomizationRound = currentCustomizationRound;
    }

    public int getNumberOfNegotiationRounds() {
        return numberOfNegotiationRounds;
    }

    public void setNumberOfNegotiationRounds(int numberOfNegotiationRounds) {
        this.numberOfNegotiationRounds = numberOfNegotiationRounds;
    }

    public int getCurrentNegotiationRound() {
        return currentNegotiationRound;
    }

    public void setCurrentNegotiationRound(int currentNegotiationRound) {
        this.currentNegotiationRound = currentNegotiationRound;
    }

    public int getProcessTimeout() {
        return processTimeout;
    }

    public void setProcessTimeout(int processTimeout) {
        this.processTimeout = processTimeout;
    }

    public int getMaxCounterOffersAllowed() {
        return maxCounterOffersAllowed;
    }

    public void setMaxCounterOffersAllowed(int maxCounterOffersAllowed) {
        this.maxCounterOffersAllowed = maxCounterOffersAllowed;
    }

    public boolean isOptionalCritiqueOnQoS() {
        return optionalCritiqueOnQoS;
    }
    
    public boolean getOptionalCritiqueOnQoS() {
        return optionalCritiqueOnQoS;
    }

    public void setOptionalCritiqueOnQoS(boolean optionalCritiqueOnQoS) {
        this.optionalCritiqueOnQoS = optionalCritiqueOnQoS;
    }
    
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String toString(){
        return " [belongsTo ="+ belongsTo + "\n" +
        	   "numberOfCustomizationRounds = "+numberOfCustomizationRounds+"\n"+
               "numberOfNegotiationRounds = "+numberOfNegotiationRounds+"\n"+ 
               "processTimeout = "+processTimeout+"\n"+ 
               "maxCounterOffersAllowed = "+maxCounterOffersAllowed+"\n"+ 
               "optionalCritiqueOnQoS = "+optionalCritiqueOnQoS+"]\n";
    }
    
    public Ownership getBelongsTo() {
        return belongsTo;
    }

    public void setBelongsTo(Ownership belongsTo) {
        this.belongsTo = belongsTo;
    }

    public enum Ownership {
        SELF,
        PARTNER
    }

}
