package org.slasoi.gslam.protocolengine.persistence;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.slasoi.gslam.protocolengine.persistence.profiles.CustomerProfile;
import org.slasoi.gslam.protocolengine.persistence.profiles.Education;
import org.slasoi.gslam.protocolengine.persistence.profiles.Hobbies;
import org.slasoi.gslam.protocolengine.persistence.profiles.Location;
import org.slasoi.gslam.protocolengine.persistence.profiles.MarketSegment;
import org.slasoi.gslam.protocolengine.persistence.profiles.NegotiationHistory;
import org.slasoi.gslam.protocolengine.persistence.profiles.Party;
import org.slasoi.gslam.protocolengine.persistence.profiles.ProviderProfile;
import org.slasoi.gslam.protocolengine.persistence.profiles.Rank;
import org.slasoi.gslam.protocolengine.persistence.profiles.Reason;
import org.slasoi.gslam.protocolengine.persistence.profiles.Status;
import org.slasoi.gslam.protocolengine.persistence.profiles.StrategyRanks;

public class PersistenceManager {

    public static void main(String[] args) {
        PersistenceManager mgr = new PersistenceManager();
        mgr.createAndStoreTest();
    }

    private void createAndStoreTest() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        
        ProviderProfile pProfile = new ProviderProfile();
        NegotiationHistory negHistory = new NegotiationHistory();
        negHistory.setStartTime(new java.sql.Timestamp(System.currentTimeMillis()));
        StrategyRanks sRanks = new StrategyRanks();
        sRanks.setProviderRank(Rank.NOT_DESIRABLE);
        sRanks.setNegotiationRank(Rank.NORMAL);
        negHistory.setRankings(sRanks);
        Set<NegotiationHistory> negHistorySet = new HashSet<NegotiationHistory>();
        negHistorySet.add(negHistory);
        pProfile.setNegotiationHistory(negHistorySet);
        pProfile.setProfileId("http://localhost:8080/services/ISNegotiation?wsdl");
        
        CustomerProfile cProfile = new CustomerProfile();
        Set<Hobbies> hobbies = new HashSet<Hobbies>();
        hobbies.add(Hobbies.BOOKS);
        hobbies.add(Hobbies.ANIMALS);
        hobbies.add(Hobbies.ARTS);
        cProfile.setHobbies(hobbies);
        cProfile.setProfileId("http://localhost:8080/services/BZNegotiation?wsdl");                
        
        System.out.println("PersistenceManager :: GenericSLAM :: ProtocolEngine :: Just before Saving");
        Integer cProfileId = (Integer)session.save(cProfile);
        session.save(pProfile);
        session.getTransaction().commit();
        
        session.beginTransaction();
        cProfile.setEducation(Education.VOCATIONAL);
        session.update(cProfile);
        session.getTransaction().commit();
        session.close();
    }

}

//public class PersistenceManager {
//
//    public static void main(String[] args) {
//        PersistenceManager mgr = new PersistenceManager();
//        mgr.createAndStoreTest();
//        HibernateUtil.getSessionFactory().close();
//    }
//
//    private void createAndStoreTest() {
//        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//        session.beginTransaction();
//        Location location = new Location();
//        location.setAddress("klm");        
//        Party party = new Party();
//        party.setMarketSegment(MarketSegment.LARGE_ENTERPRISE);
//        party.setLocation(location);
//        session.save(party);
//        
//        Status status = new Status();
//        status.setBlackListed(true);
//        Set<Reason> blackListingReason = new HashSet<Reason>();
//        blackListingReason.add(Reason.DEFAULTER);
//        status.setBlackListingReason(blackListingReason);
//        session.save(status);
//        
//        CustomerProfile cProfile = new CustomerProfile();
//        Set<Hobbies> hobbies = new HashSet<Hobbies>();
//        hobbies.add(Hobbies.BOOKS);
//        hobbies.add(Hobbies.ANIMALS);
//        hobbies.add(Hobbies.ARTS);
//        cProfile.setHobbies(hobbies);        
//        session.save(cProfile);
//        
//        ProviderProfile pProfile = new ProviderProfile();
//        NegotiationHistory negHistory = new NegotiationHistory();
//        negHistory.setStartTime(new java.sql.Timestamp(System.currentTimeMillis()));
//        StrategyRanks sRanks = new StrategyRanks();
//        sRanks.setProviderRank(Rank.NOT_DESIRABLE);
//        sRanks.setNegotiationRank(Rank.NORMAL);
//        negHistory.setRankings(sRanks);
//        Set<NegotiationHistory> negHistorySet = new HashSet<NegotiationHistory>();
//        negHistorySet.add(negHistory);
//        pProfile.setNegotiationHistory(negHistorySet);
//        session.save(pProfile);
//        
//        session.getTransaction().commit();
//    }
//
//}
