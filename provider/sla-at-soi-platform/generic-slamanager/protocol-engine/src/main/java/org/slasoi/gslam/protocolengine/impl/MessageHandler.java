/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1674 $
 * @lastrevision   $Date: 2011-05-11 20:00:57 +0200 (sre, 11 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/MessageHandler.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.gslam.protocolengine.INegotiationOutgoing;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>MessageHandler</code> class implements <code>ProtocolEngine</code>'s <code>INegotiation</code> interface.
 * <p>
 * It receives negotiation requests destined for the current SLA-Manager as well as posting requests to other
 * SLA-Manager(s) on behalf of the current SLA-Manager. Received requests are delegated to
 * <code>NegotiationManager</code> class. The <code>MessageHandler</code> class assumes that the communication is
 * reliable.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public class MessageHandler implements INegotiation, INegotiationOutgoing {

    private SLAManagerContext context;
    private java.util.HashMap<String, NegotiationManager> negotiationManagerHashMap;
    private String defaultSelfAddress = null;// The contact address of this SLAM.
    private SLARegistry.IRegister register;
    private SLARegistry.IQuery query;
    private ISyntaxConverter syntaxConverter;
    private PlanningOptimization planningOptimization;
    private static final Logger LOGGER = Logger.getLogger(MessageHandler.class);
    private BundleContext bundleContext;

    /**
     * Default constructor.
     */
    public MessageHandler() {
        LOGGER.info("*** INFO *** MessageHandler - Constructor ***");
        negotiationManagerHashMap = new HashMap<String, NegotiationManager>();
    }

    /**
     * Sets the SLA-Manager context at instance level.
     * 
     * @param context
     *            the slaManagerContext
     */
    public void setContext(SLAManagerContext context) {
        LOGGER.info("*** INFO *** MessageHandler - CONTEXT HAS BEEN SET ***");
        this.context = context;
        try {
            this.setDefaultSelfAddress(this.context.getEPR());
        }
        catch (SLAManagerContextException e) {
            LOGGER.info("*** INFO *** MessageHandler - context.getEPR() raised SLAManagerContextException ***");
            e.printStackTrace();
        }
    }

    /**
     * Gets the SLA-Manager context.
     * 
     * @return SLAManagerContext stored at instance level
     */
    public SLAManagerContext getContext() {
        return this.context;
    }

    /**
     * Gets the register object.
     * 
     * @return IRegister object of <code>SLARegistry</code>
     */
    protected SLARegistry.IRegister getRegistry() {
        try {
            this.register = (this.register != null ? this.register : this.context.getSLARegistry().getIRegister());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        return this.register;
    }

    /**
     * Gets the query object.
     * 
     * @return IQuery object of <code>SLARegistry</code>
     */
    protected SLARegistry.IQuery getQuery() {
        try {
            this.query = (this.query != null ? this.query : this.context.getSLARegistry().getIQuery());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        return this.query;
    }

    /**
     * Gets the planningoptimization instance.
     * 
     * @return PlanningOptimization of the SLA-Manager
     */
    protected PlanningOptimization getPlanningOptimization() {
        try {
            this.planningOptimization =
                    (this.planningOptimization != null ? this.planningOptimization : this.context
                            .getPlanningOptimization());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        return this.planningOptimization;
    }

    /**
     * Gets the syntaxconverter instance.
     * 
     * @return ISyntaxConverter of the SLA-Manager
     */
    protected ISyntaxConverter getSyntaxConverter() {
        try {
            this.syntaxConverter =
                    (this.syntaxConverter != null ? this.syntaxConverter
                            : ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) this.context
                                    .getSyntaxConverters())
                                    .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter));
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        return this.syntaxConverter;
    }

    /**
     * Checks if the invokee has access to initiate negotiation with this SLA-Manager.
     * 
     * @param slaTemplate
     *            the slaTemplate containing the party information
     * @return boolean value showing if access is allowed or not
     */
    protected boolean checkAccess(SLATemplate slaTemplate) {
        boolean allowed = false;
        try {
            allowed = this.context.getAuthorization().checkAccess(slaTemplate);
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        LOGGER.info("*** INFO *** MessageHandler - checkAccess - returning ***" + allowed);
        return allowed;
    }

    /**
     * Gets the default self address of this SLA-Manager.
     * 
     * @return String representing the epr of this SLA-Manager
     */
    protected String getDefaultSelfAddress() {
        try {
            this.defaultSelfAddress =
                    (this.defaultSelfAddress != null ? this.defaultSelfAddress : this.context.getEPR());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        return this.defaultSelfAddress;
    }

    /**
     * Sets the default self address of this SLA-Manager at the instance level.
     * 
     * @param address
     *            the string representing the epr of this SLA-Manager
     */
    protected void setDefaultSelfAddress(String address) {
        this.defaultSelfAddress = address;
    }

    /**
     * Checks if given address matches self address.
     * 
     * @param endPointAddress
     *            the String representing an epr
     * @return boolean value showing if given value matches the epr of this SLA-Manager.
     */
    protected boolean isSelfAddressed(String endPointAddress) {
        if (endPointAddress != null && endPointAddress.length() > 0) {
            return getDefaultSelfAddress().equalsIgnoreCase(endPointAddress.trim()) ? true : false;
        }
        else
            return false;
    }

    /**
     * Stores NegotiationManager instance in a Hashmap.
     * 
     * @param key
     *            string representing the negotiation session id
     * @param negotiationManager
     *            negotiationManager for the corresponding key
     */
    public void addToNegotiationManagerHashMap(String key, NegotiationManager negotiationManager) {
        negotiationManagerHashMap.put(key, negotiationManager);
    }

    /**
     * Removes NegotiationManager instance from the Hashmap.
     * 
     * @param key
     *            string representing the negotiation session id
     */
    public void removeFromNegotiationManagerHashMap(String key) {
        negotiationManagerHashMap.remove(key);
    }

    /****************************************** ProtocolEngine.INegotiation Operations follow **********************************************/

    /**
     * Initiates a negotiation session, using either the protocol of current SLA-Manager if present or the default one.
     * 
     * @param slaTemplate
     *            the slaTemplate used to setup negotiation session
     * @return String representing the id of the negotiation session established
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String initiateNegotiation(SLATemplate slaTemplate) throws OperationNotPossibleException {
        LOGGER.info("*** INFO *** MessageHandler.initiateNegotiation ***");
        String negotiationID = null;
        NegotiationManager negotiationManager = null;
        try {
            assert (slaTemplate != null) : "slaTemplate should not be null";
            if (slaTemplate != null) {
                // 1- validate SLATemplate, call authenticate on BM as well.
                negotiationManager = new NegotiationManager(this);
                negotiationID = negotiationManager.initiateNegotiation(slaTemplate);
                LOGGER.info(" ProtocolEngine.MessageHandler.initiateNegotiation() invoked");
            }
        }
        catch (OperationNotPossibleException e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
            throw e;
        }
        return negotiationID;
    }

    /**
     * Negotiates with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing an offer
     * @return SLATemplate array representing counter offers returned
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            OperationNotPossibleException, InvalidNegotiationIDException {
        // 1- Handle Message related logic first.
        // 2- Handover to appropriate NegotiationManager and return what it returns.
        SLATemplate[] slaTemplates = null;
        try {
            assert (negotiationID != null && slaTemplate != null) : "Negotiation ID AND SLATemplate should not be null";
            if (negotiationID != null && slaTemplate != null) {
                if (this.negotiationManagerHashMap.containsKey(negotiationID)) {
                    slaTemplates =
                            this.negotiationManagerHashMap.get(negotiationID).negotiate(negotiationID, slaTemplate);
                }
                else {
                    throw new InvalidNegotiationIDException("Negotiation ID does not exist.");
                }
            }
        }
        catch (OperationNotPossibleException nNPE) {
            LOGGER.info(nNPE.getMessage());
            nNPE.printStackTrace();
            throw nNPE;
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        return slaTemplates;
    }

    /**
     * Creates agreement with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the final offer
     * @return SLA created, if at all
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws SLACreationException
     *             if sla could not be created
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLA createAgreement(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            SLACreationException, InvalidNegotiationIDException {
        SLA finalSLA = null;
        try {
            assert (negotiationID != null && slaTemplate != null) : "Negotiation ID AND SLATemplate should not be null";
            if (negotiationID != null && slaTemplate != null) {
                if (this.negotiationManagerHashMap.containsKey(negotiationID)) {
                    finalSLA =
                            this.negotiationManagerHashMap.get(negotiationID).createAgreement(negotiationID,
                                    slaTemplate);
                }
                else {
                    throw new InvalidNegotiationIDException("Negotiation ID does not exist.");
                }
            }
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        catch (SLACreationException slaCE) {
            LOGGER.info(slaCE.getMessage());
            slaCE.printStackTrace();
            throw slaCE;
        }
        return finalSLA;
    }

    /**
     * Cancels ongoing negotiation with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param cancellationReason
     *            list of cancellation reasons that must be provided
     * @return boolean value showing if negotiation could be cancelled or not
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public boolean cancelNegotiation(String negotiationID, List<CancellationReason> cancellationReason)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException {
        boolean result = false;
        try {
            assert (negotiationID != null && cancellationReason != null) : "Negotiation ID AND cancellationReason should not be null";
            if (negotiationID != null && cancellationReason != null) {
                if (this.negotiationManagerHashMap.containsKey(negotiationID)) {
                    result =
                            this.negotiationManagerHashMap.get(negotiationID).cancelNegotiation(negotiationID,
                                    cancellationReason);
                }
                else {
                    throw new InvalidNegotiationIDException("Negotiation ID does not exist.");
                }
            }
        }
        catch (OperationNotPossibleException oNPE) {
            LOGGER.info(oNPE.getMessage());
            oNPE.printStackTrace();
            throw oNPE;
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        return result;
    }

    /**
     * Renegotiates with the SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to renegotiate over
     * @return String representing the id of the negotiation session established
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String renegotiate(UUID slaID) throws SLANotFoundException, OperationNotPossibleException {
        // For renegotiation, use the slaID as the key for negotiationManagerHashMap! This will ensure that at anytime,
        // no two renegotiations take place for the same SLA. Otherwise raise OperationInProgressException.
        String renegotiationId = null;
        NegotiationManager negotiationManager = null;
        try {
            assert (slaID != null) : "SLA ID should not be null";
            if (slaID != null) {
                if (!this.negotiationManagerHashMap.containsKey(slaID.getValue())) {
                    negotiationManager = new NegotiationManager(this);
                    renegotiationId = negotiationManager.renegotiate(slaID);
                }
                else {
                    throw new OperationNotPossibleException("Renegotiation already ongoing");
                }
            }
        }
        catch (OperationNotPossibleException oNPE) {
            LOGGER.info(oNPE.getMessage());
            oNPE.printStackTrace();
            throw oNPE;
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        return renegotiationId;
    }

    /**
     * Terminates SLA with the SLA-Manager. This operation is independent of negotiation session.
     * 
     * @param slaID
     *            the uuid of the sla to terminate
     * @param terminationReasons
     *            list of termination reasons that must be provided
     * @return boolean value showing if termination was successful or not
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     */
    public boolean terminate(UUID slaID, List<TerminationReason> terminationReasons) throws SLANotFoundException {
        boolean result = false;
        try {
            assert (slaID != null && terminationReasons != null) : "SLA ID AND terminationReasons should not be null";
            if (slaID != null && terminationReasons != null) {
                // 1- validate SLA by querying the SLARegistryIQueryImpl.getSLA(new UUID[]{slaID});
                NegotiationManager negotiationManager = new NegotiationManager(this);
                result = negotiationManager.terminate(slaID, terminationReasons);
            }
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        return result;
    }

    /**
     * This method is here for legacy reasons. It is not to be used.
     * <p>
     * Provisioning is related to <code>SLARegistry</code> and <code>ProvisioningAdjustment</code>. Provisioning is
     * independent of negotiation session.
     * 
     * @param slaID
     *            the uuid of the sla to provision
     * @return SLA with provisioning information, if at all
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     * @throws ProvisioningException
     *             if provisioning was not possible
     * @see org.slasoi.gslam.core.poc.PlanningOptimization.INotification.activate(SLA sla)
     * @see org.slasoi.gslam.core.pac.ProvisioningAdjustment
     */
    public SLA provision(UUID slaID) throws SLANotFoundException, ProvisioningException {
        SLA provisionedSLA = null;
        try {
            assert (slaID != null) : "SLA ID should not be null";
            if (slaID != null) {
                NegotiationManager negotiationManager = new NegotiationManager(this);
                provisionedSLA = negotiationManager.provision(slaID);
            }
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        catch (ProvisioningException pE) {
            LOGGER.info(pE.getMessage());
            pE.printStackTrace();
            throw pE;
        }
        return provisionedSLA;
    }

    /****************************************** INegotiationOutgoing Operations follow ********************************************************/

    /**
     * Invokes initiate negotiation between current SLA-Manager and destination SLA-Manager.
     * 
     * @param slaTemplate
     *            the slaTemplate used to setup negotiation session
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return String representing the id of the negotiation session established
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String initiateNegotiationOutgoing(SLATemplate slaTemplate, String endpointAddress)
            throws OperationNotPossibleException {
        String negId = "";
        // Post to Provider:
        LOGGER.info("C- About to Post initiate() to Provider");
        INegotiation negotiationClient;
        try {
            LOGGER.info("C- Getting NegotiationClient");
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            OperationNotPossibleException oNPE =
                    new OperationNotPossibleException("Failed to get the NegotiationStub object");
            oNPE.setCause(e.getCause());
            throw oNPE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            negId = negotiationClient.initiateNegotiation(slaTemplate);
        }
        catch (OperationNotPossibleException oNPE) {
            oNPE.getMessage();
            oNPE.printStackTrace();
            throw oNPE;
        }
        return negId;
    }

    /**
     * Invokes negotiate on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the offer to be made
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLATemplate array representing counter offers received from the other SLA-Manager
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLATemplate[] negotiateOutgoing(String negotiationID, SLATemplate slaTemplate, String endpointAddress)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException {
        // Invoke negotiate() operation on negotiating party and return results to NegotiationManager. To be
        // conducted by SyntaxConverter.
        SLATemplate[] slaTemplates = new SLATemplate[] {};
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            OperationNotPossibleException oNPE =
                    new OperationNotPossibleException("Failed to get the NegotiationStub object");
            oNPE.setCause(e.getCause());
            throw oNPE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post negotiate() to Provider");
            slaTemplates = negotiationClient.negotiate(negotiationID, slaTemplate);
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        catch (OperationNotPossibleException nNPE) {
            LOGGER.info(nNPE.getMessage());
            nNPE.printStackTrace();
            throw nNPE;
        }
        catch (InvalidNegotiationIDException iNIDE) {
            LOGGER.info(iNIDE.getMessage());
            iNIDE.printStackTrace();
            throw iNIDE;
        }
        catch (Exception e) {
            e.printStackTrace();
            OperationNotPossibleException oNPE = new OperationNotPossibleException(e.getMessage());
            oNPE.setCause(e.getCause());
            throw oNPE;
        }
        return slaTemplates;
    }

    /**
     * Invokes createAgreement on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the final offer to be made
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLA created, if at all
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws SLACreationException
     *             if sla could not be created
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLA createAgreementOutgoing(String negotiationID, SLATemplate slaTemplate, String endpointAddress)
            throws OperationInProgressException, SLACreationException, InvalidNegotiationIDException {
        SLA sla;
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            SLACreationException slaCE = new SLACreationException("Failed to get the NegotiationStub object");
            slaCE.setCause(e.getCause());
            throw slaCE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post createAgreement() to Provider");
            sla = negotiationClient.createAgreement(negotiationID, slaTemplate);
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        catch (SLACreationException slaCE) {
            LOGGER.info(slaCE.getMessage());
            slaCE.printStackTrace();
            throw slaCE;
        }
        catch (InvalidNegotiationIDException iNIDE) {
            LOGGER.info(iNIDE.getMessage());
            iNIDE.printStackTrace();
            throw iNIDE;
        }
        return sla;
    }

    /**
     * Invokes cancelNegotiation on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param cancellationReason
     *            list of cancellation reasons that must be provided
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return boolean value showing if negotiation could be cancelled or not
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public boolean cancelNegotiationOutgoing(String negotiationID, List<CancellationReason> cancellationReason,
            String endpointAddress) throws OperationInProgressException, OperationNotPossibleException,
            InvalidNegotiationIDException {
        boolean result = false;
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            OperationNotPossibleException oNPE =
                    new OperationNotPossibleException("Failed to get the NegotiationStub object");
            oNPE.setCause(e.getCause());
            throw oNPE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post cancelNegotiation() to Provider");
            result = negotiationClient.cancelNegotiation(negotiationID, cancellationReason);
        }
        catch (OperationInProgressException oIPE) {
            LOGGER.info(oIPE.getMessage());
            oIPE.printStackTrace();
            throw oIPE;
        }
        catch (OperationNotPossibleException oNPE) {
            LOGGER.info(oNPE.getMessage());
            oNPE.printStackTrace();
            throw oNPE;
        }
        catch (InvalidNegotiationIDException iNIDE) {
            LOGGER.info(iNIDE.getMessage());
            iNIDE.printStackTrace();
            throw iNIDE;
        }
        return result;
    }

    /**
     * Invokes renegotiate on destination SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to renegotiate over
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return String representing the id of the negotiation session established
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String renegotiateOutgoing(UUID slaID, String endpointAddress) throws SLANotFoundException,
            OperationNotPossibleException {
        String reNegId = "";
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            OperationNotPossibleException oNPE =
                    new OperationNotPossibleException("Failed to get the NegotiationStub object");
            oNPE.setCause(e.getCause());
            throw oNPE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post renegotiate() to Provider");
            reNegId = negotiationClient.renegotiate(slaID);
        }
        catch (OperationNotPossibleException oNPE) {
            LOGGER.info(oNPE.getMessage());
            oNPE.printStackTrace();
            throw oNPE;
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        return reNegId;
    }

    /**
     * Invokes terminate on destination SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to terminate
     * @param terminationReason
     *            list of termination reasons that must be provided
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return boolean value showing if termination was successful or not
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     */
    public boolean terminateOutgoing(UUID slaID, List<TerminationReason> terminationReasons, String endpointAddress)
            throws SLANotFoundException {
        boolean result = false;
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            SLANotFoundException slaNFE = new SLANotFoundException("Failed to get the NegotiationStub object");
            slaNFE.setCause(e.getCause());
            throw slaNFE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post terminate() to Provider");
            result = negotiationClient.terminate(slaID, terminationReasons);
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        return result;
    }

    /**
     * This method is only for legacy reasons. It is not to be used. Provisioning is related to SLARegistry and PAC.
     * 
     * @param slaID
     *            the uuid of the sla to provision
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLA with provisioning information, if at all
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     * @throws ProvisioningException
     *             if provisioning was not possible
     * @see org.slasoi.gslam.core.poc.PlanningOptimization.INotification.activate(SLA sla)
     * @see org.slasoi.gslam.core.pac.ProvisioningAdjustment
     */
    public SLA provisionOutgoing(UUID slaID, String endpointAddress) throws SLANotFoundException, ProvisioningException {
        SLA provisionedSLA;
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            SLANotFoundException slaNFE = new SLANotFoundException("Failed to get the NegotiationStub object");
            slaNFE.setCause(e.getCause());
            throw slaNFE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            // Post to Provider:
            LOGGER.info("C- About to Post provision() to Provider");
            provisionedSLA = negotiationClient.provision(slaID);
        }
        catch (SLANotFoundException slaNFE) {
            LOGGER.info(slaNFE.getMessage());
            slaNFE.printStackTrace();
            throw slaNFE;
        }
        catch (ProvisioningException pE) {
            LOGGER.info(pE.getMessage());
            pE.printStackTrace();
            throw pE;
        }
        return provisionedSLA;
    }

    public Customization customizeOutgoing(String negotiationID, Customization customization, String endpointAddress)
            throws NegotiationException {
        Customization customizationResult = null;
        INegotiation negotiationClient;
        try {
            negotiationClient = this.getSyntaxConverter().getNegotiationClient(endpointAddress);
        }
        catch (Exception e) {
            e.printStackTrace();
            SLANotFoundException slaNFE = new SLANotFoundException("Failed to get the NegotiationStub object");
            slaNFE.setCause(e.getCause());
            throw slaNFE;
        }
        try {
            assert (negotiationClient != null) : "Negotiation Client is null";
            if (negotiationClient != null) {
                // Post to Provider:
                LOGGER.info("C- About to Post customize() to Provider");
                customizationResult = negotiationClient.customize(negotiationID, customization);
            }
        }
        catch (NegotiationException nE) {
            LOGGER.info(nE.getMessage());
            nE.printStackTrace();
            throw nE;
        }

        return customizationResult;
    }

    public Customization customize(String negotiationID, Customization customization) throws NegotiationException {
        Customization customizationResult = null;
        try {
            assert (negotiationID != null) : "Negotiation ID should not be null";
            if (negotiationID != null) {
                if (this.negotiationManagerHashMap.containsKey(negotiationID)) {
                    customizationResult =
                            this.negotiationManagerHashMap.get(negotiationID).customize(negotiationID, customization);
                }
                else {
                    throw new InvalidNegotiationIDException("Negotiation ID does not exist.");
                }
            }
        }
        catch (NegotiationException nE) {
            LOGGER.info(nE.getMessage());
            nE.printStackTrace();
            throw nE;
        }
        return customizationResult;
    }

    public Object getNegotiationManager(String id) {
        return this.negotiationManagerHashMap.get(id);
    }
}
