package org.slasoi.gslam.protocolengine.persistence.profiles;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CollectionOfElements;

@Entity
public class Status implements java.io.Serializable{
    
    private static final long serialVersionUID = 4111452573282024049L;
    private Integer id;
    private boolean isBlackListed;
    private Set<Reason> blackListingReason;
    private boolean isWhiteListed;
    private Set<Reason> whiteListingReason;
    
    public Status(){
        
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(nullable=true)
    public boolean isBlackListed() {
        return isBlackListed;
    }

    public void setBlackListed(boolean isBlackListed) {
        this.isBlackListed = isBlackListed;
    }


    @CollectionOfElements
    @Column(nullable=true)
    public Set<Reason> getBlackListingReason() {
        return blackListingReason;
    }

    public void setBlackListingReason(Set<Reason> blackListingReason) {
        this.blackListingReason = blackListingReason;
    }

    @Column(nullable=true)
    public boolean isWhiteListed() {
        return isWhiteListed;
    }

    public void setWhiteListed(boolean isWhiteListed) {
        this.isWhiteListed = isWhiteListed;
    }

    @CollectionOfElements
    @Column(nullable=true)
    public Set<Reason> getWhiteListingReason() {
        return whiteListingReason;
    }

    public void setWhiteListingReason(Set<Reason> whiteListingReason) {
        this.whiteListingReason = whiteListingReason;
    }
    
}
