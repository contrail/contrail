/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1863 $
 * @lastrevision   $Date: 2011-05-25 18:45:33 +0200 (sre, 25 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/ProtocolEngineServices.java $
 */

package org.slasoi.gslam.protocolengine;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slasoi.gslam.core.builder.ProtocolEngineBuilder;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.control.IControl;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.protocolengine.impl.IControlImpl;
import org.slasoi.gslam.protocolengine.impl.MessageHandler;
import org.slasoi.gslam.protocolengine.persistence.HibernateUtil;
import org.slasoi.gslam.protocolengine.persistence.profiles.BusinessHistory;
import org.slasoi.gslam.protocolengine.persistence.profiles.Profile;

/**
 * The <code>ProtocolEngineServices</code> is an interface for the <code>ProtocolEngine</code> component.
 * <p>
 * Provides an implementation of <code>INegotiation</code> and <code>IControl</code> interfaces.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public class ProtocolEngineServices implements ProtocolEngine, ProtocolEngineBuilder, SLAMContextAware {

    @SuppressWarnings("unused")
    private SLAManagerContext context;
    private MessageHandler messageHandler;
    private IControlImpl iControlImpl;
    private static final Logger LOGGER = Logger.getLogger(ProtocolEngineServices.class);

    /**
     * Default constructor.
     */
    public ProtocolEngineServices() {
        assert (System.getenv("SLASOI_HOME") != null) : "Environment Variable SLASOI_HOME not set!";
        System.setProperty("SLASOI_HOME", System.getenv("SLASOI_HOME"));
        PropertyConfigurator.configure(System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                + "generic-slamanager" + System.getProperty("file.separator") + "protocol-engine"
                + System.getProperty("file.separator") + "protocol-engine-config"
                + System.getProperty("file.separator") + "log4j.properties");
        LOGGER.info("*** INFO *** ProtocolEngineServices - Constructor ***");
        this.messageHandler = new MessageHandler();
        this.iControlImpl = new IControlImpl();
    }

    /**
     * Gives a handle on <code>INegotiation</code> interface of current SLA-Manager.
     * 
     * @return INegotiation implementation
     */
    public INegotiation getINegotiation() {
        return this.messageHandler;
    }

    /**
     * Gives a handle on <code>IControl</code> interface of current SLA-Manager.
     * 
     * @return IControl implementation
     */
    public IControl getIControl() {
        return this.iControlImpl;
    }

    /**
     * Creates a new <code>ProtocolEngine</code>. This method will be invoked only from Generic SLA-Manager during
     * creation of a its context.
     * 
     * @return ProtocolEngine implementation
     */
    public ProtocolEngine create() {
        ProtocolEngine protocolEngine = new ProtocolEngineServices();
        return protocolEngine;
    }

    /**
     * Sets the SLA-Manager context in the <code>ProtocolEngine</code>
     * 
     * @param context
     *            the slaManagerContext
     */
    public void setSLAManagerContext(SLAManagerContext context) {
        this.context = context;
        this.messageHandler.setContext(context);
        this.iControlImpl.setContext(context);
        LOGGER.info("*** INFO *** ProtocolEngineServices - CONTEXT IS SET ***");
    }

    public Object getNegotiationManager(String negotiationId) {
        if (this.messageHandler != null) {
            return this.messageHandler.getNegotiationManager(negotiationId);
        }
        return null;
    }

    @Override
    public void appendBusinessHistoryToProfile(String profileId, Object businessHistory) throws Exception {
        Profile profile = null;
        try {
            Session hSession = HibernateUtil.getSession();
            hSession.beginTransaction();

            List list = hSession.createQuery("from Profile where profileId='" + profileId + "'").list();
            if (list != null && list.size() > 0) {
                profile = (Profile) list.get(0); // 1 item expected per primary-key
            }

            if (profile != null) {
                profile.getBusinessHistory().add((BusinessHistory) businessHistory);
                hSession.update(profile);
            }

            hSession.getTransaction().commit();
            hSession.close();
        }
        catch (HibernateException hE) {
            hE.printStackTrace();
            throw hE;
        }
    }

    @Override
    public Object retrieveProfile(String profileId) throws Exception {
        Profile profile = null;
        try {
            Session hSession = HibernateUtil.getSession();
            hSession.beginTransaction();

            List list = hSession.createQuery("from Profile where profileId='" + profileId + "'").list();
            if (list != null && list.size() > 0) {
                profile = (Profile) list.get(0); // 1 item expected per primary-key
            }
            hSession.getTransaction().commit();
            hSession.close();
        }
        catch (HibernateException hE) {
            hE.printStackTrace();
            throw hE;
        }
        return profile;
    }

    @Override
    public void updateProfile(Object profile) throws Exception {
        try {
            Session hSession = HibernateUtil.getSession();
            hSession.beginTransaction();

            Profile receivedProfile = (Profile) profile;
            Profile profileFromDB = null;

            List list =
                    hSession.createQuery("from Profile where profileId='" + receivedProfile.getProfileId() + "'")
                            .list();
            if (list != null && list.size() > 0) {
                profileFromDB = (Profile) list.get(0); // 1 item expected per primary-key
            }

            profileFromDB.setAnnotations(receivedProfile.getAnnotations());
            profileFromDB.setBusinessHistory(receivedProfile.getBusinessHistory());
            profileFromDB.setLastModified(receivedProfile.getLastModified());
            profileFromDB.setStatus(receivedProfile.getStatus());

            hSession.update(profileFromDB);
            hSession.getTransaction().commit();
            hSession.close();
        }
        catch (HibernateException hE) {
            hE.printStackTrace();
            throw hE;
        }
    }

    @Override
    public int createProfile(Object profile) throws Exception {
        Integer profileId = null;
        try {
            Session hSession = HibernateUtil.getSession();
            hSession.beginTransaction();

            Profile receivedProfile = (Profile) profile;
            profileId = (Integer) hSession.save(receivedProfile);

            hSession.getTransaction().commit();
            hSession.close();

        }
        catch (HibernateException hE) {
            hE.printStackTrace();
            throw hE;
        }
        return profileId.intValue();
    }
}
