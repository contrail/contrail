package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum ProfileType {
    
    CUSTOMER,
    PROVIDER,
    PRODUCT

}
