/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 2116 $
 * @lastrevision   $Date: 2011-06-09 15:56:14 +0200 (Ä�et, 09 jun 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/NegotiationManager.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.INegotiation.Customization;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidStateException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.protocolengine.INegotiationIncoming;
import org.slasoi.gslam.protocolengine.IStateEngine;
import org.slasoi.gslam.protocolengine.impl.SimplifiedNegotiableParameters.Ownership;
import org.slasoi.gslam.protocolengine.persistence.HibernateUtil;
import org.slasoi.gslam.protocolengine.persistence.profiles.*;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.sla;

/**
 * The <code>NegotiationManager</code> class processes the negotiation related requests received by the SLA-Manager.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public class NegotiationManager implements INegotiationIncoming {

    private IStateEngine stateEngine;
    private NegotiationSession session;
    private MessageHandler messageHandler;
    private static final Logger LOGGER = Logger.getLogger(NegotiationManager.class);
    private final STND GSLAM_EPR = org.slasoi.slamodel.vocab.sla.gslam_epr;
    // Standard messages:
    private final String CUSTOMER_OP_DISALLOWED_BY_PROTOCOL =
            "The Operation was not allowed by the Negotiation Protocol at Customer side.";
    private final String PROVIDER_OP_DISALLOWED_BY_PROTOCOL =
            "The Operation was not allowed by the Negotiation Protocol at Provider side.";
    private final String AUTHORIZATION_FAILED = "Authorization Failed : Access not granted.";
    private final String CUSTOMER_EPR_MISSING = "The EPR of the Customer party is missing";
    private final String PROVIDER_EPR_MISSING = "The EPR of the Provider party is missing";
    private final String CONSENSUS_NOT_REACHED_IN_CUSTOMIZATION_PHASE =
            "Cannot Negotiate as consensus not reached in Customization phase";
    private boolean performProfiling = false; // Default behaviour will not profile negotiating parties.

    public NegotiationManager(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        String profile = System.getProperty("gslam.protocolengine.profiling");
        if (profile != null && profile.equals("true")) {
            this.performProfiling = true;
        }

        LOGGER.info("*** INFO *** NegotiationManager - Constructor ***");
    }

    /****************************************** INegotiationIncoming Operations follow **********************************************/

    /**
     * Initiates a negotiation session, using either the protocol of current SLA-Manager (if present) or the default
     * one.
     * 
     * @param slaTemplate
     *            the slaTemplate used to setup negotiation session
     * @return String representing the id of the negotiation session established
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String initiateNegotiation(SLATemplate slaTemplate) throws OperationNotPossibleException {
        String negotiationId = null;
        if (slaTemplate != null) {
            Party[] parties = slaTemplate.getParties();
            assert (parties != null && parties.length > 0) : "Partys null or empty";
            if (parties != null && parties.length > 0) {
                Party providerParty = null;
                Party customerParty = null;
                for (Party party : parties) {
                    STND partyRole = (STND) party.getAgreementRole();
                    if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                        providerParty = party;
                    }
                    else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                        customerParty = party;
                    }
                }
                // Read EPR-Annotation:
                String providerAddress = null;
                String customerAddress = null;
                assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
                if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                    providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new OperationNotPossibleException(PROVIDER_EPR_MISSING);
                }
                assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
                if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                    customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new OperationNotPossibleException(CUSTOMER_EPR_MISSING);
                }

                if (this.messageHandler.isSelfAddressed(providerAddress)) {
                    LOGGER.info("P- I'm the Provider");

                    if (this.performProfiling) {
                        CustomerProfile cProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();

                            List list =
                                    hSession.createQuery("from Profile where profileId='" + customerAddress + "'")
                                            .list();
                            if (list != null && list.size() > 0) {
                                cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key
                            }

                            if (cProfile == null) {
                                // Profile does not exist, create one:
                                cProfile = new CustomerProfile();
                                cProfile.setProfileId(customerAddress);
                                Integer profileId = (Integer) hSession.save(cProfile);
                                cProfile.setId(profileId);
                            }
                            hSession.getTransaction().commit();
                            hSession.close();
                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }
                    // 0- Authorize customer:
                    assert (this.messageHandler.checkAccess(slaTemplate)) : "AUTHORIZATION_FAILED";
                    if (!this.messageHandler.checkAccess(slaTemplate)) {
                        // Provider's authorization does not allow this customer to proceed!
                        throw new OperationNotPossibleException(AUTHORIZATION_FAILED);
                    }
                    // 1- Initialize the NegotiationSession with whatever info we got as of now + a NegotiationID.
                    this.session = new NegotiationSession();
                    this.session.generateAndSetUuid();
                    LOGGER.info("P- I generated the session ID = " + this.session.getUuid());
                    this.session.addToInvolvedParties(slaTemplate);
                    this.session.addTemplateUUIDInSession(slaTemplate.getUuid().getValue());
                    // 2- Involve StateEngine to spawn the State Machine (according to the protocol) by sending the
                    // StartNegotiationEvent.
                    this.stateEngine = new StateEngine();
                    this.stateEngine.setContext(this.messageHandler.getContext());
                    Event startNegotiationEvent = new Event(EventName.StartNegotiationEvent);
                    // startNegotiationEvent.setPartnerProfile(cProfile);
                    startNegotiationEvent.setOffer(slaTemplate);
                    List<Event> listOfEvents = new ArrayList<Event>();
                    listOfEvents.add(startNegotiationEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {

                        if (this.performProfiling) {
                            listOfEvents = new ArrayList<Event>();
                            Event updateNegotiationEvent = new Event(EventName.UpdateNegotiationEvent);
                            NegotiationHistory negotiationHistory = new NegotiationHistory();
                            negotiationHistory.setStartTime(new Timestamp(System.currentTimeMillis()));
                            negotiationHistory.setSlaTemplateId(slaTemplate.getUuid().toString());
                            negotiationHistory.setNegotiationId(this.session.getUuid());
                            CustomerProfile cProfile = null;
                            try {
                                Session hSession = HibernateUtil.getSession();
                                hSession.beginTransaction();

                                List list =
                                        hSession.createQuery("from Profile where profileId='" + customerAddress + "'")
                                                .list();
                                if (list != null && list.size() > 0) {
                                    cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key
                                }

                                if (cProfile.getNegotiationHistory() != null) {
                                    cProfile.getNegotiationHistory().add(negotiationHistory);
                                }
                                else {
                                    Set<NegotiationHistory> negotiationHistories = new HashSet<NegotiationHistory>();
                                    negotiationHistories.add(negotiationHistory);
                                    cProfile.setNegotiationHistory(negotiationHistories);
                                }

                                if (cProfile.getBusinessHistory() != null) {
                                    // Instigating lazy loading on BusinessHistory:
                                    LOGGER.info("P- Size of BusinessHistory = " + cProfile.getBusinessHistory().size());
                                }

                                updateNegotiationEvent.setPartnerProfile(cProfile);
                                updateNegotiationEvent.setNegotiationId(this.session.getUuid());
                                updateNegotiationEvent.setOffer(slaTemplate);

                                listOfEvents.add(updateNegotiationEvent);// Passing Profile object to compute ranks
                                listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                                // Update the CustomerProfile as rules have now computed Ranks
                                CustomerProfile cProfileWithRanks =
                                        (CustomerProfile) listOfEvents.get(0).getPartnerProfile();

                                cProfileWithRanks.setLastModified(new Timestamp(System.currentTimeMillis()));
                                hSession.update(cProfileWithRanks);
                                hSession.getTransaction().commit();
                                hSession.close();

                            }
                            catch (HibernateException he) {
                                he.printStackTrace();
                            }
                        }
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            // 3- Put this NegotiationManager into the MessageHandler.negotiationManagerHashMap
                            // MessageHandler.getInstance().addToNegotiationManagerHashMap(session.getUuid(), this);
                            this.messageHandler.addToNegotiationManagerHashMap(this.session.getUuid(), this);
                            LOGGER.info("P- Just added NegotiationManager to MessageHandler");
                            // 4- Return the NegotiationID.
                            negotiationId = this.session.getUuid();
                            return negotiationId;
                        }
                        else {
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new OperationNotPossibleException(message != null ? message
                                    : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                    }
                    else {
                        // Session could not be established!
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify the reason:
                        // e.g., blacklisted party,
                        // blocked until, DOS attack, etc.
                        // whatever rules determine.
                        throw new OperationNotPossibleException(message != null ? message
                                : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                    // Case b) partyAddress != me => I'm Customer
                    LOGGER.info("C- I'm the Customer");

                    if (this.performProfiling) {
                        ProviderProfile pProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();

                            List list =
                                    hSession.createQuery("from Profile where profileId='" + providerAddress + "'")
                                            .list();

                            if (list != null && list.size() > 0) {
                                pProfile = (ProviderProfile) list.get(0);
                            }

                            if (pProfile == null) {
                                // Profile does not exist, create one:
                                pProfile = new ProviderProfile();
                                pProfile.setProfileId(providerAddress);
                                Integer profileId = (Integer) hSession.save(pProfile);
                                pProfile.setId(profileId);
                            }
                            hSession.getTransaction().commit();
                            hSession.close();
                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }
                    // 1- Initialize a NegotiationSession with whatever information we have as of now minus the
                    // NegotiationID
                    this.session = new NegotiationSession();
                    this.session.addToInvolvedParties(slaTemplate);
                    this.session.addTemplateUUIDInSession(slaTemplate.getUuid().getValue());
                    // 2- Involve StateEngine to spawn the State Machine (according to the protocol) by sending the
                    // StartNegotiationEvent.
                    this.stateEngine = new StateEngine();
                    this.stateEngine.setContext(this.messageHandler.getContext());
                    Event startNegotiationEvent = new Event(EventName.StartNegotiationEvent);
                    List<Event> listOfEvents = new ArrayList<Event>();
                    startNegotiationEvent.setOffer(slaTemplate);
                    listOfEvents.add(startNegotiationEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        // 3- Call the initiate of the Provider: MessageHandler.initiateNegotiationOutgoing().
                        LOGGER.info("C- Before calling initiate() on provider");
                        negotiationId = this.messageHandler.initiateNegotiationOutgoing(slaTemplate, providerAddress);

                        assert (negotiationId != null) : "negotiationId is null";
                        LOGGER.info("C- After calling initiate() on provider. Got NegID = " + negotiationId);
                        // 4- Set the NegotiationSession.id = the returned NegotiationID from Provider. Also put
                        // yourself in the session partyList.
                        this.session.setUuid(negotiationId);

                        if (this.performProfiling) {
                            // 5- Send a UpdateNegotiationEvent to StateEngine.
                            listOfEvents.removeAll(listOfEvents);
                            listOfEvents = new ArrayList<Event>();
                            Event updateNegotiationEvent = new Event(EventName.UpdateNegotiationEvent);
                            NegotiationHistory negotiationHistory = new NegotiationHistory();
                            negotiationHistory.setStartTime(new Timestamp(System.currentTimeMillis()));
                            negotiationHistory.setSlaTemplateId(slaTemplate.getUuid().toString());
                            negotiationHistory.setNegotiationId(this.session.getUuid());

                            try {
                                Session hSession = HibernateUtil.getSession();
                                hSession.beginTransaction();
                                ProviderProfile pProfile = null;
                                List list =
                                        hSession.createQuery("from Profile where profileId='" + providerAddress + "'")
                                                .list();

                                if (list != null && list.size() > 0) {
                                    pProfile = (ProviderProfile) list.get(0);
                                }

                                if (pProfile.getNegotiationHistory() != null) {
                                    pProfile.getNegotiationHistory().add(negotiationHistory);
                                }
                                else {
                                    Set<NegotiationHistory> negotiationHistories = new HashSet<NegotiationHistory>();
                                    negotiationHistories.add(negotiationHistory);
                                    pProfile.setNegotiationHistory(negotiationHistories);
                                }

                                if (pProfile.getBusinessHistory() != null) {
                                    // Instigating lazy loading on BusinessHistory:
                                    LOGGER.info("C- Size of BusinessHistory = " + pProfile.getBusinessHistory().size());
                                }

                                updateNegotiationEvent.setPartnerProfile(pProfile);
                                updateNegotiationEvent.setNegotiationId(this.session.getUuid());
                                updateNegotiationEvent.setOffer(slaTemplate);
                                listOfEvents.add(updateNegotiationEvent);// Passing in Profile object to compute ranks
                                // fact in working memory.
                                listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                                // Update the ProviderProfile as rules have now computed Ranks
                                ProviderProfile pProfileWithRanks =
                                        (ProviderProfile) listOfEvents.get(0).getPartnerProfile();

                                pProfileWithRanks.setLastModified(new Timestamp(System.currentTimeMillis()));
                                hSession.update(pProfileWithRanks);
                                hSession.getTransaction().commit();
                                hSession.close();

                            }
                            catch (HibernateException he) {
                                he.printStackTrace();
                            }
                        }
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            // 6- Only now make this NegotiationManager available into
                            // MessageHandler.negotiationManagerHashMap
                            this.messageHandler.addToNegotiationManagerHashMap(this.session.getUuid(), this);
                            LOGGER.info("C- Just added NegotiationManager to MessageHandler");
                            // 7- Return the NegotiationID to original caller.
                            return negotiationId;
                        }
                        else {
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new OperationNotPossibleException(message != null ? message
                                    : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                        }

                    }
                    else {
                        // Session could not be established!
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
            }

        }
        return negotiationId;
    }

    /**
     * Negotiates with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing an offer
     * @return SLATemplate array representing counter offers returned
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            OperationNotPossibleException, InvalidNegotiationIDException {
        SLATemplate[] counterOffers = null;
        if (negotiationID != null && slaTemplate != null
                && this.session.isTemplateUUIDInSession(slaTemplate.getUuid().getValue())) {
            Party[] parties = slaTemplate.getParties();

            assert (parties != null && parties.length > 0) : "Partys null or empty";
            if (parties != null && parties.length > 0) {
                Party providerParty = null;
                Party customerParty = null;
                for (Party party : parties) {
                    STND partyRole = (STND) party.getAgreementRole();
                    if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                        providerParty = party;
                    }
                    else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                        customerParty = party;
                    }
                }
                // Read EPR-Annotation:
                String providerAddress = null;
                String customerAddress = null;
                assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
                if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                    providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new OperationNotPossibleException(PROVIDER_EPR_MISSING);
                }
                assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
                if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                    customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new OperationNotPossibleException(CUSTOMER_EPR_MISSING);
                }

                // Incase Customization was used, check if it resulted in a consensus. Block otherwise.
                if (this.session.isCustomizationUsed()) {
                    if (this.session.isConsensusReachedOnCustomization() == false) {
                        throw new OperationNotPossibleException(CONSENSUS_NOT_REACHED_IN_CUSTOMIZATION_PHASE);
                    }
                }

                if (this.messageHandler.isSelfAddressed(providerAddress)) {
                    // 1- Change status to busy so any simultaneous call wont be entertained and will generate a
                    // OperationInProgress Exception.
                    if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                        this.session.setStatus(SessionStatus.BUSY);

                        // 2- Invoke the SM, pass in Event(s).
                        Event proposalArrivedEvent = new Event(EventName.ProposalArrivedEvent);
                        proposalArrivedEvent.setOffer(slaTemplate);
                        List<Event> listOfEvents = new ArrayList<Event>();
                        listOfEvents.add(proposalArrivedEvent);
                        listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                        // 3- Process the results.
                        LOGGER.info("P- " + listOfEvents.get(0).isProcessedSuccessfully());// If this tells us that
                        // everything is ok, go
                        // ahead, else not.
                        assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            // 4- Update NegotiationSession
                            this.session.addToReceivedProposals(new SLATemplate[] { slaTemplate });

                            if (this.session.getPreNegotiationOrNegotiationStartTime() == null
                                    && listOfEvents.get(0).getSimplifiedNegotiableParameters() != null) {
                                LOGGER
                                        .info("P - negotiate() - SETTING PRENEG-NEG-START_TIME + Customization IN SESSION ");
                                this.session
                                        .setCustomization(createCustomizationFromSimplifiedNegotiableParameters(listOfEvents
                                                .get(0).getSimplifiedNegotiableParameters()));
                            }
                            else {
                                LOGGER
                                        .info("P - negotiate() - NOT SETTING PRENEG-NEG-START_TIME + Customization IN SESSION. Already Set.");
                            }
                            // 5-Invoke POC via IAssessmentAndCustomize.negotiate() and return back counter-offers which
                            // POC returns.
                            try {
								counterOffers =
								        (SLATemplate[]) this.messageHandler.getPlanningOptimization()
								                .getIAssessmentAndCustomize().negotiate(negotiationID, slaTemplate);
							} catch (Exception e) {
								e.printStackTrace();
								throw new OperationNotPossibleException(e.getMessage());
							}
                            this.session.addToSentProposals(counterOffers);
                            // Provider may only generate counter offers where ProviderParty's EPR is set to his.
                            this.addSLATemplatesUUIDInSession(counterOffers, providerAddress);

                            if (this.performProfiling) {
                                CustomerProfile cProfile = null;
                                try {
                                    Session hSession = HibernateUtil.getSession();
                                    hSession.beginTransaction();

                                    List list =
                                            hSession.createQuery(
                                                    "from Profile where profileId='" + customerAddress + "'").list();
                                    if (list != null && list.size() > 0) {
                                        cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key

                                        if (cProfile != null && cProfile.getNegotiationHistory() != null) {
                                            Set<NegotiationHistory> negotiationHistories =
                                                    cProfile.getNegotiationHistory();
                                            for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                                if (negotiationHistory != null
                                                        && negotiationHistory.getNegotiationId().equalsIgnoreCase(
                                                                negotiationID)) {
                                                    negotiationHistory.setNegotiationRounds(negotiationHistory
                                                            .getNegotiationRounds() + 1);
                                                }
                                            }
                                        }
                                        cProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                        hSession.update(cProfile);
                                    }
                                    hSession.getTransaction().commit();
                                    hSession.close();
                                }
                                catch (HibernateException he) {
                                    he.printStackTrace();
                                }
                            }

                            this.session.setStatus(SessionStatus.AVAILABLE);
                            return counterOffers;
                        }
                        else {
                            this.session.setStatus(SessionStatus.AVAILABLE);// Make Session available even though this
                            // operation didnt go completely fine.
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new OperationNotPossibleException(message != null ? message
                                    : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                    }
                    else {
                        throw new OperationInProgressException("Session in use");
                    }
                }

                else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                    // 1- Change status to busy so any simultaneous call wont be entertained and will generate a
                    // OperationInProgress Exception.
                    if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                        this.session.setStatus(SessionStatus.BUSY);
                        // 2- Invoke the SM, pass in Event(s).
                        Event sendProposalEvent = new Event(EventName.SendProposalEvent);
                        sendProposalEvent.setOffer(slaTemplate);
                        List<Event> listOfEvents = new ArrayList<Event>();
                        listOfEvents.add(sendProposalEvent);
                        listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                        // 3- Process the results
                        LOGGER.info("C- " + listOfEvents.get(0).isProcessedSuccessfully());
                        assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {

                            if (this.session.getPreNegotiationOrNegotiationStartTime() == null
                                    && listOfEvents.get(0).getSimplifiedNegotiableParameters() != null) {
                                LOGGER
                                        .info("C - negotiate() - SETTING PRENEG-NEG-START_TIME + Customization IN SESSION ");
                                this.session
                                        .setCustomization(createCustomizationFromSimplifiedNegotiableParameters(listOfEvents
                                                .get(0).getSimplifiedNegotiableParameters()));
                            }
                            else {
                                LOGGER
                                        .info("C - negotiate() - NOT SETTING PRENEG-NEG-START_TIME + Customization IN SESSION. Already Set.");
                            }

                            // 4- Update NegotiationSession
                            this.session.addToSentProposals(new SLATemplate[] { slaTemplate });
                            // 5- Invoke negotiate of Provider via MessageHandler.negotiateOutgoing() operation.
                            counterOffers =
                                    this.messageHandler.negotiateOutgoing(negotiationID, slaTemplate, providerAddress);
                            this.session.addToReceivedProposals(counterOffers);
                            // Customer also keeps record of UUIDs of counter offers received from this provider:
                            this.addSLATemplatesUUIDInSession(counterOffers, providerAddress);

                            if (this.performProfiling) {
                                ProviderProfile pProfile = null;
                                try {
                                    Session hSession = HibernateUtil.getSession();
                                    hSession.beginTransaction();

                                    List list =
                                            hSession.createQuery(
                                                    "from Profile where profileId='" + providerAddress + "'").list();
                                    if (list != null && list.size() > 0) {
                                        pProfile = (ProviderProfile) list.get(0); // 1 item expected per primary-key

                                        if (pProfile != null && pProfile.getNegotiationHistory() != null) {
                                            Set<NegotiationHistory> negotiationHistories =
                                                    pProfile.getNegotiationHistory();
                                            for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                                if (negotiationHistory != null
                                                        && negotiationHistory.getNegotiationId().equalsIgnoreCase(
                                                                negotiationID)) {
                                                    negotiationHistory.setNegotiationRounds(negotiationHistory
                                                            .getNegotiationRounds() + 1);
                                                }
                                            }
                                        }
                                        pProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                        hSession.update(pProfile);
                                    }
                                    hSession.getTransaction().commit();
                                    hSession.close();
                                }
                                catch (HibernateException he) {
                                    he.printStackTrace();
                                }
                            }
                            // 6- Return the counter-offers returned by the Provider.
                            this.session.setStatus(SessionStatus.AVAILABLE);
                            return counterOffers;
                        }
                        else {
                            this.session.setStatus(SessionStatus.AVAILABLE);
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new OperationNotPossibleException(message != null ? message
                                    : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                    }
                    else {
                        throw new OperationInProgressException("Session in use");
                    }
                }
            }

        }
        else {
            throw new OperationNotPossibleException("SLATemplate not recognized by current negotiation session.");
        }
        return counterOffers;
    }

    /**
     * Creates agreement with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the final offer
     * @return SLA created, if at all
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws SLACreationException
     *             if sla could not be created
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLA createAgreement(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            SLACreationException, InvalidNegotiationIDException {
        SLA sla = null;
        if (negotiationID != null && slaTemplate != null
                && this.session.isTemplateUUIDInSession(slaTemplate.getUuid().getValue())) {
            Party[] parties = slaTemplate.getParties();
            assert (parties != null && parties.length > 0) : "Partys null or empty";
            if (parties != null && parties.length > 0) {
                Party providerParty = null;
                Party customerParty = null;
                for (Party party : parties) {
                    STND partyRole = (STND) party.getAgreementRole();
                    if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                        providerParty = party;
                    }
                    else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                        customerParty = party;
                    }
                }
                // Read EPR-Annotation:
                String providerAddress = null;
                String customerAddress = null;
                assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
                if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                    providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new SLACreationException(PROVIDER_EPR_MISSING);
                }
                assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
                if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                    customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
                }
                else {
                    throw new SLACreationException(CUSTOMER_EPR_MISSING);
                }

                if (this.messageHandler.isSelfAddressed(providerAddress)) {
                    if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                        this.session.setStatus(SessionStatus.BUSY);
                        // 2- Invoke the SM, pass in Event(s).
                        Event agreementRequestedEvent = new Event(EventName.AgreementRequestedEvent);
                        agreementRequestedEvent.setOffer(slaTemplate);
                        List<Event> listOfEvents = new ArrayList<Event>();
                        listOfEvents.add(agreementRequestedEvent);
                        listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                        // 3- Process the results.
                        LOGGER.info("P- " + listOfEvents.get(0).isProcessedSuccessfully());// If this tells us that
                        // everything is ok, go
                        // ahead, else not.
                        assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            // 4- Update NegotiationSession
                            this.session.addToReceivedProposals(new SLATemplate[] { slaTemplate });

                            // 5-Invoke POC via IAssessmentAndCustomize.createAgreement() and return back the SLA if POC
                            // returns, otherwise raise Exception.
                            sla =
                                    this.messageHandler.getPlanningOptimization().getIAssessmentAndCustomize()
                                            .createAgreement(negotiationID, slaTemplate);

                            this.session.addToSentProposals(new SLATemplate[] { sla });
                            // Future directions: Persist session before removing this.NegotiationManager?
                            assert (sla != null) : "SLA could not be created or registered in the SLA Registry by POC.";
                            if (sla == null) {
                                SLACreationException slaCE =
                                        new SLACreationException(
                                                "SLA could not be created or registered in the SLA Registry by POC.");
                                throw slaCE;
                            }
                            this.stateEngine.disposeWorkingMemory();
                            this.session.setStatus(SessionStatus.INVALID);
                            this.messageHandler.removeFromNegotiationManagerHashMap(this.session.getUuid());

                            if (this.performProfiling) {
                                CustomerProfile cProfile = null;
                                try {
                                    Session hSession = HibernateUtil.getSession();
                                    hSession.beginTransaction();
                                    List list =
                                            hSession.createQuery(
                                                    "from Profile where profileId='" + customerAddress + "'").list();
                                    if (list != null && list.size() > 0) {
                                        cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key

                                        if (cProfile != null && cProfile.getNegotiationHistory() != null) {
                                            Set<NegotiationHistory> negotiationHistories =
                                                    cProfile.getNegotiationHistory();
                                            for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                                if (negotiationHistory != null
                                                        && negotiationHistory.getNegotiationId().equalsIgnoreCase(
                                                                negotiationID)) {
                                                    negotiationHistory.setSlaId(sla.getUuid().toString());
                                                    negotiationHistory.setOutcome(Outcome.NEG_SUCCEEDED);
                                                    // TODO: Also Store the NegotiationSession object as BLOB in
                                                    // negotiationHistory at this point.
                                                }
                                            }
                                        }

                                        cProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                        hSession.update(cProfile);
                                    }
                                    hSession.getTransaction().commit();
                                    hSession.close();
                                }
                                catch (HibernateException he) {
                                    he.printStackTrace();
                                }
                            }

                            return sla;
                        }
                        else {
                            this.session.setStatus(SessionStatus.AVAILABLE);
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new SLACreationException(message != null ? message
                                    : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                    }
                    else {
                        throw new OperationInProgressException("Session in use");
                    }
                }

                else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                    if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                        this.session.setStatus(SessionStatus.BUSY);
                        // 2- Invoke the SM, pass in Event(s).
                        Event requestAgreementEvent = new Event(EventName.RequestAgreementEvent);
                        requestAgreementEvent.setOffer(slaTemplate);
                        List<Event> listOfEvents = new ArrayList<Event>();
                        listOfEvents.add(requestAgreementEvent);
                        listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                        // 3- Process the results.
                        LOGGER.info("C- " + listOfEvents.get(0).isProcessedSuccessfully());// If this tells us that
                        // everything is ok, go
                        // ahead, else not.
                        assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            // 4- Update NegotiationSession
                            this.session.addToSentProposals(new SLATemplate[] { slaTemplate });
                            sla =
                                    this.messageHandler.createAgreementOutgoing(negotiationID, slaTemplate,
                                            providerAddress);
                            this.session.addToReceivedProposals(new SLATemplate[] { sla });
                            // Future directions: Persist session before removing this.NegotiationManager?
                            assert (sla != null) : "SLA could not be created or registered in the SLA Registry by POC.";
                            if (sla == null) {
                                SLACreationException slaCE =
                                        new SLACreationException(
                                                "SLA could not be created or registered in the SLA Registry by POC.");
                                throw slaCE;
                            }
                            this.stateEngine.disposeWorkingMemory();
                            this.session.setStatus(SessionStatus.INVALID);
                            this.messageHandler.removeFromNegotiationManagerHashMap(this.session.getUuid());

                            if (this.performProfiling) {
                                ProviderProfile pProfile = null;
                                try {
                                    Session hSession = HibernateUtil.getSession();
                                    hSession.beginTransaction();

                                    List list =
                                            hSession.createQuery(
                                                    "from Profile where profileId='" + providerAddress + "'").list();
                                    if (list != null && list.size() > 0) {
                                        pProfile = (ProviderProfile) list.get(0); // 1 item expected per primary-key

                                        if (pProfile != null && pProfile.getNegotiationHistory() != null) {
                                            Set<NegotiationHistory> negotiationHistories =
                                                    pProfile.getNegotiationHistory();
                                            for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                                if (negotiationHistory != null
                                                        && negotiationHistory.getNegotiationId().equalsIgnoreCase(
                                                                negotiationID)) {
                                                    negotiationHistory.setSlaId(sla.getUuid().toString());
                                                    negotiationHistory.setOutcome(Outcome.NEG_SUCCEEDED);
                                                    // TODO: Also Store the NegotiationSession object as BLOB in
                                                    // negotiationHistory at this point.

                                                }
                                            }
                                        }

                                        pProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                        hSession.update(pProfile);
                                    }
                                    hSession.getTransaction().commit();
                                    hSession.close();

                                }
                                catch (HibernateException he) {
                                    he.printStackTrace();
                                }
                            }

                            return sla;
                        }
                        else {
                            this.session.setStatus(SessionStatus.AVAILABLE);
                            // Message to identify exact reason.
                            String message = listOfEvents.get(0).getProcessingAfterMath();
                            throw new SLACreationException(message != null ? message
                                    : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                    }
                    else {
                        throw new OperationInProgressException("Session in use");
                    }
                }

            }
        }
        else {
            throw new SLACreationException("SLATemplate not recognized by current negotiation session.");
        }
        return sla;
    }

    /**
     * Cancels ongoing negotiation with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param cancellationReason
     *            list of cancellation reasons that must be provided
     * @return boolean value showing if negotiation could be cancelled or not
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public boolean cancelNegotiation(String negotiationID, List<CancellationReason> cancellationReason)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException {
        boolean result = false;
        if (negotiationID != null && cancellationReason != null) {
            Party providerParty = null;
            Party customerParty = null;
            for (Party party : this.session.getInvolvedParties()) {
                STND partyRole = (STND) party.getAgreementRole();
                if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                    providerParty = party;
                }
                else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                    customerParty = party;
                }
            }
            // Read EPR-Annotation:
            String providerAddress = null;
            String customerAddress = null;
            assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
            if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(PROVIDER_EPR_MISSING);
            }
            assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
            if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(CUSTOMER_EPR_MISSING);
            }

            if (this.messageHandler.isSelfAddressed(providerAddress)) {
                // I'm Provider - The Customer has decided to cancel ongoing negotiation. I'm gonna cancel session at my
                // side and then return.
                if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                    this.session.setStatus(SessionStatus.BUSY);

                    if (this.performProfiling) {
                        CustomerProfile cProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();
                            List list =
                                    hSession.createQuery("from Profile where profileId='" + customerAddress + "'")
                                            .list();
                            if (list != null && list.size() > 0) {
                                cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key

                                if (cProfile != null && cProfile.getNegotiationHistory() != null) {
                                    Set<NegotiationHistory> negotiationHistories = cProfile.getNegotiationHistory();
                                    for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                        if (negotiationHistory != null
                                                && negotiationHistory.getNegotiationId()
                                                        .equalsIgnoreCase(negotiationID)) {
                                            negotiationHistory.setOutcome(Outcome.NEG_ABORTED);
                                        }
                                    }
                                }
                                cProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                hSession.update(cProfile);
                            }
                            hSession.getTransaction().commit();
                            hSession.close();

                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }
                    // 2- Invoke the SM, pass in Event(s).
                    Event cancelNegotiationEvent = new Event(EventName.CancelNegotiationEvent);
                    List<Event> listOfEvents = new ArrayList<Event>();
                    listOfEvents.add(cancelNegotiationEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        this.session.setCancellationReasons(cancellationReason);
                        this.session.setStatus(SessionStatus.INVALID);
                        result = true;
                        this.stateEngine.disposeWorkingMemory();
                        this.messageHandler.removeFromNegotiationManagerHashMap(this.session.getUuid());
                        return result;
                    }
                    else {
                        this.session.setStatus(SessionStatus.AVAILABLE);
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else {
                    throw new OperationInProgressException("Session in use");
                }
            }
            else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                // I'm customer - I decided to cancel negotiation with the provider.
                if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                    this.session.setStatus(SessionStatus.BUSY);

                    if (this.performProfiling) {
                        ProviderProfile pProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();
                            List list =
                                    hSession.createQuery("from Profile where profileId='" + providerAddress + "'")
                                            .list();
                            if (list != null && list.size() > 0) {
                                pProfile = (ProviderProfile) list.get(0); // 1 item expected per primary-key

                                if (pProfile != null && pProfile.getNegotiationHistory() != null) {
                                    Set<NegotiationHistory> negotiationHistories = pProfile.getNegotiationHistory();
                                    for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                        if (negotiationHistory != null
                                                && negotiationHistory.getNegotiationId()
                                                        .equalsIgnoreCase(negotiationID)) {
                                            negotiationHistory.setOutcome(Outcome.NEG_ABORTED);
                                        }
                                    }
                                }
                                pProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                                hSession.update(pProfile);
                            }
                            hSession.getTransaction().commit();
                            hSession.close();
                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }
                    // 2- Invoke the SM, pass in Event(s).
                    Event cancelNegotiationEvent = new Event(EventName.CancelNegotiationEvent);
                    List<Event> listOfEvents = new ArrayList<Event>();
                    listOfEvents.add(cancelNegotiationEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);
                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        this.session.setCancellationReasons(cancellationReason);
                        // At some point, post to provider:
                        result =
                                this.messageHandler.cancelNegotiationOutgoing(negotiationID, cancellationReason,
                                        providerAddress);
                        this.session.setStatus(SessionStatus.INVALID);
                        this.stateEngine.disposeWorkingMemory();
                        this.messageHandler.removeFromNegotiationManagerHashMap(this.session.getUuid());
                        return result;
                    }
                    else {
                        this.session.setStatus(SessionStatus.AVAILABLE);
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else {
                    throw new OperationInProgressException("Session in use");
                }
            }
        }
        return result;
    }

    /**
     * Renegotiates with the SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to renegotiate over
     * @return String representing the id of the negotiation session established
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String renegotiate(UUID slaID) throws SLANotFoundException, OperationNotPossibleException {
        String reNegotiationId = null;
        if (slaID != null) {
            SLA sla = getSLAFromSLARegistry(slaID);
            assert (sla != null) : "SLA not retrieved from SLAR";
            Party providerParty = null;
            Party customerParty = null;
            assert (sla.getParties() != null) : "SLA does not specify any parties";
            for (Party party : sla.getParties()) {
                STND partyRole = (STND) party.getAgreementRole();
                if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                    providerParty = party;
                }
                else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                    customerParty = party;
                }
            }
            // Read EPR-Annotation:
            String providerAddress = null;
            String customerAddress = null;
            assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
            if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(PROVIDER_EPR_MISSING);
            }
            assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
            if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(CUSTOMER_EPR_MISSING);
            }

            if (this.messageHandler.isSelfAddressed(providerAddress)) {
                LOGGER.info("P- I'm the Provider");
                // 0- Authorize customer:
                assert (this.messageHandler.checkAccess(sla)) : "AUTHORIZATION_FAILED";
                if (!this.messageHandler.checkAccess(sla)) {
                    // Provider's authorization does not allow this customer to proceed!
                    throw new OperationNotPossibleException(AUTHORIZATION_FAILED);
                }

                if (this.performProfiling) {
                    CustomerProfile cProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();

                        List list =
                                hSession.createQuery("from Profile where profileId='" + customerAddress + "'").list();
                        if (list != null && list.size() > 0) {
                            cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key
                        }

                        if (cProfile == null) {
                            // Profile does not exist, create one:
                            cProfile = new CustomerProfile();
                            cProfile.setProfileId(customerAddress);
                            Integer profileId = (Integer) hSession.save(cProfile);
                            cProfile.setId(profileId);
                        }
                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }
                }
                // 1- Initialize the NegotiationSession.
                this.session = new NegotiationSession();

                this.session.setUuid(sla.getUuid().getValue());// Using the SAL.UUID as RenegotiationID so only 1
                // renegotiation attempt happens at a time!
                LOGGER.info("P- I am using the session ID = " + this.session.getUuid());

                // Adding involved parties from SLAT to NegotiationSession. Assuming customer appended itself to
                // list.
                this.session.addToInvolvedParties(sla);
                this.session.addTemplateUUIDInSession(sla.getTemplateId().getValue());
                // 2- Involve StateEngine to spawn the State Machine (according to the protocol) by sending the
                // StartNegotiationEvent.
                this.stateEngine = new StateEngine();
                this.stateEngine.setContext(this.messageHandler.getContext());
                Event startRenegotiationEvent = new Event(EventName.StartNegotiationEvent);
                List<Event> listOfEvents = new ArrayList<Event>();
                listOfEvents.add(startRenegotiationEvent);
                listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                if (listOfEvents.get(0).isProcessedSuccessfully()) {

                    if (this.performProfiling) {
                        listOfEvents = new ArrayList<Event>();
                        Event updateNegotiationEvent = new Event(EventName.UpdateNegotiationEvent);
                        NegotiationHistory negotiationHistory = new NegotiationHistory();
                        negotiationHistory.setStartTime(new Timestamp(System.currentTimeMillis()));
                        negotiationHistory.setSlaTemplateId(sla.getTemplateId().toString());
                        negotiationHistory.setSlaId(sla.getUuid().toString());
                        negotiationHistory.setNegotiationId(this.session.getUuid());
                        negotiationHistory.setRenegotiation(true);
                        CustomerProfile cProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();
                            List list =
                                    hSession.createQuery("from Profile where profileId='" + customerAddress + "'")
                                            .list();
                            if (list != null && list.size() > 0) {
                                cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key
                            }

                            if (cProfile.getNegotiationHistory() != null) {
                                cProfile.getNegotiationHistory().add(negotiationHistory);
                            }
                            else {
                                Set<NegotiationHistory> negotiationHistories = new HashSet<NegotiationHistory>();
                                negotiationHistories.add(negotiationHistory);
                                cProfile.setNegotiationHistory(negotiationHistories);
                            }

                            updateNegotiationEvent.setPartnerProfile(cProfile);
                            updateNegotiationEvent.setNegotiationId(this.session.getUuid());
                            updateNegotiationEvent.setOffer(sla);// Incase of Renegotiation, passing the corresponding
                            // SLA.
                            listOfEvents.add(updateNegotiationEvent);// Passing Profile object to compute ranks
                            listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                            // Update the CustomerProfile as rules have now computed Ranks
                            CustomerProfile cProfileWithRanks =
                                    (CustomerProfile) listOfEvents.get(0).getPartnerProfile();

                            cProfileWithRanks.setLastModified(new Timestamp(System.currentTimeMillis()));
                            hSession.update(cProfileWithRanks);
                            hSession.getTransaction().commit();
                            hSession.close();

                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }
                    // 3- Put this NegotiationManager into the MessageHandler.negotiationManagerHashMap
                    this.messageHandler.addToNegotiationManagerHashMap(this.session.getUuid(), this);
                    LOGGER.info("P- Just added NegotiationManager to MessageHandler");
                    // 4- Return the NegotiationID.
                    reNegotiationId = this.session.getUuid();
                    return reNegotiationId;
                }
                else {
                    String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify the reason:
                    // e.g., blacklisted party, blocked
                    // until, DOS attack, etc. whatever
                    // rules determine.
                    throw new OperationNotPossibleException(message != null ? message
                            : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                }
            }

            else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                LOGGER.info("C- I'm the Customer");

                if (this.performProfiling) {
                    ProviderProfile pProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();
                        List list =
                                hSession.createQuery("from Profile where profileId='" + providerAddress + "'").list();

                        if (list != null && list.size() > 0) {
                            pProfile = (ProviderProfile) list.get(0);
                        }

                        if (pProfile == null) {
                            // Profile does not exist, create one:
                            pProfile = new ProviderProfile();
                            pProfile.setProfileId(providerAddress);
                            Integer profileId = (Integer) hSession.save(pProfile);
                            pProfile.setId(profileId);
                        }
                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }
                }
                // 1- Initialize a NegotiationSession.
                this.session = new NegotiationSession();
                this.session.addToInvolvedParties(sla);
                // List<Party> involvedParties = this.session.getInvolvedParties();
                // involvedParties.add(providerParty);
                // involvedParties.add(customerParty);
                this.session.addTemplateUUIDInSession(sla.getTemplateId().getValue());

                // 2- Involve StateEngine to spawn the State Machine (according to the protocol) by sending the
                // StartNegotiationEvent.
                this.stateEngine = new StateEngine();
                this.stateEngine.setContext(this.messageHandler.getContext());
                Event startRenegotiationEvent = new Event(EventName.StartNegotiationEvent);
                List<Event> listOfEvents = new ArrayList<Event>();
                listOfEvents.add(startRenegotiationEvent);
                listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                if (listOfEvents.get(0).isProcessedSuccessfully()) {
                    // 3- Call the renegotiate on the Provider.
                    LOGGER.info("C- Before calling renegotiate() on provider");
                    reNegotiationId = this.messageHandler.renegotiateOutgoing(sla.getUuid(), providerAddress);

                    assert (reNegotiationId != null) : "reNegotiationId is null";
                    LOGGER.info("C- After calling renegotiate() on provider. Got ReNegID = " + reNegotiationId);
                    // 4- Set the NegotiationSession.id = the returned NegotiationID from Provider. Also put yourself in
                    // the session partyList.
                    this.session.setUuid(reNegotiationId);

                    if (this.performProfiling) {
                        // 5- Send a UpdateNegotiationEvent to StateEngine
                        listOfEvents.removeAll(listOfEvents);
                        listOfEvents = new ArrayList<Event>();

                        Event updateNegotiationEvent = new Event(EventName.UpdateNegotiationEvent);

                        NegotiationHistory negotiationHistory = new NegotiationHistory();
                        negotiationHistory.setStartTime(new Timestamp(System.currentTimeMillis()));
                        negotiationHistory.setSlaTemplateId(sla.getTemplateId().toString());
                        negotiationHistory.setSlaId(sla.getUuid().toString());
                        negotiationHistory.setNegotiationId(this.session.getUuid());
                        negotiationHistory.setRenegotiation(true);
                        ProviderProfile pProfile = null;
                        try {
                            Session hSession = HibernateUtil.getSession();
                            hSession.beginTransaction();
                            List list =
                                    hSession.createQuery("from Profile where profileId='" + providerAddress + "'")
                                            .list();

                            if (list != null && list.size() > 0) {
                                pProfile = (ProviderProfile) list.get(0);
                            }

                            if (pProfile.getNegotiationHistory() != null) {
                                pProfile.getNegotiationHistory().add(negotiationHistory);
                            }
                            else {
                                Set<NegotiationHistory> negotiationHistories = new HashSet<NegotiationHistory>();
                                negotiationHistories.add(negotiationHistory);
                                pProfile.setNegotiationHistory(negotiationHistories);
                            }

                            updateNegotiationEvent.setPartnerProfile(pProfile);
                            updateNegotiationEvent.setNegotiationId(this.session.getUuid());
                            updateNegotiationEvent.setOffer(sla);// Incase of Renegotiation, passing the corresponding
                            // SLA.
                            listOfEvents.add(updateNegotiationEvent);// Passing in Profile object to compute ranks
                            // fact in working memory.
                            listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                            // Update the ProviderProfile as rules have now computed Ranks
                            ProviderProfile pProfileWithRanks =
                                    (ProviderProfile) listOfEvents.get(0).getPartnerProfile();

                            pProfileWithRanks.setLastModified(new Timestamp(System.currentTimeMillis()));
                            hSession.update(pProfileWithRanks);
                            hSession.getTransaction().commit();
                            hSession.close();

                        }
                        catch (HibernateException he) {
                            he.printStackTrace();
                        }
                    }

                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        // 6- Put this NegotiationManager into the MessageHandler.negotiationManagerHashMap
                        this.messageHandler.addToNegotiationManagerHashMap(this.session.getUuid(), this);
                        LOGGER.info("C- Just added NegotiationManager to MessageHandler");
                        // 7- Return the ReNegotiationID to original caller.
                        return reNegotiationId;
                    }
                    else {
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else {
                    String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify the reason.
                    throw new OperationNotPossibleException(message != null ? message
                            : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                }
            }
        }
        return reNegotiationId;
    }

    /**
     * Terminates SLA with the SLA-Manager.
     * <p>
     * This operation is independent of NegotiationSession. The call is delegated to POC which decides actions to be
     * taken and also updates the status of SLA in the SLARegistry. This operation can be invoked by both the Client and
     * the Provider.
     * 
     * @param slaID
     *            the uuid of the sla to terminate
     * @param terminationReasons
     *            list of termination reasons that must be provided. Must contain CUSTOMER_INITIATED or
     *            PROVIDER_INITIATED among others.
     * @return boolean value showing if termination was successful or not
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     */
    public boolean terminate(UUID slaID, List<TerminationReason> terminationReasons) throws SLANotFoundException {
        LOGGER.info("*** NegotiationManager.terminate() ");
        boolean result = false;
        if (slaID != null && terminationReasons != null) {
            for (TerminationReason tr : terminationReasons) {
                LOGGER.info("*** termination reason = " + tr);
                LOGGER.info("*** termination reason = " + tr.getClass());
                LOGGER.info("*** termination reason = " + tr.getClass().getName());
            }

            SLA sla = getSLAFromSLARegistry(slaID);
            assert (sla != null) : "SLA not retrieved from SLAR";
            Party providerParty = null;
            Party customerParty = null;
            assert (sla.getParties() != null) : "SLA does not specify any parties";
            for (Party party : sla.getParties()) {
                STND partyRole = (STND) party.getAgreementRole();
                if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                    providerParty = party;
                }
                else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                    customerParty = party;
                }
            }
            // Read EPR-Annotation:
            String providerAddress = null;
            String customerAddress = null;
            assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
            if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new SLANotFoundException(PROVIDER_EPR_MISSING);
            }
            assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
            if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new SLANotFoundException(CUSTOMER_EPR_MISSING);
            }

            if (this.messageHandler.isSelfAddressed(providerAddress)) {
                LOGGER.info("P- I'm the Provider");

                if (this.performProfiling) {
                    CustomerProfile cProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();

                        List list =
                                hSession.createQuery("from Profile where profileId='" + customerAddress + "'").list();
                        if (list != null && list.size() > 0) {
                            cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key

                            if (cProfile != null && cProfile.getNegotiationHistory() != null) {
                                Set<NegotiationHistory> negotiationHistories = cProfile.getNegotiationHistory();
                                for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                    if (negotiationHistory != null
                                            && negotiationHistory.getSlaId().equalsIgnoreCase(slaID.toString().trim())) {
                                        negotiationHistory.setOutcome(Outcome.SLA_TERMINATED);
                                    }
                                }
                            }

                            cProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                            hSession.update(cProfile);
                        }
                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }
                }

                assert (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) : "SLA State is already EXPIRED.";

                if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED
                        && terminationReasons.contains(TerminationReason.CUSTOMER_INITIATED)) {
                    LOGGER.info("*** terminate - case1");
                    // Customer's POC has asked Provider to terminate the SLA - Provider POC updates its SLARegistry and
                    // takes necessary actions on its side.
                    result =
                            this.messageHandler.getPlanningOptimization().getIAssessmentAndCustomize().terminate(slaID,
                                    terminationReasons);
                    return result;
                }
                else if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED
                        && terminationReasons.contains(TerminationReason.PROVIDER_INITIATED)) {
                    LOGGER.info("*** terminate - case2");
                    // Provider's POC has terminated the SLA, taken necessary actions on its side and now asks Customer
                    // to also terminate SLA at its side.
                    result = this.messageHandler.terminateOutgoing(sla.getUuid(), terminationReasons, customerAddress);
                    return true;
                }
            }

            else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                LOGGER.info("C- I'm the Customer");

                if (this.performProfiling) {
                    ProviderProfile pProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();

                        List list =
                                hSession.createQuery("from Profile where profileId='" + providerAddress + "'").list();
                        if (list != null && list.size() > 0) {
                            pProfile = (ProviderProfile) list.get(0); // 1 item expected per primary-key

                            if (pProfile != null && pProfile.getNegotiationHistory() != null) {
                                Set<NegotiationHistory> negotiationHistories = pProfile.getNegotiationHistory();
                                for (NegotiationHistory negotiationHistory : negotiationHistories) {
                                    if (negotiationHistory != null
                                            && negotiationHistory.getSlaId().equalsIgnoreCase(slaID.toString().trim())) {
                                        negotiationHistory.setOutcome(Outcome.SLA_TERMINATED);
                                    }
                                }
                            }

                            pProfile.setLastModified(new Timestamp(System.currentTimeMillis()));
                            hSession.update(pProfile);
                        }
                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }
                }

                assert (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) : "SLA State is already EXPIRED.";

                if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED
                        && terminationReasons.contains(TerminationReason.CUSTOMER_INITIATED)) {
                    LOGGER.info("*** terminate - case3");
                    // Customer's POC has terminated the SLA, taken necessary actions on its side and now asks Provider
                    // to also terminate SLA at its side.
                    result = this.messageHandler.terminateOutgoing(sla.getUuid(), terminationReasons, providerAddress);
                    return result;
                }
                else if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED
                        && terminationReasons.contains(TerminationReason.PROVIDER_INITIATED)) {
                    LOGGER.info("*** terminate - case4");
                    // Provider's POC has asked Customer to terminate the SLA - Customer POC updates its SLARegistry and
                    // takes necessary actions on its side.
                    result =
                            this.messageHandler.getPlanningOptimization().getIAssessmentAndCustomize().terminate(slaID,
                                    terminationReasons);
                    return result;
                }
            }
        }
        return result;
    }

    /**
     * This method is here for legacy reasons. It is not to be used. Provisioning is related to <code>SLARegistry</code>
     * and <code>ProvisioningAdjustment</code>.
     * 
     * @param slaID
     *            the uuid of the sla to provision
     * @return SLA with provisioning information, if at all
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     * @throws ProvisioningException
     *             if provisioning was not possible
     * @see org.slasoi.gslam.core.poc.PlanningOptimization.INotification.activate(SLA sla)
     * @see org.slasoi.gslam.core.pac.ProvisioningAdjustment
     */
    public SLA provision(UUID slaID) throws SLANotFoundException, ProvisioningException {
        SLA provisionedSLA = null;
        if (slaID != null) {
            SLA sla = getSLAFromSLARegistry(slaID);
            assert (sla != null) : "SLA not retrieved from SLAR";
            Party providerParty = null;
            Party customerParty = null;
            assert (sla.getParties() != null) : "SLA does not specify any parties";
            for (Party party : sla.getParties()) {
                STND partyRole = (STND) party.getAgreementRole();
                if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                    providerParty = party;
                }
                else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                    customerParty = party;
                }
            }
            // Read EPR-Annotation:
            String providerAddress = null;
            String customerAddress = null;
            assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
            if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new SLANotFoundException(PROVIDER_EPR_MISSING);
            }
            assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
            if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new SLANotFoundException(CUSTOMER_EPR_MISSING);
            }

            if (this.messageHandler.isSelfAddressed(providerAddress)) {

                assert (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) : "Cannot provision an SLA whose State is already EXPIRED.";

                if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) {
                    provisionedSLA =
                            this.messageHandler.getPlanningOptimization().getIAssessmentAndCustomize().provision(
                                    sla.getUuid());
                }
            }
            else if (this.messageHandler.isSelfAddressed(customerAddress)) {

                assert (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) : "Cannot provision an SLA whose State is already EXPIRED.";

                if (getSLAStateFromSLARegistry(sla.getUuid()) != SLARegistry.SLAState.EXPIRED) {
                    provisionedSLA = this.messageHandler.provisionOutgoing(sla.getUuid(), providerAddress);
                }
            }
        }
        return provisionedSLA;
    }

    /**
     * Gets SLA from the <code>SLARegistry</code>.
     * 
     * @param slaID
     *            the uuid of the sla
     * @return SLA if found, throwing exception otherwise
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     */
    protected SLA getSLAFromSLARegistry(UUID slaID) throws SLANotFoundException {
        SLA sla = null;
        try {
            if (this.messageHandler.getQuery() != null) {
                SLA[] slas = this.messageHandler.getQuery().getSLA(new UUID[] { slaID });
                if (slas != null && slas.length > 0) {
                    sla = slas[0];
                    LOGGER.info("*** SLA retrieved from SLAR with UUID = " + sla.getUuid());
                }
                else {
                    LOGGER.info("*** SLAR did not return any SLA");
                }
            }
        }
        catch (InvalidUUIDException iUUIDE) {
            iUUIDE.printStackTrace();
            throw new SLANotFoundException("SLARegistry.IQuery.getSLA() raised InvalidUUIDException with message = "
                    + iUUIDE.getMessage());
        }
        return sla;
    }

    /**
     * Gets the state of SLA from the <code>SLARegistry</code>.
     * 
     * @param slaID
     *            the uuid of the sla
     * @return SLAState if found, throwing exception otherwise
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     */
    protected SLARegistry.SLAState getSLAStateFromSLARegistry(UUID slaID) {
        SLARegistry.SLAState slaState = null;
        try {
            if (this.messageHandler.getQuery() != null && slaID != null) {
                SLAStateInfo[] slaStateInfos = this.messageHandler.getQuery().getStateHistory(slaID, true);
                if (slaStateInfos != null && slaStateInfos.length > 0) {
                    slaState = slaStateInfos[0].state;// For current=true, the
                    // method returns the current STATE of SLA.
                    LOGGER.info("*** SLA-State-Info retrieved from SLAR with state = " + slaState);
                }
                else {
                    LOGGER.info("*** SLAR did not return any SLA-State-Infos ");
                }
            }
        }
        catch (InvalidUUIDException iUUIDE) {
            iUUIDE.printStackTrace();
        }
        catch (InvalidStateException iSE) {
            iSE.printStackTrace();
        }
        return slaState;
    }

    protected void addSLATemplatesUUIDInSession(SLATemplate[] slaTemplates, String providerEPR)
            throws OperationNotPossibleException {
        assert (slaTemplates != null && slaTemplates.length > 0) : "SLATemplates received or being sent are either null or empty";
        if (slaTemplates != null && slaTemplates.length > 0) {
            LOGGER.info("*** addSLATemplatesUUIDInSession.length = " + slaTemplates.length);
            for (SLATemplate slaTemplate : slaTemplates) {
                for (Party party : slaTemplate.getParties()) {
                    if (party != null) {
                        if (party.getAgreementRole().equals(sla.provider)) {
                            String partyEPR = party.getPropertyValue(sla.gslam_epr);
                            assert (partyEPR != null) : "One of the SLATemplates received or being sent does not specify the GSLAM_EPR annotation for the Provider";
                            if (partyEPR != null && partyEPR.equals(providerEPR)) {
                                this.session.addTemplateUUIDInSession(slaTemplate.getUuid().getValue());
                                continue;
                            }
                            else {
                                LOGGER
                                        .info("*** One of the SLATemplates received or being sent does not specify the GSLAM_EPR annotation for the Provider");
                                throw new OperationNotPossibleException(
                                        "One of the SLATemplates received or being sent does not specify the GSLAM_EPR annotation for the Provider");
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 
     * This method allows to use customization feature. It requires that profiling feature is being used.
     * 
     */
    public Customization customize(String negotiationID, Customization customization) throws NegotiationException {

        Customization customizationResult = null;
        if (this.session.isCustomizationUsed() == false) {
            this.session.setCustomizationUsed(true);
        }
        List<Party> parties = this.session.getInvolvedParties();

        assert (parties != null && parties.size() > 0) : "Session contains Null or Empty party list";
        if (parties != null && parties.size() > 0) {
            Party providerParty = null;
            Party customerParty = null;
            for (Party party : parties) {
                STND partyRole = (STND) party.getAgreementRole();
                if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
                    providerParty = party;
                }
                else if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
                    customerParty = party;
                }
            }
            // Read EPR-Annotation:
            String providerAddress = null;
            String customerAddress = null;
            assert (providerParty.getPropertyValue(GSLAM_EPR) != null) : "Provider GSLAM_EPR not specified";
            if (providerParty.getPropertyValue(GSLAM_EPR) != null) {
                providerAddress = providerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(PROVIDER_EPR_MISSING);
            }
            assert (customerParty.getPropertyValue(GSLAM_EPR) != null) : "Customer GSLAM_EPR not specified";
            if (customerParty.getPropertyValue(GSLAM_EPR) != null) {
                customerAddress = customerParty.getPropertyValue(GSLAM_EPR);
            }
            else {
                throw new OperationNotPossibleException(CUSTOMER_EPR_MISSING);
            }

            // Now Process further based on Role.
            if (this.messageHandler.isSelfAddressed(providerAddress)) {
                LOGGER.info("P- customize invoked on Provider");
                if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                    this.session.setStatus(SessionStatus.BUSY);

                    SimplifiedNegotiableParameters customerParameters =
                            extractCustomerParametersFromCustomization(customization);

                    LOGGER.info("P- Customer sent customization params = " + customerParameters.toString());

                    Event customizationArrivedEvent = new Event(EventName.CustomizationArrivedEvent);
                    customizationArrivedEvent.setNegotiationId(negotiationID);
                    customizationArrivedEvent.setSimplifiedNegotiableParameters(customerParameters);

                    CustomerProfile cProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();
                        List list =
                                hSession.createQuery("from Profile where profileId='" + customerAddress + "'").list();
                        if (list != null && list.size() > 0) {
                            cProfile = (CustomerProfile) list.get(0); // 1 item expected per primary-key
                        }
                        // Instigating lazy loading so rules may have access to history.
                        if (cProfile != null) {
                            LOGGER.info("P- Negotiation History size = " + cProfile.getNegotiationHistory().size());
                        }
                        if (cProfile.getBusinessHistory() != null) {
                            LOGGER.info("P- Business History size = " + cProfile.getBusinessHistory().size());
                        }

                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }

                    customizationArrivedEvent.setPartnerProfile(cProfile);

                    List<Event> listOfEvents = new ArrayList<Event>();
                    listOfEvents.add(customizationArrivedEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);
                    // Process the results.
                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : PROVIDER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        // process event.getSimplifiedNegotiableParameters and Consolidate own changes in Customization
                        // parameter.
                        SimplifiedNegotiableParameters ownParameters =
                                listOfEvents.get(0).getSimplifiedNegotiableParameters();

                        LOGGER.info("P- Own params received from Rules = " + ownParameters.toString());
                        LOGGER.info("P - customize() - Attempting to SET PRENEG-NEG-START_TIME IN SESSION ");
                        customizationResult = consolidateCustomization(ownParameters, customization);

                        LOGGER.info("P- Own consolidated customization = " + customizationResult.toString());

                        // Set own Customization Status (till this point in time) inside Session
                        if (customizationResult.getResult().equals(RESULT.AGREED)) {
                            this.session.setConsensusReachedOnCustomization(true);
                            LOGGER.info("P- Consensus reached on Customization till this point");
                            // Set customizationResult object in the session at this point.
                            this.session.setCustomization(customizationResult);
                        }
                        else {
                            this.session.setConsensusReachedOnCustomization(false);
                            LOGGER.info("P- Consensus NOT reached on Customization till this point");
                            // Unset customizationResult object.
                            this.session.setCustomization(null);
                        }

                        this.session.setStatus(SessionStatus.AVAILABLE);
                    }
                    else {
                        this.session.setStatus(SessionStatus.AVAILABLE);// Make Session available even though this
                        // operation didnt go completely fine.
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : PROVIDER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else {
                    throw new OperationInProgressException("Session in use");
                }
            }

            else if (this.messageHandler.isSelfAddressed(customerAddress)) {
                LOGGER.info("C- customize invoked on Customer");
                if (this.session.getStatus().equals(SessionStatus.AVAILABLE)) {
                    this.session.setStatus(SessionStatus.BUSY);
                    Event sendCustomizationEvent = new Event(EventName.SendCustomizationEvent);
                    sendCustomizationEvent.setNegotiationId(negotiationID);

                    ProviderProfile pProfile = null;
                    try {
                        Session hSession = HibernateUtil.getSession();
                        hSession.beginTransaction();
                        List list =
                                hSession.createQuery("from Profile where profileId='" + providerAddress + "'").list();
                        if (list != null && list.size() > 0) {
                            pProfile = (ProviderProfile) list.get(0); // 1 item expected per primary-key
                        }
                        // Instigating lazy loading so rules may have access to history.
                        if (pProfile != null) {
                            LOGGER.info("C- Negotiation History size = " + pProfile.getNegotiationHistory().size());
                        }
                        if (pProfile.getBusinessHistory() != null) {
                            LOGGER.info("P- Business History size = " + pProfile.getBusinessHistory().size());
                        }
                        hSession.getTransaction().commit();
                        hSession.close();
                    }
                    catch (HibernateException he) {
                        he.printStackTrace();
                    }
                    sendCustomizationEvent.setPartnerProfile(pProfile);

                    List<Event> listOfEvents = new ArrayList<Event>();
                    listOfEvents.add(sendCustomizationEvent);
                    listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);
                    // Process the results.
                    assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                    if (listOfEvents.get(0).isProcessedSuccessfully()) {
                        SimplifiedNegotiableParameters ownParameters =
                                listOfEvents.get(0).getSimplifiedNegotiableParameters();
                        LOGGER.info("C- Own customization params received from Rules = " + ownParameters.toString());
                        LOGGER.info("C - customize() - Attempting to SET PRENEG-NEG-START_TIME IN SESSION ");
                        Customization ownCustomization =
                                createCustomizationFromSimplifiedNegotiableParameters(ownParameters);

                        LOGGER.info("C- Own customization created = " + ownCustomization.toString());
                        LOGGER.info("C- Before calling customize() on provider");
                        customizationResult =
                                this.messageHandler.customizeOutgoing(negotiationID, ownCustomization, providerAddress);
                        LOGGER.info("C- After calling customize() on provider");

                        LOGGER.info("C- Provider's consolidated customization received = "
                                + customizationResult.toString());

                        SimplifiedNegotiableParameters partnerParameters =
                                extractProviderParametersFromCustomization(customizationResult);

                        LOGGER.info("C- Provider's customization params received = " + partnerParameters.toString());
                        // Pass partnerParameters to StateEngine inside CustomizationArrivedEvent.
                        Event customizationArrivedEvent = new Event(EventName.CustomizationArrivedEvent);
                        customizationArrivedEvent.setNegotiationId(negotiationID);
                        customizationArrivedEvent.setSimplifiedNegotiableParameters(partnerParameters);

                        customizationArrivedEvent.setPartnerProfile(pProfile);

                        listOfEvents.removeAll(listOfEvents);
                        listOfEvents = new ArrayList<Event>();
                        listOfEvents.add(customizationArrivedEvent);
                        listOfEvents = (List<Event>) this.stateEngine.process(listOfEvents);

                        assert (listOfEvents.get(0).isProcessedSuccessfully()) : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL;
                        if (listOfEvents.get(0).isProcessedSuccessfully()) {
                            LOGGER.info("C- Provider's customization feedback processed successfully");
                            // Must now consolidate Customization object 1 more time. Provider's feedback is also
                            // already updated in working memory.

                            ownParameters = listOfEvents.get(0).getSimplifiedNegotiableParameters();
                            customizationResult = consolidateCustomization(ownParameters, customizationResult);

                            LOGGER.info("C- Consolidated customization result = " + customizationResult.toString());

                            // Set own Customization Status (till this point in time) inside Session
                            if (customizationResult.getResult().equals(RESULT.AGREED)) {
                                this.session.setConsensusReachedOnCustomization(true);
                                LOGGER.info("C- Consensus reached on Customization till this point");
                                // Set customizationResult object in the session at this point.
                                this.session.setCustomization(customizationResult);
                            }
                            else {
                                this.session.setConsensusReachedOnCustomization(false);
                                LOGGER.info("C- Consensus NOT reached on Customization till this point");
                                // Unset customizationResult object.
                                this.session.setCustomization(null);
                            }

                        }
                        else {
                            LOGGER.info("C- Provider's customization feedback could not be processed successfully");
                            this.session.setStatus(SessionStatus.AVAILABLE);// Make Session available even though this
                            // operation didnt go completely fine.
                            String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                            // reason.
                            throw new OperationNotPossibleException(message != null ? message
                                    : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                        }
                        this.session.setStatus(SessionStatus.AVAILABLE);
                    }
                    else {
                        this.session.setStatus(SessionStatus.AVAILABLE);// Make Session available even though this
                        // operation didnt go completely fine.
                        String message = listOfEvents.get(0).getProcessingAfterMath();// Message to identify exact
                        // reason.
                        throw new OperationNotPossibleException(message != null ? message
                                : CUSTOMER_OP_DISALLOWED_BY_PROTOCOL);
                    }
                }
                else {
                    throw new OperationInProgressException("Session in use");
                }
            }
        }
        return customizationResult;
    }

    public Object getNegotiationSession() {
        return this.session;
    }

    private SimplifiedNegotiableParameters extractCustomerParametersFromCustomization(Customization customization) {
        SimplifiedNegotiableParameters simplifiedNegotiableParameters = new SimplifiedNegotiableParameters();
        simplifiedNegotiableParameters.setBelongsTo(Ownership.PARTNER);
        List<NegotiableParameters> listOfNegotiableParameters = customization.getListOfNegotiableParameters();
        for (NegotiableParameters negotiableParameter : listOfNegotiableParameters) {

            if (negotiableParameter.getName().equals(NegotiationGlossary.CUSTOMIZATION_ROUNDS)) {
                simplifiedNegotiableParameters.setNumberOfCustomizationRounds(Integer.parseInt(negotiableParameter
                        .getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.IS_SEALED)) {
                simplifiedNegotiableParameters.setSealed(Boolean.parseBoolean(negotiableParameter.getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.MAX_COUNTER_OFFERS)) {
                simplifiedNegotiableParameters.setMaxCounterOffersAllowed(Integer.parseInt(negotiableParameter
                        .getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.NEGOTIATION_ROUNDS)) {
                simplifiedNegotiableParameters.setNumberOfNegotiationRounds(Integer.parseInt(negotiableParameter
                        .getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS)) {
                simplifiedNegotiableParameters.setOptionalCritiqueOnQoS(Boolean.parseBoolean(negotiableParameter
                        .getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.PROCESS_TIMEOUT)) {
                simplifiedNegotiableParameters.setProcessTimeout(Integer.parseInt(negotiableParameter.getValue()));
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.CREDENTIALS)) {
                simplifiedNegotiableParameters.setCredentials(negotiableParameter.getValue());
            }
        }

        return simplifiedNegotiableParameters;
    }

    private Customization consolidateCustomization(SimplifiedNegotiableParameters ownParameters,
            Customization customization) {
        if (this.session.getPreNegotiationOrNegotiationStartTime() == null) {
            // LOGGER.info("*** SETTING PRENEG-NEG-START_TIME IN SESSION ");
            this.session.setPreNegotiationOrNegotiationStartTime(new Long(ownParameters.getStartTime()));
        }
        else {
            // LOGGER.info("*** NOT SETTING PRENEG-NEG-START_TIME IN SESSION ");
        }

        List<NegotiableParameters> listOfNegotiableParameters = customization.getListOfNegotiableParameters();
        for (NegotiableParameters partnerParameters : listOfNegotiableParameters) {

            if (partnerParameters.getName().equals(NegotiationGlossary.CUSTOMIZATION_ROUNDS)) {
                int customizationRounds = Integer.parseInt(partnerParameters.getValue());
                // Now compare with ownParameters and set CRITIQUE accordingly.
                if (ownParameters.getNumberOfCustomizationRounds() < customizationRounds) {
                    partnerParameters.setCritique(CRITIQUE.DECREASE);
                    partnerParameters.setCounterValue(ownParameters.getNumberOfCustomizationRounds() + "");
                }
                else if (ownParameters.getNumberOfCustomizationRounds() > customizationRounds) {
                    partnerParameters.setCritique(CRITIQUE.INCREASE);
                    partnerParameters.setCounterValue(ownParameters.getNumberOfCustomizationRounds() + "");
                }
                else if (ownParameters.getNumberOfCustomizationRounds() == customizationRounds) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.MAX_COUNTER_OFFERS)) {
                int maxCounterOffers = Integer.parseInt(partnerParameters.getValue());
                if (ownParameters.getMaxCounterOffersAllowed() < maxCounterOffers) {
                    partnerParameters.setCritique(CRITIQUE.DECREASE);
                    partnerParameters.setCounterValue(ownParameters.getMaxCounterOffersAllowed() + "");
                }
                else if (ownParameters.getMaxCounterOffersAllowed() > maxCounterOffers) {
                    partnerParameters.setCritique(CRITIQUE.INCREASE);
                    partnerParameters.setCounterValue(ownParameters.getMaxCounterOffersAllowed() + "");
                }
                else if (ownParameters.getMaxCounterOffersAllowed() == maxCounterOffers) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.NEGOTIATION_ROUNDS)) {
                int negotiationRounds = Integer.parseInt(partnerParameters.getValue());
                if (ownParameters.getNumberOfNegotiationRounds() < negotiationRounds) {
                    partnerParameters.setCritique(CRITIQUE.DECREASE);
                    partnerParameters.setCounterValue(ownParameters.getNumberOfNegotiationRounds() + "");
                }
                else if (ownParameters.getNumberOfNegotiationRounds() > negotiationRounds) {
                    partnerParameters.setCritique(CRITIQUE.INCREASE);
                    partnerParameters.setCounterValue(ownParameters.getNumberOfNegotiationRounds() + "");
                }
                else if (ownParameters.getNumberOfNegotiationRounds() == negotiationRounds) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS)) {
                boolean optionalCritiqueOnQoS = Boolean.parseBoolean(partnerParameters.getValue());
                if (ownParameters.isOptionalCritiqueOnQoS() == optionalCritiqueOnQoS) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
                else {
                    partnerParameters.setCritique(CRITIQUE.CHANGE);
                    partnerParameters.setCounterValue("" + (!optionalCritiqueOnQoS));
                }
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.PROCESS_TIMEOUT)) {
                int processTimeout = Integer.parseInt(partnerParameters.getValue());
                if (ownParameters.getProcessTimeout() < processTimeout) {
                    partnerParameters.setCritique(CRITIQUE.DECREASE);
                    partnerParameters.setCounterValue(ownParameters.getProcessTimeout() + "");
                }
                else if (ownParameters.getProcessTimeout() > processTimeout) {
                    partnerParameters.setCritique(CRITIQUE.INCREASE);
                    partnerParameters.setCounterValue(ownParameters.getProcessTimeout() + "");
                }
                else if (ownParameters.getProcessTimeout() == processTimeout) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.CREDENTIALS)) {
                String credentials = partnerParameters.getValue();
                // Assuming that a Credential system like PKI based verification will test credential and ACCEPT or
                // REJECT.
                // Default behavior here is to accept it
                partnerParameters.setCritique(CRITIQUE.ACCEPTED);
            }
            else if (partnerParameters.getName().equals(NegotiationGlossary.IS_SEALED)) {
                // This parameter is not of interest to Bilateral Negotiations - so not to be used in that case.
                boolean isSealed = Boolean.parseBoolean(partnerParameters.getValue());
                if (ownParameters.isSealed() == isSealed) {
                    partnerParameters.setCritique(CRITIQUE.ACCEPTED);
                }
                else {
                    partnerParameters.setCritique(CRITIQUE.CHANGE);
                    partnerParameters.setCounterValue("" + (!isSealed));
                }
            }
        }
        return customization;
    }

    private SimplifiedNegotiableParameters extractProviderParametersFromCustomization(Customization customization) {
        SimplifiedNegotiableParameters simplifiedNegotiableParameters = new SimplifiedNegotiableParameters();
        simplifiedNegotiableParameters.setBelongsTo(Ownership.PARTNER);
        List<NegotiableParameters> listOfNegotiableParameters = customization.getListOfNegotiableParameters();
        for (NegotiableParameters negotiableParameter : listOfNegotiableParameters) {

            if (negotiableParameter.getName().equals(NegotiationGlossary.CUSTOMIZATION_ROUNDS)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setNumberOfCustomizationRounds(Integer.parseInt(negotiableParameter
                            .getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setNumberOfCustomizationRounds(Integer.parseInt(negotiableParameter
                            .getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.MAX_COUNTER_OFFERS)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setMaxCounterOffersAllowed(Integer.parseInt(negotiableParameter
                            .getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setMaxCounterOffersAllowed(Integer.parseInt(negotiableParameter
                            .getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.NEGOTIATION_ROUNDS)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setNumberOfNegotiationRounds(Integer.parseInt(negotiableParameter
                            .getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setNumberOfNegotiationRounds(Integer.parseInt(negotiableParameter
                            .getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setOptionalCritiqueOnQoS(Boolean.parseBoolean(negotiableParameter
                            .getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setOptionalCritiqueOnQoS(Boolean.parseBoolean(negotiableParameter
                            .getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.PROCESS_TIMEOUT)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setProcessTimeout(Integer.parseInt(negotiableParameter.getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setProcessTimeout(Integer.parseInt(negotiableParameter
                            .getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.IS_SEALED)) {
                if (negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                    simplifiedNegotiableParameters.setSealed(Boolean.parseBoolean(negotiableParameter.getValue()));
                }
                else {
                    simplifiedNegotiableParameters.setSealed(Boolean
                            .parseBoolean(negotiableParameter.getCounterValue()));
                }
            }
            else if (negotiableParameter.getName().equals(NegotiationGlossary.CREDENTIALS)) {
                simplifiedNegotiableParameters.setCredentials(negotiableParameter.getValue());
            }
        }

        return simplifiedNegotiableParameters;
    }

    private Customization createCustomizationFromSimplifiedNegotiableParameters(
            SimplifiedNegotiableParameters ownParameters) {

        if (this.session.getPreNegotiationOrNegotiationStartTime() == null) {
            // LOGGER.info("*** SETTING PRENEG-NEG-START_TIME IN SESSION ");
            this.session.setPreNegotiationOrNegotiationStartTime(new Long(ownParameters.getStartTime()));
        }
        else {
            // LOGGER.info("*** NOT SETTING PRENEG-NEG-START_TIME IN SESSION ");
        }

        Customization customization = new Customization();
        NegotiableParameters negotiableParameter1 = new NegotiableParameters(NegotiationGlossary.NEGOTIATION_ROUNDS);
        negotiableParameter1.setValue(ownParameters.getNumberOfNegotiationRounds() + "");
        negotiableParameter1.setCritique(CRITIQUE.ACCEPTED);

        NegotiableParameters negotiableParameter2 = new NegotiableParameters(NegotiationGlossary.PROCESS_TIMEOUT);
        negotiableParameter2.setValue(ownParameters.getProcessTimeout() + "");
        negotiableParameter2.setCritique(CRITIQUE.ACCEPTED);

        NegotiableParameters negotiableParameter3 = new NegotiableParameters(NegotiationGlossary.CUSTOMIZATION_ROUNDS);
        negotiableParameter3.setValue(ownParameters.getNumberOfCustomizationRounds() + "");
        negotiableParameter3.setCritique(CRITIQUE.ACCEPTED);

        NegotiableParameters negotiableParameter4 = new NegotiableParameters(NegotiationGlossary.MAX_COUNTER_OFFERS);
        negotiableParameter4.setValue(ownParameters.getMaxCounterOffersAllowed() + "");
        negotiableParameter4.setCritique(CRITIQUE.ACCEPTED);

        NegotiableParameters negotiableParameter5 =
                new NegotiableParameters(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS);
        negotiableParameter5.setValue(ownParameters.getOptionalCritiqueOnQoS() + "");
        negotiableParameter5.setCritique(CRITIQUE.ACCEPTED);

        List<NegotiableParameters> listOfNegotiableParameters = new ArrayList<NegotiableParameters>();
        listOfNegotiableParameters.add(negotiableParameter1);
        listOfNegotiableParameters.add(negotiableParameter2);
        listOfNegotiableParameters.add(negotiableParameter3);
        listOfNegotiableParameters.add(negotiableParameter4);
        listOfNegotiableParameters.add(negotiableParameter5);

        customization.setListOfNegotiableParameters(listOfNegotiableParameters);

        return customization;
    }
}
