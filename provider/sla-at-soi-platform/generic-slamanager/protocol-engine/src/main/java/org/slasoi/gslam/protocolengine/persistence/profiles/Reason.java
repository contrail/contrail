package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum Reason {
    
    DEFAULTER,
    PUNCTUAL,
    UNPROMISING,
    PROMISING

}
