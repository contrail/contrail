package org.slasoi.gslam.protocolengine.persistence.profiles;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StrategyRanks implements java.io.Serializable{
    
    private static final long serialVersionUID = -5679063958732485045L;
    private Integer id;
    private Rank customerRank;
    private Rank providerRank;
    private Rank productRank;
    private Rank negotiationRank;
    
    public StrategyRanks(){
        
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Rank getCustomerRank() {
        return customerRank;
    }

    public void setCustomerRank(Rank customerRank) {
        this.customerRank = customerRank;
    }

    public Rank getProviderRank() {
        return providerRank;
    }

    public void setProviderRank(Rank providerRank) {
        this.providerRank = providerRank;
    }

    public Rank getProductRank() {
        return productRank;
    }

    public void setProductRank(Rank productRank) {
        this.productRank = productRank;
    }

    public Rank getNegotiationRank() {
        return negotiationRank;
    }

    public void setNegotiationRank(Rank negotiationRank) {
        this.negotiationRank = negotiationRank;
    }
    
}
