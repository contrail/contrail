/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1674 $
 * @lastrevision   $Date: 2011-05-11 20:00:57 +0200 (sre, 11 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/StateEngine.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.protocolengine.IStateEngine;

import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseConfiguration;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.command.Command;
import org.drools.command.CommandFactory;
import org.drools.conf.MaintainTMSOption;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.rule.Rule;
import org.drools.io.ResourceFactory;
import org.drools.runtime.ExecutionResults;
import org.drools.runtime.StatefulKnowledgeSession;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.core.*;

/**
 * The <code>StateEngine</code> class is a thin wrapper over a rule engine. It is meant to provide a layer of
 * independence over a rule engine of choice (currently Drools). It converts Events from ProtocolEngine into Drools
 * commands.
 * 
 * @author Edwin
 * @version 1.0-SNAPSHOT
 */
public class StateEngine implements IStateEngine {

    private KnowledgeBuilder knowledgeBuilder;
    private KnowledgeBase knowledgeBase;
    private StatefulKnowledgeSession session;
    private SLAManagerContext context;
    private static final Logger LOGGER = Logger.getLogger(StateEngine.class);
    private String pathToDRL;
    private ClassLoader classLoader;

    public StateEngine() {
        LOGGER.info("*** INFO *** StateEngine - constructor ***");
    }

    public StateEngine(ClassLoader classLoader) {
        this.classLoader = classLoader;
        LOGGER.info("*** INFO *** StateEngine - classLoader = ***" + classLoader.getClass().toString());
    }

    public void setContext(SLAManagerContext context) {
        this.context = context;
        LOGGER.info("*** INFO *** StateEngine - CONTEXT HAS BEEN SET ***");
        init();
    }

    public void init() {
        LOGGER.info("inside init");
        knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        String pathToDRL = "";
        FileInputStream fstream = null;
        try {
            pathToDRL =
                    System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                            + System.getProperty("file.separator") + "protocol-engine"
                            + System.getProperty("file.separator") + "protocol-engine-config"
                            + System.getProperty("file.separator") + this.context.getSLAManagerID()
                            + System.getProperty("file.separator") + "protocol" + System.getProperty("file.separator")
                            + "BilateralNegotiationProtocolExtended.drl";
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        // LOGGER.info(pathToDRL);
        try {
            LOGGER.info("1- Trying to open this path = \n" + pathToDRL);
            fstream = new FileInputStream(pathToDRL);
        }
        catch (FileNotFoundException fnfe1) {
            LOGGER.info("2- Could not find file at above path.");
            // fnfe1.printStackTrace();
            // 2- File not found at SLA-specific location, that means we are using the one at default location.
            try {
                pathToDRL =
                        System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                                + System.getProperty("file.separator") + "protocol-engine"
                                + System.getProperty("file.separator") + "protocol-engine-config"
                                + System.getProperty("file.separator") + "protocol"
                                + System.getProperty("file.separator") + "BilateralNegotiationProtocolExtended.drl";
                LOGGER.info("3- Now trying to open this path = \n" + pathToDRL);
                fstream = new FileInputStream(pathToDRL);
            }
            catch (FileNotFoundException fnfe2) {
                LOGGER.info("4- Could not find file at above path as well.");
                // fnfe2.printStackTrace();
            }
            catch (Exception e1) {
                LOGGER.info("5- Exception caught");
            }
        }
        catch (Exception e1) {
            LOGGER.info("6- Exception caught");
        }
        
        try {
            if (fstream != null) {
                fstream.close();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        catch (Exception e1) {
            LOGGER.info("7- Exception caught");
        }

        assert (knowledgeBuilder != null) : "KnowledgeBuilder is null";
        knowledgeBuilder.add(ResourceFactory.newFileResource(pathToDRL), ResourceType.DRL);

        assert (!knowledgeBuilder.hasErrors()) : "KnowledgeBuilder has errors. Check syntax of Protocol file!";
        if (knowledgeBuilder.hasErrors()) {
            LOGGER.info("KnowledgeBuilder has errors: " + knowledgeBuilder.getErrors());
            return;
        }

        knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        /*
         * //Following code is test-code: Properties properties = new Properties(); properties.setProperty(
         * "org.drools.sequential", "true"); if(this.classLoader == null){ this.classLoader =
         * this.context.getClass().getClassLoader(); }
         * LOGGER.info("*** INFO *** StateEngine - Using classLoader = ***"+this.classLoader.getClass().toString());
         * KnowledgeBaseConfiguration kbConf = KnowledgeBaseFactory.newKnowledgeBaseConfiguration(properties,
         * this.classLoader); knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase(kbConf);
         */
        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
        session = knowledgeBase.newStatefulKnowledgeSession();
        try {
            LOGGER.info("*** INFO *** StateEngine of " + this.context.getSLAManagerID());
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
        }
//        KnowledgePackage kp = knowledgeBase.getKnowledgePackage("protocol");
//        assert (kp != null) : "KnowledgePackage not found under name protocol.";
//        Collection<Rule> rules = kp.getRules();
//        for (Rule rule : rules) {
//            LOGGER.info("*** INFO *** StateEngine Rule -> " + rule.getName());
//        }

        // session.fireAllRules();//Just a test instruction.

        /*
         * Message message = new Message(); message.setMessage( "Hello World" ); message.setStatus( Message.HELLO );
         * //session.insert( message );
         * 
         * Event startNegEvent = new Event(EventName.StartNegotiationEvent); List<Event> listOfEvents = new
         * ArrayList<Event>(); listOfEvents.add(startNegEvent); listOfEvents = (List<Event>) process(listOfEvents);
         * Event returnedStartNegEvent = listOfEvents.get(0); System.out.println("*************** Event: "+
         * returnedStartNegEvent.getEventName()+ "********* "+returnedStartNegEvent.isProcessedSuccessfully());
         * 
         * Event negEvent = new Event(EventName.ProposalArrivedEvent); listOfEvents.removeAll(listOfEvents);
         * listOfEvents.add(negEvent); listOfEvents = (List<Event>) process(listOfEvents); Event returnedNegEvent =
         * listOfEvents.get(0); System.out.println("*************** Event: "+ returnedNegEvent.getEventName()+
         * "********* "+returnedNegEvent.isProcessedSuccessfully());
         * 
         * listOfEvents.removeAll(listOfEvents); listOfEvents.add(new Event(EventName.ProposalArrivedEvent));
         * listOfEvents = (List<Event>) process(listOfEvents); Event returnedNegEvent2 = listOfEvents.get(0);
         * System.out.println("*************** Event: "+ returnedNegEvent2.getEventName()+
         * "********* "+returnedNegEvent2.isProcessedSuccessfully());
         * 
         * listOfEvents.removeAll(listOfEvents); listOfEvents.add(new Event(EventName.ProposalArrivedEvent));
         * listOfEvents = (List<Event>) process(listOfEvents); Event returnedNegEvent3 = listOfEvents.get(0);
         * System.out.println("*************** Event: "+ returnedNegEvent3.getEventName()+
         * "********* "+returnedNegEvent3.isProcessedSuccessfully());
         */

        /*
         * Event startNegEvent = new Event(EventName.StartNegotiationEvent); List<Event> listOfEvents = new
         * ArrayList<Event>(); listOfEvents.add(startNegEvent); listOfEvents.add(message); ExecutionResults results =
         * (ExecutionResults)process(listOfEvents);
         * System.out.println("***********************************************************");
         * 
         * Event negEvent = new Event(EventName.ProposalArrivedEvent); listOfEvents.removeAll(listOfEvents);
         * listOfEvents.add(negEvent); results = (ExecutionResults)process(listOfEvents);
         * System.out.println("***********************************************************");
         * 
         * listOfEvents.removeAll(listOfEvents); Event negEvent2 = new Event(EventName.ProposalArrivedEvent);
         * listOfEvents.add(negEvent2); results = (ExecutionResults)process(listOfEvents);
         * System.out.println("***********************************************************");
         * 
         * listOfEvents.removeAll(listOfEvents); Event negEvent3 = new Event(EventName.ProposalPartyEvent);
         * listOfEvents.add(negEvent3); results = (ExecutionResults)process(listOfEvents);
         * System.out.println("***********************************************************");
         * listOfEvents.removeAll(listOfEvents);
         */
    }

    public Object process(Object listOfEvents) {
        List<Event> eventsList = (List<Event>) listOfEvents;
        // Convert the listOfEvents received into Drools Commands and batch execute as follows:
        List<Command> commands = convertEventsToCommands(eventsList);
        ExecutionResults results = session.execute(CommandFactory.newBatchExecution(commands));
        int i = session.fireAllRules();// StatelessSession does this implicitly, use will override implicit call.
        // Now, convert all the received result objects back to Events, put them in a List and return.
        eventsList = convertResultsToEvents(results, eventsList.size());
        // The rules should remove them from the session as well so they dont go on firing once their purpose is done!
        return eventsList;
    }

    public List<Command> convertEventsToCommands(List<Event> listOfEvents) {
        List<Command> commands = new ArrayList<Command>();
        if (listOfEvents != null && listOfEvents.size() > 0) {
            int counter = 0;
            for (Event e : listOfEvents) {
                // cmds.add( CommandFactory.newInsert( new Person("edwin", "yaqub","father", 29, false), "p1" ) );
                commands.add(CommandFactory.newInsert(e, counter + ""));
                counter++;
            }
        }
        return commands;
    }

    public List<Event> convertResultsToEvents(ExecutionResults results, int length) {
        List<Event> listOfEvents = new ArrayList<Event>();
        for (int i = 0; i < length; i++) {
            listOfEvents.add((Event) results.getValue(i + ""));
        }
        return listOfEvents;
    }

    // This method to be called at the end of createAgreement() and cancelNegotiation().
    public void disposeWorkingMemory() {
        // Call dispose on session
        session.dispose();
    }

//    public static void main(String a[]){
//            StateEngine stateEngine = new StateEngine(); 
//            stateEngine.init(); 
//    }
    
}