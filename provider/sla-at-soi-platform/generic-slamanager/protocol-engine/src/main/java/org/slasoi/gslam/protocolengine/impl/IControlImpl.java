/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1214 $
 * @lastrevision   $Date: 2011-04-04 13:17:30 +0200 (pon, 04 apr 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/IControlImpl.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.rule.Rule;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.control.*;

/**
 * The <code>IControlImpl</code> class implements the <code>IControl</code> interface. It allows the administrative
 * users to retrieve and modify the protocol rules as <code>Policy</code> objects.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public class IControlImpl implements IControl {

    private SLAManagerContext context;
    private String pathToDRL;
    private KnowledgeBuilder knowledgeBuilder;
    private KnowledgeBase knowledgeBase;
    private StatefulKnowledgeSession session;
    private static final Logger LOGGER = Logger.getLogger(IControlImpl.class);

    /**
     * Default constructor.
     */
    public IControlImpl() {
//        initializeLogger();
        LOGGER.info("Inside IControlImpl()");
    }
    
//    private void initializeLogger()
//    {
//      Properties logProperties = new Properties();
//      try
//      {
//        logProperties.load(new FileReader(System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
//                + "generic-slamanager" + System.getProperty("file.separator") + "protocol-engine"
//                + System.getProperty("file.separator") + "protocol-engine-config" + System.getProperty("file.separator") + "log4j.properties") );
//        PropertyConfigurator.configure(logProperties);
//        LOGGER.info("Logging initialized.");
//      }
//      catch(IOException e)
//      {
//        throw new RuntimeException("Unable to load logging property ");
//      }
//    }

    /**
     * Gets the protocol policies of this SLA-Manager.
     * 
     * @param policyClassType
     *            the string containing the type of Policy
     * @return Policy[] representing the policies of this SLA-Manager
     */
    public Policy[] getPolicies(String policyClassType) {
        LOGGER.info("**** INSIDE getPolicies *****");
        String fileContent = "";
        Policy[] policies = null;
        FileInputStream fstream = null;
        if (policyClassType.equals(PolicyClassType.NEGOTIATION_POLICY)) {
            try {
                // 1- Look at SLA-specific location first:
                try {
                    pathToDRL =
                            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator") + "protocol-engine"
                                    + System.getProperty("file.separator") + "protocol-engine-config"
                                    + System.getProperty("file.separator") + this.context.getSLAManagerID()
                                    + System.getProperty("file.separator") + "protocol"
                                    + System.getProperty("file.separator") + "BilateralNegotiationProtocolExtended.drl";
                }
                catch (SLAManagerContextException e) {
                    e.printStackTrace();
                }
                LOGGER.info(pathToDRL);
                try {
                    LOGGER.info("1- Trying to open this path = \n" + pathToDRL);
                    fstream = new FileInputStream(pathToDRL);
                }
                catch (FileNotFoundException fnfe1) {
                    LOGGER.info("2- Could not find file at above path.");
                    // fnfe1.printStackTrace();
                    // 2- File not found at SLA-specific location, that means we are using the one at default location.
                    try {
                        pathToDRL =
                                System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                                        + "generic-slamanager" + System.getProperty("file.separator")
                                        + "protocol-engine" + System.getProperty("file.separator")
                                        + "protocol-engine-config" + System.getProperty("file.separator") + "protocol"
                                        + System.getProperty("file.separator") + "BilateralNegotiationProtocolExtended.drl";
                        LOGGER.info("3- Now trying to open this path = \n" + pathToDRL);
                        fstream = new FileInputStream(pathToDRL);
                    }
                    catch (FileNotFoundException fnfe2) {
                        LOGGER.info("4- Could not find file at above path as well.");
                        // fnfe2.printStackTrace();
                        return null;
                    }
                }
                // Extract Rules from the KnowledgeBuilder:
                policies = createPoliciesFromRules(pathToDRL);
            }
            catch (Exception e) {
                LOGGER.error("Error: " + e.getMessage());
            }
        }
        else if (policyClassType.equals(PolicyClassType.ADJUSTMENT_POLICY)) {
            try {
                this.context.getProvisioningAdjustment().getPolicies(policyClassType); // Delegating to PAC!
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
            }
        }
        return policies;
    }

    /**
     * Sets the protocol policies of this SLA-Manager.
     * 
     * @param policyClassType
     *            the string containing the type of Policy
     * @param Policy
     *            [] an array of policy representing the policies of this SLA-Manager to be stored in protocol file as
     *            rules with metadata
     * @return int value 1 if successful, -1 otherwise
     */
    public int setPolicies(String policyClassType, Policy[] policies) {
        LOGGER.info("**** INSIDE setPolicies *****");
        boolean result = false;

        assert (policies != null && policies.length > 0) : "Null of Empty Policies provided.";
        if (policyClassType.equals(PolicyClassType.NEGOTIATION_POLICY)) {
            try {
                FileInputStream fstream = null;
                try {
                    pathToDRL =
                            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator") + "protocol-engine"
                                    + System.getProperty("file.separator") + "protocol-engine-config"
                                    + System.getProperty("file.separator") + this.context.getSLAManagerID()
                                    + System.getProperty("file.separator") + "protocol";
                }
                catch (SLAManagerContextException e) {
                    e.printStackTrace();
                }
                LOGGER.info(pathToDRL);
                File file = new File(pathToDRL);
                file.mkdirs();
                pathToDRL += System.getProperty("file.separator") + "BilateralNegotiationProtocolExtended.drl";
                FileWriter fileWriter = new FileWriter(pathToDRL);
                BufferedWriter out = new BufferedWriter(fileWriter);
                String rules = extractRulesFromPolicyObjects(policies);
                out.write(rules);
                // Close the output stream
                out.close();
                result = true;
            }
            catch (Exception e) {
                LOGGER.info("Error: " + e.getMessage());
            }
        }
        else if (policyClassType.equals(PolicyClassType.ADJUSTMENT_POLICY)) {
            try {
                this.context.getProvisioningAdjustment().setPolicies(policyClassType, policies);// Delegating to PAC!
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
            }
        }
        return (result == true ? 1 : -1);
    }

    /**
     * Sets the SLA-Manager context at instance level.
     * 
     * @param context
     *            the slaManagerContext
     */
    public void setContext(SLAManagerContext context) {
        this.context = context;
    }

    protected String extractRulesFromPolicyObjects(Policy[] policies) {
        String rulesString = "package protocol;" + "\n\n";
        for (Policy policy : policies) {
            int firstIndexOfClosedQuote = policy.getRule().indexOf("\"", policy.getRule().indexOf("\"") + 1);
            String ruleString = policy.getRule().substring(0, firstIndexOfClosedQuote + 1);
            if (policy.getId() + "".length() > 0)
                ruleString += ("\n@id(" + policy.getId() + ")");
            if (policy.getName() != null)
                ruleString += ("\n@name(\"" + policy.getName() + "\"" + ")");
            if (policy.getRule() != null)
                ruleString += ("\n@rulebody(" + policy.getRule() + ")");
            if (policy.getContext() != null) {
                ruleString += ("\n@ContextId(" + policy.getContext().getId() + ")");
                ruleString += ("\n@ContextName(\"" + policy.getContext().getName() + "\"" + ")");
                ruleString += ("\n@ContextContent(\"" + policy.getContext().getContent() + "\"" + ")");
            }
            ruleString += policy.getRule().substring(firstIndexOfClosedQuote + 1);
            rulesString += (ruleString + "\n\n");
        }
        return rulesString;
    }

    protected Policy[] createPoliciesFromRules(String pathToDRL) {
        KnowledgeBuilder knowledgeBuilder;
        KnowledgeBase knowledgeBase;
        knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        knowledgeBuilder.add(ResourceFactory.newFileResource(pathToDRL), ResourceType.DRL);
        if (knowledgeBuilder.hasErrors()) {
            LOGGER.info("KnowledgeBuilder has errors: " + knowledgeBuilder.getErrors());
            return null;
        }
        knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());

        KnowledgePackage kp = knowledgeBase.getKnowledgePackage("protocol");
        assert (kp != null) : "KnowledgePackage not found under name protocol.";
        Collection<Rule> rules = kp.getRules();
        Policy[] policies = new Policy[rules.size()];
        Iterator<Rule> rulesIterator = rules.iterator();
        int counter = 0;
        for (Rule rule : rules) {
            Policy policy = new Policy();
            policy.setId(Long.parseLong(rule.getMetaAttribute("id")));
            policy.setName(rule.getMetaAttribute("name"));
            policy.setRule(rule.getMetaAttribute("rulebody"));
            if(rule.getMetaAttribute("ContextId") != null) { //Use of Context is optional in Rule Annotations
              Context context = new Context();
              context.setId(Long.parseLong(rule.getMetaAttribute("ContextId")));
              context.setName(rule.getMetaAttribute("ContextName"));
              context.setContent(rule.getMetaAttribute("ContextContent"));
              policy.setContext(context);
            }
            policies[counter] = policy;
            counter++;
        }
        return policies;
    }
    
//    public static void main(String[] args) {
//        IControlImpl impl = new IControlImpl();
//        Policy[] policies = impl.getPolicies(PolicyClassType.NEGOTIATION_POLICY);
//        for(Policy policy : policies){
//            System.out.println("Policy Id = "+policy.getId()+" - Policy Name = "+policy.getName()+" - Policy Rule:");
//            System.out.println(policy.getRule());
//        }
//    }

}
