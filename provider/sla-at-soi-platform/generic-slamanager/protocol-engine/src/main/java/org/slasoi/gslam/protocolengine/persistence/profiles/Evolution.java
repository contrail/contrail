package org.slasoi.gslam.protocolengine.persistence.profiles;


public enum Evolution {
    
    DECREASING,
    STABLE,
    GROWING

}
