/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1674 $
 * @lastrevision   $Date: 2011-05-11 20:00:57 +0200 (sre, 11 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/INegotiationOutgoing.java $
 */

package org.slasoi.gslam.protocolengine;

import java.util.List;

import org.slasoi.gslam.core.negotiation.INegotiation.CancellationReason;
import org.slasoi.gslam.core.negotiation.INegotiation.Customization;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.NegotiationException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.ProvisioningException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>INegotiationOutgoing</code> interface is internal to <code>ProtocolEngine</code>. It is required by
 * <code>NegotiationManager</code> and provided (implemented) by <code>MessageHandler</code>.
 * 
 * @author Edwin
 * @version 1.0-SNAPSHOT
 * @see org.slasoi.gslam.core.negotiation.INegotiation
 */
public interface INegotiationOutgoing {

    /**
     * Invokes initiate negotiation between current SLA-Manager and destination SLA-Manager.
     * 
     * @param slaTemplate
     *            the slaTemplate used to setup negotiation session
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return String representing the id of the negotiation session established
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String initiateNegotiationOutgoing(SLATemplate slaTemplate, String endpointAddress)
            throws OperationNotPossibleException;

    /**
     * Invokes negotiate on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the offer to be made
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLATemplate array representing counter offers received from the other SLA-Manager
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLATemplate[] negotiateOutgoing(String negotiationID, SLATemplate slaTemplate, String endpointAddress)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException;

    /**
     * Invokes createAgreement on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the final offer to be made
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLA created, if at all
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws SLACreationException
     *             if sla could not be created
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLA createAgreementOutgoing(String negotiationID, SLATemplate slaTemplate, String endpointAddress)
            throws OperationInProgressException, SLACreationException, InvalidNegotiationIDException;

    /**
     * Invokes cancelNegotiation on destination SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param cancellationReason
     *            list of cancellation reasons that must be provided
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return boolean value showing if negotiation could be cancelled or not
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public boolean cancelNegotiationOutgoing(String negotiationID, List<CancellationReason> cancellationReason,
            String endpointAddress) throws OperationInProgressException, OperationNotPossibleException,
            InvalidNegotiationIDException;

    /**
     * Invokes renegotiate on destination SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to renegotiate over
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return String representing the id of the negotiation session established
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String renegotiateOutgoing(UUID slaID, String endpointAddress) throws SLANotFoundException,
            OperationNotPossibleException;

    /**
     * Invokes terminate on destination SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to terminate
     * @param terminationReason
     *            list of termination reasons that must be provided
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return boolean value showing if termination was successful or not
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     */
    public boolean terminateOutgoing(UUID slaID, List<TerminationReason> terminationReason, String endpointAddress)
            throws SLANotFoundException;

    /**
     * This method is here for legacy reasons. It is not to be used. Provisioning is related to SLARegistry and PAC.
     * 
     * @param slaID
     *            the uuid of the sla to provision
     * @param endpointAddress
     *            the string representing epr of destination SLA-Manager
     * @return SLA with provisioning information, if at all
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the destination SLA-Manager
     * @throws ProvisioningException
     *             if provisioning was not possible
     * @see org.slasoi.gslam.core.poc.PlanningOptimization.INotification.activate(SLA sla)
     * @see org.slasoi.gslam.core.pac.ProvisioningAdjustment
     */
    public SLA provisionOutgoing(UUID slaID, String endpointAddress) throws SLANotFoundException, ProvisioningException;

    public Customization customizeOutgoing(String negotiationID, Customization customization, String endpointAddress)
            throws NegotiationException;
}
