package org.slasoi.gslam.protocolengine.persistence.profiles;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BusinessHistory implements java.io.Serializable{
    
    private static final long serialVersionUID = -1870242138380534875L;
    private Integer id;
    private String slaId;
    private String slaTemplateId;
    private Level billingSatisfactionLevel;
    private Level serviceSatisfactionLevel;
    private Level slaWorth;
    private int penaltiesAllowed;
    private double allowedPenaltiesAmount;
    private double currentPenaltiesAmount;
    private Level penaltySatisfactionLevel;
    private SLAStatus slaStatus;
    
    public BusinessHistory(){
        
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getSlaTemplateId() {
        return slaTemplateId;
    }

    public void setSlaTemplateId(String slaTemplateId) {
        this.slaTemplateId = slaTemplateId;
    }

    public Level getBillingSatisfactionLevel() {
        return billingSatisfactionLevel;
    }

    public void setBillingSatisfactionLevel(Level billingSatisfactionLevel) {
        this.billingSatisfactionLevel = billingSatisfactionLevel;
    }

    public Level getServiceSatisfactionLevel() {
        return serviceSatisfactionLevel;
    }

    public void setServiceSatisfactionLevel(Level serviceSatisfactionLevel) {
        this.serviceSatisfactionLevel = serviceSatisfactionLevel;
    }

    public Level getSlaWorth() {
        return slaWorth;
    }

    public void setSlaWorth(Level slaWorth) {
        this.slaWorth = slaWorth;
    }

    public int getPenaltiesAllowed() {
        return penaltiesAllowed;
    }

    public void setPenaltiesAllowed(int penaltiesAllowed) {
        this.penaltiesAllowed = penaltiesAllowed;
    }

    public double getAllowedPenaltiesAmount() {
        return allowedPenaltiesAmount;
    }

    public void setAllowedPenaltiesAmount(double allowedPenaltiesAmount) {
        this.allowedPenaltiesAmount = allowedPenaltiesAmount;
    }

    public double getCurrentPenaltiesAmount() {
        return currentPenaltiesAmount;
    }

    public void setCurrentPenaltiesAmount(double currentPenaltiesAmount) {
        this.currentPenaltiesAmount = currentPenaltiesAmount;
    }

    public Level getPenaltySatisfactionLevel() {
        return penaltySatisfactionLevel;
    }

    public void setPenaltySatisfactionLevel(Level penaltySatisfactionLevel) {
        this.penaltySatisfactionLevel = penaltySatisfactionLevel;
    }

    public SLAStatus getSlaStatus() {
        return slaStatus;
    }

    public void setSlaStatus(SLAStatus slaStatus) {
        this.slaStatus = slaStatus;
    }

}
