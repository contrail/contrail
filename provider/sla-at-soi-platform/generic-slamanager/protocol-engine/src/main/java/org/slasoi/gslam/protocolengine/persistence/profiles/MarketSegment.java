package org.slasoi.gslam.protocolengine.persistence.profiles;


public enum MarketSegment {
    
    INDIVIDUAL,
    SME,
    LARGE_ENTERPRISE,
    MNE
    
}
