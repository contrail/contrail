/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1207 $
 * @lastrevision   $Date: 2011-04-04 11:20:27 +0200 (pon, 04 apr 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/State.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * The <code>State</code> class is used while defining the state machine of the protocol in terms of rules.
 * 
 * @author Edwin
 * @version 1.0-SNAPSHOT
 */
public class State {

    private StateStatus status;
    private StateName name;
    private boolean isCurrentState;

    private int numberOfHopsAllowed;
    private int currentHop;

    public State(StateName name) {
        this.name = name;
        this.currentHop = 0;
        this.status = StateStatus.NOT_STARTED;
    }
    
    public State(StateName name, StateStatus status) {
        this.name = name;
        this.currentHop = 0;
        this.status = status;
    }

    public StateName getName() {
        return this.name;
    }

    public void setName(StateName name) {
        this.name = name;
    }

    public StateStatus getStatus() {
        return this.status;
    }

    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public void setStatus(StateStatus newStatus) {
        final StateStatus oldStatus = this.status;
        this.status = newStatus;
        this.changes.firePropertyChange("status", oldStatus, newStatus);
    }

    public boolean inState(final StateName name, final StateStatus status) {
        return this.name.equals(name) && this.status == status;
    }

    public String toString() {
        switch (this.status) {
        case STOPPED:
            return this.name + "[" + "STOPPED" + "]";
        case RUNNING:
            return this.name + "[" + "RUNNING" + "]";
        case READY_TO_TRANSIT:
            return this.name + "[" + "STARTING" + "]";
        default:
            return this.name + "[" + "NOT STARTED" + "]";
        }
    }

    public void addPropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
        this.changes.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
        this.changes.removePropertyChangeListener(propertyChangeListener);
    }

    public void onEntry() {
        System.out.println("Inside onEntry() of State[" + getName() + "] having status:" + getStatus());
    }

    public void onExit() {

    }

    public int getNumberOfHopsAllowed() {
        return numberOfHopsAllowed;
    }

    public void setNumberOfHopsAllowed(int numberOfHopsAllowed) {
        this.numberOfHopsAllowed = numberOfHopsAllowed;
    }

    public int getCurrentHop() {
        return currentHop;
    }

    public void setCurrentHop(int newValue) {
        final int oldValue = this.currentHop;
        this.currentHop = newValue;
        this.changes.firePropertyChange("currentHop", oldValue, newValue);
    }

    public boolean isCurrentState() {
        return isCurrentState;
    }

    public void setCurrentState(boolean isCurrentState) {
        this.isCurrentState = isCurrentState;
    }

}
