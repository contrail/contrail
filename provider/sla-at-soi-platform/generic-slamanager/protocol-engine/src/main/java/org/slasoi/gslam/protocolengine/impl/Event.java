/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 1674 $
 * @lastrevision   $Date: 2011-05-11 20:00:57 +0200 (sre, 11 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/Event.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Timestamp;

import org.slasoi.gslam.protocolengine.persistence.profiles.Profile;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>Event</code> class is used by the <code>NegotiationManager</code> class to send information about invoked
 * operation along with some related information to the <code>StateEngine</code>.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public class Event {

    private EventName eventName;
    private Timestamp arrivalTime;
    private boolean processedSuccessfully = false;
    private String processingAfterMath;
    private SLATemplate offer;
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private SimplifiedNegotiableParameters simplifiedNegotiableParameters;
    private String negotiationId; //support to be provided by PE
    private int numberOfCounterOffersGeneratedByOwnPOC; //support to be provided by PE
    private Profile partnerProfile;
    /**
     * Default constructor.
     */
    public Event() {
        arrivalTime = new Timestamp(System.currentTimeMillis());
    }

    /**
     * Constructor with name of Event.
     * 
     * @param eventName
     *            the name of the Event
     */
    public Event(EventName eventName) {
        this.eventName = eventName;
        arrivalTime = new Timestamp(System.currentTimeMillis());
    }

    /**
     * Gets the eventName object.
     * 
     * @return EventName of the Event
     */
    public EventName getEventName() {
        return eventName;
    }

    /**
     * Sets the eventName of the Event.
     * 
     * @param eventName
     *            the name of the Event
     */
    public void setEventName(EventName eventName) {
        final EventName oldEventName = this.eventName;
        this.eventName = eventName;
        this.changes.firePropertyChange("eventName", oldEventName, eventName);
    }

    /**
     * Adds the <code>PropertyChangeListener</code> to this class.
     * 
     * @param propertyChangeListener
     *            the PropertyChangeListener to add to listener list
     */
    public void addPropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
        this.changes.addPropertyChangeListener(propertyChangeListener);
    }

    /**
     * Removes the <code>PropertyChangeListener</code> to this class.
     * 
     * @param propertyChangeListener
     *            the PropertyChangeListener to remove from listener list
     */
    public void removePropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
        this.changes.removePropertyChangeListener(propertyChangeListener);
    }

    /**
     * Gets the arrivalTime object.
     * 
     * @return Timestamp representing the arrival time of Event
     */
    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the arrivalTime of the Event.
     * 
     * @param arrivalTime
     *            the timestamp of the Event
     */
    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Gets the processedSuccessfully property of the Event.
     * 
     * @return boolean parameter that shows if Event is successfully processed
     */
    public boolean isProcessedSuccessfully() {
        return processedSuccessfully;
    }

    /**
     * Sets the processedSuccessfully property of the Event.
     * 
     * @param processedSuccessfully
     *            the boolean parameter that shows if Event is successfully processed
     */
    public void setProcessedSuccessfully(boolean processedSuccessfully) {
        this.processedSuccessfully = processedSuccessfully;
    }

    /**
     * Gets the processingAfterMath of the Event.
     * 
     * @return String representing a possible outcome message of the Event
     */
    public String getProcessingAfterMath() {
        return processingAfterMath;
    }

    /**
     * Sets the processingAfterMath of the Event.
     * 
     * @param processingAfterMath
     *            the string representing a possible outcome message of the Event
     */
    public void setProcessingAfterMath(String processingAfterMath) {
        this.processingAfterMath = processingAfterMath;
    }

    /**
     * Gets the offer object.
     * 
     * @return SLATemplate representing the offer accompanying the Event
     */
    public SLATemplate getOffer() {
        return offer;
    }

    /**
     * Sets the SLATemplate in the Event.
     * 
     * @param offer
     *            the slaTemplate accompanying the Event
     */
    public void setOffer(SLATemplate offer) {
        this.offer = offer;
    }

    public SimplifiedNegotiableParameters getSimplifiedNegotiableParameters() {
        return simplifiedNegotiableParameters;
    }

    public void setSimplifiedNegotiableParameters(SimplifiedNegotiableParameters simplifiedNegotiableParameters) {
        this.simplifiedNegotiableParameters = simplifiedNegotiableParameters;
    }

    public String getNegotiationId() {
        return negotiationId;
    }

    public void setNegotiationId(String negotiationId) {
        this.negotiationId = negotiationId;
    }

    public int getNumberOfCounterOffersGeneratedByOwnPOC() {
        return numberOfCounterOffersGeneratedByOwnPOC;
    }

    public void setNumberOfCounterOffersGeneratedByOwnPOC(int numberOfCounterOffersGeneratedByOwnPOC) {
        this.numberOfCounterOffersGeneratedByOwnPOC = numberOfCounterOffersGeneratedByOwnPOC;
    }

    public Profile getPartnerProfile() {
        return partnerProfile;
    }

    public void setPartnerProfile(Profile partnerProfile) {
        this.partnerProfile = partnerProfile;
    }
    
}
