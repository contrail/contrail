The ProtocolEngine is an integral component of GenericSLAManager. It is therefore inherited by all
concrete implementations of GenericSLAManager. It provides a generic mechanism to conduct negotiations
on behalf of the SLAManager. As such, a ProtocolEngine can only be tested in an integration test environment
where two or more SLAManagers communicate and negotiate with each other using their individual ProtocolEngines.
Unit testing therefore is not enough or even applicable to the ProtocolEngine. 

The SLAManager and other components are realized as OSGi bundles in the SLA@SOI project. This means that integration
tests must be conducted in an OSGi container. A basic integration test bundle was realized and several integration
tests were run which are available at following location:

http://sla-at-soi.svn.sourceforge.net/viewvc/sla-at-soi/trunk/generic-slamanager/commons/test-negotiation

The output logs of some of these tests are documented at the following location, where two SLAManagers negotiate 
with each other:

http://sla-at-soi.svn.sourceforge.net/viewvc/sla-at-soi/trunk/generic-slamanager/commons/test-negotiation/src/main/resources/test-logs/