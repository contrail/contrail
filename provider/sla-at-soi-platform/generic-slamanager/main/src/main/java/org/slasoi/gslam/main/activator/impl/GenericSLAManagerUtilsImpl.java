/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/main/src/main/java/org/slasoi/gslam/main/activator/impl/GenericSLAManagerUtilsImpl.java $
 */

package org.slasoi.gslam.main.activator.impl;

import java.util.Hashtable;
import java.util.Vector;

import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;

/**
 * This class maintains a list of SLAManagers created under the same VM / OSGi via the
 * {@link GenericSLAManager#createContext(org.osgi.framework.BundleContext, String, String, String, String)}.
 * 
 * This class exposes its functionality as OSGi-service
 * 
 * @see
 * @author Miguel Rojas (UDO)
 * 
 */
public class GenericSLAManagerUtilsImpl implements GenericSLAManagerUtils {
    /**
     * Default constructor
     */
    public GenericSLAManagerUtilsImpl() {
        contextSet = new Hashtable<String, Vector<SLAManagerContext>>();
    }

    /**
     * Returns an array of <i>SLAManagerContext</i> associated to the groupID
     */
    public SLAManagerContext[] getContextSet(String groupID) throws GenericSLAManagerUtilsException {
        if (groupID == null)
            groupID = "";

        Vector<SLAManagerContext> vector = contextSet.get(groupID);
        if (vector == null || vector.isEmpty())
            return null;

        return vector.toArray(new SLAManagerContext[] {});
    }

    /**
     * Registers the specified context
     */
    public void register(SLAManagerContext context) throws GenericSLAManagerUtilsException {
        try {
            String groupID = context.getGroupID();
            if (groupID == null)
                groupID = "";

            Vector<SLAManagerContext> set = contextSet.get(groupID);
            if (set == null) {
                set = new Vector<SLAManagerContext>(3, 2);
                contextSet.put(groupID, set);
            }

            set.add(context);
        }
        catch (Exception e) {
            throw new GenericSLAManagerUtilsException(e.toString());
        }
    }

    /**
     * ContextSet. It contains references to all context created via <i>createContext</i>
     */
    protected Hashtable<String, Vector<SLAManagerContext>> contextSet;
}
