/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/main/src/main/java/org/slasoi/gslam/main/activator/impl/DefaultGslamSubscriber.java $
 */

package org.slasoi.gslam.main.activator.impl;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.sa.pss4slam.core.ISubscribable;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * This class is responsible for storing SLA-templates coming from AdvertisementsSystem
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class DefaultGslamSubscriber implements Runnable {
    /**
     * Constructor using a SLAManagerContext
     * 
     * @param ctx
     *            Owner of this subscriber
     */
    public DefaultGslamSubscriber(SLAManagerContext ctx) {
        context = ctx;
    }

    /**
     * Starts a thread for checking the SLATemplates coming from AdvertisementsSystem
     */
    public void start() {
        Thread tr = new Thread(this);
        tr.start();
    }

    /**
     * Runnable method for the thread created in {@link DefaultGslamSubscriber#start()}
     */
    public void run() {
        try {
            ServiceAdvertisement sa = context.getServiceAdvertisement();

            assert (sa != null) : "A ServiceAdvertisement reference is required";

            ISubscribable iSubscribable = sa.getISubscribable();
            iSubscribable.subscribe(new ISubscriber() {
                public void notify(Template<?>[] templates) {
                    try {
                        LOGGER.info("DefaultGslamSubscriber :: getting templates from pss4slam :: " + templates.length
                                + " (items)");
                        SLATemplateRegistry slaTemplateRegistry = context.getSLATemplateRegistry();

                        for (int i = 0; i < templates.length; i++) {
                            @SuppressWarnings("unchecked")
                            Template<SLATemplate> tmp = (Template<SLATemplate>) templates[i];

                            assert (tmp != null & tmp.getContent() != null) : "A NULL template was received from AdvertisementsService";

                            Metadata mtd = getMetadata(tmp.getContent());
                            slaTemplateRegistry.addSLATemplate(tmp.getContent(), mtd);
                        }
                    }
                    catch (Exception e) {
                        LOGGER.error(e);
                    }
                }

            }, ServiceAdvertisementChannel.SLAMODEL);
        }
        catch (Exception exc) {
            LOGGER.error(exc);
        }
    }

    /**
     * Helper method for initialization of the metadata of the just received SLATemplate
     * 
     * @param owner
     *            SLATemplate
     * 
     * @return
     */
    protected Metadata getMetadata(SLATemplate owner) {
        Metadata result = new Metadata();

        String providerID = getProvider(owner);
        String templateID = owner.getUuid().getValue();

        result.setPropertyValue(new STND("md:registrar_id"), "md:auto_registered_from_service_ads");
        result.setPropertyValue(new STND("md:provider_uuid"), providerID);
        result.setPropertyValue(new STND("md:template_uuid"), templateID);

        return result;
    }

    /**
     * Helper method.
     * 
     * @param owner
     * 
     * @return the provider name of the specified template
     */
    private String getProvider(SLATemplate owner) {
        // org.slasoi.slamodel.vocab.sla.$provider
        String result = "";

        Party[] parties = owner.getParties();
        for (Party p : parties) {
            if (p.getAgreementRole().getValue().equalsIgnoreCase("provider")) {
                result = p.getId().getValue();
                break;
            }
        }

        return result;

    }

    protected SLAManagerContext context;

    private static final Logger LOGGER = Logger.getLogger(DefaultGslamSubscriber.class);
}
