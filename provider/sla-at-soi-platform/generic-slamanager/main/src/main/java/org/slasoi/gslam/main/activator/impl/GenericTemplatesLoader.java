/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/main/src/main/java/org/slasoi/gslam/main/activator/impl/GenericTemplatesLoader.java $
 */

package org.slasoi.gslam.main.activator.impl;

import java.io.File;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContextAdapter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Helper class of GenericSLAManager for loading of SLA-templates during the creation of SLAManagerContext.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class GenericTemplatesLoader {
    /**
     * Constructor
     * 
     * @param context
     *            : owner of this loader
     */
    public GenericTemplatesLoader(SLAManagerContext context) {
        this.context = context;
    }

    /**
     * Loader of SLATemplates using the specified configuration file
     * 
     * @param config
     * @param slaManagerID
     */
    public SLATemplate[] getSLATemplates(String config, String slaManagerID) {
        try {
            Hashtable<SyntaxConverterType, ISyntaxConverter> syntaxConverters = context.getSyntaxConverters();
            ISyntaxConverter slasoi = syntaxConverters.get(SyntaxConverterType.SLASOISyntaxConverter);

            TemplatesGroup[] templates = getListOfTemplates(config, slaManagerID);

            if (templates == null || templates.length == 0) {
                LOGGER.info("#-- GenericTemplatesLoader :: templates will not be loaded. (for details, check templates configuration file)");
                return null;
            }

            LOGGER.info("#-- GenericTemplatesLoader tries to load following templates '" + Arrays.toString(templates)
                    + "'");

            Vector<SLATemplate> result = new Vector<SLATemplate>(3, 2);
            for (TemplatesGroup gr : templates) {
                if (gr.name != null && gr.name.equals(slaManagerID)) {
                    Vector<String> files = gr.templates;
                    for (String f : files) {
                        String slaTemplateXml = FileUtils.readFileToString(new File(f));
                        SLATemplate slaTemplate = (SLATemplate) slasoi.parseSLATemplate(slaTemplateXml);
                        result.add(slaTemplate);
                    }
                }
            }

            if (!result.isEmpty()) {
                return result.toArray(new SLATemplate[] {});
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Helper method
     */
    protected TemplatesGroup[] getListOfTemplates(String configfilename, String slaManagerID) {
        try {
            String separator = System.getProperty("file.separator");

            String orcPath = System.getenv("SLASOI_ORC_HOME");
            if (orcPath == null)
                orcPath = "";

            String templatesPath = System.getenv("SLASOI_HOME");
            if (templatesPath == null) {
                System.out.println("****  GSLAM :: GenericTemplatesLoader could not find path of resources! ****");
                return null;
            }

            Hashtable<String, TemplatesGroup> result = new Hashtable<String, TemplatesGroup>();

            LineIterator lines =
                    FileUtils.lineIterator(new File(templatesPath + separator + "slams" + separator + configfilename));
            while (lines.hasNext()) {
                String line = lines.nextLine();

                if (line == null || line.equals("") || line.startsWith(COMMENT))
                    continue;
                if (line.contains(slaManagerID + COMMAND)) {
                    if (line.contains(CMD_NONE))
                        return null;

                    continue;
                }
                if (!line.contains(slaManagerID))
                    continue; // discard all lines without slaManagerID

                TemplatesGroup tg = null;
                String[] split = StringUtils.split(line, "=");

                if (split.length != 2)
                    continue;

                String header = split[0];
                String content = split[1];

                String[] pattern = StringUtils.split(header, ".");
                String ID = pattern[0];

                tg = result.get(ID);
                if (tg == null) {
                    tg = new TemplatesGroup();
                    tg.name = ID;
                    result.put(ID, tg);
                }

                if (content.contains(ORC_HOME)) {
                    content = content.replace(ORC_HOME, orcPath);
                }

                tg.templates.add(content);
            }

            return result.values().toArray(new TemplatesGroup[] {});
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Resets the 'loader.command' to 'none'
     * 
     * @param filename
     */
    public void reset(String filename, String slaManagerID) {
        try {
            String separator = System.getProperty("file.separator");

            String templatesPath = System.getenv("SLASOI_HOME");
            if (templatesPath == null) {
                System.out.println("****  GSLAM :: GenericTemplatesLoader could not find path of resources! ****");
                return;
            }

            String fullFN = templatesPath + separator + "slams" + separator + filename;

            Vector<String> newContent = new Vector<String>();
            LineIterator lines = FileUtils.lineIterator(new File(fullFN));
            while (lines.hasNext()) {
                String line = lines.nextLine();

                if (line.contains(slaManagerID + COMMAND)) {
                    line = slaManagerID + COMMAND + CMD_NONE;
                }

                newContent.add(line);
            }

            FileUtils.writeLines(new File(fullFN), newContent);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper Inner Class
     */
    class TemplatesGroup {
        public TemplatesGroup() {
            templates = new Vector<String>(3, 2);
        }

        public String toString() {
            return "{" + name + ", " + templates + "}";
        }

        public String name;
        public Vector<String> templates;
    }

    public static void main(String[] args) {
        String fn = "templates.conf";
        SLAManagerContextAdapter adapter = new SLAManagerContextAdapter();
        GenericTemplatesLoader loader = new GenericTemplatesLoader(adapter);

        String SLAM = "SW-SLAMANAGER";
        TemplatesGroup[] TG = loader.getListOfTemplates(fn, SLAM);

        if (TG == null) {
            System.out.println("No templates active!");
            return;
        }

        for (TemplatesGroup gr : TG) {
            System.out.println("--------------------------");
            System.out.println(gr.name);
            System.out.println("\t" + gr.templates);
        }

        loader.reset(fn, SLAM);
    }

    protected SLAManagerContext context;

    private static final String COMMENT = "#";

    private static final String COMMAND = ".command=";
    private static final String CMD_NONE = "none";

    private static final String ORC_HOME = "{SLASOI_ORC_HOME}";

    private static final Logger LOGGER = Logger.getLogger(GenericTemplatesLoader.class);
}
