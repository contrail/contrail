-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.24-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema gslaregistry
--

set FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ gslaregistry;
USE gslaregistry;

--
-- Table structure for table `gslaregistry`.`sla`
--

DROP TABLE IF EXISTS `sla`;
CREATE TABLE `sla` (
  `sla_id` int(10) unsigned NOT NULL auto_increment,
  `sla_content` longtext NOT NULL,
  `slam_id` varchar(256) NOT NULL default '',
  `sla_id_idx` varchar(512) default NULL,
  PRIMARY KEY  (`sla_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gslaregistry`.`sla`
--

--
-- Table structure for table `gslaregistry`.`sla_basic_info`
--

DROP TABLE IF EXISTS `sla_basic_info`;
CREATE TABLE `sla_basic_info` (
  `agreed_at` datetime NOT NULL default '0000-00-00 00:00:00',
  `effective_from` datetime NOT NULL default '0000-00-00 00:00:00',
  `effective_until` datetime NOT NULL default '0000-00-00 00:00:00',
  `template_id` varchar(256) NOT NULL default '',
  `parties` longtext NOT NULL,
  `sla_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`sla_id`),
  CONSTRAINT `FK_sla_basic_info_fksla` FOREIGN KEY (`sla_id`) REFERENCES `sla` (`sla_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gslaregistry`.`sla_basic_info`
--

--
-- Table structure for table `gslaregistry`.`sla_dependency`
--

DROP TABLE IF EXISTS `sla_dependency`;
CREATE TABLE `sla_dependency` (
  `sla_id` int(10) unsigned NOT NULL default '0',
  `dependencies` longtext NOT NULL,
  PRIMARY KEY  (`sla_id`),
  CONSTRAINT `FK_sla_dependency_fksla` FOREIGN KEY (`sla_id`) REFERENCES `sla` (`sla_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gslaregistry`.`sla_dependency`
--

--
-- Table structure for table `gslaregistry`.`sla_status`
--

DROP TABLE IF EXISTS `sla_status`;
CREATE TABLE `sla_status` (
  `sla_id` int(10) unsigned NOT NULL default '0',
  `status_id` int(10) unsigned NOT NULL default '0',
  `status_timestamp` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`sla_id`,`status_id`,`status_timestamp`),
  KEY `FK_status_id` (`status_id`),
  CONSTRAINT `FK_sla_status_fksla` FOREIGN KEY (`sla_id`) REFERENCES `sla` (`sla_id`),
  CONSTRAINT `FK_status_fkref` FOREIGN KEY (`status_id`) REFERENCES `sla_status_ref` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 3072 kB; (`status_id`) REFER `slasoi-slaregistr';

--
-- Dumping data for table `gslaregistry`.`sla_status`
--

--
-- Table structure for table `gslaregistry`.`sla_status_ref`
--

DROP TABLE IF EXISTS `sla_status_ref`;
CREATE TABLE `sla_status_ref` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(256) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gslaregistry`.`sla_status_ref`
--
/*!40000 ALTER TABLE `sla_status_ref` DISABLE KEYS */;
INSERT INTO `sla_status_ref` (`id`,`name`) VALUES 
 (1,'OBSERVED'),
 (2,'VIOLATED'),
 (3,'WARN'),
 (4,'EXPIRED');
/*!40000 ALTER TABLE `sla_status_ref` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

set FOREIGN_KEY_CHECKS = 1;