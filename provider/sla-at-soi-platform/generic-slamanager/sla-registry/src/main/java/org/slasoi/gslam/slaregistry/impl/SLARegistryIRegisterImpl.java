/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1579 $
 * @lastrevision   $Date: 2011-05-04 16:22:40 +0200 (sre, 04 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/SLARegistryIRegisterImpl.java $
 */

package org.slasoi.gslam.slaregistry.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.gslam.core.poc.PlanningOptimization.INotification;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaBasicInfo;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaDependency;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;
import org.slasoi.gslam.slaregistry.impl.db.utils.Base64Utils;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.gslam.slaregistry.impl.db.utils.SLARegistryUtilities;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * Helper class of SLARegistry which implements the IRegister services.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLARegistryIRegisterImpl implements SLARegistry.IRegister {
    /**
     * Stores the specified SLA, its dependencies and sets its current 'status'.<br>
     * <br>
     * 
     * Internally a validation process is executed for checking the agreement content and the existence of its
     * dependencies. After the agreement is stored, this class contact to the domain-specific POC and notifies about the
     * new agreement.
     * 
     * @return the agreement Id only if the agreement was stored and the POC notified. Otherwise an exception will be
     *         thrown
     */
    public UUID register(SLA agreement, UUID[] dependencies, SLAState status) throws RegistrationFailureException {
        assert (agreement != null && status != null) : "it requires an agreement != null and a status != null";

        try {
            validate(agreement, dependencies);
            if (existAgreement(agreement)) {
                throw new ValidationException("agreement already exists.");
            }
        }
        catch (ValidationException exc) {
            throw new RegistrationFailureException(exc.getMessage());
        }

        Session s = HibernateUtils.getSession();
        Transaction tx = s.beginTransaction();

        // SLA ------------------------------------------------------------------------
        Sla sla = new Sla();

        String content = Base64Utils.encode(agreement);

        if (content == null)
            throw new RegistrationFailureException("SLA Object could not be serialized :: " + agreement);

        sla.setSlaContent(content);
        sla.setSlaIdIdx(agreement.getUuid().getValue());

        try {
            sla.setSlamId(context.getSLAManagerID());
        }
        catch (Exception e) {
            throw new RegistrationFailureException("SLAManagerID has not been initialized");
        }

        // Dependencies ---------------------------------------------------------------

        String dependenciesAsString = Base64Utils.encode(dependencies);

        if (dependenciesAsString == null)
            throw new RegistrationFailureException("Dependencies could not be serialized :: " + dependencies);
        SlaDependency sd = new SlaDependency();
        sd.setDependencies(dependenciesAsString);
        sd.setSla(sla);

        s.persist(sla);

        // States ----------------------------------------------------------------------

        SlaStatusRef statusDB = SLARegistryUtilities.getStatus(status.name());
        if (statusDB == null)
            throw new RegistrationFailureException("Status has not been found.  Check the SLARegistry database.  '"
                    + status.name() + "'");

        Set<SlaStatus> slaStatuses = sla.getSlaStatuses();
        if (slaStatuses == null) {
            slaStatuses = new HashSet<SlaStatus>(0);
            sla.setSlaStatuses(slaStatuses);
        }

        Date time = new Date(System.currentTimeMillis());
        SlaStatusId statusID = new SlaStatusId(sla.getSlaId(), statusDB.getId(), time);

        SlaStatus slaStatus = new SlaStatus(statusID, sla, statusDB);
        slaStatuses.add(slaStatus);

        // Basic Info ----------------------------------------------------------------------

        SlaBasicInfo slaBasicInfo = sla.getSlaBasicInfo();
        if (slaBasicInfo == null) {
            slaBasicInfo = new SlaBasicInfo();
            sla.setSlaBasicInfo(slaBasicInfo);
            slaBasicInfo.setSla(sla);
        }

        TIME agreedAt = agreement.getAgreedAt();
        TIME effectiveFrom = agreement.getEffectiveFrom();
        TIME effectiveUntil = agreement.getEffectiveUntil();
        UUID templateId = agreement.getTemplateId();
        Party[] parties = agreement.getParties();

        String partiesAsString = null;
        if (parties != null)
            partiesAsString = Base64Utils.encode(parties);

        slaBasicInfo.setAgreedAt(agreedAt.getValue().getTime());
        slaBasicInfo.setEffectiveFrom(effectiveFrom.getValue().getTime());
        slaBasicInfo.setEffectiveUntil(effectiveUntil.getValue().getTime());
        slaBasicInfo.setTemplateId(templateId.getValue());

        if (partiesAsString == null)
            throw new RegistrationFailureException("SLA Parties could not be resolved. \n" + agreement);

        slaBasicInfo.setParties(partiesAsString);

        // DB Persistence ---------------------------------------------------------------

        s.persist(sla);
        s.persist(sd);
        s.persist(slaBasicInfo);
        s.persist(slaStatus);

        tx.commit();
        s.close();
        
        if ( status == SLAState.UNSIGNED ) // ==> then a manually provisioning should be done later.
        {
            return agreement.getUuid();
        }

        // POC notification
        try {
            PlanningOptimization planningOptimization = context.getPlanningOptimization();
            String SLAM = "";
            if (context != null) {
                try {
                    SLAM = context.getSLAManagerID();
                }
                catch (Exception e) {
                }
            }

            assert (planningOptimization != null) : SLAM + "-POC could not be contacted.";
            INotification iNotification = planningOptimization.getINotification();

            assert (iNotification != null) : SLAM + "-POC has not a 'Notification' agent active.";
            iNotification.activate(agreement);
        }
        catch (Exception e1) {
            e1.printStackTrace();
            throw new RegistrationFailureException("Domain Specific POC could not be contacted.");
        }
        
        notifyViaEmail( agreement );

        return agreement.getUuid();
    }

    /**
     * Does the provisioning of the specified SLA (PRECONDITION: 'unsigned') and changes its status to 'OBSERVED'.<br>
     */
    public UUID sign(UUID slaID) throws UpdateFailureException {
        assert (slaID != null) : "it requires an id != null";

        try {
            // POC notification
            try {
                SLA unsignedSLA = SLARegistryUtilities.getUnsignedSLA( slaID );
                if ( unsignedSLA == null )
                {
                    throw new UpdateFailureException("The specified unsigned SLA does not exist.");
                }
                
                // Update status to 'OBSERVED'
                if ( !SLARegistryUtilities.signSLA( slaID ) )
                {
                    throw new UpdateFailureException("The specified unsigned SLA could not be signed.");
                }
                
                PlanningOptimization planningOptimization = context.getPlanningOptimization();
                String SLAM = "";
                if (context != null) {
                    try {
                        SLAM = context.getSLAManagerID();
                    }
                    catch (Exception e) {
                    }
                }

                assert (planningOptimization != null) : SLAM + "-POC could not be contacted.";
                INotification iNotification = planningOptimization.getINotification();

                assert (iNotification != null) : SLAM + "-POC has not a 'Notification' agent active.";
                iNotification.activate(unsignedSLA);
            }
            catch (Exception e1) {
                e1.printStackTrace();
                if ( e1 instanceof UpdateFailureException )
                {
                    throw new UpdateFailureException( e1.getMessage() );
                }
                
                throw new RegistrationFailureException("Domain Specific POC could not be contacted.");
            }
        }
        catch (Exception exc) {
            throw new UpdateFailureException(exc.getMessage());
        }
        
        return slaID;
    }
    
    /**
     * Updates the specified agreement associated to 'id', its dependencies and sets its new status
     */
    public UUID update(UUID id, SLA agreement, UUID[] dependencies, SLAState status) throws UpdateFailureException {
        assert (id != null && agreement != null && status != null) : "it requires an id != null and an agreement != null and a status != null";

        try {
            validate(agreement, dependencies);
        }
        catch (ValidationException exc) {
            throw new UpdateFailureException(exc.getMessage());
        }

        Session s = HibernateUtils.getSession();
        Transaction tx = s.beginTransaction();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + id.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new UpdateFailureException("SLA could not be found :: " + id.getValue());

        Sla sla = (Sla) list.get(0);

        String content = Base64Utils.encode(agreement);

        if (content == null)
            throw new UpdateFailureException("SLA Object could not be serialized :: " + agreement);

        sla.setSlaContent(content);
        sla.setSlaIdIdx(agreement.getUuid().getValue());

        try {
            sla.setSlamId(context.getSLAManagerID());
        }
        catch (Exception e) {
            throw new UpdateFailureException("SLAManagerID has not been initialized");
        }

        // Dependencies ---------------------------------------------------------------

        String dependenciesAsString = Base64Utils.encode(dependencies);

        if (dependenciesAsString == null)
            throw new UpdateFailureException("Dependencies could not be serialized :: " + dependencies);
        SlaDependency sd = sla.getSlaDependency();
        if (sd == null) {
            sd = new SlaDependency();
        }

        sd.setDependencies(dependenciesAsString);
        sd.setSla(sla);
        sla.setSlaDependency(sd);

        s.persist(sla);

        // States ----------------------------------------------------------------------

        SlaStatusRef statusDB = SLARegistryUtilities.getStatus(status.name());
        if (statusDB == null)
            throw new UpdateFailureException("Status has not been found.  Check the SLARegistry database.  '"
                    + status.name() + "'");

        Set<SlaStatus> slaStatuses = sla.getSlaStatuses();
        if (slaStatuses == null) {
            slaStatuses = new HashSet<SlaStatus>(0);
            sla.setSlaStatuses(slaStatuses);
        }

        Date time = new Date(System.currentTimeMillis());
        SlaStatusId statusID = new SlaStatusId(sla.getSlaId(), statusDB.getId(), time);

        SlaStatus slaStatus = new SlaStatus(statusID, sla, statusDB);
        slaStatuses.add(slaStatus);

        // Basic Info ----------------------------------------------------------------------

        SlaBasicInfo slaBasicInfo = sla.getSlaBasicInfo();
        if (slaBasicInfo == null) {
            slaBasicInfo = new SlaBasicInfo();
            sla.setSlaBasicInfo(slaBasicInfo);
            slaBasicInfo.setSla(sla);
        }

        TIME agreedAt = agreement.getAgreedAt();
        TIME effectiveFrom = agreement.getEffectiveFrom();
        TIME effectiveUntil = agreement.getEffectiveUntil();
        UUID templateId = agreement.getTemplateId();
        Party[] parties = agreement.getParties();

        String partiesAsString = null;
        if (parties != null)
            partiesAsString = Base64Utils.encode(parties);

        slaBasicInfo.setAgreedAt(agreedAt.getValue().getTime());
        slaBasicInfo.setEffectiveFrom(effectiveFrom.getValue().getTime());
        slaBasicInfo.setEffectiveUntil(effectiveUntil.getValue().getTime());
        slaBasicInfo.setTemplateId(templateId.getValue());

        if (partiesAsString == null)
            throw new UpdateFailureException("SLA Parties could not be resolved. \n" + agreement);

        slaBasicInfo.setParties(partiesAsString);

        // DB Persistence ---------------------------------------------------------------

        s.persist(sla);
        s.persist(sd);
        s.persist(slaStatus);

        tx.commit();
        s.close();

        return agreement.getUuid();
    }

    /**
     * Helper method for validation of the agreement content and its dependencies.
     * 
     * The validation process checks following fields (as mandatory):<br>
     * <blockquote> <i> templateId<br>
     * uuid<br>
     * effectiveFrom<br>
     * effectiveUntil<br>
     * </i> </blockquote>
     * 
     * @param agreement
     * @param dependencies
     * @throws ValidationException
     */
    public void validate(SLA agreement, UUID[] dependencies) throws ValidationException {
        try {
            UUID templateId = agreement.getTemplateId();
            UUID uuid = agreement.getUuid();
            TIME effectiveFrom = agreement.getEffectiveFrom();
            TIME effectiveUntil = agreement.getEffectiveUntil();

            if (templateId == null || templateId.getValue() == null)
                throw new ValidationException("Template ID of Agreement cannot be null nor empty");

            if (uuid == null || uuid.getValue() == null)
                throw new ValidationException("Agreement ID cannot be null nor empty");

            if (effectiveFrom == null || effectiveFrom.getValue() == null)
                throw new ValidationException("'EffectiveFrom' field of agreement cannot be null nor empty");

            if (effectiveUntil == null || effectiveUntil.getValue() == null)
                throw new ValidationException("'EffectiveUntil' field of agreement cannot be null nor empty");

            /*
            // checking of dependencies  :: TOBE FIXED in Y3.
            // dependencies could be located on each SLAM (in some cases remote).  For querying those SLAs each
            // WS of respective SLAM has to be used.
             * 
            if (dependencies == null || dependencies.length == 0)
                return;

            Session s = HibernateUtils.getSession();

            Query q = s.createQuery("from Sla");
            List<?> list = q.list();
            if (list.isEmpty())
                throw new ValidationException("agreement dependencies not found in the sla-registry");

            Vector<String> existingSLAs = new Vector<String>(3, 2);
            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                Sla curr = (Sla) it.next();
                String slaId = curr.getSlaIdIdx();
                existingSLAs.add(slaId);
            }
            s.close();

            for (UUID u : dependencies) {
                if (!existingSLAs.contains(u.getValue())) {
                    throw new ValidationException("agreement dependency has not been found in the sla-registry : "
                            + u.getValue());
                }
            }
            */
        }
        catch (Exception e) {
            if (e instanceof ValidationException)
                throw (ValidationException) e;

            throw new ValidationException("agreement cannot be null nor empty");
        }
    }

    /**
     * Checks if an agreement exist
     * 
     * @param agreement
     * @return true if the agreement already exist
     * @throws ValidationException
     */
    public boolean existAgreement(SLA agreement) throws ValidationException {
        try {
            
            assert ( context != null ) :  "SLARegistry does not belong to any SLAM";
            
            String slamID = context.getSLAManagerID();
            
            Session s = HibernateUtils.getSession();

            // SLA ------------------------------------------------------------------------
            UUID uuid = agreement.getUuid();
            Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + uuid.getValue() + "' and s.slamId = '" + slamID + "'" );
            List<?> list = q.list();
            if (!list.isEmpty())
                return true;
        }
        catch (Exception e) {
            throw new ValidationException("error checking agreement");
        }

        return false;
    }

    /**
     * @return its SLAM context
     */
    public SLAManagerContext getContext() {
        return context;
    }
    
    protected void notifyViaEmail(SLA sla)
    {
        try
        {
            Hashtable<String,String> props = context.getProperties();
            String active = props.get( "slam.slaregistry.email.active" );
            String sender = props.get( "slam.slaregistry.email.sender" );
            String target = props.get( "slam.slaregistry.email.target" );
            
            if ( active != null && active.equals( "true" ) )
            {
                String username = null;
                String password = null;
                String emailsrv = null;
                String[] tokens = null;
                
                // Pattern =  username:pwd@smtp.server.address
                tokens = sender.split( "\\:" );
                username = tokens[0];
                tokens = tokens[1].split( "\\@" );
                password = tokens[0];
                emailsrv = tokens[1];
                
                //Set the host smtp address
                Properties properties = new Properties();
                properties.put( "mail.smtp.host", emailsrv );
                properties.put( "mail.smtp.auth", "false"  );

               Authenticator auth = new SMTPAuthenticator( username, password );
               javax.mail.Session session = javax.mail.Session.getDefaultInstance( properties, auth );
               session.setDebug( true );

               // create a message
               Message msg = new MimeMessage( session );

               // set the from and to address
               InternetAddress addressFrom = new InternetAddress(username+"@"+emailsrv);
               msg.setFrom( addressFrom );

               InternetAddress[] addressTo = new InternetAddress[ 1 ];
               addressTo[ 0 ] = new InternetAddress( target );
               msg.setRecipients( Message.RecipientType.TO, addressTo );

               // Setting the Subject and Content Type
               msg.setSubject( String.format( "SLA changed in '%s'", context.getSLAManagerID() ) );
               msg.setContent( "--CONTENT--\n"+sla.toString(), "text/plain" );
               Transport.send(msg);
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    protected class SMTPAuthenticator extends javax.mail.Authenticator
    {
        public SMTPAuthenticator( String usr, String pwd )
        {
            this.usr = usr;
            this.pwd = pwd;
        }
        
        public PasswordAuthentication getPasswordAuthentication( String usr, String pwd )
        {
            return new PasswordAuthentication(this.usr, this.pwd);
        }
        
        protected String usr;
        protected String pwd;
    }
    
    /**
     * Injection method for the SLAM-context
     * 
     * @param context
     */
    public void setContext(SLAManagerContext context) {
        this.context = context;
    }

    private SLAManagerContext context;

    @SuppressWarnings("serial")
    class ValidationException extends Exception {
        public ValidationException(String reason) {
            super(reason);
        }
    }
}
