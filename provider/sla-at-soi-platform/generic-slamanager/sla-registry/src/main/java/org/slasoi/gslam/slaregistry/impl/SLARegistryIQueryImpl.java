/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/SLARegistryIQueryImpl.java $
 */

package org.slasoi.gslam.slaregistry.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidStateException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaBasicInfo;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaDependency;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;
import org.slasoi.gslam.slaregistry.impl.db.utils.Base64Utils;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * Helper class of SLARegistry which implements the IQuery services.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLARegistryIQueryImpl implements SLARegistry.IQuery {
    /**
     * Query method which returns an array of dependencies of an existing SLA with specified 'id'
     */
    public UUID[] getDependencies(UUID id) throws InvalidUUIDException {
        assert (id != null && id.getValue() != null && !id.getValue().equals("")) : "it requires an Id != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + id.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("SLA has not been found :: " + id.getValue());

        Sla sla = (Sla) list.get(0);
        SlaDependency sd = sla.getSlaDependency();
        if (sd == null)
            return null;

        String dependencies = sd.getDependencies();
        UUID[] result = Base64Utils.decodeUUIDs(dependencies);

        s.close();

        return result;
    }

    /**
     * Query method which returns an array of existing SLAs with specified 'ids'
     */
    public SLA[] getSLA(UUID[] ids) throws InvalidUUIDException {
        assert (ids != null && ids.length != 0) : "it requires ids != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("from Sla");
        List<?> list = q.list();
        if (list.isEmpty())
            return null;

        Vector<SLA> selected = new Vector<SLA>(3, 2);
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            String slaId = curr.getSlaIdIdx();

            for (UUID id : ids) {
                if (id.getValue().equals(slaId)) {
                    SLA sla = Base64Utils.decodeSLA(curr.getSlaContent());
                    if (sla != null) {
                        selected.add(sla);
                    }
                }
            }
        }

        s.close();

        return selected.toArray(new SLA[] {});
    }

    /**
     * Query method which returns an array of SLAs based on the specified 'slatID' (template id)
     */
    public SLA[] getSLAsByTemplateId(UUID slatID) throws InvalidUUIDException {
        assert (slatID != null && slatID.getValue() != null && !slatID.getValue().equals("")) : "it requires a slatID != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("select s from Sla s where s.slaBasicInfo.templateId = '" + slatID.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("slatID has not been found :: " + slatID.getValue());

        Vector<SLA> selected = new Vector<SLA>(3, 2);
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            SLA sla = Base64Utils.decodeSLA(curr.getSlaContent());
            selected.add(sla);
        }

        return selected.toArray(new SLA[] {});
    }

    /**
     * Returns an array of SLAs containing the specified 'party'
     */
    public SLA[] getSLAsByParty(UUID party) throws InvalidUUIDException {
        assert (party != null && party.getValue() != null && !party.getValue().equals("")) : "it requires a party != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("from SlaBasicInfo");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("party has not been found :: " + party.getValue());

        Vector<SLA> selected = new Vector<SLA>(3, 2);
        Iterator<?> it = list.iterator();

        while (it.hasNext()) {
            SlaBasicInfo curr = (SlaBasicInfo) it.next();
            String encodedParties = curr.getParties();
            Party[] partiesArray = Base64Utils.decodeParties(encodedParties);

            boolean bParty = false;
            for (Party p : partiesArray) {
                if (p.getId().equals(party)) {
                    bParty = true;
                    break;
                }
            }

            if (bParty) {
                Sla slaDB = curr.getSla();
                SLA sla = Base64Utils.decodeSLA(slaDB.getSlaContent());
                if (sla != null) {
                    selected.add(sla);
                }
            }
        }

        s.close();

        return selected.toArray(new SLA[] {});
    }

    /**
     * Query method which returns an array of SLAStateInfo representing the history of the status of the specified
     * id (SLA id)
     */
    public SLAStateInfo[] getStateHistory(UUID id, boolean current) throws InvalidUUIDException, InvalidStateException {
        assert (id != null && id.getValue() != null && !id.getValue().equals("")) : "it requires an Id != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + id.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("SLA has not been found :: " + id.getValue());

        Sla sla = (Sla) list.get(0);

        Vector<SLAStateInfo> result = new Vector<SLAStateInfo>(3, 2);
        Set<SlaStatus> slaStatuses = sla.getSlaStatuses();

        for (SlaStatus ss : slaStatuses) {
            SlaStatusId ssID = ss.getId();
            SlaStatusRef ssRef = ss.getSlaStatusRef();

            SLAStateInfo ssi = new SLAStateInfo();
            ssi.state = SLAState.valueOf(ssRef.getName());
            ssi.date = ssID.getStatusTimestamp();

            result.add(ssi);
        }

        s.close();

        class SLAStateInfoComparator implements Comparator<SLAStateInfo> {
            public int compare(SLAStateInfo stateA, SLAStateInfo stateB) {
                return stateA.date.compareTo(stateB.date);
            }
        }

        SLAStateInfo[] array = result.toArray(new SLAStateInfo[] {});
        Arrays.sort(array, new SLAStateInfoComparator());

        if (current) // returns just the last state
        {
            return new SLAStateInfo[] { array[array.length - 1] };
        }

        return array;
    }

    /**
     * Query method which returns an array of the minimal information related to the specified SLA-ids.
     * 
     * @see org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo
     */
    public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] ids) throws InvalidUUIDException {
        assert (ids != null && ids.length != 0) : "it requires ids != null or not empty";

        Session s = HibernateUtils.getSession();

        if (ids == null || ids.length == 0)
            throw new InvalidUUIDException("IDs of SLA have not been specified");

        StringBuffer sb = new StringBuffer();
        sb.append("(");
        for (int i = 0; i < ids.length; i++) {
            sb.append("'" + ids[i].getValue() + "'");
            if (i < ids.length - 1)
                sb.append(",");
            else
                sb.append(")");
        }

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("SELECT s FROM Sla s where s.slaIdIdx in " + sb.toString());
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("no SLAs were found :: " + sb.toString());

        Vector<SLAMinimumInfo> result = new Vector<SLAMinimumInfo>(3, 2);

        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            SlaBasicInfo slaBasicInfo = curr.getSlaBasicInfo();

            SLAMinimumInfo smi = new SLAMinimumInfo();

            // SLA ID
            smi.SLAID = new UUID(curr.getSlaIdIdx());
            // -- agreedAt --
            Calendar cAA = Calendar.getInstance();
            cAA.setTime(slaBasicInfo.getAgreedAt());
            smi.agreedAt = new TIME(cAA);

            // -- effectiveFrom --
            Calendar cEF = Calendar.getInstance();
            cEF.setTime(slaBasicInfo.getEffectiveFrom());
            smi.effectiveFrom = new TIME(cEF);

            // -- effectiveUntil --
            Calendar cEU = Calendar.getInstance();
            cEU.setTime(slaBasicInfo.getEffectiveUntil());
            smi.effectiveUntil = new TIME(cEU);

            // -- templateID --
            smi.templateID = new UUID(slaBasicInfo.getTemplateId());

            // -- parties --
            smi.parties = Base64Utils.decodeParties(slaBasicInfo.getParties());

            result.add(smi);
        }

        s.close();

        return result.toArray(new SLAMinimumInfo[] {});
    }

    /**
     * Query method which returns an array of dependable SLA-ids of the queried id (SLA id)
     */
    public UUID[] getUpwardDependencies(UUID id) throws InvalidUUIDException {
        assert (id != null && id.getValue() != null && !id.getValue().equals("")) : "it requires an Id != null or not empty";

        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("from Sla");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidUUIDException("SLA has not been found :: " + id.getValue());

        Vector<UUID> result = new Vector<UUID>(3, 2);
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            SlaDependency sd = curr.getSlaDependency();

            if (sd != null) {
                String dependencies = sd.getDependencies();
                UUID[] currDep = Base64Utils.decodeUUIDs(dependencies);
                if ( currDep != null )
                {
                    for (UUID ui : currDep) {
                        if (ui.getValue().equals(id.getValue())) {
                            result.add(new UUID(curr.getSlaIdIdx()));
                        }
                    }
                }
            }
        }

        s.close();

        return result.toArray(new UUID[] {});
    }

    /**
     * Query method which returns an array of existing SLA-ids with specified SLA-states
     */
    public UUID[] getSLAsByState(SLAState[] states, boolean inclusive) throws InvalidStateException {
        assert (states != null && states.length != 0) : "it requires states != null or not empty";

        Session s = HibernateUtils.getSession();

        Query q = s.createQuery("from Sla");
        List<?> list = q.list();
        if (list.isEmpty())
            throw new InvalidStateException("SLA registry empty");

        Vector<UUID> result = new Vector<UUID>(3, 2);
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            Set<SlaStatus> statuses = curr.getSlaStatuses();
            if (inclusive) {
                for (SlaStatus st : statuses) {
                    String sname = st.getSlaStatusRef().getName();
                    boolean contained = contains(sname, states);
                    if (inclusive && contained) {
                        result.add(new UUID(curr.getSlaIdIdx()));
                        break; // next sla
                    }
                }
            }
            else {
                boolean notcontained = notcontains(statuses, states);
                if (notcontained) {
                    String idx = curr.getSlaIdIdx();
                    if (idx != null && !idx.equals("") && !result.contains(new UUID(idx)))
                        result.add(new UUID(idx));
                }
            }
        }

        return result.toArray(new UUID[] {});
    }

    /**
     * Helper method for checking if the specified state 'name' is located within the 'states' array.
     */
    protected boolean contains(String name, SLAState[] states) {
        for (SLAState st : states) {
            if (st.name().equals(name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Helper method for checking if the specified set of status 'statuses' is located within the 'states' array.
     */
    protected boolean notcontains(Set<SlaStatus> statuses, SLAState[] states) {
        for (SlaStatus st : statuses) {
            for (SLAState s : states) {
                if (s.name().equals(st.getSlaStatusRef().getName())) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Query method which returns a set of SLA-ids using the specified 'states'
     * 
     * @throws InvalidStateException
     */
    public UUID[] getSLAsByStateUsingCriterias(SLAState[] states, boolean inclusive) throws InvalidStateException {
        assert (states != null && states.length != 0) : "it requires states != null or not empty";

        Session s = HibernateUtils.getSession();

        Criteria cJoin = s.createCriteria(Sla.class).createCriteria("slaStatuses").createCriteria("slaStatusRef");

        if (inclusive) {
            for (SLAState in : states) {
                cJoin.add(Restrictions.like("name", in.name()));
            }
        }

        List<?> list = cJoin.list();

        if (list.isEmpty())
            return null;

        Vector<UUID> selected = new Vector<UUID>(3, 2);
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            UUID uuid = new UUID(curr.getSlaIdIdx());

            if (!selected.contains(uuid))
                selected.add(uuid);
        }

        return selected.toArray(new UUID[] {});
    }

    public SLAManagerContext getContext() {
        return context;
    }

    public void setContext(SLAManagerContext context) {
        this.context = context;
    }

    private SLAManagerContext context;
}
