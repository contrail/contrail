/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 2601 $
 * @lastrevision   $Date: 2011-07-11 16:32:29 +0200 (pon, 11 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/SLARegistryImpl.java $
 */

package org.slasoi.gslam.slaregistry.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.slamodel.sla.SLA;

/**
 * SLARegistry implements IRegister and IQuery interfaces and represents the SLARegistry of a SLA-Manager. This object
 * will be available via SLAMContext of its SLA-manager
 * 
 * @see org.slasoi.gslam.core.negotiation.SLARegistry
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLARegistryImpl implements SLARegistry, SLAMContextAware {
    /**
     * Default constructor
     */
    public SLARegistryImpl() {
        register = new SLARegistryIRegisterImpl();
        query    = new SLARegistryIQueryImpl   ();
        admin    = new SLARegistryIAdminImpl   ();
    }

    /**
     * Injection method. This will be invoked only from the G-SLAM by the initialization of a SLA-manager.
     * 
     * @param context
     *            SLA-manager context which belongs this SLARegistry
     */
    public void setSLAManagerContext(SLAManagerContext context) {
        this.context = context;
        register.setContext(context);
        query   .setContext(context);
        admin   .setContext(context);
    }

    /**
     * Implementation of the IRegister interface
     */
    public SLARegistry.IRegister getIRegister() {
        return register;
    }

    /**
     * Implementation of the IQuery interface
     */
    public SLARegistry.IQuery getIQuery() {
        return query;
    }

    /**
     * Implementation of the IAdminUtil interface
     */
    public SLARegistry.IAdminUtils getIAdmin() {
        return admin;
    }
    
    public String toString()
    {
        int i = 0;
        if ( i == 0 )
            return super.toString();
        
        try
        {
            HibernateUtils.loadResourcesFromEnvironment();
            Session s = HibernateUtils.getSession();

            // SLA ------------------------------------------------------------------------
            Query q = s.createQuery("from Sla");
            List<?> list = q.list();
            Vector<SLA> selected = new Vector<SLA>(3, 2);
            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                Sla curr = (Sla) it.next();
                System.out.println( curr );
            }            
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        
        return super.toString();
    }
    
    @SuppressWarnings("unused")
    private SLAManagerContext context;

    private SLARegistryIRegisterImpl register;
    private SLARegistryIQueryImpl    query   ;
    private SLARegistryIAdminImpl    admin   ;
}
