/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/db/utils/Base64Utils.java $
 */

package org.slasoi.gslam.slaregistry.impl.db.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.SerializationUtils;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * Helper class for serialization of SLA Objects into the SLARegistry.<br>
 * <br>
 * 
 * Note that the String returned by encoded methods does not reflect the toString method of the specified object.
 * Additionally each 'encode' method has an associated 'decode' method for restoring the encoded object.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class Base64Utils {
    /**
     * Default constructor
     */
    public Base64Utils() {
    }

    /**
     * Encodes a SLA object into a Java-String.
     * 
     * @param sla
     *            to be encoded
     * @return a java string representation of the 'sla'
     */
    public static String encode(SLA sla) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(sla);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Helper method which invokes internally the method encode
     * 
     * @see Base64Utils#encode(SLA sla )
     * @param sla
     * @return the encoded sla as String
     */
    public static String ext_encode(SLA sla) {
        String result = null;

        try {
            byte[] serialized = SerializationUtils.serialize(sla);
            result = new String(Base64.encodeBase64(serialized));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Encodes an array of SLA-ids into a Java-String.
     * 
     * @param uuids
     * @return the Java-String representation of the specified uuids.
     */
    public static String encode(UUID[] uuids) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(uuids);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Encodes an array of Parties into a Java-String.
     * 
     * @param parties
     *            array to be encoded
     * @return the Java-String representation of the specified 'parties'.
     */
    public static String encode(Party[] parties) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(parties);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Decodes a Java-String which contains an array of Parties into a Java-String.
     * 
     * @param content
     *            Java-String array to be decoded
     * @return an Array of 'Parties'.
     */
    public static Party[] decodeParties(String content) {
        Party[] result = null;

        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64.decodeBase64(content.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            Object read = is.readObject();
            result = (Party[]) read;
            is.close();

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Decodes a Java-String which contains an array of UUIDs
     * 
     * @param content
     *            Java-String to be decoded
     * @return an Array of UUIDs
     */
    public static UUID[] decodeUUIDs(String content) {
        UUID[] result = null;

        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64.decodeBase64(content.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            Object read = is.readObject();
            result = (UUID[]) read;
            is.close();

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Decodes a Java-String which contains an encoded SLA
     * 
     * @param content
     *            Java-String to be decoded
     * @return a SLA object
     */
    public static SLA decodeSLA(String content) {
        SLA result = null;

        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64.decodeBase64(content.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            Object read = is.readObject();
            result = (SLA) read;
            is.close();

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Helper method for decoding a SLA object
     * 
     * @param content
     *            Java-String to be decoded
     * 
     * @return a new SLA object
     */
    public static SLA ext_decodeSLA(String content) {
        SLA result = null;

        try {
            Object deserialized = SerializationUtils.deserialize(Base64.decodeBase64(content.getBytes()));
            result = (SLA) deserialized;

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static class PluginObjectInputStream extends ObjectInputStream {
        public PluginObjectInputStream() throws Exception {
        }

        @SuppressWarnings({ "rawtypes", "unchecked" })
        protected Class resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
            Thread.currentThread().getContextClassLoader();

            return null;
        }
    }
}
