/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1554 $
 * @lastrevision   $Date: 2011-05-03 16:16:20 +0200 (tor, 03 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/db/utils/SLARegistryUtilities.java $
 */

package org.slasoi.gslam.slaregistry.impl.db.utils;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

/**
 * Helper class.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLARegistryUtilities {
    /**
     * Query method which return the SlaStatusRef based on the specified 'name'
     * 
     * @param name
     *            of the SlaStatusRef to be searched
     */
    @SuppressWarnings("unchecked")
    public static SlaStatusRef getStatus(String name) {
        try {
            SlaStatusRef result = null;

            Session s = HibernateUtils.getSession();

            Query q = s.createQuery("from SlaStatusRef");
            List list = q.list();

            Iterator it = list.iterator();
            while (it.hasNext()) {
                SlaStatusRef curr = (SlaStatusRef) it.next();

                if (curr.getName().equals(name)) {
                    result = curr;
                    break;
                }
            }

            s.close();

            return result;
        }
        catch (Exception e) {
        }

        return null;
    }
    
    public static SLA getUnsignedSLA( UUID id )
    {
        Sla slaRef = null;
        Session s = HibernateUtils.getSession();

        // SLA ------------------------------------------------------------------------
        Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + id.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            return null;

        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            slaRef = (Sla) it.next();
            Set<SlaStatus> slaStatuses = slaRef.getSlaStatuses();
            
            // check if len( statuses ) == 1 && status == UNSIGNED
            if ( slaStatuses.size() == 1 )
            {
                SlaStatus ss = slaStatuses.iterator().next();
                if ( ss.getSlaStatusRef().getName().equals( SLAState.UNSIGNED.name() ) )
                {
                    break;
                }
            }
            
            slaRef = null;
        }

        s.close();

        if ( slaRef != null )
        {
            return Base64Utils.decodeSLA(slaRef.getSlaContent());
        }
        
        return null;
    }
    
    public static boolean signSLA( UUID slaID )
    {
        // insert a new state related to slaID;  it has to be OBSERVED.
        SlaStatusRef statusDB = SLARegistryUtilities.getStatus( SLAState.OBSERVED.name() );

        Session s = HibernateUtils.getSession();
        Transaction tx = s.beginTransaction();

        // SLA ------------------------------------------------------------------------
        Sla slaRef = null;
        Set<SlaStatus> slaStatuses = null;
        
        Query q = s.createQuery("select s from Sla s where s.slaIdIdx = '" + slaID.getValue() + "'");
        List<?> list = q.list();
        if (list.isEmpty())
            return false;

        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            slaRef = (Sla) it.next();
            slaStatuses = slaRef.getSlaStatuses();
            if ( slaStatuses == null ) {
                slaStatuses = new HashSet<SlaStatus>(0);
                slaRef.setSlaStatuses(slaStatuses);
            }
        }

        Date time = new Date(System.currentTimeMillis());
        SlaStatusId statusID = new SlaStatusId(slaRef.getSlaId(), statusDB.getId(), time);

        SlaStatus slaStatus = new SlaStatus(statusID, slaRef, statusDB);
        slaStatuses.add(slaStatus);

        s.persist(slaStatus);
        s.persist(slaRef);
        tx.commit();
        
        return true;
    }
}
