/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 2758 $
 * @lastrevision   $Date: 2011-07-22 13:10:40 +0200 (pet, 22 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/IQueryTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Calendar;

import junit.framework.TestCase;

import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;
import org.slasoi.gslam.slaregistry.impl.SLARegistryIQueryImpl;
import org.slasoi.gslam.slaregistry.impl.SLARegistryIRegisterImpl;
import org.slasoi.gslam.slaregistry.impl.SLARegistryImpl;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * TestCase of SLARegistryIQueryImpl
 * 
 * @see org.slasoi.gslam.slaregistry.impl.SLARegistryIQueryImpl
 * @author Miguel Rojas (UDO)
 */
public class IQueryTest extends TestCase {
    public void testDependencies() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            UUID[] dependencies = object.getDependencies(new UUID(TestBed.SLAS[0]));
            for (UUID dep : dependencies) {
                dep.getValue();
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testUpwardDependencies() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            UUID[] dependencies = object.getUpwardDependencies(new UUID(TestBed.SLAS[2]));
            for (UUID dep : dependencies) {
                dep.getValue();
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testStateHistory() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            SLAStateInfo[] stateHistory = object.getStateHistory(new UUID(TestBed.SLAS[2]), !true);

            System.out.println("testStateHistory (" + stateHistory.length + ")");
            System.out.println("--------------------------------");
            for (SLAStateInfo ssi : stateHistory) {
                System.out.println(ssi);
            }

        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testMinimumSLAInfo() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            SLAMinimumInfo[] minimumSLAInfo =
                    object.getMinimumSLAInfo(new UUID[] { new UUID(TestBed.SLAS[0]), new UUID(TestBed.SLAS[2]),
                            new UUID(TestBed.SLAS[4]) });

            System.out.println("testMinimumSLAInfo (" + minimumSLAInfo.length + ")");
            System.out.println("--------------------------------");
            for (SLAMinimumInfo smi : minimumSLAInfo) {
                System.out.println(smi);
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testSLAsByState() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            UUID[] slAsByState = object.getSLAsByState(new SLAState[] { SLAState.WARN, SLAState.EXPIRED }, !true);

            if (slAsByState == null) {
                System.out.println("testSLAsByState (empty)");
                return;
            }

            System.out.println("testSLAsByState (" + slAsByState.length + ")");
            System.out.println("--------------------------------");
            for (UUID smi : slAsByState) {
                System.out.println(smi.getValue());
            }

        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testSLAsByTemplateId() {
        System.out.println("Running testSLAsBySlatId");

        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            // register 2 SLAs with the same TemplateId
            registerSLA(new UUID(AGREEMENT_ID1));
            registerSLA(new UUID(AGREEMENT_ID2));

            SLA[] testSLAsByTemplateId = object.getSLAsByTemplateId(new UUID(TEMPLATE_ID));

            if (testSLAsByTemplateId == null) {
                System.out.println("testSLAsByTemplateId (empty)");
                return;
            }

            System.out.println("testSLAsByTemplateId (" + testSLAsByTemplateId.length + ")");
            System.out.println("--------------------------------");
            for (SLA sla : testSLAsByTemplateId) {
                System.out.println(sla);
            }

            assertEquals(testSLAsByTemplateId.length, 2);
            assertTrue(testSLAsByTemplateId[0].getUuid() != testSLAsByTemplateId[1].getUuid());
            for (SLA sla : testSLAsByTemplateId)
                assertTrue(sla.getUuid().equals(AGREEMENT_ID1) || sla.getUuid().equals(AGREEMENT_ID2));

        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

    }

    public void testSLAsByParty() {
        System.out.println("Running testSLAsByParty");

        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());

            // register 2 SLAs with the same TemplateId
            registerSLA(new UUID(AGREEMENT_ID1));
            registerSLA(new UUID(AGREEMENT_ID2));

            SLA[] testSLAsByParty = object.getSLAsByParty(new UUID(PARTY_ID));

            if (testSLAsByParty == null) {
                System.out.println("testSLAsByParty (empty)");
                return;
            }

            System.out.println("testSLAsByParty (" + testSLAsByParty.length + ")");
            System.out.println("--------------------------------");
            for (SLA sla : testSLAsByParty) {
                System.out.println(sla);
            }

            assertEquals(testSLAsByParty.length, 2);
            assertTrue(testSLAsByParty[0].getUuid() != testSLAsByParty[1].getUuid());
            for (SLA sla : testSLAsByParty)
                assertTrue(sla.getUuid().equals(AGREEMENT_ID1) || sla.getUuid().equals(AGREEMENT_ID2));

        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

    }

    public void testCurrent() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(IRegisterTest.getContext());
            SLA[] slas = object.getSLA(new UUID[] { new UUID("AG-1"), new UUID("AG-2"), new UUID("AG-3") });

            for (SLA s : slas) {
                System.out.println(s);
            }
        }
        catch (Exception e) {

        }
    }

    private SLA registerSLA(UUID slaId) throws RegistrationFailureException, UpdateFailureException {
        // First, check if the SLAs already exist
        SLARegistryIQueryImpl query = new SLARegistryIQueryImpl();
        query.setContext(IRegisterTest.getContext());

        UUID[] ids = new UUID[] { slaId };
        SLA[] slas = null;
        boolean exists = false;
        try {
            slas = query.getSLA(ids);

            if (slas.length > 0) {
                exists = true;
            }
        }
        catch (InvalidUUIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SLARegistryIRegisterImpl object = new SLARegistryIRegisterImpl();
        object.setContext(IRegisterTest.getContext());

        SLA agreement = new SLA();
        agreement.setUuid(slaId);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);

        agreement.setAgreedAt(new TIME(cal));
        agreement.setEffectiveFrom(new TIME(cal));
        cal.add(Calendar.DAY_OF_YEAR, 1);
        agreement.setEffectiveUntil(new TIME(cal));
        agreement.setTemplateId(new UUID(TEMPLATE_ID));

        Party[] parties = new Party[1];
        parties[0] = new Party(new ID(PARTY_ID), new STND("MR"));
        agreement.setParties(parties);

        UUID[] dependencies = new UUID[REGISTER_SLA_DEPS];
        for (int i = 0; i < dependencies.length; i++) {
            dependencies[i] = new UUID("AG-" + (i + 1));
        }

        SLAState state = SLAState.UNSIGNED;

        if (!exists) {
            object.register(agreement, dependencies, state);
        }
        else {
            object.update(slaId, agreement, dependencies, SLAState.UNSIGNED);
        }

        return agreement;
    }
    
    public static final int REGISTER_SLA_DEPS = 3;

    private static final String AGREEMENT_ID1 = "TEST-QUERY-SLA001" + java.util.UUID.randomUUID().toString();
    private static final String AGREEMENT_ID2 = "TEST-QUERY-SLA002" + java.util.UUID.randomUUID().toString();
    private static final String TEMPLATE_ID = "TEMPLATE-TEST-QUERY" + java.util.UUID.randomUUID().toString();
    private static final String PARTY_ID = "PARTY_ID-TEST-QUERY" + java.util.UUID.randomUUID().toString();
}
