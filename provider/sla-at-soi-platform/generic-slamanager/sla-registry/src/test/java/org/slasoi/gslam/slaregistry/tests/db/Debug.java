/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/Debug.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContextAdapter;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.slaregistry.impl.SLARegistryIQueryImpl;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

/**
 * Class for debugging purposes.
 * 
 * @author Miguel Rojas (UDO)
 */
public class Debug {
    public void testDependencies() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());

            UUID[] dependencies = object.getDependencies(new UUID(SLAS[0]));
            for (UUID dep : dependencies) {
                dep.getValue();
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
    }

    public void testUpwardDependencies() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());

            UUID[] dependencies = object.getUpwardDependencies(new UUID(SLAS[2]));
            for (UUID dep : dependencies) {
                dep.getValue();
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
    }

    public void testStateHistory() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());

            SLAStateInfo[] stateHistory = object.getStateHistory(new UUID(SLAS[2]), !true);

            System.out.println("testStateHistory (" + stateHistory.length + ")");
            System.out.println("--------------------------------");
            for (SLAStateInfo ssi : stateHistory) {
                System.out.println(ssi);
            }

        }
        catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
    }

    public void testMinimumSLAInfo() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());

            SLAMinimumInfo[] minimumSLAInfo =
                    object.getMinimumSLAInfo(new UUID[] { new UUID(SLAS[0]), new UUID(SLAS[2]), new UUID(SLAS[4]) });

            System.out.println("testMinimumSLAInfo (" + minimumSLAInfo.length + ")");
            System.out.println("--------------------------------");
            for (SLAMinimumInfo smi : minimumSLAInfo) {
                System.out.println(smi);
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
    }

    public void testSLAsByState() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());

            UUID[] slAsByState = object.getSLAsByState(new SLAState[] { SLAState.WARN, SLAState.EXPIRED }, !true);

            if (slAsByState == null) {
                System.out.println("testSLAsByState (empty)");
                return;
            }

            System.out.println("testSLAsByState (" + slAsByState.length + ")");
            System.out.println("--------------------------------");
            for (UUID smi : slAsByState) {
                System.out.println(smi.getValue());
            }

        }
        catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
    }

    public void testCurrent() {
        try {
            SLARegistryIQueryImpl object = new SLARegistryIQueryImpl();
            object.setContext(getContext());
            SLA[] slas = object.getSLA(new UUID[] { new UUID("AG-1"), new UUID("AG-2"), new UUID("AG-3") });

            for (SLA s : slas) {
                System.out.println(s);
            }
        }
        catch (Exception e) {

        }
    }

    public static void debug() {
        Debug dbg = new Debug();

        dbg.testMinimumSLAInfo();
        dbg.testStateHistory();
        dbg.testSLAsByState();

        dbg.testCurrent();
    }

    static String[] SLAS = new String[] { "AG-1", "AG-2", "AG-3", "AG-4", "AG-5" };

    static int I = 1;

    public static SLAManagerContext getContext() {
        SLAManagerContext context = new SLAManagerContextAdapter() {
            public String getSLAManagerID() throws SLAManagerContextException {
                return "IRegisterTest-SLAM";
            }
        };

        return context;
    }

    public static void main(String[] args) {
        debug();
    }
}
