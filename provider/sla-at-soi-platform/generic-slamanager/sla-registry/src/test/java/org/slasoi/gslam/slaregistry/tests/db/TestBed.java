/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/TestBed.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.slaregistry.impl.SLARegistryIRegisterImpl;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.gslam.slaregistry.impl.db.utils.SLARegistryUtilities;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * Helper class for initialization of TestBed for all TestCases
 * 
 * @author Miguel Rojas (UDO)
 */
public class TestBed {
    public static void create() {
        try {
            SLARegistryIRegisterImpl object = new SLARegistryIRegisterImpl();
            object.setContext(IRegisterTest.getContext());

            for (String sla : SLAS) {
                SLA agree = createSLA(sla);
                UUID[] dependencies = createDependencies(sla, SLAS);
                SLAState state = SLAState.OBSERVED;

                object.register(agree, dependencies, state);

                try {
                    Thread.sleep(500);
                }
                catch (Exception e) {
                }
            }
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void updateStatus() {
        try {
            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            Query q = s.createQuery("from Sla");

            List<?> list = q.list();
            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                Sla sla = (Sla) it.next();
                Set<SlaStatus> slaStatuses = sla.getSlaStatuses();

                long serial = System.currentTimeMillis() + (2000 * I++);
                SLAState[] sts = SLAState.values();
                SLAState selected = sts[Math.abs((int) serial % sts.length)];
                SlaStatusRef statusDB = SLARegistryUtilities.getStatus(selected.name());

                Date time = new Date(serial);
                SlaStatusId statusID = new SlaStatusId(sla.getSlaId(), statusDB.getId(), time);

                SlaStatus slaStatus = new SlaStatus(statusID, sla, statusDB);
                slaStatuses.add(slaStatus);

                sla.setSlaStatuses(slaStatuses);
                s.persist(slaStatus);
                s.persist(sla);
            }

            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SLA createSLA(String agreementID) {
        SLA result = new SLA();

        result.setUuid(new UUID(agreementID));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, (int) Math.round(Math.random() * 10000));

        result.setAgreedAt(new TIME(cal));
        result.setEffectiveFrom(new TIME(cal));
        cal.add(Calendar.DAY_OF_YEAR, (int) Math.round(Math.random() * 10000));
        result.setEffectiveUntil(new TIME(cal));
        result.setTemplateId(new UUID("template::" + System.currentTimeMillis() + Math.round(Math.random() * 10000)));

        Party[] parties = new Party[1];
        parties[0] = new Party(new ID("UDO"), new STND("MR"));
        result.setParties(parties);

        return result;
    }

    protected static UUID[] createDependencies(String sla, String[] dependencies) {
        Vector<UUID> result = new Vector<UUID>(3, 2);

        for (int i = 0; i < dependencies.length; i++) {
            if (dependencies[i].equals(sla))
                continue;

            result.add(new UUID(dependencies[i]));
        }

        if (result.size() > 2) {
            result.remove(0);
        }

        return result.toArray(new UUID[] {});
    }

    public static void main(String[] args) {
        // create();
        // updateStatus();
    }

    static String[] SLAS = new String[] { "AG-1", "AG-2", "AG-3", "AG-4", "AG-5" };

    static int I = 1;
}
