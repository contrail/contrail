/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 2758 $
 * @lastrevision   $Date: 2011-07-22 13:10:40 +0200 (pet, 22 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/SLARegistryImplTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import junit.framework.TestCase;

import org.slasoi.gslam.core.negotiation.SLARegistry.IQuery;
import org.slasoi.gslam.core.negotiation.SLARegistry.IRegister;
import org.slasoi.gslam.slaregistry.impl.SLARegistryImpl;
import org.slasoi.slamodel.primitives.UUID;

public class SLARegistryImplTest extends TestCase {
    public void testIQuery()
    {
        try {
            SLARegistryImpl registry = new SLARegistryImpl();
            
            IQuery iQuery = registry.getIQuery();
            
            if ( iQuery != null )
            {
            }
        }
        catch (Exception e) {
            assertTrue(false);
        }
        
        assertTrue(true);
    }

    public void testIRegister()
    {
        try {
            SLARegistryImpl registry = new SLARegistryImpl();
            
            IRegister iRegister = registry.getIRegister();
            
            if ( iRegister != null )
            {
                
            }
        }
        catch (Exception e) {
            assertTrue(false);
        }
        
        assertTrue(true);
    }
    
    public void  testExceptions()
    {
        try {
            SLARegistryImpl registry = new SLARegistryImpl();
            
            IQuery iQuery = registry.getIQuery();
            IRegister iRegister = registry.getIRegister();
            
            registry.setSLAManagerContext( null );
            
            if ( iRegister != null )
            {
                try {
                    iRegister.register( null, null, null );
                }
                catch (Throwable e) {
                } 
                
                try {
                    iRegister.update( null, null, null, null );
                }
                catch (Throwable e) {
                } 
            }
            
            if ( iQuery != null )
            {
                try {
                    iQuery.getDependencies(null);
                }
                catch (Throwable e) {
                } 
                try {
                    iQuery.getMinimumSLAInfo(null);
                }
                catch (Throwable e) {
                } 
                try {
                    iQuery.getSLA(null);
                }
                catch (Throwable e) {
                } 
                try {
                    iQuery.getSLAsByParty(null);
                }
                catch (Throwable e) {
                } 
                try {
                    iQuery.getSLAsByState(null,false);
                }
                catch (Throwable e) {
                } 
                try {
                    iQuery.getSLAsByTemplateId(null);
                }
                catch (Throwable e) {
                } 
            }
        }
        catch (Throwable e) {
            assertTrue(false);
        }
        
        assertTrue(true);
    }
    
    public void  testExtra()
    {
        try {
            SLARegistryImpl registry = new SLARegistryImpl();
            
            IQuery iQuery = registry.getIQuery();
            iQuery.getSLAsByTemplateId( new UUID( "254742" ) );
        }
        catch (Exception e) {
        }
        try {
            SLARegistryImpl registry = new SLARegistryImpl();
            
            IQuery iQuery = registry.getIQuery();
            iQuery.getSLAsByParty( new UUID( "254742" ) );
        }
        catch (Exception e) {
        }
        assertTrue(true);
    }
}
