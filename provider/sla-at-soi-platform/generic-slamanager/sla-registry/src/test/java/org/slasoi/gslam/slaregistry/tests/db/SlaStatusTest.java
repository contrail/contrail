/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/SlaStatusTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import junit.framework.TestCase;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;

/**
 * TestCase of HibernateUtils and SlaStatus
 * 
 * @see org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils
 * @author Miguel Rojas (UDO)
 */
public class SlaStatusTest extends TestCase {
    public void testCreateStatusRef() {
        try {
            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            for (String str : StatusSet.names) {
                SlaStatusRef state = new SlaStatusRef();
                state.setName(str);
                s.save(state);
            }

            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    @SuppressWarnings("unchecked")
    public void testCreateReferences() {
        try {
            SlaTest st = new SlaTest();
            st.testSaveSLA();

            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            Query q = s.createQuery("from SlaStatusRef");
            List list = q.list();

            SlaStatusRef curr01 = (SlaStatusRef) list.get(0);
            SlaStatusRef curr02 = (SlaStatusRef) list.get(1);

            Query qs = s.createQuery("select s from Sla s where s.slamId = '" + SLAPattern.slam + "'");
            List ls = qs.list();
            Sla sla = (Sla) ls.get(0);

            Set<SlaStatus> slaStatusRefs = sla.getSlaStatuses();
            if (slaStatusRefs.isEmpty()) {
                SlaStatus status01 = new SlaStatus();
                SlaStatus status02 = new SlaStatus();

                status01.setSla(sla);
                status02.setSla(sla);

                status01.setSlaStatusRef(curr01);
                status02.setSlaStatusRef(curr02);

                SlaStatusId id01 = new SlaStatusId();
                SlaStatusId id02 = new SlaStatusId();

                id01.setSlaId(sla.getSlaId());
                id02.setSlaId(sla.getSlaId());
                id01.setStatusId(curr01.getId());
                id02.setStatusId(curr02.getId());
                id01.setStatusTimestamp(new Date(System.currentTimeMillis()));
                id02.setStatusTimestamp(new Date(System.currentTimeMillis()));

                status01.setId(id01);
                status02.setId(id02);

                slaStatusRefs.add(status01);
                slaStatusRefs.add(status02);
            }

            s.update(sla);

            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    @SuppressWarnings("unchecked")
    public void xtestClean() throws Exception {
        try {
            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            Query qs = s.createQuery("select s from Sla s where s.slamId = '" + SLAPattern.slam + "'");
            List ls = qs.list();
            Sla sla = (Sla) ls.get(0);
            Set<SlaStatus> slaStatusRefs = sla.getSlaStatuses();
            slaStatusRefs.clear();

            s.update(sla);

            Query q = s.createQuery("from SlaStatusRef");

            Vector<Object> selected = new Vector<Object>(3, 2);
            List list = q.list();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                SlaStatusRef curr = (SlaStatusRef) it.next();

                if (StatusSet.names.contains(curr.getName())) {
                    selected.add(curr);
                }
            }

            if (!selected.isEmpty()) {
                for (Object obj : selected) {
                    s.delete(obj);
                }
            }

            s.delete(sla);

            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public static class SLAPattern {
        public static String content = "SLA :: created from hibernate-java " + System.currentTimeMillis();
        public static String sla = "SLA_ID_254742";
        public static String slam = "SLAM_3524";
    }

    public static class StatusSet {
        public static Vector<String> names;

        static {
            names = new Vector<String>(3, 2);
            names.add("STATUS_1a");
            names.add("STATUS_2b");
            names.add("STATUS_3c");
        }
    }
}
