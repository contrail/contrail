/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/SlaTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import junit.framework.TestCase;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaBasicInfo;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;

/**
 * TestCase of HibernateUtils and Sla with SlaBasicInfo
 * 
 * @see org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils
 * @author Miguel Rojas (UDO)
 */
public class SlaTest extends TestCase {
    public void testSaveSLA() {
        try {
            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            // CREATE
            Sla sla = new Sla();

            sla.setSlaContent(SLAPattern.content);
            sla.setSlamId(SLAPattern.slam);

            s.save(sla);
            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    @SuppressWarnings("unchecked")
    public void testSLABasicInfo() throws Exception {
        try {
            Session s = HibernateUtils.getSession();
            Transaction tx = s.beginTransaction();

            Query q = s.createQuery("select s from Sla s where s.slamId = '" + SLAPattern.slam + "'");
            List list = q.list();
            Sla sla = (Sla) list.get(0);

            SlaBasicInfo basic = sla.getSlaBasicInfo();
            if (basic == null) {
                basic = new SlaBasicInfo();
                basic.setSla(sla);
            }

            long curr = System.currentTimeMillis();
            Date agreedAt = new Date(curr - 1000);
            Date effectiveFrom = new Date(curr + 1000);
            Date effectiveUntil = new Date(curr + 8000);

            basic.setAgreedAt(agreedAt);
            basic.setEffectiveFrom(effectiveFrom);
            basic.setEffectiveUntil(effectiveUntil);

            basic.setTemplateId("template-" + curr);
            basic.setParties("parties::" + curr);

            s.persist(basic);

            tx.commit();
            s.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    @SuppressWarnings("unchecked")
    public void testClean() throws Exception {
        Session s = HibernateUtils.getSession();
        Transaction tx = s.beginTransaction();

        Query q = s.createQuery("select s from Sla s where s.slamId = '" + SLAPattern.slam + "'");

        Vector<Object> selected = new Vector<Object>(3, 2);
        List list = q.list();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Sla curr = (Sla) it.next();
            if (curr.getSlamId().equals(SLAPattern.slam)) {
                selected.add(curr);
            }
        }

        if (!selected.isEmpty()) {
            for (Object obj : selected) {
                Sla curr = (Sla) obj;
                SlaBasicInfo sbi = curr.getSlaBasicInfo();
                s.delete(sbi);
                s.delete(curr);

                break;
            }

            tx.commit();
        }

        s.close();
    }

    public void testSla()
    {
        try {
            Sla s = new Sla( "content", "sla-id" );
            s.getSlaBasicInfo();
        }
        catch (Exception e) {
        }
        
        try {
            Sla s = new Sla( "content", "sla-id", "5", null, null, null );
            s.getSlaBasicInfo();
        }
        catch (Exception e) {
        }
    }
    
    public static void main(String[] args) {
        SlaTest t = new SlaTest();

        try {
            // t.testSaveSLA();
            t.testClean();
        }
        catch (Exception e) {
        }
    }

    @SuppressWarnings("unused")
    private static class SLAPattern {
        public static String content = "SLA :: created from hibernate-java " + System.currentTimeMillis();
        public static String sla = "SLA_ID_254742";
        public static String slam = "SLAM_3524";
    }
}
