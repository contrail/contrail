/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 2758 $
 * @lastrevision   $Date: 2011-07-22 13:10:40 +0200 (pet, 22 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/IRegisterTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Calendar;

import junit.framework.TestCase;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContextAdapter;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.slaregistry.impl.SLARegistryIRegisterImpl;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * TestCase of SLARegistryIRegisterImpl
 * 
 * @see org.slasoi.gslam.slaregistry.impl.SLARegistryIRegisterImpl
 * @author Miguel Rojas (UDO)
 */
public class IRegisterTest extends TestCase {
    public void testSaveSLA() {
        try {
            SLARegistryIRegisterImpl object = new SLARegistryIRegisterImpl();
            object.setContext(getContext());

            SLA agreement = new SLA();
            agreement.setUuid(new UUID(AGREEMENT_ID));

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, -1);

            agreement.setAgreedAt(new TIME(cal));
            agreement.setEffectiveFrom(new TIME(cal));
            cal.add(Calendar.DAY_OF_YEAR, 1);
            agreement.setEffectiveUntil(new TIME(cal));
            agreement.setTemplateId(new UUID("template::" + System.currentTimeMillis()));

            Party[] parties = new Party[1];
            parties[0] = new Party(new ID("UDO"), new STND("MR"));
            agreement.setParties(parties);

            UUID[] dependencies = new UUID[3];
            for (int i = 0; i < dependencies.length; i++) {
                dependencies[i] = new UUID("AG-" + (i + 1));
            }

            SLAState state = SLAState.UNSIGNED;

            UUID newUUID = object.register(agreement, dependencies, state);
            assertTrue(newUUID != null && newUUID.equals(agreement.getUuid()));
        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public void testUpdateSLA() {
        try {
            SLARegistryIRegisterImpl object = new SLARegistryIRegisterImpl();
            object.setContext(getContext());

            SLA agreement = new SLA();
            agreement.setUuid(new UUID(AGREEMENT_ID + "-modified"));

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, -1);

            agreement.setAgreedAt(new TIME(cal));
            agreement.setEffectiveFrom(new TIME(cal));
            cal.add(Calendar.DAY_OF_YEAR, 1);
            agreement.setEffectiveUntil(new TIME(cal));
            agreement.setTemplateId(new UUID("template::" + System.currentTimeMillis()));

            Party[] parties = new Party[1];
            parties[0] = new Party(new ID("UDO"), new STND("MR"));
            agreement.setParties(parties);

            UUID[] dependencies = new UUID[3];
            for (int i = 0; i < dependencies.length; i++) {
                dependencies[i] = new UUID("AG-" + (i + 1));
            }

            SLAState state = SLAState.VIOLATED;

            UUID newUUID = object.update(new UUID(AGREEMENT_ID), agreement, dependencies, state);
            assertTrue(newUUID != null && newUUID.equals(agreement.getUuid()));
        }
        catch (Exception exc) {
            exc.printStackTrace();
            assertTrue(false);
            return;
        }

        assertTrue(true);
    }

    public static SLAManagerContext getContext() {
        SLAManagerContext context = new SLAManagerContextAdapter() {
            public String getSLAManagerID() throws SLAManagerContextException {
                return "IRegisterTest-SLAM";
            }
        };

        return context;
    }

    public static final String AGREEMENT_ID = "AGREEMENT-4-test";
}
