/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/Base64UtilsTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import junit.framework.TestCase;

import org.slasoi.gslam.slaregistry.impl.db.utils.Base64Utils;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

public class Base64UtilsTest extends TestCase {
    public void testBase64Utils() {
        try {
            Party[] parties = new Party[1];
            parties[0] = new Party(new ID("UDO"), new STND("MR"));
            Base64Utils.encode(parties);
        }
        catch (Exception e) {
        }
        try {
            SLA sl = TestBed.createSLA("SLA-test");
            Base64Utils.encode(sl);
        }
        catch (Exception e) {
        }
    }

    public void testExceptions() {
        try {
            Base64Utils.encode((Party[]) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.encode((SLA) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.ext_encode((SLA) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.encode((UUID[]) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.decodeParties((String) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.decodeSLA((String) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.ext_decodeSLA((String) null);
        }
        catch (Exception e) {
        }
        try {
            Base64Utils.decodeUUIDs((String) null);
        }
        catch (Exception e) {
        }
    }
}
