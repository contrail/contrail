/**
 * 
 */
package org.slasoi.gslam.core.builder;

import org.slasoi.gslam.core.monitoring.IMonitoringManager;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date Mar 23, 2010
 */
public interface IMonitoringManagerBuilder {
	/**
	 * @return an instance of the implementation of <code>IMonitoringManager</code>
	 */
	IMonitoringManager create();
}
