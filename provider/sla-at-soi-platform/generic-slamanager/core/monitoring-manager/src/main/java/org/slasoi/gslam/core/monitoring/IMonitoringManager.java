package org.slasoi.gslam.core.monitoring;

import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.slamodel.sla.SLA;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date Mar 23, 2010
 */
public interface IMonitoringManager {
	
	/**
	 * @param slaModel the SLA requirements in the form of an SLA-T Model
	 * @param monitoingFeatures an array of type IMonitoringFeature of candidate services for selection
	 * @return a MonitoringConfiguration object
	 */
	MonitoringSystemConfiguration checkMonitorability(SLA slaModel, ComponentMonitoringFeatures[] componentMonitoringFeatures);
}
