package org.slasoi.gslam.core.builder;

import org.slasoi.gslam.core.negotiation.SLARegistry;


/**
 * Interface for exposing an osgi-service that allows to create
 * SLARegistries.
 * 
 * @see org.slasoi.gslam.core.negotiation.SLARegistry
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public interface SLARegistryBuilder {
    public SLARegistry create();
}
