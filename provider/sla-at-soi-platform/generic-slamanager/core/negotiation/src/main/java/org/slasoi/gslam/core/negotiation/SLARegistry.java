package org.slasoi.gslam.core.negotiation;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

/**
 * SLARegistry defines the services to be provided by a generic SLA registry in the Generic-SLAM.
 * The IQuery interface defines query services that allow to perform several queries and filters
 * on this registry (see details {@link SLARegistry.IQuery})<p>
 * IRegister interface provides functionality for saving and updating of SLAs into this registry
 * (see details {@link SLARegistry.IRegister})
 *   
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public interface SLARegistry {
    
    public IRegister   getIRegister();
    public IQuery      getIQuery   ();
    public IAdminUtils getIAdmin   ();

    /**
     * Helper-Inner class for SLARegistry.  Defines the services for storing/updating the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public interface IRegister {
        
        /**
         * Stores the specified SLA, its dependencies and sets its current 'status'.<br>
         * <br>
         */
        public UUID register(SLA agreement, UUID[] dependencies, SLAState state) throws RegistrationFailureException;

        /**
         * Does the provisioning of the specified SLA ('unsigned') and changes its status to 'OBSERVED'.<br>
         * <br>
         */
        public UUID sign(UUID id) throws UpdateFailureException;
        
        /**
         * Updates the specified agreement associated to 'id', its dependencies and sets its new status
         */
        public UUID update(UUID id, SLA agreement, UUID[] dependencies, SLAState state) throws UpdateFailureException;
    }

    /**
     * Helper-Inner class for SLARegistry.  Defines the services for querying the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public interface IQuery {
        /**
         * Returns an array of existing SLAs with specified 'ids'
         */
        public SLA[] getSLA(UUID[] ids) throws InvalidUUIDException;

        /**
         * Returns an array of SLAs based on the specified 'slatID' (template id)
         */
        public SLA[] getSLAsByTemplateId(UUID slatId) throws InvalidUUIDException;

        /**
         * Returns an array of SLAs containing the specified 'party'
         */
        public SLA[] getSLAsByParty(UUID party) throws InvalidUUIDException;

        /**
         * Returns an array of dependencies of an existing SLA with specified 'id'
         */
        public UUID[] getDependencies(UUID id) throws InvalidUUIDException;

        /**
         * Returns an array of SLAStateInfo representing the history of the status of the specified
         * id (SLA id)
         */
        public SLAStateInfo[] getStateHistory(UUID id, boolean current) throws InvalidUUIDException,
                InvalidStateException;

        /**
         * Returns an array of existing SLAs (just their identifiers) with specified 'states'
         */
        public UUID[] getSLAsByState(SLAState[] states, boolean inclusive) throws InvalidStateException;

        /**
         * Returns an array of dependable SLA-ids of the queried id (SLA id)
         */
        public UUID[] getUpwardDependencies(UUID id) throws InvalidUUIDException;

        /**
         * Returns an array of the minimal information related to the specified SLA-ids.
         * 
         * @see org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo
         */
        public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] ids) throws InvalidUUIDException;
    }

    public interface IUtilities {
        public UUID[] getUUIDsFromSLAs();
    }

    /**
     * Helper-Inner class for SLARegistry.  Defines the services for performing administrative 
     * operations on the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public interface IAdminUtils {
        public void clearAll();
        public void clear(UUID[] ids);
    }
    
    /**
     * Enumeration which defines the possible states of a SLA
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public enum SLAState {
        OBSERVED,
        VIOLATED,
        WARN,
        EXPIRED,
        RENEGOCIATED,
        UNSIGNED
    }

    /**
     * Helper class for IQuery services
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class SLAStateInfo {
        public SLAState state;
        public Date date;

        public String toString() {
            return "SLAStateInfo : (" + state + ", " + date + ")";
        }
    }

    /**
     * Helper class for IQuery services. Used just internally
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class Dependency implements Serializable {
        public String depending;
        public Vector<String> antecedents = new Vector<String>(5, 2);
    }

    /**
     * Helper class for IQuery services. Used just internally
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class SLAMinimumInfo implements Serializable {
        public UUID SLAID;
        public TIME agreedAt;
        public TIME effectiveFrom;
        public TIME effectiveUntil;
        public UUID templateID;
        public Party[] parties;

        public String toString() {
            return "SLAMinimumInfo : ( slaID=" + SLAID + "," + "templateID=" + templateID + "," + "agreedAt="
                    + get(agreedAt) + "," + "effectiveFrom=" + get(effectiveFrom) + "," + "effectiveUntil="
                    + get(effectiveUntil) + "," + "parties=" + parties + " )";
        }

        private static String get(TIME time) {
            if (time == null)
                return "";

            Calendar value = time.getValue();
            if (value != null)
                return value.getTime().toString();

            return "";
        }
    }

    /**
     * Helper inner class for handling exceptions in the SLARegistry
     * 
     * @see RegistrationFailureException
     * @see UpdateFailureException
     * @see InvalidUUIDException
     * @see InvalidStateException
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class SLARegistryException extends Exception {
        public SLARegistryException(String cause) {
            super(cause);
        }
    }

    /**
     * Helper inner class for handling exceptions while registering operations in the SLARegistry
     *
     * @author Miguel Rojas (UDO)
     *
     */
    public class RegistrationFailureException extends SLARegistryException {
        public RegistrationFailureException(String cause) {
            super(cause);
        }
    }

    /**
     * Helper inner class for handling exceptions while updating operations in the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class UpdateFailureException extends SLARegistryException {
        public UpdateFailureException(String cause) {
            super(cause);
        }
    }

    /**
     * Helper inner class for handling exceptions while validation of SLAs in the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class InvalidUUIDException extends SLARegistryException {
        public InvalidUUIDException(String cause) {
            super(cause);
        }
    }

    /**
     * Helper inner class for handling exceptions while status validation of SLAs in the SLARegistry
     * 
     * @author Miguel Rojas (UDO)
     *
     */
    public class InvalidStateException extends Exception {
        public InvalidStateException(String cause) {
            super(cause);
        }
    }
}
