package org.slasoi.gslam.core.builder;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;

public interface SLATemplateRegistryBuilder
{
    public SLATemplateRegistry create();
}
