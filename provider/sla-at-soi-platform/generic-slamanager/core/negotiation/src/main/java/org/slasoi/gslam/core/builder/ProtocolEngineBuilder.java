package org.slasoi.gslam.core.builder;

import org.slasoi.gslam.core.negotiation.ProtocolEngine;

public interface ProtocolEngineBuilder
{
    public ProtocolEngine create();
}
