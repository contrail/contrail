package org.slasoi.gslam.core.builder;

import java.util.Hashtable;

import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;

public interface ISyntaxConverterBuilder
{
    public Hashtable<SyntaxConverterType, ISyntaxConverter> create();
    public void createWS();
}
