package org.slasoi.gslam.core.negotiation;

import org.slasoi.gslam.core.control.IControl;

/**
 * The <code>ProtocolEngine</code> interface provides methods to get implementations for <code>INegotiation</code> and
 * <code>IControl</code> interfaces.
 * <p>
 * This interface is implemented by <code>ProtocolEngineServices</code>
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 * @see org.slasoi.gslam.protocolengine.ProtocolEngineServices
 */
public interface ProtocolEngine {
    
    /**
     * Gives a handle on <code>INegotiation</code> interface of current SLA-Manager.
     * 
     * @return INegotiation implementation
     */
    public INegotiation getINegotiation();

    /**
     * Gives a handle on <code>IControl</code> interface of current SLA-Manager.
     * 
     * @return IControl implementation
     */
    public IControl getIControl();
    
    public Object getNegotiationManager(String negotiationId);

    public Object retrieveProfile(String profileId) throws Exception;
    public void appendBusinessHistoryToProfile(String profileId, Object businessHistory) throws Exception;
    public void updateProfile(Object profile) throws Exception;
    public int createProfile(Object profile) throws Exception;

}
